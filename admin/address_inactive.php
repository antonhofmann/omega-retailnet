<?php
/********************************************************************

    address_inactive.php

    Creation and mutation of address records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-29
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-10-17
    Version:        1.0.4

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("user.php");


$client_type = 0;
$sql = "select * from addresses where address_id = " . id();
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$client_type = $row["address_type"];
}

if(param("address_country"))
{
	$sql_places = "select place_id, concat(place_name, ' (', province_canton, ')') as place " . 
				  "from places " .
				  "left join provinces on province_id = place_province " . 
				  "where place_country = " . param("address_country") . " order by place_name";
}
else
{
	$sql_places = "select place_id, concat(place_name, ' (', province_canton, ')') as place " . 
				  "from places " .
				  "left join provinces on province_id = place_province " . 
				  "where place_country = {address_country} order by place_name";
}

// Build form

$form = new Form("addresses", "address");

$form->add_hidden("af", param("af"));

$form->add_section("Name and address");
$form->add_list("address_type", "Address Type",
    "select address_type_id, address_type_name from address_types  where address_type_id <> 7 order by address_type_name", SUBMIT | NOTNULL);

$form->add_edit("address_number", "Address Number");
$form->add_edit("address_legnr", "Legal Number");
$form->add_edit("address_sapnr", "SAP Number");
$form->add_edit("address_shortcut", "Shortcut", NOTNULL | UNIQUE);
$form->add_edit("address_legal_entity_name", "Legal Entity Name");
$form->add_edit("address_company", "Company", NOTNULL);
$form->add_edit("address_company2", "");
$form->add_edit("address_address", "Address");
$form->add_edit("address_address2", "");


$form->add_list("address_country", "Country*",
    "select country_id, country_name from countries order by country_name", NOTNULL | SUBMIT);

$form->add_edit("address_zip", "Zip");
$form->add_list("address_place_id", "City*", $sql_places, NOTNULL | SUBMIT);
$form->add_edit("address_place", "", DISABLED);

$form->add_section("Communication");
$form->add_edit("address_phone", "Phone");
$form->add_edit("address_fax", "Fax");
$form->add_edit("address_email", "Email");
$form->add_edit("address_contact_name", "Contact Name");
$form->add_edit("address_website", "Website");



if($client_type == 1) // client
{
	$sql_invoice_addresses = "select address_id, concat(country_name, ': ', address_company) as company from addresses " .
                 "left join countries on country_id = address_country " .
				 "where (address_active = 1 and address_type = 1) or  {address_invoice_recipient}=address_id order by country_name, address_company";


	$form->add_section("Different Notify Address");
	$form->add_comment("Please indicate the corresponding notify address in case invoices are not sent to the client.");
	$form->add_list("address_invoice_recipient", "Company",$sql_invoice_addresses);
}
else
{
	$form->add_hidden("address_invoice_recipient");
}


$form->add_section("Other information");

if(param("address_type") == 1) // client
{
	$form->add_hidden("address_currency", 1);
}
else
{
	$form->add_list("address_currency", "Currency","select currency_id, currency_symbol from currencies order by currency_symbol");
}


if($client_type == 1)
{
	$form->add_list("address_client_type", "Client Type",
		"select client_type_id, client_type_code from client_types order by client_type_code");
}
else
{
	$form->add_hidden("address_client_type");
}

$form->add_list("address_contact", "Contact",
    "select user_id, concat(user_name, ' ', user_firstname) from users " .
    "where user_active = 1 and user_address = " . id() . " order by user_name, user_firstname", OPTIONAL);

$form->add_section();
//$form->add_checkbox("address_canbefranchisor", "Can Be Franchisor", false);
$form->add_edit("address_company_is_partner_since", "Company is partner since", 0, "", TYPE_DATE);
$form->add_checkbox("address_canbefranchisee", "Can Be Franchisee", false);
$form->add_checkbox("address_showinposindex", "Show Address in POS Index", false);


$form->add_comment("Please indicate the corresponding client address in case this address is a franchisee address.");

$sql_addresses = "select address_id, concat(country_name, ': ', address_company) as company from addresses " .
                 "left join countries on country_id = address_country " .
				 "where address_type = 1  order by country_name, address_company";

$form->add_list("address_parent", "Parent*",$sql_addresses);

$form->add_section();
$form->add_checkbox("address_active", "Address in Use", true);
$form->add_checkbox("address_checked", "Address checked", true);


$form->add_section('Merchandising Planning');
$form->add_checkbox("address_involved_in_planning", "can order merchandising material", false, 0, 'Merchandising');
$form->add_checkbox("address_can_own_independent_retailers", "can own independent retailers", false, 0, 'Retailers');
$form->add_edit("address_mps_customernumber", "MPS Customer Number");
$form->add_edit("address_mps_shipto", "MPS Ship To Number");


$form->add_section('Retail Environment Development');
$form->add_checkbox("address_involved_in_red", "can be involved in retail environment development", false, 0, 'RED');


$form->add_subtable("users", "<br />Active Users", "user.php", "Name",
    "select user_id, user_firstname as Firstname, user_name as Name, user_phone as Phone, user_email as Email " .
    "from users " .
    "where user_active = 1 and user_address = " . id() . " " .
    "order by user_name, user_firstname");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button("back", "Back");
$form->add_button("add_user", "Add User", "user.php?address=" . id(), OPTIONAL);

$form->add_validation("is_shortcut({address_shortcut})", "The shortcut is invalid. It may only contain lower case characters a-z, digits and the underscore");
//$form->add_validation("is_email_address({address_email})", "The email address is invalid.");
//$form->add_validation("is_web_address({address_website})", "The website url is invalid.");

// Populate form and process button clicks

$form->populate();

if($form->value("address_canbefranchisee") == 1)
{
	$form->add_validation("{address_parent}", "You must indicate a parent address for a franchisee address.");
}
$form->process();

if($form->button("address_type"))
{
	if($form->value("address_type") == 1) // client
	{
		$form->value("address_canbefranchisee", 1);
		$form->value("address_showinposindex", 1);
	}
	elseif($form->value("address_type") == 12) //RED external partner
	{
		$form->value("address_involved_in_red", 1);
	}
}
elseif($form->button("address_place_id"))
{
	$sql = "select place_name from places where place_id = " . dbquote($form->value("address_place_id"));
	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$form->value("address_place", $row["place_name"]);
	}

}
elseif($form->button("address_country"))
{
	$form->value("address_place", "");
	$form->value("address_place_id", "");
	$form->value("address_zip", "");
}
elseif($form->button("back"))
{
	redirect("addresses_inactive.php?af=" . param("af"));
}

// Render page

$page = new Page("addresses");

$page->header();
$page->title(id() ? "Edit Retail Net Address" : "Add Retail Net Address");
$form->render();
$page->footer();

?>