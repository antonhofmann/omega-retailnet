<?php
/********************************************************************

    snagging_elements.php

    Object Definitions of Snagging-List elements

    Created by:     Oliver Hofer (oliver.hofer@pageagent.com)
    Date created:   2003-05-19
    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date modified:  2003-05-29
    Version:        1.0.0

    Copyright (c) 2003, Swatch AG, All Rights Reserved.

*********************************************************************/

define ("SNAGGING_TITLE_FONT_SIZE", 14);
define ("SNAGGING_SECTION_FONT_SIZE", 9);
define ("SNAGGING_CAPTION_FONT_SIZE", 8);
define ("SNAGGING_FONT_SIZE", 7);
define ("SNAGGING_LINE_OFFSET" , 5);

/********************************************************************
    SnaggingList
*********************************************************************/

class SnaggingList
{
    var $elements;
    var $name;
    var $template;
    var $list;
    var $product_line;

    function SnaggingList($list, $template = 0)
    {
        $this->list = $list;
        $this->elements = array();

        if ($list)
        {
            // get template and list properties from list-table
            
            $sql = "select snagging_list_name, snagging_list_template ". 
                   "from snagging_lists " .
                   "where snagging_list_id = " . $list;

            $res = mysql_query($sql) or dberror($sql);
            if (mysql_num_rows($res) > 0)
            {
                $row = mysql_fetch_assoc($res); 
                $this->name = $row['snagging_list_name'];
                $this->template = $row['snagging_list_template'];
            }
        }
        else
        {
            $this->template = $template;
        }

        // product line name

        if ($this->template)
        {
            $sql = "select product_line_name " .
                   "from snagging_templates " .
                   "left join product_lines on product_line_id = snagging_template_product_line ".
                   "where snagging_template_id = " . $this->template;
            $res = mysql_query($sql) or dberror($sql);
            $row = mysql_fetch_assoc($res);
            $this->product_line = $row['product_line_name'];
        }
    }

    function load_structure()
    {
        if (!$this->template)
        {
            error("Invalid Snagging-List");
        }

        // Select template data

        $sql = "select snagging_template_element_id, " .
               "    snagging_template_element_name, snagging_element_type_name " . 
               "from snagging_template_elements " .
               "left join snagging_element_types on snagging_template_element_type = " .
               "    snagging_element_type_id " .
               "where snagging_template_element_template = " .$this->template ." " .
               "order by snagging_template_element_priority ";

        $res = mysql_query($sql) or dberror($sql);;
        while ($row = mysql_fetch_assoc($res))
        {
            if (class_exists($row['snagging_element_type_name']))
            {
                 $tmp_obj = new $row['snagging_element_type_name'](
                            $row['snagging_template_element_id'], 
                            $row['snagging_template_element_name']);
                 $this->elements[] = $tmp_obj;
            }
            else
            {
                $this->elements[] = new SnaggingElement;
            }
        }
    }

    function populate()
    {
    }

    function add_element($type = 0)
    {

    }

    function edit()
    {
        foreach ($this->elements as $tmp_obj)
        {
            $tmp_obj->edit();
        }
    }

    function render($format = "")
    {
        switch ($format)
        {
            case "html":
            break;
            case "form":
            break;
            default: // pdf
                global $pdf;

                //  Title

                $pdf->SetFont("Arial", "B", 18);
                $pdf->SetXY(COLUMN1_XPOS, TITLE_YPOS);
                $pdf->Cell(0, 8, $this->product_line . (($this->name)? ":" . $this->name: ""), 1);

                $ypos = GROUP1_YPOS;
                if (isset($this->elements) && is_array($this->elements))
                {
                    foreach ($this->elements as $tmp_obj)
                    {
                        $ypos += $tmp_obj->get_top_margin();
                        $tmp_obj->render_pdf(COLUMN1_XPOS, $ypos);
                        $ypos += LINE_HEIGHT;
                    }
                }
            break;
        }
    }
}

/********************************************************************
    Base Class SnaggingElement
*********************************************************************/

class SnaggingElement
{
    var $caption;
    var $font_size;
    var $tmplate_id;
    var $list;
    var $element_id;

    function SnaggingElement($element_id, $caption = "undefined")
    {
        $this->caption = $caption;
        $this->font_size = SNAGGING_CAPTION_FONT_SIZE;
        $this->tmplate_id = 0;
        $this->list = 0;
        $this->element_id = $element_id;
    }

    function set_list($id)
    {
        $this->template_id = $id;
    }

    function render()
    {
        echo "Unknown element";
    }

    function render_pdf($xpos, & $ypos)
    {
        global $pdf;

        $pdf->SetFont("Arial", "", SNAGGING_FONT_SIZE);
        $pdf->SetXY($xpos, $ypos);
        $pdf->Cell(0, 8, "Overload Method! (Caption=" .$this->caption . ")");
    }

    function save()
    {
    }

    function edit()
    {
        global $form;
        $form->add_comment("Edit method not implemented:" . $this->caption . "(" .$this->element_id . ")");
    }

    function get_top_margin()
    {
        return 0;
    }

    function draw_caption($xpos, & $ypos, $style = "")
    {
        global $pdf;

        $pdf->SetFont("Arial", $style, $this->font_size);
        $pdf->SetXY($xpos + 2, $ypos);
        $pdf->Cell(0, 8, $this->caption, 0);
    }
}

/********************************************************************
    Title
*********************************************************************/

class Title extends SnaggingElement
{
    function Title ($element_id, $caption = "undefined")
    {
        parent::SnaggingElement($element_id, $caption);
        $this->font_size = SNAGGING_TITLE_FONT_SIZE;
    }

    function render_pdf($xpos, & $ypos)
    {
        $this->draw_caption($xpos, $ypos, "B");
        $ypos += 2;
    }

    function get_top_margin()
    {
        return 1;
    }

    function edit()
    {
        global $form;
        $form->add_section($this->caption);
    }
}

/********************************************************************
    Section
*********************************************************************/

class Section extends SnaggingElement
{
    function Section ($element_id, $caption = "undefined")
    {
        parent::SnaggingElement($element_id, $caption);
        $this->font_size = SNAGGING_SECTION_FONT_SIZE;
    }

    function render_pdf($xpos, & $ypos)
    {
        $this->draw_caption($xpos, $ypos, "B");
        $ypos += 1;
    }

    function get_top_margin()
    {
        return 4;
    }

    function edit()
    {
        global $form;
        $form->add_section($this->caption);
    }
}

/********************************************************************
    SingleLine
*********************************************************************/

class SingleLine extends SnaggingElement
{
    function SingleLine($element_id, $caption = "undefined")
    {
        parent::SnaggingElement($element_id, $caption);
    }
    
    function render_pdf($xpos, & $ypos)
    {
        global $pdf;

        $pdf->SetXY($xpos, $ypos);
        $pdf->Line(COLUMN1_XPOS + 3,$ypos + 4, COLUMN2_XPOS - COLUMN_DATA_OFFSET, $ypos + 4);
        $pdf->Line(COLUMN2_XPOS,$ypos + 4, PAGE_FULL_WIDTH, $ypos + 4 );
    }

    function edit()
    {
        global $form;
        $form->add_edit("sl_edit_" . $this->element_id , $this->caption, 0);
    }
}

/********************************************************************
    CheckboxLine
*********************************************************************/

class CheckboxLine extends SnaggingElement
{
    function render_pdf($xpos, & $ypos)
    {
        global $pdf;

        $pdf->Rect(COLUMN3_XPOS, $ypos + 3, 3 ,3);
        $pdf->Line(COLUMN4_XPOS - 7, $ypos + 6, PAGE_FULL_WIDTH, $ypos + 6 );
        $this->draw_caption($xpos + 2, $ypos);
    }
    
    function edit()
    {
        global $form;
        $form->add_edit("cbl_edit_". $this->element_id, $this->caption, 0);
    }
}

/********************************************************************
    TrippleCheckboxLine
*********************************************************************/

class TrippleCheckboxLine extends SnaggingElement
{
    function render_pdf($xpos, & $ypos)
    {
        global $pdf;

        $this->draw_caption($xpos + 2, $ypos);
        $pdf->Line(COLUMN4_XPOS, $ypos + SNAGGING_LINE_OFFSET, PAGE_FULL_WIDTH, 
                   $ypos + SNAGGING_LINE_OFFSET );
        $pdf->Rect(COLUMN3_XPOS, $ypos + 3, 3 ,3);
        $pdf->Rect(COLUMN3_XPOS + 3.5, $ypos + 3, 3 ,3);
        $pdf->Rect(COLUMN3_XPOS + 7, $ypos + 3, 3 ,3);
    }
    
    function edit()
    {
        global $form;

        $values = array(1 => "Excellent", "Acceptable", "Poor");
        $form->add_radiolist("tripplecheckboxline_chk_" . $this->element_id, 
                             $this->caption, $values, 0 );
        $form->add_edit("tripplecheckboxline_edit_". $this->element_id, "", 0);
    }
}

/********************************************************************
    SubSection
*********************************************************************/

class SubSection extends SnaggingElement
{
    function render_pdf($xpos, & $ypos)
    {
        $this->draw_caption($xpos + 2, $ypos, "B");
    }

    function edit()
    {
        global $form;
        $form->add_section($this->caption);
    }

}

/********************************************************************
    TwoCheckboxLine
*********************************************************************/

class TwoCheckboxLine extends SnaggingElement
{
    function render_pdf($xpos, & $ypos)
    {
        global $pdf;

        $pdf->Rect(COLUMN3_XPOS, $ypos + 3, 3 ,3);
        $pdf->Line(COLUMN4_XPOS - 7, $ypos + 6, PAGE_FULL_WIDTH, 
                   $ypos + 6 );
        $this->draw_caption($xpos + 2, $ypos);

        $ypos += LINE_HEIGHT;
        $pdf->Rect(COLUMN3_XPOS, $ypos + 3, 3 ,3);
        $pdf->Line(COLUMN4_XPOS - 7, $ypos + 6, PAGE_FULL_WIDTH, 
                   $ypos + 6 );
    }
}


/********************************************************************
    ThreeCheckboxLine
*********************************************************************/

class ThreeCheckboxLine extends SnaggingElement
{

}

/********************************************************************
    GenericComment
*********************************************************************/

class GenericComment extends SnaggingElement
{

}

/********************************************************************
    ThreeTrippleCheckboxLines
*********************************************************************/

class ThreeTrippleCheckboxLines extends SnaggingElement
{
    function render_pdf($xpos, & $ypos)
    {
        global $pdf;

        $this->draw_caption($xpos + 2, $ypos);

        for ($i = 0; $i < 3; $i++)
        {
            $yoffset = $i * (LINE_HEIGHT + $this->get_top_margin());
            $pdf->Line(COLUMN4_XPOS, $ypos + SNAGGING_LINE_OFFSET + $yoffset, 
                             PAGE_FULL_WIDTH, $ypos + SNAGGING_LINE_OFFSET +$yoffset);
            $pdf->Rect(COLUMN3_XPOS, $ypos + 3 +$yoffset, 3 ,3);
            $pdf->Rect(COLUMN3_XPOS + 3.5, $ypos + 3 + $yoffset, 3 ,3);
            $pdf->Rect(COLUMN3_XPOS + 7, $ypos + 3 + $yoffset, 3 ,3);
        }
        $ypos += $yoffset;
    }

    function edit()
    {
        global $form;

        $values = array(1 => "Excellent", "Acceptable", "Poor");

        $form->add_radiolist("threetripplecheckboxlines_chk_1_" . $this->element_id,
                             $this->caption, $values, 0 );
        $form->add_edit("threetripplecheckboxlines_edit_1_". $this->element_id, "", 0);
        $form->add_radiolist("threetripplecheckboxlines_1_chk_2_" . $this->element_id, 
                             "", $values, 0 );
        $form->add_edit("threetripplecheckboxlines_1_edit_2_". $this->element_id, "", 0);
        $form->add_radiolist("threetripplecheckboxlines_1_chk_3_" . $this->element_id,
                             "", $values, 0 );
        $form->add_edit("threetripplecheckboxlines_1_edit_3_". $this->element_id, "", 0);
    }

}

/********************************************************************
    Address
*********************************************************************/

class Address extends SnaggingElement
{

    function render_pdf($xpos, & $ypos)
    {
        global $pdf;

        $pdf->SetFont("Arial", "", SNAGGING_CAPTION_FONT_SIZE);
        $pdf->SetXY($xpos + 4, $ypos);
        $pdf->Cell(0, 8, "Address", 0);
 
        $pdf->Line(COLUMN2_XPOS -4, $ypos + SNAGGING_LINE_OFFSET, 
                   PAGE_FULL_WIDTH, $ypos + SNAGGING_LINE_OFFSET);

        $ypos += LINE_HEIGHT;
        $pdf->Line($xpos + 5, $ypos + SNAGGING_LINE_OFFSET, 
                   PAGE_FULL_WIDTH, $ypos + SNAGGING_LINE_OFFSET);

        $ypos += LINE_HEIGHT;
        $pdf->SetXY($xpos + 4, $ypos);
        $pdf->Cell(0, 8, "Zip Code / City", 0);
        $pdf->Line(COLUMN2_XPOS + 4, $ypos + SNAGGING_LINE_OFFSET, 
                   COLUMN3_XPOS + 4, $ypos + SNAGGING_LINE_OFFSET);
        $pdf->Line(COLUMN3_XPOS + 6, $ypos + SNAGGING_LINE_OFFSET, 
                   PAGE_FULL_WIDTH, $ypos + SNAGGING_LINE_OFFSET);

        $ypos += LINE_HEIGHT;
        $pdf->Line($xpos + 5, $ypos + SNAGGING_LINE_OFFSET, 
                   PAGE_FULL_WIDTH, $ypos + SNAGGING_LINE_OFFSET);

        $ypos += LINE_HEIGHT;
        $pdf->SetXY($xpos + 4, $ypos);
        $pdf->Cell(0, 8, "Targeted Opening Date", 0);
        $pdf->Line(COLUMN3_XPOS - 8, $ypos + SNAGGING_LINE_OFFSET, 
                   COLUMN4_XPOS + 20, $ypos + SNAGGING_LINE_OFFSET);
    }
}

/********************************************************************
    Surveyor
*********************************************************************/

class Surveyor extends SnaggingElement
{

}

/********************************************************************
    Issued
*********************************************************************/

class Issued extends SnaggingElement
{
    function render_pdf($xpos, & $ypos)
    {
        global $pdf;
        $pdf->Rect($xpos, $ypos + 1, (PAGE_FULL_WIDTH - $xpos), (3 * LINE_HEIGHT));

        $pdf->SetFont("Arial", "B", SNAGGING_CAPTION_FONT_SIZE);
        $pdf->SetXY($xpos + 4, $ypos);
        $pdf->Cell(0, 8, "Name");
        $pdf->Line(COLUMN2_XPOS, $ypos + SNAGGING_LINE_OFFSET, 
                   PAGE_FULL_WIDTH -4, $ypos + SNAGGING_LINE_OFFSET);


        $ypos += LINE_HEIGHT;
        $pdf->SetXY($xpos + 4, $ypos);
        $pdf->Cell(0, 8, "Company");
        $pdf->Line(COLUMN2_XPOS, $ypos + SNAGGING_LINE_OFFSET, 
                   PAGE_FULL_WIDTH -4, $ypos + SNAGGING_LINE_OFFSET);

        $ypos += LINE_HEIGHT;
        $pdf->SetXY($xpos + 4, $ypos);
        $pdf->Cell(0, 8, "Date");
        $pdf->Line(COLUMN2_XPOS, $ypos + SNAGGING_LINE_OFFSET, 
                   COLUMN4_XPOS -4, $ypos + SNAGGING_LINE_OFFSET);

    }

}

/********************************************************************
    Comment
*********************************************************************/

class Comment extends SnaggingElement
{
    function render_pdf($xpos, & $ypos)
    {
        $this->draw_caption($xpos + 2, $ypos, "");
    }
}

/********************************************************************
    CommentLine
*********************************************************************/

class CommentLine extends SnaggingElement
{
    function render_pdf($xpos, & $ypos)
    {
        global $pdf;

        $this->draw_caption($xpos + 2, $ypos, "");
        $pdf->Line(COLUMN3_XPOS, $ypos + SNAGGING_LINE_OFFSET, 
                   PAGE_FULL_WIDTH, $ypos + SNAGGING_LINE_OFFSET);
    }
}


/********************************************************************
    Spacer
*********************************************************************/

class Spacer extends SnaggingElement
{

    function render_pdf($xpos, & $ypos)
    {
        $$ypos =+ LINE_HEIGHT;
    }

    function edit()
    {
    }
}

/********************************************************************
    PageBreak
*********************************************************************/

class PageBreak extends SnaggingElement
{
    function render_pdf($xpos, & $ypos)
    {
        global $pdf;

        $pdf->SetFont("Arial", "", SNAGGING_CAPTION_FONT_SIZE);
        $pdf->SetXY(195, 275);
        $pdf->Cell(0, 8, $pdf->PageNo(), 0);

        $pdf->Image("pictures/jelly_logo.png", 100, 270, 22.5,22.2);
        $pdf->AddPage("P");
        $ypos = PAGE_TOP_YPOS;
    }

    function edit()
    {
    }
}


?>