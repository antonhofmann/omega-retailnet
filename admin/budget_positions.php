<?php
/********************************************************************

    budget_positions.php

    Lists standard budget positions for editing.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-15
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-09-16
    Version:        1.0.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("budget_position.php");

$list = new ListView("select item_id, item_code, item_description, item_type_name " .
                     "from items left join item_types on item_type = item_type_id",
                     LIST_HAS_HEADER | LIST_SHOW_COUNT);

$list->set_entity("budget positions");
$list->set_filter("item_type >= " . ITEM_TYPE_EXCLUSION);
$list->set_sorter("item_priority");

$list->add_column("item_type_name", "Type");
$list->add_column("item_code", "Code", "budget_position.php");
$list->add_column("item_description", "Description");

$list->add_button(LIST_BUTTON_NEW, "New", "budget_position.php");

$list->process();

$page = new Page("budget_positions");

$page->header();
$page->title("Exclusions And Notifications");
$list->render();
$page->footer();

?>