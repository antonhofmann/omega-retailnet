<?php
/********************************************************************

    addresses.php

    Lists addresses for editing.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-29
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-08-19
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("address3rd_inactive.php");



$images = array();
$sql_images = "select address_id " . 
			  "from addresses " .
			  "where address_active = 0 and address_type = 7 and address_checked = 0";

$res = mysql_query($sql_images) or dberror($sql_images);

while ($row = mysql_fetch_assoc($res))
{
    $images[$row["address_id"]] = "/pictures/wf_warning.gif";
}


/********************************************************************
    Create Form
*********************************************************************/
$address_filter = array();


//franchisees
$sql = "select distinct address_country, country_name from addresses " . 
       "left join countries on country_id = address_country " . 
	   "where address_active = 0 and address_type = 7 and (address_canbefranchisee = 1 or address_canbefranchisee_worldwide = 1) " . 
	   "order by country_name";

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
	$address_filter["f_" . $row['address_country']] = "Franchisees " . $row['country_name'];
}

//independent retailers
$sql = "select distinct address_country, country_name from addresses " . 
       "left join countries on country_id = address_country " . 
	   "where address_active = 1 and address_is_independent_retailer = 1 " . 
	   "order by country_name";

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
	$address_filter["i_" . $row['address_country']] = "Independent retailers " . $row['country_name'];
}

$address_filter["o"] = "Others";


//create form
$form = new Form("addresses", "address");

$form->add_section("List Filter Selection");

if(param("af"))
{
	$form->add_list("af", "Address Filter", $address_filter, SUBMIT | NOTNULL, param("af"));
}
else
{
	$form->add_list("af", "Address Filter", $address_filter, SUBMIT | NOTNULL, "f_1");
}


/********************************************************************
    Create List
*********************************************************************/ 

$sql = "select address_id, address_shortcut, address_company, address_zip,  " .
	   "    address_place, country_name, province_canton, " .
	   "IF(address_canbefranchisee = 1 or address_canbefranchisee_worldwide = 1, 'yes', '') as franchisee, " .
	   "IF(address_is_independent_retailer = 1, 'yes', '') as retailer " .
	   "from addresses " .
	   "left join countries on address_country = country_id " . 
	   "left join places on place_id = address_place_id " .
  	   "left join provinces on province_id = place_province ";

if(param('af')) {
	
	$type = substr(param('af'), 0, 1);
	
	if($type == 'o') // other
	{
		$list_filter = "address_active = 0 and (address_type = 7 or address_type is null) and address_canbefranchisee <> 1 and address_canbefranchisee_worldwide <> 1 and address_is_independent_retailer <> 1";
	}
	else
	{
		$country = substr(param('af'), 2, strlen(param('af')));

		if($type == 'f') // franchisee
		{
			$list_filter = "address_active = 0 and address_type = 7 and (address_canbefranchisee = 1 or address_canbefranchisee_worldwide = 1) and address_country = " . $country;
		}
		else // independent retailer
		{
			$list_filter = "address_active = 0 and address_type = 7 and address_is_independent_retailer = 1 and address_country = " . $country;
		}
	}
	
	
}
else
{
	$list_filter = "address_active = 0 and address_type = 7 and address_is_independent_retailer = 1 and address_country = 1";
}



$list = new ListView($sql);

$list->set_entity("addresses");
$list->set_order("address_shortcut");
$list->set_filter($list_filter);

$list->add_image_column("notchecked", "", 0, $images);
$list->add_column("address_shortcut", "Shortcut", "address3rd_inactive.php?af=" . param("af"));
$list->add_column("address_company", "Company");
$list->add_column("franchisee", "Franchisee");
$list->add_column("retailer", "Retailer");
$list->add_column("province_canton", "Province");
$list->add_column("address_zip", "Zip");
$list->add_column("address_place", "City");
$list->add_column("country_name", "Country");


//$list->add_button(LIST_BUTTON_NEW, "New", "address3rd.php?af=" . param("af"));
$list->add_button(LIST_BUTTON_FILTER, "Filter");
$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove Filter");

$list->process();


/********************************************************************
    Populate and process button clicks
*********************************************************************/ 
$form->populate();
$list->process();


$page = new Page("addresses");

$page->header();
$page->title("Third Party Inactive Addresses");
$form->render();
$list->render();
$page->footer();

?>
