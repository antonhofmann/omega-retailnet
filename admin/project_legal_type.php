<?php
/********************************************************************

    project_legal_type.php

    Editdit project legal type.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-03-22
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-03-22
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("project_costtypes", "project_costtypes");


$form->add_section();

$form->add_label("project_costtype_text", "Name");
$form->add_comment("Indicate recipient's email on creation of a new project of the above legal type.");
$form->add_edit("project_costtype_email1", "Recipient 1");
$form->add_edit("project_costtype_email2", "Recipient 2");
$form->add_edit("project_costtype_email3", "Recipient 3");
$form->add_edit("project_costtype_email4", "Recipient 4");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->add_validation("is_email_address({project_costtype_email1})", "The email address 1 is invalid.");
$form->add_validation("is_email_address({project_costtype_email2})", "The email address 2 is invalid.");
$form->add_validation("is_email_address({project_costtype_email3})", "The email address 3 is invalid.");
$form->add_validation("is_email_address({project_costtype_email4})", "The email address 4 is invalid.");

$form->populate();
$form->process();

$page = new Page("project_cost_types");

$page->header();
$page->title(id() ? "Edit Legal Type" : "Add Legal Type");
$form->render();
$page->footer();

?>
