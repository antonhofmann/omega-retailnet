<?php
/********************************************************************

    database_info.php

    Lists informations about the database.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-12-04
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2002-12-04
    Version:        1.0.0

    Copyright (c) 2003, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("database_ino.php");




/********************************************************************
    Create Table Form
*********************************************************************/
$form = new Form("", "", 600);
$form->add_section("List of Tables in the Database");

$form->add_label("title", "Table Name", 0, "Number of Records");
$form->add_label("spacer", "");


foreach($__db_structure as $key=>$value)
{
    $key_name = $value["key"];

    $num_recs = get_number_of_records($key, $key_name);
    
    //$line = $num_recs . " in retail\n" . $num_recs_archived . " in retail_archive\n" . $total . " total records";
    
    $line = "ww\n" . $num_recs;
    
    $form->add_label($key, $key, 0, $line);
}

$form->populate();
$form->process();

/********************************************************************
    Create Page
*********************************************************************/
$page = new Page("strings");

$page->header();
$page->title("Data Base Info");
$form->render();
$page->footer();

?>
