<?php
/********************************************************************

    house_keeping.php

    Delete orders/projects

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-12-03
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2002-06-18
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_access_to_housekeeping");

$from_year = 0;
$sql = "select min(left(order_date,4)) as year ".
       "from orders " .
       "where order_archive_date is not null";

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
    $from_year = $row["year"];
}


$sql_years = "select distinct left(order_date,4) as year ".
             "from orders " .
             "where order_archive_date is not null";

$form = new Form("orders", "order");

$form->add_section("Explanation");
$form->add_comment("All projects and orders from the selected range will be deleted. No data will be left in the database!");
$form->add_comment("At the same time the stock control starting date will be set to the 1st. of January of the year following the 'to year' in the selected range!");

$form->add_comment("ATTENTION: The data will be deleted definitly! There will be no confirmation request.");

$form->add_section("Range");
$form->add_comment("Please select the range of years!");
$form->add_label("from_year", "From Year", 0, $from_year);
$form->add_list("to_year", "To Year", $sql_years, NOTNULL);

$form->add_section("Confirmation");
$form->add_comment("Please confirm that you are sure!");
$form->add_checkbox("imsure", "I am sure", 0, NOTNULL);

$form->add_button("delete", "Delete Selected Projects and Orders");

$form->populate();
$form->process();

if ($form->button("delete"))
{
    if($form->validate())
    {
        delete_orders($form);
        $form->message("The selected orders and projects have been removed from the database!");
    }
}

$page = new Page("system");
$page->header();
$page->title("Delete Orders and Projects");
$form->render();
$page->footer();



/********************************************************************
    delete project/order from archive
*********************************************************************/
function  delete_orders($form)
{
    define("ORDER_CANCELLED", "900");

    $y1 = $form->value("from_year");    
    $y2 = $form->value("to_year");
    
    // delete all orders beeing cancelled
    // get order state id
    $sql = "select order_state_id ".
           "from order_states ".
           "left join order_state_groups on order_state_group = order_state_group_id ".
           "where order_state_group_order_type = 2 " .
           "   and order_state_code = " . dbquote(ORDER_CANCELLED);
    
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $order_state_id = $row["order_state_id"];
    }

    $sql = "select order_id " .
           "from orders ".
           "left join actual_order_states on order_id = actual_order_state_order ".
           "where left(order_date, 4) >=" . $y1 .
           "    and left(order_date, 4) <= " . $y2 .
           "    and order_archive_date is not null ".
           "    and actual_order_state_state = " . $order_state_id;

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        delete_record("orders", $row["order_id"], true);
    }


    // delete all projects beeing cancelled
    // get order state id
    $sql = "select order_state_id ".
           "from order_states ".
           "left join order_state_groups on order_state_group = order_state_group_id ".
           "where order_state_group_order_type = 1 " .
           "   and order_state_code = " . dbquote(ORDER_CANCELLED);
    
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $order_state_id = $row["order_state_id"];
    }

    $sql = "select order_id " .
           "from orders ".
           "left join actual_order_states on order_id = actual_order_state_order ".
           "where left(order_date, 4) >=" . $y1 .
           "    and left(order_date, 4) <= " . $y2 .
           "    and order_archive_date is not null ".
           "    and actual_order_state_state = " . $order_state_id;

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        delete_record("orders", $row["order_id"], true);
    }


    // update stock data
    // diminish global order and inventory fro each item
        
    $sql_order_items = "select order_id, order_item_item, order_item_quantity ".
                       "from orders ".
                       "left join order_items on order_item_order = order_id " .
                       "where left(order_date, 4) >=" . $y1 .
                       "    and left(order_date, 4) <= " . $y2 .
                       "    and order_archive_date is not null " .
                       "    and order_item_type <= " . ITEM_TYPE_SPECIAL .
                       "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null)";

    $res = mysql_query($sql_order_items) or dberror($sql_order_items);
    while ($row = mysql_fetch_assoc($res))
    {
        if($row["order_item_item"])
        {
            $sql_stores = "select store_id, store_global_order, store_inventory " .
                          "from stores ".
                          "where store_item = " . $row["order_item_item"];
    
            $res1 = mysql_query($sql_stores) or dberror($sql_stores);
            if ($row1 = mysql_fetch_assoc($res1))
            {
                $global_order = $row1["store_global_order"] - $row["order_item_quantity"];
                $inventory = $row1["store_inventory"] - $row["order_item_quantity"];
                
                $sql = "update stores set store_global_order = " . $global_order . ", ".
                       "    store_inventory = " . $inventory . " " .
                       "where store_item = " . $row["order_item_item"];
                mysql_query($sql) or dberror($sql);
            }
        }
    }

    // delete the rest of the selected orders
    $sql = "select order_id " .
           "from orders ".
           "where left(order_date, 4) >=" . $y1 .
           "    and left(order_date, 4) <= " . $y2 .
           "    and order_archive_date is not null";

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {

        delete_record("orders", $row["order_id"], true);
    }
   
   
   // update stock control starting date
   // for all items having a stock control starting date < 1.1.to_year

   $year = number_format($y2,0) + 1;
   $sql = "update stores set store_stock_control_starting_date = '". $year . "-01-01', " .
          "    store_consumed_upto_021101=0";
   mysql_query($sql) or dberror($sql);

}
?>