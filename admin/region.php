<?php
/********************************************************************

    region.php

    Creation and mutation of region records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-01
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-09-17
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

set_referer("country.php");

$form = new Form("regions", "region");

$form->add_edit("region_name", "Name", NOTNULL | UNIQUE);

$form->add_subtable("countries", "<br />Countries", "country.php", "Name",
    "select country_id, country_name as Name " .
    "from countries " .
    "where country_region = " . id() . " " .
    "order by country_name");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);



$form->populate();
$form->process();

$page = new Page("regions");
$page->header();
$page->title(id() ? "Edit Supplying Region" : "Add Supplying Region");
$form->render();
$page->footer();

?>