<?php
/********************************************************************

    link_topic.php

    Creation and mutation of link topic records.

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2013-03-23
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2013-03-23
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
//set_referer("link.php");

$form = new Form("link_topics", "link topic");

$form->add_section();
$form->add_edit("link_topic_name", "Name", NOTNULL);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("link_topics");
$page->header();
$page->title(id() ? "Edit Download/Link Topic" : "Add Download/Link Topic");
$form->render();
$page->footer();

?>