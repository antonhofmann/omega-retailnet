<?php
/********************************************************************

    system.php

    Entry page for the people system group.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-31
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-08-19
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$page = new Page("system");

$page->header();
$page->footer();

?>