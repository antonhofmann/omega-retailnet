<?php
/********************************************************************

    project_kinds.php

    Lists project_kindss for editing.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-07-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-07-15
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("project_kind.php");

$list = new ListView("select projectkind_id , projectkind_code, projectkind_name, " . 
                     "projectkind_email1, projectkind_email2, projectkind_email3 " .
                     "from projectkinds", LIST_HAS_HEADER | LIST_SHOW_COUNT);

$list->set_entity("projectkinds");

$list->set_order("projectkind_name");
$list->set_filter("projectkind_id IN (4, 5)");

$list->add_column("projectkind_code", "Code");
$list->add_column("projectkind_name", "Name", "project_kind.php");
$list->add_column("projectkind_email1", "Notify 1");
$list->add_column("projectkind_email2", "Notify 2");
$list->add_column("projectkind_email3", "Notify 3");


$list->process();

$page = new Page("project_kinds");

$page->header();
$page->title("Project Kinds - Email Notifications");
$list->render();
$page->footer();

?>
