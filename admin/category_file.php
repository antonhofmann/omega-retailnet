<?php
/********************************************************************

    category_file.php

    Creation and mutation of category file records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-13
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-09-17
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_browse_catalog_in_admin");

$form = new Form("category_files", "file");

if (id())
{
    $sql = "select category_file_category from category_files where category_file_id = " . id();
    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_row($res);
    param("category", $row[0]);
}

$form->add_hidden("category", param("category"));

$form->add_section();
$form->add_lookup("category_file_category", "Category", "categories", "category_name", 0, param("category"));

$form->add_section();
$form->add_edit("category_file_title", "Title", NOTNULL);
$form->add_multiline("category_file_description", "Description", 4);

$sql = "select category_name from categories where category_id = " . param("category");
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_row($res);
$category_name = make_valid_filename($row[0]);

$form->add_section();
$form->add_upload("category_file_path", "File", "/files/categories/$category_name");

$form->add_section();
$form->add_list("category_file_type", "Type",
    "select file_type_id, file_type_name from file_types order by file_type_name");
$form->add_list("category_file_purpose", "Purpose",
    "select file_purpose_id, file_purpose_name from file_purposes order by file_purpose_name");

if(has_access("can_edit_catalog"))
{
	$form->add_button(FORM_BUTTON_SAVE, "Save");
	$form->add_button(FORM_BUTTON_BACK, "Back");
	$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);
}
else
{
	$form->add_button(FORM_BUTTON_BACK, "Back");
}
$form->populate();
$form->process();

$page = new Page("categories");

$page->header();
$page->title(id() ? "Edit Category File" : "Add Category File");
$form->render();
$page->footer();

?>
