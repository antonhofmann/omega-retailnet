<?php
/********************************************************************

    message_send_mails.php

    Send mails to inform checked notification recipients about
    a message.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2007-11-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2007-11-14
    Version:        1.0.0

    Copyright (c) 2007, Swatch AG, All Rights Reserved.

*********************************************************************/
$server_address = APPLICATION_URL;

$mimetypes = array();
$mimetype['ai']='application/postscript';
$mimetype['aif']='audio/x-aiff';
$mimetype['aifc']='audio/x-aiff';
$mimetype['aiff']='audio/x-aiff';
$mimetype['arj']='application/x-arj-compressed';
$mimetype['asc']='text/plain';
$mimetype['asc txt']='text/plain';
$mimetype['asf']='video/x-ms-asf';
$mimetype['asx']='video/x-ms-asx';
$mimetype['au']='audio/basic';
$mimetype['au']='audio/ulaw';
$mimetype['avi']='video/x-msvideo';
$mimetype['bat']='application/x-msdos-program';
$mimetype['bcpio']='application/x-bcpio';
$mimetype['bin']='application/octet-stream';
$mimetype['c']='text/plain';
$mimetype['cc']='text/plain';
$mimetype['ccad']='application/clariscad';
$mimetype['cdf']='application/x-netcdf';
$mimetype['class']='application/octet-stream';
$mimetype['cod']='application/vnd.rim.cod';
$mimetype['com']='application/x-msdos-program';
$mimetype['cpio']='application/x-cpio';
$mimetype['cpt']='application/mac-compactpro';
$mimetype['csh']='application/x-csh';
$mimetype['css']='text/css';
$mimetype['dcr']='application/x-director';
$mimetype['deb']='application/x-debian-package';
$mimetype['dir']='application/x-director';
$mimetype['dl']='video/dl';
$mimetype['dms']='application/octet-stream';
$mimetype['doc']='application/msword';
$mimetype['drw']='application/drafting';
$mimetype['dvi']='application/x-dvi';
$mimetype['dwg']='application/acad';
$mimetype['dxf']='application/dxf';
$mimetype['dxr']='application/x-director';
$mimetype['eps']='application/postscript';
$mimetype['etx']='text/x-setext';
$mimetype['exe']='application/octet-stream';
$mimetype['exe']='application/x-msdos-program';
$mimetype['ez']='application/andrew-inset';
$mimetype['f']='text/plain';
$mimetype['f90']='text/plain';
$mimetype['fli']='video/fli';
$mimetype['fli']='video/x-fli';
$mimetype['flv']='video/flv';
$mimetype['gif']='image/gif';
$mimetype['gl']='video/gl';
$mimetype['gtar']='application/x-gtar';
$mimetype['gz']='application/x-gunzip';
$mimetype['gz']='application/x-gzip';
$mimetype['h']='text/plain';
$mimetype['hdf']='application/x-hdf';
$mimetype['hh']='text/plain';
$mimetype['hqx']='application/mac-binhex40';
$mimetype['htm']='text/html';
$mimetype['html']='text/html';
$mimetype['html htm']='text/html';
$mimetype['ice']='x-conference/x-cooltalk';
$mimetype['ief']='image/ief';
$mimetype['iges']='model/iges';
$mimetype['igs']='model/iges';
$mimetype['ips']='application/x-ipscript';
$mimetype['ipx']='application/x-ipix';
$mimetype['jad']='text/vnd.sun.j2me.app-descriptor';
$mimetype['jar']='application/java-archive';
$mimetype['jpe']='image/jpeg';
$mimetype['jpeg']='image/jpeg';
$mimetype['jpg']='image/jpeg';
$mimetype['js']='application/x-javascript';
$mimetype['kar']='audio/midi';
$mimetype['latex']='application/x-latex';
$mimetype['lha']='application/octet-stream';
$mimetype['lsp']='application/x-lisp';
$mimetype['lzh']='application/octet-stream';
$mimetype['m']='text/plain';
$mimetype['m3u']='audio/x-mpegurl';
$mimetype['man']='application/x-troff-man';
$mimetype['me']='application/x-troff-me';
$mimetype['mesh']='model/mesh';
$mimetype['mid']='audio/midi';
$mimetype['midi']='audio/midi';
$mimetype['mif']='application/vnd.mif';
$mimetype['mif']='application/x-mif';
$mimetype['mime']='www/mime';
$mimetype['mov']='video/quicktime';
$mimetype['movie']='video/x-sgi-movie';
$mimetype['mp2']='audio/mpeg';
$mimetype['mp2']='video/mpeg';
$mimetype['mp3']='audio/mpeg';
$mimetype['mpe']='video/mpeg';
$mimetype['mpeg']='video/mpeg';
$mimetype['mpg']='video/mpeg';
$mimetype['mpga']='audio/mpeg';
$mimetype['ms']='application/x-troff-ms';
$mimetype['msh']='model/mesh';
$mimetype['nc']='application/x-netcdf';
$mimetype['oda']='application/oda';
$mimetype['ogg']='application/ogg';
$mimetype['ogm']='application/ogg';
$mimetype['pbm']='image/x-portable-bitmap';
$mimetype['pdb']='chemical/x-pdb';
$mimetype['pdf']='application/pdf';
$mimetype['pgm']='image/x-portable-graymap';
$mimetype['pgn']='application/x-chess-pgn';
$mimetype['pgp']='application/pgp';
$mimetype['pl']='application/x-perl';
$mimetype['pm']='application/x-perl';
$mimetype['png']='image/png';
$mimetype['pnm']='image/x-portable-anymap';
$mimetype['pot']='application/mspowerpoint';
$mimetype['ppm']='image/x-portable-pixmap';
$mimetype['pps']='application/mspowerpoint';
$mimetype['ppt']='application/mspowerpoint';
$mimetype['ppz']='application/mspowerpoint';
$mimetype['pre']='application/x-freelance';
$mimetype['prt']='application/pro_eng';
$mimetype['ps']='application/postscript';
$mimetype['qt']='video/quicktime';
$mimetype['ra']='audio/x-realaudio';
$mimetype['ram']='audio/x-pn-realaudio';
$mimetype['rar']='application/x-rar-compressed';
$mimetype['ras']='image/cmu-raster';
$mimetype['ras']='image/x-cmu-raster';
$mimetype['rgb']='image/x-rgb';
$mimetype['rm']='audio/x-pn-realaudio';
$mimetype['roff']='application/x-troff';
$mimetype['rpm']='audio/x-pn-realaudio-plugin';
$mimetype['rtf']='application/rtf';
$mimetype['rtf']='text/rtf';
$mimetype['rtx']='text/richtext';
$mimetype['scm']='application/x-lotusscreencam';
$mimetype['set']='application/set';
$mimetype['sgm']='text/sgml';
$mimetype['sgml']='text/sgml';
$mimetype['sh']='application/x-sh';
$mimetype['shar']='application/x-shar';
$mimetype['silo']='model/mesh';
$mimetype['sit']='application/x-stuffit';
$mimetype['skd']='application/x-koan';
$mimetype['skm']='application/x-koan';
$mimetype['skp']='application/x-koan';
$mimetype['skt']='application/x-koan';
$mimetype['smi']='application/smil';
$mimetype['smil']='application/smil';
$mimetype['snd']='audio/basic';
$mimetype['sol']='application/solids';
$mimetype['spl']='application/x-futuresplash';
$mimetype['src']='application/x-wais-source';
$mimetype['step']='application/STEP';
$mimetype['stl']='application/SLA';
$mimetype['stp']='application/STEP';
$mimetype['sv4cpio']='application/x-sv4cpio';
$mimetype['sv4crc']='application/x-sv4crc';
$mimetype['swf']='application/x-shockwave-flash';
$mimetype['t']='application/x-troff';
$mimetype['tar']='application/x-tar';
$mimetype['tar.gz']='application/x-tar-gz';
$mimetype['tcl']='application/x-tcl';
$mimetype['tex']='application/x-tex';
$mimetype['texi']='application/x-texinfo';
$mimetype['texinfo']='application/x-texinfo';
$mimetype['tgz']='application/x-tar-gz';
$mimetype['tif']='image/tiff';
$mimetype['tif tiff']='image/tiff';
$mimetype['tiff']='image/tiff';
$mimetype['tr']='application/x-troff';
$mimetype['tsi']='audio/TSP-audio';
$mimetype['tsp']='application/dsptype';
$mimetype['tsv']='text/tab-separated-values';
$mimetype['txt']='text/plain';
$mimetype['unv']='application/i-deas';
$mimetype['ustar']='application/x-ustar';
$mimetype['vcd']='application/x-cdlink';
$mimetype['vda']='application/vda';
$mimetype['viv']='video/vnd.vivo';
$mimetype['vivo']='video/vnd.vivo';
$mimetype['vrm']='x-world/x-vrml';
$mimetype['vrml']='model/vrml';
$mimetype['vrml']='x-world/x-vrml';
$mimetype['wav']='audio/x-wav';
$mimetype['wax']='audio/x-ms-wax';
$mimetype['wma']='audio/x-ms-wma';
$mimetype['wmv']='video/x-ms-wmv';
$mimetype['wmx']='video/x-ms-wmx';
$mimetype['wrl']='model/vrml';
$mimetype['wvx']='video/x-ms-wvx';
$mimetype['xbm']='image/x-xbitmap';
$mimetype['xlc']='application/vnd.ms-excel';
$mimetype['xll']='application/vnd.ms-excel';
$mimetype['xlm']='application/vnd.ms-excel';
$mimetype['xls']='application/excel';
$mimetype['xls']='application/vnd.ms-excel';
$mimetype['xlw']='application/vnd.ms-excel';
$mimetype['xml']='text/xml';
$mimetype['xpm']='image/x-xpixmap';
$mimetype['xwd']='image/x-xwindowdump';
$mimetype['xyz']='chemical/x-pdb';
$mimetype['zip']='application/x-zip-compressed';
$mimetype['zip']='application/zip';

$reciepients = array();
$reciepientemails = array();

$sql = "select user_email as sender, " .
       "concat(user_name, ' ', user_firstname) as user_fullname ".
       "from users ".
       "where user_id = " . user_id();

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
    $mail_sent_to = array();
	$sender_email = $row["sender"];
    $sender_name =  $row["user_fullname"];

    $subject = MAIL_SUBJECT_PREFIX . ": " . $form->items["message_title"]["value"];
    $bodytext = $form->items["message_text"]["value"];
    
    //get attachment
    $attachment_data = "";

    if($form->items["attachment"]["value"] > 0)
    {
        $link_id = $form->items["attachment"]["value"];
        $sql_l = "select link_title, link_path from links where link_id = " . $link_id;
        $res_l = mysql_query($sql_l) or dberror($sql_l);
        if ($row_l = mysql_fetch_assoc($res_l))
        {
            $attachment = $row_l["link_path"];
            $filename = $_SERVER["DOCUMENT_ROOT"] . $attachment;
            $handle = fopen ($filename, "r");
            $attachment_data = fread ($handle, filesize ($filename));
            fclose ($handle);
        }
    }

    // mails to agents
	if(is_array($form->items["AGENTSs"]["value"]))
	{
		foreach($form->items["AGENTSs"]["value"] as $key=>$value)
		{
			$sql = "select user_id, " .
			   "concat(user_name, ' ' , user_firstname) as username, " .
			   "user_email " .
			   "from users " .
			   "where user_id = " . $value;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$reciepients[$row["user_id"]] = $row["user_id"];
				$reciepientemails[$row["user_email"]] = $row["username"];
				$mail_sent_to['agents'] = 'Agents';
			}
		}
	}
	   
    

    // mails to subsidiaries
	if(is_array($form->items["SUBs"]["value"]))
	{
		foreach($form->items["SUBs"]["value"] as $key=>$value)
		{
			$sql = "select user_id, " .
			   "concat(user_name, ' ' , user_firstname) as username, " .
			   "user_email " .
			   "from users " .
			   "where user_id = " . $value;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$reciepients[$row["user_id"]] = $row["user_id"];
				$reciepientemails[$row["user_email"]] = $row["username"];
				$mail_sent_to['subsidiaries'] = 'Subsidiaries';
			}
		}
	}
    
    
	// mails to retail operators
	if(is_array($form->items["RTOPs"]["value"]))
	{
		foreach($form->items["RTOPs"]["value"] as $key=>$value)
		{
			$sql = "select user_id, " .
			   "concat(user_name, ' ' , user_firstname) as username, " .
			   "user_email " .
			   "from users " .
			   "where user_id = " . $value;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$reciepients[$row["user_id"]] = $row["user_id"];
				$reciepientemails[$row["user_email"]] = $row["username"];
				$mail_sent_to['rto'] = 'Retail Operators';
			}
		}
	}
	
    
    
    // mails to retail coordinators
    
	if(is_array($form->items["RTCOs"]["value"]))
	{
		foreach($form->items["RTCOs"]["value"] as $key=>$value)
		{
			$sql = "select user_id, " .
			   "concat(user_name, ' ' , user_firstname) as username, " .
			   "user_email " .
			   "from users " .
			   "where user_id = " . $value;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$reciepients[$row["user_id"]] = $row["user_id"];
				$reciepientemails[$row["user_email"]] = $row["username"];
				$mail_sent_to['rtc'] = 'Project Managers';
			}
		}
	}
	
    
    
	// mails to local brand managers
	if(is_array($form->items["BRMAs"]["value"]))
	{
		foreach($form->items["BRMAs"]["value"] as $key=>$value)
		{
			$sql = "select user_id, " .
			   "concat(user_name, ' ' , user_firstname) as username, " .
			   "user_email " .
			   "from users " .
			   "where user_id = " . $value;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$reciepients[$row["user_id"]] = $row["user_id"];
				$reciepientemails[$row["user_email"]] = $row["username"];
				$mail_sent_to['brandmanagers'] = 'Brand Managers';
			}
		}
	}


	// mails to regional sales managers
	if(is_array($form->items["RSMAs"]["value"]))
	{
		foreach($form->items["RSMAs"]["value"] as $key=>$value)
		{
			$sql = "select user_id, " .
			   "concat(user_name, ' ' , user_firstname) as username, " .
			   "user_email " .
			   "from users " .
			   "where user_id = " . $value;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$reciepients[$row["user_id"]] = $row["user_id"];
				$reciepientemails[$row["user_email"]] = $row["username"];
				$mail_sent_to['rmsas'] = 'Regional Sales Manager HQ';
			}
		}
	}
	
    // mails to local retail managers
	if(is_array($form->items["RTMAs"]["value"]))
	{
		foreach($form->items["RTMAs"]["value"] as $key=>$value)
		{
			$sql = "select user_id, " .
			   "concat(user_name, ' ' , user_firstname) as username, " .
			   "user_email " .
			   "from users " .
			   "where user_id = " . $value;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$reciepients[$row["user_id"]] = $row["user_id"];
				$reciepientemails[$row["user_email"]] = $row["username"];
				$mail_sent_to['lrtc'] = 'Local Retail Managers';
			}
		}
	}
	
    
	
    // mails to Administrators
	if(is_array($form->items["ADMNs"]["value"]))
	{
		foreach($form->items["ADMNs"]["value"] as $key=>$value)
		{
			$sql = "select user_id, " .
			   "concat(user_name, ' ' , user_firstname) as username, " .
			   "user_email " .
			   "from users " .
			   "where user_id = " . $value;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$reciepients[$row["user_id"]] = $row["user_id"];
				$reciepientemails[$row["user_email"]] = $row["username"];
				$mail_sent_to['admin'] = 'Retail Net Administrators';
			}
		}
	}
	
	// mails to Design Contractors
	if(is_array($form->items["DCONs"]["value"]))
	{
		foreach($form->items["DCONs"]["value"] as $key=>$value)
		{
			$sql = "select user_id, " .
			   "concat(user_name, ' ' , user_firstname) as username, " .
			   "user_email " .
			   "from users " .
			   "where user_id = " . $value;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$reciepients[$row["user_id"]] = $row["user_id"];
				$reciepientemails[$row["user_email"]] = $row["username"];
				$mail_sent_to['dcon'] = 'Design Contractors';
			}
		}
	}
	
    


	// mails to Desig Supervisors
	if(is_array($form->items["DSUPs"]["value"]))
	{
		foreach($form->items["DSUPs"]["value"] as $key=>$value)
		{
			$sql = "select user_id, " .
			   "concat(user_name, ' ' , user_firstname) as username, " .
			   "user_email " .
			   "from users " .
			   "where user_id = " . $value;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$reciepients[$row["user_id"]] = $row["user_id"];
				$reciepientemails[$row["user_email"]] = $row["username"];
				$mail_sent_to['dsup'] = 'Design Supervisors';
			}
		}
	}
	
    
	
	// mails to Forwarders
	if(is_array($form->items["FWRDs"]["value"]))
	{
		foreach($form->items["FWRDs"]["value"] as $key=>$value)
		{
			$sql = "select user_id, " .
			   "concat(user_name, ' ' , user_firstname) as username, " .
			   "user_email " .
			   "from users " .
			   "where user_id = " . $value;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$reciepients[$row["user_id"]] = $row["user_id"];
				$reciepientemails[$row["user_email"]] = $row["username"];
				$mail_sent_to['frwd'] = 'Forwarders';
			}
		}
	}
	  


	// mails to Suppliers
	if(is_array($form->items["SUPPs"]["value"]))
	{
		foreach($form->items["SUPPs"]["value"] as $key=>$value)
		{
			$sql = "select user_id, " .
			   "concat(user_name, ' ' , user_firstname) as username, " .
			   "user_email " .
			   "from users " .
			   "where user_id = " . $value;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$reciepients[$row["user_id"]] = $row["user_id"];
				$reciepientemails[$row["user_email"]] = $row["username"];
				$mail_sent_to['supp'] = 'Suppliers';
			}
		}
	}
		

	// mails to Contract Supervisors
	if(is_array($form->items["COSUs"]["value"]))
	{
		foreach($form->items["COSUs"]["value"] as $key=>$value)
		{
			$sql = "select user_id, " .
			   "concat(user_name, ' ' , user_firstname) as username, " .
			   "user_email " .
			   "from users " .
			   "where user_id = " . $value;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$reciepients[$row["user_id"]] = $row["user_id"];
				$reciepientemails[$row["user_email"]] = $row["username"];
				$mail_sent_to['cosu'] = 'Contract Supervisors';
			}
		}
	}
	  


	// mails to Financial COntrollers
	if(is_array($form->items["FINCs"]["value"]))
	{
		foreach($form->items["FINCs"]["value"] as $key=>$value)
		{
			$sql = "select user_id, " .
			   "concat(user_name, ' ' , user_firstname) as username, " .
			   "user_email " .
			   "from users " .
			   "where user_id = " . $value;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$reciepients[$row["user_id"]] = $row["user_id"];
				$reciepientemails[$row["user_email"]] = $row["username"];
				$mail_sent_to['financial'] = 'Financial COntrollers';
			}
		}
	}
	  


	// mails to Local Retail Managers
	if(is_array($form->items["LORCs"]["value"]))
	{
		foreach($form->items["LORCs"]["value"] as $key=>$value)
		{
			$sql = "select user_id, " .
			   "concat(user_name, ' ' , user_firstname) as username, " .
			   "user_email " .
			   "from users " .
			   "where user_id = " . $value;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$reciepients[$row["user_id"]] = $row["user_id"];
				$reciepientemails[$row["user_email"]] = $row["username"];
				$mail_sent_to['rm'] = 'Local Project Managers';
				
			}
		}
	}
	  


	// mails to HQ Project Manager
	/*
	foreach($form->items["RRMAs"]["value"] as $key=>$value)
	{
		$sql = "select user_id, " .
		   "concat(user_name, ' ' , user_firstname) as username, " .
		   "user_email " .
		   "from users " .
		   "where user_id = " . $value;

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$reciepients[$row["user_id"]] = $row["user_id"];
			$reciepientemails[$row["user_email"]] = $row["username"];
		}
	}
	*/
	  


	// mails to Visitors CER / AF
	if(is_array($form->items["VCERs"]["value"]))
	{
		foreach($form->items["VCERs"]["value"] as $key=>$value)
		{
			$sql = "select user_id, " .
			   "concat(user_name, ' ' , user_firstname) as username, " .
			   "user_email " .
			   "from users " .
			   "where user_id = " . $value;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$reciepients[$row["user_id"]] = $row["user_id"];
				$reciepientemails[$row["user_email"]] = $row["username"];
				$mail_sent_to['cervisitors'] = 'Visitors of the CER';

			}
		}
	}
	

	// mails to Visitors POS Index
	if(is_array($form->items["VPOSs"]["value"]))
	{
		foreach($form->items["VPOSs"]["value"] as $key=>$value)
		{
			$sql = "select user_id, " .
			   "concat(user_name, ' ' , user_firstname) as username, " .
			   "user_email " .
			   "from users " .
			   "where user_id = " . $value;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$reciepients[$row["user_id"]] = $row["user_id"];
				$reciepientemails[$row["user_email"]] = $row["username"];
				$mail_sent_to['posvisitors'] = 'Visitors of the POS Index';
			}
		}
	}
	
    
	// mails to Visitors RetailNet Projects
	if(is_array($form->items["VIEWs"]["value"]))
	{
		foreach($form->items["VIEWs"]["value"] as $key=>$value)
		{
			$sql = "select user_id, " .
			   "concat(user_name, ' ' , user_firstname) as username, " .
			   "user_email " .
			   "from users " .
			   "where user_id = " . $value;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$reciepients[$row["user_id"]] = $row["user_id"];
				$reciepientemails[$row["user_email"]] = $row["username"];
				$mail_sent_to['projectvisitors'] = 'Visitors of Projects';
			}
		}
	}
	   


	// mails to Visualisation Local Country
	if(is_array($form->items["LOCMs"]["value"]))
	{
		foreach($form->items["LOCMs"]["value"] as $key=>$value)
		{
			$sql = "select user_id, " .
				   "concat(user_name, ' ' , user_firstname) as username, " .
				   "user_email " .
				   "from users " .
				   "where user_id = " . $value;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$reciepients[$row["user_id"]] = $row["user_id"];
				$reciepientemails[$row["user_email"]] = $row["username"];
				$mail_sent_to['visuali'] = 'Local Visualisation Team';
			}
		}
	}

	if(count($mail_sent_to) > 0) {
	
		$role_text = "P.S: This message was sent to " . implode(', ', $mail_sent_to);
		$bodytext .= "\n\n" . $role_text;
	}

	//send mails
	foreach($reciepientemails as $email=>$name) {
		
		$mail = new PHPMailer();
		$mail->Subject = $subject;
		$mail->SetFrom($sender_email, $sender_name);
		$mail->AddReplyTo($sender_email, $sender_name);

		$mail->AddAddress($email, $name);

		$mail->Body = $bodytext;

		if(isset($attachment))
		{
			$filepath = $_SERVER["DOCUMENT_ROOT"] . $attachment;
			$mail->AddAttachment($filepath);
			
		}   
		$mail->Send();
	}


    //insert into reciepients table
    foreach($reciepients as $key=>$value)
    {
        $sql = "Insert into message_email_sent (" .
               "message_email_sent_user, " .
               "message_email_sent_touser, " .
               "message_email_sent_message, " . 
               "date_created) " .
               "values (" .
               user_id() . ", " .
               $key . ", " .
               id() . ", '" .
               date("Y-m-d G:i:s") . "')";

        $result = mysql_query($sql) or dberror($sql);

    }
}
?>