<?php
/********************************************************************

    country.php

    Creation and mutation of country records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-30
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-09-17
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


$time_formats = array("24"=>"24 hours", "12"=>"12 hours");

$form = new Form("countries", "country");

$form->add_edit("country_code", "Code", NOTNULL | UNIQUE);
$form->add_edit("country_name", "Name", NOTNULL | UNIQUE);
$form->add_list("country_region", "Supplying Region", 
    "select region_id, region_name from regions order by region_name");

$form->add_list("country_salesregion", "Geographical Region", 
    "select salesregion_id, salesregion_name from salesregions order by salesregion_name");

$form->add_list("country_currency", "Currency", 
    "select currency_id,currency_symbol from currencies order by currency_symbol");

$form->add_edit("country_store_locator_id", "Store Locator ID", NOTNULL | UNIQUE);

$form->add_checkbox("country_provinces_complete", "The list of provinces for this country is complete", false, 0, 'Provinces');


$form->add_list("country_timeformat", "Time Format", $time_formats);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("countries");
$page->header();
$page->title(id() ? "Edit Country" : "Add Country");
$form->render();
$page->footer();

?>