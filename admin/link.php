<?php
/********************************************************************

    link.php

    Creation and mutation of link records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-03
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-10-10
    Version:        1.0.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("links", "link");


$form->add_section();
$form->add_list("link_topic", "Topic",
    "select link_topic_id, link_topic_name from link_topics order by link_topic_name",
    NOTNULL, param("topic"));

$form->add_list("link_category", "Category",
    "select link_category_id, link_category_name from link_categories order by link_category_name",
    NOTNULL, param("category"));

$form->add_section();
$form->add_edit("link_title", "Title", NOTNULL);
$form->add_hidden("ltf", param("ltf"));

$form->add_section();
$form->add_edit("link_url", "URL");
$form->add_upload("link_path", "File", "/files/links");



$form->add_section();
$form->add_multiline("link_description", "Description", 10);


$form->add_section("Accessibility for Roles");
$form->add_comment("Please indicate who is allowed to see this message.");
$form->add_checklist("download_roles", "", "download_roles",
    "select role_id, role_name from roles order by role_name");

$form->add_section("Accessibility for Countries");
$form->add_comment("Please indicate the countries that can see the message.");
$form->add_checkbox("all_countries", "all countries", false);
$form->add_checklist("download_countries", "", "download_countries",
    "select country_id, country_name from countries order by country_name");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button("back", "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->add_validation("is_web_address({link_url})", "The URL is invalid");
$form->add_validation("{link_url} || {link_path}", "Either an URL or a file must be specified");

$form->populate();
$form->process();

if($form->button(FORM_BUTTON_SAVE))
{

	if($form->validate() and $form->value("all_countries") == 1)
	{
		$sql = "delete from download_countries where download_countries_download = " . id();
		$result = mysql_query($sql) or dberror($sql);

		$sql = "select country_id from countries";
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			
			$fields = array();
			$values = array();

			$fields[] = "download_countries_country";
			$values[] = $row["country_id"];

			$fields[] = "download_countries_download";
			$values[] = id();

			$fields[] = "date_created";
			$values[] = "now()";

			$fields[] = "date_modified";
			$values[] = "now()";

			$fields[] = "user_created";
			$values[] = dbquote(user_login());

			$fields[] = "user_modified";
			$values[] = dbquote(user_login());
			
			$sql = "insert into download_countries (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			mysql_query($sql) or dberror($sql);

	
		}

		$link = "link.php?id=" . id() . "&ltf=" . param("ltf");
		redirect($link);

	}
	elseif($form->validate())
	{
		$link = "links.php?ltf=" .  param("ltf");
		redirect($link);
	}
}
elseif($form->button("back"))
{
	$link = "links.php?ltf=" .  param("ltf");
	redirect($link);
}

$page = new Page("links");
$page->header();
$page->title(id() ? "Edit Link" : "Add Link");
$form->render();
$page->footer();

?>