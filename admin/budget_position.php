<?php
/********************************************************************

    budget_position.php

    Add and Edit standard budget positions for editing.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-06-22
    Version:        1.0.3

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("items", "budget position");
$form->add_hidden("item_priority");

$form->add_section("Description");

$form->add_edit("item_code", "Code", NOTNULL | UNIQUE);
$form->add_list("item_type", "Type",
    "select item_type_id, item_type_name " .
    "from item_types " .
    "where item_type_id >= " . ITEM_TYPE_EXCLUSION, NOTNULL);

$form->add_multiline("item_description", "Description", 10, NOTNULL);

$form->add_section("Other");
$form->add_checkbox("item_active", "Active");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();

if (!$form->value("item_priority"))
{
    $sql = "select max(item_priority) from items";
    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_row($res);
    $form->value("item_priority", $row[0] + 1);
}

$form->process();

$page = new Page("budget_positions");
$page->header();
$page->title(id() ? "Edit Exclusion/Notification" : "Add Exclusion/Notification");
$form->render();
$page->footer();

?>