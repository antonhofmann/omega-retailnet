<?php
/********************************************************************

    standard_delivery_address.php

    Creation and mutation of standard delivery address records.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-03-22
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-03-22
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("user.php");

$country = 0;
$province_name = 0;
if(id()) {
	$sql = "select * from standard_delivery_addresses " .
		   "left join places on place_id = delivery_address_place_id " .
		   "left join provinces on province_id =  place_province " .
		   "where delivery_address_id = " . id();

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$country = $row["delivery_address_country"];
		$province_name = $row["province_canton"];
	}
}

// Build form

$form = new Form("standard_delivery_addresses", "Standard Delivery Address");

$form->add_section("Owner");
$form->add_list("delivery_address_address_id", "Address*",
    "select address_id, concat(country_name, ', ', address_company) as company from addresses left join countries on country_id = address_country where address_type = 1  order by company", NOTNULL);

$form->add_section("Delivery Address");

$form->add_edit("delivery_address_company", "Company*", NOTNULL);
$form->add_edit("delivery_address_company2", "");


$form->add_list("delivery_address_country", "Country*", 
    "select country_id, country_name from countries order by country_name", NOTNULL | SUBMIT);


if(param("delivery_address_country")) {
	$sql_places = "select place_id, place_name from places where place_country = " . param("delivery_address_country") . " order by place_name";
}
else
{
	$sql_places = "select place_id, place_name from places where place_country = " . dbquote($country) . " order by place_name";
}


$form->add_hidden("delivery_address_place");



$form->add_list("delivery_address_place_id", "Place*", $sql_places, SUBMIT | NOTNULL);

$form->add_label("delivery_address_province", "Province", 0, $province_name);

$form->add_edit("delivery_address_address", "Address*", NOTNULL);
$form->add_edit("delivery_address_address2", "");
$form->add_edit("delivery_address_zip", "Zip*", NOTNULL);



$form->add_section("Communication");
$form->add_edit("delivery_address_phone", "Phone");
$form->add_edit("delivery_address_fax", "Fax");
$form->add_edit("delivery_address_email", "Email");
$form->add_edit("delivery_address_contact", "Contact Name");



$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_DELETE, "Delete");
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->add_validation("is_email_address({delivery_address_email})", "The email address is invalid.");

// Populate form and process button clicks

$form->populate();
$form->process();

if($form->button("delivery_address_place_id"))
{
    if ($form->value("delivery_address_place_id"))
    {
        $sql = "select place_name, province_canton from places " . 
			   "left join provinces on province_id = place_province " . 
			   "where place_id = " . dbquote($form->value("delivery_address_place_id"));
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$form->value("delivery_address_place", $row["place_name"]);
			$form->value("delivery_address_province", $row["province_canton"]);
			$form->value("delivery_address_zip", "");
			$form->value("delivery_address_address", "");
			$form->value("delivery_address_address2", "");
			$form->value("delivery_address_phone", "");
			$form->value("delivery_address_fax", "");
			$form->value("delivery_address_email", "");
			$form->value("delivery_address_contact", "");
		}
    }
}
elseif ($form->button("delivery_address_country"))
{
	$form->value("delivery_address_place", "");
	$form->value("delivery_address_province", "");
	$form->value("delivery_address_place_id", 0);
	$form->value("delivery_address_zip", "");
	$form->value("delivery_address_address", "");
	$form->value("delivery_address_address2", "");
	$form->value("delivery_address_phone", "");
	$form->value("delivery_address_fax", "");
	$form->value("delivery_address_email", "");
	$form->value("delivery_address_contact", "");
}



// Render page

$page = new Page("standard_delivery_addresses");

$page->header();
$page->title(id() ? "Edit Standard Delivery Address" : "Add Standard Delivery Address");
$form->render();
$page->footer();

?>