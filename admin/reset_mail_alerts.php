<?php
/********************************************************************

    reset_mail_alerts.php

    Resets the table mailalerts and sends mailalerts again

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-11-29
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-11-29
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

require_once "../shared/func_mailalerts.php";

$form = new Form("mail_alerts", "mail_alert");

$form->add_section("POS Dates");
$form->add_comment("Resets the mail alerts for the agreed POS opening date and the actual POS opening date and sends notifications mails again.");
$form->add_comment("Send mail to Project Manager/Retail Operator if the project's agreed POS opening date is missing or today's date is greater than the agreed POS opening date and the actual shop opening date is missing.");
$form->add_comment("Send mail to client if the project has as state greater or equal to 800 telling him to fill in the actual POS opening date in case it is missing.");


$form->add_button("reset", "Reset");

$form->populate();
$form->process();


if($form->button("reset"))
{
	$sql = "delete from mail_alerts where mail_alert_type IN (8, 12, 14, 1, 13, 15, 2, 17, 18)";
	
	$result = mysql_query($sql) or dberror($sql);
	
	$result = ma_enter_realistic_sopd();
	$result = ma_enter_actual_sopd();

	$form->error("Mail alerts were reset and notification mails were sent.");
}

$page = new Page("resetmailalerts");
$page->header();
$page->title("Reset Mail Alerts");
$form->render();
$page->footer();

?>