<?php
/********************************************************************

    taskworks.php

    Lists taskworks for editing.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-02-18
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-02-18
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("taskwork.php");

$sql = "select taskwork_id, taskwork_shortcut,order_state_code, product_line_name, postype_name,  " .
       "address_shortcut, item_code " . 
       "from taskworks " .
	   "left join product_lines on product_line_id = taskwork_productline " . 
	   "left join postypes on postype_id = taskwork_postype " . 
	   "left join order_states on order_state_id = taskwork_order_state " . 
	   "left join addresses on address_id = taskwork_address " .
	   "left join items on item_id = taskwork_item";

$list = new ListView($sql);

$list->set_entity("taskworks");
$list->set_order("order_state_code, product_line_name, postype_name, taskwork_shortcut");

$list->add_column("taskwork_shortcut", "Name", "taskwork.php");
$list->add_column("order_state_code", "Step");
$list->add_column("product_line_name", "Product Line");
$list->add_column("postype_name", "POS Type");
$list->add_column("item_code", "Auto Item");
$list->add_column("address_shortcut", "Supplier");

$list->add_button(LIST_BUTTON_NEW, "New", "taskwork.php");


$list->process();

$page = new Page("taskworks");

$page->header();
$page->title("Project State Standard Texts");
$list->render();
$page->footer();

?>
