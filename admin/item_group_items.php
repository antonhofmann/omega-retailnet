<?php
/********************************************************************

    item_group_items.php

    Lists items to be assigned as composite items to an item group

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-08-05
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-08-05
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$parent_link = "item_group.php?id=" . param("id");

$values = array();
$sql = "select item_group_item_item, item_group_item_quantity " .
       "from item_group_items where item_group_item_group = " . param("id");

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
    $values[$row["item_group_item_item"]] = $row["item_group_item_quantity"];
}

$list = new ListView("select item_id, item_code, item_name, item_price, item_type_name " .
                     "from items left join item_types on item_type = item_type_id", LIST_HAS_HEADER | LIST_SHOW_COUNT);

$list->set_entity("items");
$list->set_order("item_code");
$list->set_filter("item_type = " . ITEM_TYPE_STANDARD);

$list->add_hidden("id", param("id"));
$list->add_edit_column("quantity", "Quantity", "6", 0, $values);
$list->add_column("item_code", "Code", "", LIST_FILTER_FREE);
$list->add_column("item_name", "Name", "", LIST_FILTER_FREE);

$list->add_button("ad_items", "Save");
$list->add_button("back", "Back");

$list->populate();
$list->process();


if ($list->button("ad_items"))
{
    $sql = "delete from item_group_items " .
           "where item_group_item_group = " . param("id");

    $result = mysql_query($sql) or dberror($sql);

    foreach($list->values("quantity") as $key => $value)
    {
        if($value > 0 and is_int_value($value, 12, false))
        {
            $sql = "insert into item_group_items (" .
                   "item_group_item_group, item_group_item_item, " .
                   "item_group_item_quantity, user_created, date_created) " .
                   " values (" .
                   param("id") . ", " .
                   $key . ", " .
                   $value . ", " .
                   "'" . user_login() . "', " .
                   "'" . date("Y-m-d") . "')";
            $result = mysql_query($sql) or dberror($sql);
        }
    }

    header("location: " . $parent_link);


}
if ($list->button("back"))
{
    header("location: " . $parent_link);
}

$page = new Page("item_groups");

$sql = "select item_group_name from item_groups where item_group_id = " . param("id");
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_row($res);

$page->header();
$page->title("Assign Composite Items To Group \"" . htmlspecialchars($row[0]) . "\"");
$list->render();
$page->footer();
?>
