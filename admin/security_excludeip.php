<?php
/********************************************************************

    security_excludeip.php

    Edit excluded IPS.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-09-05
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-09-05
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_unlock_ips");


$form = new Form("sec_excluded_ips", "IP Exception");
$form->add_section();
$form->add_edit("sec_excluded_ip_ip", "IP Address", NOTNULL);
$form->add_edit("sec_excluded_ip_maxtrials", "Max Number of Trials", NOTNULL, "", TYPE_INT);
$form->add_checkbox("sec_excluded_ip_exclude", "Exclude this IP from security check", false, 0, "Exclusion");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_DELETE, "Delete");
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();
$form->process();

$page = new Page("security");
$page->header();
$page->title("IP Exception");
$form->render();
$page->footer();

?>