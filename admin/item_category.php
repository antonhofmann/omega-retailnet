<?php
/********************************************************************

    item_category.php

    Creation and mutation of category group records.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-05-29
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-29
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


$form = new Form("item_categories", "item category");

$form->add_section();
$form->add_edit("item_category_name", "Name", NOTNULL);
$form->add_edit("item_category_sortorder", "Sortorder");

//$form->add_section("Masterplan");
//$form->add_checkbox("item_category_include_in_masterplan", "Consider items of this group in the masterplan", 0, 0, "Masterplan");



$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();
$form->process();

$page = new Page("item_categories");
$page->header();
$page->title(id() ? "Edit Item Category" : "Add Item Category");
$form->render();
$page->footer();

?>