<?php
/********************************************************************

    pos_type_new_project_notifications.php

    Lists notification reciepienst of pos types in case of a new project.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2010-11-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2010-11-14
    Version:        1.0.0

    Copyright (c) 2010, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("pos_type_new_project_notification.php");

$sql = "select projecttype_newproject_notification_id , " . 
		"projecttype_newproject_notification_email, " .
		"projecttype_newproject_notification_emailcc1, " .
		"projecttype_newproject_notification_emailcc2, " .
		"projecttype_newproject_notification_emailcc3, " .
		"projecttype_newproject_notification_emailcc4, " .
		"projecttype_newproject_notification_emailcc5, " .
		"projecttype_newproject_notification_emailcc6, " .
		"projecttype_newproject_notification_emailcc7, " .
		"postype_name,  country_name " .
		"from projecttype_newproject_notifications " .
		"left join postypes on postype_id = projecttype_newproject_notification_postype " . 
		"left join countries on country_id = projecttype_newproject_notification_country";

$list = new ListView($sql);

$list->set_entity("projecttype_newproject_notifications");
$list->set_order("country_name, projecttype_newproject_notification_postype");

$list->add_column("country_name", "Country", "pos_type_new_project_notification.php");
$list->add_column("postype_name", "POS Type");
$list->add_column("projecttype_newproject_notification_email", "Recipient 1");
$list->add_column("projecttype_newproject_notification_emailcc1", "Recipient 2");
$list->add_column("projecttype_newproject_notification_emailcc2", "Recipient 3");
$list->add_column("projecttype_newproject_notification_emailcc3", "Recipient 4");
$list->add_column("projecttype_newproject_notification_emailcc4", "Recipient 5");
$list->add_column("projecttype_newproject_notification_emailcc5", "Recipient 6");
$list->add_column("projecttype_newproject_notification_emailcc6", "Recipient 7");
$list->add_column("projecttype_newproject_notification_emailcc7", "Recipient 8");


$list->add_button(LIST_BUTTON_NEW, "Add New Notification Rule", "pos_type_new_project_notification.php");
$list->add_button("replace", "Replace Recipients", "pos_type_new_project_notification.php");


$list->process();


if($list->button("replace") )
{
	$link = "pos_type_new_project_notifications_replace_mail.php";
	redirect($link);
}

$page = new Page("projecttype_newproject_notifications");

$page->header();
$page->title("Notifications for Submissions");

$list->render();
$page->footer();

?>
