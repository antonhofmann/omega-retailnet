<?php
/********************************************************************

    message.php

    Creation and mutation of message records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-02
    Modified by:    Anbton Hofmann (aho@mediaparx.ch)
    Date modified:  2007-11-14
    Version:        1.1.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


//prepare data

$sql_admins = "select distinct user_id, concat(user_name, ' ' , user_firstname, ', ', address_company, ', ', country_name) as username from users left join user_roles on user_id = user_role_user left join addresses on address_id = user_address  left join countries on country_id = address_country where user_role_role = 1 and user_active = 1 order by username";

$admins = array();
$res = mysql_query($sql_admins) or dberror($sql_admins);
while ($row = mysql_fetch_assoc($res))
{
	$admins[] = $row["user_id"];
}

$sql_agents = "select distinct user_id, concat(country_name, ', ', user_name, ' ' , user_firstname, ', ', address_company) as username from users left join user_roles on user_id = user_role_user left join addresses on address_id = user_address left join countries on country_id = address_country where address_client_type = 1 and user_active = 1 order by country_name, username";

$agents = array();
$res = mysql_query($sql_agents) or dberror($sql_agents);
while ($row = mysql_fetch_assoc($res))
{
	$agents[] = $row["user_id"];
}

$sql_brand_managers = "select distinct user_id, concat(country_name, ', ', user_name, ' ' , user_firstname, ', ', address_company) as username from users left join user_roles on user_id = user_role_user left join addresses on address_id = user_address  left join countries on country_id = address_country where user_role_role = 15 and user_active = 1 order by country_name, username";

$brand_managers = array();
$res = mysql_query($sql_brand_managers) or dberror($sql_brand_managers);
while ($row = mysql_fetch_assoc($res))
{
	$brand_managers[] = $row["user_id"];
}

$sql_contract_supervisors = "select distinct user_id, concat(country_name, ', ', user_name, ' ' , user_firstname, ', ', address_company) as username from users left join user_roles on user_id = user_role_user left join addresses on address_id = user_address  left join countries on country_id = address_country where user_role_role = 13 and user_active = 1 order by country_name, username";

$contract_supervisors = array();
$res = mysql_query($sql_contract_supervisors) or dberror($sql_contract_supervisors);
while ($row = mysql_fetch_assoc($res))
{
	$contract_supervisors[] = $row["user_id"];
}

$sql_design_contractors = "select distinct user_id, concat(country_name, ', ', user_name, ' ' , user_firstname, ', ', address_company) as username from users left join user_roles on user_id = user_role_user left join addresses on address_id = user_address  left join countries on country_id = address_country where user_role_role = 7 and user_active = 1 order by country_name, username";

$design_contractors = array();
$res = mysql_query($sql_design_contractors) or dberror($sql_design_contractors);
while ($row = mysql_fetch_assoc($res))
{
	$design_contractors[] = $row["user_id"];
}


$sql_design_supervisors = "select distinct user_id, concat(country_name, ', ', user_name, ' ' , user_firstname, ', ', address_company) as username from users left join user_roles on user_id = user_role_user left join addresses on address_id = user_address  left join countries on country_id = address_country where user_role_role = 8 and user_active = 1 order by country_name, username";

$design_supervisors = array();
$res = mysql_query($sql_design_supervisors) or dberror($sql_design_supervisors);
while ($row = mysql_fetch_assoc($res))
{
	$design_supervisors[] = $row["user_id"];
}


$sql_financial_controllers = "select distinct user_id, concat(country_name, ', ', user_name, ' ' , user_firstname, ', ', address_company) as username from users left join user_roles on user_id = user_role_user left join addresses on address_id = user_address  left join countries on country_id = address_country where user_role_role = 22 and user_active = 1 order by country_name, username";

$financial_controllers = array();
$res = mysql_query($sql_financial_controllers) or dberror($sql_financial_controllers);
while ($row = mysql_fetch_assoc($res))
{
	$financial_controllers[] = $row["user_id"];
}


$sql_forwarders = "select distinct user_id, concat(country_name, ', ', user_name, ' ' , user_firstname, ', ', address_company) as username from users left join user_roles on user_id = user_role_user left join addresses on address_id = user_address  left join countries on country_id = address_country where user_role_role = 6 and user_active = 1 order by country_name, username";

$forwarders = array();
$res = mysql_query($sql_forwarders) or dberror($sql_forwarders);
while ($row = mysql_fetch_assoc($res))
{
	$forwarders[] = $row["user_id"];
}

$sql_local_project_managers = "select distinct user_id, concat(country_name, ', ', user_name, ' ' , user_firstname, ', ', address_company) as username from users left join user_roles on user_id = user_role_user left join addresses on address_id = user_address  left join countries on country_id = address_country where user_role_role = 10 and user_active = 1 order by country_name, username";


$local_project_managers = array();
$res = mysql_query($sql_local_project_managers) or dberror($sql_local_project_managers);
while ($row = mysql_fetch_assoc($res))
{
	$local_project_managers[] = $row["user_id"];
}

/*
$sql_regional_retail_managers = "select distinct user_id, concat(country_name, ', ', user_name, ' ' , user_firstname, ', ', address_company) as username from users left join user_roles on user_id = user_role_user left join addresses on address_id = user_address  left join countries on country_id = address_country where user_role_role = 19 and user_active = 1 order by country_name, username";

$regional_retail_managers = array();
$res = mysql_query($sql_regional_retail_managers) or dberror($sql_regional_retail_managers);
while ($row = mysql_fetch_assoc($res))
{
	$regional_retail_managers[] = $row["user_id"];
}
*/

$sql_project_managers = "select distinct user_id, concat(user_name, ' ' , user_firstname, ', ', address_company, ', ', country_name) as username from users left join user_roles on user_id = user_role_user left join addresses on address_id = user_address  left join countries on country_id = address_country where user_role_role = 3 and user_active = 1 order by username";


$project_managers = array();
$res = mysql_query($sql_project_managers) or dberror($sql_project_managers);
while ($row = mysql_fetch_assoc($res))
{
	$project_managers[] = $row["user_id"];
}


$sql_regional_sales_managers = "select distinct user_id, concat(country_name, ', ', user_name, ' ' , user_firstname, ', ', address_company) as username from users left join user_roles on user_id = user_role_user left join addresses on address_id = user_address  left join countries on country_id = address_country where user_role_role = 33 and user_active = 1 order by country_name, username";


$regional_sales_managers = array();
$res = mysql_query($sql_regional_sales_managers) or dberror($sql_regional_sales_managers);
while ($row = mysql_fetch_assoc($res))
{
	$regional_sales_managers[] = $row["user_id"];
}


$sql_retail_managers = "select distinct user_id, concat(country_name, ', ', user_name, ' ' , user_firstname, ', ', address_company) as username from users left join user_roles on user_id = user_role_user left join addresses on address_id = user_address  left join countries on country_id = address_country where user_role_role = 16 and user_active = 1 order by country_name, username";


$retail_managers = array();
$res = mysql_query($sql_retail_managers) or dberror($sql_retail_managers);
while ($row = mysql_fetch_assoc($res))
{
	$retail_managers[] = $row["user_id"];
}


$sql_retail_operators = "select distinct user_id, concat(user_name, ' ' , user_firstname, ', ', address_company, ', ', country_name) as username from users left join user_roles on user_id = user_role_user left join addresses on address_id = user_address  left join countries on country_id = address_country where user_role_role = 2 and user_active = 1 order by username";


$retail_operators = array();
$res = mysql_query($sql_retail_operators) or dberror($sql_retail_operators);
while ($row = mysql_fetch_assoc($res))
{
	$retail_operators[] = $row["user_id"];
}


$sql_subsidiaries = "select distinct user_id, concat(country_name, ', ', user_name, ' ' , user_firstname, ', ', address_company) as username from users left join user_roles on user_id = user_role_user left join addresses on address_id = user_address  left join countries on country_id = address_country where address_client_type = 2 and user_active = 1 order by country_name, username";

$subsidiaries = array();
$res = mysql_query($sql_subsidiaries) or dberror($sql_subsidiaries);
while ($row = mysql_fetch_assoc($res))
{
	$subsidiaries[] = $row["user_id"];
}


$sql_suppliers = "select distinct user_id, concat(country_name, ', ', user_name, ' ' , user_firstname, ', ', address_company) as username from users left join user_roles on user_id = user_role_user left join addresses on address_id = user_address  left join countries on country_id = address_country where user_role_role = 5 and user_active = 1 order by country_name, username";


$suppliers = array();
$res = mysql_query($sql_suppliers) or dberror($sql_suppliers);
while ($row = mysql_fetch_assoc($res))
{
	$suppliers[] = $row["user_id"];
}


$sql_visitors_cer = "select distinct user_id, concat(country_name, ', ', user_name, ' ' , user_firstname, ', ', address_company) as username from users left join user_roles on user_id = user_role_user left join addresses on address_id = user_address  left join countries on country_id = address_country where user_role_role = 25 and user_active = 1 order by country_name, username";


$visitors_cer = array();
$res = mysql_query($sql_visitors_cer) or dberror($sql_visitors_cer);
while ($row = mysql_fetch_assoc($res))
{
	$visitors_cer[] = $row["user_id"];
}


$sql_visitors_posindex = "select distinct user_id, concat(country_name, ', ', user_name, ' ' , user_firstname, ', ', address_company) as username from users left join user_roles on user_id = user_role_user left join addresses on address_id = user_address  left join countries on country_id = address_country where user_role_role = 20 and user_active = 1 order by country_name, username";


$visitors_posindex = array();
$res = mysql_query($sql_visitors_posindex) or dberror($sql_visitors_posindex);
while ($row = mysql_fetch_assoc($res))
{
	$visitors_posindex[] = $row["user_id"];
}


$sql_visitors_projects = "select distinct user_id, concat(country_name, ', ', user_name, ' ' , user_firstname, ', ', address_company) as username from users left join user_roles on user_id = user_role_user left join addresses on address_id = user_address  left join countries on country_id = address_country where user_role_role = 9 and user_active = 1 order by country_name, username";

$visitors_projects = array();
$res = mysql_query($sql_visitors_projects) or dberror($sql_visitors_projects);
while ($row = mysql_fetch_assoc($res))
{
	$visitors_projects[] = $row["user_id"];
}


$sql_visualisation = "select distinct user_id, concat(country_name, ', ', user_name, ' ' , user_firstname, ', ', address_company) as username from users left join user_roles on user_id = user_role_user left join addresses on address_id = user_address  left join countries on country_id = address_country where user_role_role = 11 and user_active = 1 order by country_name, username";

$visualisation = array();
$res = mysql_query($sql_visualisation) or dberror($sql_visualisation);
while ($row = mysql_fetch_assoc($res))
{
	$visualisation[] = $row["user_id"];
}

//create form

$form = new Form("messages", "message");

$form->add_section();
$form->add_edit("message_title", "Title", NOTNULL);
$form->add_edit("message_date", "Date", NOTNULL, date("d.m.y"));
$form->add_edit("message_expiry_date", "Expiry Date");

$form->add_section();
$form->add_multiline("message_text", "Content", 10);

$form->add_section("Attach File");

$form->add_comment("Attach the following File to the mail message:");
$form->add_list("attachment", "File",
    "select link_id, link_title from links " .
    "where link_path is not null " . 
    "order by link_title");

$form->add_section("Importance");
$form->add_checkbox("message_important", "Show Warning Icon");


$form->add_section("Accessibility");
$form->add_comment("Please indicate who is allowed to see this message.");
$form->add_checklist("message_roles", "", "message_roles",
    "select role_id, role_name from roles order by role_name");


$form->add_section("Notification Recipients");
$form->add_comment("Indicate all the persons to recieve an email notification containing the message.");

$form->add_comment("");
$form->add_label("select0", "Administrators", RENDER_HTML, "<div id='select0'><a href='javascript:select_all_admins();'>select all</a></div>");
//$form->add_checkbox("allADMNs", "to all Administrators or to",0, 0, "Administrators");
//$form->add_checklist("ADMNs", "", "message_emails", $sql_admins);
if(id() == 0)
{
	$form->add_checklist("ADMNs", "Persons", "message_emails", $sql_admins, 0, "", true );
}
else
{
	$form->add_checklist("ADMNs", "Persons", "message_emails", $sql_admins, 0, "", false );
}


$form->add_comment("");
$form->add_label("select1", "Agents", RENDER_HTML, "<div id='select1'><a href='javascript:select_all_agents();'>select all</a></div>");
//$form->add_checkbox("allAGENTSs", "to all Agents or to",0, 0, "Agents (Clients)");
//$form->add_checklist("AGENTSs", "", "message_emails", $sql_agents);

if(id() == 0)
{
	$form->add_checklist("AGENTSs", "Persons", "message_emails", $sql_agents, 0, "", true );
}
else
{
	$form->add_checklist("AGENTSs", "Persons", "message_emails", $sql_agents, 0, "", false );
}

$form->add_comment("");
$form->add_label("select2", "Brand Managers", RENDER_HTML, "<div id='select2'><a href='javascript:select_all_brand_managers();'>select all</a></div>");
//$form->add_checkbox("allBRMAs", "to all Brand Managers or to",0, 0, "Brand Managers");
//$form->add_checklist("BRMAs", "", "message_emails", $sql_brand_managers);

if(id() == 0)
{
	$form->add_checklist("BRMAs", "Persons", "message_emails", $sql_brand_managers, 0, "", true );
}
else
{
	$form->add_checklist("BRMAs", "Persons", "message_emails", $sql_brand_managers, 0, "", false );
}

$form->add_comment("");
$form->add_label("select3", "Contract Supervisors", RENDER_HTML, "<div id='select3'><a href='javascript:select_all_contract_supervisors();'>select all</a></div>");
//$form->add_checkbox("allCOSUs", "to all Contract Supervisors or to",0, 0, "Contract Supervisors");
//$form->add_checklist("COSUs", "", "message_emails", $sql_contract_supervisors);

if(id() == 0)
{
	$form->add_checklist("COSUs", "Persons", "message_emails", $sql_contract_supervisors, 0, "", true );
}
else
{
	$form->add_checklist("COSUs", "Persons", "message_emails", $sql_contract_supervisors, 0, "", false );
}
$form->add_comment("");
$form->add_label("select4", "Design Contractors", RENDER_HTML, "<div id='select4'><a href='javascript:select_all_design_contractors();'>select all</a></div>");
//$form->add_checkbox("allDCONs", "to all Design Contractors or to",0, 0, "Design Contractors");
//$form->add_checklist("DCONs", "", "message_emails", $sql_design_contractors);

if(id() == 0)
{
	$form->add_checklist("DCONs", "Persons", "message_emails", $sql_design_contractors, 0, "", true );
}
else
{
	$form->add_checklist("DCONs", "Persons", "message_emails", $sql_design_contractors, 0, "", false );
}

$form->add_comment("");
$form->add_label("select5", "Design Supervisors", RENDER_HTML, "<div id='select5'><a href='javascript:select_all_design_supervisors();'>select all</a></div>");
//$form->add_checkbox("allDSUPs", "to all Design Supervisors or to",0, 0, "Design Supervisors");
//$form->add_checklist("DSUPs", "", "message_emails", $sql_design_supervisors);

if(id() == 0)
{
	$form->add_checklist("DSUPs", "Persons", "message_emails", $sql_design_supervisors, 0, "", true );
}
else
{
	$form->add_checklist("DSUPs", "Persons", "message_emails", $sql_design_supervisors, 0, "", false );
}

$form->add_comment("");
$form->add_label("select6", "Financial Controllers", RENDER_HTML, "<div id='select6'><a href='javascript:select_all_financial_controllers();'>select all</a></div>");
//$form->add_checkbox("allFINCs", "to all Financial Controllers or to",0, 0, "Financial Controllers");
//$form->add_checklist("FINCs", "", "message_emails", $sql_financial_controllers);

if(id() == 0)
{
	$form->add_checklist("FINCs", "Persons", "message_emails", $sql_financial_controllers, 0, "", true );
}
else
{
	$form->add_checklist("FINCs", "Persons", "message_emails", $sql_financial_controllers, 0, "", false );
}
$form->add_comment("");
$form->add_label("select7", "Forwarders", RENDER_HTML, "<div id='select7'><a href='javascript:select_all_forwarders();'>select all</a></div>");
//$form->add_checkbox("allFWRDs", "to all Forwarders or to",0, 0, "Forwarders");
//$form->add_checklist("FWRDs", "", "message_emails", $sql_forwarders);

if(id() == 0)
{
	$form->add_checklist("FWRDs", "Persons", "message_emails", $sql_forwarders, 0, "", true );
}
else
{
	$form->add_checklist("FWRDs", "Persons", "message_emails", $sql_forwarders, 0, "", false );
}


$form->add_comment("");
$form->add_label("select8", "Local Project Managers", RENDER_HTML, "<div id='select8'><a href='javascript:select_all_local_project_managers();'>select all</a></div>");
//$form->add_checkbox("allLORCs", "to all Local Project Managers or to",0, 0, "Local Project Managers");
//$form->add_checklist("LORCs", "", "message_emails", $sql_local_project_managers);

if(id() == 0)
{
	$form->add_checklist("LORCs", "Persons", "message_emails", $sql_local_project_managers, 0, "", true );
}
else
{
	$form->add_checklist("LORCs", "Persons", "message_emails", $sql_local_project_managers, 0, "", false );
}

/*
$form->add_comment("");
$form->add_label("select10", "HQ Project Managers", RENDER_HTML, "<div id='select10'><a href='javascript:select_all_regional_retail_managers();'>select all</a></div>");
//$form->add_checkbox("allRRMAs", "to all HQ Project Managers or to",0, 0, "HQ Project Managers");
$form->add_checklist("RRMAs", "", "message_emails", $sql_regional_retail_managers);
*/

$form->add_comment("");
$form->add_label("select11", "Project Managers", RENDER_HTML, "<div id='select11'><a href='javascript:select_all_project_managers();'>select all</a></div>");
//$form->add_checkbox("allRTCOs", "to all Project Managers or to",0, 0, "Project Manager");
//$form->add_checklist("RTCOs", "", "message_emails", $sql_project_managers);

if(id() == 0)
{
	$form->add_checklist("RTCOs", "Persons", "message_emails", $sql_project_managers, 0, "", true );
}
else
{
	$form->add_checklist("RTCOs", "Persons", "message_emails", $sql_project_managers, 0, "", false );
}


$form->add_comment("");
$form->add_label("select11a", "Regional Sales Manager HQ", RENDER_HTML, "<div id='select11a'><a href='javascript:select_all_regional_sales_managers();'>select all</a></div>");
//$form->add_checklist("RSMAs", "", "message_emails", $sql_regional_sales_managers);

if(id() == 0)
{
	$form->add_checklist("RSMAs", "Persons", "message_emails", $sql_regional_sales_managers, 0, "", true );
}
else
{
	$form->add_checklist("RSMAs", "Persons", "message_emails", $sql_regional_sales_managers, 0, "", false );
}

$form->add_comment("");
$form->add_label("select12", "Retail Managers", RENDER_HTML, "<div id='select12'><a href='javascript:select_all_retail_managers();'>select all</a></div>");
//$form->add_checkbox("allRTMAs", "to all Retail Managers or to",0, 0, "Retail Managers");
//$form->add_checklist("RTMAs", "", "message_emails", $sql_retail_managers);

if(id() == 0)
{
	$form->add_checklist("RTMAs", "Persons", "message_emails", $sql_retail_managers, 0, "", true );
}
else
{
	$form->add_checklist("RTMAs", "Persons", "message_emails", $sql_retail_managers, 0, "", false );
}
$form->add_comment("");
$form->add_label("select13", "Retail Operators", RENDER_HTML, "<div id='select13'><a href='javascript:select_all_retail_operators();'>select all</a></div>");
//$form->add_checkbox("allRTOPs", "to all Retail Operators or to",0, 0, "Retail Operators");
//$form->add_checklist("RTOPs", "", "message_emails", $sql_retail_operators);


if(id() == 0)
{
	$form->add_checklist("RTOPs", "Persons", "message_emails", $sql_retail_operators, 0, "", true );
}
else
{
	$form->add_checklist("RTOPs", "Persons", "message_emails", $sql_retail_operators, 0, "", false );
}
$form->add_comment("");
$form->add_label("select15", "Subsidiaries", RENDER_HTML, "<div id='select15'><a href='javascript:select_all_subsidiaries();'>select all</a></div>");
//$form->add_checkbox("allSUBs", "to all Subsidiaries or to",0, 0, "Subsidiaries (Clients)");
//$form->add_checklist("SUBs", "", "message_emails", $sql_subsidiaries);

if(id() == 0)
{
	$form->add_checklist("SUBs", "Persons", "message_emails", $sql_subsidiaries, 0, "", true );
}
else
{
	$form->add_checklist("SUBs", "Persons", "message_emails", $sql_subsidiaries, 0, "", false );
}


$form->add_comment("");
$form->add_label("select16", "Suppliers", RENDER_HTML, "<div id='select16'><a href='javascript:select_all_suppliers();'>select all</a></div>");
//$form->add_checkbox("allSUPPs", "to all Suppliers or to",0, 0, "Suppliers");
//$form->add_checklist("SUPPs", "", "message_emails", $sql_suppliers);

if(id() == 0)
{
	$form->add_checklist("SUPPs", "Persons", "message_emails", $sql_suppliers, 0, "", true );
}
else
{
	$form->add_checklist("SUPPs", "Persons", "message_emails", $sql_suppliers, 0, "", false );
}

$form->add_comment("");
$form->add_label("select18", "Visitors CER", RENDER_HTML, "<div id='select18'><a href='javascript:select_all_visitors_cer();'>select all</a></div>");
//$form->add_checkbox("allVCERs", "to all Visitors CER / AF or to",0, 0, "Visitors CER / AF");
//$form->add_checklist("VCERs", "", "message_emails", $sql_visitors_cer);

if(id() == 0)
{
	$form->add_checklist("VCERs", "Persons", "message_emails", $sql_visitors_cer, 0, "", true );
}
else
{
	$form->add_checklist("VCERs", "Persons", "message_emails", $sql_visitors_cer, 0, "", false );
}
$form->add_comment("");
$form->add_label("select19", "Visitors POS Index", RENDER_HTML, "<div id='select19'><a href='javascript:select_all_visitors_posindex();'>select all</a></div>");
//$form->add_checkbox("allVPOSs", "to all Visitors POS Index or to",0, 0, "Visitors POS Index");
//$form->add_checklist("VPOSs", "", "message_emails", $sql_visitors_posindex);

if(id() == 0)
{
	$form->add_checklist("VPOSs", "Persons", "message_emails", $sql_visitors_posindex, 0, "", true );
}
else
{
	$form->add_checklist("VPOSs", "Persons", "message_emails", $sql_visitors_posindex, 0, "", false );
}

$form->add_comment("");
$form->add_label("select20", "Visitors Projects", RENDER_HTML, "<div id='select20'><a href='javascript:select_all_visitors_projects();'>select all</a></div>");
//$form->add_checkbox("allVIEWs", "to all Visitors RetailNet Projects or to",0, 0, "Visitors RetailNet Projects");
//$form->add_checklist("VIEWs", "", "message_emails", $sql_visitors_projects);

if(id() == 0)
{
	$form->add_checklist("VIEWs", "Persons", "message_emails", $sql_visitors_projects, 0, "", true );
}
else
{
	$form->add_checklist("VIEWs", "Persons", "message_emails", $sql_visitors_projects, 0, "", false );
}

$form->add_comment("");
$form->add_label("select21", "Visualization Local Country", RENDER_HTML, "<div id='select21'><a href='javascript:select_all_visualisation();'>select all</a></div>");

if(id() == 0)
{
	$form->add_checklist("LOCMs", "Persons", "message_emails", $sql_visualisation, 0, "", true );
}
else
{
	$form->add_checklist("LOCMs", "Persons", "message_emails", $sql_visualisation, 0, "", false );
}

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

if ($form->button(FORM_BUTTON_SAVE))
{
    //sen mails to the checked recipients

	require_once("message_send_mails.php");
}


$page = new Page("messages");
$page->header();
$page->title(id() ? "Edit Message" : "Add Message");
$form->render();

?>


<script language='javascript'>

	function select_all_admins()
	{
		<?php
		foreach($admins as $key=>$user_id)
		{
			echo "document.getElementById('ADMNs_" . $user_id . "').checked = true;";
		}
		?>

		var div = document.getElementById('select0');
		div.innerHTML = "<a href='javascript:deselect_all_admins();'>deselect all</a>";
		
		
	}

	function deselect_all_admins()
	{
		<?php
		foreach($admins as $key=>$user_id)
		{
			echo "document.getElementById('ADMNs_" . $user_id . "').checked = false;";
		}
		?>

		var div = document.getElementById('select0');
		div.innerHTML = "<a href='javascript:select_all_admins();'>select all</a>";
	}

	function select_all_agents()
	{
		<?php
		foreach($agents as $key=>$user_id)
		{
			echo "document.getElementById('AGENTSs_" . $user_id . "').checked = true;";
		}
		?>
		
		var div = document.getElementById('select1');
		div.innerHTML = "<a href='javascript:deselect_all_agents();'>deselect all</a>";
	}

	function deselect_all_agents()
	{
		<?php
		foreach($agents as $key=>$user_id)
		{
			echo "document.getElementById('AGENTSs_" . $user_id . "').checked = false;";
		}
		?>
		var div = document.getElementById('select1');
		div.innerHTML = "<a href='javascript:select_all_agents();'>select all</a>";
	}
	
	function select_all_brand_managers()
	{
		<?php
		foreach($brand_managers as $key=>$user_id)
		{
			echo "document.getElementById('BRMAs_" . $user_id . "').checked = true;";
		}
		?>
		var div = document.getElementById('select2');
		div.innerHTML = "<a href='javascript:deselect_all_brand_managers();'>deselect all</a>";
	}

	function deselect_all_brand_managers()
	{
		<?php
		foreach($brand_managers as $key=>$user_id)
		{
			echo "document.getElementById('BRMAs_" . $user_id . "').checked = false;";
		}
		?>
		var div = document.getElementById('select2');
		div.innerHTML = "<a href='javascript:select_all_brand_managers();'>select all</a>";
	}

	function select_all_contract_supervisors()
	{
		<?php
		foreach($contract_supervisors as $key=>$user_id)
		{
			echo "document.getElementById('COSUs_" . $user_id . "').checked = true;";
		}
		?>
		var div = document.getElementById('select3');
		div.innerHTML = "<a href='javascript:deselect_all_contract_supervisors();'>deselect all</a>";
	}

	function deselect_all_contract_supervisors()
	{
		<?php
		foreach($contract_supervisors as $key=>$user_id)
		{
			echo "document.getElementById('COSUs_" . $user_id . "').checked = false;";
		}
		?>
		var div = document.getElementById('select3');
		div.innerHTML = "<a href='javascript:select_all_contract_supervisors();'>select all</a>";
	}

	function select_all_design_contractors()
	{
		<?php
		foreach($design_contractors as $key=>$user_id)
		{
			echo "document.getElementById('DCONs_" . $user_id . "').checked = true;";
		}
		?>
		var div = document.getElementById('select4');
		div.innerHTML = "<a href='javascript:deselect_all_design_contractors();'>deselect all</a>";
	}

	function deselect_all_design_contractors()
	{
		<?php
		foreach($design_contractors as $key=>$user_id)
		{
			echo "document.getElementById('DCONs_" . $user_id . "').checked = false;";
		}
		?>
		var div = document.getElementById('select4');
		div.innerHTML = "<a href='javascript:select_all_design_contractors();'>select all</a>";
	}

	function select_all_design_supervisors()
	{
		<?php
		foreach($design_supervisors as $key=>$user_id)
		{
			echo "document.getElementById('DSUPs_" . $user_id . "').checked = true;";
		}
		?>
		var div = document.getElementById('select5');
		div.innerHTML = "<a href='javascript:deselect_all_design_supervisors();'>deselect all</a>";
	}

	function deselect_all_design_supervisors()
	{
		<?php
		foreach($design_supervisors as $key=>$user_id)
		{
			echo "document.getElementById('DSUPs_" . $user_id . "').checked = false;";
		}
		?>
		var div = document.getElementById('select5');
		div.innerHTML = "<a href='javascript:select_all_design_supervisors();'>select all</a>";
	}

	function select_all_financial_controllers()
	{
		<?php
		foreach($financial_controllers as $key=>$user_id)
		{
			echo "document.getElementById('FINCs_" . $user_id . "').checked = true;";
		}
		?>
		var div = document.getElementById('select6');
		div.innerHTML = "<a href='javascript:deselect_all_financial_controllers();'>deselect all</a>";
	}
	
	function deselect_all_financial_controllers()
	{
		<?php
		foreach($financial_controllers as $key=>$user_id)
		{
			echo "document.getElementById('FINCs_" . $user_id . "').checked = false;";
		}
		?>
		var div = document.getElementById('select6');
		div.innerHTML = "<a href='javascript:select_all_financial_controllers();'>select all</a>";
	}

	function select_all_forwarders()
	{
		<?php
		foreach($forwarders as $key=>$user_id)
		{
			echo "document.getElementById('FWRDs_" . $user_id . "').checked = true;";
		}
		?>
		var div = document.getElementById('select7');
		div.innerHTML = "<a href='javascript:deselect_all_forwarders();'>deselect all</a>";
	}

	function deselect_all_forwarders()
	{
		<?php
		foreach($forwarders as $key=>$user_id)
		{
			echo "document.getElementById('FWRDs_" . $user_id . "').checked = false;";
		}
		?>
		var div = document.getElementById('select7');
		div.innerHTML = "<a href='javascript:select_all_forwarders();'>select all</a>";
	}


	function select_all_regional_sales_managers()
	{
		<?php
		foreach($regional_sales_managers as $key=>$user_id)
		{
			echo "document.getElementById('RSMAs_" . $user_id . "').checked = true;";
		}
		?>
		var div = document.getElementById('select11a');
		div.innerHTML = "<a href='javascript:deselect_all_regional_sales_managers();'>deselect all</a>";
	}

	function deselect_all_regional_sales_managers()
	{
		<?php
		foreach($regional_sales_managers as $key=>$user_id)
		{
			echo "document.getElementById('RSMAs_" . $user_id . "').checked = false;";
		}
		?>
		var div = document.getElementById('select11a');
		div.innerHTML = "<a href='javascript:select_all_regional_sales_managers();'>select all</a>";
	}

	function select_all_local_project_managers()
	{
		<?php
		foreach($local_project_managers as $key=>$user_id)
		{
			echo "document.getElementById('LORCs_" . $user_id . "').checked = true;";
		}
		?>
		var div = document.getElementById('select8');
		div.innerHTML = "<a href='javascript:deselect_all_local_project_managers();'>deselect all</a>";
	}

	function deselect_all_local_project_managers()
	{
		<?php
		foreach($local_project_managers as $key=>$user_id)
		{
			echo "document.getElementById('LORCs_" . $user_id . "').checked = false;";
		}
		?>
		var div = document.getElementById('select8');
		div.innerHTML = "<a href='javascript:select_all_local_project_managers();'>select all</a>";
	}

	
	
	

	function select_all_project_managers()
	{
		<?php
		foreach($project_managers as $key=>$user_id)
		{
			echo "document.getElementById('RTCOs_" . $user_id . "').checked = true;";
		}
		?>
		var div = document.getElementById('select11');
		div.innerHTML = "<a href='javascript:deselect_all_project_managers();'>deselect all</a>";
	}

	function deselect_all_project_managers()
	{
		<?php
		foreach($project_managers as $key=>$user_id)
		{
			echo "document.getElementById('RTCOs_" . $user_id . "').checked = false;";
		}
		?>
		var div = document.getElementById('select11');
		div.innerHTML = "<a href='javascript:select_all_project_managers();'>select all</a>";
	}

	function select_all_retail_managers()
	{
		<?php
		foreach($retail_managers as $key=>$user_id)
		{
			echo "document.getElementById('RTMAs_" . $user_id . "').checked = true;";
		}
		?>
		var div = document.getElementById('select12');
		div.innerHTML = "<a href='javascript:deselect_all_retail_managers();'>deselect all</a>";
	}

	function deselect_all_retail_managers()
	{
		<?php
		foreach($retail_managers as $key=>$user_id)
		{
			echo "document.getElementById('RTMAs_" . $user_id . "').checked = false;";
		}
		?>
		var div = document.getElementById('select12');
		div.innerHTML = "<a href='javascript:select_all_retail_managers();'>select all</a>";
	}

	function select_all_retail_operators()
	{
		<?php
		foreach($retail_operators as $key=>$user_id)
		{
			echo "document.getElementById('RTOPs_" . $user_id . "').checked = true;";
		}
		?>
		var div = document.getElementById('select13');
		div.innerHTML = "<a href='javascript:deselect_all_retail_operators();'>deselect all</a>";
	}

	function deselect_all_retail_operators()
	{
		<?php
		foreach($retail_operators as $key=>$user_id)
		{
			echo "document.getElementById('RTOPs_" . $user_id . "').checked = false;";
		}
		?>
		var div = document.getElementById('select13');
		div.innerHTML = "<a href='javascript:select_all_retail_operators();'>select all</a>";
	}

	
	function select_all_subsidiaries()
	{
		<?php
		foreach($subsidiaries as $key=>$user_id)
		{
			echo "document.getElementById('SUBs_" . $user_id . "').checked = true;";
		}
		?>
		var div = document.getElementById('select15');
		div.innerHTML = "<a href='javascript:deselect_all_subsidiaries();'>deselect all</a>";
	}

	function deselect_all_subsidiaries()
	{
		<?php
		foreach($subsidiaries as $key=>$user_id)
		{
			echo "document.getElementById('SUBs_" . $user_id . "').checked = false;";
		}
		?>
		var div = document.getElementById('select15');
		div.innerHTML = "<a href='javascript:select_all_subsidiaries();'>select all</a>";
	}


	function select_all_suppliers()
	{
		<?php
		foreach($suppliers as $key=>$user_id)
		{
			echo "document.getElementById('SUPPs_" . $user_id . "').checked = true;";
		}
		?>
		var div = document.getElementById('select16');
		div.innerHTML = "<a href='javascript:deselect_all_suppliers();'>deselect all</a>";
	}

	function deselect_all_suppliers()
	{
		<?php
		foreach($suppliers as $key=>$user_id)
		{
			echo "document.getElementById('SUPPs_" . $user_id . "').checked = false;";
		}
		?>
		var div = document.getElementById('select16');
		div.innerHTML = "<a href='javascript:select_all_suppliers();'>select all</a>";
	}

	
	function select_all_visitors_cer()
	{
		<?php
		foreach($visitors_cer as $key=>$user_id)
		{
			echo "document.getElementById('VCERs_" . $user_id . "').checked = true;";
		}
		?>
		var div = document.getElementById('select18');
		div.innerHTML = "<a href='javascript:deselect_all_visitors_cer();'>deselect all</a>";
	}

	function deselect_all_visitors_cer()
	{
		<?php
		foreach($visitors_cer as $key=>$user_id)
		{
			echo "document.getElementById('VCERs_" . $user_id . "').checked = false;";
		}
		?>
		var div = document.getElementById('select18');
		div.innerHTML = "<a href='javascript:select_all_visitors_cer();'>select all</a>";
	}

	function select_all_visitors_posindex()
	{
		<?php
		foreach($visitors_posindex as $key=>$user_id)
		{
			echo "document.getElementById('VPOSs_" . $user_id . "').checked = true;";
		}
		?>
		var div = document.getElementById('select19');
		div.innerHTML = "<a href='javascript:deselect_all_visitors_posindex();'>deselect all</a>";
	}

	function deselect_all_visitors_posindex()
	{
		<?php
		foreach($visitors_posindex as $key=>$user_id)
		{
			echo "document.getElementById('VPOSs_" . $user_id . "').checked = false;";
		}
		?>
		var div = document.getElementById('select19');
		div.innerHTML = "<a href='javascript:select_all_visitors_posindex();'>select all</a>";
	}

	function select_all_visitors_projects()
	{
		<?php
		foreach($visitors_projects as $key=>$user_id)
		{
			echo "document.getElementById('VIEWs_" . $user_id . "').checked = true;";
		}
		?>
		var div = document.getElementById('select20');
		div.innerHTML = "<a href='javascript:deselect_all_visitors_projects();'>deselect all</a>";
	}

	function deselect_all_visitors_projects()
	{
		<?php
		foreach($visitors_projects as $key=>$user_id)
		{
			echo "document.getElementById('VIEWs_" . $user_id . "').checked = false;";
		}
		?>
		var div = document.getElementById('select20');
		div.innerHTML = "<a href='javascript:select_all_visitors_projects();'>select all</a>";
	}

	function select_all_visualisation()
	{
		<?php
		foreach($visualisation as $key=>$user_id)
		{
			echo "document.getElementById('LOCMs_" . $user_id . "').checked = true;";
		}
		?>
		var div = document.getElementById('select21');
		div.innerHTML = "<a href='javascript:deselect_all_visualisation();'>deselect all</a>";
	}

	function deselect_all_visualisation()
	{
		<?php
		foreach($visualisation as $key=>$user_id)
		{
			echo "document.getElementById('LOCMs_" . $user_id . "').checked = false;";
		}
		?>
		var div = document.getElementById('select21');
		div.innerHTML = "<a href='javascript:select_all_visualisation();'>select all</a>";
	}

		

</script>

<?php

$page->footer();

?>