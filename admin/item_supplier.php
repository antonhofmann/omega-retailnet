<?php
/********************************************************************

    item_supplier.php

    Mutation of supplier records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-20
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-06-04
    Version:        1.3.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_browse_catalog_in_admin");

// Determine store id

$sql = "select store_id " .
       "from items right join suppliers on item_id = supplier_item " .
       "    left join stores on item_id = store_item " .
       "where supplier_item = store_item and supplier_address = store_address and supplier_id = " . id();

$res = mysql_query($sql) or dberror($sql);

if (mysql_num_rows($res))
{
    $row = mysql_fetch_assoc($res);
    $store_id = $row["store_id"];
}
else
{
    $store_id = 0;
}


$item_code = "";
$item_description = "";
$sql = "select item_code, item_description from items where item_id = " . dbquote(param("item"));
$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res)) {
    $item_code = $row["item_code"];
	$item_description = $row["item_description"];
}


$sql_suppliers = "select address_id, address_company " .
				"from addresses left join address_types on address_type = address_type_id " .
				"where address_active = 1 and address_type_code IN ('SUPP', 'WAHO') " .
				"order by address_company";

// Build form

$form = new Form("suppliers", "supplier");
$form->add_hidden("sa", param("sa"));
$form->add_hidden("item", param("item"));

$form->add_section();
$form->add_lookup("supplier_item", "Item", "items", "item_code", 0, param("item"));

$form->add_section();



$form->add_list("supplier_address", "Supplier", $sql_suppliers, NOTNULL |SUBMIT);


$sql_currencies = "select currency_id, currency_symbol ".
                  "from currencies ".
                  "order by currency_symbol";

$form->add_section();
if(id() > 0) {
	$form->add_edit("supplier_item_code", "Code", NOTNULL);
	$form->add_multiline("supplier_item_name", "Name", 4, 0);
}
else
{
	$form->add_edit("supplier_item_code", "Code", NOTNULL, $item_code);
	$form->add_multiline("supplier_item_name", "Name", 4, 0, $item_description);
}

$form->add_edit("supplier_item_price", "Price", NOTNULL);
$form->add_list("supplier_item_currency", "Currency", $sql_currencies, NOTNULL);

// Build static store part

$sql = "select store_global_order, store_last_global_order, store_last_global_order_date, " .
       "    store_last_global_order_confirmation_date, store_inventory, " .
       "    store_stock_control_starting_date, store_consumed_upto_021101, store_minimum_order_quantity, " .
       "    store_reproduction_time_in_weeks, store_consumption_coverage, store_physical_stock, " .
       "    store_minimum_stock, store_show_in_stock_control ".
       "from stores " .
       "where store_id = $store_id";

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

$form->add_section("Store");

$form->add_section();


//$form->add_label("store_global_order", "Sum of Global Orders ever made:", 0, $row["store_global_order"]);


$form->add_edit("store_global_order", "Sum of Global Orders ever made:", 0, $row["store_global_order"], TYPE_DECIMAL, 10, 2);

$form->add_label("store_last_global_order", "Last Global Order", 0, $row["store_last_global_order"]);
$form->add_label("store_last_global_order_date", "Last Global Order Date", 0, to_system_date($row["store_last_global_order_date"]));

$form->add_section();
$form->add_checkbox("store_show_in_stock_control", "Show in Stock Control", $row["store_show_in_stock_control"]);
$form->add_edit("store_stock_control_starting_date", "Stock Control Starting Date", 0, to_system_date($row["store_stock_control_starting_date"]), TYPE_DATE, 8);

$form->add_edit("store_consumed_upto_021101", "Consumed since Starting Date up to 1.11.02", 0, $row["store_consumed_upto_021101"], TYPE_DECIMAL, 10, 2);

$form->add_edit("store_minimum_order_quantity", "Minimum Order Quantity", 0, $row["store_minimum_order_quantity"], TYPE_DECIMAL, 10, 2);
$form->add_edit("store_reproduction_time_in_weeks", "Reproduction Time in Weeks", 0, $row["store_reproduction_time_in_weeks"], TYPE_INT, 6);

$form->add_edit("store_physical_stock", "Physical Stock", 0, $row["store_physical_stock"], TYPE_DECIMAL, 10, 2);
$form->add_edit("store_minimum_stock", "Minimum Stock", 0, $row["store_minimum_stock"], TYPE_DECIMAL, 10, 2);

if(has_access("can_edit_catalog"))
{
	$form->add_button(FORM_BUTTON_SAVE, "Save");
	$form->add_button("back", "Back");
	$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);
}
else
{
	$form->add_button("back", "Back");
}

// Populate form and process button clicks

$form->populate();
$form->process();

if ($form->button(FORM_BUTTON_SAVE) && !$form->error())
{
    $fields[] = "store_item";
    $values[] = dbquote($form->value("supplier_item"));

    $fields[] = "store_address";
    $values[] = dbquote($form->value("supplier_address"));

    $fields[] = "store_global_order";
    $values[] = dbquote($form->value("store_global_order"));

    $fields[] = "store_stock_control_starting_date";
    $values[] = dbquote(from_system_date($form->value("store_stock_control_starting_date")), true);

    $fields[] = "store_consumed_upto_021101";
    $values[] = dbquote($form->value("store_consumed_upto_021101"));

    $fields[] = "store_minimum_order_quantity";
    $values[] = dbquote($form->value("store_minimum_order_quantity"));

    $fields[] = "store_reproduction_time_in_weeks";
    $values[] = dbquote($form->value("store_reproduction_time_in_weeks"));
    
    $fields[] = "store_physical_stock";
    $values[] = dbquote($form->value("store_physical_stock"));

    $fields[] = "store_minimum_stock";
    $values[] = dbquote($form->value("store_minimum_stock"));

    $fields[] = "store_show_in_stock_control";
    $values[] = dbquote($form->value("store_show_in_stock_control"));

    $fields[] = "user_modified";
    $values[] = dbquote(user_login());

    $fields[] = "date_modified";
    $values[] = "current_timestamp";

    if ($store_id)
    {
        $elems = array();

        for ($i = 0; $i < count($fields); $i++)
        {
            $elems[] = $fields[$i] . " = " . $values[$i];
        }

        $sql = "update stores set " . join(", ", $elems) . " where store_id = $store_id";
    }
    else
    {
        $fields[] = "user_created";
        $values[] = dbquote(user_login());

        $fields[] = "date_created";
        $values[] = "current_timestamp";

        $sql = "insert into stores (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
    }

    $res = mysql_query($sql) or dberror($sql);
}
elseif($form->button("supplier_address")) {

	$sql = 'select address_currency from addresses where address_id = ' . dbquote($form->value("supplier_address"));
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);

	$form->value("supplier_item_currency", $row["address_currency"]);
}
elseif ($form->button("back"))
{
	redirect("item.php?sa=" . param("sa") . "&id=" . param("item"));
}

// Render page

$page = new Page("items");

$page->header();
if(has_access("can_edit_catalog"))
{
	$page->title(id() ? "Edit Supplier Data" : "Add Supplier");
}
else
{
	$page->title("View Supplier Data");
}
$form->render();
$page->footer();

?>