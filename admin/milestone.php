<?php
/********************************************************************

    milestone.php

    Creation and mutation of milestones.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-23
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-23
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


$milestones = "select milestone_id, milestone_text from milestones order by milestone_code";



$form = new Form("milestones", "milestone");

$form->add_section();
$form->add_edit("milestone_code", "Code", NOTNULL | UNIQUE);
$form->add_edit("milestone_text", "Description", NOTNULL);
$form->add_edit("milestone_text_state", "State's Name", NOTNULL);
$form->add_edit("milestone_email", "Notification to");
$form->add_edit("milestone_ccmail", "CC Mail to");
$form->add_edit("milestone_ccmail2", "CC Mail to");
$form->add_checkbox("milestone_on_check_cer_lease", "Check CER lease details and send mail to client", 0, 0, "On date entry");
$form->add_checkbox("milestone_in_new_project", "Insert date of submission", 0, 0, "On new project");
$form->add_checkbox("milestone_on_cer_submission", "Insert date of submission", 0, 0, "On CER/AF submission");
$form->add_checkbox("milestone_use_in_CER", "Show milestone in CER/AF project overview", 0, 0, "Projects");
$form->add_checkbox("milestone_active", "Is Active");
$form->add_section("List of Project's State");
$form->add_checkbox("milestone_in_statelist", "Show milestone in project's state list", 0, 0, "List of Project's State");
$form->add_checkbox("milestone_in_statelist_showdate", "Show date insstaed of x", 0, 0, "");


$form->add_section("MIS: List of Milestones");
$form->add_list("milestone_start_id", "Sum up duration starting with", $milestones);
$form->add_list("milestone_start_id_replacement_id", "Conditional replacement of duration with", $milestones);
$form->add_list("milestone_replacement_if_empty", "If date is empty replace with", $milestones);
$form->add_edit("milestone_sumup_text", "Caption", 0);


$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();
$form->process();

$page = new Page("milestones");
$page->header();
$page->title(id() ? "Edit Project Milestone" : "Add Project Milestone");
$form->render();
$page->footer();

?>