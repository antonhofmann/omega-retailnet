<?php
/********************************************************************

    snagging_template_element.php

    Add and mutation of snagging_template_elements templates.

    Created by:     Oliver Hofer (oliver.hofer@pageagent.com)
    Date created:   2003-05-14
    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date modified:  2003-05-14
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

//check_access("can_edit_catalog");

//param("snagging_template_element_template", param("template"));
$sql_element_types = "select snagging_element_type_id, snagging_element_type_description " .
                     "from snagging_element_types " .
                     "order by snagging_element_type_priority asc";

$form = new Form("snagging_template_elements", "snagging template element");

$form->add_section();
$form->add_hidden("snagging_template_element_template", param("template"));
$form->add_hidden("snagging_template_element_priority");
$form->add_list("snagging_template_element_type", "Element Type", $sql_element_types);
$form->add_edit("snagging_template_element_name", "Caption", NOTNULL);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();

if (!$form->value("snagging_template_element_priority"))
{
    $snagging_template = id() ? $form->value("snagging_template_element_template") :
                                param("template");
    $sql = "select max(snagging_template_element_priority) " .
           "from snagging_template_elements " .
           "where snagging_template_element_template = " .
                $form->value("snagging_template_element_template");

    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_row($res);
    $form->value("snagging_template_element_priority", $row[0] + 1);
}

$form->process();

if ($form->button(FORM_BUTTON_SAVE))
{
    if ($form->validate())
    {
        redirect("snagging_template.php?id=" . param("snagging_template_element_template"));
    }
}

$page = new Page("snagging_list_templates");
$page->header();
$page->title(id() ? "Edit Snagging Template Element" : "Add Snagging Template Element");
$form->render();
$page->footer();


?>