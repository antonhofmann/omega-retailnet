<?php
/********************************************************************

    product_line_subclasses.php

    Lists product line subclasses for editing.

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2010-12-03
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2010-12-03
    Version:        2.0.0

    Copyright (c) 2010, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_browse_catalog_in_admin");
set_referer("product_line_subclass.php");


// new version depending on pos type
$list = new ListView("select productline_subclass_id, productline_subclass_name, " .
                     "    product_line_name " .
                     "from productline_subclasses left join product_lines on product_line_id = productline_subclass_productline");

$list->set_entity("product_line_subclasses");
$list->set_order("product_line_name, productline_subclass_name");
$list->set_filter("product_line_budget = 1");

$list->add_column("product_line_name", "Product Line");




if(has_access("can_edit_catalog"))
{
	$list->add_column("productline_subclass_name", "Product Line Subclass", "product_line_subclass.php");
	$list->add_button(LIST_BUTTON_NEW, "New", "product_line_subclass.php");
}
else
{
	$list->add_column("productline_subclass_name", "Product Line Subclass");
}

$list->process();

$page = new Page("product_line_subclasses");

$page->header();
$page->title("Product Line Subclasses");
$list->render();
$page->footer();

?>
