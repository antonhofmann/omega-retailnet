<?php
/********************************************************************

    business_unit.php

    Creation and mutation of role records.

    Created by:     Oliver Hofer (oliver.hofer@pageagent.com)
    Date created:   2003-05-07
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-09-28
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


$applications = array();
$applications['all'] = "All Applications";
$applications['projects'] = "Projects";
//$applications['red'] = "RED";


$sql_users = "select user_id, concat(user_name,' ', user_firstname, ' - ', address_company) as user_name " .
             "from users " .
             "left join addresses on users.user_address = addresses.address_id " .
             "order by address_company, user_name asc ";

$form = new Form("business_units", "business_unit");

$form->add_section();
$form->add_list("business_unit_application", "Application", $applications, NOTNULL | STRING);
$form->add_edit("business_unit_name", "Business Unit", NOTNULL | UNIQUE);
$form->add_list("business_unit_responsible", "Responsible", $sql_users);
$form->add_edit("business_unit_valid_to", "Valid Until", 0, "", TYPE_DATE, 20);



$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("business_units");
$page->header();
$page->title(id() ? "Edit Business Unit" : "Add Business Unit");
$form->render();
$page->footer();

?>