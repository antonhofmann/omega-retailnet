<?php
/********************************************************************

    pos_type_standardroles.php

    Lists standardroles for editing.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-03-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("pos_type_standardrole.php");


$sql = "select standardrole_id, standardrole_country, postype_name, project_costtype_text, country_name,  " .
       " concat(users1.user_name, ' ', users1.user_firstname) as rtco, " .
	   " concat(users2.user_name, ' ', users2.user_firstname) as dsup, " .
	   " concat(users3.user_name, ' ', users3.user_firstname) as rtop, " .
	   " concat(users4.user_name, ' ', users4.user_firstname) as cmsa, " .
	   "salesregion_name " . 
       "from standardroles " .
	   "left join postypes on postype_id = standardrole_postype " .
	   "left join project_costtypes on project_costtype_id = standardrole_legaltype " .
	   "left join countries on country_id = standardrole_country " .
	   "left join salesregions on salesregion_id = country_salesregion " .
	   "left join users as users1 on users1.user_id = standardrole_rtco_user " .
	   "left join users as users2 on users2.user_id = standardrole_dsup_user " .
	   "left join users as users3 on users3.user_id = standardrole_rtop_user " . 
	   "left join users as users4 on users4.user_id = standardrole_cms_approver_user ";



$list = new ListView($sql);

$list->set_entity("standardroles");
$list->set_order("salesregion_name, country_name, postype_name");

$list->add_column("salesregion_name", "Geographical Region");
$list->add_column("country_name", "Country", "pos_type_standardrole.php");

$list->add_column("postype_name", "POS Type");
$list->add_column("project_costtype_text", "Legal Type");
$list->add_column("rtco", "Project Manager");
$list->add_column("dsup", "Design Supervisor");
$list->add_column("rtop", "Retail Operator");
$list->add_column("cmsa", "CMS Approval by");

$list->add_button(LIST_BUTTON_NEW, "New", "pos_type_standardrole.php");

$list->add_button("replace_user", "Replace User");
$list->add_button("export_excel", "Export to Excel");


$list->process();

if($list->button("replace_user")) {
	redirect("pos_type_standardroles_replace_user.php");
}
elseif($list->button("export_excel"))
{
	$link = "pos_type_standardroles_xls.php";
	redirect($link);
}

$page = new Page("standardroles");

$page->header();
$page->title("POS Type Standard Roles");

$list->render();
$page->footer();

?>
