<?php
/********************************************************************

    item_group.php

    Edit item_group

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-08-05
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-08-05
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

set_referer("item_group_option.php");

set_referer("item_group_items.php");

$form = new Form("item_groups", "item_group");

$form->add_hidden("group", id());

$form->add_list("item_group_category", "Category",
    "select category_id, concat(product_line_name, ': ', category_name) as categoryname " .
    "from categories  " .
    "left join product_lines on product_line_id = category_product_line " .
    "where category_catalog = 1 " .
    "order by product_line_name, category_name", NOTNULL);

$form->add_edit("item_group_name", "Group Name", NOTNULL);
$form->add_multiline("item_group_description", "Description", 4);


// group composition
$form->add_subtable("item_group_items", "<br />Composition of Items", "", "Code",
    "select item_group_item_id, item_group_item_quantity as Quantity, item_code as Code, item_name as Name from item_group_items left join items as items on item_group_item_item = item_id " .
    "where item_group_item_group = " . id() . " " .
    "order by item_code", "", "Remove");

// options
$form->add_subtable("item_group_options", "<br />Options", "item_group_option.php", "Name",
    "select item_group_option_id, item_group_option_name as Name " .
    "from item_group_options " .
    "where item_group_option_group = " . id() . " " .
    "order by item_group_option_name");


$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button("composition", "Composite Items", "item_group_items.php?id=" . id(), OPTIONAL);
$form->add_button("replecements", "Add Option", "item_group_option.php?group=" . id(), OPTIONAL);

$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);


$form->populate();
$form->process();

$page = new Page("item_groups");
$page->header();
$page->title(id() ? "Edit Item Group" : "Add Item Group");
$form->render();
$page->footer();

?>