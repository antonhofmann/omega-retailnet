<?php
/********************************************************************

    item_groups.php

    Lists item_groups for editing

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-08-05
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-08-05
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("item_group.php");


/********************************************************************
    prepare all data needed
*********************************************************************/

/********************************************************************
    item group list
*********************************************************************/

$list = new ListView("select item_group_id, item_group_name " .
                     "from item_groups ");

$list->set_entity("item_groups");
$list->set_order("item_group_name");

$list->add_column("item_group_name", "Name", "item_group.php", LIST_FILTER_FREE);

$list->add_button(LIST_BUTTON_NEW, "New", "item_group.php");

$list->process();

$page = new Page("item_groups");

$page->header();
$page->title("Item Groups");
$list->render();
$page->footer();

?>
