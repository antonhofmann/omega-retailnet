<?php
/********************************************************************

    product_line_pos_type.php

    Creation and edit product lines pos type.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-04-04
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-04-04
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("product_line_pos_types", "POS Type");


$form->add_section();

$form->add_list("product_line_pos_type_pos_type", "POS Project Type",
				"select postype_id, postype_name from postypes order by postype_name ", NOTNULL);

/*
$form->add_list("product_line_pos_type_product_line", "Product Line",
				"select product_line_id, product_line_name from product_lines where product_line_budget = 1 order by product_line_name ", 0);
*/

$form->add_edit("product_line_pos_type_starting_number", "Starting Number", NOTNULL, "", TYPE_INT, 4);


$form->add_section("Availability for Clients");
$form->add_checkbox("product_line_pos_type_available_to_cients", "", 0, 0, "Available for Clients");

$form->add_section("Available POS Type Subclasses");
$form->add_checklist("possubclasses", "Pos Type Subclasses", "postype_subclasses",
    "select possubclass_id, possubclass_name from possubclasses order by possubclass_name");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
//$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("product_line_pos_types");

$page->header();
$page->title(id() ? "Edit POS Project Type" : "Add POS Project Type");
$form->render();
$page->footer();

?>
