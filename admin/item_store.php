<?php
/********************************************************************

    item_store.php

    Mutation of supplier records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-10-06
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-10-06
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("stores", "store");

$form->add_section();
$form->add_lookup("store_item", "Item", "items", "item_code", 0, param("item"));

$form->add_section();
$form->add_list("store_address", "Supplier",
    "select address_id, address_company " .
    "from addresses left join address_types on address_type = address_type_id " .
    "where address_type_code = 'SUPP' " .
    "order by address_company");

$form->add_section();
$form->add_edit("store_global_order", "Global Order", NOTNULL);
$form->add_edit("store_last_global_order", "Last Global Order", NOTNULL);
$form->add_edit("store_last_global_order_date", "Last Global Order Date", NOTNULL);
$form->add_edit("store_last_global_order_confirmation_date", "Last Global Order Confirmation", NOTNULL);

$form->add_section();
$form->add_edit("store_booking", "Booking", NOTNULL);
$form->add_edit("store_external_stock", "External Stock", NOTNULL);
$form->add_edit("store_inventory", "Inventory", NOTNULL);
$form->add_edit("store_physical_stock", "Physical Stock", NOTNULL);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

// Populate form and process button clicks

$form->populate();
$form->process();

// Render page

$page = new Page("items");

$page->header();
$page->title(id() ? "Edit Store" : "Add Store");
$form->render();
$page->footer();

?>