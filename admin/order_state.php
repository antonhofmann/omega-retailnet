<?php
/********************************************************************

    order_state.php

    Mutation of order state records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-10-18
    Modified by:    Anton.Hofmann (aho@mediaparx.ch)
    Date modified:  2004-02-10
    Version:        1.0.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("order_states", "order status");

$sql = "select order_state_group_order_type " .
       "from order_states left join order_state_groups on order_state_group = order_state_group_id " .
       "where order_state_id = " . id();

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_row($res);

$order_type = $row[0];

$form->add_section();
$form->add_label("order_state_code", "Code");

$form->add_section();
$form->add_edit("order_state_name", "Order Status", NOTNULL);
$form->add_edit("order_state_action_name", "Action Name");
$form->add_list("order_state_predecessor", "Predecessor when planning is required", 
    "select order_state_id, concat(order_state_code, ' ', order_state_name) " .
    "from order_states left join order_state_groups on order_state_group = order_state_group_id " .
    "where order_state_group_order_type = $order_type and order_state_id <> " . id() . " " .
    "order by order_state_code");

$form->add_list("order_state_predecessor_no_planning", "Predecessor when no planning is requred", 
    "select order_state_id, concat(order_state_code, ' ', order_state_name) " .
    "from order_states left join order_state_groups on order_state_group = order_state_group_id " .
    "where order_state_group_order_type = $order_type and order_state_id <> " . id() . " " .
    "order by order_state_code");


$form->add_section();
$form->add_comment("If no notification recipient group is selected notifications can be sent to every person involved in a project.");
$form->add_list("order_state_notification_recipient", "Notification Recipient", 
    "select notification_recipient_id, notification_recipient_name " .
    "from notification_recipients " .
    "order by notification_recipient_name");

//$form->add_list("order_state_notification_recipient2", "Notification Recipient 2", 
//    "select notification_recipient_id, notification_recipient_name " .
//    "from notification_recipients " .
//    "order by notification_recipient_name");

$form->add_list("order_state_performer", "Action is performed by", 
    "select role_id, role_name " .
    "from roles " .
    "order by role_name");


$form->add_list("order_state_order_state_type", "Submission Type", 
    "select order_state_type_id, order_state_type_name " .
    "from order_state_types " .
    "order by order_state_type_name");

$form->add_section();

$form->add_checkbox("order_state_append_task", "Add task to recipient's task list", 0, 0);
$form->add_checkbox("order_state_send_email", "Send mail when performed", 0, 0);
$form->add_checkbox("order_state_change_state", "Change status when performed", 0, 0);
$form->add_checkbox("order_state_delete_tasks", "Deletes tasks when performed", 0, 0);
$form->add_checkbox("order_state_manually_deleted", "Task must be deleted manually", 0, 0);

$form->add_section("Email Notification");
$form->add_list("order_state_email_copy_to", "Copy of Email to", 
    "select user_id, concat(user_name, ' ', user_firstname) as username " .
    "from users " .
	"where user_active = 1  " .
    "order by user_name");

/*
$form->add_list("order_state_email_copy2_to", "Copy of Email to", 
    "select user_id, concat(user_name, ' ', user_firstname) as username " .
    "from users " .
	"where user_active = 1  " .
    "order by user_name");

*/

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();
$form->process();

$page = new Page("order_states");
$page->header();
$page->title("Edit Order Status");
$form->render();
$page->footer();

?>