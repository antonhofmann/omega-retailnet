<?php
/********************************************************************

    role.php

    Creation and mutation of role records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-01
    Modified by:    Anton.Hofmann (aho@mediaparx.ch)
    Date modified:  2011-05-06
    Version:        2.0.0

    Copyright (c) 2002-2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$view = 'role';
if(array_key_exists('view', $_GET) and $_GET['view']) {
	$view = $_GET['view'];
}
elseif(param('view')) {
	$view = param('view');
}

$role_name = '';
$sql = 'select role_name from roles where role_id = ' . id();
$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res)) {
	$role_name = $row['role_name'];
}


$sql_order_states = 'select order_state_code, concat(order_state_code, " ", order_state_name) as orderstate ' . 
                    'from order_states ' .
					'left join order_state_groups on order_state_group_id = order_state_group ' . 
					'left join order_types on order_type_id = order_state_group_order_type ' . 
					'where order_type_id = 1 ' .
					'order by order_state_code';

$applications = array();
$applications['retailnet'] = "Retail Net " . BRAND;
//$applications['mps'] = "MPS";
//$applications['red'] = "RED";


$form = new Form("roles", "role");
$form->add_hidden("view", $view);
$form->add_hidden("id", id());

if($view == 'role') {
	$form->add_section();
	
	$form->add_list("role_application", "Application", $applications, NOTNULL | STRING);

	$form->add_edit("role_code", "Code*", NOTNULL);
	$form->add_edit("role_name", "Name*", NOTNULL | UNIQUE);

	$form->add_section('Restrictions in Visibility of Projects');
	$form->add_comment('Projects will be visible to the role only in the range of project states indicated.');

	$form->add_list("role_order_state_visible_from", "Visible from project state", $sql_order_states);
	$form->add_list("role_order_state_visible_to", "Visible to project state", $sql_order_states);


	$form->add_section("Visibility");
	$form->add_checkbox("role_used_in_mps", "Role is used in Merchandising Planning", false, 0, "MPS");
	$form->add_checkbox("role_used_in_red", "Role is used in Retail Environment Development", false, 0, "RED");


	$form->add_section("Restrictions in Visibility of Attachments");
	$form->add_checklist("order_file_categories", "Attachment Catgories", "role_file_categories",
    "select order_file_category_id, order_file_category_name from order_file_categories order by order_file_category_name");

	/*
	$sql_permissions = "select permission_id, permission_description from permissions where permission_application = '" . $view . "' order by permission_description";
	$form->add_section('Permissions for ' . $role_name);
	$form->add_checklist("role_permissions", "Permissions", "role_permissions",$sql_permissions);
	*/

}
elseif($view) {
	
	$permission_values = array();
	$sql = "select role_permission_permission from role_permissions " . 
		   "inner join permissions on permission_id = role_permission_permission " . 
	       "where role_permission_role = " . id() . " and permission_application = '" . $view . "' order by permission_description";
	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res)) {
		$permission_values[$row['role_permission_permission']] = $row['role_permission_permission'];
	}

	$sql_permissions = "select permission_id, permission_description from permissions where permission_application = '" . $view . "' order by permission_description";

	$form->add_section('Permissions for ' . $role_name . ' in section ' . $view);
	
	$res = mysql_query($sql_permissions) or dberror($sql_permissions);
	while($row = mysql_fetch_assoc($res)) {
		
		if(array_key_exists($row['permission_id'], $permission_values)) {
			$form->add_checkbox("role_permissions_" . $row['permission_id'], "", 1,0, $row['permission_description']);
		}
		else {
			$form->add_checkbox("role_permissions_" . $row['permission_id'], "", 0, 0, $row['permission_description']);
		}
		
	}
	
	
}

$form->add_button("save", "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");

if(id() and $view == 'role')
{
	$form->add_button("delete", "Delete");
}



$form->populate();
$form->process();

if($form->button("save")) {

	if($form->value('view') == 'role') {
		
		$form->add_validation("strlen({role_code}) == 4 && is_upper_case({role_code})", "The role code is supposed to consist of exactly 4 uppercase characters.");

		if($form->validate())
		{
			$form->save();
		}
	}
	elseif($form->validate()) {
		
		$res = mysql_query($sql_permissions) or dberror($sql_permissions);
		while($row = mysql_fetch_assoc($res)) {
			$sql = "delete from role_permissions " . 
				   "where role_permission_role = " . id() . 
				   " and role_permission_permission = " . $row["permission_id"];
			$result = mysql_query($sql) or dberror($sql);
		}
		
		

		foreach($form->items as $item) {
			if(substr($item['name'], 0, 17) == 'role_permissions_') {
				$permission_id = substr($item['name'], 17, strlen($item['name']));
				if($form->value($item['name']) == 1) {
					$sql = "insert into role_permissions (role_permission_role, role_permission_permission, user_created, date_created) VALUES (" . 
						   id() . ',' . $permission_id . ',"' . user_login() . '","' . date("Y-m-d:h:i:s") . '")';
					$res = mysql_query($sql) or dberror($sql);
				}
			}
		}

	}

	$form->message("The data has been saved!");
}
elseif($form->button("delete")) {

	$sql = "delete from role_permissions where role_permission_role = " . id();
	$res = mysql_query($sql) or dberror($sql);

	$sql = "delete from roles where role_id = " . id();
	$res = mysql_query($sql) or dberror($sql);

	redirect('roles.php');

}

$page = new Page("roles");
$page->header();
$page->title(id() ? "Edit Role and Permissions" : "Add Role");

if(id())
{
	require_once("include/permission_tabs.php");
}

$form->render();
$page->footer();

?>