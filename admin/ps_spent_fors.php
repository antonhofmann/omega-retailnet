<?php
/********************************************************************

    ps_spent_fors.php

    Lists project sheet spent for for editing .

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2004-09-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-09-27
    Version:        1.0.0

    Copyright (c) 2004, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("ps_spent_for.php");

$sql = "select ps_spent_for_id, ps_spent_for_name from ps_spent_fors ";

$list = new ListView($sql);

$list->set_entity("ps_spent_fors");
$list->set_order("ps_spent_for_name");

$list->add_column("ps_spent_for_name", "Spent For Name", "ps_spent_for.php");

$list->add_button(LIST_BUTTON_NEW, "New", "ps_spent_for.php");

$list->process();

$page = new Page("ps_spent_fors");

$page->header();
$page->title("Project Sheet Spent For Names");
$list->render();
$page->footer();

?>
