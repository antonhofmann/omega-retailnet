<?php
/********************************************************************

    pcost_position.php

    Creation and mutation of project cost positions.

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2004-09-27
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2014, OMEGA SA, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


if(!param("gid") and !param("tid"))
{
	$link = "pcost_template.php";
	redirect($link);
}


//prepare data
$costgroup_name = "";
$costgroup_id = 0;
$sql = "select pcost_group_id, concat(pcost_group_code, ' ', pcost_group_name) as gname " . 
       "from pcost_positions " .
	   "left join pcost_groups on pcost_group_id = pcost_position_pcostgroup_id " .
	   "where pcost_position_id = " . id();
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$costgroup_id = $row["pcost_group_id"];
	$costgroup_name = $row["gname"];
}

if($costgroup_id > 0)
{
	$sql_subgroups = "select pcost_subgroup_id, concat(pcost_subgroup_code, ' ', pcost_subgroup_name) as name " . 
					 "from pcost_subgroups " . 
					 "where pcost_subgroup_pcostgroup_id =  " . $costgroup_id .  
					 " order by pcost_subgroup_code ";
}
else
{
	$sql_subgroups = "select pcost_subgroup_id, concat(pcost_subgroup_code, ' ', pcost_subgroup_name) as name " . 
					 "from pcost_subgroups " . 
					 "where pcost_subgroup_pcostgroup_id =  " . param("gid") .  
					 " order by pcost_subgroup_code ";
}
//prepare form
$form = new Form("pcost_positions", "pcost_position");

$form->add_section();
$form->add_hidden("gid", $costgroup_id);
$form->add_hidden("tid", param("tid"));
$form->add_section($costgroup_name);

$form->add_hidden("pcost_position_pcost_template_id", param("tid"));
$form->add_hidden("pcost_position_pcostgroup_id", param("gid"));
$form->add_list("pcost_position_pcost_subgroup_id", "Cost Subgroup",$sql_subgroups);
$form->add_edit("pcost_position_code", "Code*", NOTNULL);
$form->add_multiline("pcost_position_text", "Text*", 12, NOTNULL);


$form->add_button("save", "Save");
$form->add_button("back", "Back");
$form->add_button("delete", "Delete", "", OPTIONAL);

$form->populate();
$form->process();

if($form->button("back"))
{
	$link = "pcost_template.php?id=" .param("tid");
	redirect($link);
}
elseif($form->button("save"))
{
	if($form->validate())
	{
		$form->save();
		$link = "pcost_template.php?id=" .param("tid");
		redirect($link);
	}
}
elseif($form->button("delete"))
{
	$sql = "delete from pcost_positions where pcost_position_id = " . id();
	$result = mysql_query($sql) or dberror($sql);
	$link = "pcost_template.php?id=" .param("tid");
	redirect($link);
}

$page = new Page("pcost_groups");
$page->header();
$page->title(id() ? "Edit Project Cost Position" : "Add Project Cost Position");
$form->render();
$page->footer();

?>