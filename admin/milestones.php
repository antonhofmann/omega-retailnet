<?php
/********************************************************************

    milestones.php

    Lists milestones for editing .

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-23
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-23
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("milestone.php");

$sql = "select * from milestones ";

$list = new ListView($sql);

$list->set_entity("milestone");
$list->set_order("milestone_code");

$list->add_column("milestone_code", "Code", "milestone.php");
$list->add_column("milestone_text", "Description");
$list->add_column("milestone_text_state", "State's name");
$list->add_column("milestone_email", "Notification to");
$list->add_column("milestone_ccmail", "Notification CC");
$list->add_column("milestone_ccmail2", "Notification CC");

$list->add_button(LIST_BUTTON_NEW, "New", "milestone.php");

$list->process();

$page = new Page("system");

$page->header();
$page->title("Project Milestones");
$list->render();
$page->footer();

?>
