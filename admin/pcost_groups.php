<?php
/********************************************************************

    pcost_groups.php

    Lists project cost groups for editing .

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2004-09-27
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2014, OMEGA SA, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("pcost_group.php");

$sql = "select pcost_group_id, pcost_group_code, pcost_group_name " .
       "from pcost_groups ";

$list = new ListView($sql);

$list->set_entity("pcost_groups");
$list->set_order("pcost_group_code");

$list->add_column("pcost_group_code", "Code", "pcost_group.php");
$list->add_column("pcost_group_name", "Name");

$list->add_button(LIST_BUTTON_NEW, "New", "pcost_group.php");

$list->process();

$page = new Page("pcost_groups");

$page->header();
$page->title("Project Cost Groups");
$list->render();
$page->footer();

?>
