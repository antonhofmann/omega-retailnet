<?php
/********************************************************************

    security.php

    Lists locked IPS for unlocking .

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-09-05
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-09-05
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("security_unlock_ip.php");

$sql = "select sec_lockedip_id, sec_lockedip_ip, sec_lockedip_date from sec_lockedips";



$list = new ListView($sql);

$list->set_entity("sec_lockedips");
$list->set_order("sec_lockedip_ip, sec_lockedip_date");

$list->add_column("sec_lockedip_ip", "IP Address", "security_unlock_ip.php");
$list->add_column("sec_lockedip_date", "Date:Time");

$iplocked = get_session_value("iplocked");

$list->process();

$page = new Page("security");

$page->header();
$page->title("Login Failures: Locked IP Addresses");

$list->render();
$page->footer();

?>
