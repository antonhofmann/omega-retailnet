<?php
/********************************************************************

    link_topics.php

    Lists link topics for editing.

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2013-03-23
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2013-03-23
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("link_topic.php");

$list = new ListView("select link_topic_id, link_topic_name " .
                     "from link_topics");

$list->set_entity("link_topics");
$list->set_order("link_topic_name");

$list->add_column("link_topic_name", "Name", "link_topic.php", LIST_FILTER_FREE);


$list->add_button(LIST_BUTTON_NEW, "New", "link_topic.php");

$list->process();

$page = new Page("link_topics");

$page->header();
$page->title("Download/Link Topics");
$list->render();
$page->footer();

?>
