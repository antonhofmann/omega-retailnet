<?php
/********************************************************************

    item_group_repgroups.php

    Edit Replacement Groupsof a Group

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-08-05
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-08-05
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$list = new ListView("select item_group_id, item_group_name " .
                     "from item_groups ", LIST_HAS_HEADER | LIST_SHOW_COUNT);

$list->set_entity("item_groups");
$list->set_order("item_group_name");
$list->set_filter("item_group_id <> " . param("id"));

$list->set_checkbox("item_group_repgroups", "item_groups", param("id"));

$list->add_hidden("id", param("id"));
$list->add_column("item_group_name", "Name", "", LIST_FILTER_FREE);

$list->add_button(LIST_BUTTON_SAVE, "Save");
$list->add_button(LIST_BUTTON_BACK, "Back");

$list->process();

$page = new Page("items");

$sql = "select item_group_name from item_groups where item_group_id = " . param("id");
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_row($res);

$page->header();
$page->title("Assign Replacement Groups To Group \"" . htmlspecialchars($row[0]) . "\"");
$list->render();
$page->footer();
?>
