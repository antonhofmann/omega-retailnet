<?php
/********************************************************************

    provinces.php

    Lists provinces for editing.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-07-12
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-07-12
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("province.php");

$list_filter = "";
if(param("province_country") > 1)
{
	$list_filter .=	" province_country = " . param("province_country") . " ";
}
else
{
	$list_filter .=	" province_country = 1 ";
}

$country = 1;
if( param("province_country") > 0)
{
	$country =  param("province_country");
}

/********************************************************************
    Create Form
*********************************************************************/ 
$form = new Form("provinces", "province");

$form->add_section("List Filter Selection");

$form->add_list("province_country", "Country",
    "select country_id, country_name from countries order by country_name", SUBMIT | NOTNULL, $country );



/********************************************************************
    Create List
*********************************************************************/ 

$list = new ListView("select province_id, province_canton, province_region, province_adminregion, province_shortcut from provinces ");

$list->set_entity("provinces");
$list->set_order("province_canton");
$list->set_filter($list_filter);

$list->add_column("province_canton", "Province", "province.php");
$list->add_column("province_region", "Region", "");
$list->add_column("province_adminregion", "Admin Region");
$list->add_column("province_shortcut", "Shortcut");

$list->add_button(LIST_BUTTON_NEW, "New", "province.php?country=" . param("province_country"));


/********************************************************************
    Populate and process button clicks
*********************************************************************/ 
$form->populate();
$list->process();


/********************************************************************
   Create Page
*********************************************************************/ 
$page = new Page("provinces");

$page->header();
$page->title("Provinces");
$form->render();
$list->render();
$page->footer();

?>
