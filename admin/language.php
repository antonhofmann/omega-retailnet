<?php
/********************************************************************

    language.php

    Creation and mutation of language records.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.


*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");


$form = new Form("languages", "language");

$form->add_edit("language_iso639_1", "ISO Code 639-1", NOTNULL | UNIQUE);
$form->add_edit("language_name", "Name", NOTNULL | UNIQUE);
$form->add_edit("language_local_name", "Local Name");
$form->add_checkbox("language_used_in_mps", "Language used in merchandising planning");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("locales");
$page->header();
$page->title(id() ? "Edit Language" : "Add Language");
$form->render();
$page->footer();

?>