<?php
/********************************************************************

	roles_users_xls.php

    Generate Excel File of Roles and users

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2012-01-25
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2012-01-25
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

require_once "../include/xls/Writer.php";

/********************************************************************
    prepare Data Needed
*********************************************************************/
$header = "Users and Roles: " . " (" . date("d.m.Y") . ")";

/********************************************************************
    prepare Excel Sheet
*********************************************************************/

$filename = "roles_and_users.xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename); 
$xls->setVersion(8);
$sheet =& $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();


//formats
$header_row =& $xls->addFormat();
$header_row->setSize(10);
$header_row->setAlign('left');
$header_row->setBold();

$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);


$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);


$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextWrap();

$f_caption1 =& $xls->addFormat();
$f_caption1->setSize(8);
$f_caption1->setAlign('left');
$f_caption1->setBorder(1);
$f_caption1->setBold();
$f_caption1->setTextRotation(270);
$f_caption1->setTextWrap();


/********************************************************************
    write header
*********************************************************************/
$sheet->write(0, 0, $header, $header_row);

/********************************************************************
    write all roles
*********************************************************************/
$rowindex = 2;
$cellindex = 4;
$sheet->setRow($rowindex, 120);

$sql = "select * from roles order by role_name";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$role_name = $row['role_name'];

	if($row['role_order_state_visible_from'] != '' and $row['role_order_state_visible_to'] != '') 
	{
		$role_name .= "\n" . 'from step ' . $row['role_order_state_visible_from'] . ' to step ' . $row['role_order_state_visible_to'];
	}
	elseif($row['role_order_state_visible_from'] != '')
	{
		$role_name .= "\n" . 'from step ' . $row['role_order_state_visible_from'];
	}
	elseif($row['role_order_state_visible_to'] != '') 
	{
		$role_name .= "\n" . 'to step ' . $row['role_order_state_visible_to'];
	}

	
	$sheet->write($rowindex,$cellindex, $role_name, $f_caption1);
	$sheet->setColumn($cellindex, $cellindex, 4);

	$cellindex++;
}




/********************************************************************
    write all permissions
*********************************************************************/
$rowindex = 3;


$sheet->setColumn($cellindex, $cellindex, 100);

$sql = "select country_name, address_type_name, address_company, user_id, user_name, user_firstname " . 
       "from users " .
	   "left join addresses on address_id = user_address " .
	   "left join countries on country_id = address_country " .
	   "left join address_types on address_type_id = address_type " .
	   "where user_active = 1 " .
	   "order by country_name, address_type_name, address_company, user_name, user_firstname" ;

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$cellindex = 0;

	$sheet->write($rowindex,$cellindex, $row["country_name"],$f_normal);
	$cellindex++;

	$sheet->write($rowindex,$cellindex, $row["address_type_name"],$f_normal);
	$cellindex++;

	$sheet->write($rowindex,$cellindex, $row["address_company"],$f_normal);
	$cellindex++;

	$sheet->write($rowindex,$cellindex, $row["user_name"] . " " . $row["user_firstname"],$f_normal);
	$cellindex++;

	

	//print permissions

	$sql = "select * from roles order by role_name";

	$res1 = mysql_query($sql) or dberror($sql);
	while ($row1 = mysql_fetch_assoc($res1))
	{
		

		$sql = "select count(user_role_id) as num_recs from user_roles " . 
			   "where user_role_role = " . $row1['role_id'] .
			   " and user_role_user = " . $row['user_id'];

		$res2 = mysql_query($sql) or dberror($sql);
		$row2 = mysql_fetch_assoc($res2);
		
		if($row2['num_recs'] == 1) 
		{
			$sheet->write($rowindex,$cellindex, "x", $f_center);
		}
		else
		{
			$sheet->write($rowindex,$cellindex, "", $f_center);
		}

		$cellindex++;
	}



	$cellindex = 0;
	$rowindex++;
}


$sheet->setColumn(0, 0,20);
$sheet->setColumn(1, 1,10);
$sheet->setColumn(2, 2,35);
$sheet->setColumn(3, 3,30);

$xls->close(); 

?>