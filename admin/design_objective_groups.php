<?php
/********************************************************************

    design_objective_groups.php

    Lists design objective groups for editing.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-05
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2010-12-03
    Version:        2.0.0

    Copyright (c) 2002-2010, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("design_objective_group.php");


/* old version depending on product line
$list = new ListView("select design_objective_group_id, design_objective_group_name, " .
                     "    if (design_objective_group_multiple, 'yes', 'no') as design_objective_group_multiple, " .
                     "    product_line_name " .
                     "from design_objective_groups left join product_lines on design_objective_group_product_line = product_line_id");

$list->set_entity("design objective groups");
$list->set_order("product_line_name, design_objective_group_priority");

$list->add_column("product_line_name", "Product Line", "", LIST_FILTER_LIST,
    "select product_line_name from product_lines order by product_line_name");
$list->add_column("design_objective_group_name", "Name", "design_objective_group.php", LIST_FILTER_FREE);
$list->add_column("design_objective_group_multiple", "Multiple Choice", "", LIST_FILTER_LIST, array(0 => "No", 1 => "Yes"));

$list->add_button(LIST_BUTTON_NEW, "New", "design_objective_group.php");
$list->add_button(LIST_BUTTON_FILTER, "Filter");
$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove filter");
*/


// new version depending on pos type
$list = new ListView("select design_objective_group_id, design_objective_group_name, " .
                     " IF(design_objective_group_active = 1, 'x', '') as active, " . 
                     "    if (design_objective_group_multiple, 'yes', 'no') as design_objective_group_multiple, " .
                     "    postype_name " .
                     "from design_objective_groups left join postypes on design_objective_group_postype = postype_id");

$list->set_entity("design_objective_groups");
$list->set_order("postype_name, design_objective_group_priority");
$list->set_filter("design_objective_group_postype > 0");

$list->add_column("postype_name", "POS Type");
$list->add_column("design_objective_group_name", "Name", "design_objective_group.php");
$list->add_column("design_objective_group_multiple", "Multiple Choice", "", "", "", COLUMN_ALIGN_CENTER);

$list->add_column("active", "Active", "", "", "", COLUMN_ALIGN_CENTER);

$list->add_button(LIST_BUTTON_NEW, "New", "design_objective_group.php");

$list->process();

$page = new Page("design_objective_groups");

$page->header();
$page->title("Design Objective Groups");
$list->render();
$page->footer();

?>
