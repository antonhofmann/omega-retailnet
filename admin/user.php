<?php
/********************************************************************

    user.php

    Mutation of user records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-31
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.com)
    Date modified:  2002-10-10
    Version:        1.0.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/admin_functions.php";

check_access("can_edit_catalog");


//get countries
$sql_country = "select DISTINCT country_id, country_name " .
			   "from countries " .
			   "order by country_name";

$countries = array();
$sql = "select country_access_country, country_name " . 
       "from country_access " . 
	   "left join countries on country_id = country_access_country " . 
	   "where country_access_user = " . id();
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res)) 
{
	$countries[$row["country_access_country"]] = $row["country_name"];
}

//get substitute user
$subsitute_user = 0;
$sql = "select user_substitute from users where user_id = " . id();

$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res)) 
{
	$subsitute_user = $row["user_substitute"];
}

$icon = "../pictures/add_item.gif";
$link = "javascript:open_selector('')";


$user_active = "";
$tracking_data = array();
if(id())
{
	$sql = "select user_added_by, user_set_to_inactive_by, user_set_to_inactive_date, date_created, user_active " . 
		   "from users " . 
		   "where user_id = " . id();

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res)) 
	{
		
		$user_active = $row["user_active"];
		$tracking_data["user_added_by"] = $row["user_added_by"];
		$tracking_data["user_set_to_inactive_by"] = $row["user_set_to_inactive_by"];

		$date_created["user_added_by"] = $row["date_created"];
		$date_created["user_set_to_inactive_by"] = $row["user_set_to_inactive_date"];

		foreach($tracking_data as $key=>$user_id)
		{
			$sql = "select user_name, user_firstname " . 
				   "from users " . 
					"where user_id = " . dbquote($user_id);

			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res)) 
			{
				$tracking_data[$key] = $row["user_name"] . " " . $row["user_firstname"] . " " . $date_created[$key];
			}
		}
	}

}


//get user_roles
$retailnet_roles = get_user_roles(id(), 'retailnet');
//$mps_roles = get_user_roles(id(), 'mps');
//$red_roles = get_user_roles(id(), 'red');

// Build form

$form = new Form("users", "user");
$form->add_hidden("address", param("address"));

$form->add_section("Personal data");
$form->add_lookup("user_address", "Address", "addresses", "address_company", 0, param("address"));

$form->add_section();
$form->add_edit("user_firstname", "Firstname*", NOTNULL);
$form->add_edit("user_name", "Name*", NOTNULL);

$form->add_multiline("user_description", "Description/Position", 2);

$form->add_section("Communication");
$form->add_edit("user_phone", "Phone*", NOTNULL);
$form->add_edit("user_fax", "Fax");
$form->add_edit("user_email", "Email*", NOTNULL);
$form->add_edit("user_email_cc", "CC");
$form->add_edit("user_email_deputy", "Deputy");

$form->add_section("Security");
$form->add_edit("user_login", "Username", UNIQUE);
$form->add_edit("user_password", "Password");

/*
$form->add_section("Special Access Rules for Projects and Catalogue Orders");
$form->add_checkbox("user_can_only_see_his_projects", "only to his own projects", true, 0, "Access");
$form->add_checkbox("user_can_only_see_projects_of_his_address", "only projects of his company", true, 0, "Access");
$form->add_checkbox("user_can_only_see_his_orders", "only to his own catalogue orders", true, 0, "Access");
$form->add_checkbox("user_can_only_see_orders_of_his_address", "only catalogue orders of his company", true, 0, "Access");
*/

$form->add_section("Special Access Rules for Projects");
$form->add_checkbox("user_can_only_see_his_projects", "only to his own projects", true, 0, "Access");
$form->add_checkbox("user_can_only_see_projects_of_his_address", "only projects of his company", true, 0, "Access");
$form->add_hidden("user_can_only_see_his_orders", 1);
$form->add_hidden("user_can_only_see_orders_of_his_address", 1);


$form->add_section("Other information");
$form->add_radiolist( "user_sex", "Sex",
	array("m" => "Male", "f" => "Female"), 0, "m");

$form->add_list("user_substitute", "Substitute",
	"select user_id, concat(user_name, ' ', user_firstname) " .
	"from users " .
	"where user_id = '" . $subsitute_user . "' or (user_active = 1 and user_address = {user_address} and user_id <> " . id() . ") " .
	"order by user_name, user_firstname");

$form->add_section();
$form->add_checkbox("user_active", "Active", true);
$form->add_checkbox("user_project_requester", "Can request projects", false);
$form->add_checkbox("user_project_reciepient", "Appears as selectable email recipient for projects of his own country", false);
//$form->add_checkbox("user_order_reciepient", "Appears as selectable email recipient for catalogue orders of his own country", false);
$form->add_hidden("user_order_reciepient", 0);
$form->add_checkbox("user_cer_reciepient", "Appears as selectable email recipient in the CER/AF mailbox for projects of his own country", false);
$form->add_checkbox("user_cer_mailbox_available_for_copy", "Can be selected as recipient for CC Mails in CER/AF, projects and orders", false);

if(id() > 0) 
{

	$form->add_section("Roles");
	$form->add_label_selector("retailnet", "Basic Roles", 0, implode(', ', $retailnet_roles), $icon, $link);
	//$form->add_label_selector("mps", "MPS", 0, implode(', ', $mps_roles), $icon, $link);
	//$form->add_label_selector("red", "RED", 0, implode(', ', $red_roles), $icon, $link);

	$form->add_section("Country Access");
	$form->add_comment("Only for Regional Sales Managers (HQ). Indicate the countries a  Regional Sales Manager has access to.");

	$i= 0;
	$selected_countries = "";
	foreach($countries as $id=>$country_name)
	{
		if($i == 7)
		{
			$selected_countries .= "<br />";
			$i= 0;
		}
		$selected_countries .= $country_name . ", ";
		$i++;
	}
	$selected_countries = substr($selected_countries, 0, strlen($selected_countries) - 2);

	$form->add_label_selector("countries", "Countries", RENDER_HTML, $selected_countries, $icon, $link);

	$form->add_hidden("new_user", 0);
	
}
else
{
	$form->add_hidden("new_user", 1);
}


if(!array_key_exists("user_added_by", $tracking_data))
{
	$form->add_hidden("user_added_by", user_id());
}


if(array_key_exists("user_added_by", $tracking_data) and $tracking_data["user_added_by"] != "")
{
	$form->add_section("Tracking infos");
	$form->add_label("added_by", "User created by", 0, $tracking_data["user_added_by"]);


	if($user_active == 0 and array_key_exists("user_set_to_inactive_by", $tracking_data) and $tracking_data["user_set_to_inactive_by"] != "")
	{
		$form->add_label("set_to_inactive", "User set to inactive by", 0, $tracking_data["user_set_to_inactive_by"]);
	}
}





$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button("save", "Save");

if(id() > 0) 
{
	
	$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);
}


//$form->add_validation("is_unique_value_multiple('users', array('user_firstname', 'user_name'), array({user_firstname}, {user_name}), " . id() . ")",
//    "A user with this combination of firstname and name already exists.");
$form->add_validation("is_email_address({user_email})", "The email address is invalid.");

// Populate form and process button clicks

$form->populate();
$form->process();


if($form->button("save")) {
	
	if($form->validate())
	{
		$form->save();
	
		//delete user from standarroles
		//echo $form->value('user_active');

		if($form->value('user_active') != 1) {
			$sql = 'update standardroles set standardrole_rtco_user = 0 where standardrole_rtco_user = ' . id();
			mysql_query($sql) or dberror($sql);

			$sql = 'update standardroles set standardrole_dsup_user = 0 where standardrole_dsup_user = ' . id();
			mysql_query($sql) or dberror($sql);

			$sql = 'update standardroles set standardrole_rtop_user = 0 where standardrole_rtop_user = ' . id();
			mysql_query($sql) or dberror($sql);

			$sql = 'update standardroles set standardrole_cms_approver_user = 0 where standardrole_cms_approver_user = ' . id();
			mysql_query($sql) or dberror($sql);
		}

		if($form->value('user_active') != 1 and $user_active == 1)
		{
			$sql = 'update users set user_set_to_inactive_by  = ' . user_id() . ', user_set_to_inactive_date = ' . dbquote(date("Y-m-d H:i:s")) . ' where user_id = ' . id();
			mysql_query($sql) or dberror($sql);
		}

		if($form->value("new_user") == 1)
		{
			redirect("user.php?id=" . id());
		}
		
		/*
		if($form->value("new_user") == 1)
		{
			redirect("user.php?id=" . mysql_insert_id() . "&address=" . $form->value("user_address"));
		}
		*/
	}
}

// Render page

$page = new Page("users");

$page->header();
$page->title(id() ? "Edit User" : "Add User");
$form->render();

?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#countries_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/admin/country_selector.php?context=user&user_id=<?php echo id();?>&address=<?php echo param("address");?>'
    });
    return false;
  });

  $('#retailnet_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/admin/role_selector.php?id=<?php echo id();?>&application=retailnet'
    });
    return false;
  });

  
  $('#mps_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/admin/role_selector.php?id=<?php echo id();?>&application=mps'
    });
    return false;
  });

  
  $('#red_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/admin/role_selector.php?id=<?php echo id();?>&application=red'
    });
    return false;
  });


});
</script>


<?php
$page->footer();

?>