<?php
/********************************************************************

    item_file.php

    Creation and mutation of item file records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-13
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-09-28
    Version:        1.0.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("item_files", "file");

if (id())
{
    $sql = "select item_file_item from item_files where item_file_id = " . id();
    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_row($res);
    param("item", $row[0]);
}

$form->add_hidden("item", param("item"));
$form->add_hidden("sa", param("sa"));

$form->add_section();
$form->add_lookup("item_file_item", "Item", "items", "item_code", 0, param("item"));

$form->add_section();
$form->add_edit("item_file_title", "Title", NOTNULL);
$form->add_multiline("item_file_description", "Description", 4);

$sql = "select item_code from items where item_id = " . param("item");
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_row($res);
$item_code = make_valid_filename($row[0]);

$form->add_section();
$form->add_upload("item_file_path", "File", "/files/items/$item_code", NOTNULL);

$form->add_section();
$form->add_list("item_file_type", "Type",
    "select file_type_id, file_type_name from file_types order by file_type_name");
$form->add_list("item_file_purpose", "Purpose",
    "select file_purpose_id, file_purpose_name from file_purposes order by file_purpose_name");

$form->add_edit("item_file_picture_config_code", "Config Group Code");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button("back", "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

if ($form->button("back"))
{
	redirect("item.php?sa=" . param("sa") . "&id=" . param("item"));
}


$page = new Page("items");

$page->header();
$page->title(id() ? "Edit Item File" : "Add Item File");
$form->render();
$page->footer();

?>
