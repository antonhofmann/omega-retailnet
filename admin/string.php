<?php
/********************************************************************

    string.php

    Mutation of string records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-03
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-09-17
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("strings", "string");

$form->add_section();
$form->add_label("string_name", "Name");
$form->add_label("string_usage", "Usage");

$form->add_section();
$form->add_multiline("string_text", "Text", 10);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();
$form->process();

$page = new Page("strings");
$page->header();
$page->title("Edit String");
$form->render();
$page->footer();

?>