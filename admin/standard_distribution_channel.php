<?php
/********************************************************************

    standard_distribution_channel.php

    Creation and mutation of standard distribution channels.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2012-06-26
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2012-06-26
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


$sql_d = "select mps_distchannel_id, " . 
         "concat(mps_distchannel_group, ' - ' , mps_distchannel_name, ' - ', mps_distchannel_code) as dchannel " .
		 "from  mps_distchannels " .
         "order by mps_distchannel_group, mps_distchannel_name, mps_distchannel_code";

$sql_p = "select postype_id, postype_name ". 
         "from postypes ". 
		 "order by postype_name";

$sql_l = "select posowner_type_id, posowner_type_name ". 
         "from posowner_types ". 
		 "order by posowner_type_name";

$sql_s = "select possubclass_id, possubclass_name ". 
         "from possubclasses ". 
		 "order by possubclass_name";

$form = new Form("posdistributionchannels", "posdistributionchannels");

$form->add_list("posdistributionchannel_legaltype_id", "Legal Type",$sql_l, NOTNULL);
$form->add_list("posdistributionchannel_postype_id", "POS Type",$sql_p, NOTNULL);
$form->add_list("posdistributionchannel_possubclass_id", "POS Subclass",$sql_s, 0);
$form->add_list("posdistributionchannel_dchannel_id", "Distribution Channel",$sql_d, NOTNULL);


$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

if($form->button(FORM_BUTTON_SAVE)) {
	
	if($form->validate())
	{
		$form->save();
		redirect('standard_distribution_channels.php');
	}
}

$page = new Page("posdistributionchannels");
$page->header();
$page->title(id() ? "Edit Standard Distribution Channel" : "Add Standard Distribution Channel");
$form->render();
$page->footer();

?>