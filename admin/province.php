<?php
/********************************************************************

    province.php

    Creation and mutation of province records.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-07-12
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-07-12
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$country = param("country");

// Build form

$form = new Form("provinces", "province");

$form->add_section("Name and regions");
$form->add_list("province_country", "Country*",
    "select country_id, country_name from countries order by country_name", NOTNULL, $country);
$form->add_edit("province_canton", "Province*", NOTNULL);
$form->add_edit("province_region", "Region");
$form->add_edit("province_adminregion", "Administrative Region");
$form->add_edit("province_shortcut", "Shortcut");

$form->add_button("save", "Save");
$form->add_button(FORM_BUTTON_DELETE, "Delete");
$form->add_button("back", "Back");

// Populate form and process button clicks

$form->populate();
$form->process();

if($form->button("back"))
{
	$link = "provinces.php?province_country=" . param("province_country");
	redirect($link);
}
elseif($form->button("save"))
{
	if($form->validate())
	{
		$form->save();

		//update store locator
		$db2 = mysql_connect(STORE_LOCATOR_SERVER, STORE_LOCATOR_USER, STORE_LOCATOR_PASSWORD);
		$dbname2 = STORE_LOCATOR_DB;
		mysql_query( "SET NAMES 'utf8'");
		mysql_select_db($dbname2, $db2);

		$sql = "Update provinces set " . 
			   "province_country_id = " . $form->value("province_country") . ", " .
			   "province_canton = " . dbquote($form->value("province_canton")) . ", " .  
			   "province_region = " . dbquote($form->value("province_region")) . ", " .  
			   "province_adminregion = " . dbquote($form->value("province_adminregion")) . ", " .  
			   "province_region = " . dbquote($form->value("province_region")) . ", " .  
			   "province_shortcut = " . dbquote($form->value("province_shortcut")) . 
			   " where province_id = " . id();

		$result = mysql_query($sql) or dberror($sql);

		$db = mysql_pconnect(RETAILNET_SERVER, RETAILNET_USER, RETAILNET_PASSWORD);
		$dbname = RETAILNET_DB;
		mysql_query( "SET NAMES 'utf8'");
		mysql_select_db($dbname, $db);

		$link = "provinces.php?province_country=" . param("province_country");
		redirect($link);
	}
}

// Render page

$page = new Page("provinces");

$page->header();
$page->title(id() ? "Edit Province" : "New Province");
$form->render();
$page->footer();

?>