<?php
/********************************************************************

	roles_permissions_xls.php

    Generate Excel File of Permissions and Roles

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-04-21
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-04-21
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

require_once "../include/xls/Writer.php";

/********************************************************************
    prepare Data Needed
*********************************************************************/
$header = "Roles and Permissions: " . " (" . date("d.m.Y") . ")";

/********************************************************************
    prepare Excel Sheet
*********************************************************************/

$filename = "roles_and_permissions.xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename); 
$xls->setVersion(8);
$sheet =& $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();


//formats
$header_row =& $xls->addFormat();
$header_row->setSize(10);
$header_row->setAlign('left');
$header_row->setBold();

$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);


$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);


$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextWrap();

$f_caption1 =& $xls->addFormat();
$f_caption1->setSize(8);
$f_caption1->setAlign('left');
$f_caption1->setBorder(1);
$f_caption1->setBold();
$f_caption1->setTextRotation(270);
$f_caption1->setTextWrap();


/********************************************************************
    write header
*********************************************************************/
$sheet->write(0, 0, $header, $header_row);

/********************************************************************
    write all roles
*********************************************************************/
$rowindex = 2;
$cellindex = 1;
$sheet->setRow($rowindex, 120);

$sql = "select * from roles order by role_name";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$role_name = $row['role_name'];

	if($row['role_order_state_visible_from'] != '' and $row['role_order_state_visible_to'] != '') 
	{
		$role_name .= "\n" . 'from step ' . $row['role_order_state_visible_from'] . ' to step ' . $row['role_order_state_visible_to'];
	}
	elseif($row['role_order_state_visible_from'] != '')
	{
		$role_name .= "\n" . 'from step ' . $row['role_order_state_visible_from'];
	}
	elseif($row['role_order_state_visible_to'] != '') 
	{
		$role_name .= "\n" . 'to step ' . $row['role_order_state_visible_to'];
	}

	
	$sheet->write($rowindex,$cellindex, $role_name, $f_caption1);
	$sheet->setColumn($cellindex, $cellindex, 4);

	$cellindex++;
}




/********************************************************************
    write all permissions
*********************************************************************/
$rowindex = 3;
$cellindex = 0;

$sheet->setColumn($cellindex, $cellindex, 100);

$sql = "select * from permissions order by permission_application, permission_description";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$permission = str_replace("\r\n", "", $row['permission_description']);
	$permission = str_replace("\r", "", $permission);
	$permission = str_replace("\n", "", $permission);

	$permission = $row['permission_application'] . ' - ' . $permission;

	$sheet->write($rowindex,$cellindex, $permission, $f_caption);

	//print permissions

	$sql = "select * from roles order by role_name";

	$res1 = mysql_query($sql) or dberror($sql);
	while ($row1 = mysql_fetch_assoc($res1))
	{
		$cellindex++;

		$sql = "select count(role_permission_id) as num_recs from role_permissions " . 
			   "where role_permission_role = " . $row1['role_id'] .
			   " and role_permission_permission = " . $row['permission_id'];

		$res2 = mysql_query($sql) or dberror($sql);
		$row2 = mysql_fetch_assoc($res2);
		
		if($row2['num_recs'] == 1) 
		{
			$sheet->write($rowindex,$cellindex, "x", $f_center);
		}
		else
		{
			$sheet->write($rowindex,$cellindex, "", $f_center);
		}
	}



	$cellindex = 0;
	$rowindex++;
}


$xls->close(); 

?>