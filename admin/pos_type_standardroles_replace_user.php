<?php
/********************************************************************

    pos_type_standardroles_replace_user.php

    Repalce Users.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-03-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-03-14
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("pos_type_standardrole.php");

$sql_pos_types = "select postype_id, postype_name ".
				 "from postypes " .
				 "where postype_showin_openprojects = 1 " .
				 "order by postype_name";


$sql_country = "select DISTINCT country_id, country_salesregion, country_name, salesregion_name " .
			   "from standardroles " . 
			   "left join countries on country_id = standardrole_country " . 
			   "left join salesregions on salesregion_id = country_salesregion " . 
			   "order by salesregion_name, country_name";



$form = new Form("postype_notifications", "POS Type Notifications");

$form->add_section("New Users");

$form->add_list("standardrole_rtco_user", "Project Manager",
    "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as username from user_roles left join users on user_id = user_role_user where (user_role_role = 3 or user_role_role = 10) and user_active = 1 order by username ", SUBMIT);
$form->add_list("standardrole_dsup_user", "Design Supervisor",
    "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as username from user_roles left join users on user_id = user_role_user where (user_role_role = 8 ) and user_active = 1 order by username ");
$form->add_list("standardrole_rtop_user", "Retail Operator",
    "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as username from user_roles left join users on user_id = user_role_user where (user_role_role = 2 ) and user_active = 1 order by username ");
$form->add_list("standardrole_cms_approver_user", "CMS Approval by",
    "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as username from user_roles left join users on user_id = user_role_user where (user_role_role in (3, 8, 10)) and user_active = 1 order by username ");


$form->add_comment("");
$form->add_label("L2", "POS Types", "", "Select the Project Types");
$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
while($row = mysql_fetch_assoc($res))
{
    $form->add_checkbox("PT_" . $row["postype_id"], $row["postype_name"], false);

}

$salesregion = '';
$form->add_comment("");
$form->add_label("L5", "Country", "", "Select the Countries");
$sales_regions = array();
$res = mysql_query($sql_country) or dberror($sql_country);
while($row = mysql_fetch_assoc($res))
{
    if($salesregion != $row["salesregion_name"]) {
		$salesregion =  $row["salesregion_name"];
		$sales_region_id = $row['country_salesregion'];

		$form->add_section($row["salesregion_name"]);

		$form->add_label("select_" . $sales_region_id, "", RENDER_HTML, "<div id='select_" . $sales_region_id ."'><a href='javascript:select_all_" . $sales_region_id. "();'>select all</a></div>");

		$sales_regions[$sales_region_id]['region'] = $salesregion;
		$sales_regions[$sales_region_id]['countries'] = array();
	}
	
	$form->add_checkbox("CO_" . $row["country_id"], $row["country_name"], false, 0, "", false);
	$sales_regions[$sales_region_id]['countries'][] = "CO_" . $row["country_id"];
}



$form->add_button("replace", "Replace User");
$form->add_button("back", "Back");
$form->populate();
$form->process();

if($form->button("back")) {

	redirect("pos_type_standardroles.php");
}
elseif($form->button("replace") and $form->validate()) {
    
	$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
	while($row = mysql_fetch_assoc($res)) 
	{
		if($form->value("PT_" . $row["postype_id"]) == 1) {
			$res2 = mysql_query($sql_country) or dberror($sql_country);
			while($row2 = mysql_fetch_assoc($res2)) 
			{
				if($form->value("CO_" . $row2["country_id"]) == 1) {
					
					if($form->value("standardrole_rtco_user") > 0) {
						$sql = "update standardroles SET " .
							   "standardrole_rtco_user = " . $form->value("standardrole_rtco_user") . 
							   " where standardrole_postype = " .$row["postype_id"]  .
							   " and standardrole_country = " . $row2["country_id"];

						$result = mysql_query($sql) or dberror($sql);
					}

					if($form->value("standardrole_dsup_user") > 0) {
						$sql = "update standardroles SET " .
							   "standardrole_dsup_user = " . $form->value("standardrole_dsup_user") . 
							   " where standardrole_postype = " . $row["postype_id"]  .
							   " and standardrole_country = " . $row2["country_id"];

						$result = mysql_query($sql) or dberror($sql);
					}

					if($form->value("standardrole_rtop_user") > 0) {
						$sql = "update standardroles SET " .
							   "standardrole_rtop_user = " . $form->value("standardrole_rtop_user") . 
							   " where standardrole_postype = " . $row["postype_id"]  .
							   " and standardrole_country = " . $row2["country_id"];

						$result = mysql_query($sql) or dberror($sql);

					}

					if($form->value("standardrole_cms_approver_user") > 0) {
						$sql = "update standardroles SET " .
							   "standardrole_cms_approver_user = " . $form->value("standardrole_cms_approver_user") . 
							   " where standardrole_postype = " .$row["postype_id"]  .
							   " and standardrole_country = " . $row2["country_id"];

						$result = mysql_query($sql) or dberror($sql);
					}
				}
			}
		}
	}

	redirect("pos_type_standardroles.php");

}

$page = new Page("standardroles");

$page->header();
$page->title("POS Type Standard Roles: Replace User");

$form->render();


?>
<script language='javascript'>
<?php
foreach($sales_regions as $region_id=>$salesregion) {
?>	
	function select_all_<?php echo $region_id;?>()
	{
	
	<?php
	foreach($salesregion['countries'] as $key=>$formfield)
	{
		echo   "document.getElementById('" . $formfield . "').checked = true;" .  "\n";
	}
	?>

	var div = document.getElementById("select_<?php echo $region_id;?>");
    div.innerHTML = "<a href='javascript:deselect_all_<?php echo $region_id;?>();'>deselect all</a>";
	}


	function deselect_all_<?php echo $region_id;?>()
	{
		<?php
		foreach($salesregion['countries'] as $key=>$formfield)
		{
			echo   "document.getElementById('" . $formfield . "').checked = false;" .  "\n";
		}
		?>
		var div = document.getElementById("select_<?php echo$region_id;?>");
		div.innerHTML = "<a href='javascript:select_all_<?php echo $region_id;?>();'>select all</a>";
	}

<?php
}
?>
</script>

<?php

$page->footer();

?>
