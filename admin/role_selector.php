<?php
/********************************************************************

    role_selector.php

    Enter user Roels

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2012-11-04
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2012-11-04
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../include/page_modal.php";

check_access("can_edit_catalog");


/********************************************************************
    prepare all data needed
*********************************************************************/
if(param("application")) {
	$user_id = param("id");
	$application = param("application");
}
else
{
	exit;
}


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new form("users", "role_selector");
$form->add_hidden("id", id());
$form->add_hidden("user_id", id());
$form->add_hidden("application", param("application"));
$form->add_hidden("save_form", 1);

$page_title = "Role Selector";

if(!array_key_exists('user_roles', $_POST))
{
	
	$form->add_checklist("user_roles", "", "user_roles",
			"select role_id, role_name from roles where role_application = " . dbquote($application) . " order by role_name");
}

$form->add_input_submit("submit", "Save Values", 0);

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page_Modal("users");

$page->header();
$page->title($page_title);

$form->render();

if(param("save_form"))
{
	
	$role_ids = array();
	$sql = "select role_id from roles where role_application = " . dbquote($application);
	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res)) 
	{
		$role_ids[] = $row["role_id"];
	}

	$sql = "delete from user_roles where user_role_user = " . dbquote(id()) . 
			   " and user_role_role IN(" . implode(',', $role_ids). ")";
	mysql_query($sql) or dberror($sql);


	if( array_key_exists('user_roles', $_POST))
	{
		foreach($_POST['user_roles'] as $key=>$value)
		{
			$fields = array("user_role_user", "user_role_role", "date_created", "user_created", "date_modified", "user_modified");
			$values = array(id(), $value, "current_timestamp", dbquote(user_login()), "current_timestamp", dbquote(user_login()));

			$sql = "insert into user_roles (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			mysql_query($sql) or dberror($sql);

		}
	}

?>

	<script languege="javascript">
	var back_link = "user.php?id=<?php echo id();?>"; 
	$.nyroModalRemove();
	</script>

<?php
}
$page->footer();
