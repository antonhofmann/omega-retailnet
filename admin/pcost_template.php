<?php
/********************************************************************

    pcost_template.php

    Creation and mutation of project cost templates.

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2004-09-27
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2014, OMEGA SA, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("pcost_templates", "pcost_template");

$form->add_section();

$form->add_list("pcost_template_postype_id", "POS Type*",
    "select postype_id, postype_name from postypes order by postype_name", NOTNULL);
$form->add_edit("pcost_template_name", "Template Name*", NOTNULL | UNIQUE);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();



if($form->button(FORM_BUTTON_DELETE))
{
	$sql = "Delete from pcost_positions where pcost_position_pcost_template_id = " . id();
	$result = mysql_query($sql) or dberror($sql);

}
elseif($form->button(FORM_BUTTON_SAVE))
{
	$link = "pcost_template.php?id=" . id();
	redirect($link);
}

//add all cost groups and cost sub groups
$list_names = array();
$button_names = array();
$group_ids = array();
if(id() > 0)
{
	$sql = "select pcost_group_id, pcost_group_code, pcost_group_name " .
		   "from pcost_groups " . 
		   "order by pcost_group_code";


	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$listname = "list" . $row["pcost_group_code"];
		$buttonname = "add_new" . $row["pcost_group_code"];
		$list_names[] = $listname;
		$button_names[] = $buttonname;
		$group_ids[] = $row["pcost_group_id"];
		
		$sql = "select pcost_position_id, pcost_group_id, pcost_group_code, pcost_group_name, " . 
			   "concat(pcost_subgroup_code, ' ', pcost_subgroup_name) as subgroup,  " . 
			   "pcost_position_id, pcost_position_pcostgroup_id, pcost_position_pcost_subgroup_id, pcost_position_code, pcost_position_text " .
			   "from pcost_groups " .
			   "left join pcost_positions on pcost_position_pcostgroup_id = " . $row["pcost_group_id"]  . 
			   " left join pcost_subgroups on  pcost_subgroup_id = pcost_position_pcost_subgroup_id"; 

		$link = "pcost_position.php?tid=" . id();
		$$listname = new ListView($sql);

		$$listname->set_entity("pcost_groups");
		$$listname->set_order("pcost_position_code");
		$$listname->set_group("subgroup");
		$$listname->set_filter("pcost_position_pcost_template_id = " . id() . " and pcost_group_id = " . $row["pcost_group_id"]);
		$$listname->set_title($row["pcost_group_name"]);

		$$listname->add_column("pcost_position_code", "Code", $link);
		$$listname->add_column("pcost_position_text", "Text");

		$$listname->add_button($buttonname, "Add new Cost Position");

		$$listname->populate();
		$$listname->process();
	}


	
	foreach($button_names as $key=>$buttonname)
	{
		if($form->button($buttonname))
		{
			$link = "pcost_position.php?tid=" . id() . "&gid=" . $group_ids[$key];
			redirect($link);
		}
	}
	

}



$page = new Page("pcost_templates");
$page->header();
$page->title(id() ? "Edit Project Cost Template" : "Add Project Cost Template");
$form->render();

foreach($list_names as $key=>$listname)
{
	echo "<p>&nbsp;</p>";
	$$listname->render();
}
$page->footer();

?>