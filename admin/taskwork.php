<?php
/********************************************************************

    taskwork.php

    Mutation of taskwork records.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-02-18
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-02-18
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$sql_order_states = "select order_state_id, concat(order_state_code, ' ' , order_state_name) as text " .
                    "from order_states " .
					"where order_state_order_state_type = 1 " .
					"order by order_state_code";

$form = new Form("taskworks", "taskwork");

$form->add_section();
$form->add_edit("taskwork_shortcut", "Name", NOTNULL);
$form->add_list("taskwork_order_state", "Project Step", $sql_order_states, 0);
$form->add_list("taskwork_productline", "Product Line",
    "select product_line_id, product_line_name from product_lines where product_line_budget = 1 order by product_line_name", NOTNULL);
$form->add_list("taskwork_postype", "POS Type",
    "select postype_id, postype_name from postypes order by postype_name", NOTNULL);

$form->add_list("taskwork_address", "Text is applied only for",
    "select address_id, concat(address_shortcut, ': ', address_company) as name from addresses order by address_shortcut", SUBMIT);
$form->add_multiline("taskwork_text", "Text", 10);
$form->add_multiline("taskwork_alert_text", "Alert text", 10);

$form->populate();

$form->add_section("Auto Item");
$form->add_comment("Select a service that should be added to the list of materials when performing this action.");

$sql_items = "select item_id, concat(item_code, ': ', item_name) as name from items " .
             "left join suppliers on supplier_address = '" . $form->value("taskwork_address") . "' " .
			 "where item_type = 7 and supplier_item = item_id order by item_code";

$form->add_list("taskwork_item", "Service",$sql_items);


$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();
$form->process();

$page = new Page("taskworks");
$page->header();
$page->title(id() ? "Edit Project State Standard Text" : "Add Project State Standard Text");
$form->render();
$page->footer();

?>