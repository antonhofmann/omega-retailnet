<?php
/********************************************************************

    addon.php

    Creation and mutation of item addon records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-09-04
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-09-17
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("addons", "addon");

$form->add_hidden("item", param("item"));

$form->add_section();
$form->add_lookup("addon_parent", "Item", "items", "concat(item_code, '\n', item_name)", 0, param("item"));
//$form->add_lookup("addon_parent_name", "", "items", "item_name", HIDEEMPTY, param("item"));

$form->add_section();
$form->add_list("addon_child", "Addon",
    "select item_id, item_code " .
    "from items " .
    "order by item_code");

$form->add_section();
$form->add_edit("addon_package_quantity", "Package Quantity", NOTNULL);
$form->add_edit("addon_min_packages", "Minimum Packages", NOTNULL);
$form->add_edit("addon_max_packages", "Maximum Packages", NOTNULL);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("items");
$page->header();
$page->title(id() ? "Edit Addon" : "Add Addon");
$form->render();
$page->footer();

?>