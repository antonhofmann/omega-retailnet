<?php
/********************************************************************

    addresses.php

    Lists addresses for editing.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-29
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-08-19
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("address.php");

/********************************************************************
    prepare all data needed
*********************************************************************/

$images = array();
$sql_images = "select address_id " . 
			  "from addresses " .
			  "where address_active = 1 and address_type <> 7 and address_checked = 0";

$res = mysql_query($sql_images) or dberror($sql_images);

while ($row = mysql_fetch_assoc($res))
{
    $images[$row["address_id"]] = "/pictures/wf_warning.gif";
}

/********************************************************************
    Create Form
*********************************************************************/ 
$form = new Form("addresses", "address");

$form->add_section("List Filter Selection");

if(param("af"))
{
	$form->add_list("af", "Address Type",
		"select address_type_id, address_type_name from address_types  where address_type_id <> 7 order by address_type_name", SUBMIT | NOTNULL, param("af"));
}
else
{
	$form->add_list("af", "Address Type",
		"select address_type_id, address_type_name from address_types  where address_type_id <> 7 order by address_type_name", SUBMIT | NOTNULL, 1);
}


/********************************************************************
    Create List
*********************************************************************/ 

$list_filter = "address_active = 1 and (address_type <> 7 or address_type is NULL) ";

if(param("af") > 1)
{
	$list_filter .=	" and address_type = " . param("af") . " ";
}
else
{
	$list_filter .=	" and address_type = 1 ";
}


$list = new ListView("select address_id, address_shortcut, address_company, address_zip,  " .
                     "    address_place, country_name, address_type_name, province_canton, client_type_code, currency_symbol " .
                     "from addresses " .
					 " left join address_types on address_type_id = address_type " . 
					 " left join client_types on client_type_id = address_client_type " . 
					 "left join countries on address_country = country_id " . 
					 "left join places on place_id = address_place_id " . 
					 "left join provinces on province_id = place_province " . 
					 "left join currencies on currency_id = address_currency");

$list->set_entity("addresses");
$list->set_order("address_shortcut");
$list->set_filter($list_filter);

$list->add_image_column("notchecked", "", 0, $images);
$list->add_column("address_shortcut", "Shortcut", "address.php?af=" . param("af"));
$list->add_column("address_type_name", "Type", "");

if(param("af") == 1 or param("af") == '')
{
	$list->add_column("client_type_code", "Client Type");
}
$list->add_column("address_company", "Company", "");
$list->add_column("province_canton", "Province", "");
$list->add_column("address_zip", "Zip", "", "", "", COLUMN_ALIGN_RIGHT);
$list->add_column("address_place", "City", "");
$list->add_column("country_name", "Country");
$list->add_column("currency_symbol", "Currency");

$list->add_button(LIST_BUTTON_NEW, "New", "address.php?af=" . param("af"));

/********************************************************************
    Populate and process button clicks
*********************************************************************/ 
$form->populate();
$list->process();


/********************************************************************
   Create Page
*********************************************************************/ 
$page = new Page("addresses");

$page->header();
$page->title("Retail Net Active Addresses");
$form->render();
$list->render();
$page->footer();

?>
