<?php
/********************************************************************

    replace_country.php

    replaces a country by another

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2010-12-03
    Version:        1.0.1

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

// Build form

$form = new Form("countries", "country");

$form->add_list("country_old", "Old Country",
    "select country_id, country_name from countries order by country_name");

$form->add_list("country_new", "New Country",
    "select country_id, country_name from countries order by country_name");

$form->add_button(FORM_BUTTON_SAVE, "Perform the Replacement");


// Populate form and process button clicks

$form->populate();
$form->process();

if ($form->button(FORM_BUTTON_SAVE))
{
    // add validation ruels
    
    if (!$form->value("country_old"))
    {
        $form->add_validation("{country_old}", "Old country must be indicated!");
    }
    elseif (!$form->value("country_new"))
    {
        $form->add_validation("{country_new}", "New country must be indicated!");
    }

    

    // validate form
    
    if ($form->validate())
    {
        

        $sql = "Update addresses set address_country = " . $form->value("country_new") .
               " where address_country = " . $form->value("country_old");

        $res = mysql_query($sql) or dberror($sql);

		$sql = "Update _addresses set address_country = " . $form->value("country_new") .
               " where address_country = " . $form->value("country_old");

        $res = mysql_query($sql) or dberror($sql);

		$sql = "Update cer_inflationrates set inflationrate_country = " . $form->value("country_new") .
               " where inflationrate_country = " . $form->value("country_old");

        $res = mysql_query($sql) or dberror($sql);

		$sql = "Update cer_interestrates set interestrate_country = " . $form->value("country_new") .
               " where interestrate_country = " . $form->value("country_old");

        $res = mysql_query($sql) or dberror($sql);

		$sql = "Update country_access set country_access_country = " . $form->value("country_new") .
               " where country_access_country = " . $form->value("country_old");

        $res = mysql_query($sql) or dberror($sql);

		$sql = "Update download_countries set download_countries_country = " . $form->value("country_new") .
               " where download_countries_country = " . $form->value("country_old");

        $res = mysql_query($sql) or dberror($sql);

		$sql = "Update order_addresses set order_address_country = " . $form->value("country_new") .
               " where order_address_country = " . $form->value("country_old");

        $res = mysql_query($sql) or dberror($sql);

		$sql = "Update places set place_country = " . $form->value("country_new") .
               " where place_country = " . $form->value("country_old");

        $res = mysql_query($sql) or dberror($sql);

		$sql = "Update _posaddresses set posaddress_country = " . $form->value("country_new") .
               " where posaddress_country = " . $form->value("country_old");

        $res = mysql_query($sql) or dberror($sql);

		$sql = "Update posaddresses set posaddress_country = " . $form->value("country_new") .
               " where posaddress_country = " . $form->value("country_old");

        $res = mysql_query($sql) or dberror($sql);

		$sql = "Update standardroles set standardrole_country = " . $form->value("country_new") .
               " where standardrole_country = " . $form->value("country_old");

        $res = mysql_query($sql) or dberror($sql);

		$sql = "Update provinces set province_country = " . $form->value("country_new") .
               " where province_country = " . $form->value("country_old");

        $res = mysql_query($sql) or dberror($sql);

      

        redirect("replace_country_done.php");
    }

}




// Render page

$page = new Page("countries");

$page->header();
$page->title("Replace a Country by another Country");

echo "<p>", "The replacement concerns addresses, inflationrates, interestrates, country access of users, download restrictions, order addresses, places, pos addresses, pos type standard roles, provinces .", "</p>";

$form->render();
$page->footer();

?>