<?php
/********************************************************************

    pcost_group.php

    Creation and mutation of project cost groups.

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2004-09-27
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2014, OMEGA SA, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


$sql_posinvestment_types = "select posinvestment_type_id, posinvestment_type_name " .
                           "from posinvestment_types " . 
						   " where posinvestment_type_cms_sumcostgroups is not null " . 
						   " order by posinvestment_type_name ";

$form = new Form("pcost_groups", "pcost_group");

$form->add_section();

$form->add_edit("pcost_group_code", "Code*", NOTNULL | UNIQUE);
$form->add_edit("pcost_group_name", "Name*", NOTNULL | UNIQUE);
$form->add_list("pcost_group_posinvestment_type_id", "CER Investment Type*", $sql_posinvestment_types, NOTNULL);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("pcost_groups");
$page->header();
$page->title(id() ? "Edit Project Cost Group" : "Add Project Cost Group");
$form->render();
$page->footer();

?>