<?php
/********************************************************************

    project_legal_types.php

    Lists project_legal_types for editing.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-03-22
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-03-22
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("project_legal_type.php");

$list = new ListView("select project_costtype_id , project_costtype_text, project_costtype_email1, " . 
                     "project_costtype_email2, project_costtype_email3, project_costtype_email4 " .
                     "from project_costtypes", LIST_HAS_HEADER | LIST_SHOW_COUNT);

$list->set_entity("project_costtypes");



$list->set_order("project_costtype_text");

$list->add_column("project_costtype_text", "Name", "project_legal_type.php");
$list->add_column("project_costtype_email1", "Notify 1");
$list->add_column("project_costtype_email2", "Notify 2");
$list->add_column("project_costtype_email3", "Notify 3");
$list->add_column("project_costtype_email4", "Notify 4");

$list->process();

$page = new Page("project_cost_types");

$page->header();
$page->title("Legal Types");
$list->render();
$page->footer();

?>
