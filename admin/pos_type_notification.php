<?php
/********************************************************************

    pos_type_notification.php

    Creation and editing of notification recipients.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-04-21
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-07
    Version:        1.0.1

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("postype_notifications", "POS Type Notification Recipient");


$form->add_section();


$form->add_list("postype_notification_postype", "POS Type",
    "select postype_id, postype_name from postypes " .
    "order by postype_name", NOTNULL);

$form->add_list("postype_notification_prodcut_line", "Product Line",
    "select product_line_id, product_line_name from product_lines where product_line_budget = 1 " .
    "order by product_line_name", NOTNULL);

$form->add_edit("postype_notification_email8", "Email on Preview Approval by Client", 0);
$form->add_edit("postype_notification_email4", "Email on New Attachment of Type Layout", 0);
$form->add_edit("postype_notification_email7", "Email on Layout Approval by Client", 0);
$form->add_edit("postype_notification_email5", "Email on Submit Budget for Approval", 0);
$form->add_edit("postype_notification_email6", "Email on Budget Approval by Client", 0);
$form->add_edit("postype_notification_email3", "Email on Archiving Project", 0);
$form->add_edit("postype_notification_email2", "Email on Cancel Project", 0);
$form->add_edit("postype_notification_email2", "Email on Cancel Project", 0);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("postype_notifications");

$page->header();
$page->title(id() ? "Edit Project Notification Recipient" : "Add Project Notification Recipient");
$form->render();
$page->footer();

?>
