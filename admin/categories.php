<?php
/********************************************************************

    categories.php

    Lists categories for editing.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-05
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-09-04
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_browse_catalog_in_admin");
set_referer("category.php");

$list = new ListView("select category_id, category_name, category_cost_unit_number, " .
                     "    if (category_budget, 'yes', 'no') as category_budget, " .
                     "    if (category_catalog, 'yes', 'no') as category_catalog, " .
                     "    product_line_name " .
                     "from categories left join product_lines on category_product_line = product_line_id");

$list->set_filter("category_not_in_use is null or category_not_in_use = 0");
$list->set_entity("categories");
$list->set_order("product_line_name, category_priority");

$list->add_column("product_line_name", "Product Line", "", LIST_FILTER_LIST,
    "select product_line_name from product_lines order by product_line_name");
$list->add_column("category_name", "Name", "category.php", LIST_FILTER_FREE);
$list->add_column("category_cost_unit_number", "Cost Unit Number", "", LIST_FILTER_FREE);
$list->add_column("category_budget", "In Budget", "", LIST_FILTER_LIST, array(0 => "No", 1 => "Yes"));
$list->add_column("category_catalog", "In Catalog", "", LIST_FILTER_LIST, array(0 => "No", 1 => "Yes"));


if(has_access("can_edit_catalog"))
{
	$list->add_button(LIST_BUTTON_NEW, "New", "category.php");
	$list->add_button(LIST_BUTTON_FILTER, "Filter");
	$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove filter");
}
else
{

}
$list->process();

$page = new Page("categories");

$page->header();
$page->title("Categories");
$list->render();
$page->footer();

?>
