<?php
/********************************************************************

    users_inactive.php

    Lists inactive users for editing.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-07-17
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-07-17
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("user.php");

$list = new ListView("select user_id, user_firstname, user_name, address_company, user_phone, user_email " .
                     "from users left join addresses on user_address = address_id");

$list->set_entity("users");
$list->set_filter("user_active = 0");
$list->set_order("user_name, user_firstname");

$list->add_column("user_firstname", "Firstname", "", LIST_FILTER_FREE);
$list->add_column("user_name", "Name", "user.php", LIST_FILTER_FREE);
$list->add_column("address_company", "Company", "", LIST_FILTER_LIST,
    "select address_company from addresses order by address_company");
$list->add_column("user_phone", "Phone", "", LIST_FILTER_FREE);
$list->add_column("user_email", "Email", "", LIST_FILTER_FREE);

$list->add_button(LIST_BUTTON_FILTER, "Filter");
$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove filter");

$list->process();

$page = new Page("users");

$page->header();
$page->title("Users Inactive");
$list->render();
$page->footer();

?>
