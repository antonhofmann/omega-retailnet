<?php
/********************************************************************

    eventmail.php

    Edit reciepienst for system events.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-12-18
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-12-18
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/


require_once "../include/frame.php";

check_access("can_edit_catalog");


$sql_users01 = "select user_id, concat(user_name, ' ', user_firstname) as name from users " . 
             "where user_address IN(13) and (user_active = 1 or user_id = {eventmail_user01})" . 
			 "order by user_name, user_firstname";

$sql_users02 = "select user_id, concat(user_name, ' ', user_firstname) as name from users " . 
             "where user_address IN(13) and (user_active = 1 or user_id = {eventmail_user02})" . 
			 "order by user_name, user_firstname";

$sql_users03 = "select user_id, concat(user_name, ' ', user_firstname) as name from users " . 
             "where user_address IN(13) and (user_active = 1 or user_id = {eventmail_user03})" . 
			 "order by user_name, user_firstname";

$sql_users04 = "select user_id, concat(user_name, ' ', user_firstname) as name from users " . 
             "where user_address IN(13) and (user_active = 1 or user_id = {eventmail_user04})" . 
			 "order by user_name, user_firstname";

$form = new Form("eventmails", "Eventmails");


$form->add_section();


$form->add_label("eventmail_code", "Code", 0);
$form->add_label("eventmail_description", "Code", 0);
$form->add_edit("eventmail_subject", "Subject", 0);
$form->add_multiline("eventmail_text", "Comments", 10);


$form->add_list("eventmail_user01", "Recipient 1", $sql_users01, NOTNULL);
$form->add_list("eventmail_user02", "Recipient 2", $sql_users02, 0);
$form->add_list("eventmail_user03", "Recipient 3", $sql_users03, 0);
$form->add_list("eventmail_user04", "Recipient 4", $sql_users04, 0);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("eventmails");

$page->header();
$page->title(id() ? "Edit Eventmail" : "Add Eventmail");
$form->render();
$page->footer();

?>
