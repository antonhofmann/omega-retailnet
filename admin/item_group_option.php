<?php
/********************************************************************

    item_group_option.php

    Edit item_group_option

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-08-05
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-08-05
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

set_referer("item_group_file.php");


$form = new Form("item_group_options", "item_group_option");

$form->add_hidden("item_group_option_group", param("group"));
$form->add_hidden("group", param("group"));

$form->add_edit("item_group_option_name", "Option Name", NOTNULL);


$form->add_section("Items");
$form->add_edit("item_group_option_caption1", "Shortcut", NOTNULL);

$form->add_multiline("item_group_option_description1", "Description", 4);

$form->add_edit("item_group_option_quantity1", "Quantity", NOTNULL);
$form->add_list("item_group_option_item1", "Item",
    "select item_id, concat(item_code, ': ', item_name) as itemname " .
    "from items  " .
    "where item_type = 1 " .
    "order by item_code, item_name", NOTNULL);



$form->add_label("title01", "", 0, "can replace");

$form->add_edit("item_group_option_quantity2", "Quantity", NOTNULL);
$form->add_list("item_group_option_item2", "Item",
    "select item_id, concat(item_code, ': ', item_name) as itemname " .
    "from items  " .
    "where item_type = 1 " .
    "order by item_code, item_name", NOTNULL);


$form->add_edit("item_group_option_configcode1", "Picture Code", NOTNULL);

$form->add_multiline("item_group_option_info1", "Detail Info", 4);

$form->add_label("spacer01", "", 0, "");
$form->add_radiolist( "item_group_option_logical", "Logical",
    array(0 => "and", 1 => "or"), 0, 0);
$form->add_label("spacer02", "", 0, "");

$form->add_edit("item_group_option_caption2", "Shortcut");

$form->add_multiline("item_group_option_description2", "Description", 4);
$form->add_edit("item_group_option_quantity3", "Quantity", 0);
$form->add_list("item_group_option_item3", "Item",
    "select item_id, concat(item_code, ': ', item_name) as itemname " .
    "from items  " .
    "where item_type = 1 " .
    "order by item_code, item_name", 0);


$form->add_label("title02", "", 0, "can replace");

$form->add_edit("item_group_option_quantity4", "Quantity", 0);
$form->add_list("item_group_option_item4", "Item",
    "select item_id, concat(item_code, ': ', item_name) as itemname " .
    "from items  " .
    "where item_type = 1 " .
    "order by item_code, item_name", 0);

$form->add_edit("item_group_option_configcode2", "Picture Code");

$form->add_multiline("item_group_option_info2", "Detail Info", 4);

//files
$form->add_subtable("files", "<br />Files", "item_group_file.php", "Title",
    "select item_group_file_id, item_group_file_title as Title, file_purpose_name as Purpose, " .
    "    file_type_name as Type, item_group_file_path as Path " .
    "from item_group_files left join file_types on item_group_file_type = file_type_id " .
    "    left join file_purposes on item_group_file_purpose = file_purpose_id " .
    "where item_group_file_group_option = " . id() . " " .
    "order by item_group_file_title");


$form->add_button(FORM_BUTTON_SAVE, "Save");

$form->add_button("add_file", "Add File", "item_group_file.php?option=" . id(), OPTIONAL);


$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);


$form->populate();
$form->process();

$page = new Page("item_groups");
$page->header();
$page->title(id() ? "Edit Group Option" : "Add Group Option");
$form->render();
$page->footer();

?>