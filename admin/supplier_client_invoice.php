<?php
/********************************************************************

    supplier_client_invoice.php

    Mutation of supplier client relation in the context of invoicing records.

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2012-06-20
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2012-06-20
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


// Build form

$form = new Form("supplier_client_invoices", "supplier_client_invoice");

$form->add_section("Supplier");
$form->add_lookup("supplier_client_invoice_supplier", "Company", "addresses", "address_company", 0, param("address"));

$form->add_section("Client");
$form->add_list("supplier_client_invoice_client", "Client",
"select address_id, concat(country_name, ' ', address_company) as company from addresses left join countries on country_id = address_country where address_type = 1 and address_active = 1 or address_id = {supplier_client_invoice_client} order by country_name,address_company ", NOTNULL);

$form->add_section("Time Period for direct Invoicing");
$form->add_comment("Please note that the year can be 2038 in maximum.");
$form->add_edit("supplier_client_invoice_startdate", "Start invoicing directly from date*", NOTNULL, 0, TYPE_DATE, 10);
$form->add_edit("supplier_client_invoice_enddate", "Stop invoicing directly to date*", NOTNULL, 0, TYPE_DATE, 10);


$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
//$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();


// Render page

$page = new Page("addresses");

$page->header();
$page->title(id() ? "Edit Invoicing" : "Add Invoicing");
$form->render();

$page->footer();