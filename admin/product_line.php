<?php
/********************************************************************

    product_line.php

    Creation and mutation of product line records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-05
    Modified by:    Anton.Hofmann (aho@mediaparx.ch)
    Date modified:  2003-07-14
    Version:        1.1.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_browse_catalog_in_admin");
set_referer("category.php");
set_referer("design_objective_group.php");
set_referer("product_line_file.php");
set_referer("product_line_notification.php");


// create sql for the design supervisor listbox
/*
$sql_contractors = "select user_id, ".
                   "    concat(address_company, ', ', user_name, ' ', user_firstname) as user_fullname ".
                   "from users ".
                   "left join user_roles on user_role_user = user_id " .
				   "left join addresses on address_id = user_address " . 
                   "where user_role_role = 7 " .
                   "    and user_active = 1 ".
                   "order by user_name";

*/
// Build form

$form = new Form("product_lines", "product line");

if(id() > 0) {
	$form->add_hidden("product_line_priority");
}
else
{
	$form->add_hidden("product_line_priority", 1);
}

$form->add_section();
$form->add_edit("product_line_name", "Name", NOTNULL | UNIQUE);
//$form->add_list("product_line_design_contractor", "Standard Design Contractor", $sql_contractors, 0);
$form->add_multiline("product_line_description", "Description", 10);

$form->add_section("Visibility");
$form->add_checkbox("product_line_visible", "Visible in Catalog");
$form->add_checkbox("product_line_budget", "Visible in Projects");
$form->add_checkbox("product_line_clients", "Visible to Clients");
$form->add_checkbox("product_line_posindex", "Visible in POS Index");
$form->add_checkbox("product_line_mis", "Visible in MIS");


$form->add_section("Available in the following supplying regions");
$form->add_checklist("productline_regions", "", "productline_regions",
    "select region_id, region_name from regions order by region_name");


if(has_access("can_edit_catalog"))
{
	$form->add_subtable("categories", "<br />Categories", "category.php", "Name",
		"select category_id, category_name as Name, category_cost_unit_number as 'Cost Unit Number', " .
		"    if (category_budget, 'yes', 'no') as 'In Buget', " .
		"    if (category_catalog, 'yes', 'no') as 'In Catalog' " .
		"from categories " .
		"where category_product_line = " . id(),
		"category_priority");


	$form->add_subtable("design_objective_groups", "<br />Design Objective Groups", "design_objective_group.php", "Name",
		"select design_objective_group_id, design_objective_group_name as Name, " .
		"    if (design_objective_group_multiple, 'yes', 'no') as 'Multiple Choice' " .
		"from design_objective_groups " .
		"where design_objective_group_product_line = " . id(),
		"design_objective_group_priority");

	$form->add_subtable("files", "<br />Files", "product_line_file.php", "Title",
		"select product_line_file_id, product_line_file_title as Title, file_purpose_name as Purpose, " .
		"    file_type_name as Type, product_line_file_path as Path " .
		"from product_line_files left join file_types on product_line_file_type = file_type_id " .
		"    left join file_purposes on product_line_file_purpose = file_purpose_id " .
		"where product_line_file_product_line = " . id() . " " .
		"order by product_line_file_title");


	$form->add_button(FORM_BUTTON_SAVE, "Save");
	$form->add_button(FORM_BUTTON_BACK, "Back");
	$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);
	$form->add_button("add_category", "Add Category", "category.php?product_line=" . id(), OPTIONAL);
	$form->add_button("add_group", "Add Design Objective Group", "design_objective_group.php?product_line=" . id(), OPTIONAL);
	$form->add_button("add_file", "Add File", "product_line_file.php?product_line=" . id(), OPTIONAL);
}
else
{
	$form->add_subtable("categories", "<br />Categories", "category.php", "Name",
		"select category_id, category_name as Name, category_cost_unit_number as 'Cost Unit Number', " .
		"    if (category_budget, 'yes', 'no') as 'In Buget', " .
		"    if (category_catalog, 'yes', 'no') as 'In Catalog' " .
		"from categories " .
		"where category_product_line = " . id());


	$form->add_subtable("design_objective_groups", "<br />Design Objective Groups", "", "Name",
		"select design_objective_group_id, design_objective_group_name as Name, " .
		"    if (design_objective_group_multiple, 'yes', 'no') as 'Multiple Choice' " .
		"from design_objective_groups " .
		"where design_objective_group_product_line = " . id());


	$form->add_button(FORM_BUTTON_BACK, "Back");
}
// Populate form and process button clicks

$form->populate();

if($form->button(FORM_BUTTON_SAVE)) {
	
	$form->save();
	
	$prio = 1;
	$sql = 'select product_line_id from product_lines order by product_line_priority';

	$res = mysql_query($sql) or dberror($sql);
    while($row = mysql_fetch_assoc($res))
	{
		$sql = 'update product_lines set product_line_priority = ' . $prio . 
			   ' where product_line_id = ' . $row['product_line_id'];
		
		$result = mysql_query($sql) or dberror($sql);
		$prio++;
	}
}

/*
if (!$form->value("product_line_priority"))
{
    $form->value("product_line_priority", $row[0] + 1);
	
	$sql = "select max(product_line_priority) from product_lines";
    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_row($res);
    
}
*/

$form->process();

// Render page

$page = new Page("product_lines");

$page->header();
if(has_access("can_edit_catalog"))
{
	$page->title(id() ? "Edit Product Line" : "Add Product Line");
}
else
{
	$page->title("View Product Line");
}
$form->render();
$page->footer();
?>