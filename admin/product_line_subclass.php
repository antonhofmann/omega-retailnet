<?php
/********************************************************************

    product_line_subclass.php

    edit product line subclasses

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2010-12-03
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2010-12-03
    Version:        2.0.0

    Copyright (c) 2010, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


// new version depending on pos type
$form = new Form("productline_subclasses", "Product Line Subclass");

$form->add_section();
$form->add_list("productline_subclass_productline", "Product Line",
    "select product_line_id, product_line_name " .
    "from product_lines where product_line_budget = 1 " .
    "order by product_line_name", NOTNULL);
$form->add_edit("productline_subclass_name", "Name", NOTNULL);


$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

// Populate form and process button clicks

$form->populate();

$form->process();

if($form->button(FORM_BUTTON_SAVE)) {
	redirect('product_line_subclasses.php');
}

// Render page

$page = new Page("product_line_subclasses");

$page->header();
$page->title(id() ? "Edit Product Line Subclass" : "Add Product Line Subclass");
$form->render();
$page->footer();

?>