<?php
/********************************************************************

    snagging_template.php

    Creation and mutation of snagging_list templates.

    Created by:     Oliver Hofer (oliver.hofer@pageagent.com)
    Date created:   2003-05-06
    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date modified:  2003-05-14
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

//check_access("can_edit_catalog");
set_referer("snagging_template_element.php");

$sql_product_line = "select product_line_id, product_line_name " .
                    "from product_lines " .
                    "order by product_line_priority asc ";

$form = new Form("snagging_templates", "snagging template");

$form->add_section();
$form->add_edit("snagging_template_name", "Name", NOTNULL | UNIQUE);
$form->add_multiline("snagging_template_description", "Description", 4);
$form->add_list("snagging_template_product_line", "Product Line", $sql_product_line);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);


$form->add_section("","Snagging List Elements", "", "", "select");
$form->add_subtable("snagging_template_elements", "<br />Snagging List Elements", 
                    "snagging_template_element.php", "Name", 
                    "select snagging_template_element_id, " .
                    "    snagging_template_element_name as Name, " .
                    "    snagging_element_type_name as Type " . 
                    "from snagging_template_elements " .
                    "left join snagging_element_types on snagging_template_element_type = " .
                    "    snagging_element_type_id " .
                    "where snagging_template_element_template = " .id() ." " .
                    "order by snagging_template_element_priority ",
                    "snagging_template_element_priority", "Remove");

$form->add_button("add_element", "Add Element", 
                  "snagging_template_element.php?template=" . id(), OPTIONAL);
/*
$link = "http://" . $_SERVER["HTTP_HOST"] . "/admin/snagging_template_preview.php?id=" . id();
$form->add_button("preview_template", "Preview", $link, OPTIONAL);
*/
$link = "http://" . $_SERVER["HTTP_HOST"] . "/user/snagging_list_pdf.php?template=" . id();
$form->add_button("download_template", "Preview", $link, OPTIONAL);

$form->populate();
$form->process();

$page = new Page("snagging_list_templates");
$page->header();
$page->title(id() ? "Edit Snagging Template" : "Add Snagging Template");
$form->render();
$page->footer();

?>