<?php
/********************************************************************

    strings.php

    Lists strings for editing.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-03
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-08-20
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("string.php");

$list = new ListView("select string_id, string_name, string_usage " .
                     "from strings");

$list->set_entity("strings");
$list->set_order("string_name");

$list->add_column("string_name", "Name", "string.php", LIST_FILTER_FREE);
$list->add_column("string_usage", "Usage", "", LIST_FILTER_FREE);

$list->add_button(LIST_BUTTON_FILTER, "Filter");
$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove filter");

$list->process();

$page = new Page("strings");

$page->header();
$page->title("Strings");
$list->render();
$page->footer();

?>
