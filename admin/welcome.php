<?php
/********************************************************************

    welcome.php

    Main entry page after successful login.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-31
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2002-10-23
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_browse_catalog_in_admin");

$page = new Page("welcome");
$page->header();
echo "<p>Welcome to the administration section of " . APPLICATION_NAME . ".</p>";
$page->footer();

?>