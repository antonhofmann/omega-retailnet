<?php
/********************************************************************

    pos_type_standarddesigncontractor.php

    Assign Standard Design Contractor.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2010-11-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2010-11-14
    Version:        1.0.0

    Copyright (c) 2010, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("standarddesigncontractors", "Standard Design Contractors");

$form->add_section();
$form->add_list("standarddesigncontractor_productline", "Product Line",
    "select product_line_id, product_line_name from product_lines where product_line_budget = 1 and product_line_clients = 1 order by product_line_name", NOTNULL);

$form->add_list("standarddesigncontractor_postype", "POS Type",
    "select postype_id, postype_name from postypes order by postype_name", NOTNULL);

$form->add_list("standarddesigncontractor_dcon_user", "Design Contractor",
    "select DISTINCT user_id, concat(address_company, ': ', user_name, ' ', user_firstname) as username from user_roles left join users on user_id = user_role_user left join addresses on address_id = user_address where (user_role_role = 7 or user_id = {standarddesigncontractor_dcon_user}) and user_active = 1 order by address_company, username ", SUBMIT);


$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();
$form->process();

$page = new Page("standardroles");
$page->header();
$page->title(id() ? "Edit Standard Design Contractor" : "Add Standard Design Contractor");
$form->render();
$page->footer();

?>