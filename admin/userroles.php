<?php
/********************************************************************

    userroles.php

    Lists roles and all users for editing.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2007-11-21
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2007-11-21
    Version:        1.0.0

    Copyright (c) 2007, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$sql = "select role_id, role_code, role_name, " .
       "concat(user_name, ' ', user_firstname) as username, user_active, " .
       "address_company, country_name " . 
       "from roles " .
       "left join user_roles on user_role_role = role_id " .
       "left join users on user_id = user_role_user " .
       "left join addresses on address_id = user_address " .
       "left join countries on country_id = address_country";

$sql_r = "select role_id, role_name " .
             "from roles " .
		     "order by role_name";


$form = new Form("roles", "roles");

$form->add_section("List Filter Selection");

if(param("r"))
{
	$form->add_list("r", "Role", $sql_r, SUBMIT | NOTNULL, param("r"));
}
else
{
	$form->add_list("r", "Role", $sql_r, SUBMIT | NOTNULL, 15);
}


$form->populate();

if(param("r") > 1)
{
	$list_filter =	" user_active = 1 and role_id = " . param("r");
}
elseif(array_key_exists("r", $_POST))
{
	$list_filter =	" user_active = 1 ";
}
else
{
	$list_filter =	" user_active = 1 and role_id = 15";
}

$list = new ListView($sql);

$list->set_entity("roles");
$list->set_group("role_name");
$list->set_filter($list_filter);
$list->set_order("username");

$list->add_column("username", "Name", "", LIST_FILTER_FREE);
$list->add_column("address_company", "Company", "", LIST_FILTER_FREE);
$list->add_column("country_name", "Country", "", LIST_FILTER_FREE);

$list->process();

$page = new Page("userroles");

$page->header();
$page->title("Userroles");
$form->render();
$list->render();
$page->footer();

?>
