<?php
/********************************************************************

    security_unlock_ip.php

    unlock IP.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-09-05
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-09-05
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


$form = new Form("sec_lockedips", "sec_lockedips");

$form->add_section();
$form->add_label("sec_lockedip_ip", "Locked IP");

$form->add_button("unlock", "Unlock IP");
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();
$form->process();

if($form->button("unlock"))
{
	$sql = "delete from sec_lockedips where sec_lockedip_ip = " . dbquote(param("sec_lockedip_ip"));
	$result = mysql_query($sql) or dberror($sql);
	redirect("security.php");
}

$page = new Page("security");
$page->header();
$page->title("Unlock IP");
$form->render();
$page->footer();

?>