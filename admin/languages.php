<?php
/********************************************************************

    languages.php

    Lists languages for editing.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");
set_referer("language.php");

$sql = "select language_id, language_iso639_1, language_name, language_local_name " . 
       "from languages ";

$list = new ListView($sql);

$list->set_entity("languages");
$list->set_order("language_name");

$list->add_column("language_name", "Name", "language.php", LIST_FILTER_FREE);
$list->add_column("language_iso639_1", "ISO 639-1", "", LIST_FILTER_FREE);
$list->add_column("language_local_name", "Local Name");

$list->add_button(LIST_BUTTON_NEW, "New", "language.php");

$list->process();

$page = new Page("locales");

$page->header();
$page->title("Languages");
$list->render();
$page->footer();
?>
