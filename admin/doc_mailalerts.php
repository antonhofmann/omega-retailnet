<?php
/********************************************************************

    doc_mailalaerts.php

    Lists all mailalerts sent by the systm

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2013-05-01
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2013-05-01
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("doc_mailalaerts.php");


/********************************************************************
    prepare all data needed
*********************************************************************/

$sql = "select * from doc_mailalerts ";

/********************************************************************
    Create Form
*********************************************************************/ 


/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);

$list->set_entity("doc_mailalerts");
$list->set_order("doc_mailalert_context, doc_mailalert_event");

$list->add_column("doc_mailalert_context", "Context", "doc_mailalert.php");
$list->add_column("doc_mailalert_event", "Event");

$list->add_button(LIST_BUTTON_NEW, "New", "doc_mailalert.php");

/********************************************************************
    Populate and process button clicks
*********************************************************************/ 

$list->process();


/********************************************************************
   Create Page
*********************************************************************/ 
$page = new Page("mailalerts");

$page->header();
$page->title("Retail Net Mail Alerts");

$list->render();
$page->footer();

?>
