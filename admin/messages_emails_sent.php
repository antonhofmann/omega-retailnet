<?php
/********************************************************************

    messages_emails_sent.php

    Lists messages and sent mails.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2007-11-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2007-11-14
    Version:        1.0.0

    Copyright (c) 2007, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

set_referer("messages_emails_sent_recipients.php");

$sql = "select message_id, message_title, message_date " .
       "from messages ";


$list = new ListView($sql);

$list->set_entity("messages");
$list->set_order("message_date DESC");

$list->add_column("message_title", "Title", "messages_emails_sent_recipients.php");
$list->add_column("message_date", "Date");

$list->populate();
$list->process();

$page = new Page("messages");

$page->header();
$page->title("Messages - Emails sent");
$list->render();
$page->footer();

?>
