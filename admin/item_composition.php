<?php
/********************************************************************

    item_composition

    Edit Composite groups of items to an item

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-08-05
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-08-05
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


param("id", param("item"));
// Build form

$form = new Form("items", "items");


$form->add_hidden("item", param("item"));
$form->add_hidden("id", param("item"));

$form->add_lookup("name", "Item", "items", "item_name", 0, param("item"));

$form->add_section("Composite Groups");
$form->add_checklist("item_composition", "Groups", "item_compositions",
    "select item_group_id, item_group_name from item_groups order by item_group_name");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");


// Populate form and process button clicks

$form->populate();
$form->process();

// Render page

$page = new Page("items");

$page->header();
$page->title("Edit Item Composition");
$form->render();
$page->footer();

?>