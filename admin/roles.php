<?php
/********************************************************************

    roles.php

    Lists roles for editing.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-31
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-08-19
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("role.php");

$list = new ListView("select role_id, role_application, role_code, role_name from roles");

$list->set_entity("roles");
$list->set_order("role_application, role_name");

$list->add_column("role_application", "Application");
$list->add_column("role_code", "Code");
$list->add_column("role_name", "Name", "role.php");

$list->add_button(LIST_BUTTON_NEW, "New", "role.php");

$list->process();

$page = new Page("roles");

$page->header();
$page->title("Roles");
$list->render();
$page->footer();

?>
