<?php
/********************************************************************

    doc_mailalerts.php

    add/edit mailalerts

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2013-05-01
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2013-05-01
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

/********************************************************************
    prepare all data needed
*********************************************************************/

$context = array();
$context["catalog_order"] = "Catalogue Order";
$context["project"] = "Project";
$context["submission"] = "Submission";
$context["crons"] = "System";

/********************************************************************
    Create Form
*********************************************************************/ 


$form = new Form("doc_mailalerts", "doc_mailalerts");



$form->add_list("doc_mailalert_context", "Context", $context);
$form->add_edit("doc_mailalert_code", "Code*", NOTNULL);
$form->add_edit("doc_mailalert_event", "Event*", NOTNULL);
$form->add_edit("doc_mailalert_trigger", "Trigger*", NOTNULL);
$form->add_edit("doc_mailalert_mailsubject", "Mail Subject*", NOTNULL);
$form->add_multiline("doc_mailalert_mailtext", "Mail Text*", 12);
$form->add_edit("doc_mailalert_sender", "Sender", NOTNULL);
$form->add_multiline("doc_mailalert_recipients", "Recipients", 8);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button("back", "Back");

$form->populate();
$form->process();



if($form->button("back"))
{
	redirect("doc_mailalerts.php");
}


// Render page

$page = new Page("mailalerts");



$page->header();
$page->title(id() ? "Edit Mail Alert" : "Add Mail Alert");
$form->render();


$page->footer();

?>