<?php
/********************************************************************

    pcost_subgroup.php

    Creation and mutation of project cost subgroups.

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2004-09-27
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2014, OMEGA SA, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");



$cer_investment_types = array();

$cer_investment_types[18] = "Merchandising Material";
$cer_investment_types[19] = "Transportation";



$costtype = 0;
if(id() > 0) {
	$sql = "select pcost_subgroup_pcostgroup_id from pcost_subgroups " .
		   " where pcost_subgroup_id = " . id();
	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$costtype = $row["pcost_subgroup_pcostgroup_id"];
	}
}



$form = new Form("pcost_subgroups", "pcost_subgroup");

$form->add_section();

$form->add_list("pcost_subgroup_pcostgroup_id", "Cost Group*",
    "select pcost_group_id, concat(pcost_group_code, ' ', pcost_group_name) as name from pcost_groups order by pcost_group_code", NOTNULL);
$form->add_edit("pcost_subgroup_code", "Code*", NOTNULL);
$form->add_edit("pcost_subgroup_name", "Name*", NOTNULL);

$form->add_list("pcost_subgroup_cer_investment_type", "Business Plan Investment Type", $cer_investment_types);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("pcost_subgroups");
$page->header();
$page->title(id() ? "Edit Project Cost Subgroup" : "Add Project Cost Subgroup");
$form->render();
$page->footer();

?>