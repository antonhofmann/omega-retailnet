<?php
/********************************************************************

    invoice_address.php

    Mutation of client's invoice addresses.

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2012-06-20
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2012-06-20
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");



if(param("invoice_address_country_id"))
{
	$sql_places = "select place_id, concat(place_name, ' (', province_canton, ')') as place " . 
				  "from places " .
				  "left join provinces on province_id = place_province " . 
				  "where place_country = " . param("invoice_address_country_id") . " order by place_name";
}
else
{
	$sql_places = "select place_id, concat(place_name, ' (', province_canton, ')') as place " . 
				  "from places " .
				  "left join provinces on province_id = place_province " . 
				  "where place_country = {invoice_address_country_id} order by place_name";
}


// Build form

$form = new Form("invoice_addresses", "invoice_address_address");

$form->add_section("Client");
$form->add_lookup("invoice_address_address_id", "Client", "addresses", "address_company", 0, param("address"));


$form->add_section("Invoice Address");

$form->add_edit("invoice_address_company", "Company", NOTNULL);
$form->add_edit("invoice_address_company2", "");
$form->add_edit("invoice_address_address", "Address");
$form->add_edit("invoice_address_address2", "");

$form->add_list("invoice_address_country_id", "Country*",
    "select country_id, country_name from countries order by country_name", NOTNULL | SUBMIT);

$form->add_edit("invoice_address_zip", "Zip");
$form->add_list("invoice_address_place_id", "City*", $sql_places, NOTNULL | SUBMIT);

$form->add_section("Communication");
$form->add_edit("invoice_address_phone", "Phone");
$form->add_edit("invoice_address_fax", "Fax");
$form->add_edit("invoice_address_email", "Email");
$form->add_edit("invoice_address_contact_name", "Contact Name");

$form->add_checkbox("invoice_address_active", "Address in Use", true);


$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();
$form->process();


// Render page

$page = new Page("addresses");

$page->header();
$page->title(id() ? "Edit Invoice Address" : "Add Invoice Address");
$form->render();

$page->footer();