<?php
/********************************************************************

    pos_type_notifications.php

    Lists notification reciepienst of pos types.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-04-21
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-04-21
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("pos_type_notification.php");


$sql_postypes = "select pos_type_categoryname from pos_types order by pos_type_categoryname";

$sql = "select postype_notification_id , " . 
		"postype_notification_email2, " .
		"postype_notification_email3, " .
		"postype_notification_email4, " .
		"postype_notification_email5, " .
		"postype_notification_email6, " .
		"postype_notification_email7, " .
		"postype_notification_email8, " .
		"postype_name,  product_line_name " .
		"from postype_notifications " .
		"left join postypes on postype_id = postype_notification_postype " . 
		"left join product_lines on product_line_id = postype_notification_prodcut_line";


$list = new ListView($sql);

$list->set_entity("postype_notifications");
$list->set_order("product_line_name, postype_name, postype_notification_email2, postype_notification_email3, postype_notification_email4, postype_notification_email5, postype_notification_email6, postype_notification_email7, postype_notification_email8");

$list->add_column("product_line_name", "Product Line", "pos_type_notification.php", LIST_FILTER_LIST, $sql_postypes, COLUMN_NO_WRAP);
$list->add_column("postype_name", "POS Type");
$list->add_column("postype_notification_email8", "Preview Approval");
//$list->add_column("postype_notification_email4", "New Layout");
$list->add_column("postype_notification_email7", "Layout Approval");
$list->add_column("postype_notification_email5", "Budget Submitted");
$list->add_column("postype_notification_email6", "Budget Approval");
$list->add_column("postype_notification_email3", "Archive Project");
$list->add_column("postype_notification_email2", "Cancel Project");


$list->add_button(LIST_BUTTON_NEW, "New", "pos_type_notification.php");
$list->add_button("replace", "Replace Recipients", "Repleca Recipients");
$list->process();


if($list->button("replace") ) {

	redirect("pos_type_notifications_replace_mail.php");

}

$page = new Page("postype_notifications");

$page->header();
$page->title("Project Notification Recipients");

$list->render();
$page->footer();

?>
