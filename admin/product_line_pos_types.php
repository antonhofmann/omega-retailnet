<?php
/********************************************************************

    product_line_pos_types.php

    Lists product line pos types for editing.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-04-04
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-04-04
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("product_line_pos_type.php");


$sql = "select product_line_pos_type_id , postype_name, product_line_name, " . 
       "product_line_pos_type_starting_number " .
       "from product_line_pos_types " . 
	   "left join postypes on postype_id = product_line_pos_type_pos_type " . 
	   "left join product_lines on product_line_id = product_line_pos_type_product_line";

$list = new ListView($sql);

$list->set_entity("product_line_pos_types");
$list->set_order("postype_name, product_line_name");
$list->set_filter("product_line_pos_type_product_line is null ");

$list->add_column("postype_name", "POS Type", "product_line_pos_type.php");
//$list->add_column("product_line_name", "Product Line","product_line_pos_type.php");
$list->add_column("product_line_pos_type_starting_number", "Starting Number");

$list->add_button(LIST_BUTTON_NEW, "New", "product_line_pos_type.php");

$list->process();

$page = new Page("product_line_pos_types");

$page->header();
$page->title("POS Project Types");
$list->render();
$page->footer();

?>
