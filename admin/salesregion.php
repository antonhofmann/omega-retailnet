<?php
/********************************************************************

    salesregion.php

    Creation and mutation of salesregion records.

    reated by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-24
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

set_referer("country.php");

$form = new Form("salesregions", "salesregion");

$form->add_edit("salesregion_name", "Name", NOTNULL | UNIQUE);


$form->add_subtable("countries", "<br />Countries", "country.php", "Name",
    "select country_id, country_name as Name " .
    "from countries " .
    "where country_salesregion = " . id() . " " .
    "order by country_name");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("salesregions");
$page->header();
$page->title(id() ? "Edit Geographical Region" : "Add Geographical Region");
$form->render();
$page->footer();

?>