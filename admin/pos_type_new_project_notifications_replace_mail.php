<?php
/********************************************************************

    pos_type_new_project_notifications_replace_mail.php

    repleces email reciepients.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-03-11
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-03-11
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


$form = new Form("projecttype_newproject_notifications", "New Project Notifications");


$form->add_section();

$sql_pos_types = "select postype_id, postype_name ".
				 "from postypes " .
				 "where postype_showin_openprojects = 1 " .
				 "order by postype_name";


$sql_country = "select DISTINCT country_id, country_name, salesregion_name, country_salesregion " .
			   "from projecttype_newproject_notifications " . 
			   "left join countries on country_id = projecttype_newproject_notification_country " . 
			   "left join salesregions on salesregion_id = country_salesregion " . 
			   "order by salesregion_name, country_name";



$form->add_section('New Repients');
$form->add_comment('Please add all recipients for the new situation. Existing recipients will be overwritten.');
$form->add_edit("projecttype_newproject_notification_email", "Email on New Project", 0);
$form->add_edit("projecttype_newproject_notification_emailcc1", "Further Email", 0);
$form->add_edit("projecttype_newproject_notification_emailcc2", "Further Email", 0);
$form->add_edit("projecttype_newproject_notification_emailcc3", "Further Email", 0);
$form->add_edit("projecttype_newproject_notification_emailcc4", "Further Email", 0);
$form->add_edit("projecttype_newproject_notification_emailcc5", "Further Email", 0);
$form->add_edit("projecttype_newproject_notification_emailcc6", "Further Email", 0);
$form->add_edit("projecttype_newproject_notification_emailcc7", "Further Email", 0);

$form->add_section('Mail Alerts');
$form->add_checkbox("projecttype_newproject_notification_on_new_project", "on submitting new project", 0, 0, 'Send Mail');
$form->add_checkbox("projecttype_newproject_notification_on_lnsubmission", "on submitting LN");
$form->add_checkbox("projecttype_newproject_notification_on_lnresubmission", "on resubmitting LN");
$form->add_checkbox("projecttype_newproject_notification_oncerafsubmission", "on submitting CER/AF");
$form->add_checkbox("projecttype_newproject_notification_oncerafresubmission", "on resubmitting CER/AF");


$form->add_section('POS Types');
$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
while($row = mysql_fetch_assoc($res))
{
    $form->add_checkbox("PT_" . $row["postype_id"], $row["postype_name"], false);
}

$salesregion = '';
$res = mysql_query($sql_country) or dberror($sql_country);
while($row = mysql_fetch_assoc($res))
{
    if($salesregion != $row["salesregion_name"]) {
		$salesregion = $row["salesregion_name"];
		$sales_region_id = $row['country_salesregion'];

		$form->add_section($row["salesregion_name"]);

		$form->add_label("select_" . $sales_region_id, "", RENDER_HTML, "<div id='select_" . $sales_region_id ."'><a href='javascript:select_all_" . $sales_region_id. "();'>select all</a></div>");

		$sales_regions[$sales_region_id]['region'] = $salesregion;
		$sales_regions[$sales_region_id]['countries'] = array();
	}


	
	$form->add_checkbox("CO_" . $row["country_id"], $row["country_name"], false, 0, "", false);
	$sales_regions[$sales_region_id]['countries'][] = "CO_" . $row["country_id"];
}

$form->add_button("replace", "Replace Recipients");
$form->add_button("back", "Back");

$form->populate();
$form->process();



if($form->button("back")) {
	redirect("pos_type_new_project_notifications.php");
}
elseif($form->button("replace")) {
	
	$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
	while($row = mysql_fetch_assoc($res)) 
	{
		if($form->value("PT_" . $row["postype_id"]) == 1) {
			$res2 = mysql_query($sql_country) or dberror($sql_country);
			while($row2 = mysql_fetch_assoc($res2)) 
			{
				if($form->value("CO_" . $row2["country_id"]) == 1) {
					

					$sql = "update projecttype_newproject_notifications SET " .
						   "projecttype_newproject_notification_email = " . dbquote($form->value("projecttype_newproject_notification_email")) . 
						   " where projecttype_newproject_notification_postype = " .$row["postype_id"]  .
						   " and projecttype_newproject_notification_country = " . $row2["country_id"];

					$result = mysql_query($sql) or dberror($sql);
				
					$sql = "update projecttype_newproject_notifications SET " .
						   "projecttype_newproject_notification_emailcc1 = " . dbquote($form->value("projecttype_newproject_notification_emailcc1")) . 
						   " where projecttype_newproject_notification_postype = " .$row["postype_id"]  .
						   " and projecttype_newproject_notification_country = " . $row2["country_id"];

					$result = mysql_query($sql) or dberror($sql);
					
					$sql = "update projecttype_newproject_notifications SET " .
						   "projecttype_newproject_notification_emailcc2 = " . dbquote($form->value("projecttype_newproject_notification_emailcc2")) . 
						   " where projecttype_newproject_notification_postype = " .$row["postype_id"]  .
						   " and projecttype_newproject_notification_country = " . $row2["country_id"];

					$result = mysql_query($sql) or dberror($sql);
					
					$sql = "update projecttype_newproject_notifications SET " .
						   "projecttype_newproject_notification_emailcc3 = " . dbquote($form->value("projecttype_newproject_notification_emailcc3")) . 
						   " where projecttype_newproject_notification_postype = " .$row["postype_id"]  .
						   " and projecttype_newproject_notification_country = " . $row2["country_id"];

					$result = mysql_query($sql) or dberror($sql);
					$sql = "update projecttype_newproject_notifications SET " .
						   "projecttype_newproject_notification_emailcc4 = " . dbquote($form->value("projecttype_newproject_notification_emailcc4")) . 
						   " where projecttype_newproject_notification_postype = " .$row["postype_id"]  .
						   " and projecttype_newproject_notification_country = " . $row2["country_id"];

					$result = mysql_query($sql) or dberror($sql);

					$sql = "update projecttype_newproject_notifications SET " .
						   "projecttype_newproject_notification_emailcc5 = " . dbquote($form->value("projecttype_newproject_notification_emailcc5")) . 
						   " where projecttype_newproject_notification_postype = " .$row["postype_id"]  .
						   " and projecttype_newproject_notification_country = " . $row2["country_id"];

					$result = mysql_query($sql) or dberror($sql);

					$sql = "update projecttype_newproject_notifications SET " .
						   "projecttype_newproject_notification_emailcc6 = " . dbquote($form->value("projecttype_newproject_notification_emailcc6")) . 
						   " where projecttype_newproject_notification_postype = " .$row["postype_id"]  .
						   " and projecttype_newproject_notification_country = " . $row2["country_id"];

					$result = mysql_query($sql) or dberror($sql);

					$sql = "update projecttype_newproject_notifications SET " .
						   "projecttype_newproject_notification_emailcc7 = " . dbquote($form->value("projecttype_newproject_notification_emailcc7")) . 
						   " where projecttype_newproject_notification_postype = " .$row["postype_id"]  .
						   " and projecttype_newproject_notification_country = " . $row2["country_id"];

					$result = mysql_query($sql) or dberror($sql);

					

					$sql = "update projecttype_newproject_notifications SET " .
						   "projecttype_newproject_notification_on_new_project = " . dbquote($form->value("projecttype_newproject_notification_on_new_project")) . ', ' .
						   "projecttype_newproject_notification_on_lnsubmission = " . dbquote($form->value("projecttype_newproject_notification_on_lnsubmission")) . ', ' .
						   "projecttype_newproject_notification_on_lnresubmission = " . dbquote($form->value("projecttype_newproject_notification_on_lnresubmission")) . ', ' .
						   "projecttype_newproject_notification_oncerafsubmission = " . dbquote($form->value("projecttype_newproject_notification_oncerafsubmission")) . ', ' .
						   "projecttype_newproject_notification_oncerafresubmission = " . dbquote($form->value("projecttype_newproject_notification_oncerafresubmission")) . 
						   " where projecttype_newproject_notification_postype = " .$row["postype_id"]  .
						   " and projecttype_newproject_notification_country = " . $row2["country_id"];

					$result = mysql_query($sql) or dberror($sql);

					
				}
			}
		}
	}

	redirect("pos_type_new_project_notifications.php");


}

	

$page = new Page("projecttype_newproject_notifications");

$page->header();
$page->title("Notifications for Submissions: Replace Recipeints");

$form->render();


?>
<script language='javascript'>
<?php
foreach($sales_regions as $region_id=>$salesregion) {
?>	
	function select_all_<?php echo $region_id;?>()
	{
	
	<?php
	foreach($salesregion['countries'] as $key=>$formfield)
	{
		echo   "document.getElementById('" . $formfield . "').checked = true;" .  "\n";
	}
	?>

	var div = document.getElementById("select_<?php echo $region_id;?>");
    div.innerHTML = "<a href='javascript:deselect_all_<?php echo $region_id;?>();'>deselect all</a>";
	}


	function deselect_all_<?php echo $region_id;?>()
	{
		<?php
		foreach($salesregion['countries'] as $key=>$formfield)
		{
			echo   "document.getElementById('" . $formfield . "').checked = false;" .  "\n";
		}
		?>
		var div = document.getElementById("select_<?php echo$region_id;?>");
		div.innerHTML = "<a href='javascript:select_all_<?php echo $region_id;?>();'>select all</a>";
	}

<?php
}
?>
</script>

<?php

$page->footer();


?>
