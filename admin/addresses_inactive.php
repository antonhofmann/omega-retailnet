<?php
/********************************************************************

    addresses_inactive.php

    Lists inactive addresses for editing.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-29
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-08-19
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("address_inactive.php");

/********************************************************************
    prepare all data needed
*********************************************************************/

$images = array();
$sql_images = "select address_id " . 
			  "from addresses " .
			  "where address_active = 1 and address_type <> 7 and address_checked = 0";

$res = mysql_query($sql_images) or dberror($sql_images);

while ($row = mysql_fetch_assoc($res))
{
    $images[$row["address_id"]] = "/pictures/wf_warning.gif";
}

/********************************************************************
    Create Form
*********************************************************************/ 
$form = new Form("addresses", "address");

$form->add_section("List Filter Selection");

if(param("af"))
{
	$form->add_list("af", "Address Type",
		"select address_type_id, address_type_name from address_types  where address_type_id <> 7 order by address_type_name", SUBMIT | NOTNULL, param("af"));
}
else
{
	$form->add_list("af", "Address Type",
		"select address_type_id, address_type_name from address_types  where address_type_id <> 7 order by address_type_name", SUBMIT | NOTNULL, 1);
}





/********************************************************************
    Create List
*********************************************************************/ 

$list_filter = "address_active <> 1 and (address_type <> 7 or address_type is NULL) ";

if(param("af") > 1)
{
	$list_filter .=	" and Address_type = " . param("af") . " ";
}
else
{
	$list_filter .=	" and Address_type = 1 ";
}

$list = new ListView("select address_id, address_shortcut, address_company, address_zip,  " .
                     "    address_place, country_name, address_type_name, province_canton " .
                     "from addresses " .
					 " left join address_types on address_type_id = address_type " . 
					 "left join countries on address_country = country_id ". 
					"left join places on place_id = address_place_id " . 
					 "left join provinces on province_id = place_province");

$list->set_entity("addresses");
$list->set_order("address_shortcut");
$list->set_filter($list_filter);

$list->add_image_column("notchecked", "", 0, $images);
$list->add_column("address_shortcut", "Shortcut", "address_inactive.php?af=" . param("af"), LIST_FILTER_FREE);
$list->add_column("address_type_name", "Type", "", LIST_FILTER_LIST, "select address_type_name from address_types where and address_type_id <> 7 order by address_type_name");
$list->add_column("address_company", "Company", "", LIST_FILTER_FREE);
$list->add_column("province_canton", "Province", "", LIST_FILTER_FREE);
$list->add_column("address_zip", "Zip", "", LIST_FILTER_FREE);
$list->add_column("address_place", "City", "", LIST_FILTER_FREE);
$list->add_column("country_name", "Country", "", LIST_FILTER_LIST,
    "select country_name from countries order by country_name");

//$list->add_button(LIST_BUTTON_NEW, "New", "address_inactive.php?af=" . param("af"));
//$list->add_button(LIST_BUTTON_FILTER, "Filter");
$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove Filter");

/********************************************************************
    Populate and process button clicks
*********************************************************************/ 
$form->populate();
$list->process();


/********************************************************************
   Create Page
*********************************************************************/ 
$page = new Page("addresses");

$page->header();
$page->title("Retail Net Inactive Addresses");
$form->render();
$list->render();
$page->footer();

?>
