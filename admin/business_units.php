<?php
/********************************************************************

    business_units.php

    Lists roles for editing.

    Created by:     Oliver Hofer (oliver.hofer@pageagent.com)
    Date created:   2003-05-07
    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date modified:  2003-05-07
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("business_unit.php");

$sql = "select business_unit_id, business_unit_name, business_unit_application, " .
       "    concat(user_name, ' ', user_firstname) as user_name ".
       "from business_units " .
       "left join users on users.user_id = business_units.business_unit_responsible ";

$list = new ListView($sql);

$list->set_entity("business_units");
$list->set_order("business_unit_name");

$list->add_column("business_unit_name", "Business Unit", "business_unit.php", LIST_FILTER_FREE);
//$list->add_column("user_name", "Responsible", "", LIST_FILTER_FREE);
//$list->add_column("business_unit_application", "Application", "", LIST_FILTER_FREE);

$list->add_button(LIST_BUTTON_NEW, "New", "business_unit.php");
$list->add_button(LIST_BUTTON_FILTER, "Filter");
$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove filter");

$list->process();

$page = new Page("business_units");

$page->header();
$page->title("Business Units");
$list->render();
$page->footer();

?>
