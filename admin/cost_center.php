<?php
/********************************************************************

    cost_center.php

    Creation and mutation of cost centers.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2004-09-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-09-27
    Version:        1.0.0

    Copyright (c) 2004, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("cost_centers", "cost_center");

$form->add_section();
$form->add_edit("cost_center_name", "Cost Center", NOTNULL | UNIQUE);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("cost_centers");
$page->header();
$page->title(id() ? "Edit Cost Center" : "Add Cost Center");
$form->render();
$page->footer();

?>