<?php
/********************************************************************

    links.php

    Lists links for editing.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-03
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-11-22
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("link.php");


//compose filter_form
$topic_filter = "select distinct link_topic_id, link_topic_name " .
                "from links " . 
				"left join link_topics on link_topic_id = link_topic " . 
				"order by link_topic_name";

$form = new Form("links", "link");

$form->add_section("List Filter Selection");

$form->add_list("ltf", "Topic Filter", $topic_filter, SUBMIT | NOTNULL, param('ltf'));


//compose list

$sql = "select link_id, link_title, link_url, link_category_name, link_topic_name, " .
       "    right(link_path, length(link_path) - 13) as link_path " .
       "from links " . 
	   "left join link_categories on link_category = link_category_id ". 
	   "left join link_topics on link_topic = link_topic_id ";
	   



$list = new ListView($sql);

$list->set_entity("links");
$list->set_order("link_topic_name, link_category_name, link_title");

if(param('ltf') > 0)
{
	$list->set_filter("link_topic = " . param('ltf'));
}

$list->add_column("link_topic_name", "Topic", "", LIST_FILTER_LIST,
    "select link_topic_name from link_topic order by link_topic_name");

$list->add_column("link_category_name", "Category", "", LIST_FILTER_LIST,
    "select link_category_name from link_categories order by link_category_name");

$list->add_column("link_title", "Title", "link.php?ltf=" . param("ltf"), LIST_FILTER_FREE , "", COLUMN_NO_WRAP);

$list->add_column("link_url", "URL", "", LIST_FILTER_FREE);
$list->add_column("link_path", "File", "", LIST_FILTER_FREE);



$list->add_button(LIST_BUTTON_NEW, "New", "link.php?ltf=" . param("ltf"));

$list->process();

$page = new Page("links");

$page->header();
$page->title("Downloads & Links");
$form->render();
$list->render();
$page->footer();

?>
