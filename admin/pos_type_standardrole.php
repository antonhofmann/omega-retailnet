<?php
/********************************************************************

    pos_type_standardrole.php

    Mutation of taskwork records.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-03-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


$sql_project_managers = "select DISTINCT user_id, concat(country_name, ': ', user_name, ' ', user_firstname) as username 
	from user_roles 
	left join users on user_id = user_role_user 
	left join addresses on address_id = user_address
	left join countries on country_id = address_country 
	where user_role_role in (3, 4, 10, 60, {standardrole_rtco_user}) and user_active = 1 
	order by username ";

$form = new Form("standardroles", "POS Type Standard Role");

$form->add_section();
$form->add_list("standardrole_country", "Country",
    "select country_id, country_name from countries order by country_name", NOTNULL);
$form->add_list("standardrole_postype", "POS Type",
    "select postype_id, postype_name from postypes order by postype_name", NOTNULL);
$form->add_list("standardrole_legaltype", "Legal Type",
    "select project_costtype_id, project_costtype_text from project_costtypes where project_costtype_id IN(1, 2, 6) order by project_costtype_text");
	

$form->add_list("standardrole_rtco_user", "Project Manager", $sql_project_managers, SUBMIT);
$form->add_list("standardrole_dsup_user", "Design Supervisor",
    "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as username from user_roles left join users on user_id = user_role_user where (user_role_role = 8 or user_id = {standardrole_rtco_user}) and user_active = 1 order by username ");
$form->add_list("standardrole_rtop_user", "Retail Operator",
    "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as username from user_roles left join users on user_id = user_role_user where (user_role_role = 2 or user_id = {standardrole_rtco_user}) and user_active = 1 order by username ");
$form->add_list("standardrole_cms_approver_user", "CMS Approval by",
    "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as username from user_roles left join users on user_id = user_role_user where (user_role_role in (3, 8, 10) or user_id = {standardrole_rtco_user}) and user_active = 1 order by username ");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();
$form->process();

if($form->button("standardrole_rtco_user")) {
	$form->value("standardrole_cms_approver_user", $form->value("standardrole_rtco_user"));
}

elseif($form->button(FORM_BUTTON_SAVE) and $form->validate()) {
	$link = "pos_type_standardroles.php";
	redirect($link);
}

$page = new Page("standardroles");
$page->header();
$page->title(id() ? "Edit POS Type Standard Role" : "Add POS Type Standard Roles");
$form->render();
$page->footer();

?>