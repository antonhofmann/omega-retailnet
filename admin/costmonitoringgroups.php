<?php
/********************************************************************

    costmonitoringgroups.php

    Lists Cost Monitoring Groups

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-06
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-06
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("costmonitoringgroup.php");


$sql = "select costmonitoringgroup_id, costmonitoringgroup_code, " .
       "costmonitoringgroup_text, " .
	   "if(costmonitoringgroup_include_in_masterplan, 'yes', ' ') as costmonitoringgroup_include_in_masterplan " .
       "from costmonitoringgroups";

$list = new ListView($sql);

$list->set_entity("costmonitoringgroups");
$list->set_order("costmonitoringgroup_code");

$list->add_column("costmonitoringgroup_code", "Code", "costmonitoringgroup.php");
$list->add_column("costmonitoringgroup_text", "Description");
$list->add_column("costmonitoringgroup_include_in_masterplan", "Furniture");


$list->add_button(LIST_BUTTON_NEW, "New", "costmonitoringgroup.php");

$list->process();

$page = new Page("costmonitoringgroups");

$page->header();
$page->title("Cost Monitoring Groups for Items");
$list->render();
$page->footer();

?>
