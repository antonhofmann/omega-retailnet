<?php
/********************************************************************

    items.php

    Lists items for editing.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-05
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-08-25
    Version:        1.0.3

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_browse_catalog_in_admin");
set_referer("item.php");


//set icons for picture indicating column
$images = array();

$sql = "select item_file_item " . 
       "from item_files " .
       "where item_file_purpose = 2";

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
    $images[$row["item_file_item"]] = "/pictures/bullet_ball_glass_green.gif";
}





$sql = "select DISTINCT item_id, item_code, item_name, item_price, " .
		 "    if(item_visible, 'yes', ' ') as item_visible, " .
		 "    if(item_visible_in_production_order, 'yes', ' ') as item_visible_in_production_order, " . 
		 "    if(item_visible_in_mps, 'yes', ' ') as item_visible_in_mps, " .
		  "   if(item_stock_property_of_swatch, 'yes', ' ') as item_stock_property_of_swatch, unit_name " .
		 "from items " .
		 "left join suppliers on supplier_item = item_id " .
		 "left join units on unit_id = item_unit";

$filter = "item_active = 1 and (item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST . " or item_type <= " . ITEM_TYPE_COST_ESTIMATION . ")";

if(param("sa")) {
	$filter .= " and supplier_address = " . param("sa");
}

//get purchase price

$purchase_prices = array();
$property = array();

$sql_p = "select item_id, supplier_item_price, currency_symbol, currency_factor, " . 
         "currency_exchange_rate, item_stock_property_of_swatch " .
		 "from items " .
		 "left join suppliers on supplier_item = item_id " .
		 "left join currencies on currency_id = supplier_item_currency " . 
		 " where item_active = 1 and (item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST . " or item_type <= " . ITEM_TYPE_COST_ESTIMATION . ")";


$res = mysql_query($sql_p) or dberror($sql_p);

while ($row = mysql_fetch_assoc($res))
{
    //$factor = 1;
	//if($row["currency_factor"] > 0) { $factor =$row["currency_factor"];}
	//$purchase_prices[$row["item_id"]] = number_format($row["supplier_item_price"]*$row["currency_exchange_rate"]/$factor, 2);

	$purchase_prices[$row["item_id"]] = number_format($row["supplier_item_price"], 2) . ' ' . $row["currency_symbol"];
	
	if($row["item_stock_property_of_swatch"] == 1)
	{
		$property[$row["item_id"]] = "/pictures/omega.gif";
	}
}


/********************************************************************
    Create Form
*********************************************************************/ 
$form = new Form("items", "item");

$form->add_section("List Filter Selection");

$slq_suppliers = "select DISTINCT supplier_address, address_company from suppliers " . 
                 "left join addresses on address_id = supplier_address " . 
				 "where address_active = 1 " . 
				 " order by address_company";

if(param("sa")) {
	$form->add_list("sa", "Supplier", $slq_suppliers, SUBMIT, param("sa"));
}
else {
	$form->add_list("sa", "Supplier", $slq_suppliers, SUBMIT, 1);
}


/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);

$list->set_entity("items");
$list->set_order("item_code");
$list->set_filter($filter);

$list->add_column("item_code", "Code", "item.php?sa=" .param("sa"), LIST_FILTER_FREE, COLUMN_NO_WRAP);

$list->add_image_column("pix", "Pix", 0, $images);

$list->add_column("item_name", "Name", "", LIST_FILTER_FREE);

$list->add_column("unit_name", "Unit");

//$list->add_column("item_type_name", "Type", "", LIST_FILTER_LIST, "select item_type_name from item_types", COLUMN_NO_WRAP);

//$list->add_column("item_category_name", "Category Group", "", LIST_FILTER_LIST, "select item_category_name from item_categories", COLUMN_NO_WRAP);

$list->add_text_column("purchase_price", "Purchase \nPrice", COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $purchase_prices);

$list->add_column("item_price", "Sales \nPrice CHF", "", LIST_FILTER_FREE, "", COLUMN_BREAK | COLUMN_ALIGN_RIGHT);
$list->add_column("item_visible", "Visible", "", LIST_FILTER_LIST, array(0 => " ", 1 => "Yes"));
$list->add_column("item_visible_in_production_order", "Planning", "", LIST_FILTER_LIST, array(0 => " ", 1 => "Yes"));
$list->add_column("item_visible_in_mps", "Merchandising", "", LIST_FILTER_LIST, array(0 => " ", 1 => "Yes"));


$list->add_image_column("propterty", "Property of \n" . BRAND, COLUMN_BREAK | COLUMN_ALIGN_CENTER, $property);

if(has_access("can_edit_catalog"))
{
	$list->add_button(LIST_BUTTON_NEW, "New", "item.php?sa=" . param("sa"));
	$list->add_button(LIST_BUTTON_FILTER, "Filter");
	$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove filter");
}
else
{
	$list->add_button(LIST_BUTTON_FILTER, "Filter");
}

$list->process();

$form->populate();

$page = new Page("items");

$page->header();
$page->title("Items");
$form->render();
$list->render();
$page->footer();

?>
