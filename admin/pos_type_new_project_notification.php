<?php
/********************************************************************

    pos_type_new_project_notification.php

    Creation and editing of notification recipients.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2010-11-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2010-11-14
    Version:        1.0.1

    Copyright (c) 20010, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("projecttype_newproject_notifications", "New Project Notifications");


$form->add_section('Recipients');

$form->add_list("projecttype_newproject_notification_country", "Country",
    "select country_id, country_name from countries  " .
    "order by country_name", NOTNULL);

$form->add_list("projecttype_newproject_notification_postype", "POS Type",
    "select postype_id, postype_name from postypes " .
    "order by postype_name", NOTNULL);



$form->add_edit("projecttype_newproject_notification_email", "Email", 0);
$form->add_edit("projecttype_newproject_notification_emailcc1", "Further Email", 0);
$form->add_edit("projecttype_newproject_notification_emailcc2", "Further Email", 0);
$form->add_edit("projecttype_newproject_notification_emailcc3", "Further Email", 0);
$form->add_edit("projecttype_newproject_notification_emailcc4", "Further Email", 0);
$form->add_edit("projecttype_newproject_notification_emailcc5", "Further Email", 0);
$form->add_edit("projecttype_newproject_notification_emailcc6", "Further Email", 0);
$form->add_edit("projecttype_newproject_notification_emailcc7", "Further Email", 0);


//type of mail
$form->add_section('Mail Alerts');
$form->add_checkbox("projecttype_newproject_notification_on_new_project", "on submitting new project", 0, 0, 'Send Mail');
$form->add_checkbox("projecttype_newproject_notification_on_lnsubmission", "on submitting LN");
$form->add_checkbox("projecttype_newproject_notification_on_lnresubmission", "on resubmitting LN");
$form->add_checkbox("projecttype_newproject_notification_oncerafsubmission", "on submitting CER/AF");
$form->add_checkbox("projecttype_newproject_notification_oncerafresubmission", "on resubmitting CER/AF");
$form->add_checkbox("projecttype_newproject_notification_on_rfafsubmission", "on submitting request for additional funding");
$form->add_checkbox("projecttype_newproject_notification_on_rfafresubmission", "on resubmitting request for additional funding");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

if($form->button(FORM_BUTTON_SAVE) and $form->validate()) {
	$link = "pos_type_new_project_notifications.php";
	redirect($link);
}

$page = new Page("projecttype_newproject_notifications");

$page->header();
$page->title(id() ? "Edit Notifications for Submissions" : "Add Notifications for Submissions");
$form->render();
$page->footer();

?>
