<?php
/********************************************************************

    reset_project_numbers.php

    Reset Project Numbers (change year)

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.com)
    Date created:   2010-08-18
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.com)
    Date modified:  2010-08-18
    Version:        1.0.0

    Copyright (c) 2010, Swatch AG, All Rights Reserved.

*********************************************************************/




require_once "../include/frame.php";



check_access("can_edit_catalog");

require_once "include/admin_functions.php";

$years = array();
$years[date("Y")] = date("Y");
$years[date("Y")+1] = date("Y")+1;
$years[date("Y")+2] = date("Y")+2;


// create sql
$sql = "select distinct project_id, project_number, " .
       "if(project_real_opening_date = '0000-00-00', '', project_real_opening_date) as real_opening_date, ".
       "project_actual_opening_date, product_line_name, postype_name, " . 
       "concat(order_shop_address_place,', ', order_shop_address_company), country_name, ".
       "    concat(user_name,' ',user_firstname), ".
       "    order_id, order_actual_order_state_code, project_costtype_text,  ".
	   " project_cost_cms_completed, project_cost_cms_approved " . 
       "from projects ".
       "left join orders on project_order = order_id ".
	   "left join project_costs on project_cost_order = order_id " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
       "left join product_lines on project_product_line = product_line_id ".
       "left join postypes on postype_id = project_postype ".
       "left join countries on order_shop_address_country = countries.country_id ".
       "left join users on project_retail_coordinator = users.user_id ";

if(param("new_year")) 
{
	$new_year = substr(param("new_year"), 0,1) . substr(param("new_year"), 2,2);
	$list_filter = "order_actual_order_state_code <= '620' and left(project_number, 3) <> " . $new_year;
}
else
{
	$list_filter = "order_actual_order_state_code <= '620'";
}

$projectsnumbers = array();
$sql2 = $sql . ' where ' . $list_filter . ' order by project_number, country_name';
$res = mysql_query($sql2) or dberror($sql2);
while ($row = mysql_fetch_assoc($res))
{
	$projectsnumbers[$row["project_id"]] = $row["project_number"];
	$project_ids[] = $row["project_id"];
}


/********************************************************************
    Create Form
*********************************************************************/ 

$form0 = new Form("projects", "project");
$form0->add_label("select0", "Projects", RENDER_HTML, "<div id='select0'><a href='javascript:select_all_projects();'>select all</a></div>");
$form0->populate();


/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);

$list->set_entity("projects");
$list->set_title("Projects having a state less or equal than 620");
$list->set_order("project_number, country_name");
$list->set_filter($list_filter);   

$list->add_column("project_number", "Project No.", "", "", "", COLUMN_NO_WRAP);

$list->add_checkbox_column("change_number", "");

$list->add_column("project_costtype_text", "Legal Type", "", "", "", COLUMN_NO_WRAP);
$list->add_column("product_line_name", "Product Line", "", "", "", COLUMN_NO_WRAP);
$list->add_column("postype_name", "POS Type", "", "", "", COLUMN_NO_WRAP);
$list->add_column("order_actual_order_state_code", "Status");
$list->add_column("real_opening_date", "Agreed \nOpening Date", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK);
$list->add_column("country_name", "Country", "", "", "");
$list->add_column("concat(order_shop_address_place,', ', order_shop_address_company)", "POS Address");
$list->add_column("concat(user_name,' ',user_firstname)", "Project Manager", "", "", "");

$list->populate();
$list->process();


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project");
$form->add_hidden("form_saved", 1);

$form->add_comment("Bulk change year of project numbers!<br /><br />");
$form->add_edit("new_year", "New Year", NOTNULL);

$form->add_list("new_year", "Year*",$years, NOTNULL);


$form->add_button('change', "Reset Year");

$form->populate();
$form->process();


/********************************************************************
    Process Form
*********************************************************************/ 
if($form->button('change')) {
	
	if($form->validate())
	{
		$new_year = substr($form->value("new_year"), 0,1) . substr($form->value("new_year"), 2,2);

		foreach($list->columns[1]['values'] as $project_id=>$value) {
			
			
			if($value = 1 and array_key_exists($project_id, $projectsnumbers)) {
			
				$new_project_number = project_create_new_project_number($new_year, $project_id);
				
				
				//get project data
				$sql = "select project_number, project_order, country_name, order_shop_address_place, " . 
					   "order_shop_address_company, project_retail_coordinator, order_user, " .
					   "project_design_supervisor, project_design_contractor, order_actual_order_state_code, order_retail_operator " .
					   "from projects " . 
					   "left join orders on order_id = project_order " . 
					   "left join countries on country_id = order_shop_address_country " .  
					   "where project_id = " . dbquote($project_id);

				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);

				$old_project_number = $row['project_number'];
				$order_id = $row['project_order'];

				$country_name = $row['country_name'];
				$place_name = $row['order_shop_address_place'];
				$company_name = $row['order_shop_address_company'];
				$retail_coordinator = $row['project_retail_coordinator'];
				$retail_operator = $row['order_retail_operator'];
				$client = $row['order_user'];
				$design_contractor = $row['project_design_contractor'];
				$design_supervisor = $row['project_design_supervisor'];
				$order_state_code = $row['order_actual_order_state_code'];


				
				
				
				//update projects
				$sql = 'update projects set project_number = ' . dbquote($new_project_number) .
					   ' where project_id = ' . dbquote($project_id);

				$result = mysql_query($sql) or dberror($sql);


				//update table old project numbers
				$sql = "Insert into oldproject_numbers (".
					   "oldproject_number_project_id, oldproject_number_user_id, " . 
					   "oldproject_number_old_number, oldproject_number_new_number, " . 
					   "user_created, date_created) VALUES (" .
					   $project_id . ', ' .
					   user_id() . ', ' .
					   dbquote($old_project_number) . ', ' .
					   dbquote($new_project_number) . ', ' .
					   dbquote(user_login()) . ', ' .
					   dbquote(date("Y-m-d h:i:s")) . ')';
				$res = mysql_query($sql) or dberror($sql);  

				//update orders
				$sql = 'update orders set order_number = ' . dbquote($new_project_number) .
					   ' where order_id = ' . dbquote($order_id);

				$result = mysql_query($sql) or dberror($sql);

				//update posordespipeline
				$sql = 'update posorderspipeline set posorder_ordernumber = ' . dbquote($new_project_number) .
					   ' where posorder_order = ' . dbquote($order_id);

				$result = mysql_query($sql) or dberror($sql);

				//update posorders
				$sql = 'update posorders set posorder_ordernumber = ' . dbquote($new_project_number) .
					   ' where posorder_order = ' . dbquote($order_id);

				$result = mysql_query($sql) or dberror($sql);

				//send email notifications on change of project number
				$sql = "select user_id, user_email, concat(user_name, ' ', user_firstname) as username ".
					   "from users ".
					   "where (user_id = '" . user_id() . "' " . 
					   "   and user_active = 1)";
				
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				
				$sender_id = $row["user_id"];
				$sender_email = $row["user_email"];
				$sender_name = $row["username"];

				//recipients
				$subject = "Project " . $old_project_number . "/" . $new_project_number . ", " . $country_name . ", " . $place_name . ", " . $company_name. ": Project Number was changed.";


				$bodytext0 = $sender_name . " has changed the project number from " . $old_project_number . " to " . $new_project_number . ".";
				$link ="project_view_client_data.php?pid=" . $project_id;
				$bodytext = $bodytext0 . "\nClick below to have direct access to the project:\n";
				$bodytext = $bodytext .  APPLICATION_URL ."/user/" . $link . "\n\n";   

				//project manager
				$reciepients = array();
				$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
					   "from users ".
					   "where (user_id = " . dbquote($retail_coordinator) . 
					   "   and user_active = 1)";
				
				$res1 = mysql_query($sql) or dberror($sql);
				if ($row1 = mysql_fetch_assoc($res1))
				{
					$reciepients[$row1["user_id"]] = strtolower($row1["user_email"]);
					$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
					$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
				}


				//retail operator
				$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
					   "from users ".
					   "where (user_id = " . dbquote($retail_operator) . 
					   "   and user_active = 1)";
				
				$res1 = mysql_query($sql) or dberror($sql);
				if ($row1 = mysql_fetch_assoc($res1))
				{
					$reciepients[$row1["user_id"]] = strtolower($row1["user_email"]);
					$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
					$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
				}


				//design_contractor
				if($design_contractor and $order_state_code < '620') {
					$send_mail = true;
					$project_state_restrictions = get_project_state_restrictions($design_contractor);

				
					if($project_state_restrictions['from_state'] or $project_state_restrictions['to_state'])
					{
						if($project_state_restrictions['from_state'] and $order_state_code < $project_state_restrictions['from_state'])
						{
							$send_mail = false;
						}
						if($project_state_restrictions['to_state'] and $order_state_code > $project_state_restrictions['to_state'])
						{
							$send_mail = false;
						}
					}
					

					if($send_mail == true)
					{
						$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
							   "from users ".
							   "where (user_id = " . dbquote($design_contractor) . 
							   "   and user_active = 1)";
						
						$res1 = mysql_query($sql) or dberror($sql);
						if ($row1 = mysql_fetch_assoc($res1))
						{
							$reciepients[$row1["user_id"]] = strtolower($row1["user_email"]);
							$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
							$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
						}
					}
				}


				//design_supervisor
				if($design_supervisor) {
					$send_mail = true;
					$project_state_restrictions = get_project_state_restrictions($design_supervisor);

				
					if($project_state_restrictions['from_state'] or $project_state_restrictions['to_state'])
					{
						if($project_state_restrictions['from_state'] and $order_state_code < $project_state_restrictions['from_state'])
						{
							$send_mail = false;
						}
						if($project_state_restrictions['to_state'] and $order_state_code > $project_state_restrictions['to_state'])
						{
							$send_mail = false;
						}
					}
					

					if($send_mail == true)
					{
						$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
							   "from users ".
							   "where (user_id = " . dbquote($design_supervisor) . 
							   "   and user_active = 1)";
						
						$res1 = mysql_query($sql) or dberror($sql);
						if ($row1 = mysql_fetch_assoc($res1))
						{
							$reciepients[$row1["user_id"]] = strtolower($row1["user_email"]);
							$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
							$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
						}
					}
				}


				//client
				$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
					   "from users ".
					   "where (user_id = " . dbquote($client) . 
					   "   and user_active = 1)";
				
				$res1 = mysql_query($sql) or dberror($sql);
				if ($row1 = mysql_fetch_assoc($res1))
				{
					$reciepients[$row1["user_id"]] = strtolower($row1["user_email"]);
					$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
					$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
				}


				//retail supervising team
				$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
					   "from supervisingteam " . 
					   "left join users on user_id = supervisingteam_user ".
					   "where user_active = 1";
				
				$res1 = mysql_query($sql) or dberror($sql);
				while ($row1 = mysql_fetch_assoc($res1))
				{
					$reciepients[$row1["user_id"]] = strtolower($row1["user_email"]);
					$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
					$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
				}
					
				$result = 0;
				foreach($reciepients as $user_id=>$user_email) {
					
					$mail = new Mail();
					$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
					$mail->set_sender($sender_email, $sender_name);
					$mail->add_text($bodytext);
					$mail->add_recipient($user_email);

					$result = $mail->send();
				}

				if($result == 1)
				{
					foreach($reciepients as $user_id=>$user_email) {
						append_mail($order_id, $user_id, $sender_id, $bodytext0, "", 1);
					}
				}
			}

		}

		$form0->message("The operation was executed sucessfully.");
	}
}

$page = new Page("reset_project_numbers");
$page->header();
$page->title("Reset Project Numbers");



$form0->render();



$list->render();

echo '<br /><br />';
$form->render();

?>
<script language='javascript'>

	function select_all_projects()
	{
		<?php
		foreach($project_ids as $key=>$project_id)
		{
			echo "document.getElementById('__projects_change_number_" . $project_id . "').checked = true;";
		}
		?>

		var div = document.getElementById('select0');
		div.innerHTML = "<a href='javascript:deselect_all_projects();'>deselect all</a>";
		
		
	}

	function deselect_all_projects()
	{
		<?php
		foreach($project_ids as $key=>$project_id)
		{
			echo "document.getElementById('__projects_change_number_" . $project_id . "').checked = false;";
		}
		?>

		var div = document.getElementById('select0');
		div.innerHTML = "<a href='javascript:select_all_projects();'>select all</a>";
	}
</script>

<?php
$page->footer();

?>