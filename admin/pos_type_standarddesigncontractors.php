<?php
/********************************************************************

    pos_type_standarddesigncontractors.php

    Lists standardroles for editing.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2010-11-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2010-11-14
    Version:        1.0.0

    Copyright (c) 2010, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("pos_type_standarddesigncontractor.php");

$sql = "select standarddesigncontractor_id, product_line_name, postype_name,  " .
       " concat(address_company, ': ', user_name, ' ', user_firstname) as dcon " .
       "from standarddesigncontractors " .
	   "left join product_lines on product_line_id = standarddesigncontractor_productline " . 
	   "left join postypes on postype_id = standarddesigncontractor_postype " . 
	   "left join users on user_id = standarddesigncontractor_dcon_user " . 
	   "left join addresses on address_id = user_address ";

$list = new ListView($sql);

$list->set_entity("standarddesigncontractors");
$list->set_order("product_line_name, postype_name");

$list->add_column("product_line_name", "Product Line", "pos_type_standarddesigncontractor.php");
$list->add_column("postype_name", "POS Type");
$list->add_column("dcon", "Design Contractor");

$list->add_button(LIST_BUTTON_NEW, "New", "pos_type_standarddesigncontractor.php");


$list->process();

$page = new Page("standarddesigncontractors");

$page->header();
$page->title("Standard Design Contractors");
$list->render();
$page->footer();

?>
