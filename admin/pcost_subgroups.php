<?php
/********************************************************************

    pcost_subgroups.php

    Lists project cost sub groups for editing .

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2004-09-27
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2014, OMEGA SA, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("pcost_subgroup.php");

$sql = "select pcost_subgroup_id, pcost_subgroup_code, pcost_subgroup_name, " . 
       "concat(pcost_group_code, ' ' , pcost_group_name) as gname " .
       "from pcost_subgroups " .
	   "left join pcost_groups on pcost_group_id = pcost_subgroup_pcostgroup_id ";

$list = new ListView($sql);

$list->set_entity("pcost_subgroups");
$list->set_order("pcost_subgroup_code");
$list->set_group("gname");

$list->add_column("pcost_subgroup_code", "Code", "pcost_subgroup.php");
$list->add_column("pcost_subgroup_name", "Name");

$list->add_button(LIST_BUTTON_NEW, "New", "pcost_subgroup.php");

$list->process();

$page = new Page("pcost_subgroups");

$page->header();
$page->title("Project Cost Subgroups");
$list->render();
$page->footer();

?>
