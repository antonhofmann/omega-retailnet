<?php
/********************************************************************

    invoice_addresses.php

    Lists invoice_addresses for editing.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-02-11
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-02-11
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("invoice_addresses.php");


$sql = "select DISTINCT " . 
       "concat(order_billing_address_company, '@@', order_billing_address_address, '@@', order_billing_address_zip, '@@', order_billing_address_place, '@@', order_billing_address_contact, '@@', order_client_address) as okey, " . 
	   "address_shortcut, order_billing_address_company, order_billing_address_address, order_billing_address_zip, " . 
	   "order_billing_address_place,country_name, order_billing_address_contact " .
	   "from orders " . 
	   "left join countries on order_billing_address_country = country_id " . 
	   "left join addresses on address_id = order_client_address";


$sql_o = $sql . " where order_billing_address_active = 1" .
                " order by order_client_address, order_billing_address_company";

$res_o = mysql_query($sql_o) or dberror($sql_o);

$inactive_addresses = array();
$addresses = array();

while($row_o = mysql_fetch_assoc($res_o))
{
	$inactive_addresses[$row_o["okey"]] = 0;
	
	$key = str_replace(" ", "_", $row_o["okey"]);
	$key = "__orders_ai_" . str_replace(".", "_", $key);
	$addresses[$key] = $row_o["okey"];
}


$list = new ListView($sql);

$list->set_entity("orders");
$list->set_filter("order_billing_address_active = 1");
$list->set_order("address_shortcut, order_billing_address_company");

$list->add_column("address_shortcut", "Shortcut", "", LIST_FILTER_FREE);
$list->add_checkbox_column("ai", "", 0, $inactive_addresses);
$list->add_column("order_billing_address_company", "Company", "", LIST_FILTER_FREE);
$list->add_column("order_billing_address_contact", "Contact", "", LIST_FILTER_FREE);
$list->add_column("order_billing_address_address", "Address", "", LIST_FILTER_FREE);
$list->add_column("order_billing_address_zip", "Zip", "", LIST_FILTER_FREE);
$list->add_column("order_billing_address_place", "City", "", LIST_FILTER_FREE);
$list->add_column("country_name", "Country", "", LIST_FILTER_LIST, "select country_name from countries order by country_name");
$list->add_hidden("okey", "City", "okey");
$list->add_button("save", "Set Selected to Inactive");
$list->process();

if ($list->button("save"))
{
	foreach($_POST as $key=>$value)
    {
		
		if($value == 1)
		{
			
			$key = str_replace("__orders_ai_", "", $addresses[$key]);
			//echo $key . "<br />";
			
			$fields = array();
			$fields = explode("@@", $key);	
			
			$filter = "";
			$filter = " where order_billing_address_company = \"" . $fields[0] . "\" " .
			$filter = $filter . " and order_billing_address_address = \"" . $fields[1] . "\" " .
			$filter = $filter . " and order_billing_address_zip = \"" . $fields[2] . "\" " .
			$filter = $filter . " and order_billing_address_place = \"" . $fields[3] . "\" " .
			$filter = $filter . " and order_billing_address_contact = \"" . $fields[4] . "\" " .
			$filter = $filter . " and order_client_address = \"" . $fields[5] . "\" ";
			
			$sql_u = "Update orders set order_billing_address_active = 0 " . $filter;
			//echo $sql_u;
			$res = mysql_query($sql_u) or dberror($sql_u);

		}
    }
}

$page = new Page("orders");

$page->header();
$page->title("Invoice Addresses");
$list->render();
$page->footer();

?>
