<?php
/********************************************************************

    project_budget_approvals.php

    Lists budget approvals for editing.

    Created by:     Oliver Hofer (oliver.hofer@pageagent.com)
    Date created:   2003-05-06
    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date modified:  2003-06-24
    Version:        1.0.3

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

set_referer("project_budget_approval.php");

$sql = "select budget_approval_id, budget_approval_user, budget_approval_priority, " .
       "    budget_approval_budget_low, budget_approval_budget_high " .
       "from budget_approvals ";

$list = new ListView($sql);
$list->set_entity("budget_approvals");
$list->set_sorter("budget_approval_priority");
$list->set_order("budget_approval_priority desc");

$list->add_column("budget_approval_user", "Name", "project_budget_approval.php");
$list->add_column("budget_approval_budget_low", "Approval required if Budget greater than", 
                  "" , "", "", COLUMN_ALIGN_RIGHT);
$list->add_button(LIST_BUTTON_NEW, "New", "project_budget_approval.php");

$list->process();

$page = new Page("project_budget_approvals");

$page->header();
$page->title("Budget Approval");
$list->render();
$page->footer();

?>
