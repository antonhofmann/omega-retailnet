<?php
/********************************************************************

    mailalert.php

    Creation and editing of mail alerts

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2010-03-19
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2010-03-19
    Version:        1.0.0

    Copyright (c) 2010, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("mail_alert_types", "Mail Alert Types");


$form->add_section();

$form->add_label("mail_alert_type_shortcut", "Shortcut");
$form->add_label("mail_alert_type_text", "Description");
$form->add_label("mail_alert_type_recipients", "Recipients");
$form->add_section();

if(id() == 19 or id() == 10 or id() == 21)
{
	$form->add_hidden("mail_alert_type_sender_email");
	$form->add_hidden("mail_alert_type_sender_name");
}
else
{
	$form->add_edit("mail_alert_type_sender_email", "Sender Mail*", NOTNULL);
	$form->add_edit("mail_alert_type_sender_name", "Sender Name", NOTNULL);
}
$form->add_edit("mail_alert_type_cc1", "CC 1", 0);
$form->add_edit("mail_alert_type_cc2", "CC 2", 0);
$form->add_edit("mail_alert_type_cc3", "CC 3", 0);
$form->add_edit("mail_alert_type_cc4", "CC 4", 0);

$form->add_section("Mail Text");
$form->add_multiline("mail_alert_mail_text", "Mail Body", 12);


$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button("back", "Back");

$form->populate();
$form->process();

if($form->button("back"))
{
	redirect("mailalerts.php");
}

$page = new Page("mail_alert_types");

$page->header();
$page->title(id() ? "Edit Mail Alerts" : "Add Mail Alertt");
$form->render();
$page->footer();

?>
