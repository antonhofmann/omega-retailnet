<?php
/********************************************************************

    item.php

    Creation and mutation of item records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-29
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-08-04
    Version:        1.2.3

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_browse_catalog_in_admin");
set_referer("item_parts.php");
set_referer("addon.php");
set_referer("item_file.php");
set_referer("item_supplier.php");
set_referer("item_regions.php");
set_referer("item_store.php");
set_referer("item_itempart.php");
set_referer("item_option.php");
set_referer("item_composition.php");

$sql_regions = 'select region_id, region_name from regions order by region_name';
$regions = array();
$res = mysql_query($sql_regions) or dberror($sql_regions);
while ($row = mysql_fetch_assoc($res))
{
	$regions[] = $row["region_id"];
}


//get suppliers of the item
if(id()) {
	$supplier_address = 0;
	$sql = "select supplier_address from suppliers " . 
		   "where supplier_item = " . id();

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$supplier_address = $row["supplier_address"];
	}

	//get supplier of the item
	$sql_slave_items = "select item_id, concat(item_code, ' - ', item_name) as name " . 
						"from suppliers " .
						" left join items on item_id = supplier_item " . 
						" where item_type = 1 and item_active = 1 " .
						" and supplier_address = " . $supplier_address .
						" and item_id <> " . id() . 
						" order by item_code";

}


$sql_units = "select unit_id, unit_name from units order by unit_name";

// Build form

$form = new Form("items", "item");

$form->add_hidden("sa", param("sa"));

$form->add_section("Description");
$form->add_edit("item_code", "Code", NOTNULL | UNIQUE);
$form->add_edit("item_name", "Name", NOTNULL);
$form->add_list("item_type", "Type",
    "select item_type_id, item_type_name from item_types " .
    "where item_type_id = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST . 
	" or item_type_id <= " . ITEM_TYPE_COST_ESTIMATION . 
    " order by item_type_id", NOTNULL);

$form->add_list("item_category", "Item Category",
    "select item_category_id, item_category_name from item_categories " .
    "order by item_category_name", NOTNULL);



$form->add_edit("item_price", "Price " . get_system_currency_symbol());

$form->add_section();
$form->add_multiline("item_description", "Description", 6);

$form->add_section("Cost Monitoring");
$form->add_list("item_cost_group", "Cost Group",
    "select project_cost_groupname_id, project_cost_groupname_name from project_cost_groupnames " .
	"where  project_cost_groupname_active = 1 " . 
    "order by project_cost_groupname_name", NOTNULL);

$form->add_list("item_costmonitoring_group", "Cost Monitoring Group",
    "select costmonitoringgroup_id, costmonitoringgroup_text from costmonitoringgroups " .
    "order by costmonitoringgroup_text", NOTNULL);


if(id()) {
	$form->add_section("Logistic Dependency");
	$form->add_comment("As soon as a supplier enters a ready for pick up date for the item the system will mark the slave item as delivered. This is used for example in the context of a service coming up with a furniture. The furniture will be the master item in this case, the service will be the slave item.");

	$form->add_list("item_slave_item", "Slave item", $sql_slave_items, 0);
}

$form->add_section("Specification");
$form->add_edit("item_watches_displayed", "Watches Displayed");
$form->add_edit("item_watches_stored", "Watches Stored");

$form->add_section();
$form->add_multiline("item_packed_size", "Packed Size", 4);
$form->add_multiline("item_packed_weight", "Packed Weight", 4);

$form->add_section();
$form->add_multiline("item_materials", "Materials", 4);
$form->add_multiline("item_electrical_specifications", "Electrical Specifications", 4);
$form->add_multiline("item_lights_used", "Lights Used", 4);
$form->add_multiline("item_regulatory_approvals", "Regulatory Approvals", 4);

$form->add_section();
$form->add_multiline("item_install_requirements", "Install Requirements", 6);

$form->add_list("item_unit", "Unit of measure", $sql_units, 0);
$form->add_edit("item_width", "Width in mm");
$form->add_edit("item_height", "Height in mm");
$form->add_edit("item_length", "Length in mm");
$form->add_edit("item_radius", "Radius in mm");

$form->add_section("Stock Property");
$form->add_checkbox("item_stock_property_of_swatch", "is property of " . BRAND, 0, 0, "Stock");


$form->add_section("Regions");
$form->add_label("select0", "Regions", RENDER_HTML, "<div id='select0'><a href='javascript:select_all_regions();'>select all regions</a></div>");
$form->add_checklist("item_regions", "", "item_regions",
    "select region_id, region_name from regions order by region_name");

$form->add_section("Other");
$form->add_checkbox("item_visible", "Visible in Catalog");
$form->add_checkbox("item_visible_in_production_order", "Visible in Production Order Planning");
$form->add_checkbox("item_price_adjustable", "Price Adjustable");
$form->add_checkbox("item_active", "Active", true);

/*
$form->add_section("Merchandising Planning");
$form->add_checkbox("item_visible_in_mps", "Visible in Merchandising Planning");
$form->add_checkbox("item_addable_in_mps", "Can be added in the POS's furniture list in Merchandising Planning");
*/

$form->add_subtable("suppliers", "<br />Suppliers", "item_supplier.php?sa=" . param("sa") . "&item=" . id(), "Code",
    "select supplier_id, supplier_item_code as Code, supplier_item_name as Name, " .
    "    currency_symbol as Currency, supplier_item_price as Price, address_company as Supplier " .
    "from suppliers left join addresses on supplier_address = address_id " .
	"left join currencies on currency_id = address_currency " . 
    "where supplier_item = " . id() . " " .
    "order by supplier_item_code");

/*$form->add_subtable("stores", "Stock Locations", "item_store.php", "Company",
    "select store_id, address_company as Company, address_address as Address, " .
    "    address_zip as Zip, address_place as Place, country_name as Country, " .
    "    store_global_order as 'Global Order' " .
    "from stores left join addresses on store_address = address_id " .
    "    left join countries on address_country = country_id " .
    "where store_item = " . id() . " " .
    "order by address_company, address_country, address_place");*/

if(has_access("can_edit_catalog"))
{
	$form->add_subtable("item_options", "<br />Options", "item_option.php?sa=" . param("sa"), "Code",
		"select item_option_id, item_option_name as Opname, item_option_quantity as Quantity, item_code as Code,  item_name as Name " .
		"from item_options left join items on item_option_child = item_id " .
		"where item_option_parent = " . id() . " " .
		"order by item_code", "", "Remove");


	$form->add_subtable("item_compositions", "<br />Composite Groups", "", "Code",
		"select item_composition_id, item_group_name as Name " .
		"from item_compositions left join item_groups on item_composition_group = item_group_id " .
		"where item_composition_item = " . id() . " " .
		"order by item_group_name", "", "Remove");


	$form->add_subtable("parts", "<br />Spare Parts", "item.php?sa=" . param("sa"), "Code",
		"select part_id, item_id, item_code as Code, item_name as Name, item_price as Price, " .
		"    item_type_name as Type " .
		"from parts left join items on part_child = item_id " .
		"    left join item_types on item_type = item_type_id " .
		"where part_parent = " . id() . " " .
		"order by item_code", "", "Remove");

	$form->add_subtable("addons", "<br />Addons", "addon.php?sa=" . param("sa"), "Code",
		"select addon_id, item_code as Code, item_name as Name, addon_package_quantity as 'Qty', " .
		"    addon_min_packages as 'Min', addon_max_packages as 'Max' " .
		"from addons left join items on addon_child = item_id " .
		"    left join item_types on item_type = item_type_id " .
		"where addon_parent = " . id() . " " .
		"order by item_code", "", "Remove");


	$form->add_subtable("files", "<br />Files", "item_file.php?sa=" . param("sa") . "&item=" . id(), "Title",
		"select item_file_id, item_file_title as Title, file_purpose_name as Purpose, " .
		"    file_type_name as Type, item_file_path as Path " .
		"from item_files left join file_types on item_file_type = file_type_id " .
		"    left join file_purposes on item_file_purpose = file_purpose_id " .
		"where item_file_item = " . id() . " " .
		"order by item_file_title");

	$form->add_button(FORM_BUTTON_SAVE, "Save");
	$form->add_button("back", "Back");
	$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);
	$form->add_button("add_supplier", "Add Supplier", "item_supplier.php?sa=" . param("sa") ."&item=" . id(), OPTIONAL);
	//$form->add_button("add_store", "Add Stock Location", "item_store.php?item=" . id(), OPTIONAL);

	$form->add_button("options", "Add Option", "item_option.php?sa=" . param("sa") . "&item=" . id(), OPTIONAL);
	$form->add_button("composition", "Edit Composition", "item_composition.php?sa=" . param("sa") . "&item=" . id(), OPTIONAL);
	$form->add_button("assign_parts", "Add Spare Parts", "item_parts.php?sa=" . param("sa") . "&item=" . id(), OPTIONAL);
	$form->add_button("add_addon", "Add Addon", "addon.php?sa=" . param("sa") . "&item=" . id(), OPTIONAL);
	$form->add_button("add_file", "Add File", "item_file.php?sa=" . param("sa") . "&item=" . id(), OPTIONAL);
}
else
{
	$form->add_subtable("item_options", "<br />Options", "", "Code",
		"select item_option_id, item_option_name as Opname, item_option_quantity as Quantity, item_code as Code,  item_name as Name " .
		"from item_options left join items on item_option_child = item_id " .
		"where item_option_parent = " . id() . " " .
		"order by item_code");


	$form->add_subtable("item_compositions", "<br />Composite Groups", "", "Code",
		"select item_composition_id, item_group_name as Name " .
		"from item_compositions left join item_groups on item_composition_group = item_group_id " .
		"where item_composition_item = " . id() . " " .
		"order by item_group_name");


	$form->add_subtable("parts", "<br />Spare Parts", "item.php?sa=" . param("sa"), "Code",
		"select part_id, item_id, item_code as Code, item_name as Name, item_price as Price, " .
		"    item_type_name as Type " .
		"from parts left join items on part_child = item_id " .
		"    left join item_types on item_type = item_type_id " .
		"where part_parent = " . id() . " " .
		"order by item_code");

	$form->add_subtable("addons", "<br />Addons", "addon.php?sa=" . param("sa"), "Code",
		"select addon_id, item_code as Code, item_name as Name, addon_package_quantity as 'Qty', " .
		"    addon_min_packages as 'Min', addon_max_packages as 'Max' " .
		"from addons left join items on addon_child = item_id " .
		"    left join item_types on item_type = item_type_id " .
		"where addon_parent = " . id() . " " .
		"order by item_code");


	$form->add_button("back", "Back");
}
// Populate form and process button clicks

$form->populate();
$form->process();

// delete all items from the categrory context if inactive
if ($form->button(FORM_BUTTON_SAVE))
{

    $sql = "DELETE FROM category_items USING category_items, items WHERE category_item_item = item_id and item_active = 0";

    $res = mysql_query($sql);

}
elseif ($form->button("back"))
{
	redirect("items.php?sa=" . param("sa"));
}

// Render page

$page = new Page("items");

$page->header();

if(has_access("can_edit_catalog"))
{
	$page->title(id() ? "Edit Item" : "Add Item");
}
else
{
	$page->title("View Item");
}
$form->render();

?>


<script language='javascript'>

	function select_all_regions()
	{
		<?php
		foreach($regions as $key=>$region_id)
		{
			echo "document.getElementById('item_regions_" . $region_id . "').checked = true;";
		}
		?>

		var div = document.getElementById('select0');
		div.innerHTML = "<a href='javascript:deselect_all_regions();'>deselect all regions</a>";
		
		
	}

	function deselect_all_regions()
	{
		<?php
		foreach($regions as $key=>$region_id)
		{
			echo "document.getElementById('item_regions_" . $region_id . "').checked = false;";
		}
		?>

		var div = document.getElementById('select0');
		div.innerHTML = "<a href='javascript:select_all_regions();'>select all regions</a>";
	}

</script>

<?php

$page->footer();
?>