<?php
/********************************************************************

    currencies.php

    Lists currencies for editing.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-31
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-10-13
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/admin_functions.php";

check_access("can_edit_catalog");
set_referer("currency.php");


//get exchange rates from swatch group
$client = '';
try {
    //$client = new SoapClient("http://its-sgsservices.swatchgroup.net:8082/CurrencyService.svc?singleWsdl",array("trace"=>true,      "features"=>SOAP_SINGLE_ELEMENT_ARRAYS));

	//$client = new SoapClient("http://10.139.11.207:8082/CurrencyService.svc?singleWsdl",array("trace"=>true,      "features"=>SOAP_SINGLE_ELEMENT_ARRAYS));

	$client = new SoapClient("http://sgr-sgsservices.swatchgroup.net/CurrencyService.svc?singleWsdl",array("trace"=>true,      "features"=>SOAP_SINGLE_ELEMENT_ARRAYS));

} catch (SoapFault $fault) {
   
}

$exchange_rates = array();
$exchange_rates2 = array();
$exchange_rates_swatch_group = array();

$sql = "select currency_id, currency_name, currency_symbol, currency_currency_symbol, currency_exchange_rate,  currency_factor " .
                     "from currencies";

$list_filter = "currency_istop = 1";

$res = mysql_query($sql . ' where ' . $list_filter) or dberror($sql . ' where ' . $list_filter);
while ($row = mysql_fetch_assoc($res))
{
	$exchange_rates[$row['currency_id']] = $row['currency_exchange_rate'];

	if($client != '') {
		$tmp = $client->GetLastRate(array("currencyCode"=>$row["currency_symbol"]));
		
		if($tmp->GetLastRateResult->Base > 0)
		{
			
			$exchange_rates_swatch_group[$row['currency_id']] = 1*$row['currency_factor']*$tmp->GetLastRateResult->ClosingRate/$tmp->GetLastRateResult->Base;


			if(floatval(round($exchange_rates[$row['currency_id']], 6)) != floatval(round($exchange_rates_swatch_group[$row['currency_id']], 6)))
			{
			
				//echo $exchange_rates[$row['currency_id']] . "->" . $exchange_rates_swatch_group[$row['currency_id']] . "->" . floatval($exchange_rates[$row['currency_id']]) . "->" . floatval($exchange_rates_swatch_group[$row['currency_id']]) . "-><br />";
				$exchange_rates_swatch_group[$row['currency_id']] = '<span class="red">' . $exchange_rates_swatch_group[$row['currency_id']] . '</span>';
			}
		}
		else
		{
			$exchange_rates_swatch_group[$row['currency_id']] = '<span class="red">n.a.</span>';
		}
	}
	else {
		$exchange_rates_swatch_group[$row['currency_id']] = '<span class="red">n.a.</span>';
	}
}

$countries = array();
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{

	$sqlc = 'select country_name from countries where country_currency = ' . $row['currency_id']  . 
		   ' order by country_name';

	$resc = mysql_query($sqlc) or dberror($sqlc);
	while ($rowc = mysql_fetch_assoc($resc))
	{
		if(array_key_exists($row['currency_id'], $countries)) {
			$countries[$row['currency_id']] = $countries[$row['currency_id']] . ', ' . $rowc['country_name'];
		}
		else
		{
			$countries[$row['currency_id']] = $rowc['country_name'];
		}
	}

}

$form = new Form("currencies", "currency");
$form->populate();


$list1 = new ListView($sql);

$list1->set_entity("currencies");
$list1->set_order("currency_symbol");
$list1->set_filter($list_filter);



$list1->add_column("currency_name", "Name");
$list1->add_column("currency_symbol", "Code", "currency.php", LIST_FILTER_FREE);
$list1->add_column("currency_currency_symbol", "Symbol");


$list1->add_text_column("currency_exchange_rate_swatch_group", "Exchange Rate<br />Swatch Group", COLUMN_UNDERSTAND_HTML, $exchange_rates_swatch_group);


$list1->add_edit_column("currency_exchange_rate", "Exchange Rate Clients", 8, $flags = 0, $exchange_rates);
//$list1->add_column("currency_exchange_rate", "Exchange Rate", "", LIST_FILTER_FREE);
$list1->add_column("currency_factor", "Factor", "", LIST_FILTER_FREE);
$list1->add_text_column("country2", "Countries", 0, $countries);

$list1->add_button("update_from_swatch_group", "Update Exchange Rates from Swatch Group");
$list1->add_button(LIST_BUTTON_NEW, "New", "currency.php");
$list1->add_button('save', "Save");



$list2 = new ListView("select currency_id, currency_name, currency_symbol, currency_currency_symbol, currency_exchange_rate, currency_factor " .
                    "from currencies");

$list2->set_entity("currencies");
$list2->set_order("currency_symbol");
$list2->set_filter("currency_istop <> 1");

$list2->add_column("currency_name", "Name");
$list2->add_column("currency_symbol", "Symbol", "currency.php");
$list2->add_column("currency_currency_symbol", "Symbol");
$list2->add_text_column("currency_exchange_rate_swatch_group", "Exchange Rate Swatch Group", COLUMN_UNDERSTAND_HTML, $exchange_rates_swatch_group);
$list2->add_column("currency_exchange_rate", "Exchange Rate Client");
$list2->add_column("currency_factor", "Factor");
$list2->add_text_column("country2", "Countries", 0, $countries);


$list2->add_button(LIST_BUTTON_NEW, "New", "currency.php");



$list1->process();
$list1->populate();
$list2->process();

if($list1->button('save')) {

	foreach ($list1->values("currency_exchange_rate") as $key=>$value)
    {
        if(!is_decimal_value($value, 10, 6)) {
			$form->error("The list contains invalid input values.");
		}
		else
		{
		
			// update record
			if(!$value) {$value = 0;}
			$fields = array();

			$fields[] = "currency_exchange_rate = " . dbquote($value);
			$sql = "update currencies set " . join(", ", $fields) . " where currency_id = " . $key;
			mysql_query($sql) or dberror($sql);
		}
    }

	

	//update all items and onging orders and projects with new prices
	//$result = update_new_prices();

}
if($list1->button('update_from_swatch_group')) {
	
	foreach($exchange_rates_swatch_group as $currency_id=>$exchange_rate)
	{
		$exchange_rate = str_replace('<span class="red">', '', $exchange_rate);
		$exchange_rate = str_replace('</span>', '', $exchange_rate);
		

		if(is_numeric($exchange_rate) and $exchange_rate > 0)
		{
			$fields = array();

			$fields[] = "currency_exchange_rate = " . dbquote($exchange_rate);
			$sql = "update currencies set " . join(", ", $fields) . " where currency_id = " . $currency_id;
			mysql_query($sql) or dberror($sql);

			

		}

		
	}

	//update all items and onging orders and projects with new prices
	//$result = update_new_prices();

	$link = "currencies.php";
	redirect($link);
}


$page = new Page("currencies");

$page->header();
$page->title("Currencies");

$form->render();

$list1->render();
echo '<p>&nbsp;</p>';
$list2->render();
$page->footer();

?>
