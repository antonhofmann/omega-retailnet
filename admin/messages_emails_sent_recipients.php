<?php
/********************************************************************

    messages_emails_sent_recipients.php

    Lists recipients of a message.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-05-09
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-05-09
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


/********************************************************************
    prepare all data needed
*********************************************************************/

$sql = "select DISTINCT message_title, message_email_sent.date_created as date, " .
       "concat(user_name, ' ', user_firstname) as username, " .
       "user_email " .
       "from messages " .
       "left join message_email_sent on message_email_sent_message = message_id " .
       "left join users on user_id = message_email_sent_touser ";


/********************************************************************
   create form
*********************************************************************/

$form = new Form("messages", "message");

$form->add_section();
$form->add_label("message_title", "Title");
$form->add_label("message_date", "Date");
$form->add_label("message_expiry_date", "Expiry Date");

$form->add_section();
$form->add_label("message_text", "Content");

$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();
$form->process();

/********************************************************************
   create list
*********************************************************************/


$list = new ListView($sql);

$list->set_entity("messages");
$list->set_order("username");
$list->set_filter("message_id = " . id() . " and user_email <> ''");

$list->add_column("username", "Recipient", "", LIST_FILTER_FREE);
$list->add_column("user_email", "Email", "", LIST_FILTER_FREE);
$list->add_column("date", "Date", "", LIST_FILTER_FREE);

$list->process();

$page = new Page("messages");

$page->header();
$page->title("Messages - Emails sent");

$form->render();
$list->render();
$page->footer();

?>
