<?php
/********************************************************************

    replace_user.php

    replaces a user by another

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-06
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-06
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

// Build form

$form = new Form("users", "user");

$form->add_list("user_old", "Old User",
    "select user_id, concat(user_name, ' ', user_firstname, ', ', address_company) as user_fullname from users left join addresses on address_id = user_address where user_name is not null order by user_name, address_company");

$form->add_list("user_new", "News User",
    "select user_id, concat(user_name, ' ', user_firstname, ', ', address_company) as user_fullname from users left join addresses on address_id = user_address where user_name is not null order by user_name, address_company");

$form->add_button(FORM_BUTTON_SAVE, "Perform the Replacement");

$form->populate();
$form->process();

if ($form->button(FORM_BUTTON_SAVE))
{
    // add validation ruels
    
    if (!$form->value("user_old"))
    {
        $form->add_validation("{user_old}", "Old user must be indicated!");
    }
    elseif (!$form->value("user_new"))
    {
        $form->add_validation("{user_new}", "New user must be indicated!");
    }

    

    // validate form
    
    if ($form->validate())
    {
        

        $sql = "Update tasks set task_user = " . $form->value("user_new") .
               " where task_user = " . $form->value("user_old");

        $res = mysql_query($sql) or dberror($sql);

        $sql = "Update orders set order_retail_operator = " . $form->value("user_new") .
               " where order_archive_date is null and order_retail_operator = " . $form->value("user_old");

        $res = mysql_query($sql) or dberror($sql);

		$sql = "Update orders set order_user = " . $form->value("user_new") .
               " where order_archive_date is null and order_user = " . $form->value("user_old");

        $res = mysql_query($sql) or dberror($sql);

		
		$sql = "Update orders left join projects on project_order = order_id set project_retail_coordinator = " . $form->value("user_new") .
               " where order_archive_date is null and  project_retail_coordinator = " . $form->value("user_old");

        $res = mysql_query($sql) or dberror($sql);

        $sql = "Update orders left join projects on project_order = order_id  set project_design_contractor = " . $form->value("user_new") .
               " where order_archive_date is null and  project_design_contractor = " . $form->value("user_old");

        $res = mysql_query($sql) or dberror($sql);

        $sql = "Update orders left join projects on project_order = order_id  set project_design_supervisor = " . $form->value("user_new") .
               " where order_archive_date is null and project_design_supervisor = " . $form->value("user_old");

        $res = mysql_query($sql) or dberror($sql);


        /*
		$sql = "Update countries set country_designsupervisor = " . $form->value("user_new") .
               " where country_designsupervisor = " . $form->value("user_old");

        $res = mysql_query($sql) or dberror($sql);
        */


        redirect("replace_user_done.php");
    }

}
// Populate form and process button clicks



// Render page

$page = new Page("users");

$page->header();
$page->title("Replace a User by another User");

echo "<p>", "The replacement concerns tasks, retail operators, project managers, design contractors and design supervisors.", "</p>";

$form->render();
$page->footer();

?>