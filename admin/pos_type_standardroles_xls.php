<?php
/********************************************************************

    pos_type_standardroles_xls.php

    Generate Excel-File of Standard Roles

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-07-06
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-07-06
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

require_once "../include/xls/Writer.php";



/********************************************************************
    prepare Data Needed
*********************************************************************/

$sql = "select standardrole_id, standardrole_country, postype_name, project_costtype_text, country_name,  " .
       " concat(users1.user_name, ' ', users1.user_firstname) as rtco, " .
	   " concat(users2.user_name, ' ', users2.user_firstname) as dsup, " .
	   " concat(users3.user_name, ' ', users3.user_firstname) as rtop, " .
	   " concat(users4.user_name, ' ', users4.user_firstname) as cmsa, " .
	   "salesregion_name " . 
       "from standardroles " .
	   "left join postypes on postype_id = standardrole_postype " .
	   "left join project_costtypes on project_costtype_id = standardrole_legaltype " .
	   "left join countries on country_id = standardrole_country " .
	   "left join salesregions on salesregion_id = country_salesregion " .
	   "left join users as users1 on users1.user_id = standardrole_rtco_user " .
	   "left join users as users2 on users2.user_id = standardrole_dsup_user " .
	   "left join users as users3 on users3.user_id = standardrole_rtop_user " . 
	   "left join users as users4 on users4.user_id = standardrole_cms_approver_user " . 
	   " order by salesregion_name, country_name, postype_name";


$header = "";
$header = "Standard Roles: " . $header . " (" . date("d.m.Y G:i") . ")";


/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$filename = "standardsroles_" . date("Ymd") . ".xls";
$xls =& new Spreadsheet_Excel_Writer(); 
$xls->send($filename);

$xls->setVersion(8);

$sheet =& $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

//formats
$header_row =& $xls->addFormat();
$header_row->setSize(10);
$header_row->setAlign('left');
$header_row->setBold();


$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);

$f_normal_bold =& $xls->addFormat();
$f_normal_bold->setSize(8);
$f_normal_bold->setAlign('left');
$f_normal_bold->setBorder(1);
$f_normal_bold->setBold();


$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_used =& $xls->addFormat();
$f_used->setSize(8);
$f_used->setBorder(1);
$f_used->setPattern(2);
$f_used->setBgColor('yellow');



//captions
$captions = array();
//$captions[] = "Nr";
//$captions[] = "Geographical Region";
$captions[] = "Region";
$captions[] = "Country";
$captions[] = "POS Type";
$captions[] = "Legal Type";
$captions[] = "Project Manager";
$captions[] = "Design Supervisor";
$captions[] = "Retail Operator";
$captions[] = "CMS Approval by";


/********************************************************************
    write all captions
*********************************************************************/
$sheet->write(0, 0, $header, $header_row);
$sheet->writeRow(1, 0, $captions, $f_normal_bold);


$row_index = 2;
$cell_index = 0;
$counter = 0;
$col_widths = array();
for($i=0;$i<count($captions);$i++)
{
	$col_widths[$i] = strlen($captions[$i]);
}


$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
		
	$sheet->write($row_index, $cell_index, $row["salesregion_name"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["salesregion_name"]))
	{
		$col_widths[$cell_index] = strlen($row["salesregion_name"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["country_name"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["country_name"]))
	{
		$col_widths[$cell_index] = strlen($row["country_name"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["postype_name"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["postype_name"]))
	{
		$col_widths[$cell_index] = strlen($row["postype_name"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["project_costtype_text"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["project_costtype_text"]))
	{
		$col_widths[$cell_index] = strlen($row["project_costtype_text"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["rtco"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["rtco"]))
	{
		$col_widths[$cell_index] = strlen($row["rtco"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["dsup"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["dsup"]))
	{
		$col_widths[$cell_index] = strlen($row["dsup"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["rtop"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["rtop"]))
	{
		$col_widths[$cell_index] = strlen($row["rtop"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["cmsa"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["cmsa"]))
	{
		$col_widths[$cell_index] = strlen($row["cmsa"]);
	}
	$cell_index++;

	
	$cell_index = 0;
	$row_index++;
	
}

for($i=0;$i<count($captions);$i++)
{
	$sheet->setColumn($i, $i, $col_widths[$i]);
}


$xls->close(); 

?>