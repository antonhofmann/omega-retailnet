<?php
/********************************************************************

    service_supplier.php

    Mutation of supplier records.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-06-18
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-06-18
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_browse_catalog_in_admin");

// Build form

$form = new Form("suppliers", "supplier");

$form->add_section();
$form->add_lookup("supplier_item", "Item", "items", "item_code", 0, param("item"));

$form->add_section();
$form->add_list("supplier_address", "Supplier",
    "select address_id, address_company " .
    "from addresses left join address_types on address_type = address_type_id " .
    "where address_active and address_type_code IN('DECO', 'SUPP') " .
    "order by address_company");


$sql_currencies = "select currency_id, currency_symbol ".
                  "from currencies ".
                  "order by currency_symbol";

$form->add_section();
$form->add_edit("supplier_item_code", "Code", NOTNULL);
$form->add_multiline("supplier_item_name", "Name", 4);
$form->add_edit("supplier_item_price", "Price", NOTNULL);
$form->add_list("supplier_item_currency", "Currency", $sql_currencies, NOTNULL);

if(has_access("can_edit_catalog"))
{
	$form->add_button(FORM_BUTTON_SAVE, "Save");
	$form->add_button(FORM_BUTTON_BACK, "Back");
	$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);
}
else
{
	$form->add_button(FORM_BUTTON_BACK, "Back");
}
// Populate form and process button clicks

$form->populate();
$form->process();


// Render page

$page = new Page("items");

$page->header();
if(has_access("can_edit_catalog"))
{
	$page->title(id() ? "Edit Service Supplier Data" : "Add Service Supplier");
}
else
{
	$page->title("View Service Supplier");
}
$form->render();
$page->footer();

?>