<?php
/********************************************************************

    order_states.php

    Lists order states for editing.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-10-18
    Modified by:    Anton.Hofmann (aho@mediaparx.ch)
    Date modified:  2003-07-21
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("order_state.php");

$list = new ListView("select order_states.order_state_id, order_states.order_state_code, order_states.order_state_name, " .
                     "    order_states.order_state_action_name, order_state_group_name, " .
                     "    order_states.order_state_append_task, order_states.order_state_send_email, ". 
                     "    order_states.order_state_change_state, " .
                     "    order_states.order_state_delete_tasks, order_states.order_state_manually_deleted, " .
                     "    predecessors.order_state_code as predecessor_order_state_code, " . 
					 "    (select order_state_code from order_states as order_states2 where order_states2.order_state_id = order_states.order_state_predecessor_no_planning) as predecessor_no_planning_order_state_code, " . 
                     "    order_state_group_order_type, order_type_name, ".
                     "    notification_recipient_name, role_name ".
                     " from order_states " . 
					 "    left join order_states as predecessors on order_states.order_state_predecessor = predecessors.order_state_id " .
                     "    left join order_state_groups on order_states.order_state_group = order_state_group_id " .
                     "    left join order_types on order_state_group_order_type = order_type_id " .
                     "    left join notification_recipients on order_states.order_state_notification_recipient = notification_recipient_id " .
                     "    left join roles on order_states.order_state_performer = role_id");

$list->set_entity("order states");
$list->set_order("order_states.order_state_code");
$list->set_group("order_state_group_order_type", "order_type_name");

$list->add_column("order_state_code", "Code", "order_state.php", LIST_FILTER_FREE, "", 0);
$list->add_column("predecessor_order_state_code", "Predecessor", "", LIST_FILTER_FREE, "", 0);
$list->add_column("predecessor_no_planning_order_state_code", "Predecessor no planning", "", LIST_FILTER_FREE, "", 0);
$list->add_column("order_state_action_name", "Action Name", "", LIST_FILTER_FREE, "", 0);
$list->add_column("role_name", "Performer", "", LIST_FILTER_FREE, "", 0);
$list->add_column("notification_recipient_name", "Recipient", "", LIST_FILTER_FREE, "", 0);
$list->add_column("order_state_append_task", "Task", "", LIST_FILTER_FREE, "", COLUMN_ALIGN_CENTER);
$list->add_column("order_state_send_email", "Mail", "", LIST_FILTER_FREE, "", COLUMN_ALIGN_CENTER);
$list->add_column("order_state_change_state", "Change", "", LIST_FILTER_FREE, "", COLUMN_ALIGN_CENTER);
$list->add_column("order_state_delete_tasks", "Delete", "", LIST_FILTER_FREE, "", COLUMN_ALIGN_CENTER);
$list->add_column("order_state_manually_deleted", "Manually", "", LIST_FILTER_FREE, "", COLUMN_ALIGN_CENTER);

$list->add_button(LIST_BUTTON_FILTER, "Filter");
$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove filter");

$list->process();

$page = new Page("order_states");

$page->header();
$page->title("Order States");
$list->render();
$page->footer();

?>
