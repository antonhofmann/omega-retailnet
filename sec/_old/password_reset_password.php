<?php
/********************************************************************

    password_reset_password.php

    Reset user's password.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-30
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-30
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/
if(array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
	$ips = $_SERVER['HTTP_X_FORWARDED_FOR'];
	if(is_array ( $ips )) {
		$i = count($ips);
		$ip = $ips[$i-1];
	}
	else
	{
		$ip = $ips;
	}
}
else
{
	$ip = $_SERVER['REMOTE_ADDR'];
}

require_once "../include/frame.php";


if(!param("id"))
{
	redirect("noaccess.php");
}


$sql = "select user_id " . 
	   "from users " .
	   "where user_password_reset_id = " . dbquote(param("id"));

		
$res = mysql_query($sql) or dberror($sql);

if (mysql_num_rows($res) == 0)
{
	redirect("noaccess.php");
}

// Build form
$form = new Form("users", "user");
$form->add_comment("Please identify yourself and enter a new password.<br /><br />");
$form->add_section("Personal Data");
$form->add_hidden("id", param("id"));
$form->add_edit("firstname", "First Name*", NOTNULL, "", TYPE_CHAR, 50);
$form->add_edit("lastname", "Last Name*", NOTNULL, "", TYPE_CHAR, 50);
$form->add_edit("email", "Email*", NOTNULL, "", TYPE_CHAR, 50);
$form->add_section("Login Data*");
$form->add_edit("username", "Username*", NOTNULL, "", TYPE_CHAR, 20);
//$form->add_edit("password", "Old Password*", NOTNULL, "", TYPE_CHAR, 20);
$form->add_section("New Password");
$form->add_edit("password1", "New Password*", NOTNULL, "", TYPE_CHAR, 20, 0, 1, "policy");
$form->add_edit("password2", "Repeat New Password*", NOTNULL, "", TYPE_CHAR, 20);
$form->add_checkbox("sendmail", "Please send me a mail containing my new password", false, 0, "Notification");
$form->add_button("reset", "Reset Password");


$form->populate();
$form->process();


if($form->button("reset"))
{
	$form->add_validation("is_email_address({email})", "The email address is not valid.");
	$form->add_validation("is_valid_password({password1}, {username})", "The new password does not cover the policy rules. Please read the tool tip.");
	if($form->validate())
	{
		if($form->value("password1") != $form->value("password2"))
		{
			$form->error("New passwords are not identical.");
		}
		elseif(strlen($form->value("password1")) < 8)
		{
			$form->error("The new password must contain at least eight characters or digits.");
		}
		elseif(password_already_used($form->value("password1"), $form->value("email")))
		{
			$form->error("The new password is not valid because you have already used it. Please specify another password.");
		}
		else
		{
			/*
			$sql = "select user_id, user_firstname, user_name, user_email, user_used_passwords, " . 
				   "user_login, user_password_reset, DATE_ADD(user_password_reset, INTERVAL 60 DAY) AS reset_limit " .
				   "from users " .
				   "where user_login <> \"\" && user_login = " . dbquote($form->value("username")) .
				   "    and user_password = " . dbquote($form->value("password")) .
				   "    and user_firstname = " . dbquote($form->value("firstname")) .
				   "    and user_name = " . dbquote($form->value("lastname")) .
				   "    and user_email = " . dbquote($form->value("email")) .
				   "    and user_active = 1";
			*/
			$sql = "select user_id, user_firstname, user_name, user_email, user_used_passwords, " . 
				   "user_login, user_password_reset, DATE_ADD(user_password_reset, INTERVAL 60 DAY) AS reset_limit " .
				   "from users " .
				   "where user_login <> \"\" && user_login = " . dbquote($form->value("username")) .
				   "    and user_firstname = " . dbquote($form->value("firstname")) .
				   "    and user_name = " . dbquote($form->value("lastname")) .
				   "    and user_email = " . dbquote($form->value("email")) .
				   "    and user_active = 1";
			
			$res = mysql_query($sql) or dberror($sql);
			
			if ($row = mysql_fetch_assoc($res))
			{
				$user_email = $row["user_email"];
				$user_name = $row["user_name"];
				$user_firstname = $row["user_firstname"];
				
				$oldpasswords = unserialize($row["user_used_passwords"]);
				$oldpasswords[] = $form->value("password2");
				$newpasswords = array();
				
				if(count($oldpasswords) > 24) //save the last 24 passwords
				{
					foreach($oldpasswords as $key=>$value)
					{
						if($key > 0)
						{
							$newpasswords[] = $value;
						}
					}
				}
				else
				{
					$newpasswords = $oldpasswords;
				}

				$newpasswords_serialized = serialize($newpasswords);
				
				$sql = "update users set " .
					   "user_password = " . dbquote($form->value("password2")) . 
					   ", user_password_reset = " . dbquote(date("Y-m-d")) .
					   ", user_used_passwords = '" . $newpasswords_serialized . "' " .
					   ", user_password_reset_id = NULL " .
					   " where user_id = " . $row["user_id"];

				$result = mysql_query($sql) or dberror($sql);

				//send email notification
				if($form->value('sendmail') == 1)
				{
					$user_email = $row["user_email"];
					$user_name = $row["user_name"];
					$user_firstname = $row["user_firstname"];	


					$sender_email = $user_email;
					$sender_name = $user_firstname . ' ' . $user_name;

					//recipients
					$subject = "Retail Net Password Reset";
					$bodytext = "Dear " . $user_firstname . "\n\n";
					$bodytext .= "Your password was reset sucessfully to: " . $form->value("password2") . "\n\n";
					$bodytext .= "Please keep this email in a safe place." . "\n\n";
					$bodytext .= "Kind Regards" . "\n\n";
					$bodytext .= "Your Retail Team";


					$mail = new Mail();
					$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
					$mail->set_sender($sender_email, $sender_name);
					$mail->add_text($bodytext);
					$mail->add_recipient($sender_email);

					$result = $mail->send();
				}

				redirect("/user/login.php?pwreset=1");
			}
			else
			{
				$result = trace_login_failure($ip, "reset password", $form->value("username"), $form->value("password1"));
				$form->error("Identification failed. Please check your data");

			}
		}
	}

}

$page = new Page("login");

$page->header();
$page->title("Password Reset");
$form->render();
$page->footer();
?>

<script type="text/javascript">
    
	var selectedInput = null;
	$(document).ready(function(){
	  $("#firstname").focus();

	  $('input').focus(function() {
				selectedInput = this;
			});

	});

	document.onkeydown = process_key;
	
	function process_key(e)
	{
	  if( !e ) 
	  {
		if( window.event ) 
		{
		  e = window.event;
		} 
		else 
		{
		  return;
		}
	  }

	  if(selectedInput.name == "password2" && e.keyCode==13)
	  {
		button('reset');
	  }
	}
</script>


<div id="policy" style="display:none;">
    The new password must contain at least eight characters or digits.<br />
	Passwords you have used already before are not allowed.
</div> 


<?php
$page->footer();
?>