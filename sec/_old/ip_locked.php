<?php
/********************************************************************

    ip_locked.php

    Show a message that the IP is locked

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-09-05
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-09-05
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

if(array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
	$ips = $_SERVER['HTTP_X_FORWARDED_FOR'];
	if(is_array ( $ips )) {
		$i = count($ips);
		$ip = $ips[$i-1];
	}
	else
	{
		$ip = $ips;
	}
}
else
{
	$ip = $_SERVER['REMOTE_ADDR'];
}

$page = new Page("noaccess");
$page->header();
?>


<table border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td>You can not access this application anymore.<br /><br />Please contact the retail department in Biel for further information.<br />
		Your ip number is <strong><?php echo $ip;?></strong><br />
		Please report this IP to the retail department.</td>
	</tr>
</table>

<?php
$page->footer();
?>