<?php
/********************************************************************

    password_forgotten_confirm.php

    Send an email to the user with a link to reset the password.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-30
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-30
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

if(!get_session_value("pw_reset_email"))
{
	redirect("noaccess.php");
}

//send mail to the user
$id = md5(date("Y-m-d H:i:s") . "_PasswordReset__");
$sql = "Update users set user_password_reset_id = " . dbquote($id) . " where user_email = " . dbquote(get_session_value("pw_reset_email"));
$result = mysql_query($sql) or dberror($sql);

$mail = new Mail();
$mail->set_subject(MAIL_SUBJECT_PREFIX . ": Password forgotten");
$mail->set_sender("retailnet@swatch.com", "Retail Net");
$mail->add_recipient(get_session_value("pw_reset_email"));

$bodytext = "Dear " . get_session_value("pw_reset_firstname") . "\n\n";
$bodytext .= "We have recieved your request. Please follow the link below to reset your password." . "\n\n";
$bodytext .= APPLICATION_URL ."/sec/password_reset_password.php?id=" . $id . "\n\n";
$bodytext .= "Kind regards, your retail team.";
$mail->add_text($bodytext);
$mail->send();


$page = new Page("login");
$page->header();
?>


<table border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td>Your request was submitted successfully. <br /><br />You will get an email containing a link. <br />Please then follow the link in the email to reset your password.<br /><br /></td>
	</tr>

	<tr>
		<td><a href="../index.php">Go back to the Login Page</a></td>
	</tr>

</table>

<?php
$page->footer();

set_session_value("user_id", 0);
set_session_value("user_login", "");
set_session_value("permissions", array());

?>