<?php
/********************************************************************

    password_forgotten.php

    Send an email to the user with his or her password.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-30
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-30
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/
if(array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
	$ips = $_SERVER['HTTP_X_FORWARDED_FOR'];
	if(is_array ( $ips )) {
		$i = count($ips);
		$ip = $ips[$i-1];
	}
	else
	{
		$ip = $ips;
	}
}
else
{
	$ip = $_SERVER['REMOTE_ADDR'];
}

require_once "../include/frame.php";

$sql = "select count(sec_lockedip_ip) as num_recs " . 
       "from sec_lockedips " . 
	   "where sec_lockedip_ip = " . dbquote($ip);

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);
if($row["num_recs"] > 6)
{
	redirect("/sec/ip_locked.php");
}


$error = "";
$firstname = "";
$lastname = "";
$email = "";
$username = "";

if (isset($_REQUEST["action"]) && $_REQUEST["action"] == "login")
{
    
	$firstname = $_REQUEST["firstname"];
	$lastname = $_REQUEST["lastname"];
	$email = $_REQUEST["email"];
	$username = $_REQUEST["username"];
	
	if(!$_REQUEST["firstname"] 
		or !$_REQUEST["lastname"]
		or !$_REQUEST["email"]
		or !$_REQUEST["username"])
	{
		$error = "All fields are mandatory.";
	}
	else
	{
	
		$sql = "select user_id, user_firstname, user_name, user_email, " . 
			   "user_login, user_password_reset, DATE_ADD(user_password_reset, INTERVAL 60 DAY) AS reset_limit " .
			   "from users " .
			   "where user_login <> \"\" && user_login = " . dbquote($_REQUEST["username"]) .
			   "    and user_firstname = " . dbquote($_REQUEST["firstname"]) .
			   "    and user_name = " . dbquote($_REQUEST["lastname"]) .
			   "    and user_email = " . dbquote($_REQUEST["email"]) .
			   "    and user_active = 1";

		
		$res = mysql_query($sql) or dberror($sql);
		
		if (mysql_num_rows($res) > 0)
		{
			set_session_value("pw_reset_email", $_REQUEST["email"]);
			set_session_value("pw_reset_firstname", $_REQUEST["firstname"]);
			redirect("password_forgotten_confirm.php");
		}
		else
		{
			$result = trace_login_failure($ip, "password forgotten", $_REQUEST["username"], "");
			$error = "Invalid first name, last name, email or username. <br />Please reenter your data.";

		}
	}
}


// Build form
$form = new Form("users", "user");
$form->add_comment("Please identify yourself.<br /><br />");
$form->add_section("Personal Data");
$form->add_edit("firstname", "First Name*", NOTNULL, "", TYPE_CHAR, 50);
$form->add_edit("lastname", "Last Name*", NOTNULL, "", TYPE_CHAR, 50);
$form->add_edit("email", "Email*", NOTNULL, "", TYPE_CHAR, 50);
$form->add_section("Login Data*");
$form->add_edit("username", "Username*", NOTNULL, "", TYPE_CHAR, 20);
$form->add_button("submit", "Submit Request");


$form->populate();
$form->process();


if($form->button("submit"))
{
	$form->add_validation("is_email_address({email})", "The email address is not valid.");
	if($form->validate())
	{
		$sql = "select user_id, user_firstname, user_name, user_email, " . 
			   "user_login, user_password_reset, DATE_ADD(user_password_reset, INTERVAL 60 DAY) AS reset_limit " .
			   "from users " .
			   "where user_login <> \"\" && user_login = " . dbquote($form->value("username")) .
			   "    and user_firstname = " . dbquote($form->value("firstname")) .
			   "    and user_name = " . dbquote($form->value("lastname")) .
			   "    and user_email = " . dbquote($form->value("email")) .
			   "    and user_active = 1";

		
		$res = mysql_query($sql) or dberror($sql);
		
		if (mysql_num_rows($res) > 0)
		{
			set_session_value("pw_reset_email", $form->value("email"));
			set_session_value("pw_reset_firstname", $form->value("firstname"));
			redirect("password_forgotten_confirm.php");
		}
	}
}

$page = new Page("login");

$page->header();
$page->title("Password Forgotten");
$form->render();
$page->footer();
?>

<script type="text/javascript">
    
	var selectedInput = null;
	$(document).ready(function(){
	  $("#firstname").focus();

	  $('input').focus(function() {
				selectedInput = this;
			});

	});

	document.onkeydown = process_key;
	
	function process_key(e)
	{
	  if( !e ) 
	  {
		if( window.event ) 
		{
		  e = window.event;
		} 
		else 
		{
		  return;
		}
	  }

	  if(selectedInput.name == "username" && e.keyCode==13)
	  {
		button('submit');
	  }
	}
</script>