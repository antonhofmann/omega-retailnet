<?php
/********************************************************************

    login_helper.php

    Login Helper.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-09-05
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-09-05
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

if(array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
	$ips = $_SERVER['HTTP_X_FORWARDED_FOR'];
	if(is_array ( $ips )) {
		$i = count($ips);
		$ip = $ips[$i-1];
	}
	else
	{
		$ip = $ips;
	}
}
else
{
	$ip = $_SERVER['REMOTE_ADDR'];
}

$ip_excluded = false;
$sql = "select * from sec_excluded_ips where sec_excluded_ip_exclude = 1";
$res = mysql_query($sql) or dberror($sql);

while($row = mysql_fetch_assoc($res))
{
	if($ip == $row["sec_excluded_ip_ip"])
	{
		$ip_excluded = true;
		$ip_locked = false;
	}
}

$max_trials = 6;
$sql = "select * from sec_excluded_ips where sec_excluded_ip_exclude = 0 and sec_excluded_ip_ip = " . dbquote($ip);
$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	if($row["sec_excluded_ip_maxtrials"] > 0)
	{
		$max_trials = $row["sec_excluded_ip_maxtrials"];
	}
}

if($ip_excluded == false)
{
	$ip_locked = false;
	$sql = "select count(sec_lockedip_ip) as num_recs " . 
		   "from sec_lockedips " . 
		   "where sec_lockedip_ip = " . dbquote($ip);

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);

	if($row["num_recs"] > $max_trials)
	{
		set_session_value("user_id", "");
		set_session_value("user_login", "");
		redirect("/sec/ip_locked.php");
	}
}



$message = "";
if(array_key_exists("pwreset", $_GET) and $_GET["pwreset"] == 1)
{
	set_session_value("user_id", 0);
	set_session_value("user_login", "");
	set_session_value("permissions", array());

	$message = "Your password was reset successfully. <br />You can now log in with your new password.";
}
$error = "";
if (isset($_REQUEST["action"]) && $_REQUEST["action"] == "login")
{
    $sql = "select *, DATE_ADD(user_password_reset, INTERVAL 60 DAY) AS reset_limit " .
           "from users " .
           "where user_login <> \"\" && user_login = " . dbquote($_REQUEST["username"]) .
           "    and user_password = " . dbquote($_REQUEST["password"]) .
           "    and user_active = 1";

    
	$res = mysql_query($sql) or dberror($sql);
    
    if (mysql_num_rows($res) > 0)
    {
        $row = mysql_fetch_assoc($res);
        
        
        // get user role id
		$sql = "SELECT user_role_role FROM user_roles WHERE user_role_user = ".$row['user_id'];
		$role = mysql_fetch_assoc(mysql_query($sql));
				
        
		if($ip_locked == true)
		{
			$sql_p = "select distinct permission_name " .
				     "from user_roles left join roles on user_role_role = role_id " .
				     "    left join role_permissions on role_id = role_permission_role " .
				     "    left join permissions on role_permission_permission = permission_id " .
				     "where user_role_user = " . $row["user_id"] . 
				     " and permission_name = 'can_unlock_ips'";

			$res_p = mysql_query($sql_p) or dberror($sql_p);
			if($row_p = mysql_fetch_assoc($res_p))
			{
			}
			else
			{
				redirect("/sec/ip_locked.php");
			}
		}
	
		
		if($row["user_password_reset"] == NULL or $row["user_password_reset"] == '0000-00-00' or $row["reset_limit"] < date("Y-m-d"))
		{
			set_session_value("user_id", "password_reset");
			redirect("/sec/password_reset.php");
		}
		else
		{
			set_session_value("user_id", $row["user_id"]);
			set_session_value("user_login", $row["user_login"]);
			set_session_value('user_role', $role['user_role_role']);
			set_session_value('user_firstname', $row['user_firstname']);
			set_session_value('user_name', $row['user_name']);

			build_access();

			$url = get_session_value("login_page");

			if ($url)
			{
				set_session_value("login_page", "");
				redirect($url);
			}
			else
			{
				$sql = "delete from sec_lockedips where sec_lockedip_ip = " . dbquote($ip);
			    $result = mysql_query($sql) or dberror($sql);
				redirect("welcome.php");
			}
		}
    }
    else
    {
        $result = trace_login_failure($ip, "login", $_REQUEST["username"], $_REQUEST["password"]);
		if($ip_locked == true)
		{
			set_session_value("user_id", "");
			set_session_value("user_login", "");
			redirect("/sec/ip_locked.php");
		}
		
		$error = "Invalid username or password, please try again";

    }
}
?>