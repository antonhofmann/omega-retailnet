<?php
/********************************************************************

    password_reset_password.php

    Reset user's password.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-30
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-30
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/
if(array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
	$ips = $_SERVER['HTTP_X_FORWARDED_FOR'];
	if(is_array ( $ips )) {
		$i = count($ips);
		$ip = $ips[$i-1];
	}
	else
	{
		$ip = $ips;
	}
}
else
{
	$ip = $_SERVER['REMOTE_ADDR'];
}

require_once "../include/frame.php";


if(!param("id"))
{
	redirect("noaccess.php");
}



$pdofields = array(
	':id' => param("id")
);

$sql = "select user_id " . 
	   "from users " .
	   "where user_password_reset_id = :id";

		
$connection_string = "mysql:host=" . RETAILNET_SERVER . ";dbname=" . RETAILNET_DB;
$pdo = new PDO($connection_string, RETAILNET_USER, RETAILNET_PASSWORD);
$pdo->exec("set names utf8");
$prepared_sql = $pdo->prepare($sql);


$prepared_sql->execute($pdofields);
$row = $prepared_sql->fetch();


if ($row == false or count($row) == 0)
{
	redirect("noaccess.php");
}

// Build form
$form = new Form("users", "user");
$form->add_comment("Please identify yourself and enter a new password.<br /><br />");
$form->add_section("Personal Data");
$form->add_hidden("id", param("id"));
$form->add_edit("firstname", "First Name*", NOTNULL, "", TYPE_CHAR, 50);
$form->add_edit("lastname", "Last Name*", NOTNULL, "", TYPE_CHAR, 50);
$form->add_edit("email", "Email*", NOTNULL, "", TYPE_CHAR, 50);
$form->add_section("Login Data*");
$form->add_edit("username", "Username*", NOTNULL, "", TYPE_CHAR, 20);
//$form->add_edit("password", "Old Password*", NOTNULL, "", TYPE_CHAR, 20);
$form->add_section("New Password");
$form->add_edit("password1", "New Password*", NOTNULL, "", TYPE_CHAR, 20, 0, 1, "policy");
$form->add_edit("password2", "Repeat New Password*", NOTNULL, "", TYPE_CHAR, 20);
$form->add_checkbox("sendmail", "Please send me a mail containing my new password", false, 0, "Notification");
$form->add_button("reset", "Reset Password");


$form->populate();
$form->process();


if($form->button("reset"))
{
	$form->add_validation("is_email_address({email})", "The email address is not valid.");
	$form->add_validation("is_valid_password({password1}, {username})", "The new password does not cover the policy rules. Please read the tool tip.");
	if($form->validate())
	{
		if($form->value("password1") != $form->value("password2"))
		{
			$form->error("New passwords are not identical.");
		}
		elseif(strlen($form->value("password1")) < 8)
		{
			$form->error("The new password must contain at least eight characters or digits.");
		}
		elseif(password_already_used($form->value("password1"), $form->value("email")))
		{
			$form->error("The new password is not valid because you have already used it. Please specify another password.");
		}
		else
		{
			
			
			$pdofields = array(
				':user_firstname' => $_REQUEST["firstname"],
				':user_name' => $_REQUEST["lastname"],
				':user_email' => $_REQUEST["email"],
				':user_login' => $_REQUEST["username"]
			);
			
			
			$sql = "select user_id, user_firstname, user_name, user_email, " . 
				   "user_login, user_password_reset, DATE_ADD(user_password_reset, INTERVAL 60 DAY) AS reset_limit " .
				   "from users " .
				   "where user_login <> \"\" && user_login = :user_login " . 
				   "    and user_firstname = :user_firstname " . 
				   "    and user_name = :user_name " . 
				   "    and user_email = :user_email " . 
				   "    and user_active = 1";

			$connection_string = "mysql:host=" . RETAILNET_SERVER . ";dbname=" . RETAILNET_DB;
			$pdo = new PDO($connection_string, RETAILNET_USER, RETAILNET_PASSWORD);
			$pdo->exec("set names utf8");
			$prepared_sql = $pdo->prepare($sql);


			$prepared_sql->execute($pdofields);
			$row = $prepared_sql->fetch();
			
						
			if ($row != false and count($row) >0)
			{
				$user_email = $row["user_email"];
				$user_name = $row["user_name"];
				$user_firstname = $row["user_firstname"];
				
				$oldpasswords = unserialize($row["user_used_passwords"]);
				$oldpasswords[] = $form->value("password2");
				$newpasswords = array();
				
				if(count($oldpasswords) > 24) //save the last 24 passwords
				{
					foreach($oldpasswords as $key=>$value)
					{
						if($key > 0)
						{
							$newpasswords[] = $value;
						}
					}
				}
				else
				{
					$newpasswords = $oldpasswords;
				}

				$newpasswords_serialized = serialize($newpasswords);
				
				
				$pdofields = array(
					':user_id' => $row["user_id"],
					':user_password' => $form->value("password2"),
					':user_password_reset' => date("Y-m-d"),
					':user_used_passwords' => $newpasswords_serialized,
					':user_password_reset_id' => NULL
				);

				
				
				
				$sql = "update users set " .
					   "user_password = :user_password, " . 
					   " user_password_reset = :user_password_reset, " . 
					   " user_used_passwords = :user_used_passwords, " . 
					   " user_password_reset_id = :user_password_reset_id " .
					   " where user_id = :user_id";


				$prepared_sql = $pdo->prepare($sql);
				$success =  $prepared_sql->execute($pdofields);

				//send email notification
				if($success == 1 and $form->value('sendmail') == 1)
				{
					$user_email = $row["user_email"];
					$user_name = $row["user_name"];
					$user_firstname = $row["user_firstname"];	


					$sender_email = $user_email;
					$sender_name = $user_firstname . ' ' . $user_name;

					//recipients
					$subject = "Retail Net Password Reset";
					$bodytext = "Dear " . $user_firstname . "\n\n";
					$bodytext .= "Your password was reset sucessfully to: " . $form->value("password2") . "\n\n";
					$bodytext .= "Please keep this email in a safe place." . "\n\n";
					$bodytext .= "Kind Regards" . "\n\n";
					$bodytext .= "Your Retail Team";

					$senmail_activated = true;
					$mail = new Mail();
					$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
					$mail->set_sender($sender_email, $sender_name);
					$mail->add_text($bodytext);
					$mail->add_recipient($sender_email);

					$result = $mail->send();
				}

				redirect("/user/login.php?pwreset=1");
			}
			else
			{
				$result = trace_login_failure($ip, "reset password", $form->value("username"), $form->value("password1"));
				$form->error("Identification failed. Please check your data");

			}
		}
	}

}

$page = new Page("login");

$page->header();
$page->title("Password Reset");
$form->render();
$page->footer();
?>

<script type="text/javascript">
    
	var selectedInput = null;
	$(document).ready(function(){
	  $("#firstname").focus();

	  $('input').focus(function() {
				selectedInput = this;
			});

	});

	document.onkeydown = process_key;
	
	function process_key(e)
	{
	  if( !e ) 
	  {
		if( window.event ) 
		{
		  e = window.event;
		} 
		else 
		{
		  return;
		}
	  }

	  if(selectedInput.name == "password2" && e.keyCode==13)
	  {
		button('reset');
	  }
	}
</script>


<div id="policy" style="display:none;">
    The new password must contain at least eight characters or digits.<br />
	Passwords you have used already before are not allowed.
</div> 


<?php
$page->footer();
?>