<?php
/********************************************************************

    noaccess.php

    Displayed when the user tries to access a page he is
    not allowed to.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-04
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-08-19
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

$page = new Page("noaccess");
$page->header();
echo "<p>", "You don't have access to this page.", "</p>";
$page->footer();

?>