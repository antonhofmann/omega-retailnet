<?php
/********************************************************************

    inflationrate_bulk.php

    Creation and mutation of inflation rate records records.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2012_01_15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2012_01_15
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_full_access_to_cer");

/********************************************************************
    prepare all data needed
*********************************************************************/

$years = array();
$y = date("Y");
for($i=-1;$i<3;$i++)
{
	$years[$y+$i] = $y+$i;
}

$sql_countries = "select country_id, country_name ". 
                 "from countries ";


$inflationrates = array();

$res = mysql_query($sql_countries) or dberror($sql_countries);
while ($row = mysql_fetch_assoc($res))
{
	
	$inflationrates[$row['country_id']] = '';
	if(param("year") and param("year") > 0)
	{
		$sql_c = "select inflationrate_id, inflationrate_rate " .
			   "from cer_inflationrates " .
			   "where inflationrate_country = " . $row["country_id"] . " and inflationrate_year = " . param("year");

		$res_c = mysql_query($sql_c) or dberror($sql_c);
		if ($row_c = mysql_fetch_assoc($res_c))
		{
			$inflationrates[$row['country_id']] = $row_c["inflationrate_rate"];
		}
	}
}

/********************************************************************
    build form 
*********************************************************************/
$form = new Form("cer_inflationrates", "interest rate");

$form->add_section();
$form->add_list("year", "Year*", $years, NOTNULL|SUBMIT, param("year"));
$form->populate();


if($form->button("year"))
{
	redirect("inflationrate_bulk.php?year=" . param("year"));
}

$list1 = new ListView($sql_countries);

$list1->set_entity("cer_inflationrates");
$list1->set_order("country_name");

$list1->add_column("country_name", "Name");
$list1->add_edit_column("interest_rate", "Interest Rate %", 8, $flags = 0, $inflationrates);

$list1->add_button('save', "Save");

$list1->populate();
$list1->process();


if($list1->button("save"))
{
	foreach ($list1->values("interest_rate") as $key=>$value)
    {
        if(!is_decimal_value($value, 10, 2)) {
			$form->error("The list contains invalid input values.");
		}
		else
		{
		
			// update record

			
			$fields = array();
			$values = array();


			$sql = "select count(inflationrate_id) as num_recs from cer_inflationrates " . 
				   "where inflationrate_country = " . $key . " and inflationrate_year = " . param("year");

			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);
			if ($row["num_recs"] > 0)
			{
				$fields = array();

				$fields[] = "inflationrate_rate = " . dbquote($value);
				$fields[] = "user_modified = " . dbquote(user_login());
				$fields[] = "date_modified = " . dbquote(date("Y-m-d H:i:s"));

				$sql = "update cer_inflationrates set " . join(", ", $fields) .  "where inflationrate_country = " . $key . " and inflationrate_year = " . param("year");
				
				if($value != '') {
					mysql_query($sql) or dberror($sql);
				}
			}
			else
			{
				$fields[] = "inflationrate_country";
				$fields[] = "inflationrate_year";
				$fields[] = "inflationrate_rate";
				$fields[] = "user_created";
				$fields[] = "date_created";
				$fields[] = "user_modified";
				$fields[] = "date_modified";


				$values[] = $key;
				$values[] = param("year");
				$values[] = $value;
				$values[] = dbquote(user_login());
				$values[] = dbquote(date("Y-m-d H:i:s"));
				$values[] = dbquote(user_login());
				$values[] = dbquote(date("Y-m-d H:i:s"));

				$sql = "insert into cer_inflationrates (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				if($value!= '') {
					mysql_query($sql) or dberror($sql);
				}
			}
		}
    }
	redirect("inflationrates.php");
}

$page = new Page("inflationrates");

$page->header();
$page->title("Add/Modify Inflation Rates for serveral Countries");
$form->render();

if(param("year") and param("year") > 0)
{
	$list1->render();
}

$page->footer();

?>
