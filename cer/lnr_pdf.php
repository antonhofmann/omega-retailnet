<?php
/********************************************************************

    lnr_pdf.php

    Print Lease Negotiation Application Form.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-06-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-06-14
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();

$SUPPRESS_HEADERS = true;
define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 


require "../include/frame.php";


$cer_version = 0;
$ln_version = 0;
$ln_signatures = "";
if(param("cerversion"))
{
	$cer_version = param("cerversion");
}
if(param("lnversion"))
{
	$ln_version = param("lnversion");
}


require "include/get_functions.php";
require "include/get_project.php";
require "../shared/func_posindex.php";
require "include/financial_functions.php";


check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');



// data needed
$project = get_project(param("pid"));
$order_number  = $project["project_order"];


//get pos data
if($project["pipeline"] == 0)
{
	$pos_data = get_poslocation($project["posaddress_id"], "posaddresses");
}
elseif($project["pipeline"] == 1)
{
	$pos_data = get_poslocation_from_pipeline($project["posaddress_id"], $project["order_id"]);
}

$relocated_pos = '';
if($project["project_relocated_posaddress_id"] > 0)
{
		$sql = "select concat(posaddress_name, ', ', place_name) as posname
		       from posaddresses
			   left join places on place_id = posaddress_place_id 
			   where posaddress_id = " . dbquote($project["project_relocated_posaddress_id"]);
		
		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$relocated_pos = $row['posname'];
		}		
		
}



$client_currency = get_cer_currency(param("pid"), $cer_version);
$currency_symbol = $client_currency["symbol"];

$gross_surface = $project["project_cost_gross_sqms"];
$total_surface = $project["project_cost_totalsqms"];
$sale_surface = $project["project_cost_sqms"];
$bo_surface = $project["project_cost_backofficesqms"];
$other_surface = $project["project_cost_othersqms"];

$floor_surfaces = '';
if($project['project_cost_numfloors'] > 1) {
	
	if($project['project_cost_floorsurface1'] > 0) {
		$floor_surfaces .= "Floor 1: " . $project['project_cost_floorsurface1'];
	}
	if($project['project_cost_floorsurface2'] > 0) {
		$floor_surfaces .= ", Floor 2: " . $project['project_cost_floorsurface2'];
	}
	if($project['project_cost_floorsurface3'] > 0) {
		$floor_surfaces .= ", Floor31: " . $project['project_cost_floorsurface3'];
	}
}

if($floor_surfaces) {
	$floor_surfaces = " (" . $floor_surfaces . ")";
}

if($relocated_pos) {

	$postype = "Relocation" . ", " . $project["project_costtype_text"] . ", " . $project["postype_name"] . ", " . $project["possubclass_name"];
}
else {
	$postype = $project["projectkind_name"] . ", " . $project["project_costtype_text"] . ", " . $project["postype_name"] . ", " . $project["possubclass_name"];
}



$contract_starting_date = "";
$contract_ending_date = "";


//get keymoney and deposit
$tmp = get_pos_intangibles(param("pid"), 15, $cer_version);
if(count($tmp) > 0)
{
	$keymoney_full = $tmp ["cer_investment_amount_cer_loc"];
	$keymoney = round($keymoney_full/1000, 0);
	

	$tmp = get_pos_intangibles(param("pid"), 9, $cer_version);
	$deposit = $tmp ["cer_investment_amount_cer_loc"];
	$deposit = round($deposit/1000, 0);
}
else
{
	$keymoney = "";
	$deposit = "";
}

//get rents
$first_full_year = $cer_basicdata["cer_basicdata_firstyear"];
if($cer_basicdata["cer_basicdata_firstmonth"] > 1)
{
	$first_full_year = $cer_basicdata["cer_basicdata_firstyear"]+1;
}

$last_full_year = $cer_basicdata["cer_basicdata_lastyear"];
if($cer_basicdata["cer_basicdata_lastmonth"] < 12)
{
	$last_full_year = $cer_basicdata["cer_basicdata_lastyear"]-1;
}

$fullrent_firstyear = 0;
$fixedrent_firstyear = 0;
$turnoverrent_firstyear = 0;
$taxes_on_fixedrent_firstyear = '';
$deposit_value_in_months = "";

$taxes_on_fixedrent_firstyear = "";

if($first_full_year > 0 and $cer_basicdata["cer_basicdata_firstyear"] < $cer_basicdata["cer_basicdata_lastyear"])
{
	$sql_e = "select sum(cer_expense_amount) as cer_expense_amount from cer_expenses " . 
		   "where cer_expense_cer_version = " . $cer_version . "  and cer_expense_project = " . param("pid") . 
		   " and cer_expense_type in (2,20) " . 
		   " and cer_expense_year = " . dbquote($first_full_year);

	$res_e = mysql_query($sql_e) or dberror($sql_e);
	if ($row_e = mysql_fetch_assoc($res_e))
	{
		$fixedrent_firstyear = $row_e["cer_expense_amount"];

		if($cer_basicdata["cer_basicdata_tax_percentage_on_rent"] > 0)
		{
			$tmp = $cer_basicdata["cer_basicdata_tax_percentage_on_rent"] / 100;
			$tmp = $fixedrent_firstyear/(1+$tmp);
			$taxes_on_fixedrent_firstyear = round($fixedrent_firstyear - $tmp, 0);
			$taxes_on_fixedrent_firstyear = ' (including ' . $cer_basicdata["cer_basicdata_tax_percentage_on_rent"] . '% taxes - ' . $currency_symbol . ' ' . $taxes_on_fixedrent_firstyear . ')';
		}
		
	}

	$sql_e = "select cer_expense_amount from cer_expenses " . 
		   "where cer_expense_cer_version = " . $cer_version . "  and cer_expense_project = " . param("pid") . 
		   " and cer_expense_type = 16 " . 
		   " and cer_expense_year = " . dbquote($first_full_year);

	$res_e = mysql_query($sql_e) or dberror($sql_e);
	if ($row_e = mysql_fetch_assoc($res_e))
	{
		$turnoverrent_firstyear = $row_e["cer_expense_amount"];
	}
}
elseif($cer_basicdata["cer_basicdata_firstyear"] == $cer_basicdata["cer_basicdata_lastyear"])
{
	$sql_e = "select sum(cer_expense_amount) as cer_expense_amount from cer_expenses " . 
		   "where cer_expense_cer_version = " . $cer_version . "  and cer_expense_project = " . param("pid") . 
		   " and cer_expense_type in (2,20) " . 
		   " and cer_expense_year = " . dbquote($cer_basicdata["cer_basicdata_firstyear"]);

	$res_e = mysql_query($sql_e) or dberror($sql_e);
	if ($row_e = mysql_fetch_assoc($res_e))
	{
		$fixedrent_firstyear = $row_e["cer_expense_amount"];

		if($cer_basicdata["cer_basicdata_tax_percentage_on_rent"] > 0)
		{
			$tmp = $cer_basicdata["cer_basicdata_tax_percentage_on_rent"] / 100;
			$tmp = $fixedrent_firstyear/(1+$tmp);
			$taxes_on_fixedrent_firstyear = round($fixedrent_firstyear - $tmp, 0);

			$taxes_on_fixedrent_firstyear = ' (including ' . $cer_basicdata["cer_basicdata_tax_percentage_on_rent"] . '% taxes - ' . $currency_symbol . ' ' . $taxes_on_fixedrent_firstyear . ')';
		}
	}

	$sql_e = "select cer_expense_amount from cer_expenses " . 
		   "where cer_expense_cer_version = " . $cer_version . "  and cer_expense_project = " . param("pid") . 
		   " and cer_expense_type = 16 " . 
		   " and cer_expense_year = " . dbquote($cer_basicdata["cer_basicdata_firstyear"]);


	$res_e = mysql_query($sql_e) or dberror($sql_e);
	if ($row_e = mysql_fetch_assoc($res_e))
	{
		$turnoverrent_firstyear = $row_e["cer_expense_amount"];
	}
}

$fullrent_firstyear = $fixedrent_firstyear + $turnoverrent_firstyear;

$fullrent_firstyear = //$fullrent_firstyear*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
$fullrent_firstyear = round($fullrent_firstyear/1000, 0);


//$turnoverrent_firstyear = $turnoverrent_firstyear*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
$turnoverrent_firstyear = round($turnoverrent_firstyear/1000, 0);

$fixedrent_firstyear = round($fixedrent_firstyear/1000, 0);


//Lease Consitions

$poslease = get_ln_lease_data($project["project_id"], $ln_basicdata["ln_basicdata_id"], $project["pipeline"], $ln_version);

if(count($poslease) > 0)
{
	$landlord = str_replace("\r\n", " ", $poslease["poslease_landlord_name"]);
	$negotiated_rental_conditions = $poslease["poslease_negotiated_conditions"];
	$indexrate = $poslease["poslease_indexrate"] . "%";
	$real_estate_fee = $poslease["poslease_realestate_fee"] . "%";

	$free_weeks = $poslease["poslease_freeweeks"];
	$free_months = round($free_weeks/4, 1);
	$termination_deadline = $poslease["poslease_termination_time"];
	
	$renewal_option_date = "";

	if($poslease["poslease_extensionoption"] != Null and $poslease["poslease_extensionoption"] != '0000-00-00')
	{
		$d1 = new DateTime($poslease["poslease_enddate"]);
		$d2 = new DateTime($poslease["poslease_extensionoption"]);
		$interval = date_diff($d2, $d1);

		$tmp = round($interval->d/30, 1) + $interval->m + ($interval->y * 12);
		$renewal_option_date = $tmp . " months (" . to_system_date($poslease["poslease_extensionoption"]) . ")";
	}

	$average_annual_rent = 0;
	$duration_in_years = 0;
	if($poslease["poslease_startdate"] != NULL 
		and $poslease["poslease_startdate"] != '0000-00-00' 
		and $poslease["poslease_enddate"] != NULL
		and $poslease["poslease_enddate"] != '0000-00-00')
		{
			$diff = abs(strtotime($poslease["poslease_enddate"]) - strtotime($poslease["poslease_startdate"]));
			$years = floor($diff / (365*60*60*24));
			$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));

			if($months == 12)
			{
				$months = 0;
				$years++;
			}

			$rental_duration = $years . " years and " . $months . " months";

			$duration_in_years = $years + ($months/12);

			$contract_starting_date = to_system_date($poslease["poslease_startdate"]);
			$contract_ending_date = to_system_date($poslease["poslease_enddate"]);

			/*
			$date1 = date(strtotime($poslease["poslease_startdate"]));
			$date2 = date(strtotime($poslease["poslease_enddate"]));
			
			$difference = $date2 - $date1;
			$rental_duration_in_months = round($difference / 86400 / 30, 1);
            */

			$date1 = new DateTime($poslease["poslease_startdate"]);
			$date2 = new DateTime($poslease["poslease_enddate"]);
			$interval = date_diff($date1, $date2);
			$rental_duration_in_months = round($interval->d/30, 1) + $interval->m + ($interval->y * 12) . ' months';

			$sql_r = "select cer_fixed_rent_total_surface, cer_fixed_rent_amount, cer_fixed_rent_unit " . 
				   " from cer_fixed_rents " . 
				   " where cer_fixed_rent_cer_version = " . $cer_version . 
				   " and cer_fixed_rent_project_id = " . param("pid") . 
				   " order by cer_fixed_rent_from_year ASC ";
			
			$res_r = mysql_query($sql_r) or dberror($sql_r);
			if ($row_r = mysql_fetch_assoc($res_r))
			{
				$tmp_p = 0;
				if($row_r["cer_fixed_rent_unit"] == 1 or $row_r["cer_fixed_rent_unit"] == 2)
				{
					$tmp_p = $row_r["cer_fixed_rent_total_surface"] * $row_r["cer_fixed_rent_amount"];
				}
				elseif($row_r["cer_fixed_rent_unit"] == 3 or $row_r["cer_fixed_rent_unit"] == 3)
				{
					$tmp_p = ($row_r["cer_fixed_rent_total_surface"] * $row_r["cer_fixed_rent_amount"])/12;
				}
				elseif($row_r["cer_fixed_rent_unit"] == 5)
				{
					$tmp_p = $row_r["cer_fixed_rent_amount"];
				}
				elseif($row_r["cer_fixed_rent_unit"] == 6)
				{
					$tmp_p = $row_r["cer_fixed_rent_amount"]/12;
				}
				if($tmp_p > 0)
				{
					$tmp = get_pos_intangibles(param("pid"), 9, $cer_version);
					
					$deposit_value_in_months = 	 round($tmp ["cer_investment_amount_cer_loc"] / $tmp_p, 0);
				}
			}
			
			
			
			
	}
		
	if($duration_in_years > 0)
	{
		$total_lease_commitment = 0;
		$sql_cer = "select * from cer_expenses " .
					  "where cer_expense_cer_version = " . $cer_version . "  and cer_expense_project = " . param("pid") .
					  " and cer_expense_type IN(2, 3, 16, 18, 19, 20)";


		$res_cer = mysql_query($sql_cer) or dberror($sql_cer);
		while ($row_cer = mysql_fetch_assoc($res_cer))
		{
			$total_lease_commitment = $total_lease_commitment + 1*$row_cer["cer_expense_amount"];
		}
					

		if($duration_in_years > 0)
		{
			$average_annual_rent = round(($total_lease_commitment / $duration_in_years)/1000, 0);
		}
	}
}
else
{
	$landlord = "";
	$negotiated_rental_conditions = "";
	$indexrate = "";
	$real_estate_fee = "";
	$average_annual_rent = "";
	$duration_in_years = "";
	$total_lease_commitment = "";
	$contract_starting_date = "";
	$contract_ending_date = "";
	$free_weeks = "";
	$free_months = "";
	$termination_deadline = "";
	$rental_duration_in_months = "";
	$renewal_option_date = "";
}

//get averag increas in fixed rents
$average_yearly_increase = "";
$num_of_years1 = 0;
$num_of_years2 = 0;
$num_of_years3 = 0;
$index_rate_total = 0;
$increase_rate_total = 0;
$inflation_rate_total = 0;
$sql_e = "select cer_fixed_rent_index_rate, cer_fixed_rent_increas_rate, cer_fixed_rent_inflation_rate " . 
		 "from cer_fixed_rents " . 
		 "where cer_fixed_rent_cer_version = " . $cer_version . " and cer_fixed_rent_project_id = " . param("pid");

$res = mysql_query($sql_e) or dberror($sql_e);

while($row = mysql_fetch_assoc($res))
{
	if($row["cer_fixed_rent_index_rate"] > 0)
	{
		$num_of_years1++;
		$index_rate_total = $index_rate_total + $row["cer_fixed_rent_index_rate"];
	}
	if($row["cer_fixed_rent_increas_rate"] > 0)
	{
		$num_of_years2++;
		$increase_rate_total = $increase_rate_total + $row["cer_fixed_rent_increas_rate"];
	}
	if($row["cer_fixed_rent_inflation_rate"] > 0)
	{
		$num_of_years3++;
		$inflation_rate_total = $inflation_rate_total + $row["cer_fixed_rent_inflation_rate"];
	}
}

$tmp1 = "";
$tmp2 = "";
$tmp3 = "";
if($num_of_years1 > 0)
{
	$tmp1 = "Index Rate: " . number_format($index_rate_total/$num_of_years1, 2) . "% ";
}
if($num_of_years2 > 0)
{
	
	$tmp2 = "Increase Rate: " . number_format($increase_rate_total/$num_of_years2, 2) . "% ";
}
if($num_of_years3 > 0)
{
	$tmp3 = "Inflation Rate: " . number_format($inflation_rate_total/$num_of_years3, 2) . "% ";
}
$average_yearly_increase = $tmp1 . $tmp2 . $tmp3;



if($average_yearly_increase == 0) {

	$_tmp = '';
	$i = 0;
	$sql_r = "select cer_fixed_rent_total_surface, cer_fixed_rent_amount, cer_fixed_rent_unit " . 
		   " from cer_fixed_rents " . 
		   " where cer_fixed_rent_cer_version = " . $cer_version . 
		   " and cer_fixed_rent_project_id = " . param("pid") . 
		   " order by cer_fixed_rent_from_year ASC ";
	
	$res_r = mysql_query($sql_r) or dberror($sql_r);
	while ($row_r = mysql_fetch_assoc($res_r))
	{
		$amount = $row_r['cer_fixed_rent_amount'];

		if($i > 0 and $_tmp > 0) {
			$_incr = ($amount - $_tmp)/$_tmp;
			$average_yearly_increase = $average_yearly_increase + $_incr;
		}
		$_tmp = $amount;
		$i++;
	}

	if($i > 1) {
		$average_yearly_increase = round(100*($average_yearly_increase / ($i-1)), 2);
	}
}

//additional rental cost and other fees
//get additional rental costs
$annual_charges = 0;
$tax_on_rents = 0;
$passenger_index = 0;
$savings_on_rent = 0;
$sql_e = "select * from cer_expenses " .
         "where cer_expense_cer_version = " . $cer_version . "  and cer_expense_project = " . param("pid") . 
		 " and cer_expense_type in (3,18,19, 20) " . 
		 " and cer_expense_year = " . dbquote($first_full_year) . 
		 " order by cer_expense_year";
$res_e = mysql_query($sql_e) or dberror($sql_e);

while($row_e = mysql_fetch_assoc($res_e))
{
	if($row_e["cer_expense_type"] == 3)
	{
		$annual_charges = round($row_e["cer_expense_amount"]/1000, 0);
	}
	elseif($row_e["cer_expense_type"] == 18)
	{
		$tax_on_rents = round($row_e["cer_expense_amount"]/1000, 0);
	}
	elseif($row_e["cer_expense_type"] == 19)
	{
		$passenger_index = round($row_e["cer_expense_amount"]/1000, 0);
	}
	elseif($row_e["cer_expense_type"] == 20)
	{
		$savings_on_rent = round($row_e["cer_expense_amount"]/1000, 0);
	}
}


//turnover percent for rents, only first record
$sales_percent_first_year = "";
$sql_e = "select * " .
	   "from cer_rent_percent_from_sales " .
	   "where cer_rent_percent_from_sale_cer_version = " . $cer_version . "  and cer_rent_percent_from_sale_project = " . param("pid") .
	   " and cer_rent_percent_from_sale_from_year = " . dbquote($first_full_year) .
	   " order by cer_rent_percent_from_sale_from_year";
$res_e = mysql_query($sql_e) or dberror($sql_e);
if($row_e = mysql_fetch_assoc($res_e))
{
	$sales_percent_first_year = round($row_e["cer_rent_percent_from_sale_percent"], 2) . "%";
}
else {
	$sql_e = "select * " .
		   "from cer_rent_percent_from_sales " .
		   "where cer_rent_percent_from_sale_cer_version = " . $cer_version . "  and cer_rent_percent_from_sale_project = " . param("pid") .
		   " order by cer_rent_percent_from_sale_from_year";
	$res_e = mysql_query($sql_e) or dberror($sql_e);
	if($row_e = mysql_fetch_assoc($res_e))
	{
		$sales_percent_first_year = round($row_e["cer_rent_percent_from_sale_percent"], 2) . "%";
	}
}

//sales guarantee
$sales_guarantee_first_year = "";
$sql_e = "select cer_fixed_rent_sales_guarantee_amount " .
	   "from cer_fixed_rents " .
	   "where cer_fixed_rent_cer_version = " . $cer_version . "  and cer_fixed_rent_project_id = " . param("pid") .
	   " and cer_fixed_rent_from_year = " . dbquote($first_full_year);
$res_e = mysql_query($sql_e) or dberror($sql_e);
if($row_e = mysql_fetch_assoc($res_e))
{
	$sales_guarantee_first_year = round($row_e["cer_fixed_rent_sales_guarantee_amount"]/1000, 0);
}
else {
	$sql_e = "select cer_fixed_rent_sales_guarantee_amount " .
		   "from cer_fixed_rents " .
		   "where cer_fixed_rent_cer_version = " . $cer_version . "  and cer_fixed_rent_project_id = " . param("pid");
	$res_e = mysql_query($sql_e) or dberror($sql_e);
	if($row_e = mysql_fetch_assoc($res_e))
	{
		$sales_guarantee_first_year = round($row_e["cer_fixed_rent_sales_guarantee_amount"]/1000, 0);
	}
}

//sellouts
$sellout_ending_year = (int)substr($cer_basicdata["date_created"], 0, 4);

if($sellout_ending_year < (int)substr($cer_basicdata["cer_basicdata_firstyear"], 0, 4))
{
	$sellout_ending_year =(int)substr($cer_basicdata["cer_basicdata_firstyear"], 0, 4);
}

if($sellout_ending_year > date("Y"))
{
	$sellout_ending_year = date("Y");
}

$sellout_starting_year = $sellout_ending_year - 3;

$currency_symbol = get_currency_symbol($ln_basicdata["ln_basicdata_currency"]);


//approval names
$approval_name1 = $cer_basicdata["cer_basicdata_approvalname1"];
$approval_name2 = $cer_basicdata["cer_basicdata_approvalname2"];
$approval_name3 = $cer_basicdata["cer_basicdata_approvalname3"];
$approval_name4 = $cer_basicdata["cer_basicdata_approvalname4"];
$approval_name5 = $cer_basicdata["cer_basicdata_approvalname5"];
$approval_name6 = $cer_basicdata["cer_basicdata_approvalname6"];
$approval_name7 = $cer_basicdata["cer_basicdata_approvalname7"];
$approval_name8 = $cer_basicdata["cer_basicdata_approvalname8"];
$approval_name9 = $cer_basicdata["cer_basicdata_approvalname9"];
$approval_name10 = $cer_basicdata["cer_basicdata_approvalname10"];

$approval_name11 = $cer_basicdata["cer_summary_in01_sig01"];
$approval_name12 = $cer_basicdata["cer_summary_in01_sig02"];
$approval_name13 = $cer_basicdata["cer_summary_in01_sig03"];
$approval_name14 = $cer_basicdata["cer_summary_in01_sig05"];
$approval_name15 = $cer_basicdata["cer_summary_in01_sig07"];
$approval_name16 = $cer_basicdata["cer_summary_in01_sig10"];


//documents and pictures
$pix1 = ".." . $ln_basicdata["ln_basicdata_pix1"];

// Create and setup PDF document

$pdf = new TCPDF("L", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 20, 10);
$pdf->setPrintHeader(false);

$pdf->SetTitle("Retail Lease Negotiation");
$pdf->SetAuthor( BRAND . " Retailnet");
$pdf->SetDisplayMode(100);
$pdf->SetAutoPageBreak(false, 0);


$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');
$pdf->Open();



include("lnr_01_pdf_detail.php");
if($project["project_cost_type"] == 1 and $project["project_projectkind"] == 5) //corporate lease renewal
{
	include("lnr_02Abis_pdf_detail.php");
}
elseif($project["project_cost_type"] == 1) //corporate
{
	include("lnr_02A_pdf_detail.php");
}
elseif($project["project_cost_type"] == 2) //franchisee
{
	include("lnr_02B_pdf_detail.php");
}
include("lnr_03_pdf_detail.php");


$file_name = BRAND . "_ln_application_form_" . $project["project_number"] . ".pdf";

$pdf->Output($file_name);

?>