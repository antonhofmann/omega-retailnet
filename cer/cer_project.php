<?php
/********************************************************************

    cer_project.php

    Project Data

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";
require_once "../shared/project_cost_functions.php";


check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}


//check if basic data exists for this project
$result = check_cer_summary(param("pid"));
$result = check_basic_data(param("pid"));

require "include/get_project.php";
require_once "../shared/func_posindex.php";


if($cer_basicdata["cer_basicdata_lastyear"] > 0) {
	$result = build_missing_cer_investment_records(param("pid"));
	if($project["order_budget_is_locked"] != 1) // budget locked
	{
		$order_currency = get_order_currency($project["project_order"]);
		$budget = get_project_budget_totals(param("pid"), $order_currency);
		$result = update_investments_from_project_cost_sheet(param("pid"), $budget, 0);
	}
}


//update investments with default depreciation in case of a franchisee SIS


if(($cer_basicdata["cer_basicdata_firstyear_depr"] == 0
   or $cer_basicdata["cer_basicdata_firstyear_depr"] == '')
   and $project['project_cost_type'] != 1
   and $cer_basicdata["cer_basicdata_firstyear_depr"] > 0
   and $cer_basicdata["cer_basicdata_firstmonth"] > 0) {

 
	$sql = "update cer_basicdata set 
	        cer_basicdata_firstyear_depr =  " . $cer_basicdata["cer_basicdata_firstyear"] . ", " . 
			"cer_basicdata_firstmonth_depr =  " . $cer_basicdata["cer_basicdata_firstmonth"] . 
			" where cer_basicdata_version = 0 and cer_basicdata_project = " . $project['project_id'];
	
	$result = mysql_query($sql) or dberror($sql);


	$sql = "select cer_investment_id 
	        from cer_investments
			where cer_investment_cer_version = 0 
			and cer_investment_depr_years = 0
			and cer_investment_depr_months = 0
			and cer_investment_project = " . $project['project_id'];


	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$sql_u = "update cer_investments set 
				cer_investment_depr_years =  3, " . 
				"cer_investment_depr_months =  0 " .
				" where cer_investment_type<> 18 
				   and cer_investment_cer_version = 0 and cer_investment_project = " . $project['project_id'];
		
		$result = mysql_query($sql_u) or dberror($sql_u);
	}
}

/********************************************************************
    prepare all data needed
*********************************************************************/
$client_address = get_address($project["order_client_address"]);

$client = $client_address["company"] . ", " .
		  $client_address["zip"] . " " .
          $client_address["place"] . ", " .
          $client_address["country_name"];

$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_address"] . ", " .
		$project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];



$submitted_by = get_user($cer_basicdata["cer_basicdata_submitted_by"]);
$resubmitted_by = get_user($cer_basicdata["cer_basicdata_resubmitted_by"]);
$rejected_by = get_user($cer_basicdata["cer_basicdata_rejected_by"]);


$ln_basicdata = get_ln_basicdata(param("pid"));



$sales_reduction_watches = array();

foreach($cer_brands as $brand_id=>$brand_name)
{
	$sql_s = "select cer_revenue_sales_reduction_watches " . 
			 "from cer_revenues " . 
			 "where cer_revenue_cer_version = 0 " . 
		     " and cer_revenue_brand_id = " . $brand_id . 
		     " and cer_revenue_project = " . dbquote(param("pid"));

	$res_s = mysql_query($sql_s) or dberror($sql_s);
	if($row_s = mysql_fetch_assoc($res_s))
	{
		$sales_reduction_watches[$brand_id] = $row_s["cer_revenue_sales_reduction_watches"];
	}
}



//add approval names
if($project["order_actual_order_state_code"] < '820' and ($project["order_archive_date"] == NULL or $project["order_archive_date"] == '0000-00-00'))
{
	if($cer_basicdata["cer_basicdata_approvalname1"] == '' or $cer_basicdata["cer_basicdata_approvalname10"] == '' or $cer_basicdata["cer_basicdata_approvalname3"] == '')
	{
		
		$sql = "select * from cer_approvalnames where cer_approvalname_id = 1";
		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$fields = array();

			$value = dbquote($row["cer_approvalname_name1"]);
			$fields[] = "cer_basicdata_approvalname1 = " . $value;

			$value = dbquote($row["cer_approvalname_name2"]);
			$fields[] = "cer_basicdata_approvalname2 = " . $value;

			$value = dbquote($row["cer_approvalname_name3"]);
			$fields[] = "cer_basicdata_approvalname3 = " . $value;

			$value = dbquote($row["cer_approvalname_name4"]);
			$fields[] = "cer_basicdata_approvalname4 = " . $value;

			$value = dbquote($row["cer_approvalname_name5"]);
			$fields[] = "cer_basicdata_approvalname5 = " . $value;

			$value = dbquote($row["cer_approvalname_name6"]);
			$fields[] = "cer_basicdata_approvalname6 = " . $value;

			$value = dbquote($row["cer_approvalname_name7"]);
			$fields[] = "cer_basicdata_approvalname7 = " . $value;

			$value = dbquote($row["cer_approvalname_name8"]);
			$fields[] = "cer_basicdata_approvalname8 = " . $value;

			$value = dbquote($row["cer_approvalname_name9"]);
			$fields[] = "cer_basicdata_approvalname9 = " . $value;

			$value = dbquote($row["cer_approvalname_name10"]);
			$fields[] = "cer_basicdata_approvalname10 = " . $value;

			$value = dbquote($row["cer_approvalname_name11"]);
			$fields[] = "cer_basicdata_approvalname11 = " . $value;

			$sql = "update cer_basicdata set " . join(", ", $fields) . " where cer_basicdata_version = 0 and cer_basicdata_project = " . param("pid");
			mysql_query($sql) or dberror($sql);
		}
		
	}

	if($cer_basicdata["cer_basicdata_approvalname11"] == '')
	{
		
		$sql = "select * from cer_approvalnames where cer_approvalname_id = 1";
		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$fields = array();

			
			$value = dbquote($row["cer_approvalname_name11"]);
			$fields[] = "cer_basicdata_approvalname11 = " . $value;

			$sql = "update cer_basicdata set " . join(", ", $fields) . " where cer_basicdata_version = 0 and cer_basicdata_project = " . param("pid");
			mysql_query($sql) or dberror($sql);
		}
		
	}
}



$sql = "select * " . 
       "from cer_summary " . 
	   "where cer_summary_cer_version = 0 and cer_summary_project = " . dbquote(param("pid"));

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	//counttry manager
	if(!$row["cer_summary_in01_sig01"]) {

		$fm = get_country_manager($project["order_client_address"], $project["order_shop_address_country"]);

		if(array_key_exists('user_name', $fm)) {
			$fields = array();

			$value = dbquote($fm["user_name"] . ", " . $fm["user_firstname"]);
			$fields[] = "cer_summary_in01_sig01 = " . $value;

			$sql = "update cer_summary set " . join(", ", $fields) . " where cer_summary_project = " . dbquote(param("pid"));
			mysql_query($sql) or dberror($sql);
		}
	}
	
	//finance manager
	if(!$row["cer_summary_in01_sig02"]) {

		$fm = get_finance_manager($project["order_client_address"], $project["order_shop_address_country"]);

		if(array_key_exists('user_name', $fm)) {
			$fields = array();

			$value = dbquote($fm["user_name"] . ", " . $fm["user_firstname"]);
			$fields[] = "cer_summary_in01_sig02 = " . $value;

			$sql = "update cer_summary set " . join(", ", $fields) . " where cer_summary_project = " . dbquote(param("pid"));
			mysql_query($sql) or dberror($sql);
		}
	}

	//brand manager
	if(!$row["cer_summary_in01_sig03"]) {

		$bm = get_brand_manager($project["order_client_address"], $project["order_shop_address_country"]);
		
		if(array_key_exists('user_name', $bm)) {
			$fields = array();

			$value = dbquote($bm["user_name"] . ", " . $bm["user_firstname"]);
			$fields[] = "cer_summary_in01_sig03 = " . $value;

			$sql = "update cer_summary set " . join(", ", $fields) . " where cer_summary_project = " . dbquote(param("pid"));
			mysql_query($sql) or dberror($sql);
		}
	}

	//head fo controlling for china projects
	if(!$row["cer_summary_in01_sig10"]) {

		$bm = get_head_of_controlling($project["order_client_address"], $project["order_shop_address_country"]);
		
		if(array_key_exists('user_name', $bm)) {
			$fields = array();

			$value = dbquote($bm["user_name"] . ", " . $bm["user_firstname"]);
			$fields[] = "cer_summary_in01_sig10 = " . $value;

			$sql = "update cer_summary set " . join(", ", $fields) . " where cer_summary_project = " . dbquote(param("pid"));
			mysql_query($sql) or dberror($sql);
		}
	}


	//retail manager for china projects
	if(!$row["cer_summary_in01_sig05"]) {

		$bm = get_retail_manager_country($project["order_client_address"], $project["order_shop_address_country"]);
		
		if(array_key_exists('user_name', $bm)) {
			$fields = array();

			$value = dbquote($bm["user_name"] . ", " . $bm["user_firstname"]);
			$fields[] = "cer_summary_in01_sig05 = " . $value;

			$sql = "update cer_summary set " . join(", ", $fields) . " where cer_summary_project = " . dbquote(param("pid"));
			mysql_query($sql) or dberror($sql);
		}
	}

	//regional sales managers 
	if(!$row["cer_summary_in01_sig06"]) {

		$rsm = get_regional_sales_manager($project["order_shop_address_country"]);
		
		if(array_key_exists('user_name', $rsm)) {
			$fields = array();

			$value = dbquote($rsm["user_name"] . ", " . $rsm["user_firstname"]);
			$fields[] = "cer_summary_in01_sig06 = " . $value;

			$sql = "update cer_summary set " . join(", ", $fields) . " where cer_summary_project = " . dbquote(param("pid"));
			mysql_query($sql) or dberror($sql);
		}
	}


}




if($posorder_present == 0) // the project is an old project, not present in posorders nor in posorderspipeline
{
	//add the project to the pipeline
	
	if($project["project_projectkind"] == 1
		or $project["project_projectkind"] == 6) // new project
	{
		
		// posaddresspipeline
		$fields = array();
		$values = array();

		$fields[] = "posaddress_client_id";
		$values[] = dbquote($project["order_client_address"]);

		$fields[] = "posaddress_ownertype";
		$values[] = dbquote($project["project_cost_type"]);

		$fields[] = "posaddress_franchisor_id";
		$values[] = 13;

		$fields[] = "posaddress_franchisee_id";
		$values[] = dbquote($project["order_franchisee_address_id"]);

		$fields[] = "posaddress_name";
		$values[] = dbquote($project["order_shop_address_company"]);

		$fields[] = "posaddress_address";
		$values[] = dbquote($project["order_shop_address_address"]);

		$fields[] = "posaddress_address2";
		$values[] = dbquote($project["order_shop_address_address2"]);

		$fields[] = "posaddress_zip";
		$values[] = dbquote($project["order_shop_address_zip"]);

		$fields[] = "posaddress_place";
		$values[] = dbquote($project["order_shop_address_place"]);

		$fields[] = "posaddress_country";
		$values[] = dbquote($project["order_shop_address_country"]);
		
		$fields[] = "posaddress_phone";
		$values[] = dbquote($project["order_shop_address_phone"]);

		$fields[] = "posaddress_fax";
		$values[] = dbquote($project["order_shop_address_fax"]);

		$fields[] = "posaddress_email";
		$values[] = dbquote($project["order_shop_address_email"]);

		$fields[] = "posaddress_store_postype";
		$values[] = dbquote($project["project_postype"]);

		$fields[] = "posaddress_store_subclass";
		$values[] = dbquote($project["project_pos_subclass"]);

		$fields[] = "posaddress_store_furniture";
		$values[] = dbquote($project["project_product_line"]);

		$fields[] = "posaddress_store_retailarea";
		$values[] = dbquote($project["project_cost_sqms"]);

		
		$sql = "insert into posaddressespipeline (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
	    mysql_query($sql) or dberror($sql);

		$posaddress_id = mysql_insert_id();
		

		//posorderspipeline
		$fields = array();
		$values = array();

		$fields[] = "posorder_parent_table";
		$values[] = dbquote("posaddressespipeline");

		$fields[] = "posorder_posaddress";
		$values[] = dbquote($posaddress_id);

		$fields[] = "posorder_order";
		$values[] = dbquote($project["project_order"]);

		$fields[] = "posorder_type";
		$values[] = 1;

		$project_number = $project["project_number"];
		$fields[] = "posorder_ordernumber";
		$values[] = dbquote($project_number);

		$year_of_order = substr($project_number,0,2);
		if($year_of_order > 30 and $year_of_order <= 99)
		{
			$year_of_order = "19" . $year_of_order;
		}
		else
		{
			$year_of_order = substr($year_of_order, 0, 1) . '00' . substr($year_of_order, 1, 1);
		}

		if(substr($project_number,3,1) == '.')
		{
			$year_of_order = substr($project_number,0,1) . '0' . substr($project_number,1,2);
		}

		$fields[] = "posorder_year";
		$values[] = dbquote($year_of_order);

		$fields[] = "posorder_product_line";
		$values[] = dbquote($project["project_product_line"]);

		$fields[] = "posorder_postype";
		$values[] = dbquote($project["project_postype"]);

		$fields[] = "posorder_subclass";
		$values[] = dbquote($project["project_pos_subclass"]);

		$fields[] = "posorder_project_kind";
		$values[] = dbquote($project["project_projectkind"]);

		$fields[] = "posorder_legal_type";
		$values[] = dbquote($project["project_cost_type"]);

		
		$fields[] = "date_created";
		$values[] = dbquote(date("Y-m-d"));

		$fields[] = "date_modified";
		$values[] = dbquote(date("Y-m-d"));

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$fields[] = "user_modified";
		$values[] = dbquote(user_login());
		
		$sql = "insert into posorderspipeline (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
	    mysql_query($sql) or dberror($sql);

		
		$posorder_present = 1;
	}
}

if($posorder_present == 1)
{
	$posdata = get_pos_data($project["project_order"]);
	$posleases = get_pos_leasedata($posdata["posorder_posaddress"], $posdata["posorder_order"]);
	$construction = get_pos_intangibles(param("pid"), 1);
	$fixturing = get_pos_intangibles(param("pid"), 3);
	$keymoney = get_pos_intangibles(param("pid"), 15);
}

$revenues = 0;
$sql = "select sum(cer_revenue_watches) as total " .
       "from cer_revenues " .
	   " where cer_revenue_cer_version = 0 and cer_revenue_project = " . param("pid");

$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	$revenues = $row["total"];
}

$expenses = 0;
$sql = "select sum(cer_expense_amount) as total " .
       "from cer_expenses " .
	   " where cer_expense_cer_version = 0 and cer_expense_project = " . param("pid");

$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	$expenses = $row["total"];
}

//check if all necessary expenses are filled in
//Material Cost, Salaries, Aux. Material and Energy 
if($expenses > 0)
{
	$sql = "select count(cer_expense_amount) as num_recs " .
		   "from cer_expenses " .
	       " where cer_expense_cer_version = 0 and cer_expense_project = " . param("pid") . 
		   " and cer_expense_type IN(1, 4, 15) " . 
		   " and cer_expense_amount = 0";

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	if($row['num_recs'] > 0) {$expenses = 0;}

}


//get fixed rents
$fixed_rents = 0;
$sql = "select sum(cer_expense_amount) as amount " .
	   "from cer_expenses " .
	   " where cer_expense_cer_version = 0 and cer_expense_project = " . param("pid") . 
	   " and cer_expense_type IN(2) ";

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);
$fixed_rents = $row['amount'];


if($expenses > 0)
{
	$tmp1 = 1;
	$years_with_zero_values = array();
	$sql = "select cer_expense_year, sum(cer_expense_amount) as cer_expense_amount " .
		   "from cer_expenses " .
	       " where cer_expense_cer_version = 0 and cer_expense_project = " . param("pid") . 
		   " and cer_expense_type in (2) " . 
		   " and cer_expense_amount = 0 " . 
		   " group by cer_expense_year";


	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$tmp1 = 0;
		$years_with_zero_values[] = $row['cer_expense_year'];
	}

	if($tmp1 == 0)
	{
	
		$tmp2 = 1;
		foreach($years_with_zero_values as $key=>$year)
		{
			$sql = "select count(cer_expense_amount) as num_recs " .
				   "from cer_expenses " .
				   " where cer_expense_cer_version = 0 and cer_expense_project = " . param("pid") . 
				   " and cer_expense_type = 16 " . 
				   " and cer_expense_amount = 0 " .
				   " and cer_expense_year = " . dbquote($year);


			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);
			if($row['num_recs'] > 0) {$tmp2 = 0;}

			if($tmp2 == 0) {$expenses = 0;}
		}
	}
}


$salaries = 0;
$sql = "select sum(cer_salary_fixed_salary) as total " .
       "from cer_salaries " .
	   " where cer_salary_cer_version = 0 and cer_salary_project = " . param("pid");

$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	$salaries = $row["total"];
}

$other_sales_information = 1;
$sql = "select count(cer_revenue_id) as num_recs " .
	   "from cer_revenues " .
	   " where cer_revenue_cer_version = 0 and cer_revenue_project = " . param("pid") . 
	   " and (cer_revenue_total_days_open_per_year = 0 " . 
	   " or cer_revenue_total_hours_open_per_week = 0 " . 
	   " or cer_revenue_total_workinghours_per_week = 0)";

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);
if($row['num_recs'] > 0) {$other_sales_information = 0;}
	

$stock_data = 1;
$sql = "select count(cer_stock_id) as num_recs " .
	   "from cer_stocks " .
	   " where cer_stock_cer_version = 0 and cer_stock_project = " . param("pid") . 
	   " and cer_stock_stock_in_months = 0";

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);
if($row['num_recs'] > 0) {$stock_data = 0;}


$payment_terms = 1;
$sql = "select count(cer_paymentterm_id) as num_recs " .
	   "from cer_paymentterms " .
	   " where cer_paymentterm_cer_version = 0 and cer_paymentterm_project = " . param("pid") . 
	   " and cer_paymentterm_in_months = 0";

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);
if($row['num_recs'] > 0) {$payment_terms = 0;}


$sellout_necessary = 0;
$sellout_data_ok = true;
$project_id = param("pid");
$project_order = $project["project_order"];

if(($project["project_projectkind"] == 2 
        or $project["project_projectkind"] == 3 
		or $project["project_projectkind"] == 5)) // renovation or tekover/renovation or lease renewal
{
	$sellout_necessary = 1;
}
elseif($project["project_projectkind"] == 6 and $project["project_is_relocation_project"] == 1 and $project["project_relocated_posaddress_id"] > 0) // relocation
{
	$latest_project = get_latest_pos_project($project["project_relocated_posaddress_id"]);
	
	if(count($latest_project) > 0)
	{
		$sellout_necessary = 1;
		$project_id = $latest_project["project_id"];
		$project_order = $latest_project["order_id"];
	}
}

if($sellout_necessary == 1)
{
	$sellout_data_ok = check_sellout_data($project_order, $cer_basicdata["cer_basicdata_firstyear"]);
}


//check if neighbourhood is filled in
//neighbourhood
if($project["pipeline"] == 0)
{
	$pos_order_sql = "select * from posorders where posorder_order = " . $project["order_id"];
}
elseif($project["pipeline"] == 1)
{
	$pos_order_sql = "select * from posorderspipeline where posorder_order = " . $project["order_id"];
}

$pos_type = 0;
$neighbourhood = 0;
$res = mysql_query($pos_order_sql) or dberror($pos_order_sql);
if ($row = mysql_fetch_assoc($res))
{
	if($row["posorder_neighbour_left"]
	   and $row["posorder_neighbour_right"]
	   and $row["posorder_neighbour_acrleft"]
	   and $row["posorder_neighbour_acrright"]
	   and $row["posorder_neighbour_brands"])
	   {
	   		$neighbourhood = 1;	
	   }
	
	$pos_type = $row["posorder_postype"];
}	

//ln versus cer
if($ln_basicdata["ln_no_ln_submission_needed"] == 0)
{
	$state = is_there_a_difference_between_cer_and_ln(param("pid"));
	$difference_between_cer_and_ln = $state['state'];

	$delta_limit_in_percent = 0.05;
	$investments_delta = round($state['total_investments_cer'] - $state['total_investments_ln'],0);

	$investments_delta_limit = 100;
	
	if($investments_delta == 0)
	{
		$investments_delta_limit = 0;
	}
	elseif($state['total_investments_cer'])
	{
		$investments_delta_limit = abs($state['total_investments_cer'] - $state['total_investments_ln']) / $state['total_investments_cer'];
		$investments_delta_limit = round(100*$investments_delta_limit, 0);

		if($investments_delta < 0 or abs($investments_delta_limit) < 100*$delta_limit_in_percent)
		{
			$investments_delta_limit = 0;
		}
	}



	$revenues_delta = round($state['total_revenues_cer'] - $state['total_revenues_ln'],0);

	$revenues_delta_limit = 100;
	if($revenues_delta == 0)
	{
		$revenues_delta_limit = 0;
	}
	elseif($state['total_revenues_cer'])
	{
		$revenues_delta_limit = abs($state['total_revenues_cer'] - $state['total_revenues_ln']) / $state['total_revenues_cer'];
		$revenues_delta_limit = round(100*$revenues_delta_limit, 0);

		if(abs($revenues_delta_limit) < 100*$delta_limit_in_percent)
		{
			$revenues_delta_limit = 0;
		}
	}

	$expenses_delta = round($state['total_expenses_cer'] - $state['total_expenses_ln'],0);

	$expenses_delta_limit = 100;
	if($expenses_delta == 0)
	{
		$expenses_delta_limit = 0;
	}
	elseif($state['total_expenses_cer'])
	{
		$expenses_delta_limit = abs($state['total_expenses_cer'] - $state['total_expenses_ln']) / $state['total_expenses_cer'];
		$expenses_delta_limit = round(100*$expenses_delta_limit, 0);

		if(abs($expenses_delta_limit) < 100*$delta_limit_in_percent)
		{
			$expenses_delta_limit = 0;
		}
	}
}
else
{
	$investments_delta_limit = 0;
	$revenues_delta_limit = 0;
	$expenses_delta_limit = 0;
}

$ok = "<img style='padding-top:4px;' src='../pictures/ok.gif' />";
$not_ok = "<img style='padding-top:4px;' src='../pictures/not_ok.gif' />";
$number_of_criteria_to_submit = 0;


//Distibution Analysis
$distribution_analysis = true;

//get the number of operating POS locations
$num_of_operating_pos_locations = 0;
$sql = "select count(posaddress_id) as num_recs " . 
       " from posaddresses " . 
	   " where posaddress_client_id = " . dbquote($client_address["id"]) . 
	   " and (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') " . 
	   " and posaddress_country = " . dbquote($project["order_shop_address_country"]);

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);
$num_of_operating_pos_locations = $row['num_recs'];

if($form_type == "CER" or $form_type == "AF")
{
	$sql = "select count(ln_basicdata_lnr03_id) as num_recs " .
		   "from ln_basicdata_inr03 " .
		   " where ln_basicdata_lnr03_lnbasicdata_id = " . $ln_basicdata['ln_basicdata_id'] .
		   " and ln_basicdata_lnr03_ln_version = 0 ";

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	if($row['num_recs'] == 0) {
		$distribution_analysis = false;
	}
	elseif($num_of_operating_pos_locations >= 10 and $row['num_recs'] < 10)
	{
		$distribution_analysis = false;

		
	}
	elseif($row['num_recs'] < 10 and $row['num_recs'] < $num_of_operating_pos_locations)
	{
		$distribution_analysis = false;
	}
}




//other sg brands in the ares
$distribution_analysis3 = true;

	
$sql = "select count(ln_basicdata_lnr03_brand_id) as num_recs " . 
	   " from ln_basicdata_lnr03_brands " . 
		" where ln_basicdata_lnr03_brand_cerbasicdata_id = " . $cer_basicdata["cer_basicdata_id"] .
		" and ln_basicdata_lnr03_brand_cer_version = 0 ";

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

if($row['num_recs'] == 0 and $cer_basicdata["cer_basicdata_no_sg_brands"] == 0) {
	$distribution_analysis3 = false;
}

//check if LN was submitted
if($form_type == "CER" 
	and $ln_basicdata["ln_no_ln_submission_needed"] == 1 
	or ($ln_basicdata["ln_basicdata_submitted"] != NULL 
	and substr($ln_basicdata["ln_basicdata_submitted"],0,4) != '0000'
	and $ln_basicdata["ln_basicdata_rejected"] == NULL 
	and substr($ln_basicdata["ln_basicdata_rejected"],0,4) == '0000')
	)
{
	$distribution_analysis3 = true;
}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_basicdata", "cer_basicdata");


include("include/project_head.php");

$form->add_hidden("pid", param("pid"));


$form->add_section(" ");


if($project["project_cost_type"] == 2 or $project["project_cost_type"] == 5) // franchisee
{
	$submission_possible = 0;
	$form->add_section("AF Approval Form: Overview");

	if($posorder_present == 0)
	{
		$form->add_label("s1", "General Information", RENDER_HTML, $not_ok);
		$form->add_label("s3", "Franchisee Information", RENDER_HTML, $not_ok);
		$form->add_label("s4", "Rental Details", RENDER_HTML, $not_ok);
		$form->add_label("s5", "Key Money/Investment", RENDER_HTML, $not_ok);
		$form->add_label("s6", "Revenues", RENDER_HTML, $not_ok);
		$form->add_label("s7", "Human Resources", RENDER_HTML, $not_ok);
		$form->add_label("s8", "Expenses", RENDER_HTML, $not_ok);
		$form->add_label("s8", "Cash Flow Calculation", RENDER_HTML, $not_ok);
	}
	elseif($cer_basicdata["cer_basicdata_lastyear"] == NULL or $cer_basicdata["cer_basicdata_lastyear"] == "0000-00-00")
	{
		$form->add_label("s1", "General Information", RENDER_HTML, $not_ok);
		$form->add_label("s3", "Franchisee Information", RENDER_HTML, $not_ok);
		$form->add_label("s4", "Rental Details", RENDER_HTML, $not_ok);
		$form->add_label("s5", "Key Money/Investment", RENDER_HTML, $not_ok);
		$form->add_label("s6", "Revenues", RENDER_HTML, $not_ok);
		$form->add_label("s7", "Human Resources", RENDER_HTML, $not_ok);
		$form->add_label("s8", "Expenses", RENDER_HTML, $not_ok);
		$form->add_label("s8", "Cash Flow Calculation", RENDER_HTML, $not_ok);
	}
	else
	{
		$form->add_label("s1", "General Information", RENDER_HTML, $ok);
		
		//if($posdata["posaddress_fag_city_pasted"] and $ln_basicdata["ln_basicdata_floorplan"])
		if($ln_basicdata["ln_basicdata_floorplan"] 
			and $ln_basicdata["ln_basicdata_pix1"]
			and $ln_basicdata["ln_basicdata_pix2"]
			and $ln_basicdata["ln_basicdata_pix3"])
		{
			$form->add_label("s3", "Franchisee Information", RENDER_HTML, $ok);
			$submission_possible++;
		}
		else
		{
			$form->add_label("s3", "Franchisee Information", RENDER_HTML, $not_ok);
		}


		if(!isset($posleases["poslease_startdate"]) or $posleases["poslease_startdate"] == NULL or $posleases["poslease_startdate"] == '0000-00-00')
		{
			$form->add_label("s4", "Rental Details", RENDER_HTML, $not_ok);
		}
		else
		{
			$form->add_label("s4", "Rental Details", RENDER_HTML, $ok);
			$submission_possible++;
		}

		
		if((array_key_exists("cer_investment_amount_cer_loc", $construction) and $construction["cer_investment_amount_cer_loc"] > 0) or (array_key_exists("cer_investment_amount_cer_loc", $fixturing) and $fixturing["cer_investment_amount_cer_loc"] > 0)  or (array_key_exists("cer_investment_amount_cer_loc", $keymoney) and $keymoney["cer_investment_amount_cer_loc"] > 0))
		{
			$form->add_label("s5", "Key Money/Investment", RENDER_HTML, $ok);
			$submission_possible++;
		}
		else
		{
			$form->add_label("s5", "Key Money/Investment", RENDER_HTML, $not_ok);
		}

		foreach($cer_brands as $brand_id=>$brand_name)
		{
			if($revenues > 0 and $sales_reduction_watches[$brand_id] > 0)
			{
				$form->add_label("s6_" . $brand_id, "Revenues " . $brand_name, RENDER_HTML, $ok);
				$submission_possible++;
			}
			else
			{
				$form->add_label("s6_" . $brand_id, "Revenues " . $brand_name, RENDER_HTML, $not_ok);
			}
		}


		if($other_sales_information > 0)
		{
			$form->add_label("s6a", "Other Sales Relevant Information", RENDER_HTML, $ok);
			$submission_possible++;
		}
		else
		{
			$form->add_label("s6a", "Other Sales Relevant Information", RENDER_HTML, $not_ok);
		}



		if($salaries > 0)
		{
			$form->add_label("s7", "Human Resources", RENDER_HTML, $ok);
			$submission_possible++;
		}
		else
		{
			$form->add_label("s7", "Human Resources", RENDER_HTML, $not_ok);
		}
		

		if($fixed_rents == 0 and count($posleases) > 0 and $posleases["poslease_hasfixrent"] == 1)
		{
			$form->add_label("s8", "Expenses", RENDER_HTML, $not_ok);
		}
		elseif($fixed_rents > 0 and count($posleases) > 0 and $posleases["poslease_hasfixrent"] == 0)
		{
			$form->add_label("s8", "Expenses", RENDER_HTML, $not_ok);
		}
		elseif($expenses > 0)
		{
			$form->add_label("s8", "Expenses", RENDER_HTML, $ok);
			$submission_possible++;
		}
		else
		{
			$form->add_label("s8", "Expenses", RENDER_HTML, $not_ok);
		}



		


		if($sellout_necessary == 1 and $sellout_data_ok == true)
		{
			$form->add_label("s10", "Sellout Data", RENDER_HTML, $ok);
		}
		elseif($sellout_necessary == 1 and $sellout_data_ok == false)
		{
			$form->add_label("s10", "Sellout Data", RENDER_HTML, $not_ok);
		}

		
		$number_of_criteria_to_submit = 7;
		
		if($pos_type == 1 or $pos_type == 3) //store or kiosk
		{
			$number_of_criteria_to_submit = 8;
			if($neighbourhood == 1)
			{
				$form->add_label("s11", "Neighbourhood", RENDER_HTML, $ok);
				$submission_possible++;
				
			}
			else
			{
				$form->add_label("s11", "Neighbourhood", RENDER_HTML, $not_ok);
			}
		}


		if(($project["project_projectkind"] == 1 
			or $project["project_projectkind"] == 2 
			or $project["project_projectkind"] == 3 
			or $project["project_projectkind"] == 6)) // new, renovation, takeover/renovation, relocation
		{
			if($distribution_analysis == true)
			{
				$form->add_label("s12", "Distribution Analysis", RENDER_HTML, $ok);
			}
			elseif($distribution_analysis == false)
			{
				$form->add_label("s12", "Distribution Analysis", RENDER_HTML, $not_ok);
			}
			

			if($distribution_analysis3 == true)
			{
				$form->add_label("s13", "Other SG Brands", RENDER_HTML, $ok);
			}
			elseif($distribution_analysis3 == false)
			{
				$form->add_label("s13", "Other SG Brands", RENDER_HTML, $not_ok);
			}
		}
	}
	
	
	$by = "";
	if($submitted_by["name"])
	{
		$by = " by " . $submitted_by["name"] . " " . $submitted_by["firstname"];
	}
	$form->add_label("date1", "Date of Submission", 0, to_system_date($cer_basicdata["cer_basicdata_submitted"]) . $by);

	
	$by = "";
	if($resubmitted_by["name"])
	{
		$by = " by " . $resubmitted_by["name"] . " " . $resubmitted_by["firstname"];
	}

	$form->add_label("date2", "Date of latest Resubmission", 0, to_system_date($cer_basicdata["cer_basicdata_resubmitted"]) . $by);

	$by = "";
	if($rejected_by["name"])
	{
		$by = " by " . $rejected_by["name"] . " " . $rejected_by["firstname"];
	}

	$form->add_label("date3", "Date of Rejection", 0, to_system_date($cer_basicdata["cer_basicdata_rejected"]) . $by);

	if(has_access("has_access_to_his_cer") or has_access("has_full_access_to_cer"))
	{
		$number_of_criteria_to_submit = $number_of_criteria_to_submit-1 + count($cer_brands);
		
		/*temporarily deactivated
		if($submission_possible ==  $number_of_criteria_to_submit and $project["project_cost_type"] == 2 or $project["project_cost_type"] == 5) // franchisee, cooperation
		{
		*/

		if($project["project_cost_type"] == 2 or $project["project_cost_type"] == 5) // franchisee, cooperation
		{
			
			if($cer_basicdata["cer_basicdata_submitted"] != NULL and $cer_basicdata["cer_basicdata_submitted"] != "0000-00-00")
			{
				$button_Text= "Resubmit Application Form";
				$sellout_necessary = 0;
			}
			else
			{
				$button_Text= "Submit Application Form";
			}

			if($cer_basicdata["cer_basicdata_cer_locked"] == 0) {
			
				$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" /><a href=\"#\" onClick=\"button('submit')\">" .  $button_Text . "</a>";
				
				
				if($sellout_necessary == 1 and $sellout_data_ok == true)
				{
					$form->add_label("submit", "Submission", RENDER_HTML, $link);
				}
				elseif($sellout_necessary == 0) 
				{
					$form->add_label("submit", "Submission", RENDER_HTML, $link);
				}
				$form->add_label("submit", "Submission", RENDER_HTML, $link);
			}
		}
	}
}
elseif($project["project_cost_type"] == 1) // corporate
{
	//check ln_data
	$ln_ok = true;
	if($ln_basicdata['ln_no_ln_submission_needed'] != 1 
		and ($ln_basicdata['ln_basicdata_submitted'] ==  NULL or $ln_basicdata['ln_basicdata_submitted'] == '0000-00-00')) 
	{
		$ln_ok = false;
	}

	//check if LN was approved, milestone id 13
	$ln_ok1 = true;
	if($ln_basicdata['ln_no_ln_submission_needed'] != 1 
		and $ln_basicdata['ln_basicdata_submitted'] !=  NULL and $ln_basicdata['ln_basicdata_submitted'] != '0000-00-00') 
	{
		$milestone = get_project_milestone($cer_basicdata["cer_basicdata_project"], 13);
		if(count($milestone) > 0 and ($milestone['project_milestone_date'] == NULL or $milestone['project_milestone_date'] == '0000-00-00'))
		{
			$ln_ok1 = false;
		}
	}
	
	$submission_possible = 0;
	$form->add_section("CER Form: Overview");

	if($posorder_present == 0)
	{
		$form->add_label("s1", "General Information", RENDER_HTML, $not_ok);
		$form->add_label("s3", "Franchisee Information", RENDER_HTML, $not_ok);
		$form->add_label("s4", "Rental Details", RENDER_HTML, $not_ok);
		$form->add_label("s5", "Key Money/Investment", RENDER_HTML, $not_ok);
		$form->add_label("s6", "Revenues", RENDER_HTML, $not_ok);
		$form->add_label("s7", "Human Resources", RENDER_HTML, $not_ok);
		$form->add_label("s8", "Expenses", RENDER_HTML, $not_ok);
		$form->add_label("s8", "Cash Flow Calculation", RENDER_HTML, $not_ok);
	}
	elseif($cer_basicdata["cer_basicdata_lastyear"] == NULL or $cer_basicdata["cer_basicdata_lastyear"] == "0000-00-00")
	{
		$form->add_label("s1", "General Information", RENDER_HTML, $not_ok);
		$form->add_label("s3", "Franchisee Information", RENDER_HTML, $not_ok);
		$form->add_label("s4", "Rental Details", RENDER_HTML, $not_ok);
		$form->add_label("s5", "Key Money/Investment", RENDER_HTML, $not_ok);
		$form->add_label("s6", "Revenues", RENDER_HTML, $not_ok);
		$form->add_label("s7", "Human Resources", RENDER_HTML, $not_ok);
		$form->add_label("s8", "Expenses", RENDER_HTML, $not_ok);
		$form->add_label("s8", "Cash Flow Calculation", RENDER_HTML, $not_ok);
	}
	else
	{
		$form->add_label("s1", "General Information", RENDER_HTML, $ok);


		//if($posdata["posaddress_fag_city_pasted"] and $ln_basicdata["ln_basicdata_floorplan"])
		if($ln_basicdata["ln_basicdata_floorplan"])
		{
			$form->add_label("s3", "Franchisee Information", RENDER_HTML, $ok);
			$submission_possible++;
		}
		else
		{
			$form->add_label("s3", "Franchisee Information", RENDER_HTML, $not_ok);
		}


		if(!isset($posleases["poslease_startdate"]) or $posleases["poslease_startdate"] == NULL or $posleases["poslease_startdate"] == '0000-00-00')
		{
			$form->add_label("s4", "Rental Details", RENDER_HTML, $not_ok);
		}
		else
		{
			$form->add_label("s4", "Rental Details", RENDER_HTML, $ok);
			$submission_possible++;
		}


		if((array_key_exists("cer_investment_amount_cer_loc", $construction) and $construction["cer_investment_amount_cer_loc"] > 0) or (array_key_exists("cer_investment_amount_cer_loc", $fixturing) and $fixturing["cer_investment_amount_cer_loc"] > 0)  or (array_key_exists("cer_investment_amount_cer_loc", $keymoney) and $keymoney["cer_investment_amount_cer_loc"] > 0))
		{
			$form->add_label("s5", "Key Money/Investment", RENDER_HTML, $ok);
			$submission_possible++;
		}
		else
		{
			$form->add_label("s5", "Key Money/Investment", RENDER_HTML, $not_ok);
		}

		
		foreach($cer_brands as $brand_id=>$brand_name)
		{
			if($revenues > 0 and $sales_reduction_watches[$brand_id] > 0)
			{
				$form->add_label("s6_" . $brand_id, "Revenues " . $brand_name, RENDER_HTML, $ok);
				$submission_possible++;
			}
			else
			{
				$form->add_label("s6_" . $brand_id, "Revenues " . $brand_name, RENDER_HTML, $not_ok);
			}
		}

		if($other_sales_information > 0)
		{
			$form->add_label("s6a", "Other Sales Relevant Information", RENDER_HTML, $ok);
			$submission_possible++;
		}
		else
		{
			$form->add_label("s6a", "Other Sales Relevant Information", RENDER_HTML, $not_ok);
		}


		if($salaries > 0)
		{
			$form->add_label("s7", "Human Resources", RENDER_HTML, $ok);
			$submission_possible++;
		}
		else
		{
			$form->add_label("s7", "Human Resources", RENDER_HTML, $not_ok);
		}
		
		
		if($fixed_rents == 0 
			and array_key_exists("poslease_hasfixrent", $posleases) 
			and $posleases["poslease_hasfixrent"] == 1)
		{
			$form->add_label("s8", "Expenses", RENDER_HTML, $not_ok);
		}
		elseif($fixed_rents > 0 
			and array_key_exists("poslease_hasfixrent", $posleases) 
			and  $posleases["poslease_hasfixrent"] == 0)
		{
			$form->add_label("s8", "Expenses", RENDER_HTML, $not_ok);
		}
		elseif($expenses > 0)
		{
			$form->add_label("s8", "Expenses", RENDER_HTML, $ok);
			$submission_possible++;
		}
		else
		{
			$form->add_label("s8", "Expenses", RENDER_HTML, $not_ok);
		}

		if($cer_basicdata["cer_basicdata_dicount_rate"])
		{
			$form->add_label("s9", "Cash Flow Calculation", RENDER_HTML, $ok);
			$submission_possible++;
		}
		else
		{
			$form->add_label("s9", "Cash Flow Calculation", RENDER_HTML, $not_ok);
		}


		if($stock_data)
		{
			$form->add_label("s11", "Stock in Months", RENDER_HTML, $ok);
			$submission_possible++;
		}
		else
		{
			$form->add_label("s11", "Stock in Months", RENDER_HTML, $not_ok);
		}

		if($payment_terms)
		{
			$form->add_label("s12", "Payment Terms in Months for Liabilities", RENDER_HTML, $ok);
			$submission_possible++;
		}
		else
		{
			$form->add_label("s12", "Payment Terms in Months for Liabilities", RENDER_HTML, $not_ok);
		}



		if($sellout_necessary == 1 and $sellout_data_ok == true)
		{
			$form->add_label("s10", "Sellout Data", RENDER_HTML, $ok);
		}
		elseif($sellout_necessary == 1 and $sellout_data_ok == false)
		{
			$form->add_label("s10", "Sellout Data", RENDER_HTML, $not_ok);
		}


		if(($project["project_projectkind"] == 1 
			or $project["project_projectkind"] == 2 
			or $project["project_projectkind"] == 3 
			or $project["project_projectkind"] == 6)) // new, renovation takover/renovation or relocation
		{
			if($distribution_analysis == true)
			{
				$form->add_label("s12", "Distribution Analysis", RENDER_HTML, $ok);
			}
			elseif($distribution_analysis == false)
			{
				$form->add_label("s12", "Distribution Analysis", RENDER_HTML, $not_ok);
			}
			

			if($distribution_analysis3 == true)
			{
				$form->add_label("s13", "Other SG Brands", RENDER_HTML, $ok);
			}
			elseif($distribution_analysis3 == false)
			{
				$form->add_label("s13", "Other SG Brands", RENDER_HTML, $not_ok);
			}
		}


		if($cer_basicdata["cer_basicdata_submitted"] == NULL
			or $cer_basicdata["cer_basicdata_submitted"] == '0000-00-00'
			or (
					$cer_basicdata["cer_basicdata_rejected"] > $cer_basicdata["cer_basicdata_submitted"] and ($cer_basicdata["cer_basicdata_resubmitted"] == null or $cer_basicdata["cer_basicdata_resubmitted"] == '0000-00-00')
				)
			or $cer_basicdata["cer_basicdata_rejected"] > $cer_basicdata["cer_basicdata_resubmitted"]
			)
		{
			
			$error_number = 0;
			
			/*
			if($ln_ok1 == true and $project["project_cost_type"] == 1
				and $investments_delta_limit > 0
				and $revenues_delta_limit != 0
				and $expenses_delta_limit > 0
				and (strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment"]) < 20
				 or strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment2"]) < 20
				 or strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment3"]) < 20))
			{
				$error_number = 23;
			}
			elseif($ln_ok1 == true and $project["project_cost_type"] == 1
				and $investments_delta_limit > 0
				and $revenues_delta_limit != 0
				and (strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment"]) < 20
				 or strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment2"]) < 20))
			{
				$error_number = 24;
			}
			elseif($ln_ok1 == true and $project["project_cost_type"] == 1
				and $investments_delta_limit > 0
				and $expenses_delta_limit > 0
				and (strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment"]) < 20
				 or strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment3"]) < 20))
			{
				$error_number = 25;
			}
			elseif($ln_ok1 == true and $project["project_cost_type"] == 1
				and $revenues_delta_limit != 0
				and $expenses_delta_limit > 0
				and (strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment2"]) < 20
				 or strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment3"]) < 20))
			{
				$error_number = 26;
			}
			elseif($ln_ok1 == true and $project["project_cost_type"] == 1
				and $investments_delta_limit > 0
				and strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment"]) < 20)
			{
				$error_number = 27;
			}
			elseif($ln_ok1 == true and $project["project_cost_type"] == 1
				and $revenues_delta_limit != 0
				and strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment2"]) < 20)
			{
				$error_number = 28;
			}
			elseif($ln_ok1 == true and $project["project_cost_type"] == 1
				and $expenses_delta_limit > 0
				and strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment3"]) < 20)
			{
				$error_number = 29;
			}
			
			if($project["project_cost_type"] == 1
				and $error_number > 0)
			{
				$form->add_label("s10", "Sellout Data", RENDER_HTML, $ok);
				$form->add_label("difference_cer_ln", "CER versus LN", RENDER_HTML, $not_ok);
			}
			elseif($project["project_cost_type"] == 1)
			{
				$form->add_label("difference_cer_ln", "CER versus LN", RENDER_HTML, $ok);
				
			}
			*/

			if($ln_ok1 == true and $project["project_cost_type"] == 1
				and $investments_delta_limit > 0
				and $revenues_delta_limit != 0
				and $expenses_delta_limit > 0
				and (strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment4"]) < 20
				 or strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment5"]) < 20))
			{
				$error_number = 23;
			}
			elseif($ln_ok1 == true and $project["project_cost_type"] == 1
				and $investments_delta_limit > 0
				and $revenues_delta_limit != 0
				and (strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment4"]) < 20
				 or strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment5"]) < 20))
			{
				$error_number = 24;
			}
			elseif($ln_ok1 == true and $project["project_cost_type"] == 1
				and $investments_delta_limit > 0
				and $expenses_delta_limit > 0
				and (strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment4"]) < 20
				 or strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment5"]) < 20))
			{
				$error_number = 25;
			}
			elseif($ln_ok1 == true and $project["project_cost_type"] == 1
				and $revenues_delta_limit != 0
				and $expenses_delta_limit > 0
				and (strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment4"]) < 20
				 or strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment5"]) < 20))
			{
				$error_number = 26;
			}
			elseif($ln_ok1 == true and $project["project_cost_type"] == 1
				and $investments_delta_limit > 0
				and (strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment4"]) < 20
				 or strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment5"]) < 20))
			{
				$error_number = 27;
			}
			elseif($ln_ok1 == true and $project["project_cost_type"] == 1
				and $revenues_delta_limit != 0
				and (strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment4"]) < 20
				 or strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment5"]) < 20))
			{
				$error_number = 28;
			}
			elseif($ln_ok1 == true and $project["project_cost_type"] == 1
				and $expenses_delta_limit > 0
				and (strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment4"]) < 20
				 or strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment5"]) < 20))
			{
				$error_number = 29;
			}
			
			if($project["project_cost_type"] == 1
				and $error_number > 0)
			{
				$form->add_label("s10", "Sellout Data", RENDER_HTML, $ok);
				$form->add_label("difference_cer_ln", "CER versus LN", RENDER_HTML, $not_ok);
			}
			elseif($project["project_cost_type"] == 1)
			{
				$form->add_label("difference_cer_ln", "CER versus LN", RENDER_HTML, $ok);
				
			}
		}


		$by = "";
		if($submitted_by["name"])
		{
			$by = " by " . $submitted_by["name"] . " " . $submitted_by["firstname"];
		}
		$form->add_label("date1", "Date of Submission", 0, to_system_date($cer_basicdata["cer_basicdata_submitted"]) . $by);

		$by = "";
		if($resubmitted_by["name"])
		{
			$by = " by " . $resubmitted_by["name"] . " " . $resubmitted_by["firstname"];
		}
		$form->add_label("date2", "Date of latest Resubmission", 0, to_system_date($cer_basicdata["cer_basicdata_resubmitted"]) . $by);

		$by = "";
		if($rejected_by["name"])
		{
			$by = " by " . $rejected_by["name"] . " " . $rejected_by["firstname"];
		}

		$form->add_label("date3", "Date of Rejection", 0, to_system_date($cer_basicdata["cer_basicdata_rejected"]) . $by);

		
		
		
		
		if(has_access("has_access_to_his_cer") or has_access("has_full_access_to_cer"))
		{
			$tmp = 9 + count($cer_brands);
			
			/*temporarily deactivated*/
			if(1==1 or $submission_possible == $tmp )
			{
				if($cer_basicdata["cer_basicdata_submitted"] != NULL and $cer_basicdata["cer_basicdata_submitted"] != "0000-00-00")
				{
					$button_Text= "Resubmit Capital Expenditure Request";
					$sellout_necessary = 0;
				}
				elseif($ln_ok == true and $ln_ok1 == true)
				{
					$button_Text = "Submit Capital Expenditure Request";
				}
				elseif($ln_ok == false)
				{
					$button_Text = "Submission is not possible, please submit the LN form first!";
					$sellout_necessary = 0;
				}
				elseif($ln_ok1 == false)
				{
					$button_Text = "Submission is not possible,LN has not been approved yet!";
					$sellout_necessary = 0;
				}
				
				
				
				if($cer_basicdata["cer_basicdata_cer_locked"] == 0) {
				
					if($ln_ok == true and $ln_ok1 == true)
					{
						$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" /><a href=\"#\" onClick=\"button('submit')\">" . $button_Text ."</a>";
					}
					elseif($ln_ok1 == false)
					{
						$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" /><span class=\"error\">" . $button_Text . "</span>";
					}
					else
					{
						$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" /><span class=\"error\">" . $button_Text . "</span>";
					}
					
					
					if($sellout_necessary == 1 and $sellout_data_ok == true)
					{
						$form->add_label("submit", "Submission", RENDER_HTML, $link);
					}
					elseif($sellout_necessary == 0)
					{
						$form->add_label("submit", "Submission", RENDER_HTML, $link);
					}
				}
				
			}
		}

		
	}

}
elseif($project["project_cost_type"] == 6) // other
{
	$submission_possible = 0;
	$form->add_section("Request Form INR-03: Overview");

	if($posorder_present == 0)
	{
		$form->add_label("s1", "General Information", RENDER_HTML, $not_ok);
		$form->add_label("s3", "Franchisee Information", RENDER_HTML, $not_ok);
		$form->add_label("s5", "Key Money/Investment", RENDER_HTML, $not_ok);
		$form->add_label("s6", "Revenues", RENDER_HTML, $not_ok);
	}
	elseif($cer_basicdata["cer_basicdata_lastyear"] == NULL or $cer_basicdata["cer_basicdata_lastyear"] == "0000-00-00")
	{
		$form->add_label("s1", "General Information", RENDER_HTML, $not_ok);
		$form->add_label("s3", "Franchisee Information", RENDER_HTML, $not_ok);
		$form->add_label("s5", "Key Money/Investment", RENDER_HTML, $not_ok);
		$form->add_label("s6", "Revenues", RENDER_HTML, $not_ok);
	}
	else
	{
		$form->add_label("s1", "General Information", RENDER_HTML, $ok);
		
		//if($posdata["posaddress_fag_city_pasted"] and $ln_basicdata["ln_basicdata_floorplan"])
		if($ln_basicdata["ln_basicdata_floorplan"] 
			and $ln_basicdata["ln_basicdata_floorplan2"]
			and $ln_basicdata["ln_basicdata_elevation_plan"]
			and $ln_basicdata["ln_basicdata_elevation_plan2"]
			and $ln_basicdata["ln_basicdata_photoreport"]
			and $ln_basicdata["ln_basicdata_3drenderings"])
		{
			$form->add_label("s3", "Franchisee Information", RENDER_HTML, $ok);
			$submission_possible++;
		}
		else
		{
			$form->add_label("s3", "Franchisee Information", RENDER_HTML, $not_ok);
		}


		
		
		if((array_key_exists("cer_investment_amount_cer_loc", $construction) and $construction["cer_investment_amount_cer_loc"] > 0) or (array_key_exists("cer_investment_amount_cer_loc", $fixturing) and $fixturing["cer_investment_amount_cer_loc"] > 0)  or (array_key_exists("cer_investment_amount_cer_loc", $keymoney) and $keymoney["cer_investment_amount_cer_loc"] > 0))
		{
			$form->add_label("s5", "Key Money/Investment", RENDER_HTML, $ok);
			$submission_possible++;
		}
		else
		{
			$form->add_label("s5", "Key Money/Investment", RENDER_HTML, $not_ok);
		}

		foreach($cer_brands as $brand_id=>$brand_name)
		{
			if($revenues > 0)
			{
				$form->add_label("s6_" . $brand_id, "Revenues " . $brand_name, RENDER_HTML, $ok);
				$submission_possible++;
			}
			else
			{
				$form->add_label("s6_" . $brand_id, "Revenues " . $brand_name, RENDER_HTML, $not_ok);
			}
		}


		if($other_sales_information > 0)
		{
			$form->add_label("s6a", "Other Sales Relevant Information", RENDER_HTML, $ok);
			$submission_possible++;
		}
		else
		{
			$form->add_label("s6a", "Other Sales Relevant Information", RENDER_HTML, $not_ok);
		}

		$number_of_criteria_to_submit = 5;
		if($neighbourhood == 1)
		{
			$form->add_label("s11", "Neighbourhood", RENDER_HTML, $ok);
			$submission_possible++;
			
		}
		else
		{
			$form->add_label("s11", "Neighbourhood", RENDER_HTML, $not_ok);
		}

	}
	
	
	$by = "";
	if($submitted_by["name"])
	{
		$by = " by " . $submitted_by["name"] . " " . $submitted_by["firstname"];
	}
	$form->add_label("date1", "Date of Submission", 0, to_system_date($cer_basicdata["cer_basicdata_submitted"]) . $by);

	
	$by = "";
	if($resubmitted_by["name"])
	{
		$by = " by " . $resubmitted_by["name"] . " " . $resubmitted_by["firstname"];
	}

	$form->add_label("date2", "Date of latest Resubmission", 0, to_system_date($cer_basicdata["cer_basicdata_resubmitted"]) . $by);

	$by = "";
	if($rejected_by["name"])
	{
		$by = " by " . $rejected_by["name"] . " " . $rejected_by["firstname"];
	}

	$form->add_label("date3", "Date of Rejection", 0, to_system_date($cer_basicdata["cer_basicdata_rejected"]) . $by);

	if(has_access("has_access_to_his_cer") or has_access("has_full_access_to_cer"))
	{
		$number_of_criteria_to_submit = $number_of_criteria_to_submit-1 + count($cer_brands);
		
		//temporarily deactivated*/
		if(1 == 1 or $submission_possible ==  $number_of_criteria_to_submit) // franchisee, cooperation
		{
			
			if($cer_basicdata["cer_basicdata_submitted"] != NULL and $cer_basicdata["cer_basicdata_submitted"] != "0000-00-00")
			{
				$button_Text= "Resubmit INR-03";
				$sellout_necessary = 0;
			}
			else
			{
				$button_Text= "Submit INR-03";
			}

			
			if($cer_basicdata["cer_basicdata_cer_locked"] == 0) {
			
				$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" /><a href=\"#\" onClick=\"button('submit')\">" .  $button_Text . "</a>";
				
				
				if($sellout_necessary == 1 and $sellout_data_ok == true)
				{
					$form->add_label("submit", "Submission", RENDER_HTML, $link);
				}
				elseif($sellout_necessary == 0) 
				{
					$form->add_label("submit", "Submission", RENDER_HTML, $link);
				}
			}
		}
	}
}

$form->add_section("Comment");
$form->add_comment("This comment will be shown also in the benchmark.");

if(has_access("has_access_to_his_cer") or has_access("has_full_access_to_cer"))
{
	$form->add_multiline("cer_basicdata_comment", "Comment", 4, 0, $cer_basicdata["cer_basicdata_comment"]);
	$form->add_button(FORM_BUTTON_SAVE, "Save Data");
}
else
{
	$form->add_label("cer_basicdata_comment", "Comment", 0, $cer_basicdata["cer_basicdata_comment"]);
}

if(has_access("has_full_access_to_cer") and $form_type == "CER")
{

	$form->add_section("Locking");
	$form->add_checkbox("ln_basicdata_locked", "LN is locked", $ln_basicdata["ln_basicdata_locked"], 0, "LN");
	$form->add_checkbox("cer_basicdata_cer_locked", "CER is locked", $cer_basicdata["cer_basicdata_cer_locked"], 0, "CER");
	
}
elseif(has_access("has_full_access_to_cer") and $form_type == "AF")
{

	$form->add_section("Locking");
	$form->add_hidden("ln_basicdata_locked");
	$form->add_checkbox("cer_basicdata_cer_locked", "AF is locked", $cer_basicdata["cer_basicdata_cer_locked"], 0, "AF");
	
}
elseif(has_access("has_full_access_to_cer") and $form_type == "INR03")
{
	$form->add_section("Locking");
	$form->add_hidden("ln_basicdata_locked");
	$form->add_checkbox("cer_basicdata_cer_locked", "INR03 is locked", $cer_basicdata["cer_basicdata_cer_locked"], 0, "INR03");
}
else
{
	$form->add_hidden("cer_basicdata_cer_locked");
	$form->add_hidden("ln_basicdata_locked");
}


if($posorder_present == 0)
{
	$form->error("There is not entry in the POS Index for this project. Please add the project to the POS Index frist.");
}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if(param("s") == 1)
{
	$form->message("Your request was submittes successfully.");
}
if($form->button(FORM_BUTTON_SAVE))
{
	//update cer basicdata
	$fields = array();

	$value = dbquote($form->value("cer_basicdata_comment"));
	$fields[] = "cer_basicdata_comment = " . $value;

	$value = dbquote($form->value("cer_basicdata_cer_locked"));
	$fields[] = "cer_basicdata_cer_locked = " . $value;

	$value1 = "current_timestamp";
	$fields[] = "date_modified = " . $value1;

	if (isset($_SESSION["user_login"]))
	{
		$value1 = $_SESSION["user_login"];
		$fields[] = "user_modified = " . dbquote($value1);
	}

	$sql = "update cer_basicdata set " . join(", ", $fields) . " where cer_basicdata_version = 0 and cer_basicdata_project = " . param("pid");
	mysql_query($sql) or dberror($sql);



	//update ln basicdata
	$fields = array();

	$value = dbquote($form->value("ln_basicdata_locked"));
	$fields[] = "ln_basicdata_locked = " . $value;

	$value1 = "current_timestamp";
	$fields[] = "date_modified = " . $value1;

	if (isset($_SESSION["user_login"]))
	{
		$value1 = $_SESSION["user_login"];
		$fields[] = "user_modified = " . dbquote($value1);
	}

	$sql = "update ln_basicdata set " . join(", ", $fields) . " where ln_basicdata_version = 0 and ln_basicdata_project = " . param("pid");
	mysql_query($sql) or dberror($sql);


	//create business plan version in case ln was locked
	if($form->value("ln_basicdata_locked") == 1 and $ln_basicdata["ln_basicdata_locked"] == 0)
	{
		$result = create_cer_version(param("pid"), $context = "ln", $project["pipeline"]);
	}

	if($form->value("cer_basicdata_cer_locked") == 1 and $cer_basicdata["cer_basicdata_cer_locked"] == 0)
	{
		$new_cer_version = create_cer_version(param("pid"), $context = "cer", $project["pipeline"]);
	}
}
elseif($form->button("submit"))
{
	//check if conditions fro submission are given (ln data correct)
	
	$ln_error = 0;
	if($form_type == "CER")
	{
		
		$ln_basicdata = get_ln_basicdata(param("pid"));
		
		if($ln_basicdata['ln_no_ln_submission_needed'] == 0) {
			if(!$ln_basicdata['ln_basicdata_floorplan']) {
				$ln_error = 1;
			}
			if(!$ln_basicdata['ln_basicdata_pix1']) {
				$ln_error = 1;
			}
			if(!$ln_basicdata['ln_basicdata_pix2']) {
				$ln_error = 1;
			}
			if(!$ln_basicdata['ln_basicdata_pix3']) {
				$ln_error = 1;
			}
			if(!$ln_basicdata['ln_basicdata_draft_aggreement']) {
				$ln_error = 1;
			}
		}

		if(!$cer_basicdata["cer_summary_in01_description"])
		{
			$ln_error = 2;
		}
		elseif(!$cer_basicdata["cer_summary_in01_strategy"])
		{
			$ln_error = 2;
		}
		elseif(!$cer_basicdata["cer_summary_in01_investment"])
		{
			$ln_error = 2;
		}
		elseif(!$cer_basicdata["cer_summary_in01_benefits"])
		{
			$ln_error = 2;
		}
		elseif(!$cer_basicdata["cer_summary_in01_alternative"])
		{
			$ln_error = 2;
		}
		elseif(!$cer_basicdata["cer_summary_in01_risks"])
		{
			$ln_error = 2;
		}
	}
	else
	{
		
		$ln_basicdata = get_ln_basicdata(param("pid"));
		
		
		if($ln_basicdata['ln_no_ln_submission_needed'] == 0) {
			if(!$ln_basicdata['ln_basicdata_floorplan']) {
				$ln_error = 1;
			}
			elseif(!$ln_basicdata['ln_basicdata_floorplan2']) {
				$ln_error = 1;
			}
			elseif(!$ln_basicdata['ln_basicdata_elevation_plan']) {
				$ln_error = 1;
			}
			elseif(!$ln_basicdata['ln_basicdata_elevation_plan2']) {
				$ln_error = 1;
			}
			elseif(!$ln_basicdata['ln_basicdata_photoreport']) {
				$ln_error = 1;
			}
			elseif(!$ln_basicdata['ln_basicdata_3drenderings']) {
				$ln_error = 1;
			}
		}
	}	

	if($ln_error == 1 or $ln_error == 2) {
		
		$fields = array();

		$value = dbquote($form->value("cer_basicdata_comment"));
		$fields[] = "cer_basicdata_comment = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}

		$sql = "update cer_basicdata set " . join(", ", $fields) . " where cer_basicdata_version = 0 and cer_basicdata_project = " . param("pid");
		mysql_query($sql) or dberror($sql);
		
		if($form_type == "CER" and $ln_error == 1)
		{
			$form->error("CER Submission is not possible since LN Data is incomplete!");
		}
		elseif($form_type == "CER" and $ln_error == 2)
		{
			$form->error("CER Submission is not possible since the description of capital expenditure project is not complete! <br />Click 'Capital Expenditure Request Form' and complete the following information: <br />Description, Strategy, Investment (Overview), Benefits/Profitability, Alternative taken into consideration and Risks.");
		}
		else
		{
			$form->error("AF Submission is not possible since Franchisee Data is incomplete!");
		}
	}
	else
	{

		//update cer basicdata
		$fields = array();

		$value = dbquote($form->value("cer_basicdata_comment"));
		$fields[] = "cer_basicdata_comment = " . $value;

			
		if($cer_basicdata["cer_basicdata_submitted"] == NULL or $cer_basicdata["cer_basicdata_submitted"] == "0000-00-00")
		{
			$value1 = date("Y-m-d");
			$fields[] = "cer_basicdata_submitted = " . dbquote($value1);
			$fields[] = "cer_basicdata_submitted_by = " . dbquote(user_id());
		}
		else
		{
			$value1 = date("Y-m-d");
			$fields[] = "cer_basicdata_resubmitted = " . dbquote($value1);
			$fields[] = "cer_basicdata_resubmitted_by = " . dbquote(user_id());
		}

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}

		$sql = "update cer_basicdata set " . join(", ", $fields) . " where cer_basicdata_version = 0 and cer_basicdata_project = " . param("pid");
		mysql_query($sql) or dberror($sql);

		$sql = "select * from milestones " . 
			   "where milestone_on_cer_submission = 1";
		
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$reciepient_email = $row["milestone_email"];
			$reciepient_cc = $row["milestone_ccmail"];

			
			if($cer_basicdata["cer_basicdata_submitted"] == NULL or $cer_basicdata["cer_basicdata_submitted"] == "0000-00-00") //first submittion
			{
				$sql = "update project_milestones SET " . 
					   "project_milestone_date = " . dbquote(date("Y-m-d")) . ", " . 
					   "project_milestone_date_comment = " . dbquote("CER/AF submitted") . ", " .
					   "user_created = " . dbquote(user_login()) . ", " .
					   "date_created = " . dbquote(date("Y-m-d")) . 
					   " where project_milestone_project = " . dbquote(param("pid")) .
					   " and project_milestone_milestone = " . dbquote($row["milestone_id"]);
				$result = mysql_query($sql) or dberror($sql);
				
			}
			else //resubmission
			{
				
				if($row = mysql_fetch_assoc($res))
				{
					$sql = "update project_milestones SET " .
						   "project_milestone_date = " . dbquote(date("Y-m-d")) . ", " .
						   "project_milestone_date_comment = " . dbquote("CER/AF resubmitted") . ", " .
						   "user_created = " . dbquote(user_login()) . ", " .
						   "date_created = " . dbquote(date("Y-m-d")) . 
						   " where project_milestone_project = " . dbquote(param("pid")) .
						   " and project_milestone_milestone = " . dbquote($row["milestone_id"]);

					$result = mysql_query($sql) or dberror($sql);
				}			
			}
			

			
			//get recipients of project submission alerts
			$recipient_user_ids = array();
			
			if($cer_basicdata["cer_basicdata_submitted"] == NULL or $cer_basicdata["cer_basicdata_submitted"] == "0000-00-00")
			{
				$sql = 'select * from projecttype_newproject_notifications ' . 
					   'where projecttype_newproject_notification_oncerafsubmission = 1 ' . 
					   ' and projecttype_newproject_notification_country = ' . $project["order_shop_address_country"] . 
					   ' and projecttype_newproject_notification_postype = ' . $project['project_postype'];
			}
			else
			{
				$sql = 'select * from projecttype_newproject_notifications ' . 
					   'where projecttype_newproject_notification_oncerafresubmission = 1 ' . 
					   ' and projecttype_newproject_notification_country = ' . $project["order_shop_address_country"] . 
					   ' and projecttype_newproject_notification_postype = ' . $project['project_postype'];
			}

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				if($row["projecttype_newproject_notification_email"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_email"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_email"]] = $row_u["user_id"];
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_email"]] = 0;
					}
					$mailaddress = 1;
					
				}

				if($row["projecttype_newproject_notification_emailcc1"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc1"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc1"]] = $row_u["user_id"];
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc1"]] = 0;
					}
				}
				if($row["projecttype_newproject_notification_emailcc2"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc2"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc2"]] = $row_u["user_id"];
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc2"]] = 0;
					}
					
				}
				if($row["projecttype_newproject_notification_emailcc3"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc3"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc3"]] = $row_u["user_id"];
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc3"]] = 0;
					}
					
				}
				if($row["projecttype_newproject_notification_emailcc4"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc4"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc4"]] = $row_u["user_id"];
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc4"]] = 0;
					}
				
				}
				if($row["projecttype_newproject_notification_emailcc5"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc5"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc5"]] = $row_u["user_id"];
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc5"]] = 0;
					}
				
				}
				if($row["projecttype_newproject_notification_emailcc6"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc6"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc6"]] = $row_u["user_id"];
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc6"]] = 0;
					}
				
				}
				if($row["projecttype_newproject_notification_emailcc7"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc7"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc7"]] = $row_u["user_id"];
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc7"]] = 0;
					}
				}
			}

			//send mail notification

			$sql = "select order_number, order_shop_address_place, country_name, order_shop_address_company, " .
				   "project_postype, project_projectkind, project_cost_type " . 
				   "from projects " .
				   "left join orders on order_id = project_order " .
				   "left join countries on country_id = order_shop_address_country " .
				   "left join project_costs on project_cost_order = order_id " .
				   "where project_id = " .  dbquote(param("pid"));

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$user_data = get_user(user_id());
				
				
				if($cer_basicdata["cer_basicdata_submitted"] == NULL or $cer_basicdata["cer_basicdata_submitted"] == "0000-00-00")
				{
					if($form_type == "CER")
					{
						$subject = "CER submission alert - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_place"] . ", " . $row["order_shop_address_company"];
					}
					else
					{
						$subject = "AF submission alert -  Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_place"] . ", ". $row["order_shop_address_company"];
					}
				}
				else
				{
					if($form_type == "CER")
					{
						$subject = "CER resubmission alert - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_place"] . ", " . $row["order_shop_address_company"];
					}
					else
					{
						$subject = "AF resubmission alert - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_place"] . ", " . $row["order_shop_address_company"];
					}
				}

				$subject_rmas = $subject;
			
			
				if($form_type == "CER")
				{
					$sql = "select * from mail_alert_types " . 
						   "where mail_alert_type_id = 4";
				}
				else
				{
					$sql = "select * from mail_alert_types " . 
						   "where mail_alert_type_id = 20";
				}
							

				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					$sender_email = $user_data["email"];
					$sender_name = $user_data["firstname"] . " " . $user_data["name"];
					$mail_text = $row["mail_alert_mail_text"] . " " . $user_data["firstname"] . " " . $user_data["name"] . ".";

					$bodytext0 = "Dear " . $row["mail_alert_type_sender_name"] . "\n\n" . $mail_text;
					//$link ="cer_project.php?pid=" . param("pid") . "&id=" . param("pid");
					
					if($form_type == "CER")
					{
						$link ="cer_booklet_pdf.php?pid=" . param("pid");
					}
					else
					{
						$link ="af_booklet_pdf.php?pid=" . param("pid");
					}
					$bodytext = $bodytext0 . "\nClick below to download the booklet:\n";
					$bodytext = $bodytext .  APPLICATION_URL ."/cer/" . $link . "\n\n";           
					
					
					$bodytext .= "Thank you very much.\nKind regards, " . $sender_name;

					$mail = new Mail();
					$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
					$mail->set_sender($sender_email, $sender_name);
					
					$mail->add_recipient($reciepient_email);
					$mail->add_cc($reciepient_cc);

					$email1 = $reciepient_email;
					$email2 = $reciepient_cc;

					
					$mail->add_text($bodytext);

					$result = $mail->send();

					$rctps = $reciepient_email . "\n" . "and CC-Mail to:" . "\n" . $reciepient_cc;


					//add submission recipeint alerts
					foreach($recipient_user_ids as $key=>$user_id)
					{
						if($key != $reciepient_email and $key!= $reciepient_cc)
						{
							$mail->add_cc($key);
							$rctps .= "\n" . $key;
						}
					}


					if($result == 1)
					{
						$fields = array();
						$values = array();

						$fields[] = "cer_mail_project";
						$values[] = dbquote(param("pid"));

						$fields[] = "cer_mail_group";
						$values[] = dbquote($subject);

						$fields[] = "cer_mail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "cer_mail_sender";
						$values[] = dbquote($sender_name);

						$fields[] = "cer_mail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "cer_mail_reciepient";
						$values[] = dbquote($rctps);

						$fields[] = "date_created";
						$values[] = "now()";

						$fields[] = "date_modified";
						$values[] = "now()";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into cer_mails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						
						mysql_query($sql) or dberror($sql);
					}
				}
			}

			redirect("cer_project.php?pid=" . param("pid") . "&s=1");
			
		}
	}

}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");

require "include/project_page_actions.php";



$page->header();

if($form_type == "INR03")
{
	$page->title("INR-03 - Retail Furniture in Third-party Store: Project");
}
elseif($form_type == "AF")
{
	$page->title("Application Form: Project");
}
else
{
	$page->title("Capital Expenditure Request: Overview");
}

$form->render();

require "include/footer_scripts.php";

$page->footer();

?>