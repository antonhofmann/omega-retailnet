<?php
/********************************************************************

    posprojecttypes.php

    Lists project_types for editing.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-02-20
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-02-20
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_full_access_to_cer");
set_referer("posprojecttype.php");


$sql = "select posproject_type_id, postype_name, " .
       "project_costtype_text, projectkind_name, " .
	   "if(posproject_type_needs_cer = 1, 'yes', 'no') as needs_cer, " .
	   "if(posproject_type_needs_af = 1, 'yes', 'no') as needs_af, " .
	   "if(posproject_type_needs_inr03 = 1, 'yes', 'no') as needs_inr03, " .
	   "if(posproject_type_needs_agreement = 1, 'yes', 'no') as needs_agreement " .
       "from posproject_types " .
	   "left join postypes on postype_id = posproject_type_postype " . 
	   "left join projectkinds on projectkind_id = posproject_type_projectkind " . 
	   "left join project_costtypes on project_costtype_id = posproject_type_projectcosttype ";

$list = new ListView($sql, LIST_HAS_HEADER | LIST_SHOW_COUNT);

$list->set_entity("posproject_types");

$list->set_order("postype_name, projectkind_name");

$list->add_column("postype_name", "POS Type", "posprojecttype.php");
$list->add_column("project_costtype_text", "Legal Type");
$list->add_column("projectkind_name", "Project Kind");
$list->add_column("needs_cer", "CER");
$list->add_column("needs_af", "AF");
$list->add_column("needs_inr03", "INR-03");

$list->add_column("needs_agreement", "Agreement");

$list->add_button(LIST_BUTTON_NEW, "New", "posprojecttype.php");

$list->process();

$page = new Page("posprojecttypes");

$page->header();
$page->title("POS Projecttypes");
$list->render();
$page->footer();

?>
