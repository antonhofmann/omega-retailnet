<?php
/********************************************************************

    cer_in01_pdf_detail.php

    Print PDF for CER Summary (Form INR-01).

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-06-04
    Version:        2.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*********************************************************************/

//set defaul values
$legal_entity_name = "";
$project_name = "";
$project_manager = "";
$requested_amount = "";
$budget_amount = "";
$currency_symbol = "";
$investment_amount = "";
$project_start = "";
$net_present_value = "";
$key_money = "";
$project_end = "";
$pay_back_period = "";
$deposit = "";
$post_compl_review = "";
$internal_reate_of_return ="";
$other_cost = "";
$project_kind = "";


$description = "";
$strategy = "";
$investment = "";
$benefits = "";
$alternative = "";
$risks = "";

$name1 = "";
$name2 = "";
$name3 = "";
$name4 = "";
$name5 = "";
$name6 = "";
$name7 = "";
$name8 = "";
$name9 = "";

$date1 = "";
$date2 = "";
$date3 = "";
$date4 = "";
$date5 = "";
$date6 = "";
$date7 = "";
$date8 = "";
$date9 = "";


$protocol_number1 = "";
$protocol_number1 = "";
$cer_number = "";

$opening_date = "";

$project_kind_new = "";
$project_kind_renovation = "";
$project_kind_relocation = "";



// get basic data
$cer_basicdata = get_cer_basicdata(param("pid"), $cer_version);


$approval_name1 = $cer_basicdata["cer_basicdata_approvalname1"];
$approval_name2 = $cer_basicdata["cer_basicdata_approvalname2"];
$approval_name3 = $cer_basicdata["cer_basicdata_approvalname3"];
$approval_name4 = $cer_basicdata["cer_basicdata_approvalname4"];
$approval_name5 = $cer_basicdata["cer_basicdata_approvalname5"];
$approval_name6 = $cer_basicdata["cer_basicdata_approvalname6"];
$approval_name7 = $cer_basicdata["cer_basicdata_approvalname7"];
$approval_name8 = $cer_basicdata["cer_basicdata_approvalname8"];
$approval_name9 = $cer_basicdata["cer_basicdata_approvalname9"];
$approval_name10 = $cer_basicdata["cer_basicdata_approvalname10"];

$approval_name11 = $cer_basicdata["cer_summary_in01_sig01"];
$approval_name12 = $cer_basicdata["cer_summary_in01_sig02"];
$approval_name13 = $cer_basicdata["cer_summary_in01_sig03"];
$approval_name14 = $cer_basicdata["cer_summary_in01_sig04"];
$approval_name15 = $cer_basicdata["cer_summary_in01_sig05"];
$approval_name16 = $cer_basicdata["cer_summary_in01_sig07"];
$approval_name17 = $cer_basicdata["cer_summary_in01_sig10"];

// get all data needed from cer_basic_data

$sql = "select * from cer_summary where cer_summary_cer_version = " . $cer_version . "  and cer_summary_project = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{

	$description = $row["cer_summary_in01_description"];
	$strategy = $row["cer_summary_in01_strategy"];
	$investment = $row["cer_summary_in01_investment"];
	$benefits = $row["cer_summary_in01_benefits"];
	$alternative = $row["cer_summary_in01_alternative"];
	$risks = $row["cer_summary_in01_risks"];

	$name1 = $row["cer_summary_in01_sig01"];
	$name2 = $row["cer_summary_in01_sig02"];
	$name3 = $row["cer_summary_in01_sig03"];
	$name4 = $row["cer_summary_in01_sig04"];
	$name5 = $row["cer_summary_in01_sig05"];
	$name6 = $row["cer_summary_in01_sig06"];
	$name7 = $row["cer_summary_in01_sig07"];
	$name8 = $row["cer_summary_in01_sig08"];
	$name9 = $row["cer_summary_in01_sig09"];

	$name7 = $cer_basicdata["cer_basicdata_approvalname11"];

	$date1 = to_system_date($row["cer_summary_in01_date01"]);
	$date2 = to_system_date($row["cer_summary_in01_date02"]);
	$date3 = to_system_date($row["cer_summary_in01_date03"]);
	$date4 = to_system_date($row["cer_summary_in01_date04"]);
	$date5 = to_system_date($row["cer_summary_in01_date05"]);
	$date6 = to_system_date($row["cer_summary_in01_date06"]);
	$date7 = to_system_date($row["cer_summary_in01_date07"]);
	$date8 = to_system_date($row["cer_summary_in01_date08"]);
	$date9 = to_system_date($row["cer_summary_in01_date09"]);


	$protocol_number1 = $row["cer_summary_in01_prot01"];
	$protocol_number2 = $row["cer_summary_in01_prot02"];
	$cer_number = $row["cer_summary_in01_cernr"];

	
	if($row["cer_summary_in01_review_date"] and $row["cer_summary_in01_review_date"] != '0000-00-00')
	{
		$post_compl_review = substr($row["cer_summary_in01_review_date"], 5,2) . "/" . substr($row["cer_summary_in01_review_date"], 0,4);
	}
	
}

// get all data needed from project
$sql = "select * " . 
       "from projects " . 
	   "left join orders on order_id = project_order " .
	   "left join countries on country_id = order_shop_address_country " .
	   "left join product_lines on product_line_id = project_product_line " .
	   "left join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join projectkinds on projectkind_id = project_projectkind " .
       "left join addresses on address_id = order_client_address " .
       "left join users on user_id = project_retail_coordinator " . 
	   "where project_id = " . param("pid");


$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	
	if($row["address_legal_entity_name"])
	{
		$legal_entity_name = $row["address_legal_entity_name"];
	}
	else
	{
		$legal_entity_name = $row["address_company"];
	}
	
	$project_name = $row["order_shop_address_company"]. ", " . $row["order_shop_address_place"] . ", " . $row["country_name"];


	$project_manager = $row["user_firstname"] . " " . $row["user_name"];
	
	$project_kind = $row["projectkind_name"];


	
	$factor = $cer_basicdata['cer_basicdata_factor'];
	if(!$cer_basicdata['cer_basicdata_factor'])
	{
		$factor = 1;
	}

	$exchange_rate = 1;
	if($cer_basicdata['cer_basicdata_exchangerate'] > 0)
	{
		$exchange_rate = $cer_basicdata['cer_basicdata_exchangerate'];
	}
	
	
	$budget_amount = $row["project_approximate_budget"]/$cer_basicdata['cer_basicdata_exchangerate']*$factor;

	$budget_amount = number_format($budget_amount, 0, "", "'");
	$project_start = substr($row["order_date"], 5,2) . "/" . substr($row["order_date"], 0,4);
	
	//$project_start = substr($cer_basicdata['cer_basicdata_project_start'], 5,2) . "/" . substr($cer_basicdata['cer_basicdata_project_start'], 0,4);
	
	//$tmp = date('Y-m-d H:i:s', strtotime($row["project_real_opening_date"] . ' + 90 day'));
	$tmp = $row["project_real_opening_date"];
	$project_end = substr($tmp, 5,2) . "/" . substr($tmp, 0,4);

	//$project_end = substr($cer_basicdata['cer_basicdata_project_end'], 5,2) . "/" . substr($cer_basicdata['cer_basicdata_project_end'], 0,4);

	$order_number = $row["project_order"];
	$address_country = $row["address_country"];

	$opening_date = to_system_date($row["project_real_opening_date"]);

	if($row["project_projectkind"] == 1)  {
		$project_kind_new = "X";
	}
	elseif($project["project_projectkind"] == 6)  {
		$project_kind_relocation = "X";
	}
	elseif($row["project_projectkind"] == 2 or $row["project_projectkind"] == 3)  {
		$project_kind_renovation = "X";
	}
	
	
}


$client_address = get_address($project["order_client_address"]);

$client_currency = get_cer_currency(param("pid"), $cer_version);
$currency_symbol = $client_currency["symbol"];

//get milestones
$date_ln_approval = "";
$date_cer_request = "";

$sql = "select project_milestone_milestone, project_milestone_date from project_milestones " . 
       "where project_milestone_milestone in(1, 13, 15) " . 
	   " and project_milestone_project = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($row["project_milestone_milestone"] == 1)
	{
		if($row["project_milestone_date"] == NULL or $row["project_milestone_date"] == '0000-00-00')
		{
			$date_cer_request = 'n.a.';
		}
		else
		{
			$date_cer_request = to_system_date($row["project_milestone_date"]);

		}
	}
	elseif($row["project_milestone_milestone"] == 15)
	{
		if($row["project_milestone_date"] != NULL and $row["project_milestone_date"] != '0000-00-00')
		{
			$date_cer_request = to_system_date($row["project_milestone_date"]);
		}
	}
	elseif($row["project_milestone_milestone"] == 13)
	{
		if($row["project_milestone_date"] == NULL or $row["project_milestone_date"] == '0000-00-00')
		{
			$date_ln_approval = 'n.a.';
		}
		else
		{
			$date_ln_approval = to_system_date($row["project_milestone_date"]);
		}
	}
}

//get financial data
include("include/in_financial_data.php");	


$requested_amount = round(($investment_total + $intagibles_total + $deposit + $other_noncapitalized_cost) / 1000, 0);
$investment_amount = round($investment_total/1000,0);
$key_money = round($intagibles_total/1000, 0);
$deposit = round($deposit/1000, 0);
$other_cost = round($other_noncapitalized_cost/1000, 0);

$net_present_value = round($net_present_value_retail/1000, 0);
$internal_reate_of_return = "";
if($net_present_value) 
{
	$internal_reate_of_return = round($discounted_cash_flow_retail,2) . "%";
}

if($pay_back_period_retail)
{
	$pay_back_period = round($pay_back_period_retail,2);
}
else
{
	$pay_back_period = "Invest. Period";
}

//new calculation
//$requested_amount = ceil(($investment_total + $intagibles_total + $deposit + $other_noncapitalized_cost) / 1000);
//$investment_amount = ceil($investment_total/1000);
//$key_money = ceil($intagibles_total/1000);
//$deposit = ceil($deposit/1000);
//$other_cost = ceil($other_noncapitalized_cost/1000);


//set pdf parameters
$margin_top = 16;
$margin_left = 12;
$y = $margin_top;

$pdf->SetTitle("Retail Capital Expenditure Request (CER) Form INR-01");
$pdf->SetAuthor("Retailnet");
$pdf->SetDisplayMode(150);
$pdf->AddPage();
$pdf->SetAutoPageBreak(false, 0);

// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "B", 9);
$pdf->Cell(40, 8, "Swatch Group", 1);

$pdf->SetFont("arialn", "B", 11);
$pdf->Cell(82, 8, "Retail Capital Expenditure Request (CER)", 1, "", "C");

$pdf->SetFont("arialn", "", 9);
$pdf->SetFillColor(224,224,224);
$pdf->Cell(25, 8, "SUMMARY", 1, "", "C", true);

$pdf->SetFont("arialn", "", 9);

if($cer_basicdata["cer_basicdata_version"] == 0)
{
	$pdf->Cell(20, 8, date("d.m.Y"), 1, "", "C");
}
else
{
	$pdf->Cell(20, 8, to_system_date($cer_basicdata["versiondate"]), 1, "", "C");
}
$pdf->Cell(20, 8, "INR-01", 1, "", "C");


// draw first box
$pdf->SetXY($margin_left,$y+8);
$pdf->Cell(187, 32, "", 1);


	// print project and investment infos
	$y = $y+9;
	$x = $margin_left+1;

	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Legal Entity Name:", 1, "", "L");
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(102, 5, $legal_entity_name, 1, "", "L");
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(26, 5, "Currency:", 1, "", "L");
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(18, 5, $currency_symbol, 1, "", "L");


	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Project Name:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(70, 5, $project_name . " (" . $project['order_number'] . ")", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(32, 5, "Project Manager:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(44, 5, $project_manager, 1, "", "L");


	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Requested Amount:", 1, "", "L");
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(10, 5, "k" . $currency_symbol, 1, "", "R");
	$pdf->Cell(21, 5, $requested_amount, 1, "", "R");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(39, 5, "Project begin (month/year):", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(32, 5, $project_start, 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(26, 5, "Date LN approval:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(18, 5, $date_ln_approval, 1, "", "L");
	
	
	
	
	
	/*
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Budget Amount:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(32, 5, $budget_amount, 1, "", "R");
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(26, 5, "Currency:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(18, 5, $currency_symbol, 1, "", "R");
	*/

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 7);
	$pdf->Cell(39, 5, "- Investments in fixed Assets:", 1, "", "L");
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(10, 5, "k" . $currency_symbol, 1, "", "R");
	$pdf->Cell(21, 5, $investment_amount, 1, "", "R");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(39, 5, "Project end (month/year):", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(32, 5, $project_end, 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(26, 5, "Date CER request:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(18, 5, $date_cer_request, 1, "", "L");



	/*
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(26, 5, "Net present value:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(18, 5, $net_present_value, 1, "", "R");
    */

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 7);
	$pdf->Cell(39, 5, "- Key/Prem. Money/Goodwill:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(10, 5, "k" . $currency_symbol, 1, "", "R");
	$pdf->Cell(21, 5, $key_money, 1, "", "R");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(39, 5, "Post Compl. Review (month/year):", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(32, 5, $post_compl_review, 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(26, 5, "Opening Date:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(18, 5, $opening_date, 1, "", "L");
	
	
	
	/*
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(26, 5, "Pay back period:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(18, 5, $pay_back_period, 1, "", "R");
    
	

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 7);
	$pdf->Cell(39, 5, "- Deposit:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(31, 5, $deposit, 1, "", "R");
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Post Compl. Review (month/year):", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(32, 5, $post_compl_review, 1, "", "R");
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(26, 5, "Internal rate of return:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(18, 5, $internal_reate_of_return, 1, "", "R");

	


	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 7);
	$pdf->Cell(39, 5, "- Other non-capitalizable fees & taxes:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(31, 5, $other_cost, 1, "", "R");
	$pdf->SetFillColor(224,224,224);
	$pdf->Cell(115, 5, " ", 1, "", "R", true);

	*/

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Project classification: (X)", 1, "", "L");
	$pdf->Cell(20, 5, "New", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(11, 5, $project_kind_new, 1, "", "L");
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(28, 5, "Renovation", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(11, 5, $project_kind_renovation, 1, "", "L");
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(21, 5, "Relocation", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(11, 5, $project_kind_relocation, 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->SetFillColor(224,224,224);
	$pdf->Cell(44, 5, "", 1, "", "C", true);

	//$pdf->SetFillColor(224,224,224);
	//$pdf->Cell(115, 5, " ", 1, "", "R", true);

// end of first box

// draw second box
$pdf->SetXY($margin_left,$y+6);
if($project['order_client_address'] == 42
		or $project['order_client_address'] == 7) {
	$pdf->Cell(187, 222, "", 1);
}
else {
	$pdf->Cell(187, 222, "", 1);
}

	$y = $y+5;
	$pdf->SetXY($x-1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(102, 8, "1. Project desription and strategy:", 0, "", "L");
	

	$content = $description . "\r\n" . $strategy . "\r\n" . $investment;
	$content = $description;
	$y = $y+7;
	$pdf->SetXY($x,$y);
	$pdf->SetFillColor(248,251,167);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(185, 40, "", 1, "", "L", true);
	$pdf->SetXY($x,$y+1);
	$pdf->MultiCell(185,3.5, $content, 0, "T");

	
	/*
	$y = $y+20;
	$pdf->SetXY($x-1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(102, 8, "Justification: Please write a comment about the following items:", 0, "", "L");

	$y = $y+7;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(185, 129, "", 1, "", "L");
	
	$y = $y-2;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 7);
	$pdf->Cell(102, 8, "a) Strategy:", 0, "", "L");

	$y = $y+6;
	$pdf->SetXY($x+1,$y);
	$pdf->SetFillColor(248,251,167);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(183, 20, "", 1, "", "L", true);
	$pdf->SetXY($x+1,$y+1);
	$pdf->MultiCell(183,3.5, $strategy, 0, "T");


	$y = $y+20;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 7);
	$pdf->Cell(102, 8, "b) Investment (overview):", 0, "", "L");

	$y = $y+6;
	$pdf->SetXY($x+1,$y);
	$pdf->SetFillColor(248,251,167);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(183, 20, "", 1, "", "L", true);
	$pdf->SetXY($x+1,$y+1);
	$pdf->MultiCell(183,3.5, $investment, 0, "T");
	*/

	$y = $y+40;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(102, 8, "2. Benefits, risks, alternatives:", 0, "", "L");

	//$content = $benefits . "\r\n" . $risks . "\r\n" . $alternative;
	$content = $benefits;
	$content = str_replace("\r\n\r\n", "", $content);
	
	$y = $y+6;
	$pdf->SetXY($x,$y);
	$pdf->SetFillColor(248,251,167);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(185, 40, "", 1, "", "L", true);
	$pdf->SetXY($x+1,$y+1);
	$pdf->MultiCell(183,3.5, $content, 0, 'L', false, 1, '', '', true, 0, false, true, '', 'T');
	

	/*
	$y = $y+20;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 7);
	$pdf->Cell(102, 8, "d) Alternative taken into consideration:", 0, "", "L");

	$y = $y+6;
	$pdf->SetXY($x+1,$y);
	$pdf->SetFillColor(248,251,167);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(183, 20, "", 1, "", "L", true);
	$pdf->SetXY($x+1,$y+1);
	$pdf->MultiCell(183,3.5, $alternative, 0, "T");

	$y = $y+20;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 7);
	$pdf->Cell(102, 8, "e) Risks:", 0, "", "L");
	
	$y = $y+6;
	$pdf->SetXY($x+1,$y);
	$pdf->SetFillColor(248,251,167);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(183, 20, "", 1, "", "L", true);
	$pdf->SetXY($x+1,$y+1);
	$pdf->MultiCell(183,3.5, $risks, 0, "T");

	*/

	$y = $y+40;
	$pdf->SetXY($x-1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(102, 8, "3. Signatures:", 0, "", "L");

	

	//title bar 1
	$y = $y+6;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetFillColor(224,224,224);
	$pdf->Cell(185,6, "Brand", 1, "", "L", true);

	$y = $y+6;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(30, 12, "CEO " . "\r\n" . $approval_name2, 1);
	$pdf->SetXY($x+30,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(63, 12, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 12, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(62, 12, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');


	$y = $y+12;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(30, 12, "VP Finance" . "\r\n" . $approval_name7, 1);
	$pdf->SetXY($x+30,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(63, 12, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 12, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(62, 12, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');


	$y = $y+12;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(30, 12, "VP Sales " . "\r\n" . $approval_name3, 1);
	$pdf->SetXY($x+30,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(63, 12, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 12, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(62, 12, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');

	$y = $y+12;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(30, 12, "Merchandising Manager" . "\r\n" . $approval_name4, 1);
	$pdf->SetXY($x+30,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(63, 12, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 12, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(62, 12, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');


	$y = $y+12;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(30, 12, "Retail Controller" . "\r\n" . $approval_name9, 1);
	$pdf->SetXY($x+30,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(63, 12, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 12, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(62, 12, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');


	$y = $y+15;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetFillColor(224,224,224);
	$pdf->Cell(185,6, "Country", 1, "", "L", true);


	$y = $y+6;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(30, 12, "Country Manager " . "\r\n" . $approval_name11, 1);
	$pdf->SetXY($x+30,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(63, 12, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 12, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(62, 12, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');



	//china projects
	if($project['order_client_address'] == 42
		or $project['order_client_address'] == 7) {
		$y = $y+12;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->MultiCell(30, 12, "Finance/Service Center " . "\r\n" . $approval_name16, 1);
		$pdf->SetXY($x+30,$y);
		$pdf->SetFont("freesans", "", 7);
		$pdf->Cell(63, 12, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(30, 12, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
		$pdf->SetFont("freesans", "", 7);
		$pdf->Cell(62, 12, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');


		$y = $y+12;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->MultiCell(30, 12, "Head of Controlling " . "\r\n" . $approval_name17, 1);
		$pdf->SetXY($x+30,$y);
		$pdf->SetFont("freesans", "", 7);
		$pdf->Cell(63, 12, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(30, 12, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
		$pdf->SetFont("freesans", "", 7);
		$pdf->Cell(62, 12, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');
	}
	else {
		$y = $y+12;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->MultiCell(30, 12, "Finance/Service Center " . "\r\n" . $approval_name12, 1);
		$pdf->SetXY($x+30,$y);
		$pdf->SetFont("freesans", "", 7);
		$pdf->Cell(63, 12, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(30, 12, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
		$pdf->SetFont("freesans", "", 7);
		$pdf->Cell(62, 12, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');
	}

	$y = $y+12;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(30, 12, "Brand Manager " . "\r\n" . $approval_name13, 1);
	$pdf->SetXY($x+30,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(63, 12, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 12, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(62, 12, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');

	if($project['order_client_address'] != 42
		and $project['order_client_address'] != 7) {
		$y = $y+12;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->MultiCell(30, 12, "Retail Development " . "\r\n" . $approval_name15, 1);
		$pdf->SetXY($x+30,$y);
		$pdf->SetFont("freesans", "", 7);
		$pdf->Cell(63, 12, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(30, 12, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
		$pdf->SetFont("freesans", "", 7);
		$pdf->Cell(62, 12, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');
	}

//end of second box
?>