<?php
/********************************************************************

    cer_benchmark_01_xls.php

    Generate Excel File: CER Benchmark Overview

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-10-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-10-15
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_access_to_cer_benchmarks");

require_once "../include/xls/Writer.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";
require_once "../shared/project_cost_functions.php";

include("include/calculate_depreciation.php");


/********************************************************************
    prepare Data Needed
*********************************************************************/

include("cer_benchmark_filter.php");


/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$filename = "benchmark_" . $benchmark_shortcut . "_horizonal_" . date("Ymd") . ".xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename); 
$xls->setVersion(8);
$sheet = $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

//formats
$f_header =& $xls->addFormat();
$f_header->setSize(12);
$f_header->setAlign('left');
$f_header->setBold();

$f_title =& $xls->addFormat();
$f_title->setSize(10);
$f_title->setAlign('left');
$f_title->setBold();

$f_group_title =& $xls->addFormat();
$f_group_title->setSize(10);
$f_group_title->setAlign('left');
$f_group_title->setBold();

$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);

$f_normal_bold =& $xls->addFormat();
$f_normal_bold->setSize(8);
$f_normal_bold->setAlign('left');
$f_normal_bold->setBorder(1);
$f_normal_bold->setBold();

$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_number_bold =& $xls->addFormat();
$f_number_bold->setSize(8);
$f_number_bold->setAlign('right');
$f_number_bold->setBorder(1);
$f_number_bold->setBold();

$f_unused =& $xls->addFormat();
$f_unused->setSize(8);
$f_unused->setBorder(1);
$f_unused->setPattern(2);
$f_unused->setBgColor('silver');

$f_yellow =& $xls->addFormat();
$f_yellow->setSize(8);
$f_yellow->setBorder(1);
$f_yellow->setPattern(18);
$f_yellow->setBgColor(31);
$f_yellow->setColor('black');
$f_yellow->setBold();

$f_border_left=& $xls->addFormat();
$f_border_left->setTop(1);
$f_border_left->setBottom(1);
$f_border_left->setLeft(1);
$f_border_left->setSize(8);
$f_border_left->setAlign('left');
$f_border_left->setBold();

$f_border_right=& $xls->addFormat();
$f_border_right->setTop(1);
$f_border_right->setBottom(1);
$f_border_right->setRight(1);


$f_border_middle=& $xls->addFormat();
$f_border_middle->setTop(1);
$f_border_middle->setBottom(1);

$f_comment =& $xls->addFormat();
$f_comment->setSize(8);
$f_comment->setAlign('left');
$f_comment->setBorder(0);

//benchmark rows
$cer_captions = array();

$cer_captions[0] = "Currency";
$cer_captions[1] = "Sales surface in m2";
$cer_captions[2] = "Full time equivalent";
$cer_captions[3] = "Lease term in years";
$cer_captions[4] = "Watches - Units";
$cer_captions[5] = "Jewelery - Units";
$cer_captions[6] = "Watches - Average Price";
$cer_captions[7] = "Jewelery - Average Price";
$cer_captions[8] = "Sales Value Watches"; 
$cer_captions[9] = "Sales Value Jewelery";
$cer_captions[10] = "Sales Value Accessories";
$cer_captions[11] = "Customer Serivces";
$cer_captions[12] = "Total Gross Sales";
$cer_captions[13] = "Sales Reduction";
$cer_captions[14] = "Total Net Sales";

$cer_captions[15] = "Material of Products Sold";
$cer_captions[16] = "Total Gross Margin";
$cer_captions[17] = "Marketing Expenses";

$cer_captions[18] = "Indirect Salaries";
$cer_captions[19] = "Rents";
$cer_captions[20] = "Aux. Mat., energy, maintenance";
$cer_captions[21] = "Sales & administration expenses";
$cer_captions[22] = "Depreciation on fixed assets";
$cer_captions[23] = "Depreciation on Key Money";
$cer_captions[24] = "Other indirect Expenses";
$cer_captions[25] = "Total Indirect Expenses";
$cer_captions[26] = "Operating Income w/o Wholesale Margin";

$cer_captions[27] = "Investments";
$cer_captions[28] = "Local construction cost";
$cer_captions[29] = "Store fixturing / furniture";
$cer_captions[30] = "Architectural costs";
$cer_captions[31] = "Equipment";
$cer_captions[32] = "Other costs";
$cer_captions[33] = "Total Investment in Fixed Assets";
$cer_captions[34] = "Key-/Premium Money/Goodwill";
$cer_captions[35] = "Deposit/Recoverable Keymoney";
$cer_captions[36] = "Other non-capitalizable fees and taxes";
$cer_captions[37] = "Total Project costs (Requested amount)";

$cer_captions[38] = "KL Approved Investments";
$cer_captions[39] = "Local construction cost";
$cer_captions[40] = "Store fixturing / furniture";
$cer_captions[41] = "Architectural costs";
$cer_captions[42] = "Equipment";
$cer_captions[43] = "Other costs";
$cer_captions[44] = "Total KL approved Investment for Fixed Assets";
$cer_captions[45] = "Key-/Premium Money/Goodwill";
$cer_captions[46] = "Deposit/Recoverable Keymoney";
$cer_captions[47] = "Other non-capitalizable fees and taxes";
$cer_captions[48] = "Total KL approved Investments for Project";




$cms_captions = array();

$cms_captions[1] = "Local construction cost";
$cms_captions[2] = "Store fixturing / furniture";
$cms_captions[3] = "Architectural costs";
$cms_captions[4] = "Equipment";
$cms_captions[5] = "Other costs";
$cms_captions[6] = "Total Real Costs of Fixed Assets";
$cms_captions[7] = "Key-/Premium Money/Goodwill";
$cms_captions[8] = "Deposit/Recoverable Keymoney";
$cms_captions[9] = "Other non-capitalizable fees and taxes";
$cms_captions[10] = "Total Real Costs";


/********************************************************************
    write all captions
*********************************************************************/
$sheet->write(0, 0, $benchmark_title, $f_header);
$sheet->write(1, 0, "Benchmark horizontal (" . date("d.m.Y, H:m:s") . ")", $f_title);


//$sheet->setRow(4, 165);
//$sheet->writeRow(4, 0, $captions, $f_caption);

/********************************************************************
    write all data
*********************************************************************/
$list_totals = array();
$num_of_projects = 0;


$c01 = 0;
$row_index = 11;
$sheet->setColumn($c01, $c01, 38);
$sheet->write($row_index, $c01, "CER/AF Estimations (Business Plan)", $f_title);
$row_index++;


foreach($cer_captions as $key=>$caption)
{
	if($key == 12 or $key == 14 or $key == 16 or $key == 17 
		or $key == 25 or $key == 26 or $key == 33 or $key == 37
		or $key == 44 or $key == 48)
	{
		$sheet->write($row_index, $c01, $caption, $f_normal_bold);
	}
	elseif($key == 27 or $key == 38)
	{
		$row_index++;
		$sheet->write($row_index, $c01, $caption, $f_title);
	}
	else
	{
		$sheet->write($row_index, $c01, $caption, $f_normal);
	}
	
	$row_index++;
}


$row_index++;
//$row_index++;

$sheet->write($row_index, $c01, "CMS (real cost)", $f_title);
$row_index++;
foreach($cms_captions as $key=>$caption)
{
	
	if($key == 6 or $key == 10)
	{
		$sheet->write($row_index, $c01, $caption, $f_normal_bold);
	}
	else
	{
		$sheet->write($row_index, $c01, $caption, $f_normal);
	}
	$row_index++;
}


$sheet->setColumn(1, 1, 2);
$col_index01 = 2;
$col_index02 = 2;
$list_avgs = array();


foreach($posorders as $key=>$posorder)
{
	if($posorder["pipeline"] == 1)
	{
		$sql = "select * " . 
			   "from posaddressespipeline " . 
			   "left join posorderspipeline on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " .
			   "left join orders on order_id = project_order " . 
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "left join project_costs on project_cost_order = posorder_order " . 
			   "where posorder_type = 1 and posorder_id = " . $posorder["id"];

	}
	elseif($posorder["pipeline"] == 2) // renovation project
	{
		$sql = "select * " . 
			   "from posaddresses " . 
			   "left join posorderspipeline on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " .
			   "left join orders on order_id = project_order " . 
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "left join project_costs on project_cost_order = posorder_order " .
			   "where posorder_type = 1 and posorder_id = " . $posorder["id"];

	}
	else
	{
		$sql = "select * " . 
			   "from posaddresses " . 
			   "left join posorders on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " .
			   "left join orders on order_id = project_order " . 
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "left join project_costs on project_cost_order = posorder_order " .
			   "where posorder_type = 1 and posorder_id = " . $posorder["id"];

		
	}


	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		
		//$result = update_posdata_from_posorders($row["posaddress_id"]);
		//$result = build_missing_investment_records($row["posorder_id"]);
		$result = update_investment_records_from_system_currency($row["posorder_id"]);
		$result = update_investment_records_from_local_currency($row["posorder_id"]);
		$num_of_projects++;

		//get benchmark first full years
		$benchmark_year = 0;
		$exchange_rate = 1;
		$factor = 1;

		$cer_basicdata = get_cer_basicdata($row["project_id"]);

		if(count($cer_basicdata) > 0)
		{
			$first_year = $cer_basicdata["cer_basicdata_firstyear"];
			$first_month = $cer_basicdata["cer_basicdata_firstyear"];

			if($first_month == 1)
			{
				$benchmark_year = $cer_basicdata["cer_basicdata_firstyear"];
			}
			else
			{
				$benchmark_year = $cer_basicdata["cer_basicdata_firstyear"] + 1;
			}
			$exchange_rate = $cer_basicdata["cer_basicdata_exchangerate"];
			$factor = $cer_basicdata["cer_basicdata_factor"];

			if(!$factor) {$factor = 1;}
		}
		
		$row_index = 3;

		$sheet->setColumn($col_index01, $col_index01, 9);
		$sheet->setColumn($col_index01, $col_index01+1, 9);
		$sheet->setColumn($col_index01, $col_index01+2, 9);
		$sheet->setColumn($col_index01, $col_index01+4, 12);
		$sheet->setColumn($col_index01, $col_index01+4, 2);
		
		$sheet->write($row_index, $col_index01, $row["posaddress_name"], $f_border_left);
		$sheet->write($row_index, $col_index01+1,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+2,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+3,"", $f_border_right);
		$row_index++;
		$sheet->write($row_index, $col_index01, $row["country_name"] . ", " . $row["posaddress_place"], $f_border_left);
		$sheet->write($row_index, $col_index01+1,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+2,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+3,"", $f_border_right);
		$row_index++;

		$product_line_name = $row["product_line_name"];
		if($row["productline_subclass_name"]) {
			$product_line_name .= " / " . $row["productline_subclass_name"];
		}
		
		$sheet->write($row_index, $col_index01, $row["postype_name"] . ": " . $product_line_name, $f_border_left);
		$sheet->write($row_index, $col_index01+1,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+2,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+3,"", $f_border_right);
		$row_index++;
		
		$tmp = "Project : " . $row["order_number"] . ' / Kind: ' . $row["projectkind_code"] . ' / Satus: ' . $row["order_actual_order_state_code"];
		$sheet->write($row_index, $col_index01, $tmp, $f_border_left);
		$sheet->write($row_index, $col_index01+1,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+2,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+3,"", $f_border_right);
		$row_index++;

		$sheet->write($row_index, $col_index01, "Benchmark Full Year : " . $benchmark_year, $f_border_left);
		$sheet->write($row_index, $col_index01+1,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+2,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+3,"", $f_border_right);
		$row_index++;
		$row_index++;

		$sheet->write($row_index, $col_index01, "Amount", $f_yellow);
		$sheet->write($row_index, $col_index01+1,"per sqm", $f_yellow);
		$sheet->write($row_index, $col_index01+2,"per fte", $f_yellow);
		$sheet->write($row_index, $col_index01+3,"% of net sales", $f_yellow);
		
		$row_index = 12;
		//curency
		$currency_symbol = "";
		$sales_reduction_percent = 0;
		$sales_reduction = 0;
		$sql_s = "select * " . 
			     "from cer_revenues " . 
			     "where cer_revenue_cer_version = 0 " .
			     " and cer_revenue_project = " . dbquote($row["project_id"]) .
			     " and cer_revenue_year  = " . $benchmark_year . 
			     " order by cer_revenue_brand_id, cer_revenue_year";

		$res_s = mysql_query($sql_s) or dberror($sql_s);
		while($row_s = mysql_fetch_assoc($res_s))
		{
			$sales_reduction_watches = $row_s["cer_revenue_sales_reduction_watches"] / 100;
			$sales_reduction_bijoux = $row_s["cer_revenue_sales_reduction_bijoux"] / 100;
			$sales_reduction_accessories = $row_s["cer_revenue_sales_reduction_accessories"] / 100;
			$sales_reduction_services = $row_s["cer_revenue_sales_reduction_cservice"] / 100;
			//$sales_reduction_creditcard = $row_s["cer_revenue_reduction_credit_cards"] / 100;



			$srw = $row_s["cer_revenue_watches"] * $sales_reduction_watches;
			$srb = $row_s["cer_revenue_jewellery"] * $sales_reduction_bijoux;
			$sra = $row_s["cer_revenue_accessories"] * $sales_reduction_accessories;
			$src = $row_s["cer_revenue_customer_service"] * $sales_reduction_services;

			//$scard = ($row_s["cer_revenue_watches"] + $row_s["cer_revenue_jewellery"] + $row_s["cer_revenue_accessories"] + $row_s["cer_revenue_customer_service"]) * $sales_reduction_creditcard;
			//$sales_reduction = $sales_reduction + $srw + $srb + $sra + $src + $scard;

			$sales_reduction = $sales_reduction + $srw + $srb + $sra + $src;

		}


		//get currency_symbol
		$sql_s = "select currency_symbol " . 
			   " from cer_basicdata " . 
			   " left join currencies on currency_id = cer_basicdata_currency " . 
			   " where cer_basicdata_version = 0 and cer_basicdata_project = " . dbquote($row["project_id"]);

		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$currency_symbol = $row_s["currency_symbol"];
		}
		
		
		if($base_currency == "chf")
		{
			$currency_symbol = "CHF";
		}
		else
		{
			$exchange_rate = 1;
			$factor = 1;
		}

		$sheet->write($row_index, $col_index01, $currency_symbol, $f_number);
		$row_index++;

		//sales surface

		
		$retail_area = $row["project_cost_sqms"];
		$sheet->write($row_index, $col_index01, number_format($retail_area, 2, ".", ""), $f_number);
		$row_index++;
		if(array_key_exists("project_cost_sqms", $list_avgs))
		{
			$list_avgs["project_cost_sqms"] = $list_avgs["project_cost_sqms"] + $retail_area;
		}
		else
		{
			$list_avgs["project_cost_sqms"] = $retail_area;
		}

		//ful time equivalent
		// get total ftes
		$ftes = 0;
		$sql_s = "select sum(cer_salary_headcount_percent) as fte " . 
				 "from cer_salaries " .
				 "left join projects on project_id = cer_salary_project " . 
				 "left join orders on order_id = project_order " . 
				 "where cer_salary_cer_version = 0 and order_id = '" . $row["posorder_order"] . "'" . 
			     " and cer_salary_year_starting <= " . $benchmark_year;
		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		while($row_s = mysql_fetch_assoc($res_s))
		{
			$ftes = $ftes + $row_s["fte"];
		}
		
		$ftes = round($ftes/100,2);
		$sheet->write($row_index, $col_index01, $ftes, $f_number);
		if(array_key_exists("ftes", $list_avgs))
		{
			$list_avgs["ftes"] = $list_avgs["ftes"] + $ftes;
		}
		else
		{
			$list_avgs["ftes"] = $ftes;
		}


		$ftes_per_sqm = 0;
		if($ftes > 0)
		{
			$ftes_per_sqm = round($retail_area/$ftes,2);
		}
		$sheet->write($row_index, $col_index01+1, $ftes_per_sqm, $f_number);
		$row_index++;

		//lease terms
		//get the lease duration in years
		$starting_date = 0;
		$ending_date = 0;
		$extension_option = 0;
		$exit_option = 0;

		if($posorder["pipeline"] == 1)
		{
			$sql_s = "select poslease_startdate, poslease_enddate, poslease_extensionoption, poslease_exitoption " . 
					 "from posleases " . 
					 "where poslease_posaddress = " . $row["posaddress_id"];
		}
		else
		{
			$sql_s = "select poslease_startdate, poslease_enddate, poslease_extensionoption, poslease_exitoption " . 
					 "from posleasespipeline " . 
					 "where poslease_posaddress = " . $row["posaddress_id"];
		}
		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		while($row_s = mysql_fetch_assoc($res_s))
		{
			if($starting_date == 0)
			{
				$starting_date = $row_s["poslease_startdate"];
			}
			elseif($row_s["poslease_startdate"] < $starting_date)
			{
				$starting_date = $row_s["poslease_startdate"];
			}

			if($ending_date == 0)
			{
				$ending_date = $row_s["poslease_enddate"];
			}
			elseif($row_s["poslease_enddate"] > $ending_date)
			{
				$ending_date = $row_s["poslease_enddate"];
			}

			if($extension_option == 0)
			{
				$extension_option = $row_s["poslease_extensionoption"];
			}
			elseif($row_s["poslease_extensionoption"] > $extension_option)
			{
				$extension_option = $row_s["poslease_extensionoption"];
			}

			if($exit_option == 0)
			{
				$exit_option = $row_s["poslease_exitoption"];
			}
			elseif($row_s["poslease_exitoption"] > $exit_option)
			{
				$exit_option = $row_s["poslease_exitoption"];
			}
		}
		if($starting_date == 0 and $ending_date == 0)
		{
			$lease_period = 0;
		}
		else
		{
			$start = strtotime($starting_date) ;
			$end = strtotime($ending_date) ;

			$lease_period = $end - $start;
			$lease_period = $lease_period / 86400;
			$lease_period = round($lease_period/365, 2);

		}

		$sheet->write($row_index, $col_index01, $lease_period, $f_number);
		$row_index++;
		if(array_key_exists("lease_period", $list_avgs))
		{
			$list_avgs["lease_period"] = $list_avgs["lease_period"] + $lease_period;
		}
		else
		{
			$list_avgs["lease_period"] = $lease_period;
		}

		//get revenues
		$watches = 0;
		$jewellery = 0;
		$watches_revenue = 0;
		$jewellery_revenue = 0;
		$services_revenue = 0;
		$accessories_revenue = 0;

		$sql_s = "select sum(cer_revenue_quantity_watches) as watches, " .
			     "sum(cer_revenue_quantity_jewellery) as jewellery,  " .
			     "sum(cer_revenue_watches) as watches_revenue,  " .
			     "sum(cer_revenue_jewellery) as jewellery_revenue,  " .
			     "sum(cer_revenue_accessories) as accessories_revenue,  " .
			     "sum(cer_revenue_customer_service) as services_revenue  " .
				 "from cer_revenues " .
				 "left join projects on project_id = cer_revenue_project " . 
				 "left join orders on order_id = project_order " . 
				 "where cer_revenue_cer_version = 0 and cer_revenue_project = " . dbquote($row["project_id"]) . 
			     " and cer_revenue_year = " . $benchmark_year;

		$res_s = mysql_query($sql_s) or dberror($sql_s);
		while($row_s = mysql_fetch_assoc($res_s))
		{
			$watches = $watches + $row_s["watches"];
			$jewellery = $jewellery + $row_s["jewellery"];

			$watches_revenue = $watches_revenue + $row_s["watches_revenue"];
			$jewellery_revenue = $jewellery_revenue + $row_s["jewellery_revenue"];
			$accessories_revenue = $accessories_revenue + $row_s["accessories_revenue"];
			$services_revenue = $services_revenue + $row_s["services_revenue"];
		}
		
		$watches_revenue = $watches_revenue * $exchange_rate / $factor;
		$jewellery_revenue = $jewellery_revenue * $exchange_rate / $factor;
		$accessories_revenue = $accessories_revenue * $exchange_rate / $factor;
		$services_revenue = $services_revenue * $exchange_rate / $factor;
		$total_revenue = $watches_revenue + $jewellery_revenue + $accessories_revenue + $services_revenue;

		
		//???
		$percent_base = 100 + 100*$sales_reduction_percent;
		$base_unit = $total_revenue / $percent_base;

		
		if(array_key_exists("sales_reduction_percent", $list_avgs))
		{
			$list_avgs["sales_reduction_percent"] = $list_avgs["sales_reduction_percent"] + 100*$sales_reduction_percent;
		}
		else
		{
			$list_avgs["sales_reduction_percent"] = 100*$sales_reduction_percent;
		}

				

		//calculate average prices
		$watches_price = 0;
		$jewellery_price = 0;

		if($watches > 0)
		{
			$watches_price = round($watches_revenue/$watches, 2);
		}

		if($jewellery > 0)
		{
			$jewellery_price = round($jewellery_revenue/$jewellery, 2);
		}

		
		$sheet->write($row_index, $col_index01, $watches, $f_number);
		if(array_key_exists("watches", $list_avgs))
		{
			$list_avgs["watches"] = $list_avgs["watches"] + $watches;
		}
		else
		{
			$list_avgs["watches"] = $watches;
		}
		$watches_per_sqm = 0;
		if($retail_area > 0)
		{
			$watches_per_sqm = round($watches/$retail_area,2);
		}
		$sheet->write($row_index, $col_index01+1, $watches_per_sqm, $f_number);
		$watches_per_fte = 0;
		if($ftes > 0)
		{
			$watches_per_fte = round($watches/$ftes,2);
		}
		$sheet->write($row_index, $col_index01+2, $watches_per_fte, $f_number);
		$row_index++;

		$sheet->write($row_index, $col_index01, $jewellery, $f_number);
		if(array_key_exists("jewellery", $list_avgs))
		{
			$list_avgs["jewellery"] = $list_avgs["jewellery"] + $jewellery;
		}
		else
		{
			$list_avgs["jewellery"] = $jewellery;
		}
		$jewellery_per_sqm = 0;
		if($retail_area > 0)
		{
			$jewellery_per_sqm = round($jewellery/$retail_area,2);
		}
		$sheet->write($row_index, $col_index01+1, $jewellery_per_sqm, $f_number);
		$jewellery_per_fte = 0;
		if($ftes > 0)
		{
			$jewellery_per_fte = round($jewellery/$ftes,2);
		}
		$sheet->write($row_index, $col_index01+2, $jewellery_per_fte, $f_number);
		$row_index++;


		$sheet->write($row_index, $col_index01, $watches_price, $f_number);
		$row_index++;
		if(array_key_exists("watches_price", $list_avgs))
		{
			$list_avgs["watches_price"] = $list_avgs["watches_price"] + $watches_price;
		}
		else
		{
			$list_avgs["watches_price"] = $watches_price;
		}

		$sheet->write($row_index, $col_index01, $jewellery_price, $f_number);
		$row_index++;
		if(array_key_exists("jewellery_price", $list_avgs))
		{
			$list_avgs["jewellery_price"] = $list_avgs["jewellery_price"] + $jewellery_price;
		}
		else
		{
			$list_avgs["jewellery_price"] = $jewellery_price;
		}


		//revenues


		$gross_sales = $watches_revenue + $jewellery_revenue + $accessories_revenue + $services_revenue;
		$net_sales = $gross_sales - $sales_reduction;



		$sheet->write($row_index, $col_index01, number_format($watches_revenue, 0, ".", ""), $f_number);
		if(array_key_exists("watches_revenue", $list_avgs))
		{
			$list_avgs["watches_revenue"] = $list_avgs["watches_revenue"] + $watches_revenue;
		}
		else
		{
			$list_avgs["watches_revenue"] = $watches_revenue;
		}

		$watches_revenue_per_sqm = 0;
		if($retail_area > 0)
		{
			$watches_revenue_per_sqm = round($watches_revenue/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $watches_revenue_per_sqm, $f_number);

		$watches_revenue_per_fte = 0;
		if($ftes > 0)
		{
			$watches_revenue_per_fte = round($watches_revenue/$ftes,0);
		}
		$sheet->write($row_index, $col_index01+2, $watches_revenue_per_fte, $f_number);

		$percent_of_watches = "0.00%";
		$percent_of_net_sales = 0;
		if($net_sales > 0)
		{
			$percent_of_watches = number_format(100*$watches_revenue/$net_sales, 2, ".", "") . "%";
			$percent_of_net_sales = $watches_revenue/$net_sales;
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_watches, $f_number);

		$row_index++;


		$sheet->write($row_index, $col_index01, number_format($jewellery_revenue, 0, ".", ""), $f_number);
		if(array_key_exists("jewellery_revenue", $list_avgs))
		{
			$list_avgs["jewellery_revenue"] = $list_avgs["jewellery_revenue"] + $jewellery_revenue;
		}
		else
		{
			$list_avgs["jewellery_revenue"] = $jewellery_revenue;
		}

		$jewellery_revenue_per_sqm = 0;
		if($retail_area > 0)
		{
			$jewellery_revenue_per_sqm = round($jewellery_revenue/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $jewellery_revenue_per_sqm, $f_number);

		$jewellery_revenue_per_fte = 0;
		if($ftes > 0)
		{
			$jewellery_revenue_per_fte = round($jewellery_revenue/$ftes,0);
		}
		$sheet->write($row_index, $col_index01+2, $jewellery_revenue_per_fte, $f_number);

		$percent_of_jewellery = "0.00%";
		if($net_sales > 0)
		{
			$percent_of_jewellery = number_format(100*$jewellery_revenue/$net_sales, 2, ".", "") . "%";
			$percent_of_net_sales = $percent_of_net_sales + ($jewellery_revenue/$net_sales);
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_jewellery, $f_number);

		$row_index++;


		$sheet->write($row_index, $col_index01, number_format($accessories_revenue, 0, ".", ""), $f_number);
		if(array_key_exists("accessories_revenue", $list_avgs))
		{
			$list_avgs["accessories_revenue"] = $list_avgs["accessories_revenue"] + $accessories_revenue;
		}
		else
		{
			$list_avgs["accessories_revenue"] = $accessories_revenue;
		}

		$accessories_revenue_per_sqm = 0;
		if($retail_area > 0)
		{
			$accessories_revenue_per_sqm = round($accessories_revenue/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $accessories_revenue_per_sqm, $f_number);

		$accessories_revenue_per_fte = 0;
		if($ftes > 0)
		{
			$accessories_revenue_per_fte = round($accessories_revenue/$ftes,0);
		}
		$sheet->write($row_index, $col_index01+2, $accessories_revenue_per_fte, $f_number);

		$percent_of_service = "0.00%";
		if($net_sales > 0)
		{
			$percent_of_service = number_format(100*$accessories_revenue/$net_sales, 2, ".", "") . "%";
			$percent_of_net_sales = $percent_of_net_sales + ($accessories_revenue/$net_sales);
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_service, $f_number);

		$row_index++;
		
		
		$sheet->write($row_index, $col_index01, number_format($services_revenue, 0, ".", ""), $f_number);
		if(array_key_exists("services_revenue", $list_avgs))
		{
			$list_avgs["services_revenue"] = $list_avgs["services_revenue"] + $services_revenue;
		}
		else
		{
			$list_avgs["services_revenue"] = $services_revenue;
		}

		$services_revenue_per_sqm = 0;
		if($retail_area > 0)
		{
			$services_revenue_per_sqm = round($services_revenue/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $services_revenue_per_sqm, $f_number);

		$services_revenue_per_fte = 0;
		if($ftes > 0)
		{
			$services_revenue_per_fte = round($services_revenue/$ftes,0);
		}
		$sheet->write($row_index, $col_index01+2, $services_revenue_per_fte, $f_number);

		$percent_of_service = "0.00%";
		if($net_sales > 0)
		{
			$percent_of_service = number_format(100*$services_revenue/$net_sales, 2, ".", "") . "%";
			$percent_of_net_sales = $percent_of_net_sales + ($services_revenue/$net_sales);
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_service, $f_number);

		$row_index++;
		
		
		
		//gross sales
		
		$sheet->write($row_index, $col_index01, number_format($gross_sales, 0, ".", ""), $f_number_bold);
		if(array_key_exists("gross_sales", $list_avgs))
		{
			$list_avgs["gross_sales"] = $list_avgs["gross_sales"] + $gross_sales;
		}
		else
		{
			$list_avgs["gross_sales"] = $gross_sales;
		}
		$gross_sales_per_sqm = 0;
		if($retail_area > 0)
		{
			$gross_sales_per_sqm = round($gross_sales/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $gross_sales_per_sqm, $f_number_bold);
		$gross_sales_per_fte = 0;
		if($ftes > 0)
		{
			$gross_sales_per_fte = round($gross_sales/$ftes,0);
		}
		$sheet->write($row_index, $col_index01+2, $gross_sales_per_fte, $f_number_bold);

		$percent_of_net_sales = number_format(100*$percent_of_net_sales, 2, ".", "") . "%";
		$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_number_bold);


		$row_index++;


		

		$sheet->write($row_index, $col_index01, number_format($sales_reduction, 0, ".", ""), $f_number);
		if(array_key_exists("sales_reduction", $list_avgs))
		{
			$list_avgs["sales_reduction"] = $list_avgs["sales_reduction"] + $sales_reduction;
		}
		else
		{
			$list_avgs["sales_reduction"] = $sales_reduction;
		}

		$sales_reduction_per_sqm = 0;
		if($retail_area > 0)
		{
			$sales_reduction_per_sqm = round($sales_reduction/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $sales_reduction_per_sqm, $f_number);
		$sales_reduction_per_fte = 0;
		if($ftes > 0)
		{
			$sales_reduction_per_fte = round($sales_reduction/$ftes,0);
		}
		$sheet->write($row_index, $col_index01+2, $sales_reduction_per_fte, $f_number);

		
		$net_sales = $gross_sales - $sales_reduction;
		
		if($net_sales > 0)
		{
			$sales_reduction_precent = 100*$sales_reduction / $net_sales;
		}
		else
		{
			$sales_reduction_precent = 0;
		}

		$sales_reduction_precent = -1*number_format($sales_reduction_precent, 2, ".", "") . "%";
		$sheet->write($row_index, $col_index01+3, $sales_reduction_precent, $f_number_bold);

		$row_index++;

		
		//net sales
		$sheet->write($row_index, $col_index01, number_format($net_sales, 0, ".", ""), $f_number_bold);
		if(array_key_exists("net_sales", $list_avgs))
		{
			$list_avgs["net_sales"] = $list_avgs["net_sales"] + $net_sales;
		}
		else
		{
			$list_avgs["net_sales"] = $net_sales;
		}
		$net_sales_per_sqm = 0;
		if($retail_area > 0)
		{
			$net_sales_per_sqm = round($net_sales/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $net_sales_per_sqm, $f_number_bold);
		$net_sales_per_fte = 0;
		if($ftes > 0)
		{
			$net_sales_per_fte = round($net_sales/$ftes,0);
		}
		$sheet->write($row_index, $col_index01+2, $net_sales_per_fte, $f_number_bold);
		$sheet->write($row_index, $col_index01+3, "100.00%", $f_number_bold);

		$row_index++;
		

		//get expenses
		$total_cost = 0;
		$expenses = array();
		$sql_s = "select cer_expense_type, sum(cer_expense_amount) as total " . 
				 "from cer_expenses " . 
				 "where cer_expense_cer_version = 0 and cer_expense_project = " . dbquote($row["project_id"]) . 
			     " and cer_expense_year = " . dbquote($benchmark_year) . 
			     " group by cer_expense_type";

	
		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		while($row_s = mysql_fetch_assoc($res_s))
		{
			$expenses[$row_s["cer_expense_type"]] = $row_s["total"];

			if($row_s["cer_expense_type"] == 17 and array_key_exists(1, $expenses))
			{
				$expenses[1] = $expenses[1] + $row_s["total"];
			}
		}
		foreach($expenses as $key=>$value)
		{
			if($key == 6 or $key == 7 or $key == 8 or $key == 10 or $key == 11 or  $key == 12 )
			{
				$expenses[10] = $expenses[10] + $value;
			}
		}


		//Gross sales (net sales minus Material of products sold)
		$amount = 0;
		if(array_key_exists(15, $expenses))
		{
			$amount =  $expenses[15] * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, number_format($amount, 0, ".", ""), $f_number);
			if(array_key_exists("e15", $list_avgs))
			{
				$list_avgs["e15"] = $list_avgs["e15"] + $amount;
			}
			else
			{
				$list_avgs["e15"] = $amount;
			}
			$expenses_per_sqm = 0;
			if($retail_area > 0)
			{
				$expenses_per_sqm = round($amount/$retail_area,0);
			}
			$sheet->write($row_index, $col_index01+1, $expenses_per_sqm, $f_number);
			$expenses_per_fte = 0;
			if($ftes > 0)
			{
				$expenses_per_fte = round($amount/$ftes,0);
			}
			$sheet->write($row_index, $col_index01+2, $expenses_per_fte, $f_number);
			$percent_of_net_sales = "0.00%";
			if($net_sales > 0)
			{
				//$percent_of_net_sales = number_format(100 * round($amount/$net_sales,2), 2, ".", "") . "%";
				$percent_of_net_sales = -1*number_format(100 * $amount/$net_sales, 2, ".", "") . "%";
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01, "0", $f_number);
			$sheet->write($row_index, $col_index01+1, "0", $f_number);
			$sheet->write($row_index, $col_index01+2, "0", $f_number);
			$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
			if(array_key_exists("e15", $list_avgs))
			{
				$list_avgs["e15"] = $list_avgs["e15"] + 0;
			}
			else
			{
				$list_avgs["e15"] = 0;
			}
		}


		$row_index++;

		//gross margin

		$gross_margin = $net_sales - $amount;
		$sheet->write($row_index, $col_index01, number_format($gross_margin, 0, ".", ""), $f_number_bold);
		if(array_key_exists("gross_margin", $list_avgs))
		{
			$list_avgs["gross_margin"] = $list_avgs["gross_margin"] + $gross_margin;
		}
		else
		{
			$list_avgs["gross_margin"] = $gross_margin;
		}
		$gross_margin_per_sqm = 0;
		if($retail_area > 0)
		{
			$gross_margin_per_sqm = round($gross_margin/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $gross_margin_per_sqm, $f_number_bold);
		$gross_margin_per_fte = 0;
		if($ftes > 0)
		{
			$gross_margin_per_fte = round($gross_margin/$ftes,0);
		}
		$sheet->write($row_index, $col_index01+2, $gross_margin_per_fte, $f_number_bold);

		$percent_of_net_sales = "0.00%";
		if($net_sales > 0)
		{
			$percent_of_net_sales = number_format(100 * $gross_margin/$net_sales, 2, ".", "") . "%";
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_number_bold);

		$row_index++;


		//marketing expenses
		$amount = 0;
		if(array_key_exists(14, $expenses))
		{
			$amount = ($expenses[14] * $exchange_rate / $factor);
		}
		if(array_key_exists(13, $expenses))
		{
			if($amount > 0) 
			{
				$amount =  $amount - ($expenses[13] * $exchange_rate / $factor);
			}
			else
			{
				$amount =  $expenses[13] * $exchange_rate / $factor;
			}

			$sheet->write($row_index, $col_index01, number_format($amount, 0, ".", ""), $f_number_bold);
			if(array_key_exists("e13", $list_avgs))
			{
				$list_avgs["e13"] = $list_avgs["e13"] + $amount;
			}
			else
			{
				$list_avgs["e13"] = $amount;
			}
			$expenses_per_sqm = 0;
			if($retail_area > 0)
			{
				$expenses_per_sqm = round($amount/$retail_area,0);
			}
			$sheet->write($row_index, $col_index01+1, $expenses_per_sqm, $f_number_bold);
			$expenses_per_fte = 0;
			if($ftes > 0)
			{
				$expenses_per_fte = round($amount/$ftes,0);
			}
			$sheet->write($row_index, $col_index01+2, $expenses_per_fte, $f_number_bold);
			$percent_of_net_sales = "0.00%";
			if($net_sales > 0)
			{
				//$percent_of_net_sales = number_format(100 * round($amount/$net_sales,2), 2, ".", "") . "%";
				$percent_of_net_sales = -1*number_format(100 * $amount/$net_sales, 2, ".", "") . "%";
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_number_bold);
		}
		else
		{
			$sheet->write($row_index, $col_index01, "0", $f_number);
			$sheet->write($row_index, $col_index01+1, "0", $f_number);
			$sheet->write($row_index, $col_index01+2, "0", $f_number);
			$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
			if(array_key_exists("e13", $list_avgs))
			{
				$list_avgs["e13"] = $list_avgs["e13"] + 0;
			}
			else
			{
				$list_avgs["e13"] = 0;
			}
		}
		$marketing_expenses = $amount;

		$row_index++;
		
		

		//indirect salaries
		if(array_key_exists(1, $expenses))
		{
			$amount =  $expenses[1] * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, number_format($amount, 0, ".", ""), $f_number);
			if(array_key_exists("e1", $list_avgs))
			{
				$list_avgs["e1"] = $list_avgs["e1"] + $amount;
			}
			else
			{
				$list_avgs["e1"] = $amount;
			}
			$expenses_per_sqm = 0;
			if($retail_area > 0)
			{
				$expenses_per_sqm = round($amount/$retail_area,0);
			}
			$sheet->write($row_index, $col_index01+1, $expenses_per_sqm, $f_number);
			$expenses_per_fte = 0;
			if($ftes > 0)
			{
				$expenses_per_fte = round($amount/$ftes,0);
			}
			$sheet->write($row_index, $col_index01+2, $expenses_per_fte, $f_number);
			$percent_of_net_sales = "0.00%";
			if($net_sales > 0)
			{
				$percent_of_net_sales = -1*number_format(100 * $amount/$net_sales, 2, ".", "") . "%";
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_number);
			$total_cost = $total_cost +$amount;
		}
		else
		{
			$sheet->write($row_index, $col_index01, "0", $f_number);
			$sheet->write($row_index, $col_index01+1, "0", $f_number);
			$sheet->write($row_index, $col_index01+2, "0", $f_number);
			$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
			if(array_key_exists("e1", $list_avgs))
			{
				$list_avgs["e1"] = $list_avgs["e1"] + 0;
			}
			else
			{
				$list_avgs["e1"] = 0;
			}
		}

		$row_index++;

		
		//rents
		$rents = 0;
		if(array_key_exists(2, $expenses))
		{
			$rents = $expenses[2];
		}
		if(array_key_exists(3, $expenses))
		{
			$rents = $rents + $expenses[3];
		}
		if(array_key_exists(16, $expenses))
		{
			$rents = $rents + $expenses[16];
		}
		$rents =  $rents * $exchange_rate / $factor;

		$sheet->write($row_index, $col_index01, number_format($rents, 0, ".", ""), $f_number);
		if(array_key_exists("rents", $list_avgs))
		{
			$list_avgs["rents"] = $list_avgs["rents"] + $rents;
		}
		else
		{
			$list_avgs["rents"] = $rents;
		}
		$rents_per_sqm = 0;
		if($retail_area > 0)
		{
			$rents_per_sqm = round($rents/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $rents_per_sqm, $f_number);
		$rents_per_fte = 0;
		if($ftes > 0)
		{
			$rents_per_fte = round($rents/$ftes,0);
		}
		$sheet->write($row_index, $col_index01+2, $rents_per_fte, $f_number);
		$percent_of_net_sales = "0.00%";
		if($net_sales > 0)
		{
			$percent_of_net_sales = -1*number_format(100 * $rents/$net_sales, 2, ".", "") . "%";
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_number);
		$row_index++;
		$total_cost = $total_cost + $rents;

		//Aux Mat.
		if(array_key_exists(4, $expenses))
		{
			$amount =  $expenses[4] * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, number_format($amount, 0, ".", ""), $f_number);
			if(array_key_exists("e4", $list_avgs))
			{
				$list_avgs["e4"] = $list_avgs["e4"] + $amount;
			}
			else
			{
				$list_avgs["e4"] = $amount;
			}

			$expenses_per_sqm = 0;
			if($retail_area > 0)
			{
				$expenses_per_sqm = round($amount/$retail_area,0);
			}
			$sheet->write($row_index, $col_index01+1, $expenses_per_sqm, $f_number);
			$expenses_per_fte = 0;
			if($ftes > 0)
			{
				$expenses_per_fte = round($amount/$ftes,0);
			}
			$sheet->write($row_index, $col_index01+2, $expenses_per_fte, $f_number);
			$percent_of_net_sales = "0.00%";
			if($net_sales > 0)
			{
				$percent_of_net_sales = number_format(100 * $amount/$net_sales, 2, ".", "") . "%";
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_number);
			$total_cost = $total_cost + $amount;
		}
		else
		{
			$sheet->write($row_index, $col_index01, "0", $f_number);
			$sheet->write($row_index, $col_index01+1, "0", $f_number);
			$sheet->write($row_index, $col_index01+2, "0", $f_number);
			$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
			if(array_key_exists("e4", $list_avgs))
			{
				$list_avgs["e4"] = $list_avgs["e4"] + 0;
			}
			else
			{
				$list_avgs["e4"] = 0;
			}
		}
		$row_index++;

		//Sales Admin
		if(array_key_exists(5, $expenses))
		{
			$amount =  $expenses[5] * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, number_format($amount, 0, ".", ""), $f_number);
			if(array_key_exists("e5", $list_avgs))
			{
				$list_avgs["e5"] = $list_avgs["e5"] + $amount;
			}
			else
			{
				$list_avgs["e5"] = $amount;
			}
			$expenses_per_sqm = 0;
			if($retail_area > 0)
			{
				$expenses_per_sqm = round($amount/$retail_area,0);
			}
			$sheet->write($row_index, $col_index01+1, $expenses_per_sqm, $f_number);
			$expenses_per_fte = 0;
			if($ftes > 0)
			{
				$expenses_per_fte = round($amount/$ftes,0);
			}
			$sheet->write($row_index, $col_index01+2, $expenses_per_fte, $f_number);
			$percent_of_net_sales = "0.00%";
			if($net_sales > 0)
			{
				$percent_of_net_sales = -1*number_format(100 * $amount/$net_sales, 2, ".", "") . "%";
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_number);
			$total_cost = $total_cost + $amount;
		}
		else
		{
			$sheet->write($row_index, $col_index01, "0", $f_number);
			$sheet->write($row_index, $col_index01+1, "0", $f_number);
			$sheet->write($row_index, $col_index01+2, "0", $f_number);
			$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
			if(array_key_exists("e5", $list_avgs))
			{
				$list_avgs["e5"] = $list_avgs["e5"] + 0;
			}
			else
			{
				$list_avgs["e5"] = 0;
			}
		}
		$row_index++;


		
				
		//Depreciation on Investment
		$d = calculate_depreciation($cer_basicdata, $row["project_id"], 0);
		$depreciation = $d["investments"];
			
		if(is_array($depreciatio)
			and array_key_exists($benchmark_year, $depreciation))
		{
			$depreciation_benchmark_year = $depreciation[$benchmark_year];
		}
		else
		{
			$depreciation_benchmark_year = 0;
		}
		

		$depreciation_benchmark_year = $depreciation_benchmark_year * $exchange_rate / $factor;

		$sheet->write($row_index, $col_index01, round($depreciation_benchmark_year, 0), $f_number);
		if(array_key_exists("depreciation_benchmark_year", $list_avgs))
		{
			$list_avgs["depreciation_benchmark_year"] = $list_avgs["depreciation_benchmark_year"] + $depreciation_benchmark_year;
		}
		else
		{
			$list_avgs["depreciation_benchmark_year"] = $depreciation_benchmark_year;
		}
		$depreciation_benchmark_year_per_sqm = 0;
		if($retail_area > 0)
		{
			$depreciation_benchmark_year_per_sqm = round($depreciation_benchmark_year/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $depreciation_benchmark_year_per_sqm, $f_number);
		$depreciation_benchmark_year_per_fte = 0;
		if($ftes > 0)
		{
			$depreciation_benchmark_year_per_fte = round($depreciation_benchmark_year/$ftes,0);
		}
		$sheet->write($row_index, $col_index01+2, $depreciation_benchmark_year_per_fte, $f_number);
		$percent_of_net_sales = "0.00%";
		if($net_sales > 0)
		{
			$percent_of_net_sales = -1*number_format(100 * $depreciation_benchmark_year/$net_sales, 2, ".", "") . "%";
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_number);
		$row_index++;

		$total_cost = $total_cost + $depreciation_benchmark_year;

		//Depreciation on Key Money
		$depreciation_benchmark_year = 0;
		$prepayed_rents = $d["intangibles"];
		if(is_array($depreciatio)
			and array_key_exists($benchmark_year, $depreciation))
		{
			$depreciation_benchmark_year = $prepayed_rents[$benchmark_year];
		}
		else
		{
			$depreciation_benchmark_year = 0;
		}
		
		$depreciation_benchmark_year = $depreciation_benchmark_year * $exchange_rate / $factor;
		$sheet->write($row_index, $col_index01, round($depreciation_benchmark_year, 0), $f_number);
		if(array_key_exists("depreciation_benchmark_year_km", $list_avgs))
		{
			$list_avgs["depreciation_benchmark_year_km"] = $list_avgs["depreciation_benchmark_year_km"] + $depreciation_benchmark_year;
		}
		else
		{
			$list_avgs["depreciation_benchmark_year_km"] = $depreciation_benchmark_year;
		}
		$depreciation_benchmark_year_per_sqm = 0;
		if($retail_area > 0)
		{
			$depreciation_benchmark_year_per_sqm = round($depreciation_benchmark_year/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $depreciation_benchmark_year_per_sqm, $f_number);
		$depreciation_benchmark_year_per_fte = 0;
		if($ftes > 0)
		{
			$depreciation_benchmark_year_per_fte = round($depreciation_benchmark_year/$ftes,0);
		}
		$sheet->write($row_index, $col_index01+2, $depreciation_benchmark_year_per_fte, $f_number);
		$percent_of_net_sales = "0.00%";
		if($net_sales > 0)
		{
			$percent_of_net_sales = -1*number_format(100 * $depreciation_benchmark_year/$net_sales, 2, ".", "") . "%";
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_number);
		$row_index++;

		$total_cost = $total_cost + $depreciation_benchmark_year;

		//Other indirect expenses
		if(array_key_exists(10, $expenses))
		{
			$amount = $expenses[10] * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, number_format($amount, 0, ".", ""), $f_number);
			if(array_key_exists("e10", $list_avgs))
			{
				$list_avgs["e10"] = $list_avgs["e10"] + $amount;
			}
			else
			{
				$list_avgs["e10"] = $amount;
			}
			$expenses_per_sqm = 0;
			if($retail_area > 0)
			{
				$expenses_per_sqm = round($amount/$retail_area,0);
			}
			$sheet->write($row_index, $col_index01+1, $expenses_per_sqm, $f_number);
			$expenses_per_fte = 0;
			if($ftes > 0)
			{
				$expenses_per_fte = round($amount/$ftes,0);
			}
			$sheet->write($row_index, $col_index01+2, $expenses_per_fte, $f_number);
			$percent_of_net_sales = "0.00%";
			if($net_sales > 0)
			{
				$percent_of_net_sales = -1*number_format(100 * $amount/$net_sales, 2, ".", "") . "%";
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_number);
			$total_cost = $total_cost + $amount;
		}
		else
		{
			$sheet->write($row_index, $col_index01, "0", $f_number);
			$sheet->write($row_index, $col_index01+1, "0", $f_number);
			$sheet->write($row_index, $col_index01+2, "0", $f_number);
			$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
			if(array_key_exists("e10", $list_avgs))
			{
				$list_avgs["e10"] = $list_avgs["e10"] + 0;
			}
			else
			{
				$list_avgs["e10"] =0;
			}
		}
		$row_index++;

		
		// total cost
		
		$sheet->write($row_index, $col_index01, number_format($total_cost, 0, ".", ""), $f_number_bold);
		if(array_key_exists("total_cost", $list_avgs))
		{
			$list_avgs["total_cost"] = $list_avgs["total_cost"] + $total_cost;
		}
		else
		{
			$list_avgs["total_cost"] = $total_cost;
		}
		$total_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$total_cost_per_sqm = round($total_cost/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $total_cost_per_sqm, $f_number_bold);
		$total_cost_per_fte = 0;
		if($ftes > 0)
		{
			$total_cost_per_fte = round($total_cost/$ftes,0);
		}
		$sheet->write($row_index, $col_index01+2, $total_cost_per_fte, $f_number_bold);
		$percent_of_net_sales = "0.00%";
		if($net_sales > 0)
		{
			$percent_of_net_sales = -1*number_format(100 * $total_cost/$net_sales, 2, ".", "") . "%";
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_number_bold);
		$row_index++;


		//operational income
		$operational_income = $gross_margin + $marketing_expenses - $total_cost;
		$sheet->write($row_index, $col_index01, number_format($operational_income, 0, ".", ""), $f_number_bold);
		if(array_key_exists("operational_income", $list_avgs))
		{
			$list_avgs["operational_income"] = $list_avgs["operational_income"] + $operational_income;
		}
		else
		{
			$list_avgs["operational_income"] = $operational_income;
		}
		$operational_income_per_sqm = 0;
		if($retail_area > 0)
		{
			$operational_income_per_sqm = round($operational_income/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $operational_income_per_sqm, $f_number_bold);
		$operational_income_per_fte = 0;
		if($ftes > 0)
		{
			$operational_income_per_fte = round($operational_income/$ftes,0);
		}
		$sheet->write($row_index, $col_index01+2, $operational_income_per_fte, $f_number_bold);

		$percent_of_net_sales = "0.00%";
		if($net_sales > 0)
		{
			$percent_of_net_sales = number_format(100 * $operational_income/$net_sales, 2, ".", "") . "%";
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_number_bold);

		$row_index++;
		$row_index++;

		
		
		
		
		
		
		
		
		
		/********************************************************************
		investments
		*********************************************************************/
		$sheet->write($row_index, $col_index01, "Amount", $f_yellow);
		$sheet->write($row_index, $col_index01+1,"per sqm", $f_yellow);
		$sheet->write($row_index, $col_index01+2,"per fte", $f_yellow);
		$sheet->write($row_index, $col_index01+3,"% of invest.", $f_yellow);
		
		$row_index++;

		//get total investment in fixed assets
		$total_investment = 0;
		$total_investment_in_fixed_assets = 0;

		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and (cer_investment_type = 1 " . 
			     " or cer_investment_type = 3 " .
			     " or cer_investment_type = 5 " .
			     " or cer_investment_type = 7 " .
			     " or cer_investment_type = 11 " .
				 " or cer_investment_type = 9 " . 
			     " or cer_investment_type = 13 " .
			     " or cer_investment_type = 18 " .
			     " or cer_investment_type = 19 " .
			     " or cer_investment_type = 15 " .
			     " or cer_investment_type = 17) ";

		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$total_investment = $row_s["total"] * $exchange_rate / $factor;
		}

		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and (cer_investment_type = 1 " . 
			     " or cer_investment_type = 3 " .
			     " or cer_investment_type = 5 " .
			     " or cer_investment_type = 7 " .
			     " or cer_investment_type = 11 " .
			     " or cer_investment_type = 18 " .
			     " or cer_investment_type = 19 " .
			     " or cer_investment_type = 13) ";

		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$total_investment_in_fixed_assets = $row_s["total"] * $exchange_rate / $factor;
		}


		//get construction cost
		$construction = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type = 1";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$construction = $row_s["total"];
		}
		$construction = $construction * $exchange_rate / $factor;

		$sheet->write($row_index, $col_index01, number_format($construction, 0, ".", ""), $f_number);
		if(array_key_exists("construction", $list_avgs))
		{
			$list_avgs["construction"] = $list_avgs["construction"] + $construction;
		}
		else
		{
			$list_avgs["construction"] = $construction;
		}
		$investment_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$investment_cost_per_sqm = round($construction/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
		$investment_cost_per_fte = 0;
		if($ftes > 0)
		{
			$investment_cost_per_fte = round($construction/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
		$percent_of_total_cost = "0.00%";
		/*
		if($total_investment > 0)
		{
			$percent_of_total_cost = number_format(100 * $construction/$total_investment, 2, ".", "") . "%";
			
		}
		*/
		if($total_investment_in_fixed_assets > 0)
		{
			$percent_of_total_cost = number_format(100 * $construction/$total_investment_in_fixed_assets, 2, ".", "") . "%";
			
		}

		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_number);
		$row_index++;



		//get fixturing cost
		$fixturing = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type = 3";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$fixturing = $row_s["total"];
		}
		$fixturing = $fixturing * $exchange_rate / $factor;
		$sheet->write($row_index, $col_index01, number_format($fixturing, 0, ".", ""), $f_number);
		if(array_key_exists("fixturing", $list_avgs))
		{
			$list_avgs["fixturing"] = $list_avgs["fixturing"] + $fixturing;
		}
		else
		{
			$list_avgs["fixturing"] = $fixturing;
		}
		$investment_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$investment_cost_per_sqm = round($fixturing/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
		$investment_cost_per_fte = 0;
		if($ftes > 0)
		{
			$investment_cost_per_fte = round($fixturing/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
		$percent_of_total_cost = "0.00%";
		/*
		if($total_investment > 0)
		{
			$percent_of_total_cost = number_format(100 *$fixturing/$total_investment, 2, ".", "") . "%";
		}
		*/

		if($total_investment_in_fixed_assets > 0)
		{
			$percent_of_total_cost = number_format(100 *$fixturing/$total_investment_in_fixed_assets, 2, ".", "") . "%";
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_number);
		$row_index++;



		//get architectural cost
		$architectural = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type = 5";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$architectural = $row_s["total"];
		}
		$architectural = $architectural * $exchange_rate / $factor;
		$sheet->write($row_index, $col_index01, number_format($architectural, 0, ".", ""), $f_number);
		if(array_key_exists("architectural", $list_avgs))
		{
			$list_avgs["architectural"] = $list_avgs["architectural"] + $architectural;
		}
		else
		{
			$list_avgs["architectural"] = $architectural;
		}
		$investment_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$investment_cost_per_sqm = round($architectural/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
		$investment_cost_per_fte = 0;
		if($ftes > 0)
		{
			$investment_cost_per_fte = round($architectural/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
		$percent_of_total_cost = "0.00%";
		
		/*
		if($total_investment > 0)
		{
			$percent_of_total_cost = number_format(100 * $architectural/$total_investment, 2, ".", "") . "%";
		}
		*/

		if($total_investment_in_fixed_assets > 0)
		{
			$percent_of_total_cost = number_format(100 *$architectural/$total_investment_in_fixed_assets, 2, ".", "") . "%";
		}

		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_number);
		$row_index++;

		
		//get equipment cost
		$equipment = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type = 7";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$equipment = $row_s["total"];
		}
		$equipment = $equipment * $exchange_rate / $factor;
		$sheet->write($row_index, $col_index01, number_format($equipment, 0, ".", ""), $f_number);
		if(array_key_exists("equipment", $list_avgs))
		{
			$list_avgs["equipment"] = $list_avgs["equipment"] + $equipment;
		}
		else
		{
			$list_avgs["equipment"] = $equipment;
		}
		$investment_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$investment_cost_per_sqm = round($equipment/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
		$investment_cost_per_fte = 0;
		if($ftes > 0)
		{
			$investment_cost_per_fte = round($equipment/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
		$percent_of_total_cost = "0.00%";
		
		/*
		if($total_investment > 0)
		{
			$percent_of_total_cost = number_format(100 * $equipment/$total_investment, 2, ".", "") . "%";
		}
		*/

		if($total_investment_in_fixed_assets > 0)
		{
			$percent_of_total_cost = number_format(100 *$equipment/$total_investment_in_fixed_assets, 2, ".", "") . "%";
		}

		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_number);
		$row_index++;

		
		//get other cost
		$other = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type in (11, 18, 19)";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$other = $row_s["total"];
		}
		$other = $other * $exchange_rate / $factor;
		$sheet->write($row_index, $col_index01, number_format($other, 0, ".", ""), $f_number);
		if(array_key_exists("other", $list_avgs))
		{
			$list_avgs["other"] = $list_avgs["other"] + $other;
		}
		else
		{
			$list_avgs["other"] = $other;
		}
		$investment_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$investment_cost_per_sqm = round($other/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
		$investment_cost_per_fte = 0;
		if($ftes > 0)
		{
			$investment_cost_per_fte = round($other/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
		$percent_of_total_cost = "0.00%";
		
		/*
		if($total_investment > 0)
		{
			$percent_of_total_cost = number_format(100 * $other/$total_investment, 2, ".", "") . "%";
		}
		*/

		if($total_investment_in_fixed_assets > 0)
		{
			$percent_of_total_cost = number_format(100 *$other/$total_investment_in_fixed_assets, 2, ".", "") . "%";
		}

		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_number);
		$row_index++;



		//totel investment in fixed assets

		$total_cost = 0;

		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and (cer_investment_type = 1 " . 
			     " or cer_investment_type = 3 " .
			     " or cer_investment_type = 5 " .
			     " or cer_investment_type = 7 " .
			     " or cer_investment_type = 18 " .
			     " or cer_investment_type = 19 " .
			     " or cer_investment_type = 11) ";

		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$total_cost = $row_s["total"] * $exchange_rate / $factor;
		}

		$sheet->write($row_index, $col_index01, number_format($total_cost, 0, ".", ""), $f_number_bold);
		
		$total_cost_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$total_cost_per_sqm = round($total_cost/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $total_cost_per_sqm, $f_number_bold);
		$total_cost_cost_per_fte = 0;
		if($ftes > 0)
		{
			$total_cost_per_fte = round($total_cost/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $total_cost_per_fte, $f_number_bold);
		$percent_of_total_cost = "0.00%";
		/*
		if($total_investment > 0)
		{
			$percent_of_total_cost = number_format(100 * $total_cost/$total_investment, 2, ".", "") . "%";
		}
		*/

		if($total_investment_in_fixed_assets > 0)
		{
			$percent_of_total_cost = number_format(100 * $total_cost/$total_investment_in_fixed_assets, 2, ".", "") . "%";
		}


		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_number_bold);


		if(array_key_exists("total_cost2", $list_avgs))
		{
			$list_avgs["total_cost2"] = $list_avgs["total_cost2"] + $total_cost;
		}
		else
		{
			$list_avgs["total_cost2"] = $total_cost;
		}
		$row_index++;


		//get intangibles

		$intangibles = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and (cer_investment_type = 15 or cer_investment_type = 17)";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$intangibles = $row_s["total"];
		}
		$intangibles = $intangibles * $exchange_rate / $factor;
		$sheet->write($row_index, $col_index01, number_format($intangibles, 0, ".", ""), $f_number);
		if(array_key_exists("intangibles", $list_avgs))
		{
			$list_avgs["intangibles"] = $list_avgs["intangibles"] + $intangibles;
		}
		else
		{
			$list_avgs["intangibles"] = $intangibles;
		}
		$investment_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$investment_cost_per_sqm = round($intangibles/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
		$investment_cost_per_fte = 0;
		if($ftes > 0)
		{
			$investment_cost_per_fte = round($intangibles/$ftes,0);
		}
		$percent_of_total_cost = "0.00%";
		if($total_investment > 0)
		{
			$percent_of_total_cost = number_format(100 * $intangibles/$total_investment, 2, ".", "") . "%";
		}
		//$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_number);
		$sheet->write($row_index, $col_index01+3, "", $f_number);

		
		
		$row_index++;

	
		//get deposit cost
		$deposit = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type = 9";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			if(count($cer_basicdata) > 0)
			{
				$deposit = $row_s["total"] + $cer_basicdata["cer_basicdata_recoverable_keymoney"];
			}
			else
			{
				$deposit = $row_s["total"];
			}

		}
		$deposit = $deposit * $exchange_rate / $factor;
		$sheet->write($row_index, $col_index01, number_format($deposit, 0, ".", ""), $f_number);
		if(array_key_exists("deposit", $list_avgs))
		{
			$list_avgs["deposit"] = $list_avgs["deposit"] + $deposit;
		}
		else
		{
			$list_avgs["deposit"] = $deposit;
		}




		$investment_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$investment_cost_per_sqm = round($deposit/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
		$investment_cost_per_fte = 0;
		if($ftes > 0)
		{
			$investment_cost_per_fte = round($deposit/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
		$percent_of_total_cost = "0.00%";
		if($total_investment > 0)
		{
			$percent_of_total_cost = number_format(100 * $deposit/$total_investment, 2, ".", "") . "%";
		}
		//$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_number);
		$sheet->write($row_index, $col_index01+3, "", $f_number);
		$row_index++;

		//get noncapitalized cost
		$noncapitalized = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type = 13";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$noncapitalized = $row_s["total"];
		}
		$noncapitalized = $noncapitalized * $exchange_rate / $factor;
		$sheet->write($row_index, $col_index01, number_format($noncapitalized, 0, ".", ""), $f_number);
		if(array_key_exists("noncapitalized", $list_avgs))
		{
			$list_avgs["noncapitalized"] = $list_avgs["noncapitalized"] + $noncapitalized;
		}
		else
		{
			$list_avgs["noncapitalized"] = $noncapitalized;
		}
		$investment_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$investment_cost_per_sqm = round($noncapitalized/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
		$investment_cost_per_fte = 0;
		if($ftes > 0)
		{
			$investment_cost_per_fte = round($noncapitalized/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
		$percent_of_total_cost = "0.00%";
		if($total_investment > 0)
		{
			$percent_of_total_cost = number_format(100 * $noncapitalized/$total_investment, 2, ".", "") . "%";
		}
		//$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_number);
		$sheet->write($row_index, $col_index01+3, "", $f_number);
		$row_index++;

		
		
		//Total Project costs (Requested amount)
		$sheet->write($row_index, $col_index01, number_format($total_investment, 0, ".", ""), $f_number_bold);
		
		$total_cost_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$total_cost_per_sqm = round($total_investment/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $total_cost_per_sqm, $f_number_bold);
		$total_cost_cost_per_fte = 0;
		if($ftes > 0)
		{
			$total_cost_per_fte = round($total_investment/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $total_cost_per_fte, $f_number_bold);
		$percent_of_total_cost = "0.00%";
		if($total_cost > 0)
		{
			$percent_of_total_cost = number_format(100 * $total_investment/$total_investment, 2, ".", "") . "%";
		}
		//$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_number_bold);
		$sheet->write($row_index, $col_index01+3, "", $f_number_bold);


		if(array_key_exists("total_cost3", $list_avgs))
		{
			$list_avgs["total_cost3"] = $list_avgs["total_cost3"] + $total_investment;
		}
		else
		{
			$list_avgs["total_cost3"] = $total_investment;
		}

		$row_index++;
		$row_index++;
		
		
		
		//KL Approved investments
		$sheet->write($row_index, $col_index01, "Amount", $f_yellow);
		$sheet->write($row_index, $col_index01+1,"per sqm", $f_yellow);
		$sheet->write($row_index, $col_index01+2,"per fte", $f_yellow);
		$sheet->write($row_index, $col_index01+3,"% of invest.", $f_yellow);
		
		$row_index++;
		//get total investment in fixed assets
		$total_investment_approved_approved = 0;
		
		$sql_s = "select sum(cer_investment_amount_cer_loc_approved + cer_investment_amount_additional_cer_loc_approved) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and (cer_investment_type = 1 " . 
			     " or cer_investment_type = 3 " .
			     " or cer_investment_type = 5 " .
			     " or cer_investment_type = 7 " .
			     " or cer_investment_type = 11 " .
				 " or cer_investment_type = 9 " . 
			     " or cer_investment_type = 13 " .
			     " or cer_investment_type = 15 " .
			     " or cer_investment_type = 18 " .
			     " or cer_investment_type = 19 " .
			     " or cer_investment_type = 17) ";

		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$total_investment_approved = $row_s["total"] * $exchange_rate / $factor;
		}


		//get construction cost
		$construction = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc_approved + cer_investment_amount_additional_cer_loc_approved) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type = 1";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$construction = $row_s["total"];
		}
		$construction = $construction * $exchange_rate / $factor;

		$sheet->write($row_index, $col_index01, number_format($construction, 0, ".", ""), $f_number);
		if(array_key_exists("construction_approved", $list_avgs))
		{
			$list_avgs["construction_approved"] = $list_avgs["construction_approved"] + $construction;
		}
		else
		{
			$list_avgs["construction_approved"] = $construction;
		}
		$investment_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$investment_cost_per_sqm = round($construction/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
		$investment_cost_per_fte = 0;
		if($ftes > 0)
		{
			$investment_cost_per_fte = round($construction/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
		$percent_of_total_cost = "0.00%";
		if($total_investment_approved > 0)
		{
			$percent_of_total_cost = number_format(100 * $construction/$total_investment_approved, 2, ".", "") . "%";
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_number);
		$row_index++;



		//get fixturing cost
		$fixturing = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc_approved + cer_investment_amount_additional_cer_loc_approved) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type = 3";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$fixturing = $row_s["total"];
		}
		$fixturing = $fixturing * $exchange_rate / $factor;
		$sheet->write($row_index, $col_index01, number_format($fixturing, 0, ".", ""), $f_number);
		if(array_key_exists("fixturing_approved", $list_avgs))
		{
			$list_avgs["fixturing_approved"] = $list_avgs["fixturing_approved"] + $fixturing;
		}
		else
		{
			$list_avgs["fixturing_approved"] = $fixturing;
		}
		$investment_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$investment_cost_per_sqm = round($fixturing/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
		$investment_cost_per_fte = 0;
		if($ftes > 0)
		{
			$investment_cost_per_fte = round($fixturing/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
		$percent_of_total_cost = "0.00%";
		if($total_investment_approved > 0)
		{
			$percent_of_total_cost = number_format(100 *$fixturing/$total_investment_approved, 2, ".", "") . "%";
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_number);
		$row_index++;



		//get architectural cost
		$architectural = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc_approved + cer_investment_amount_additional_cer_loc_approved) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type = 5";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$architectural = $row_s["total"];
		}
		$architectural = $architectural * $exchange_rate / $factor;
		$sheet->write($row_index, $col_index01, number_format($architectural, 0, ".", ""), $f_number);
		if(array_key_exists("architectural_approved", $list_avgs))
		{
			$list_avgs["architectural_approved"] = $list_avgs["architectural_approved"] + $architectural;
		}
		else
		{
			$list_avgs["architectural_approved"] = $architectural;
		}
		$investment_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$investment_cost_per_sqm = round($architectural/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
		$investment_cost_per_fte = 0;
		if($ftes > 0)
		{
			$investment_cost_per_fte = round($architectural/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
		$percent_of_total_cost = "0.00%";
		if($total_investment_approved > 0)
		{
			$percent_of_total_cost = number_format(100 * $architectural/$total_investment_approved, 2, ".", "") . "%";
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_number);
		$row_index++;

		
		//get equipment cost
		$equipment = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc_approved + cer_investment_amount_additional_cer_loc_approved) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type = 7";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$equipment = $row_s["total"];
		}
		$equipment = $equipment * $exchange_rate / $factor;
		$sheet->write($row_index, $col_index01, number_format($equipment, 0, ".", ""), $f_number);
		if(array_key_exists("equipment_approved", $list_avgs))
		{
			$list_avgs["equipment_approved"] = $list_avgs["equipment_approved"] + $equipment;
		}
		else
		{
			$list_avgs["equipment_approved"] = $equipment;
		}
		$investment_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$investment_cost_per_sqm = round($equipment/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
		$investment_cost_per_fte = 0;
		if($ftes > 0)
		{
			$investment_cost_per_fte = round($equipment/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
		$percent_of_total_cost = "0.00%";
		if($total_investment_approved > 0)
		{
			$percent_of_total_cost = number_format(100 * $equipment/$total_investment_approved, 2, ".", "") . "%";
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_number);
		$row_index++;

		
		//get other cost
		$other = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc_approved + cer_investment_amount_additional_cer_loc_approved) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type in (11,18,19)";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$other = $row_s["total"];
		}
		$other = $other * $exchange_rate / $factor;
		$sheet->write($row_index, $col_index01, number_format($other, 0, ".", ""), $f_number);
		if(array_key_exists("other_approved", $list_avgs))
		{
			$list_avgs["other_approved"] = $list_avgs["other_approved"] + $other;
		}
		else
		{
			$list_avgs["other_approved"] = $other;
		}
		$investment_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$investment_cost_per_sqm = round($other/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
		$investment_cost_per_fte = 0;
		if($ftes > 0)
		{
			$investment_cost_per_fte = round($other/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
		$percent_of_total_cost = "0.00%";
		if($total_investment_approved > 0)
		{
			$percent_of_total_cost = number_format(100 * $other/$total_investment_approved, 2, ".", "") . "%";
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_number);
		$row_index++;



		//totel investment in fixed assets

		$total_cost_approved = 0;

		$sql_s = "select sum(cer_investment_amount_cer_loc_approved + cer_investment_amount_additional_cer_loc_approved) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and (cer_investment_type = 1 " . 
			     " or cer_investment_type = 3 " .
			     " or cer_investment_type = 5 " .
			     " or cer_investment_type = 7 " .
			     " or cer_investment_type = 18 " .
			     " or cer_investment_type = 19 " .
			     " or cer_investment_type = 11) ";

		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$total_cost_approved = $row_s["total"] * $exchange_rate / $factor;
		}

		$sheet->write($row_index, $col_index01, number_format($total_cost_approved, 0, ".", ""), $f_number_bold);
		
		$total_cost_approved_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$total_cost_approved_per_sqm = round($total_cost_approved/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $total_cost_approved_per_sqm, $f_number_bold);
		$total_cost_approved_cost_per_fte = 0;
		if($ftes > 0)
		{
			$total_cost_approved_per_fte = round($total_cost_approved/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $total_cost_approved_per_fte, $f_number_bold);
		$percent_of_total_cost_approved = "0.00%";
		if($total_investment_approved > 0)
		{
			$percent_of_total_cost_approved = number_format(100 * $total_cost_approved/$total_investment_approved, 2, ".", "") . "%";
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost_approved, $f_number_bold);


		if(array_key_exists("total_cost_approved2", $list_avgs))
		{
			$list_avgs["total_cost_approved2"] = $list_avgs["total_cost_approved2"] + $total_cost_approved;
		}
		else
		{
			$list_avgs["total_cost_approved2"] = $total_cost_approved;
		}
		$row_index++;


		//get intangibles

		$intangibles = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc_approved + cer_investment_amount_additional_cer_loc_approved) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and (cer_investment_type = 15 or cer_investment_type = 17)";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$intangibles = $row_s["total"];
		}
		$intangibles = $intangibles * $exchange_rate / $factor;
		$sheet->write($row_index, $col_index01, number_format($intangibles, 0, ".", ""), $f_number);
		if(array_key_exists("intangibles_approved", $list_avgs))
		{
			$list_avgs["intangibles_approved"] = $list_avgs["intangibles_approved"] + $intangibles;
		}
		else
		{
			$list_avgs["intangibles_approved"] = $intangibles;
		}
		$investment_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$investment_cost_per_sqm = round($intangibles/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
		$investment_cost_per_fte = 0;
		if($ftes > 0)
		{
			$investment_cost_per_fte = round($intangibles/$ftes,0);
		}
		$percent_of_total_cost_approved = "0.00%";
		if($total_investment_approved > 0)
		{
			$percent_of_total_cost_approved = number_format(100 * $intangibles/$total_investment_approved, 2, ".", "") . "%";
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost_approved, $f_number);

		
		
		$row_index++;

	
		//get deposit cost
		$deposit = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc_approved + cer_investment_amount_additional_cer_loc_approved) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type = 9";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$deposit = $row_s["total"];
		}
		$deposit = $deposit * $exchange_rate / $factor;
		$sheet->write($row_index, $col_index01, number_format($deposit, 0, ".", ""), $f_number);
		if(array_key_exists("deposit_approved", $list_avgs))
		{
			$list_avgs["deposit_approved"] = $list_avgs["deposit_approved"] + $deposit;
		}
		else
		{
			$list_avgs["deposit_approved"] = $deposit;
		}
		$investment_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$investment_cost_per_sqm = round($deposit/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
		$investment_cost_per_fte = 0;
		if($ftes > 0)
		{
			$investment_cost_per_fte = round($deposit/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
		$percent_of_total_cost_approved = "0.00%";
		if($total_investment_approved > 0)
		{
			$percent_of_total_cost_approved = number_format(100 * $deposit/$total_investment_approved, 2, ".", "") . "%";
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost_approved, $f_number);
		$row_index++;

		//get noncapitalized cost
		$noncapitalized = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc_approved + cer_investment_amount_additional_cer_loc_approved) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type = 13";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$noncapitalized = $row_s["total"];
		}
		$noncapitalized = $noncapitalized * $exchange_rate / $factor;
		$sheet->write($row_index, $col_index01, number_format($noncapitalized, 0, ".", ""), $f_number);
		if(array_key_exists("noncapitalized_approved", $list_avgs))
		{
			$list_avgs["noncapitalized_approved"] = $list_avgs["noncapitalized_approved"] + $noncapitalized;
		}
		else
		{
			$list_avgs["noncapitalized_approved"] = $noncapitalized;
		}
		$investment_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$investment_cost_per_sqm = round($noncapitalized/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
		$investment_cost_per_fte = 0;
		if($ftes > 0)
		{
			$investment_cost_per_fte = round($noncapitalized/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
		$percent_of_total_cost_approved = "0.00%";
		if($total_investment_approved > 0)
		{
			$percent_of_total_cost_approved = number_format(100 * $noncapitalized/$total_investment_approved, 2, ".", "") . "%";
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost_approved, $f_number);
		$row_index++;

		
		
		//Total Project costs (Requested amount)
		$sheet->write($row_index, $col_index01, number_format($total_investment_approved, 0, ".", ""), $f_number_bold);
		
		$total_cost_approved_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$total_cost_approved_per_sqm = round($total_investment_approved/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $total_cost_approved_per_sqm, $f_number_bold);
		$total_cost_approved_cost_per_fte = 0;
		if($ftes > 0)
		{
			$total_cost_approved_per_fte = round($total_investment_approved/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $total_cost_approved_per_fte, $f_number_bold);
		$percent_of_total_cost_approved = "0.00%";
		if($total_cost_approved > 0)
		{
			$percent_of_total_cost_approved = number_format(100 * $total_investment_approved/$total_investment_approved, 2, ".", "") . "%";
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost_approved, $f_number_bold);


		if(array_key_exists("total_cost_approved3", $list_avgs))
		{
			$list_avgs["total_cost_approved3"] = $list_avgs["total_cost_approved3"] + $total_investment_approved;
		}
		else
		{
			$list_avgs["total_cost_approved3"] = $total_investment_approved;
		}

		$row_index++;
		$row_index++;
	

	
		//CMS DATA

		$grouptotals_real = array();
		
		$grouptotals_real[2] = 0;
		$grouptotals_real[6] = 0;
		$grouptotals_real[7] = 0;
		$grouptotals_real[8] = 0;
		$grouptotals_real[9] = 0;
		$grouptotals_real[10] = 0;
		$grouptotals_real[11] = 0;
		

		$total_real_asset_cost = 0;
		$total_cost_cms = 0;

		
		if($base_currency == "chf")
		{
			$order_currency = get_order_currency($row["project_order"]);
			$realcost = get_project_budget_totals($row["project_id"], $order_currency);
			foreach($realcost["group_real_totals_by_posinvestment_type_chf"] as $posinvestment_type_id=>$value)
			{
				if(array_key_exists($posinvestment_type_id, $grouptotals_real))
				{
					$grouptotals_real[$posinvestment_type_id] =  $grouptotals_real[$posinvestment_type_id] + $value;
				}
				else
				{
					$grouptotals_real[$posinvestment_type_id] =  $value;
				}
			}
		}
		else
		{
			$realcost = get_project_budget_totals($row["project_id"]);
			foreach($realcost["group_real_totals_by_posinvestment_type"] as $posinvestment_type_id=>$value)
			{
				if(array_key_exists($posinvestment_type_id, $grouptotals_real))
				{
					$grouptotals_real[$posinvestment_type_id] =  $grouptotals_real[$posinvestment_type_id] + $value;
				}
				else
				{
					$grouptotals_real[$posinvestment_type_id] =  $value;
				}
			}
		}
		
		
	

		$sheet->write($row_index, $col_index01, "Amount", $f_yellow);
		$sheet->write($row_index, $col_index01+1,"per sqm", $f_yellow);
		$sheet->write($row_index, $col_index01+2,"per fte", $f_yellow);
		$sheet->write($row_index, $col_index01+3,"% of KL", $f_yellow);
		
		$row_index++;

		//construction
		$amount = 0;
		
		if(array_key_exists(1, $grouptotals_real))
		{
			$amount = $grouptotals_real[1];
		}

		$sheet->write($row_index, $col_index01, number_format($amount, 0, ".", ""), $f_number);
		if(array_key_exists("cms2", $list_avgs))
		{
			$list_avgs["cms2"] = $list_avgs["cms2"] + $amount;
		}
		else
		{
			$list_avgs["cms2"] = $amount;
		}
		$amount_per_sqm = 0;
		if($retail_area > 0)
		{
			$amount_per_sqm = round($amount/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number);
		$amount_per_fte = 0;
		if($ftes > 0)
		{
			$amount_per_fte = round($amount/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number);
		$percent_of_cer = "0.00%";
		if($construction > 0)
		{
			$percent_of_cer = number_format(round(100 * $amount/$construction,7), 2, ".", "") . "%";
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_number);
		$row_index++;
		$total_real_asset_cost = $total_real_asset_cost + $amount;
		$total_cost_cms = $total_cost_cms + $amount;

		//fixturing
		$amount = 0;
		
		if(array_key_exists(3, $grouptotals_real))
		{
			$amount = $grouptotals_real[3];
		}

		$sheet->write($row_index, $col_index01, number_format($amount, 0, ".", ""), $f_number);
		if(array_key_exists("cms3", $list_avgs))
		{
			$list_avgs["cms3"] = $list_avgs["cms3"] + $amount;
		}
		else
		{
			$list_avgs["cms3"] = $amount;
		}
		$amount_per_sqm = 0;
		if($retail_area > 0)
		{
			$amount_per_sqm = round($amount/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number);
		$amount_per_fte = 0;
		if($ftes > 0)
		{
			$amount_per_fte = round($amount/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number);
		$percent_of_cer = "0.00%";
		if($fixturing > 0)
		{
			$percent_of_cer = number_format(round(100 * $amount/$fixturing,7), 2, ".", "") . "%";
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_number);
		$row_index++;
		$total_real_asset_cost = $total_real_asset_cost + $amount;
		$total_cost_cms = $total_cost_cms + $amount;

		//architectural
		$amount = 0;
		if(array_key_exists(5, $grouptotals_real))
		{
			$amount = $grouptotals_real[5];
		}

		$sheet->write($row_index, $col_index01, number_format($amount, 0, ".", ""), $f_number);
		if(array_key_exists("cms4", $list_avgs))
		{
			$list_avgs["cms4"] = $list_avgs["cms4"] + $amount;
		}
		else
		{
			$list_avgs["cms4"] = $amount;
		}
		$amount_per_sqm = 0;
		if($retail_area > 0)
		{
			$amount_per_sqm = round($amount/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number);
		$amount_per_fte = 0;
		if($ftes > 0)
		{
			$amount_per_fte = round($amount/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number);
		$percent_of_cer = "0.00%";
		if($architectural > 0)
		{
			$percent_of_cer = number_format(round(100 * $amount/$architectural,7), 2, ".", "") . "%";
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_number);
		$row_index++;
		$total_real_asset_cost = $total_real_asset_cost + $amount;
		$total_cost_cms = $total_cost_cms + $amount;

		//equipment
		$amount = 0;
		$amount = $grouptotals_real[7];

		$sheet->write($row_index, $col_index01, number_format($amount, 0, ".", ""), $f_number);
		if(array_key_exists("cms5", $list_avgs))
		{
			$list_avgs["cms5"] = $list_avgs["cms5"] + $amount;
		}
		else
		{
			$list_avgs["cms5"] = $amount;
		}
		$amount_per_sqm = 0;
		if($retail_area > 0)
		{
			$amount_per_sqm = round($amount/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number);
		$amount_per_fte = 0;
		if($ftes > 0)
		{
			$amount_per_fte = round($amount/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number);
		$percent_of_cer = "0.00%";
		if($equipment > 0)
		{
			$percent_of_cer = number_format(round(100 * $amount/$equipment,7), 2, ".", "") . "%";
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_number);
		$row_index++;
		$total_real_asset_cost = $total_real_asset_cost + $amount;
		$total_cost_cms = $total_cost_cms + $amount;

		//other
		$amount = 0;
		$amount = $grouptotals_real[11];

		$sheet->write($row_index, $col_index01, number_format($amount, 0, ".", ""), $f_number);
		if(array_key_exists("cms6", $list_avgs))
		{
			$list_avgs["cms6"] = $list_avgs["cms6"] + $amount;
		}
		else
		{
			$list_avgs["cms6"] = $amount;
		}
		$amount_per_sqm = 0;
		if($retail_area > 0)
		{
			$amount_per_sqm = round($amount/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number);
		$amount_per_fte = 0;
		if($ftes > 0)
		{
			$amount_per_fte = round($amount/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number);
		$percent_of_cer = "0.00%";
		if($other > 0)
		{
			$percent_of_cer = number_format(round(100 * $amount/$other,7), 2, ".", "") . "%";
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_number);
		$row_index++;
		$total_real_asset_cost = $total_real_asset_cost + $amount;
		$total_cost_cms = $total_cost_cms + $amount;
		
		


		//total real cost fixed assets
		$sheet->write($row_index, $col_index01, number_format($total_real_asset_cost, 0, ".", ""), $f_number_bold);
		if(array_key_exists("cms_asste_total", $list_avgs))
		{
			$list_avgs["cms_asste_total"] = $list_avgs["cms_asste_total"] + $total_real_asset_cost;
		}
		else
		{
			$list_avgs["cms_asste_total"] = $total_real_asset_cost;
		}
		$amount_per_sqm = 0;
		if($retail_area > 0)
		{
			$amount_per_sqm = round($total_real_asset_cost/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number_bold);
		$amount_per_fte = 0;
		if($ftes > 0)
		{
			$amount_per_fte = round($total_real_asset_cost/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number_bold);
		$percent_of_cer = "0.00%";
		if($total_cost_approved > 0)
		{
			$percent_of_cer = number_format(round(100 * $total_real_asset_cost/$total_cost_approved,7), 2, ".", "") . "%";
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_number_bold);
		$row_index++;


	
		//get kemoney cost
		$amount = 0;
		
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and (cer_investment_type = 15 or cer_investment_type = 17)";


		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$amount = $row_s["total"];
		}

		$amount = $amount * $exchange_rate / $factor;
		$sheet->write($row_index, $col_index01, number_format($amount, 0, ".", ""), $f_number);
		if(array_key_exists("cms1", $list_avgs))
		{
			$list_avgs["cms1"] = $list_avgs["cms1"] + $amount;
		}
		else
		{
			$list_avgs["cms1"] = $amount;
		}
		$amount_per_sqm = 0;
		if($retail_area > 0)
		{
			$amount_per_sqm = round($amount/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number);
		$amount_per_fte = 0;
		if($ftes > 0)
		{
			$amount_per_fte = round($amount/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number);
		$percent_of_cer = "0.00%";
		if($intangibles > 0)
		{
			$percent_of_cer = number_format(round(100 * $amount/$intangibles,7), 2, ".", "") . "%";
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_number);
		$total_cost_cms = $total_cost_cms + $amount;

		$row_index++;
	
		
		//deposit
		$amount = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type = 9";


		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$amount = $row_s["total"];
		}
		$amount = $amount * $exchange_rate / $factor;
		$sheet->write($row_index, $col_index01, number_format($amount, 0, ".", ""), $f_number);
		if(array_key_exists("cms7", $list_avgs))
		{
			$list_avgs["cms7"] = $list_avgs["cms7"] + $amount;
		}
		else
		{
			$list_avgs["cms7"] = $amount;
		}
		$amount_per_sqm = 0;
		if($retail_area > 0)
		{
			$amount_per_sqm = round($amount/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number);
		$amount_per_fte = 0;
		if($ftes > 0)
		{
			$amount_per_fte = round($amount/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number);
		$percent_of_cer = "0.00%";
		if($deposit > 0)
		{
			$percent_of_cer = number_format(round(100 * $amount/$deposit,7), 2, ".", "") . "%";
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_number);
		$total_cost_cms = $total_cost_cms + $amount;

		$row_index++;
		

		//noncapitalized
		$amount = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type = 13";
		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$amount = $row_s["total"];
		}
		$amount = $amount * $exchange_rate / $factor;
		$sheet->write($row_index, $col_index01, number_format($amount, 0, ".", ""), $f_number);
		if(array_key_exists("cms8", $list_avgs))
		{
			$list_avgs["cms8"] = $list_avgs["cms8"] + $amount;
		}
		else
		{
			$list_avgs["cms8"] = $amount;
		}
		$amount_per_sqm = 0;
		if($retail_area > 0)
		{
			$amount_per_sqm = round($amount/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number);
		$amount_per_fte = 0;
		if($ftes > 0)
		{
			$amount_per_fte = round($amount/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number);
		$percent_of_cer = "0.00%";
		if($noncapitalized > 0)
		{
			$percent_of_cer = number_format(round(100 * $amount/$noncapitalized,7), 2, ".", "") . "%";
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_number);
		$total_cost_cms = $total_cost_cms + $amount;
		$row_index++;
		

		//total cost
		$sheet->write($row_index, $col_index01, number_format($total_cost_cms, 0, ".", ""), $f_number_bold);
		if(array_key_exists("cms_total", $list_avgs))
		{
			$list_avgs["cms_total"] = $list_avgs["cms_total"] + $total_cost_cms;
		}
		else
		{
			$list_avgs["cms_total"] = $total_cost_cms;
		}
		$amount_per_sqm = 0;
		if($retail_area > 0)
		{
			$amount_per_sqm = round($total_cost_cms/$retail_area,0);
		}
		$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number_bold);
		$amount_per_fte = 0;
		if($ftes > 0)
		{
			$amount_per_fte = round($total_cost_cms/$ftes,0);
		}
		//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number_bold);
		$percent_of_cer = "0.00%";
		if($total_investment_approved > 0)
		{
			$percent_of_cer = number_format(round(100 * $total_cost_cms/$total_investment_approved,7), 2, ".", "") . "%";
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_number_bold);
		$row_index++;
		$row_index++;

		
		if(count($cer_basicdata) > 0)
		{
			$content = $cer_basicdata["cer_basicdata_comment"];
		}
		else
		{
			$content = "";
		}
		
		$pos = 1;
		$len = 60;
		$num_of_lines = ceil(strlen($content) /$len);
		$start = 0;
		if($num_of_lines > 1)
		{
			for($i=0;$i<=$num_of_lines;$i++)
			{
				$pos = $pos-1;
				$line =  substr($content, $start, $len);
				
				if($i < ($num_of_lines-1))
				{
					$pos = strrpos ($line  , " ");
					$line =  substr($content, $start, $pos);
					
				}
				else
				{
					$line =  substr($content, $start, strlen($line));
				}


				$start = $start+$pos+1;
				
				
				if($line)
				{
					$sheet->write($row_index,$col_index01,$line, $f_comment);
					$row_index++;
				}
			}
		}
		elseif($content)
		{
			$sheet->write($row_index, $col_index01, $content, $f_comment);
			$row_index++;
		}

		$col_index01 = $col_index01 + 5;
		$col_index02 = $col_index02 + 5;
	}
}

//print average column
if($num_of_projects > 0)
{
	$row_index = 3;

	$sheet->setColumn($col_index01, $col_index01, 9);
	$sheet->setColumn($col_index01, $col_index01+1, 9);
	$sheet->setColumn($col_index01, $col_index01+2, 9);
	$sheet->setColumn($col_index01, $col_index01+3, 12);
	$sheet->setColumn($col_index01, $col_index01+4, 2);
	
	$sheet->write($row_index, $col_index01, "Average", $f_border_left);
	$sheet->write($row_index, $col_index01+1,"", $f_border_middle);
	$sheet->write($row_index, $col_index01+2,"", $f_border_middle);
	$sheet->write($row_index, $col_index01+3,"", $f_border_right);
	$row_index++;
	$sheet->write($row_index, $col_index01, "Project count: " . $num_of_projects, $f_border_left);
	$sheet->write($row_index, $col_index01+1,"", $f_border_middle);
	$sheet->write($row_index, $col_index01+2,"", $f_border_middle);
	$sheet->write($row_index, $col_index01+3,"", $f_border_right);
	$row_index++;
	$sheet->write($row_index, $col_index01, "", $f_border_left);
	$sheet->write($row_index, $col_index01+1,"", $f_border_middle);
	$sheet->write($row_index, $col_index01+2,"", $f_border_middle);
	$sheet->write($row_index, $col_index01+3,"", $f_border_right);
	$row_index++;
	$sheet->write($row_index, $col_index01, "", $f_border_left);
	$sheet->write($row_index, $col_index01+1,"", $f_border_middle);
	$sheet->write($row_index, $col_index01+2,"", $f_border_middle);
	$sheet->write($row_index, $col_index01+3,"", $f_border_right);
	$row_index++;
	$sheet->write($row_index, $col_index01, "", $f_border_left);
	$sheet->write($row_index, $col_index01+1,"", $f_border_middle);
	$sheet->write($row_index, $col_index01+2,"", $f_border_middle);
	$sheet->write($row_index, $col_index01+3,"", $f_border_right);
	$row_index++;
	$row_index++;

	$sheet->write($row_index, $col_index01, "Amount", $f_yellow);
	$sheet->write($row_index, $col_index01+1,"per sqm", $f_yellow);
	$sheet->write($row_index, $col_index01+2,"per fte", $f_yellow);
	$sheet->write($row_index, $col_index01+3,"% of net sales", $f_yellow);
	
	$row_index = 12;

	if($base_currency == "chf")
	{
		$sheet->write($row_index, $col_index01, "CHF", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01, "", $f_number);
	}
	
	$row_index++;

	$avg = round($list_avgs["project_cost_sqms"]/$num_of_projects, 2);
	$sheet->write($row_index, $col_index01, number_format($avg, 2, ".", ""), $f_number);
	$row_index++;

	$avg = round($list_avgs["ftes"]/$num_of_projects, 2);
	$sheet->write($row_index, $col_index01, number_format($avg, 2, ".", ""), $f_number);

	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["ftes"]/$list_avgs["project_cost_sqms"], 2);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 2, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}

	$row_index++;

	$avg = round($list_avgs["lease_period"]/$num_of_projects, 2);
	$sheet->write($row_index, $col_index01, number_format($avg, 2, ".", ""), $f_number);
	$row_index++;

	$avg = round($list_avgs["watches"]/$num_of_projects, 2);
	$sheet->write($row_index, $col_index01, number_format($avg, 2, ".", ""), $f_number);

	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["watches"]/$list_avgs["project_cost_sqms"], 2);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 2, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["watches"]/$list_avgs["ftes"], 2);
		$sheet->write($row_index, $col_index01+2, number_format($avg, 2, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}

	$row_index++;

	$avg = round($list_avgs["jewellery"]/$num_of_projects, 2);
	$sheet->write($row_index, $col_index01, number_format($avg, 2, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["jewellery"]/$list_avgs["project_cost_sqms"], 2);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 2, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["jewellery"]/$list_avgs["ftes"], 2);
		$sheet->write($row_index, $col_index01+2, number_format($avg, 2, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	$row_index++;


	$avg = round($list_avgs["watches_price"]/$num_of_projects, 2);
	$sheet->write($row_index, $col_index01, number_format($avg, 2, ".", ""), $f_number);
	$row_index++;

	$avg = round($list_avgs["jewellery_price"]/$num_of_projects, 2);
	$sheet->write($row_index, $col_index01, number_format($avg, 2, ".", ""), $f_number);
	$row_index++;


	//revenues
	$percent_of_net_sales = 0;
	if($num_of_projects > 0) 
	{
		$total_avg_revenue = $list_avgs["gross_sales"]/$num_of_projects;
		$percent_base = 100 + ($list_avgs["sales_reduction_percent"]/$num_of_projects);
		$base_unit = $total_avg_revenue / $percent_base;
	}
	else
	{
		$total_avg_revenue = 0;
		$percent_base = 100;
		$base_unit = $total_avg_revenue / $percent_base;
	}


	$avg = round($list_avgs["watches_revenue"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["watches_revenue"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["watches_revenue"]/$list_avgs["ftes"], 0);
		$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}

	$percent_of_watches = "0.00%";
	$percent_of_net_sales = 0;
	if($list_avgs["net_sales"] > 0 and $num_of_projects > 0) 
	{
		$avg = $list_avgs["watches_revenue"]/$num_of_projects;
		$percent_of_watches = number_format(100*$avg/$list_avgs["net_sales"], 2, ".", "") . "%";
		$percent_of_net_sales = $avg/$list_avgs["net_sales"];
	}
	$sheet->write($row_index, $col_index01+3, $percent_of_watches, $f_number);

	$row_index++;

	
	$avg = round($list_avgs["jewellery_revenue"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["jewellery_revenue"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["jewellery_revenue"]/$list_avgs["ftes"], 0);
		$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}

	$percent_of_jewellery = "0.00%";
	if($list_avgs["net_sales"] > 0 and $num_of_projects > 0) 
	{
		$avg = $list_avgs["jewellery_revenue"]/$num_of_projects;
		$percent_of_jewellery = number_format(100*$avg/$list_avgs["net_sales"], 2, ".", "") . "%";
		$percent_of_net_sales = $percent_of_net_sales + $avg/$list_avgs["net_sales"];
	}
	$sheet->write($row_index, $col_index01+3, $percent_of_jewellery, $f_number);

	$row_index++;

	
	$avg = round($list_avgs["accessories_revenue"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["accessories_revenue"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["accessories_revenue"]/$list_avgs["ftes"], 0);
		$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}

	$percent_of_service = "0.00%";
	if($list_avgs["net_sales"] > 0 and $num_of_projects > 0) 
	{
		$avg = $list_avgs["accessories_revenue"]/$num_of_projects;
		$percent_of_service = number_format(100*$avg/$list_avgs["net_sales"], 2, ".", "") . "%";
		$percent_of_net_sales = $percent_of_net_sales + $avg/$list_avgs["net_sales"];
	}
	$sheet->write($row_index, $col_index01+3, $percent_of_service, $f_number);

	$row_index++;

		
	$avg = round($list_avgs["services_revenue"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["services_revenue"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["services_revenue"]/$list_avgs["ftes"], 0);
		$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}

	$percent_of_service = "0.00%";
	if($list_avgs["net_sales"] > 0 and $num_of_projects > 0) 
	{
		$avg = $list_avgs["services_revenue"]/$num_of_projects;
		$percent_of_service = number_format(100*$avg/$list_avgs["net_sales"], 2, ".", "") . "%";
		$percent_of_net_sales = $percent_of_net_sales + $avg/$list_avgs["net_sales"];
	}
	$sheet->write($row_index, $col_index01+3, $percent_of_service, $f_number);

	$row_index++;

	$avg = round($list_avgs["gross_sales"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number_bold);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["gross_sales"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["gross_sales"]/$list_avgs["ftes"], 0);
		$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}

	$sheet->write($row_index, $col_index01+3, number_format(100*$percent_of_net_sales, 2, ".", "") . "%", $f_number_bold);

	$row_index++;

	//sales reduction

	
	
	$avg = round($list_avgs["sales_reduction"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["sales_reduction"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["sales_reduction"]/$list_avgs["ftes"], 0);
		$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}

	if($list_avgs["net_sales"] > 0)
	{
		$sales_reduction_precent = 100*$list_avgs["sales_reduction"] / $list_avgs["net_sales"];
	}
	else
	{
		$sales_reduction_precent = 0;
	}
	$sales_reduction_precent = -1*number_format($sales_reduction_precent, 2, ".", "") . "%";
	$sheet->write($row_index, $col_index01+3, $sales_reduction_precent, $f_number_bold);
	
	$row_index++;

	
	
	//net Sales
	$avg = round($list_avgs["net_sales"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number_bold);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["net_sales"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["net_sales"]/$list_avgs["ftes"], 0);
		$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}

	$sheet->write($row_index, $col_index01+3, "100%", $f_number_bold);
	$row_index++;


	//material of products sold
	$avg = round($list_avgs["e15"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["e15"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["e15"]/$list_avgs["ftes"], 2);
		$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["net_sales"] > 0)
	{
		$avg = round(100*$list_avgs["e15"]/$list_avgs["net_sales"], 2);
		$sheet->write($row_index, $col_index01+3, -1*number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;


	//gross margin
	$avg = round($list_avgs["gross_margin"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number_bold);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["gross_margin"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number_bold);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["gross_margin"]/$list_avgs["ftes"], 0);
		$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+2, "0.00", $f_number_bold);
	}
	if($list_avgs["net_sales"] > 0)
	{
		$avg = round(100*$list_avgs["gross_margin"]/$list_avgs["net_sales"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number_bold);
	}

	$row_index++;


	//marketing expenses
	$avg = round($list_avgs["e13"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number_bold);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["e13"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["e13"]/$list_avgs["ftes"], 2);
		$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["net_sales"] > 0)
	{
		$avg = round(100*$list_avgs["e13"]/$list_avgs["net_sales"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number_bold);
	}
	$row_index++;



	$avg = round($list_avgs["e1"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["e1"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["e1"]/$list_avgs["ftes"], 2);
		$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["net_sales"] > 0)
	{
		$avg = round(100*$list_avgs["e1"]/$list_avgs["net_sales"], 2);
		$sheet->write($row_index, $col_index01+3, -1*number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;

	$avg = round($list_avgs["rents"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["rents"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["rents"]/$list_avgs["ftes"], 0);
		$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["net_sales"] > 0)
	{
		$avg = round(100*$list_avgs["rents"]/$list_avgs["net_sales"], 2);
		$sheet->write($row_index, $col_index01+3, -1*number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;

	$avg = round($list_avgs["e4"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["e4"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["e4"]/$list_avgs["ftes"], 0);
		$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["net_sales"] > 0)
	{
		$avg = round(100*$list_avgs["e4"]/$list_avgs["net_sales"], 2);
		$sheet->write($row_index, $col_index01+3, -1*number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;

	$avg = round($list_avgs["e5"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["e5"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["e5"]/$list_avgs["ftes"], 0);
		$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["net_sales"] > 0)
	{
		$avg = round(100*$list_avgs["e5"]/$list_avgs["net_sales"], 2);
		$sheet->write($row_index, $col_index01+3, -1*number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;


	

	//depreciation

	$avg = round($list_avgs["depreciation_benchmark_year"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);

	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["depreciation_benchmark_year"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["depreciation_benchmark_year"]/$list_avgs["ftes"], 0);
		$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["net_sales"] > 0)
	{
		$avg = round(100*$list_avgs["depreciation_benchmark_year"]/$list_avgs["net_sales"], 2);
		$sheet->write($row_index, $col_index01+3, -1*number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	
	$row_index++;




	$avg = round($list_avgs["depreciation_benchmark_year_km"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["depreciation_benchmark_year_km"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["depreciation_benchmark_year_km"]/$list_avgs["ftes"], 0);
		$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["net_sales"] > 0)
	{
		$avg = round(100*$list_avgs["depreciation_benchmark_year_km"]/$list_avgs["net_sales"], 2);
		$sheet->write($row_index, $col_index01+3, -1*number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;

	$avg = round($list_avgs["e10"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["e10"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["e10"]/$list_avgs["ftes"], 0);
		$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["net_sales"] > 0)
	{
		$avg = round(100*$list_avgs["e10"]/$list_avgs["net_sales"], 2);
		$sheet->write($row_index, $col_index01+3, -1*number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;

	
	//total cost
	$avg = round($list_avgs["total_cost"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number_bold);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["total_cost"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["total_cost"]/$list_avgs["ftes"], 0);
		$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["net_sales"] > 0)
	{
		$avg = round(100*$list_avgs["total_cost"]/$list_avgs["net_sales"], 2);
		$sheet->write($row_index, $col_index01+3, -1*number_format($avg, 2, ".", "") . "%", $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;


	//operational income
	$avg = round($list_avgs["operational_income"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number_bold);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["operational_income"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["operational_income"]/$list_avgs["ftes"], 0);
		$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["net_sales"] > 0)
	{
		$avg = round(100*$list_avgs["operational_income"]/$list_avgs["net_sales"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;
	$row_index++;

	
	
	//investments
	$sheet->write($row_index, $col_index01, "Amount", $f_yellow);
	$sheet->write($row_index, $col_index01+1,"per sqm", $f_yellow);
	$sheet->write($row_index, $col_index01+2,"per fte", $f_yellow);
	$sheet->write($row_index, $col_index01+3,"% of invest.", $f_yellow);
	
	$row_index++;

	
	$avg = round($list_avgs["construction"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["construction"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["construction"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["total_cost2"] > 0)
	{
		$avg = round(100*$list_avgs["construction"]/$list_avgs["total_cost2"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;

	$avg = round($list_avgs["fixturing"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["fixturing"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["fixturing"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["total_cost2"] > 0)
	{
		$avg = round(100*$list_avgs["fixturing"]/$list_avgs["total_cost2"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;

	$avg = round($list_avgs["architectural"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["architectural"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["architectural"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["total_cost2"] > 0)
	{
		$avg = round(100*$list_avgs["architectural"]/$list_avgs["total_cost2"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;

	$avg = round($list_avgs["equipment"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["equipment"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["equipment"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["total_cost2"] > 0)
	{
		$avg = round(100*$list_avgs["equipment"]/$list_avgs["total_cost2"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;
	
	$avg = round($list_avgs["other"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["other"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["other"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["total_cost2"] > 0)
	{
		$avg = round(100*$list_avgs["other"]/$list_avgs["total_cost2"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;


	
	$avg = round($list_avgs["total_cost2"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number_bold);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["total_cost2"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number_bold);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["total_cost2"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number_bold);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number_bold);
	}
	
	
	if($list_avgs["total_cost2"] > 0)
	{
		$avg = round(100*$list_avgs["total_cost2"]/$list_avgs["total_cost2"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number_bold);
	}

	$row_index++;


	
	$avg = round($list_avgs["intangibles"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["intangibles"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg,0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["intangibles"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	/*
	if($list_avgs["total_cost2"] > 0)
	{
		$avg = round(100*$list_avgs["intangibles"]/$list_avgs["total_cost2"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	*/
	$sheet->write($row_index, $col_index01+3, "", $f_number);
	$row_index++;
	
	

	
	$avg = round($list_avgs["deposit"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["deposit"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["deposit"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	/*
	if($list_avgs["total_cost2"] > 0)
	{
		$avg = round(100*$list_avgs["deposit"]/$list_avgs["total_cost2"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	*/
	$sheet->write($row_index, $col_index01+3, "", $f_number);
	$row_index++;
	
	$avg = round($list_avgs["noncapitalized"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["noncapitalized"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["noncapitalized"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	
	/*
	if($list_avgs["total_cost2"] > 0)
	{
		$avg = round(100*$list_avgs["noncapitalized"]/$list_avgs["total_cost2"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	*/
	$sheet->write($row_index, $col_index01+3, "", $f_number);

	$row_index++;


	$avg = round($list_avgs["total_cost3"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number_bold);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["total_cost3"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number_bold);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["total_cost3"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number_bold);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number_bold);
	}
	
	/*
	if($list_avgs["total_cost3"] > 0)
	{
		$avg = round(100*$list_avgs["total_cost3"]/$list_avgs["total_cost3"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number_bold);
	}
	*/
	$sheet->write($row_index, $col_index01+3, "", $f_number_bold);
	
	$row_index++;
	$row_index++;



	//KL approved investment
	$sheet->write($row_index, $col_index01, "Amount", $f_yellow);
	$sheet->write($row_index, $col_index01+1,"per sqm", $f_yellow);
	$sheet->write($row_index, $col_index01+2,"per fte", $f_yellow);
	$sheet->write($row_index, $col_index01+3,"% of invest.", $f_yellow);

	$row_index++;

	$avg = round($list_avgs["construction_approved"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["construction_approved"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["construction_approved"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["total_cost_approved2"] > 0)
	{
		$avg = round(100*$list_avgs["construction_approved"]/$list_avgs["total_cost_approved2"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;

	$avg = round($list_avgs["fixturing_approved"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["fixturing_approved"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["fixturing_approved"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["total_cost_approved2"] > 0)
	{
		$avg = round(100*$list_avgs["fixturing_approved"]/$list_avgs["total_cost_approved2"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;

	$avg = round($list_avgs["architectural_approved"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["architectural_approved"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["architectural_approved"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["total_cost_approved2"] > 0)
	{
		$avg = round(100*$list_avgs["architectural_approved"]/$list_avgs["total_cost_approved2"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;

	$avg = round($list_avgs["equipment_approved"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["equipment_approved"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["equipment_approved"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["total_cost_approved2"] > 0)
	{
		$avg = round(100*$list_avgs["equipment_approved"]/$list_avgs["total_cost_approved2"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;
	
	$avg = round($list_avgs["other_approved"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["other_approved"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["other_approved"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["total_cost_approved2"] > 0)
	{
		$avg = round(100*$list_avgs["other_approved"]/$list_avgs["total_cost_approved2"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;


	
	$avg = round($list_avgs["total_cost_approved2"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number_bold);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["total_cost_approved2"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number_bold);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["total_cost_approved2"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number_bold);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number_bold);
	}
	if($list_avgs["total_cost_approved2"] > 0)
	{
		$avg = round(100*$list_avgs["total_cost_approved2"]/$list_avgs["total_cost_approved2"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number_bold);
	}

	$row_index++;


	
	
	
	$avg = round($list_avgs["intangibles_approved"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["intangibles_approved"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg,0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["intangibles_approved"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["total_cost_approved2"] > 0)
	{
		$avg = round(100*$list_avgs["intangibles_approved"]/$list_avgs["total_cost_approved2"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;

	
	$avg = round($list_avgs["deposit_approved"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["deposit_approved"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["deposit_approved"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["total_cost_approved2"] > 0)
	{
		$avg = round(100*$list_avgs["deposit_approved"]/$list_avgs["total_cost_approved2"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;
	
	$avg = round($list_avgs["noncapitalized_approved"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["noncapitalized_approved"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["noncapitalized_approved"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["total_cost_approved2"] > 0)
	{
		$avg = round(100*$list_avgs["noncapitalized_approved"]/$list_avgs["total_cost_approved2"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;


	$avg = round($list_avgs["total_cost_approved3"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number_bold);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["total_cost_approved3"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number_bold);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["total_cost_approved3"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number_bold);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number_bold);
	}
	if($list_avgs["total_cost_approved3"] > 0)
	{
		$avg = round(100*$list_avgs["total_cost_approved3"]/$list_avgs["total_cost_approved3"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number_bold);
	}
	
	
	$row_index++;
	$row_index++;


	
	//CMS
	$sheet->write($row_index, $col_index01, "Amount", $f_yellow);
	$sheet->write($row_index, $col_index01+1,"per sqm", $f_yellow);
	$sheet->write($row_index, $col_index01+2,"per fte", $f_yellow);
	$sheet->write($row_index, $col_index01+3,"% of AF/CER", $f_yellow);
	
	$row_index++;
	

	$avg = round($list_avgs["cms2"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["cms2"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["cms2"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["construction"] > 0)
	{
		$avg = round(100*$list_avgs["cms2"]/$list_avgs["construction"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;

	$avg = round($list_avgs["cms3"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["cms3"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["cms3"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["fixturing"] > 0)
	{
		$avg = round(100*$list_avgs["cms3"]/$list_avgs["fixturing"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;

	$avg = round($list_avgs["cms4"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["cms4"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["cms4"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["architectural"] > 0)
	{
		$avg = round(100*$list_avgs["cms4"]/$list_avgs["architectural"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;

	$avg = round($list_avgs["cms5"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["cms5"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["cms5"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["equipment"] > 0)
	{
		$avg = round(100*$list_avgs["cms5"]/$list_avgs["equipment"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;

	$avg = round($list_avgs["cms6"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["cms6"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["cms6"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["other"] > 0)
	{
		$avg = round(100*$list_avgs["cms6"]/$list_avgs["other"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;

	

	//total real cost of fixed assets
	$avg = round($list_avgs["cms_asste_total"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number_bold);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["cms_asste_total"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number_bold);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["cms_asste_total"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number_bold);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number_bold);
	}
	if($list_avgs["total_cost2"] > 0)
	{
		$avg = round(100*$list_avgs["cms_asste_total"]/$list_avgs["total_cost2"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;

	

	
	$avg = round($list_avgs["cms1"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["cms1"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["cms1"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["intangibles"] > 0)
	{
		$avg = round(100*$list_avgs["cms1"]/$list_avgs["intangibles"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	
	$row_index++;

	
	$avg = round($list_avgs["cms7"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["cms7"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["cms7"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["deposit"] > 0)
	{
		$avg = round(100*$list_avgs["cms7"]/$list_avgs["deposit"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;

	$avg = round($list_avgs["cms8"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["cms8"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["cms8"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["noncapitalized"] > 0)
	{
		$avg = round(100*$list_avgs["cms8"]/$list_avgs["noncapitalized"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;

	$avg = round($list_avgs["cms_total"]/$num_of_projects, 0);
	$sheet->write($row_index, $col_index01, number_format($avg, 0, ".", ""), $f_number_bold);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["cms_total"]/$list_avgs["project_cost_sqms"], 0);
		$sheet->write($row_index, $col_index01+1, number_format($avg, 0, ".", ""), $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number_bold);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["cms_total"]/$list_avgs["ftes"], 0);
		//$sheet->write($row_index, $col_index01+2, number_format($avg, 0, ".", ""), $f_number_bold);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number_bold);
	}
	if($list_avgs["total_cost2"] > 0)
	{
		$avg = round(100*$list_avgs["cms_total"]/$list_avgs["total_cost2"], 2);
		$sheet->write($row_index, $col_index01+3, number_format($avg, 2, ".", "") . "%", $f_number_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00%", $f_number);
	}
	$row_index++;

}

$xls->close(); 

?>