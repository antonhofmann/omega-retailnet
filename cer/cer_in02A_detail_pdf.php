<?php
/********************************************************************

    cer_in02A_detail_pdf.php

    Print Detail Form IN-R02A.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-06-17
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-06-17
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.
*********************************************************************/
include("include/in_financial_data.php");

//set pdf parameters
$margin_top = 12;
$margin_left = 12;
$y = $margin_top;
$x = $margin_left+1;
$standard_h = 6;
$x1 = $margin_left+1;
$x2 = $margin_left+61;

$years_page1 = array();
$j= 1;
foreach($years as $key=>$year)
{
	if($j < 6)
	{
		$years_page1[] = $years[$key];
	}
	$j++;
}

$sellouts_last_full_year = "";
$sellouts_last_full_year_gross_sales = "";
if(array_key_exists($sellout_ending_year, $sellouts_watches))
{
	$sellouts_last_full_year = $sellouts_watches[$sellout_ending_year];
}

if(array_key_exists($sellout_ending_year, $sellouts_watches))
{
	$sellouts_last_full_year_gross_sales = $sellouts_watches_gross_sales[$sellout_ending_year]/1000;
}

$turnoverrent_firstyear_share = "";
$turnoverrent_firstyear_share_ln = "";

if($cer_basicdata["cer_basicdata_tob_from_net_sales"] == 1)
{

	if($turnoverrent_firstyear_full_amount > 0 and array_key_exists($first_full_year,$total_net_sales_values) and $total_gross_sales_values["$first_full_year"] > 0) {
		
		$turnoverrent_firstyear_share = round(100*$turnoverrent_firstyear_full_amount /$total_net_sales_values[$first_full_year], 1) . "%";
	}
}
else
{
	if($turnoverrent_firstyear_full_amount > 0 and array_key_exists($first_full_year,$total_gross_sales_values) and $total_gross_sales_values["$first_full_year"] > 0) {
		
		$turnoverrent_firstyear_share = round(100*$turnoverrent_firstyear_full_amount /$total_gross_sales_values[$first_full_year], 1) . "%";
	}
}



if($cer_basicdata_ln["cer_basicdata_tob_from_net_sales"] == 1)
{

	if($turnoverrent_firstyear_full_amount_ln > 0 and array_key_exists($first_full_year_ln,$total_net_sales_values_ln) and $total_gross_sales_values_ln["$first_full_year"] > 0) {
		
		$turnoverrent_firstyear_share_ln = round(100*$turnoverrent_firstyear_full_amount_ln /$total_net_sales_values_ln[$first_full_year_ln], 1) . "%";
	}
}
else
{
	if($turnoverrent_firstyear_full_amount_ln > 0 and array_key_exists($first_full_year_ln,$total_gross_sales_values_ln) and $total_gross_sales_values["$first_full_year"] > 0) {
		
		$turnoverrent_firstyear_share_ln = round(100*$turnoverrent_firstyear_full_amount_ln /$total_gross_sales_values_ln[$first_full_year_ln], 1) . "%";
	}
}


//turnover percent for rents, only first cer record
//overrides lines 47 to 82 from above
$sql_e = "select * " .
	   "from cer_rent_percent_from_sales " .
	   "where cer_rent_percent_from_sale_cer_version = " . $cer_version . "  and cer_rent_percent_from_sale_project = " . param("pid") .
	   " order by cer_rent_percent_from_sale_year";

$res_e = mysql_query($sql_e) or dberror($sql_e);
if($row_e = mysql_fetch_assoc($res_e))
{
	$turnoverrent_firstyear_share = round($row_e["cer_rent_percent_from_sale_percent"], 2) . "%";
}

//turnover percent for rents, only first ln record
//overrides lines 47 to 82 from above
$sql_e = "select * " .
	   "from cer_rent_percent_from_sales " .
	   "where cer_rent_percent_from_sale_cer_version = " . $cer_version . "  and cer_rent_percent_from_sale_project = " . param("pid") .
	   " order by cer_rent_percent_from_sale_year";

$res_e = mysql_query($sql_e) or dberror($sql_e);
if($row_e = mysql_fetch_assoc($res_e))
{
	$turnoverrent_firstyear_share_ln = round($row_e["cer_rent_percent_from_sale_percent"], 2) . "%";
}

//get first frozen ln
$sql_e = "select cer_basicdata_version " .
		   "from cer_basicdata " .
		   "where cer_basicdata_version_context = 'ln' and cer_basicdata_project = " . param("pid") .
		   " order by cer_basicdata_id DESC";

$res_e = mysql_query($sql_e) or dberror($sql_e);
if($row_e = mysql_fetch_assoc($res_e))
{
	$_version = $row_e["cer_basicdata_version"];

	$sql_e = "select * " .
		   "from cer_rent_percent_from_sales " .
		   "where cer_rent_percent_from_sale_cer_version = " . $_version . "  and cer_rent_percent_from_sale_project = " . param("pid") .
		   " order by cer_rent_percent_from_sale_year";

	$res_e = mysql_query($sql_e) or dberror($sql_e);
	if($row_e = mysql_fetch_assoc($res_e))
	{
		$turnoverrent_firstyear_share_ln = round($row_e["cer_rent_percent_from_sale_percent"], 2) . "%";
	}
}


$pdf->AddPage("L", "A4");

// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "B", 10);
$pdf->Cell(51, 8, "Swatch Group", 1);

$pdf->SetFont("arialn", "B", 14);
$pdf->Cell(159, 8, "RETAIL BUSINESS PLAN OVERVIEW", 1, "", "C");


$pdf->SetFont("arialn", "", 10);

$pdf->SetFillColor(224,224,224);
$pdf->Cell(20, 8, "DETAILS", 1, "", "C", true);
$pdf->SetFillColor(248,251,167);
if($cer_basicdata["cer_basicdata_version"] == 0 and $cer_basicdata["cer_basicdata_cer_locked"] == 0)
{
	$pdf->Cell(20, 8, date("d.m.Y"), 1, "", "C", true);
}
else
{
	$pdf->Cell(20, 8, to_system_date($cer_basicdata["versiondate"]), 1, "", "C", true);
}
$pdf->Cell(20, 8, "INR-02A", 1, "", "C");

	
	$y = $pdf->GetY()+10;
	$y_at_start = $y - 1;

	

	// 1. General Information
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(268, 6, "1. General Information", 1, "", "L");

	$y = $pdf->GetY()+6;
	$pdf->SetXY($margin_left+1,$y);
	

	//draw investment boxes
	$pdf->Cell(120, 15, "", 1);
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(40, 5, "Brand:", 0, "", "L");
	$pdf->Cell(80, 5, BRAND, 0, "", "L");
	$pdf->Cell(40, 5, "Legal entity:", 0, "", "L");
	$pdf->Cell(80, 5, $project["address_company"], 0, "", "L");
	
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(40, 5, "Country:", 0, "", "L");
	$pdf->Cell(80, 5, $pos_data["country_name"], 0, "", "L");
	$pdf->Cell(40, 5, "Address:", 0, "", "L");
	$pdf->Cell(80, 5, $client_address["place"] . ", " . $client_address["address"], 0, "", "L");
	
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(40, 5, "Project Name:", 0, "", "L");
	$pdf->Cell(80, 5, $pos_data["posaddress_name"] . " (" . $project['order_number'] . ")", 0, "", "L");
	$pdf->Cell(40, 5, "Project manager:", 0, "", "L");
	$pdf->Cell(80, 5, $project_manager, 0, "", "L");
	
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	
	
	
	// 2. Investment & lease agreement
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(268, 6, "2. Investment & lease agreement", 1, "", "L");

	$y = $pdf->GetY()+6;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(60, 5, "Project costs:", 1, "", "L");
	$pdf->Cell(20, 5, "(k" . $currency_symbol .")", 1, "", "C");
	$pdf->Cell(20, 5, "per sqm (" .$currency_symbol . ")", 1, "", "R");
	$pdf->Cell(20, 5, "per sqm (CHF)", 1, "", "R");
	
	
	
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	
	
	$y_tmp = $y;

	
	

	//list investments
	$pdf->SetFont("arialn", "", 9);
		
	$y = $y - 5;

	$y_lease_details = $y;


	
	foreach($fixed_assets as $key=>$itype)
	{
		if(array_key_exists($itype, $amounts))
		{
			if(!$amounts[$itype]){
				$amounts[$itype] = "";
			}
			
			
			$y = $y + 5;
			$pdf->SetXY($x1,$y);
			$pdf->SetFont("arialn", "", 9);

			$pdf->Cell(20, 5, $investment_names[$itype] , 0, "", "L");

			$pdf->SetXY($x2,$y);
			

			if($amounts[$itype] != "-" and $amounts[$itype] != 0)
			{	
				//$tmp = $cer_basicdata["cer_basicdata_exchangerate"]*$amounts[$itype]/$cer_basicdata["cer_basicdata_factor"];
				$tmp = $amounts[$itype];
				$pdf->Cell(20, 5, number_format(round($tmp/ 1000,0), 0, '.', "'"), 0, "", "R", true);
				
				if($gross_surface > 0)
				{
					$tmp = $amounts[$itype]/$gross_surface;
				}
				else
				{
					$tmp = "";
				}

				if($sales_surface > 0)
				{
					$tmp2 = $amounts[$itype]/$sales_surface;
				}
				else
				{
					$tmp2 = "";
				}

				/*
				if($tmp2)
				{
					$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'") . "/" . number_format(round($tmp2,0), 0, '.', "'"), 0, "", "R");
				}
				else
				{
					$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 0, "", "R");
				}
				*/

				$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 0, "", "R");

				
				if($gross_surface > 0)
				{
					$tmp = $cer_basicdata["cer_basicdata_exchangerate"]*$amounts[$itype]/$cer_basicdata["cer_basicdata_factor"];
					$tmp = $tmp/$gross_surface;
				}
				else
				{
					$tmp = "";
				}
				
				if($sales_surface > 0)
				{
					$tmp2 = $cer_basicdata["cer_basicdata_exchangerate"]*$amounts[$itype]/$cer_basicdata["cer_basicdata_factor"];
					$tmp2 = $tmp2/$sales_surface;
				}
				else
				{
					$tmp2 = "";
				}
				
				/*
				if($tmp2)
				{
					$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'") . "/" . number_format(round($tmp2,0), 0, '.', "'"), 0, "", "R");
				}
				else
				{
					$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 0, "", "R");
				}
				*/
				$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 0, "", "R");
			}
			else
			{
				$pdf->Cell(20, 5, "-", 0, "", "R", true);
				$pdf->Cell(20, 5, "-", 0, "", "R");
				$pdf->Cell(20, 5, "-", 0, "", "R");
			}

			
		}
		else
		{
			$y = $y + 5;
		}
	}


	
	$y_tmp2 = $y;
	//draw investment boxes
	$y_tmp = $y_tmp;
	$pdf->SetXY($x,$y_tmp);
	$pdf->Cell(60, 25, "", 1);
	$pdf->SetXY($x2,$y_tmp );
	$pdf->Cell(20, 25, "", 1);
	$pdf->SetXY($x2+20,$y_tmp);
	$pdf->Cell(20, 25, "", 1);
	$pdf->SetXY($x2+40,$y_tmp);
	$pdf->Cell(20, 25, "", 1);
	
	
	//Total Investment
	
	$y = $y_tmp2+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(60, 5, "Total investment in fixed assets", 1, "", "L");

	$pdf->Cell(20, 5, number_format(round($investment_total/1000, 0), 0, '.', "'"), 1, "", "R");

	if($gross_surface > 0)
	{
		$tmp = $investment_total/$gross_surface;
	}
	else
	{
		$tmp = "";
	}

	if($sales_surface > 0)
	{
		$tmp2 = $investment_total/$sales_surface;
	}
	else
	{
		$tmp2 = "";
	}

	/*
	if($tmp2)
	{
		$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'") . "/" . number_format(round($tmp2,0), 0, '.', "'"), 1, "", "R");
	}
	else
	{
		$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 1, "", "R");
	}
	*/
	$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 1, "", "R");
	
	if($gross_surface > 0)
	{
		$tmp = $cer_basicdata["cer_basicdata_exchangerate"]*$investment_total/$cer_basicdata["cer_basicdata_factor"];
		$tmp = $tmp/$gross_surface;
	}
	else
	{
		$tmp = "";
	}

	if($sales_surface > 0)
	{
		$tmp2 = $cer_basicdata["cer_basicdata_exchangerate"]*$investment_total/$cer_basicdata["cer_basicdata_factor"];
		$tmp2 = $tmp2/$sales_surface;
	}
	else
	{
		$tmp2 = "";
	}

	/*
	if($tmp2)
	{
		$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'") . "/" . number_format(round($tmp2,0), 0, '.', "'"), 1, "", "R");
	}
	else
	{
		$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 1, "", "R");
	}
	*/
	$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 1, "", "R");



	$y_tmp = $pdf->getY();

	$y = $y + 5;
	$pdf->SetXY($x1,$y);
	

	//intangibles
	$pdf->SetXY($x1,$y);
	$pdf->SetFont("arialn", "", 9);

	

	$pdf->Cell(60, 5, $intagible_name , 0, "", "L");

	$pdf->Cell(20, 5, number_format(round($intagible_amount/1000, 0), 0, '.', "'"), 0, "", "R", true);

	if($gross_surface > 0)
	{
		$tmp = $intagible_amount/$gross_surface;
	}
	else
	{
		$tmp = "";
	}

	if($sales_surface > 0)
	{
		$tmp2 = $intagible_amount/$sales_surface;
	}
	else
	{
		$tmp2 = "";
	}

	/*
	if($tmp2)
	{
		$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'") . "/" . number_format(round($tmp2,0), 0, '.', "'"), 0, "", "R");
	}
	else
	{
		$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 0, "", "R");
	}
	*/
	$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 0, "", "R");
	
	if($gross_surface > 0)
	{
		$tmp = $cer_basicdata["cer_basicdata_exchangerate"]*$intagible_amount/$cer_basicdata["cer_basicdata_factor"];
		$tmp = $tmp/$gross_surface;
	}
	else
	{
		$tmp = "";
	}

	if($sales_surface > 0)
	{
		$tmp2 = $cer_basicdata["cer_basicdata_exchangerate"]*$intagible_amount/$cer_basicdata["cer_basicdata_factor"];
		$tmp2 = $tmp2/$sales_surface;
	}
	else
	{
		$tmp2 = "";
	}

	/*
	if($tmp2)
	{
		$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'") . "/".  number_format(round($tmp2,0), 0, '.', "'"), 0, "", "R");
	}
	else
	{
		$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 0, "", "R");
	}
	*/
	$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 0, "", "R");
	
	$investment_total2 = $investment_total;
	if(is_numeric($intagible_amount))
	{
		$investment_total2 = $investment_total + $intagible_amount;
	}
	

	


	//deposit
	$y = $y + 5;
	$pdf->SetXY($x1,$y);
	$pdf->SetFont("arialn", "", 9);

	$pdf->Cell(60, 5, $investment_names[9] , 0, "", "L");

	$pdf->Cell(20, 5, number_format(round($amounts[9]/1000, 0), 0, '.', "'"), 0, "", "R", true);

	if($gross_surface > 0)
	{
		$tmp = $amounts[9]/$gross_surface;
	}
	else
	{
		$tmp = "";
	}

	if($sales_surface > 0)
	{
		$tmp2 = $amounts[9]/$sales_surface;
	}
	else
	{
		$tmp2 = "";
	}

	/*
	if($tmp2)
	{
		$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'") . "/" . number_format(round($tmp2,0), 0, '.', "'"), 0, "", "R");
	}
	else
	{
		$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 0, "", "R");
	}
	*/
	$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 0, "", "R");
	

	
	if($gross_surface > 0)
	{
		$tmp = $cer_basicdata["cer_basicdata_exchangerate"]*$amounts[9]/$cer_basicdata["cer_basicdata_factor"];
		$tmp = $tmp/$gross_surface;
	}
	else
	{
		$tmp = "";
	}
	if($sales_surface > 0)
	{
		$tmp2 = $cer_basicdata["cer_basicdata_exchangerate"]*$amounts[9]/$cer_basicdata["cer_basicdata_factor"];
		$tmp2 = $tmp2/$sales_surface;
	}
	else
	{
		$tmp2 = "";
	}

	/*
	if($tmp2)
	{
		$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'") . "/". number_format(round($tmp2,0), 0, '.', "'"), 0, "", "R");
	}
	else
	{
		$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 0, "", "R");
	}
	*/

	$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 0, "", "R");

	if(is_numeric($amounts[9]))
	{
		$investment_total2 = $investment_total2 + $amounts[9];
	}


	//other non capitalized cost
	$y = $y + 5;
	$pdf->SetXY($x1,$y);
	$pdf->SetFont("arialn", "", 9);

	$pdf->Cell(60, 5, $investment_names[13] , 0, "", "L");

	$pdf->Cell(20, 5, number_format(round($amounts[13]/1000, 0), 0, '.', "'"), 0, "", "R", true);

	if($gross_surface > 0)
	{
		$tmp = $amounts[13]/$gross_surface;
	}
	else
	{
		$tmp = "";
	}

	if($sales_surface > 0)
	{
		$tmp2 = $amounts[13]/$sales_surface;
	}
	else
	{
		$tmp2 = "";
	}

	/*
	if($tmp2)
	{
		$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'") . "/" . number_format(round($tmp2,0), 0, '.', "'"), 0, "", "R");
	}
	else
	{
		$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 0, "", "R");
	}
	*/
	
	$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 0, "", "R");
	
	if($gross_surface > 0)
	{
		$tmp = $cer_basicdata["cer_basicdata_exchangerate"]*$amounts[13]/$cer_basicdata["cer_basicdata_factor"];
		$tmp = $tmp/$gross_surface;
	}
	else
	{
		$tmp = "";
	}

	if($sales_surface > 0)
	{
		$tmp2 = $cer_basicdata["cer_basicdata_exchangerate"]*$amounts[13]/$cer_basicdata["cer_basicdata_factor"];
		$tmp2 = $tmp2/$sales_surface;
	}
	else
	{
		$tmp2 = "";
	}

	/*
	if($tmp2)
	{
		$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'") . "/" . number_format(round($tmp2,0), 0, '.', "'"), 0, "", "R");
	}
	else
	{
		$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 0, "", "R");
	}
	*/
	$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 0, "", "R");
	
	if(is_numeric($amounts[13]))
	{
		$investment_total2 = $investment_total2 + $amounts[13];
	}
	
	$y_tmp2 = $y;
	

	//draw intagible boxes
	
	$y_tmp = $y_tmp +5;
	$pdf->SetXY($x,$y_tmp);
	$pdf->Cell(60, 15, "", 1);
	$pdf->SetXY($x2,$y_tmp );
	$pdf->Cell(20, 15, "", 1);
	$pdf->SetXY($x2+20,$y_tmp);
	$pdf->Cell(20, 15, "", 1);
	$pdf->SetXY($x2+40,$y_tmp);
	$pdf->Cell(20, 15, "", 1);

	
	
	
	



	//Total project costs (requested amount)
	
	$y = $y_tmp2+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(60, 5, "Total project costs (requested amount)", 1, "", "L");

	$pdf->Cell(20, 5, number_format(round($investment_total2/1000, 0), 0, '.', "'"), 1, "", "R");

	if($gross_surface > 0)
	{
		$tmp = $investment_total2/$gross_surface;
	}
	else
	{
		$tmp = "";
	}

	if($sales_surface > 0)
	{
		$tmp2 = $investment_total2/$sales_surface;
	}
	else
	{
		$tmp2 = "";
	}

	/*
	if($tmp2)
	{
		$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'") . "/" . number_format(round($tmp2,0), 0, '.', "'"), 1, "", "R");
	}
	else
	{
		$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 1, "", "R");
	}
	*/
	$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 1, "", "R");

	
	if($gross_surface > 0)
	{
		$tmp = $cer_basicdata["cer_basicdata_exchangerate"]*$investment_total2/$cer_basicdata["cer_basicdata_factor"];
		$tmp = $tmp/$gross_surface;
	}
	else
	{
		$tmp = "";
	}

	if($sales_surface > 0)
	{
		$tmp2 = $cer_basicdata["cer_basicdata_exchangerate"]*$investment_total2/$cer_basicdata["cer_basicdata_factor"];
		$tmp2 = $tmp2/$sales_surface;
	}
	else
	{
		$tmp2 = "";
	}

	/*
	if($tmp2)
	{
		$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'") . "/" . number_format(round($tmp2,0), 0, '.', "'"), 1, "", "R");
	}
	else
	{
		$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 1, "", "R");
	}
	*/

	$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 1, "", "R");

	//legend
	/*
	$y = $y_tmp2+10;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 7);
	$pdf->Cell(60, 5, "", 0, "", "L");
	$pdf->Cell(20, 5, "", 0, "", "L");
	$pdf->Cell(40, 5, "*per total surface/sales surface", 1, "", "L");
	*/


	


	//future investments
	if($cer_basicdata['cer_basicdata_future_investment'] > 0) {
		$y = $y_tmp2+10;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 9);
		$pdf->Cell(60, 5, "Planned Future Investment for Renovation*", 1, "", "L");

		$pdf->Cell(20, 5, number_format(round($cer_basicdata['cer_basicdata_future_investment']/1000, 0), 0, '.', "'"), 1, "", "R");

		if($gross_surface > 0)
		{
			$tmp = $cer_basicdata['cer_basicdata_future_investment']/$gross_surface;
		}
		else
		{
			$tmp = "";
		}

		if($sales_surface > 0)
		{
			$tmp2 = $cer_basicdata['cer_basicdata_future_investment']/$sales_surface;
		}
		else
		{
			$tmp2 = "";
		}

		$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 1, "", "R");

		
		if($gross_surface > 0)
		{
			$tmp = $cer_basicdata["cer_basicdata_exchangerate"]*$cer_basicdata['cer_basicdata_future_investment']/$cer_basicdata["cer_basicdata_factor"];
			$tmp = $tmp/$gross_surface;
		}
		else
		{
			$tmp = "";
		}

		if($sales_surface > 0)
		{
			$tmp2 = $cer_basicdata["cer_basicdata_exchangerate"]*$cer_basicdata['cer_basicdata_future_investment']/$cer_basicdata["cer_basicdata_factor"];
			$tmp2 = $tmp2/$sales_surface;
		}
		else
		{
			$tmp2 = "";
		}


		$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 1, "", "R");
		
		$y = $y_tmp2+16;
		$pdf->SetXY($x,$y);
		
		$tmp = $cer_basicdata['cer_basicdata_future_depr_start_month'] . "/" . $cer_basicdata['cer_basicdata_future_depr_start_year'];
		$tmp = "*Depreciation considered strating from "  . $tmp;
		$pdf->Cell(113, 5, $tmp, 0, "", "L");
	}
	

	//lease details

	$pdf->SetXY($x+120, $y_lease_details);
	$pdf->Cell(42, 6, "Rental:", 1, "", "L");
	$pdf->Cell(22, 6, "Contract:", 1, "", "L");
	$pdf->Cell(22, 6, "LN Information:", 1, "", "L");
	$pdf->Cell(40, 6, "Other information:", 1, "", "L");
	$pdf->Cell(22, 6, "Contract:", 1, "", "L");

	$y = $pdf->GetY()+6;
	$pdf->SetXY($x+120,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(42, 5, "Contract beg. date", 0, "", "L");
	if(count($posleases) > 0)
	{
		$pdf->Cell(22, 5, to_system_date($posleases["poslease_startdate"]), 0, "", "L");
	}
	else
	{
		$pdf->Cell(22, 5, "", 0, "", "L");
	}
	if(count($posleases_ln) > 0)
	{
		$pdf->Cell(22, 5, to_system_date($posleases_ln["poslease_startdate"]), 0, "", "L");
	}
	else
	{
		$pdf->Cell(22, 5, "", 0, "", "L");
	}
	$pdf->Cell(40, 5, "Headcount/FTE", 0, "", "L");
	$pdf->Cell(22, 5, $head_counts . "/" . $ftes, 0, "", "L");

	$y = $pdf->GetY()+5;
	$pdf->SetXY($x+120,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(42, 5, "Contract end date", 0, "", "L");
	if(count($posleases) > 0)
	{
		$pdf->Cell(22, 5, to_system_date($posleases["poslease_enddate"]), 0, "", "L");
	}
	else
	{
		$pdf->Cell(22, 5, "", 0, "", "L");
	}
	if(count($posleases_ln) > 0)
	{
		$pdf->Cell(22, 5, to_system_date($posleases_ln["poslease_enddate"]), 0, "", "L");
	}
	else
	{
		$pdf->Cell(22, 5, "", 0, "", "L");
	}
	$pdf->Cell(40, 5, "Gross surface (sqm)", 0, "", "L");
	$pdf->Cell(22, 5, $gross_surface, 0, "", "L");

	$y = $pdf->GetY()+5;
	$pdf->SetXY($x+120,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(42, 5, "Duration (years)", 0, "", "L");
	$pdf->Cell(22, 5, $contract_duration, 0, "", "L");
	$pdf->Cell(22, 5, $contract_duration_ln, 0, "", "L");
	$pdf->Cell(40, 5, "Sales Surface (sqm)", 0, "", "L");
	$pdf->Cell(22, 5, $sales_surface, 0, "", "L");

	$y = $pdf->GetY()+5;
	$pdf->SetXY($x+120,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(42, 5, "Renewal Option (years)", 0, "", "L");
	$pdf->Cell(22, 5, $renewal_option, 0, "", "L");
	$pdf->Cell(22, 5, $renewal_option_ln, 0, "", "L");
	$pdf->Cell(40, 5, "Other Surface (sqm)", 0, "", "L");
	$pdf->Cell(22, 5, $other_surface, 0, "", "L");
	
	

	$y = $pdf->GetY()+5;
	$pdf->SetXY($x+120,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(42, 5, "Date exit option", 0, "", "L");
	$pdf->Cell(22, 5, to_system_date($exit_option), 0, "", "L");
	$pdf->Cell(22, 5, to_system_date($exit_option_ln), 0, "", "L");
	$pdf->Cell(40, 5, "Opening date", 0, "", "L");
	$pdf->Cell(22, 5, $planned_opening_date, 0, "", "L");
	

	$y = $pdf->GetY()+5;
	$pdf->SetXY($x+120,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(42, 5, "Termination period (months)", 0, "", "L");
	$pdf->Cell(22, 5, $termination_time, 0, "", "L");
	$pdf->Cell(22, 5, $termination_time_ln, 0, "", "L");
	$pdf->Cell(40, 5, "Date LN approval", 0, "", "L");
	$pdf->Cell(22, 5, $ln_approved, 0, "", "L");
	

	$y = $pdf->GetY()+5;
	$pdf->SetXY($x+120,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(42, 5, "Index clause", 0, "", "L");
	$pdf->Cell(22, 5, $index_clause, 0, "", "L");
	$pdf->Cell(22, 5, $index_clause_ln, 0, "", "L");
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(40, 5, "In case of Renovation:", 0, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(22, 5, "", 0, "", "L");

	$y = $pdf->GetY()+5;
	$pdf->SetXY($x+120,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(42, 5, "", 0, "", "L");
	$pdf->Cell(22, 5, "", 0, "", "L");
	$pdf->Cell(22, 5, "", 0, "", "L");
	$pdf->Cell(40, 5, "Date of store reopening", 0, "", "L");
	//$pdf->Cell(22, 5, "", 0, "", "L");
	

	if($project["project_projectkind"] == 1)
	{
		$pdf->Cell(22, 5, "", 0, "", "L");
	}
	else
	{
		$pdf->Cell(22, 5, to_system_date($project["project_real_opening_date"]), 0, "", "L");
	}

	$y = $pdf->GetY()+5;
	$pdf->SetXY($x+120,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(42, 5, "Annual rent (k" . $currency_symbol . "):", 0, "", "L");
	$pdf->Cell(22, 5, "", 0, "", "L");
	$pdf->Cell(22, 5, "", 0, "", "L");
	$pdf->Cell(40, 5, "Last Year's sales (units)", 0, "", "L");
	
	if($project["project_projectkind"] == 1)
	{
		$pdf->Cell(22, 5, "", 0, "", "L");
	}
	else
	{
		$pdf->Cell(22, 5, $sellouts_last_full_year, 0, "", "L");
	}

	
	$y = $pdf->GetY()+5;
	$pdf->SetXY($x+120,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(42, 5, "Fix part", 0, "", "L");
	
	$pdf->Cell(22, 5, number_format($fixedrent_firstyear, 0, '.', "'"), 0, "", "L");
	$pdf->Cell(22, 5, number_format($fixedrent_firstyear_ln, 0, '.', "'"), 0, "", "L");
	$pdf->Cell(40, 5, "Last year's sales (k". $currency_symbol . ")", 0, "", "L");
	
	if($project["project_projectkind"] == 1)
	{
		$pdf->Cell(22, 5,  "", 0, "", "L");
	}
	else
	{
		$pdf->Cell(22, 5,  number_format(1*$sellouts_last_full_year_gross_sales, 0, '.', "'"), 0, "", "L");
	}

	$y = $pdf->GetY()+5;
	$pdf->SetXY($x+120,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(42, 5, "Turnover based part", 0, "", "L");
	$pdf->Cell(22, 5, $turnoverrent_firstyear . " (" . $turnoverrent_firstyear_share . ")", 0, "", "L");
	$pdf->Cell(22, 5, $turnoverrent_firstyear_ln . " (" .  $turnoverrent_firstyear_share_ln . ")", 0, "", "L");
	$pdf->Cell(40, 5, "Store fixture residual value", 0, "", "L");
	if($residual_value_former_investment > 0)
	{
		$pdf->Cell(22, 5, number_format(round($residual_value_former_investment/1000, 0), 0, '.', "'"), 0, "", "L");
	}
	else
	{
		$pdf->Cell(22, 5, "", 0, "", "L");
	}
	



	$y = $pdf->GetY()+5;
	$pdf->SetXY($x+206,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(40, 5, "Exchange rate (" . $currency_symbol . "/CHF)", 1, "", "L");
	$pdf->Cell(22, 5, $cer_basicdata["cer_basicdata_exchangerate"], 1, "", "L");
	$y = $pdf->GetY()-5;
	$pdf->SetXY($x+120,$y);
	
	$additional_y = 0;
	if($savings_on_rent !=0)
	{
		$y = $pdf->GetY()+5;
		$pdf->SetXY($x+120,$y);
		$pdf->SetFont("arialn", "", 9);
		$pdf->Cell(42, 5, "Savings on Rental Costs", 0, "", "L");
		$pdf->Cell(22, 5, $savings_on_rent, 0, "", "L");
		$pdf->Cell(22, 5, $savings_on_rent_ln, 0, "", "L");
		$additional_y = $additional_y + 5;
	}
	
	
	if($tax_on_rents !=0)
	{
		$y = $pdf->GetY()+5;
		$pdf->SetXY($x+120,$y);
		$pdf->SetFont("arialn", "", 9);
		$pdf->Cell(42, 5, "Tax on Rents", 0, "", "L");
		$pdf->Cell(22, 5, $tax_on_rents, 0, "", "L");
		$pdf->Cell(22, 5, $tax_on_rents_ln, 0, "", "L");
		$additional_y = $additional_y + 5;
	}
	

	if($passenger_index !=0)
	{
		$y = $pdf->GetY()+5;
		$pdf->SetXY($x+120,$y);
		$pdf->SetFont("arialn", "", 9);
		$pdf->Cell(42, 5, "Airport Passenger Index Costs ", 0, "", "L");
		$pdf->Cell(22, 5, $passenger_index, 0, "", "L");
		$pdf->Cell(22, 5, $passenger_index_ln, 0, "", "L");
		$additional_y = $additional_y + 5;
	}
	

	$y = $pdf->GetY()+5;
	$pdf->SetXY($x+120,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(42, 5, "Additional rental costs", 0, "", "L");
	$pdf->Cell(22, 5, $annual_charges, 0, "", "L");
	$pdf->Cell(22, 5, $annual_charges_ln, 0, "", "L");
	

	//draw boxes for lease details
	$y_tmp = $y;
	$y_lease_details = $y_lease_details+6;
	$pdf->SetXY($x+120, $y_lease_details);
	$pdf->Cell(42, 35, "", 1);
	
	$pdf->SetXY($x+162, $y_lease_details);
	$pdf->Cell(22, 35, "", 1);
	$pdf->SetXY($x+184, $y_lease_details);
	$pdf->Cell(22, 35, "", 1);
	
	$pdf->SetXY($x+206, $y_lease_details);
	$pdf->Cell(40, 35, "", 1);
	$pdf->SetXY($x+246, $y_lease_details);
	$pdf->Cell(22, 35, "", 1);

	//@bottom

	$y_lease_details = $y_lease_details + 40;
	
	$pdf->SetXY($x+120, $y_lease_details);
	$pdf->Cell(42, 20+$additional_y, "", 1);
	$pdf->SetXY($x+162, $y_lease_details);
	$pdf->Cell(22, 20+$additional_y, "", 1);
	
	
	$pdf->SetXY($x+184, $y_lease_details);
	$pdf->Cell(22, 20+$additional_y, "", 1);
	$y_lease_details = $y_lease_details - 5;
	$pdf->SetXY($x+206, $y_lease_details);
	$pdf->Cell(40, 25+$additional_y, "", 1);
	$pdf->SetXY($x+246, $y_lease_details);
	$pdf->Cell(22, 25+$additional_y, "", 1);

	
	
	$pdf->SetY($y_tmp);

	//draw outer box
	$y = $pdf->GetY()-$margin_top-3;
	$pdf->SetXY($margin_left,$y_at_start);
	$pdf->Cell(270, $y, "", 1);


	//print cost over view per SQM Detail in local currency
	
	
	$pdf->SetFont("arialn", "B", 9);
	$y = 120;
	$x = $x-1;
	$x2 = $x2 -1;


	$pdf->SetXY($x,$y);
	$pdf->Cell(60, 5, "Project costs, cost groups" , 1, "", "L");
	$pdf->SetXY($x2,$y);
	$pdf->Cell(25, 5, "Amount in " . $currency_symbol , 1, "", "R");
	$pdf->Cell(20, 5, "Share in %" , 1, "", "R");
	$pdf->Cell(20, 5, "per sqm GS" , 1, "", "R");
	$pdf->Cell(20, 5, "per sqm SS" , 1, "", "R");


	foreach($fixed_assets as $key=>$itype)
	{
		if(array_key_exists($itype, $amounts))
		{
			
			if(!$amounts[$itype]){
				$amounts[$itype] = "";
			}
			
			
			$y = $y + 5;
			$pdf->SetXY($x,$y);
			$pdf->SetFont("arialn", "", 9);

			$pdf->Cell(60, 5, $investment_names[$itype] , 1, "", "L");

			$pdf->SetXY($x2,$y);
			

			if($amounts[$itype] != "-" and $amounts[$itype] != 0)
			{	
				$tmp = $amounts[$itype];
				
				$pdf->Cell(25, 5, number_format($tmp, 0, '.', "'"), 1, "", "R", false);
				if($investment_total > 0)
				{
					$tmp = round(100*$tmp/$investment_total, 2);
				}
				else
				{
					$tmp = "";
				}
				$pdf->Cell(20, 5, $tmp . "%", 1, "", "R", false);
				
				
				if($gross_surface > 0)
				{
					$tmp = $amounts[$itype]/$gross_surface;
				}
				else
				{
					$tmp = "";
				}

				if($sales_surface > 0)
				{
					$tmp2 = $amounts[$itype]/$sales_surface;
				}
				else
				{
					$tmp2 = "";
				}

				$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 1, "", "R");

				$pdf->Cell(20, 5, number_format(round($tmp2,0), 0, '.', "'"), 1, "", "R");

			}
			else
			{
				$pdf->Cell(25, 5, "-", 1, "", "R", true);
				$pdf->Cell(20, 5, "-", 1, "", "R");
				$pdf->Cell(20, 5, "-", 1, "", "R");
				$pdf->Cell(20, 5, "-", 1, "", "R");
			}

			
		}
		else
		{
			$y = $y + 5;
		}
	}


	
	//Total Investment
	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(60, 5, "Total investment in fixed assets", 1, "", "L");

	$pdf->Cell(25, 5, number_format($investment_total, 0, '.', "'"), 1, "", "R");

	if($investment_total > 0)
	{
		$pdf->Cell(20, 5, "100.00%", 1, "", "R");
	}
	else
	{
		$pdf->Cell(20, 5, "", 1, "", "R");
	}

	if($gross_surface > 0)
	{
		$tmp = $investment_total/$gross_surface;
	}
	else
	{
		$tmp = "";
	}

	if($sales_surface > 0)
	{
		$tmp2 = $investment_total/$sales_surface;
	}
	else
	{
		$tmp2 = "";
	}

	$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 1, "", "R");
	$pdf->Cell(20, 5, number_format(round($tmp2,0), 0, '.', "'"), 1, "", "R");


	//print cost over view per SQM Detail in CHF
	$investment_total_chf = $cer_basicdata["cer_basicdata_exchangerate"]*$investment_total/$cer_basicdata["cer_basicdata_factor"];
	
	$pdf->SetFont("arialn", "B", 9);
	$y = 160;

	$pdf->SetXY($x,$y);
	$pdf->Cell(60, 5, "Project costs, cost groups" , 1, "", "L");
	$pdf->SetXY($x2,$y);
	$pdf->Cell(25, 5, "Amount in CHF" , 1, "", "R");
	$pdf->Cell(20, 5, "Share in %" , 1, "", "R");
	$pdf->Cell(20, 5, "per sqm GS" , 1, "", "R");
	$pdf->Cell(20, 5, "per sqm SS" , 1, "", "R");


	foreach($fixed_assets as $key=>$itype)
	{
		if(array_key_exists($itype, $amounts))
		{
			

			if(!$amounts[$itype]){
				$amounts[$itype] = "";
			}
			
			
			$y = $y + 5;
			$pdf->SetXY($x,$y);
			$pdf->SetFont("arialn", "", 9);

			$pdf->Cell(60, 5, $investment_names[$itype] , 1, "", "L");

			$pdf->SetXY($x2,$y);

			if($amounts[$itype] != "-" and $amounts[$itype] != 0)
			{	
								
				$tmp = $cer_basicdata["cer_basicdata_exchangerate"]*$amounts[$itype]/$cer_basicdata["cer_basicdata_factor"];
				$pdf->Cell(25, 5, number_format($tmp, 0, '.', "'"), 1, "", "R", false);


				if($investment_total_chf > 0)
				{
					$tmp = round(100*$tmp/$investment_total_chf, 2);
				}
				else
				{
					$tmp = "";
				}
				$pdf->Cell(20, 5, $tmp . "%", 1, "", "R", false);
				
				
				if($gross_surface > 0)
				{
					$tmp = $cer_basicdata["cer_basicdata_exchangerate"]*$amounts[$itype]/$cer_basicdata["cer_basicdata_factor"];
					$tmp = $tmp/$gross_surface;
				}
				else
				{
					$tmp = "";
				}

				if($sales_surface > 0)
				{
					$tmp2 = $cer_basicdata["cer_basicdata_exchangerate"]*$amounts[$itype]/$cer_basicdata["cer_basicdata_factor"];
					$tmp2 = $tmp2/$sales_surface;
				}
				else
				{
					$tmp2 = "";
				}

				$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 1, "", "R");

				$pdf->Cell(20, 5, number_format(round($tmp2,0), 0, '.', "'"), 1, "", "R");
				
			}
			else
			{
				$pdf->Cell(25, 5, "-", 1, "", "R", true);
				$pdf->Cell(20, 5, "-", 1, "", "R");
				$pdf->Cell(20, 5, "-", 1, "", "R");
				$pdf->Cell(20, 5, "-", 1, "", "R");
			}

			
		}
		else
		{
			$y = $y + 5;
		}
	}


	
	//Total Investment
	
	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(60, 5, "Total investment in fixed assets", 1, "", "L");

	$pdf->Cell(25, 5, number_format($investment_total_chf, 0, '.', "'"), 1, "", "R");

	if($investment_total_chf > 0)
	{
		$pdf->Cell(20, 5, "100.00%", 1, "", "R");
	}
	else
	{
		$pdf->Cell(20, 5, "", 1, "", "R");
	}

	if($gross_surface > 0)
	{
		$tmp = $investment_total_chf/$gross_surface;
	}
	else
	{
		$tmp = "";
	}

	if($sales_surface > 0)
	{
		$tmp2 = $investment_total_chf/$sales_surface;
	}
	else
	{
		$tmp2 = "";
	}

	$pdf->Cell(20, 5, number_format(round($tmp,0), 0, '.', "'"), 1, "", "R");
	$pdf->Cell(20, 5, number_format(round($tmp2,0), 0, '.', "'"), 1, "", "R");

			
?>