<?php
/********************************************************************

    cer_inr02c_pdf.php

    Print CER versus LN Figures (Form INR-02C).

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2016-01-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2016-01-15
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();

$SUPPRESS_HEADERS = true;
define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 

require "../include/frame.php";


check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}


require_once "../shared/func_posindex.php";
require "include/get_functions.php";
require "include/get_project.php";
require "include/financial_functions.php";
include("include/in_project_data.php");



//get data of latest ln version
$sql = "select ln_basicdata_version_cer_version from ln_basicdata where ln_basicdata_project = " . dbquote(param("pid")) . 
" and ln_basicdata_submitted is not null and ln_basicdata_submitted <> '0000-00-00' " .
" order by ln_basicdata_version desc";

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$cer_version = $row["ln_basicdata_version_cer_version"];

	include("include/in_financial_data.php");
	$ln_amounts = $amounts;
	$ln_exchange_rate = $cer_basicdata["cer_basicdata_exchangerate"];
	$ln_exchange_rate_factor = $cer_basicdata["cer_basicdata_factor"];
	$ln_investment_total = $investment_total;
	$ln_intagible_amount = $intagible_amount;
	$ln_totals = $cer_totals;
	$ln_sales_units_watches_values = $sales_units_watches_values;
	$ln_sales_units_jewellery_values = $sales_units_jewellery_values;
	$ln_sales_accessories_values = $sales_accessories_values;
	$ln_sales_customer_service_values = $sales_customer_service_values;
	$ln_total_net_sales_values = $total_net_sales_values;
	$ln_operating_income02_values = $operating_income02_values;
	$ln_operating_income02_80_percent_values = $operating_income02_80_percent_values;

	$ln_number_of_months_first_year = $number_of_months_first_year;
	$ln_number_of_months_last_year = $number_of_months_last_year;
	$ln_wholesale_margin = $weighted_average_whole_sale_margin;
	$ln_future_investment = $cer_basicdata['cer_basicdata_future_investment'];

}
elseif(isset($number_of_months_first_year))
{
	$ln_amounts = array();
	$ln_exchange_rate = $cer_basicdata["cer_basicdata_exchangerate"];
	$ln_exchange_rate_factor = $cer_basicdata["cer_basicdata_factor"];
	$ln_investment_total = 0;
	$ln_intagible_amount = 0;
	$ln_totals =0;
	$ln_sales_units_watches_values = array();
	$ln_sales_units_jewellery_values = array();
	$ln_sales_accessories_values = array();
	$ln_sales_customer_service_values = array();
	$ln_total_net_sales_values = array();
	$ln_operating_income02_values = array();
	$ln_operating_income02_80_percent_values = array();

	$ln_number_of_months_first_year = $number_of_months_first_year;
	$ln_number_of_months_last_year = $number_of_months_last_year;
	$ln_wholesale_margin = "";
	$ln_future_investment = $cer_basicdata['cer_basicdata_future_investment'];
}
else
{
	$ln_amounts = array();
	$ln_exchange_rate = 0;
	$ln_exchange_rate_factor = 0;
	$ln_investment_total = 0;
	$ln_intagible_amount = 0;
	$ln_totals =0;
	$ln_sales_units_watches_values = array();
	$ln_sales_units_jewellery_values = array();
	$ln_sales_accessories_values = array();
	$ln_sales_customer_service_values = array();
	$ln_total_net_sales_values = array();
	$ln_operating_income02_values = array();
	$ln_operating_income02_80_percent_values = array();

	$ln_number_of_months_first_year = 0;
	$ln_number_of_months_last_year = 0;
	$ln_wholesale_margin = "";
	$ln_future_investment = 0;
}


//actual cer
$cer_version = 0;
$cer_basicdata = get_cer_basicdata(param("pid"), $cer_version);
$exchange_rate = $cer_basicdata["cer_basicdata_exchangerate"];
$exchange_rate_factor = $cer_basicdata["cer_basicdata_factor"];
include("include/in_financial_data.php");	

//show data in client's currency
if(array_key_exists('cid', $_GET) and $_GET['cid'] == 'c1')
{
	$exchange_rate = 1;
	$exchange_rate_factor = 1;

	$ln_exchange_rate = 1;
	$ln_exchange_rate_factor = 1;
}


require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');


// Create and setup PDF document

$pdf = new TCPDF("L", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 10, 10);
$pdf->setPrintHeader(false);

$pdf->SetDisplayMode(100); 		

$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');
$pdf->Open();

$pdf->SetTitle("Retail Capital Expenditure Request (CER) Form INR-02C");
$pdf->SetAuthor("Swatch Retailnet");
$pdf->SetDisplayMode(100);
$pdf->SetAutoPageBreak(false, 0);

include("cer_inr02c_pdf_detail.php");

$pdf->Output(BRAND . '_INR-02C_' . $project_name . "_" . $project_number . '.pdf');

?>