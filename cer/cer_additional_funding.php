<?php
/********************************************************************

    cer_additional_funding.php

    Application Form: request for additional refunding
    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2012-03-06
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2012-03-06
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";

check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}
/********************************************************************
    prepare all data needed
*********************************************************************/


$project = get_project(param("pid"));
$cer_currency = get_cer_currency(param("pid"));
$currency = get_currency($cer_currency["id"]);

$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_address"] . ", " .
		$project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];

$project_detail = $project["project_number"] . ", " . $project["project_costtype_text"] . " " . $project["postype_name"] . ", " . $project["product_line_name"] . ", " . $project["projectkind_name"];


//insert approval names in case they are missing
$sql = "select cer_refunding_appronalname1, cer_refunding_appronalname5, " . 
       "cer_refunding_status from cer_refundings  " . 
       "where cer_refunding_project = " .param("pid");

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	if(!$row["cer_refunding_status"])
	{
		$sql = "select * from cer_approvalnames where cer_approvalname_id = 1";
		$res_p = mysql_query($sql) or dberror($sql);
		if($row_p = mysql_fetch_assoc($res_p))
		{
			$fields = array();

			$value = dbquote($row_p["cer_approvalname_name7"]);
			$fields[] = "cer_refunding_appronalname5 = " . $value;

			$value = dbquote($row_p["cer_approvalname_name3"]);
			$fields[] = "cer_refunding_appronalname6 = " . $value;

			$value = dbquote($row_p["cer_approvalname_name9"]);
			$fields[] = "cer_refunding_appronalname4 = " . $value;

			$value = dbquote($row_p["cer_approvalname_name2"]);
			$fields[] = "cer_refunding_appronalname7 = " . $value;

			$value = dbquote($row_p["cer_approvalname_name11"]);
			$fields[] = "cer_refunding_appronalname8 = " . $value;

			$sql = "update cer_refundings set " . join(", ", $fields) . " where cer_refunding_project = " .param("pid");
			mysql_query($sql) or dberror($sql);
		}
	}

	if(!$row["cer_refunding_appronalname1"])
	{
		$sql = "select cer_summary_in01_sig01, cer_summary_in01_sig02, cer_summary_in01_sig03 from cer_summary " . 
				"where cer_summary_cer_version = 0 and cer_summary_project = " .param("pid");

		$res_p = mysql_query($sql) or dberror($sql);
		if ($row_p = mysql_fetch_assoc($res_p))
		{

			$fields = array();

			$value = dbquote($row_p["cer_summary_in01_sig01"]);
			$fields[] = "cer_refunding_appronalname1 = " . $value;

			$value = dbquote($row_p["cer_summary_in01_sig02"]);
			$fields[] = "cer_refunding_appronalname2 = " . $value;

			$value = dbquote($row_p["cer_summary_in01_sig03"]);
			$fields[] = "cer_refunding_appronalname3 = " . $value;

			$sql = "update cer_refundings set " . join(", ", $fields) . " where cer_refunding_project = " .param("pid");
			mysql_query($sql) or dberror($sql);


			$cer_refunding_appronalname1 = $row_p["cer_summary_in01_sig01"];
			$cer_refunding_appronalname2 = $row_p["cer_summary_in01_sig02"];
			$cer_refunding_appronalname3 = $row_p["cer_summary_in01_sig03"];
		}
	}
}


//check if data structure is present
$refunding_status = "";
$refunding_justification = "";
$refunding_basicdata = "";
$refunding_submission = "";
$refunding_submission_by = "";
$refunding_resubmission = "";
$refunding_submission_by = "";
$refunding_rejected = "";
$refunding_rejected_by = "";
$attachment1 = "";
$attachment2 = "";
$attachment3 = "";
$attachment4 = "";
$refunding_locked_status = "";
$share_business_partner = "";

$submitted_by = array();
$resubmitted_by = array();
$rejected_by = array();


$sql = "select * from cer_refundings where cer_refunding_project = " . param("pid");
$res = mysql_query($sql) or dberror($sql);


if ($row = mysql_fetch_assoc($res))
{
	$id = $row["cer_refunding_id"];
	$refunding_status = $row["cer_refunding_status"];
	$refunding_justification = $row["cer_refunding_justification"];
	$refunding_basicdata = $row["cer_refunding_effects"];

	$refunding_submission = to_system_date($row["cer_refunding_submissiondate"]);
	$refunding_submission_by = $row["cer_refunding_sumbitted_by"];
	$refunding_resubmission = to_system_date($row["cer_refunding_resumissiondate"]);
	$refunding_resubmission_by = $row["cer_refunding_resubmitted_by"];

	$refunding_rejected = to_system_date($row["cer_refunding_rejected"]);
	$refunding_rejected_by = $row["cer_refunding_rejected_by"];

	$attachment1 = $row["cer_refunding_attachment1"];
	$attachment2 = $row["cer_refunding_attachment2"];
	$attachment3 = $row["cer_refunding_attachment3"];
	$attachment4 = $row["cer_refunding_attachment4"];

	$refunding_locked_status = $row["cer_refunding_locked"];

	$submitted_by = get_user($refunding_submission_by);
	$resubmitted_by = get_user($refunding_resubmission_by);
	$rejected_by = get_user($refunding_rejected_by);


	$cer_refunding_appronalname1 = $row["cer_refunding_appronalname1"];
	$cer_refunding_appronalname2 = $row["cer_refunding_appronalname2"];
	$cer_refunding_appronalname3 = $row["cer_refunding_appronalname3"];
	$cer_refunding_appronalname5 = $row["cer_refunding_appronalname5"];
	$cer_refunding_appronalname6 = $row["cer_refunding_appronalname6"];
	$cer_refunding_appronalname7 = $row["cer_refunding_appronalname7"];

	$share_business_partner = $row["cer_refunding_share_businesspartner"];
	
}
else
{
	
	$sql = "insert into cer_refundings (cer_refunding_project, user_created, date_created) VALUES (" . 
		   $project["project_id"] . ", '" . user_login() . "', '" . date("Y-m-d H:i:s") . "')";

	$result = mysql_query($sql) or dberror($sql);
	$id = mysql_insert_id();

	$sql = "select cer_summary_in01_sig01, cer_summary_in01_sig02, cer_summary_in01_sig03 from cer_summary " . 
			"where cer_summary_cer_version = 0 and cer_summary_project = " .param("pid");

	$res_p = mysql_query($sql) or dberror($sql);
	if ($row_p = mysql_fetch_assoc($res_p))
	{

		$fields = array();

		$value = dbquote($row_p["cer_summary_in01_sig01"]);
		$fields[] = "cer_refunding_appronalname1 = " . $value;

		$value = dbquote($row_p["cer_summary_in01_sig02"]);
		$fields[] = "cer_refunding_appronalname2 = " . $value;

		$value = dbquote($row_p["cer_summary_in01_sig03"]);
		$fields[] = "cer_refunding_appronalname3 = " . $value;

		$sql = "update cer_refundings set " . join(", ", $fields) . " where cer_refunding_project = " .param("pid");
		mysql_query($sql) or dberror($sql);


		$cer_refunding_appronalname1 = $row_p["cer_summary_in01_sig01"];
		$cer_refunding_appronalname2 = $row_p["cer_summary_in01_sig02"];
		$cer_refunding_appronalname3 = $row_p["cer_summary_in01_sig03"];
	}
	

	$sql = "select cer_summary_in01_sig01, cer_summary_in01_sig02, cer_summary_in01_sig03 from cer_summary " . 
				"where cer_summary_cer_version = 0 and cer_summary_project = " .param("pid");

	$res_p = mysql_query($sql) or dberror($sql);
	if ($row_p = mysql_fetch_assoc($res_p))
	{
		$cer_refunding_appronalname1 = $row_p["cer_summary_in01_sig01"];
		$cer_refunding_appronalname2 = $row_p["cer_summary_in01_sig02"];
		$cer_refunding_appronalname3 = $row_p["cer_summary_in01_sig03"];
	}
	
	$sql = "select * from cer_approvalnames where cer_approvalname_id = 1";
	$res_p = mysql_query($sql) or dberror($sql);
	if($row_p = mysql_fetch_assoc($res_p))
	{
		
		$cer_refunding_appronalname5 = $row_p["cer_approvalname_name7"];
		$cer_refunding_appronalname6 = $row_p["cer_approvalname_name3"];
		$cer_refunding_appronalname7 = $row_p["cer_approvalname_name4"];
	}
}



$result = build_missing_cer_investment_records(param("pid"));

$keymoney = get_pos_intangibles(param("pid"), 15);
$goodwill = get_pos_intangibles(param("pid"), 17);
$construction = get_pos_intangibles(param("pid"), 1);
$fixturing = get_pos_intangibles(param("pid"), 3);
$architectural = get_pos_intangibles(param("pid"), 5);
$equipment = get_pos_intangibles(param("pid"), 7);
$deposit = get_pos_intangibles(param("pid"), 9);
$other1 = get_pos_intangibles(param("pid"), 11);
$merchandising = get_pos_intangibles(param("pid"), 18);
$transportation = get_pos_intangibles(param("pid"), 19);
$other2 = get_pos_intangibles(param("pid"), 13);


if(!has_access("has_access_to_his_cer") and !has_access("has_full_access_to_cer"))
{
	$refunding_locked_status = 1;
}
/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_refundings", "cer_refundings");


$form->add_hidden("pid", param("pid"));
$form->add_hidden("id", $id);
$form->add_hidden("cer_refunding_project", param("pid"));


include("include/project_head.php");

$form->add_multiline("cer_refunding_status", "Status of Project*", 4, NOTNULL, $refunding_status);
$form->add_multiline("cer_refunding_justification", "Justification for the Request*", 4, NOTNULL, $refunding_justification);
$form->add_multiline("cer_refunding_effects", "Effects on the Project*", 4, NOTNULL, $refunding_basicdata);


//Additional Funding fields for editing
$total_additional = 0;
$form->add_section("Additional Funding in " . $currency["symbol"]);

$form->add_edit("construction_additional", $construction["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $construction["cer_investment_amount_additional_cer_loc"], TYPE_DECIMAL, 12, 2);

$total_additional = $construction["cer_investment_amount_additional_cer_loc"];

$form->add_edit("fixturing_additional", $fixturing["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $fixturing["cer_investment_amount_additional_cer_loc"], TYPE_DECIMAL, 12, 2);

$form->add_edit("architectural_additional", $architectural["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $architectural["cer_investment_amount_additional_cer_loc"], TYPE_DECIMAL, 12, 2);

$form->add_edit("equipment_additional", $equipment["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $equipment["cer_investment_amount_additional_cer_loc"], TYPE_DECIMAL, 12, 2);

$form->add_edit("other1_additional", $other1["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other1["cer_investment_amount_additional_cer_loc"], TYPE_DECIMAL, 12, 2);

$form->add_edit("merchandising_additional", $merchandising["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $merchandising["cer_investment_amount_additional_cer_loc"], TYPE_DECIMAL, 12, 2);

$form->add_edit("transportation_additional", $transportation["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $transportation["cer_investment_amount_additional_cer_loc"], TYPE_DECIMAL, 12, 2);

$total_additional = $total_additional + $fixturing["cer_investment_amount_additional_cer_loc"] + $architectural["cer_investment_amount_additional_cer_loc"] + $equipment["cer_investment_amount_additional_cer_loc"] + $other1["cer_investment_amount_additional_cer_loc"] + $merchandising["cer_investment_amount_additional_cer_loc"] + $transportation["cer_investment_amount_additional_cer_loc"];

$form->add_label("additional_total", "Total additional Investments in Fixed Assets in " . $currency["symbol"], 0, number_format($total_additional, 2, ".", "'"));


$form->add_edit("deposit_additional", $deposit["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $deposit["cer_investment_amount_additional_cer_loc"], TYPE_DECIMAL, 12, 2);

$form->add_edit("other2_additional", $other2["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other2["cer_investment_amount_additional_cer_loc"], TYPE_DECIMAL, 12, 2);

$total_additional2 = $total_additional + $deposit["cer_investment_amount_additional_cer_loc"] + $other2["cer_investment_amount_additional_cer_loc"];

$form->add_label("additional_total2", "Total additional amount in " . $currency["symbol"], 0, number_format($total_additional2, 2, ".", "'"));

$form->add_edit("cer_refunding_share_businesspartner", "Share Business Partner in %" , 0, $share_business_partner, TYPE_DECIMAL, 5, 2);

$deletable = "";
if(has_access("can_delete_uploaded_files"))
{
	$deletable = DELETABLE;
}
$form->add_section("Attachments");
$form->add_upload("cer_refunding_attachment1", "Attachment 1 (PDF only)", "/files/cer/". $project["project_number"], $deletable, $attachment1, 1, "");
$form->add_upload("cer_refunding_attachment2", "Attachment 2 (PDF only)", "/files/cer/". $project["project_number"], $deletable, $attachment2, 1, "");
$form->add_upload("cer_refunding_attachment3", "Attachment 3 (PDF only)", "/files/cer/". $project["project_number"], $deletable, $attachment3, 1, "");
$form->add_upload("cer_refunding_attachment4", "Attachment 4 (PDF only)", "/files/cer/". $project["project_number"], $deletable, $attachment4, 1, "");



$form->add_section("Signature of requesting entity (PC, Country)");
$form->add_comment("Please put in the names of the persons.");
$form->add_edit("cer_refunding_appronalname1", "Country Manager*", NOTNULL, $cer_refunding_appronalname1);
$form->add_edit("cer_refunding_appronalname2", "Finance/Service Center Manager*", NOTNULL, $cer_refunding_appronalname2);
$form->add_edit("cer_refunding_appronalname3", "Brand Manager*", NOTNULL, $cer_refunding_appronalname3);

if(has_access("has_full_access_to_cer"))
{
	$form->add_edit("cer_refunding_appronalname5", "VP Finance", 0, $cer_refunding_appronalname5);
	$form->add_edit("cer_refunding_appronalname6", "VP Sales", 0, $cer_refunding_appronalname6);
	$form->add_edit("cer_refunding_appronalname7", "Merchandising Manager", 0, $cer_refunding_appronalname7);
}

$form->add_section("Submission");


$by = "";
if(count($submitted_by) > 0 and $submitted_by["name"])
{
	$by = " by " . $submitted_by["name"] . " " . $submitted_by["firstname"];
}
$form->add_label("date1",  "Submission Date", 0,  $refunding_submission . $by);

$by = "";
if(count($resubmitted_by) > 0 and $resubmitted_by["name"])
{
	$by = " by " . $resubmitted_by["name"] . " " . $resubmitted_by["firstname"];
}

$form->add_label("date2",  "Date of latest Resubmission", 0,  $refunding_resubmission . $by);


$by = "";
if(count($rejected_by) > 0 and $rejected_by["name"])
{
	$by = " by " . $rejected_by["name"] . " " . $rejected_by["firstname"];
}

$form->add_label("date3",  "Date Rejection", 0,  $refunding_rejected . $by);




if($refunding_submission == "")
{
	$button_Text = "Submit Request";
	$submission_type = "submit";
}
else
{
	$button_Text = "Resubmit Request";
	$submission_type = "rsubmit";
}

if($refunding_locked_status == 0)
{
	$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" /><a href=\"#\" onClick=\"button('submit')\">" .  $button_Text . "</a>";

	$form->add_label("submit", "Submission", RENDER_HTML, $link);
}

if(has_access("has_full_access_to_cer"))
{
	$form->add_checkbox("cer_refunding_locked", "Request is locked", $refunding_locked_status, 0, "Locking");
}
else
{
	$form->add_hidden("cer_basicdata_cer_locked");
}


$link = "cer_additional_funding_pdf.php?pid=" . param("pid"); 
$link = "javascript:popup('". $link . "', 800, 600)";


if($id > 0)
{
	$form->add_button("pdf", "Print Request Form (PDF)", $link);
}

if($refunding_locked_status == 0 or has_access("has_full_access_to_cer"))
{
	$form->add_button("save", "Save");
}


if($refunding_locked_status == 0 and $refunding_submission)
{
	if(has_access("can_reject_submissions"))
	{
		if($refunding_submission and from_system_date($refunding_rejected) > from_system_date($refunding_resubmission))
		{

		}
		elseif(!$refunding_resubmission and from_system_date($refunding_rejected) > from_system_date($refunding_submission))
		{
		}
		else
		{
			$link = "javascript:createWindowWithRemotingUrl2('Reject Request fro Additional Funding', 'reject_submission.php?pid=" . param("pid") . "&type=in03');";
			$form->add_button("reject", "Reject Request", $link);
		}
	}
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if($form->button("save") or $form->button("submit"))
{
	$valid = true;
	if($form->value("cer_refunding_attachment1") and strpos(strtolower($form->value("cer_refunding_attachment1")), 'pdf') == 0)
	{
		$form->error("Attachments must be a PDF-File.");
		$valid = false;
	}
	elseif($form->value("cer_refunding_attachment2") and strpos(strtolower($form->value("cer_refunding_attachment2")), 'pdf') == 0)
	{
		
		$form->error("Attachments must be a PDF-File.");
		$valid = false;
	}
	elseif($form->value("cer_refunding_attachment3") and strpos(strtolower($form->value("cer_refunding_attachment3")), 'pdf') == 0)
	{
		$form->error("Attachments must be a PDF-File.");
		$valid = false;
	}
	elseif($form->value("cer_refunding_attachment4") and strpos(strtolower($form->value("cer_refunding_attachment4")), 'pdf') == 0)
	{
		$form->error("Attachments must be a PDF-File.");
		$valid = false;
	}


	if($valid == true and $form->validate())
	{
		$form->save();

		//save investment type construction
		$fields = array();
    
		$value = dbquote($form->value("construction_additional"));
		$fields[] = "cer_investment_amount_additional_cer_loc = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $construction["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		
		
		
		//save submission information
		$fields = array();
    
		$value = dbquote($form->value("fixturing_additional"));
		$fields[] = "cer_investment_amount_additional_cer_loc = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $fixturing["cer_investment_id"];
		mysql_query($sql) or dberror($sql);


		//save investment type architectural
		$fields = array();
    
		$value = dbquote($form->value("architectural_additional"));
		$fields[] = "cer_investment_amount_additional_cer_loc = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $architectural["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		//save investment type equipment
		$fields = array();
    
		$value = dbquote($form->value("equipment_additional"));
		$fields[] = "cer_investment_amount_additional_cer_loc = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $equipment["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		//save investment type deposit
		$fields = array();
    
		$value = dbquote($form->value("deposit_additional"));
		$fields[] = "cer_investment_amount_additional_cer_loc = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " .$value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $deposit["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		//save investment type other costs
		$fields = array();
    
		$value = dbquote($form->value("other1_additional"));
		$fields[] = "cer_investment_amount_additional_cer_loc = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $other1["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		//save investment type merchandising
		$fields = array();
    
		$value = dbquote($form->value("merchandising_additional"));
		$fields[] = "cer_investment_amount_additional_cer_loc = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $merchandising["cer_investment_id"];
		mysql_query($sql) or dberror($sql);


		//save investment type transportation
		$fields = array();
    
		$value = dbquote($form->value("transportation_additional"));
		$fields[] = "cer_investment_amount_additional_cer_loc = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $transportation["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		//save investment type other non captalized costs
		$fields = array();
    
		$value = dbquote($form->value("other2_additional"));
		$fields[] = "cer_investment_amount_additional_cer_loc = " . $value;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $other2["cer_investment_id"];
		mysql_query($sql) or dberror($sql);


		$total_additional = $form->value('construction_additional') + $form->value('fixturing_additional')  + $form->value('architectural_additional')  + $form->value('equipment_additional')  + $form->value('other1_additional')  + $form->value('merchandising_additional')  + $form->value('transportation_additional');

		$form->value("additional_total", number_format($total_additional, 2, ".", "'"));
		
		$total_additional2 = $total_additional + $form->value("deposit_additional") + $form->value("other2_additional");

		$form->value("additional_total2", number_format($total_additional2, 2, ".", "'"));

		

		if($form->button("submit"))
		{

			if($total_additional2 > 0)
			{
				//submit request
				//get recipients of project submission alerts
				$recipient_user_ids = array();

				
				if($submission_type == "submit")
				{
					$sql = 'select * from projecttype_newproject_notifications ' . 
						   'where projecttype_newproject_notification_on_rfafsubmission = 1 ' . 
						   ' and projecttype_newproject_notification_country = ' . $project["order_shop_address_country"] . 
						   ' and projecttype_newproject_notification_postype = ' . $project['project_postype'];

					$fields = array();
    
					$value = dbquote(date("Y-m-d"));
					$fields[] = "cer_refunding_submissiondate = " . $value;

					$value = dbquote(user_id());
					$fields[] = "cer_refunding_sumbitted_by = " . $value;

					$value = $currency["id"];
					$fields[] = "cer_refunding_currency_id = " . $value;

					$value = $currency["exchange_rate"];
					$fields[] = "cer_refunding_exchangerate = " . $value;

					$value = $currency["factor"];
					$fields[] = "cer_refunding_factor = " . $value;

					$value1 = "current_timestamp";
					$fields[] = "date_modified = " . $value1;

					if (isset($_SESSION["user_login"]))
					{
						$value1 = $_SESSION["user_login"];
						$fields[] = "user_modified = " . dbquote($value1);
					}
			   
					$sql_u = "update cer_refundings set " . join(", ", $fields) . " where cer_refunding_id = " . param("id");
					mysql_query($sql_u) or dberror($sql_u);

				}
				else
				{
					$sql = 'select * from projecttype_newproject_notifications ' . 
						   'where projecttype_newproject_notification_on_rfafresubmission = 1 ' . 
						   ' and projecttype_newproject_notification_country = ' . $project["order_shop_address_country"] . 
						   ' and projecttype_newproject_notification_postype = ' . $project['project_postype'];


					$fields = array();
    
					$value = dbquote(date("Y-m-d"));
					$fields[] = "cer_refunding_resumissiondate = " . $value;

					$value = dbquote(user_id());
					$fields[] = "cer_refunding_resubmitted_by = " . $value;

					
					$value1 = "current_timestamp";
					$fields[] = "date_modified = " . $value1;

					if (isset($_SESSION["user_login"]))
					{
						$value1 = $_SESSION["user_login"];
						$fields[] = "user_modified = " . dbquote($value1);
					}
			   
					$sql_u = "update cer_refundings set " . join(", ", $fields) . " where cer_refunding_id = " . param("id");
					mysql_query($sql_u) or dberror($sql_u);
				}

				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					if($row["projecttype_newproject_notification_email"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["projecttype_newproject_notification_email"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_email"]] = $row_u["user_id"];
						}
						else
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_email"]] = 0;
						}
						$mailaddress = 1;
						
					}

					if($row["projecttype_newproject_notification_emailcc1"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc1"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc1"]] = $row_u["user_id"];
						}
						else
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc1"]] = 0;
						}
					}
					if($row["projecttype_newproject_notification_emailcc2"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc2"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc2"]] = $row_u["user_id"];
						}
						else
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc2"]] = 0;
						}
						
					}
					if($row["projecttype_newproject_notification_emailcc3"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc3"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc3"]] = $row_u["user_id"];
						}
						else
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc3"]] = 0;
						}
						
					}
					if($row["projecttype_newproject_notification_emailcc4"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc4"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc4"]] = $row_u["user_id"];
						}
						else
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc4"]] = 0;
						}
					
					}
					if($row["projecttype_newproject_notification_emailcc5"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc5"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc5"]] = $row_u["user_id"];
						}
						else
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc5"]] = 0;
						}
					
					}
					if($row["projecttype_newproject_notification_emailcc6"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc6"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc6"]] = $row_u["user_id"];
						}
						else
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc6"]] = 0;
						}
					
					}
					if($row["projecttype_newproject_notification_emailcc7"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc7"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc7"]] = $row_u["user_id"];
						}
						else
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc7"]] = 0;
						}
					}
				}

				//send mail notification

				$sql = "select order_number, order_shop_address_place, country_name, order_shop_address_company, " .
					   "project_postype, project_projectkind, project_cost_type " . 
					   "from projects " .
					   "left join orders on order_id = project_order " .
					   "left join countries on country_id = order_shop_address_country " .
					   "left join project_costs on project_cost_order = order_id " .
					   "where project_id = " .  dbquote(param("pid"));

				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					$user_data = get_user(user_id());

					$sender_email = $user_data["email"];
					$sender_name = $user_data["firstname"] . " " . $user_data["name"];

					
					if($submission_type == "submit")
					{
						$subject = "IN-03 submission alert  - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];

						$bodytext = "The IN-03 form was submitted by " . $sender_name . ".\n"; 
					}
					else
					{
						$subject = "IN-03 resubmission alert - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_place"] . ", " . $row["order_shop_address_company"];

						$bodytext = "The IN-03 form was resubmitted by " . $sender_name . ".\n"; 
					}

												

					$link ="cer_additional_funding_pdf.php?pid=" . param("pid");
					$bodytext .= "\nClick below to download the request form:\n";
					$bodytext .= APPLICATION_URL ."/cer/" . $link . "\n\n";           
					
					
					$bodytext .= "Thank you very much.\nKind regards, " . $sender_name;

					$mail = new PHPMailer();
					$mail->Subject = MAIL_SUBJECT_PREFIX . ": " . $subject;
					$mail->SetFrom($sender_email, $sender_name);
					$mail->AddReplyTo($sender_email, $sender_name);


					
					//get project manager
					$recipient_email = "";
					$sql = 'select user_email, concat(user_firstname, " ", user_name) as username from users ' . 
						   'where user_id = ' . dbquote($project["project_retail_coordinator"]);
					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$mail->AddAddress($row_u["user_email"], $row_u["username"]);
						$recipient_email .= $row_u["user_email"] . "\n";
					}


					//get brand manager
					$reciepient_cc = "";
					$sql = 'select user_email, concat(user_firstname, " ", user_name) as username ' .
					       'from addresses ' .
						   'left join users on user_address = address_id ' .
						   'left join user_roles on user_role_user = user_id ' . 
						   ' where user_active = 1 and user_role_role = 15 and address_id = ' . dbquote($project["order_client_address"]);
					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$mail->AddCC($row_u["user_email"], $row_u["username"]);
						$reciepient_cc .= strtolower($row_u["user_email"]) . "\n";
					}


					
					
					
	

					//add submission recipeint alerts
					
					foreach($recipient_user_ids as $key=>$user_id)
					{
						if(strpos($reciepient_cc, strtolower($key)) === 0)
						{
						
						}
						else
						{
							$mail->AddCC($key);
							$reciepient_cc .= $key. "\n";
						
						}
					}


					//get mailalerttype emails
					$sql = "select * from mail_alert_types " . 
						   "where mail_alert_type_id = 21";

					$res = mysql_query($sql) or dberror($sql);
					if ($row = mysql_fetch_assoc($res))
					{
						if($row["mail_alert_type_cc1"] and strpos($reciepient_cc, $row["mail_alert_type_cc1"]) === false)
						{
							$mail->AddCC($row["mail_alert_type_cc1"]);
							$reciepient_cc .= strtolower($row["mail_alert_type_cc1"]) . "\n";
						}
						if($row["mail_alert_type_cc2"] and strpos($reciepient_cc, $row["mail_alert_type_cc2"]) === false)
						{
							$mail->AddCC($row["mail_alert_type_cc2"]);
							$reciepient_cc .= strtolower($row["mail_alert_type_cc2"]) . "\n";
						}
						if($row["mail_alert_type_cc3"] and strpos($reciepient_cc, $row["mail_alert_type_cc3"]) === false)
						{
							$mail->AddCC($row["mail_alert_type_cc3"]);
							$reciepient_cc .= strtolower($row["mail_alert_type_cc3"]) . "\n";
						}
						if($row["mail_alert_type_cc4"] and strpos($reciepient_cc, $row["mail_alert_type_cc4"])=== false)
						{
							$mail->AddCC($row["mail_alert_type_cc4"]);
							$reciepient_cc .= strtolower($row["mail_alert_type_cc4"]) . "\n";
						}
					
					}
					
											
					$mail->Body = $bodytext;

					$result = $mail->send();

					$rctps = $recipient_email . "and CC-Mail to:" . "\n" . $reciepient_cc;

					if($result == 1)
					{
						$fields = array();
						$values = array();

						$fields[] = "cer_mail_project";
						$values[] = dbquote(param("pid"));

						$fields[] = "cer_mail_group";
						$values[] = dbquote($subject);

						$fields[] = "cer_mail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "cer_mail_sender";
						$values[] = dbquote($sender_name);

						$fields[] = "cer_mail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "cer_mail_reciepient";
						$values[] = dbquote($rctps);

						$fields[] = "date_created";
						$values[] = "now()";

						$fields[] = "date_modified";
						$values[] = "now()";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into cer_mails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

						mysql_query($sql) or dberror($sql);
					}
				}

				redirect("cer_additional_funding.php?pid=" . param("pid"));
			}
			else
			{
				$form->error("Please idicate at least one amount in the section \"Additional Funding in " . $currency["symbol"] . ".");
			}
		}
		else
		{
			$form->message("Your data has been saved.");
		}
	}
}


/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();
$page->title("Request for Additional Funding");

$form->render();


require "include/footer_scripts.php";
$page->footer();

?>