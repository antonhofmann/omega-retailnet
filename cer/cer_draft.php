<?php
/********************************************************************

    cer_draft.php

    Add/Edit Business Plan Draft

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-02-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-02-16
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require "include/get_draft_functions.php";

check_access("has_access_to_cer_drafts");


/********************************************************************
    prepare all data needed
*********************************************************************/



$currencies = array();
$currencies_exchangerates = array();
$currencies_factors = array();
$user_country = 0;
$currency = 0;
$exchangerate = 0;
$factor = 0;
$title = "";
$basicdata = array();

if(param('did')) {
	param('id', param('did'));

	$currency = get_draft_currency(param("did"));
	$basicdata = get_draft_basicdata(param("did"));
	$title = $basicdata["cer_basicdata_title"];
	$user_country = $basicdata['cer_basicdata_country'];


	//build missing investment records
	$result = build_missing_draft_investment_records(param('did'));

}

//get user's currencies
if(param('cer_basicdata_country')) {
	$sql = 'select country_id, currency_id,currency_symbol, currency_exchange_rate, currency_factor from countries ' . 
		   'left join currencies on currency_id = country_currency ' .
		   'where currency_id = 1 or country_id = ' . param('cer_basicdata_country');

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$currencies[$row['currency_id']] = $row['currency_symbol'];
		$currencies_exchangerates[$row['currency_id']] = $row['currency_exchange_rate'];
		$currencies_factors[$row['currency_id']] = $row['currency_factor'];
	
	}
}
elseif(param('did'))
{
	$basicdata = get_draft_basicdata(param("did"));
	$sql = 'select cer_basicdata_title, cer_basicdata_legal_type, cer_basicdata_country, cer_basicdata_currency, ' . 
		   'cer_basicdata_exchangerate, cer_basicdata_factor, ' .
		   'currency_id, currency_symbol ' .
		   'from cer_drafts ' . 
		   'left join currencies on currency_id = cer_basicdata_currency ' . 
		   'where cer_basicdata_id = ' . param("did");

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$currencies[$row['currency_id']] = $row['currency_symbol'];
		$currencies_exchangerates[$row['currency_id']] = $row['cer_basicdata_exchangerate'];
		$currencies_factors[$row['currency_id']] = $row['cer_basicdata_factor'];
	}
}
elseif(id() == 0)
{
	$sql = 'select country_id, currency_id,currency_symbol, currency_exchange_rate, currency_factor, address_country from users ' . 
		   'left join addresses on address_id = user_address ' .
		   'left join countries on country_id = address_country ' . 
		   'left join currencies on currency_id = country_currency ' .
		   'where user_id = ' . user_id();

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$currencies[$row['currency_id']] = $row['currency_symbol'];
		$currencies_exchangerates[$row['currency_id']] = $row['currency_exchange_rate'];
		$currencies_factors[$row['currency_id']] = $row['currency_factor'];
		$user_country = $row['address_country'];

		if($row['country_id'] == $row['address_country']) {
			$currency = $row['currency_id'];
			$exchangerate = $row['currency_exchange_rate'];
			$factor = $row['currency_factor'];
		}
	}

	$currencies[1] = 'CHF';
	$currencies_exchangerates[1] = 1;
	$currencies_factors[1] = 1;
}



$months = array();
for($i=1;$i<13;$i++)
{
	$months[$i] = $i;
}

$years = array();

if(id() > 0) {
	$s = date("Y")-10;
}
else
{
	$s = date("Y")-1;
}
$l = $s+31;
for($i=$s;$i<$l;$i++)
{
	$years[$i] = $i;
}



//get all brands
$brands = array();
$selected_brands = array();
$sql_b = "select cer_brand_id, cer_brand_name " . 
         " from cer_brands " . 
		 " where cer_brand_active = 1";

$res_b = mysql_query($sql_b) or dberror($sql_b);
while ($row_b = mysql_fetch_assoc($res_b))
{
	$brands[$row_b["cer_brand_id"]] = $row_b["cer_brand_name"];
}

$sql_b = "select DISTINCT cer_revenue_brand_id, cer_brand_name " . 
         " from cer_draft_revenues " . 
		 " left join cer_brands on cer_brand_id = cer_revenue_brand_id " . 
		 " where cer_revenue_draft_id = " . dbquote(param("did"));


$res_b = mysql_query($sql_b) or dberror($sql_b);
while ($row_b = mysql_fetch_assoc($res_b))
{
	$brands[$row_b["cer_revenue_brand_id"]] = $row_b["cer_brand_name"];
	$selected_brands[$row_b["cer_revenue_brand_id"]] = $row_b["cer_brand_name"];
}
asort($brands);


$sql_countires = 'select country_id, country_name from countries order by country_name';
$sql_cost_types = "select * from project_costtypes where project_costtype_id < 3";
$sql_pos_types = "select * from postypes where postype_id < 4";

$form = new Form("cer_drafts", "CER/AF Draft");

$form->add_section("Draft");
$form->add_hidden("did", param("did"));
$form->add_hidden("id", param("id"));
$form->add_hidden("cer_basicdata_user_id", user_id());
$form->add_edit("cer_basicdata_title", "Title of the Draft*", NOTNULL);
$form->add_list("cer_basicdata_legal_type", "Legal Type*", $sql_cost_types, NOTNULL);
$form->add_list("cer_basicdata_pos_type", "POS Type", $sql_pos_types, 0);

$form->add_list("cer_basicdata_country", "Country*", $sql_countires, NOTNULL|SUBMIT, $user_country);

$form->add_section("Business Plan Period");
$form->add_list("cer_basicdata_firstyear", "First Year*", $years, NOTNULL);
$form->add_list("cer_basicdata_firstmonth", "Month of First Year*", $months, NOTNULL);
$form->add_list("cer_basicdata_lastyear", "Last Year*", $years, NOTNULL);
$form->add_list("cer_basicdata_lastmonth", "Month of Last Year*", $months, NOTNULL);


$form->add_section("Currency and Exchange Rate");
$form->add_list("cer_basicdata_currency", "Currency*", $currencies, NOTNULL | SUBMIT, $currency);
$form->add_edit("cer_basicdata_exchangerate", "Exchange Rate*", NOTNULL, $exchangerate, TYPE_DECIMAL, 10,6);
$form->add_hidden("cer_basicdata_factor", $factor);



$form->add_section("Surfaces");
$form->add_edit("cer_basicdata_sqms", "Total Sales Surface in Square Meters*", NOTNULL, "", TYPE_DECIMAL, 12,2);

$form->add_section("Rental Information");
$form->add_list("cer_basicdata_lease_type", "Lease Type*",
				"select poslease_type_id, poslease_type_name from poslease_types order by poslease_type_id", NOTNULL);
$form->add_edit("cer_basicdata_lease_startdate", "Start Date*", NOTNULL, "", TYPE_DATE);
$form->add_edit("cer_basicdata_lease_enddate", "Expiry Date*", NOTNULL, "", TYPE_DATE);


$form->add_section("Brands included in the business plan");
foreach($brands as $brand_id=>$brand_name)
{
	if(array_key_exists($brand_id, $selected_brands))
	{
		$form->add_checkbox("brand_" . $brand_id, $brand_name, 1, 0, "Include");
	}
	else
	{
		$form->add_checkbox("brand_" . $brand_id, $brand_name, 0, 0, "Include");
	}
}


$form->add_button(FORM_BUTTON_SAVE, "Save");


//if(count($basicdata)> 0 and (user_id() == $basicdata['cer_basicdata_user_id'])) {

if(id()) {
	$form->add_button("delete_draft", "Delete Draft");
}
$form->add_button(FORM_BUTTON_BACK, "Back");
//}


$form->populate();
$form->process();


if($form->button(FORM_BUTTON_SAVE)) {
	
	$brand_selected = false;
	$selected_brands = array();
	foreach($brands as $brand_id=>$brand_name)
	{
		if(array_key_exists("brand_" .$brand_id, $_POST) and $_POST["brand_" .$brand_id] == 1)
		{
			$brand_selected = true;
			$selected_brands[$brand_id] = $brands[$brand_id];
		}
	}

	if($brand_selected == false)
	{
		$form->error("Please select at least one brand to be included in the business plan!");
	}
	elseif($form->validate())
	{
		if(id() > 0) {
			param('did', id());
		}


		if(param('did') == 0)
		{
			$draft_id = mysql_insert_id();
		}
		else
		{
			$draft_id = param('did');
		}

		
		//get standardparameters
		$sparams = array();
		$sql = "select * from cer_standardparameters";
		$res = mysql_query($sql) or dberror($sql);
		while($row = mysql_fetch_assoc($res))
		{
			$sparams[$row["cer_standardparameter_id"]] = $row["cer_standardparameter_value"];
		}

		//get inflationrates
		$inflationrates = array();
		$sql = "select * from cer_inflationrates " . 
			   "where inflationrate_country = " . $form->value('cer_basicdata_country') . 
			   " order by inflationrate_year";

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$inflationrates[$row["inflationrate_year"]] = $row["inflationrate_rate"];
		}

		$inflationrates = serialize($inflationrates);
		
		//get interestrates
		$interestrates = array();

		$sql = "select * " .
			   "from cer_interestrates where interestrate_country = " . $form->value('cer_basicdata_country')  . 
			   " order by interestrate_year ASC";

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$interestrates[$row["interestrate_year"]] = $row["interestrate_rate"];
		}

		$interestrates = serialize($interestrates);

		//update only if project state <= 620
		$fields = array();


		$fields[] = "cer_basicdata_dicount_rate  = " . dbquote($sparams[1]);
		$fields[] = "cer_bascidata_liquidation_keymoney = " . dbquote($sparams[2]);
		$fields[] = "cer_basicdata_liquidation_deposit = " . dbquote($sparams[3]);
		$fields[] = "cer_bascidata_liquidation_stock = " . dbquote($sparams[4]);
		$fields[] = "cer_bascicdate_liquidation_staff = " . dbquote($sparams[5]);
		$fields[] = "cer_basicdata_inflationrates = " . dbquote($inflationrates);
		$fields[] = "cer_basic_data_interesrates = " . dbquote($interestrates);


		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}

		$sql = "update cer_drafts set " . join(", ", $fields) . " where cer_basicdata_id = " . $draft_id;
		mysql_query($sql) or dberror($sql);

		
		//delete brands not being selected
		$brands_not_selected = array();
		foreach($brands as $brand_id=>$brand_name)
		{
			if(!array_key_exists($brand_id, $selected_brands))
			{
				$brands_not_selected[] = $brand_id;
			}
		}

		if(count($brands_not_selected) > 0)
		{
			$sql = "delete from cer_draft_revenues " . 
				   " where cer_revenue_draft_id = " . $draft_id . 
				   " and cer_revenue_brand_id in (" . implode(",", $brands_not_selected) . ") ";

			$result = mysql_query($sql) or dberror($sql);

		}

		//build missing revenue_records
		if($form->value('cer_basicdata_firstyear') > 0 and $form->value('cer_basicdata_lastyear') > 0) {
			
			check_revenues($draft_id, $form->value('cer_basicdata_firstyear'), $form->value('cer_basicdata_lastyear'), $selected_brands);
			check_expenses($draft_id, $form->value('cer_basicdata_firstyear'), $form->value('cer_basicdata_lastyear'));
			check_stocks($draft_id, $form->value('cer_basicdata_firstyear'), $form->value('cer_basicdata_lastyear'));
			check_paymentterms($draft_id, $form->value('cer_basicdata_firstyear'), $form->value('cer_basicdata_lastyear'));

			$result = update_turnoverbased_rental_cost($draft_id);

		}

		
		$form->message("Your data has bee saved.");

		$link = "cer_draft.php?did=" . $draft_id;
		redirect($link);
	}
}
elseif($form->button('cer_basicdata_currency')) {

	if($form->value('cer_basicdata_currency')) {
		$form->value('cer_basicdata_exchangerate', $currencies_exchangerates[$form->value('cer_basicdata_currency')]);
		$form->value('cer_basicdata_factor', $currencies_factors[$form->value('cer_basicdata_currency')]);
	}
	else
	{
		$form->value('cer_basicdata_exchangerate', '');
		$form->value('cer_basicdata_factor', '');
	}
}
elseif($form->button('cer_basicdata_country')) {
	$sql = 'select * from countries ' . 
		   'left join currencies on currency_id = country_currency ' . 
		   'where country_id = ' . $form->value('cer_basicdata_country');
	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$form->value('cer_basicdata_currency', $row['currency_id']);
		$form->value('cer_basicdata_exchangerate', $row['currency_exchange_rate']);
		$form->value('cer_basicdata_factor', $row['currency_factor']);
	}
}
elseif($form->button('delete_draft')) {
	
	//delete files
	$sql = "select * from cer_draft_files where cer_draft_file_draft = " . param('did');
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		unlink($_SERVER["DOCUMENT_ROOT"] . $row["cer_draft_file_path"]);
	}
	
	
	$sql = 'Delete from cer_drafts where cer_basicdata_id =' . param('did');
	$res = mysql_query($sql) or dberror($sql);

	$sql = 'Delete from cer_draft_expenses where cer_expense_draft_id =' . param('did');
	$res = mysql_query($sql) or dberror($sql);

	//new additional rental costs
	$sql = 'Delete from cer_draft_additional_rental_costs where cer_additional_rental_cost_draft_id =' . param('did');
	$res = mysql_query($sql) or dberror($sql);

	
	$sql = 'Delete from cer_draft_additional_rental_cost_amounts where cer_additional_rental_cost_amount_draft_id =' . param('did');
	$res = mysql_query($sql) or dberror($sql);

	//end new additional rental costs

	$sql = 'Delete from cer_draft_investments where cer_investment_draft_id =' . param('did');
	$res = mysql_query($sql) or dberror($sql);

	$sql = 'Delete from cer_draft_paymentterms where cer_paymentterm_draft_id =' . param('did');
	$res = mysql_query($sql) or dberror($sql);

	$sql = 'Delete from cer_draft_revenues where cer_revenue_draft_id =' . param('did');
	$res = mysql_query($sql) or dberror($sql);

	$sql = 'Delete from cer_draft_salaries where cer_salary_draft_id =' . param('did');
	$res = mysql_query($sql) or dberror($sql);

	$sql = 'Delete from cer_draft_stocks where cer_stock_draft_id =' . param('did');
	$res = mysql_query($sql) or dberror($sql);

	$sql = 'Delete from cer_draft_rent_percent_from_sales where cer_rent_percent_from_sale_draft_id =' . param('did');
	$res = mysql_query($sql) or dberror($sql);

	$sql = 'Delete from cer_draft_fixed_rents where cer_fixed_rent_draft_id =' . param('did');
	$res = mysql_query($sql) or dberror($sql);

	$sql = 'Delete from cer_draft_files where cer_draft_file_draft =' . param('did');
	$res = mysql_query($sql) or dberror($sql);

	$link = '/cer/cer_drafts.php';
	redirect($link);
	

}

$page = new Page("cer_drafts");
require "include/draft_page_actions.php";
$page->header();



$page->title(id() ? $title . ": General Information" : "Add Business Plan Draft: General Information");

if(id() > 0) {
	require_once("include/tabs_draft.php");
}

$form->render();

require "include/draft_footer_scripts.php";
$page->footer();

?>