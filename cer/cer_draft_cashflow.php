<?php
/********************************************************************

	cer_draft_cashflow.php

    Application Form: revenues
    
	Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-02-17
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-02-17
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";

require "include/get_draft_functions.php";

check_access("has_access_to_cer_drafts");


/********************************************************************
    prepare all data needed
*********************************************************************/
$currency = get_draft_currency(param("did"));
$basicdata = get_draft_basicdata(param("did"));


param("id", $basicdata['cer_basicdata_id']);

//get stock_data
$stock_data = array();
$sql = "select * from cer_draft_stocks " .
       "where cer_stock_draft_id = " . param("did") . 
	   " order by cer_stock_year";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$stock_data[$row["cer_stock_year"]] = $row["cer_stock_stock_in_months"];
}

//get paymentterm_data
$paymentterm_data = array();
$sql = "select * from cer_draft_paymentterms " .
       "where cer_paymentterm_draft_id = " . param("did") . 
	   " order by cer_paymentterm_year";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$paymentterm_data[$row["cer_paymentterm_year"]] = $row["cer_paymentterm_in_months"];
}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_drafts", "cer_drafts");

$form->add_hidden("did", param("did"));


$form->add_section("Rental Formula");
$form->add_checkbox("calculate_rent_avg", "Calculate average rent per year", $basicdata["cer_basicdata_calculate_rent_avg"], 0, "Formula");


$form->add_section("Exchange Rate CER");
$form->add_label("symbol", "Currency", 0, $currency["symbol"]);
$form->add_label("exchangerate", "Exchange Rate", 0, $currency["exchange_rate"]);


$form->add_section("Calculation of Depreciation");
$form->add_checkbox("cer_extend_depreciation_period", "Extend depreciation period beyond the business plan period", $basicdata["cer_extend_depreciation_period"], 0, "Extension");


$form->add_section("Inflation Rate");

$inflation_rates = unserialize($basicdata["cer_basicdata_inflationrates"]);
foreach($inflation_rates as $key=>$value)
{
	$form->add_label("i" . $key, $key, 0, $value . "%");
}
if(!$inflation_rates)
{
	$form->add_label("i0", "Inflation Rate", 0, "not available");
}

$form->add_section("Interest Rate");

$interest_rates = unserialize($basicdata["cer_basic_data_interesrates"]);
foreach($interest_rates as $key=>$value)
{
	$form->add_label("ir" . $key, $key, 0, $value . "%");
}
if(!$interest_rates)
{
	$form->add_label("ir0", "Interest Rate", 0, "not available");
}



$form->add_section("Discounted Cash Flow Rate");
$form->add_edit("cer_basicdata_dicount_rate", "Discounted Cash Flow Rate in %", 0, "", TYPE_DECIMAL, 12,2);


if($basicdata['cer_basicdata_legal_type'] == 1) {
	$form->add_section("Extraordinary Financial Closing Impacts");
	$form->add_edit("cer_bascidata_liquidation_keymoney", "Liquidation Revenue on Keymoney in %", 0, "", TYPE_DECIMAL, 12,2);
	$form->add_edit("cer_basicdata_liquidation_deposit", "Liquidation Revenue on  Deposit in %", 0, "", TYPE_DECIMAL, 12,2);
	$form->add_edit("cer_bascidata_liquidation_stock", "Liquidation Revenue on Stock in %", 0, "", TYPE_DECIMAL, 12,2);
	$form->add_edit("cer_bascicdate_liquidation_staff", "Liquidation Revenue on  Staff in %", 0, "", TYPE_DECIMAL, 12,2);

	$form->add_section("Stock in Months");
	foreach($stock_data as $year=>$stock)
	{
		$form->add_edit("s" . $year, $year, 0, $stock, TYPE_DECIMAL, 10, 2);
	}

	$form->add_section("Payment Terms in Months for Liabilities");
	foreach($paymentterm_data as $year=>$paymentterm)
	{
		$form->add_edit("p" . $year, $year, 0, $paymentterm, TYPE_INT, 10);
	}
}

$form->add_button("form_save", "Save Data");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if($form->button("form_save"))
{
	if($form->validate())
	{
		
		$fields = array();
    
		$value = dbquote($form->value("cer_extend_depreciation_period"));
		$fields[] = "cer_extend_depreciation_period = " . $value;

		$value = dbquote($form->value("cer_basicdata_dicount_rate"));
		$fields[] = "cer_basicdata_dicount_rate = " . $value;

		$value = dbquote($form->value("calculate_rent_avg"));
		$fields[] = "cer_basicdata_calculate_rent_avg = " . $value;

		if($basicdata['cer_basicdata_legal_type'] == 1) 
		{
			$value = dbquote($form->value("cer_bascidata_liquidation_keymoney"));
			$fields[] = "cer_bascidata_liquidation_keymoney = " . $value;

			$value = dbquote($form->value("cer_basicdata_liquidation_deposit"));
			$fields[] = "cer_basicdata_liquidation_deposit = " . $value;

			$value = dbquote($form->value("cer_bascicdate_liquidation_staff"));
			$fields[] = "cer_bascicdate_liquidation_staff = " . $value;

		}

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_drafts set " . join(", ", $fields) . " where cer_basicdata_id = " . param("did");
		mysql_query($sql) or dberror($sql);

		$form->message("Your data has bee saved.");
	}

	if($basicdata['cer_basicdata_legal_type'] == 1 and $form->validate()) 
	{

		//update stocks

		foreach($stock_data as $year=>$stock)
		{
			$fields = array();
			$value = $form->value("s" . $year);
			
			$value = dbquote($value);
			$fields[] = "cer_stock_stock_in_months = " . $value;

			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_draft_stocks set " . join(", ", $fields) . " where cer_stock_year = " . $year . " and cer_stock_draft_id = " . param("did");
			mysql_query($sql) or dberror($sql);


		}

		//update paymentterms

		foreach($paymentterm_data as $year=>$paymentterm)
		{
			$fields = array();
			$value = $form->value("p" . $year);
			
			$value = dbquote($value);
			$fields[] = "cer_paymentterm_in_months = " . $value;

			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_draft_paymentterms set " . join(", ", $fields) . " where cer_paymentterm_year = " . $year . " and cer_paymentterm_draft_id = " . param("did");
			mysql_query($sql) or dberror($sql);


		}
	}
}
  
/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_drafts");
require "include/draft_page_actions.php";
$page->header();

$page->title($basicdata['cer_basicdata_title'] . ": Cash Flow Calculation");

require_once("include/tabs_draft.php");
$form->render();

?>

<div id="whole_sale_margin" style="display:none;">
    Wholesale margin in percent on costs of products sold
</div> 

<?php
require "include/draft_footer_scripts.php";
$page->footer();

?>