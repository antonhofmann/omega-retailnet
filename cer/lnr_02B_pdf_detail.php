<?php
/********************************************************************

    lnr_01_pdf_detail.php

    Print Detail Form LNR-01.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-06-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-06-14
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.
*********************************************************************/
include("include/in_financial_data.php");


$years_page1 = array();
$j= 1;
foreach($years as $key=>$year)
{
	if($j < 6)
	{
		$years_page1[] = $years[$key];
	}
	$j++;
}


//set pdf parameters
$margin_top = 12;
$margin_left = 12;
$y = $margin_top;
$x = $margin_left+1;
$standard_h = 6;

$pdf->AddPage();

//CHF
$exr = $ln_basicdata["ln_basicdata_exchangerate"];
$exrf = $ln_basicdata["ln_basicdata_factor"];

$investment_amount_chf = round($exr*$investment_total/$exrf/1000,0);
$key_money_chf = round($exr*$intagibles_total/$exrf/1000, 0);
$deposit_chf = round($exr*$deposit/$exrf/1000, 0);
$other_cost_chf = round($exr*$other_noncapitalized_cost/$exrf/1000, 0);

$construction_total = $construction_total + $other_costs + $equipment_costs;

$construction_total_chf  = round($exr*$construction_total/$exrf/1000,0);
$fixed_assets_total_chf  = round($exr*$fixed_assets_total/$exrf/1000,0);

$merchandising_total_chf =  round($exr*$merchandising_total/$exrf/1000,0);
$transportation_total_chf =  round($exr*$transportation_total/$exrf/1000,0);


//local currency
$investment_amount = round($investment_total/1000,0);
$key_money = round($intagibles_total/1000, 0);
$deposit = round($deposit/1000, 0);
$other_cost = round($other_noncapitalized_cost/1000, 0);

$construction_total  = round($construction_total/1000,0);
$fixed_assets_total  = round($fixed_assets_total/1000,0);


$merchandising_total =  round($merchandising_total/1000,0);
$transportation_total =  round($transportation_total/1000,0);

$franchisee_address = get_address($project["order_franchisee_address_id"]);
$client_address = get_address($project["order_client_address"]);

// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "B", 10);
$pdf->Cell(51, 8, "Swatch Group", 1);

$pdf->SetFont("arialn", "B", 11);
$pdf->Cell(179, 8, "LEASE NEGOTIATION - ADDITIONAL INFORMATION (FOR FRANCHISE / OTHER THIRD-PARTY STORES ONLY)", 1, "", "C");


$pdf->SetFont("arialn", "", 10);
$pdf->SetFillColor(248,251,167);
if($ln_version == 0 and $ln_basicdata["ln_basicdata_locked"] == 0)
{
	$pdf->Cell(20, 8, date("d.m.Y"), 1, "", "C", true);
}
else
{
	$pdf->Cell(20, 8, to_system_date($ln_basicdata["versiondate"]), 1, "", "C", true);
}
$pdf->Cell(20, 8, "LNR-02B", 1, "", "C");

	
	$y = $pdf->GetY()+10;
	$y_at_start = $y - 1;

	
	
	// 2.1. Business Partner Basic Information
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(268, 6, "2.1. Business Partner Basic Information (" . $project["order_shop_address_company"] . ", " . $project['order_number']. ")", 1, "", "L");

	
	
	$y = $pdf->GetY()+6;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(80, $standard_h, "Company Name", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(188, $standard_h, $franchisee_address["company"], 1, "", "L", true);

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(80, $standard_h, "Address", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(188, $standard_h, $franchisee_address["address"], 1, "", "L", true);

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(80, $standard_h, "City/Country", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(188, $standard_h, $franchisee_address["place"] . "/" . $franchisee_address["country"], 1, "", "L", true);

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(80, $standard_h, "Is Business Partner already part of our distribution (Yes/No)?", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);

	if($cer_basicdata["cer_basicdata_franchsiee_already_partner"] == 1)
	{
		$pdf->Cell(188, $standard_h, "Yes", 1, "", "L", true);
	}
	else
	{
		$pdf->Cell(188, $standard_h, "No", 1, "", "L", true);
	}

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(80, $standard_h, "If yes, number of stores", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	if($cer_basicdata["cer_basicdata_franchsiee_number_of_pos"] >0)
	{
		$pdf->Cell(188, $standard_h, $cer_basicdata["cer_basicdata_franchsiee_number_of_pos"], 1, "", "L", true);
	}
	else
	{
		$pdf->Cell(188, $standard_h, "", 1, "", "L", true);
	}

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(80, $standard_h, "If yes, which brand(s)?", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(188, $standard_h, $cer_basicdata["cer_basicdata_franchsiee_brands"], 1, "", "L", true);

	
	
	
	// 2.1. Business Partner Basic Information
	$y = $pdf->GetY()+ $standard_h+1;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(268, 6, "2.2. Furniture", 1, "", "L");

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(80, $standard_h, "Store Concept", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(188, $standard_h, $project["product_line_name"], 1, "", "L", true);

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(80, $standard_h, "Cost of furniture", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(188, $standard_h, $fixed_assets_total . " K" . $currency_symbol . " / " . $fixed_assets_total_chf . " KCHF", 1, "", "L", true);




	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(80, $standard_h, "Cost of merchandising material", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(188, $standard_h, $merchandising_total . " K" . $currency_symbol . " / " . $merchandising_total_chf . " KCHF", 1, "", "L", true);

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(80, $standard_h, "Cost of construction/installation", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(188, $standard_h, $construction_total . " K" . $currency_symbol . " / " . $construction_total_chf . " KCHF", 1, "", "L", true);

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(80, $standard_h, "Cost of transportation", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(188, $standard_h, $transportation_total . " K" . $currency_symbol . " / " . $transportation_total_chf . " KCHF", 1, "", "L", true);

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(80, $standard_h, "Total cost", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(188, $standard_h, $investment_amount . " K" . $currency_symbol . " / " . $investment_amount_chf . " KCHF", 1, "", "L", true);



	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(80, $standard_h, "Business Partner Contribution in %", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(188, $standard_h, $cer_basicdata["cer_basicdata_franchsiee_investment_share"] . "%", 1, "", "L", true);



	// 3. Sales
	$y = $pdf->GetY()+ $standard_h+1;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(80, 6, "3. Sales", 1, "", "L");
	$i=0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(37.6, $standard_h, $year , 1, "", "R");
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(37.6, $standard_h, "" , 1, "", "R");
	}

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(80, $standard_h, "Watches: Expected units sold", 1, "", "L");

	$pdf->SetFont("arialn", "", 9);
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(37.6, $standard_h, $sales_units_watches_values[$year] , 1, "", "R", true);
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(37.6, $standard_h, "" , 1, "", "R");
	}

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(80, $standard_h, "Bijoux: Expected units sold", 1, "", "L");
	

	$pdf->SetFont("arialn", "", 9);
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(37.6, $standard_h, $sales_units_jewellery_values[$year] , 1, "", "R", true);
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(37.6, 5, "" , 1, "", "R");
	}




	// 4. Approvals
	$y = $pdf->GetY()+7;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(268, 6, "4. Approvals", 1, "", "L");

	
	if($client_address["address_is_hq_agent"] == 1) {

		$y = $pdf->GetY()+6;
		$pdf->SetXY($margin_left+1,$y);

		$pdf->SetFont("arialn", "B", 9);

		$pdf->MultiCell(60,10, "", 1, "L");
		$pdf->SetXY($margin_left+1,$y);
		$pdf->MultiCell(20,10, "Brand", 0, "T");
		$pdf->SetFont("arialn", "", 9);
		$pdf->SetXY($margin_left+20, $y);
		$pdf->MultiCell(40,10, BRAND . " Retail Controller" . "\r\n" . $approval_name9, 0, "T");
		$pdf->SetXY($margin_left+60,$y);
		$pdf->MultiCell(75,10, "", 1, "T", true);


		$y = $pdf->GetY();
		$pdf->SetXY($margin_left+1,$y);

		
		$pdf->SetFont("arialn", "B", 9);

		$pdf->MultiCell(60,10, "", 1, "L");
		$pdf->SetXY($margin_left+134,$y);
		$pdf->MultiCell(20,10, "", 0, "T");
		$pdf->SetFont("arialn", "", 9);
		$pdf->SetXY($margin_left+20,$y);
		$pdf->MultiCell(40,10, "VP Sales" . "\r\n" . $approval_name3, 0, "T");
		$pdf->SetXY($margin_left+60,$y);
		$pdf->MultiCell(75,10, "", 1, "T", true);

		
		
		$y = $pdf->GetY();
		$pdf->SetXY($margin_left+1,$y);
		$pdf->SetFont("arialn", "B", 9);

		$pdf->MultiCell(60,10, "", 1, "L");
		$pdf->SetXY($margin_left+134,$y);
		$pdf->MultiCell(20,10, "", 0, "T");
		$pdf->SetFont("arialn", "", 9);
		$pdf->SetXY($margin_left+20,$y);
		$pdf->MultiCell(40,10, "VP Finance/CFO " . "\r\n" . $approval_name7, 0, "T");
		$pdf->SetXY($margin_left+60,$y);
		$pdf->MultiCell(75,10, "", 1, "T", true);


		$y = $pdf->GetY();
		$pdf->SetXY($margin_left+1,$y);
		

		$pdf->SetFont("arialn", "B", 9);

		$pdf->MultiCell(60,10, "", 1, "L");

		$pdf->MultiCell(20,10, "", 0, "T");
		$pdf->SetFont("arialn", "", 9);
		$pdf->SetXY($margin_left+20,$y);
		$pdf->MultiCell(40,10, "President" . "\r\n" . $approval_name2, 0, "T");
		$pdf->SetXY($margin_left+60,$y);
		$pdf->MultiCell(75,10, "", 1, "T", true);
	}
	else {
		$y = $pdf->GetY()+6;
		$pdf->SetXY($margin_left+1,$y);

		$pdf->SetFont("arialn", "B", 9);
		$pdf->MultiCell(60,10, "", 1, "L");
		$pdf->SetXY($margin_left+1,$y);
		$pdf->MultiCell(20,10, "Country", 0, "T");
		$pdf->SetFont("arialn", "", 9);
		$pdf->SetXY($margin_left+20,$y);
		$pdf->MultiCell(40,10, "Country Manager " . "\r\n" . $approval_name11, 0, "T");
		$pdf->SetXY($margin_left+60,$y);
		$pdf->MultiCell(74,10, "", 1, "T", true);

		$pdf->SetFont("arialn", "B", 9);
		$pdf->SetXY($margin_left+134,$y);
		$pdf->MultiCell(60,10, "", 1, "L");
		$pdf->SetXY($margin_left+134,$y);
		$pdf->MultiCell(20,10, "Brand", 0, "T");
		$pdf->SetFont("arialn", "", 9);
		$pdf->SetXY($margin_left+154, $y);
		$pdf->MultiCell(40,10, BRAND . " Retail Controller" . "\r\n" . $approval_name9, 0, "T");
		$pdf->SetXY($margin_left+194,$y);
		$pdf->MultiCell(75,10, "", 1, "T", true);


		$y = $pdf->GetY();
			$pdf->SetXY($margin_left+1,$y);
		
		//china projects
		if($project['order_client_address'] == 42
			or $project['order_client_address'] == 7) {
			

			$pdf->SetFont("arialn", "B", 9);
			$pdf->MultiCell(60,10, "", 1, "L");
			$pdf->SetXY($margin_left+1,$y);
			$pdf->MultiCell(20,10, "", 0, "T");
			$pdf->SetFont("arialn", "", 9);
			$pdf->SetXY($margin_left+20,$y);
			$pdf->MultiCell(40,10, "Head of Controlling " . "\r\n" . $approval_name16, 0, "T");
			$pdf->SetXY($margin_left+60,$y);
			$pdf->MultiCell(74,10, "", 1, "T", true);
		}
		else {
			

			$pdf->SetFont("arialn", "B", 9);
			$pdf->MultiCell(60,10, "", 1, "L");
			$pdf->SetXY($margin_left+1,$y);
			$pdf->MultiCell(20,10, "", 0, "T");
			$pdf->SetFont("arialn", "", 9);
			$pdf->SetXY($margin_left+20,$y);
			$pdf->MultiCell(40,10, "Finance/Service Center  " . "\r\n" . $approval_name12, 0, "T");
			$pdf->SetXY($margin_left+60,$y);
			$pdf->MultiCell(74,10, "", 1, "T", true);
		}

		$pdf->SetFont("arialn", "B", 9);
		$pdf->SetXY($margin_left+134,$y);
		$pdf->MultiCell(60,10, "", 1, "L");
		$pdf->SetXY($margin_left+134,$y);
		$pdf->MultiCell(20,10, "", 0, "T");
		$pdf->SetFont("arialn", "", 9);
		$pdf->SetXY($margin_left+154,$y);
		$pdf->MultiCell(40,10, "VP Sales" . "\r\n" . $approval_name3, 0, "T");
		$pdf->SetXY($margin_left+194,$y);
		$pdf->MultiCell(75,10, "", 1, "T", true);


		

		$y = $pdf->GetY();
		$pdf->SetXY($margin_left+1,$y);
		$pdf->SetXY($margin_left+1,$y);
		$pdf->SetFont("arialn", "B", 9);
		$pdf->MultiCell(60,10, "", 1, "L");
		$pdf->SetXY($margin_left+1,$y);
		$pdf->MultiCell(20,10, "", 0, "T");
		$pdf->SetFont("arialn", "", 9);
		$pdf->SetXY($margin_left+20, $y);
		$pdf->MultiCell(40,10, "Brand Manager " . "\r\n" . $approval_name13, 0, "T");
		$pdf->SetXY($margin_left+60,$y);
		$pdf->MultiCell(74,10, "", 1, "T", true);


		//china projects
		if($project['order_client_address'] == 42
			or $project['order_client_address'] == 7) {

			$y = $pdf->GetY();
			$pdf->SetXY($margin_left+1,$y);

			$pdf->SetFont("arialn", "B", 9);
			$pdf->MultiCell(60,10, "", 1, "L");
			$pdf->SetXY($margin_left+1,$y);
			$pdf->MultiCell(20,10, "", 0, "T");
			$pdf->SetFont("arialn", "", 9);
			$pdf->SetXY($margin_left+20,$y);
			$pdf->MultiCell(40,10, "Head of Retail Development " . "\r\n" . $approval_name14, 0, "T");
			$pdf->SetXY($margin_left+60,$y);
			$pdf->MultiCell(74,10, "", 1, "T", true);
		}
		
		
		


		//china projects
		if($project['order_client_address'] == 42
			or $project['order_client_address'] == 7) {
				$y = $pdf->GetY();
				$pdf->SetXY($margin_left+1,$y);
		
			$y = $pdf->GetY()-20;
		}

		$pdf->SetFont("arialn", "B", 9);
		$pdf->SetXY($margin_left+134,$y);
		$pdf->MultiCell(60,10, "", 1, "L");
		$pdf->SetXY($margin_left+134,$y);
		$pdf->MultiCell(20,10, "", 0, "T");
		$pdf->SetFont("arialn", "", 9);
		$pdf->SetXY($margin_left+154,$y);
		$pdf->MultiCell(40,10, "VP Finance/CFO " . "\r\n" . $approval_name7, 0, "T");
		$pdf->SetXY($margin_left+194,$y);
		$pdf->MultiCell(75,10, "", 1, "T", true);


		
		//china projects
		if($project['order_client_address'] != 42
			and $project['order_client_address'] != 7) {
				$y = $pdf->GetY();
				$pdf->SetXY($margin_left+1,$y);
				$pdf->SetFont("arialn", "B", 9);
				$pdf->MultiCell(60,10, "", 1, "L");
				$pdf->SetXY($margin_left+1,$y);
				$pdf->MultiCell(20,10, "", 0, "T");
				$pdf->SetFont("arialn", "", 9);
				$pdf->SetXY($margin_left+20, $y);
				$pdf->MultiCell(40,10, "", 0, "T");
				$pdf->SetXY($margin_left+60,$y);
				$pdf->MultiCell(74,10, "", 1, "T", true);
		}

		if($project['order_client_address'] == 42
			or $project['order_client_address'] == 7) {
				$y = $pdf->GetY();
				$pdf->SetXY($margin_left+1,$y);
		
			$y = $pdf->GetY();
		}
		
		$pdf->SetFont("arialn", "B", 9);
		$pdf->SetXY($margin_left+134,$y);
		$pdf->MultiCell(60,10, "", 1, "L");
		$pdf->SetXY($margin_left+134,$y);
		$pdf->MultiCell(20,10, "", 0, "T");
		$pdf->SetFont("arialn", "", 9);
		$pdf->SetXY($margin_left+154,$y);
		$pdf->MultiCell(40,10, "President" . "\r\n" . $approval_name2, 0, "T");
		$pdf->SetXY($margin_left+194,$y);
		$pdf->MultiCell(75,10, "", 1, "T", true);
	}

	


	


	//draw outer box
	$y = $pdf->GetY()-$margin_top - 8;
	$pix_h = $pdf->GetY()-$margin_top - 12;
	$pdf->SetXY($margin_left,$y_at_start);
	$pdf->Cell(270, $y, "", 1);


	
?>