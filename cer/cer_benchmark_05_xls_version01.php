<?php
/********************************************************************

    cer_benchmark_05_xls.php

    Generate Excel File: CER BenchmarkRental Investments

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-10-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-10-15
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_access_to_cer_benchmarks");

require_once "../include/xls/Writer.php";
require_once "include/get_functions.php";

/********************************************************************
    prepare Data Needed
*********************************************************************/

include("cer_benchmark_filter.php");

/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$filename = "benchmark_" . $benchmark_shortcut . "_investment_details_" . date("Ymd") . ".xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename);
$xls->setVersion(8);
$sheet = $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

//formats
$f_header =& $xls->addFormat();
$f_header->setSize(12);
$f_header->setAlign('left');
$f_header->setBold();

$f_title =& $xls->addFormat();
$f_title->setSize(10);
$f_title->setAlign('left');
$f_title->setBold();

$f_group_title =& $xls->addFormat();
$f_group_title->setSize(10);
$f_group_title->setAlign('left');
$f_group_title->setBold();

$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);

$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);


$f_unused =& $xls->addFormat();
$f_unused->setSize(8);
$f_unused->setBorder(1);
$f_unused->setPattern(2);
$f_unused->setBgColor('silver');

$f_border_left=& $xls->addFormat();
$f_border_left->setTop(1);
$f_border_left->setBottom(1);
$f_border_left->setLeft(1);
$f_border_left->setSize(10);
$f_border_left->setAlign('left');
$f_border_left->setBold();

$f_border_right=& $xls->addFormat();
$f_border_right->setTop(1);
$f_border_right->setBottom(1);
$f_border_right->setRight(1);


$f_border_middle=& $xls->addFormat();
$f_border_middle->setTop(1);
$f_border_middle->setBottom(1);

//captions
$c01 = 0;
$captions = array();
$captions[] = "Project Number";
$sheet->setColumn($c01, $c01, 10);
$c01++;

$captions[] = "Project State";
$sheet->setColumn($c01, $c01, 6);
$c01++;

$captions[] = "Country";
$sheet->setColumn($c01, $c01, 14);
$c01++;


$captions[] = "POS Location Address";
$sheet->setColumn($c01, $c01, 60);
$c01++;

$captions[] = "Product Line";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Product Line Subclass";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Opening Date";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Legal Type";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Project Kind";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Project Type";
$sheet->setColumn($c01, $c01, 12);
$c01++;

$captions[] = "Lease period in years";
$sheet->setColumn($c01, $c01, 6);
$c01++;

$captions[] = "Sales Surface";
$sheet->setColumn($c01, $c01, 6);
$c01++;

$captions[] = "Goodwill/Key Money";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Construction costs";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Construction costs per sqm";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Store fixturing";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Store fixturing per sqm";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Architectural costs";
$sheet->setColumn($c01, $c01, 8);

$captions[] = "Equipment IT";
$sheet->setColumn($c01, $c01, 8);

$captions[] = "Other Costs";
$sheet->setColumn($c01, $c01, 8);

$captions[] = "Deposit/Recoverable Keymoney";
$sheet->setColumn($c01, $c01, 8);

$captions[] = "Other non-capitalizable fees and taxes";
$sheet->setColumn($c01, $c01, 8);

$captions[] = "Total investment without Key Money";
$sheet->setColumn($c01, $c01, 8);

$captions[] = "Total investment without Key Money per sqm";
$sheet->setColumn($c01, $c01, 8);
$c01++;
$c01++;

//KL Approved
$captions[] = "";
$captions[] = "Construction costs KL approved";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Construction costs per sqm KL approved";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Store fixturing KL approved";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Store fixturing per sqm KL approved";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Architectural costs KL approved";
$sheet->setColumn($c01, $c01, 8);

$captions[] = "Equipment IT KL approved";
$sheet->setColumn($c01, $c01, 8);

$captions[] = "Other Costs KL approved";
$sheet->setColumn($c01, $c01, 8);

$captions[] = "Deposit KL approved";
$sheet->setColumn($c01, $c01, 8);

$captions[] = "Other non-capitalizable fees and taxes KL approved";
$sheet->setColumn($c01, $c01, 8);

$captions[] = "Total investment without Key Money KL approved";
$sheet->setColumn($c01, $c01, 8);

$captions[] = "Total investment without Key Money per sqm KL approved";
$sheet->setColumn($c01, $c01, 8);

/********************************************************************
    write all captions
*********************************************************************/
$sheet->write(0, 0, $benchmark_title, $f_header);

if($base_currency == "chf")
{
	$sheet->write(1, 0, "Investment Details in CHF (" . date("d.m.Y, H:m:s") . ")", $f_title);
}
else
{
	$sheet->write(1, 0, "Investment Details in LOC (" . date("d.m.Y, H:m:s") . ")", $f_title);
}

$sheet->setRow(4, 170);
$sheet->writeRow(4, 0, $captions, $f_caption);

/********************************************************************
    write all data
*********************************************************************/
$list_totals = array();
$num_of_projects = 0;

$row_index = 5;
$c01 = 0;

foreach($posorders as $key=>$posorder)
{
	if($posorder["pipeline"] == 1)
	{
		$sql = "select * " . 
			   "from posaddressespipeline " . 
			   "left join posorderspipeline on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " . 
			   "left join orders on order_id = project_order " .
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "left join project_costs on project_cost_order = posorder_order " .
			   "where posorder_type = 1 and posorder_id = " . $posorder["id"];
	}
	elseif($posorder["pipeline"] == 2)
	{
		$sql = "select * " . 
			   "from posaddresses " . 
			   "left join posorderspipeline on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " . 
			   "left join orders on order_id = project_order " .
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "left join project_costs on project_cost_order = posorder_order " .
			   "where posorder_type = 1 and posorder_id = " . $posorder["id"];
	}
	else
	{
		$sql = "select * " . 
			   "from posaddresses " . 
			   "left join posorders on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " . 
			   "left join orders on order_id = project_order " .
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "left join project_costs on project_cost_order = posorder_order " .
			   "where posorder_type = 1 and posorder_id = " . $posorder["id"];
	}	

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$num_of_projects++;

		$exchange_rate = 1;
		$factor = 1;


		$cer_basicdata = get_cer_basicdata($row["project_id"]);

		if(count($cer_basicdata) > 0)
		{
			$exchange_rate = $cer_basicdata["cer_basicdata_exchangerate"];
			$factor = $cer_basicdata["cer_basicdata_factor"];
			if(!$factor) {$factor = 1;}
		}

		if($base_currency == "chf")
		{
		}
		else
		{
			$exchange_rate = 1;
			$factor = 1;
		}

		$sheet->write($row_index, $c01, $row["posorder_ordernumber"], $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["order_actual_order_state_code"], $f_normal);
		$c01++;


		$sheet->write($row_index, $c01, $row["country_name"], $f_normal);
		$c01++;
		$tmp = $row["posaddress_place"] . ", " . $row["posaddress_name"] . ", " . $row["posaddress_address"];
		$sheet->write($row_index, $c01, $tmp, $f_normal);
		$c01++;
		
		
		$sheet->write($row_index, $c01, $row["product_line_name"], $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["productline_subclass_name"], $f_normal);
		$c01++;
		
		$sheet->write($row_index, $c01, to_system_date($row["posaddress_store_openingdate"]), $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["project_costtype_text"], $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["projectkind_name"], $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["postype_name"], $f_normal);
		$c01++;


		//get the lease duration in years
		$starting_date = 0;
		$ending_date = 0;
		$extension_option = 0;
		$exit_option = 0;

		if($posorder["pipeline"] == 1)
		{
			$sql_s = "select poslease_startdate, poslease_enddate, poslease_extensionoption, poslease_exitoption " . 
					 "from posleasespipeline " . 
					 "where poslease_posaddress = " . $row["posaddress_id"];
		}
		else
		{
			$sql_s = "select poslease_startdate, poslease_enddate, poslease_extensionoption, poslease_exitoption " . 
					 "from posleases " . 
					 "where poslease_posaddress = " . $row["posaddress_id"];
		}
		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		while($row_s = mysql_fetch_assoc($res_s))
		{
			if($starting_date == 0)
			{
				$starting_date = $row_s["poslease_startdate"];
			}
			elseif($row_s["poslease_startdate"] < $starting_date)
			{
				$starting_date = $row_s["poslease_startdate"];
			}

			if($ending_date == 0)
			{
				$ending_date = $row_s["poslease_enddate"];
			}
			elseif($row_s["poslease_enddate"] > $ending_date)
			{
				$ending_date = $row_s["poslease_enddate"];
			}
		}
		if($starting_date == 0 and $ending_date == 0)
		{
			$lease_period = 0;
		}
		else
		{
			$start = strtotime($starting_date) ;
			$end = strtotime($ending_date) ;

			$lease_period = $end - $start;
			$lease_period = $lease_period / 86400;
			$lease_period = round($lease_period/365, 2);

		}

		$sheet->write($row_index, $c01, $lease_period, $f_number);
		$c01++;

		if(array_key_exists("lease_period", $list_totals))
		{
			$list_totals["lease_period"] = $list_totals["lease_period"] + $lease_period;
		}
		else
		{
			$list_totals["lease_period"] = $lease_period;
		}


		//get space in square meters
		$gross_space = $row["project_cost_gross_sqms"];
		$total_space = $row["project_cost_totalsqms"];
		$retail_area = $row["project_cost_sqms"];
		$back_office = $row["project_cost_backofficesqms"];

		
		if(array_key_exists("gross_space", $list_totals))
		{
			$list_totals["gross_space"] = $list_totals["gross_space"] + $gross_space;
		}
		else
		{
			$list_totals["gross_space"] = $gross_space;
		}

		
		if(array_key_exists("total_space", $list_totals))
		{
			$list_totals["total_space"] = $list_totals["total_space"] + $total_space;
		}
		else
		{
			$list_totals["total_space"] = $total_space;
		}

		if(array_key_exists("retail_area", $list_totals))
		{
			$list_totals["retail_area"] = $list_totals["retail_area"] + $retail_area;
		}
		else
		{
			$list_totals["retail_area"] = $retail_area;
		}

		if(array_key_exists("back_office", $list_totals))
		{
			$list_totals["back_office"] = $list_totals["back_office"] + $back_office;
		}
		else
		{
			$list_totals["back_office"] = $back_office;
		}

		$sheet->write($row_index, $c01, number_format($retail_area, 2, ".", ""), $f_number);
		$c01++;

		
		//get intagibles
		$intagibles = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and (cer_investment_type = 15 or cer_investment_type = 17)";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$intagibles = $row_s["total"] * $exchange_rate / $factor;
		}
		$sheet->write($row_index, $c01, $intagibles, $f_number);
		$c01++;

		if(array_key_exists("intagibles", $list_totals))
		{
			$list_totals["intagibles"] = $list_totals["intagibles"] + $intagibles;
		}
		else
		{
			$list_totals["intagibles"] = $intagibles;
		}

		//INVESTMENTS
		$total_cost = 0;
		//get construction cost
		$construction = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type = 1";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$construction = $row_s["total"] * $exchange_rate / $factor;
		}
		$sheet->write($row_index, $c01, $construction, $f_number);
		$c01++;

		if(array_key_exists("construction", $list_totals))
		{
			$list_totals["construction"] = $list_totals["construction"] + $construction;
		}
		else
		{
			$list_totals["construction"] = $construction;
		}
		$total_cost = $total_cost + $construction;

		$construction_per_sqm = 0;
		if($retail_area > 0)
		{
			$construction_per_sqm = round($construction / $retail_area, 2);
		}
		$sheet->write($row_index, $c01, number_format($construction_per_sqm, 2, ".", ""), $f_number);
		$c01++;

		if(array_key_exists("construction_per_sqm", $list_totals))
		{
			$list_totals["construction_per_sqm"] = $list_totals["construction_per_sqm"] + $construction_per_sqm;
		}
		else
		{
			$list_totals["construction_per_sqm"] = $construction_per_sqm;
		}


		//get fixturing cost
		$fixturing = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type = 3";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$fixturing = $row_s["total"] * $exchange_rate / $factor;
		}
		$sheet->write($row_index, $c01, $fixturing, $f_number);
		$c01++;

		if(array_key_exists("fixturing", $list_totals))
		{
			$list_totals["fixturing"] = $list_totals["fixturing"] + $fixturing;
		}
		else
		{
			$list_totals["fixturing"] = $fixturing;
		}

		$total_cost = $total_cost + $fixturing;

		$fixturing_per_sqm = 0;
		if($retail_area > 0)
		{
			$fixturing_per_sqm = round($fixturing / $retail_area, 2);
		}
		$sheet->write($row_index, $c01, number_format($fixturing_per_sqm, 2, ".", ""), $f_number);
		$c01++;

		if(array_key_exists("fixturing_per_sqm", $list_totals))
		{
			$list_totals["fixturing_per_sqm"] = $list_totals["fixturing_per_sqm"] + $fixturing_per_sqm;
		}
		else
		{
			$list_totals["fixturing_per_sqm"] = $fixturing_per_sqm;
		}


		//get architectural cost
		$architectural = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type = 5";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$architectural = $row_s["total"] * $exchange_rate / $factor;
		}
		$sheet->write($row_index, $c01, $architectural, $f_number);
		$c01++;

		if(array_key_exists("architectural", $list_totals))
		{
			$list_totals["architectural"] = $list_totals["architectural"] + $architectural;
		}
		else
		{
			$list_totals["architectural"] = $architectural;
		}
		$total_cost = $total_cost + $architectural;


		//get equipment cost
		$architectural = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type = 7";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$equipment = $row_s["total"] * $exchange_rate / $factor;
		}
		$sheet->write($row_index, $c01, $equipment, $f_number);
		$c01++;

		if(array_key_exists("equipment", $list_totals))
		{
			$list_totals["equipment"] = $list_totals["equipment"] + $equipment;
		}
		else
		{
			$list_totals["equipment"] = $equipment;
		}
		$total_cost = $total_cost + $equipment;

		//get other cost
		$other = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type IN (11, 18, 19)";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$other = $row_s["total"] * $exchange_rate / $factor;
		}
		$sheet->write($row_index, $c01, $other, $f_number);
		$c01++;

		if(array_key_exists("other", $list_totals))
		{
			$list_totals["other"] = $list_totals["other"] + $other;
		}
		else
		{
			$list_totals["other"] = $other;
		}
		$total_cost = $total_cost + $other;

		//get deposit cost
		$deposit = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type = 9";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$deposit = ($row_s["total"] + $cer_basicdata["cer_basicdata_recoverable_keymoney"]) * $exchange_rate / $factor;
		}
		$sheet->write($row_index, $c01, $deposit, $f_number);
		$c01++;

		if(array_key_exists("deposit", $list_totals))
		{
			$list_totals["deposit"] = $list_totals["deposit"] + $deposit;
		}
		else
		{
			$list_totals["deposit"] = $deposit;
		}
		$total_cost = $total_cost + $deposit;

		//get noncapitalized cost
		$noncapitalized = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type = 13";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$noncapitalized = $row_s["total"] * $exchange_rate / $factor;
		}
		$sheet->write($row_index, $c01, $noncapitalized, $f_number);
		$c01++;

		if(array_key_exists("noncapitalized", $list_totals))
		{
			$list_totals["noncapitalized"] = $list_totals["noncapitalized"] + $noncapitalized;
		}
		else
		{
			$list_totals["noncapitalized"] = $noncapitalized;
		}
		$total_cost = $total_cost + $noncapitalized;

		$sheet->write($row_index, $c01, $total_cost, $f_number);
		$c01++;

		if(array_key_exists("total_cost", $list_totals))
		{
			$list_totals["total_cost"] = $list_totals["total_cost"] + $total_cost;
		}
		else
		{
			$list_totals["total_cost"] = $total_cost;
		}

		$total_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$total_cost_per_sqm = round($total_cost / $retail_area, 2);
		}
		$sheet->write($row_index, $c01, number_format($total_cost_per_sqm, 2, ".", ""), $f_number);
		$c01++;

		if(array_key_exists("total_cost_per_sqm", $list_totals))
		{
			$list_totals["total_cost_per_sqm"] = $list_totals["total_cost_per_sqm"] + $total_cost_per_sqm;
		}
		else
		{
			$list_totals["total_cost_per_sqm"] = $total_cost_per_sqm;
		}


		//KL approved investments
		$c01++;
		$total_cost = 0;
		//get construction cost
		$construction = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc_approved + cer_investment_amount_additional_cer_loc_approved) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type = 1";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$construction = $row_s["total"] * $exchange_rate / $factor;
		}
		$sheet->write($row_index, $c01, $construction, $f_number);
		$c01++;

		if(array_key_exists("construction_approved", $list_totals))
		{
			$list_totals["construction_approved"] = $list_totals["construction_approved"] + $construction;
		}
		else
		{
			$list_totals["construction_approved"] = $construction;
		}
		$total_cost = $total_cost + $construction;

		$construction_per_sqm = 0;
		if($retail_area > 0)
		{
			$construction_per_sqm = round($construction / $retail_area, 2);
		}
		$sheet->write($row_index, $c01, number_format($construction_per_sqm, 2, ".", ""), $f_number);
		$c01++;

		if(array_key_exists("construction_per_sqm_approved", $list_totals))
		{
			$list_totals["construction_per_sqm_approved"] = $list_totals["construction_per_sqm_approved"] + $construction_per_sqm;
		}
		else
		{
			$list_totals["construction_per_sqm_approved"] = $construction_per_sqm;
		}


		//get fixturing cost
		$fixturing = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc_approved + cer_investment_amount_additional_cer_loc_approved) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type = 3";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$fixturing = $row_s["total"] * $exchange_rate / $factor;
		}
		$sheet->write($row_index, $c01, $fixturing, $f_number);
		$c01++;

		if(array_key_exists("fixturing_approved", $list_totals))
		{
			$list_totals["fixturing_approved"] = $list_totals["fixturing_approved"] + $fixturing;
		}
		else
		{
			$list_totals["fixturing_approved"] = $fixturing;
		}

		$total_cost = $total_cost + $fixturing;

		$fixturing_per_sqm = 0;
		if($retail_area > 0)
		{
			$fixturing_per_sqm = round($fixturing / $retail_area, 2);
		}
		$sheet->write($row_index, $c01, number_format($fixturing_per_sqm, 2, ".", ""), $f_number);
		$c01++;

		if(array_key_exists("fixturing_per_sqm_approved", $list_totals))
		{
			$list_totals["fixturing_per_sqm_approved"] = $list_totals["fixturing_per_sqm_approved"] + $fixturing_per_sqm;
		}
		else
		{
			$list_totals["fixturing_per_sqm_approved"] = $fixturing_per_sqm;
		}


		//get architectural cost
		$architectural = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc_approved + cer_investment_amount_additional_cer_loc_approved) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type = 5";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$architectural = $row_s["total"] * $exchange_rate / $factor;
		}
		$sheet->write($row_index, $c01, $architectural, $f_number);
		$c01++;

		if(array_key_exists("architectural_approved", $list_totals))
		{
			$list_totals["architectural_approved"] = $list_totals["architectural_approved"] + $architectural;
		}
		else
		{
			$list_totals["architectural_approved"] = $architectural;
		}
		$total_cost = $total_cost + $architectural;


		//get equipment cost
		$architectural = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc_approved + cer_investment_amount_additional_cer_loc_approved) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type = 7";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$equipment = $row_s["total"] * $exchange_rate / $factor;
		}
		$sheet->write($row_index, $c01, $equipment, $f_number);
		$c01++;

		if(array_key_exists("equipment_approved", $list_totals))
		{
			$list_totals["equipment_approved"] = $list_totals["equipment_approved"] + $equipment;
		}
		else
		{
			$list_totals["equipment_approved"] = $equipment;
		}
		$total_cost = $total_cost + $equipment;

		//get other cost
		$other = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc_approved + cer_investment_amount_additional_cer_loc_approved) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type in (11, 18, 19)";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$other = $row_s["total"] * $exchange_rate / $factor;
		}
		$sheet->write($row_index, $c01, $other, $f_number);
		$c01++;

		if(array_key_exists("other_approved", $list_totals))
		{
			$list_totals["other_approved"] = $list_totals["other_approved"] + $other;
		}
		else
		{
			$list_totals["other_approved"] = $other;
		}
		$total_cost = $total_cost + $other;

		//get deposit cost
		$deposit = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc_approved + cer_investment_amount_additional_cer_loc_approved) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type = 9";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$deposit = $row_s["total"] * $exchange_rate / $factor;
		}
		$sheet->write($row_index, $c01, $deposit, $f_number);
		$c01++;

		if(array_key_exists("deposit_approved", $list_totals))
		{
			$list_totals["deposit_approved"] = $list_totals["deposit_approved"] + $deposit;
		}
		else
		{
			$list_totals["deposit_approved"] = $deposit;
		}
		$total_cost = $total_cost + $deposit;

		//get noncapitalized cost
		$noncapitalized = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc_approved + cer_investment_amount_additional_cer_loc_approved) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_type = 13";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$noncapitalized = $row_s["total"] * $exchange_rate / $factor;
		}
		$sheet->write($row_index, $c01, $noncapitalized, $f_number);
		$c01++;

		if(array_key_exists("noncapitalized_approved", $list_totals))
		{
			$list_totals["noncapitalized_approved"] = $list_totals["noncapitalized_approved"] + $noncapitalized;
		}
		else
		{
			$list_totals["noncapitalized_approved"] = $noncapitalized;
		}
		$total_cost = $total_cost + $noncapitalized;

		$sheet->write($row_index, $c01, $total_cost, $f_number);
		$c01++;

		if(array_key_exists("total_cost_approved", $list_totals))
		{
			$list_totals["total_cost_approved"] = $list_totals["total_cost_approved"] + $total_cost;
		}
		else
		{
			$list_totals["total_cost_approved"] = $total_cost;
		}

		$total_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$total_cost_per_sqm = round($total_cost / $retail_area, 2);
		}
		$sheet->write($row_index, $c01, number_format($total_cost_per_sqm, 2, ".", ""), $f_number);
		$c01++;

		if(array_key_exists("total_cost_per_sqm_approved", $list_totals))
		{
			$list_totals["total_cost_per_sqm_approved"] = $list_totals["total_cost_per_sqm_approved"] + $total_cost_per_sqm;
		}
		else
		{
			$list_totals["total_cost_per_sqm_approved"] = $total_cost_per_sqm;
		}
		
		
		
		$c01 = 0;
		$row_index++;

	}
}


$listtotal_rowindex = $row_index;
//list totals for investments
if(count($posorders) > 0)
{
	$c01 = 9;
	$row_index++;
	$sheet->write($row_index, $c01,"Sum", $f_normal);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$sheet->write($row_index, $c01,number_format($list_totals["retail_area"], 2, ".", ""), $f_number);
	$c01++;

	$sheet->write($row_index, $c01,$list_totals["intagibles"], $f_number);
	$c01++;

	$sheet->write($row_index, $c01,$list_totals["construction"], $f_number);
	$c01++;
	$sheet->write($row_index, $c01,number_format($list_totals["construction_per_sqm"], 2, ".", ""), $f_number);
	$c01++;

	$sheet->write($row_index, $c01,$list_totals["fixturing"], $f_number);
	$c01++;
	$sheet->write($row_index, $c01,number_format($list_totals["fixturing_per_sqm"], 2, ".", ""), $f_number);
	$c01++;

	$sheet->write($row_index, $c01,$list_totals["architectural"], $f_number);
	$c01++;

	$sheet->write($row_index, $c01,$list_totals["equipment"], $f_number);
	$c01++;

	$sheet->write($row_index, $c01,$list_totals["other"], $f_number);
	$c01++;

	$sheet->write($row_index, $c01,$list_totals["deposit"], $f_number);
	$c01++;
	$sheet->write($row_index, $c01,$list_totals["noncapitalized"], $f_number);
	$c01++;
	$sheet->write($row_index, $c01,$list_totals["total_cost"], $f_number);
	$c01++;

	$sheet->write($row_index, $c01,$list_totals["total_cost_per_sqm"], $f_number);
	$c01++;



	$c01 = 9;
	$row_index++;
	$sheet->write($row_index, $c01,"Average", $f_normal);
	$c01++;


	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["lease_period"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;


	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["retail_area"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["intagibles"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["construction"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["construction_per_sqm"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;


	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["fixturing"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["fixturing_per_sqm"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["architectural"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["equipment"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["other"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["deposit"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["noncapitalized"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["total_cost"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["total_cost_per_sqm"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;



	$c01 = 9;
	$row_index++;
	$row_index++;
	$sheet->write($row_index, $c01,"Average per sqm", $f_normal);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["intagibles"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["construction"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["fixturing"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["architectural"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["equipment"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;


	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["other"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["deposit"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["noncapitalized"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["total_cost"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;
}



$row_index = $listtotal_rowindex;
//list totals for investments
if(count($posorders) > 0)
{
	$c01 = 25;
	$row_index++;
	
	$sheet->write($row_index, $c01,$list_totals["construction_approved"], $f_number);
	$c01++;
	$sheet->write($row_index, $c01,number_format($list_totals["construction_per_sqm_approved"], 2, ".", ""), $f_number);
	$c01++;

	$sheet->write($row_index, $c01,$list_totals["fixturing_approved"], $f_number);
	$c01++;
	$sheet->write($row_index, $c01,number_format($list_totals["fixturing_per_sqm_approved"], 2, ".", ""), $f_number);
	$c01++;

	$sheet->write($row_index, $c01,$list_totals["architectural_approved"], $f_number);
	$c01++;

	$sheet->write($row_index, $c01,$list_totals["equipment_approved"], $f_number);
	$c01++;

	$sheet->write($row_index, $c01,$list_totals["other_approved"], $f_number);
	$c01++;

	$sheet->write($row_index, $c01,$list_totals["deposit_approved"], $f_number);
	$c01++;
	$sheet->write($row_index, $c01,$list_totals["noncapitalized_approved"], $f_number);
	$c01++;
	$sheet->write($row_index, $c01,$list_totals["total_cost_approved"], $f_number);
	$c01++;

	$sheet->write($row_index, $c01,$list_totals["total_cost_per_sqm_approved"], $f_number);
	$c01++;



	$c01 = 25;
	$row_index++;
	

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["construction_approved"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["construction_per_sqm_approved"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;


	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["fixturing_approved"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["fixturing_per_sqm_approved"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["architectural_approved"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["equipment_approved"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["other_approved"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["deposit_approved"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["noncapitalized_approved"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["total_cost_approved"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["total_cost_per_sqm_approved"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;



	$c01 = 25;
	$row_index++;
	$row_index++;
	

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["construction_approved"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["fixturing_approved"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["architectural_approved"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["equipment_approved"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;


	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["other_approved"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["deposit_approved"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["noncapitalized_approved"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["total_cost_approved"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;
}
$xls->close(); 

?>