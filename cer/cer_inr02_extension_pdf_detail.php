<?php
/********************************************************************

    cer_inr02_extension_pdf_detail.php

    Print PDF for CER Business Plan Extension (Form IN-R02)

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2012-04-29
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2012-04-29
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.
*********************************************************************/
include("include/in_project_data.php");
//get financial data
include("include/in_financial_data.php");	


//set pdf parameters

$margin_top = 5;
$margin_left = 12;
$y = $margin_top;
$standard_y = 3.5;

$pdf->SetTitle("Retail Busines Plan Extension Form IN-R02");
$pdf->SetAuthor(BRAND . " Retailnet");
$pdf->SetDisplayMode(110);
$pdf->AddPage("L", "A4");
$pdf->SetAutoPageBreak(false, 0);




//get head counts
$number_of_sales_staff = array();
$ftes = array();
$fte_missing_firstyear = array();
$fte_missing_lastyear = array();
$total_working_hours_per_week_all_staff = array();


if(param("pid") > 0 )
{
	$sql = "select * " .
		   "from cer_salaries where cer_salary_cer_version = " . $cer_version . "  and cer_salary_project = " . param("pid");
}
else
{
	$sql = "select * " .
		   "from cer_draft_salaries where cer_salary_draft_id = " . param("did");
}

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	if(array_key_exists($row["cer_salary_year_starting"], $number_of_sales_staff))
	{
		$number_of_sales_staff[$row["cer_salary_year_starting"]] = $number_of_sales_staff[$row["cer_salary_year_starting"]] + 1;
	}
	else
	{
		$number_of_sales_staff[$row["cer_salary_year_starting"]] = 1;
	}


	if(array_key_exists($row["cer_salary_year_starting"], $ftes))
	{
		$ftes[$row["cer_salary_year_starting"]] = $ftes[$row["cer_salary_year_starting"]] + $row["cer_salary_headcount_percent"];
	}
	else
	{
		$ftes[$row["cer_salary_year_starting"]] = $row["cer_salary_headcount_percent"];
	}

	/*
	//missing fte frist year
	$missing_months = $row["cer_salary_month_starting"] -1;

	if(array_key_exists($row["cer_salary_year_starting"], $fte_missing_firstyear))
	{
		$fte_missing_firstyear[$row["cer_salary_year_starting"]] = $fte_missing_firstyear[$row["cer_salary_year_starting"]] + (($missing_months/12)*$row["cer_salary_headcount_percent"]);
	}
	else
	{
		$fte_missing_firstyear[$row["cer_salary_year_starting"]] = ($missing_months/12)*$row["cer_salary_headcount_percent"];
	}

	//missing fte last year
	$missing_months = 12 - $cer_basicdata["cer_basicdata_lastmonth"];

	if(array_key_exists($cer_basicdata["cer_basicdata_lastyear"], $fte_missing_lastyear))
	{
		$fte_missing_lastyear[$cer_basicdata["cer_basicdata_lastyear"]] = $fte_missing_lastyear[$cer_basicdata["cer_basicdata_lastyear"]] + (($missing_months/12)*$row["cer_salary_headcount_percent"]);
	}
	else
	{
		$fte_missing_lastyear[$cer_basicdata["cer_basicdata_lastyear"]] = ($missing_months/12)*$row["cer_salary_headcount_percent"];
	}
	*/
}


//add data to all years of the busines plan
$tmp = 0;
$tmp1 = 0;
foreach($years as $key=>$year)
{
	if(array_key_exists($year, $number_of_sales_staff))
	{
		if(array_key_exists(($year - 1), $number_of_sales_staff))
		{
			$number_of_sales_staff[$year] = $number_of_sales_staff[$year] + $number_of_sales_staff[$year-1];
			$ftes[$year] = $ftes[$year] + $ftes[$year-1];
		}

		$tmp = $number_of_sales_staff[$year];
		$tmp1 = $ftes[$year];
	}
	elseif(array_key_exists(($year - 1), $number_of_sales_staff))
	{
		if(array_key_exists($year, $number_of_sales_staff))
		{
			$number_of_sales_staff[$year] = $number_of_sales_staff[$year] + $number_of_sales_staff[$year-1];
			$ftes[$year] = $ftes[$year] + $ftes[$year-1];
		}
		else
		{
			$number_of_sales_staff[$year] = $number_of_sales_staff[$year-1];
			$ftes[$year] = $ftes[$year-1];
		}
	}
	else
	{
		$number_of_sales_staff[$year] = $tmp;
		$ftes[$year] = $tmp1;
	}
}

//make fte correction for the first year an employee is hired
foreach($ftes as $year=>$number)
{
	if(array_key_exists($year, $fte_missing_firstyear))
	{
		$ftes[$year] = $ftes[$year] - $fte_missing_firstyear[$year];
	}
}

//make correction for the last year an employee is hired
foreach($ftes as $year=>$number)
{
	if($year == $cer_basicdata["cer_basicdata_lastyear"] and array_key_exists($year, $fte_missing_lastyear))
	{
		$ftes[$year] = $ftes[$year] - $fte_missing_lastyear[$year];
	}
}


//calculate total working hour of all staff
foreach($ftes as $year=>$number)
{
	if(array_key_exists($year, $cer_revenue_total_workinghours_per_week))
	{
		$ftes[$year] = $ftes[$year] / 100;
		$total_working_hours_per_week_all_staff[$year] = $cer_revenue_total_workinghours_per_week[$year] * $ftes[$year];
	}
}


$years_page_row1 = array();
$years_page_row2 = array();
$j= 1;
foreach($years as $key=>$year)
{
	if($j < 11)
	{
		$years_page_row1[] = $years[$key];
	}
	$j++;
}


//page top data
include "include/inR02_extension_page_top_data.php";




$years_page_row = $years_page_row1;
include("include/in_finacial_extended_data.php");



$y = $y + 5;

include "include/inR02_extended_data.php";	





if(count($years) > 10)
{
	
	$pdf->AddPage("L", "A4");
	//page top data
	include "include/inR02_extension_page_top_data.php";

	
	$j= 1;
	foreach($years as $key=>$year)
	{
		if($j > 10 and $j < 21)
		{
			$years_page_row2[] = $years[$key];
		}
		$j++;
	}


	$years_page_row = $years_page_row2;

	include("include/in_finacial_extended_data.php");	
	
	

	$y = $y + 8;

	include "include/inR02_extended_data.php";	
	

}



?>