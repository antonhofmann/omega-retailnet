<?php
/********************************************************************

    cer_draft_file.php

    Creation and mutation of Business Plan Drafts file records.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2011-03-20
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2011-03-20
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require "include/get_draft_functions.php";

check_access("has_access_to_cer_drafts");

$title = "";

if(param('did')) {
	$basicdata = get_draft_basicdata(param("did"));
	$title = $basicdata["cer_basicdata_title"];
}


$form = new Form("cer_draft_files", "cer_draft_files");

$form->add_section("File Description");
$form->add_hidden("did", param("did"));
$form->add_hidden("cer_draft_file_draft", param("did"));
$form->add_edit("cer_draft_file_title", "Title*", NOTNULL);
$form->add_multiline("cer_draft_file_description", "Description", 4);

$form->add_section("File");
$file_code = make_valid_filename("draft_". param("did"));
$form->add_upload("cer_draft_file_path", "File", "/files/cer_drafts/$file_code");

$form->add_section();

$form->add_button(FORM_BUTTON_SAVE, "Save");

if(id() > 0 )
{
	$form->add_button("delete", "Delete");
}
$form->add_button("back", "Back");

$form->populate();
$form->process();

if($form->button("back"))
{
	$link = "cer_draft_files.php?did=" . param("did");
	redirect($link);
}
elseif($form->button("delete"))
{
	$sql = "delete from cer_draft_files where cer_draft_file_id = " . id();
    $res = mysql_query($sql) or dberror($sql);
	unlink($_SERVER["DOCUMENT_ROOT"] . $form->value("cer_draft_file_path"));
	$link = "cer_draft_files.php?did=" . param("did");
	redirect($link);
}

$page = new Page("cer_drafts");
require "include/draft_page_actions.php";
$page->header();

$page->title(id() ? $title . ": Files" : $title . " Files: Add new File");

require_once("include/tabs_draft.php");


$form->render();

require "include/draft_footer_scripts.php";
$page->footer();

?>
