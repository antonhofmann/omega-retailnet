<?php
/********************************************************************

    cer_drafts.php

    Business Plan Drafts

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-02-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-02-16
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
//require "include/get_functions.php";
//require "include/get_project.php";

check_access("has_access_to_cer_drafts");

set_referer("cer_draft.php");
set_referer("cer_draft_create_from_project.php");

/********************************************************************
    prepare all data needed
*********************************************************************/

//create a copy of an existing query
if(param("copy_id"))
{

	$id = 0;
	
	$sql = 'Select * from cer_drafts where cer_basicdata_id =' . param('copy_id');
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{

		$fields = '';
		$values = '';
		foreach($row as $field_name=>$value)
		{
			if($field_name == 'cer_basicdata_title') {
				$fields .= $field_name . ',';
				$values .= dbquote($value . " copy ") . ',';
			}
			elseif($field_name == 'cer_basicdata_user_id') {
				$fields .= $field_name . ',';
				$values .= user_id() . ',';
			}
			elseif($field_name != 'cer_basicdata_id') {
				$fields .= $field_name . ',';
				$values .= dbquote($value) . ',';
			}
		}

		$fields = substr($fields, 0, strlen($fields)-1);
		$values = substr($values, 0, strlen($values)-1);

		$sql_i = 'insert into cer_drafts (' . $fields . ') VALUES (' . $values . ')';
		$result = mysql_query($sql_i) or dberror($sql_i);

		$id = mysql_insert_id();

	}

	if($id > 0)
	{
		//expenses
		$sql = 'Select * from cer_draft_expenses where cer_expense_draft_id =' . param('copy_id');
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				if($field_name == 'cer_expense_draft_id') {
					$fields .= $field_name . ',';
					$values .= $id . ',';
				}
				elseif($field_name != 'cer_expense_id') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_draft_expenses (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//fixed rents
		$sql = 'Select * from cer_draft_fixed_rents ' . 
			   'where cer_fixed_rent_draft_id =' . param('copy_id');
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				
				if($field_name == 'cer_fixed_rent_draft_id') {
					$fields .= $field_name . ',';
					$values .= $id . ',';
				}
				elseif($field_name != 'cer_fixed_rent_id') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}

				
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_draft_fixed_rents (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//trunoverbased rental percentages
		$sql = 'Select * from cer_draft_rent_percent_from_sales where cer_rent_percent_from_sale_draft_id =' . param('copy_id');
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				if($field_name == 'cer_rent_percent_from_sale_draft_id') {
					$fields .= $field_name . ',';
					$values .= $id . ',';
				}
				elseif($field_name != 'cer_rent_percent_from_sale_id') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_draft_rent_percent_from_sales (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		}
		

		//investments
		$sql = 'Select * from cer_draft_investments where cer_investment_draft_id =' . param('copy_id');
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				if($field_name == 'cer_investment_draft_id') {
					$fields .= $field_name . ',';
					$values .= $id . ',';
				}
				elseif($field_name != 'cer_investment_id') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_draft_investments (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//payments
		$sql = 'Select * from cer_draft_paymentterms where cer_paymentterm_draft_id =' . param('copy_id');
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				if($field_name == 'cer_paymentterm_draft_id') {
					$fields .= $field_name . ',';
					$values .= $id . ',';
				}
				elseif($field_name != 'cer_paymentterm_id') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_draft_paymentterms (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//revenues
		$sql = 'Select * from cer_draft_revenues where cer_revenue_draft_id =' . param('copy_id');
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				if($field_name == 'cer_revenue_draft_id') {
					$fields .= $field_name . ',';
					$values .= $id . ',';
				}
				elseif($field_name != 'cer_revenue_id') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_draft_revenues (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//salaries
		$sql = 'Select * from cer_draft_salaries where cer_salary_draft_id =' . param('copy_id');
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				if($field_name == 'cer_salary_draft_id') {
					$fields .= $field_name . ',';
					$values .= $id . ',';
				}
				elseif($field_name != 'cer_salary_id') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_draft_salaries (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//stocks
		$sql = 'Select * from cer_draft_stocks where cer_stock_draft_id =' . param('copy_id');
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				if($field_name == 'cer_stock_draft_id') {
					$fields .= $field_name . ',';
					$values .= $id . ',';
				}
				elseif($field_name != 'cer_stock_id') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_draft_stocks (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}

	}
}

if(has_access("has_access_to_drafts_of_his_country")) {
	
	$sql = "select * from cer_drafts";
	$list_filter = 'cer_basicdata_user_id = ' . user_id();

	
	//get the team members
	$user_list = '';
	$sql = 'select address_id from addresses ' .
		   'left join users on user_address = address_id ' . 
		   'where user_id = ' . user_id();

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$sql = 'select user_id from users ' .
			   'where user_address = ' . $row["address_id"] . 
			   ' and user_active = 1';

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$user_list .= $row['user_id'] . ',';
		}

		$user_list = substr($user_list, 0, strlen($user_list)-1);

		$sql = "select cer_basicdata_id, cer_basicdata_title, concat(user_name, ' ', user_firstname) as uname,  " .
		       "cer_drafts.date_created as cdate, cer_drafts.date_modified as mdate, country_name " . 
		       "from cer_drafts " . 
			   "left join countries on country_id = cer_basicdata_country " . 
		       "left join users on user_id = cer_basicdata_user_id";
		$list_filter = 'cer_basicdata_user_id IN (' . $user_list . ')';
	}
	
	
}
else
{
	$sql = "select cer_basicdata_id, cer_basicdata_title, concat(user_name, ' ', user_firstname) as uname,  " .
		   "cer_drafts.date_created as cdate, cer_drafts.date_modified as mdate, country_name " . 
		   "from cer_drafts " . 
		   "left join countries on country_id = cer_basicdata_country " . 
		   "left join users on user_id = cer_basicdata_user_id";
	
	if(param("selected_country") > 0)
	{
		$list_filter = " cer_basicdata_country = " . param("selected_country");
	}
	else
	{
		//$list_filter = ' cer_basicdata_user_id = ' . user_id();
		$list_filter = '';
	}
}


//get modification dates
$copy_links = array();
$mdates = array();
if($list_filter) {
	$sql_m = $sql . ' where ' . $list_filter;
}
else
{
	$sql_m = $sql;
}

$res = mysql_query($sql_m) or dberror($sql_m);
while ($row = mysql_fetch_assoc($res))
{
	$datem = $row["mdate"];
	//expenses
	$sql_c = 'Select * from cer_draft_expenses where cer_expense_draft_id =' . $row["cer_basicdata_id"] . ' order by date_modified DESC';
	$res_c = mysql_query($sql_c) or dberror($sql_c);
	if ($row_c = mysql_fetch_assoc($res_c))
	{
		if($row_c["date_modified"] > $datem) {
			 $datem = $row_c["date_modified"];
		}
	}

	//investments
	$sql_c = 'Select * from cer_draft_investments where cer_investment_draft_id =' . $row["cer_basicdata_id"] . ' order by date_modified DESC';
	$res_c = mysql_query($sql_c) or dberror($sql_c);
	if ($row_c = mysql_fetch_assoc($res_c))
	{
		if($row_c["date_modified"] > $datem) {
			$datem = $row_c["date_modified"];
		}
	}

	//payments
	$sql_c = 'Select * from cer_draft_paymentterms where cer_paymentterm_draft_id =' . $row["cer_basicdata_id"] . ' order by date_modified DESC';
	$res_c = mysql_query($sql_c) or dberror($sql_c);
	if ($row_c = mysql_fetch_assoc($res_c))
	{
		if($row_c["date_modified"] > $datem) {
			$datem = $row_c["date_modified"];
		}
	}

	//revenues
	$sql_c = 'Select * from cer_draft_revenues where cer_revenue_draft_id =' . $row["cer_basicdata_id"] . ' order by date_modified DESC';
	$res_c = mysql_query($sql_c) or dberror($sql_c);
	if ($row_c = mysql_fetch_assoc($res_c))
	{
		if($row_c["date_modified"] > $datem) {
			$datem = $row_c["date_modified"];
		}
	}

	//salaries
	$sql_c = 'Select * from cer_draft_salaries where cer_salary_draft_id =' . $row["cer_basicdata_id"] . ' order by date_modified DESC';
	$res_c = mysql_query($sql_c) or dberror($sql_c);
	if ($row_c = mysql_fetch_assoc($res_c))
	{
		if($row_c["date_modified"] > $datem) {
			$datem = $row_c["date_modified"];
		}
	}

	//stocks
	$sql_c = 'Select * from cer_draft_stocks where cer_stock_draft_id =' . $row["cer_basicdata_id"] . ' order by date_modified DESC';
	$res_c = mysql_query($sql_c) or dberror($sql_c);
	if ($row_c = mysql_fetch_assoc($res_c))
	{
		if($row_c["date_modified"] > $datem) {
			$datem = $row_c["date_modified"];
		}
	}

	$mdates[$row["cer_basicdata_id"]] = $datem;

	$copy_links[$row["cer_basicdata_id"]] = "<a href='cer_drafts.php?copy_id=" .  $row["cer_basicdata_id"] . "'>create copy</a>";
}

$form = new Form("cer_drafts", "CER/AF Draft");
$form->add_comment("This section allwos you to create business plans without having a project. <br />So the section offers you a playground for business plan drafts. <br />If ever in the future you will be creating an AF or a CER you can copy the data from the playground into the real AF or CER.");

$form->populate();


/********************************************************************
	List Filter Forms
*********************************************************************/
$form1 = new Form("cer_drafts", "cer_drafts");

$form1->add_section("List Filter Selection");

if(param("selected_country"))
{
	$form1->add_list("selected_country", "Country",
		"select DISTINCT country_id, country_name from cer_drafts " . 
		"left join countries on country_id = cer_basicdata_country " . 
		"order by country_name", SUBMIT | NOTNULL, param("selected_country"));

}
else
{
	$form1->add_list("selected_country", "Country",
		"select DISTINCT country_id, country_name from cer_drafts " . 
		"left join countries on country_id = cer_basicdata_country " . 
		"order by country_name", SUBMIT | NOTNULL);
}

$form1->populate();

/********************************************************************
	Create List
*********************************************************************/ 
$list = new ListView($sql);


$list->set_entity("cer_drafts");
$list->set_order("country_name, cer_basicdata_title");
$list->set_filter($list_filter);   

$list->add_column("country_name", "Country", "", "", "", COLUMN_NO_WRAP);
$list->add_column("cer_basicdata_title", "Title", "cer_draft.php?did={cer_basicdata_id}", "", "", COLUMN_NO_WRAP);
$list->add_column("uname", "Owner", "", "", "", COLUMN_NO_WRAP);
$list->add_column("cdate", "Created", "", "", "", COLUMN_NO_WRAP);
$list->add_text_column("dates_2", "Modified", 0, $mdates);
$list->add_text_column("copylinks", "", COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $copy_links);

$list->add_button(LIST_BUTTON_NEW, "Add New Business Plan Draft", "cer_draft.php");
$list->add_button("create_draft_from_project", "Create Draft from existing Project");

/********************************************************************
	Populate list and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();

if($list->button("create_draft_from_project")) {
	$link = "cer_draft_create_from_project.php";
	redirect($link);
}


/********************************************************************
	render page
*********************************************************************/ 
$page = new Page("cer_drafts");

$page->header();
$page->title("CER/AF: Business Plan Drafts");

$form->render();
if(!has_access("has_access_to_drafts_of_his_country")) {
	$form1->render();
}
$list->render();
$page->footer();

?>