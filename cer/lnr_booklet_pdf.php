<?php
/********************************************************************

    lnr_booklet_pdf.php

    Print Lease Negotiation Application Form.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-06-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-06-16
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();

$SUPPRESS_HEADERS = true;
define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 


require "../include/frame.php";

$cer_version = 0;
$ln_version = 0;
if(param("cerversion"))
{
	$cer_version = param("cerversion");
}
if(param("lnversion"))
{
	$ln_version = param("lnversion");
}



check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}

require "include/get_functions.php";
require "include/get_project.php";
require "../shared/func_posindex.php";
require "include/financial_functions.php";
require_once "../shared/project_cost_functions.php";


require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');
require_once('../include/fpdi/fpdi.php'); 




//get signed sheet
$ln_signatures = "";

$sql_sheets = "select cer_summary_ln_coversheet " .
	   "from cer_summary ". 
	   " where cer_summary_cer_version = 0 and cer_summary_project = " . param("pid");

$res_sheets = mysql_query($sql_sheets) or dberror($sql_sheets);
if ($row_ssheets = mysql_fetch_assoc($res_sheets))
{
	$ln_signatures = $row_ssheets["cer_summary_ln_coversheet"];
}

// data needed
$page_title = "Lease Negotiation ";

$project = get_project(param("pid"));
$order_number  = $project["project_order"];

//get pos data
if($project["pipeline"] == 0)
{
	$pos_data = get_poslocation($project["posaddress_id"], "posaddresses");
}
elseif($project["pipeline"] == 1)
{
	$pos_data = get_poslocation_from_pipeline($project["posaddress_id"], $project["order_id"]);
}

$relocated_pos = '';
$relocated_coordinates = array();
if($project["project_relocated_posaddress_id"] > 0)
{
		$sql = "select concat(posaddress_name, ', ', place_name) as posname, 
		       posaddress_google_lat, posaddress_google_long
		       from posaddresses
			   left join places on place_id = posaddress_place_id 
			   where posaddress_id = " . dbquote($project["project_relocated_posaddress_id"]);
		
		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$relocated_pos = $row['posname'];
			$relocated_coordinates = array('lat'=>$row["posaddress_google_lat"], 'long'=>$row["posaddress_google_long"]);

		}		
		
}


$client_currency = get_cer_currency(param("pid"), $cer_version);
$currency_symbol = $client_currency["symbol"];

$gross_surface = $project["project_cost_gross_sqms"];
$total_surface = $project["project_cost_totalsqms"];
$sale_surface = $project["project_cost_sqms"];
$bo_surface = $project["project_cost_backofficesqms"];
$other_surface = $project["project_cost_othersqms"];


$floor_surfaces = '';
if($project['project_cost_numfloors'] > 1) {
	
	if($project['project_cost_floorsurface1'] > 0) {
		$floor_surfaces .= "Floor 1: " . $project['project_cost_floorsurface1'];
	}
	if($project['project_cost_floorsurface2'] > 0) {
		$floor_surfaces .= ", Floor 2: " . $project['project_cost_floorsurface2'];
	}
	if($project['project_cost_floorsurface3'] > 0) {
		$floor_surfaces .= ", Floor31: " . $project['project_cost_floorsurface3'];
	}
}

if($floor_surfaces) {
	$floor_surfaces = " (" . $floor_surfaces . ")";
}

if($relocated_pos) {

	$postype = "Relocation" . ", " . $project["project_costtype_text"] . ", " . $project["postype_name"] . ", " . $project["possubclass_name"];
}
else {
	$postype = $project["projectkind_name"] . ", " . $project["project_costtype_text"] . ", " . $project["postype_name"] . ", " . $project["possubclass_name"];
}


$contract_starting_date = "";
$contract_ending_date = "";

//get keymoney and deposit
$tmp = get_pos_intangibles(param("pid"), 15, $cer_version);
if(count($tmp) > 0)
{
	$keymoney_full = $tmp ["cer_investment_amount_cer_loc"];
	$keymoney = round($keymoney_full/1000, 0);
	

	$tmp = get_pos_intangibles(param("pid"), 9, $cer_version);
	$deposit = $tmp ["cer_investment_amount_cer_loc"];
	$deposit = round($deposit/1000, 0);
}
else
{
	$keymoney = "";
	$deposit = "";
}

//get rents
$first_full_year = $cer_basicdata["cer_basicdata_firstyear"];
if($cer_basicdata["cer_basicdata_firstmonth"] > 1)
{
	$first_full_year = $cer_basicdata["cer_basicdata_firstyear"]+1;
}

$last_full_year = $cer_basicdata["cer_basicdata_lastyear"];
if($cer_basicdata["cer_basicdata_lastmonth"] < 12)
{
	$last_full_year = $cer_basicdata["cer_basicdata_lastyear"]-1;
}

$fullrent_firstyear = 0;
$fixedrent_firstyear = 0;
$turnoverrent_firstyear = 0;
$taxes_on_fixedrent_firstyear = '';
$deposit_value_in_months = "";


if($first_full_year > 0 and $cer_basicdata["cer_basicdata_firstyear"] < $cer_basicdata["cer_basicdata_lastyear"])
{
	$sql_e = "select sum(cer_expense_amount) as cer_expense_amount from cer_expenses " . 
		   "where cer_expense_cer_version = " . $cer_version . "  and cer_expense_project = " . param("pid") . 
		   " and cer_expense_type in (2,20) " . 
		   " and cer_expense_year = " . dbquote($first_full_year);

	$res_e = mysql_query($sql_e) or dberror($sql_e);
	if ($row_e = mysql_fetch_assoc($res_e))
	{
		$fixedrent_firstyear = $row_e["cer_expense_amount"];
		if($cer_basicdata["cer_basicdata_tax_percentage_on_rent"] > 0)
		{
			$tmp = $cer_basicdata["cer_basicdata_tax_percentage_on_rent"] / 100;
			$tmp = $fixedrent_firstyear/(1+$tmp);
			$taxes_on_fixedrent_firstyear = round($fixedrent_firstyear - $tmp, 0);
			$taxes_on_fixedrent_firstyear = ' (including ' . $cer_basicdata["cer_basicdata_tax_percentage_on_rent"] . '% taxes - ' . $currency_symbol . ' ' . $taxes_on_fixedrent_firstyear . ')';
		}
	}

	$sql_e = "select cer_expense_amount from cer_expenses " . 
		   "where cer_expense_cer_version = " . $cer_version . "  and cer_expense_project = " . param("pid") . 
		   " and cer_expense_type = 16 " . 
		   " and cer_expense_year = " . dbquote($first_full_year);

	$res_e = mysql_query($sql_e) or dberror($sql_e);
	if ($row_e = mysql_fetch_assoc($res_e))
	{
		$turnoverrent_firstyear = $row_e["cer_expense_amount"];
	}
}
elseif($cer_basicdata["cer_basicdata_firstyear"] == $cer_basicdata["cer_basicdata_lastyear"])
{
	$sql_e = "select sum(cer_expense_amount) as cer_expense_amount from cer_expenses " . 
		   "where cer_expense_cer_version = " . $cer_version . "  and cer_expense_project = " . param("pid") . 
		   " and cer_expense_type in (2,20) " . 
		   " and cer_expense_year = " . dbquote($cer_basicdata["cer_basicdata_firstyear"]);

	$res_e = mysql_query($sql_e) or dberror($sql_e);
	if ($row_e = mysql_fetch_assoc($res_e))
	{
		$fixedrent_firstyear = $row_e["cer_expense_amount"];
		if($cer_basicdata["cer_basicdata_tax_percentage_on_rent"] > 0)
		{
			$tmp = $cer_basicdata["cer_basicdata_tax_percentage_on_rent"] / 100;
			$tmp = $fixedrent_firstyear/(1+$tmp);
			$taxes_on_fixedrent_firstyear = round($fixedrent_firstyear - $tmp, 0);
			$taxes_on_fixedrent_firstyear = ' (including ' . $cer_basicdata["cer_basicdata_tax_percentage_on_rent"] . '% taxes - ' . $currency_symbol . ' ' . $taxes_on_fixedrent_firstyear . ')';
		}
	}

	$sql_e = "select cer_expense_amount from cer_expenses " . 
		   "where cer_expense_cer_version = " . $cer_version . "  and cer_expense_project = " . param("pid") . 
		   " and cer_expense_type = 16 " . 
		   " and cer_expense_year = " . dbquote($cer_basicdata["cer_basicdata_firstyear"]);


	$res_e = mysql_query($sql_e) or dberror($sql_e);
	if ($row_e = mysql_fetch_assoc($res_e))
	{
		$turnoverrent_firstyear = $row_e["cer_expense_amount"];
	}
}

$fullrent_firstyear = $fixedrent_firstyear + $turnoverrent_firstyear;

$fullrent_firstyear = //$fullrent_firstyear*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
$fullrent_firstyear = round($fullrent_firstyear/1000, 0);

//$turnoverrent_firstyear = $turnoverrent_firstyear*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
$turnoverrent_firstyear = round($turnoverrent_firstyear/1000, 0);

$fixedrent_firstyear = round($fixedrent_firstyear/1000, 0);


//Lease Consitions
$poslease = get_ln_lease_data($project["project_id"], $ln_basicdata["ln_basicdata_id"], $project["pipeline"], $ln_version);

if(count($poslease) > 0)
{
	$landlord = str_replace("\r\n", " ", $poslease["poslease_landlord_name"]);
	$negotiated_rental_conditions = $poslease["poslease_negotiated_conditions"];
	$indexrate = $poslease["poslease_indexrate"] . "%";
	$real_estate_fee = $poslease["poslease_realestate_fee"] . "%";
	
	$free_weeks = $poslease["poslease_freeweeks"];
	$free_months = round($free_weeks/4, 1);
	$termination_deadline = $poslease["poslease_termination_time"];

	$renewal_option_date = "";
	if($poslease["poslease_extensionoption"] != Null and $poslease["poslease_extensionoption"] != '0000-00-00')
	{
		$d1 = new DateTime($poslease["poslease_enddate"]);
		$d2 = new DateTime($poslease["poslease_extensionoption"]);
		$interval = date_diff($d2, $d1);

		$tmp = round($interval->d/30, 1) + $interval->m + ($interval->y * 12);
		$renewal_option_date = $tmp . " months (" . to_system_date($poslease["poslease_extensionoption"]) . ")";
	}


	$average_annual_rent = 0;
	$duration_in_years = 0;
	if($poslease["poslease_startdate"] != NULL 
		and $poslease["poslease_startdate"] != '0000-00-00' 
		and $poslease["poslease_enddate"] != NULL
		and $poslease["poslease_enddate"] != '0000-00-00')
		{
			$diff = abs(strtotime($poslease["poslease_enddate"]) - strtotime($poslease["poslease_startdate"]));
			$years = floor($diff / (365*60*60*24));
			$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));

			if($months == 12)
			{
				$months = 0;
				$years++;
			}

			$rental_duration = $years . " years and " . $months . " months";

			$duration_in_years = $years + ($months/12);

			$contract_starting_date = to_system_date($poslease["poslease_startdate"]);
			$contract_ending_date = to_system_date($poslease["poslease_enddate"]);

			/*
			$date1 = date(strtotime($poslease["poslease_startdate"]));
			$date2 = date(strtotime($poslease["poslease_enddate"]));
			
			$difference = $date2 - $date1;
			$rental_duration_in_months = round($difference / 86400 / 30, 1);
			*/
	
			$date1 = new DateTime($poslease["poslease_startdate"]);
			$date2 = new DateTime($poslease["poslease_enddate"]);
			$interval = date_diff($date1, $date2);
			$rental_duration_in_months = round($interval->d/30, 1) + $interval->m + ($interval->y * 12) . ' months';

			$sql_r = "select cer_fixed_rent_total_surface, cer_fixed_rent_amount, cer_fixed_rent_unit " . 
				   " from cer_fixed_rents " . 
				   " where cer_fixed_rent_cer_version = " . $cer_version . 
				   " and cer_fixed_rent_project_id = " . param("pid") . 
				   " order by cer_fixed_rent_from_year ASC ";
			
			$res_r = mysql_query($sql_r) or dberror($sql_r);
			if ($row_r = mysql_fetch_assoc($res_r))
			{
				if($row_r["cer_fixed_rent_unit"] == 1 or $row_r["cer_fixed_rent_unit"] == 2)
				{
					$tmp_p = $row_r["cer_fixed_rent_total_surface"] * $row_r["cer_fixed_rent_amount"];
				}
				elseif($row_r["cer_fixed_rent_unit"] == 3 or $row_r["cer_fixed_rent_unit"] == 3)
				{
					$tmp_p = ($row_r["cer_fixed_rent_total_surface"] * $row_r["cer_fixed_rent_amount"])/12;
				}
				elseif($row_r["cer_fixed_rent_unit"] == 5)
				{
					$tmp_p = $row_r["cer_fixed_rent_amount"];
				}
				elseif($row_r["cer_fixed_rent_unit"] == 6)
				{
					$tmp_p = $row_r["cer_fixed_rent_amount"]/12;
				}
				if($tmp_p > 0)
				{
					$tmp = get_pos_intangibles(param("pid"), 9, $cer_version);
					
					$deposit_value_in_months = 	 round($tmp ["cer_investment_amount_cer_loc"] / $tmp_p, 0);
				}
			}

			
			
	}
		
	if($duration_in_years > 0)
	{
		$total_lease_commitment = 0;
		$sql_cer = "select * from cer_expenses " .
					  "where cer_expense_cer_version = " . $cer_version . "  and cer_expense_project = " . param("pid") .
					  " and cer_expense_type IN(2, 3, 16, 18, 19, 20)";


		$res_cer = mysql_query($sql_cer) or dberror($sql_cer);
		while ($row_cer = mysql_fetch_assoc($res_cer))
		{
			$total_lease_commitment = $total_lease_commitment + 1*$row_cer["cer_expense_amount"];
		}

		if($duration_in_years > 0)
		{
			$average_annual_rent = round(($total_lease_commitment / $duration_in_years)/1000, 0);
		}
	}
}
else
{
	$landlord = "";
	$negotiated_rental_conditions = "";
	$indexrate = "";
	$real_estate_fee = "";
	$average_annual_rent = "";
	$duration_in_years = "";
	$total_lease_commitment = "";
	$contract_starting_date = "";
	$contract_ending_date = "";
	$free_weeks = "";
	$free_months = "";
	$termination_deadline = "";
	$rental_duration_in_months = "";
	$renewal_option_date = "";
}


//get averag increas in fixed rents
$average_yearly_increase = "";
$num_of_years1 = 0;
$num_of_years2 = 0;
$num_of_years3 = 0;
$index_rate_total = 0;
$increase_rate_total = 0;
$inflation_rate_total = 0;
$sql_e = "select cer_fixed_rent_index_rate, cer_fixed_rent_increas_rate, cer_fixed_rent_inflation_rate " . 
		 "from cer_fixed_rents " . 
		 "where cer_fixed_rent_cer_version = " . $cer_version . " and cer_fixed_rent_project_id = " . param("pid");

$res = mysql_query($sql_e) or dberror($sql_e);

while($row = mysql_fetch_assoc($res))
{
	if($row["cer_fixed_rent_index_rate"] > 0)
	{
		$num_of_years1++;
		$index_rate_total = $index_rate_total + $row["cer_fixed_rent_index_rate"];
	}
	if($row["cer_fixed_rent_increas_rate"] > 0)
	{
		$num_of_years2++;
		$increase_rate_total = $increase_rate_total + $row["cer_fixed_rent_increas_rate"];
	}
	if($row["cer_fixed_rent_inflation_rate"] > 0)
	{
		$num_of_years3++;
		$inflation_rate_total = $inflation_rate_total + $row["cer_fixed_rent_inflation_rate"];
	}
}

$tmp1 = "";
$tmp2 = "";
$tmp3 = "";
if($num_of_years1 > 0)
{
	$tmp1 = "Index Rate: " . number_format($index_rate_total/$num_of_years1, 2) . "% ";
}
if($num_of_years2 > 0)
{
	
	$tmp2 = "Increase Rate: " . number_format($increase_rate_total/$num_of_years2, 2) . "% ";
}
if($num_of_years3 > 0)
{
	$tmp3 = "Inflation Rate: " . number_format($inflation_rate_total/$num_of_years3, 2) . "% ";
}
$average_yearly_increase = $tmp1 . $tmp2 . $tmp3;


if($average_yearly_increase == 0) {

	$_tmp = '';
	$i = 0;
	$sql_r = "select cer_fixed_rent_total_surface, cer_fixed_rent_amount, cer_fixed_rent_unit " . 
		   " from cer_fixed_rents " . 
		   " where cer_fixed_rent_cer_version = " . $cer_version . 
		   " and cer_fixed_rent_project_id = " . param("pid") . 
		   " order by cer_fixed_rent_from_year ASC ";
	
	$res_r = mysql_query($sql_r) or dberror($sql_r);
	while ($row_r = mysql_fetch_assoc($res_r))
	{
		$amount = $row_r['cer_fixed_rent_amount'];

		if($i > 0 and $_tmp > 0) {
			$_incr = ($amount - $_tmp)/$_tmp;
			$average_yearly_increase = $average_yearly_increase + $_incr;
		}
		$_tmp = $amount;
		$i++;
	}

	if($i > 1) {
		$average_yearly_increase = round(100*($average_yearly_increase / ($i-1)), 2);
	}
}

//additional rental cost and other fees
//get additional rental costs
$annual_charges = 0;
$tax_on_rents = 0;
$passenger_index = 0;
$savings_on_rent = 0;

$sql_e = "select * from cer_expenses " .
         "where cer_expense_cer_version = " . $cer_version . "  and cer_expense_project = " . param("pid") . 
		 " and cer_expense_type in (3,18,19, 20) " .
		 " and cer_expense_year = " . dbquote($first_full_year) . 
		 " order by cer_expense_year";
$res_e = mysql_query($sql_e) or dberror($sql_e);

while($row_e = mysql_fetch_assoc($res_e))
{
	if($row_e["cer_expense_type"] == 3)
	{
		$annual_charges = round($row_e["cer_expense_amount"]/1000, 0);
	}
	elseif($row_e["cer_expense_type"] == 18)
	{
		$tax_on_rents = round($row_e["cer_expense_amount"]/1000, 0);
	}
	elseif($row_e["cer_expense_type"] == 19)
	{
		$passenger_index = round($row_e["cer_expense_amount"]/1000, 0);
	}
	elseif($row_e["cer_expense_type"] == 20)
	{
		$savings_on_rent = round($row_e["cer_expense_amount"]/1000, 0);
	}
}

//turnover percent for rents, only first record
$sales_percent_first_year = "";
$sql_e = "select * " .
	   "from cer_rent_percent_from_sales " .
	   "where cer_rent_percent_from_sale_cer_version = " . $cer_version . "  and cer_rent_percent_from_sale_project = " . param("pid") .
	   " and cer_rent_percent_from_sale_from_year = " . dbquote($first_full_year) .
	   " order by cer_rent_percent_from_sale_from_year";
$res_e = mysql_query($sql_e) or dberror($sql_e);
if($row_e = mysql_fetch_assoc($res_e))
{
	$sales_percent_first_year = round($row_e["cer_rent_percent_from_sale_percent"], 2) . "%";
}
else {
	$sql_e = "select * " .
		   "from cer_rent_percent_from_sales " .
		   "where cer_rent_percent_from_sale_cer_version = " . $cer_version . "  and cer_rent_percent_from_sale_project = " . param("pid") .
		   " order by cer_rent_percent_from_sale_from_year";
	$res_e = mysql_query($sql_e) or dberror($sql_e);
	if($row_e = mysql_fetch_assoc($res_e))
	{
		$sales_percent_first_year = round($row_e["cer_rent_percent_from_sale_percent"], 2) . "%";
	}
}

//sales guarantee
$sales_guarantee_first_year = "";
$sql_e = "select cer_fixed_rent_sales_guarantee_amount " .
	   "from cer_fixed_rents " .
	   "where cer_fixed_rent_cer_version = " . $cer_version . "  and cer_fixed_rent_project_id = " . param("pid") .
	   " and cer_fixed_rent_from_year = " . dbquote($first_full_year);
$res_e = mysql_query($sql_e) or dberror($sql_e);
if($row_e = mysql_fetch_assoc($res_e))
{
	$sales_guarantee_first_year = round($row_e["cer_fixed_rent_sales_guarantee_amount"]/1000, 0);
}
else {
	$sql_e = "select cer_fixed_rent_sales_guarantee_amount " .
		   "from cer_fixed_rents " .
		   "where cer_fixed_rent_cer_version = " . $cer_version . "  and cer_fixed_rent_project_id = " . param("pid");
	$res_e = mysql_query($sql_e) or dberror($sql_e);
	if($row_e = mysql_fetch_assoc($res_e))
	{
		$sales_guarantee_first_year = round($row_e["cer_fixed_rent_sales_guarantee_amount"]/1000, 0);
	}
}

//sellouts
$sellout_ending_year = (int)substr($cer_basicdata["date_created"], 0, 4);

if($sellout_ending_year < (int)substr($cer_basicdata["cer_basicdata_firstyear"], 0, 4))
{
	$sellout_ending_year =(int)substr($cer_basicdata["cer_basicdata_firstyear"], 0, 4);
}

if($sellout_ending_year > date("Y"))
{
	$sellout_ending_year = date("Y");
}

$sellout_starting_year = $sellout_ending_year - 3;

$currency_symbol = get_currency_symbol($ln_basicdata["ln_basicdata_currency"]);


//approval names
$approval_name1 = $cer_basicdata["cer_basicdata_approvalname1"];
$approval_name2 = $cer_basicdata["cer_basicdata_approvalname2"];
$approval_name3 = $cer_basicdata["cer_basicdata_approvalname3"];
$approval_name4 = $cer_basicdata["cer_basicdata_approvalname4"];
$approval_name5 = $cer_basicdata["cer_basicdata_approvalname5"];
$approval_name6 = $cer_basicdata["cer_basicdata_approvalname6"];
$approval_name7 = $cer_basicdata["cer_basicdata_approvalname7"];
$approval_name8 = $cer_basicdata["cer_basicdata_approvalname8"];
$approval_name9 = $cer_basicdata["cer_basicdata_approvalname9"];
$approval_name10 = $cer_basicdata["cer_basicdata_approvalname10"];

$approval_name11 = $cer_basicdata["cer_summary_in01_sig01"];
$approval_name12 = $cer_basicdata["cer_summary_in01_sig02"];
$approval_name13 = $cer_basicdata["cer_summary_in01_sig03"];
$approval_name14 = $cer_basicdata["cer_summary_in01_sig05"];
$approval_name15 = $cer_basicdata["cer_summary_in01_sig07"];
$approval_name16 = $cer_basicdata["cer_summary_in01_sig10"];

//documents and pictures
$pix1 = ".." . $ln_basicdata["ln_basicdata_pix1"];
$pix2 = ".." . $ln_basicdata["ln_basicdata_pix2"];
$pix3 = ".." . $ln_basicdata["ln_basicdata_pix3"];

$floor_plan = ".." . $ln_basicdata["ln_basicdata_floorplan"];
$lease_agreement = ".." . $ln_basicdata["ln_basicdata_draft_aggreement"];
$mall_map = ".." . $ln_basicdata["ln_basicdata_mall_all_floors"];
$store_layout = ".." . $ln_basicdata["ln_basicdata_store_layout"];


//LN supporting documents
$ln_supporting_documents = array();
$ln_supporting_document_titles = array();
$sql_d = "select order_file_path, order_file_title " . 
		 " from order_files " . 
		 " where order_file_order = " . dbquote($project["order_id"]) . 
		 " and order_file_category = 16 " . 
		 " and order_file_type IN (2,13)";

$res_d = mysql_query($sql_d) or dberror($sql_d);
while ($row_d = mysql_fetch_assoc($res_d))
{
	$ln_supporting_documents[] = $_SERVER["DOCUMENT_ROOT"] . $row_d["order_file_path"];
	$ln_supporting_document_titles[] = $row_d["order_file_title"];
}

// Create and setup PDF document

$pdf = new FPDI("L", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 20, 10);
$pdf->setPrintHeader(false);

$pdf->SetTitle("Retail Lease Negotiation");
$pdf->SetAuthor( BRAND . " Retailnet");
$pdf->SetDisplayMode(100);
$pdf->SetAutoPageBreak(false, 0);


$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');
$pdf->Open();



include("lnr_01_pdf_detail.php");
if($project["project_cost_type"] == 1) //corporate
{
	
	if($ln_signatures)
	{
		$source_file = ".." . $ln_signatures;
		if(file_exists($source_file))
		{
			
			$layout = new FPDI();
			$num_of_pages = $pdf->setSourceFile($source_file);
			$num_of_pages = $layout->setSourceFile($source_file);
			for($i=1;$i<=$num_of_pages;$i++)
			{
				$tplIdx = $pdf->importPage($i);
				$layout->AddPage("L");
				$tplIdx = $layout->importPage($i);
				
				$tpl = $layout->useTemplate($tplIdx);
				//$pdf->AddPage("", array(0=>$tpl["w"], 1=>$tpl["h"]));

				if($tpl["w"] > $tpl["h"])
				{
					$pdf->AddPage("L", array(0=>$tpl["h"], 1=>$tpl["w"]));
				}
				else
				{
					$pdf->AddPage("P", array(0=>$tpl["w"], 1=>$tpl["h"]));
				}
				$tpl = $pdf->useTemplate($tplIdx);
			}
		}
	}
	else
	{
		
		if($project["project_projectkind"] == 5) //corporate lease renewal
		{
			include("lnr_02Abis_pdf_detail.php");
		}
		elseif($project["project_cost_type"] == 1) //corporate
		{
			include("lnr_02A_pdf_detail.php");
		}
	}
}
elseif($project["project_cost_type"] == 2) //franchisee
{
	include("lnr_02B_pdf_detail.php");
}

if ($project['project_cost_type'] == 1) {
	include("ln_lease_details_detail_pdf.php");
}

include("lnr_03_pdf_detail.php");



//former lease conditions
if($project['project_projectkind'] == 5
		or $project['project_projectkind'] == 2
		or $project['project_projectkind'] == 3
		or $project['project_projectkind'] == 6) {
		include("ln_former_lease_conditions_pdf_detail.php");
}


//business plan

if($project['project_cost_type'] != 2) {
	$currencies = array();
	$sql = 'select currency_id, currency_symbol from currencies where currency_system = 1';

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$currencies["s"] = $row['currency_symbol'];
	}


	if($cer_basicdata['cer_basicdata_currency']) {
		$sql = 'select currency_id, currency_symbol from currencies where currency_id = ' . $cer_basicdata['cer_basicdata_currency'];

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res) and !in_array($row['currency_symbol'], $currencies))
		{
			$currencies["c1"] = $row['currency_symbol'];
		}
	}

	if($cer_basicdata['cer_basicdata_currency2']) {
		$sql = 'select currency_id, currency_symbol from currencies where currency_id = ' . $cer_basicdata['cer_basicdata_currency2'];

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res) and !in_array($row['currency_symbol'], $currencies))
		{
			$currencies["c2"] = $row['currency_symbol'];
		}
	}

	//business plan in client's currency
	$currency_source = "LN";


	include("cer_inr02_pdf_detail.php");


	//business plan in system'currency
	if(array_key_exists("s", $currencies) and array_key_exists("c1", $currencies) and $currencies["s"] != $currencies["c1"])
	{
		$cid = 's';
		include("cer_inr02_pdf_detail.php");
	}

	//business plan in pos'currency
	if(array_key_exists("c2", $currencies) and array_key_exists("c1", $currencies) and $currencies["c2"] != $currencies["c1"])
	{
		$cid = 'c2';
		include("cer_inr02_pdf_detail.php");
	}
}


//add files for the booklet)
if($form_type == 'AF')
{
	
	//project budget in local currency

	include("../user/project_costs_budget_detail_pdf.php");


	//project budget in CHF
	if(isset($order_currency) and $order_currency["symbol"] != 'CHF')
	{
		$sc = 1;
		include("../user/project_costs_budget_detail_pdf.php");
	}


	//quotes for local works
	//include("../user/project_offer_comparison_pdf_main.php");
	include("../user/project_costs_bid_comparison_pdf_main.php");

	//list of variables
	include("cer_listofvars_pdf_detail.php");
	
	
	// add all files
	$source_file = ".." . $ln_basicdata["ln_basicdata_floorplan"];
	if(file_exists($source_file)) {

		if(substr($source_file, strlen($source_file)-3, 3) == "pdf" 
			or substr($source_file, strlen($source_file)-3, 3) == "PDF")
		{
			
			$num_of_pages = $pdf->setSourceFile($source_file);
			for($i=1;$i<=$num_of_pages;$i++)
			{
				$tplidx = $pdf->ImportPage($i);
				$s = $pdf->getTemplatesize($tplidx);
				if($s["w"] > $s["h"])
				{
					$pdf->AddPage("L", array(0=>$s["h"], 1=>$s["w"]));
				}
				else
				{
					$pdf->AddPage("P", array(0=>$s["w"], 1=>$s["h"]));
				}
				$pdf->useTemplate($tplidx);
			}
			
			
		}
	}

	if(file_exists($source_file)) {

		$source_file = ".." . $ln_basicdata["ln_basicdata_elevation_plan"];

		if(substr($source_file, strlen($source_file)-3, 3) == "pdf" 
			or substr($source_file, strlen($source_file)-3, 3) == "PDF")
		{
			
			$num_of_pages = $pdf->setSourceFile($source_file);
			for($i=1;$i<=$num_of_pages;$i++)
			{
				$tplidx = $pdf->ImportPage($i);
				$s = $pdf->getTemplatesize($tplidx);
				if($s["w"] > $s["h"])
				{
					$pdf->AddPage("L", array(0=>$s["h"], 1=>$s["w"]));
				}
				else
				{
					$pdf->AddPage("P", array(0=>$s["w"], 1=>$s["h"]));
				}
				$pdf->useTemplate($tplidx);
			}
			
			
		}
	}

	$source_file = ".." . $ln_basicdata["ln_basicdata_elevation_plan3"];

	if(file_exists($source_file)) {
		if(substr($source_file, strlen($source_file)-3, 3) == "pdf" 
			or substr($source_file, strlen($source_file)-3, 3) == "PDF")
		{
			
			$num_of_pages = $pdf->setSourceFile($source_file);
			for($i=1;$i<=$num_of_pages;$i++)
			{
				$tplidx = $pdf->ImportPage($i);
				$s = $pdf->getTemplatesize($tplidx);
				if($s["w"] > $s["h"])
				{
					$pdf->AddPage("L", array(0=>$s["h"], 1=>$s["w"]));
				}
				else
				{
					$pdf->AddPage("P", array(0=>$s["w"], 1=>$s["h"]));
				}
				$pdf->useTemplate($tplidx);
			}
			
			
		}
	}

	$source_file = ".." . $ln_basicdata["ln_basicdata_photoreport"];

	if(file_exists($source_file)) {
		if(substr($source_file, strlen($source_file)-3, 3) == "pdf" 
			or substr($source_file, strlen($source_file)-3, 3) == "PDF")
		{
			
			$num_of_pages = $pdf->setSourceFile($source_file);
			for($i=1;$i<=$num_of_pages;$i++)
			{
				$tplidx = $pdf->ImportPage($i);
				$s = $pdf->getTemplatesize($tplidx);
				if($s["w"] > $s["h"])
				{
					$pdf->AddPage("L", array(0=>$s["h"], 1=>$s["w"]));
				}
				else
				{
					$pdf->AddPage("P", array(0=>$s["w"], 1=>$s["h"]));
				}
				$pdf->useTemplate($tplidx);
			}
			
			
		}
	}


	$source_file = ".." . $ln_basicdata["ln_basicdata_3drenderings"];

	if(file_exists($source_file)) {
		if(substr($source_file, strlen($source_file)-3, 3) == "pdf" 
			or substr($source_file, strlen($source_file)-3, 3) == "PDF")
		{
			
			$num_of_pages = $pdf->setSourceFile($source_file);
			for($i=1;$i<=$num_of_pages;$i++)
			{
				$tplidx = $pdf->ImportPage($i);
				$s = $pdf->getTemplatesize($tplidx);
				if($s["w"] > $s["h"])
				{
					$pdf->AddPage("L", array(0=>$s["h"], 1=>$s["w"]));
				}
				else
				{
					$pdf->AddPage("P", array(0=>$s["w"], 1=>$s["h"]));
				}
				$pdf->useTemplate($tplidx);
			}
			
			
		}
	}
}
else
{
	//mall map, street map	
	$source_file = $floor_plan;
	if(file_exists($source_file))
	{
		
		//PDF
		if(substr($source_file, strlen($source_file)-3, 3) == "pdf" or substr($source_file, strlen($source_file)-3, 3) == "PDF")
		{
			$layout = new FPDI();
			$num_of_pages = $pdf->setSourceFile($source_file);
			$num_of_pages = $layout->setSourceFile($source_file);
			for($i=1;$i<=$num_of_pages;$i++)
			{
				$tplIdx = $pdf->importPage($i);
				$layout->AddPage("L");
				$tplIdx = $layout->importPage($i);
				
				$tpl = $layout->useTemplate($tplIdx);
				//$pdf->AddPage("", array(0=>$tpl["w"], 1=>$tpl["h"]));

				if($tpl["w"] > $tpl["h"])
				{
					$pdf->AddPage("L", array(0=>$tpl["h"], 1=>$tpl["w"]));
				}
				else
				{
					$pdf->AddPage("P", array(0=>$tpl["w"], 1=>$tpl["h"]));
				}
				$tpl = $pdf->useTemplate($tplIdx);

			}
		}
	}

	$source_file = $mall_map;
	if(file_exists($source_file))
	{
		
		//PDF
		if(substr($source_file, strlen($source_file)-3, 3) == "pdf" or substr($source_file, strlen($source_file)-3, 3) == "PDF")
		{
			if(!isset($layout))
			{
				$layout = new FPDI();
			}
			$num_of_pages = $pdf->setSourceFile($source_file);
			$num_of_pages = $layout->setSourceFile($source_file);
			for($i=1;$i<=$num_of_pages;$i++)
			{
				$tplIdx = $pdf->importPage($i);
				$layout->AddPage("L");
				$tplIdx = $layout->importPage($i);
				
				$tpl = $layout->useTemplate($tplIdx);
				//$pdf->AddPage("", array(0=>$tpl["w"], 1=>$tpl["h"]));

				if($tpl["w"] > $tpl["h"])
				{
					$pdf->AddPage("L", array(0=>$tpl["h"], 1=>$tpl["w"]));
				}
				else
				{
					$pdf->AddPage("P", array(0=>$tpl["w"], 1=>$tpl["h"]));
				}
				$tpl = $pdf->useTemplate($tplIdx);
			}
		}
	}


	$source_file = $store_layout;
	if(file_exists($source_file))
	{
		
		//PDF
		if(substr($source_file, strlen($source_file)-3, 3) == "pdf" or substr($source_file, strlen($source_file)-3, 3) == "PDF")
		{
			if(!isset($layout))
			{
				$layout = new FPDI();
			}
			$num_of_pages = $pdf->setSourceFile($source_file);
			$num_of_pages = $layout->setSourceFile($source_file);
			for($i=1;$i<=$num_of_pages;$i++)
			{
				$tplIdx = $pdf->importPage($i);
				$layout->AddPage("L");
				$tplIdx = $layout->importPage($i);
				
				$tpl = $layout->useTemplate($tplIdx);
				//$pdf->AddPage("", array(0=>$tpl["w"], 1=>$tpl["h"]));

				if($tpl["w"] > $tpl["h"])
				{
					$pdf->AddPage("L", array(0=>$tpl["h"], 1=>$tpl["w"]));
				}
				else
				{
					$pdf->AddPage("P", array(0=>$tpl["w"], 1=>$tpl["h"]));
				}
				$tpl = $pdf->useTemplate($tplIdx);
			}
		}
	}
}

//google map

if($pos_data["posaddress_google_lat"] and $pos_data["posaddress_google_long"])
{ 
	$page_title = "Lease Negotiation - Maps " . $project["order_shop_address_company"] . ", " . $project["order_number"];
	$latitude = $pos_data["posaddress_google_lat"];
	$longitude = $pos_data["posaddress_google_long"];

	//insert google map

	$pdf->AddPage("L", "A4");
	$pdf->Image('../pictures/logo.jpg',10,8,33);
	//arialn bold 15
	$pdf->SetFont('arialn','B',12);
	//Move to the right
	$pdf->Cell(80);
	//Title
	$pdf->SetY(2);
	$pdf->Cell(0,33,$page_title,0,0,'R');
	//Line break
	$pdf->SetY(25);



	$url = STATIC_MAPS_HOST;
	$url .= '?center=' . $latitude . ',' . $longitude;
	$url .= '&zoom=17';
	$url .= '&size=640x640';
	$url .= '&maptype=roadmap' . "\\";
	$url .= '&markers=color:red%7Clabel:P%7C' . $latitude . ',' . $longitude;
	
	if(count($relocated_coordinates) > 0) {
		$url .= '&markers=color:blue%7Clabel:R%7C' . $relocated_coordinates['lat'] . ',' . $relocated_coordinates['long'];
	}

	$url .= '&format=jpg';
	$url .= '&key=' . GOOGLE_API_KEY;
	$url .= '&sensor=false';

	
	$tmpfilename1 = "map1" . time() . ".jpg";

	
	if(!$context) {
		
		$mapImage1 = @file_get_contents($url);
	}
	else
	{
		$mapImage1 = @file_get_contents($url, false, $context);
	}

	if($mapImage1) {
	

		$fh = fopen(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1, 'w') or die("can't open file");
		fwrite($fh, $mapImage1);
		fclose($fh);
		
		if(file_exists(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1)) {
			
			$pdf->Image(TMP_FILE_DIR_RELATIVE . $tmpfilename1, 10, $pdf->getY(), 135, 135);
		}
	}
	
	$url = STATIC_MAPS_HOST;
	$url .= '?center=' . $latitude . ',' . $longitude;
	$url .= '&zoom=15';
	$url .= '&size=640x640';
	$url .= '&maptype=roadmap' . "\\";
	$url .= '&markers=color:red%7Clabel:P%7C' . $latitude . ',' . $longitude;

	if(count($relocated_coordinates) > 0) {
		$url .= '&markers=color:blue%7Clabel:R%7C' . $relocated_coordinates['lat'] . ',' . $relocated_coordinates['long'];
	}

	$url .= '&format=jpg';
	$url .= '&key=' . GOOGLE_API_KEY;
	$url .= '&sensor=false';

	$tmpfilename2 = "map2" . time() . ".jpg";
	
	
	if(!$context) {
		
		$mapImage2 = @file_get_contents($url);
	}
	else
	{
		$mapImage2 = @file_get_contents($url, false, $context);
	}

	if($mapImage2) {
		$fh = fopen(TMP_FILE_DIR_ABSOLUTE . $tmpfilename2, 'w') or die("can't open file");
		fwrite($fh, $mapImage2);
		fclose($fh);
		
		if(file_exists(TMP_FILE_DIR_ABSOLUTE . $tmpfilename2)) {
			$pdf->Image(TMP_FILE_DIR_RELATIVE . $tmpfilename2, 151, $pdf->getY(), 135, 135);
		}
	}

	if(file_exists(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1))
	{
		unlink(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1);
	}

	if(file_exists(TMP_FILE_DIR_ABSOLUTE . $tmpfilename2))
	{
		unlink(TMP_FILE_DIR_ABSOLUTE . $tmpfilename2);
	}


	//Position at 1.5 cm from bottom
	$pdf->SetY(192);
	//arialn italic 8
	$pdf->SetFont('arialn','I',8);
	//Page number
	$pdf->Cell(0,10, to_system_date(date("d.m.y")),0,0,'R');
}

// Lease Agreement 
$source_file = $lease_agreement;
if(file_exists($source_file))
{
	//PDF
	if(substr($source_file, strlen($source_file)-3, 3) == "pdf" or substr($source_file, strlen($source_file)-3, 3) == "PDF")
	{
		if(!isset($layout))
		{
			$layout = new FPDI();
		}
		$num_of_pages = $pdf->setSourceFile($source_file);
		$num_of_pages = $layout->setSourceFile($source_file);

		for($i=1;$i<=$num_of_pages;$i++)
		{
			$tplIdx = $pdf->importPage($i);
			$layout->AddPage("L");
			$tplIdx = $layout->importPage($i);
			
			$tpl = $layout->useTemplate($tplIdx);
			if($tpl["w"] > $tpl["h"])
			{
				$tpl = $layout->useTemplate($tplIdx, 0, 0, $tpl["w"], $tpl["h"], true);
				//$pdf->AddPage("L", array(0=>$tpl["w"], 1=>$tpl["h"]));
				$pdf->AddPage("L");
			}
			else
			{
				$tpl = $layout->useTemplate($tplIdx, 0, 0, $tpl["w"], $tpl["h"], true);
				$pdf->AddPage("P");
				//$pdf->AddPage("P", array(0=>$tpl["w"], 1=>$tpl["h"]));
			}
			$tpl = $pdf->useTemplate($tplIdx);
		}
	}
}


	
//Picture 2
if($pix2 != ".." and file_exists($pix2))
{
	if(substr($pix2, strlen($pix2)-3, 3) == "jpg" or substr($pix2, strlen($pix2)-3, 3) == "JPG")
	{
		$pdf->AddPage("L", "A4");
		$pdf->Image('../pictures/logo.jpg',10,8,33);
		//arialn bold 15
		$pdf->SetFont('arialn','B',12);
		//Move to the right
		$pdf->Cell(80);
		//Title
		$pdf->SetY(2);
		$pdf->Cell(0,33,"Lease Negotiation - Pictures " . $project["order_shop_address_company"],0,0,'R');
		//Line break
		$pdf->SetY(23);

		$x = $pdf->GetX();
		$y = $pdf->GetY() + 2;
		
		
		$pdf->Image($pix2,$x,$y, 0, 165);
		
		//Position at 1.5 cm from bottom
		$pdf->SetY(192);
		//arialn italic 8
		$pdf->SetFont('arialn','I',8);
		//Page number
		$pdf->Cell(0,10, to_system_date(date("d.m.y")),0,0,'R');
	}
}


//Picture 3
if($pix3 != ".." and file_exists($pix3))
{
		
	if(substr($pix3, strlen($pix3)-3, 3) == "jpg" or substr($pix3, strlen($pix3)-3, 3) == "JPG")
	{		
		$pdf->AddPage("L", "A4");
		$pdf->Image('../pictures/logo.jpg',10,8,33);
		//arialn bold 15
		$pdf->SetFont('arialn','B',12);
		//Move to the right
		$pdf->Cell(80);
		//Title
		$pdf->SetY(2);
		$pdf->Cell(0,33,"Lease Negotiation - Pictures " . $project["order_shop_address_company"],0,0,'R');
		//Line break
		$pdf->SetY(23);

		$x = $pdf->GetX();
		$y = $pdf->GetY() + 2;
		
		
		$pdf->Image($pix3,$x,$y, 0, 165);
		
		//Position at 1.5 cm from bottom
		$pdf->SetY(192);
		//arialn italic 8
		$pdf->SetFont('arialn','I',8);
		//Page number
		$pdf->Cell(0,10, to_system_date(date("d.m.y")),0,0,'R');
	}
}



if(count($ln_supporting_documents) > 0)
{
	foreach($ln_supporting_documents as $key=>$source_file)
	{
		$file_title = $ln_supporting_document_titles[$key];
		if(file_exists($source_file))
		{
			//PDF
			if(substr($source_file, strlen($source_file)-3, 3) == "pdf" or substr($source_file, strlen($source_file)-3, 3) == "PDF")
			{
				if(!isset($layout))
				{
					$layout = new FPDI();
				}
				$num_of_pages = $pdf->setSourceFile($source_file);
				$num_of_pages = $layout->setSourceFile($source_file);

				for($i=1;$i<=$num_of_pages;$i++)
				{
					$tplIdx = $pdf->importPage($i);
					$layout->AddPage("L");
					$tplIdx = $layout->importPage($i);
					
					$tpl = $layout->useTemplate($tplIdx);
					if($tpl["w"] > $tpl["h"])
					{
						$tpl = $layout->useTemplate($tplIdx, 0, 0, $tpl["w"], $tpl["h"], true);
						//$pdf->AddPage("L", array(0=>$tpl["w"], 1=>$tpl["h"]));
						$pdf->AddPage("L");
					}
					else
					{
						$tpl = $layout->useTemplate($tplIdx, 0, 0, $tpl["w"], $tpl["h"], true);
						$pdf->AddPage("P");
						//$pdf->AddPage("P", array(0=>$tpl["w"], 1=>$tpl["h"]));
					}
					$tpl = $pdf->useTemplate($tplIdx);
				}
			}
			elseif(substr($source_file, strlen($source_file)-3, 3) == "jpg" or substr($source_file, strlen($source_file)-3, 3) == "JPG")
			{
				$pdf->AddPage("L", "A4");
				$pdf->Image('../pictures/logo.jpg',10,8,33);
				//arialn bold 15
				$pdf->SetFont('arialn','B',12);
				//Move to the right
				$pdf->Cell(80);
				//Title
				$pdf->SetY(2);
				$pdf->Cell(0,33,"Lease Negotiation - " . $file_title  . " - " . $project["order_shop_address_company"],0,0,'R');
				//Line break
				$pdf->SetY(23);

				$x = $pdf->GetX();
				$y = $pdf->GetY() + 2;
				
				
				$pdf->Image($source_file,$x,$y, 0, 165);
				
				//Position at 1.5 cm from bottom
				$pdf->SetY(192);
				//arialn italic 8
				$pdf->SetFont('arialn','I',8);
				//Page number
				$pdf->Cell(0,10, to_system_date(date("d.m.y")),0,0,'R');
			}
		}
	}
}




//brand details
if(count($cer_brands) > 1)
{
	include("cer_inr02_brand_detail_pdf.php");
}


if($form_type == 'AF')
{
	//check if project has jpg attachments to be added to the booklet
	$sql = "select order_file_path " . 
		   "from projects " . 
		   "left join order_files on order_file_order = project_order " . 
		   "where order_file_attach_to_cer_booklet = 1 and order_file_type= 2 and project_id = " .param("pid") . 
		   " order by order_files.date_created DESC";

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		
		
		$source_file = ".." . $row["order_file_path"];
		if(file_exists($source_file))
		{
			$pdf->AddPage("L");
			$margin_top = 16;
			$margin_left = 12;
			$pdf->SetXY($margin_left,$margin_top);

			/*
			$pdf->SetFont("arialn", "I", 9);
			$pdf->Cell(45, 10, "", 1);

			$pdf->Image('../pictures/brand_logo.jpg',13,$margin_top + 1,22);

			$pdf->SetFont("arialn", "B", 11);
			$pdf->Cell(172, 10, "Layout: " . $project_name, 1, "", "L");
			$pdf->Cell(20, 10, date("d.m.Y"), 1, "", "C");
			*/

			$pdf->Image($source_file,20,40, 200);
		}
	}


	//check if project has pdf attachments to be added to the booklet
	$PDFmerger_was_used = false;
	$pdf_print_output = true;

	$sql = "select order_file_path " . 
		   "from projects " . 
		   "left join order_files on order_file_order = project_order " . 
		   "where order_file_attach_to_cer_booklet = 1 and order_file_type = 13 and project_id = " .param("pid") . 
		   " order by order_files.date_created DESC";

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{

		$source_file = ".." . $row["order_file_path"];	if(file_exists($source_file))
		$source_file = ".." . $row["order_file_path"];	
		
		if(substr($source_file, strlen($source_file)-3, 3) == "pdf" 
			or substr($source_file, strlen($source_file)-3, 3) == "PDF")
		{
			if(file_exists($source_file))
			{
				
				$num_of_pages = $pdf->setSourceFile($source_file);
				for($i=1;$i<=$num_of_pages;$i++)
				{
					$tplidx = $pdf->ImportPage($i);
					$s = $pdf->getTemplatesize($tplidx);
					if($s["w"] > $s["h"])
					{
						$pdf->AddPage("L", array(0=>$s["h"], 1=>$s["w"]));
					}
					else
					{
						$pdf->AddPage("P", array(0=>$s["w"], 1=>$s["h"]));
					}
					$pdf->useTemplate($tplidx);
				}
			}
		}
		
	}
}

// write pdf
if($form_type == 'AF')
{
	$pdf->Output(BRAND . '_af_booklet_' . $pos_data["posaddress_name"] . '_' . $project["project_number"] . '.pdf', 'D');
}
else
{
	$pdf->Output(BRAND . '_ln_booklet_' . $pos_data["posaddress_name"] . '_' . $project["project_number"] . '.pdf', 'D');
}

?>