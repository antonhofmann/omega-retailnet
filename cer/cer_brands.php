<?php
/********************************************************************

    cer_brands.php

    Lists Brands in volved in CER (Multibrand Option)

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-11-22
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-11-22
    Version:        1.0.0

    Copyright (c) 2014 Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_full_access_to_cer");
set_referer("cer_brand.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
//get all data from interest rates


$sql = "select cer_brand_id, cer_brand_name, " .
       "if(cer_brand_active = 1, 'x', '') as inuse " . 
       "from cer_brands ";


/********************************************************************
    build list 
*********************************************************************/

$list = new ListView($sql);

$list->set_entity("cer_brands");
$list->set_order("cer_brand_name");

$list->add_column("cer_brand_name", "Brand", "cer_brand.php");
$list->add_column("inuse", "in use");

$list->add_button(LIST_BUTTON_NEW, "New", "cer_brand.php");

$list->populate();
$list->process();

$page = new Page("cer_brands");

$page->header();
$page->title("Brands Involved in CER");
$list->render();
$page->footer();
?>
