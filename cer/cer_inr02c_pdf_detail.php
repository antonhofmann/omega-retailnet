<?php
/********************************************************************

    cer_inr02c_pdf_detail.php

    Print CER versus LN Figures Detail Page (Form INR-02C).

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2016-01-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2016-01-15
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.
*********************************************************************/
include("include/in_project_data.php");

//set up years

//compose dataset for sales data
$cer_ln_years = array();
$cer_ln_years_first_year = 99999999;
$cer_first_year = "";
$ln_first_year = "";
$cer_last_year = "";
$ln_last_year = "";

if(count($sales_units_watches_values) > 0)
{
	end($sales_units_watches_values);         
	$cer_last_year = key($sales_units_watches_values); 
	
	reset($sales_units_watches_values);
	$cer_first_year = key($sales_units_watches_values);
}

if(count($ln_sales_units_watches_values) > 0)
{
	end($ln_sales_units_watches_values);         
	$ln_last_year = key($ln_sales_units_watches_values); 
	

	reset($ln_sales_units_watches_values);
	$ln_first_year = key($ln_sales_units_watches_values);
}


foreach($sales_units_watches_values as $year=>$value)
{
	$cer_ln_years[$year] = $year;
	if($year < $cer_ln_years_first_year)
	{
		$cer_ln_years_first_year = $year;
	}
}
foreach($ln_sales_units_watches_values as $year=>$value)
{
	$cer_ln_years[$year] = $year;
	if($year < $cer_ln_years_first_year)
	{
		$cer_ln_years_first_year = $year;
	}
}



//set pdf parameters
$margin_top = 10;
$margin_left = 10;
$y = $margin_top;

$pdf->AddPage();






//general information
if(!function_exists('print_inr_02c_general_information'))
{
	function print_inr_02c_general_information($pdf, $project_number, $project_name, $legal_entity, $pos_type_name, $project_kind , $legal_type_name, $pos_country, $pos_city, $pos_address, $project_manager, $relocated_pos_name)
	{
		$margin_top = 10;
		$margin_left = 10;
		$y = $margin_top;
		
		// Title first line
		
		$pdf->SetFont("arialn", "B", 11);
		$pdf->Cell(275, 6, "", 1);

		$pdf->SetXY($margin_left,$margin_top);

		$pdf->Cell(235, 6, "Retail Business Plan Overview for Project " . $project_number . ": " . $project_name , 0, "", "L");
		$pdf->SetFont("arialn", "", 10);

		$pdf->Cell(20, 6, date("d.m.Y"), 1, "", "C");

		$pdf->Cell(20, 6, "INR-02C", 1, "", "C");
		
		$pdf->SetXY($margin_left,$y+7);
		$pdf->Cell(275, 13, "", 1);

		$y = $y+8;
		$x = $margin_left;

		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 10);
		$pdf->Cell(20, 3.5, "General information", 0, "", "L");

		//row 1
		$y = $y + 4.5;

		$pdf->SetXY($margin_left,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(20, 3.5, "Brand: " . BRAND, 0, "", "L");

		$pdf->SetXY($margin_left+106,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(20, 3.5, "Legal Entity: " . $legal_entity, 0, "", "L");

		if($relocated_pos_name)
		{
			//row 2a
			$pdf->SetXY($margin_left+190,$y-3.5);
			$pdf->SetFont("arialn", "B", 8);
			$pdf->Cell(20, 3.5, "Project Classification: " . $pos_type_name . "/" . $project_kind . "/" . $legal_type_name, 0, "", "L");
			
			
			$y = $y;
			$pdf->SetXY($margin_left+190,$y);
			$pdf->SetFont("arialn", "B", 8);
			$pdf->Cell(20, 3.5, "Relocated POS: " . $relocated_pos_name, 0, "", "L");

		}
		else
		{
			$pdf->SetXY($margin_left+190,$y);
			$pdf->SetFont("arialn", "B", 8);
			$pdf->Cell(20, 3.5, "Project Classification: " . $pos_type_name . "/" . $project_kind . "/" . $legal_type_name, 0, "", "L");
		}

		//row 2b
		$y = $y + 3.5;
		$pdf->SetXY($margin_left,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(20, 3.5, "Country/City: " . $pos_country . '/' . $pos_city, 0, "", "L");



		$pdf->SetXY($margin_left+106,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(20, 3.5, "Address: " . $pos_address, 0, "", "L");


		$pdf->SetXY($margin_left+190,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(20, 3.5, "Project Leader: " . $project_manager, 0, "", "L");
		
		$y = $y + 4;

		return $y;
	}

}
$y = print_inr_02c_general_information($pdf, $project_number, $project_name, $legal_entity, $pos_type_name, $project_kind , $legal_type_name, $pos_country, $pos_city, $pos_address, $project_manager, $relocated_pos_name);

// SALES DATA

$standard_y = 3.5;


$is_fist_page = 1;
if(count($cer_ln_years) > 0 and $cer_ln_years_first_year > 2000)
{
	$number_of_years_per_data_set = 4;
	$number_of_data_sets = ceil(count($cer_ln_years) / $number_of_years_per_data_set);

	$tmp_first_year = $cer_ln_years_first_year-$number_of_years_per_data_set;


	for($i=1;$i<=$number_of_data_sets;$i++)
	{
		$tmp_first_year = $tmp_first_year+$number_of_years_per_data_set;
	
		
		if($i==5 and $is_fist_page == 1)
		{
			$pdf->AddPage();
			$y = print_inr_02c_general_information($pdf, $project_number, $project_name, $legal_entity, $pos_type_name, $project_kind , $legal_type_name, $pos_country, $pos_city, $pos_address, $project_manager, $relocated_pos_name);

			$y = $y+2;

			$is_fist_page = 0;
		}
		else
		{
			$y = $pdf->GetY() + 6;
		}

		$pdf->SetXY($margin_left,$y);
		$pdf->SetFont("arialn", "B", 10);
		$pdf->Cell(75, 6, "Sales Data: CER Figures versus LN Figures in T" . $currency_symbol, 1, "", "L");

		for($k=0;$k<$number_of_years_per_data_set;$k++)
		{
			$year = $tmp_first_year+$k;
			if(array_key_exists($year, $cer_ln_years))
			{
				
				if($year == $cer_first_year and $year == $ln_first_year)
				{
					$tmp = $year . " - CER=" . $number_of_months_first_year . "m, LN=" . $ln_number_of_months_first_year . "m";
					$pdf->Cell(50, 6, $tmp, 1, "", "C");
				}
				elseif($year == $cer_first_year)
				{
					$tmp = $year . " - CER=" . $number_of_months_first_year. "m";
					$pdf->Cell(50, 6, $tmp, 1, "", "C");
				}
				elseif($year == $cer_first_year)
				{
					$tmp = $year . " - LN=" . $ln_number_of_months_first_year. "m";
					$pdf->Cell(50, 6, $tmp, 1, "", "C");
				}
				elseif($year == $cer_last_year and $year == $ln_last_year)
				{
					$tmp = $year . " - CER=" . $number_of_months_last_year . "m, LN=" . $ln_number_of_months_last_year . "m";
					$pdf->Cell(50, 6, $tmp, 1, "", "C");
				}
				elseif($year == $cer_last_year)
				{
					$tmp = $year . " - CER=" . $number_of_months_last_year. "m";
					$pdf->Cell(50, 6, $tmp, 1, "", "C");
				}
				elseif($year == $ln_last_year)
				{
					$tmp = $tmp_first_year+$k . " - LN=" . $ln_number_of_months_last_year. "m";
					$pdf->Cell(50, 6, $tmp, 1, "", "C");
				}
				else
				{
					$pdf->Cell(50, 6, $year, 1, "", "C");
				}
			}
		}
		
		$y = $y + 6;

		$pdf->SetXY($margin_left,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(75, 3.5, "", 1, "", "R");

		for($k=0;$k<$number_of_years_per_data_set;$k++)
		{
			$year = $tmp_first_year+$k;
			if(array_key_exists($year, $cer_ln_years))
			{
				$pdf->Cell(20, 3.5, "CER (INR-02B)", 1, "", "R");
				$pdf->Cell(19, 3.5, "LN (LNR-02A)", 1, "", "R");
				$pdf->Cell(11, 3.5, "Delta", 1, "", "R");
			}
		}

		$pdf->Ln();
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(75, 3.5, "Watches Units", 1, "", "L");

		for($k=0;$k<$number_of_years_per_data_set;$k++)
		{	
			$year = $tmp_first_year+$k;
			$delta = "";
			if(array_key_exists($year, $cer_ln_years))
			{
				if(array_key_exists($year, $sales_units_watches_values))
				{
					$pdf->Cell(20, 3.5, $sales_units_watches_values[$year], 1, "", "R");
				}
				else
				{
					$pdf->Cell(20, 3.5, "-", 1, "", "R");
				}


				if(array_key_exists($year, $ln_sales_units_watches_values))
				{
					$pdf->Cell(19, 3.5, $ln_sales_units_watches_values[$year], 1, "", "R");

					if(array_key_exists($year, $sales_units_watches_values) and $ln_sales_units_watches_values[$year] > 0)
					{
						$delta = number_format(round((100*$sales_units_watches_values[$year] / $ln_sales_units_watches_values[$year]) - 100, 1), 1) . "%";
					}
				}
				else
				{
					$pdf->Cell(19, 3.5, "-", 1, "", "R");
				}

				

				$pdf->Cell(11, 3.5, $delta, 1, "", "R");
			}
		}
		$pdf->Ln();
		$pdf->Cell(75, 3.5, "Jewellery Units", 1, "", "L");

		for($k=0;$k<$number_of_years_per_data_set;$k++)
		{	
			$year = $tmp_first_year+$k;
			$delta = "";
			if(array_key_exists($year, $cer_ln_years))
			{
				if(array_key_exists($year, $sales_units_jewellery_values))
				{
					$pdf->Cell(20, 3.5, $sales_units_jewellery_values[$year], 1, "", "R");

				}
				else
				{
					$pdf->Cell(20, 3.5, "-", 1, "", "R");
				}


				if(array_key_exists($year, $ln_sales_units_jewellery_values))
				{
					$pdf->Cell(19, 3.5, $ln_sales_units_jewellery_values[$year], 1, "", "R");

					if(array_key_exists($year, $sales_units_jewellery_values) and $ln_sales_units_jewellery_values[$year] > 0)
					{
						$delta = number_format(round((100*$sales_units_jewellery_values[$year] / $ln_sales_units_jewellery_values[$year])- 100, 1), 1) . "%";
					}
				}
				else
				{
					$pdf->Cell(19, 3.5, "-", 1, "", "R");
				}

				$pdf->Cell(11, 3.5, $delta, 1, "", "R");
			}
		}

		$pdf->Ln();
		
		
		$pdf->Cell(75, 3.5, "Accessories Amount", 1, "", "L");
		for($k=0;$k<$number_of_years_per_data_set;$k++)
		{	
			$year = $tmp_first_year+$k;
			$delta = "";
			if(array_key_exists($year, $cer_ln_years))
			{
				if(array_key_exists($year, $sales_accessories_values))
				{
					$pdf->Cell(20, 3.5, round($sales_accessories_values[$year]/1000, 0), 1, "", "R");
				}
				else
				{
					$pdf->Cell(20, 3.5, "-", 1, "", "R");
				}


				if(array_key_exists($year, $ln_sales_accessories_values))
				{
					$pdf->Cell(19, 3.5, round($ln_sales_accessories_values[$year]/1000, 0), 1, "", "R");

					if(array_key_exists($year, $sales_accessories_values) and $ln_sales_accessories_values[$year] > 0)
					{
						$delta = number_format(round((100*$sales_accessories_values[$year] / $ln_sales_accessories_values[$year]) - 100, 1), 1) . "%";
					}
				}
				else
				{
					$pdf->Cell(19, 3.5, "-", 1, "", "R");
				}

				$pdf->Cell(11, 3.5, $delta, 1, "", "R");
			}
		}

		
		$pdf->Ln();
		$pdf->Cell(75, 3.5, "Customer Service Amount", 1, "", "L");
		for($k=0;$k<$number_of_years_per_data_set;$k++)
		{	
			$year = $tmp_first_year+$k;
			$delta = "";
			if(array_key_exists($year, $cer_ln_years))
			{
				if(array_key_exists($year, $sales_customer_service_values))
				{
					$pdf->Cell(20, 3.5, round($sales_customer_service_values[$year]/1000, 0), 1, "", "R");
				}
				else
				{
					$pdf->Cell(20, 3.5, "-", 1, "", "R");
				}


				if(array_key_exists($year, $ln_sales_customer_service_values))
				{
					$pdf->Cell(19, 3.5, round($ln_sales_customer_service_values[$year]/1000, 0), 1, "", "R");

					if(array_key_exists($year, $sales_customer_service_values) and $ln_sales_customer_service_values[$year] > 0)
					{
						$delta = number_format(round((100*$sales_customer_service_values[$year] / $ln_sales_customer_service_values[$year]) - 100, 1), 1) . "%";
					}
				}
				else
				{
					$pdf->Cell(19, 3.5, "-", 1, "", "R");
				}

				$pdf->Cell(11, 3.5, $delta, 1, "", "R");
			}
		}
	

		$pdf->SetFont("arialn", "B", 8);
		$pdf->Ln();
		$pdf->SetXY($pdf->GetX(), $pdf->GetY()+1);
		$pdf->Cell(75, 3.5, "Net Sales", 1, "", "L");

		for($k=0;$k<$number_of_years_per_data_set;$k++)
		{	
			$year = $tmp_first_year+$k;
			$delta = "";
			if(array_key_exists($year, $cer_ln_years))
			{
				if(array_key_exists($year, $total_net_sales_values))
				{
					$pdf->Cell(20, 3.5, round(($exchange_rate*$total_net_sales_values[$year]/$exchange_rate_factor) / 1000,0), 1, "", "R");
				}
				else
				{
					$pdf->Cell(20, 3.5, "-", 1, "", "R");
				}


				if(array_key_exists($year, $total_net_sales_values) and array_key_exists($year, $ln_total_net_sales_values))
				{
					$pdf->Cell(19, 3.5, round(($ln_exchange_rate*$ln_total_net_sales_values[$year]/$ln_exchange_rate_factor) / 1000,0), 1, "", "R");

					if($ln_total_net_sales_values[$year] > 0)
					{
						$delta = number_format(round((100*($exchange_rate*$total_net_sales_values[$year]/$exchange_rate_factor) / ($ln_exchange_rate*$ln_total_net_sales_values[$year]/$ln_exchange_rate_factor)) - 100, 1), 1) . "%";
					}
				}
				else
				{
					$pdf->Cell(19, 3.5, "-", 1, "", "R");
				}
				
				$pdf->SetFont("arialn", "", 8);
				$pdf->Cell(11, 3.5, $delta, 1, "", "R");
				$pdf->SetFont("arialn", "B", 8);
			}
		}

		$pdf->Ln();
		$pdf->Cell(75, 3.5, "Operating Income (incl. WSM: CER = " . $weighted_average_whole_sale_margin . "%, LN=" . $ln_wholesale_margin . "%)", 1, "", "L");

		
		for($k=0;$k<$number_of_years_per_data_set;$k++)
		{	
			$year = $tmp_first_year+$k;
			$delta = "";
			if(array_key_exists($year, $cer_ln_years))
			{
				if(array_key_exists($year, $operating_income02_values))
				{
					$pdf->Cell(20, 3.5, round(($exchange_rate*$operating_income02_values[$year]/$exchange_rate_factor) / 1000,0), 1, "", "R");
				}
				else
				{
					$pdf->Cell(20, 3.5, "-", 1, "", "R");
				}


				if(array_key_exists($year, $operating_income02_values) and array_key_exists($year, $ln_operating_income02_values))
				{
					$pdf->Cell(19, 3.5, round(($ln_exchange_rate*$ln_operating_income02_values[$year]/$ln_exchange_rate_factor) / 1000,0), 1, "", "R");

					if($ln_operating_income02_values[$year] != 0)
					{
						
						if($operating_income02_values[$year] < 0 
							and $ln_operating_income02_values[$year] < 0)
						{
							$delta = round
								(100*
									(
										(
											($ln_exchange_rate*$ln_operating_income02_values[$year]/$ln_exchange_rate_factor)-
											($exchange_rate*$operating_income02_values[$year]/$exchange_rate_factor)
										)
										/ 
										($ln_exchange_rate*$ln_operating_income02_values[$year]/$ln_exchange_rate_factor)
									)
								, 1
								) . "%";
						}
						else
						{
							$delta = number_format(round((100*($exchange_rate*$operating_income02_values[$year]/$exchange_rate_factor) / ($ln_exchange_rate*$ln_operating_income02_values[$year]/$ln_exchange_rate_factor)) - 100, 1), 1) . "%";
						}
					}
				}
				else
				{
					$pdf->Cell(19, 3.5, "-", 1, "", "R");
				}

				$pdf->SetFont("arialn", "", 8);
				$pdf->Cell(11, 3.5, $delta, 1, "", "R");
				$pdf->SetFont("arialn", "B", 8);
			}
		}



		$pdf->Ln();
		$pdf->Cell(75, 3.5, "Operating income incl. WHS margin in %", 1, "", "L");

		
		for($k=0;$k<$number_of_years_per_data_set;$k++)
		{	
			$year = $tmp_first_year+$k;
			$delta = "";
			if(array_key_exists($year, $cer_ln_years))
			{
				if(array_key_exists($year, $operating_income02_values) and $total_net_sales_values[$year] > 0)
				{
					$pdf->Cell(20, 3.5, round(100*($exchange_rate*$operating_income02_values[$year]/$exchange_rate_factor)/($ln_exchange_rate*$total_net_sales_values[$year]/$ln_exchange_rate_factor),1) . "%", 1, "", "R");
				}
				else
				{
					$pdf->Cell(20, 3.5, "-", 1, "", "R");
				}


				if(array_key_exists($year, $ln_operating_income02_values) and $ln_total_net_sales_values[$year] > 0)
				{
					$pdf->Cell(19, 3.5, round(100*($ln_exchange_rate*$ln_operating_income02_values[$year]/$ln_exchange_rate_factor)/($ln_exchange_rate*$ln_total_net_sales_values[$year]/$ln_exchange_rate_factor),1) . "%", 1, "", "R");
				}
				else
				{
					$pdf->Cell(19, 3.5, "-", 1, "", "R");
				}

				$pdf->SetFont("arialn", "", 8);
				$pdf->Cell(11, 3.5, $delta, 1, "", "R");
				$pdf->SetFont("arialn", "B", 8);
			}
		}
	}
}




//INVESTMENTS

if($pdf->GetY() > 150)
{
	$pdf->AddPage();

	$y = print_inr_02c_general_information($pdf, $project_number, $project_name, $legal_entity, $pos_type_name, $project_kind , $legal_type_name, $pos_country, $pos_city, $pos_address, $project_manager, $relocated_pos_name);
}

$y = $pdf->GetY() + 6;

$pdf->SetXY($margin_left,$y);
$pdf->SetFont("arialn", "B", 10);
$pdf->Cell(75, 6, "Investment: CER Figures versus LN Figures in T" . $currency_symbol, 1, "", "L");

if(array_key_exists("cid", $_GET) and $_GET["cid"] ==  "s")
{
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(90, 6, "Exchange Rate CER = " . $exchange_rate . ", Exchange Rate LN = " . $ln_exchange_rate, 1, "" , "L");
}
else
{
	$pdf->Cell(90, 6, "", 1, "" , "L");
}
$y = $y + 6;


//draw boxes for investments
$pdf->SetXY($margin_left,$y);
$pdf->SetFont("arialn", "B", 8);
$pdf->Cell(75, 40.2, "", 1, "", "L");


$pdf->SetXY($margin_left+75,$y);
$pdf->Cell(20, 40.2, "", 1, "", "L");

$pdf->SetXY($margin_left+95,$y);
$pdf->Cell(15, 40.2, "", 1, "", "L");

$pdf->SetXY($margin_left+110,$y);
$pdf->Cell(20, 40.2, "", 1, "", "L");


$pdf->SetXY($margin_left+130,$y);
$pdf->Cell(15, 40.2, "", 1, "", "L");


$pdf->SetXY($margin_left+145,$y);
$pdf->Cell(20, 40.2, "", 1, "", "L");

$x1 = $margin_left+1;
$x2 = $margin_left+75;

$y = $y + 1;
$y2 = $y;   // for column2


$pdf->SetXY($margin_left,$y);
$pdf->SetFont("arialn", "B", 9);
$pdf->Cell(20, 3.5, "Project cost:", 0, "", "L");


$pdf->SetXY($x2,$y);
$pdf->SetFont("arialn", "B", 8);
$pdf->Cell(20, 3.5, "CER (INR-02B)", 1, "", "R");
$pdf->Cell(15, 3.5, "Share", 1, "", "R");
$pdf->Cell(20, 3.5, "LN (LNR-02A)", 1, "", "R");
$pdf->Cell(15, 3.5, "Share", 1, "", "R");
$pdf->Cell(20, 3.5, "Delta CER/LN", 1, "", "R");

foreach($fixed_assets as $key=>$itype)
{

	if(array_key_exists($itype, $amounts))
	{
		if(!$amounts[$itype]){
			$amounts[$itype] = "-";
			
		}

		$y = $y + $standard_y;
		$pdf->SetXY($x1,$y);
		$pdf->SetFont("arialn", "", 8);

		$pdf->Cell(63, 3.5, $investment_names[$itype] , 0, "", "L");

		$pdf->SetXY($x2,$y);
		$pdf->SetFont("arialn", "", 8);
		
		
		if($amounts[$itype] != "-" and $amounts[$itype] != 0)
		{
			$pdf->Cell(20, 3.5, round(($exchange_rate*$amounts[$itype]/$exchange_rate_factor) / 1000,0), 0, "", "R");

			if($investment_total > 0)
			{
				
				$pdf->Cell(15, 3.5, round(($exchange_rate*$amounts[$itype]/$exchange_rate_factor)/($exchange_rate*$investment_total/$exchange_rate_factor), 2)*100 . "%", 0, "", "R");
			}
			else
			{
				$pdf->Cell(15, 3.5, "-", 0, "", "R");
			}
		}
		else
		{
			$pdf->Cell(20, 3.5, "-", 0, "", "R");
			$pdf->Cell(15, 3.5, "-", 0, "", "R");
		}

		if(array_key_exists($itype, $ln_amounts) and $ln_amounts[$itype] != "-" and $ln_amounts[$itype] != 0)
		{
			$pdf->Cell(20, 3.5, round(($ln_exchange_rate*$ln_amounts[$itype]/$ln_exchange_rate_factor) / 1000,0), 0, "", "R");

			if($ln_investment_total > 0)
			{
				$pdf->Cell(15, 3.5, round(($ln_exchange_rate*$ln_amounts[$itype]/$ln_exchange_rate_factor)/($ln_exchange_rate*$ln_investment_total/$ln_exchange_rate_factor), 2)*100 . "%", 0, "", "R");
			}
			else
			{
				$pdf->Cell(15, 3.5, "-", 0, "-", "R");
			}
		}
		else
		{
			$pdf->Cell(20, 3.5, "-", 0, "", "R");
			$pdf->Cell(15, 3.5, "-", 0, "", "R");
		}


		if(array_key_exists($itype, $ln_amounts) and $ln_amounts[$itype] > 0)
		{
			$delta = number_format(round((100*($exchange_rate*$amounts[$itype]/$exchange_rate_factor) / ($ln_exchange_rate*$ln_amounts[$itype]/$ln_exchange_rate_factor)) - 100, 1), 1) . "%";
			$pdf->Cell(20, 3.5, $delta, 0, "-", "R");
		}
		else
		{
			$pdf->Cell(20, 3.5, "-", 0, "-", "R");
		}
	}
	else
	{
		$y = $y + $standard_y;
	}
}

$y = $y + $standard_y;
$pdf->SetXY($x1,$y);
$pdf->SetFont("arialn", "B", 8);
$pdf->Cell(20, 3.5, "Total Investment in Fixed Assets", 0, "", "L");

$pdf->SetXY($x2,$y);
$pdf->SetFont("arialn", "B", 8);

$pdf->Cell(20, 3.5, round(($exchange_rate*$investment_total/$exchange_rate_factor) / 1000,0), 1, "", "R");
$pdf->Cell(15, 3.5, "", 1, "", "R");
$pdf->Cell(20, 3.5, round(($ln_exchange_rate*$ln_investment_total/$ln_exchange_rate_factor) / 1000,0), 1, "", "R");
$pdf->Cell(15, 3.5, "", 1, "", "R");

if($ln_investment_total > 0)
{
	$delta = number_format(round((100*($exchange_rate*$investment_total/$exchange_rate_factor) / ($ln_exchange_rate*$ln_investment_total/$ln_exchange_rate_factor)) - 100, 1), 1) . "%";
	$pdf->Cell(20, 3.5, $delta, 1, "-", "R");
}
else
{
	$pdf->Cell(20, 3.5, "", 1, "", "R");
}

/*INTANGIBLES*/

$y = $y + $standard_y;
$pdf->SetXY($x1,$y);
$pdf->SetFont("arialn", "", 8);

$pdf->Cell(20, 3.5, $intagible_name, 0, "", "L");

$pdf->SetXY($x2,$y);
$pdf->SetFont("arialn", "", 8);

if($intagible_amount != 0)
{
	$pdf->Cell(20, 3.5, round(($exchange_rate*$intagible_amount/$exchange_rate_factor) / 1000,0), 0, "", "R");
}
else
{
	$pdf->Cell(20, 3.5, "-", 0, "", "R");
}
$pdf->Cell(15, 3.5, "-", 0, "", "R");

if($ln_intagible_amount != 0)
{
	$pdf->Cell(20, 3.5, round(($ln_exchange_rate*$ln_intagible_amount/$ln_exchange_rate_factor) / 1000,0), 0, "", "R");
}
else
{
	$pdf->Cell(20, 3.5, "-", 0, "", "R");
}

$pdf->Cell(15, 3.5, "-", 0, "", "R");

if($ln_intagible_amount > 0)
{
	$delta = number_format(round((100*($exchange_rate*$intagible_amount/$exchange_rate_factor) / ($ln_exchange_rate*$ln_intagible_amount/$ln_exchange_rate_factor)) - 100, 1), 1) . "%";
	$pdf->Cell(20, 3.5, $delta, 0, "-", "R");
}
else
{
	if($intagible_amount > 0)
	{
		$pdf->Cell(20, 3.5, "100.0%", 0, "-", "R");
	}
	else
	{
		$pdf->Cell(20, 3.5, "-", 0, "-", "R");
	}
}

//$pdf->SetXY($x3,$y);
//$pdf->SetFont("arialn", "", 8);


$y = $y + $standard_y;
$pdf->SetXY($x1,$y);
$pdf->SetFont("arialn", "", 8);

if(array_key_exists(9, $investment_names))
{
	$pdf->Cell(20, 3.5, $investment_names[9], 0, "", "L");
}
else
{
	$pdf->Cell(20, 3.5,"", 0, "", "L");
}

$pdf->SetXY($x2,$y);
$pdf->SetFont("arialn", "", 8);


if(array_key_exists(9, $amounts))
{
	if($amounts[9] != 0)
	{
		$pdf->Cell(20, 3.5, round(($exchange_rate*$amounts[9]/$exchange_rate_factor) / 1000,0), 0, "", "R");
	}
	else
	{
		$pdf->Cell(20, 3.5, "-", 0, "", "R");
	}
}
else
{
	$pdf->Cell(20, 3.5, "", 0, "", "R");
}
$pdf->Cell(15, 3.5, "-", 0, "", "R");

if(array_key_exists(9, $ln_amounts))
{
	if($ln_amounts[9] != 0)
	{
		$pdf->Cell(20, 3.5, round(($ln_exchange_rate*$ln_amounts[9]/$ln_exchange_rate_factor) / 1000,0), 0, "", "R");
	}
	else
	{
		$pdf->Cell(20, 3.5, "-", 0, "", "R");
	}
}
else
{
	$pdf->Cell(20, 3.5, "", 0, "", "R");
}
$pdf->Cell(15, 3.5, "-", 0, "", "R");

if(array_key_exists(9, $ln_amounts) and $ln_amounts[9] > 0)
{
	$delta = number_format(round((100*($exchange_rate*$amounts[9]/$exchange_rate_factor) / ($ln_exchange_rate*$ln_amounts[9]/$ln_exchange_rate_factor)) - 100, 1), 1) . "%";
	$pdf->Cell(20, 3.5, $delta, 0, "-", "R");
}
else
{
	if($amounts[9] > 0)
	{
		$pdf->Cell(20, 3.5, "100.0%", 0, "-", "R");
	}
	else
	{
		$pdf->Cell(20, 3.5, "-", 0, "-", "R");
	}
}

$y = $y + $standard_y;
$pdf->SetXY($x1,$y);
$pdf->SetFont("arialn", "", 8);

if(array_key_exists(13, $ln_amounts) and array_key_exists(13, $investment_names))
{
	$pdf->Cell(20, 3.5, $investment_names[13], 0, "", "L");


}
else
{
	$pdf->Cell(20, 3.5, "", 0, "", "L");
}


$pdf->SetXY($x2,$y);
$pdf->SetFont("arialn", "", 8);

if(array_key_exists(13, $amounts))
{
	if(array_key_exists(13, $ln_amounts) and $amounts[13] != 0)
	{
		$pdf->Cell(20, 3.5, round(($exchange_rate*$amounts[13]/$exchange_rate_factor) / 1000,0), 0, "", "R");
	}
	else
	{
		$pdf->Cell(20, 3.5, "-", 0, "", "R");
	}
}
else
{
	$pdf->Cell(20, 3.5, "", 0, "", "R");
}
$pdf->Cell(15, 3.5, "-", 0, "", "R");

if(array_key_exists(13, $ln_amounts))
{
	if($ln_amounts[13] != 0)
	{
		$pdf->Cell(20, 3.5, round(($ln_exchange_rate*$ln_amounts[13]/$ln_exchange_rate_factor) / 1000,0), 0, "", "R");
	}
	else
	{
		$pdf->Cell(20, 3.5, "-", 0, "", "R");
	}
}
else
{
	$pdf->Cell(20, 3.5, "", 0, "", "R");
}
$pdf->Cell(15, 3.5, "-", 0, "", "R");

if(array_key_exists(13, $ln_amounts) and $ln_amounts[13] > 0)
{
	$delta = number_format(round((100*($exchange_rate*$amounts[13]/$exchange_rate_factor) / ($ln_exchange_rate*$ln_amounts[13]/$ln_exchange_rate_factor)) - 100, 1), 1) . "%";
	$pdf->Cell(20, 3.5, $delta, 0, "-", "R");
}
else
{
	if(array_key_exists(13, $ln_amounts) and $amounts[13] > 0)
	{
		$pdf->Cell(20, 3.5, "100.0%", 0, "-", "R");
	}
	else
	{
		$pdf->Cell(20, 3.5, "-", 0, "-", "R");
	}
}


$y = $y+ $standard_y;



$pdf->SetXY($margin_left,$y);
$pdf->SetFont("arialn", "B", 9);
$pdf->Cell(50, 3.5, "Total Project costs (Requested amount)", 0, "", "L");

$pdf->SetXY($x2,$y);
$pdf->SetFont("arialn", "B", 9);
$pdf->Cell(20, 3.5, round(($exchange_rate*$cer_totals/$exchange_rate_factor) / 1000,1), 1, "", "R");
$pdf->Cell(15, 3.5, "", 1, "", "R");
$pdf->Cell(20, 3.5, round(($ln_exchange_rate*$ln_totals/$ln_exchange_rate_factor) / 1000,1), 1, "", "R");
$pdf->Cell(15, 3.5, "", 1, "", "R");


if($ln_totals > 0)
{
	$delta = number_format(round((100*($exchange_rate*$cer_totals/$exchange_rate_factor) / ($ln_exchange_rate*$ln_totals/$ln_exchange_rate_factor)) - 100 , 1), 1) . "%";
	$pdf->Cell(20, 3.5, $delta, 1, "-", "R");
}
else
{
	$pdf->Cell(20, 3.5, "-", 1, "-", "R");
}


//future investments
if($cer_basicdata['cer_basicdata_future_investment'] > 0) {
	$y = $y+ $standard_y + 1.5;
	$pdf->SetXY($margin_left,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(75, 3.5, "Planned Future Investment for Renovation", 1, "", "L");

	$pdf->SetXY($x2,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(20, 3.5, round(($cer_basicdata['cer_basicdata_future_investment']) / 1000,1), 1, "", "R");
	$pdf->Cell(15, 3.5, "", 1, "", "R");
	$pdf->Cell(20, 3.5, round(($ln_future_investment) / 1000,1), 1, "", "R");
	$pdf->Cell(15, 3.5, "", 1, "", "R");


	if($ln_totals > 0)
	{
		$delta = number_format(round((100*($cer_basicdata['cer_basicdata_future_investment']) / ($ln_future_investment)) - 100 , 1), 1) . "%";
		$pdf->Cell(20, 3.5, $delta, 1, "-", "R");
	}
	else
	{
		$pdf->Cell(20, 3.5, "-", 1, "-", "R");
	}
}

//$pdf->Cell(20, 3.5, "-", 1, "-", "R");

$predecessor_printed = false;

/*
if($cer_basicdata["cer_basicdata_cer_versus_ln_comment"])
{
	if($pdf->getY() > 160)
	{
		$pdf->AddPage();
	}
	$pdf->SetAutoPageBreak(true, 10);
	$pdf->ln();
	$pdf->ln();
	
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(50, 3.5, "Justification for Difference in Investments", 0, "", "L");
	$pdf->ln();
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(275,0, $cer_basicdata["cer_basicdata_cer_versus_ln_comment"], 1, "T");
	$predecessor_printed = true;

}


if($cer_basicdata["cer_basicdata_cer_versus_ln_comment2"])
{
	$pdf->setY($pdf->getY()+3);
	if($pdf->getY() > 160)
	{
		$pdf->AddPage();
	}
	$pdf->SetAutoPageBreak(true, 10);
	if($predecessor_printed == false)
	{
		$pdf->ln();
		$pdf->ln();
	}
	
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(50, 3.5, "Justification for Difference in Revenues", 0, "", "L");
	$pdf->ln();
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(275,0, $cer_basicdata["cer_basicdata_cer_versus_ln_comment2"], 1, "T");
	$predecessor_printed = true;
	
}

if($cer_basicdata["cer_basicdata_cer_versus_ln_comment3"])
{	
	$pdf->setY($pdf->getY()+3);
	if($pdf->getY() > 160)
	{
		$pdf->AddPage();
	}
	$pdf->SetAutoPageBreak(true, 10);
	if($predecessor_printed == false)
	{
		$pdf->ln();
		$pdf->ln();
	}
	
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(50, 3.5, "Justification for Difference in Expenses", 0, "", "L");
	$pdf->ln();
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(275,0, $cer_basicdata["cer_basicdata_cer_versus_ln_comment3"], 1, "T");

}
*/


if($cer_basicdata["cer_basicdata_cer_versus_ln_comment4"])
{
	if($pdf->getY() > 160)
	{
		$pdf->AddPage();
	}
	$pdf->SetAutoPageBreak(true, 10);
	$pdf->ln();
	$pdf->ln();
	
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(50, 3.5, "Comment on deviation CER (INR02B) versus LNR (LNR-02A) - Country Comment", 0, "", "L");
	$pdf->ln();
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(275,0, $cer_basicdata["cer_basicdata_cer_versus_ln_comment4"], 1, "T");
	$predecessor_printed = true;

}


if($cer_basicdata["cer_basicdata_cer_versus_ln_comment5"])
{
	$pdf->setY($pdf->getY()+3);
	if($pdf->getY() > 160)
	{
		$pdf->AddPage();
	}
	$pdf->SetAutoPageBreak(true, 10);
	if($predecessor_printed == false)
	{
		$pdf->ln();
		$pdf->ln();
	}
	
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(50, 3.5, "Comment on deviation CER (INR02B) versus LNR (LNR-02A) - Brand HQ Comment", 0, "", "L");
	$pdf->ln();
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(275,0, $cer_basicdata["cer_basicdata_cer_versus_ln_comment5"], 1, "T");
	$predecessor_printed = true;
	
}


?>