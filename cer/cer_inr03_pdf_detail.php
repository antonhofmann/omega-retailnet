<?php
/********************************************************************

    cer_inr03_pdf_detail.php

    rint PDF for Request Retail Furniture for Third-Party POS (Form INR-03).

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2016-08-12
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2016-08-12
    Version:        1.0.0

    Copyright (c) 2016, Swatch AG, All Rights Reserved.
*********************************************************************/

//set defaul values
$client_is_hq_agent = 0;
$legal_entity_name = "";
$project_name = "";
$project_manager = "";
$requested_amount = "";
$budget_amount = "";
$currency_symbol = "";
$investment_amount = "";
$project_start = "";
$net_present_value = "";
$key_money = "";
$project_end = "";
$pay_back_period = "";
$deposit = "";
$post_compl_review = "";
$internal_reate_of_return ="";
$other_cost = "";
$project_kind = "";


$description = "";
$strategy = "";
$investment = "";
$benefits = "";
$alternative = "";
$risks = "";
$suppliers = "";

$name1 = "";
$name2 = "";
$name3 = "";
$name4 = "";
$name5 = "";
$name6 = "";
$name7 = "";
$name8 = "";
$name9 = "";

$date1 = "";
$date2 = "";
$date3 = "";
$date4 = "";
$date5 = "";
$date6 = "";
$date7 = "";
$date8 = "";
$date9 = "";


$protocol_number1 = "";
$protocol_number1 = "";
$cer_number = "";

$opening_date = "";

$project_kind_new = "";
$project_kind_renovation = "";
$project_kind_relocation = "";



// get basic data
$cer_basicdata = get_cer_basicdata(param("pid"), $cer_version);


$approval_name1 = $cer_basicdata["cer_basicdata_approvalname1"];
$approval_name2 = $cer_basicdata["cer_basicdata_approvalname2"];
$approval_name3 = $cer_basicdata["cer_basicdata_approvalname3"];
$approval_name4 = $cer_basicdata["cer_basicdata_approvalname4"];
$approval_name5 = $cer_basicdata["cer_basicdata_approvalname5"];
$approval_name6 = $cer_basicdata["cer_basicdata_approvalname6"];
$approval_name7 = $cer_basicdata["cer_basicdata_approvalname7"];
$approval_name8 = $cer_basicdata["cer_basicdata_approvalname8"];
$approval_name9 = $cer_basicdata["cer_basicdata_approvalname9"];
$approval_name10 = $cer_basicdata["cer_basicdata_approvalname10"];

$approval_name11 = $cer_basicdata["cer_summary_in01_sig01"];
$approval_name12 = $cer_basicdata["cer_summary_in01_sig02"];
$approval_name13 = $cer_basicdata["cer_summary_in01_sig03"];
$approval_name14 = $cer_basicdata["cer_summary_in01_sig04"];
$approval_name15 = $cer_basicdata["cer_summary_in01_sig05"];
$approval_name16 = $cer_basicdata["cer_summary_in01_sig06"];

$approval_name17 = $cer_basicdata["cer_summary_in01_sig10"];
$approval_name18 = $cer_basicdata["cer_summary_in01_sig07"];

// get all data needed from cer_basic_data

$sql = "select * from cer_summary where cer_summary_cer_version = " . $cer_version . "  and cer_summary_project = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{

	$description = $row["cer_summary_in01_description"];
	$strategy = $row["cer_summary_in01_strategy"];
	$investment = $row["cer_summary_in01_investment"];
	$benefits = $row["cer_summary_in01_benefits"];
	$alternative = $row["cer_summary_in01_alternative"];
	$risks = $row["cer_summary_in01_risks"];

	$name1 = $row["cer_summary_in01_sig01"];
	$name2 = $row["cer_summary_in01_sig02"];
	$name3 = $row["cer_summary_in01_sig03"];
	$name4 = $row["cer_summary_in01_sig04"];
	$name5 = $row["cer_summary_in01_sig05"];
	$name6 = $row["cer_summary_in01_sig06"];
	$name7 = $row["cer_summary_in01_sig07"];
	$name8 = $row["cer_summary_in01_sig08"];
	$name9 = $row["cer_summary_in01_sig09"];

	$name7 = $cer_basicdata["cer_basicdata_approvalname11"];

	$date1 = to_system_date($row["cer_summary_in01_date01"]);
	$date2 = to_system_date($row["cer_summary_in01_date02"]);
	$date3 = to_system_date($row["cer_summary_in01_date03"]);
	$date4 = to_system_date($row["cer_summary_in01_date04"]);
	$date5 = to_system_date($row["cer_summary_in01_date05"]);
	$date6 = to_system_date($row["cer_summary_in01_date06"]);
	$date7 = to_system_date($row["cer_summary_in01_date07"]);
	$date8 = to_system_date($row["cer_summary_in01_date08"]);
	$date9 = to_system_date($row["cer_summary_in01_date09"]);


	$protocol_number1 = $row["cer_summary_in01_prot01"];
	$protocol_number2 = $row["cer_summary_in01_prot02"];
	$cer_number = $row["cer_summary_in01_cernr"];

	
	if($row["cer_summary_in01_review_date"] and $row["cer_summary_in01_review_date"] != '0000-00-00')
	{
		$post_compl_review = substr($row["cer_summary_in01_review_date"], 5,2) . "/" . substr($row["cer_summary_in01_review_date"], 0,4);
	}
	
}

// get all data needed from project
$sql = "select * " . 
       "from projects " . 
	   "left join orders on order_id = project_order " .
	   "left join countries on country_id = order_shop_address_country " .
	   "left join product_lines on product_line_id = project_product_line " .
	   "left join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join projectkinds on projectkind_id = project_projectkind " .
       "left join addresses on address_id = order_client_address " .
       "left join users on user_id = project_retail_coordinator " . 
	   "where project_id = " . dbquote(param("pid"));


$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	
	if($row["address_legal_entity_name"])
	{
		$legal_entity_name = $row["address_legal_entity_name"];
	}
	else
	{
		$legal_entity_name = $row["address_company"];
	}
	
	$project_name = $row["order_shop_address_company"]. ", " . $row["order_shop_address_place"] . ", " . $row["country_name"];


	$project_manager = $row["user_firstname"] . " " . $row["user_name"];
	
	$project_kind = $row["projectkind_name"];


	
	$factor = $cer_basicdata['cer_basicdata_factor'];
	if(!$cer_basicdata['cer_basicdata_factor'])
	{
		$factor = 1;
	}

	$exchange_rate = 1;
	if($cer_basicdata['cer_basicdata_exchangerate'] > 0)
	{
		$exchange_rate = $cer_basicdata['cer_basicdata_exchangerate'];
	}
	
	
	if($cer_basicdata['cer_basicdata_exchangerate']) {
		$budget_amount = $row["project_approximate_budget"]/$cer_basicdata['cer_basicdata_exchangerate']*$factor;
		$budget_amount = number_format($budget_amount, 0, "", "'");
	}
	else {
		$budget_amount = '';
	}

	
	
	if($row["order_date"]) {
		$project_start = substr($row["order_date"], 5,2) . "/" . substr($row["order_date"], 0,4);
	}
	else {
		$project_start = '';
	}
	
	//$project_start = substr($cer_basicdata['cer_basicdata_project_start'], 5,2) . "/" . substr($cer_basicdata['cer_basicdata_project_start'], 0,4);
	
	//$tmp = date('Y-m-d H:i:s', strtotime($row["project_real_opening_date"] . ' + 90 day'));
	$tmp = $row["project_real_opening_date"];
	$project_end = substr($tmp, 5,2) . "/" . substr($tmp, 0,4);

	//$project_end = substr($cer_basicdata['cer_basicdata_project_end'], 5,2) . "/" . substr($cer_basicdata['cer_basicdata_project_end'], 0,4);

	$order_number = $row["project_order"];
	$address_country = $row["address_country"];

	if($row["project_real_opening_date"]) {
		$opening_date = to_system_date($row["project_real_opening_date"]);
	}
	else {
		$opening_date = '';
	}

	if($row["project_projectkind"] == 1)  {
		$project_kind_new = "X";
	}
	elseif($project["project_projectkind"] == 6)  {
		$project_kind_relocation = "X";
	}
	elseif($row["project_projectkind"] == 2 or $row["project_projectkind"] == 3)  {
		$project_kind_renovation = "X";
	}

	$client_is_hq_agent = $row["address_is_hq_agent"];
}

//get predecessor project
$predecessor_opening_date = '';
$sql = "select posorder_posaddress from posorders " . 
       " where posorder_order = " . dbquote($project["project_order"]);

$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	$predecessor_project = get_predecessor_project($row["posorder_posaddress"], $project["project_order"]);
	
	if(count($predecessor_project) > 0) {
		$predecessor_opening_date = to_system_date($predecessor_project["posorder_opening_date"]);
	}
}



//get suppliers
$tmp = array();
$sql = "select DISTINCT costsheet_company from costsheets " . 
       "left join pcost_groups on pcost_group_id = costsheet_pcost_group_id " .
	   " where costsheet_project_id = " . dbquote(param("pid")) . 
	   " and costsheet_is_in_budget = 1 " . 
	   " and pcost_group_posinvestment_type_id = 3 " . 
	   " and costsheet_company <> '' and costsheet_company is not null";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$tmp[] = $row["costsheet_company"];
}
if(count($tmp) > 0)
{
	$suppliers = implode(', ', $tmp);
}

$franchisee_address = get_address($project["order_franchisee_address_id"]);

$owner_company = $franchisee_address['company'];
if($franchisee_address['address']) {
	$owner_company .= ", " . $franchisee_address['address'];
}
$owner_company .= ", " . $franchisee_address['place'];
$owner_company .= ", " . $franchisee_address['country_name'];
$owner_company_sap_nr = $franchisee_address['address_sapnr'];

$client_address = get_address($project["order_client_address"]);

$client_currency = get_cer_currency(param("pid"), $cer_version);
$currency_symbol = $client_currency["symbol"];

//get milestones
$date_sis_approval = "";

$sql = "select project_milestone_milestone, project_milestone_date from project_milestones " . 
       "where project_milestone_milestone in(23) " . 
	   " and project_milestone_project = " . dbquote(param("pid"));
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($row["project_milestone_milestone"] == 23)
	{
		if($row["project_milestone_date"] == NULL or $row["project_milestone_date"] == '0000-00-00')
		{
			$date_sis_approval = '';
		}
		else
		{
			$date_sis_approval = to_system_date($row["project_milestone_date"]);
		}
	}
}


if($project["pipeline"] == 0)
{
	$pos_data = get_poslocation($project["posaddress_id"], "posaddresses");
}
elseif($project["pipeline"] == 1)
{
	$pos_data = get_poslocation_from_pipeline($project["posaddress_id"], $project["order_id"]);
}

//get financial data
include("include/in_financial_data.php");	



//CHF
$exr = $cer_basicdata["cer_basicdata_exchangerate"];
$exrf = $cer_basicdata["cer_basicdata_factor"];

$investment_amount_chf = round($exr*$investment_total/$exrf/1000,0);
$key_money_chf = round($exr*$intagibles_total/$exrf/1000, 0);
$deposit_chf = round($exr*$deposit/$exrf/1000, 0);
$other_noncapitalized_cost_chf = round($exr*$other_noncapitalized_cost/$exrf/1000, 0);

//$construction_total = $construction_total + $other_costs + $equipment_costs;

$construction_total_chf  = round($exr*$construction_total/$exrf/1000,0);
$fixed_assets_total_chf  = round($exr*$fixed_assets_total/$exrf/1000,0);


$architectural_total_chf  = round($exr*$architectural_total/$exrf/1000,0);
$equipment_total_chf  = round($exr*$equipment_total/$exrf/1000,0);
$other_costs_chf = round($exr*$other_costs/$exrf/1000, 0);

$merchandising_total_chf =  round($exr*$merchandising_total/$exrf/1000,0);
$transportation_total_chf =  round($exr*$transportation_total/$exrf/1000,0);

$investment_business_partner_chf = round($exr*$investment_total/$exrf*$cer_basicdata["cer_basicdata_franchsiee_investment_share"]/100/1000, 0);

$requested_amount_chf = $exr*$investment_total/$exrf - ($exr*$investment_total/$exrf*$cer_basicdata["cer_basicdata_franchsiee_investment_share"]/100);
$requested_amount_chf = round($requested_amount_chf/1000,0);


//calculate investmen shares


//local currency
$investment_amount = round($investment_total/1000,0);
$key_money = round($intagibles_total/1000, 0);
$deposit = round($deposit/1000, 0);
$other_noncapitalized_cost = round($other_noncapitalized_cost/1000, 0);

$construction_total  = round($construction_total/1000,0);
$fixed_assets_total  = round($fixed_assets_total/1000,0);

$architectural_total  = round($architectural_total/1000,0);
$equipment_total  = round($equipment_total/1000,0);

$merchandising_total =  round($merchandising_total/1000,0);
$transportation_total =  round($transportation_total/1000,0);

$other_costs =  round($other_costs/1000,0);

$investment_business_partner = round($investment_total*$cer_basicdata["cer_basicdata_franchsiee_investment_share"]/100/1000, 0);

$requested_amount1 = $investment_total - ($investment_total*$cer_basicdata["cer_basicdata_franchsiee_investment_share"]/100);
$requested_amount = round($requested_amount1/1000,0);


//get investmen shares of business partner in %
$investmen_shares_business_partner = array();
$investmen_shares_business_partner['furniture'] = array();
$investmen_shares_business_partner['posmaterial'] = array();
$investmen_shares_business_partner['construction'] = array();
$investmen_shares_business_partner['architectural'] = array();
$investmen_shares_business_partner['equipment'] = array();
$investmen_shares_business_partner['transportation'] = array();
$investmen_shares_business_partner['other'] = array();


$sql = "SELECT
		costsheets.costsheet_pcost_group_id,
		costsheets.costsheet_pcost_subgroup_id,
		costsheets.costsheet_text,
		costsheets.costsheet_partner_contribution,
		pcost_groups.pcost_group_name,
		pcost_groups.pcost_group_posinvestment_type_id,
		costsheet_budget_amount
		FROM
		costsheets
		INNER JOIN pcost_groups ON costsheets.costsheet_pcost_group_id = pcost_groups.pcost_group_id
		where costsheets.costsheet_project_id = " . dbquote($project['project_id']) . 
		" order by costsheet_pcost_group_id";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($row["costsheet_pcost_subgroup_id"] == 58) // transportation
	{
		$investmen_shares_business_partner['transportation'][] = array('amount'=>$row['costsheet_budget_amount'], 'share'=>$row['costsheet_partner_contribution']);
	}
	elseif($row["costsheet_pcost_subgroup_id"] == 56) // POS Material
	{
		$investmen_shares_business_partner['posmaterial'][] = array('amount'=>$row['costsheet_budget_amount'], 'share'=>$row['costsheet_partner_contribution']);
	}
	elseif($row["pcost_group_posinvestment_type_id"] == 1) // Construction
	{
		$investmen_shares_business_partner['construction'][] = array('amount'=>$row['costsheet_budget_amount'], 'share'=>$row['costsheet_partner_contribution']);
	}
	elseif($row["pcost_group_posinvestment_type_id"] == 3) // Furniture
	{
		$investmen_shares_business_partner['furniture'][] = array('amount'=>$row['costsheet_budget_amount'], 'share'=>$row['costsheet_partner_contribution']);
	}
	elseif($row["pcost_group_posinvestment_type_id"] == 5) // Architectural
	{
		$investmen_shares_business_partner['architectural'][] = array('amount'=>$row['costsheet_budget_amount'], 'share'=>$row['costsheet_partner_contribution']);
	}
	elseif($row["pcost_group_posinvestment_type_id"] == 7) // Equipment
	{
		$investmen_shares_business_partner['equipment'][] = array('amount'=>$row['costsheet_budget_amount'], 'share'=>$row['costsheet_partner_contribution']);
	}
	elseif($row["pcost_group_posinvestment_type_id"] == 11) // Other
	{
		$investmen_shares_business_partner['other'][] = array('amount'=>$row['costsheet_budget_amount'], 'share'=>$row['costsheet_partner_contribution']);
	}
}

$investmen_shares_business_partner_average = array();

$_tmp_total = 0;
$_tmp_share = 0;
foreach($investmen_shares_business_partner['transportation'] as $key=>$data) {
	$_tmp_total = $_tmp_total + $data['amount'];
	$_tmp_share = $_tmp_share + $data['amount']*$data['share']/100;
}

if($_tmp_share > 0 and $_tmp_total > 0) {
	$investmen_shares_business_partner_average['transportation'] = " (BPC " . round(100*$_tmp_share/$_tmp_total,2) . "%)*";
}
else
{
	$investmen_shares_business_partner_average['transportation'] = "";
}

$_tmp_total = 0;
$_tmp_share = 0;
foreach($investmen_shares_business_partner['posmaterial'] as $key=>$data) {
	$_tmp_total = $_tmp_total + $data['amount'];
	$_tmp_share = $_tmp_share + $data['amount']*$data['share']/100;
}
if($_tmp_share > 0 and $_tmp_total > 0) {
	$investmen_shares_business_partner_average['posmaterial'] = " (BPC " . round(100*$_tmp_share/$_tmp_total,2) . "%)*";
}
else
{
	$investmen_shares_business_partner_average['posmaterial'] = "";
}

$_tmp_total = 0;
$_tmp_share = 0;
foreach($investmen_shares_business_partner['construction'] as $key=>$data) {
	$_tmp_total = $_tmp_total + $data['amount'];
	$_tmp_share = $_tmp_share + $data['amount']*$data['share']/100;
}
if($_tmp_share > 0 and $_tmp_total > 0) {
	$investmen_shares_business_partner_average['construction'] = " (BPC " . round(100*$_tmp_share/$_tmp_total,2) . "%)*";
}
else
{
	$investmen_shares_business_partner_average['construction'] = "";
}



$_tmp_total = 0;
$_tmp_share = 0;
foreach($investmen_shares_business_partner['furniture'] as $key=>$data) {
	$_tmp_total = $_tmp_total + $data['amount'];
	$_tmp_share = $_tmp_share + $data['amount']*$data['share']/100;
}
if($_tmp_share > 0 and $_tmp_total > 0) {
	$investmen_shares_business_partner_average['furniture'] = " (BPC " . round(100*$_tmp_share/$_tmp_total,2) . "%)*";
}
else
{
	$investmen_shares_business_partner_average['furniture'] = "";
}

$_tmp_total = 0;
$_tmp_share = 0;
foreach($investmen_shares_business_partner['architectural'] as $key=>$data) {
	$_tmp_total = $_tmp_total + $data['amount'];
	$_tmp_share = $_tmp_share + $data['amount']*$data['share']/100;
}
if($_tmp_share > 0 and $_tmp_total > 0) {
	$investmen_shares_business_partner_average['architectural'] = " (BPC " . round(100*$_tmp_share/$_tmp_total,2) . "%)*";
}
else
{
	$investmen_shares_business_partner_average['architectural'] = "";
}

$_tmp_total = 0;
$_tmp_share = 0;
foreach($investmen_shares_business_partner['equipment'] as $key=>$data) {
	$_tmp_total = $_tmp_total + $data['amount'];
	$_tmp_share = $_tmp_share + $data['amount']*$data['share']/100;
}
if($_tmp_share > 0 and $_tmp_total > 0) {
	$investmen_shares_business_partner_average['equipment'] = " (BPC " . round(100*$_tmp_share/$_tmp_total,2) . "%)*";
}
else
{
	$investmen_shares_business_partner_average['equipment'] = "";
}


$_tmp_total = 0;
$_tmp_share = 0;
foreach($investmen_shares_business_partner['other'] as $key=>$data) {
	$_tmp_total = $_tmp_total + $data['amount'];
	$_tmp_share = $_tmp_share + $data['amount']*$data['share']/100;
}
if($_tmp_share > 0 and $_tmp_total > 0) {
	$investmen_shares_business_partner_average['other'] = " (BPC " . round(100*$_tmp_share/$_tmp_total,2) . "%)*";
}
else
{
	$investmen_shares_business_partner_average['other'] = "";
}





//calculate depreciation on requested amounts
$depreciation_on_requested_amount_in_percent = array();
$share = 1;
if($cer_basicdata["cer_basicdata_franchsiee_investment_share"] > 0)
{
	$share = (100-$cer_basicdata["cer_basicdata_franchsiee_investment_share"])/100;
}

$k = 1;
for($i=$first_year;$i<=$last_year;$i++)
{
	if($k < 11)
	{
		if(array_key_exists($i, $depreciation_values) and $total_gross_sales_values[$i] > 0) {
			
			
			$depreciation_on_requested_amount_in_percent[$i] = round($share*100*$depreciation_values[$i]/$total_gross_sales_values[$i], 2) . "%";
		}
		else
		{
			$depreciation_on_requested_amount_in_percent[$i] = '';
		}
		$k++;
	}
}

//depreciation period
$dyears = '';
$dmonths = '';
$depr_period = '';
foreach($depryears as $key=>$value)
{
	if($value > $dyears) {
		$dyears = $value;
	}
	if($deprmonths[$key] > $dmonths) {
		$dmonths = $deprmonths[$key];
	}
}

if($dyears > 0 and $dmonths > 0)
{
	$depr_period = $dyears . "y-" . $dmonths . "m";
}
elseif($dyears > 0)
{
	$depr_period = $dyears . "y-0m";
}
elseif($dmonths > 0)
{
	$depr_period = "0y-" . $dmonths . "m";
}


//set pdf parameters
$margin_top = 16;
$margin_left = 12;
$y = $margin_top;

$pdf->SetTitle("Requets - Retail Furniture in Third Party POS Form INR-03");
$pdf->SetAuthor("Retailnet");
$pdf->SetDisplayMode(150);
$pdf->AddPage();
$pdf->SetAutoPageBreak(false, 0);

// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "B", 9);
$pdf->Cell(40, 8, "Swatch Group", 1);

$pdf->SetFont("arialn", "B", 11);
$pdf->Cell(82, 8, "Request - Retail Furniture in Third-party-Store", 1, "", "C");

$pdf->SetFont("arialn", "", 9);
$pdf->SetFillColor(224,224,224);
$pdf->Cell(25, 8, "SUMMARY", 1, "", "C", true);

$pdf->SetFont("arialn", "", 9);

if($cer_basicdata["cer_basicdata_version"] == 0)
{
	$pdf->Cell(20, 8, date("d.m.Y"), 1, "", "C");
}
else
{
	$pdf->Cell(20, 8, to_system_date($cer_basicdata["versiondate"]), 1, "", "C");
}
$pdf->Cell(20, 8, "INR-03", 1, "", "C");


// draw outer box
$pdf->SetXY($margin_left,$y+8);


if($project['order_client_address'] == 42
			or $project['order_client_address'] == 7) {
	$pdf->Cell(187, 273, "", 1);
}
elseif($approval_name14 and $approval_name17)
{
	$pdf->Cell(187, 283, "", 1);
}
elseif($approval_name14 or $approval_name17)
{
	$pdf->Cell(187, 273, "", 1);
}
elseif($client_is_hq_agent == 1) {
	$pdf->Cell(187, 222, "", 1);
}
else
{
	$pdf->Cell(187, 263, "", 1);
}



	// print project and investment infos
	
	$y = $y+9;
	$x = $margin_left+1;

	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Business Partner Information:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(146, 5, substr($owner_company, 0, 130), 1, "", "L");


	$y = $y+5;
	$x = $margin_left+1;

	$pdf->SetXY($x,$y);
	//$pdf->Cell(39, 5, "Client Nr: " . $owner_company_sap_nr, 1, "", "L");
	$pdf->Cell(39, 5, "Reporting Nr: " . $pos_data['posaddress_eprepnr'], 1, "", "L");
	
	$pdf->Cell(107, 5, $project_name . " (" . $project['order_number'] . ")", 1, "", "L");
	$pdf->Cell(39, 5, "SIS Opening date: " . $opening_date, 1, "", "L");

	
	
	
	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Project classification: (X)", 1, "", "L");
	$pdf->Cell(20, 5, "New", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(11, 5, $project_kind_new, 1, "", "L");
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(28, 5, "Replacement", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(48, 5, $project_kind_renovation, 1, "", "L");
	$pdf->Cell(39, 5, "Last SIS Installation: " . $predecessor_opening_date, 1, "", "L");


	$y = $y+5;
	$x = $margin_left+1;

	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Existing Business Partner:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	
	if($cer_basicdata["cer_basicdata_franchsiee_already_partner"] == 1)
	{
		$pdf->Cell(146, 5, "YES", 1, "", "L");
	}
	else
	{
		$pdf->Cell(146, 5, "NO", 1, "", "L");
	}


	$y = $y+5;
	$x = $margin_left+1;

	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Furniture Supplies Name:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(146, 5, $suppliers, 1, "", "L");


	$y = $y+5;
	$x = $margin_left+1;

	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Cost of Furniture:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(72, 5, $fixed_assets_total . " / " . $fixed_assets_total_chf .  $investmen_shares_business_partner_average['furniture'] , 1, "", "L");

	$pdf->Cell(35, 5, "Partner Since:", 1, "", "L");
	$pdf->Cell(39, 5, to_system_date($franchisee_address["address_company_is_partner_since"]), 1, "", "L");

	$y = $y+5;
	$x = $margin_left+1;

	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Cost of Merchandising Material:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(72, 5, $merchandising_total . " / " . $merchandising_total_chf .  $investmen_shares_business_partner_average['posmaterial'] , 1, "", "L");

	$pdf->Cell(35, 5, "SIS Approval Form approved on:", 1, "", "L");
	$pdf->Cell(39, 5, $date_sis_approval, 1, "", "L");


	$y = $y+5;
	$x = $margin_left+1;

	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Cost of Construction/Installation:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(72, 5, $construction_total . " / " . $construction_total_chf .  $investmen_shares_business_partner_average['construction'] , 1, "", "L");

	$pdf->Cell(35, 5, "Surface in sqm", 1, "", "L");
	$pdf->Cell(39, 5, $project["project_cost_sqms"], 1, "", "L");



	$y = $y+5;
	$x = $margin_left+1;

	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Architectural costs:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(72, 5, $architectural_total . " / " . $architectural_total_chf .  $investmen_shares_business_partner_average['architectural'] , 1, "", "L");
	$pdf->Cell(35, 5, "Currency", 1, "", "L");
	$pdf->Cell(39, 5, $currency_symbol . " / CHF", 1, "", "L");


	$y = $y+5;
	$x = $margin_left+1;

	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Equipment (IT, security etc.):", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(72, 5, $equipment_total . " / " . $equipment_total_chf .  $investmen_shares_business_partner_average['equipment'] , 1, "", "L");
	$pdf->Cell(35, 5, "Distribution Agreement Starting:", 1, "", "L");
	$pdf->Cell(39, 5, to_system_date($project["project_fagrstart"]), 1, "", "L");


	$y = $y+5;
	$x = $margin_left+1;

	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Cost of Transportation:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(72, 5, $transportation_total . " / " . $transportation_total_chf .  $investmen_shares_business_partner_average['transportation'] , 1, "", "L");
	$pdf->Cell(35, 5, "Distribution Agreement Ending:", 1, "", "L");
	$pdf->Cell(39, 5, to_system_date($project["project_fagrend"]), 1, "", "L");


	$y = $y+5;
	$x = $margin_left+1;

	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Other costs:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(72, 5, $other_costs . " / " . $other_costs_chf .  $investmen_shares_business_partner_average['other'] , 1, "", "L");
	$pdf->Cell(35, 5, "Years of depreciation", 1, "", "L");
	$pdf->Cell(39, 5, $depr_period, 1, "", "L");
	

	$y = $y+5;
	$x = $margin_left+1;

	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Total Cost:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(72, 5, $investment_amount . " / " . $investment_amount_chf, 1, "", "L");
	$pdf->Cell(35, 5, "", 1, "", "L");
	$pdf->Cell(39, 5, "", 1, "", "L");

	$y = $y+5;
	$x = $margin_left+1;

	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "*Business Partner Contribution:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(37, 5, $investment_business_partner . " / " . $investment_business_partner_chf, 1, "", "L");
	$pdf->Cell(8, 5, "in %", 1, "", "L");
	$pdf->Cell(27, 5, $cer_basicdata["cer_basicdata_franchsiee_investment_share"] . "%", 1, "", "L");

	$pdf->Cell(35, 5, "", 1, "", "L");
	$pdf->Cell(39, 5, "", 1, "", "L");


	$y = $y+5;
	$x = $margin_left+1;

	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Requested Amount:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(72, 5, $requested_amount . " / " . $requested_amount_chf, 1, "", "L");

	$pdf->Cell(35, 5, "", 1, "", "L");
	$pdf->Cell(39, 5, "", 1, "", "L");
	
	
	
	
	if(($project["project_projectkind"] == 2 
				or $project["project_projectkind"] == 3 
				or $project["project_projectkind"] == 5)
		and count($sellouts_watches) > 0) // renovation or takover/renovation or lease renewal
	{
		//reduce to the last 2 years
		$sellouts_watches = array_slice($sellouts_watches,count($sellouts_watches)-2, 2, true);
		$sellouts_watches_gross_sales = array_slice($sellouts_watches_gross_sales,count($sellouts_watches_gross_sales)-2,2, true);


		$y = $y+7;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(21, 5, "1. Sales*", 1, "", "L");
		$pdf->SetFont("arialn", "B", 8);


		//data from the past
		$k = 1;
		foreach($sellouts_watches as $year=>$value)
		{
			if($k < 11)
			{
				$pdf->Cell(12, 5, $year . " (" . $sellouts_months[$year] . ")", 1, "", "R");
				$k++;
			}
		}

		//expected data
		$pdf->Cell(30, 5, "", 1, "", "L");
		$k = 1;
		for($i=$first_year;$i<=$last_year;$i++)
		{
			if($k < 9)
			{
				$pdf->Cell(12, 5, $i, 1, "", "R");
				$k++;
			}
		}


		$y = $y+5;
		$x = $margin_left+1;

		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(21, 5, "Watches sold:", 1, "", "L");

		//data from the past
		$pdf->SetFont("arialn", "n", 8);
		foreach($sellouts_watches as $year=>$value)
		{
			$pdf->Cell(12, 5, $value, 1, "", "R");
		}


		//expected data
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(30, 5, "Expected watches sold:", 1, "", "L");
		$pdf->SetFont("arialn", "n", 8);
		$k = 1;
		for($i=$first_year;$i<=$last_year;$i++)
		{
			if($k < 9)
			{
				if(array_key_exists($i, $sales_units_watches_values)) {
					$pdf->Cell(12, 5, $sales_units_watches_values[$i], 1, "", "R");
				}
				else
				{
					$pdf->Cell(12, 5, "", 1, "", "R");
				}
				$k++;
			}
		}





		$y = $y+5;
		$x = $margin_left+1;

		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(21, 5, "Gross sales:", 1, "", "L");

		$pdf->SetFont("arialn", "n", 8);
		
		//data from the past
		foreach($sellouts_watches_gross_sales as $year=>$value)
		{
			$pdf->Cell(12, 5, round($value/1000, 0), 1, "", "R");
		}


		//expected data
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(30, 5, "Expected gross sales:", 1, "", "L");
		$pdf->SetFont("arialn", "n", 8);
		$k = 1;
		$total_expected_gross_sales = 0;
		for($i=$first_year;$i<=$last_year;$i++)
		{
			if($k < 9)
			{
				if(array_key_exists($i, $gross_sales_values_watches)) {
					$pdf->Cell(12, 5, round($gross_sales_values_watches[$i]/1000, 0), 1, "", "R");
				}
				else
				{
					$pdf->Cell(12, 5, "", 1, "", "R");
				}
				$k++;
			}

			if(array_key_exists($i, $gross_sales_values_watches)) {
				$total_expected_gross_sales = $total_expected_gross_sales + $gross_sales_values_watches[$i];
			}
		}
		
		if($total_expected_gross_sales > 0)
		{
			$pdf->setX($pdf->getX() + 10);
			$pdf->Cell(12, 5, round($total_expected_gross_sales/1000, 0), 1, "", "R");
		}

		//investment in percent of sales
		$y = $y+5;
		$x = $margin_left+1;
		$pdf->SetXY($x,$y);
		
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(45, 5, "", 1, "", "L");
		$pdf->Cell(30, 5, "Investment in % of sales:", 1, "", "L");

		$pdf->SetFont("arialn", "n", 8);
		$k = 1;
		for($i=$first_year;$i<=$last_year;$i++)
		{
			if($k < 11)
			{
				if(array_key_exists($i, $depreciation_on_requested_amount_in_percent))
				{
					$pdf->Cell(12, 5, $depreciation_on_requested_amount_in_percent[$i], 1, "", "R");
				}
				else
				{
					$pdf->Cell(12, 5, "", 1, "", "R");
				}
				$k++;
			}
		}

		
		if($total_expected_gross_sales > 0)
		{
			$pdf->setX($pdf->getX() + 10);
			$pdf->Cell(12, 5, round(100*$requested_amount1/$total_expected_gross_sales, 2) .'%', 1, "", "R");
		}

		$y = $y+5;
		$pdf->SetXY($margin_left+1,$y);
		
		$pdf->Cell(80, 5, "*Units are sell through with Wholesale average price ", 0, "", "L");

		
	}
	else
	{
		$y = $y+7;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(39, 5, "1. Sales*", 1, "", "L");
		

		$pdf->SetFont("arialn", "B", 8);
		$k = 1;
		for($i=$first_year;$i<=$last_year;$i++)
		{
			if($k < 11)
			{
				$pdf->Cell(12, 5, $i, 1, "", "R");
				$k++;
			}
		}

		$y = $y+5;
		$x = $margin_left+1;

		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(39, 5, "Expected watches sold:", 1, "", "L");


		$pdf->SetFont("arialn", "n", 8);
		$k = 1;
		for($i=$first_year;$i<=$last_year;$i++)
		{
			if($k < 11)
			{
				if(array_key_exists($i, $sales_units_watches_values)) {
					$pdf->Cell(12, 5, $sales_units_watches_values[$i], 1, "", "R");
				}
				else
				{
					$pdf->Cell(12, 5, "", 1, "", "R");
				}
				$k++;
			}
		}



		$y = $y+5;
		$x = $margin_left+1;

		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(39, 5, "Expected gross sales:", 1, "", "L");

		$pdf->SetFont("arialn", "n", 8);
		$k = 1;
		$total_expected_gross_sales = 0;
		for($i=$first_year;$i<=$last_year;$i++)
		{
			if($k < 11)
			{
				if(array_key_exists($i, $gross_sales_values_watches)) {
					$pdf->Cell(12, 5, round($gross_sales_values_watches[$i]/1000, 0), 1, "", "R");
				}
				else
				{
					$pdf->Cell(12, 5, "", 1, "", "R");
				}
				$k++;
			}

			if(array_key_exists($i, $gross_sales_values_watches)) {
				$total_expected_gross_sales = $total_expected_gross_sales + $gross_sales_values_watches[$i];
			}
		}
		
		if($total_expected_gross_sales > 0)
		{
			$pdf->setX($pdf->getX() + 10);
			$pdf->Cell(12, 5, round($total_expected_gross_sales/1000, 0), 1, "", "R");
		}


		$y = $y+5;
		$x = $margin_left+1;

		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(39, 5, "Investment in % of sales:", 1, "", "L");

		$pdf->SetFont("arialn", "n", 8);
		$k = 1;
		for($i=$first_year;$i<=$last_year;$i++)
		{
			if($k < 11)
			{
				if(array_key_exists($i, $depreciation_on_requested_amount_in_percent))
				{
					$pdf->Cell(12, 5, $depreciation_on_requested_amount_in_percent[$i], 1, "", "R");
				}
				else
				{
					$pdf->Cell(12, 5, "", 1, "", "R");
				}
				$k++;
			}
		}

		if($total_expected_gross_sales > 0)
		{
			$pdf->setX($pdf->getX() + 10);
			$pdf->Cell(12, 5, round(100*$requested_amount1/$total_expected_gross_sales, 2) .'%', 1, "", "R");
		}

		$y = $y+5;
		$pdf->SetXY($margin_left+1,$y);
		
		$pdf->Cell(80, 5, "*Units are sell through with Wholesale average price ", 0, "", "L");
	}

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(102, 8, "2. Additional Comments / Short Description", 0, "", "L");

	$content = $description;
	$content = str_replace("\r\n\r\n", "", $content);
	
	$y = $y+6;
	$pdf->SetXY($x,$y);
	$pdf->SetFillColor(248,251,167);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(185, 40, "", 1, "", "L", false);
	$pdf->SetXY($x+1,$y+1);
	$pdf->MultiCell(183,3.5, $content, 0, 'L', false, 1, '', '', true, 0, false, true, '', 'T');
	

	

	$y = $y+40;
	$pdf->SetXY($x-1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(102, 8, "3. Signatures:", 0, "", "L");

	

	//title bar 1
	$y = $y+6;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetFillColor(224,224,224);
	$pdf->Cell(185,6, "Brand", 1, "", "L", true);

	$y = $y+6;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(30, 10, "CEO " . "\r\n" . $approval_name2, 1);
	$pdf->SetXY($x+30,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(63, 10, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 10, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(62, 10, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');


	$y = $y+10;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(30, 10, "VP Sales " . "\r\n" . $approval_name3, 1);
	$pdf->SetXY($x+30,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(63, 10, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 10, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(62, 10, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');


	$y = $y+10;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(30, 10, "VP Finance" . "\r\n" . $approval_name7, 1);
	$pdf->SetXY($x+30,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(63, 10, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 10, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(62, 10, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');

	
	$y = $y+10;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(30, 10, "Retail Controller" . "\r\n" . $approval_name9, 1);
	$pdf->SetXY($x+30,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(63, 10, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 10, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(62, 10, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');
	

	$y = $y+10;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(30, 10, "Merchandising Manager" . "\r\n" . $approval_name4, 1);
	$pdf->SetXY($x+30,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(63, 10, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 10, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(62, 10, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');


	$y = $y+10;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(30, 10, "Regional Sales Manager" . "\r\n" . $approval_name16, 1);
	$pdf->SetXY($x+30,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(63, 10, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 10, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(62, 10, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');


	
	if($client_is_hq_agent != 1)
	{

		$y = $y+15;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 10);
		$pdf->SetFillColor(224,224,224);
		$pdf->Cell(185,6, "Country", 1, "", "L", true);

		$y = $y+6;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->MultiCell(30, 10, "Country Manager " . "\r\n" . $approval_name11, 1);
		$pdf->SetXY($x+30,$y);
		$pdf->SetFont("freesans", "", 7);
		$pdf->Cell(63, 10, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(30, 10, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
		$pdf->SetFont("freesans", "", 7);
		$pdf->Cell(62, 10, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');


		//china projects
		if($project['order_client_address'] == 42
			or $project['order_client_address'] == 7) {
			
			$y = $y+10;
			$pdf->SetXY($x,$y);
			$pdf->SetFont("arialn", "", 8);
			$pdf->MultiCell(30, 10, "Finance/Service Center " . "\r\n" . $approval_name18, 1);
			$pdf->SetXY($x+30,$y);
			$pdf->SetFont("freesans", "", 7);
			$pdf->Cell(63, 10, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
			$pdf->SetFont("arialn", "", 8);
			$pdf->Cell(30, 10, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
			$pdf->SetFont("freesans", "", 7);
			$pdf->Cell(62, 10, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');
		}
		else {
						
			$y = $y+10;
			$pdf->SetXY($x,$y);
			$pdf->SetFont("arialn", "", 8);
			$pdf->MultiCell(30, 10, "Finance/Service Center " . "\r\n" . $approval_name12, 1);
			$pdf->SetXY($x+30,$y);
			$pdf->SetFont("freesans", "", 7);
			$pdf->Cell(63, 10, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
			$pdf->SetFont("arialn", "", 8);
			$pdf->Cell(30, 10, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
			$pdf->SetFont("freesans", "", 7);
			$pdf->Cell(62, 10, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');
		
		
	
			if($approval_name17)
			{
				$y = $y+10;
				$pdf->SetXY($x,$y);
				$pdf->SetFont("arialn", "", 8);
				$pdf->MultiCell(30, 10, "Head of Controlling " . "\r\n" . $approval_name17, 1);
				$pdf->SetXY($x+30,$y);
				$pdf->SetFont("freesans", "", 7);
				$pdf->Cell(63, 10, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
				$pdf->SetFont("arialn", "", 8);
				$pdf->Cell(30, 10, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
				$pdf->SetFont("freesans", "", 7);
				$pdf->Cell(62, 10, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');
			}
		}



		//china projects
		if($project['order_client_address'] == 42
			or $project['order_client_address'] == 7) {
		
			
			$y = $y+10;
			$pdf->SetXY($x,$y);
			$pdf->SetFont("arialn", "", 8);
			$pdf->MultiCell(30, 10, "Head of Controlling " . "\r\n" . $approval_name17, 1);
			$pdf->SetXY($x+30,$y);
			$pdf->SetFont("freesans", "", 7);
			$pdf->Cell(63, 10, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
			$pdf->SetFont("arialn", "", 8);
			$pdf->Cell(30, 10, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
			$pdf->SetFont("freesans", "", 7);
			$pdf->Cell(62, 10, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');
			
		}

		$y = $y+10;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->MultiCell(30, 10, "Brand Manager " . "\r\n" . $approval_name13, 1);
		$pdf->SetXY($x+30,$y);
		$pdf->SetFont("freesans", "", 7);
		$pdf->Cell(63, 10, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(30, 10, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
		$pdf->SetFont("freesans", "", 7);
		$pdf->Cell(62, 10, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');



		

		/*
		$y = $y+12;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->MultiCell(30, 12, "Retail Development " . "\r\n" . $approval_name15, 1);
		$pdf->SetXY($x+30,$y);
		$pdf->SetFont("freesans", "", 7);
		$pdf->Cell(63, 12, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(30, 12, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
		$pdf->SetFont("freesans", "", 7);
		$pdf->Cell(62, 12, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');
		*/

	}



	//add files for the booklet)
	if(array_key_exists("booklet", $_GET) and $_GET["booklet"] == 1)
	{
		
		//project budget in local currency

		include("../user/project_costs_budget_detail_pdf.php");


		//project budget in CHF
		if(isset($order_currency) and $order_currency["symbol"] != 'CHF')
		{
			$sc = 1;
			include("../user/project_costs_budget_detail_pdf.php");
		}


		//quotes for local works
		//include("../user/project_offer_comparison_pdf_main.php");
		include("../user/project_costs_bid_comparison_pdf_main.php");

		//list of variables
		include("cer_listofvars_pdf_detail.php");
		
		
		
		$source_file = ".." . $ln_basicdata["ln_basicdata_floorplan"];

		if(file_exists($source_file))
		{
			if(substr($source_file, strlen($source_file)-3, 3) == "pdf" 
				or substr($source_file, strlen($source_file)-3, 3) == "PDF")
			{
				
				$num_of_pages = $pdf->setSourceFile($source_file);
				for($i=1;$i<=$num_of_pages;$i++)
				{
					$tplidx = $pdf->ImportPage($i);
					$s = $pdf->getTemplatesize($tplidx);
					if($s["w"] > $s["h"])
					{
						$pdf->AddPage("L", array(0=>$s["h"], 1=>$s["w"]));
					}
					else
					{
						$pdf->AddPage("P", array(0=>$s["w"], 1=>$s["h"]));
					}
					$pdf->useTemplate($tplidx);
				}
				
				
			}
		}

		$source_file = ".." . $ln_basicdata["ln_basicdata_elevation_plan"];

		if(file_exists($source_file))
		{
			if(substr($source_file, strlen($source_file)-3, 3) == "pdf" 
				or substr($source_file, strlen($source_file)-3, 3) == "PDF")
			{
				
				$num_of_pages = $pdf->setSourceFile($source_file);
				for($i=1;$i<=$num_of_pages;$i++)
				{
					$tplidx = $pdf->ImportPage($i);
					$s = $pdf->getTemplatesize($tplidx);
					if($s["w"] > $s["h"])
					{
						$pdf->AddPage("L", array(0=>$s["h"], 1=>$s["w"]));
					}
					else
					{
						$pdf->AddPage("P", array(0=>$s["w"], 1=>$s["h"]));
					}
					$pdf->useTemplate($tplidx);
				}
				
				
			}
		}

		$source_file = ".." . $ln_basicdata["ln_basicdata_elevation_plan3"];

		if(file_exists($source_file))
		{
			if(substr($source_file, strlen($source_file)-3, 3) == "pdf" 
				or substr($source_file, strlen($source_file)-3, 3) == "PDF")
			{
				
				$num_of_pages = $pdf->setSourceFile($source_file);
				for($i=1;$i<=$num_of_pages;$i++)
				{
					$tplidx = $pdf->ImportPage($i);
					$s = $pdf->getTemplatesize($tplidx);
					if($s["w"] > $s["h"])
					{
						$pdf->AddPage("L", array(0=>$s["h"], 1=>$s["w"]));
					}
					else
					{
						$pdf->AddPage("P", array(0=>$s["w"], 1=>$s["h"]));
					}
					$pdf->useTemplate($tplidx);
				}
				
				
			}
		}

		$source_file = ".." . $ln_basicdata["ln_basicdata_photoreport"];

		if(file_exists($source_file))
		{
			if(substr($source_file, strlen($source_file)-3, 3) == "pdf" 
				or substr($source_file, strlen($source_file)-3, 3) == "PDF")
			{
				
				$num_of_pages = $pdf->setSourceFile($source_file);
				for($i=1;$i<=$num_of_pages;$i++)
				{
					$tplidx = $pdf->ImportPage($i);
					$s = $pdf->getTemplatesize($tplidx);
					if($s["w"] > $s["h"])
					{
						$pdf->AddPage("L", array(0=>$s["h"], 1=>$s["w"]));
					}
					else
					{
						$pdf->AddPage("P", array(0=>$s["w"], 1=>$s["h"]));
					}
					$pdf->useTemplate($tplidx);
				}
				
				
			}
		}

		$source_file = ".." . $ln_basicdata["ln_basicdata_3drenderings"];

		if(file_exists($source_file))
		{
			if(substr($source_file, strlen($source_file)-3, 3) == "pdf" 
				or substr($source_file, strlen($source_file)-3, 3) == "PDF")
			{
				
				$num_of_pages = $pdf->setSourceFile($source_file);
				for($i=1;$i<=$num_of_pages;$i++)
				{
					$tplidx = $pdf->ImportPage($i);
					$s = $pdf->getTemplatesize($tplidx);
					if($s["w"] > $s["h"])
					{
						$pdf->AddPage("L", array(0=>$s["h"], 1=>$s["w"]));
					}
					else
					{
						$pdf->AddPage("P", array(0=>$s["w"], 1=>$s["h"]));
					}
					$pdf->useTemplate($tplidx);
				}
				
				
			}
		}
	}


		
	//check if project has jpg attachments to be added to the booklet
	$sql = "select order_file_path " . 
		   "from projects " . 
		   "left join order_files on order_file_order = project_order " . 
		   "where order_file_attach_to_cer_booklet = 1 and order_file_type= 2 and project_id = " .param("pid") . 
		   " order by order_files.date_created DESC";

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		
		
		$source_file = ".." . $row["order_file_path"];
		if(file_exists($source_file))
		{
			$pdf->AddPage("L");
			$margin_top = 16;
			$margin_left = 12;
			$pdf->SetXY($margin_left,$margin_top);

			/*
			$pdf->SetFont("arialn", "I", 9);
			$pdf->Cell(45, 10, "", 1);

			$pdf->Image('../pictures/brand_logo.jpg',13,$margin_top + 1,22);

			$pdf->SetFont("arialn", "B", 11);
			$pdf->Cell(172, 10, "Layout: " . $project_name, 1, "", "L");
			$pdf->Cell(20, 10, date("d.m.Y"), 1, "", "C");
			*/

			$pdf->Image($source_file,20,40, 200);
		}
	}


	//check if project has pdf attachments to be added to the booklet
	$PDFmerger_was_used = false;
	$pdf_print_output = true;

	$sql = "select order_file_path " . 
		   "from projects " . 
		   "left join order_files on order_file_order = project_order " . 
		   "where order_file_attach_to_cer_booklet = 1 and order_file_type = 13 and project_id = " .param("pid") . 
		   " order by order_files.date_created DESC";

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{

		$source_file = ".." . $row["order_file_path"];	if(file_exists($source_file))
		$source_file = ".." . $row["order_file_path"];	
		
		if(substr($source_file, strlen($source_file)-3, 3) == "pdf" 
			or substr($source_file, strlen($source_file)-3, 3) == "PDF")
		{
			if(file_exists($source_file))
			{
				
				$num_of_pages = $pdf->setSourceFile($source_file);
				for($i=1;$i<=$num_of_pages;$i++)
				{
					$tplidx = $pdf->ImportPage($i);
					$s = $pdf->getTemplatesize($tplidx);
					if($s["w"] > $s["h"])
					{
						$pdf->AddPage("L", array(0=>$s["h"], 1=>$s["w"]));
					}
					else
					{
						$pdf->AddPage("P", array(0=>$s["w"], 1=>$s["h"]));
					}
					$pdf->useTemplate($tplidx);
				}
			}
		}
		
	}

	

//end of second box
?>