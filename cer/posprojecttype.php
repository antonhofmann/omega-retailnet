<?php
/********************************************************************

    posprojecttype.php

    Creation and mutation of posprojecttype records records.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-02-20
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-02-20
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_full_access_to_cer");

/********************************************************************
    build form 
*********************************************************************/
$form = new Form("posproject_types", "posproject_types");


$form->add_list("posproject_type_postype", "POS Type",
    "select postype_id, postype_name from postypes order by postype_name", NOTNULL);

$form->add_list("posproject_type_projectcosttype", "Legal Type",
    "select project_costtype_id, project_costtype_text from project_costtypes order by project_costtype_text", NOTNULL);

$form->add_list("posproject_type_projectkind", "Project Kind",
    "select projectkind_id, projectkind_name from projectkinds where projectkind_id > 0 order by projectkind_name", NOTNULL);


$form->add_checkbox("posproject_type_needs_cer", "Needs a CER", "", 0, "CER");
$form->add_checkbox("posproject_type_needs_af", "Needs an AF", "", 0, "AF");
$form->add_checkbox("posproject_type_needs_inr03", "Needs an INR-03", "", 0, "INR-03");

$form->add_checkbox("posproject_type_needs_agreement", "Needs a Franchisee Agreement", "", 0, "Agreement");


$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();
$form->process();

if($form->button(FORM_BUTTON_SAVE)) {
	redirect("posprojecttypes.php");
}

$page = new Page("posprojecttypes");

$page->header();
$page->title("Edit POS Projecttypes");
$form->render();
$page->footer();

?>
