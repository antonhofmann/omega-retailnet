<?php
/********************************************************************

    ln_lease_details_detail_pdf.php

    Print Lease Details

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-09-26
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-09-26
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

	
/********************************************************************
	prepare all data needed
*********************************************************************/

// get all data needed from project
$sql = "select * " . 
       "from projects " . 
	   "left join orders on order_id = project_order " .
	   "left join countries on country_id = order_shop_address_country " .
	   "left join product_lines on product_line_id = project_product_line " .
	   "left join productline_subclasses on productline_subclass_id = project_product_line_subclass " .
	   "left join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join projectkinds on projectkind_id = project_projectkind " .
       "left join addresses on address_id = order_client_address " .
	   "left join currencies on currency_id = country_currency " . 
       "left join postypes on postype_id = project_postype ".
	   "left join ln_basicdata on ln_basicdata_project = project_id ".
	   "left join floors on floor_id = project_floor " .
	   "where ln_basicdata_version = " . $ln_version . " and project_id = " . param("pid");

$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	$posdata = get_pos_data($row["project_order"]);
	$poslease_data = get_pos_leasedata($posdata["posaddress_id"], $row["project_order"]);
	$currency = get_cer_currency(param("pid"), $cer_version);

	$order_number = $row["project_order"];
	//POS Basic Data
	$posname = $row["order_shop_address_company"];

	if($row["order_shop_address_company2"])
    {
		$posname .= ", " . $row["order_shop_address_company2"];
	}

	$rent_review_date = '';
	if(array_key_exists('rent_review_date', $poslease_data)) {
		$rent_review_date = $poslease_data["rent_review_date"];
	}
	
	$posaddress = $row["country_name"];

	if($row["order_shop_address_zip"])
    {
		$posaddress .= ", " . $row["order_shop_address_zip"] . " " . $row["order_shop_address_place"];
	}
	else
	{
		$posaddress .= ", " . $row["order_shop_address_place"];
	}

	$posaddress .= ", " . $row["order_shop_address_address"];
	
	if($row["order_shop_address_address2"])
    {
		$posaddress .= ", " . $row["order_shop_address_address2"];
	}

	$city_name = $row["order_shop_address_place"];
	
	$planned_opening_date = to_system_date($row["project_real_opening_date"]);
	$sales_surface = $row["project_cost_sqms"];
	$gross_surface = $row["project_cost_gross_sqms"];
	$total_surface = $row["project_cost_totalsqms"];
	$other_surface = $row["project_cost_othersqms"] + $row["project_cost_backofficesqms"];

	$page_title = "Lease Details: " . $row["project_number"];
	$page_title = "Lease Details ";

	$project_number = $row["project_number"];
	$country_name = $row["country_name"];
	$placement = $row["floor_name"];
	$postype2 = $row["project_costtype_text"] . " " . $row["postype_name"];
	
	if($row["productline_subclass_name"])
	{
		$postype = $row["projectkind_name"] . " - " . $row["project_costtype_text"] . " " . $row["postype_name"] . " - " . $row["product_line_name"] . " / " . $row["productline_subclass_name"];
	}
	else {
		$postype = $row["projectkind_name"] . " - " . $row["project_costtype_text"] . " " . $row["postype_name"] . " - " . $row["product_line_name"];
	}

	$ln_remarks = $row["ln_basicdata_remarks"];
	$ln_brands = $row["ln_basicdata_brands"];
	$ln_size_remarks = $row["ln_basicdata_size_remarks"];
	$ln_area = $row["ln_basicdata_area"];
	$ln_rent = $row["ln_basicdata_rent"];
	$ln_availability = $row["ln_basicdata_availability"];


	$exchange_rate_factor = $ln_basicdata['ln_basicdata_factor'];
	if(!$ln_basicdata['ln_basicdata_factor'])
	{
		$exchange_rate_factor = 1;
	}

	$exchange_rate = 1;
	if($ln_basicdata['ln_basicdata_exchangerate'] > 0)
	{
		$exchange_rate = $ln_basicdata['ln_basicdata_exchangerate'];
	}



	
	//get annual rent
	$annual_rent = "";
	$salespercent = "";

	$sql = "select * from posleases " .
		   "where poslease_lease_type = 1 and poslease_posaddress = " . dbquote($posdata["posaddress_id"]) .
		   " order by poslease_startdate DESC ";
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		
		$duration_in_years = $row["poslease_enddate"] - $row["poslease_startdate"];
		$tmp1 = 13 - substr($row["poslease_startdate"], 5,2) + substr($row["poslease_enddate"], 5,2);
		$tmp2 = ($duration_in_years - 1)*12;
		$duration_in_years = round(($tmp1 + $tmp2) / 12, 1);

		$total_lease_commitment = 0;
		$sql_cer = "select * from cer_expenses " .
					  "where cer_expense_cer_version = " . $cer_version . " and cer_expense_project = " . param("pid") .
					  " and cer_expense_type in (2, 3,18,19, 20) ";

		$res_cer = mysql_query($sql_cer) or dberror($sql_cer);
		while ($row_cer = mysql_fetch_assoc($res_cer))
		{
			$total_lease_commitment = $total_lease_commitment + 1*$row_cer["cer_expense_amount"];
			
		}

		$total_sales = 0;
		$sql =  "select * from cer_revenues " .
				"where cer_revenue_cer_version = " . $cer_version . " and cer_revenue_project = " . param("pid") ;

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$sales_watches_values = $row["cer_revenue_watches"];
			$sales_jewellery_values = $row["cer_revenue_jewellery"];
			$sales_accessories_values = $row["cer_revenue_accessories"];
			$sales_customer_service_values = $row["cer_revenue_customer_service"];
			
			$total_sales = $total_sales + 1*$sales_watches_values + 1*$sales_jewellery_values + 1*$sales_accessories_values + 1*$sales_customer_service_values;

		}

		
		if($duration_in_years > 0)
		{
			$annual_rent = $total_lease_commitment / $duration_in_years;
		}
		if($total_sales > 0)
		{ 
			$salespercent = 100 * $total_lease_commitment / $total_sales/100;
		}

		$annual_rent = round($total_lease_commitment / $duration_in_years, 0);

		$salespercent = round(100*$salespercent, 2) . "%";
		
	}

	


	//get areas and neighbourhood information
	$posareas = "";
	if($project["pipeline"] == 0)
	{
		$sql_i = "select * from posareas " . 
				 "left join posareatypes on posareatype_id = posarea_area " .
				 "where posarea_posaddress = " . $project["posaddress_id"];
		
	}
	elseif($project["pipeline"] == 1)
	{
		
		$sql_p = "select posorder_posaddress from posorderspipeline where posorder_order = " . $project["order_id"];
		$res_p = mysql_query($sql_p) or dberror($sql_p);

		if ($row_p = mysql_fetch_assoc($res_p))
		{
			$sql_i = "select * from posareaspipeline " . 
					 "left join posareatypes on posareatype_id = posarea_area " .
					 "where posarea_posaddress = " . $row_p["posorder_posaddress"];
		}
	}
	$res_i = mysql_query($sql_i) or dberror($sql_i);
	while ($row_i = mysql_fetch_assoc($res_i))
	{
		$posareas .= $row_i["posareatype_name"] . ", ";
	}
	$posareas = substr($posareas,0,strlen($posareas)-2);



	/********************************************************************
    build list of calculated additional rental cost
	*********************************************************************/
	$rental_cost_types = array();
	$rental_cost_years = array();
	$sql_a = "select additional_rental_cost_type_id, additional_rental_cost_type_name " . 
			 " from cer_additional_rental_costs " . 
			 " left join additional_rental_cost_types on additional_rental_cost_type_id = cer_additional_rental_cost_type_id";

	$list_filter_a = "cer_additional_rental_cost_project = " . dbquote(param("pid"));
	$list_filter_a .= " and cer_additional_rental_cost_cer_version = 0";


	$sql_l = $sql_a . " where " . $list_filter_a;
	$res = mysql_query($sql_l) or dberror($sql_l);
	while($row = mysql_fetch_assoc($res))
	{
		$rental_cost_types[$row["additional_rental_cost_type_id"]] = $row["additional_rental_cost_type_name"];
	}
	
	$rental_cost_amounts = array();
	foreach($rental_cost_types as $rental_cost_type_id=>$rental_cost_type_name)
	{

		$tmp = array();
		$sql_am = "select cer_additional_rental_cost_amount_id, cer_additional_rental_cost_amount_year, " . 
			      " cer_additional_rental_cost_amount_costtype_id, cer_additional_rental_cost_amount_amount " . 
				  " from cer_additional_rental_cost_amounts " . 
				  " where cer_additional_rental_cost_amount_version = 0 " . 
				  " and cer_additional_rental_cost_amount_project = " . dbquote(param("pid")) . 
				  " and cer_additional_rental_cost_amount_costtype_id = " . $rental_cost_type_id . 
			      " order by cer_additional_rental_cost_amount_year " ;

		$sql_am = mysql_query($sql_am) or dberror($sql_am);
		while($row_am = mysql_fetch_assoc($sql_am))
		{
			
			$rental_cost_years[$row_am["cer_additional_rental_cost_amount_year"]] = $row_am["cer_additional_rental_cost_amount_year"];
			$tmp[$row_am["cer_additional_rental_cost_amount_year"]] = "";
			if($row_am["cer_additional_rental_cost_amount_amount"] > 0)
			{
				$tmp[$row_am["cer_additional_rental_cost_amount_year"]] = $row_am["cer_additional_rental_cost_amount_amount"];
			}
		}


		$rental_cost_amounts[ $rental_cost_type_id] = $tmp;
	}


	include "include/in_financial_data.php";

	
	$pdf->SetFillColor(220, 220, 220); 

	$pdf->AddFont('arialn','');
	$pdf->AddFont('arialn','B');
	$pdf->AddFont('arialn','I');
	$pdf->AddFont('arialn','BI');

	$pdf->AddPage('P');
	$new_page = 0;

	//PRINT DATA

	if($exchange_rate_factor > 0)
	{
		$e_rate = $exchange_rate/$exchange_rate_factor;
	}
	else
	{
		$e_rate = $exchange_rate;
	}


	//Logo
	$pdf->Image('../pictures/logo.jpg',10,8,33);
	//arialn bold 15
	$pdf->SetFont('arialn','B',18);
	//Move to the right
	$pdf->Cell(80);
	//Title
	$pdf->SetY(0);
	$pdf->Cell(0,33,$page_title,0,0,'R');
	//Line break
	$pdf->SetY(23);


	
	
	$pdf->setXY(10, $pdf->getY());
	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(97,4,"Address Data",1, 0, 'L', 1);
	$pdf->Cell(90,4,"Location Data",1, 0, 'L', 1);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->Cell(30,4,"POS Name:",1, 0, 'L', 0);
	$pdf->Cell(67,4,$posname,1, 0, 'L', 0);
	$pdf->Cell(40,4,"Type:",1, 0, 'L', 0);
	$pdf->Cell(50,4,$postype2,1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->Cell(30,4,"Country:",1, 0, 'L', 0);
	$pdf->Cell(67,4,$country_name,1, 0, 'L', 0);
	$pdf->Cell(40,4,"Placement:",1, 0, 'L', 0);
	$pdf->Cell(50,4,$placement,1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->Cell(30,4,"City:",1, 0, 'L', 0);	
	$pdf->Cell(67,4,$city_name,1, 0, 'L', 0);
	$pdf->Cell(40,4,"Gross/Total/Sales surface:",1, 0, 'L', 0);
	$pdf->Cell(50,4,$gross_surface . " m2 / ". $total_surface . " m2 / " . $sales_surface . " m2",1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->Cell(30,4,"Environment:",1, 0, 'L', 0);	
	$pdf->Cell(107,4,$posareas,1, 0, 'L', 0);
	$pdf->Cell(50,4,"",1, 0, 'L', 0);
	$pdf->Ln();
	$pdf->Ln();


	
	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(187,7,"Key points",1, 0, 'L', 1);
	$pdf->Ln();
	$pdf->SetFont('arialn','',10);
	$pdf->MultiCell(187,3.5, $ln_remarks, 1, "T");
	
	$pdf->setY($pdf->getY()+4);

		
	//rental Details
	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(72,5,"",1, 0, 'L', 0);
	for($i=0;$i<5;$i++)
	{
		if(array_key_exists($i, $years))
		{
			$pdf->Cell(23,5,$years[$i],1, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(23,5," ",1, 0, 'R', 0);
		}
	}
	
	$pdf->Ln();

	
	$pdf->SetFont('arialn','B',9);
	$x = $pdf->GetX();
	$pdf->Cell(187,20,"",1, 0, 'L', 0);
	$pdf->SetX($x);

	$pdf->Cell(72,20," ",0, 0, 'R', 0);
	for($i=0;$i<5;$i++)
	{
		$pdf->Cell(23,20," ",1, 0, 'R', 0);
	}
	

	$pdf->SetX($x);

	$pdf->SetFont('arialn','',9);

	$pdf->Cell(72,5,"Annual fix rental costs in KCHF",0, 0, 'L', 0);
	
	$pdf->SetFont('arialn','',9);
	for($i=0;$i<5;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $fixedrents))
		{
			$pdf->Cell(23,5,number_format($e_rate*$fixedrents[$years[$i]]/1000, 0, ".", "'"),0, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(23,5," ",0, 0, 'R', 0);
		}
	}
	$pdf->Ln();



	$pdf->Cell(72,5,"Tax on fixed rents in KCHF",0, 0, 'L', 0);
	
	$pdf->SetFont('arialn','',9);
	for($i=0;$i<5;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $tax_on_fixedrents))
		{
			$pdf->Cell(23,5,number_format($e_rate*$tax_on_fixedrents[$years[$i]]/1000, 0, ".", "'"),0, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(23,5," ",0, 0, 'R', 0);
		}
	}
	$pdf->Ln();

	$pdf->Cell(72,5,"Turnover based rental costs per year in KCHF",0, 0, 'L', 0);
	
	$pdf->SetFont('arialn','',9);
	for($i=0;$i<5;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $turnoverbasedrents))
		{
			$pdf->Cell(23,5,number_format($e_rate*$turnoverbasedrents[$years[$i]]/1000, 0, ".", "'"),0, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(23,5," ",0, 0, 'R', 0);
		}
	}
	$pdf->Ln();


	$pdf->Cell(72,5,"Additional rental costs per year in KCHF",0, 0, 'L', 0);
	
	$pdf->SetFont('arialn','',9);
	for($i=0;$i<5;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $additionalrents))
		{
			$pdf->Cell(23,5,number_format($e_rate*$additionalrents[$years[$i]]/1000, 0, ".", "'"),0, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(23,5," ",0, 0, 'R', 0);
		}
	}
	$pdf->Ln();


	$pdf->SetFont('arialn','B',9);

	$pdf->Cell(72,5,"Totals",1, 0, 'L', 0);
	for($i=0;$i<5;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $sales_units_watches_values))
		{
			$pdf->Cell(23,5,number_format($e_rate*$rents[$years[$i]]/1000, 0, ".", "'"),1, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(23,5," ",1, 0, 'R', 0);
		}
	}

	$pdf->SetFont('arialn','',9);
	$pdf->Ln();
	


	if(count($rental_cost_years) > 0)
	{
		$pdf->setY($pdf->getY()+4);
		$pdf->SetFont('arialn','B',10);
		$pdf->Cell(187,7,"Additional Rental Costs in CHF " ,1, 0, 'L', 1);
		$pdf->Ln();
		$pdf->SetFont('arialn','B',8);

		$pdf->Cell(35,3.5,"Cost Type",1, 0, 'L', 0);

		
		$i = 1;
		foreach($rental_cost_years as $key=>$year)
		{
			if($i < 11)
			{
				$pdf->Cell(14,3.5,$year,1, 0, 'R', 0);
			}
			$i++;
		}
		$pdf->Ln();

		$pdf->SetFont('arialn','',8);

		
		
		foreach($rental_cost_types as $rental_cost_type_id=>$rental_cost_type_name)
		{
			$pdf->Cell(35,3.5,$rental_cost_type_name,1, 0, 'L', 0);
			
			$i = 1;
			foreach($rental_cost_years as $key=>$year)
			{
				if($i < 11)
				{
					if(array_key_exists($year, $rental_cost_amounts[$rental_cost_type_id])
						and $rental_cost_amounts[$rental_cost_type_id][$year] > 0)
					{
						$tmp = number_format($e_rate*$rental_cost_amounts[$rental_cost_type_id][$year], 0, ".", "'");
						$pdf->Cell(14,3.5,$tmp,1, 0, 'R', 0);
					}
					else
					{
						$pdf->Cell(14,3.5,"",1, 0, 'R', 0);
					}
				}
				$i++;
			}

					

			$pdf->Ln();
		}
	}
	
	$pdf->setY($pdf->getY()+4);
	//lease details (TAB Rental from CER)


	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(187,7,"Lease Details" ,1, 0, 'L', 1);
	$pdf->Ln();
	
	$pdf->SetFont('arialn','',9);
	$pdf->Cell(35,6,"Lease Type:",1, 0, 'L', 0);
	if(count($poslease_data) > 0) {
		$pdf->Cell(31,6,$poslease_data["poslease_type_name"],1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(31,6,"",1, 0, 'L', 0);
	}
	$pdf->Cell(31,6,"Start Date:",1, 0, 'L', 0);
	if(count($poslease_data) > 0) {
		$pdf->Cell(31,6, to_system_date($poslease_data["poslease_startdate"]),1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(31,6, "",1, 0, 'L', 0);
	}
	$pdf->Cell(31,6,"End Date:",1, 0, 'L', 0);
	if(count($poslease_data) > 0) {
		$pdf->Cell(28,6,to_system_date($poslease_data["poslease_enddate"]),1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(28,6,"",1, 0, 'L', 0);
	}
	$pdf->Ln();



	$pdf->Cell(35,6,"Extension Option:",1, 0, 'L', 0);
	if(count($poslease_data) > 0) {
		$pdf->Cell(31,6,to_system_date($poslease_data["poslease_extensionoption"]),1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(31,6,"",1, 0, 'L', 0);
	}
	$pdf->Cell(31,6,"Exit Option to Date:",1, 0, 'L', 0);
	if(count($poslease_data) > 0) {
		$pdf->Cell(31,6,to_system_date($poslease_data["poslease_exitoption"]),1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(31,6,"",1, 0, 'L', 0);
	}
	$pdf->Cell(31,6,"Term.Deadline:",1, 0, 'L', 0);
	if(count($poslease_data) > 0) {
		$pdf->Cell(28,6,$poslease_data["poslease_termination_time"],1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(28,6,"",1, 0, 'L', 0);
	}
	$pdf->Ln();

	
		
	

	$pdf->Cell(35,6,"Rent p.a. in CHF:",1, 0, 'L', 0);
	$pdf->Cell(31,6, number_format(round($e_rate*$annual_rent,0)),1, 0, 'L', 0);
	$pdf->Cell(31,6,"Rent as % of Sales:",1, 0, 'L', 0);
	$pdf->Cell(31,6,$salespercent,1, 0, 'L', 0);
	
	
	$text = "";
	if(count($poslease_data) > 0) {
		if($poslease_data["poslease_indexclause_in_contract"] == 1) {
			$text .= "Index Clause";
		}
		if($poslease_data["poslease_indexclause_in_contract"] == 1) {
			if($text) {
			$text .= " / ";	
			}
			$text .= "Tacit Renewal Clause";
		}
	}
	
	$pdf->Cell(59,6,$text,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->Cell(35,6,"Deadline for property:",1, 0, 'L', 0);
	$pdf->Cell(31,6,to_system_date($cer_basicdata["cer_basicdata_deadline_property"]),1, 0, 'L', 0);
	$pdf->Cell(31,6,"Handover Date:",1, 0, 'L', 0);
	if(count($poslease_data) > 0) {
		$pdf->Cell(31,6,to_system_date($poslease_data["poslease_handoverdate"]),1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(31,6,"",1, 0, 'L', 0);
	}
	$pdf->Cell(31,6,"First Rent Payed on:",1, 0, 'L', 0);
	if(count($poslease_data) > 0) {
		$pdf->Cell(28,6,to_system_date($poslease_data["poslease_firstrentpayed"]),1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(28,6,"",1, 0, 'L', 0);
	}
	$pdf->Ln();


	$pdf->Cell(66,6,"Rent Free Period in Weeks:",1, 0, 'L', 0);
	if(count($poslease_data) > 0) {
		$pdf->Cell(31,6,$poslease_data["poslease_freeweeks"],1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(31,6,"",1, 0, 'L', 0);
	}
	$pdf->Cell(31,6,"",1, 0, 'L', 0);

	if($rent_review_date) {
		$pdf->Cell(31,6,"Rent Review Date:",1, 0, 'L', 0);
		$pdf->Cell(28,6,$rent_review_date,1, 0, 'L', 0);
	}
	else {
		$pdf->Cell(31,6,"",1, 0, 'L', 0);
		$pdf->Cell(28,6,"",1, 0, 'L', 0);
	}
	$pdf->Ln();

	$pdf->Cell(35,6,"Negotiator:",1, 0, 'L', 0);
	if(count($poslease_data) > 0) {
		$pdf->Cell(152,6,$poslease_data["poslease_negotiator"],1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(152,6,"",1, 0, 'L', 0);
	}
	$pdf->Ln();

	$text = "Landlord (Negotiation Partner):\r\n";
	if(count($poslease_data) > 0) {
		$text .= $poslease_data["poslease_landlord_name"];
	}

	$pdf->MultiCell(187,3.5, $text, 1, "T");

	$text = "Negotiated Rental Conditions:\r\n";
	if(count($poslease_data) > 0) {
		$text .= $poslease_data["poslease_negotiated_conditions"];
	}

	
	//Additional rental costs
	if($pdf->GetY() > 230)
	{
		$pdf->AddPage();
		//Logo
		$pdf->Image('../pictures/logo.jpg',10,8,33);
		//arialn bold 15
		$pdf->SetFont('arialn','B',18);
		//Move to the right
		$pdf->Cell(80);
		//Title
		$pdf->SetY(0);
		$pdf->Cell(0,33,$page_title,0,0,'R');
		//Line break
		$pdf->SetY(23);


		
		
		$pdf->setXY(10, $pdf->getY());
	}

	$pdf->MultiCell(187,3.5, $text, 1, "T");
	
	
	//turnover based rents
	$tob_conditions = "";
	if($cer_basicdata['cer_basicdata_tob_from_net_sales'] == 1) {
		$tob_conditions .= "Turnover based rents are calculated on the base of net sales values." . "\r\n";
	}
	else {
		$tob_conditions .= "Turnover based rents are calculated on the base of gross sales values." . "\r\n";
	}
	if($cer_basicdata['cer_basicdata_add_tob_rents'] == 1) {
		$tob_conditions .= "Turnover based rents are to be paid in addition to fixed rents." . "\r\n";
	}
	else {
		$tob_conditions .= "Turnover based rents are to be paid if they exeed the fixed rent (what ever is higher)." . "\r\n";
	}
	if($cer_basicdata['cer_basicdata_tob_from_breakpoint_difference'] == 1) {
		$tob_conditions .= "Turnover based rents are to be calculated on the base of the difference between sales values and breakpoints." . "\r\n";
	}
	else {
		$tob_conditions .= "Turnover based rents are calculated on the base of sales values." . "\r\n";
	}

	

	$tob_title_printed = false;
	$sql_tob = "select cer_rent_percent_from_sale_id, cer_rent_percent_from_sale_percent, " . 
           "cer_rent_percent_from_sale_amount, cer_rent_percent_from_sale_from_year, " . 
		   "cer_rent_percent_from_sale_from_month, cer_rent_percent_from_sale_from_day," . 
		   "cer_rent_percent_from_sale_to_year, cer_rent_percent_from_sale_to_month, " . 
		   "cer_rent_percent_from_sale_to_day, cer_rent_percent_from_sale_number_of_months, " .
		   "cer_rent_percent_from_sale_tax_rate, cer_rent_percent_from_sale_passenger_rate " . 
	       "from cer_rent_percent_from_sales " . 
		   " where cer_rent_percent_from_sale_cer_version = 0 and cer_rent_percent_from_sale_project = " . dbquote(param("pid")) . 
		   " order by cer_rent_percent_from_sale_from_year, cer_rent_percent_from_sale_from_month";
	
	

	$res = mysql_query($sql_tob) or dberror($sql_tob);

	while($row = mysql_fetch_assoc($res))
	{
		if($tob_title_printed == false) {
			$pdf->SetFont('arialn','B',10);
			$pdf->Cell(152,6,"Total Annual Rent in % of Sales Values and Breakpoints",0, 0, 'L', 0);
			$pdf->SetFont('arialn','',9);
			$pdf->Ln();

			$pdf->MultiCell(187,3.5, $tob_conditions, 1, "T");


			$pdf->SetFont('arialn','B',9);
			$pdf->Cell(20,6,"Perent",1, 0, 'L', 0);
			$pdf->Cell(20,6,"Breakpoint",1, 0, 'L', 0);
			$pdf->Cell(20,6,"From Date",1, 0, 'L', 0);
			$pdf->Cell(20,6,"To Date",1, 0, 'L', 0);
			$pdf->SetFont('arialn','',9);

			$pdf->Ln();

			$tob_title_printed = true;
		}

		$from_date = $row['cer_rent_percent_from_sale_from_day'] . '.' . $row['cer_rent_percent_from_sale_from_month']. '.' . $row['cer_rent_percent_from_sale_from_year'];
		$to_date = $row['cer_rent_percent_from_sale_to_day'] . '.' . $row['cer_rent_percent_from_sale_to_month']. '.' . $row['cer_rent_percent_from_sale_to_year'];

		$pdf->Cell(20,6,$row['cer_rent_percent_from_sale_percent'],1, 0, 'L', 0);
		$pdf->Cell(20,6,$row['cer_rent_percent_from_sale_amount'],1, 0, 'L', 0);
		$pdf->Cell(20,6,$from_date,1, 0, 'L', 0);
		$pdf->Cell(20,6,$to_date,1, 0, 'L', 0);
		$pdf->Ln();
	}
	
	//Position at 1.5 cm from bottom
	$pdf->SetY(282);
	$pdf->SetFont('arialn','I',8);
	$pdf->Cell(0,10, to_system_date(date("d.m.y")),0,0,'R');

}

?>