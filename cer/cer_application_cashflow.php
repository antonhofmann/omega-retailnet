<?php
/********************************************************************

	cer_application_revenues.php

    Application Form: revenues
    
	Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";

check_access("has_access_to_cer");
set_referer("cer_application_revenue.php");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}


/********************************************************************
    prepare all data needed
*********************************************************************/

$posdata = get_pos_data($project["project_order"]);

$currency = get_cer_currency(param("pid"));
$currency2 = get_ln_currency(param("pid"));

$old_ln_basicdata_exchangerate = $ln_basicdata["ln_basicdata_exchangerate"];


param("id", get_id_of_cer_basicdata(param("pid")));

$result = check_basic_data(param("pid"));

//get stock_data
$stock_data = array();
$sql = "select * from cer_stocks " .
       "where cer_stock_cer_version = 0 and cer_stock_project = " . param("pid") . 
	   " order by cer_stock_year";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$stock_data[$row["cer_stock_year"]] = $row["cer_stock_stock_in_months"];
}

//get paymentterm_data
$paymentterm_data = array();
$sql = "select * from cer_paymentterms " .
       "where cer_paymentterm_cer_version = 0 and cer_paymentterm_project = " . param("pid") . 
	   " order by cer_paymentterm_year";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$paymentterm_data[$row["cer_paymentterm_year"]] = $row["cer_paymentterm_in_months"];
}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_basicdata", "cer_basicdata");
include("include/project_head.php");

$form->add_hidden("pid", param("pid"));


if($form_type == "CER" or $form_type == "AF")
{
	$form->add_section("Rental Formula");
	$form->add_checkbox("calculate_rent_avg", "Calculate average rent per year", $cer_basicdata["cer_basicdata_calculate_rent_avg"], 0, "Formula");
}
else
{
	$form->add_hidden("calculate_rent_avg", 0);
}


if($form_type == "CER" and has_access("has_full_access_to_cer"))
{
	$form->add_section("Exchange Rate LN Form");
	
	$form->add_list("ln_currency", "Currency", "select currency_id, concat(currency_symbol, ': ', currency_name) as currency from currencies order by currency_symbol", NOTNULL, $currency2["id"]);

	$form->add_edit("exchangerate2", "Exchange Rate*", NOTNULL, $currency2["exchange_rate"], TYPE_DECIMAL, 10,6);

	if($ln_basicdata["ln_basicdata_exr_date"] != NULL and $ln_basicdata["ln_basicdata_exr_date"] != '0000-00-00')
	{
		$form->add_label("exrdateln", "Fixed on", 0, to_system_date($ln_basicdata["ln_basicdata_exr_date"]));
	}

}
elseif($form_type == "CER") 
{
	$form->add_section("Exchange Rate LN Form");
	$form->add_hidden("ln_currency", $currency2["id"]);
	$form->add_label("symbol2", "Currency", 0, $currency2["symbol"]);
	$form->add_label("exchangerate2", "Exchange Rate", 0, $currency2["exchange_rate"]);

	if($ln_basicdata["ln_basicdata_exr_date"] != NULL and $ln_basicdata["ln_basicdata_exr_date"] != '0000-00-00')
	{
		$form->add_label("exrdateln", "Fixed on", 0, to_system_date($ln_basicdata["ln_basicdata_exr_date"]));
	}
}

if($form_type == "CER")
{
	$form->add_section("Exchange Rate CER");
}
elseif($form_type == "AF")
{
	$form->add_section("Exchange Rate AF");
}
else
{
	$form->add_section("Exchange Rate INR-03");
}


if(has_access("has_full_access_to_cer"))
{
	$form->add_list("cer_currency", "Currency", "select currency_id, concat(currency_symbol, ': ', currency_name) as currency from currencies order by currency_symbol", NOTNULL, $currency["id"]);
}
else
{
	$form->add_hidden("cer_currency", $currency["id"]);
	$form->add_label("symbol", "Currency", 0, $currency["symbol"]);
}

$form->add_edit("exchangerate", "Exchange Rate*", NOTNULL, $currency["exchange_rate"], TYPE_DECIMAL, 10,6);

if($cer_basicdata["cer_basicdata_exr_date"] != NULL and $cer_basicdata["cer_basicdata_exr_date"] != '0000-00-00')
{
	$form->add_label("exrdatecer", "Fixed on", 0, to_system_date($cer_basicdata["cer_basicdata_exr_date"]));
}


$form->add_section("Calculation of Depreciation");
$form->add_checkbox("cer_extend_depreciation_period", "Extend depreciation period beyond the business plan period", $cer_basicdata["cer_extend_depreciation_period"], 0, "Extension");


$form->add_section("Inflation Rate");

$inflation_rates = unserialize($cer_basicdata["cer_basicdata_inflationrates"]);

if(!$inflation_rates)
{
	$form->add_label("i0", "Inflation Rate", 0, "not available");
}
else {
	foreach($inflation_rates as $key=>$value)
	{
		$form->add_label("i" . $key, $key, 0, $value . "%");
	}
}

$form->add_section("Interest Rate");

$interest_rates = unserialize($cer_basicdata["cer_basic_data_interesrates"]);

if(!$interest_rates)
{
	$form->add_label("ir0", "Interest Rate", 0, "not available");
}
else {
	foreach($interest_rates as $key=>$value)
	{
		$form->add_label("ir" . $key, $key, 0, $value . "%");
	}
}


if(($cer_basicdata["cer_basicdata_cer_locked"] == 0 and has_access("has_access_to_his_cer")) or has_access("has_full_access_to_cer"))
{
	$form->add_section("Discounted Cash Flow Rate");
	$form->add_edit("cer_basicdata_dicount_rate", "Discounted Cash Flow Rate in %", 0, "", TYPE_DECIMAL, 12,2);

	
	if($form_type == "CER")
	{
		$form->add_section("Extraordinary Financial Closing Impacts");
		$form->add_edit("cer_bascidata_liquidation_keymoney", "Liquidation Revenue on Keymoney in %", 0, "", TYPE_DECIMAL, 12,2);
		$form->add_edit("cer_basicdata_liquidation_deposit", "Liquidation Revenue on  Deposit in %", 0, "", TYPE_DECIMAL, 12,2);
		$form->add_edit("cer_bascidata_liquidation_stock", "Liquidation Revenue on Stock in %", 0, "", TYPE_DECIMAL, 12,2);
		$form->add_edit("cer_bascicdate_liquidation_staff", "Liquidation Revenue on  Staff in %", 0, "", TYPE_DECIMAL, 12,2);


		$form->add_section("Stock in Months");
		foreach($stock_data as $year=>$stock)
		{
			$form->add_edit("s" . $year, $year, 0, $stock, TYPE_DECIMAL, 10, 2);
		}

		$form->add_section("Payment Terms in Months for Liabilities");
		foreach($paymentterm_data as $year=>$paymentterm)
		{
			$form->add_edit("p" . $year, $year, 0, $paymentterm, TYPE_INT, 10);
		}
	}
	$form->add_button("form_save", "Save Data");
}
else
{
	$form->add_section("Discounted Cash Flow Rate");
	$form->add_label("cer_basicdata_dicount_rate", "Discounted Cash Flow Rate in %", 0);

	if($form_type == "CER")
	{
		$form->add_section("Extraordinary Financial Closing Impacts");
		$form->add_label("cer_bascidata_liquidation_keymoney", "Liquidation Revenue on Keymoney in %", 0);
		$form->add_label("cer_basicdata_liquidation_deposit", "Liquidation Revenue on  Deposit in %", 0);
		$form->add_label("cer_bascidata_liquidation_stock", "Liquidation Revenue on Stock in %", 0, "");
		$form->add_label("cer_bascicdate_liquidation_staff", "Liquidation Revenue on  Staff in %", 0);


		$form->add_section("Stock in Months");
		foreach($stock_data as $year=>$stock)
		{
			$form->add_label("s" . $year, $year, 0, $stock);
		}

		$form->add_section("Payment Terms in Months for Liabilities");
		foreach($paymentterm_data as $year=>$paymentterm)
		{
			$form->add_label("p" . $year, $year, 0, $paymentterm);
		}
	}
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if($form->button("form_save"))
{
	if($form->validate())
	{
		
		
		$currency = get_currency($form->value("cer_currency"));

	
		$error = 0;
		if($form_type == "CER" and $form->validate())
		{
			
			//update stocks

			foreach($stock_data as $year=>$stock)
			{
				if(!$form->value("s" . $year))
				{
					$error = 1;
				}
				
				

			}

			//update paymentterms

			foreach($paymentterm_data as $year=>$paymentterm)
			{
				
				if(!$form->value("p" . $year))
				{
					$error = 2;
				}
			}
		}
		
		$fields = array();
    
		$value = dbquote($form->value("cer_extend_depreciation_period"));
		$fields[] = "cer_extend_depreciation_period = " . $value;
		
		$value = dbquote($form->value("calculate_rent_avg"));
		$fields[] = "cer_basicdata_calculate_rent_avg = " . $value;

		$value = dbquote($form->value("cer_basicdata_dicount_rate"));
		$fields[] = "cer_basicdata_dicount_rate = " . $value;

		$value = dbquote($form->value("cer_currency"));
		$fields[] = "cer_basicdata_currency = " . $value;
		
		$value = dbquote($form->value("exchangerate"));
		$fields[] = "cer_basicdata_exchangerate = " . $value;

		$value = dbquote($currency['factor']);
		$fields[] = "cer_basicdata_factor = " . $value;

		
		if($form_type == "CER")
		{
			$value = dbquote($form->value("cer_bascidata_liquidation_keymoney"));
			$fields[] = "cer_bascidata_liquidation_keymoney = " . $value;

			$value = dbquote($form->value("cer_basicdata_liquidation_deposit"));
			$fields[] = "cer_basicdata_liquidation_deposit = " . $value;

			$value = dbquote($form->value("cer_bascicdate_liquidation_staff"));
			$fields[] = "cer_bascicdate_liquidation_staff = " . $value;

		}

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_basicdata set " . join(", ", $fields) . " where cer_basicdata_version = 0 and cer_basicdata_project = " . param("pid");
		mysql_query($sql) or dberror($sql);

		
		if($error == 1)
		{
			$form->error("Stock in months must be indicated for all years.");
		}
		elseif($error == 2)
		{
			$form->error("Payment terms must be indicated for all years.");
		}
		else
		{
			$form->message("Your data has bee saved.");
		}
	}

	if($form_type == "CER" and $form->validate())
	{

		//update LN exchangerate
		if(has_access("has_full_access_to_cer"))
		{
			$currency2 = get_currency($form->value("ln_currency"));
			
			$fields = array();
			
			$value = dbquote($form->value("ln_currency"));
			$fields[] = "ln_basicdata_currency = " . $value;

			$value = $form->value("exchangerate2");
			$fields[] = "ln_basicdata_exchangerate = " . $value;

			$value = dbquote($currency2['factor']);
			$fields[] = "ln_basicdata_factor = " . $value;

			if($old_ln_basicdata_exchangerate <> $form->value("exchangerate2"))
			{
				$value = dbquote(date("Y-m-d"));
				$fields[] = "ln_basicdata_exr_date = " . $value;
			}

			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update ln_basicdata set " . join(", ", $fields) . " where  ln_basicdata_version = 0 and ln_basicdata_project = " . param("pid");
			mysql_query($sql) or dberror($sql);
		}


		//update stocks

		foreach($stock_data as $year=>$stock)
		{
			$fields = array();
			$value = $form->value("s" . $year);
			
			$value = dbquote($value);
			$value = ($value) ? $value : 'NULL';
			$fields[] = "cer_stock_stock_in_months = " . $value;

			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_stocks set " . join(", ", $fields) . " where cer_stock_cer_version = 0 and cer_stock_year = " . $year . " and cer_stock_project = " . param("pid");
			mysql_query($sql) or dberror($sql);


		}

		//update paymentterms

		foreach($paymentterm_data as $year=>$paymentterm)
		{
			$fields = array();
			$value = $form->value("p" . $year);
			
			$value = dbquote($value);
			$value = ($value) ? $value : 'NULL';
			$fields[] = "cer_paymentterm_in_months = " . $value;

			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_paymentterms set " . join(", ", $fields) . " where cer_paymentterm_cer_version = 0 and cer_paymentterm_year = " . $year . " and cer_paymentterm_project = " . param("pid");
			mysql_query($sql) or dberror($sql);


		}
	}

	if($form_type == "AF" and $form->validate())
	{

		//update LN exchangerate
		if(has_access("has_full_access_to_cer"))
		{
			$fields = array();
			
			$value = dbquote($form->value("cer_currency"));
			$fields[] = "ln_basicdata_currency = " . $value;

			$value = $form->value("exchangerate");
			$fields[] = "ln_basicdata_exchangerate = " . $value;

			$value = dbquote($currency['factor']);
			$fields[] = "ln_basicdata_factor = " . $value;

			if($old_cer_basicdata_exchangerate <> $form->value("exchangerate"))
			{
				$value = dbquote(date("Y-m-d"));
				$fields[] = "ln_basicdata_exr_date = " . $value;
			}

			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update ln_basicdata set " . join(", ", $fields) . " where  ln_basicdata_version = 0 and ln_basicdata_project = " . param("pid");
			mysql_query($sql) or dberror($sql);
		}
	}
}
  
/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();

if($form_type == "INR03")
{
	$page->title("INR-03 - Retail Furniture in Third-party Store: Cash Flow Calculation");
}
elseif($form_type == "AF")
{
	$page->title("Application Form: Cash Flow Calculation");
}
else
{
	$page->title("Capital Expenditure Request: Cash Flow Calculation");
}

require_once("include/tabs.php");
$form->render();

?>

<div id="whole_sale_margin" style="display:none;">
    Wholesale margin in percent on costs of products sold
</div> 

<?php

require "include/footer_scripts.php";
$page->footer();

?>