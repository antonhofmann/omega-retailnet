<?php
/********************************************************************

    cer_application_general.php

    Application Form: general information
    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";

check_access("has_access_to_cer");

if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}

/********************************************************************
    prepare all data needed
*********************************************************************/
if($cer_basicdata["cer_basicdata_firstyear"]) {
	$result = check_expenses(param("pid"), $cer_basicdata["cer_basicdata_firstyear"], $cer_basicdata["cer_basicdata_lastyear"]);
}


//get exchange rate
if(!$cer_basicdata["cer_basicdata_exchangerate"]) {
	
	$cdata = get_currency($cer_basicdata["cer_basicdata_currency"]);

	$sql_c = 'update cer_basicdata SET cer_basicdata_exchangerate = ' . dbquote($cdata["exchange_rate"]) . ',' . 
		     ' cer_basicdata_factor = ' . dbquote($cdata["factor"]) . 
		     ' where cer_basicdata_id = ' . $cer_basicdata["cer_basicdata_id"];


	$result = mysql_query($sql_c) or dberror($sql_c);

	$cdata = get_currency($cer_basicdata["cer_basicdata_currency2"]);
	$sql_c = 'update cer_basicdata SET cer_basicdata_exchangerate2 = ' . dbquote($cdata["exchange_rate"]) . ',' . 
		     ' cer_basicdata_factor2 = ' . dbquote($cdata["factor"]) . 
		     ' where cer_basicdata_id = ' . $cer_basicdata["cer_basicdata_id"];


	$result = mysql_query($sql_c) or dberror($sql_c);
}

if(!$cer_basicdata["cer_basicdata_exchangerate2"]) {
	

	$cdata = get_currency($cer_basicdata["cer_basicdata_currency2"]);
	$sql_c = 'update cer_basicdata SET cer_basicdata_exchangerate2 = ' . dbquote($cdata["exchange_rate"]) . ',' . 
		     ' cer_basicdata_factor2 = ' . dbquote($cdata["factor"]) . 
		     ' where cer_basicdata_id = ' . $cer_basicdata["cer_basicdata_id"];


	$result = mysql_query($sql_c) or dberror($sql_c);
}

//get all brands
$brands = array();
$selected_brands = array();
$sql_b = "select cer_brand_id, cer_brand_name " . 
         " from cer_brands " . 
		 " where cer_brand_active = 1";

$res_b = mysql_query($sql_b) or dberror($sql_b);
while ($row_b = mysql_fetch_assoc($res_b))
{
	$brands[$row_b["cer_brand_id"]] = $row_b["cer_brand_name"];
}

$sql_b = "select DISTINCT cer_revenue_brand_id, cer_brand_name " . 
         " from cer_revenues " . 
		 " left join cer_brands on cer_brand_id = cer_revenue_brand_id " . 
		 " where cer_revenue_cer_version = 0 " . 
		 " and cer_revenue_project = " . param("pid");


$res_b = mysql_query($sql_b) or dberror($sql_b);
while ($row_b = mysql_fetch_assoc($res_b))
{
	$brands[$row_b["cer_revenue_brand_id"]] = $row_b["cer_brand_name"];
	$selected_brands[$row_b["cer_revenue_brand_id"]] = $row_b["cer_brand_name"];
}
asort($brands);

if($cer_basicdata["cer_basicdata_firstyear"]) {
	$result = check_revenues(param("pid"), $cer_basicdata["cer_basicdata_firstyear"], $cer_basicdata["cer_basicdata_lastyear"], 0, $brands);
}

$posdata = get_pos_data($project["project_order"]);

$sql =  "select * from project_costs " .
        "where project_cost_order = " . $project["project_order"];

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

$retail_surface = $row["project_cost_sqms"];
$gross_surface = $row["project_cost_gross_sqms"];
$total_surface = $row["project_cost_totalsqms"];
$other_surface = $row["project_cost_othersqms"]  + $row["project_cost_backofficesqms"];

//neighbourhood
if($project["pipeline"] == 0)
{
	$pos_order_sql = "select * from posorders where posorder_order = " . $project["order_id"];
}
elseif($project["pipeline"] == 1)
{
	$pos_order_sql = "select * from posorderspipeline where posorder_order = " . $project["order_id"];
}

$pos_type = 0;
$neighbourhoods = array();
$res = mysql_query($pos_order_sql) or dberror($pos_order_sql);
if ($row = mysql_fetch_assoc($res))
{
	$neighbourhoods["Shop on Left Side"] = $row["posorder_neighbour_left"];
	$neighbourhoods["Shop on Right Side"] = $row["posorder_neighbour_right"];
	$neighbourhoods["Shop Across Left Side"] = $row["posorder_neighbour_acrleft"];
	$neighbourhoods["Shop Across Right Side"] = $row["posorder_neighbour_acrright"];
	$neighbourhoods["Other Brands in Area"] = $row["posorder_neighbour_brands"];
	
	$pos_type = $row["posorder_postype"];
}	


param("id", get_id_of_cer_summary(param("pid")));

$currency = get_cer_currency(param("pid"));

$months = array();
for($i=1;$i<13;$i++)
{
	$months[$i] = $i;
}

$years = array();
$s = date("Y")-5;
$l = $s+31;
for($i=$s;$i<$l;$i++)
{
	$years[$i] = $i;
}

//get RRMA: HQ Project Managers
$sql_rrmas = "select user_id, concat(user_name, ' ', user_firstname) as username " . 
             "from user_roles " .
	         "left join users on user_id = user_role_user " . 
	         "where user_role_role = 19 " .
			 "order by user_name, user_firstname";


//ckeck if the business plan period has been filled in
$business_plan_period_filled = 0;
if($cer_basicdata["cer_basicdata_firstyear"] 
   and $cer_basicdata["cer_basicdata_lastyear"] 
   and $cer_basicdata["cer_basicdata_firstmonth"]
   and $cer_basicdata["cer_basicdata_lastmonth"])
{
	$business_plan_period_filled = 1;
}

if(has_access("can_edit_catalog")) {
	//$business_plan_period_filled = 0;
}





/********************************************************************
    copy from drafts
*********************************************************************/

//check if drafts from the country are available
$project = get_project(param("pid"));
$country = $project["order_shop_address_country"];

$posleases = get_pos_leasedata($posdata["posaddress_id"], $project["project_order"]);

if(has_access("has_access_to_drafts_of_his_country")) {
	
	$sql = "select count(cer_basicdata_id) as num_recs from cer_drafts ";
	$list_filter = 'cer_basicdata_user_id = ' . user_id();

	
	//get the team members
	$user_list = '';
	$sql = 'select address_id from addresses ' .
		   'left join users on user_address = address_id ' . 
		   'where user_id = ' . user_id();

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$sql = 'select user_id from users ' .
			   'where user_address = ' . $row["address_id"] . 
			   ' and user_active = 1';

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$user_list .= $row['user_id'] . ',';
		}

		$user_list = substr($user_list, 0, strlen($user_list)-1);

		$sql = "select count(cer_basicdata_id) as num_recs from cer_drafts ";
		$list_filter = ' where cer_basicdata_user_id IN (' . $user_list . ')';
	}
	
	
}
else
{
	$sql = "select count(cer_basicdata_id) as num_recs  from cer_drafts ";
	$list_filter = '';
}

if($list_filter) {
	$list_filter .= ' and cer_basicdata_country = ' . $country;
}
else
{
	$list_filter = ' where cer_basicdata_country = ' . $country;
}

$sql_darfts = $sql . $list_filter;

$res = mysql_query($sql_darfts) or dberror($sql_darfts);
$row = mysql_fetch_assoc($res);

if($row["num_recs"] == 0) {
	$business_plan_period_filled = 1;
}

if(has_access("has_access_to_cer_drafts") and $business_plan_period_filled == 0)
{

	$copy_form = new Form("cer_summary", "cer_summary");

	$copy_form->add_comment("Since you have not yet set a businessplan period you may want to copy data from an existing business plan draft.");
	$copy_form->add_hidden("pid", param("pid"));
	$copy_form->add_button("copy_from_draft", "Yes I want to copy data from a Business Plan Draft");
}


/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_summary", "cer_summary");

include("include/project_head.php");

$form->add_hidden("pid", param("pid"));

$form->add_section(" ");
$form->add_label("start", "Project Starting Date", 0, to_system_date($project["order_date"]));
if($project["project_projectkind"] != 4 and $project["project_projectkind"] != 5) //Take Over and lease renewal
{
	$form->add_label("planned_opening_date", "Client's preferred Shop Opening Date", 0, to_system_date($project["project_planned_opening_date"]));
}

$form->add_label("real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]));
$form->add_label("actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]));


$form->add_section(" ");

if(($cer_basicdata["cer_basicdata_cer_locked"] == 0 and has_access("has_access_to_his_cer")) or has_access("has_full_access_to_cer"))
{
	/*
	$form->add_section("Project Duration");
	$form->add_edit("basicdata_project_start", "Start Date", NOTNULL, to_system_date($cer_basicdata["cer_basicdata_project_start"]), TYPE_DATE);
	$form->add_edit("basicdata_project_end", "End Date", NOTNULL, to_system_date($cer_basicdata["cer_basicdata_project_end"]), TYPE_DATE);
	*/


	$form->add_section("Busines Plan Period");
	$form->add_list("basicdata_firstyear", "First Year*", $years, NOTNULL, $cer_basicdata["cer_basicdata_firstyear"]);
	$form->add_list("basicdata_firstmonth", "Month of First Year*", $months, NOTNULL, $cer_basicdata["cer_basicdata_firstmonth"]);
	$form->add_list("basicdata_lastyear", "Last Year*", $years, NOTNULL, $cer_basicdata["cer_basicdata_lastyear"]);
	$form->add_list("basicdata_lastmonth", "Month of Last Year*", $months, NOTNULL, $cer_basicdata["cer_basicdata_lastmonth"]);
	$form->add_label("bp_duration", "Business Plan Duration", 0, $business_plan_period);

	if(count($posleases) > 0)
	{
		$form->add_section("Lease Period");
		$form->add_label("l_startdate", "Start Date", NOTNULL, to_system_date($posleases["poslease_startdate"]), TYPE_DATE);
		$form->add_label("l_enddate", "Expiry Date", NOTNULL, to_system_date($posleases["poslease_enddate"]), TYPE_DATE);
		$form->add_label("rental_duration", "Rental Duration", 0, $posleases["rental_duration"]);
	}

	$form->add_section("Other Information");
	//$form->add_list("project_hq_project_manager", "HQ Project Manager*", $sql_rrmas, NOTNULL, $project["project_hq_project_manager"]);

	$line = "concat(user_name, ' ', user_firstname)";
	if ($project["project_local_retail_coordinator"])
	{
		$form->add_lookup("local_retail_coordinator", "Local Project Manager", "users", $line, 0, $project["project_local_retail_coordinator"]);
	}
	else
	{
		$form->add_label("local_retail_coordinator", "Local Project Manager");
	}

	if($form_type == "CER")
	{
		$form->add_edit("basicdata_local_architect", "Local Architect", 0, $cer_basicdata["cer_basicdata_local_architect"]);

		$form->add_section(" ");
		$form->add_label("currency", "Client's Currency", 0, $currency["symbol"]);

		$form->add_section("Description of Capital Expenditure Project");
		$form->add_multiline("cer_summary_in01_description", "Description and Strategy", 8);
		
		//$form->add_comment("Justification: Please write a comment about the follwing irems.");
		//$form->add_multiline("cer_summary_in01_strategy", "Strategy", 6);
		//$form->add_multiline("cer_summary_in01_investment", "Investment (Overview)", 4);
		$form->add_multiline("cer_summary_in01_benefits", "Benefits, Risks and Alternatives", 8);
		//$form->add_multiline("cer_summary_in01_alternative", "Alternative taken into consideration", 4);
		//$form->add_multiline("cer_summary_in01_risks", "Risks", 4);
		
		if(count($brands) > 1)
		{
			$form->add_section("Brands included in the business plan");
			foreach($brands as $brand_id=>$brand_name)
			{
				if(array_key_exists($brand_id, $selected_brands))
				{
					$form->add_checkbox("brand_" . $brand_id, $brand_name, 1, 0, "Include");
				}
				else
				{
					$form->add_checkbox("brand_" . $brand_id, $brand_name, 0, 0, "Include");
				}
			}
		}
		else
		{
			foreach($brands as $brand_id=>$brand_name)
			{
				$form->add_hidden("brand_" . $brand_id, 1);
			}
		}


		/*
		$form->add_section("Signature of requesting entity (PC, Country)");
		$form->add_comment("Please put in the names of the persons.");
		$form->add_edit("cer_summary_in01_sig01", "Country Manager");
		$form->add_edit("cer_summary_in01_sig02", "Service Center Manager");
		$form->add_edit("cer_summary_in01_sig03", "Brand Manager");
		*/

		$form->add_section("Other Information");
		$form->add_edit("cer_summary_in01_review_date", "Post Completion Review Date", 0, "", TYPE_DATE);
		$form->add_edit("cer_summary_in01_cernr", "CER Number");
		$form->add_edit("cer_summary_in01_prot01", "Protocol Number 1");
		$form->add_edit("cer_summary_in01_prot02", "Protocol Number 2");

		$form->add_edit("cer_summary_in01_approval_date", "Approval Date", 0, "", TYPE_DATE);
		$form->add_edit("cer_summary_in01_approval_by", "Approved by");
	}
	else
	{
		
		$form->add_section("Key Points and General Remarks");
		$form->add_comment("Please indicate key information about the location which you feel is important to know and which is not yet mentioned with the businessplan, photos or mall maps.");
		$form->add_multiline("cer_summary_in01_description", "Key Points*", 8, NOTNULL);

		if(count($brands) > 1)
		{
			$form->add_section("Brands included in the business plan");
			foreach($brands as $brand_id=>$brand_name)
			{
				if(array_key_exists($brand_id, $selected_brands))
				{
					$form->add_checkbox("brand_" . $brand_id, $brand_name, 1, 0, "Include");
				}
				else
				{
					$form->add_checkbox("brand_" . $brand_id, $brand_name, 0, 0, "Include");
				}
			}
		}
		else
		{
			foreach($brands as $brand_id=>$brand_name)
			{
				$form->add_hidden("brand_" . $brand_id, 1);
			}
		}
		
		/*
		$form->add_section("Signature of requesting entity (PC, Country)");
		$form->add_edit("cer_summary_in01_sig03", "Brand Manager");
		*/

		$form->add_hidden("basicdata_local_architect");


		
	}

	$form->add_section("Surfaces");
	$form->add_label("s0", "Gross Surface sqms", 0, $gross_surface);
	$form->add_label("s1", "Total Surface sqms", 0, $total_surface);
	$form->add_label("s2", "Sales Surface sqms", 0, $retail_surface);
	$form->add_label("s3", "Other Surface sqms", 0, $other_surface);

	

	
	$form->add_section("Neighbourhood");
	$form->add_edit("posorder_neighbour_left", "Shop on Left Side*", 0, $neighbourhoods["Shop on Left Side"]);
	$form->add_edit("posorder_neighbour_right", "Shop on Right Side*", 0, $neighbourhoods["Shop on Right Side"]);
	$form->add_edit("posorder_neighbour_acrleft", "Shop Across Left Side*", 0, $neighbourhoods["Shop Across Left Side"]);
	$form->add_edit("posorder_neighbour_acrright", "Shop Across Right Side*", 0, $neighbourhoods["Shop Across Right Side"]);
	$form->add_multiline("posorder_neighbour_brands", "Other Brands in Area*", 4, 0, $neighbourhoods["Other Brands in Area"]);
	    

	$form->add_button(FORM_BUTTON_SAVE, "Save Data");

}
else
{
	/*
	$form->add_section("Project Duration");
	$tmp = to_system_date($cer_basicdata["cer_basicdata_project_start"]) . " / " . to_system_date($cer_basicdata["cer_basicdata_project_end"]);
	$form->add_label("project_duration", "From / To", 0, $tmp);
    */
	
	
	$form->add_section("Business Plan Period");
	$tmp = $cer_basicdata["cer_basicdata_firstyear"] . "-" . $cer_basicdata["cer_basicdata_firstmonth"] . " / " .
	$cer_basicdata["cer_basicdata_lastyear"] . "-" . $cer_basicdata["cer_basicdata_lastmonth"] . " / " . $business_plan_period;
	$form->add_label("business_plan_period", "From / To / Duration", 0, $tmp);

	if(count($posleases) > 0)
	{
		
		$rental_duration = "";
		if($posleases["poslease_startdate"] != NULL 
			and $posleases["poslease_startdate"] != '0000-00-00' 
			and $posleases["poslease_enddate"] != NULL
			and $posleases["poslease_enddate"] != '0000-00-00')
		{
			$diff = abs(strtotime($posleases["poslease_enddate"]) - strtotime($posleases["poslease_startdate"]));
			$years = floor($diff / (365*60*60*24));
			$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));

			if($months == 12)
			{
				$months = 0;
				$years++;
			}

			$rental_duration = $years . " years and " . $months . " months";


		}
		$form->add_section("Lease Period");
		$form->add_label("l_startdate", "Start Date", NOTNULL, to_system_date($posleases["poslease_startdate"]), TYPE_DATE);
		$form->add_label("l_enddate", "Expiry Date", NOTNULL, to_system_date($posleases["poslease_enddate"]), TYPE_DATE);
		$form->add_label("rental_duration", "Rental Duration", 0, $rental_duration);
	}

	$form->add_section("Other Information");
	$line = "concat(user_name, ' ', user_firstname)";
	//$form->add_lookup("project_hq_project_manager", "HQ Project Manager", "users", $line, 0, dbquote($project["project_hq_project_manager"]));
	$form->add_lookup("local_retail_coordinator", "Local Project Manager", "users", $line, 0, dbquote($project["project_local_retail_coordinator"]));

	if($form_type == "CER")
	{
		$form->add_label("basicdata_local_architect", "Local Architect", 0, $cer_basicdata["cer_basicdata_local_architect"]);

		$form->add_section(" ");
		$form->add_label("currency", "Client's Currency", 0, $currency["symbol"]);

		$form->add_section("Description of Capital Expenditure Project");
		$form->add_label("cer_summary_in01_description", "Description and Strategy");
		//$form->add_comment("Justification: Please write a comment about the follwing irems.");
		//$form->add_label("cer_summary_in01_strategy", "Strategy");
		//$form->add_label("cer_summary_in01_investment", "Investment (Overview)");
		$form->add_label("cer_summary_in01_benefits", "Benefits, Risks and Alternatives");
		//$form->add_label("cer_summary_in01_alternative", "Alternative taken into consideration");
		//$form->add_label("cer_summary_in01_risks", "Risks");

		if(count($brands) > 1)
		{
			$form->add_section("Brands included in the business plan");
			foreach($brands as $brand_id=>$brand_name)
			{
				if(array_key_exists($brand_id, $selected_brands))
				{
					$form->add_checkbox("brand_" . $brand_id, $brand_name, 1, 0, "Include");
				}
				else
				{
					$form->add_checkbox("brand_" . $brand_id, $brand_name, 0, 0, "Include");
				}
			}
		}
		else
		{
			foreach($brands as $brand_id=>$brand_name)
			{
				$form->add_hidden("brand_" . $brand_id, 1);
			}
		}
		
		/*
		$form->add_section("Signature of requesting entity (PC, Country)");
		$form->add_comment("Please put in the names of the persons.");
		$form->add_label("cer_summary_in01_sig01", "Country Manager");
		$form->add_label("cer_summary_in01_sig02", "Service Center Manager");
		$form->add_label("cer_summary_in01_sig03", "Brand Manager");
		*/

		$form->add_section("Other Information");
		$form->add_label("cer_summary_in01_review_date", "Post Completion Review Date");
		$form->add_label("cer_summary_in01_cernr", "CER Number");
		$form->add_label("cer_summary_in01_prot01", "Protocol Number 1");
		$form->add_label("cer_summary_in01_prot02", "Protocol Number 2");

		$form->add_label("cer_summary_in01_approval_date", "Approval Date");
		$form->add_label("cer_summary_in01_approval_by", "Approved by");


	}
	else
	{
		$form->add_section("Key Points and General Remarks");
		$form->add_label("cer_summary_in01_description", "Key Points");

		
		if(count($brands) > 1)
		{
			$form->add_section("Brands included in the business plan");
			foreach($brands as $brand_id=>$brand_name)
			{
				if(array_key_exists($brand_id, $selected_brands))
				{
					$form->add_checkbox("brand_" . $brand_id, $brand_name, 1, 0, "Include");
				}
				else
				{
					$form->add_checkbox("brand_" . $brand_id, $brand_name, 0, 0, "Include");
				}
			}
		}
		else
		{
			foreach($brands as $brand_id=>$brand_name)
			{
				$form->add_hidden("brand_" . $brand_id, 1);
			}
		}
		$form->add_section("Signature of requesting entity (PC, Country)");
		$form->add_label("cer_summary_in01_sig02", "Brand Division Manager");
		$form->add_hidden("basicdata_local_architect");
	}


	$form->add_section("Surfaces");
	$form->add_label("s0", "Gross Surface sqms", 0, $gross_surface);
	$form->add_label("s1", "Total Surface sqms", 0, $total_surface);
	$form->add_label("s2", "Sales Surface sqms", 0, $retail_surface);
	$form->add_label("s3", "Other Surface sqms", 0, $other_surface);

	$form->add_section("Neighbourhood");
	$form->add_label("posorder_neighbour_left", "Shop on Left Side", 0, $neighbourhoods["Shop on Left Side"]);
	$form->add_label("posorder_neighbour_right", "Shop on Right Side", 0, $neighbourhoods["Shop on Right Side"]);
	$form->add_label("posorder_neighbour_acrleft", "Shop Across Left Side", 0, $neighbourhoods["Shop Across Left Side"]);
	$form->add_label("posorder_neighbour_acrright", "Shop Across Right Side", 0, $neighbourhoods["Shop Across Right Side"]);
	$form->add_label("posorder_neighbour_brands", "Other Brands in Area", 0, $neighbourhoods["Other Brands in Area"]);


}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
if(has_access("has_access_to_cer_drafts") and $business_plan_period_filled == 0)
{
	$copy_form->populate();
	$copy_form->process();

	
	if($form->button("copy_from_draft"))
	{
		$link = 'cer_application_general_copy_draft.php?pid=' . param("pid") . '&p=' . $posorder_present . '&ft=' . $form_type;
		redirect($link);
	}
}

$form->populate();
$form->process();



if($form->button(FORM_BUTTON_SAVE))
{
	
	$form->add_validation("{basicdata_firstyear} <= {basicdata_lastyear}", "Hey, the last year must be equal or greater than the first year!");

	$brand_selected = false;
	$selected_brands = array();
	foreach($brands as $brand_id=>$brand_name)
	{
		if(array_key_exists("brand_" .$brand_id, $_POST) and $_POST["brand_" .$brand_id] == 1)
		{
			$brand_selected = true;
			$selected_brands[$brand_id] = $brands[$brand_id];
		}
	}

	if($brand_selected == false)
	{
		$form->error("Please select at least one brand to be included in the business plan!");
	}
	elseif($form->validate())
	{
		
		//update cer_basic_data
		$fields = array();
    
		$value = dbquote($form->value("basicdata_firstyear"));
		$fields[] = "cer_basicdata_firstyear = " . $value;

		$value = dbquote($form->value("basicdata_firstmonth"));
		$fields[] = "cer_basicdata_firstmonth = " . $value;

		$value = dbquote($form->value("basicdata_lastyear"));
		$fields[] = "cer_basicdata_lastyear = " . $value;

		$value = dbquote($form->value("basicdata_lastmonth"));
		$fields[] = "cer_basicdata_lastmonth = " . $value;

		$value = dbquote($form->value("basicdata_local_architect"));
		$fields[] = "cer_basicdata_local_architect = " . $value;

		
		/*
		$value = dbquote(to_system_date($form->value("basicdata_project_start")));
		$fields[] = "cer_basicdata_project_start = " . $value;

		$value = dbquote(to_system_date($form->value("basicdata_project_end")));
		$fields[] = "cer_basicdata_project_end = " . $value;
		*/


		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_basicdata set " . join(", ", $fields) . " where cer_basicdata_version = 0 and cer_basicdata_project = " . param("pid");
		mysql_query($sql) or dberror($sql);
		
		
		//update posorders
		
			
		$fields = array();

		$value = dbquote($form->value("posorder_neighbour_left"));
		$fields[] = "posorder_neighbour_left = " . $value;

		$value = dbquote($form->value("posorder_neighbour_right"));
		$fields[] = "posorder_neighbour_right = " . $value;

		$value = dbquote($form->value("posorder_neighbour_acrleft"));
		$fields[] = "posorder_neighbour_acrleft = " . $value;

		$value = dbquote($form->value("posorder_neighbour_acrright"));
		$fields[] = "posorder_neighbour_acrright = " . $value;

		$value = dbquote($form->value("posorder_neighbour_brands"));
		$fields[] = "posorder_neighbour_brands = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
		
		
		if($project["pipeline"] == 0)
		{
			$sql = "update posorders set " . join(", ", $fields) . " where posorder_order = " . $project["order_id"];
		}
		else
		{
			$sql = "update posorderspipeline set " . join(", ", $fields) . " where posorder_order = " . $project["order_id"];
		}
		mysql_query($sql) or dberror($sql);

		
		
		//update relations and uploads
		if($form_type == "INR03")
		{
			if($project['project_fagrstart'] == NULL or $project['project_fagrstart'] == '0000-00-00') {
				
				$start = $cer_basicdata["cer_basicdata_firstyear"] . "-" . $cer_basicdata["cer_basicdata_firstmonth"] . "-01";
				
				
				$end = $cer_basicdata["cer_basicdata_lastyear"] . "-" . $cer_basicdata["cer_basicdata_lastmonth"] . "-" . cal_days_in_month(CAL_GREGORIAN,$cer_basicdata["cer_basicdata_lastmonth"],$cer_basicdata["cer_basicdata_lastyear"]);

				$sql = "update projects set 
				       project_fagrstart = " . dbquote($start) . ", 
					   project_fagrend  = " . dbquote($end) . "
					   where project_id = " . dbquote($project['project_id']);

				mysql_query($sql) or dberror($sql);
			
			
				$sql = "update cer_basicdata Set cer_basicdata_franchsiee_already_partner = 1
				       where cer_basicdata_id = " . dbquote($cer_basicdata["cer_basicdata_id"]);

				mysql_query($sql) or dberror($sql);
			}
		}
		//update project
		/*
		$fields = array();
    
		$value = dbquote($form->value("project_hq_project_manager"));
		$fields[] = "project_hq_project_manager = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " .$value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
		
   
		$sql = "update projects set " . join(", ", $fields) . " where project_id = " . param("pid");
		mysql_query($sql) or dberror($sql);

		*/

		
		//delete brands not being selected
		$brands_not_selected = array();
		foreach($brands as $brand_id=>$brand_name)
		{
			if(!array_key_exists($brand_id, $selected_brands))
			{
				$brands_not_selected[] = $brand_id;
			}
		}

		if(count($brands_not_selected) > 0)
		{
			$sql = "delete from cer_revenues " . 
				   " where cer_revenue_cer_version = 0 " .
				   " and cer_revenue_project = " . param("pid") . 
				   " and cer_revenue_brand_id in (" . implode(",", $brands_not_selected) . ") ";

			$result = mysql_query($sql) or dberror($sql);

		}
		
		
		//check if expense records exist
		$result = check_expenses(param("pid"), $form->value("basicdata_firstyear"), $form->value("basicdata_lastyear"));
		$result = check_paymentterms(param("pid"), $form->value("basicdata_firstyear"), $form->value("basicdata_lastyear"));

		//check if cer_revenue_records exist
		$result = check_revenues(param("pid"), $form->value("basicdata_firstyear"), $form->value("basicdata_lastyear"), 0, $selected_brands);
		$result = check_stocks(param("pid"), $form->value("basicdata_firstyear"), $form->value("basicdata_lastyear"));
		
		$result = update_turnoverbased_rental_cost($project_id = 0, $version = 0);
		$result = update_cost_of_products_sold(param("pid"));
				
		$link = "cer_application_general.php?pid=" . param("pid");
		redirect($link);
	}

}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();

if($form_type == "INR03")
{
	$page->title("INR-03 - Retail Furniture in Third-party Store: General Information");
}
elseif($form_type == "AF")
{
	$page->title("Application Form: General Information");
}
else
{
	$page->title("Capital Expenditure Request: General Information");
}
if($business_plan_period_filled == 1)
{
	require_once("include/tabs.php");
}
else
{
	
}

if(has_access("has_access_to_cer_drafts") and $business_plan_period_filled == 0)
{
	$copy_form->render();
	echo '<p>&nbsp;</p>';
}

$form->render();


require "include/footer_scripts.php";
$page->footer();

?>