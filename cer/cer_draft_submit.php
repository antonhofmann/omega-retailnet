<?php
/********************************************************************

    cer_draft_submit.php

    Submit Business Plan Draft

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-02-21
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-02-21
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require "include/get_draft_functions.php";

check_access("has_access_to_cer_drafts");

/********************************************************************
    prepare all data needed
*********************************************************************/
$user_data = get_user(user_id());

$title = "";
$basicdata = array();

$basicdata = get_draft_basicdata(param("did"));
$title = $basicdata["cer_basicdata_title"];

$recipients = array();
$text = "";

$sql = 'select * from mail_alert_types where mail_alert_type_id = 9';
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$recipients[0] = $row["mail_alert_type_sender_email"];
	if($row["mail_alert_type_cc1"]) {
		$recipients[1] = $row["mail_alert_type_cc1"];
	}
	if($row["mail_alert_type_cc2"]) {
		$recipients[2] = $row["mail_alert_type_cc2"];
	}
	if($row["mail_alert_type_cc3"]) {
		$recipients[3] = $row["mail_alert_type_cc3"];
	}
	if($row["mail_alert_type_cc4"]) {
		$recipients[4] = $row["mail_alert_type_cc4"];
	}

	$text = $row["mail_alert_mail_text"];
}

$form = new Form("cer_drafts", "CER/AF Draft");

$form->add_section("Draft");
$form->add_hidden("did", param("did"));


$tmp = '';
if(count($recipients) > 0)
{
	foreach($recipients as $recipient) {
		$tmp .= $recipient . ' ';
	}
}

$form->add_label("l1", "Recipients", 0, $tmp);
$form->add_multiline("message", "Message", 8, NOTNULL);

if(!param("message")) {
	$form->add_button("submit", "Submit Draft");
}


$form->populate();
$form->process();

if($form->button("submit")) {
	
	if(count($recipients) > 0) {
		
	
		$subject = "New Business Plan Draft - " . $title;
		$sender_email = $user_data["email"];
		$sender_name = $user_data["firstname"] . ' ' . $user_data["name"];

		$bodytext0 = $text . " " . $user_data["firstname"] . " " . $user_data["name"] . ".";

		$bodytext0 .= "\n\n" . $form->value('message') . "\n\n";

		$link ="cer_draft.php?did=" . param("did") . "&id=" . param("did");

		$bodytext = $bodytext0 . "\nClick below to have direct access to the draft:\n";
		$bodytext = $bodytext .  APPLICATION_URL ."/cer/" . $link . "\n\n";           
		
		
		//$bodytext .= "Thank you very much.\r\nKind regards, " . $sender_name;

		$mail = new Mail();
		$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
		$mail->set_sender($sender_email, $sender_name);
		
		
		foreach($recipients as $recipient) {
			$mail->add_recipient($recipient);
		}
		
		$mail->add_text($bodytext);

		$result = $mail->send();

		$form->message("Your draft was submitted successfully.");
	}
}

$page = new Page("cer_drafts");
require "include/draft_page_actions.php";
$page->header();



$page->title($title . ": Submit Draft");

$form->render();
$page->footer();

?>