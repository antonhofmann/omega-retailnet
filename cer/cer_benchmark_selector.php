<?php
/********************************************************************

    cer_benchmark_selector.php

    Enter filter criteria for cer benchmark

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-13
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2007-12-07
    Version:        1.1.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../include/page_modal.php";

check_access("has_access_to_cer_benchmarks");


/********************************************************************
    prepare all data needed
*********************************************************************/
$sql_region = "select salesregion_id, salesregion_name ".
               "from salesregions order by salesregion_name";


$filter = array();
//get benchmark parameters
$regions = array();
if(id() > 0)
{
	$sql = "select * from cer_benchmarks " .
		   "where cer_benchmark_id = " . param("id");

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{

		$filter = unserialize($row["cer_benchmark_filter"]);

	}
}

if(count($filter["re"]) > 0)
{
	$regions = explode("-", $filter["re"]);
	$tmp_filter = "";
	foreach($regions as $key=>$region)
	{
		$tmp_filter = $tmp_filter . $region . ",";
	}
	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);

	if($tmp_filter)
	{
		$tmp_filter = "where country_salesregion in (" . $tmp_filter . ") ";
	}
}
$sql_country = "select DISTINCT country_id, country_name " .
			   "from posaddresses " .
			   "left join countries on country_id = posaddress_country " .
			   $tmp_filter . 
			   " order by country_name";


$tmp_filter = "";
if(count($filter["co"]) > 0)
{
	$countries = explode("-", $filter["co"]);
	
	foreach($countries as $key=>$country)
	{
		$tmp_filter = $tmp_filter . $country . ",";
	}

	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);

	if($tmp_filter)
	{
		$tmp_filter = "where posaddress_country in (" .  $tmp_filter . ") ";
	}
	elseif(count($filter["re"]) > 0)
	{
		$regions = explode("-", $filter["re"]);
		
		foreach($regions as $key=>$region)
		{
			$tmp_filter = $tmp_filter . $region . ",";
		}

		$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);

		if($tmp_filter)
		{
			$tmp_filter = "where country_salesregion in (" .  $tmp_filter . ") ";
		}
	}
}
$sql_cities = "select DISTINCT posaddress_place " .
			  "from posaddresses " .
			  "left join countries on country_id = posaddress_country " .
			  $tmp_filter . 
			  " order by posaddress_place";





$sql_areas = "select posareatype_id, posareatype_name ".
              "from posareatypes order by posareatype_name";

$sql_project_cost_types = "select project_costtype_id, project_costtype_text ".
						  "from project_costtypes " .
                          "order by project_costtype_text";

$sql_product_lines = "select product_line_id, product_line_name ".
                     "from product_lines " . 
                     "where product_line_mis = 1 " . 
                     "   order by product_line_name";




$tmp_filter = "";
if(count($filter["pl"]) > 0)
{
	$productlines = explode("-", $filter["pl"]);
	$tmp_filter = "";
	foreach($productlines as $key=>$productline)
	{
		$tmp_filter = $tmp_filter . $productline . ",";
	}
	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);

	if($tmp_filter)
	{
		$tmp_filter = "where productline_subclass_productline in (" . $tmp_filter . ") ";
	}
}
$sql_product_line_subclasses = "select productline_subclass_id, productline_subclass_name ".
						 "from productline_subclasses " . 
						 $tmp_filter . 
						 "   order by productline_subclass_name";

$sql_pos_types = "select DISTINCT postype_name ".
                     "from postypes " .
                     "order by postype_name";

$sql_possubclasses = "select possubclass_id, possubclass_name ".
                     "from possubclasses " .
                     "order by possubclass_name";

$sql_project_kinds = "select projectkind_id, projectkind_name ".
                     "from projectkinds " . 
					 " where projectkind_id in (1,2,3,5,6) " . 
                     "   order by projectkind_name";

/********************************************************************
    save data
*********************************************************************/
if(param("save_form"))
{
	$new_filter = array();
	
	$new_filter["re"] =  $filter["re"];
	$new_filter["co"] =  $filter["co"];
	$new_filter["ci"] =  $filter["ci"];
	$new_filter["ar"] =  $filter["ar"];
	$new_filter["pct"] =  $filter["pct"];
	$new_filter["pl"] =  $filter["pl"];
	$new_filter["pls"] =  $filter["pls"];
	$new_filter["pt"] =  $filter["pt"];
	$new_filter["sc"] =  $filter["sc"];



	$new_filter["pkinds"] = "";
	if(array_key_exists("pkinds", $filter)) {
		$new_filter["pkinds"] =  $filter["pkinds"];
	}
	

	if(param("save_form") == "re") //Regions
	{
		$RE = "";

		$res = mysql_query($sql_region) or dberror($sql_region);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["RE_" . $row["salesregion_id"]])
			{
				$RE .=$row["salesregion_id"] . "-";
			}
		}
		
		$new_filter["re"] = $RE;
	}
	elseif(param("save_form") == "co") //Countries
	{
		$CO = "";

		$res = mysql_query($sql_country) or dberror($sql_country);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["CO_" . $row["country_id"]])
			{
				$CO .=$row["country_id"] . "-";
			}
		}
		
		$new_filter["co"] = $CO;
	}
	elseif(param("save_form") == "ci") //Cities
	{
		$CI = "";

		$res = mysql_query($sql_cities) or dberror($sql_cities);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["CI_" . md5($row["posaddress_place"])])
			{
				$CI .= trim($row["posaddress_place"]) . "-";
			}
		}
	
		$new_filter["ci"] = $CI;

	}
	elseif(param("save_form") == "ar") //Areas
	{
		$AR = "";

		$res = mysql_query($sql_areas) or dberror($sql_areas);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["AR_" . $row["posareatype_id"]])
			{
				$AR .=$row["posareatype_id"] . "-";
			}
		}
		
		$new_filter["ar"] = $AR;
	}
	elseif(param("save_form") == "pct") //project cost types
	{
		$PCT = "";

		$res = mysql_query($sql_project_cost_types) or dberror($sql_project_cost_types);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PCT_" . $row["project_costtype_id"]])
			{
				$PCT .=$row["project_costtype_id"] . "-";
			}
		}
		$new_filter["pct"] = $PCT;
	}
	elseif(param("save_form") == "pl") //Product Lines
	{
		$PL = "";

		$res = mysql_query($sql_product_lines) or dberror($sql_product_lines);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PL_" . $row["product_line_id"]])
			{
				$PL .=$row["product_line_id"] . "-";
			}
		}
		
		$new_filter["pl"] = $PL;
	}

	elseif(param("save_form") == "pls") //Product Line Subclasses
	{
		$PLS = "";

		$res = mysql_query($sql_product_line_subclasses) or dberror($sql_product_line_subclasses);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PLS_" . $row["productline_subclass_id"]])
			{
				$PLS .=$row["productline_subclass_id"] . "-";
			}
		}
		
		$new_filter["pls"] = $PLS;
	}
	elseif(param("save_form") == "pkinds") //Project Kinds
	{
		$PK = "";

		$res = mysql_query($sql_project_kinds) or dberror($sql_project_kinds);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PK_" . $row["projectkind_id"]])
			{
				$PK .=$row["projectkind_id"] . "-";
			}
		}
		
		$new_filter["pkinds"] = $PK;
	}
	elseif(param("save_form") == "pt") //Project Types
	{
		$PT = "";

		$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PT_" . $row["postype_name"]])
			{
				$PT .=$row["postype_name"] . "-";
			}
		}
		
		$new_filter["pt"] = $PT;
	}
	elseif(param("save_form") == "sc") //POS Type Subclasses
	{
		$SC = "";

		$res = mysql_query($sql_possubclasses) or dberror($sql_possubclasses);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["SC_" . $row["possubclass_id"]])
			{
				$SC .=$row["possubclass_id"] . "-";
			}
		}
		
		$new_filter["sc"] = $SC;
	}

	$new_filter["dfrom"] =  $filter["dfrom"];
	$new_filter["mfrom"] =  $filter["mfrom"];
	$new_filter["dto"] = $filter["dto"];
	$new_filter["mto"] = $filter["mto"];

	$sql = "update cer_benchmarks " . 
		   "set cer_benchmark_filter = " . dbquote(serialize($new_filter)) . 
		   " where cer_benchmark_id = " . param("id");

	$result = mysql_query($sql) or dberror($sql);
	
}

/********************************************************************
    get existant filter
*********************************************************************/
$filter = array();
//get benchmark parameters
$regions = array();
if(id() > 0)
{
	$sql = "select * from cer_benchmarks " .
		   "where cer_benchmark_id = " . param("id");

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{

		$filter = unserialize($row["cer_benchmark_filter"]);
		$regions = explode("-", $filter["re"]);
		$countries = explode("-", $filter["co"]);
		$cities = explode("-", $filter["ci"]);
		$areas = explode("-", $filter["ar"]);
		$project_cost_types = explode("-", $filter["pct"]);
		$product_lines = explode("-", $filter["pl"]);
		if(array_key_exists("pls", $filter)) {
			$product_line_subclasses = explode("-", $filter["pls"]);
		}
		else
		{
			$product_line_subclasses = array();
		}
		$pos_types = explode("-", $filter["pt"]);
		$subclasses = explode("-", $filter["sc"]);

		if(array_key_exists("pkinds", $filter)) {
			$project_kinds = explode("-", $filter["pkinds"]);
		}
		else
		{
			$project_kinds = array();
		}
	}
}


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new form("cer_benchmarks", "benchmark_selector");

$page_title = "";
if(param("s") == "re") // regions
{
	$page_title = "Region Selector";
	$form->add_hidden("save_form", "re");
	$form->add_label("L4", "", "", "Select the Regions to be included in the Benchmark");
	$res = mysql_query($sql_region) or dberror($sql_region);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["salesregion_id"], $regions))
		{
			$form->add_checkbox("RE_" . $row["salesregion_id"], $row["salesregion_name"], true);
		}
		else
		{
			$form->add_checkbox("RE_" . $row["salesregion_id"], $row["salesregion_name"], false);
		}
		
	}
}
elseif(param("s") == "co") // countries
{
	$page_title = "Country Selector";
	$form->add_hidden("save_form", "co");
	$form->add_label("L4", "", "", "Select the Countries to be included in the Benchmark");
	$res = mysql_query($sql_country) or dberror($sql_country);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["country_id"], $countries))
		{
			$form->add_checkbox("CO_" . $row["country_id"], $row["country_name"], true);
		}
		else
		{
			$form->add_checkbox("CO_" . $row["country_id"], $row["country_name"], false);
		}
		
	}
}
elseif(param("s") == "ci") // cities
{
	$page_title = "City Selector";
	$form->add_hidden("save_form", "ci");
	$form->add_label("L4", "", "", "Select the Cities to be included in the Benchmark");
	$res = mysql_query($sql_cities) or dberror($sql_cities);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array(trim($row["posaddress_place"]), $cities))
		{
			$form->add_checkbox("CI_" . md5($row["posaddress_place"]), $row["posaddress_place"], true);
		}
		else
		{
			$form->add_checkbox("CI_" . md5($row["posaddress_place"]),  $row["posaddress_place"], false);
		}
		
	}
}
elseif(param("s") == "ar") // Neighbourhood Areas
{
	$page_title = "Area Selector";
	$form->add_hidden("save_form", "ar");
	$form->add_label("L4", "", "", "Select the Areas to be included in the Benchmark");
	$res = mysql_query($sql_areas) or dberror($sql_areas);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["posareatype_id"], $areas))
		{
			$form->add_checkbox("AR_" . $row["posareatype_id"], $row["posareatype_name"], true);
		}
		else
		{
			$form->add_checkbox("AR_" . $row["posareatype_id"], $row["posareatype_name"], false);
		}
		
	}
}
elseif(param("s") == "pct") // Project Cost Types
{
	$page_title = "Legal Type Selector";
	$form->add_hidden("save_form", "pct");
	$form->add_label("L4", "", "", "Select the Legal types to be included in the Benchmark");
	$res = mysql_query($sql_project_cost_types) or dberror($sql_project_cost_types);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["project_costtype_id"], $project_cost_types))
		{
			$form->add_checkbox("PCT_" . $row["project_costtype_id"], $row["project_costtype_text"], true);
		}
		else
		{
			$form->add_checkbox("PCT_" . $row["project_costtype_id"], $row["project_costtype_text"], false);
		}
		
	}
}
elseif(param("s") == "pl") // Product Lines
{
	$page_title = "Product Line Selector";
	$form->add_hidden("save_form", "pl");
	$form->add_label("L4", "", "", "Select the Product Lines to be included in the Benchmark");
	$res = mysql_query($sql_product_lines) or dberror($sql_product_lines);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["product_line_id"], $product_lines))
		{
			$form->add_checkbox("PL_" . $row["product_line_id"], $row["product_line_name"], true);
		}
		else
		{
			$form->add_checkbox("PL_" . $row["product_line_id"], $row["product_line_name"], false);
		}
		
	}
}
elseif(param("s") == "pls") // Product Line Subclasse
{
	$page_title = "Product Line Subclass Selector";
	$form->add_hidden("save_form", "pls");
	$form->add_label("L4s", "", "", "Select the Product Line Subclasses to be included in the Benchmark");
	$res = mysql_query($sql_product_line_subclasses) or dberror($sql_product_line_subclasses);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["productline_subclass_id"], $product_line_subclasses))
		{
			$form->add_checkbox("PLS_" . $row["productline_subclass_id"], $row["productline_subclass_name"], true);
		}
		else
		{
			$form->add_checkbox("PLS_" . $row["productline_subclass_id"], $row["productline_subclass_name"], false);
		}
		
	}
}
elseif(param("s") == "pkinds") // Project Kinds
{
	$page_title = "Project Kind Selector";
	$form->add_hidden("save_form", "pkinds");
	$form->add_label("L4", "", "", "Select the Project Kind to be included in the Benchmark");
	$res = mysql_query($sql_project_kinds) or dberror($sql_project_kinds);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["projectkind_id"], $project_kinds))
		{
			$form->add_checkbox("PK_" . $row["projectkind_id"], $row["projectkind_name"], true);
		}
		else
		{
			$form->add_checkbox("PK_" . $row["projectkind_id"], $row["projectkind_name"], false);
		}
		
	}
}
elseif(param("s") == "pt") // Project Types
{
	$page_title = "Project Type Selector";
	$form->add_hidden("save_form", "pt");
	$form->add_label("L4", "", "", "Select the Project Types to be included in the Benchmark");
	$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["postype_name"], $pos_types))
		{
			$form->add_checkbox("PT_" . $row["postype_name"], $row["postype_name"], true);
		}
		else
		{
			$form->add_checkbox("PT_" . $row["postype_name"], $row["postype_name"], false);
		}
	}
}
elseif(param("s") == "sc") // POS Subclasses
{
	$page_title = "POS Type Subclass Selector";
	$form->add_hidden("save_form", "sc");
	$form->add_label("L4", "", "", "Select the POS Subclasses to be included in the Benchmark");
	$res = mysql_query($sql_possubclasses) or dberror($sql_possubclasses);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["possubclass_id"], $subclasses))
		{
			$form->add_checkbox("SC_" . $row["possubclass_id"], $row["possubclass_name"], true);
		}
		else
		{
			$form->add_checkbox("SC_" . $row["possubclass_id"], $row["possubclass_name"], false);
		}
		
	}
}

$form->add_input_submit("submit", "Save Values", 0);

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Process buttons
*********************************************************************/ 

/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page_Modal("cer_benchmarks");

$page->header();
$page->title($page_title);

$form->render();

if(param("save_form"))
{
?>

<script languege="javascript">
var back_link = "cer_benchmark.php?id=<?php echo param("id");?>"; 
$.nyroModalRemove();
</script>

<?php
}
$page->footer();
