<?php
/********************************************************************

    cer_benchmark_02_xls.php

    Generate Excel File: CER Benchmark Human Resources

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-10-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-10-15
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_access_to_cer_benchmarks");

require_once "../include/xls/Writer.php";
require_once "include/get_functions.php";

/********************************************************************
    prepare Data Needed
*********************************************************************/
$staff_types = array();
$sql = "select * from cer_staff_types";
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$staff_types[$row["cer_staff_type_id"]] = $row["cer_staff_type_name"];
}
$num_of_staff_types = count($staff_types);


include("cer_benchmark_filter.php");

/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$filename = "benchmark_" . $benchmark_shortcut . "_human_resources_" . date("Ymd") . ".xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename); 
$xls->setVersion(8);
$sheet = $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

//formats
$f_header =& $xls->addFormat();
$f_header->setSize(12);
$f_header->setAlign('left');
$f_header->setBold();

$f_title =& $xls->addFormat();
$f_title->setSize(10);
$f_title->setAlign('left');
$f_title->setBold();

$f_group_title =& $xls->addFormat();
$f_group_title->setSize(10);
$f_group_title->setAlign('left');
$f_group_title->setBold();

$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);

$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);


$f_unused =& $xls->addFormat();
$f_unused->setSize(8);
$f_unused->setBorder(1);
$f_unused->setPattern(2);
$f_unused->setBgColor('silver');

$f_border_left=& $xls->addFormat();
$f_border_left->setTop(1);
$f_border_left->setBottom(1);
$f_border_left->setLeft(1);
$f_border_left->setSize(10);
$f_border_left->setAlign('left');
$f_border_left->setBold();

$f_border_right=& $xls->addFormat();
$f_border_right->setTop(1);
$f_border_right->setBottom(1);
$f_border_right->setRight(1);


$f_border_middle=& $xls->addFormat();
$f_border_middle->setTop(1);
$f_border_middle->setBottom(1);

//captions
$c01 = 0;
$captions = array();
$captions[] = "Project Number";
$sheet->setColumn($c01, $c01, 10);
$c01++;

$captions[] = "Project State";
$sheet->setColumn($c01, $c01, 6);
$c01++;

$captions[] = "Country";
$sheet->setColumn($c01, $c01, 14);
$c01++;

$captions[] = "POS Location Address";
$sheet->setColumn($c01, $c01, 60);
$c01++;

$captions[] = "Product Line";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Product Line Subclass";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Opening Date";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Legal Type";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Project Kind";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Project Type";
$sheet->setColumn($c01, $c01, 12);
$c01++;

$captions[] = "Total FTE";
$sheet->setColumn($c01, $c01, 8);
$c01++;

foreach($staff_types as $key=>$name)
{
	$captions[] = $name;
	$sheet->setColumn($c01, $c01, 6);
	$c01++;
}	



$captions[] = "Total salaries for all functions";
$sheet->setColumn($c01, $c01, 6);
$c01++;

$captions[] = "Average salaries per FTE";
$sheet->setColumn($c01, $c01, 6);
$c01++;

$captions[] = "Total annual Bonus for all functions";
$sheet->setColumn($c01, $c01, 6);
$c01++;

$captions[] = "Average annual Bonus per FTE";
$sheet->setColumn($c01, $c01, 6);
$c01++;

$captions[] = "Total social charges for all function";
$sheet->setColumn($c01, $c01, 6);
$c01++;

$captions[] = "Average social charges per FTE";
$sheet->setColumn($c01, $c01, 6);



/********************************************************************
    write all captions
*********************************************************************/
$sheet->write(0, 0, $benchmark_title, $f_header);
$sheet->write(1, 0, "Personal Details in LOC (" . date("d.m.Y, H:m:s") . ")", $f_title);


if($base_currency == "chf")
{
	$sheet->write(3,8,"Salaries per function in CHF", $f_border_left);
}
else
{
	$sheet->write(3,8,"Salaries per function in LOC", $f_border_left);
}
for($i=1;$i<$num_of_staff_types;$i++)
{
	$sheet->write(3,8+$i,"", $f_border_middle);
}
$sheet->write(3,8+$num_of_staff_types,"", $f_border_right);

$sheet->setRow(4, 150);
$sheet->writeRow(4, 0, $captions, $f_caption);

/********************************************************************
    write all data
*********************************************************************/
$list_totals = array();
$num_of_projects = 0;

$row_index = 5;
$c01 = 0;


foreach($posorders as $key=>$posorder)
{
	if($posorder["pipeline"] == 1)
	{
		$sql = "select * " . 
			   "from posaddressespipeline " . 
			   "left join posorderspipeline on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " .
			   "left join orders on order_id = project_order " .
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "where posorder_type = 1 and posorder_id = " . $posorder["id"];

	}
	elseif($posorder["pipeline"] == 2)
	{
		$sql = "select * " . 
			   "from posaddresses " . 
			   "left join posorderspipeline on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " . 
			   "left join orders on order_id = project_order " .
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "where posorder_type = 1 and posorder_id = " . $posorder["id"];

	}
	else
	{
		$sql = "select * " . 
			   "from posaddresses " . 
			   "left join posorders on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " .
			   "left join orders on order_id = project_order " .
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "where posorder_type = 1 and posorder_id = " . $posorder["id"];
	}

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$num_of_projects++;
		$exchange_rate = 1;
		$factor = 1;


		$cer_basicdata = get_cer_basicdata($row["project_id"]);

		if(count($cer_basicdata) > 0)
		{
			$exchange_rate = $cer_basicdata["cer_basicdata_exchangerate"];
			$factor = $cer_basicdata["cer_basicdata_factor"];
			if(!$factor) {$factor = 1;}
		}

		if($base_currency == "chf")
		{
		}
		else
		{
			$exchange_rate = 1;
			$factor = 1;
		}

		$sheet->write($row_index, $c01, $row["posorder_ordernumber"], $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["order_actual_order_state_code"], $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["country_name"], $f_normal);
		$c01++;
		$tmp = $row["posaddress_place"] . ", " . $row["posaddress_name"] . ", " . $row["posaddress_address"];
		$sheet->write($row_index, $c01, $tmp, $f_normal);
		$c01++;
		
		$sheet->write($row_index, $c01, $row["product_line_name"], $f_normal);
		$c01++;
		
		$sheet->write($row_index, $c01, $row["productline_subclass_name"], $f_normal);
		$c01++;
		

		$sheet->write($row_index, $c01, to_system_date($row["posaddress_store_openingdate"]), $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["project_costtype_text"], $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["projectkind_name"], $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["postype_name"], $f_normal);
		$c01++;

		$salaries = array();
		$bonus = 0;
		$socialcharges = 0;
		$ftes = 0;

		foreach($staff_types as $key=>$name)
		{
			$sql_s = "select sum(cer_salary_fixed_salary) as total, " .
				     "sum(cer_salary_other) as other, " .
					 "sum(cer_salary_bonus) as bonus, " .
					 "sum(cer_salary_social_charges) as charges, " .
				     "sum(cer_salary_headcount_percent) as fte " . 
					 "from cer_salaries " .
					 "where cer_salary_cer_version = 0 and cer_salary_project = '" . $row["project_id"] . "' " . 
				     "and cer_salary_staff_type = " . $key;
			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				//$salaries[$key] = $row_s["total"] + $row_s["other"];
				//$bonus = $bonus + $row_s["bonus"];
				//$socialcharges = $socialcharges + $row_s["charges"];
				
				
				$salaries[$key] = ($row_s["total"] + $row_s["other"])  * $exchange_rate / $factor;
				$bonus = $bonus + ($row_s["bonus"]  * $exchange_rate / $factor);
				$socialcharges = $socialcharges + ($row_s["charges"]  * $exchange_rate / $factor);
				
				$ftes = $ftes + $row_s["fte"];
			}
			else
			{
				$salaries[$key] = "0";
				$ftes = "0";
			}
		}

		$ftes = round($ftes/100,2);
		$sheet->write($row_index, $c01, $ftes, $f_number);
		$c01++;

		if(array_key_exists("ftes", $list_totals))
		{
			$list_totals["ftes"] = $list_totals["ftes"] + $ftes;
		}
		else
		{
			$list_totals["ftes"] = $ftes;
		}

		$total_salaries = 0;
		foreach($salaries as $key=>$value)
		{
			$total_salaries = $total_salaries + $value;
			$sheet->write($row_index, $c01, $value, $f_number);
			$c01++;

			if(array_key_exists($key, $list_totals))
			{
				$list_totals[$key] = $list_totals[$key] + $value;
			}
			else
			{
				$list_totals[$key] = $value;
			}
		}


		if(array_key_exists("total_salaries", $list_totals))
		{
			$list_totals["total_salaries"] = $list_totals["total_salaries"] + $total_salaries;
		}
		else
		{
			$list_totals["total_salaries"] = $total_salaries;
		}

		if($total_salaries)
		{
			$sheet->write($row_index, $c01, $total_salaries, $f_number);
		}
		else
		{
			$sheet->write($row_index, $c01,"", $f_number);
		}
		$c01++;
		
		$avg_total_salaries = 0;
		if($ftes != 0)
		{
			$avg_total_salaries = round($total_salaries/$ftes,0);
		}

		if(array_key_exists("avg_total_salaries", $list_totals))
		{
			$list_totals["avg_total_salaries"] = $list_totals["avg_total_salaries"] + $avg_total_salaries;
		}
		else
		{
			$list_totals["avg_total_salaries"] = $avg_total_salaries;
		}
		
		$sheet->write($row_index, $c01, $avg_total_salaries, $f_number);
		$c01++;

		if(array_key_exists("bonus", $list_totals))
		{
			$list_totals["bonus"] = $list_totals["bonus"] + $bonus;
		}
		else
		{
			$list_totals["bonus"] = $bonus;
		}
		
		if($bonus)
		{
			$sheet->write($row_index, $c01, $bonus, $f_number);
		}
		else
		{
			$sheet->write($row_index, $c01,"", $f_number);
		}
		$c01++;

		$avg_bonus = 0;
		if($bonus != 0)
		{
			$avg_bonus = round($bonus/$ftes,0);
		}

		if(array_key_exists("avg_bonus", $list_totals))
		{
			$list_totals["avg_bonus"] = $list_totals["avg_bonus"] + $avg_bonus;
		}
		else
		{
			$list_totals["avg_bonus"] = $avg_bonus;
		}

		$sheet->write($row_index, $c01, $avg_bonus, $f_number);
		$c01++;

		if(array_key_exists("socialcharges", $list_totals))
		{
			$list_totals["socialcharges"] = $list_totals["socialcharges"] + $socialcharges;
		}
		else
		{
			$list_totals["socialcharges"] = $socialcharges;
		}
		
		if($socialcharges)
		{
			$sheet->write($row_index, $c01, $socialcharges, $f_number);
		}
		else
		{
			$sheet->write($row_index, $c01,"", $f_number);
		}
		$c01++;

		$avg_socialcharges = 0;
		if($socialcharges != 0)
		{
			$avg_socialcharges = round($socialcharges/$ftes,0);
		}


		if(array_key_exists("avg_socialcharges", $list_totals))
		{
			$list_totals["avg_socialcharges"] = $list_totals["avg_socialcharges"] + $avg_socialcharges;
		}
		else
		{
			$list_totals["avg_socialcharges"] = $avg_socialcharges;
		}

		
		$sheet->write($row_index, $c01, $avg_socialcharges, $f_number);
		$c01++;

	
		
		$c01 = 0;
		$row_index++;

	}
}

if(count($posorders) > 0)
{
	$c01 = 7;
	$row_index++;
	$sheet->write($row_index, $c01,"Sum", $f_normal);
	$c01++;
	$sheet->write($row_index, $c01,$list_totals["ftes"], $f_number);
	$c01++;
	foreach($staff_types as $key=>$name)
	{
		$sheet->write($row_index, $c01,$list_totals[$key], $f_number);
		$c01++;
	}
	$sheet->write($row_index, $c01,$list_totals["total_salaries"], $f_number);
	$c01++;
	$sheet->write($row_index, $c01,$list_totals["avg_total_salaries"], $f_number);
	$c01++;
	$sheet->write($row_index, $c01,$list_totals["bonus"], $f_number);
	$c01++;
	$sheet->write($row_index, $c01,$list_totals["avg_bonus"], $f_number);
	$c01++;
	$sheet->write($row_index, $c01,$list_totals["socialcharges"], $f_number);
	$c01++;
	$sheet->write($row_index, $c01,$list_totals["avg_socialcharges"], $f_number);
	$c01++;

	$c01 = 7;
	$row_index++;
	$sheet->write($row_index, $c01,"Average", $f_normal);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["ftes"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;
	foreach($staff_types as $key=>$name)
	{
		
		$avg = 0;
		if($num_of_projects > 0)
		{
			$avg = round($list_totals[$key]/$num_of_projects, 0);
		}
		
		$sheet->write($row_index, $c01,$avg, $f_number);
		$c01++;
	}


	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["total_salaries"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;


	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["avg_total_salaries"]/$num_of_projects, 0);
	}

	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["bonus"]/$num_of_projects, 0);
	}

	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["avg_bonus"]/$num_of_projects, 0);
	}


	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["socialcharges"]/$num_of_projects, 0);
	}

	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["avg_socialcharges"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$c01 = 7;
	$row_index++;
	$row_index++;
	$sheet->write($row_index, $c01,"Average per FTE", $f_normal);
	$c01++;
	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;
	foreach($staff_types as $key=>$name)
	{
		$sheet->write($row_index, $c01,"", $f_unused);
		$c01++;
	}

	$avg = 0;
	if($list_totals["ftes"] > 0)
	{
		$avg = round($list_totals["total_salaries"]/$list_totals["ftes"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;
	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$avg = 0;
	if($list_totals["ftes"] > 0)
	{
		$avg = round($list_totals["bonus"]/$list_totals["ftes"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;
	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$avg = 0;
	if($list_totals["ftes"] > 0)
	{
		$avg = round($list_totals["socialcharges"]/$list_totals["ftes"], 0);
	}

	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;
	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;
}
$xls->close(); 

?>