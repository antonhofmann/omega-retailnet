<?php
/********************************************************************

    sg_brands.php

    Lists Swatch Group Brands

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2016-10-20
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2016-10-20
    Version:        1.0.0

    Copyright (c) 2016 Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("sg_brand.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
//get all data from interest rates


$sql = "select sg_brand_id, sg_brand_name, if(sg_brand_active = 1, 'x', '') as inuse  from sg_brands";


/********************************************************************
    build list 
*********************************************************************/

$list = new ListView($sql);

$list->set_entity("sg_brands");
$list->set_order("sg_brand_name");

$list->add_column("sg_brand_name", "Brand Name", "sg_brand.php");
$list->add_column("inuse", "in use");

$list->add_button(LIST_BUTTON_NEW, "New", "sg_brand.php");

$list->populate();
$list->process();

$page = new Page("sg_brands");

$page->header();
$page->title("Swatch Group Brands");
$list->render();
$page->footer();
?>
