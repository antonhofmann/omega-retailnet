<?php
/********************************************************************

    cer_application_salary.php

    Add/edit New salary position

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";

check_access("has_access_to_cer");

if(!has_access("has_full_access_to_cer") 
	and !has_access("has_access_to_his_cer") 
	and !has_access("can_only_edit_staff_in_cer")
	and !has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}

if(!has_access("has_access_to_human_resources")) {
	redirect("noaccess.php");
}

/********************************************************************
    prepare all data needed
*********************************************************************/
$posdata = get_pos_data($project["project_order"]);
$currency = get_cer_currency(param("pid"));

// create list values for the years
$years = array();
$first_year = $cer_basicdata["cer_basicdata_firstyear"];
$first_month = $cer_basicdata["cer_basicdata_firstmonth"];
$last_year = $cer_basicdata["cer_basicdata_lastyear"];

for($i = $first_year;$i <= $last_year;$i++)
{
	$years[$i] = $i;
}

// create list values for the years
$months = array();
//for($i=$first_month;$i<13;$i++)
//{
//	$months[$i] = $i;
//}

for($i=1;$i<13;$i++)
{
	$months[$i] = $i;
}


for($i=1;$i<13;$i++)
{
	$numbers[$i] = $i .  ' times';
}



/********************************************************************
    build form
*********************************************************************/

$form = new Form("cer_salaries", "CER Salaries");
include("include/project_head.php");

$form->add_section("Salaries in " . $currency["symbol"]);
$form->add_hidden("pid", param("pid"));
$form->add_hidden("cer_salary_project", param("pid"));

$form->add_list("cer_salary_staff_type", "Function*", "select cer_staff_type_id, cer_staff_type_name from cer_staff_types", NOTNULL);
$form->add_list("cer_salary_year_starting", "Starting Year*", $years, NOTNULL);
$form->add_list("cer_salary_month_starting", "Starting Month*", $months, NOTNULL);
$form->add_edit("cer_salary_headcount_percent", "Headcounts Fulltime Equivalent in %*", NOTNULL, "", TYPE_DECIMAL, 7, 2);

$form->add_comment("Please enter all figures based on a 100% occupation and a full year.");
$form->add_edit("cer_salary_fixed_salary", "Fixed Salary in " . $currency["symbol"] ."*", NOTNULL, "", TYPE_INT, 12);
$form->add_edit("cer_salary_bonus", "Bonus in " . $currency["symbol"], "", "", TYPE_INT, 12);
$form->add_edit("cer_salary_other", "Other Salary in " . $currency["symbol"], "", "", TYPE_INT, 12);

$form->add_edit("cer_salary_social_charges_percent", "Social Charges in %", "", "", TYPE_DECIMAL, 5, 2);
//$form->add_edit("cer_salary_social_charges", "Social Charges", "", "", TYPE_DECIMAL, 12, 2);
$form->add_label("cer_salary_social_charges", "Social Charges in " . $currency["symbol"]);

$form->add_label("cer_salary_total", "Total 100% per year in " . $currency["symbol"]);

$tmp = "Commission";
if($cer_basicdata["cer_basicdata_commission_from_net_sales"] == 1)
{
	$tmp .= " in Percent of NET SALES";
}
else
{
	$tmp .= " in Percent of GROSS SALES";
}

$form->add_section($tmp);
$form->add_edit("cer_salary_commission_percent", "Commission in % per year", "", "", TYPE_DECIMAL, 5, 2);
$form->add_edit("cer_salary_commission_social_charges_percent", "Social Charges in % on Commission per year", "", "", TYPE_DECIMAL, 5, 2);


$form->add_section("Bulk Operation");
$form->add_list("number", "Duplicate the above human resource", $numbers);

$form->add_button("save_form", "Save");
$form->add_button("back", "Back");
$form->add_button("delete", "Delete", "", OPTIONAL);

$form->populate();
$form->process();

if($form->button("save_form"))
{
	if($form->validate())
	{
		

		$total = $form->value("cer_salary_fixed_salary") + $form->value("cer_salary_bonus") + $form->value("cer_salary_other");

		$social_charges = $form->value("cer_salary_social_charges_percent") * $total/100;
		$form->value("cer_salary_social_charges", $social_charges);

		$total = $form->value("cer_salary_fixed_salary") + $form->value("cer_salary_bonus") + $form->value("cer_salary_other") + $social_charges;
		$form->value("cer_salary_total", $total);

		$form->save();


		if($form->value('number') > 0) {
		 
			for($i=1;$i<=$form->value('number');$i++) {
				param('id', 0);
				$form->save();
			}
		
		}

		calculate_forcasted_salaries(param("pid"), $years, $project["order_shop_address_country"]);
		$form->message("The data has been saved.");

		$link = "cer_application_salaries.php?pid=" . param("pid");
		redirect($link);
	}
}
elseif($form->button("back"))
{
	$link = "cer_application_salaries.php?pid=" . param("pid");
	redirect($link);
}
elseif($form->button("delete"))
{
	$sql = "delete from cer_salaries where cer_salary_id = " . id();
	$result = mysql_query($sql) or dberror($sql);

	calculate_forcasted_salaries(param("pid"), $years, $project["order_shop_address_country"]);


	$link = "cer_application_salaries.php?pid=" . param("pid");
	redirect($link);
}

$page = new Page("cer_projects");
require "include/project_page_actions.php";
$page->header();

if($form_type == "INR03")
{
	$page->title(id() ? "INR-03 - Retail Furniture in Third-party Store: Edit Human Resource" : "INR-03 - Retail Furniture in Third-party Store: Add Human Resource");
}
elseif($form_type == "AF")
{
	$page->title(id() ? "Application Form: Edit Human Resource" : "Application Form: Add Human Resource");
}
else
{
	$page->title(id() ? "Capital Expenditure Request: Edit Human Resource" : "Capital Expenditure Request: Add Human Resource");
}
require_once("include/tabs.php");
$form->render();

require "include/footer_scripts.php";
$page->footer();

?>