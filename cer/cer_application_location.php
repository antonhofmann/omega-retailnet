<?php
/********************************************************************

    cer_application_location.php

    Application Form: location information
    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";

check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}

/********************************************************************
    prepare all data needed
*********************************************************************/
$posdata = get_pos_data($project["project_order"]);

$ratings = array();
$ratings[1] = "1";
$ratings[2] = "2";
$ratings[3] = "3";
$ratings[4] = "4";
$ratings[5] = "5";

/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

$form->add_hidden("pid", param("pid"));

include("include/project_head.php");

$form->add_label("cer_basicdata_concept_reason", "Reason for choosing this Concept");

$form->add_section(" ");
$form->add_label("gross_surface", "Gross Space rented in sqms", 0, $posdata["posaddress_store_grosssurface"]);
$form->add_label("total_surface", "Total Space rented in sqms", 0, $posdata["posaddress_store_totalsurface"]);
$form->add_label("retail_surface", "Sales Surface in sqms", 0, $posdata["posaddress_store_retailarea"]);
$form->add_label("areas", "Neighbourhood", 0, $posdata["posareas"]);


$form->add_section("Area Perception");
$form->add_comment("Please rate: 5 ist the best value.");
$form->add_list("posaddress_perc_class", "Class/Image Area", $ratings, NOTNULL, $posdata["posaddress_perc_class"]);
$form->add_list("posaddress_perc_tourist", "Tourist/Historical Area", $ratings, NOTNULL, $posdata["posaddress_perc_tourist"]);
$form->add_list("posaddress_perc_transport", "Public Transportation", $ratings, NOTNULL, $posdata["posaddress_perc_transport"]);
$form->add_list("posaddress_perc_people", "People Traffic Area", $ratings, NOTNULL, $posdata["posaddress_perc_people"]);
$form->add_list("posaddress_perc_parking", "Parking Possibilities", $ratings, NOTNULL, $posdata["posaddress_perc_parking"]);
$form->add_list("posaddress_perc_visibility1", "Visibility from Pavement", $ratings, NOTNULL, $posdata["posaddress_perc_visibility1"]);
$form->add_list("posaddress_perc_visibility2", "Visibility from accross the Street", $ratings, NOTNULL, $posdata["posaddress_perc_visibility2"]);

//$form->add_rating("class", "Class/Image Area", $posdata["posaddress_perc_class"]);
//$form->add_rating("tourist", "Tourist/Historical Area", $posdata["posaddress_perc_tourist"]);
//$form->add_rating("transport", "Public Transportation", $posdata["posaddress_perc_transport"]);
//$form->add_rating("people", "People Traffic Area", $posdata["posaddress_perc_people"]);
//$form->add_rating("parking", "Parking Possibilities Area", $posdata["posaddress_perc_parking"]);
//$form->add_rating("visibility1", "Visibility from Pavement", $posdata["posaddress_perc_visibility1"]);
//$form->add_rating("visibility2", "Visibility from accross the Street", $posdata["posaddress_perc_visibility2"]);

$form->add_section("Neighbourhood");
$form->add_label("neighbour_left", "Shop on Left Side", 0, $posdata["posorder_neighbour_left"]);
$form->add_label("neighbour_right", "Shop on Right Side", 0, $posdata["posorder_neighbour_right"]);
$form->add_label("neighbour_acrleft", "Shop Across Left Side", 0, $posdata["posorder_neighbour_acrleft"]);
$form->add_label("neighbour_acrright", "Shop Across Right Side", 0, $posdata["posorder_neighbour_acrright"]);
$form->add_label("neighbour_brands", "Other Brands in Area", 0, $posdata["posorder_neighbour_brands"]);
$form->add_label("neighbour_comment", "Comment", 0, $posdata["posorder_neighbour_comment"]);

$form->add_button("form_save", "Save Data");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


if($form->button("form_save"))
{
	if($form->validate())
	{
		$fields = array();
    
		$value = dbquote($form->value("posaddress_perc_class"));
		$fields[] = "posaddress_perc_class = " . $value;

		$value = dbquote($form->value("posaddress_perc_tourist"));
		$fields[] = "posaddress_perc_tourist = " . $value;

		$value = dbquote($form->value("posaddress_perc_transport"));
		$fields[] = "posaddress_perc_transport = " . $value;

		$value = dbquote($form->value("posaddress_perc_people"));
		$fields[] = "posaddress_perc_people = " . $value;

		$value = dbquote($form->value("posaddress_perc_parking"));
		$fields[] = "posaddress_perc_parking = " . $value;

		$value = dbquote($form->value("posaddress_perc_visibility1"));
		$fields[] = "posaddress_perc_visibility1 = " . $value;

		$value = dbquote($form->value("posaddress_perc_visibility2"));
		$fields[] = "posaddress_perc_visibility2 = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update " . $posdata["table"] . " set " . join(", ", $fields) . " where posaddress_id = " . $posdata["posaddress_id"];
		mysql_query($sql) or dberror($sql);
			
		$form->message("Your data has bee saved.");
	}
}
  
/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");

require "include/project_page_actions.php";

$page->header();

if($form_type == "INR03")
{
	$page->title("INR-03 - Retail Furniture in Third-party Store: Location Information");
}
elseif($form_type == "AF")
{
	$page->title("Application Form: Location Information");
}
else
{
	$page->title("Capital Expenditure Request: Location Information");
}
require_once("include/tabs.php");
$form->render();

require "include/footer_scripts.php";
$page->footer();

?>