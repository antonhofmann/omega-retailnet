<?php
/********************************************************************

    cer_benchmark_06_xls.php

    Generate Excel File: CER BenchmarkRental Revenues

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-10-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-10-15
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_access_to_cer_benchmarks");

require_once "../include/xls/Writer.php";
require_once "include/get_functions.php";

/********************************************************************
    prepare Data Needed
*********************************************************************/

include("cer_benchmark_filter.php");

/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$filename = "benchmark_" . $benchmark_shortcut . "_revenue_details_" . date("Ymd") . ".xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename); 
$xls->setVersion(8);
$sheet = $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

//formats
$f_header =& $xls->addFormat();
$f_header->setSize(12);
$f_header->setAlign('left');
$f_header->setBold();

$f_title =& $xls->addFormat();
$f_title->setSize(10);
$f_title->setAlign('left');
$f_title->setBold();

$f_group_title =& $xls->addFormat();
$f_group_title->setSize(10);
$f_group_title->setAlign('left');
$f_group_title->setBold();

$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);

$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);


$f_unused =& $xls->addFormat();
$f_unused->setSize(8);
$f_unused->setBorder(1);
$f_unused->setPattern(2);
$f_unused->setBgColor('silver');

$f_border_left=& $xls->addFormat();
$f_border_left->setTop(1);
$f_border_left->setBottom(1);
$f_border_left->setLeft(1);
$f_border_left->setSize(10);
$f_border_left->setAlign('left');
$f_border_left->setBold();

$f_border_right=& $xls->addFormat();
$f_border_right->setTop(1);
$f_border_right->setBottom(1);
$f_border_right->setRight(1);


$f_border_middle=& $xls->addFormat();
$f_border_middle->setTop(1);
$f_border_middle->setBottom(1);

//captions
$c01 = 0;
$captions = array();
$captions[] = "Project Number";
$sheet->setColumn($c01, $c01, 10);
$c01++;

$captions[] = "Project State";
$sheet->setColumn($c01, $c01, 6);
$c01++;

$captions[] = "Country";
$sheet->setColumn($c01, $c01, 14);
$c01++;


$captions[] = "POS Location Address";
$sheet->setColumn($c01, $c01, 60);
$c01++;

$captions[] = "Product Line";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Product Line Subclass";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Opening Date";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Legal Type";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Project Kind";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Project Type";
$sheet->setColumn($c01, $c01, 12);
$c01++;

$captions[] = "Total FTE";
$sheet->setColumn($c01, $c01, 6);
$c01++;

$captions[] = "Sales Surface";
$sheet->setColumn($c01, $c01, 6);
$c01++;

$captions[] = "Units Watches";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Units Jewellery";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Average price Watches";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Average Price Jewellery";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Accessories";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Customer Services";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Gross sales";
$sheet->setColumn($c01, $c01, 8);

$captions[] = "Sales Reduction";
$sheet->setColumn($c01, $c01, 8);

$captions[] = "Net sales";
$sheet->setColumn($c01, $c01, 8);


/********************************************************************
    write all captions
*********************************************************************/
$sheet->write(0, 0, $benchmark_title, $f_header);


if($base_currency == "chf")
{
	$sheet->write(1, 0, "Revenue Details in CHF (" . date("d.m.Y, H:m:s") . ")", $f_title);
}
else
{
	$sheet->write(1, 0, "Revenue Details in LOC (" . date("d.m.Y, H:m:s") . ")", $f_title);
}

$sheet->setRow(4, 130);
$sheet->writeRow(4, 0, $captions, $f_caption);

/********************************************************************
    write all data
*********************************************************************/
$list_totals = array();
$num_of_projects = 0;

$row_index = 5;
$c01 = 0;

foreach($posorders as $key=>$posorder)
{
	if($posorder["pipeline"] == 1)
	{
		$sql = "select * " . 
			   "from posaddressespipeline " . 
			   "left join posorderspipeline on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " . 
			   "left join orders on order_id = project_order " .
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "left join project_costs on project_cost_order = posorder_order " .
			   "where posorder_type = 1 and posorder_id = " . $posorder["id"];
	}
	elseif($posorder["pipeline"] == 2)
	{
		$sql = "select * " . 
			   "from posaddressespipeline " . 
			   "left join posorderspipeline on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " .
			   "left join orders on order_id = project_order " .
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "left join project_costs on project_cost_order = posorder_order " .
			   "where posorder_type = 1 and posorder_id = " . $posorder["id"];
	}
	else
	{
		$sql = "select * " . 
			   "from posaddresses " . 
			   "left join posorders on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " . 
			   "left join orders on order_id = project_order " .
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "left join project_costs on project_cost_order = posorder_order " .
			   "where posorder_type = 1 and posorder_id = " . $posorder["id"];
	}

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$num_of_projects++;
		$exchange_rate = 1;
		$factor = 1;


		$cer_basicdata = get_cer_basicdata($row["project_id"]);

		if(count($cer_basicdata) > 0)
		{
			$exchange_rate = $cer_basicdata["cer_basicdata_exchangerate"];
			$factor = $cer_basicdata["cer_basicdata_factor"];
			if(!$factor) {$factor = 1;}
		}

		if($base_currency == "chf")
		{
		}
		else
		{
			$exchange_rate = 1;
			$factor = 1;
		}

		$sheet->write($row_index, $c01, $row["posorder_ordernumber"], $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["order_actual_order_state_code"], $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["country_name"], $f_normal);
		$c01++;
		$tmp = $row["posaddress_place"] . ", " . $row["posaddress_name"] . ", " . $row["posaddress_address"];
		$sheet->write($row_index, $c01, $tmp, $f_normal);
		$c01++;


		$sheet->write($row_index, $c01, $row["product_line_name"], $f_normal);
		$c01++;
		
		$sheet->write($row_index, $c01, $row["productline_subclass_name"], $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, to_system_date($row["posaddress_store_openingdate"]), $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["project_costtype_text"], $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["projectkind_name"], $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["postype_name"], $f_normal);
		$c01++;
		

		// get total ftes
		$ftes = 0;
		$sql_s = "select sum(cer_salary_headcount_percent) as fte " . 
				 "from cer_salaries " .
				 "left join projects on project_id = cer_salary_project " . 
				 "left join orders on order_id = project_order " . 
				 "where cer_salary_cer_version = 0 and order_id = '" . $row["posorder_order"] . "'";
		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		while($row_s = mysql_fetch_assoc($res_s))
		{
			$ftes = $ftes + $row_s["fte"];
		}
		
		$ftes = round($ftes/100,2);
		$sheet->write($row_index, $c01, $ftes, $f_number);
		$c01++;
		
		if(array_key_exists("ftes", $list_totals))
		{
			$list_totals["ftes"] = $list_totals["ftes"] + $ftes;
		}
		else
		{
			$list_totals["ftes"] = $ftes;
		}
		
		//get space in square meters
		$gross_space = $row["project_cost_gross_sqms"];
		$total_space = $row["project_cost_totalsqms"];
		$retail_area = $row["project_cost_sqms"];
		$back_office = $row["project_cost_backofficesqms"];

		if(array_key_exists("gross_space", $list_totals))
		{
			$list_totals["gross_space"] = $list_totals["gross_space"] + $gross_space;
		}
		else
		{
			$list_totals["gross_space"] = $gross_space;
		}
		
		if(array_key_exists("total_space", $list_totals))
		{
			$list_totals["total_space"] = $list_totals["total_space"] + $total_space;
		}
		else
		{
			$list_totals["total_space"] = $total_space;
		}

		if(array_key_exists("retail_area", $list_totals))
		{
			$list_totals["retail_area"] = $list_totals["retail_area"] + $retail_area;
		}
		else
		{
			$list_totals["retail_area"] = $retail_area;
		}

		if(array_key_exists("back_office", $list_totals))
		{
			$list_totals["back_office"] = $list_totals["back_office"] + $back_office;
		}
		else
		{
			$list_totals["back_office"] = $back_office;
		}

		$sheet->write($row_index, $c01, number_format($retail_area, 2, ".", ""), $f_number);
		$c01++;
		

		//get revenues
		$watches = 0;
		$jewellery = 0;
		$watches_revenue = 0;
		$jewellery_revenue = 0;
		$services_revenue = 0;
		$accessories_revenue = 0;

		$sql_s = "select sum(cer_revenue_quantity_watches) as watches, " .
			     "sum(cer_revenue_quantity_jewellery) as jewellery,  " .
			     "sum(cer_revenue_watches) as watches_revenue,  " .
			     "sum(cer_revenue_jewellery) as jewellery_revenue,  " .
			     "sum(cer_revenue_accessories) as accessories_revenue,  " .
			     "sum(cer_revenue_customer_service) as services_revenue  " .
				 "from cer_revenues " .
				 "left join projects on project_id = cer_revenue_project " . 
				 "left join orders on order_id = project_order " . 
				 "where cer_revenue_cer_version = 0 and order_id = '" . $row["posorder_order"] . "'";
		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		while($row_s = mysql_fetch_assoc($res_s))
		{
			$watches = $watches + $row_s["watches"];
			$jewellery = $jewellery + $row_s["jewellery"];

			$watches_revenue = $watches_revenue + $row_s["watches_revenue"];
			$jewellery_revenue = $jewellery_revenue + $row_s["jewellery_revenue"];
			$accessories_revenue = $accessories_revenue + $row_s["accessories_revenue"];
			$services_revenue = $services_revenue + $row_s["services_revenue"];
		}

		$watches_revenue = $watches_revenue * $exchange_rate / $factor;
		$jewellery_revenue = $jewellery_revenue * $exchange_rate / $factor;
		$accessories_revenue = $accessories_revenue * $exchange_rate / $factor;
		$services_revenue = $services_revenue * $exchange_rate / $factor;

 
		
		$sheet->write($row_index, $c01, $watches, $f_number);
		$c01++;
		$sheet->write($row_index, $c01, $jewellery, $f_number);
		$c01++;


		if(array_key_exists("watches", $list_totals))
		{
			$list_totals["watches"] = $list_totals["watches"] + $watches;
		}
		else
		{
			$list_totals["watches"] = $watches;
		}

		if(array_key_exists("jewellery", $list_totals))
		{
			$list_totals["jewellery"] = $list_totals["jewellery"] + $jewellery;
		}
		else
		{
			$list_totals["jewellery"] = $jewellery;
		}


		if(array_key_exists("watches_revenue", $list_totals))
		{
			$list_totals["watches_revenue"] = $list_totals["watches_revenue"] + $watches_revenue;
		}
		else
		{
			$list_totals["watches_revenue"] = $watches_revenue;
		}

		if(array_key_exists("jewellery_revenue", $list_totals))
		{
			$list_totals["jewellery_revenue"] = $list_totals["jewellery_revenue"] + $jewellery_revenue;
		}
		else
		{
			$list_totals["jewellery_revenue"] = $jewellery_revenue;
		}

		
		if(array_key_exists("services_revenue", $list_totals))
		{
			$list_totals["services_revenue"] = $list_totals["services_revenue"] + $services_revenue;
		}
		else
		{
			$list_totals["services_revenue"] = $services_revenue;
		}

		if(array_key_exists("accessories_revenue", $list_totals))
		{
			$list_totals["accessories_revenue"] = $list_totals["accessories_revenue"] + $accessories_revenue;
		}
		else
		{
			$list_totals["accessories_revenue"] = $accessories_revenue;
		}

		

		//calculate average prices
		$watches_price = 0;
		$jewellery_price = 0;

		if($watches > 0)
		{
			$watches_price = round($watches_revenue/$watches, 2);
		}

		if($jewellery > 0)
		{
			$jewellery_price = round($jewellery_revenue/$jewellery, 2);
		}

		
		$sheet->write($row_index, $c01, number_format($watches_price, 2), $f_number);
		$c01++;
		$sheet->write($row_index, $c01, number_format($jewellery_price, 2), $f_number);
		$c01++;

		if(array_key_exists("watches_price", $list_totals))
		{
			$list_totals["watches_price"] = $list_totals["watches_price"] + $watches_price;
		}
		else
		{
			$list_totals["watches_price"] = $watches_price;
		}

		if(array_key_exists("jewellery_price", $list_totals))
		{
			$list_totals["jewellery_price"] = $list_totals["jewellery_price"] + $jewellery_price;
		}
		else
		{
			$list_totals["jewellery_price"] = $jewellery_price;
		}

		$sheet->write($row_index, $c01, number_format($accessories_revenue, 0, ".", ""), $f_number);
		$c01++;

		$sheet->write($row_index, $c01, number_format($services_revenue, 0, ".", ""), $f_number);
		$c01++;

		$gross_sales = $watches_revenue + $jewellery_revenue  + $accessories_revenue + $services_revenue;

		$sheet->write($row_index, $c01, number_format($gross_sales, 0, ".", ""), $f_number);
		$c01++;

		if(array_key_exists("gross_sales", $list_totals))
		{
			$list_totals["gross_sales"] = $list_totals["gross_sales"] + $gross_sales;
		}
		else
		{
			$list_totals["gross_sales"] = $gross_sales;
		}


		//get Sales Reduction
		$sales_reduction = 0;
		$sql_s = "select * " . 
			     "from cer_revenues " . 
			     "where cer_revenue_cer_version = 0 " .
			     " and cer_revenue_project = " . dbquote($row["project_id"]) .
			     " order by cer_revenue_brand_id, cer_revenue_year";

		$res_s = mysql_query($sql_s) or dberror($sql_s);
		while($row_s = mysql_fetch_assoc($res_s))
		{
			$sales_reduction_watches = $row_s["cer_revenue_sales_reduction_watches"] / 100;
			$sales_reduction_bijoux = $row_s["cer_revenue_sales_reduction_bijoux"] / 100;
			$sales_reduction_accessories = $row_s["cer_revenue_sales_reduction_accessories"] / 100;
			$sales_reduction_services = $row_s["cer_revenue_sales_reduction_cservice"] / 100;
			//$sales_reduction_creditcard = $row_s["cer_revenue_reduction_credit_cards"] / 100;


			$srw = $row_s["cer_revenue_watches"] * $sales_reduction_watches;
			$srb = $row_s["cer_revenue_jewellery"] * $sales_reduction_bijoux;
			$sra = $row_s["cer_revenue_accessories"] * $sales_reduction_accessories;
			$src = $row_s["cer_revenue_customer_service"] * $sales_reduction_services;
			
			//$scard = ($row_s["cer_revenue_watches"] + $row_s["cer_revenue_jewellery"] + $row_s["cer_revenue_accessories"] + $row_s["cer_revenue_customer_service"]) * $sales_reduction_creditcard;
			//$sales_reduction = $sales_reduction + $srw + $srb + $sra + $src + $scard;

			$sales_reduction = $sales_reduction + $srw + $srb + $sra + $src;
		}

		
		$sheet->write($row_index, $c01, number_format($sales_reduction, 0, ".", ""), $f_number);
		$c01++;

		if(array_key_exists("sales_reduction", $list_totals))
		{
			$list_totals["sales_reduction"] = $list_totals["sales_reduction"] + $sales_reduction;
		}
		else
		{
			$list_totals["sales_reduction"] = $sales_reduction;
		}



		$net_sales = $gross_sales - $sales_reduction;

		$sheet->write($row_index, $c01, number_format($net_sales, 0, ".", ""), $f_number);
		$c01++;

		if(array_key_exists("net_sales", $list_totals))
		{
			$list_totals["net_sales"] = $list_totals["net_sales"] + $net_sales;
		}
		else
		{
			$list_totals["net_sales"] = $net_sales;
		}
		$c01 = 0;
		$row_index++;

	}
}

if(count($posorders) > 0)
{
	$c01 = 9;
	$row_index++;
	$sheet->write($row_index, $c01,"Sum", $f_normal);
	$c01++;

	$sheet->write($row_index, $c01,number_format($list_totals["ftes"], 2, ".", ""), $f_number);
	$c01++;

	$sheet->write($row_index, $c01,number_format($list_totals["retail_area"], 2, ".", ""), $f_number);
	$c01++;

	$sheet->write($row_index, $c01,$list_totals["watches"], $f_number);
	$c01++;

	$sheet->write($row_index, $c01,$list_totals["jewellery"], $f_number);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	
	$sheet->write($row_index, $c01,number_format($list_totals["accessories_revenue"], 0, ".", ""), $f_number);
	$c01++;


	$sheet->write($row_index, $c01,number_format($list_totals["services_revenue"], 0, ".", ""), $f_number);
	$c01++;

	$sheet->write($row_index, $c01,number_format($list_totals["gross_sales"], 0, ".", ""), $f_number);
	$c01++;

	$sheet->write($row_index, $c01,number_format($list_totals["sales_reduction"], 0, ".", ""), $f_number);
	$c01++;

	$sheet->write($row_index, $c01,number_format($list_totals["net_sales"], 0, ".", ""), $f_number);
	$c01++;


	$c01 = 9;
	$row_index++;
	$sheet->write($row_index, $c01,"Average", $f_normal);
	$c01++;


	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["ftes"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;


	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["retail_area"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["watches"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["jewellery"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	
	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["watches_price"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["jewellery_price"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;
	

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["accessories_revenue"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["services_revenue"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;


	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["gross_sales"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["sales_reduction"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["net_sales"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;


	$c01 = 9;
	$row_index++;
	$row_index++;
	$sheet->write($row_index, $c01,"Average per sqm", $f_normal);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["watches"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["jewellery"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	
	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	
	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["accessories_revenue"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["services_revenue"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["gross_sales"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["sales_reduction"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;


	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["net_sales"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$c01 = 9;
	$row_index++;
	$sheet->write($row_index, $c01,"Average per FTE", $f_normal);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$avg = 0;
	if($list_totals["ftes"] > 0)
	{
		$avg = round($list_totals["retail_area"]/$list_totals["ftes"], 2);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["ftes"] > 0)
	{
		$avg = round($list_totals["watches"]/$list_totals["ftes"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["ftes"] > 0)
	{
		$avg = round($list_totals["jewellery"]/$list_totals["ftes"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	
	$avg = 0;
	if($list_totals["ftes"] > 0)
	{
		$avg = round($list_totals["accessories_revenue"]/$list_totals["ftes"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["ftes"] > 0)
	{
		$avg = round($list_totals["services_revenue"]/$list_totals["ftes"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["ftes"] > 0)
	{
		$avg = round($list_totals["gross_sales"]/$list_totals["ftes"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["ftes"] > 0)
	{
		$avg = round($list_totals["sales_reduction"]/$list_totals["ftes"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;


	$avg = 0;
	if($list_totals["ftes"] > 0)
	{
		$avg = round($list_totals["net_sales"]/$list_totals["ftes"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

}

$xls->close(); 

?>