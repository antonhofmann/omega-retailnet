<?php
/********************************************************************

    lnr_01_pdf_detail.php

    Print Detail Form LNR01.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-06-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-06-14
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.
*********************************************************************/
include("include/in_financial_data.php");



$average_buildout_costs = round($ln_basicdata["ln_basicdata_average_buildout_costs"]/1000, 0);

//set pdf parameters
$margin_top = 10;
$margin_left = 12;
$y = $margin_top;
$x = $margin_left+1;
$standard_h = 4.5;
$x1 = $margin_left+1;
$x2 = $margin_left+71;

$years_page1 = array();
$j= 1;
foreach($years as $key=>$year)
{
	if($j < 6)
	{
		$years_page1[] = $years[$key];
	}
	$j++;
}

$pdf->AddPage();

// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "B", 10);
$pdf->Cell(51, 8, "Swatch Group", 1);

$pdf->SetFont("arialn", "B", 14);

$pdf->Cell(179, 8, "LEASE NEGOTIATION - BUSINESS PLAN", 1, "", "C");


$pdf->SetFont("arialn", "", 10);
$pdf->SetFillColor(248,251,167);
if($ln_version == 0 and $ln_basicdata["ln_basicdata_locked"] == 0)
{
	$pdf->Cell(20, 8, date("d.m.Y"), 1, "", "C", true);
}
else
{
	$pdf->Cell(20, 8, to_system_date($ln_basicdata["versiondate"]), 1, "", "C", true);
}
$pdf->Cell(20, 8, "LNR-02A", 1, "", "C");

	
	$y = $pdf->GetY()+10;
	$y_at_start = $y - 1;

	
	
	// 2. Investment
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(268, 6, "2. Investment (" . $project["order_shop_address_company"] . ', Project: ' . $project["project_number"] . ")", 1, "", "L");

	$y = $pdf->GetY()+6;
	$pdf->SetXY($margin_left+1,$y);

	$y_tmp = $y;

	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(70, $standard_h, "Investment in Fixed Assets - Cost", 1, "", "L");
	$pdf->Cell(25, $standard_h, "k" . $currency_symbol, 1, "", "R");
	$pdf->Cell(25, $standard_h, "kCHF", 1, "", "R");
	
	$y = $pdf->GetY()+$standard_h;
	$pdf->SetXY($margin_left+1,$y);

	//draw investment boxes
	$pdf->Cell(70, 6*$standard_h, "", 1);
	$pdf->SetXY($x2,$y);
	$pdf->Cell(25, 6*$standard_h, "", 1);
	
	//list investments
	$pdf->SetFont("arialn", "", 9);
		
	$y = $y - $standard_h;
	
	foreach($fixed_assets as $key=>$itype)
	{
		if(array_key_exists($itype, $amounts))
		{
			if(!$amounts[$itype]){
				$amounts[$itype] = "";
			}
			
			
			$y = $y + $standard_h;
			$pdf->SetXY($x1,$y);
			$pdf->SetFont("arialn", "", 9);

			$pdf->Cell($x, $standard_h, $investment_names[$itype] , 0, "", "L");

			$pdf->SetXY($x2,$y);
			$pdf->SetFont("arialn", "", 9);
			
			if($amounts[$itype] != "-" and $amounts[$itype] != 0)
			{
				$pdf->Cell(25, $standard_h, number_format(round($amounts[$itype]/ 1000,0), 0, '.', "'"), 0, "", "R");
			}
			else
			{
				$pdf->Cell(25, $standard_h, "-", 0, "", "R");
			}


			if($amounts[$itype] != "-" and $amounts[$itype] != 0)
			{	
				$tmp = $ln_basicdata["ln_basicdata_exchangerate"]*$amounts[$itype]/$ln_basicdata["ln_basicdata_factor"];
				$pdf->Cell(25, $standard_h, number_format(round($tmp/ 1000,0), 0, '.', "'"), 0, "", "R");
			}
			else
			{
				$pdf->Cell(25, $standard_h, "-", 0, "", "R");
			}

			
		}
		else
		{
			$y = $y + $standard_h;
		}
	}

	//dummy for dismantling costs later to come

	//$tmp = $cer_basicdata["cer_basicdata_exchangerate"]*$intagible_amount/$cer_basicdata["cer_basicdata_factor"];

	$_tmp = $deposit + $other_noncapitalized_cost + $intagible_amount;
	$_tmp2 = $cer_basicdata["cer_basicdata_exchangerate"]*$_tmp/$cer_basicdata["cer_basicdata_factor"];

	$investment_total = $investment_total + $_tmp;

	$y = $y + $standard_h;
	$pdf->SetXY($x1,$y);
	$pdf->SetFont("arialn", "", 9);

	$pdf->Cell($x, $standard_h, "Non capitalized" , 0, "L");

	$pdf->SetXY($x2,$y);
	$pdf->SetFont("arialn", "", 9);
	
	$pdf->Cell(25, $standard_h, number_format(round($_tmp/1000, 0), 0, '.', "'"), 0, "", "R");
	$pdf->Cell(25, $standard_h, number_format(round($_tmp2/1000, 0), 0, '.', "'"), 0, "", "R");
	//end dummy

	
	$y = $pdf->GetY()+$standard_h;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(70, $standard_h, "Total Investment", 1, "", "L");

	$pdf->Cell(25, $standard_h, number_format(round($investment_total/1000, 0), 0, '.', "'"), 1, "", "R");
	
	$tmp = $ln_basicdata["ln_basicdata_exchangerate"]*$investment_total/$ln_basicdata["ln_basicdata_factor"];
	$pdf->Cell(25, $standard_h, number_format(round($tmp/ 1000,0), 0, '.', "'"), 1, "", "R");

	$y = $pdf->GetY()+$standard_h;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(70, $standard_h, "Total Investment per sqm (total surface)", 1, "", "L");
	

	$tmp = $investment_total/$gross_surface;
	$pdf->Cell(25, $standard_h, number_format(round($tmp/1000, 0), 0, '.', "'"), 1, "", "R");
	
	$tmp = $ln_basicdata["ln_basicdata_exchangerate"]*$investment_total/$ln_basicdata["ln_basicdata_factor"];
	$tmp = $tmp/$gross_surface;
	$pdf->Cell(25, $standard_h, number_format(round($tmp/1000, 0), 0, '.', "'"), 1, "", "R");
	
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(128, $standard_h, "Exch. rate (" . $currency_symbol . "/CHF)", 1, "", "L");
	$pdf->Cell(20, $standard_h, $ln_basicdata["ln_basicdata_exchangerate"], 1, "", "L", true);

	//box city information
	$y = $y_tmp;
	$pdf->SetXY($margin_left+121,$y);
	$pdf->Cell(148, 9*$standard_h, "", 1);

	
	$pdf->SetXY($margin_left+121,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(148, $standard_h, "City information", 1, "", "L");
	
	$y = $y + $standard_h;
	$pdf->SetXY($margin_left+121,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(128, $standard_h, "Number of POS (for the same Brand) in the same city area", 1, "", "L");
	$pdf->Cell(20, $standard_h, $ln_basicdata["ln_basicdata_numofpos_in_area"], 1, "", "L", true);
	
	
	$y = $y + $standard_h;
	$pdf->SetXY($margin_left+121,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(128, $standard_h, "", 1, "", "L");
	$pdf->SetXY($margin_left+121,$y);
	$pdf->Cell(20, $standard_h, "of which :", 0, "", "L");
	$pdf->Cell(108, $standard_h, "- Swatch Group Corporate Stores", 0, "", "L");
	$pdf->Cell(20, $standard_h, $ln_basicdata["ln_basicdata_numofcpos_in_area"], 1, "", "L", true);
	
	$y = $y + $standard_h;
	$pdf->SetXY($margin_left+121,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(128, $standard_h, "", 1, "", "L");
	$pdf->SetXY($margin_left+121,$y);
	$pdf->Cell(20, $standard_h, "", 0, "", "L");
	$pdf->Cell(108, $standard_h, "- Swatch Group Franchise Stores", 0, "", "L");
	$pdf->Cell(20, $standard_h, $ln_basicdata["ln_basicdata_numoffpos_in_area"], 1, "", "L", true);
	
	
	$y = $y + $standard_h;
	$pdf->SetXY($margin_left+121,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(128, $standard_h, "Average buildout costs (k" . $currency_symbol. "/m2) of corporate POS in the same area", 1, "", "L");
	$pdf->Cell(20, $standard_h, number_format($average_buildout_costs, 0, '.', "'"), 1, "", "L", true);
	
	$y = $y + $standard_h;
	$pdf->SetXY($margin_left+121,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(148, $standard_h, "HR information", 1, "", "L");
	
	$y = $y + $standard_h;
	$pdf->SetXY($margin_left+121,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(20, $standard_h, "Headcount:", 0, "", "L");
	$pdf->Cell(20, $standard_h, $head_counts, 0, "", "L");
	$y = $y + $standard_h;
	$pdf->SetXY($margin_left+121,$y);
	$pdf->Cell(20, $standard_h, "FTE:", 0, "", "L");
	$pdf->Cell(20, $standard_h, $ftes, 0, "", "L");
	
	// 3. Financial Figures
	$y = $pdf->GetY()+10;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(268, 6, "3. Financial figures " . $currency_symbol, 1, "", "L");
	

	$pdf->SetFont("arialn", "B", 9);
	$y = $pdf->GetY()+6;
	$y_tmp = $y;
	$pdf->SetXY($margin_left+1,$y);	
	$pdf->Cell(70, 5, "" , 1, "", "L");

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(25, 5, $year , 1, "", "R");
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(25, 5, "" , 1, "", "R");
	}

	$x_for_sellout_table = $pdf->GetX();
	$y_for_sellout_table = $pdf->GetY();

	$pdf->SetFont("arialn", "", 9);
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);	
	//draw boxes
	$pdf->Cell(70, 8, "" , 1, "", "L");
	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(25, 8, "" , 1, "", "R", true);
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(25, 8, "" , 1, "", "R", true);
	}



	$pdf->SetXY($margin_left+1,$y);	
	$pdf->Cell(70,4, "Watches units", 0, "L");

	

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(25, 4, $sales_units_watches_values[$year] , 0, "", "R");
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(25, 4, "" , 0, "", "R");
	}


	

	$y = $pdf->GetY()+4;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->Cell(70,4, "Bijoux units", 0, "L");

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(25, 4, $sales_units_jewellery_values[$year] , 0, "", "R");
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(25, 4, "" , 0, "", "R");
	}

	
	$pdf->SetFont("arialn", "B", 9);
	$y = $pdf->GetY()+4;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->Cell(70,5, "Gross sales", 1, "L");


	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		//$pdf->Cell(25, 5, number_format(round($ln_basicdata["ln_basicdata_exchangerate"]*$total_gross_sales_values[$year]/$ln_basicdata["ln_basicdata_factor"]/1000, 0), 0, '.', "'") , 1, "", "R", true);
		$pdf->Cell(25, 5, number_format(round($total_gross_sales_values[$year]/1000, 0), 0, '.', "'"), 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(25, 5, "" , 0, "", "R");
	}


	$pdf->SetFont("arialn", "", 9);
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);	
	//draw box
	$pdf->Cell(70, 10, "" , 1, "", "L");
	$pdf->SetXY($margin_left+1,$y);	
	$pdf->Cell(70,5, "Sales reductions", 0, "L");


	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$tmp = $sales_reduction_values[$year];
		
		//$pdf->Cell(25, 5, number_format(round($ln_basicdata["ln_basicdata_exchangerate"]*$tmp/$ln_basicdata["ln_basicdata_factor"]/1000, 0), 0, '.', "'") , 1, "", "R", true);
		$pdf->Cell(25, 5, number_format(round($tmp/1000, 0), 0, '.', "'"), 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(25, 5, "" , 0, "", "R");
	}
	
	$y = $pdf->GetY()+5;
	$pdf->SetFont("arialn", "B", 9);
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->Cell(70,5, "Net sales", 1, "L");

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		//$pdf->Cell(25, 5, number_format(round($ln_basicdata["ln_basicdata_exchangerate"]*$total_net_sales_values[$year]/$ln_basicdata["ln_basicdata_factor"]/1000, 0), 0, '.', "'") , 1, "", "R", true);
		$pdf->Cell(25, 5, number_format(round($total_net_sales_values[$year]/1000, 0), 0, '.', "'"), 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(25, 5, "" , 0, "", "R");
	}


	/*
	$pdf->SetFont("arialn", "", 9);
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);	
	//draw box
	$pdf->Cell(70, 10, "" , 1, "", "L");
	$pdf->SetXY($margin_left+1,$y);	
	$pdf->Cell(70,5, "Material of Products Sold", 0, "L");
	

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$tmp = -1*$material_of_products_values[$year];
		
		//$pdf->Cell(25, 5, number_format(round($ln_basicdata["ln_basicdata_exchangerate"]*$tmp/$ln_basicdata["ln_basicdata_factor"]/1000, 0), 0, '.', "'") , 1, "", "R", true);
		$pdf->Cell(25, 5, number_format(round($tmp/1000, 0), 0, '.', "'"), 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(25, 5, "" , 0, "", "R");
	}
	*/


	$pdf->SetFont("arialn", "", 9);
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);	
	//draw box
	$pdf->Cell(70, 8, "" , 1, "", "L");
	$pdf->SetXY($margin_left+1,$y);	
	$pdf->Cell(70,4, "Gross margin", 0, "L");


	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		//$pdf->Cell(25, 5, number_format(round($ln_basicdata["ln_basicdata_exchangerate"]*$total_gross_margin_values[$year]/$ln_basicdata["ln_basicdata_factor"]/1000, 0), 0, '.', "'") , 1, "", "R", true);
		$pdf->Cell(25, 4, number_format(round($total_gross_margin_values[$year]/1000, 0), 0, '.', "'"), 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(25, 4, "" , 0, "", "R");
	}
	
	$y = $pdf->GetY()+4;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(70,4, "Gross margin % (of sales)", 0, "L");

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{

		$pdf->Cell(25, 4, number_format(round($total_goss_margin_shares[$year], 1), 0, '.', "'") . "%", 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(25, 4, "" , 0, "", "R");
	}


	$pdf->SetFont("arialn", "", 9);
	$y = $pdf->GetY()+4;
	$pdf->SetXY($margin_left+1,$y);	
	//draw box
	$pdf->Cell(70, 25, "" , 1, "", "L");
	$pdf->SetXY($margin_left+1,$y);	
	$pdf->Cell(70,5, "Indirect salaries & social benefits", 0, "L");


	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$tmp = -1*($indirect_salaries_values[$year]);
		//$pdf->Cell(25, 5, number_format(round($ln_basicdata["ln_basicdata_exchangerate"]*$tmp/$ln_basicdata["ln_basicdata_factor"]/1000, 0), 0, '.', "'") , 1, "", "R", true);
		$pdf->Cell(25, 5, number_format(round($tmp/1000, 0), 0, '.', "'"), 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(25, 5, "" , 0, "", "R");
	}


	$pdf->SetFont("arialn", "", 9);
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);	
	$pdf->Cell(70,5, "Rents, amortization key money", 0, "L");


	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$tmp = -1*($rents_total_values[$year] + $prepayed_rent_values[$year]);
		//$pdf->Cell(25, 5, number_format(round($ln_basicdata["ln_basicdata_exchangerate"]*$tmp/$ln_basicdata["ln_basicdata_factor"]/1000, 0), 0, '.', "'") , 1, "", "R", true);
		$pdf->Cell(25, 5, number_format(round($tmp/1000, 0), 0, '.', "'"), 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(25, 5, "" , 0, "", "R");
	}
	
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->Cell(70,5, "% (of sales)", 0, "L");


	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$tmp = $rents_total_values[$year] + $prepayed_rent_values[$year];
		if($total_net_sales_values[$year] > 0)
		{
			$tmp = $tmp/$total_net_sales_values[$year];
			$tmp = number_format(round(100*$tmp, 2), 0, '.', "'") . "%";
		}
		else
		{
			$tmp = "";
		}
		$pdf->Cell(25, 5, $tmp, 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(25, 5, "" , 0, "", "R");
	}

	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->Cell(70,5, "Depreciation on fixed assets", 0, "L");

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$tmp = -1*$depreciation_values[$year];
		//$pdf->Cell(25, 5, number_format(round($ln_basicdata["ln_basicdata_exchangerate"]*$tmp/$ln_basicdata["ln_basicdata_factor"]/1000, 0), 0, '.', "'") , 1, "", "R", true);
		$pdf->Cell(25, 5, number_format(round($tmp/1000, 0), 0, '.', "'"), 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(25, 5, "" , 0, "", "R");
	}

	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->Cell(70,5, "Other expenses", 0, "L");


	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$tmp = -1*($auxmat_values[$year] + $sales_admin_values[$year] + $other_expenses_values[$year]+ $marketing_expenses[$year]) ;
		//$pdf->Cell(25, 5, number_format(round($ln_basicdata["ln_basicdata_exchangerate"]*$tmp/$ln_basicdata["ln_basicdata_factor"]/1000, 0), 0, '.', "'") , 1, "", "R", true);
		$pdf->Cell(25, 5, number_format(round($tmp/1000, 0), 0, '.', "'"), 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(25, 5, "" , 0, "", "R");
	}

	
	$pdf->SetFont("arialn", "B", 9);
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->Cell(70,5, "Operating income (excl. wholesale margin)", 1, "L");


	$i = 0;
	foreach($years_page1 as $key=>$year)
	{

		//$pdf->Cell(25, 5, number_format(round($ln_basicdata["ln_basicdata_exchangerate"]*$operating_income01_values[$year]/$ln_basicdata["ln_basicdata_factor"]/1000, 0), 0, '.', "'") , 1, "", "R", true);
		$pdf->Cell(25, 5, number_format(round($operating_income01_values[$year]/1000, 0), 0, '.', "'"), 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(25, 5, "" , 0, "", "R");
	}

	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->Cell(58,5, "Operating income (incl. wholesale margin)", 1, "L");
	$pdf->SetFont("arialn", "", 9);

	
	$pdf->Cell(12,5, $weighted_average_whole_sale_margin . "%", 1, "", "R", true);
	$pdf->SetFont("arialn", "B", 9);
	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		//$pdf->Cell(25, 5, number_format(round($ln_basicdata["ln_basicdata_exchangerate"]*$operating_income02_values[$year]/$ln_basicdata["ln_basicdata_factor"]/1000, 0), 0, '.', "'") , 1, "", "R", true);
		$pdf->Cell(25, 5, number_format(round($operating_income02_values[$year]/1000, 0), 0, '.', "'"), 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(25, 5, "" , 0, "", "R");
	}


	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->Cell(70,5, "Operating income (incl. WSM scenario 80%)", 1, "L");
	$pdf->SetFont("arialn", "", 9);

	$pdf->SetFont("arialn", "B", 9);
	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(25, 5, number_format(round($operating_income02_80_percent_values[$year]/1000, 0), 0, '.', "'"), 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(25, 5, "" , 0, "", "R");
	}


	$pdf->SetFont("arialn", "", 9);
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->Cell(70,5, "Break even units (excl. wholesale margin)", 1, "L");

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(25, 5, number_format(round($break_even_retail_margin[$year], 0), 0, '.', "'"), 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(25, 5, "" , 0, "", "R");
	}

	$pdf->SetFont("arialn", "", 9);
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->Cell(70,5, "Break even units (incl. wholesale margin)", 1, "L");

	
	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(25, 5, number_format(round($break_even_wholesale_margin[$year], 0), 0, '.', "'"), 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(25, 5, "" , 0, "", "R");
	}
	


	//textbox at the right of sales figures
	$y_tmp2 = $pdf->GetY();

	$x_textbox = $margin_left+197;
	$y_textbox = $y_tmp;

	
	
	// 4. Approvals
	$y = $y_tmp2+6;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(268, 6, "4. Approvals", 1, "", "L");

	
	$y = $pdf->GetY()+6;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "B", 9);
	$pdf->MultiCell(60,9, "", 1, "L");
	$pdf->SetXY($margin_left+1,$y);
	$pdf->MultiCell(20,9, "Country", 0, "T");
	$pdf->SetFont("arialn", "", 9);
	$pdf->SetXY($margin_left+20,$y);
	$pdf->MultiCell(40,9, "Country Manager " . "\r\n" . $approval_name11, 0, "T");
	$pdf->SetXY($margin_left+60,$y);
	$pdf->MultiCell(74,9, "", 1, "T", true);

	$pdf->SetFont("arialn", "B", 9);
	$pdf->SetXY($margin_left+134,$y);
	$pdf->MultiCell(60,9, "", 1, "L");
	$pdf->SetXY($margin_left+134,$y);
	$pdf->MultiCell(20,9, "Brand", 0, "T");
	$pdf->SetFont("arialn", "", 9);
	$pdf->SetXY($margin_left+154, $y);
	$pdf->MultiCell(40,9, "Retail Controller" . "\r\n" . $approval_name9, 0, "T");
	$pdf->SetXY($margin_left+194,$y);
	$pdf->MultiCell(75,9, "", 1, "T", true);


	$y = $pdf->GetY();
	$pdf->SetXY($margin_left+1,$y);


	$pdf->SetFont("arialn", "B", 9);
	$pdf->MultiCell(60,9, "", 1, "L");
	$pdf->SetXY($margin_left+1,$y);
	$pdf->MultiCell(20,9, "", 0, "T");
	$pdf->SetFont("arialn", "", 9);
	$pdf->SetXY($margin_left+20,$y);
	$pdf->MultiCell(40,9, "Finance/Service Center " . "\r\n" . $approval_name12, 0, "T");
	$pdf->SetXY($margin_left+60,$y);
	$pdf->MultiCell(74,9, "", 1, "T", true);



	$pdf->SetFont("arialn", "B", 9);
	$pdf->SetXY($margin_left+134,$y);
	$pdf->MultiCell(60,9, "", 1, "L");
	$pdf->SetXY($margin_left+134,$y);
	$pdf->MultiCell(20,9, "", 0, "T");
	$pdf->SetFont("arialn", "", 9);
	$pdf->SetXY($margin_left+154,$y);
	$pdf->MultiCell(40,9, "VP Sales" . "\r\n" . $approval_name3, 0, "T");
	$pdf->SetXY($margin_left+194,$y);
	$pdf->MultiCell(75,9, "", 1, "T", true);



	$y = $pdf->GetY();
	$pdf->SetXY($margin_left+1,$y);

	//china projects
	if($project['order_client_address'] == 42
		or $project['order_client_address'] == 7) {
		$pdf->SetFont("arialn", "B", 9);
		$pdf->MultiCell(60,9, "", 1, "L");
		$pdf->SetXY($margin_left+1,$y);
		$pdf->MultiCell(20,9, "", 0, "T");
		$pdf->SetFont("arialn", "", 9);
		$pdf->SetXY($margin_left+20,$y);
		$pdf->MultiCell(40,9, "Head of Controlling " . "\r\n" . $approval_name16, 0, "T");
		$pdf->SetXY($margin_left+60,$y);
		$pdf->MultiCell(74,9, "", 1, "T", true);
	}
	else {
		$pdf->SetFont("arialn", "B", 9);
		$pdf->MultiCell(60,9, "", 1, "L");
		$pdf->SetXY($margin_left+1,$y);
		$pdf->MultiCell(20,9, "", 0, "T");
		$pdf->SetFont("arialn", "", 9);
		$pdf->SetXY($margin_left+20,$y);
		$pdf->MultiCell(40,9, "Brand Manager " . "\r\n" . $approval_name13, 0, "T");
		$pdf->SetXY($margin_left+60,$y);
		$pdf->MultiCell(74,9, "", 1, "T", true);
	}

	$pdf->SetFont("arialn", "B", 9);
	$pdf->SetXY($margin_left+134,$y);
	$pdf->MultiCell(60,9, "", 1, "L");
	$pdf->SetXY($margin_left+134,$y);
	$pdf->MultiCell(20,9, "", 0, "T");
	$pdf->SetFont("arialn", "", 9);
	$pdf->SetXY($margin_left+154,$y);
	$pdf->MultiCell(40,9, "VP Finance/CFO" . "\r\n" . $approval_name7, 0, "T");
	$pdf->SetXY($margin_left+194,$y);
	$pdf->MultiCell(75,9, "", 1, "T", true);

	
	$y = $pdf->GetY();
	$pdf->SetXY($margin_left+1,$y);
	
	//china projects
	if($project['order_client_address'] == 42
		or $project['order_client_address'] == 7) {
		$pdf->SetFont("arialn", "B", 9);
		$pdf->MultiCell(60,9, "", 1, "L");
		$pdf->SetXY($margin_left+1,$y);
		$pdf->MultiCell(20,9, "", 0, "T");
		$pdf->SetFont("arialn", "", 9);
		$pdf->SetXY($margin_left+20, $y);
		$pdf->MultiCell(40,9, "Brand Manager " . "\r\n" . $approval_name13, 0, "T");
		$pdf->SetXY($margin_left+60,$y);
		$pdf->MultiCell(74,9, "", 1, "T", true);
	}
	else {
		$pdf->SetFont("arialn", "B", 9);
		$pdf->MultiCell(60,9, "", 1, "L");
		$pdf->SetXY($margin_left+1,$y);
		$pdf->MultiCell(20,9, "", 0, "T");
		$pdf->SetFont("arialn", "", 9);
		$pdf->SetXY($margin_left+20, $y);
		$pdf->MultiCell(40,9, "", 0, "T");
		$pdf->SetXY($margin_left+60,$y);
		$pdf->MultiCell(74,9, "", 1, "T", true);
	}

	$pdf->SetFont("arialn", "B", 9);
	$pdf->SetXY($margin_left+134,$y);
	$pdf->MultiCell(60,9, "", 1, "L");
	$pdf->SetXY($margin_left+134,$y);
	$pdf->MultiCell(20,9, "", 0, "T");
	$pdf->SetFont("arialn", "", 9);
	$pdf->SetXY($margin_left+154,$y);
	$pdf->MultiCell(40,9, "President" . "\r\n" . $approval_name2, 0, "T");
	$pdf->SetXY($margin_left+194,$y);
	$pdf->MultiCell(75,9, "", 1, "T", true);


	

	//draw outer box
	$y = $pdf->GetY()-$margin_top - 8;
	$pdf->SetXY($margin_left,$y_at_start);
	$pdf->Cell(270, $y, "", 1);




	//list historical data in case of a relocation project
	$sales_figures_printed = false;
	if($project["project_relocated_posaddress_id"] > 0)
	{
		
		
		$pdf->SetXY($x_for_sellout_table + 2,$y_for_sellout_table);
		$_tmp_y1 = $pdf->getY();
		$pdf->SetFont('arialn','B',9);
		$pdf->MultiCell(71,9, "Sellouts of relocated POS: " .$relocated_pos, 1, "L", false, 1);
		$_tmp_y2 = $pdf->getY();

		if(($sellout_ending_year-$sellout_starting_year) > 2)
		{
			$sellout_starting_year = $sellout_starting_year + ($sellout_ending_year-$sellout_starting_year- 2);
		}
		
		foreach($sellouts_watches as $key=>$value)
		{
			if($key == $cer_basicdata["cer_basicdata_firstyear"])
			{
				$sellout_starting_year++;
			}
			$sales_figures_printed = true;
		}
		
		
		$pdf->SetXY($x_for_sellout_table + 31,$y_for_sellout_table+1+($_tmp_y2 - $_tmp_y1));

		
		for($i=$sellout_starting_year;$i<=$sellout_ending_year;$i++)
		{
			if(array_key_exists($i, $sellouts_watches))
			{
				$pdf->Cell(14,5,$i . "(" . $sellouts_months[$i] . ")",0, 0, 'R', 0);
			}
		}
		
		$pdf->SetXY($x_for_sellout_table + 2,$y_for_sellout_table+1+($_tmp_y2 - $_tmp_y1) + 10);
		$pdf->Cell(29,5,"Watches units (sold)",1, "", 'L');
		$pdf->SetFont('arialn','',9);
		for($i=$sellout_starting_year;$i<=($sellout_ending_year);$i++)
		{
			if(array_key_exists($i, $sellouts_watches))
			{
				$pdf->Cell(14,5,number_format($sellouts_watches[$i], 0, ".", "'"),1, "", 'R');
			}
		}


		$pdf->SetXY($x_for_sellout_table + 2,$y_for_sellout_table + 15);
		$pdf->SetFont('arialn','',9);
		$pdf->Cell(29,5,"Bjoux units (sold)",1, "", 'L', 0);
		$pdf->SetFont('arialn','',9);
		for($i=$sellout_starting_year;$i<=($sellout_ending_year);$i++)
			{
			if(array_key_exists($i, $sellouts_bjoux))
			{
				$pdf->Cell(14,5,number_format($sellouts_bjoux[$i], 0, ".", "'"),1, 0, 'R', 0);
			}
		}
	}



	//output textbox at the right of sales figures
	if($sales_figures_printed == true)
	{
		$y_textbox = $pdf->GetY() + 8;
	}

	$pdf->SetFont("arialn", "", 9);
	$pdf->SetXY($x_textbox,$y_textbox);
	$pdf->MultiCell(70,10, $ln_basicdata["ln_basicdata_rent"], 0, "L");

		
?>