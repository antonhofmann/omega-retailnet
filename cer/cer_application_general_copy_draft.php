<?php
/********************************************************************

    cer_application_general_copy_draft.php

    copy a draft into an existing project

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-03-17
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-03-17
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require "include/get_draft_functions.php";

check_access("has_access_to_cer_drafts");

$posorder_present = param("p");
$form_type = param("ft");

$project = get_project(param("pid"));
$cer_basicdata = get_project_cer_basicdata(param("pid"));

/********************************************************************
    prepare all data needed
*********************************************************************/
$project = get_project(param("pid"));
$country = $project["order_shop_address_country"];

if(has_access("has_access_to_drafts_of_his_country")) {

	
	$list_filter = ' where cer_basicdata_user_id = ' . user_id();

	
	//get the team members
	$user_list = '';
	$sql = 'select address_id from addresses ' .
		   'left join users on user_address = address_id ' . 
		   'where user_id = ' . user_id();

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$sql = 'select user_id from users ' .
			   'where user_address = ' . $row["address_id"] . 
			   ' and user_active = 1';

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$user_list .= $row['user_id'] . ',';
		}

		$user_list = substr($user_list, 0, strlen($user_list)-1);

		//$sql_c = "select *, concat(user_name, ' ', user_firstname) as uname from cer_drafts " . 
		//	     "left join users on user_id = cer_basicdata_user_id";


		$list_filter .= ' and cer_basicdata_user_id IN (' . $user_list . ')';
	}

	$sql = "select cer_basicdata_id, cer_basicdata_title from cer_drafts ";
	
	
}
else
{
	$sql = "select cer_basicdata_id, cer_basicdata_title  from cer_drafts " . 
			   "left join users on user_id = cer_basicdata_user_id";
	$list_filter = '';
}

if($list_filter) {
	$list_filter .= ' and cer_basicdata_country = ' . $country;
}
else
{
	$list_filter = ' where cer_basicdata_country = ' . $country;
}

$sql_darfts = $sql . $list_filter . ' order by cer_basicdata_title';



$years = array();

$s = date("Y");

$l = $s+21;
for($i=$s;$i<$l;$i++)
{
	$years[$i] = $i;
}

$form = new Form("cer_drafts", "CER/AF Draft");

$form->add_section("");
$form->add_comment("The data of the selected business plan draft will be copied into your project.");
$form->add_hidden("pid", param('pid'));

$form->add_list("selected_draft_id", "Selected Draft*", $sql_darfts, NOTNULL);
$form->add_list("first_year", "First Year*", $years, NOTNULL);

$form->add_button("copy_draft", "Copy Business Plan Draft");
$form->add_button("back", "Back");

$form->populate();
$form->process();

if($form->button("back")) {
	$link = 'cer_application_general.php?pid=' . param('pid');
	redirect($link);
}
elseif($form->button("copy_draft") and $form->validate()) {
	
	$id = $form->value('selected_draft_id');
	
	$year_offset = 0;
	
	
	$sql = "select * from cer_drafts  " . 
		   "where cer_basicdata_id = " . $id;


	$res = mysql_query($sql) or dberror($sql);
	if ($row_p = mysql_fetch_assoc($res))
	{

		$year_offset = $form->value("first_year") - $row_p['cer_basicdata_firstyear'];
		
		$values = '';

				
		$values .= 'cer_basicdata_firstyear' . ' = ';
		$values .= dbquote(($row_p['cer_basicdata_firstyear'] + $year_offset)) . ',';

		$values .= 'cer_basicdata_firstmonth' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_firstmonth']) . ',';

		$values .= 'cer_basicdata_lastyear' . ' = ';
		$values .= dbquote(($row_p['cer_basicdata_lastyear'] + $year_offset)) . ',';

		$values .= 'cer_basicdata_lastmonth' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_lastmonth']) . ',';

		
		$values .= 'cer_basicdata_dicount_rate' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_dicount_rate']) . ',';

		$values .= 'cer_bascidata_liquidation_keymoney' . ' = ';
		$values .= dbquote($row_p['cer_bascidata_liquidation_keymoney']) . ',';

		$values .= 'cer_basicdata_liquidation_deposit' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_liquidation_deposit']) . ',';

		$values .= 'cer_bascidata_liquidation_stock' . ' = ';
		$values .= dbquote($row_p['cer_bascidata_liquidation_stock']) . ',';

		$values .= 'cer_bascicdate_liquidation_staff' . ' = ';
		$values .= dbquote($row_p['cer_bascicdate_liquidation_staff']) . ',';

		$values .= 'cer_basicdata_residual_value' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_residual_value']) . ',';

		$values .= 'cer_basicdata_firstyear_depr' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_firstyear_depr']) . ',';

		$values .= 'cer_basicdata_firstmonth_depr' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_firstmonth_depr']) . ',';

		$values .= 'cer_basicdata_residual_depryears' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_residual_depryears']) . ',';

		$values .= 'cer_basicdata_residual_deprmonths' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_residual_deprmonths']) . ',';

		$values .= 'cer_basicdata_residualkeymoney_value' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_residualkeymoney_value']) . ',';

		$values .= 'cer_basicdata_residualkeymoney_depryears' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_residualkeymoney_depryears']) . ',';

		$values .= 'cer_basicdata_residualkeymoney_deprmonths' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_residualkeymoney_deprmonths']) . ',';

		$values .= 'cer_basicdata_recoverable_keymoney' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_recoverable_keymoney']) . ',';

		$values .= 'cer_basicdata_calculate_rent_avg' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_calculate_rent_avg']) . ',';

		$values .= 'cer_basicdata_add_tob_rents' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_add_tob_rents']) . ',';

		$values .= 'cer_extend_depreciation_period' . ' = ';
		$values .= dbquote($row_p['cer_extend_depreciation_period']) . ',';

		$values .= 'cer_basicdata_salary_growth' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_salary_growth']) . ',';

		$values .= 'cer_basicdata_rental_increase_mode' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_rental_increase_mode']) . ',';

		$values .= 'cer_basicdata_rental_index_mode' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_rental_index_mode']) . ',';

		$values .= 'cer_basicdata_tob_from_net_sales' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_tob_from_net_sales']) . ',';

		$values .= 'cer_basicdata_commission_from_net_sales' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_commission_from_net_sales']) . ',';

		$values .= 'cer_basicdata_commission' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_commission']) . ',';

		$values .= 'cer_basicdata_commission_tax_percent' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_commission_tax_percent']) . ',';

		$values .= 'cer_basicdata_cost_on_net_sales' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_cost_on_net_sales']) . ',';

		$values .= 'cer_basicdata_tob_from_breakpoint_difference' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_tob_from_breakpoint_difference']) . ',';

		$values .= 'cer_basicdata_revenue_p1' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_revenue_p1']) . ',';

		$values .= 'cer_basicdata_revenue_p2' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_revenue_p2']) . ',';

		$values .= 'cer_basicdata_revenue_p3' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_revenue_p3']) . ',';

		$values .= 'cer_basicdata_revenue_p4' . ' = ';
		$values .= dbquote($row_p['cer_basicdata_revenue_p4']) . ',';

		/*
		$fields .= 'cer_basicdata_future_investment' . ',';
		$values .= dbquote($row_p['cer_basicdata_future_investment']) . ',';

		$fields .= 'cer_basicdata_future_depr_start_year' . ',';
		$values .= dbquote($row_p['cer_basicdata_future_depr_start_year']) . ',';

		$fields .= 'cer_basicdata_future_depr_start_month' . ',';
		$values .= dbquote($row_p['cer_basicdata_future_depr_start_month']) . ',';
		*/

		$values = substr($values, 0, strlen($values)-1);

		$sql_i = 'update cer_basicdata SET ' . $values . ' where cer_basicdata_version = 0 and cer_basicdata_project = ' . param("pid");
		$result = mysql_query($sql_i) or dberror($sql_i);

	}

	if($id > 0)
	{
		//expenses
		$sql = 'delete from cer_expenses where cer_expense_cer_version = 0 and cer_expense_project = ' . param("pid");
		$result = mysql_query($sql) or dberror($sql);

		$sql = 'Select * from cer_draft_expenses where cer_expense_draft_id =' . $id;

		if($project["project_cost_type"] == 2) // franchisee
		{
			$sql .= " and cer_expense_type <> 14 " . 
		            " and cer_expense_type <> 11 " . 
		            " and cer_expense_type <> 12 "; 
		}
		
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				if($field_name == 'cer_expense_draft_id') {
					$fields .= 'cer_expense_project,';
					$values .= param('pid') . ',';
				}
				elseif($field_name == 'cer_expense_year')
				{
					$fields .= 'cer_expense_year,';
					$values .= dbquote(($value + $year_offset)) . ',';
					
				}
				elseif($field_name != 'cer_expense_id') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_expenses (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//new additional rental costs

		$sql = 'delete from cer_additional_rental_costs where cer_additional_rental_cost_cer_version = 0 and cer_additional_rental_cost_project = ' . param("pid");
		$result = mysql_query($sql) or dberror($sql);

		$sql = 'Select * from cer_draft_additional_rental_costs where cer_additional_rental_cost_draft_id =' . $id;

				
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				if($field_name == 'cer_additional_rental_cost_draft_id') {
					$fields .= 'cer_additional_rental_cost_project,';
					$values .= param('pid') . ',';
				}
				elseif($field_name != 'cer_additional_rental_cost_id') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_additional_rental_costs (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}



		$sql = 'delete from cer_additional_rental_cost_amounts where cer_additional_rental_cost_amount_version = 0 and cer_additional_rental_cost_amount_project = ' . param("pid");
		$result = mysql_query($sql) or dberror($sql);

		$sql = 'Select * from cer_draft_additional_rental_cost_amounts where cer_additional_rental_cost_amount_draft_id =' . $id;

				
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				if($field_name == 'cer_additional_rental_cost_amount_draft_id') {
					$fields .= 'cer_additional_rental_cost_amount_project,';
					$values .= param('pid') . ',';
				}
				elseif($field_name != 'cer_additional_rental_cost_amount_id') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_additional_rental_cost_amounts (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}




		//end new additional rental costs


		
		//percentages for turn over based rats
		$sql = 'delete from cer_rent_percent_from_sales where cer_rent_percent_from_sale_cer_version = 0 and cer_rent_percent_from_sale_project = ' . param("pid");
		$result = mysql_query($sql) or dberror($sql);

		$sql = 'Select * from cer_draft_rent_percent_from_sales where cer_rent_percent_from_sale_draft_id =' . $id;

				
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{

			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				if($field_name == 'cer_rent_percent_from_sale_draft_id') {
					$fields .= 'cer_rent_percent_from_sale_project,';
					$values .= param('pid') . ',';
				}
				elseif($field_name == 'cer_rent_percent_from_sale_year')
				{
					$fields .= 'cer_rent_percent_from_sale_year,';
					$values .= dbquote(($value + $year_offset)) . ',';
					
				}
				elseif($field_name != 'cer_rent_percent_from_sale_id') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_rent_percent_from_sales (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//fixed rents
		$sql = 'Select * from cer_draft_fixed_rents ' . 
			   'where cer_fixed_rent_draft_id =' . $id;
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				if($field_name == 'cer_fixed_rent_draft_id') {
					$fields .= 'cer_fixed_rent_project_id,';
					$values .=  param('pid') . ',';
				}
				elseif($field_name == 'cer_fixed_rent_from_year')
				{
					$fields .= $field_name . ',';
					$values .= dbquote(($value + $year_offset)) . ',';
					
				}
				elseif($field_name == 'cer_fixed_rent_to_year')
				{
					$fields .= $field_name . ',';
					$values .= dbquote(($value + $year_offset)) . ',';
					
				}
				elseif($field_name != 'cer_fixed_rent_id') {
						$fields .= $field_name . ',';
						$values .= dbquote($value) . ',';
				}
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_fixed_rents (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//investments
		$sql = 'delete from cer_investments where cer_investment_cer_version = 0 and cer_investment_project = ' . param("pid");
		
		$result = mysql_query($sql) or dberror($sql);

		$sql = 'Select * from cer_draft_investments where cer_investment_draft_id =' . $id;


		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				if($field_name == 'cer_investment_draft_id') {
					$fields .= 'cer_investment_project,';
					$values .= param('pid') . ',';
				}
				elseif($field_name != 'cer_investment_id') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_investments (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//payments
		$sql = 'delete from cer_paymentterms where cer_paymentterm_cer_version = 0 and cer_paymentterm_project = ' . param("pid");
		$result = mysql_query($sql) or dberror($sql);

		$sql = 'Select * from cer_draft_paymentterms where cer_paymentterm_draft_id =' . $id;
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				if($field_name == 'cer_paymentterm_draft_id') {
					$fields .= 'cer_paymentterm_project,';
					$values .= param('pid') . ',';
				}
				elseif($field_name == 'cer_paymentterm_year')
				{
					$fields .= 'cer_paymentterm_year,';
					$values .= dbquote(($value + $year_offset)) . ',';
					
				}
				elseif($field_name != 'cer_paymentterm_id') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_paymentterms (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//revenues
		$sql = 'delete from cer_revenues where cer_revenue_cer_version = 0 and cer_revenue_project = ' . param("pid");
		$result = mysql_query($sql) or dberror($sql);

		$sql = 'Select * from cer_draft_revenues where cer_revenue_draft_id =' . $id;
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				if($field_name == 'cer_revenue_draft_id') {
					$fields .= 'cer_revenue_project,';
					$values .= param('pid') . ',';
				}
				elseif($field_name == 'cer_revenue_year')
				{
					$fields .= 'cer_revenue_year,';
					$values .= dbquote(($value + $year_offset)) . ',';
					
				}
				elseif($field_name != 'cer_revenue_id') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_revenues (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//salaries
		$sql = 'delete from cer_salaries where cer_salary_cer_version = 0 and cer_salary_project = ' . param("pid");
		$result = mysql_query($sql) or dberror($sql);

		$sql = 'Select * from cer_draft_salaries where cer_salary_draft_id =' . $id;
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				if($field_name == 'cer_salary_draft_id') {
					$fields .= 'cer_salary_project,';
					$values .= param('pid') . ',';
				}
				elseif($field_name == 'cer_salary_year_starting')
				{
					$fields .= 'cer_salary_year_starting,';
					$values .= dbquote(($value + $year_offset)) . ',';
					
				}
				elseif($field_name != 'cer_salary_id') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_salaries (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//stocks
		$sql = 'delete from cer_stocks where cer_stock_cer_version = 0 and cer_stock_project = ' . param("pid");
		$result = mysql_query($sql) or dberror($sql);

		$sql = 'Select * from cer_draft_stocks where cer_stock_draft_id =' . $id;
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				if($field_name == 'cer_stock_draft_id') {
					$fields .= 'cer_stock_project,';
					$values .= param('pid') . ',';
				}
				elseif($field_name == 'cer_stock_year')
				{
					$fields .= 'cer_stock_year,';
					$values .= dbquote(($value + $year_offset)) . ',';
					
				}
				elseif($field_name != 'cer_stock_id') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_stocks (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}

		//adapt all timedepenent data
		//get standardparameters
		$sparams = array();
		$sql = "select * from cer_standardparameters";
		$res = mysql_query($sql) or dberror($sql);
		while($row = mysql_fetch_assoc($res))
		{
			$sparams[$row["cer_standardparameter_id"]] = $row["cer_standardparameter_value"];
		}

		//get inflationrates
		$inflationrates = array();
		$sql = "select * from cer_inflationrates " . 
			   "where inflationrate_country = " . $row_p['cer_basicdata_country'] . 
			   " order by inflationrate_year";

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$inflationrates[$row["inflationrate_year"]] = $row["inflationrate_rate"];
		}

		$inflationrates = serialize($inflationrates);
		
		//get interestrates
		$interestrates = array();

		$sql = "select * " .
			   "from cer_interestrates where interestrate_country = " . $row_p['cer_basicdata_country']  . 
			   " order by interestrate_year ASC";

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$interestrates[$row["interestrate_year"]] = $row["interestrate_rate"];
		}

		$interestrates = serialize($interestrates);

		//update cer_basicdata
		$fields = array();


		$fields[] = "cer_basicdata_dicount_rate  = " . dbquote($sparams[1]);
		$fields[] = "cer_bascidata_liquidation_keymoney = " . dbquote($sparams[2]);
		$fields[] = "cer_basicdata_liquidation_deposit = " . dbquote($sparams[3]);
		$fields[] = "cer_bascidata_liquidation_stock = " . dbquote($sparams[4]);
		$fields[] = "cer_bascicdate_liquidation_staff = " . dbquote($sparams[5]);
		$fields[] = "cer_basicdata_inflationrates = " . dbquote($inflationrates);
		$fields[] = "cer_basic_data_interesrates = " . dbquote($interestrates);

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}

		$sql = "update cer_basicdata set " . join(", ", $fields) . 
			" where cer_basicdata_project = " . param("pid");
		mysql_query($sql) or dberror($sql);

		$years = array();

		$fy = $row_p['cer_basicdata_firstyear'] + $year_offset;
		$ly = $row_p['cer_basicdata_lastyear'] + $year_offset;

	
		for($i = $fy;$i<=$ly;$i++)
		{
			
			$years[] = $i;
		}

		$result = calculate_forcasted_salaries(param("pid"), $years, $row_p['cer_basicdata_country']);


		//add pos leases
		if($row_p["cer_basicdata_lease_type"] > 0) {

			//get pos address
			$table_pos_leases = "";
			$posaddress_id = 0;
			$posorder_order = 0;
			$order_id = $project['project_order'];

			$sql = 'select posorder_posaddress, posorder_order from posorderspipeline ' . 
				   'where posorder_order = ' . $order_id;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$table_pos_leases = "posleasepipeline";
				$posaddress_id = $row['posorder_posaddress'];
				$posorder_order = $row['posorder_order'];
			}
			else {
				$sql = 'select posorder_posaddress, posorder_order from posorders ' . 
						'where posorder_order = ' . $order_id;

				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					$table_pos_leases = "posleases";
					$posaddress_id = $row['posorder_posaddress'];
					$posorder_order = $row['posorder_order'];
				}
			}

			if($posaddress_id > 0) {
				$fields = array();
				$values = array();

				$fields[] = "poslease_posaddress";
				$values[] = dbquote($posaddress_id);

				$fields[] = "poslease_order";
				$values[] = dbquote($posorder_order);

				$fields[] = "poslease_lease_type";
				$values[] = dbquote($row_p['cer_basicdata_lease_type']);
				
				
				$tmp = $year_offset + 1*substr($row_p['cer_basicdata_lease_startdate'], 0, 4);
				$tmp = $tmp . substr($row_p['cer_basicdata_lease_startdate'], 4, 6);
				$fields[] = "poslease_startdate";
				$values[] = dbquote($tmp);

				
				$tmp = $year_offset + 1*substr($row_p['cer_basicdata_lease_enddate'], 0, 4);
				$tmp = $tmp . substr($row_p['cer_basicdata_lease_enddate'], 4, 6);
				$fields[] = "poslease_enddate";
				$values[] = dbquote($tmp);
				
				$fields[] = "poslease_breakpoint_percent";
				$values[] = dbquote($row_p['cer_basicdata_breakpoint_percent']);

				$fields[] = "poslease_breakpoint_amount";
				$values[] = dbquote($row_p['cer_basicdata_breakpoint_amount']);

				$fields[] = "poslease_breakpoint_percent2";
				$values[] = dbquote($row_p['cer_basicdata_breakpoint_percent2']);

				$fields[] = "poslease_breakpoint_amount2";
				$values[] = dbquote($row_p['cer_basicdata_breakpoint_amount2']);

				$fields[] = "poslease_breakpoint_percent3";
				$values[] = dbquote($row_p['cer_basicdata_breakpoint_percent3']);

				$fields[] = "poslease_breakpoint_amount3";
				$values[] = dbquote($row_p['cer_basicdata_breakpoint_amount3']);


				$fields[] = "poslease_indexclause_in_contract";
				$values[] = dbquote($row_p['cer_basicdata_indexclause_in_contract']);

				$fields[] = "poslease_indexrate";
				$values[] = dbquote($row_p['cer_basicdata_indexrate']);


				$fields[] = "poslease_average_increase";
				$values[] = dbquote($row_p['cer_basicdata_average_increase']);

				$fields[] = "poslease_realestate_fee";
				$values[] = dbquote($row_p['cer_basicdata_realestate_fee']);

				$fields[] = "poslease_annual_charges";
				$values[] = dbquote($row_p['cer_basicdata_annual_charges']);

				$fields[] = "poslease_other_fees";
				$values[] = dbquote($row_p['cer_basicdata_other_fees']);

				$fields[] = "date_created";
				$values[] = "now()";

				$fields[] = "date_modified";
				$values[] = "now()";

				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				$fields[] = "user_modified";
				$values[] = dbquote(user_login());

				if($table_pos_leases == "posleasepipeline")
				{
					$sql = 'delete from posleasespipeline where poslease_posaddress = ' . $posaddress_id . 
						   ' and poslease_startdate = ' . dbquote($row_p['cer_basicdata_lease_startdate']) .
						   ' and poslease_enddate = ' . dbquote($row_p['cer_basicdata_lease_enddate']);

					$result = mysql_query($sql) or dberror($sql);
					
					$sql = "insert into posleasespipeline (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				}
				else
				{
					$sql = 'delete from posleases where poslease_posaddress = ' . $posaddress_id . 
						   ' and poslease_startdate = ' . dbquote($row_p['cer_basicdata_lease_startdate']) .
						   ' and poslease_enddate = ' . dbquote($row_p['cer_basicdata_lease_enddate']);

					$result = mysql_query($sql) or dberror($sql);
					
					$sql = "insert into posleases (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				}
				
				mysql_query($sql) or dberror($sql);
			}
		}


	}

	$link = 'cer_application_general.php?pid=' . param('pid');
	redirect($link);
	
}


$page = new Page("cer_projects");
require "include/project_page_actions.php";
$page->header();

if($form_type == "INR03")
{
	$page->title("INR-03 - Retail Furniture in Third-party Store: Copy Business Plan Draft");
}
elseif($form_type == "AF")
{
	$page->title("AF/Business Plan: Copy Business Plan Draft");
}
else
{
	$page->title("Capital Expenditure Request: Copy Business Plan Draft");
}


$form->render();

require "include/footer_scripts.php";
$page->footer();

?>