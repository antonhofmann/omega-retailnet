<?php
/********************************************************************

    cer_benchmark_05_xls.php

    Generate Excel File: CER BenchmarkRental Investments

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-10-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-10-15
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_access_to_cer_benchmarks");

require_once "../include/xls/Writer.php";
require_once "include/get_functions.php";

/********************************************************************
    prepare Data Needed
*********************************************************************/
$benchmark_id = '05';
include("cer_benchmark_filter.php");


/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$filename = "benchmark_" . $benchmark_shortcut . "_investment_details_" . date("Ymd") . ".xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename);
$xls->setVersion(8);
$sheet = $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

//formats
$f_header =& $xls->addFormat();
$f_header->setSize(12);
$f_header->setAlign('left');
$f_header->setBold();

$f_title =& $xls->addFormat();
$f_title->setSize(10);
$f_title->setAlign('left');
$f_title->setBold();

$f_group_title =& $xls->addFormat();
$f_group_title->setSize(10);
$f_group_title->setAlign('left');
$f_group_title->setBold();

$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);

$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_date =& $xls->addFormat();
$f_date->setSize(8);
$f_date->setAlign('left');
$f_date->setBorder(1);
$f_date->setNumFormat('DD.MM.YYYY');

$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_unused =& $xls->addFormat();
$f_unused->setSize(8);
$f_unused->setBorder(1);
$f_unused->setPattern(2);
//$f_unused->setBgColor('silver');
$f_unused->setBgColor(9);

$f_border_left=& $xls->addFormat();
$f_border_left->setTop(1);
$f_border_left->setBottom(1);
$f_border_left->setLeft(1);
$f_border_left->setSize(10);
$f_border_left->setAlign('left');
$f_border_left->setBold();

$f_border_right=& $xls->addFormat();
$f_border_right->setTop(1);
$f_border_right->setBottom(1);
$f_border_right->setRight(1);


$f_border_middle=& $xls->addFormat();
$f_border_middle->setTop(1);
$f_border_middle->setBottom(1);

//captions
$c01 = 0;
$captions = array();
$captions[] = "Project Number";
$sheet->setColumn($c01, $c01, 10);
$c01++;

$captions[] = "Project State";
$sheet->setColumn($c01, $c01, 6);
$c01++;

$captions[] = "Country";
$sheet->setColumn($c01, $c01, 14);
$c01++;


$captions[] = "POS Location Address";
$sheet->setColumn($c01, $c01, 60);
$c01++;

$captions[] = "Product Line";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Project Submission Date";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Opening Date";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Legal Type";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Project Kind";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Project Type";
$sheet->setColumn($c01, $c01, 12);
$c01++;

$captions[] = "Lease period in years";
$sheet->setColumn($c01, $c01, 6);
$c01++;

$captions[] = "Total Surface sqm";
$sheet->setColumn($c01, $c01, 6);
$c01++;

$captions[] = "Sales Surface sqm";
$sheet->setColumn($c01, $c01, 6);
$c01++;

$captions[] = "Goodwill/Key Money";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Construction costs";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Construction costs per sqm";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Store fixturing";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Store fixturing per sqm";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Architectural costs";
$sheet->setColumn($c01, $c01, 8);

$captions[] = "Equipment IT";
$sheet->setColumn($c01, $c01, 8);

$captions[] = "Other Costs";
$sheet->setColumn($c01, $c01, 8);

$captions[] = "Deposit/Recoverable Keymoney";
$sheet->setColumn($c01, $c01, 8);

$captions[] = "Other non-capitalizable fees and taxes";
$sheet->setColumn($c01, $c01, 8);

$captions[] = "Total investment without Key Money";
$sheet->setColumn($c01, $c01, 8);

$captions[] = "Total investment without Key Money per sqm";
$sheet->setColumn($c01, $c01, 8);
$c01++;
$c01++;

$captions[] = "Total investment without Key Money per sqm sales surface";
$sheet->setColumn($c01, $c01, 8);
$c01++;
$c01++;

/********************************************************************
    write all captions
*********************************************************************/
$sheet->write(0, 0, $benchmark_title, $f_header);

if($base_currency == "chf")
{
	$sheet->write(1, 0, "Investment Details in CHF (" . date("d.m.Y, H:m:s") . ")", $f_title);
}
else
{
	$sheet->write(1, 0, "Investment Details in LOC (" . date("d.m.Y, H:m:s") . ")", $f_title);
}

if($dfrom and $mfrom and  $dto and $mto) {
	$sheet->write(2, 1, "", $f_normal);
	$sheet->write(2, 2, "", $f_normal);
	$sheet->write(2, 0, "Time Period Filter: " . date("F", mktime(null, null, null, $mfrom)) . " ". $dfrom . " to " . date("F", mktime(null, null, null, $mto)) . " " . $dto, $f_normal);
}
elseif($mfrom and $mto) {
	$sheet->write(2, 1, "", $f_normal);
	$sheet->write(2, 2, "", $f_normal);
	$sheet->write(2, 0, "Time Period Filter: " . date("F", mktime(null, null, null, $mfrom)) . " to " . date("F", mktime(null, null, null, $mto)), $f_normal);
}




$sheet->setRow(4, 170);
$sheet->writeRow(4, 0, $captions, $f_caption);

/********************************************************************
    write all data
*********************************************************************/
$list_totals = array();
$list_totals_per_year = array();
$num_of_projects = 0;

$row_index = 5;
$c01 = 0;

asort($posorders);
foreach($posorders as $key=>$posorder)
{
	if($posorder["pipeline"] == 1)
	{
		$sql = "select *, projects.date_created as pdate " . 
			   "from posaddressespipeline " . 
			   "left join posorderspipeline on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " . 
			   "left join orders on order_id = project_order " .
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "left join project_costs on project_cost_order = posorder_order " .
			   "where posorder_type = 1 and posorder_id = " . $posorder["id"];
	}
	elseif($posorder["pipeline"] == 2)
	{
		$sql = "select *, projects.date_created as pdate " . 
			   "from posaddresses " . 
			   "left join posorderspipeline on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " . 
			   "left join orders on order_id = project_order " .
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "left join project_costs on project_cost_order = posorder_order " .
			   "where posorder_type = 1 and posorder_id = " . $posorder["id"];
	}
	else
	{
		$sql = "select *, projects.date_created as pdate " . 
			   "from posaddresses " . 
			   "left join posorders on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " . 
			   "left join orders on order_id = project_order " .
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "left join project_costs on project_cost_order = posorder_order " .
			   "where posorder_type = 1 and posorder_id = " . $posorder["id"];
	}	

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		
		//check if project has investments
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]);

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s)
			and $row_s['total'] > 0)
		{
		
			$year = substr($row['pdate'], 0, 4);
			
			if(!array_key_exists($year, $list_totals_per_year)) {
				$list_totals_per_year[$year] = array();
			}
			
			$num_of_projects++;

			$exchange_rate = 1;
			$factor = 1;


			$cer_basicdata = get_cer_basicdata($row["project_id"]);

			if(count($cer_basicdata) > 0)
			{
				$exchange_rate = $cer_basicdata["cer_basicdata_exchangerate"];
				$factor = $cer_basicdata["cer_basicdata_factor"];
				if(!$factor) {$factor = 1;}
			}

			if($base_currency == "chf")
			{
			}
			else
			{
				$exchange_rate = 1;
				$factor = 1;
			}

			$sheet->write($row_index, $c01, $row["posorder_ordernumber"], $f_normal);
			$c01++;

			$sheet->write($row_index, $c01, $row["order_actual_order_state_code"], $f_normal);
			$c01++;


			$sheet->write($row_index, $c01, $row["country_name"], $f_normal);
			$c01++;
			$tmp = $row["posaddress_place"] . ", " . $row["posaddress_name"] . ", " . $row["posaddress_address"];
			$sheet->write($row_index, $c01, $tmp, $f_normal);
			$c01++;
			
			
			$sheet->write($row_index, $c01, $row["product_line_name"], $f_normal);
			$c01++;

			$sheet->write($row_index, $c01, mysql_date_to_xls_date($row["pdate"]), $f_date);
			$c01++;
			
			$sheet->write($row_index, $c01, mysql_date_to_xls_date($row["posaddress_store_openingdate"]), $f_date);
			$c01++;

			$sheet->write($row_index, $c01, $row["project_costtype_text"], $f_normal);
			$c01++;

			$sheet->write($row_index, $c01, $row["projectkind_name"], $f_normal);
			$c01++;

			$sheet->write($row_index, $c01, $row["postype_name"], $f_normal);
			$c01++;


			//get the lease duration in years
			$starting_date = 0;
			$ending_date = 0;
			$extension_option = 0;
			$exit_option = 0;

			if($posorder["pipeline"] == 1)
			{
				$sql_s = "select poslease_startdate, poslease_enddate, poslease_extensionoption, poslease_exitoption " . 
						 "from posleasespipeline " . 
						 "where poslease_posaddress = " . $row["posaddress_id"];
			}
			else
			{
				$sql_s = "select poslease_startdate, poslease_enddate, poslease_extensionoption, poslease_exitoption " . 
						 "from posleases " . 
						 "where poslease_posaddress = " . $row["posaddress_id"];
			}
			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			while($row_s = mysql_fetch_assoc($res_s))
			{
				if($starting_date == 0)
				{
					$starting_date = $row_s["poslease_startdate"];
				}
				elseif($row_s["poslease_startdate"] < $starting_date)
				{
					$starting_date = $row_s["poslease_startdate"];
				}

				if($ending_date == 0)
				{
					$ending_date = $row_s["poslease_enddate"];
				}
				elseif($row_s["poslease_enddate"] > $ending_date)
				{
					$ending_date = $row_s["poslease_enddate"];
				}
			}
			if($starting_date == 0 and $ending_date == 0)
			{
				$lease_period = 0;
			}
			else
			{
				$start = strtotime($starting_date) ;
				$end = strtotime($ending_date) ;

				$lease_period = $end - $start;
				$lease_period = $lease_period / 86400;
				$lease_period = round($lease_period/365, 2);

			}

			$sheet->write($row_index, $c01, $lease_period, $f_number);
			$c01++;

			if(array_key_exists("lease_period", $list_totals))
			{
				$list_totals["lease_period"] = $list_totals["lease_period"] + $lease_period;
			}
			else
			{
				$list_totals["lease_period"] = $lease_period;
			}


			//get space in square meters
			$gross_space = $row["project_cost_gross_sqms"];
			$total_space = $row["project_cost_totalsqms"];
			$retail_area = $row["project_cost_sqms"];
			$back_office = $row["project_cost_backofficesqms"];


			if(array_key_exists("total_space",$list_totals_per_year[$year]))
			{
				$list_totals_per_year[$year]["total_space"] = $list_totals_per_year[$year]["total_space"] + $total_space;
			}
			else
			{
				$list_totals_per_year[$year]["total_space"] = $total_space;
			}

			if(array_key_exists("retail_area",$list_totals_per_year[$year]))
			{
				$list_totals_per_year[$year]["retail_area"] = $list_totals_per_year[$year]["retail_area"] + $retail_area;
			}
			else
			{
				$list_totals_per_year[$year]["retail_area"] = $retail_area;
			}

			
			if(array_key_exists("gross_space", $list_totals))
			{
				$list_totals["gross_space"] = $list_totals["gross_space"] + $gross_space;
			}
			else
			{
				$list_totals["gross_space"] = $gross_space;
			}

			
			if(array_key_exists("total_space", $list_totals))
			{
				$list_totals["total_space"] = $list_totals["total_space"] + $total_space;
			}
			else
			{
				$list_totals["total_space"] = $total_space;
			}

			if(array_key_exists("retail_area", $list_totals))
			{
				$list_totals["retail_area"] = $list_totals["retail_area"] + $retail_area;
			}
			else
			{
				$list_totals["retail_area"] = $retail_area;
			}

			if(array_key_exists("back_office", $list_totals))
			{
				$list_totals["back_office"] = $list_totals["back_office"] + $back_office;
			}
			else
			{
				$list_totals["back_office"] = $back_office;
			}

			
			$sheet->write($row_index, $c01, number_format($total_space, 2, ".", ""), $f_number);
			$c01++;


			$sheet->write($row_index, $c01, number_format($retail_area, 2, ".", ""), $f_number);
			$c01++;

			
			//get intagibles
			$intagibles = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and (cer_investment_type = 15 or cer_investment_type = 17)";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$intagibles = $row_s["total"] * $exchange_rate / $factor;
			}
			$sheet->write($row_index, $c01, round($intagibles, 0), $f_number);
			$c01++;

			if(array_key_exists("intagibles", $list_totals))
			{
				$list_totals["intagibles"] = $list_totals["intagibles"] + $intagibles;
			}
			else
			{
				$list_totals["intagibles"] = $intagibles;
			}

			if(array_key_exists("intagibles",$list_totals_per_year[$year]))
			{
				$list_totals_per_year[$year]["intagibles"] = $list_totals_per_year[$year]["intagibles"] + $intagibles;
			}
			else
			{
				$list_totals_per_year[$year]["intagibles"] = $intagibles;
			}


			

			//INVESTMENTS
			$total_cost = 0;
			//get construction cost
			$construction = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_type = 1";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$construction = $row_s["total"] * $exchange_rate / $factor;
			}
			$sheet->write($row_index, $c01, round($construction, 0), $f_number);
			$c01++;

			if(array_key_exists("construction", $list_totals))
			{
				$list_totals["construction"] = $list_totals["construction"] + $construction;
			}
			else
			{
				$list_totals["construction"] = $construction;
			}
			$total_cost = $total_cost + $construction;


			if(array_key_exists("construction",$list_totals_per_year[$year]))
			{
				$list_totals_per_year[$year]["construction"] = $list_totals_per_year[$year]["construction"] + $construction;
			}
			else
			{
				$list_totals_per_year[$year]["construction"] = $construction;
			}

			if(array_key_exists("total_cost",$list_totals_per_year[$year]))
			{
				$list_totals_per_year[$year]["total_cost"] = $list_totals_per_year[$year]["total_cost"] + $construction;
			}
			else
			{
				$list_totals_per_year[$year]["total_cost"] = $construction;
			}

			$construction_per_sqm = 0;
			if($total_space > 0)
			{
				$construction_per_sqm = round($construction / $total_space, 2);
			}
			$sheet->write($row_index, $c01, round($construction_per_sqm,0), $f_number);
			$c01++;

			if(array_key_exists("construction_per_sqm", $list_totals))
			{
				$list_totals["construction_per_sqm"] = $list_totals["construction_per_sqm"] + $construction_per_sqm;
			}
			else
			{
				$list_totals["construction_per_sqm"] = $construction_per_sqm;
			}


			//get fixturing cost
			$fixturing = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_type = 3";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$fixturing = $row_s["total"] * $exchange_rate / $factor;
			}
			$sheet->write($row_index, $c01, round($fixturing, 0), $f_number);
			$c01++;

			if(array_key_exists("fixturing", $list_totals))
			{
				$list_totals["fixturing"] = $list_totals["fixturing"] + $fixturing;
			}
			else
			{
				$list_totals["fixturing"] = $fixturing;
			}

			$total_cost = $total_cost + $fixturing;

			if(array_key_exists("fixturing",$list_totals_per_year[$year]))
			{
				$list_totals_per_year[$year]["fixturing"] = $list_totals_per_year[$year]["fixturing"] + $fixturing;
			}
			else
			{
				$list_totals_per_year[$year]["fixturing"] = $fixturing;
			}

			if(array_key_exists("total_cost",$list_totals_per_year[$year]))
			{
				$list_totals_per_year[$year]["total_cost"] = $list_totals_per_year[$year]["total_cost"] + $fixturing;
			}
			else
			{
				$list_totals_per_year[$year]["total_cost"] = $fixturing;
			}

			$fixturing_per_sqm = 0;
			if($total_space > 0)
			{
				$fixturing_per_sqm = round($fixturing / $total_space, 2);
			}
			$sheet->write($row_index, $c01, round($fixturing_per_sqm,0), $f_number);
			$c01++;

			if(array_key_exists("fixturing_per_sqm", $list_totals))
			{
				$list_totals["fixturing_per_sqm"] = $list_totals["fixturing_per_sqm"] + $fixturing_per_sqm;
			}
			else
			{
				$list_totals["fixturing_per_sqm"] = $fixturing_per_sqm;
			}


			//get architectural cost
			$architectural = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_type = 5";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$architectural = $row_s["total"] * $exchange_rate / $factor;
			}
			$sheet->write($row_index, $c01, round($architectural, 0), $f_number);
			$c01++;

			if(array_key_exists("architectural", $list_totals))
			{
				$list_totals["architectural"] = $list_totals["architectural"] + $architectural;
			}
			else
			{
				$list_totals["architectural"] = $architectural;
			}
			$total_cost = $total_cost + $architectural;

			if(array_key_exists("architectural",$list_totals_per_year[$year]))
			{
				$list_totals_per_year[$year]["architectural"] = $list_totals_per_year[$year]["architectural"] + $architectural;
			}
			else
			{
				$list_totals_per_year[$year]["architectural"] = $architectural;
			}

			if(array_key_exists("total_cost",$list_totals_per_year[$year]))
			{
				$list_totals_per_year[$year]["total_cost"] = $list_totals_per_year[$year]["total_cost"] + $architectural;
			}
			else
			{
				$list_totals_per_year[$year]["total_cost"] = $architectural;
			}


			//get equipment cost
			$architectural = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_type = 7";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$equipment = $row_s["total"] * $exchange_rate / $factor;
			}
			$sheet->write($row_index, $c01, round($equipment, 0), $f_number);
			$c01++;

			if(array_key_exists("equipment", $list_totals))
			{
				$list_totals["equipment"] = $list_totals["equipment"] + $equipment;
			}
			else
			{
				$list_totals["equipment"] = $equipment;
			}
			$total_cost = $total_cost + $equipment;


			if(array_key_exists("equipment",$list_totals_per_year[$year]))
			{
				$list_totals_per_year[$year]["equipment"] = $list_totals_per_year[$year]["equipment"] + $equipment;
			}
			else
			{
				$list_totals_per_year[$year]["equipment"] = $equipment;
			}

			if(array_key_exists("total_cost",$list_totals_per_year[$year]))
			{
				$list_totals_per_year[$year]["total_cost"] = $list_totals_per_year[$year]["total_cost"] + $equipment;
			}
			else
			{
				$list_totals_per_year[$year]["total_cost"] = $equipment;
			}

			//get other cost
			$other = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_type IN (11, 18, 19)";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$other = $row_s["total"] * $exchange_rate / $factor;
			}
			$sheet->write($row_index, $c01, round($other,0), $f_number);
			$c01++;

			if(array_key_exists("other", $list_totals))
			{
				$list_totals["other"] = $list_totals["other"] + $other;
			}
			else
			{
				$list_totals["other"] = $other;
			}
			$total_cost = $total_cost + $other;


			if(array_key_exists("other",$list_totals_per_year[$year]))
			{
				$list_totals_per_year[$year]["other"] = $list_totals_per_year[$year]["other"] + $other;
			}
			else
			{
				$list_totals_per_year[$year]["other"] = $other;
			}

			if(array_key_exists("total_cost",$list_totals_per_year[$year]))
			{
				$list_totals_per_year[$year]["total_cost"] = $list_totals_per_year[$year]["total_cost"] + $other;
			}
			else
			{
				$list_totals_per_year[$year]["total_cost"] = $other;
			}

			//get deposit cost
			$deposit = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_type = 9";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$deposit = ($row_s["total"] + $cer_basicdata["cer_basicdata_recoverable_keymoney"]) * $exchange_rate / $factor;
			}
			$sheet->write($row_index, $c01, round($deposit,0), $f_number);
			$c01++;

			if(array_key_exists("deposit", $list_totals))
			{
				$list_totals["deposit"] = $list_totals["deposit"] + $deposit;
			}
			else
			{
				$list_totals["deposit"] = $deposit;
			}
			$total_cost = $total_cost + $deposit;

			if(array_key_exists("deposit",$list_totals_per_year[$year]))
			{
				$list_totals_per_year[$year]["deposit"] = $list_totals_per_year[$year]["deposit"] + $deposit;
			}
			else
			{
				$list_totals_per_year[$year]["deposit"] = $deposit;
			}

			if(array_key_exists("total_cost",$list_totals_per_year[$year]))
			{
				$list_totals_per_year[$year]["total_cost"] = $list_totals_per_year[$year]["total_cost"] + $deposit;
			}
			else
			{
				$list_totals_per_year[$year]["total_cost"] = $deposit;
			}

			//get noncapitalized cost
			$noncapitalized = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_type = 13";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$noncapitalized = $row_s["total"] * $exchange_rate / $factor;
			}
			$sheet->write($row_index, $c01, round($noncapitalized,0), $f_number);
			$c01++;

			if(array_key_exists("noncapitalized", $list_totals))
			{
				$list_totals["noncapitalized"] = $list_totals["noncapitalized"] + $noncapitalized;
			}
			else
			{
				$list_totals["noncapitalized"] = $noncapitalized;
			}
			$total_cost = $total_cost + $noncapitalized;


			if(array_key_exists("noncapitalized",$list_totals_per_year[$year]))
			{
				$list_totals_per_year[$year]["noncapitalized"] = $list_totals_per_year[$year]["noncapitalized"] + $noncapitalized;
			}
			else
			{
				$list_totals_per_year[$year]["noncapitalized"] = $noncapitalized;
			}

			if(array_key_exists("total_cost",$list_totals_per_year[$year]))
			{
				$list_totals_per_year[$year]["total_cost"] = $list_totals_per_year[$year]["total_cost"] + $noncapitalized;
			}
			else
			{
				$list_totals_per_year[$year]["total_cost"] = $noncapitalized;
			}

			$sheet->write($row_index, $c01, round($total_cost,0), $f_number);
			$c01++;

			if(array_key_exists("total_cost", $list_totals))
			{
				$list_totals["total_cost"] = $list_totals["total_cost"] + $total_cost;
			}
			else
			{
				$list_totals["total_cost"] = $total_cost;
			}

			$total_cost_per_sqm = 0;
			if($total_space > 0)
			{
				$total_cost_per_sqm = round($total_cost / $total_space, 2);
			}
			$sheet->write($row_index, $c01, round($total_cost_per_sqm,0), $f_number);
			$c01++;

			if(array_key_exists("total_cost_per_sqm", $list_totals))
			{
				$list_totals["total_cost_per_sqm"] = $list_totals["total_cost_per_sqm"] + $total_cost_per_sqm;
			}
			else
			{
				$list_totals["total_cost_per_sqm"] = $total_cost_per_sqm;
			}


			$retail_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$retail_cost_per_sqm = round($total_cost / $retail_area, 2);
			}
			$sheet->write($row_index, $c01, round($retail_cost_per_sqm, 0), $f_number);
			$c01++;

			if(array_key_exists("total_cost_per_sqm_retail", $list_totals))
			{
				$list_totals["total_cost_per_sqm_retail"] = $list_totals["total_cost_per_sqm_retail"] + $retail_cost_per_sqm;
			}
			else
			{
				$list_totals["total_cost_per_sqm_retail"] = $retail_cost_per_sqm;
			}
			
			$c01 = 0;
			$row_index++;
		}

	}
}


$listtotal_rowindex = $row_index;
//list totals for investments
if(count($posorders) > 0)
{
	$c01 = 8;
	$row_index++;
	$sheet->write($row_index, $c01,"Total for all projects", $f_normal);
	$c01++;
	$sheet->write($row_index, $c01,"", $f_normal);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$sheet->write($row_index, $c01,round($list_totals["total_space"], 0), $f_number);
	$c01++;

	$sheet->write($row_index, $c01,round($list_totals["retail_area"], 0), $f_number);
	$c01++;

	$sheet->write($row_index, $c01,round($list_totals["intagibles"], 0), $f_number);
	$c01++;

	$sheet->write($row_index, $c01,round($list_totals["construction"], 0), $f_number);
	$c01++;
	$sheet->write($row_index, $c01,round($list_totals["construction_per_sqm"], 0), $f_number);
	$c01++;

	$sheet->write($row_index, $c01,round($list_totals["fixturing"], 0), $f_number);
	$c01++;
	$sheet->write($row_index, $c01,round($list_totals["fixturing_per_sqm"],0), $f_number);
	$c01++;

	$sheet->write($row_index, $c01,round($list_totals["architectural"], 0), $f_number);
	$c01++;

	$sheet->write($row_index, $c01,round($list_totals["equipment"], 0), $f_number);
	$c01++;

	$sheet->write($row_index, $c01,round($list_totals["other"], 0), $f_number);
	$c01++;

	$sheet->write($row_index, $c01,round($list_totals["deposit"], 0), $f_number);
	$c01++;
	$sheet->write($row_index, $c01,round($list_totals["noncapitalized"], 0), $f_number);
	$c01++;
	$sheet->write($row_index, $c01,round($list_totals["total_cost"], 0), $f_number);
	$c01++;

	
	$sheet->write($row_index, $c01,round($list_totals["total_cost_per_sqm"], 0), $f_number);
	$c01++;

	$sheet->write($row_index, $c01,round($list_totals["total_cost_per_sqm_retail"], 0), $f_number);
	$c01++;


	$c01 = 8;
	$row_index++;
	$sheet->write($row_index, $c01,"Average per project", $f_normal);
	$c01++;
	$sheet->write($row_index, $c01,"", $f_normal);
	$c01++;


	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["lease_period"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;


	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["total_space"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;


	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["retail_area"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;


	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["intagibles"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["construction"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["construction_per_sqm"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;


	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["fixturing"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["fixturing_per_sqm"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["architectural"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["equipment"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["other"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["deposit"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["noncapitalized"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["total_cost"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["total_cost_per_sqm"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["total_cost_per_sqm_retail"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;



	$c01 = 8;
	$row_index++;
	$row_index++;
	$sheet->write($row_index, $c01,"Average per sqm", $f_normal);
	$c01++;
	$sheet->write($row_index, $c01,"", $f_normal);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$avg = 0;
	if($list_totals["total_space"] > 0)
	{
		$avg = round($list_totals["intagibles"]/$list_totals["total_space"], 0);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["total_space"] > 0)
	{
		$avg = round($list_totals["construction"]/$list_totals["total_space"], 0);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$avg = 0;
	if($list_totals["total_space"] > 0)
	{
		$avg = round($list_totals["fixturing"]/$list_totals["total_space"], 0);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$avg = 0;
	if($list_totals["total_space"] > 0)
	{
		$avg = round($list_totals["architectural"]/$list_totals["total_space"], 0);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["total_space"] > 0)
	{
		$avg = round($list_totals["equipment"]/$list_totals["total_space"], 0);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;


	$avg = 0;
	if($list_totals["total_space"] > 0)
	{
		$avg = round($list_totals["other"]/$list_totals["total_space"], 0);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["total_space"] > 0)
	{
		$avg = round($list_totals["deposit"]/$list_totals["total_space"], 0);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["total_space"] > 0)
	{
		$avg = round($list_totals["noncapitalized"]/$list_totals["total_space"], 0);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["total_space"] > 0)
	{
		$avg = round($list_totals["total_cost"]/$list_totals["total_space"], 0);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;




	$c01 = 8;
	$row_index++;
	$sheet->write($row_index, $c01,"Average per sqm sales area", $f_normal);
	$c01++;
	$sheet->write($row_index, $c01,"", $f_normal);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["intagibles"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["construction"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,round($avg,0), $f_number);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["fixturing"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["architectural"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["equipment"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;


	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["other"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,round($avg,0), $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["deposit"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["noncapitalized"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["total_cost"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,round($avg, 0), $f_number);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	ksort($list_totals_per_year);

	if(count($list_totals_per_year) > 1) {
		$row_index++;

		foreach($list_totals_per_year as $year=>$data) {
			
			$c01 = 8;
			$row_index++;
			$sheet->write($row_index, $c01,"Average per sqm " . $year, $f_normal);
			$c01++;
			$sheet->write($row_index, $c01,"", $f_normal);
			$c01++;

			$sheet->write($row_index, $c01,"", $f_unused);
			$c01++;

			$sheet->write($row_index, $c01,"", $f_unused);
			$c01++;

			$sheet->write($row_index, $c01,"", $f_unused);
			$c01++;

			$avg = 0;
			if($data["total_space"] > 0)
			{
				$avg = round($data["intagibles"]/$data["total_space"], 0);
			}
			$sheet->write($row_index, $c01,round($avg, 0), $f_number);
			$c01++;

			$avg = 0;
			if($data["total_space"] > 0)
			{
				$avg = round($data["construction"]/$data["total_space"], 0);
			}
			$sheet->write($row_index, $c01,round($avg, 0), $f_number);
			$c01++;

			$sheet->write($row_index, $c01,"", $f_unused);
			$c01++;

			$avg = 0;
			if($data["total_space"] > 0)
			{
				$avg = round($data["fixturing"]/$data["total_space"], 0);
			}
			$sheet->write($row_index, $c01,round($avg, 0), $f_number);
			$c01++;

			$sheet->write($row_index, $c01,"", $f_unused);
			$c01++;

			$avg = 0;
			if($data["total_space"] > 0)
			{
				$avg = round($data["architectural"]/$data["total_space"], 0);
			}
			$sheet->write($row_index, $c01,round($avg,0), $f_number);
			$c01++;

			$avg = 0;
			if($data["total_space"] > 0)
			{
				$avg = round($data["equipment"]/$data["total_space"], 0);
			}
			$sheet->write($row_index, $c01,round($avg, 0), $f_number);
			$c01++;


			$avg = 0;
			if($data["total_space"] > 0)
			{
				$avg = round($data["other"]/$data["total_space"], 0);
			}
			$sheet->write($row_index, $c01,round($avg, 0), $f_number);
			$c01++;

			$avg = 0;
			if($data["total_space"] > 0)
			{
				$avg = round($data["deposit"]/$data["total_space"], 0);
			}
			$sheet->write($row_index, $c01,round($avg, 0), $f_number);
			$c01++;

			$avg = 0;
			if($data["total_space"] > 0)
			{
				$avg = round($data["noncapitalized"]/$data["total_space"], 0);
			}
			$sheet->write($row_index, $c01,round($avg, 0), $f_number);
			$c01++;

			$avg = 0;
			if($data["total_space"] > 0)
			{
				$avg = round($data["total_cost"]/$data["total_space"], 0);
			}
			$sheet->write($row_index, $c01,round($avg, 0), $f_number);
			$c01++;

			$sheet->write($row_index, $c01,"", $f_unused);
			$c01++;

			$sheet->write($row_index, $c01,"", $f_unused);
			$c01++;

		}

		$row_index++;

		foreach($list_totals_per_year as $year=>$data) {
			
			$c01 = 8;
			$row_index++;
			$sheet->write($row_index, $c01,"Av. per sqm sales area " . $year, $f_normal);
			$c01++;
			$sheet->write($row_index, $c01,"", $f_normal);
			$c01++;

			$sheet->write($row_index, $c01,"", $f_unused);
			$c01++;

			$sheet->write($row_index, $c01,"", $f_unused);
			$c01++;

			$sheet->write($row_index, $c01,"", $f_unused);
			$c01++;

			$avg = 0;
			if($data["retail_area"] > 0)
			{
				$avg = round($data["intagibles"]/$data["retail_area"], 0);
			}
			$sheet->write($row_index, $c01,round($avg, 0), $f_number);
			$c01++;

			$avg = 0;
			if($data["retail_area"] > 0)
			{
				$avg = round($data["construction"]/$data["retail_area"], 0);
			}
			$sheet->write($row_index, $c01,round($avg, 0), $f_number);
			$c01++;

			$sheet->write($row_index, $c01,"", $f_unused);
			$c01++;

			$avg = 0;
			if($data["retail_area"] > 0)
			{
				$avg = round($data["fixturing"]/$data["retail_area"], 0);
			}
			$sheet->write($row_index, $c01,round($avg, 0), $f_number);
			$c01++;

			$sheet->write($row_index, $c01,"", $f_unused);
			$c01++;

			$avg = 0;
			if($data["retail_area"] > 0)
			{
				$avg = round($data["architectural"]/$data["retail_area"], 0);
			}
			$sheet->write($row_index, $c01,round($avg, 0), $f_number);
			$c01++;

			$avg = 0;
			if($data["retail_area"] > 0)
			{
				$avg = round($data["equipment"]/$data["retail_area"], 0);
			}
			$sheet->write($row_index, $c01,round($avg, 0), $f_number);
			$c01++;


			$avg = 0;
			if($data["retail_area"] > 0)
			{
				$avg = round($data["other"]/$data["retail_area"], 0);
			}
			$sheet->write($row_index, $c01,round($avg, 0), $f_number);
			$c01++;

			$avg = 0;
			if($data["retail_area"] > 0)
			{
				$avg = round($data["deposit"]/$data["retail_area"], 0);
			}
			$sheet->write($row_index, $c01,round($avg, 0), $f_number);
			$c01++;

			$avg = 0;
			if($data["retail_area"] > 0)
			{
				$avg = round($data["noncapitalized"]/$data["retail_area"], 0);
			}
			$sheet->write($row_index, $c01,round($avg, 0), $f_number);
			$c01++;

			$avg = 0;
			if($data["retail_area"] > 0)
			{
				$avg = round($data["total_cost"]/$data["retail_area"], 0);
			}
			$sheet->write($row_index, $c01,round($avg, 0), $f_number);
			$c01++;

			$sheet->write($row_index, $c01,"", $f_unused);
			$c01++;

			$sheet->write($row_index, $c01,"", $f_unused);
			$c01++;

		}
	}


}

$xls->close(); 

?>