<?php
/********************************************************************

    cer_application_lease.php

    Application Form: lease information
    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-08-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-08-14
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";

check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}

/********************************************************************
    prepare all data needed
*********************************************************************/
$client_address = get_address($project["order_client_address"]);
$posdata = get_pos_data($project["project_order"]);

$currency = get_cer_currency(param("pid"));
$posleases = get_pos_leasedata($posdata["posaddress_id"], $project["project_order"]);


//build missing investment records
$result = build_missing_cer_investment_records(param("pid"));


$yes_no = array();
$yes_no[0] = "No";
$yes_no[1] = "Yes";


//calculate annual rent

$average_annual_rent = "";
$rental_duration = "";

$old_lease_start_date = '';
$old_lease_end_date = '';


if(count($posleases) > 0)
{
	if($posleases["poslease_startdate"] != NULL 
		and $posleases["poslease_startdate"] != '0000-00-00' 
		and $posleases["poslease_enddate"] != NULL
		and $posleases["poslease_enddate"] != '0000-00-00')
	{
		$diff = abs(strtotime($posleases["poslease_enddate"]) - strtotime($posleases["poslease_startdate"]));
		$years = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));

		if($months == 12)
		{
			$months = 0;
			$years++;
		}

		$rental_duration = $years . " years and " . $months . " months";

		$duration_in_years = $years + ($months/12);
	}
	
}

$sql_pos = "select posaddress_id, " . 
           " concat(posaddress_place, ', ', posaddress_name, ' - ', product_line_name) as posname " . 
           "from posaddresses " .
		   "left join countries on country_id = posaddress_country " .
		   "left join product_lines on product_line_id = posaddress_store_furniture " . 
		   "where posaddress_country = " . $client_address["country"] . 
		   " and posaddress_store_postype = " . $project["project_postype"] .
		   " order by posaddress_place, posaddress_name";


$sql_pos_locations = "select posaddress_id, " . 
           " concat(posaddress_place, ', ', posaddress_name) as posname " . 
           "from posaddresses " .
		   "left join countries on country_id = posaddress_country " .
		   "left join product_lines on product_line_id = posaddress_store_furniture " . 
		   "where posaddress_country = " . $client_address["country"] . 
		   " and posaddress_ownertype in (2,6) " .
		   " order by posaddress_place, posaddress_name";


//check if LN is locked and LN is submitted and LN is not rejected and LN is not approved
$can_edit_business_plan = true;
if($ln_basicdata["ln_basicdata_locked"] == 1
   and ($cer_basicdata["cer_basicdata_submitted"] == NULL or $cer_basicdata["cer_basicdata_submitted"] == '0000-00-00')
   and ($ln_basicdata["ln_basicdata_resubmitted"] != NULL and $ln_basicdata["ln_basicdata_resubmitted"] != '0000-00-00'))

{
	//check if ln was approved
	$milestone = get_project_milestone($cer_basicdata["cer_basicdata_project"], 13);
	if(count($milestone) > 0 and ($milestone['project_milestone_date'] == NULL or $milestone['project_milestone_date'] == '0000-00-00'))
	{
		$can_edit_business_plan = false;
	}
	
}
elseif($ln_basicdata["ln_basicdata_locked"] == 1
   and ($cer_basicdata["cer_basicdata_submitted"] == NULL or $cer_basicdata["cer_basicdata_submitted"] == '0000-00-00')
   and ($ln_basicdata["ln_basicdata_rejected"] == NULL or $ln_basicdata["ln_basicdata_rejected"] == '0000-00-00'))

{
	//check if ln was approved
	$milestone = get_project_milestone($cer_basicdata["cer_basicdata_project"], 13);
	if(count($milestone) > 0 and ($milestone['project_milestone_date'] == NULL or $milestone['project_milestone_date'] == '0000-00-00'))
	{
		$can_edit_business_plan = false;
	}
	
}

if(count($posleases) > 0)
{
	$old_lease_start_date = $posleases["poslease_startdate"];
	$old_lease_end_date = $posleases["poslease_enddate"];
}

//get possellouts
//count years in sellouts
if($project["project_relocated_posaddress_id"] > 0) {
	$sql = "select max(possellout_year) as last_year from possellouts 
			where possellout_posaddress_id = " .  dbquote($project["project_relocated_posaddress_id"]);
}
else {
	$sql = "select max(possellout_year) as last_year from possellouts 
			where possellout_posaddress_id = " .  dbquote($project["posaddress_id"]);
}
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);
$last_year = $row['last_year'];
$previous_year = $cer_basicdata["cer_basicdata_firstyear"] - 1;

if (!$last_year) {
	$last_year = date('Y') - 1;
}

$sellouts = array();
if($previous_year > date('Y')) {
	$previous_year = date('Y');
}
elseif($cer_basicdata["cer_basicdata_firstyear"] == date('Y')) {
	$previous_year = date('Y');
}

if(!$last_year) {
	$year1 = "";
	$year2 = "";
	$year3 = "";
}
elseif($last_year == $previous_year) {
	$year1 = $previous_year;
	$year2 = $previous_year - 1;
	$year3 = $previous_year - 2;
}
elseif($last_year and $last_year < $previous_year) {
	$year1 = $previous_year;
	$year2 = $last_year;
	$year3 = $last_year - 1;
}
elseif($last_year > $previous_year) {
	$year1 = $previous_year;
	$year2 = $previous_year -1;
	$year3 = $previous_year - 2;
}
else {
	$year1 = $previous_year;
	$year2 = $previous_year - 1;
	$year3 = $previous_year - 2;
}


if($project['project_relocated_posaddress_id'] > 0) {
	$sql = "select * from possellouts 
			where possellout_posaddress_id = " .  dbquote($project["project_relocated_posaddress_id"]) . 
			" and possellout_year in ('" . $year1 . "', '" . $year2 . "', '" . $year3 ."')
			order by possellout_year desc ";
}
else {
	$sql = "select * from possellouts 
			where possellout_posaddress_id = " .  dbquote($project["posaddress_id"]) . 
			" and possellout_year in ('" . $year1 . "', '" . $year2 . "', '" . $year3 ."')
			order by possellout_year desc ";
}

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$sellouts[$row['possellout_year']] = $row;
}

//add missing years
if(!array_key_exists($year1, $sellouts)) {
	$sellouts[$year1] = array("possellout_month"=>"", "possellout_watches_units"=>"", "possellout_watches_grossales"=>"", "possellout_bijoux_units"=>"", "possellout_bijoux_grossales"=>"", "possellout_net_sales"=>"", "possellout_grossmargin"=>"", "possellout_operating_expenses"=>"", "possellout_operating_income_excl"=>"", "possellout_wholsale_margin"=>"", "possellout_operating_income_incl"=>"");
}
if(!array_key_exists($year2, $sellouts)) {
	$sellouts[$year2] = array("possellout_month"=>"", "possellout_watches_units"=>"", "possellout_watches_grossales"=>"", "possellout_bijoux_units"=>"", "possellout_bijoux_grossales"=>"", "possellout_net_sales"=>"", "possellout_grossmargin"=>"", "possellout_operating_expenses"=>"", "possellout_operating_income_excl"=>"", "possellout_wholsale_margin"=>"", "possellout_operating_income_incl"=>"");
}
if(!array_key_exists($year3, $sellouts)) {
	$sellouts[$year3] = array("possellout_month"=>"", "possellout_watches_units"=>"", "possellout_watches_grossales"=>"", "possellout_bijoux_units"=>"", "possellout_bijoux_grossales"=>"", "possellout_net_sales"=>"", "possellout_grossmargin"=>"", "possellout_operating_expenses"=>"", "possellout_operating_income_excl"=>"", "possellout_wholsale_margin"=>"", "possellout_operating_income_incl"=>"");
}

krsort($sellouts);
//echo '<pre>';
//print_r($sellouts);die;
/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

include("include/project_head.php");

$form->add_hidden("pid", param("pid"));



if(($can_edit_business_plan == true 
	and $project["order_actual_order_state_code"] < '820' 
	and $cer_basicdata["cer_basicdata_cer_locked"] == 0 
	and has_access("has_access_to_his_cer") 
	and $project["project_state"] != 2) or (has_access("has_full_access_to_cer") 
	and $project["project_state"] != 2))
{
	$form->add_section("Benchmark Info");
	$form->add_comment("Please indicate an existing POS similar to this one in case there is any.");
	$form->add_list("posaddress_best_benchmark_pos", "Similar POS Location", $sql_pos, 0, $posdata["posaddress_best_benchmark_pos"]);
	

	if(count($posleases) > 0)
	{
				
		$form->add_section("Lease Details");
		$form->add_comment("There is a contract in the system for this POS location as follows:");
		$form->add_list("poslease_lease_type", "Lease Type*",
				"select poslease_type_id, poslease_type_name from poslease_types order by poslease_type_id", NOTNULL, $posleases["poslease_lease_type"]);
		$form->add_edit("poslease_startdate", "Start Date*", NOTNULL, to_system_date($posleases["poslease_startdate"]), TYPE_DATE, 10);
		$form->add_edit("poslease_enddate", "Expiry Date*", NOTNULL, to_system_date($posleases["poslease_enddate"]), TYPE_DATE, 10);
		$form->add_label("rental_duration", "Rental Duration", 0, $rental_duration);
		
		$form->add_list("poslease_hasfixrent", "Contract contains fixed rent*", $yes_no, 0, $posleases["poslease_hasfixrent"]);

		$form->add_checkbox("poslease_isindexed", "contains tacit renewal clause", $posleases["poslease_isindexed"], "", "Contract");

		$form->add_checkbox("poslease_indexclause_in_contract", "Index Clause in Contract", $posleases["poslease_indexclause_in_contract"], "", "Index");
		$form->add_edit("poslease_indexrate", "Average Index Rate in %", 0, $posleases["poslease_indexrate"], TYPE_DECIMAL, 6, 2);
		$form->add_edit("poslease_average_increase", "Average yearly Increase in %", 0, $posleases["poslease_average_increase"], TYPE_DECIMAL, 12, 2);

		
		$form->add_section("Lease Options");
		$form->add_edit("poslease_extensionoption", "Extension Option to Date", 0, to_system_date($posleases["poslease_extensionoption"]), TYPE_DATE, 10, "", 1, "extenstion_option_info");
		$form->add_edit("poslease_exitoption", "Exit Option to Date", 0, to_system_date($posleases["poslease_exitoption"]), TYPE_DATE, 10, "", 1, "exit_option_info");
		$form->add_edit("poslease_termination_time", "Termination deadline*", NOTNULL, $posleases["poslease_termination_time"], TYPE_INT, 8, 0, 1, "termination");
		
		if($project["project_projectkind"] != 2 and $project["project_projectkind"] !=7)
		{
			$form->add_edit("cer_basicdata_deadline_property", "Deadline for property*", NOTNULL, to_system_date($cer_basicdata["cer_basicdata_deadline_property"]), TYPE_DATE, 10, "", 1, "deadline_for_property_info");
		}
		else
		{
			$form->add_edit("cer_basicdata_deadline_property", "Deadline for property", 0, to_system_date($cer_basicdata["cer_basicdata_deadline_property"]), TYPE_DATE, 10, "", 1, "deadline_for_property_info");
		}

		$form->add_edit("poslease_handoverdate", "Handover Date (Key)", 0, to_system_date($posleases["poslease_handoverdate"]), TYPE_DATE, 10);
		$form->add_edit("poslease_firstrentpayed", "First Rent Payed on", 0, to_system_date($posleases["poslease_firstrentpayed"]), TYPE_DATE, 10);

		$form->add_edit("poslease_freeweeks", "Rent Free Period in Weeks*", 0, $posleases["poslease_freeweeks"], TYPE_INT, 6);
		
		$form->add_section("Negotiation Details");
		$form->add_edit("poslease_negotiator", "Negotiator of Key Money and Lease*", NOTNULL, $posleases["poslease_negotiator"]);
		$form->add_multiline("poslease_landlord_name", "Landlord (Negotiation Partner)*", 4, NOTNULL, $posleases["poslease_landlord_name"]);
		
		$form->add_comment("Please indicate detailed conditions as stipulated in your contract for fixed rent - per measurement unit (m2/sqft) <br />per month or year or, if not specified, total amount.<br />
As well mention turnover percentages, additional rental cost, index rate, yearly increases.
");
		$form->add_multiline("poslease_negotiated_conditions", "Negotiated Rental Conditions*", 8, NOTNULL, $posleases["poslease_negotiated_conditions"], 1, "condition_info");
	}
	else
	{
		$form->add_section("Lease Details");
		$form->add_list("poslease_lease_type", "Lease Type*",
				"select poslease_type_id, poslease_type_name from poslease_types order by poslease_type_id", NOTNULL);
		
		$form->add_edit("poslease_startdate", "Start Date*", NOTNULL, "", TYPE_DATE, 10);
		$form->add_edit("poslease_enddate", "Expiry Date*", NOTNULL, "", TYPE_DATE, 10);
		
		$form->add_list("poslease_hasfixrent", "Contract contains fixed rent*", $yes_no, 0, 1);

		$form->add_checkbox("poslease_isindexed", "contains tacit renewal clause", "", "", "Contract");
		$form->add_checkbox("poslease_indexclause_in_contract", "Index Clause in Contract", "", "", "Index");
		$form->add_label("poslease_indexrate", "Average Index Rate in %", 0, "", TYPE_DECIMAL, 6, 2);
		$form->add_label("poslease_average_increase", "Average yearly Increase in %");
		

		$form->add_section("Lease Options");
		$form->add_edit("poslease_extensionoption", "Extension Option to Date", 0, "", TYPE_DATE, 10, "", 1, "extenstion_option_info");
		$form->add_edit("poslease_exitoption", "Exit Option to Date", 0, "", TYPE_DATE, 10, "", 1, "exit_option_info");
		$form->add_edit("poslease_termination_time", "Termination deadline*", NOTNULL, "", TYPE_INT, 10, 0, 1, "termination");

		
		if($project["project_projectkind"] != 2 and $project["project_projectkind"] !=7)
		{
			$form->add_edit("cer_basicdata_deadline_property", "Deadline for property*", NOTNULL, "", TYPE_DATE, 10, "", 1, "deadline_for_property_info");
		}
		else
		{
			$form->add_edit("cer_basicdata_deadline_property", "Deadline for property", 0, "", TYPE_DATE, 10, "", 1, "deadline_for_property_info");
		}


		$form->add_edit("poslease_handoverdate", "Handover Date (Key)", 0, "", TYPE_DATE, 10);
		$form->add_edit("poslease_firstrentpayed", "First Rent Payed on", 0, "", TYPE_DATE, 10);
		$form->add_edit("poslease_freeweeks", "Rent Free Period in Weeks*", 0, "", TYPE_INT, 6);
				
		$form->add_section("Negotiation Details");
		$form->add_edit("poslease_negotiator", "Negotiator of Key Money and Lease*", NOTNULL, "");
		$form->add_multiline("poslease_landlord_name", "Landlord (Negotiation Partner)*", 4, NOTNULL, "");

		$form->add_comment("Please indicate detailed conditions as stipulated in your contract for fixed rent - per measurement unit (m2/sqft) <br />per month or year or, if not specified, total amount.<br />
		As well mention turnover percentages, additional rental cost, index rate, yearly increases.
		");
		$form->add_multiline("poslease_negotiated_conditions", "Negotiated Rental Conditions*", 8, NOTNULL, "", 1, "condition_info");

		

	}

	if($project['project_projectkind'] == 5
		or $project['project_projectkind'] == 2
		or $project['project_projectkind'] == 3
		or $project['project_projectkind'] == 6) {
		$form->add_section("Former Lease Conditions");
		$form->add_comment("Please give us some information about the former lease conditions.");

		if($project['project_projectkind'] == 3) {
			$form->add_list("ln_basicdata_former_pos_id", "Indicate the former POS location", $sql_pos_locations, 0, $ln_basicdata["ln_basicdata_former_pos_id"]);
		}
		else {
			$form->add_hidden("ln_basicdata_former_pos_id", 0);
		}
		
		//$form->add_edit("ln_basicdata_pos_opening_date", "When was this POS opened?*", NOTNULL, $ln_basicdata["ln_basicdata_pos_opening_date"], TYPE_DATE, 10);
		
		$form->add_edit("ln_basicdata_pos_last_renovation_date", "When was this POS renovated for the last time?", 0, to_system_date($ln_basicdata["ln_basicdata_pos_last_renovation_date"]), TYPE_DATE, 10);

		if($project['project_projectkind'] == 5
		or $project['project_projectkind'] == 3
			or $project['project_projectkind'] == 6) {
			$form->add_edit("ln_basicdata_last_fixed_rent", "What was the last yearly fixed rent paid in " . $currency['symbol'] . "?*", NOTNULL, $ln_basicdata["ln_basicdata_last_fixed_rent"], TYPE_INT, 12);
		}
		else {
			$form->add_edit("ln_basicdata_last_fixed_rent", "What was the last yearly fixed rent paid in " . $currency['symbol'] . "?", 0, $ln_basicdata["ln_basicdata_last_fixed_rent"], TYPE_INT, 12);
		}

		$form->add_edit("ln_basicdata_last_additional_rent", "What were the last yearly additional rental costs paid in " . $currency['symbol'] . "?", 0, $ln_basicdata["ln_basicdata_last_additional_rent"], TYPE_INT, 12);

		$form->add_edit("ln_basicdata_last_tob_rent", "What was the turnover based percentage in the lease contract?", 0, $ln_basicdata["ln_basicdata_last_tob_rent"], TYPE_DECIMAL, 5,2);

		if($project['project_projectkind'] == 5
		or $project['project_projectkind'] == 3
			or $project['project_projectkind'] == 6) {
			$form->add_edit("ln_basicdata_last_fixed_rent", "What was the last yearly fixed rent paid in " . $currency['symbol'] . "?*", NOTNULL, $ln_basicdata["ln_basicdata_last_fixed_rent"], TYPE_INT, 12);

			$form->add_multiline("ln_basicdata_changes_in_enviroment", "Describe the changes in the environment since the POS opened*", 8, NOTNULL, $ln_basicdata["ln_basicdata_changes_in_enviroment"]);
		}
		else {
			$form->add_multiline("ln_basicdata_changes_in_enviroment", "Describe the changes in the environment since the POS opened*", 8, 0, $ln_basicdata["ln_basicdata_changes_in_enviroment"]);
		}

		$form->add_section("Profitability");
		$form->add_comment("<strong>What was the profitability of this POS in " . $year1 . "?</strong>");
		$form->add_edit("possellout_month1", "Number of months selling in " . $year1, NOTNULL, $sellouts[$year1]['possellout_month'], TYPE_INT, 3);
		$form->add_edit("possellout_watches_units1", "Watches Units sold", NOTNULL, $sellouts[$year1]['possellout_watches_units'], TYPE_INT, 12);
		$form->add_edit("possellout_watches_grossales1", "Watches Gross Sales", NOTNULL, $sellouts[$year1]['possellout_watches_grossales'], TYPE_INT, 12);
		$form->add_edit("possellout_bijoux_units1", "Bijoux Units sold", 0, $sellouts[$year1]['possellout_bijoux_units'], TYPE_INT, 12);
		$form->add_edit("possellout_bijoux_grossales1", "Bijoux Gross Sales", 0, $sellouts[$year1]['possellout_bijoux_grossales'], TYPE_INT, 12);

		if($project['project_projectkind'] != 3
			and $project['project_projectkind'] != 4) {
			$form->add_edit("possellout_net_sales1", "Net Sales", NOTNULL, $sellouts[$year1]['possellout_net_sales'], TYPE_INT, 12);

			$form->add_edit("possellout_grossmargin1", "Gross Margin", NOTNULL, $sellouts[$year1]['possellout_grossmargin'], TYPE_INT, 12);
			$form->add_edit("possellout_operating_expenses1", "Indirect operating expenses", NOTNULL, $sellouts[$year1]['possellout_operating_expenses'], TYPE_INT, 12);
			$form->add_edit("possellout_operating_income_excl1", "Operating Income", NOTNULL, $sellouts[$year1]['possellout_operating_income_excl'], TYPE_INT, 12);
			$form->add_edit("possellout_wholsale_margin1", "Wholesale margin (%)", NOTNULL, $sellouts[$year1]['possellout_wholsale_margin'], TYPE_DECIMAL, 6, 2);
			$form->add_edit("possellout_operating_income_incl1", "Operating income incl. Whole Sale Margin", NOTNULL, $sellouts[$year1]['possellout_operating_income_incl'], TYPE_INT, 12);
		}
		else {
			$form->add_hidden("possellout_net_sales1");

			$form->add_hidden("possellout_grossmargin1");
			$form->add_hidden("possellout_operating_expenses1");
			$form->add_hidden("possellout_operating_income_excl1");
			$form->add_hidden("possellout_wholsale_margin1");
			$form->add_hidden("possellout_operating_income_incl1");

		}
		


		$form->add_comment("<strong>What was the profitability of this POS in " . $year2 . "?</strong>");
		$form->add_edit("possellout_month2", "Number of months selling in " . $year2, NOTNULL, $sellouts[$year2]['possellout_month'], TYPE_INT, 3);
		$form->add_edit("possellout_watches_units2", "Watches Units sold", NOTNULL, $sellouts[$year2]['possellout_watches_units'], TYPE_INT, 12);
		$form->add_edit("possellout_watches_grossales2", "Watches Gross Sales", NOTNULL, $sellouts[$year2]['possellout_watches_grossales'], TYPE_INT, 12);
		$form->add_edit("possellout_bijoux_units2", "Bijoux Units sold", 0, $sellouts[$year2]['possellout_bijoux_units'], TYPE_INT, 12);
		$form->add_edit("possellout_bijoux_grossales2", "Bijoux Gross Sales", 0, $sellouts[$year2]['possellout_bijoux_grossales'], TYPE_INT, 12);

		
		if($project['project_projectkind'] != 3
			and $project['project_projectkind'] != 4) {
			$form->add_edit("possellout_net_sales2", "Net Sales", NOTNULL, $sellouts[$year2]['possellout_net_sales'], TYPE_INT, 12);

			$form->add_edit("possellout_grossmargin2", "Gross Margin", NOTNULL, $sellouts[$year2]['possellout_grossmargin'], TYPE_INT, 12);
			$form->add_edit("possellout_operating_expenses2", "Indirect operating expenses", NOTNULL, $sellouts[$year2]['possellout_operating_expenses'], TYPE_INT, 12);
			$form->add_edit("possellout_operating_income_excl2", "Operating Income", NOTNULL, $sellouts[$year2]['possellout_operating_income_excl'], TYPE_INT, 12);
			$form->add_edit("possellout_wholsale_margin2", "Wholesale margin (%)", NOTNULL, $sellouts[$year2]['possellout_wholsale_margin'], TYPE_DECIMAL, 6, 2);
			$form->add_edit("possellout_operating_income_incl2", "Operating income incl. Whole Sale Margin", NOTNULL, $sellouts[$year2]['possellout_operating_income_incl'], TYPE_INT, 12);
		}
		else {
			$form->add_hidden("possellout_net_sales2");
			$form->add_hidden("possellout_grossmargin2");
			$form->add_hidden("possellout_operating_expenses2");
			$form->add_hidden("possellout_operating_income_excl2");
			$form->add_hidden("possellout_wholsale_margin2");
			$form->add_hidden("possellout_operating_income_incl2");
		}


		$form->add_comment("<strong>What was the profitability of this POS in " . $year3 . "?</strong>");
		$form->add_edit("possellout_month3", "Number of months selling in " . $year3, NOTNULL, $sellouts[$year3]['possellout_month'], TYPE_INT, 3);
		$form->add_edit("possellout_watches_units3", "Watches Units sold", NOTNULL, $sellouts[$year3]['possellout_watches_units'], TYPE_INT, 12);
		$form->add_edit("possellout_watches_grossales3", "Watches Gross Sales", NOTNULL, $sellouts[$year3]['possellout_watches_grossales'], TYPE_INT, 12);
		$form->add_edit("possellout_bijoux_units3", "Bijoux Units sold", 0, $sellouts[$year3]['possellout_bijoux_units'], TYPE_INT, 12);
		$form->add_edit("possellout_bijoux_grossales3", "Bijoux Gross Sales", 0, $sellouts[$year3]['possellout_bijoux_grossales'], TYPE_INT, 12);

		
		if($project['project_projectkind'] != 3
			and $project['project_projectkind'] != 4) {
			$form->add_edit("possellout_net_sales3", "Net Sales", NOTNULL, $sellouts[$year3]['possellout_net_sales'], TYPE_INT, 12);

			$form->add_edit("possellout_grossmargin3", "Gross Margin", NOTNULL, $sellouts[$year3]['possellout_grossmargin'], TYPE_INT, 12);
			$form->add_edit("possellout_operating_expenses3", "Indirect operating expenses", NOTNULL, $sellouts[$year3]['possellout_operating_expenses'], TYPE_INT, 12);
			$form->add_edit("possellout_operating_income_excl3", "Operating Income", NOTNULL, $sellouts[$year3]['possellout_operating_income_excl'], TYPE_INT, 12);
			$form->add_edit("possellout_wholsale_margin3", "Wholesale margin (%)", NOTNULL, $sellouts[$year3]['possellout_wholsale_margin'], TYPE_DECIMAL, 6, 2);
			$form->add_edit("possellout_operating_income_incl3", "Operating income incl. Whole Sale Margin", NOTNULL, $sellouts[$year3]['possellout_operating_income_incl'], TYPE_INT, 12);
		}
		else {
			$form->add_hidden("possellout_net_sales3");
			$form->add_hidden("possellout_grossmargin3");
			$form->add_hidden("possellout_operating_expenses3");
			$form->add_hidden("possellout_operating_income_excl3");
			$form->add_hidden("possellout_wholsale_margin3");
			$form->add_hidden("possellout_operating_income_incl3");
		}
		
		
	}

	$form->add_button("form_save", "Save Data");
}
else
{
	$form->add_section("Benchmark Info");

	$pos_name = "concat(posaddress_name, ', ', posaddress_place)";
	
	if($posdata["posaddress_best_benchmark_pos"] > 0)
	{
		$form->add_lookup("similar_pos", "Similar POS Location", "posaddresses", $pos_name, 0, $posdata["posaddress_best_benchmark_pos"]);
	}
	else
	{
		$form->add_label("similar_pos", "Similar POS Location");
	}


	if(count($posleases) > 0)
	{
		
		$rental_duration = "";
		if($posleases["poslease_startdate"] != NULL 
			and $posleases["poslease_startdate"] != '0000-00-00' 
			and $posleases["poslease_enddate"] != NULL
			and $posleases["poslease_enddate"] != '0000-00-00')
		{
			$diff = abs(strtotime($posleases["poslease_enddate"]) - strtotime($posleases["poslease_startdate"]));
			$years = floor($diff / (365*60*60*24));
			$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));

			if($months == 12)
			{
				$months = 0;
				$years++;
			}

			$rental_duration = $years . " years and " . $months . " months";


		}
		
		
		$form->add_section("Lease Details");
		$form->add_comment("There is a contract in the system for this POS location as follows:");
		$form->add_lookup("poslease_lease_type", "Lease Type", "poslease_types", "poslease_type_name", 0, $posleases["poslease_lease_type"]);
		
		$form->add_label("poslease_startdate", "Start Date", 0, to_system_date($posleases["poslease_startdate"]));
		$form->add_label("poslease_enddate", "Expiry Date", 0, to_system_date($posleases["poslease_enddate"]));
		$form->add_label("rental_duration", "Rental Duration", 0, $rental_duration);
		
		$form->add_label("poslease_hasfixrent", "Contract contains fixed rent", $yes_no[$posleases["poslease_hasfixrent"]]);

		$form->add_checkbox("poslease_isindexed", "contains tacit renewal clause", $posleases["poslease_isindexed"], "", "Contract");
		$form->add_checkbox("poslease_indexclause_in_contract", "Index Clause in Contract", $posleases["poslease_isindexed"], "", "Index");
		$form->add_label("poslease_indexrate", "Average Index Rate in %", 0, $posleases["poslease_indexrate"]);
		$form->add_label("poslease_average_increase", "Average yearly Increase in %", 0, $posleases["poslease_average_increase"]);
		
		$form->add_section("Lease Options");
		$form->add_label("poslease_extensionoption", "Extension Option to Date", 0, to_system_date($posleases["poslease_extensionoption"]));
		$form->add_label("poslease_exitoption", "Exit Option to Date", 0, to_system_date($posleases["poslease_exitoption"]));
		$form->add_label("poslease_termination_time", "Terminationdeadline", 0, $posleases["poslease_termination_time"]);

		
		$form->add_label("cer_basicdata_deadline_property", "Deadline for property", 0, to_system_date($cer_basicdata["cer_basicdata_deadline_property"]), TYPE_DATE, 10);

		$form->add_label("poslease_handoverdate", "Handover Date (Key)", 0, to_system_date($posleases["poslease_handoverdate"]));
		$form->add_label("poslease_firstrentpayed", "First Rent Payed on", 0, to_system_date($posleases["poslease_firstrentpayed"]));

		$form->add_label("poslease_freeweeks", "Rent Free Period in Weeks", 0, $posleases["poslease_freeweeks"]);


		$form->add_section("Negotiation Details");
		$form->add_label("poslease_negotiator", "Negotiator of Key Money and Lease", 0, $posleases["poslease_negotiator"]);
		$form->add_label("poslease_landlord_name", "Landlord (Negotiation Partner)", 0, $posleases["poslease_landlord_name"]);
		$form->add_label("poslease_negotiated_conditions", "Negotiated Rental Conditions", 0, $posleases["poslease_negotiated_conditions"], 1, "condition_info");
		
	}
	else
	{
		$form->add_section("Lease Details");			
		$form->add_label("poslease_lease_type", "Lease Type");
		$form->add_label("poslease_startdate", "Start Date");
		$form->add_label("poslease_enddate", "Expiry Date");
		
		$form->add_label("poslease_hasfixrent", "Contract contains fixed rent", "");


		$form->add_checkbox("poslease_isindexed", "contains tacit renewal clause", "", "", "Contract");
		$form->add_checkbox("poslease_indexclause_in_contract", "Index Clause in Contract", "", "", "Index");
		$form->add_label("poslease_indexrate", "Average Index Rate in %");
		$form->add_label("poslease_average_increase", "Average yearly Increase in %");

		$form->add_section("Lease Options");
		$form->add_label("poslease_extensionoption", "Extension Option to Date");
		$form->add_label("poslease_exitoption", "Exit Option to Date");
		$form->add_label("poslease_termination_time", "Termination deadline");

		$form->add_label("cer_basicdata_deadline_property", "Deadline for property");

		$form->add_label("poslease_handoverdate", "Handover Date (Key)", 0, "");
		$form->add_label("poslease_firstrentpayed", "First Rent Payed on", 0, "");

		$form->add_label("poslease_freeweeks", "Rent Free Period in Weeks", 0, "");
		
		$form->add_section("Negotiation Details");
		$form->add_label("poslease_negotiator", "Negotiator of Key Money and Lease");
		$form->add_label("poslease_landlord_name", "Landlord (Negotiation Partner)");
		$form->add_label("poslease_negotiated_conditions", "Negotiated Rental Conditions");

	}


	if($project['project_projectkind'] == 5
		or $project['project_projectkind'] == 2
		or $project['project_projectkind'] == 3
		or $project['project_projectkind'] == 6) {
		$form->add_section("Former Lease Conditions");
		$form->add_comment("Please give us some information about the former lease conditions.");
		
		if($project['project_projectkind'] == 3) {
			$form->add_lookup("ln_basicdata_former_pos_id", "Former POS Location", "posaddresses", "posaddress_name", 0, $ln_basicdata["ln_basicdata_former_pos_id"]);
		}
		
		$form->add_label("ln_basicdata_pos_last_renovation_date", "When was this POS renovated for the last time?", 0, to_system_date($ln_basicdata["ln_basicdata_pos_last_renovation_date"]));

		$form->add_label("ln_basicdata_last_fixed_rent", "What was the last yearly fixed rent paid in " . $currency['symbol'] . "?*", 0, $ln_basicdata["ln_basicdata_last_fixed_rent"]);

		$form->add_label("ln_basicdata_last_additional_rent", "What were the last yearly additional rental costs paid in " . $currency['symbol'] . "?", 0, $ln_basicdata["ln_basicdata_last_additional_rent"]);

		$form->add_label("ln_basicdata_last_tob_rent", "What was the turnover based percentage in the lease contract?", 0, $ln_basicdata["ln_basicdata_last_tob_rent"]);

		$form->add_label("ln_basicdata_changes_in_enviroment", "Describe the changes in the environment since the POS opened*", 0, $ln_basicdata["ln_basicdata_changes_in_enviroment"]);

	}
}




/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if($form->button("form_save"))
{

	if($form->value("poslease_hasfixrent") == '')
	{
		$form->error("Please indicate if the contract includes fixed rents.");
	}
	elseif($form->value("poslease_extensionoption") 
		  and $form->value("poslease_enddate") 
		  and from_system_date($form->value("poslease_extensionoption")) <= from_system_date($form->value("poslease_enddate"))
	)
	{
		$form->error("Extension option to date must be a date in the future of the expiry date.");
	}
	elseif($form->value("poslease_exitoption") 
		  and $form->value("poslease_enddate") 
		  and from_system_date($form->value("poslease_exitoption")) > from_system_date($form->value("poslease_enddate"))
	)
	{
		$form->error("Exit option to date must be a date before of the expiry date.");
	}
	elseif($form->value("poslease_handoverdate") and $form->value("poslease_firstrentpayed") and from_system_date($form->value("poslease_handoverdate")) < from_system_date($form->value("poslease_firstrentpayed"))
		   and !$form->value("poslease_freeweeks"))
	{
		$form->error("Please indicate rent free period in weeks!");
	}
	elseif($form->validate())
	{

		// save pos data
		$fields = array();
    
		$value = dbquote($form->value("posaddress_best_benchmark_pos"));
		$fields[] = "posaddress_best_benchmark_pos = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update " . $posdata["table"] . " set " . join(", ", $fields) . " where posaddress_id = " . $posdata["posaddress_id"];
		mysql_query($sql) or dberror($sql);

		//save data to posleases
		
		if(count($posleases) > 0) // update existing record
		{
			
			$fields = array();
		
			$value = dbquote($form->value("poslease_lease_type"));
			$fields[] = "poslease_lease_type = " . $value;

			$value = dbquote($form->value("poslease_isindexed"));
			$fields[] = "poslease_isindexed = " . $value;

			$value = dbquote($form->value("poslease_indexclause_in_contract"));
			$fields[] = "poslease_indexclause_in_contract = " . $value;

			$value = dbquote($form->value("poslease_indexrate"));
			$fields[] = "poslease_indexrate = " . $value;

			$value = dbquote($form->value("poslease_average_increase"));
			$fields[] = "poslease_average_increase = " . $value;
					
			$value = dbquote(from_system_date($form->value("poslease_startdate")));
			$fields[] = "poslease_startdate = " . $value;

			$value = dbquote(from_system_date($form->value("poslease_enddate")));
			$fields[] = "poslease_enddate = " . $value;

			$value = dbquote(from_system_date($form->value("poslease_extensionoption")));
			$fields[] = "poslease_extensionoption = " . $value;

			$value = dbquote(from_system_date($form->value("poslease_exitoption")));
			$fields[] = "poslease_exitoption = " . $value;


			$value = dbquote(from_system_date($form->value("poslease_handoverdate")));
			$fields[] = "poslease_handoverdate = " . $value;

			$value = dbquote(from_system_date($form->value("poslease_firstrentpayed")));
			$fields[] = "poslease_firstrentpayed = " . $value;

			$value = dbquote($form->value("poslease_freeweeks"));
			$fields[] = "poslease_freeweeks = " . $value;

			$value = dbquote($form->value("poslease_hasfixrent"));
			$fields[] = "poslease_hasfixrent = " . $value;

			
			$value = dbquote($form->value("poslease_negotiator"));
			$fields[] = "poslease_negotiator = " . $value;

			$value = dbquote($form->value("poslease_landlord_name"));
			$fields[] = "poslease_landlord_name = " . $value;

			$value = dbquote($form->value("poslease_negotiated_conditions"));
			$fields[] = "poslease_negotiated_conditions = " . $value;

			$value = dbquote($form->value("poslease_termination_time"));
			$fields[] = "poslease_termination_time = " . $value;

			
			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update " . $posleases["table"] . " set " . join(", ", $fields) . " where poslease_id = " . $posleases["poslease_id"];
			mysql_query($sql) or dberror($sql);

		}
		else //insert new record
		{

			$fields = array();
			$values = array();

			$fields[] = "poslease_posaddress";
			$values[] = dbquote($posdata["posaddress_id"]);

			$fields[] = "poslease_order";
			$values[] = dbquote($project["project_order"]);

			$fields[] = "poslease_lease_type";
			$values[] = dbquote($form->value("poslease_lease_type"));
			
			$fields[] = "poslease_isindexed";
			$values[] = dbquote($form->value("poslease_isindexed"));

			$fields[] = "poslease_indexclause_in_contract";
			$values[] = dbquote($form->value("poslease_indexclause_in_contract"));

			$fields[] = "poslease_indexrate";
			$values[] = dbquote($form->value("poslease_indexrate"));

			$fields[] = "poslease_average_increase";
			$values[] = dbquote($form->value("poslease_average_increase"));

			$fields[] = "poslease_startdate";
			$values[] = dbquote(from_system_date($form->value("poslease_startdate")));

			$fields[] = "poslease_enddate";
			$values[] = dbquote(from_system_date($form->value("poslease_enddate")));

			$fields[] = "poslease_extensionoption";
			$values[] = dbquote(from_system_date($form->value("poslease_extensionoption")));

			$fields[] = "poslease_exitoption";
			$values[] = dbquote(from_system_date($form->value("poslease_exitoption")));

			$fields[] = "poslease_handoverdate";
			$values[] = dbquote(from_system_date($form->value("poslease_handoverdate")));

			$fields[] = "poslease_firstrentpayed";
			$values[] = dbquote(from_system_date($form->value("poslease_firstrentpayed")));

			$fields[] = "poslease_freeweeks";
			$values[] = dbquote($form->value("poslease_freeweeks"));

			$fields[] = "poslease_hasfixrent";
			$values[] = dbquote($form->value("poslease_hasfixrent"));

						
			$fields[] = "poslease_negotiator";
			$values[] = dbquote($form->value("poslease_negotiator"));

			$fields[] = "poslease_landlord_name";
			$values[] = dbquote($form->value("poslease_landlord_name"));

			$fields[] = "poslease_negotiated_conditions";
			$values[] = dbquote($form->value("poslease_negotiated_conditions"));

			$fields[] = "poslease_termination_time";
			$values[] = dbquote($form->value("poslease_termination_time"));

			$fields[] = "date_created";
			$values[] = "now()";

			$fields[] = "date_modified";
			$values[] = "now()";

			$fields[] = "user_created";
			$values[] = dbquote(user_login());

			$fields[] = "user_modified";
			$values[] = dbquote(user_login());

			if($posdata["table_leases"] == "posleasepipeline")
			{
				$sql = "insert into posleasespipeline (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			}
			else
			{
				$sql = "insert into posleases (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			}
			
			mysql_query($sql) or dberror($sql);
		
		}

		
		//update cer_basic_data
		$fields = array();
    
		$value = dbquote(from_system_date($form->value("cer_basicdata_deadline_property")));
		$fields[] = "cer_basicdata_deadline_property = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;
		$fields[] = "user_modified = " . dbquote(user_login());
   
		$sql = "update cer_basicdata set " . join(", ", $fields) . 
			" where cer_basicdata_version = 0 and cer_basicdata_project = " . param("pid");
		mysql_query($sql) or dberror($sql);


		//update additional rental costs in case poslease dates have changed
		if(count($posleases) > 0)
		{
			if($old_lease_start_date != '' and $old_lease_start_date <> from_system_date($form->value("poslease_startdate")))
			{
				//$result = calculate_additional_rental_costs(param('pid'), from_system_date($form->value("poslease_startdate")),  from_system_date($form->value("poslease_enddate")), $project["project_cost_totalsqms"]);

				$result = calculate_additional_rental_costs(param('pid'), from_system_date($form->value("poslease_startdate")),  from_system_date($form->value("poslease_enddate")), $project["project_cost_gross_sqms"]);
			}
			elseif($old_lease_end_date != '' and $old_lease_end_date <> from_system_date($form->value("poslease_enddate")))
			{
				//$result = calculate_additional_rental_costs(param('pid'), from_system_date($form->value("poslease_startdate")), from_system_date($form->value("poslease_enddate")), $project["project_cost_totalsqms"]);

				$result = calculate_additional_rental_costs(param('pid'), from_system_date($form->value("poslease_startdate")), from_system_date($form->value("poslease_enddate")), $project["project_cost_gross_sqms"]);
			}
		}

		if($project['project_projectkind'] == 5
		or $project['project_projectkind'] == 2
		or $project['project_projectkind'] == 3
		or $project['project_projectkind'] == 6) {

			//update ln_basic_data
			$fields = array();
		
			$value = dbquote(from_system_date($form->value("ln_basicdata_pos_last_renovation_date")));
			$fields[] = "ln_basicdata_pos_last_renovation_date = " . $value;

			$value = dbquote($form->value("ln_basicdata_last_fixed_rent"));
			$fields[] = "ln_basicdata_last_fixed_rent = " . $value;

			$value = dbquote($form->value("ln_basicdata_last_additional_rent"));
			$fields[] = "ln_basicdata_last_additional_rent = " . $value;

			$value = dbquote($form->value("ln_basicdata_last_tob_rent"));
			$fields[] = "ln_basicdata_last_tob_rent = " . $value;

			$value = dbquote($form->value("ln_basicdata_changes_in_enviroment"));
			$fields[] = "ln_basicdata_changes_in_enviroment = " . $value;

			$value = dbquote($form->value("ln_basicdata_former_pos_id"));
			$fields[] = "ln_basicdata_former_pos_id = " . $value;

			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;
			$fields[] = "user_modified = " . dbquote(user_login());
	   
			$sql = "update ln_basicdata set " . join(", ", $fields) . 
				" where ln_basicdata_version = 0 and ln_basicdata_project = " . param("pid");
			mysql_query($sql) or dberror($sql);


			//add or update profitability data
			if($project["project_relocated_posaddress_id"] > 0) {
				$pos_id = $project["project_relocated_posaddress_id"];
			}
			else {
				$pos_id = $project["posaddress_id"];
			};

			//update profitability first year
			$sql = "select count(possellout_id) as num_recs from possellouts
					where possellout_posaddress_id = " . dbquote($pos_id) . 
					" and possellout_year = " . dbquote($year1);

			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);

			if($row['num_recs'] > 0) {
				$fields = array();

				$value = dbquote($form->value("possellout_month1"));
				$fields[] = "possellout_month = " . $value;

				$value = dbquote($form->value("possellout_watches_units1"));
				$fields[] = "possellout_watches_units = " . $value;

				$value = dbquote($form->value("possellout_watches_grossales1"));
				$fields[] = "possellout_watches_grossales = " . $value;

				$value = dbquote($form->value("possellout_bijoux_units1"));
				$fields[] = "possellout_bijoux_units = " . $value;

				$value = dbquote($form->value("possellout_bijoux_grossales1"));
				$fields[] = "possellout_bijoux_grossales = " . $value;

				$value = dbquote($form->value("possellout_net_sales1"));
				$fields[] = "possellout_net_sales = " . $value;

				$value = dbquote($form->value("possellout_grossmargin1"));
				$fields[] = "possellout_grossmargin = " . $value;

				$value = dbquote($form->value("possellout_operating_expenses1"));
				$fields[] = "possellout_operating_expenses = " . $value;

				$value = dbquote($form->value("possellout_operating_income_excl1"));
				$fields[] = "possellout_operating_income_excl = " . $value;

				$value = dbquote($form->value("possellout_wholsale_margin1"));
				$fields[] = "possellout_wholsale_margin = " . $value;

				$value = dbquote($form->value("possellout_operating_income_incl1"));
				$fields[] = "possellout_operating_income_incl = " . $value;

				$value = dbquote(date("Y-d-m H:i:s"));
				$fields[] = "date_modified = " . $value;

				$value = dbquote(user_login());
				$fields[] = "user_modified = " . $value;

				$sql = "update possellouts set " . join(", ", $fields) . " where possellout_posaddress_id = " . dbquote($pos_id) . 
					   " and possellout_year = " . dbquote($year1);
				mysql_query($sql) or dberror($sql);

			}
			else {
				$fields = array();
				$values = array();

				$fields[] = 'possellout_posaddress_id';
				$values[] = dbquote($pos_id);

				$fields[] = 'possellout_year';
				$values[] = dbquote($year1);

				$fields[] = 'possellout_month';
				$values[] = dbquote($form->value("possellout_month1"));

				$fields[] = 'possellout_watches_units';
				$values[] = dbquote($form->value("possellout_watches_units1"));

				$fields[] = 'possellout_watches_grossales';
				$values[] = dbquote($form->value("possellout_watches_grossales1"));

				$fields[] = 'possellout_bijoux_units';
				$values[] = dbquote($form->value("possellout_bijoux_units1"));

				$fields[] = 'possellout_bijoux_grossales';
				$values[] = dbquote($form->value("possellout_bijoux_grossales1"));

				$fields[] = 'possellout_net_sales';
				$values[] = dbquote($form->value("possellout_net_sales1"));

				$fields[] = 'possellout_grossmargin';
				$values[] = dbquote($form->value("possellout_grossmargin1"));

				$fields[] = 'possellout_operating_expenses';
				$values[] = dbquote($form->value("possellout_operating_expenses1"));

				$fields[] = 'possellout_operating_income_excl';
				$values[] = dbquote($form->value("possellout_operating_income_excl1"));

				$fields[] = 'possellout_wholsale_margin';
				$values[] = dbquote($form->value("possellout_wholsale_margin1"));

				$fields[] = 'possellout_operating_income_incl';
				$values[] = dbquote($form->value("possellout_operating_income_incl1"));

				$fields[] = 'user_created';
				$values[] = dbquote(user_login());

				$fields[] = 'date_created';
				$values[] = dbquote(date("Y-m-d H:i:s"));


				$sql = "insert into possellouts (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);
			}


			//update profitability second year
			$sql = "select count(possellout_id) as num_recs from possellouts
					where possellout_posaddress_id = " . dbquote($pos_id) . 
					" and possellout_year = " . dbquote($year2);

			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);

			if($row['num_recs'] > 0) {
				$fields = array();

				$value = dbquote($form->value("possellout_month2"));
				$fields[] = "possellout_month = " . $value;

				$value = dbquote($form->value("possellout_watches_units2"));
				$fields[] = "possellout_watches_units = " . $value;

				$value = dbquote($form->value("possellout_watches_grossales2"));
				$fields[] = "possellout_watches_grossales = " . $value;

				$value = dbquote($form->value("possellout_bijoux_units2"));
				$fields[] = "possellout_bijoux_units = " . $value;

				$value = dbquote($form->value("possellout_bijoux_grossales2"));
				$fields[] = "possellout_bijoux_grossales = " . $value;

				$value = dbquote($form->value("possellout_net_sales2"));
				$fields[] = "possellout_net_sales = " . $value;

				$value = dbquote($form->value("possellout_grossmargin2"));
				$fields[] = "possellout_grossmargin = " . $value;

				$value = dbquote($form->value("possellout_operating_expenses2"));
				$fields[] = "possellout_operating_expenses = " . $value;

				$value = dbquote($form->value("possellout_operating_income_excl2"));
				$fields[] = "possellout_operating_income_excl = " . $value;

				$value = dbquote($form->value("possellout_wholsale_margin2"));
				$fields[] = "possellout_wholsale_margin = " . $value;

				$value = dbquote($form->value("possellout_operating_income_incl2"));
				$fields[] = "possellout_operating_income_incl = " . $value;

				$value = dbquote(date("Y-d-m H:i:s"));
				$fields[] = "date_modified = " . $value;

				$value = dbquote(user_login());
				$fields[] = "user_modified = " . $value;

				$sql = "update possellouts set " . join(", ", $fields) . " where possellout_posaddress_id = " . dbquote($pos_id) . 
					   " and possellout_year = " . dbquote($year2);
				mysql_query($sql) or dberror($sql);

			}
			else {
				$fields = array();
				$values = array();

				$fields[] = 'possellout_posaddress_id';
				$values[] = dbquote($pos_id);

				$fields[] = 'possellout_year';
				$values[] = dbquote($year2);

				$fields[] = 'possellout_month';
				$values[] = dbquote($form->value("possellout_month2"));

				$fields[] = 'possellout_watches_units';
				$values[] = dbquote($form->value("possellout_watches_units2"));

				$fields[] = 'possellout_watches_grossales';
				$values[] = dbquote($form->value("possellout_watches_grossales2"));

				$fields[] = 'possellout_bijoux_units';
				$values[] = dbquote($form->value("possellout_bijoux_units2"));

				$fields[] = 'possellout_bijoux_grossales';
				$values[] = dbquote($form->value("possellout_bijoux_grossales2"));

				$fields[] = 'possellout_net_sales';
				$values[] = dbquote($form->value("possellout_net_sales2"));

				$fields[] = 'possellout_grossmargin';
				$values[] = dbquote($form->value("possellout_grossmargin2"));

				$fields[] = 'possellout_operating_expenses';
				$values[] = dbquote($form->value("possellout_operating_expenses2"));

				$fields[] = 'possellout_operating_income_excl';
				$values[] = dbquote($form->value("possellout_operating_income_excl2"));

				$fields[] = 'possellout_wholsale_margin';
				$values[] = dbquote($form->value("possellout_wholsale_margin2"));

				$fields[] = 'possellout_operating_income_incl';
				$values[] = dbquote($form->value("possellout_operating_income_incl2"));

				$fields[] = 'user_created';
				$values[] = dbquote(user_login());

				$fields[] = 'date_created';
				$values[] = dbquote(date("Y-m-d H:i:s"));


				$sql = "insert into possellouts (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);
			}


			//update profitability second year
			$sql = "select count(possellout_id) as num_recs from possellouts
					where possellout_posaddress_id = " . dbquote($pos_id) . 
					" and possellout_year = " . dbquote($year3);

			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);

			if($row['num_recs'] > 0) {
				$fields = array();

				$value = dbquote($form->value("possellout_month3"));
				$fields[] = "possellout_month = " . $value;

				$value = dbquote($form->value("possellout_watches_units3"));
				$fields[] = "possellout_watches_units = " . $value;

				$value = dbquote($form->value("possellout_watches_grossales3"));
				$fields[] = "possellout_watches_grossales = " . $value;

				$value = dbquote($form->value("possellout_bijoux_units3"));
				$fields[] = "possellout_bijoux_units = " . $value;

				$value = dbquote($form->value("possellout_bijoux_grossales3"));
				$fields[] = "possellout_bijoux_grossales = " . $value;

				$value = dbquote($form->value("possellout_net_sales3"));
				$fields[] = "possellout_net_sales = " . $value;

				$value = dbquote($form->value("possellout_grossmargin3"));
				$fields[] = "possellout_grossmargin = " . $value;

				$value = dbquote($form->value("possellout_operating_expenses3"));
				$fields[] = "possellout_operating_expenses = " . $value;

				$value = dbquote($form->value("possellout_operating_income_excl3"));
				$fields[] = "possellout_operating_income_excl = " . $value;

				$value = dbquote($form->value("possellout_wholsale_margin3"));
				$fields[] = "possellout_wholsale_margin = " . $value;

				$value = dbquote($form->value("possellout_operating_income_incl3"));
				$fields[] = "possellout_operating_income_incl = " . $value;

				$value = dbquote(date("Y-d-m H:i:s"));
				$fields[] = "date_modified = " . $value;

				$value = dbquote(user_login());
				$fields[] = "user_modified = " . $value;

				$sql = "update possellouts set " . join(", ", $fields) . " where possellout_posaddress_id = " . dbquote($pos_id) . 
					   " and possellout_year = " . dbquote($year3);
				mysql_query($sql) or dberror($sql);

			}
			else {
				$fields = array();
				$values = array();

				$fields[] = 'possellout_posaddress_id';
				$values[] = dbquote($pos_id);

				$fields[] = 'possellout_year';
				$values[] = dbquote($year3);

				$fields[] = 'possellout_month';
				$values[] = dbquote($form->value("possellout_month3"));

				$fields[] = 'possellout_watches_units';
				$values[] = dbquote($form->value("possellout_watches_units3"));

				$fields[] = 'possellout_watches_grossales';
				$values[] = dbquote($form->value("possellout_watches_grossales3"));

				$fields[] = 'possellout_bijoux_units';
				$values[] = dbquote($form->value("possellout_bijoux_units3"));

				$fields[] = 'possellout_bijoux_grossales';
				$values[] = dbquote($form->value("possellout_bijoux_grossales3"));

				$fields[] = 'possellout_net_sales';
				$values[] = dbquote($form->value("possellout_net_sales3"));

				$fields[] = 'possellout_grossmargin';
				$values[] = dbquote($form->value("possellout_grossmargin3"));

				$fields[] = 'possellout_operating_expenses';
				$values[] = dbquote($form->value("possellout_operating_expenses3"));

				$fields[] = 'possellout_operating_income_excl';
				$values[] = dbquote($form->value("possellout_operating_income_excl3"));

				$fields[] = 'possellout_wholsale_margin';
				$values[] = dbquote($form->value("possellout_wholsale_margin3"));

				$fields[] = 'possellout_operating_income_incl';
				$values[] = dbquote($form->value("possellout_operating_income_incl3"));

				$fields[] = 'user_created';
				$values[] = dbquote(user_login());

				$fields[] = 'date_created';
				$values[] = dbquote(date("Y-m-d H:i:s"));


				$sql = "insert into possellouts (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);
			}
		}

	
		$form->message("Your data has bee saved.");

	}
}
  
/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");
require "include/project_page_actions.php";

$page->header();

if($form_type == "INR03")
{
	$page->title("INR-03 - Retail Furniture in Third-party Store: Lease Conditions");
}
elseif($form_type == "AF")
{
	$page->title("AF/Business Plan: Lease Conditions");
}
else
{
	$page->title("Capital Expenditure Request: Lease Conditions");
}
require_once("include/tabs.php");
$form->render();
?>

<div id="condition_info" style="display:none;">
    Please indicate the negotiated rental condidtions. Negotations might have been about:
	<ul>
	<li>Fixed rent per measurement unit (m2/sqft) per month or year</li>
	<li>Participation of landlord</li>
	<li>Guarantess made to landlord</li>
	<li>Conditions of landlord</li>
	<li>Special other conditions</li>
	</ul>
</div> 

<div id="termination" style="display:none;">
    Please provide the number of months prior to termination date that notice must be given to landlord of tenant's intention to extend or terminate.
</div> 

<div id="extenstion_option_info" style="display:none;">
    This date shall indicate until when a contract can be extended after its expiry.
</div> 

<div id="exit_option_info" style="display:none;">
    This date shall indicate the date you can terminate the contract before its expiry.
</div> 

<div id="deadline_for_property_info" style="display:none;">
    This date shall indicate until when you have to give final answer to your landlord.
</div> 

<?php

require "include/footer_scripts.php";
$page->footer();

?>