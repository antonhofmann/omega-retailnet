<?php
/********************************************************************

    ln_booklet_pdf.php

    Print Lease Negotiation Booklet

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-11-06
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-11-06
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require "../include/frame.php";


$cer_version = 0;
$ln_version = 0;
if(param("cerversion"))
{
	$cer_version = param("cerversion");
}
if(param("lnversion"))
{
	$ln_version = param("lnversion");
}

require_once "include/get_functions.php";
require "../shared/func_posindex.php";
require "include/financial_functions.php";
require "include/get_project.php";

check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}


/********************************************************************
    prepare all data needed
*********************************************************************/
if($ln_basicdata["ln_basicdata_factor"] == 0) {$ln_basicdata["ln_basicdata_factor"] = 1;}
$currency_symbol = get_currency_symbol($ln_basicdata["ln_basicdata_currency"]);



// get all data needed from project
$sql = "select * " . 
       "from projects " . 
	   "left join orders on order_id = project_order " .
	   "left join countries on country_id = order_shop_address_country " .
	   "left join product_lines on product_line_id = project_product_line " .
	   "left join productline_subclasses on productline_subclass_id = project_product_line_subclass " .
	   "left join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join projectkinds on projectkind_id = project_projectkind " .
       "left join addresses on address_id = order_client_address " .
	   "left join currencies on currency_id = country_currency " . 
       "left join postypes on postype_id = project_postype ".
	   "left join possubclasses on possubclass_id = project_pos_subclass ".
	   "left join ln_basicdata on ln_basicdata_project = project_id ".
	   "left join floors on floor_id = project_floor " . 
	   "where ln_basicdata_version = " . $ln_version . " and project_id = " . param("pid");


$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	$order_number = $row["project_order"];
	//POS Basic Data
	$posname = $row["order_shop_address_company"];

	if($row["order_shop_address_company2"])
    {
		$posname .= ", " . $row["order_shop_address_company2"];
	}

	
	$posaddress = $row["country_name"];
	$country_name = $row["country_name"];

	if($row["order_shop_address_zip"])
    {
		$posaddress .= ", " . $row["order_shop_address_zip"] . " " . $row["order_shop_address_place"];
	}
	else
	{
		$posaddress .= ", " . $row["order_shop_address_place"];
	}

	$city_name = $row["order_shop_address_place"];

	$posaddress .= ", " . $row["order_shop_address_address"];
	
	if($row["order_shop_address_address2"])
    {
		$posaddress .= ", " . $row["order_shop_address_address2"];
	}
	
	$planned_opening_date = to_system_date($row["project_real_opening_date"]);
	//$planned_opening_date = to_system_date($row["project_planned_opening_date"]);
	$sales_surface = $row["project_cost_sqms"];
	$gross_surface = $row["project_cost_gross_sqms"];
	$total_surface = $row["project_cost_totalsqms"];
	$bo_surface = $row["project_cost_backofficesqms"];
	$other_surface = $row["project_cost_othersqms"];

	$page_title = "Lease Negotiation Booklet: " . $row["project_number"];
	$page_title = "Lease Negotiation ";

	
	if($row["productline_subclass_name"])
	{
		$postype = $row["projectkind_name"] . " - " . $row["project_costtype_text"] . " " . $row["postype_name"] . " - " . $row["product_line_name"] . " / " . $row["productline_subclass_name"];
	}
	else {
		$postype = $row["projectkind_name"] . " - " . $row["project_costtype_text"] . " " . $row["postype_name"] . " - " . $row["product_line_name"];
	}

	$postype = $row["project_costtype_text"] . " " . $row["postype_name"] . " " . $row["possubclass_name"];

	$project_kind = $row["projectkind_name"];
	
	$placement = $row["floor_name"];

	
	$deadline_for_property = to_system_date($cer_basicdata["cer_basicdata_deadline_property"]);


	//get keymoney and deposit
	$tmp = get_pos_intangibles(param("pid"), 15, $cer_version);
	if(count($tmp) > 0)
	{
		$keymoney = $tmp ["cer_investment_amount_cer_loc"];
		$keymoney = $keymoney*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
		$keymoney = round($keymoney/1000, 0);
		

		$tmp = get_pos_intangibles(param("pid"), 9, $cer_version);
		$depositposted = $tmp ["cer_investment_amount_cer_loc"];
		$depositposted = $depositposted*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
		$depositposted = round($depositposted/1000, 0);
	}
	else
	{
		$keymoney = "";
		$depositposted = "";
	}



	//get rents
	$first_full_year = $cer_basicdata["cer_basicdata_firstyear"];
	if($cer_basicdata["cer_basicdata_firstmonth"] > 1)
	{
		$first_full_year = $cer_basicdata["cer_basicdata_firstyear"]+1;
	}

	$last_full_year = $cer_basicdata["cer_basicdata_lastyear"];
	if($cer_basicdata["cer_basicdata_lastmonth"] < 12)
	{
		$last_full_year = $cer_basicdata["cer_basicdata_lastyear"]-1;
	}

	$fullrent_firstyear = 0;
	$$cer_basicdata = 0;
	$turnoverrent_firstyear = 0;
	

	if($first_full_year > 0 and $cer_basicdata["cer_basicdata_firstyear"] < $cer_basicdata["cer_basicdata_lastyear"])
	{
		$sql_e = "select sum(cer_expense_amount) as cer_expense_amount from cer_expenses " . 
			   "where cer_expense_cer_version = " . $cer_version . " and cer_expense_project = " . param("pid") . 
			   " and cer_expense_type in (2,20) " . 
			   " and cer_expense_year = " . dbquote($first_full_year);

		$res_e = mysql_query($sql_e) or dberror($sql_e);
		if ($row_e = mysql_fetch_assoc($res_e))
		{
			$$cer_basicdata = $row_e["cer_expense_amount"];

			if($cer_basicdata["cer_basicdata_tax_percentage_on_rent"] > 0)
			{
				$tmp = $cer_basidata["cer_basicdata_tax_percentage_on_rent"] / 100;
				$tmp = $$cer_basicdata/(1+$tmp);
				$taxes_on_$cer_basicdata = $$cer_basicdata - $tmp;

				$taxes_on_$cer_basicdata = ' (including taxes of ' . $curency_symbol . ' ' . $taxes_on_$cer_basicdata . ')'; 
			}
		}

		$sql_e = "select cer_expense_amount from cer_expenses " . 
			   "where cer_expense_cer_version = " . $cer_version . " and cer_expense_project = " . param("pid") . 
			   " and cer_expense_type = 16 " . 
			   " and cer_expense_year = " . dbquote($first_full_year);

		$res_e = mysql_query($sql_e) or dberror($sql_e);
		if ($row_e = mysql_fetch_assoc($res_e))
		{
			$turnoverrent_firstyear = $row_e["cer_expense_amount"];
		}
	}
	elseif($cer_basicdata["cer_basicdata_firstyear"] == $cer_basicdata["cer_basicdata_lastyear"])
	{
		$sql_e = "select sum(cer_expense_amount) as cer_expense_amount from cer_expenses " . 
			   "where cer_expense_cer_version = " . $cer_version . " and cer_expense_project = " . param("pid") . 
			   " and cer_expense_type in (2,20) " . 
			   " and cer_expense_year = " . dbquote($cer_basicdata["cer_basicdata_firstyear"]);

		$res_e = mysql_query($sql_e) or dberror($sql_e);
		if ($row_e = mysql_fetch_assoc($res_e))
		{
			$$cer_basicdata = $row_e["cer_expense_amount"];
			if($cer_basicdata["cer_basicdata_tax_percentage_on_rent"] > 0)
			{
				$tmp = $cer_basidata["cer_basicdata_tax_percentage_on_rent"] / 100;
				$tmp = $$cer_basicdata/(1+$tmp);
				$taxes_on_$cer_basicdata = $$cer_basicdata - $tmp;

				$taxes_on_$cer_basicdata = ' (including taxes of ' . $curency_symbol . ' ' . $taxes_on_$cer_basicdata . ')'; 
			}
		}

		$sql_e = "select cer_expense_amount from cer_expenses " . 
			   "where cer_expense_cer_version = " . $cer_version . " and cer_expense_project = " . param("pid") . 
			   " and cer_expense_type = 16 " . 
			   " and cer_expense_year = " . dbquote($cer_basicdata["cer_basicdata_firstyear"]);


		$res_e = mysql_query($sql_e) or dberror($sql_e);
		if ($row_e = mysql_fetch_assoc($res_e))
		{
			$turnoverrent_firstyear = $row_e["cer_expense_amount"];
		}
	}

	$fullrent_firstyear = $$cer_basicdata + $turnoverrent_firstyear;

	$fullrent_firstyear = $fullrent_firstyear*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
	$fullrent_firstyear = round($fullrent_firstyear/1000, 0);

	$turnoverrent_firstyear = $turnoverrent_firstyear*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
	$turnoverrent_firstyear = round($turnoverrent_firstyear/1000, 0);



	/*
	$fullrent_lastyear = 0;
	$fixedrent_lastyear = 0;
	$turnoverrent_lastyear = 0;

	if($last_full_year > 0)
	{
		$sql_e = "select sum(cer_expense_amount) as cer_expense_amount from cer_expenses " . 
			   "where cer_expense_cer_version = " . $cer_version . " and cer_expense_project = " . param("pid") . 
			   " and cer_expense_type in (2,20) " . 
			   " and cer_expense_year = " . $last_full_year;

		$res_e = mysql_query($sql_e) or dberror($sql_e);
		if ($row_e = mysql_fetch_assoc($res_e))
		{
			$fixedrent_lastyear = $row_e["cer_expense_amount"];
		}

		$sql_e = "select cer_expense_amount from cer_expenses " . 
			   "where cer_expense_cer_version = " . $cer_version . " and cer_expense_project = " . param("pid") . 
			   " and cer_expense_type = 16 " . 
			   " and cer_expense_year = " . $last_full_year;

		$res_e = mysql_query($sql_e) or dberror($sql_e);
		if ($row_e = mysql_fetch_assoc($res_e))
		{
			$turnoverrent_lastyear = $row_e["cer_expense_amount"];
		}
	}
	
	$fullrent_lastyear = $fixedrent_lastyear + $turnoverrent_lastyear;
	*/
	
	$fullrent_lastyear = $ln_basicdata["ln_basicdata_passedrental"];
	

	$fullrent_lastyear = $fullrent_lastyear*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
	$fullrent_lastyear = round($fullrent_lastyear/1000, 0);

	//$turnoverrent_lastyear = $turnoverrent_lastyear*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
	//$turnoverrent_lastyear = round($turnoverrent_lastyear/1000, 0);


	
	//Lease Consitions
	$poslease = get_ln_lease_data($project["project_id"], $ln_basicdata["ln_basicdata_id"], $project["pipeline"], $ln_version);

	if(count($poslease) > 0)
	{
		$landlord = $poslease["poslease_landlord_name"];
		$negotiated_rental_conditions = $poslease["poslease_negotiated_conditions"];
		$indexrate = $poslease["poslease_indexrate"] . "%";
		$average_yearly_increase = $poslease["poslease_average_increase"] . "%";
		$real_estate_fee = $poslease["poslease_realestate_fee"] . "%";

		$free_weeks = $poslease["poslease_freeweeks"];


		$average_annual_rent = 0;
		$duration_in_years = 0;
		if($poslease["poslease_startdate"] != NULL 
			and $poslease["poslease_startdate"] != '0000-00-00' 
			and $poslease["poslease_enddate"] != NULL
			and $poslease["poslease_enddate"] != '0000-00-00')
			{
				$diff = abs(strtotime($poslease["poslease_enddate"]) - strtotime($poslease["poslease_startdate"]));
				$years = floor($diff / (365*60*60*24));
				$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));

				if($months == 12)
				{
					$months = 0;
					$years++;
				}

				$rental_duration = $years . " years and " . $months . " months";

				$duration_in_years = $years + ($months/12);

				$contract_starting_date = to_system_date($poslease["poslease_startdate"]);
				$contract_ending_date = to_system_date($poslease["poslease_enddate"]);
		}
			
		if($duration_in_years > 0)
		{
			$total_lease_commitment = 0;
			$sql_cer = "select * from cer_expenses " .
						  "where cer_expense_cer_version = " . $cer_version . " and cer_expense_project = " . param("pid") .
						  " and cer_expense_type IN(2, 3, 16, 18, 19, 20)";


			$res_cer = mysql_query($sql_cer) or dberror($sql_cer);
			while ($row_cer = mysql_fetch_assoc($res_cer))
			{
				$total_lease_commitment = $total_lease_commitment + 1*$row_cer["cer_expense_amount"];
			}

			if($duration_in_years > 0)
			{
				$average_annual_rent = round(($total_lease_commitment / $duration_in_years)/1000, 0);
			}
		}
	}
	else
	{
		$landlord = "";
		$negotiated_rental_conditions = "";
		$indexrate = "";
		$indexrate = "";
		$average_yearly_increase = "";
		$real_estate_fee = "";
		$average_annual_rent = "";
		$duration_in_years = "";
		$total_lease_commitment = "";
		$contract_starting_date = "";
		$contract_ending_date = "";
		$free_weeks = "";
	}


	//additional rental cost and other fees
	//get additional rental costs
	$annual_charges = 0;
	$num_of_years = 0;
	$sql_e = "select * from cer_expenses " .
			 "where cer_expense_cer_version = " . $cer_version . " and cer_expense_project = " . param("pid") . 
			 " and cer_expense_type in (3,18,19, 20) " . 
			 " order by cer_expense_year";
	$res_e = mysql_query($sql_e) or dberror($sql_e);

	while($row_e = mysql_fetch_assoc($res_e))
	{
		$annual_charges = $annual_charges + $row_e["cer_expense_amount"];
		$num_of_years++;
	}

	if($num_of_years > 0)
	{
		$annual_charges = $annual_charges/$num_of_years;
		$annual_charges = 1*round($annual_charges/1000, 0);
	}

	
	//get areas and neighbourhood information
	$posareas = "";
	if($project["pipeline"] == 0)
	{
		$sql_i = "select * from posareas " . 
				 "left join posareatypes on posareatype_id = posarea_area " .
				 "where posarea_posaddress = " . $project["posaddress_id"];
		
	}
	elseif($project["pipeline"] == 1)
	{
		
		$sql_p = "select posorder_posaddress from posorderspipeline where posorder_order = " . $project["order_id"];
		$res_p = mysql_query($sql_p) or dberror($sql_p);

		if ($row_p = mysql_fetch_assoc($res_p))
		{
			$sql_i = "select * from posareaspipeline " . 
					 "left join posareatypes on posareatype_id = posarea_area " .
					 "where posarea_posaddress = " . $row_p["posorder_posaddress"];
		}
	}
	$res_i = mysql_query($sql_i) or dberror($sql_i);
	while ($row_i = mysql_fetch_assoc($res_i))
	{
		$posareas .= $row_i["posareatype_name"] . ", ";
	}
	$posareas = substr($posareas,0,strlen($posareas)-2);


	//neighbourhood
	$neighbourhoods = array();
	if($project["pipeline"] == 0)
	{
		$sql_e = "select * from posorders where posorder_order = " . $project["order_id"];
	}
	elseif($project["pipeline"] == 1)
	{
		$sql_e = "select * from posorderspipeline where posorder_order = " . $project["order_id"];
	}
	
	$res_e = mysql_query($sql_e) or dberror($sql_e);
	if ($row_e = mysql_fetch_assoc($res_e))
	{
		$neighbourhoods["Shop on left side"] = $row_e["posorder_neighbour_left"];
		$neighbourhoods["Shop on right side"] = $row_e["posorder_neighbour_right"];
		$neighbourhoods["Shop across left side"] = $row_e["posorder_neighbour_acrleft"];
		$neighbourhoods["Shop across right side"] = $row_e["posorder_neighbour_acrright"];
		$neighbourhoods["Other brands in area"] = $row_e["posorder_neighbour_brands"];
	}



	$ln_remarks = $row["ln_basicdata_remarks"];
	$ln_brands = $row["ln_basicdata_brands"];
	$ln_size_remarks = $row["ln_basicdata_size_remarks"];
	$ln_area = $row["ln_basicdata_area"];
	$ln_rent = $row["ln_basicdata_rent"];
	$ln_availability = $row["ln_basicdata_availability"];


	$approved = "";
	$approvedby_cm = $row["ln_basicdata_approvedby_cm"];
	$approved_by_ekl = $row["ln_basicdata_approved_by_ekl"];

	if($approvedby_cm and $approved_by_ekl)
	{
		$approved = "Approved by Counry Manager and EKL Responsable";
	}
	elseif($approvedby_cm)
	{
		$approved = "Approved by Counry Manager";
	}
	elseif($approved_by_ekl)
	{
		$approved = "Approved by EKL Responsable";
	}
	else
	{
		$approved = "Approved by ";
	}

	
	$pix1 = ".." . $row["ln_basicdata_pix1"];
	$pix2 = ".." . $row["ln_basicdata_pix2"];
	$pix3 = ".." . $row["ln_basicdata_pix3"];

	$floor_plan = ".." . $row["ln_basicdata_floorplan"];
	$lease_agreement = ".." . $row["ln_basicdata_draft_aggreement"];


	//get names of roles
	$brand_manager = "";
	$sales_manager = "";
	$vp_sales = "";
	$president = "";

	$sql_u = "select cer_summary_in01_sig03 " . 
		     "from cer_summary " . 
		     "where cer_summary_cer_version = " . $cer_version . " and cer_summary_project = " . param("pid");
	
	$res_u = mysql_query($sql_u) or dberror($sql_u);
	if($row_u = mysql_fetch_assoc($res_u))
	{
		$brand_manager = $row_u["cer_summary_in01_sig03"];
	}

	$sql_u = "select user_firstname, user_name from users " . 
		     "left join addresses on address_id = user_address " . 
		     "left join user_roles on user_role_user = user_id " . 
		     "where address_country = " . dbquote($row["order_shop_address_country"]) .
		     " and user_active = 1 and user_role_role = 33";
	
	$res_u = mysql_query($sql_u) or dberror($sql_u);
	if($row_u = mysql_fetch_assoc($res_u))
	{
		$sales_manager = "\r\n" . $row_u["user_name"] . " " . $row_u["user_firstname"];
		
	}

	if(!$sales_manager)
	{
		$sql_u = "select user_firstname, user_name from addresses " . 
			     "left join country_access on country_access_country = address_country " .
			     "left join users on user_id = country_access_user " . 
			     "left join user_roles on user_role_user = user_id " . 
				 "where address_country = " . dbquote($row["order_shop_address_country"]) .
				 " and user_active = 1 and user_role_role = 33";
	
		$res_u = mysql_query($sql_u) or dberror($sql_u);
		if($row_u = mysql_fetch_assoc($res_u))
		{
			$sales_manager = "\r\n" . $row_u["user_name"] . " " . $row_u["user_firstname"];
		}
	}

	
	$approval_name1 = $cer_basicdata["cer_basicdata_approvalname1"];
	$president = $cer_basicdata["cer_basicdata_approvalname2"];
	$vp_sales = $cer_basicdata["cer_basicdata_approvalname3"];
	$retail_head = $cer_basicdata["cer_basicdata_approvalname4"];

	/*
	$sql = "select * from cer_approvalnames where cer_approvalname_id = 1";
	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$approval_name1 = $row["cer_approvalname_name1"];
		$president = $row["cer_approvalname_name2"];
		$vp_sales = $row["cer_approvalname_name3"];
		$retail_head = $row["cer_approvalname_name4"];
	}
	*/



	/*
	$exchange_rate_factor = $cer_basicdata['cer_basicdata_factor'];
	if(!$cer_basicdata['cer_basicdata_factor'])
	{
		$exchange_rate_factor = 1;
	}

	$exchange_rate = 1;
	if($cer_basicdata['cer_basicdata_exchangerate'] > 0)
	{
		$exchange_rate = $cer_basicdata['cer_basicdata_exchangerate'];
	}
	*/
	

	$exchange_rate_factor = $ln_basicdata['ln_basicdata_factor'];
	if(!$ln_basicdata['ln_basicdata_factor'])
	{
		$exchange_rate_factor = 1;
	}

	$exchange_rate = 1;
	if($ln_basicdata['ln_basicdata_exchangerate'] > 0)
	{
		$exchange_rate = $ln_basicdata['ln_basicdata_exchangerate'];
	}



	
    
	include("include/in_financial_data.php");	

	/********************************************************************
		prepare pdf
	*********************************************************************/

	require_once('../include/tcpdf/config/lang/eng.php');
	require_once('../include/tcpdf/tcpdf.php');
	require_once('../include/fpdi/fpdi.php'); 


	
	//Instanciation of inherited class
	$pdf = new FPDI("P", "mm", "A4", true, 'UTF-8', false);
	$pdf->setPrintHeader(false);
	$pdf->SetMargins(10, 23, 10);

	$pdf->Open();

	$pdf->SetAutoPageBreak(false);

	$pdf->SetFillColor(220, 220, 220); 


	$pdf->AddFont('arialn','');
	$pdf->AddFont('arialn','B');
	$pdf->AddFont('arialn','I');
	$pdf->AddFont('arialn','BI');

	$pdf->AddPage();
	$new_page = 0;


	//Logo
	$pdf->Image('../pictures/logo.jpg',10,8,33);
	//arialn bold 15
	$pdf->SetFont('arialn','B',18);
	//Move to the right
	$pdf->Cell(80);
	//Title
	$pdf->SetY(0);
	$pdf->Cell(0,33,$page_title,0,0,'R');
	//Line break
	$pdf->SetY(23);
	
	
	include("ln_form_pdf_detail.php");

	//Position at 1.5 cm from bottom
	$pdf->SetY(282);
	//arialn italic 8
	$pdf->SetFont('arialn','I',8);
	//Page number
	$pdf->Cell(0,10, to_system_date(date("d.m.y")),0,0,'R');


	
	//Picture 2
	if($pix2 != ".." and file_exists($pix2))
	{
		//PDF
		if(substr($pix2, strlen($pix2)-3, 3) == "pdf" or substr($pix2, strlen($pix2)-3, 3) == "PDF")
		{
			if(!isset($layout))
			{
				$layout = new FPDI();
			}
			$num_of_pages = $pdf->setSourceFile($pix2);
			$num_of_pages = $layout->setSourceFile($pix2);
			for($i=1;$i<=$num_of_pages;$i++)
			{
				$tplIdx = $pdf->importPage($i);
				$layout->AddPage("L");
				$tplIdx = $layout->importPage($i);
				
				$tpl = $layout->useTemplate($tplIdx);
				//$pdf->AddPage("", array(0=>$tpl["w"], 1=>$tpl["h"]));

				if($tpl["w"] > $tpl["h"])
				{
					$pdf->AddPage("L", array(0=>$tpl["h"], 1=>$tpl["w"]));
				}
				else
				{
					$pdf->AddPage("P", array(0=>$tpl["w"], 1=>$tpl["h"]));
				}

				$tpl = $pdf->useTemplate($tplIdx);
			}
		}
		elseif(substr($pix2, strlen($pix2)-3, 3) == "jpg" or substr($pix2, strlen($pix2)-3, 3) == "JPG")
		{
			$pdf->AddPage("P", "A4");
			$pdf->Image('../pictures/logo.jpg',10,8,33);
			//arialn bold 15
			$pdf->SetFont('arialn','B',12);
			//Move to the right
			$pdf->Cell(80);
			//Title
			$pdf->SetY(0);
			$pdf->Cell(0,33,$page_title,0,0,'R');
			//Line break
			$pdf->SetY(23);

			$x = $pdf->GetX();
			$y = $pdf->GetY() + 2;
			
			
			$pdf->Image($pix2,$x,$y, 0, 110);
			
			//Position at 1.5 cm from bottom
			$pdf->SetY(282);
			//arialn italic 8
			$pdf->SetFont('arialn','I',8);
			//Page number
			$pdf->Cell(0,10, to_system_date(date("d.m.y")),0,0,'R');
		}
	}


	//Picture 3
	if($pix3 != ".." and file_exists($pix3))
	{
		//PDF
		if(substr($pix3, strlen($pix3)-3, 3) == "pdf" or substr($pix3, strlen($pix3)-3, 3) == "PDF")
		{
			if(!isset($layout))
			{
				$layout = new FPDI();
			}
			$num_of_pages = $pdf->setSourceFile($pix3);
			$num_of_pages = $layout->setSourceFile($pix3);
			for($i=1;$i<=$num_of_pages;$i++)
			{
				$tplIdx = $pdf->importPage($i);
				$layout->AddPage("L");
				$tplIdx = $layout->importPage($i);
				
				$tpl = $layout->useTemplate($tplIdx);
				//$pdf->AddPage("", array(0=>$tpl["w"], 1=>$tpl["h"]));

				if($tpl["w"] > $tpl["h"])
				{
					$pdf->AddPage("L", array(0=>$tpl["h"], 1=>$tpl["w"]));
				}
				else
				{
					$pdf->AddPage("P", array(0=>$tpl["w"], 1=>$tpl["h"]));
				}

				$tpl = $pdf->useTemplate($tplIdx);
			}
		}
		elseif(substr($pix3, strlen($pix3)-3, 3) == "jpg" or substr($pix3, strlen($pix3)-3, 3) == "JPG")
		{
			//$pdf->AddPage("P", "A4");
			$pdf->Image('../pictures/logo.jpg',10,8,33);
			//arialn bold 15
			$pdf->SetFont('arialn','B',12);
			//Move to the right
			$pdf->Cell(80);
			//Title
			$pdf->SetY(0);
			$pdf->Cell(0,33,$page_title,0,0,'R');
			//Line break
			//$pdf->SetY(100);
			
			$y = 145;
			$pdf->Image($pix3,$x,$y, 0, 110);

			//Position at 1.5 cm from bottom
			$pdf->SetY(282);
			//arialn italic 8
			$pdf->SetFont('arialn','I',8);
			//Page number
			$pdf->Cell(0,10, to_system_date(date("d.m.y")),0,0,'R');
		}
	}

	
	// Lease Agreement 
	$source_file = $lease_agreement;
	if(file_exists($source_file))
	{
		//PDF
		if(substr($source_file, strlen($source_file)-3, 3) == "pdf" or substr($source_file, strlen($source_file)-3, 3) == "PDF")
		{
			if(!isset($layout))
			{
				$layout = new FPDI();
			}
			$num_of_pages = $pdf->setSourceFile($source_file);
			$num_of_pages = $layout->setSourceFile($source_file);

			for($i=1;$i<=$num_of_pages;$i++)
			{
				$tplIdx = $pdf->importPage($i);
				$layout->AddPage("L");
				$tplIdx = $layout->importPage($i);
				
				$tpl = $layout->useTemplate($tplIdx);
				if($tpl["w"] > $tpl["h"])
				{
					$tpl = $layout->useTemplate($tplIdx, 0, 0, $tpl["w"], $tpl["h"], true);
					//$pdf->AddPage("L", array(0=>$tpl["w"], 1=>$tpl["h"]));
					$pdf->AddPage("L");
				}
				else
				{
					$tpl = $layout->useTemplate($tplIdx, 0, 0, $tpl["w"], $tpl["h"], true);
					$pdf->AddPage("P");
					//$pdf->AddPage("P", array(0=>$tpl["w"], 1=>$tpl["h"]));
				}
				$tpl = $pdf->useTemplate($tplIdx);
			}
		}
	}


	//business plan


	$currencies = array();
	$sql = 'select currency_id, currency_symbol from currencies where currency_system = 1';

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$currencies["s"] = $row['currency_symbol'];
	}


	if($cer_basicdata['cer_basicdata_currency']) {
		$sql = 'select currency_id, currency_symbol from currencies where currency_id = ' . $cer_basicdata['cer_basicdata_currency'];

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res) and !in_array($row['currency_symbol'], $currencies))
		{
			$currencies["c1"] = $row['currency_symbol'];
		}
	}

	if($cer_basicdata['cer_basicdata_currency2']) {
		$sql = 'select currency_id, currency_symbol from currencies where currency_id = ' . $cer_basicdata['cer_basicdata_currency2'];

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res) and !in_array($row['currency_symbol'], $currencies))
		{
			$currencies["c2"] = $row['currency_symbol'];
		}
	}

	//business plan in client's currency
	$currency_source = "LN";


	include("cer_inr02_pdf_detail.php");


	//business plan in system'currency
	if(array_key_exists("s", $currencies) and array_key_exists("c1", $currencies) and $currencies["s"] != $currencies["c1"])
	{
		$cid = 's';
		include("cer_inr02_pdf_detail.php");
	}

	//business plan in pos'currency
	if(array_key_exists("c2", $currencies) and array_key_exists("c1", $currencies) and $currencies["c2"] != $currencies["c1"])
	{
		$cid = 'c2';
		include("cer_inr02_pdf_detail.php");
	}



	// write pdf
	$pdf->Output(BRAND . '_ln_booklet_' . $posname . '_' . $project["project_number"] . '.pdf');
}

?>