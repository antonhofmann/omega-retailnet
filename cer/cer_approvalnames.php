<?php
/********************************************************************

    cer_approvalnames.php

    Edit CER Approval names for Sheet IN-R03.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-06-03
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-06-03
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_full_access_to_cer");

param("id", 1);

$form = new Form("cer_approvalnames", "Approval Names");

$form->add_section();
$form->add_edit("cer_approvalname_name1", "Retail Investment Comitee", 0 );
$form->add_edit("cer_approvalname_name5", "Swatch Group HQ Retail Development", 0 );
$form->add_edit("cer_approvalname_name6", "Swatch Group HQ Retail Controlling", 0 );
$form->add_edit("cer_approvalname_name10", "Swatch Group HQ Retail Comitee", 0 );
$form->add_edit("cer_approvalname_name11", "Swatch Group HQ Central Services", 0 );

$form->add_edit("cer_approvalname_name2", BRAND . " CEO", 0 );
$form->add_edit("cer_approvalname_name7", BRAND . " VP Finance", 0 );
$form->add_edit("cer_approvalname_name3", BRAND . " VP Sales", 0 );
$form->add_edit("cer_approvalname_name4", BRAND . " Merchandising Manager", 0 );

$form->add_edit("cer_approvalname_name8", BRAND . " Retail Development", 0 );
$form->add_edit("cer_approvalname_name9", BRAND . " Retail Controller", 0 );
$form->add_edit("cer_approvalname_name12", BRAND . " Retail Operations", 0 );

$form->add_button(FORM_BUTTON_SAVE, "Save");

$form->populate();
$form->process();

$page = new Page("approvalnames");
$page->header();
$page->title("Edit Approval Names");
$form->render();
$page->footer();

?>