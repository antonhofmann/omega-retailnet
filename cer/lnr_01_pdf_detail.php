<?php
/********************************************************************

    lnr_01_pdf_detail.php

    Print Detail Form LNR-01.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-06-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-06-14
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.
*********************************************************************/

//set pdf parameters
$margin_top = 10;
$margin_left = 12;
$y = $margin_top;
$x = $margin_left+1;
$standard_h = 5;

$pdf->AddPage();

// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "B", 10);
$pdf->Cell(51, 8, "Swatch Group", 1);

$pdf->SetFont("arialn", "B", 14);
$pdf->Cell(178, 8, "LEASE NEGOTIATION - APPLICATION FORM", 1, "", "C");

$pdf->SetFont("arialn", "", 10);
$pdf->SetFillColor(248,251,167);

if($ln_version == 0 and $ln_basicdata["ln_basicdata_locked"] == 0)
{
	$pdf->Cell(20, 8, date("d.m.Y"), 1, "", "C", true);
}
else
{
	$pdf->Cell(20, 8, to_system_date($ln_basicdata["versiondate"]), 1, "", "C", true);
}
$pdf->Cell(21, 8, "LNR-01", 1, "", "C");

	
	$y = $pdf->GetY()+10;
	$y_at_start = $y - 1;

	
	
	// 1. General information
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(228, 6, "1. General information (" . $project["order_shop_address_company"] . ', Project: ' . $project["project_number"]. ")", 1, "", "L");

	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(20, 6, "Currency:", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(20, 6, $currency_symbol, 1, "", "L", true);
	
	$y = $pdf->GetY()+6;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(50, $standard_h, "Proposed Brand(s)", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, BRAND, 1, "", "L", true);

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(50, $standard_h, "Country", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, $project["country_name"], 1, "", "L", true);

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(50, $standard_h, "City + State / Province", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, $pos_data["place_name"] . " / " . $pos_data["province_canton"], 1, "", "L", true);


	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(50, $standard_h, "Landlord Name", 1, "", "L");
	
	$pdf->Cell(84, $standard_h, substr($landlord, 0,64), 1, "", "L", true);

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(50, $standard_h, "Street and/or name of mall/store", 1, "T");
	$pdf->SetXY($margin_left+ 51,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(84,$standard_h, $pos_data["posaddress_address"], 1, "T", true);

	
	$y = $pdf->GetY();
	$pdf->SetXY($margin_left+1,$y);

	$h = $y;
	$pdf->SetXY($margin_left+ 51,$y);
	$pdf->SetFont("arialn", "", 9);
	if($ln_basicdata["ln_basicdata_remarks"])
	{
		$pdf->MultiCell(84,1.8*$standard_h, $postype . "\n\n" . $ln_basicdata["ln_basicdata_remarks"], 1, "T", true);
	}
	else
	{
		$pdf->MultiCell(84,$standard_h, $postype, 1, "T", true);
	}
	
	$h = $pdf->GetY() - $h;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(50, $h, "Type / Key points", 1, "L", false);
	$y = $pdf->GetY();
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(50, $standard_h, "Gross / Total / Sales Surface (sqm)", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, $gross_surface . ' / ' .$total_surface .  ' / ' . $sale_surface . $floor_surfaces, 1, "", "L", true);
	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(50, $standard_h, "Back Office / Other Surface (sqm)", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, $bo_surface . ' / ' . $other_surface, 1, "", "L", true);
	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);


	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(50, $standard_h, "Contract period begin", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, $contract_starting_date, 1, "", "L", true);

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(50, $standard_h, "Contract period end", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, $contract_ending_date, 1, "", "L", true);

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(50, $standard_h, "Renewal Option (months)", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, $renewal_option_date , 1, "", "L", true);

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(50, $standard_h, "Planned opening", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, to_system_date($project["project_real_opening_date"]), 1, "", "L", true);

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(50, $standard_h, "Rent free period (months)", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, $free_months, 1, "", "L", true);

	
	//case sales guarantee
	if($cer_basicdata["cer_basicdata_sales_guarantee"] == 1){


		$y = $pdf->GetY()+ $standard_h;
		$pdf->SetXY($margin_left+1,$y);
		
		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(50, $standard_h, "Annual fix rent (K" . $currency_symbol .")", 1, "", "L");
		$pdf->SetFont("arialn", "B", 9);
		
		if($savings_on_rent !=0)
		{
			$tmp = $fixedrent_firstyear . " (Savings on rental costs " . $savings_on_rent . ")";
			$pdf->Cell(84, $standard_h, $tmp, 1, "", "L", true);
		}
		$pdf->SetFont("arialn", "", 9);
		$pdf->Cell(84, $standard_h, $fixedrent_firstyear . " (sales guarantee " . $sales_guarantee_first_year  . " K" . $currency_symbol .")", 1, "", "L", true);
		

		$y = $pdf->GetY()+ $standard_h;
		$pdf->SetXY($margin_left+1,$y);

		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(50, $standard_h, "Annual variable rent (% turnover)", 1, "", "L");
		$pdf->SetFont("arialn", "B", 9);
		
		$pdf->Cell(84, $standard_h, $sales_percent_first_year, 1, "", "L", true);

		$y = $pdf->GetY()+ $standard_h;
		$pdf->SetXY($margin_left+1,$y);

		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(50, $standard_h, "Total annual rent first full year", 1, "", "L");
		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(84, $standard_h, $fixedrent_firstyear+$turnoverrent_firstyear, 1, "", "L", true);
	
	}
	elseif($cer_basicdata['cer_basicdata_add_tob_rents'] == 1) {
		
		$y = $pdf->GetY()+ $standard_h;
		$pdf->SetXY($margin_left+1,$y);
		
		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(50, $standard_h, "Annual fix rent (K" . $currency_symbol .")", 1, "", "L");
		$pdf->SetFont("arialn", "B", 9);
		
		if($savings_on_rent !=0)
		{
			$tmp = $fixedrent_firstyear . " (Savings on rental costs " . $savings_on_rent . ")";
			$pdf->Cell(84, $standard_h, $tmp, 1, "", "L", true);
		}
		elseif($fixedrent_firstyear > 0)
		{
			$pdf->Cell(84, $standard_h, $fixedrent_firstyear, 1, "", "L", true);
		}
		else
		{
			$pdf->Cell(84, $standard_h, "no fixed rent", 1, "", "L", true);
		}

		$y = $pdf->GetY()+ $standard_h;
		$pdf->SetXY($margin_left+1,$y);

		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(50, $standard_h, "Annual variable rent (% turnover)", 1, "", "L");
		$pdf->SetFont("arialn", "B", 9);
		
		if($turnoverrent_firstyear == 0
			and $sales_percent_first_year > 0) {
			$pdf->Cell(84, $standard_h, $turnoverrent_firstyear . " (" . $sales_percent_first_year . ") " . "breakpoint not reached", 1, "", "L", true);
		}
		elseif($turnoverrent_firstyear == 0
			and $sales_percent_first_year == 0) {
			$pdf->Cell(84, $standard_h, $turnoverrent_firstyear . " (" . $sales_percent_first_year . ") " . "no variable rent", 1, "", "L", true);
		}
		else {
			$pdf->Cell(84, $standard_h, $turnoverrent_firstyear . " (" . $sales_percent_first_year . ")", 1, "", "L", true);
		}

		$y = $pdf->GetY()+ $standard_h;
		$pdf->SetXY($margin_left+1,$y);

		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(50, $standard_h, "Total annual rent first full year", 1, "", "L");
		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(84, $standard_h, $fullrent_firstyear, 1, "", "L", true);
	}
	elseif($turnoverrent_firstyear > 0) {
		
		$y = $pdf->GetY()+ $standard_h;
		$pdf->SetXY($margin_left+1,$y);
		
		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(50, $standard_h, "Annual fix rent (K" . $currency_symbol .")", 1, "", "L");
		$pdf->SetFont("arialn", "B", 9);
		
		if($savings_on_rent !=0)
		{
			$tmp = $fixedrent_firstyear . " (Savings on rental costs " . $savings_on_rent . ")";
			$pdf->Cell(84, $standard_h, $tmp, 1, "", "L", true);
		}
		elseif($fixedrent_firstyear > 0)
		{
			//$pdf->Cell(84, $standard_h, "Breakpoint reached", 1, "", "L", true);
			$pdf->Cell(84, $standard_h, $fixedrent_firstyear, 1, "", "L", true);
		}
		else
		{
			$pdf->Cell(84, $standard_h, "no fixed rent", 1, "", "L", true);
		}

		$y = $pdf->GetY()+ $standard_h;
		$pdf->SetXY($margin_left+1,$y);

		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(50, $standard_h, "Annual variable rent (% turnover)", 1, "", "L");
		$pdf->SetFont("arialn", "B", 9);
		
		$pdf->Cell(84, $standard_h, $fixedrent_firstyear+$turnoverrent_firstyear . " (" . $sales_percent_first_year . ")", 1, "", "L", true);

		$y = $pdf->GetY()+ $standard_h;
		$pdf->SetXY($margin_left+1,$y);

		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(50, $standard_h, "Total annual rent first full year", 1, "", "L");
		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(84, $standard_h, $fixedrent_firstyear+$turnoverrent_firstyear, 1, "", "L", true);
	}
	else {
		
		
		$y = $pdf->GetY()+ $standard_h;
		$pdf->SetXY($margin_left+1,$y);
		
		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(50, $standard_h, "Annual fix rent (K" . $currency_symbol .")", 1, "", "L");
		$pdf->SetFont("arialn", "B", 9);
		
		if($savings_on_rent !=0)
		{
			$tmp = $fixedrent_firstyear . " (Savings on rental costs " . $savings_on_rent . ")";
			$pdf->Cell(84, $standard_h, $tmp, 1, "", "L", true);
		}
		elseif($fixedrent_firstyear > 0)
		{
			$pdf->Cell(84, $standard_h, $fixedrent_firstyear, 1, "", "L", true);
		}
		else
		{
			$pdf->Cell(84, $standard_h, "no fixed rent", 1, "", "L", true);
		}

		$y = $pdf->GetY()+ $standard_h;
		$pdf->SetXY($margin_left+1,$y);

		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(50, $standard_h, "Annual variable rent (% turnover)", 1, "", "L");
		$pdf->SetFont("arialn", "B", 9);
		
		if($turnoverrent_firstyear == 0
			and $sales_percent_first_year > 0) {
			$pdf->Cell(84, $standard_h, $turnoverrent_firstyear . " (" . $sales_percent_first_year . ") " . "breakpoint not reached", 1, "", "L", true);
		}
		elseif($turnoverrent_firstyear == 0
			and $sales_percent_first_year == 0) {
			$pdf->Cell(84, $standard_h, $turnoverrent_firstyear . " (" . $sales_percent_first_year . ") " . "no variable rent", 1, "", "L", true);
		}
		else {
			$pdf->Cell(84, $standard_h, $turnoverrent_firstyear . " (" . $sales_percent_first_year . ")", 1, "", "L", true);
		}

		$y = $pdf->GetY()+ $standard_h;
		$pdf->SetXY($margin_left+1,$y);

		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(50, $standard_h, "Total annual rent first full year", 1, "", "L");
		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(84, $standard_h, $fullrent_firstyear, 1, "", "L", true);
	}

	
	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(50, $standard_h, "Average annual lease commitment", 1, "", "L");
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(84, $standard_h, $average_annual_rent, 1, "", "L", true);
	
	
	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$h = $y;
	$pdf->SetXY($margin_left + 51,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(84,1.8*$standard_h, $negotiated_rental_conditions, 1, "T", true);
	
	$h = $pdf->GetY() - $h;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(50, $h, "Conditions for the application/ " . "\n" .  "of variable rent", 1, "L", false);
	$y = $pdf->GetY();
	$pdf->SetXY($margin_left+1,$y);

	

	
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(50, $standard_h, "Average annual increase (%)", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	
	if($average_yearly_increase) {
		$pdf->Cell(84, $standard_h, $average_yearly_increase ."%", 1, "", "L", true);
	}
	else {
		$pdf->Cell(84, $standard_h, "no increase", 1, "", "L", true);
	}

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(50, $standard_h, "Additional rental costs (K" . $currency_symbol .")", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	
	
	
	$tmp = $annual_charges + $tax_on_rents + $passenger_index;
	
	
	if($tmp == 0)
	{
		$pdf->Cell(84, $standard_h, "no additional rental costs", 1, "", "L", true);
	}
	else
	{
		$tmp = "";
		if($tax_on_rents != 0)
		{
			$tmp .= "  Tax: " . $tax_on_rents;
		}
		if($tax_on_rents != 0)
		{
			$tmp .= "  Passenger Index: " . $passenger_index;
		}

		if($tmp != "")
		{
			$tmp = "Charges: " . $annual_charges . $tmp;
			$pdf->Cell(84, $standard_h, $tmp, 1, "", "L", true);
		}
		else
		{
			$pdf->Cell(84, $standard_h, $annual_charges, 1, "", "L", true);
		}
	}

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(50, $standard_h, "Key money (K" . $currency_symbol . ")", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, $keymoney, 1, "", "L", true);

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(50, $standard_h, "Deposit (value/months of rent)", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, $deposit . '/' . $deposit_value_in_months, 1, "", "L", true);

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(50, $standard_h, "Real estate fees (%)", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, $real_estate_fee, 1, "", "L", true);


	//draw outer box
	$y = $pdf->GetY()-$margin_top - 2;
	$pix_h = $pdf->GetY()-$margin_top - 12;
	$pdf->SetXY($margin_left,$y_at_start);
	$pdf->Cell(270, $y, "", 1);



	//picture 1

	$y = $margin_top + 18;
	$x = $margin_left+136;
	$picture_printed = false;
	if($pix1 != ".." and file_exists($pix1))
	{
		if(substr($pix1, strlen($pix1)-3, 3) == "jpg" or substr($pix1, strlen($pix1)-3, 3) == "JPG")
		{
			$imagesize = getimagesize($pix1);
			$w = $imagesize[0];
			$h = $imagesize[1];
			
			if($w >= $h)
			{
				$pdf->Image($pix1,$x,$y, 132);
			}
			else
			{
				$pdf->Image($pix1,$x,$y, 132, $pix_h, '', '', '', true, 300, '', false, false, 0, true);

			}
			
		}
	}
	
?>