<?php
/********************************************************************

    ln_form_pdf_detail.php

    Print Lease Negotiation Form

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-11-06
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-11-06
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/
	

	
	if($exchange_rate_factor > 0)
	{
		$e_rate = $exchange_rate/$exchange_rate_factor;
	}
	else
	{
		$e_rate = $exchange_rate;
	}


	$number_of_months_first_year = 13-$cer_basicdata["cer_basicdata_firstmonth"];
	$number_of_months_last_year = $cer_basicdata["cer_basicdata_lastmonth"];

	if($number_of_months_first_year == 12)
	{
		$i = 0;
	}
	else
	{
		$i = 1;
	}

	$sd01 = "";
	$sd02 = "";
	$sd03 = "";
	$sd04 = "";
	if(array_key_exists($i, $years))
	{
			if(is_numeric($sales_units_watches_values[$years[$i]]))
			{
				$sd01 = number_format($sales_units_watches_values[$years[$i]], 0, ".", "'");
			}

			if(is_numeric($total_gross_sales_values[$years[$i]]))
			{
				$sd02 = number_format(round($e_rate*$total_gross_sales_values[$years[$i]]/1000,0), 0, ".", "'");
			}
			if(is_numeric($operating_income01_values[$years[$i]]))
			{
				$sd03 = number_format(round($e_rate*$operating_income01_values[$years[$i]]/1000,0), 0, ".", "'");
			}
			if(is_numeric($operating_income02_values[$years[$i]]))
			{
				$sd04 = number_format(round($e_rate*$operating_income02_values[$years[$i]]/1000,0), 0, ".", "'");
			}
			
	}

	//if business plan period is shorter than 12 months
	$first_full_year_exists = true;
	if($cer_basicdata["cer_basicdata_firstyear"] == $cer_basicdata["cer_basicdata_lastyear"])
	{
		if(array_key_exists($cer_basicdata["cer_basicdata_firstyear"], $sales_units_watches_values))
		{
			$i = 0;
			$sd01 = number_format($sales_units_watches_values[$years[$i]], 0, ".", "'");
			$sd02 = number_format(round($e_rate*$total_gross_sales_values[$years[$i]]/1000,0), 0, ".", "'");
			$sd03 = number_format(round($e_rate*$operating_income01_values[$years[$i]]/1000,0), 0, ".", "'");
			$sd04 = number_format(round($e_rate*$operating_income02_values[$years[$i]]/1000,0), 0, ".", "'");
			$first_full_year_exists = false;
		}
	}
	

	//Logo
	$pdf->Image('../pictures/logo.jpg',10,8,33);
	//arialn bold 15
	$pdf->SetFont('arialn','B',18);
	//Move to the right
	$pdf->Cell(80);
	//Title
	$pdf->SetY(0);
	$pdf->Cell(0,33,$page_title,0,0,'R');
	//Line break
	$pdf->SetY(23);




	$pdf->setXY(10, $pdf->getY());
	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(97,4,"Address Data",1, 0, 'L', 1);
	$pdf->Cell(90,4,"Location Data",1, 0, 'L', 1);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->Cell(30,4,"POS Name:",1, 0, 'L', 0);
	$pdf->Cell(67,4,$posname,1, 0, 'L', 0);
	//$pdf->Cell(40,4,"Environment:",1, 0, 'L', 0);
	$pdf->Cell(90,4,$posareas,1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->Cell(30,4,"Country:",1, 0, 'L', 0);
	$pdf->Cell(67,4,$country_name,1, 0, 'L', 0);
	$pdf->Cell(40,4,"Placement:",1, 0, 'L', 0);
	$pdf->Cell(50,4,$placement,1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->Cell(30,4,"City:",1, 0, 'L', 0);	
	$pdf->Cell(67,4,$city_name,1, 0, 'L', 0);
	$pdf->Cell(40,4,"Total/Sales surface:",1, 0, 'L', 0);
	$pdf->Cell(50,4,$total_surface . " m2 / " . $sales_surface . " m2",1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->Cell(30,4,"Pos Type:",1, 0, 'L', 0);	
	$pdf->Cell(67,4,$postype,1, 0, 'L', 0);
	$pdf->Cell(40,4,"Project Type:",1, 0, 'L', 0);
	$pdf->Cell(50,4,$project_kind,1, 0, 'L', 0);
	$pdf->Ln();


	$x = $pdf->GetX();
	$y = $pdf->GetY() + 2;


	//picture 1
	$picture_printed = false;
	if($pix1 != ".." and file_exists($pix1))
	{
		if(substr($pix1, strlen($pix1)-3, 3) == "jpg" or substr($pix1, strlen($pix1)-3, 3) == "JPG")
		{
			
			$imagesize = getimagesize($pix1);
			$w = $imagesize[0];
			$h = $imagesize[1];

			$imgratio=$w/$h;

			if ($imgratio>1)
			{
				if($w >= 95)
				{
					$scale_factor = 95/$w;
					$h = $h*$scale_factor;
					$w = $w*$scale_factor;
					$pdf->Image($pix1,$x,$y, $w, $h);
					$y1 = $h;
				}
				else
				{
					$pdf->Image($pix1,$x,$y);
					$y1 = ($h / 72) * 25.4;
				}
				
			}
			else
			{
				if($w >= 71)
				{
					$scale_factor = 71/$h;
					$h = $h*$scale_factor;
					$w = $w*$scale_factor;
					$pdf->Image($pix1,$x,$y, $w, $h);
					$y1 = $h;
				}
				else
				{
					$pdf->Image($pix1,$x,$y);
					$y1 = ($h / 72) * 25.4;
				}
				
			}

			$pdf->SetY($y1 + $pdf->GetY() + 5);
			
			$picture_printed = true;
		}
	}

	


	$tmp_x = $pdf->getX();
	$tmp_y = $pdf->getY();


	$pdf->setXY(107, $y);
	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(90,4,"Sales Data",1, 0, 'L', 1);
	$pdf->Ln();
	
	$pdf->SetFont('arialn','',9);
	$pdf->setXY(107, $pdf->getY());
	if($first_full_year_exists == false)
	{
		$pdf->Cell(70,4,"Total watch units in first year:",1, 0, 'L', 0);	
	}
	else
	{
		$pdf->Cell(70,4,"Total watch units in first full year:",1, 0, 'L', 0);	
	}
	$pdf->Cell(20,4,$sd01,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->setXY(107, $pdf->getY());
	if($first_full_year_exists == false)
	{
		$pdf->Cell(70,4,"Gross Sales 1st year in KCHF:",1, 0, 'L', 0);	
	}
	else
	{
		$pdf->Cell(70,4,"Gross Sales 1st full year in KCHF:",1, 0, 'L', 0);	
	}
	$pdf->Cell(20,4,$sd02,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->setXY(107, $pdf->getY());
	if($first_full_year_exists == false)
	{
		$pdf->Cell(70,4,"Operating Income 1st year Retail:",1, 0, 'L', 0);	
	}
	else
	{
		$pdf->Cell(70,4,"Operating Income 1st full year Retail:",1, 0, 'L', 0);	
	}
	$pdf->Cell(20,4,$sd03,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->setXY(107, $pdf->getY());
	if($first_full_year_exists == false)
	{
		$pdf->Cell(70,4,"Operating Income1st year WS: ",1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(70,4,"Operating Income1st full year WS: ",1, 0, 'L', 0);	
	}
	$pdf->Cell(20,4,$sd04,1, 0, 'R', 0);
	$pdf->Ln();
	$pdf->Ln();

	$pdf->setXY(107, $pdf->getY());
	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(90,4,"KeyData",1, 0, 'L', 1);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->setXY(107, $pdf->getY());
	$pdf->Cell(70,4,"Deadline for property:",1, 0, 'L', 0);	
	$pdf->Cell(20,4,$deadline_for_property,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->setXY(107, $pdf->getY());
	
	if($project["project_projectkind"] == 4)
	{
		$pdf->Cell(70,4,"Planned take over date:",1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(70,4,"Planned opening date:",1, 0, 'L', 0);	
	}
	$pdf->Cell(20,4,$planned_opening_date,1, 0, 'R', 0);
	
	
	$pdf->Ln();

	$pdf->setXY(107, $pdf->getY());
	$pdf->Cell(70,4,"Keymoney in KCHF:",1, 0, 'L', 0);	
	$pdf->Cell(20,4,$keymoney,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->setXY(107, $pdf->getY());
	$pdf->Cell(70,4,"Deposit/Recoverable Keymoney in KCHF:",1, 0, 'L', 0);	
	$pdf->Cell(20,4,$depositposted,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->setXY(107, $pdf->getY());
	$pdf->Cell(70,4,"Rent Free Period in Weeks:",1, 0, 'L', 0);	
	$pdf->Cell(20,4,$free_weeks,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->setXY(107, $pdf->getY());
	$pdf->Cell(70,4,"Total rent 1st year in KCHF:",1, 0, 'L', 0);	
	$pdf->Cell(20,4,$fullrent_firstyear,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->setXY(107, $pdf->getY());
	
	if($project["project_projectkind"] == 2 or $project["project_projectkind"] == 3 or $project["project_projectkind"] == 4 )
	{
		$pdf->Cell(70,4,"Total rent last full year in KCHF:",1, 0, 'L', 0);	
		$pdf->Cell(20,4,$fullrent_lastyear,1, 0, 'R', 0);
		$pdf->Ln();
	}
	elseif($project["project_projectkind"] == 6 and $project["project_is_relocation_project"] == 1)
	{
		$pdf->Cell(70,4,"Total rent last full year in KCHF:",1, 0, 'L', 0);	
		$pdf->Cell(20,4,$fullrent_lastyear,1, 0, 'R', 0);
		$pdf->Ln();
	}
	else
	{
		//$pdf->Cell(70,4,"",0, 0, 'L', 0);	
		//$pdf->Cell(20,4,"",0, 0, 'R', 0);
	}
	
	$pdf->Ln();


	$pdf->setXY(107, $pdf->getY());
	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(90,4,"Contract Period:",1, 0, 'L', 1);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->setXY(107, $pdf->getY());
	$pdf->Cell(70,4,"Contract Duration:",1, 0, 'L', 0);	
	$pdf->Cell(20,4,$duration_in_years_and_months,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->setXY(107, $pdf->getY());
	$pdf->Cell(70,4,"Contract Starting Date:",1, 0, 'L', 0);	
	$pdf->Cell(20,4,$contract_starting_date,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->setXY(107, $pdf->getY());
	$pdf->Cell(70,4,"Contract Ending Date:",1, 0, 'L', 0);	
	$pdf->Cell(20,4,$contract_ending_date,1, 0, 'R', 0);
	$pdf->Ln();



	$pdf->setX(10);



	if($picture_printed == false)
	{
		$pdf->setY($tmp_y+85);
	}
	else
	{
		if($tmp_y < $pdf->getY())
		{
			$pdf->setY($pdf->getY()+5);
		}
		else
		{		
			$pdf->setY($tmp_y+5);
		}
	}

	
	

	$pdf->setXY(10, $pdf->getY());
	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(187,4,"Neighbourhood",1, 0, 'L', 1);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->Cell(30,4,"Left side:",1, 0, 'L', 0);
	$pdf->Cell(67,4,$neighbourhoods["Shop on left side"],1, 0, 'L', 0);
	$pdf->Cell(90,4,"Other brands in the area:",1, 0, 'L', 0);
	$pdf->Ln();

	$tmp_y = $pdf->getY();

	$pdf->Cell(30,4,"Right side:",1, 0, 'L', 0);
	$pdf->Cell(67,4,$neighbourhoods["Shop on right side"],1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->Cell(30,4,"Opposite Left:",1, 0, 'L', 0);
	$pdf->Cell(67,4,$neighbourhoods["Shop across left side"],1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->Cell(30,4,"Opposite right:",1, 0, 'L', 0);
	$pdf->Cell(67,4,$neighbourhoods["Shop across right side"],1, 0, 'L', 0);
	$pdf->Ln();
	$pdf->Ln();

	$y = $pdf->getY();
	$pdf->setXY(107,$tmp_y);


	$pdf->MultiCell(90,12.5, $neighbourhoods["Other brands in area"], 1, "T");

	$pdf->setXY(10,$y);

	

	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(187,7,"Key points",1, 0, 'L', 1);
	$pdf->Ln();
	$pdf->SetFont('arialn','',10);
	$pdf->MultiCell(187,3.5, $ln_basicdata["ln_basicdata_remarks"], 1, "T");




	$y = $pdf->GetY()+5;
	$pdf->SetY($y);

	$x1 = $pdf->GetX();
	$pdf->Cell(90,17,"",1, 0, 'L', 0);
	$pdf->SetX($x1);


	$y = $pdf->GetY();

	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(60,7,"Approval",0, 0, 'L', 0);
	$pdf->Cell(30,7,"YES  NO",0, 0, 'L', 0);

	$x1 = $pdf->GetX()-28;
	$y1 = $pdf->GetY()+7.5;

	
	$radiobutton_on = "../pictures/ico_crossed.jpg";
	//$radiobutton_off = "../pictures/radio_off.jpg";

	if($approvedby_cm == 1)
	{
		$pdf->Image($radiobutton_on,$x1,$y1, 3.5);
		//$pdf->Image($radiobutton_off,$x1+9,$y1, 3.5);
	}
	else
	{
		//$pdf->Image($radiobutton_off,$x1,$y1, 3.5);
		$pdf->Image($radiobutton_on,$x1+9,$y1, 3.5);
	}
	
	if($approved_by_ekl == 1)
	{
		$pdf->Image($radiobutton_on,$x1,$y1+5, 3.5);
		//$pdf->Image($radiobutton_off,$x1+9,$y1+5, 3.5);
	}
	else
	{
		//$pdf->Image($radiobutton_off,$x1,$y1+5, 3.5);
		$pdf->Image($radiobutton_on,$x1+9,$y1+5, 3.5);
	}
	
	$pdf->Ln();

	

	$pdf->SetFont('arialn','',9);
	$pdf->Cell(90,5,"Approved by Country Manager:",0, 0, 'L', 0);
	$pdf->Ln();
	$pdf->SetFont('arialn','',9);
	$pdf->Cell(90,5,"Approved by EKL Responsable:",0, 0, 'L', 0);
	$pdf->Ln();
	$pdf->Ln();

	
	

	
	$pdf->SetFont('arialn','',9);
	$pdf->Cell(55,9,"Brand Manager, " . $brand_manager,1, 0, 'L', 0);
	$pdf->Cell(35,9,"" ,1, 0, 'L', 0);
	$pdf->Ln();
	
	$x = 107;
	$pdf->setY($y);
	$pdf->setX($x);
	
	

	$pdf->SetFont('freesans','',7);

	$tmp_y = $pdf->getY($y);
	$tmp_x = $pdf->getX($x);

	$pdf->Cell(90,9,"" ,1, 'L');
	$pdf->setY($tmp_y);
	$pdf->setX($tmp_x);
	$pdf->MultiCell(55,9,"Regional Sales Manager, " . $sales_manager,1, 'T');
	
	//$pdf->Ln();

	$pdf->setX($x);
	$pdf->SetFont('freesans','',7);
	if($retail_head)
	{
		$pdf->Cell(55,9,"Merchandising Manager, " . $retail_head,1, 0, 'L', 0);
	}
	elseif($vp_sales)
	{
		$pdf->Cell(55,9,"VP Sales, " . $vp_sales,1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(55,9,"",1, 0, 'L', 0);
	}
	$pdf->Cell(35,9,"" ,1, 0, 'L', 0);
	$pdf->Ln();
	
	$pdf->setX($x);
	$pdf->SetFont('freesans','',7);
	if($retail_head and $vp_sales)
	{
		$pdf->Cell(55,9,"VP Sales, " . $vp_sales,1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(55,9,"",1, 0, 'L', 0);
	}

	$pdf->Cell(35,9,"" ,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->setX($x);
	$pdf->SetFont('freesans','',7);

	if($retail_head and $vp_sales)
	{
		$pdf->Cell(55,9,"President, " . $president,1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(55,9,"",1, 0, 'L', 0);
	}
	$pdf->Cell(35,9,"" ,1, 0, 'L', 0);
	$pdf->Ln();

	

	

	//Position at 1.5 cm from bottom
	$pdf->SetY(282);
	$pdf->SetFont('arialn','I',8);
	$pdf->Cell(0,10, to_system_date(date("d.m.y")),0,0,'R');





	//page 2
	$pdf->AddPage("P", "A4");


	//Logo
	$pdf->Image('../pictures/logo.jpg',10,8,33);
	//arialn bold 15
	$pdf->SetFont('arialn','B',18);
	//Move to the right
	$pdf->Cell(80);
	//Title
	$pdf->SetY(0);
	$pdf->Cell(0,33,$page_title,0,0,'R');
	//Line break
	$pdf->SetY(23);


	$pdf->setXY(10, $pdf->getY());
	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(97,4,"Address Data",1, 0, 'L', 1);
	$pdf->Cell(90,4,"Location Data",1, 0, 'L', 1);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->Cell(30,4,"POS Name:",1, 0, 'L', 0);
	$pdf->Cell(67,4,$posname,1, 0, 'L', 0);
	//$pdf->Cell(40,4,"Environment:",1, 0, 'L', 0);
	$pdf->Cell(90,4,$posareas,1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->Cell(30,4,"Country:",1, 0, 'L', 0);
	$pdf->Cell(67,4,$country_name,1, 0, 'L', 0);
	$pdf->Cell(40,4,"Placement:",1, 0, 'L', 0);
	$pdf->Cell(50,4,$placement,1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->Cell(30,4,"City:",1, 0, 'L', 0);	$pdf->Cell(67,4,$city_name,1, 0, 'L', 0);
	$pdf->Cell(40,4,"Total/Sales surface:",1, 0, 'L', 0);
	$pdf->Cell(50,4,$total_surface . " m2 / " . $sales_surface . " m2",1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->Cell(30,4,"Pos Type:",1, 0, 'L', 0);	
	$pdf->Cell(67,4,$postype,1, 0, 'L', 0);
	$pdf->Cell(40,4,"Project Type:",1, 0, 'L', 0);
	$pdf->Cell(50,4,$project_kind,1, 0, 'L', 0);
	$pdf->Ln();
	$pdf->Ln();


	
	//Sales data
	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(97,5,"Sales Data",1, 0, 'L', 1);
	for($i=0;$i<4;$i++)
	{
		if(array_key_exists($i, $years))
		{
			if($i == 0)
			{
				$pdf->Cell(22.5,5,$years[$i] . "(" . $number_of_months_first_year . ")",1, 0, 'R', 0);
			}
			elseif($i == 3)
			{
				$pdf->Cell(22.5,5,$years[$i] . "(" . $number_of_months_last_year . ")",1, 0, 'R', 0);
			}
			else
			{
				$pdf->Cell(22.5,5,$years[$i],1, 0, 'R', 0);
			}
		}
		else
		{
			$pdf->Cell(22.5,5," ",1, 0, 'R', 0);
		}
	}
	
	$pdf->Ln();

	
	$x = $pdf->GetX();
	$pdf->Cell(187,25,"",1, 0, 'L', 0);
	$pdf->SetX($x);

	$pdf->Cell(97,10," ",0, 0, 'R', 0);
	for($i=0;$i<4;$i++)
	{
		$pdf->Cell(22.5,10," ",1, 0, 'R', 0);
	}
	

	$pdf->SetX($x);

	$pdf->SetFont('arialn','',9);
	$pdf->Cell(97,5,"Total Gross Sales in KCHF" ,0, 0, 'L', 0);

	for($i=0;$i<4;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $total_gross_sales_values))
		{
			$pdf->Cell(22.5,5,number_format(round($e_rate*$total_gross_sales_values[$years[$i]]/1000,0), 0, ".", "'"),0, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(22.5,5," ",0, 0, 'R', 0);
		}
	}
	$pdf->Ln();

	
	$pdf->Cell(97,5,"Total Gross Margin in KCHF",0, 0, 'L', 0);
	for($i=0;$i<4;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $total_gross_margin_values))
		{
			$pdf->Cell(22.5,5,number_format(round($e_rate*$total_gross_margin_values[$years[$i]]/1000,0), 0, ".", "'"),0, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(22.5,5," ",0, 0, 'R', 0);
		}
	}
	$pdf->Ln();

	$pdf->Cell(97,5,"Total Indirect Expenses in KCHF",1, 0, 'L', 0);
	for($i=0;$i<4;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $total_indirect_expenses_values))
		{
			$pdf->Cell(22.5,5,number_format(round(-1*$e_rate*$total_indirect_expenses_values[$years[$i]]/1000,0), 0, ".", "'"),1, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(22.5,5," ",1, 0, 'R', 0);
		}
	}
	$pdf->Ln();


	$pdf->Cell(97,10," ",0, 0, 'R', 0);
	for($i=0;$i<4;$i++)
	{
		$pdf->Cell(22.5,10," ",1, 0, 'R', 0);
	}

	$pdf->SetX($x);

	$pdf->Cell(97,5,"Operating Income without WS Margin in KCHF",0, 0, 'L', 0);
	for($i=0;$i<4;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $operating_income01_values))
		{
			$pdf->Cell(22.5,5,number_format(round($e_rate*$operating_income01_values[$years[$i]]/1000,0), 0, ".", "'"),0, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(22.5,5," ",0, 0, 'R', 0);
		}
	}
	$pdf->Ln();

	$pdf->Cell(97,5,"Operating Income incl. WS Margin in KCHF",0, 0, 'L', 0);
	for($i=0;$i<4;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $operating_income02_values))
		{
			$pdf->Cell(22.5,5,number_format(round($e_rate*$operating_income02_values[$years[$i]]/1000,0), 0, ".", "'"),0, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(22.5,5," ",0, 0, 'R', 0);
		}
	}
	$pdf->Ln();
	$pdf->Ln();


	//estimates sellouts
	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(97,5,"Estimated sell out (planned units)",1, 0, 'L', 1);
	for($i=0;$i<4;$i++)
	{
		if(array_key_exists($i, $years))
		{
			if($i == 0)
			{
				$pdf->Cell(22.5,5,$years[$i] . "(" . $number_of_months_first_year . ")",1, 0, 'R', 0);
			}
			elseif($i == 3)
			{
				$pdf->Cell(22.5,5,$years[$i] . "(" . $number_of_months_last_year . ")",1, 0, 'R', 0);
			}
			else
			{
				$pdf->Cell(22.5,5,$years[$i],1, 0, 'R', 0);
			}
		}
		else
		{
			$pdf->Cell(22.5,5," ",1, 0, 'R', 0);
		}
	}
	
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$x = $pdf->GetX();
	$pdf->Cell(187,15,"",1, 0, 'L', 0);
	$pdf->SetX($x);

	$pdf->Cell(97,15," ",1, 0, 'R', 0);
	for($i=0;$i<4;$i++)
	{
		$pdf->Cell(22.5,15," ",1, 0, 'R', 0);
	}

	$pdf->SetX($x);

	$pdf->Cell(97,5,"Watches",1, 0, 'L', 0);
	
	for($i=0;$i<4;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $sales_units_watches_values) and $sales_units_watches_values[$years[$i]] > 0)
		{
			$pdf->Cell(22.5,5,number_format($sales_units_watches_values[$years[$i]], 0, ".", "'"),1, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(22.5,5," ",1, 0, 'R', 0);
		}
	}
	$pdf->Ln();

	$pdf->Cell(97,5,"Bjoux",1, 0, 'L', 0);
	for($i=0;$i<4;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $sales_units_jewellery_values) and $sales_units_jewellery_values[$years[$i]] > 0)
		{
			$pdf->Cell(22.5,5,number_format($sales_units_jewellery_values[$years[$i]], 0, ".", "'"),1, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(22.5,5," ",1, 0, 'R', 0);
		}
	}
	$pdf->Ln();
	$pdf->Ln();


	$x = $pdf->GetX();
	$pdf->Cell(97,17,"",1, 0, 'L', 0);
	$pdf->SetX($x);
	
	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(97,7,"Total Investment in fixed assets in KCHF",1, 0, 'L', 1);
	$pdf->SetFont('arialn','',9);
	$pdf->Cell(22.5,7,number_format(round($e_rate*$investment_total/1000,0), 0, ".", "'"),1, 0, 'R', 0);

	$pdf->Ln();
    
	
	foreach($fixed_assets as $key=>$itype)
	{
		if($itype == 1 and array_key_exists($itype, $investment_names))
		{
			$pdf->Cell(97,5," of which - " . $investment_names[$itype] . " in KCHF",0, 0, 'L', 0);
			$pdf->Cell(22.5,5,number_format(round($e_rate*$amounts[$itype] / 1000,0), 0, ".", "'"),1, 0, 'R', 0);
			$pdf->Ln();
		}
		if($itype == 3 and array_key_exists($itype, $investment_names))
		{
			$pdf->Cell(97,5," of which - " . $investment_names[$itype] . " in KCHF",0, 0, 'L', 0);
			$pdf->Cell(22.5,5,number_format(round($e_rate*$amounts[$itype] / 1000,0), 0, ".", "'"),1, 0, 'R', 0);
			$pdf->Ln();
		}
		
	    
	}

	$pdf->Ln();
	$pdf->Ln();


	//watches sold out in the past
	// renovation or tekover/renovation or lease renewal or new relocation project
	if(($project["project_projectkind"] == 2 
        or $project["project_projectkind"] == 3 
		or $project["project_projectkind"] == 5)
		or ($project["project_projectkind"] == 6 and $project["project_is_relocation_project"] == 1 and $project["project_relocated_posaddress_id"] > 0)
	   )

	{
	
		$sellout_ending_year = (int)substr($cer_basicdata["date_created"], 0, 4);
		if($sellout_ending_year < (int)substr($cer_basicdata["cer_basicdata_firstyear"], 0, 4))
		{
			$sellout_ending_year =(int)substr($cer_basicdata["cer_basicdata_firstyear"], 0, 4);
		}


		if($sellout_ending_year > date("Y"))
		{
			$sellout_ending_year = date("Y");
		}

		$sellout_starting_year = $sellout_ending_year - 3;

		$pdf->SetFont('arialn','B',9);
		$pdf->Cell(97,5,"Historical sell out (sold units)",1, 0, 'L', 1);

		for($i=$sellout_starting_year;$i<=($sellout_ending_year);$i++)
		{
			if(array_key_exists($i, $sellouts_watches))
			{
				$pdf->Cell(22.5,5,$i . "(" . $sellouts_months[$i] . ")",1, 0, 'R', 0);
			}
			else
			{
				$pdf->Cell(22.5,5,$i,1, 0, 'R', 0);
			}
		}
		
		
		$pdf->Ln();

		$pdf->SetFont('arialn','B',9);
		$x = $pdf->GetX();
		$pdf->Cell(187,15,"",1, 0, 'L', 0);
		$pdf->SetX($x);

		$pdf->Cell(97,15," ",1, 0, 'R', 0);
		for($i=0;$i<4;$i++)
		{
			$pdf->Cell(22.5,10," ",1, 0, 'R', 0);
		}

		$pdf->SetX($x);

		$pdf->Cell(97,5,"Watches",1, 0, 'L', 0);
		
		$pdf->SetFont('arialn','',9);


		for($i=$sellout_starting_year;$i<=($sellout_ending_year);$i++)
		{
			if(array_key_exists($i, $sellouts_watches))
			{
				$pdf->Cell(22.5,5,number_format($sellouts_watches[$i], 0, ".", "'"),1, 0, 'R', 0);
			}
			else
			{
				$pdf->Cell(22.5,5," ",1, 0, 'R', 0);
			}
		}
		$pdf->Ln();

		$pdf->SetFont('arialn','B',9);
		$pdf->Cell(97,5,"Bjoux",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',9);
		for($i=$sellout_starting_year;$i<=($sellout_ending_year);$i++)
			{
			if(array_key_exists($i, $sellouts_bjoux))
			{
				$pdf->Cell(22.5,5,number_format($sellouts_bjoux[$i], 0, ".", "'"),1, 0, 'R', 0);
			}
			else
			{
				$pdf->Cell(22.5,5," ",1, 0, 'R', 0);
			}
		}
		$pdf->Ln();
		$pdf->Ln();
	}
	

	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(187,7,"Negotiated rental conditions",1, 0, 'L', 1);
	$pdf->Ln();
	$pdf->SetFont('arialn','',9);
	$pdf->MultiCell(187,3.5, $negotiated_rental_conditions, 1, "T");
	


	$pdf->setXY(10, $pdf->getY()+7);
	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(97,4,"Rental Contract",1, 0, 'L', 1);
	$pdf->Cell(90,4,"Additional rental costs",1, 0, 'L', 1);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->Cell(50,4,"Renewal Option in  years:",1, 0, 'L', 0);
	$pdf->Cell(47,4,$renewal_option,1, 0, 'L', 0);
	$pdf->Cell(45,4,"Index rate in %:",1, 0, 'L', 0);
	$pdf->Cell(45,4,$indexrate,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->Cell(50,4,"Exit option date:",1, 0, 'L', 0);
	$pdf->Cell(47,4,$exit_option,1, 0, 'L', 0);
	$pdf->Cell(45,4,"Yearly increase in %:",1, 0, 'L', 0);
	$pdf->Cell(45,4,$average_yearly_increase,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->Cell(50,4,"Notice Period in months:",1, 0, 'L', 0);
	$pdf->Cell(47,4,$termination_time,1, 0, 'L', 0);
	$pdf->Cell(45,4,"Real estate fees in % of KM:",1, 0, 'L', 0);
	$pdf->Cell(45,4,$real_estate_fee,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->Cell(50,4,"Index clause:",1, 0, 'L', 0);
	$pdf->Cell(47,4,$index_clause,1, 0, 'L', 0);
	$pdf->Cell(45,4,"Annual charges in KCHF:",1, 0, 'L', 0);
	$pdf->Cell(45,4,$annual_charges,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->Cell(50,4,"Tacit renewal clause:",1, 0, 'L', 0);
	$pdf->Cell(47,4,$index_clause2,1, 0, 'L', 0);
	$pdf->Cell(45,4,"",1, 0, 'L', 0);
	$pdf->Cell(45,4,"",1, 0, 'R', 0);
	$pdf->Ln();
	

	//Position at 1.5 cm from bottom
	$pdf->SetY(282);
	$pdf->SetFont('arialn','I',8);
	$pdf->Cell(0,10, to_system_date(date("d.m.y")),0,0,'R');


	//Page 3 Floor Plan

	$source_file = $floor_plan;
	if(file_exists($source_file))
	{
		
		//PDF
		if(substr($source_file, strlen($source_file)-3, 3) == "pdf" or substr($source_file, strlen($source_file)-3, 3) == "PDF")
		{
			$layout = new FPDI();
			$num_of_pages = $pdf->setSourceFile($source_file);
			$num_of_pages = $layout->setSourceFile($source_file);
			for($i=1;$i<=$num_of_pages;$i++)
			{
				$tplIdx = $pdf->importPage($i);
				$layout->AddPage("L");
				$tplIdx = $layout->importPage($i);
				
				$tpl = $layout->useTemplate($tplIdx);
				//$pdf->AddPage("", array(0=>$tpl["w"], 1=>$tpl["h"]));

				if($tpl["w"] > $tpl["h"])
				{
					$pdf->AddPage("L", array(0=>$tpl["h"], 1=>$tpl["w"]));
				}
				else
				{
					$pdf->AddPage("P", array(0=>$tpl["w"], 1=>$tpl["h"]));
				}
				
				/*
				if($tpl["w"] > $tpl["h"])
				{
					$pdf->AddPage("L", array(0=>$tpl["w"], 1=>$tpl["h"]));
				}
				else
				{
					$pdf->AddPage("P", array(0=>$tpl["w"], 1=>$tpl["h"]));
				}
				*/
				$tpl = $pdf->useTemplate($tplIdx);
			}
		}
		elseif(substr($source_file, strlen($source_file)-3, 3) == "jpg" or substr($source_file, strlen($source_file)-3, 3) == "JPG")
		{
			$pdf->AddPage("P");
			$pdf->Image('../pictures/logo.jpg',10,8,33);
			//arialn bold 15
			$pdf->SetFont('arialn','B',12);
			//Move to the right
			$pdf->Cell(80);
			//Title
			$pdf->SetY(0);
			$pdf->Cell(0,33,$page_title,0,0,'R');
			//Line break
			$pdf->SetY(23);
			$pdf->Image($source_file,10,30, 180);

			//Position at 1.5 cm from bottom
			$pdf->SetY(282);
			//arialn italic 8
			$pdf->SetFont('arialn','I',8);
			//Page number
			$pdf->Cell(0,10, to_system_date(date("d.m.y")),0,0,'R');
		}
	}



	//Position at 1.5 cm from bottom
	$pdf->SetY(282);
	$pdf->SetFont('arialn','I',8);
	$pdf->Cell(0,10, to_system_date(date("d.m.y")),0,0,'R');
	
?>