<?php
/********************************************************************

    cer_benchmarks.php

    Entry page for the cer benchmark section.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-26
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-26
    Version:        1.1.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("has_access_to_cer_benchmarks");
set_referer("cer_benchmark.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data


//create a copy of an existing query
if(param("copy_id"))
{
	$sql = "select * from cer_benchmarks where cer_benchmark_id = " . param("copy_id");
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$sql_i = "Insert into cer_benchmarks (" . 
			     "cer_benchmark_user_id, " . 
			     "cer_benchmark_shortcut, " . 
			     "cer_benchmark_title, " . 
			     "cer_benchmark_filter, " . 
			     "cer_benchmark_project_filter, " . 
			     "cer_benchmark_basic_currency, " . 
			     "cer_benchmark_include_pipeline, " . 
			     "cer_benchmark_last_project, " . 
			     "cer_benchmark_project_state, " . 
			     "cer_benchmark_cer_state, " . 
			     "cer_benchmark_af_state, " . 
			     "cer_benchmark_from_year, " . 
			     "cer_benchmark_to_year, " .
			     "cer_benchmark_from_state, " .
			     "cer_benchmark_to_state, " .
			     "user_created, " . 
			     "date_created) VALUES (" . 
			     user_id() . ", " .
				 dbquote($row["cer_benchmark_shortcut"] .  " - copy") . ", " .
			     dbquote($row["cer_benchmark_title"].  " - copy") . ", " . 
			     dbquote($row["cer_benchmark_filter"]) . ", " . 
			     dbquote($row["cer_benchmark_project_filter"]) . ", " . 
			     dbquote($row["cer_benchmark_basic_currency"]) . ", " . 
			     dbquote($row["cer_benchmark_include_pipeline"]) . ", " . 
			     dbquote($row["cer_benchmark_last_project"]) . ", " . 
			     dbquote($row["cer_benchmark_project_state"]) . ", " . 
			     dbquote($row["cer_benchmark_cer_state"]) . ", " . 
			     dbquote($row["cer_benchmark_af_state"]) . ", " . 
			     dbquote($row["cer_benchmark_from_year"]) . ", " . 
			     dbquote($row["cer_benchmark_to_year"]) . ", " . 
			     dbquote($row["cer_benchmark_from_state"]) . ", " . 
			     dbquote($row["cer_benchmark_to_state"]) . ", " . 
			     dbquote(user_login()) . ", " .
			     "CURRENT_TIMESTAMP)";
		$result = mysql_query($sql_i);
	}
}

// create sql
$sql = "select cer_benchmark_id, cer_benchmark_shortcut, cer_benchmark_title,  cer_benchmark_user_id, " . 
       "cer_benchmarks.date_created as cdate, cer_benchmarks.date_modified as mdate, " .
	   "concat(user_name, ' ', user_firstname) as uname " . 
       "from cer_benchmarks " .
	   "left join users on user_id = cer_benchmark_user_id";


$benchmark_links = array();
$copyk_links = array();
$benchmark_permissions = array();
$queries1 = array();
$queries2 = array();
$queries3 = array();
$queries4 = array();
$queries5 = array();
$queries6 = array();

//$sql_a = $sql . " where cer_benchmark_user_id = " . user_id();
$res = mysql_query($sql) or dberror($sql);

//$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
	if(user_id() == $row["cer_benchmark_user_id"]) {
		
		$benchmark_links[$row["cer_benchmark_id"]] = "<a href='cer_benchmark.php?id=" .  $row["cer_benchmark_id"] . "'>" . $row["cer_benchmark_shortcut"] . "</a>";
	}
	else
	{
		$benchmark_links[$row["cer_benchmark_id"]] = $row["cer_benchmark_shortcut"];	
	}

	$copyk_links[$row["cer_benchmark_id"]] = "<a href='cer_benchmarks.php?copy_id=" .  $row["cer_benchmark_id"] . "'>create copy</a>";

	$url = "cer_benchmark_01_xls.php?id=" . $row["cer_benchmark_id"];
	$link = "javascript:popup('" . $url . "', 1024, 768)";
	$link = '<a title="Benchmark horizontal" href="' . $link . '"><img src="/pictures/ico_xls.gif" border="0"/></a>';

	$queries1[$row["cer_benchmark_id"]] = $link;

	$url = "cer_benchmark_02_xls.php?id=" . $row["cer_benchmark_id"];
	$link = "javascript:popup('" . $url . "', 1024, 768)";
	$link = '<a title="Human Resources" href="' . $link . '"><img src="/pictures/ico_xls.gif" border="0"/></a>';

	$queries2[$row["cer_benchmark_id"]] = $link;

	$url = "cer_benchmark_03_xls.php?id=" . $row["cer_benchmark_id"];
	$link = "javascript:popup('" . $url . "', 1024, 768)";
	$link = '<a title="Rental" href="' . $link . '"><img src="/pictures/ico_xls.gif" border="0"/></a>';

	$queries3[$row["cer_benchmark_id"]] = $link;

	$url = "cer_benchmark_04_xls.php?id=" . $row["cer_benchmark_id"];
	$link = "javascript:popup('" . $url . "', 1024, 768)";
	$link = '<a title="Environment" href="' . $link . '"><img src="/pictures/ico_xls.gif" border="0"/></a>';

	$queries4[$row["cer_benchmark_id"]] = $link;

	$url = "cer_benchmark_05_xls.php?id=" . $row["cer_benchmark_id"];
	$link = "javascript:popup('" . $url . "', 1024, 768)";
	$link = '<a title="Investments" href="' . $link . '"><img src="/pictures/ico_xls.gif" border="0"/></a>';

	$queries5[$row["cer_benchmark_id"]] = $link;

	$url = "cer_benchmark_06_xls.php?id=" . $row["cer_benchmark_id"];
	$link = "javascript:popup('" . $url . "', 1024, 768)";
	$link = '<a title="Revenues" href="' . $link . '"><img src="/pictures/ico_xls.gif" border="0"/></a>';

	$queries6[$row["cer_benchmark_id"]] = $link;

	//get permissions
	$sql_p = "select * from cer_benchmarkpermissions " .
			 "left join cer_benchmarks on cer_benchmark_id = cer_benchmarkpermission_benchmark " . 
			 "where  cer_benchmarkpermission_benchmark = " . $row["cer_benchmark_id"];

	$res_p = mysql_query($sql_p) or dberror($sql_p);
	while ($row_p = mysql_fetch_assoc($res_p))
	{
		if($row_p["cer_benchmarkpermission_user"] == user_id())
		{
			$benchmark_permissions[$row_p["cer_benchmarkpermission_benchmark"]] = $row_p["cer_benchmarkpermission_benchmark"];
		}
	}
}


$list_filter = "cer_benchmark_user_id = " . user_id();
if(count($benchmark_permissions) > 0)
{
	$permission_filter =  implode(',', $benchmark_permissions);
	$list_filter .= " or cer_benchmark_id in (" . $permission_filter . ") ";
}


/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);

$list->set_entity("cer_benchmarks");
$list->set_order("cer_benchmark_shortcut");
$list->set_filter($list_filter);


$list->add_text_column("benchmarklinks", "Shortcut", COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $benchmark_links);
$list->add_column("cer_benchmark_title", "Title", "", "", "", COLUMN_NO_WRAP);
$list->add_column("uname", "Owner", "", "", "", COLUMN_NO_WRAP);
$list->add_column("cdate", "Created", "", "", "", COLUMN_NO_WRAP);
$list->add_column("mdate", "Modified", "", "", "", COLUMN_NO_WRAP);

$list->add_text_column("q1", "HOR", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML, $queries1);
$list->add_text_column("q2", "HR", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML, $queries2);
$list->add_text_column("q3", "REN", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML, $queries3);
$list->add_text_column("q4", "ENV", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML, $queries4);
$list->add_text_column("q5", "INV", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML, $queries5);
$list->add_text_column("q6", "REV", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML, $queries6);

$list->add_text_column("benchmarklinks", "", COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $copyk_links);

$list->add_button(LIST_BUTTON_NEW, "Add New Benchmark", "cer_benchmark.php");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->process();

$page = new Page("cer_benchmarks");

$page->header();
$page->title("Benchmarks");
$list->render();
$page->footer();
?>