<?php
/********************************************************************

    cer_edit_approval_names.php

    Edit CER Approval names for a projects.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-04-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-04-15
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";

check_access("has_full_access_to_cer");


//get cer approval names
$cer_approval_names1 = array();
$cer_approval_names2 = array();
$sql = "select cer_basicdata_approvalname1, cer_basicdata_approvalname2, " . 
       "cer_basicdata_approvalname3, cer_basicdata_approvalname4, " . 
	   "cer_basicdata_approvalname5, cer_basicdata_approvalname6, " . 
	   "cer_basicdata_approvalname7, cer_basicdata_approvalname8, " . 
	   "cer_basicdata_approvalname9, cer_basicdata_approvalname10, " . 
	   "cer_basicdata_approvalname11 " .
       "from cer_basicdata " . 
	   "where cer_basicdata_version = 0 and cer_basicdata_project = " . param("pid");

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$cer_approval_names1 = $row;
}

$sql = "select cer_summary_in01_sig01, cer_summary_in01_sig02, " . 
       "cer_summary_in01_sig03, cer_summary_in01_sig04, cer_summary_in01_sig05, cer_summary_in01_sig06, cer_summary_in01_sig07, cer_summary_in01_sig10 " . 
       "from cer_summary " . 
	   "where cer_summary_cer_version = 0 and cer_summary_project = " . param("pid");

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$cer_approval_names2 = $row;
}


//get ln approval names


//get approval names for refunding


/********************************************************************
   cer approval names
*********************************************************************/

$form = new Form("cer_approvalnames", "Approval Names");



$form->add_edit("cer_basicdata_approvalname1", "Retail Investment Comitee", 0, $cer_approval_names1["cer_basicdata_approvalname1"] );
$form->add_edit("cer_basicdata_approvalname5", "Swatch Group HQ Retail Development", 0, $cer_approval_names1["cer_basicdata_approvalname5"] );
$form->add_edit("cer_basicdata_approvalname6", "Swatch Group HQ Retail Controlling", 0, $cer_approval_names1["cer_basicdata_approvalname6"] );

$form->add_edit("cer_basicdata_approvalname2", BRAND . " CEO", 0, $cer_approval_names1["cer_basicdata_approvalname2"]);
$form->add_edit("cer_basicdata_approvalname7", BRAND . " VP Finance", 0, $cer_approval_names1["cer_basicdata_approvalname7"]);
$form->add_edit("cer_basicdata_approvalname3", BRAND . " VP Sales", 0, $cer_approval_names1["cer_basicdata_approvalname3"] );
$form->add_edit("cer_basicdata_approvalname4", BRAND . " Merchandising Manager", 0, $cer_approval_names1["cer_basicdata_approvalname4"] );
$form->add_edit("cer_basicdata_approvalname9", BRAND . " Retail Controller", 0, $cer_approval_names1["cer_basicdata_approvalname9"] );

$form->add_edit("cer_summary_in01_sig01", "Country Manager", 0, $cer_approval_names2["cer_summary_in01_sig01"] );
$form->add_edit("cer_summary_in01_sig04", "Country VP", 0, $cer_approval_names2["cer_summary_in01_sig04"] );
$form->add_edit("cer_summary_in01_sig02", "Finance/Service Center Manager", 0, $cer_approval_names2["cer_summary_in01_sig02"] );
$form->add_edit("cer_summary_in01_sig03", "Brand Manager", 0, $cer_approval_names2["cer_summary_in01_sig03"] );
$form->add_edit("cer_summary_in01_sig05", "Country Retail Development", 0, $cer_approval_names2["cer_summary_in01_sig05"] );
$form->add_edit("cer_summary_in01_sig06", "Regional Sales Manager", 0, $cer_approval_names2["cer_summary_in01_sig06"] );
$form->add_edit("cer_summary_in01_sig10", "Head of Controlling (country)", 0, $cer_approval_names2["cer_summary_in01_sig10"] );

$form->add_edit("cer_summary_in01_sig07", "Finance/Service Center Manager", 0, $cer_approval_names2["cer_summary_in01_sig07"] );

$form->add_hidden("pid", param("pid"));


$form->add_button("save", "Save");

$form->populate();
$form->process();

if($form->button("save"))
{
	//update cer_basic_data
	$fields = array();
	$value = dbquote($form->value("cer_basicdata_approvalname1"));
	$fields[] = "cer_basicdata_approvalname1 = " . $value;

	$value = dbquote($form->value("cer_basicdata_approvalname2"));
	$fields[] = "cer_basicdata_approvalname2 = " . $value;

	$value = dbquote($form->value("cer_basicdata_approvalname3"));
	$fields[] = "cer_basicdata_approvalname3 = " . $value;

	$value = dbquote($form->value("cer_basicdata_approvalname4"));
	$fields[] = "cer_basicdata_approvalname4 = " . $value;

	$value = dbquote($form->value("cer_basicdata_approvalname5"));
	$fields[] = "cer_basicdata_approvalname5 = " . $value;

	$value = dbquote($form->value("cer_basicdata_approvalname6"));
	$fields[] = "cer_basicdata_approvalname6 = " . $value;

	$value = dbquote($form->value("cer_basicdata_approvalname7"));
	$fields[] = "cer_basicdata_approvalname7 = " . $value;

	$value = dbquote($form->value("cer_basicdata_approvalname9"));
	$fields[] = "cer_basicdata_approvalname9 = " . $value;

	

	$value = "current_timestamp";
	$fields[] = "date_modified = " . $value;

	$value = $_SESSION["user_login"];
	$fields[] = "user_modified = " . dbquote($value);

	$sql = "update cer_basicdata set " . join(", ", $fields) . " where cer_basicdata_version = 0 and cer_basicdata_project = " . param("pid");
	mysql_query($sql) or dberror($sql);

	//update cer_summary
	$fields = array();

	$value = dbquote($form->value("cer_summary_in01_sig01"));
	$fields[] = "cer_summary_in01_sig01 = " . $value;

	$value = dbquote($form->value("cer_summary_in01_sig02"));
	$fields[] = "cer_summary_in01_sig02 = " . $value;

	$value = dbquote($form->value("cer_summary_in01_sig03"));
	$fields[] = "cer_summary_in01_sig03 = " . $value;

	$value = dbquote($form->value("cer_summary_in01_sig04"));
	$fields[] = "cer_summary_in01_sig04 = " . $value;

	$value = dbquote($form->value("cer_summary_in01_sig05"));
	$fields[] = "cer_summary_in01_sig05 = " . $value;

	$value = dbquote($form->value("cer_summary_in01_sig06"));
	$fields[] = "cer_summary_in01_sig06 = " . $value;

	$value = dbquote($form->value("cer_summary_in01_sig07"));
	$fields[] = "cer_summary_in01_sig07 = " . $value;

	$value = dbquote($form->value("cer_summary_in01_sig10"));
	$fields[] = "cer_summary_in01_sig10 = " . $value;

	$value = "current_timestamp";
	$fields[] = "date_modified = " . $value;

	$value = $_SESSION["user_login"];
	$fields[] = "user_modified = " . dbquote($value);

	$sql = "update cer_summary set " . join(", ", $fields) . " where cer_summary_cer_version = 0 and cer_summary_project = " . param("pid");
	mysql_query($sql) or dberror($sql);

	//update cer_refundings
	/*
	$fields = array();

	$value = dbquote($form->value("cer_basicdata_approvalname1"));
	$fields[] = "cer_refunding_appronalname1 = " . $value;

	$value = dbquote($form->value("cer_basicdata_approvalname3"));
	$fields[] = "cer_refunding_appronalname3 = " . $value;

	$value = dbquote($form->value("cer_basicdata_approvalname4"));
	$fields[] = "cer_refunding_appronalname4 = " . $value;

	$value = dbquote($form->value("cer_basicdata_approvalname5"));
	$fields[] = "cer_refunding_appronalname5 = " . $value;

	$value = dbquote($form->value("cer_basicdata_approvalname6"));
	$fields[] = "cer_refunding_appronalname6 = " . $value;

	$value = dbquote($form->value("cer_basicdata_approvalname7"));
	$fields[] = "cer_refunding_appronalname7 = " . $value;

	$value = dbquote($form->value("cer_basicdata_approvalname9"));
	$fields[] = "cer_refunding_appronalname9 = " . $value;

	$value = dbquote($form->value("cer_summary_in01_sig01"));
	$fields[] = "cer_refunding_appronalname11 = " . $value;

	$value = dbquote($form->value("cer_summary_in01_sig02"));
	$fields[] = "cer_refunding_appronalname12 = " . $value;

	$value = dbquote($form->value("cer_summary_in01_sig03"));
	$fields[] = "cer_refunding_appronalname13 = " . $value;

	$value = "current_timestamp";
	$fields[] = "date_modified = " . $value;

	$value = $_SESSION["user_login"];
	$fields[] = "user_modified = " . dbquote($value);

	$sql = "update cer_refundings set " . join(", ", $fields) . " where cer_refunding_project = " . param("pid");
	mysql_query($sql) or dberror($sql);
	*/

	$form->message("Your data has been saved.");


}

$page = new Page("cer_projects");
require "include/project_page_actions.php";
$page->header();
$page->title("Edit Approval Names");
$form->render();
$page->footer();

?>