<?php
/********************************************************************

    cer_additional_rental_cost_type.php

    Creation and mutation of additional rental cost types involved in CER.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2015-04-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2015-04-15
    Version:        1.0.0

    Copyright (c) 2015 Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_full_access_to_cer");

/********************************************************************
    build form 
*********************************************************************/
$form = new Form("additional_rental_cost_types", "Additional Rental Cost Type");


$form->add_edit("additional_rental_cost_type_name", "Type Name", NOTNULL);
$form->add_checkbox("additional_rental_cost_type_active", "In use", "", 0, "Active");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();
$form->process();

if($form->button(FORM_BUTTON_SAVE)) {
	redirect("cer_additional_rental_cost_types.php");
}

$page = new Page("cer_additional_rental_cost_types");

$page->header();
$page->title("Edit Additional Rental Cost Type");
$form->render();
$page->footer();

?>
