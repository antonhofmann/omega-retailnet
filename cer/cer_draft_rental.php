<?php
/********************************************************************

	cer_draft_rental.php

    Draft: rental information
    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-01-22
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-01-22
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";

require "include/get_draft_functions.php";

check_access("has_access_to_cer_drafts");


/********************************************************************
    prepare all data needed
*********************************************************************/
$currency = get_draft_currency(param("did"));
$cer_basicdata = get_draft_basicdata(param("did"));

param("id", $cer_basicdata['cer_basicdata_id']);



$duration_in_years = $cer_basicdata["cer_basicdata_lastyear"] - $cer_basicdata["cer_basicdata_firstyear"];
$tmp1 = 13 - $cer_basicdata["cer_basicdata_firstmonth"] + $cer_basicdata["cer_basicdata_lastmonth"];
$tmp2 = ($duration_in_years - 1)*12;
$duration_in_years = round(($tmp1 + $tmp2) / 12, 1);

$duration_in_years_and_months = floor(($tmp1 + $tmp2) / 12);
$duration_in_years_and_months = $duration_in_years_and_months . " years and " . (($tmp1 + $tmp2) - ($duration_in_years_and_months*12)) . " months";

$business_plan_period = $duration_in_years_and_months;


//get data for fixed rental cost
$years = array();
$months = array();
$days = array();

$first_year = 0;
$first_month = 0;
$first_day = 0;
$last_year = 0;
$last_month = 0;
$last_day = 0;


for($i=1; $i<=12; $i++)
{
	$months[$i] = $i;
}

for($i=1; $i<=31; $i++)
{
	$days[$i] = $i;
}

$first_year = $cer_basicdata["cer_basicdata_firstyear"];
$last_year = $cer_basicdata["cer_basicdata_lastyear"];

for($i=$first_year; $i<=$last_year; $i++)
{
	$years[$i] = $i;
}

$result = check_expenses(param("did"), $first_year, $last_year);



//check if revenues are present
$revenues = 0;
$sql = "select sum(cer_revenue_watches) as total " .
       "from cer_draft_revenues " .
	   " where cer_revenue_draft_id = " . param("did");

$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	$revenues = $row["total"];
}


//new additional rental costs
//check if additional rental cost records are present and create if not
$sql = "select additional_rental_cost_type_id " . 
	   "from additional_rental_cost_types " . 
	   " where additional_rental_cost_type_active = 1";
$sql_l = $sql;
$res = mysql_query($sql_l) or dberror($sql_l);
while($row = mysql_fetch_assoc($res))
{
	$sql_a = "select count(cer_additional_rental_cost_id) as num_recs " . 
		     " from cer_draft_additional_rental_costs " . 
		     " where cer_additional_rental_cost_draft_id = " . dbquote(param("did")) . 
		     " and cer_additional_rental_cost_type_id = " . $row["additional_rental_cost_type_id"];
	
	$res_a = mysql_query($sql_a) or dberror($sql_a);
	$row_a = mysql_fetch_assoc($res_a);

	if($row_a["num_recs"] == 0)
	{
		$fields = array();
		$values = array();

		$fields[] = "cer_additional_rental_cost_draft_id";
		$values[] = dbquote(param("did"));

		$fields[] = "cer_additional_rental_cost_type_id";
		$values[] = dbquote($row["additional_rental_cost_type_id"]);

		$fields[] = "cer_additional_rental_cost_unit";
		$values[] = 2;

		$fields[] = "cer_additional_rental_cost_period";
		$values[] = 1;

		$fields[] = "date_created";
		$values[] = "now()";

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$sql_i = "insert into cer_draft_additional_rental_costs (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

		//insert one empty record
		mysql_query($sql_i) or dberror($sql_i);
		
		
		//mysql_query($sql) or dberror($sql);
		//mysql_query($sql) or dberror($sql);


		for($year=$first_year;$year<=$last_year;$year++)
		{
			$fields = array();
			$values = array();

			$fields[] = "cer_additional_rental_cost_amount_draft_id";
			$values[] = dbquote(param("did"));

			$fields[] = "cer_additional_rental_cost_amount_costtype_id";
			$values[] = dbquote($row["additional_rental_cost_type_id"]);

			$fields[] = "cer_additional_rental_cost_amount_year";
			$values[] = $year;

			
			$fields[] = "date_created";
			$values[] = "now()";

			$fields[] = "user_created";
			$values[] = dbquote(user_login());

			$sql_i = "insert into cer_draft_additional_rental_cost_amounts (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

			mysql_query($sql_i) or dberror($sql_i);
			$last_id = mysql_insert_id();

			
			//update data from expenses, if the record was created the first time
			// this is necessary to move the amount after the introduction 
			//of the calculator for additonal rental costs in April 2015
			if($row["additional_rental_cost_type_id"] == 7)
			{
				$sql_e = "select * from cer_draft_expenses " . 
					     " where cer_expense_draft_id = " . dbquote(param("did")) .
					     " and cer_expense_year = " . dbquote($year) . 
					     " and cer_expense_type = 3";

				$res_e = mysql_query($sql_e) or dberror($sql_e);
				while($row_e = mysql_fetch_assoc($res_e))
				{
					$sql_u = "update cer_draft_additional_rental_cost_amounts set " . 
						   " cer_additional_rental_cost_amount_draft_id = " . dbquote(param("did")) . ", " .
						   " cer_additional_rental_cost_amount_costtype_id = 7, " . 
						   " cer_additional_rental_cost_amount_year = " . dbquote($row_e["cer_expense_year"]) . ", " . 
						   " cer_additional_rental_cost_amount_amount = " . dbquote($row_e["cer_expense_amount"]) . ", " .
						   " user_created = " . dbquote($row_e["user_created"]) . ", " .
						   " date_created = " . dbquote($row_e["date_created"]) . ", " .
						   " user_modified = " . dbquote($row_e["user_modified"]) . ", " .
						   " date_modified = " . dbquote($row_e["date_modified"]) . " " .
						   " where cer_additional_rental_cost_amount_id = " . $last_id;

					mysql_query($sql_u) or dberror($sql_u);
				}
			}
		}

	}
	else
	{
		//check records in table cer_draft_additional_rental_cost_amounts
		for($year=$first_year;$year<=$last_year;$year++)
		{
			
			 $sql_c = "select count(cer_additional_rental_cost_amount_id) as num_recs " . 
					 " from cer_draft_additional_rental_cost_amounts " . 
					 " where cer_additional_rental_cost_amount_draft_id = " . dbquote(param("did")) . 
					 " and cer_additional_rental_cost_amount_costtype_id = " . $row["additional_rental_cost_type_id"] . 
				     " and cer_additional_rental_cost_amount_year = " . $year;

			$res_c = mysql_query($sql_c) or dberror($sql_c);
			$row_c = mysql_fetch_assoc($res_c);

			if($row_c["num_recs"] == 0)
			{

				$fields = array();
				$values = array();

				$fields[] = "cer_additional_rental_cost_amount_draft_id";
				$values[] = dbquote(param("did"));

				$fields[] = "cer_additional_rental_cost_amount_costtype_id";
				$values[] = dbquote($row["additional_rental_cost_type_id"]);

				$fields[] = "cer_additional_rental_cost_amount_year";
				$values[] = $year;

				
				$fields[] = "date_created";
				$values[] = "now()";

				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				$sql_i = "insert into cer_draft_additional_rental_cost_amounts (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

				mysql_query($sql_i) or dberror($sql_i);
				$last_id = mysql_insert_id();
			}
		}
	}
}
//end new additional rental costs



$units = array();
$units[1] = "square feet and month";
$units[2] = "square meters and month";
$units[3] = "square feet and year";
$units[4] = "square meters and year";
$units[5] = "total surface per month";
$units[6] = "total surface per year";

$surfaces = array();
$surfaces[1] = "Total Rented Boutique Surface in Square Feet";
$surfaces[2] = "Total Rented Boutique Surface in Square Meters";
$surfaces[3] = "Total Rented Boutique Surface in Square Feet";
$surfaces[4] = "Total Rented Boutique Surface in Square Meters";
$surfaces[5] = "Total Rented Boutique Surface ";
$surfaces[6] = "Total Rented Boutique Surface ";


$surfaces2 = array();
$surfaces2[1] = "Total Rented Additional Surface in Square Feet";
$surfaces2[2] = "Total Rented Additional Surface in Square Meters";
$surfaces2[3] = "Total Rented Additional Surface in Square Feet";
$surfaces2[4] = "Total Rented Additional Surface in Square Meters";
$surfaces2[5] = "Total Rented Additional Surface ";
$surfaces2[6] = "Total Rented Additional Surface ";

$bases = array();
$bases[1] = "Month";
$bases[2] = "Year";


$selected_unit = 2;
$indicated_surface = $cer_basicdata["cer_basicdata_sqms"];
$project["project_cost_totalsqms"] = $indicated_surface; // workaround because no project is there for helper JS
$indicated_surface2 = 0;



$fixed_rental_amounts = array();
$from_years = array();
$to_years = array();
$number_of_months = array();
$index_rates = array();
$increase_rates = array();
$inflation_rates = array();

$index_rate_startdate_years = array();
$index_rate_startdate_months = array();
$index_rate_startdate_days = array();

$increase_rate_startdate_years = array();
$increase_rate_startdate_months = array();
$increase_rate_startdate_days = array();

$inflation_rate_startdate_years = array();
$inflation_rate_startdate_months = array();
$inflation_rate_startdate_days = array();

$tax_rates = array();
$passenger_rates = array();

$fixed_rent_units = array();

$price_records = array();

$from_months = array();
$to_months = array();

$from_days = array();
$to_days = array();
$rental_price_per_unit = array();


//check if rental records are present and create if not
$sql = "select count(cer_fixed_rent_id) as num_recs " . 
	   "from cer_draft_fixed_rents";
$list_filter = "cer_fixed_rent_draft_id =" . param("did");

$sql_l = $sql . " where " . $list_filter;
$res = mysql_query($sql_l) or dberror($sql_l);
$row = mysql_fetch_assoc($res);
if($row["num_recs"] == 0)
{
	$fields = array();
	$values = array();

	$fields[] = "cer_fixed_rent_draft_id";
	$values[] = dbquote(param("did"));

	$fields[] = "cer_fixed_rent_unit";
	$values[] = 2;

	$fields[] = "cer_fixed_rent_total_surface";
	$values[] = dbquote($indicated_surface);
	

	$fields[] = "date_created";
	$values[] = "now()";

	$fields[] = "user_created";
	$values[] = dbquote(user_login());

	$sql = "insert into cer_draft_fixed_rents (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

	//insert one empty record
	mysql_query($sql) or dberror($sql);
}



//get rental records
$fixed_rental_amounts = array();
$from_years = array();
$to_years = array();
$number_of_months = array();
$index_rates = array();
$increase_rates = array();


$from_months = array();
$to_months = array();

$from_days = array();
$to_days = array();
$rental_price_per_unit = array();

$last_payed_amounts = array();

$sales_guarantee_amounts = array();
$sales_guarantee_percents = array();
$sales_guarantee_bases = array();

$sql = "select cer_fixed_rent_id, cer_fixed_rent_amount, cer_fixed_rent_unit, " . 
	   "cer_fixed_rent_from_year, cer_fixed_rent_from_month, cer_fixed_rent_from_day," . 
	   "cer_fixed_rent_to_year, cer_fixed_rent_to_month, cer_fixed_rent_to_day," . 
	   "cer_fixed_rent_number_of_months, cer_fixed_rent_unit,  cer_fixed_rent_pos_surface, " . 
	   "cer_fixed_rent_other_surface, cer_fixed_rent_index_rate, cer_fixed_rent_increas_rate, ".
	   "cer_fixed_rent_inflation_rate, cer_fixed_rent_index_startdate, cer_fixed_rent_tax_rate, " . 
	   "cer_fixed_rent_passenger_rate, cer_fixed_rent_increas_startdate, cer_fixed_rent_inflation_startdate, " . 
	   "cer_fixed_rent_last_amount_payed, cer_fixed_rent_total_surface, cer_fixed_rent_sales_guarantee_amount, cer_fixed_rent_sales_guarantee_base, cer_fixed_rent_sales_guarantee_percent " . 
	   "from cer_draft_fixed_rents";
$list_filter = "cer_fixed_rent_draft_id =" . param("did");


$sql_l = $sql . " where " . $list_filter;
$res = mysql_query($sql_l) or dberror($sql_l);
while($row = mysql_fetch_assoc($res))
{
	$fixed_rental_amounts[$row["cer_fixed_rent_id"]] = $row["cer_fixed_rent_amount"];
	$number_of_months[$row["cer_fixed_rent_id"]] = $row["cer_fixed_rent_number_of_months"];
	$from_years[$row["cer_fixed_rent_id"]] = $row["cer_fixed_rent_from_year"];
	$from_months[$row["cer_fixed_rent_id"]] = $row["cer_fixed_rent_from_month"];
	$from_days[$row["cer_fixed_rent_id"]] = $row["cer_fixed_rent_from_day"];
	$to_years[$row["cer_fixed_rent_id"]] = $row["cer_fixed_rent_to_year"];
	$to_months[$row["cer_fixed_rent_id"]] = $row["cer_fixed_rent_to_month"];
	$to_days[$row["cer_fixed_rent_id"]] = $row["cer_fixed_rent_to_day"];

	$selected_unit = $row["cer_fixed_rent_unit"];
	
	if($row["cer_fixed_rent_pos_surface"] > 0)
	{
		$indicated_surface = $row["cer_fixed_rent_pos_surface"];
	}
	$indicated_surface2 = $row["cer_fixed_rent_other_surface"];

	$rental_price_per_unit[$row["cer_fixed_rent_id"]] = $row["cer_fixed_rent_amount"];

	$total_surface = $row["cer_fixed_rent_pos_surface"]+$row["cer_fixed_rent_other_surface"];

	$index_rates[$row["cer_fixed_rent_id"]] = $row["cer_fixed_rent_index_rate"];
	$increase_rates[$row["cer_fixed_rent_id"]] = $row["cer_fixed_rent_increas_rate"];
	$inflation_rates[$row["cer_fixed_rent_id"]] = $row["cer_fixed_rent_inflation_rate"];


	$index_rate_startdate_years[$row["cer_fixed_rent_id"]] = substr($row["cer_fixed_rent_index_startdate"], 0, 4);
	$index_rate_startdate_months[$row["cer_fixed_rent_id"]] = substr($row["cer_fixed_rent_index_startdate"], 5, 2);
	$index_rate_startdate_days[$row["cer_fixed_rent_id"]] = substr($row["cer_fixed_rent_index_startdate"],8, 2);

	$increase_rate_startdate_years[$row["cer_fixed_rent_id"]] = substr($row["cer_fixed_rent_increas_startdate"], 0, 4);
	$increase_rate_startdate_months[$row["cer_fixed_rent_id"]] = substr($row["cer_fixed_rent_increas_startdate"], 5, 2);
	$increase_rate_startdate_days[$row["cer_fixed_rent_id"]] = substr($row["cer_fixed_rent_increas_startdate"],8, 2);

	$inflation_rate_startdate_years[$row["cer_fixed_rent_id"]] = substr($row["cer_fixed_rent_inflation_startdate"], 0, 4);
	$inflation_rate_startdate_months[$row["cer_fixed_rent_id"]] = substr($row["cer_fixed_rent_inflation_startdate"], 5, 2);
	$inflation_rate_startdate_days[$row["cer_fixed_rent_id"]] = substr($row["cer_fixed_rent_inflation_startdate"],8, 2);

	$fixed_rent_units[$row["cer_fixed_rent_id"]] = $row["cer_fixed_rent_unit"];
	$tax_rates[$row["cer_fixed_rent_id"]] = $row["cer_fixed_rent_tax_rate"];
	$passenger_rates[$row["cer_fixed_rent_id"]] = $row["cer_fixed_rent_passenger_rate"];

	$last_payed_amounts[$row["cer_fixed_rent_id"]] = "";

	if($row["cer_fixed_rent_total_surface"] > 0 and ($row["cer_fixed_rent_unit"] == 1 or $row["cer_fixed_rent_unit"] == 2))
	{
		$last_payed_amounts[$row["cer_fixed_rent_id"]] = round($row["cer_fixed_rent_last_amount_payed"]/$row["cer_fixed_rent_total_surface"], 2);
	}
	elseif(($row["cer_fixed_rent_unit"] == 3 or $row["cer_fixed_rent_unit"] == 3))
	{
		$last_payed_amounts[$row["cer_fixed_rent_id"]] = $row["cer_fixed_rent_last_amount_payed"];
	}
	elseif($row["cer_fixed_rent_total_surface"] > 0 and $row["cer_fixed_rent_unit"] == 5)
	{
		$last_payed_amounts[$row["cer_fixed_rent_id"]] = round($row["cer_fixed_rent_last_amount_payed"]/$row["cer_fixed_rent_total_surface"], 2);
	}
	elseif($row["cer_fixed_rent_unit"] == 6)
	{
		$last_payed_amounts[$row["cer_fixed_rent_id"]] = $row["cer_fixed_rent_last_amount_payed"];
	}

	$sales_guarantee_amounts[$row["cer_fixed_rent_id"]] = $row["cer_fixed_rent_sales_guarantee_amount"];
	$sales_guarantee_bases[$row["cer_fixed_rent_id"]] = $row["cer_fixed_rent_sales_guarantee_base"];
	$sales_guarantee_percents[$row["cer_fixed_rent_id"]] = $row["cer_fixed_rent_sales_guarantee_percent"];

}


$sql2 = "select cer_fixed_rent_id, cer_fixed_rent_amount as fixed_rent_amount2, " .
        "cer_fixed_rent_amount as fixed_rent_amount3, cer_fixed_rent_amount as fixed_rent_amount4, " . 
	   "concat(cer_fixed_rent_from_day, '.', cer_fixed_rent_from_month, '.', cer_fixed_rent_from_year) as fdate2, " .
	   "concat(cer_fixed_rent_to_day, '.', cer_fixed_rent_to_month, '.', cer_fixed_rent_to_year) as tdate2, " .
	   "concat(cer_fixed_rent_from_day, '.', cer_fixed_rent_from_month, '.', cer_fixed_rent_from_year) as fdate3, " .
	   "concat(cer_fixed_rent_to_day, '.', cer_fixed_rent_to_month, '.', cer_fixed_rent_to_year) as tdate3, " .
	   "concat(cer_fixed_rent_from_day, '.', cer_fixed_rent_from_month, '.', cer_fixed_rent_from_year) as fdate4, " .
	   "concat(cer_fixed_rent_to_day, '.', cer_fixed_rent_to_month, '.', cer_fixed_rent_to_year) as tdate4, " .
	   "cer_fixed_rent_number_of_months, cer_fixed_rent_unit,  cer_fixed_rent_pos_surface, " . 
	   "cer_fixed_rent_other_surface, cer_fixed_rent_index_rate, cer_fixed_rent_increas_rate, ".
	   "cer_fixed_rent_inflation_rate " . 
	   "from cer_draft_fixed_rents";
$list2_filter = "cer_fixed_rent_draft_id =" . param("did");



//check if turnover based rental percentages records are present and create if not

$sql_c = "select count(cer_rent_percent_from_sale_id) as num_recs " . 
	   "from cer_draft_rent_percent_from_sales";
$list_filter_c = "cer_rent_percent_from_sale_draft_id =" . param("did");

$sql_l = $sql_c . " where " . $list_filter_c;
$res = mysql_query($sql_l) or dberror($sql_l);
$row = mysql_fetch_assoc($res);
if($row["num_recs"] == 0)
{
	$fields = array();
	$values = array();

	$fields[] = "cer_rent_percent_from_sale_draft_id";
	$values[] = dbquote(param("did"));

	$fields[] = "date_created";
	$values[] = "now()";

	$fields[] = "user_created";
	$values[] = dbquote(user_login());

	$sql_i = "insert into cer_draft_rent_percent_from_sales (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

	//insert one empty record
	mysql_query($sql_i) or dberror($sql_i);
	
}


//get turnover based rental percentages records

$tob_percents = array();
$tob_amounts = array();
$tob_number_of_months = array();
$tob_from_years = array();
$tob_from_months = array();
$tob_from_days = array();
$tob_to_years = array();
$tob_to_months = array();
$tob_to_days = array();
$tob_tax_rates = array();
$tob_passenger_index = array();

$sql_tob = "select cer_rent_percent_from_sale_id, cer_rent_percent_from_sale_percent, " . 
           "cer_rent_percent_from_sale_amount, cer_rent_percent_from_sale_from_year, " . 
		   "cer_rent_percent_from_sale_from_month, cer_rent_percent_from_sale_from_day," . 
		   "cer_rent_percent_from_sale_to_year, cer_rent_percent_from_sale_to_month, " . 
		   "cer_rent_percent_from_sale_to_day, cer_rent_percent_from_sale_number_of_months, " .
		   "cer_rent_percent_from_sale_tax_rate, cer_rent_percent_from_sale_passenger_rate " .  
	       "from cer_draft_rent_percent_from_sales";
$list_tob_filter = "cer_rent_percent_from_sale_draft_id =" . param("did");


$sql_l = $sql_tob . " where " . $list_tob_filter;
$res = mysql_query($sql_l) or dberror($sql_l);

while($row = mysql_fetch_assoc($res))
{
	$tob_amounts[$row["cer_rent_percent_from_sale_id"]] = $row["cer_rent_percent_from_sale_amount"];
	$tob_percents[$row["cer_rent_percent_from_sale_id"]] = $row["cer_rent_percent_from_sale_percent"];
	$tob_number_of_months[$row["cer_rent_percent_from_sale_id"]] = $row["cer_rent_percent_from_sale_number_of_months"];
	$tob_from_years[$row["cer_rent_percent_from_sale_id"]] = $row["cer_rent_percent_from_sale_from_year"];
	$tob_from_months[$row["cer_rent_percent_from_sale_id"]] = $row["cer_rent_percent_from_sale_from_month"];
	$tob_from_days[$row["cer_rent_percent_from_sale_id"]] = $row["cer_rent_percent_from_sale_from_day"];
	$tob_to_years[$row["cer_rent_percent_from_sale_id"]] = $row["cer_rent_percent_from_sale_to_year"];
	$tob_to_months[$row["cer_rent_percent_from_sale_id"]] = $row["cer_rent_percent_from_sale_to_month"];
	$tob_to_days[$row["cer_rent_percent_from_sale_id"]] = $row["cer_rent_percent_from_sale_to_day"];
	$tob_tax_rates[$row["cer_rent_percent_from_sale_id"]] = $row["cer_rent_percent_from_sale_tax_rate"];
	$tob_passenger_index[$row["cer_rent_percent_from_sale_id"]] = $row["cer_rent_percent_from_sale_passenger_rate"];

}



//get additional rental costs
$amounts = array();
$comments = array();
$sql_e = "select * from cer_draft_expenses ";
$list_filter_e = "cer_expense_draft_id = " . param("did") . " and cer_expense_type = 3 ";


$sql_l = $sql_e . " where " . $list_filter_e;
$res = mysql_query($sql_l) or dberror($sql_l);

while($row = mysql_fetch_assoc($res))
{
	$amounts[$row["cer_expense_id"]] = $row["cer_expense_amount"];
	$comments[$row["cer_expense_id"]] = $row["cer_expense_comment"];
}

$rental_duration = "";
if($cer_basicdata["cer_basicdata_lease_startdate"] != NULL 
	and $cer_basicdata["cer_basicdata_lease_startdate"] != '0000-00-00' 
	and $cer_basicdata["cer_basicdata_lease_enddate"] != NULL
	and $cer_basicdata["cer_basicdata_lease_enddate"] != '0000-00-00')
{
	$diff = abs(strtotime($cer_basicdata["cer_basicdata_lease_enddate"]) - strtotime($cer_basicdata["cer_basicdata_lease_startdate"]));
	$rd_years = floor($diff / (365*60*60*24));
	$rd_months = floor(($diff - $rd_years * 365*60*60*24) / (30*60*60*24));

	if($rd_months == 12)
	{
		$rd_months = 0;
		$rd_years++;
	}

	$rental_duration = $rd_years . " years and " . $rd_months . " months";
}



/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_drafts", "cer_drafts");

$form->add_hidden("did", param("did"));


$tmp =  $cer_basicdata["cer_basicdata_firstmonth"] . "/" . $cer_basicdata["cer_basicdata_firstyear"] . " - " .
$cer_basicdata["cer_basicdata_lastmonth"] . "/" . $cer_basicdata["cer_basicdata_lastyear"] . " / " . $business_plan_period;
$form->add_label("business_plan_period_label", "Business Plan Period", 0, $tmp);


$form->add_label("l_startdate_label", 'Lease Start Date / Expiry Date / Rental Duration', 0, to_system_date($cer_basicdata["cer_basicdata_lease_startdate"]) . " / " . to_system_date($cer_basicdata["cer_basicdata_lease_enddate"]) . " / " . $rental_duration);

$form->add_section("Surfaces");
$form->add_edit("cer_fixed_rent_pos_surface", "<span id=\"unit2\">" . $surfaces[$selected_unit] ."</span>", 0, $indicated_surface, TYPE_DECIMAL, 6,2, 1, "pos_surface_info");
$form->add_edit("cer_fixed_rent_other_surface", "<span id=\"unit3\">" . $surfaces2[$selected_unit] ."</span>", 0, $indicated_surface2, TYPE_DECIMAL, 6,2, 1, "other_surface_info");


$form->add_section(' ');
$form->add_checkbox("cer_basicdata_sales_guarantee", "The lease contract contains a sales guarantee clause", $cer_basicdata["cer_basicdata_sales_guarantee"], SUBMIT, "Sales Guarantee");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();



/********************************************************************
    build list for sales quarantee
*********************************************************************/
$list_sg = new ListView($sql);
$list_sg->set_filter($list_filter);
$list_sg->set_order("CASE cer_fixed_rent_from_year WHEN 0 THEN 9999 ELSE cer_fixed_rent_from_year END, cer_fixed_rent_from_month, cer_fixed_rent_from_day");


$list_sg->set_title("Sales Guarantee");
if($revenues == 0) {
	$list_sg->set_comment('<span class="error">Before you can use the sales guarantee option you have to add revenues first!</span>');
}

$list_sg->add_edit_column("cer_fixed_rent_sales_guarantee_amount", "Amount", 12, COLUMN_UNDERSTAND_HTML, $sales_guarantee_amounts);
$list_sg->add_edit_column("cer_fixed_rent_sales_guarantee_percent", "Precentage", 5, COLUMN_UNDERSTAND_HTML, $sales_guarantee_percents);
$list_sg->add_list_column("cer_fixed_rent_sales_guarantee_base", "Base", $bases, 0, $sales_guarantee_bases);

$list_sg->add_list_column("cer_fixed_rent_from_year_sg", "from year", $years, 0, $from_years);
$list_sg->add_list_column("cer_fixed_rent_from_month_sg", "from month", $months, 0, $from_months);
$list_sg->add_list_column("cer_fixed_rent_from_day_sg", "from day", $days, 0, $from_days);

$list_sg->add_list_column("cer_fixed_rent_to_year_sg", "to year", $years, 0, $to_years);
$list_sg->add_list_column("cer_fixed_rent_to_month_sg", "to month", $months, 0, $to_months);
$list_sg->add_list_column("cer_fixed_rent_to_day_sg", "to day", $days, 0, $to_days);
$list_sg->add_edit_column("cer_fixed_rent_number_of_months_sg", "Months", 10, DISABLED, $number_of_months);


if($revenues > 0)
{
	$list_sg->add_button("update_fixed_rents", "Save and calculate rents");
	$list_sg->add_button("add_new_line", "Add new Line");
}


$list_sg->populate();
$list_sg->process();


/********************************************************************
    create list of foxed rents
*********************************************************************/ 
$list = new ListView($sql);
$list->set_filter($list_filter);
$list->set_order("CASE cer_fixed_rent_from_year WHEN 0 THEN 9999 ELSE cer_fixed_rent_from_year END, cer_fixed_rent_from_month, cer_fixed_rent_from_day");

$list->set_title("Fixed Rents");
$list->set_comment("Indicate the tax rate in percent in case you have to pay taxes on rents.<br />Indicate the airport passenger index and average increase by percentage (API in %):	");


$list->add_edit_column("cer_fixed_rent_amount", "Fixed Rent", 12, COLUMN_UNDERSTAND_HTML, $fixed_rental_amounts);

$list->add_list_column("cer_fixed_rent_unit", "Payed per", $units, 0, $fixed_rent_units);

$list->add_list_column("cer_fixed_rent_from_year", "from year", $years, 0, $from_years);
$list->add_list_column("cer_fixed_rent_from_month", "from month", $months, 0, $from_months);
$list->add_list_column("cer_fixed_rent_from_day", "from day", $days, 0, $from_days);

$list->add_list_column("cer_fixed_rent_to_year", "to year", $years, 0, $to_years);
$list->add_list_column("cer_fixed_rent_to_month", "to month", $months, 0, $to_months);
$list->add_list_column("cer_fixed_rent_to_day", "to day", $days, 0, $to_days);

$list->add_edit_column("cer_fixed_rent_number_of_months", "Months", 10, DISABLED, $number_of_months);

$list->add_edit_column("cer_fixed_rent_tax_rate", "Tax Rate in %", 10, COLUMN_UNDERSTAND_HTML, $tax_rates);
$list->add_edit_column("cer_fixed_rent_passenger_rate", "API in %", 10, COLUMN_UNDERSTAND_HTML, $passenger_rates);

$list->add_edit_column("cer_fixed_rent_last_amount_payed", "Last Amount", 10, DISABLED, $last_payed_amounts);


$list->add_button("add_new_line", "Add new Line");


$list->populate();
$list->process();


/********************************************************************
    build list of increases
*********************************************************************/
$list2 = new ListView($sql2);
$list2->set_filter($list2_filter);
$list2->set_order("CASE cer_fixed_rent_from_year WHEN 0 THEN 9999 ELSE cer_fixed_rent_from_year END, cer_fixed_rent_from_month, cer_fixed_rent_from_day");


$list2->set_title("Annual Index Rate on Fixed Rents");
$list2->set_comment("Indicate the index rate in percent and the date on which indexing starts.");

$list2->add_column("fixed_rent_amount2", "Fixed Rent", "", "", $fixed_rental_amounts);
$list2->add_column("fdate2", "From");
$list2->add_column("tdate2", "To");


$list2->add_edit_column("cer_fixed_rent_index_rate", "Index Rate</br>in % per Year", 10, COLUMN_UNDERSTAND_HTML, $index_rates);
$list2->add_list_column("cer_fixed_rent_index_startyear", "from year", $years, COLUMN_UNDERSTAND_HTML, $index_rate_startdate_years);
$list2->add_list_column("cer_fixed_rent_index_startmonth", "from month", $months, COLUMN_UNDERSTAND_HTML, $index_rate_startdate_months);
$list2->add_list_column("cer_fixed_rent_index_startday", "from day", $days, COLUMN_UNDERSTAND_HTML, $index_rate_startdate_days);


$list2->populate();
$list2->process();


/********************************************************************
    build list of increases
*********************************************************************/
$list3 = new ListView($sql2);
$list3->set_filter($list2_filter);
$list3->set_order("CASE cer_fixed_rent_from_year WHEN 0 THEN 9999 ELSE cer_fixed_rent_from_year END, cer_fixed_rent_from_month, cer_fixed_rent_from_day");


$list3->set_title("Annual Increase of Fixed Rents");
$list3->set_comment("Indicate the increase rate in percent and the date on which increasing starts.");

$list3->add_column("fixed_rent_amount3", "Fixed Rent", "", "", $fixed_rental_amounts);
$list3->add_column("fdate3", "From");
$list3->add_column("tdate3", "To");


$list3->add_edit_column("cer_fixed_rent_increas_rate", "Increas Rate</br>in % per Year", 10, COLUMN_UNDERSTAND_HTML, $increase_rates);
$list3->add_list_column("cer_fixed_rent_increas_startyear", "from year", $years, COLUMN_UNDERSTAND_HTML, $increase_rate_startdate_years);
$list3->add_list_column("cer_fixed_rent_increas_startmonth", "from month", $months, COLUMN_UNDERSTAND_HTML, $increase_rate_startdate_months);
$list3->add_list_column("cer_fixed_rent_increas_startday", "from day", $days, COLUMN_UNDERSTAND_HTML, $increase_rate_startdate_days);


$list3->populate();
$list3->process();



/********************************************************************
    build list of increases
*********************************************************************/
$list4 = new ListView($sql2);
$list4->set_filter($list2_filter);
$list4->set_order("CASE cer_fixed_rent_from_year WHEN 0 THEN 9999 ELSE cer_fixed_rent_from_year END, cer_fixed_rent_from_month, cer_fixed_rent_from_day");


$list4->set_title("Annual Inflation on Fixed Rents");
$list4->set_comment("Indicate the inflation rate in percent per year and the date on which inflating starts.");

$list4->add_column("fixed_rent_amount4", "Fixed Rent", "", "", $fixed_rental_amounts);
$list4->add_column("fdate4", "From");
$list4->add_column("tdate4", "To");


$list4->add_edit_column("cer_fixed_rent_inflation_rate", "Inflation</br>in % per Year", 10, COLUMN_UNDERSTAND_HTML, $inflation_rates);
$list4->add_list_column("cer_fixed_rent_inflation_startyear", "from year", $years, COLUMN_UNDERSTAND_HTML, $inflation_rate_startdate_years);
$list4->add_list_column("cer_fixed_rent_inflation_startmonth", "from month", $months, COLUMN_UNDERSTAND_HTML, $inflation_rate_startdate_months);
$list4->add_list_column("cer_fixed_rent_inflation_startday", "from day", $days, COLUMN_UNDERSTAND_HTML, $inflation_rate_startdate_days);

$list4->populate();
$list4->process();


/********************************************************************
    build list of turnoverbased amounts
*********************************************************************/
$list1 = new ListView($sql_tob);
$list1->set_filter($list_tob_filter);
$list1->set_title("Total Annual Rent in % of Sales Values and Breakpoints");
$list1->set_comment("Indicate the tax rate in percent in case you have to pay taxes on rents.<br />Indicate the airport passenger index and average increase by percentage (API in %):	");


$list1->set_order("CASE cer_rent_percent_from_sale_from_year WHEN 0 THEN 9999 ELSE cer_rent_percent_from_sale_from_year END, cer_rent_percent_from_sale_from_month, cer_rent_percent_from_sale_from_day");


$list1->add_edit_column("cer_rent_percent_from_sale_percent", "Percent", 10, COLUMN_UNDERSTAND_HTML, $tob_percents);
$list1->add_edit_column("cer_rent_percent_from_sale_amount", "Breakpoint", 20, COLUMN_UNDERSTAND_HTML, $tob_amounts);

$list1->add_list_column("cer_rent_percent_from_sale_from_year", "from year", $years, 0, $tob_from_years);
$list1->add_list_column("cer_rent_percent_from_sale_from_month", "from month", $months, 0, $tob_from_months);
$list1->add_list_column("cer_rent_percent_from_sale_from_day", "from day", $days, 0, $tob_from_days);

$list1->add_list_column("cer_rent_percent_from_sale_to_year", "to year", $years, 0, $tob_to_years);
$list1->add_list_column("cer_rent_percent_from_sale_to_month", "to month", $months, 0, $tob_to_months);
$list1->add_list_column("cer_rent_percent_from_sale_to_day", "to day", $days, 0, $tob_to_days);

$list1->add_edit_column("cer_rent_percent_from_sale_number_of_months", "Months", 10, DISABLED, $tob_number_of_months);
$list1->add_edit_column("cer_rent_percent_from_sale_tax_rate", "Tax Rate in %", 10, COLUMN_UNDERSTAND_HTML, $tob_tax_rates);
$list1->add_edit_column("cer_rent_percent_from_sale_passenger_rate", "API in %", 10, COLUMN_UNDERSTAND_HTML, $tob_passenger_index);

$list1->add_button("add_new_line2", "Add new Line");

$list1->populate();
$list1->process();



/********************************************************************
    build form head and part for fixed rental cost
*********************************************************************/

$form1 = new Form("cer_drafts", "cer_drafts");
$form1->add_hidden("did", param("did"));



$form1->add_comment("Please check the following option if turnover based rents are to be calculated on the base of NET SALES VALUES. <br />Otherwise they will be calculated on the base of GROSS SALES VALUES.");
$form1->add_checkbox("cer_basicdata_tob_from_net_sales", "turnover based rents are to be calculated on the base of NET SALES VALUES", $cer_basicdata["cer_basicdata_tob_from_net_sales"], "", "Calculation Mode");

$form1->add_comment("Please check the following option if turnover based rents are to be paid in addition to fixed rents. <br />Otherwise rents are calculated on a 'whatever is higher base'.");
$form1->add_checkbox("cer_basicdata_add_tob_rents", "turnover based rents are to be paid in addition to fixed rents", $cer_basicdata["cer_basicdata_add_tob_rents"], "", "Calculation Mode");

$form1->add_comment("Please check the following option if turnover based rents are to be calculated on the base of the DIFFERENCE between SALES VALUES and BREAKPOINTS. <br />Otherwise they will be calculated on the base of SALES VALUES.");

$form1->add_checkbox("cer_basicdata_tob_from_breakpoint_difference", "turnover based rents are to be calculated on the base of the DIFFERENCE between SALES VALUES and BREAKPOINTS", $cer_basicdata["cer_basicdata_tob_from_breakpoint_difference"], "", "Calculation Mode");

$form1->populate();
$form1->process();




/********************************************************************
    build list of additional rental cost
*********************************************************************/
$list_e = new ListView($sql_e);
$list_e->set_filter($list_filter_e);
$list_e->set_order("cer_expense_year");


$list_e->set_title("Additional Rental Costs");
$list_e->set_comment("Please indicate additional rental costs like taxes, fees, communal charges, maintenace and other additional costs.");

$list_e->add_column("cer_expense_year", "Year");

$list_e->add_edit_column("cer_expense_amount", "Amount", 12, COLUMN_UNDERSTAND_HTML, $amounts);
$list_e->add_edit_column("cer_expense_comment", "Comment", 40, COLUMN_UNDERSTAND_HTML, $comments);


$list_e->populate();
$list_e->process();



//new additional rental costs
/********************************************************************
    build list of additional rental cost calculation parameters
*********************************************************************/
$ac_amounts = array();
$ac_fields = array();
$ac_units = array();
$ac_increase_rates = array();
$ac_periods = array();

$periods = array();
$periods[1] = "Calendar year";
$periods[2] = "Lease year";

$sql_a = "select cer_additional_rental_cost_id, cer_additional_rental_cost_amount, " . 
         "cer_additional_rental_cost_increase, cer_additional_rental_cost_unit, cer_additional_rental_cost_period, " .
		 "additional_rental_cost_type_id, additional_rental_cost_type_name " . 
		 " from cer_draft_additional_rental_costs " . 
		 " left join additional_rental_cost_types on additional_rental_cost_type_id = cer_additional_rental_cost_type_id";

$list_filter_a = "cer_additional_rental_cost_draft_id = " . dbquote(param("did"));


$sql_l = $sql_a . " where " . $list_filter_a;
$res = mysql_query($sql_l) or dberror($sql_l);
while($row = mysql_fetch_assoc($res))
{
	if($row["cer_additional_rental_cost_amount"] > 0)
	{
		$ac_amounts[$row["cer_additional_rental_cost_id"]] = $row["cer_additional_rental_cost_amount"];
	}
	else
	{
		$ac_amounts[$row["cer_additional_rental_cost_id"]] = "";
	}
	
	$ac_units[$row["cer_additional_rental_cost_id"]] = $row["cer_additional_rental_cost_unit"];
	
	if($row["cer_additional_rental_cost_increase"] > 0)
	{
		$ac_increase_rates[$row["cer_additional_rental_cost_id"]] = $row["cer_additional_rental_cost_increase"];
	}
	$ac_periods[$row["cer_additional_rental_cost_id"]] = $row["cer_additional_rental_cost_period"];

	$ac_fields[$row["cer_additional_rental_cost_id"]] = $row["additional_rental_cost_type_id"];
}


$list_a = new ListView($sql_a);
$list_a->set_filter($list_filter_a);
$list_a->set_order("additional_rental_cost_type_name");

$list_a->set_title("Calculation Parameters for Additional Rental Costs");

$list_a->set_comment("Please indicate the calculation parameters for additional rental costs as indicated in the lease contract.");

$list_a->add_column("additional_rental_cost_type_name", "Cost Type");
$list_a->add_edit_column("cer_additional_rental_cost_amount", "Amount", 12, 0, $ac_amounts);
$list_a->add_list_column("cer_additional_rental_cost_unit", "Payed per", $units, 0, $ac_units);
$list_a->add_edit_column("cer_additional_rental_cost_increase", "Increas Rate</br>in % per Year", 10, COLUMN_UNDERSTAND_HTML, $ac_increase_rates);
$list_a->add_list_column("cer_additional_rental_cost_period", "Calculation per", $periods, 0, $ac_periods);

$list_a->populate();
$list_a->process();



/********************************************************************
    build list of calculated additional rental cost
*********************************************************************/
$amounts2 = array();
$ac_amounts2 = array();
$sql_a = "select additional_rental_cost_type_id, additional_rental_cost_type_name " . 
		 " from cer_draft_additional_rental_costs " . 
		 " left join additional_rental_cost_types on additional_rental_cost_type_id = cer_additional_rental_cost_type_id";

$list_filter_a = "cer_additional_rental_cost_draft_id = " . dbquote(param("did"));


$sql_l = $sql_a . " where " . $list_filter_a;
$res = mysql_query($sql_l) or dberror($sql_l);
while($row = mysql_fetch_assoc($res))
{
	for($i=$first_year;$i<=$last_year;$i++)
	{
		$tmp = array();
		$sql_am = "select cer_additional_rental_cost_amount_id, cer_additional_rental_cost_amount_costtype_id, cer_additional_rental_cost_amount_amount " . 
			      " from cer_draft_additional_rental_cost_amounts " . 
			      " where cer_additional_rental_cost_amount_draft_id = " . dbquote(param("did")) . 
			      " and cer_additional_rental_cost_amount_year = " . $i;


		$sql_am = mysql_query($sql_am) or dberror($sql_am);
		while($row_am = mysql_fetch_assoc($sql_am))
		{
			
			if($row_am["cer_additional_rental_cost_amount_amount"] > 0)
			{
				$tmp[$row_am["cer_additional_rental_cost_amount_costtype_id"]] = $row_am["cer_additional_rental_cost_amount_amount"];
			}
		}

		$amounts2[$i] = $tmp;

		 
	}
}

$list_am = new ListView($sql_a);
$list_am->set_filter($list_filter_a);
$list_am->set_order("additional_rental_cost_type_name");

$list_am->set_title("Amounts for Additional Rental Costs");
$list_am->set_comment("Enter the annual values for those lines not specified by the calculation parameters for additional rental costs.");
$list_am->add_column("additional_rental_cost_type_name", "Cost Type");

for($i=$first_year;$i<=$last_year;$i++)
{
	$values = $amounts2[$i];
	$list_am->add_edit_column("ac_amounts_" . $i, $i, 12, 0, $values);
}

$list_am->populate();
$list_am->process();

//end new additional rental costs

/********************************************************************
    build form head and part for fixed rental cost
*********************************************************************/

$form2 = new Form("cer_drafts", "cer_drafts");
$form2->add_hidden("did", param("did"));

$form2->add_button("form_save", "Save Data");



$form2->populate();
$form2->process();

/********************************************************************
    process buttons
*********************************************************************/


if($form->button('cer_basicdata_sales_guarantee')) {

	//update cer_basic_data
	$fields = array();


	$value = dbquote($form->value("cer_basicdata_sales_guarantee"));
	$fields[] = "cer_basicdata_sales_guarantee = " . $value;

	
	$value1 = "current_timestamp";
	$fields[] = "date_modified = " . $value1;

	$fields[] = "user_modified = " . dbquote(user_login());


	$sql = "update cer_drafts set " . join(", ", $fields) . 
		" where cer_basicdata_id = " . param("did");
	
	mysql_query($sql) or dberror($sql);


	$link = "cer_draft_rental.php?did=" . param("did");
	redirect($link);
	
}
elseif($form2->button("form_save") or $list->button("add_new_line") or $list->button("add_new_line2"))
{
	
	if($form1->validate())
	{
		
		
		//update cer_basic_data
		$fields = array();
	
		$value = dbquote($form1->value("cer_basicdata_add_tob_rents"));
		$fields[] = "cer_basicdata_add_tob_rents = " . $value;

		$value = dbquote($form1->value("cer_basicdata_tob_from_net_sales"));
		$fields[] = "cer_basicdata_tob_from_net_sales = " . $value;

		$value = dbquote($form1->value("cer_basicdata_tob_from_breakpoint_difference"));
		$fields[] = "cer_basicdata_tob_from_breakpoint_difference = " . $value;


		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		$fields[] = "user_modified = " . dbquote(user_login());

   
		$sql = "update cer_drafts set " . join(", ", $fields) . 
			" where cer_basicdata_id = " . param("did");
		mysql_query($sql) or dberror($sql);

		
		//new additional rental costs

		/*
		//update additional rental costs
		$amounts = $list_e->values("cer_expense_amount");
		$comments = $list_e->values("cer_expense_comment");
		
		foreach($amounts as $key=>$amount)
		{
			

			$fields = array();
	
			$value = dbquote($amount);
			$fields[] = "cer_expense_amount = " . $value;

			$value = dbquote($comments[$key]);
			$fields[] = "cer_expense_comment = " . $value;

			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			$fields[] = "user_modified = " . dbquote(user_login());

			$sql = "update cer_draft_expenses set " . join(", ", $fields) . 
			" where cer_expense_draft_id = " . param("did") . 
			" and cer_expense_id = " . $key;

			mysql_query($sql) or dberror($sql);
		}

		*/


		//update calculation parameters for additional rental costs
		$l_amounts = $list_a->values("cer_additional_rental_cost_amount");
		$l_increases = $list_a->values("cer_additional_rental_cost_increase");
		$l_units = $list_a->values("cer_additional_rental_cost_unit");
		$l_periods = $list_a->values("cer_additional_rental_cost_period");
		
		
		foreach($l_amounts as $key=>$amount)
		{
			

			$fields = array();
	
			$value = dbquote($amount);
			$fields[] = "cer_additional_rental_cost_amount = " . $value;

			$value = 0;
			if($amount > 0)
			{
				$value = dbquote($l_increases[$key]);
			}
			$fields[] = "cer_additional_rental_cost_increase = " . $value;

			$value = 0;
			if($amount > 0)
			{
				$value = dbquote($l_units[$key]);
			}
			$fields[] = "cer_additional_rental_cost_unit = " . $value;

			$value = 0;
			if($amount > 0)
			{
				$value = dbquote($l_periods[$key]);
			}
			$fields[] = "cer_additional_rental_cost_period = " . $value;

			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			$fields[] = "user_modified = " . dbquote(user_login());

			$sql = "update cer_draft_additional_rental_costs set " . join(", ", $fields) . 
			" where cer_additional_rental_cost_draft_id = " . param("did") . 
			" and cer_additional_rental_cost_id =  " . $key;

			mysql_query($sql) or dberror($sql);
		}


		
		//update calculated amounts for additional rental costs
		for($year=$first_year;$year<=$last_year;$year++)
		{
			$l_amounts = $list_am->values("ac_amounts_" . $year);


			foreach($l_amounts as $key=>$amount)
			{
				

				$fields = array();
		
				$value = dbquote($amount);
				$fields[] = "cer_additional_rental_cost_amount_amount = " . $value;

				
				$value1 = "current_timestamp";
				$fields[] = "date_modified = " . $value1;

				$fields[] = "user_modified = " . dbquote(user_login());

				$sql = "update cer_draft_additional_rental_cost_amounts set " . join(", ", $fields) . 
						" where cer_additional_rental_cost_amount_draft_id = " . param("did") . 
						" and cer_additional_rental_cost_amount_year = " . $year . 
						" and cer_additional_rental_cost_amount_costtype_id =  " . $key;


				//echo $sql . "<br />";

				mysql_query($sql) or dberror($sql);
			}
		}


		//update additional rental costs in expenses
		for($year=$first_year;$year<=$last_year;$year++)
		{
			$sql = "select sum(cer_additional_rental_cost_amount_amount) as amount " . 
				   "from cer_draft_additional_rental_cost_amounts " . 
				   " where cer_additional_rental_cost_amount_draft_id = " . param("did") . 
				   " and cer_additional_rental_cost_amount_year = " . $year;
				   
			$res = mysql_query($sql) or dberror($sql);
			while($row = mysql_fetch_assoc($res))
			{
				$fields = array();
		
				$value = dbquote($amount);
				$fields[] = "cer_expense_amount = " . dbquote($row["amount"]);

				$value1 = "current_timestamp";
				$fields[] = "date_modified = " . $value1;

				$fields[] = "user_modified = " . dbquote(user_login());

				$sql = "update cer_draft_expenses set " . join(", ", $fields) . 
				" where cer_expense_draft_id = " . param("did") . 
				" and cer_expense_type = 3 " . 
				" and cer_expense_year = " . dbquote($year);

				mysql_query($sql) or dberror($sql);
			}
		}

		//end new additional rental costs
	}


	//save data of fixed rents
	$sql = "delete from cer_draft_fixed_rents " . 
		   " where cer_fixed_rent_draft_id = " . dbquote(param("did"));
	mysql_query($sql) or dberror($sql);
	

	$v1 = $list->values("cer_fixed_rent_amount");

	$fy = $list->values("cer_fixed_rent_from_year");
	$fm = $list->values("cer_fixed_rent_from_month");
	$fd = $list->values("cer_fixed_rent_from_day");
	$ty = $list->values("cer_fixed_rent_to_year");
	$tm = $list->values("cer_fixed_rent_to_month");
	$td = $list->values("cer_fixed_rent_to_day");

	$un = $list->values("cer_fixed_rent_unit");
	$tr = $list->values("cer_fixed_rent_tax_rate");
	$pr = $list->values("cer_fixed_rent_passenger_rate");


	$v2 = $list->values("cer_fixed_rent_number_of_months");

	
	$ir = $list2->values("cer_fixed_rent_index_rate");
	$ir2 = $list3->values("cer_fixed_rent_increas_rate");
	$in = $list4->values("cer_fixed_rent_inflation_rate");
	
	$ird_y = $list2->values("cer_fixed_rent_index_startyear");
	$ird_m = $list2->values("cer_fixed_rent_index_startmonth");
	$ird_d = $list2->values("cer_fixed_rent_index_startday");

	$ird = array();
	foreach($ird_y as $key=>$value)
	{
		if( $ird_y[$key] == 0)
		{
			$ird[$key] = NULL;
		}
		else
		{
			$ird[$key] = $ird_y[$key] . '-' . $ird_m[$key] . '-' . $ird_d[$key];   
		}
	}

	$ir2d_y = $list3->values("cer_fixed_rent_increas_startyear");
	$ir2d_m = $list3->values("cer_fixed_rent_increas_startmonth");
	$ir2d_d = $list3->values("cer_fixed_rent_increas_startday");

	$ir2d = array();
	foreach($ir2d_y as $key=>$value)
	{
		if( $ir2d_y[$key] == 0)
		{
			$ir2d[$key] = NULL;
		}
		else
		{
			$ir2d[$key] = $ir2d_y[$key] . '-' . $ir2d_m[$key] . '-' . $ir2d_d[$key];  
		}
	}

	$ind_y = $list4->values("cer_fixed_rent_inflation_startyear");
	$ind_m = $list4->values("cer_fixed_rent_inflation_startmonth");
	$ind_d = $list4->values("cer_fixed_rent_inflation_startday");
	
	$ind = array();
	foreach($ind_y as $key=>$value)
	{
		if( $ind_y[$key] == 0)
		{
			$ind[$key] = NULL;
		}
		else
		{
			$ind[$key] = $ind_y[$key] . '-' . $ind_m[$key] . '-' . $ind_d[$key]; 
		}
	}

	if($cer_basicdata["cer_basicdata_sales_guarantee"] == 1) {
		$sg_amounts = $list_sg->values("cer_fixed_rent_sales_guarantee_amount");
		$sg_percents = $list_sg->values("cer_fixed_rent_sales_guarantee_percent");
		$sg_bases = $list_sg->values("cer_fixed_rent_sales_guarantee_base");
	}


	
	$price_records = array();

	
	foreach($v1 as $key=>$value)
	{
		$key = 1*$key;	
		if($value > 0)
		{
			//echo $key . " " . $value . " " . $ir[$key] . " " . $fy[$key]. "<br />";
			
			$fields = array();
			$values = array();

			$fields[] = "cer_fixed_rent_draft_id";
			$values[] = dbquote(param("did"));

			$fields[] = "cer_fixed_rent_unit";
			$values[] = dbquote($un[$key]);

			
			$total_surface = param("cer_fixed_rent_pos_surface")+param("cer_fixed_rent_other_surface");
			$fields[] = "cer_fixed_rent_total_surface";
			$values[] = dbquote($total_surface);

			$fields[] = "cer_fixed_rent_pos_surface";
			$values[] = dbquote(param("cer_fixed_rent_pos_surface"));

			$fields[] = "cer_fixed_rent_other_surface";
			$values[] = dbquote(param("cer_fixed_rent_other_surface"));
			

			$fields[] = "cer_fixed_rent_amount";
			$values[] = dbquote($value);

			$fields[] = "cer_fixed_rent_index_rate";
			$values[] = dbquote($ir[$key]);

			$fields[] = "cer_fixed_rent_increas_rate";
			$values[] = dbquote($ir2[$key]);

			$fields[] = "cer_fixed_rent_inflation_rate";
			$values[] = dbquote($in[$key]);


			$fields[] = "cer_fixed_rent_index_startdate";
			$values[] = dbquote(from_system_date($ird[$key]));

			$fields[] = "cer_fixed_rent_increas_startdate";
			$values[] = dbquote(from_system_date($ir2d[$key]));

			$fields[] = "cer_fixed_rent_inflation_startdate";
			$values[] = dbquote(from_system_date($ind[$key]));

			$fields[] = "cer_fixed_rent_from_year";
			$values[] = dbquote($fy[$key]);

			$fields[] = "cer_fixed_rent_from_month";
			$values[] = dbquote($fm[$key]);

			$fields[] = "cer_fixed_rent_from_day";
			$values[] = dbquote($fd[$key]);

			$fields[] = "cer_fixed_rent_to_year";
			$values[] = dbquote($ty[$key]);

			$fields[] = "cer_fixed_rent_to_month";
			$values[] = dbquote($tm[$key]);

			$fields[] = "cer_fixed_rent_to_day";
			$values[] = dbquote($td[$key]);


			$fields[] = "cer_fixed_rent_number_of_months";
			$values[] = dbquote($v2[$key]);

			$fields[] = "cer_fixed_rent_tax_rate";
			$values[] = dbquote($tr[$key]);

			$fields[] = "cer_fixed_rent_passenger_rate";
			$values[] = dbquote($pr[$key]);

			if($cer_basicdata["cer_basicdata_sales_guarantee"] == 1) {
				$fields[] = "cer_fixed_rent_sales_guarantee_amount";
				$values[] = dbquote($sg_amounts[$key]);

				$fields[] = "cer_fixed_rent_sales_guarantee_percent";
				$values[] = dbquote($sg_percents[$key]);

				$fields[] = "cer_fixed_rent_sales_guarantee_base";
				$values[] = dbquote($sg_bases[$key]);
			}

			$fields[] = "date_created";
			$values[] = "now()";

			$fields[] = "user_created";
			$values[] = dbquote(user_login());

			$sql = "insert into cer_draft_fixed_rents (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

			//echo $sql . "<br />";
			mysql_query($sql) or dberror($sql);

			$id = mysql_insert_id();

			$price_records[$id] = array("fy"=>$fy[$key], "fm"=>$fm[$key], "fd"=>$fd[$key], "ty"=>$ty[$key], "tm"=>$tm[$key], "td"=>$td[$key], "amount"=>$value, "surface"=>$total_surface, "unit"=>$un[$key], "indexrate"=>$ir[$key], "increaserate"=>$ir2[$key], "inflationrate"=>$in[$key], "taxrate"=>$tr[$key], "passengerrate"=>$pr[$key], "index_start_year"=>$ird_y[$key], "index_start_month"=>$ird_m[$key], "index_start_day"=>$ird_d[$key], "increase_start_year"=>$ir2d_y[$key], "increase_start_month"=>$ir2d_m[$key], "increase_start_day"=>$ir2d_d[$key], "inflation_start_year"=>$ind_y[$key], "inflation_start_month"=>$ind_m[$key], "inflation_start_day"=>$ind_d[$key]);
			
		}
	}


	//save data of turnoverbased rental information
	$sql = "delete from cer_draft_rent_percent_from_sales " . 
		   " where cer_rent_percent_from_sale_draft_id = " . dbquote(param("did"));
	mysql_query($sql) or dberror($sql);

	$v1 = $list1->values("cer_rent_percent_from_sale_amount");

	$fy = $list1->values("cer_rent_percent_from_sale_from_year");
	$fm = $list1->values("cer_rent_percent_from_sale_from_month");
	$fd = $list1->values("cer_rent_percent_from_sale_from_day");
	$ty = $list1->values("cer_rent_percent_from_sale_to_year");
	$tm = $list1->values("cer_rent_percent_from_sale_to_month");
	$td = $list1->values("cer_rent_percent_from_sale_to_day");

	$v2 = $list1->values("cer_rent_percent_from_sale_number_of_months");

	$pct = $list1->values("cer_rent_percent_from_sale_percent");

	$tr1 = $list1->values("cer_rent_percent_from_sale_tax_rate");
	$pr1 = $list1->values("cer_rent_percent_from_sale_passenger_rate");

	
	foreach($pct as $key=>$value)
	{
		$key = 1*$key;	
		if($value > 0)
		{
			//echo $key . " " . $value . " " . $ir[$key] . " " . $fy[$key]. "<br />";
			$fields = array();
			$values = array();

			$fields[] = "cer_rent_percent_from_sale_draft_id";
			$values[] = dbquote(param("did"));

			$fields[] = "cer_rent_percent_from_sale_percent";
			$values[] = dbquote($value);

			$fields[] = "cer_rent_percent_from_sale_amount";
			$values[] = dbquote($v1[$key]);

			$fields[] = "cer_rent_percent_from_sale_from_year";
			$values[] = dbquote($fy[$key]);

			$fields[] = "cer_rent_percent_from_sale_from_month";
			$values[] = dbquote($fm[$key]);

			$fields[] = "cer_rent_percent_from_sale_from_day";
			$values[] = dbquote($fd[$key]);

			$fields[] = "cer_rent_percent_from_sale_to_year";
			$values[] = dbquote($ty[$key]);

			$fields[] = "cer_rent_percent_from_sale_to_month";
			$values[] = dbquote($tm[$key]);

			$fields[] = "cer_rent_percent_from_sale_to_day";
			$values[] = dbquote($td[$key]);


			$fields[] = "cer_rent_percent_from_sale_number_of_months";
			$values[] = dbquote($v2[$key]);

			$fields[] = "cer_rent_percent_from_sale_tax_rate";
			$values[] = dbquote($tr1[$key]);

			$fields[] = "cer_rent_percent_from_sale_passenger_rate";
			$values[] = dbquote($pr1[$key]);

			$fields[] = "date_created";
			$values[] = "now()";

			$fields[] = "user_created";
			$values[] = dbquote(user_login());

			$sql = "insert into cer_draft_rent_percent_from_sales (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

			//echo $sql . "<br />";
			mysql_query($sql) or dberror($sql);

			$id = mysql_insert_id();
		}
	}


	require_once("include/helper_calculate_fixed_rents.php");
	
	$result = update_turnoverbased_rental_cost(param("did"));
	

	if($list->button("add_new_line")) 
	{

		$tmp_unit = 2;
		foreach($un as $key=>$value)
		{
			$tmp_unit = $value;
		}

		$fields = array();
		$values = array();

		$fields[] = "cer_fixed_rent_draft_id";
		$values[] = dbquote(param("did"));
		
		$fields[] = "cer_fixed_rent_unit";
		$values[] = dbquote($tmp_unit);

		$fields[] = "date_created";
		$values[] = "now()";

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$sql = "insert into cer_draft_fixed_rents (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
		
		mysql_query($sql) or dberror($sql);

	}

	if($list->button("add_new_line2")) 
	{

		$fields = array();
		$values = array();

		$fields[] = "cer_rent_percent_from_sale_draft_id";
		$values[] = dbquote(param("did"));

		$fields[] = "date_created";
		$values[] = "now()";

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$sql = "insert into cer_draft_rent_percent_from_sales (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
		
		mysql_query($sql) or dberror($sql);

	}



	$link = "cer_draft_rental.php?did=" . param("did");
	redirect($link);
}
elseif($list_sg->button("update_fixed_rents")) {
	
	
	//save data of fixed rents
	$sql = "delete from cer_draft_fixed_rents " . 
		   " where cer_fixed_rent_draft_id = " . dbquote(param("did"));
	mysql_query($sql) or dberror($sql);


	$sql = "delete from cer_draft_rent_percent_from_sales " . 
		   " where cer_rent_percent_from_sale_draft_id = " . dbquote(param("did"));
	mysql_query($sql) or dberror($sql);
	
	$fy = $list_sg->values("cer_fixed_rent_from_year_sg");
	$fm = $list_sg->values("cer_fixed_rent_from_month_sg");
	$fd = $list_sg->values("cer_fixed_rent_from_day_sg");
	$ty = $list_sg->values("cer_fixed_rent_to_year_sg");
	$tm = $list_sg->values("cer_fixed_rent_to_month_sg");
	$td = $list_sg->values("cer_fixed_rent_to_day_sg");
	$v2 = $list_sg->values("cer_fixed_rent_number_of_months_sg");

	$sg_amounts = $list_sg->values("cer_fixed_rent_sales_guarantee_amount");
	$sg_percents = $list_sg->values("cer_fixed_rent_sales_guarantee_percent");
	$sg_bases = $list_sg->values("cer_fixed_rent_sales_guarantee_base");

	$price_records = array();

	foreach($sg_amounts as $key=>$value)
	{
		$key = 1*$key;	
		if($sg_amounts > 0 and $sg_bases[$key] > 0)
		{
			
			//calculate fixed rental amount
			
			if($sg_bases[$key] == 1) {
				$fixed_rent = 12*$sg_amounts[$key]*$sg_percents[$key]/100;
			}
			else {
				$fixed_rent = $sg_amounts[$key]*$sg_percents[$key]/100;
			}

			
			$fields = array();
			$values = array();

			$fields[] = "cer_fixed_rent_draft_id";
			$values[] = dbquote(param("did"));

			$fields[] = "cer_fixed_rent_unit";
			$values[] = 6;


			$total_surface = param("cer_fixed_rent_pos_surface")+param("cer_fixed_rent_other_surface");
			$fields[] = "cer_fixed_rent_total_surface";
			$values[] = dbquote($total_surface);

			$fields[] = "cer_fixed_rent_pos_surface";
			$values[] = dbquote(param("cer_fixed_rent_pos_surface"));

			$fields[] = "cer_fixed_rent_other_surface";
			$values[] = dbquote(param("cer_fixed_rent_other_surface"));

			$fields[] = "cer_fixed_rent_amount";
			$values[] = $fixed_rent;

			$fields[] = "cer_fixed_rent_from_year";
			$values[] = dbquote($fy[$key]);

			$fields[] = "cer_fixed_rent_from_month";
			$values[] = dbquote($fm[$key]);

			$fields[] = "cer_fixed_rent_from_day";
			$values[] = dbquote($fd[$key]);

			$fields[] = "cer_fixed_rent_to_year";
			$values[] = dbquote($ty[$key]);

			$fields[] = "cer_fixed_rent_to_month";
			$values[] = dbquote($tm[$key]);

			$fields[] = "cer_fixed_rent_to_day";
			$values[] = dbquote($td[$key]);

			$fields[] = "cer_fixed_rent_number_of_months";
			$values[] = dbquote($v2[$key]);

			$fields[] = "cer_fixed_rent_sales_guarantee_amount";
			$values[] = dbquote($sg_amounts[$key]);

			$fields[] = "cer_fixed_rent_sales_guarantee_percent";
			$values[] = dbquote($sg_percents[$key]);

			$fields[] = "cer_fixed_rent_sales_guarantee_base";
			$values[] = dbquote($sg_bases[$key]);
	

			$fields[] = "date_created";
			$values[] = "now()";

			$fields[] = "user_created";
			$values[] = dbquote(user_login());

			$sql = "insert into cer_draft_fixed_rents (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

			mysql_query($sql) or dberror($sql);

			$id = mysql_insert_id();



			//vuild price records
			$un[$key] = 6;
			$tr[$key] = 0;
			$pr[$key] = 0;
			$ir[$key] = 0;
			$ir2[$key] = 0;
			$in[$key] = 0;
			$ird_y[$key] = 0;
			$ird_m[$key] =  0;
			$ird_d[$key] =  0;
			$ird2_y[$key] = 0;
			$ird2_m[$key] =  0;
			$ird2_d[$key] =  0;
			$ind_y[$key] = 0;
			$ind_m[$key] =  0;
			$ind_d[$key] =  0;
			

			$price_records[$id] = array("fy"=>$fy[$key], "fm"=>$fm[$key], "fd"=>$fd[$key], "ty"=>$ty[$key], "tm"=>$tm[$key], "td"=>$td[$key], "amount"=>$fixed_rent, "surface"=>$total_surface, "unit"=>$un[$key], "indexrate"=>$ir[$key], "increaserate"=>$ir2[$key], "inflationrate"=>$in[$key], "taxrate"=>$tr[$key], "passengerrate"=>$pr[$key], "index_start_year"=>$ird_y[$key], "index_start_month"=>$ird_m[$key], "index_start_day"=>$ird_d[$key], "increase_start_year"=>$ir2d_y[$key], "increase_start_month"=>$ir2d_m[$key], "increase_start_day"=>$ir2d_d[$key], "inflation_start_year"=>$ind_y[$key], "inflation_start_month"=>$ind_m[$key], "inflation_start_day"=>$ind_d[$key]);

			//add record for turn over based rents

			if($sg_bases[$key] == 1) {
				$breakpoint = 12*$sg_amounts[$key];
			}
			else {
				$breakpoint = $sg_amounts[$key];
			}


			$fields = array();
			$values = array();

			$fields[] = "cer_rent_percent_from_sale_draft_id";
			$values[] = dbquote(param("did"));


			$fields[] = "cer_rent_percent_from_sale_percent";
			$values[] = dbquote($sg_percents[$key]);

			$fields[] = "cer_rent_percent_from_sale_amount";
			$values[] = dbquote($breakpoint);

			$fields[] = "cer_rent_percent_from_sale_from_year";
			$values[] = dbquote($fy[$key]);

			if($cer_basicdata['cer_basicdata_firstyear'] == $fy[$key]
				and $cer_basicdata['cer_basicdata_firstmonth'] > $fm[$key]) {

				$fields[] = "cer_rent_percent_from_sale_from_month";
				$values[] = dbquote($cer_basicdata['cer_basicdata_firstmonth']);

				$fields[] = "cer_rent_percent_from_sale_from_day";
				$values[] = 1;
			}
			else {
			
				$fields[] = "cer_rent_percent_from_sale_from_month";
				$values[] = dbquote($fm[$key]);

				$fields[] = "cer_rent_percent_from_sale_from_day";
				$values[] = dbquote($fd[$key]);
			}

			$fields[] = "cer_rent_percent_from_sale_to_year";
			$values[] = dbquote($ty[$key]);

			$fields[] = "cer_rent_percent_from_sale_to_month";
			$values[] = dbquote($tm[$key]);

			$fields[] = "cer_rent_percent_from_sale_to_day";
			$values[] = dbquote($td[$key]);

			$fields[] = "cer_rent_percent_from_sale_number_of_months";
			$values[] = dbquote($v2[$key]);

			$fields[] = "date_created";
			$values[] = "now()";

			$fields[] = "user_created";
			$values[] = dbquote(user_login());

			$sql = "insert into cer_draft_rent_percent_from_sales (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

			//echo $sql . "<br />";
			mysql_query($sql) or dberror($sql);

			$id = mysql_insert_id();



			
			
		}
	}

	require_once("include/helper_calculate_fixed_rents.php");
	$result = update_turnoverbased_rental_cost(param("did"));

	$link = "cer_draft_rental.php?did=" . param("did");
	redirect($link);
}



/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_drafts");
require "include/draft_page_actions.php";
$page->header();

$page->title($cer_basicdata['cer_basicdata_title'] . ": Rental Costs");

require_once("include/tabs_draft.php");
$form->render();

if($cer_basicdata["cer_basicdata_sales_guarantee"] == 1) {
	echo "<hr />";
	$list_sg->render();

	echo "<hr /><p>&nbsp;</p>";
	$list->render();
}
else {
	echo "<hr />";
	$list->render();
}

echo "<hr /><p>&nbsp;</p>";
$list2->render();

echo "<hr /><p>&nbsp;</p>";
$list3->render();

echo "<hr /><p>&nbsp;</p>";
$list4->render();

echo "<hr /><p>&nbsp;</p>";
$list1->render();


$form1->render();
echo "<hr /><p>&nbsp;</p>";

//new additional rental costs
//$list_e->render();
//echo "<hr /><p>&nbsp;</p>";


$list_a->render();
echo "<hr /><p>&nbsp;</p>";

$list_am->render();
echo "<hr /><p>&nbsp;</p>";
//end new additional rental costs

$form2->render();

?>

<div id="pos_surface_info" style="display:none;">
    Total area occupied by the boutique.
</div> 

<div id="other_surface_info" style="display:none;">
    Additional areas like corridors, common space, stock areas etc.
</div> 


<?php

$page->footer();
require "include/draft_footer_scripts.php";
require "include/helper_calculate_fixed_rents.js";

//new additional rental costs
require "include/helper_calculate_additional_rents.js";
//end new additional rental costs

?>