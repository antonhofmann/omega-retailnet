<?php

//workaround
$sql = 'update posorderspipeline set posorder_postype = (select project_postype from projects where project_order = posorder_order) where posorder_order > 0';
$result = mysql_query($sql) or dberror($sql);

$sql = 'update posorders set posorder_postype = (select project_postype from projects where project_order = posorder_order)  where posorder_order > 0';
$result = mysql_query($sql) or dberror($sql);

//end workaround

$dfrom = '';
$mfrom = '';
$dto = '';
$mto = '';

$sql = "select * from cer_benchmarks where cer_benchmark_id = " . param("id");
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$benchmark_shortcut = $row["cer_benchmark_shortcut"];
	$benchmark_title = $row["cer_benchmark_title"];
	$benchmark_filter = unserialize($row["cer_benchmark_filter"]);
	$benchmark_project_filter = $row["cer_benchmark_project_filter"];
	$base_currency = $row["cer_benchmark_basic_currency"];

	$project_numbers = trim($benchmark_project_filter);
	$projects = explode(";",$project_numbers);
	if(isset($projects[0]) and !$projects[0])
	{
		$projects = array();
	}

	$benchmark_pipeline = $row["cer_benchmark_include_pipeline"];
	$benchmark_only_latest = $row["cer_benchmark_last_project"];
	$benchmark_project_state = $row["cer_benchmark_project_state"];
	$benchmark_from_year = $row["cer_benchmark_from_year"];
	$benchmark_to_year = $row["cer_benchmark_to_year"];
	$benchmark_from_state = $row["cer_benchmark_from_state"];
	$benchmark_to_state = $row["cer_benchmark_to_state"];


	if(array_key_exists("dfrom", $benchmark_filter)) {
		$dfrom = $benchmark_filter["dfrom"];
	}
	if(array_key_exists("mfrom", $benchmark_filter)) {
		$mfrom = $benchmark_filter["mfrom"];
	}
	if(array_key_exists("dto", $benchmark_filter)) {
		$dto = $benchmark_filter["dto"];
	}
	if(array_key_exists("mto", $benchmark_filter)) {
		$mto = $benchmark_filter["mto"];
	}
}

$filter = "";
$filter_regions = "";
$filter_countries = "";
$filter_cities = "";
$filter_areas = "";
$filter_project_costtypes = "";
$filter_product_lines = "";
$filter_product_line_subclasses = "";
$filter_pos_types = "";
$filter_possubclasses = "";
$filter_project_kinds = "";


if(count($benchmark_filter["re"]) > 0)
{
	$tmp = explode("-", $benchmark_filter["re"]);
	foreach($tmp as $key=>$value)
	{
		if($value)
		{
			$filter_regions .= $value . ",";
		}
	}
	$filter_regions = "(" . substr($filter_regions, 0, strlen($filter_regions)-1) . ")";
	if($filter_regions != "()")
	{
		$filter .= "country_salesregion in " . $filter_regions . " ";
	}
}

if(count($benchmark_filter["co"]) > 0)
{
	$tmp = explode("-", $benchmark_filter["co"]);
	foreach($tmp as $key=>$value)
	{
		if($value)
		{
			$filter_countries .= $value . ",";
		}
	}
	$filter_countries = "(" . substr($filter_countries, 0, strlen($filter_countries)-1) . ")";
	if($filter_countries != "()")
	{
		if($filter)
		{
			$filter .= " and posaddress_country in " . $filter_countries . " ";
		}
		else
		{
			$filter .= "posaddress_country in " . $filter_countries . " ";
		}
	}
}

if(count($benchmark_filter["ci"]) > 0)
{
	$tmp = explode("-", $benchmark_filter["ci"]);
	foreach($tmp as $key=>$value)
	{
		if($value)
		{
			$filter_cities .= "'" . $value . "',";
		}
	}
	$filter_cities = "(" . substr($filter_cities, 0, strlen($filter_cities)-1) . ")";
	if($filter_cities != "()")
	{
		if($filter)
		{
			$filter .= " and posaddress_place in " . $filter_cities . " ";
		}
		else
		{
			$filter .= "posaddress_place in " . $filter_cities . " ";
		}
	}
}

if(count($benchmark_filter["ar"]) > 0)
{
	$tmp = explode("-", $benchmark_filter["ar"]);
	foreach($tmp as $key=>$value)
	{
		if($value)
		{
			$filter_areas .= $value . ",";
		}
	}
	$filter_areas = "(" . substr($filter_areas, 0, strlen($filter_areas)-1) . ")";
	if($filter_areas != "()")
	{
		if($filter)
		{
			$filter .= " and posarea_area in " . $filter_areas . " ";
		}
		else
		{
			$filter .= "posarea_area in " . $filter_areas . " ";
		}
	}
}


if(count($benchmark_filter["pct"]) > 0)
{
	$tmp = explode("-", $benchmark_filter["pct"]);
	foreach($tmp as $key=>$value)
	{
		if($value)
		{
			$filter_project_costtypes .= $value . ",";
		}
	}
	$filter_project_costtypes = "(" . substr($filter_project_costtypes, 0, strlen($filter_project_costtypes)-1) . ")";
	if($filter_project_costtypes != "()")
	{
		if($filter)
		{
			$filter .= " and posorder_legal_type in " . $filter_project_costtypes . " ";
		}
		else
		{
			$filter .= "posorder_legal_type in " . $filter_project_costtypes . " ";
		}
	}
}

if(count($benchmark_filter["pl"]) > 0)
{
	$tmp = explode("-", $benchmark_filter["pl"]);
	foreach($tmp as $key=>$value)
	{
		if($value)
		{
			$filter_product_lines .= $value . ",";
		}
	}
	$filter_product_lines = "(" . substr($filter_product_lines, 0, strlen($filter_product_lines)-1) . ")";
	if($filter_product_lines != "()")
	{
		/*
		if($filter)
		{
			$filter .= " and posaddress_store_furniture in " . $filter_product_lines . " ";
		}
		else
		{
			$filter .= "(posaddress_store_furniture in " . $filter_product_lines . " ";
		}
		*/

		
		if($filter)
		{
			$filter .= " and (project_product_line in " . $filter_product_lines . " or posaddress_store_furniture in " . $filter_product_lines . ") ";
		}
		else
		{
			$filter .= "(project_product_line in " . $filter_product_lines . " or posaddress_store_furniture in " . $filter_product_lines . ") ";
		}
		
	}
}


if(array_key_exists("pls", $benchmark_filter) and count($benchmark_filter["pls"]) > 0)
{
	
	$tmp = explode("-", $benchmark_filter["pls"]);
	foreach($tmp as $key=>$value)
	{
		if($value)
		{
			$filter_product_line_subclasses .= $value . ",";
		}
	}
	$filter_product_line_subclasses = "(" . substr($filter_product_line_subclasses, 0, strlen($filter_product_line_subclasses)-1) . ")";
	if($filter_product_line_subclasses != "()")
	{
		
		if($filter)
		{
			$filter .= " and (project_product_line_subclass in " . $filter_product_line_subclasses . " or posaddress_store_furniture_subclass in " . $filter_product_line_subclasses . ") ";
		}
		else
		{
			$filter .= "(project_product_line_subclass in " . $filter_product_line_subclasses . " or posaddress_store_furniture_subclass in " . $filter_product_line_subclasses . ") ";
		}
		
	}
}

if(count($benchmark_filter["pt"]) > 0)
{
	$tmp = explode("-", $benchmark_filter["pt"]);
	foreach($tmp as $key=>$value)
	{
		if($value)
		{
			$filter_pos_types .= "'" . $value . "',";
		}
	}
	$filter_pos_types = "(" . substr($filter_pos_types, 0, strlen($filter_pos_types)-1) . ")";
	if($filter_pos_types != "()")
	{
		if($filter)
		{
			$filter .= " and postype_name in " . $filter_pos_types . " ";
		}
		else
		{
			$filter .= "postype_name in " . $filter_pos_types . " ";
		}
	}
}


if(array_key_exists("pkinds", $benchmark_filter) and count($benchmark_filter["pkinds"]) > 0)
{
	$tmp = explode("-", $benchmark_filter["pkinds"]);
	foreach($tmp as $key=>$value)
	{
		if($value)
		{
			$filter_project_kinds .= "'" . $value . "',";
		}
	}
	$filter_project_kinds = "(" . substr($filter_project_kinds, 0, strlen($filter_project_kinds)-1) . ")";
	if($filter_project_kinds != "()")
	{
		if($filter)
		{
			$filter .= " and project_projectkind in " . $filter_project_kinds . " ";
		}
		else
		{
			$filter .= "project_projectkind in " . $filter_project_kinds . " ";
		}
	}
}

if(count($benchmark_filter["sc"]) > 0)
{
	$tmp = explode("-", $benchmark_filter["sc"]);
	foreach($tmp as $key=>$value)
	{
		if($value)
		{
			$filter_possubclasses .= $value . ",";
		}
	}
	$filter_possubclasses = "(" . substr($filter_possubclasses, 0, strlen($filter_possubclasses)-1) . ")";
	if($filter_possubclasses != "()")
	{
		/*
		if($filter)
		{
			$filter .= " and posaddress_store_subclass in " . $filter_possubclasses . " ";
		}
		else
		{
			$filter .= "posaddress_store_subclass in " . $filter_possubclasses . " ";
		}
		*/


		if($filter)
		{
			$filter .= " and (project_pos_subclass in " . $filter_possubclasses . " or posaddress_store_subclass in " . $filter_possubclasses . ") ";
		}
		else
		{
			$filter .= "(project_pos_subclass in " . $filter_possubclasses . " or posaddress_store_subclass in " . $filter_possubclasses . " )";
		}
	}
}

if($benchmark_project_state > 0)
{
	if($filter)
	{
		$filter .= " and project_state = " . $benchmark_project_state . " ";
	}
	else
	{
		$filter .= "project_state = " . $benchmark_project_state . " ";
	}
}



if(isset($benchmark_id) 
	and $benchmark_id == '05'
	and $dfrom > 0)
{
	if($filter)
	{
		$filter .= ' and DAY(order_date) >= ' . $dfrom . ' ';
	}
	else
	{
		$filter .= 'DAY(order_date) >= ' . $dfrom . ' ';
	}
}

if(isset($benchmark_id) 
	and $benchmark_id == '05'
	and $mfrom > 0)
{
	if($filter)
	{
		$filter .= ' and MONTH(order_date) >= ' . $mfrom . ' ';
	}
	else
	{
		$filter .= 'MONTH(order_date) >= ' . $mfrom . ' ';
	}
}

if($benchmark_from_year > 0)
{
	if($filter)
	{
		$filter .= ' and YEAR(order_date) >= ' . $benchmark_from_year . ' ';
	}
	else
	{
		$filter .= 'YEAR(order_date) >= ' . $benchmark_from_year . ' ';
	}
}


if(isset($benchmark_id) 
	and $benchmark_id == '05'
	and $dto > 0)
{
	if($filter)
	{
		$filter .= ' and DAY(order_date) <= ' . $dto . ' ';
	}
	else
	{
		$filter .= 'DAY(order_date) <= ' . $dto . ' ';
	}
}

if(isset($benchmark_id) 
	and $benchmark_id == '05'
	and $mto > 0)
{
	if($filter)
	{
		$filter .= ' and MONTH(order_date) <= ' . $mto . ' ';
	}
	else
	{
		$filter .= 'MONTH(order_date) <= ' . $mto . ' ';
	}
}


if($benchmark_to_year > 0)
{
	if($filter)
	{
		$filter .= ' and YEAR(order_date) <= ' . $benchmark_to_year . ' ';
	}
	else
	{
		$filter .= 'YEAR(order_date) <= ' . $benchmark_to_year . ' ';
	}
}

if($benchmark_from_state > 0)
{
	if($filter)
	{
		$filter .= " and order_actual_order_state_code >= '" . $benchmark_from_state . "' ";
	}
	else
	{
		$filter .= "order_actual_order_state_code >= '" . $benchmark_from_state . "' ";
	}
}

if($benchmark_to_state > 0)
{
	if($filter)
	{
		$filter .= " and order_actual_order_state_code <= '" . $benchmark_to_state . "' ";
	}
	else
	{
		$filter .= "order_actual_order_state_code <= '" . $benchmark_to_state . "' ";
	}
}

if(count($projects) > 0)
{
	
	$pfilter = "";
	foreach($projects as $key=>$value)
	{
		$pfilter .= "'" . $value . "',";
	}
	$pfilter = substr($pfilter, 0, strlen($pfilter) - 1);

	if($filter)
	{
		$filter .= " and order_number in (" . $pfilter . ") ";
	}
	else
	{
		$filter .= " order_number in (" . $pfilter . ") ";
	}
}


$posorders = array();
$posorders_tmp = array();

if($benchmark_only_latest == 1)
{
	$posaddresses = array();
	$sql = "select posaddress_id, posorder_ordernumber, posorder_id " . 
		   "from posaddresses " .
		   "left join posorders on posorder_posaddress = posaddress_id " .
		   "left join orders on posorder_order = order_id " .
		   "left join posareas on posarea_posaddress = posaddress_id " .
		   "left join countries on country_id = posaddress_country " . 
		   "left join postypes on postype_id = posaddress_store_postype " .
 		   "left join projects on project_order = posorder_order " .
		   "where posorder_type = 1 ";

	if($filter)
	{
		$sql .= " and " . $filter . " order by posaddress_name";
	}

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$posaddresses[$row["posaddress_id"]] =  $row["posaddress_id"];
	}

	foreach($posaddresses as $key=>$posaddress_id)
	{
		$sql = "select posorder_ordernumber, posorder_id, project_cost_type, posorder_product_line " . 
			     " from posorders  " .
			     "left join projects on project_order = posorder_order " .
				 "left join project_costs on project_cost_order = posorder_order " .
		         "where posorder_type = 1 " . 
			     " and posorder_posaddress = " . $posaddress_id . 
			     " order by posorder_year DESC, posorder_id DESC";

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			
			if(strpos($filter_product_lines, $row["posorder_product_line"]) > 0)
			{
				if($row["project_cost_type"] == 1 
					or $row["project_cost_type"] == 3 
					or $row["project_cost_type"] == 4
					or $row["project_cost_type"] == 6) //CER
				{
					if(count($projects) > 0)
					{
						if(in_array($row["posorder_ordernumber"], $projects))
						{
							$posorders_tmp[$row["posorder_id"]] = array("pipeline" => 0, "id"=>$row["posorder_id"]);
						}
					}
					else
					{
						$posorders_tmp[$row["posorder_id"]] = array("pipeline" => 0, "id"=>$row["posorder_id"]);
					}
				}
				elseif($row["project_cost_type"] == 2 or $row["project_cost_type"] == 5)
				{
					if(count($projects) > 0)
					{
						if(in_array($row["posorder_ordernumber"], $projects))
						{
							$posorders_tmp[$row["posorder_id"]] = array("pipeline" => 0, "id"=>$row["posorder_id"]);
						}
					}
					else
					{
						$posorders_tmp[$row["posorder_id"]] = array("pipeline" => 0, "id"=>$row["posorder_id"]);
					}
				}
			}
		}
	}

	foreach($posorders_tmp as $key=>$posorder)
	{
		$posorders[] = $posorder;
	}

}
else
{

	$sql = "select posaddress_id, posorder_ordernumber, posorder_id, project_cost_type, posorder_product_line " .
	       "from posaddresses " .
		   "left join posorders on posorder_posaddress = posaddress_id " .
		   "left join posareas on posarea_posaddress = posaddress_id " .
		   "left join countries on country_id = posaddress_country " . 
		   "left join postypes on postype_id = posaddress_store_postype " .
		   "left join projects on project_order = posorder_order " .
		   "left join project_costs on project_cost_order = posorder_order " .
		   "left join orders on order_id = project_order " .
		   "where posorder_type = 1 ";
	
	if($filter)
	{
		$sql .= " and " . $filter . " order by posaddress_name";
	}

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		if($filter_product_lines != '()')
		{
			if(strpos($filter_product_lines, $row["posorder_product_line"]) > 0)
			{
			
				if($row["project_cost_type"] == 1 
					or $row["project_cost_type"] == 3 
					or $row["project_cost_type"] == 4
					or $row["project_cost_type"] == 6) //CER
				{
					if(count($projects) > 0)
					{
						if(in_array($row["posorder_ordernumber"], $projects))
						{
							$posorders_tmp[$row["posorder_id"]] = array("pipeline" => 0, "id"=>$row["posorder_id"]);
						}
					}
					else
					{
						$posorders_tmp[$row["posorder_id"]] = array("pipeline" => 0, "id"=>$row["posorder_id"]);
					}
				}
				elseif($row["project_cost_type"] == 2 or $row["project_cost_type"] == 5)
				{
					if(count($projects) > 0)
					{
						if(in_array($row["posorder_ordernumber"], $projects))
						{
							$posorders_tmp[$row["posorder_id"]] = array("pipeline" => 0, "id"=>$row["posorder_id"]);
						}
					}
					else
					{
						$posorders_tmp[$row["posorder_id"]] = array("pipeline" => 0, "id"=>$row["posorder_id"]);
					}
				}
			}
		}
		else
		{
			$posorders_tmp[$row["posorder_id"]] = array("pipeline" => 0, "id"=>$row["posorder_id"]);
		}
	}

	foreach($posorders_tmp as $key=>$posorder)
	{
		$posorders[] = $posorder;
	}
}



if($benchmark_pipeline == 1)
{
	
	//renovation projects
	$posorders_tmp = array();
	$sql = "select posaddress_id, posorder_ordernumber, posorder_id, project_cost_type, posorder_product_line " .
	       "from posaddresses " .
		   "left join posorderspipeline on posorder_posaddress = posaddress_id " .
		   "left join posareaspipeline on posarea_posaddress = posaddress_id " .
		   "left join countries on country_id = posaddress_country " . 
		   "left join postypes on postype_id = posorder_postype " .
		   "left join projects on project_order = posorder_order " .
   	       "left join project_costs on project_cost_order = posorder_order " .
		   "left join orders on order_id = project_order " .
		   "where posorder_type = 1 and posorder_parent_table = 'posaddresses' " . 
		   " and " . $filter . " order by posaddress_name";


	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		
		if($filter_product_lines != '()')
		{
			if(strpos($filter_product_lines, $row["posorder_product_line"]) > 0)
			{
				if($row["project_cost_type"] == 1 
					or $row["project_cost_type"] == 3 
					or $row["project_cost_type"] == 4
					or $row["project_cost_type"] == 6) //CER
				{
					if(count($projects) > 0)
					{
						if(in_array($row["posorder_ordernumber"], $projects))
						{
							$posorders_tmp[$row["posorder_id"]] = array("pipeline" => 2, "id"=>$row["posorder_id"]);
						}
					}
					else
					{
						$posorders_tmp[$row["posorder_id"]] = array("pipeline" => 2, "id"=>$row["posorder_id"]);
					}
				}
				elseif($row["project_cost_type"] == 2 or $row["project_cost_type"] == 5)
				{
					if(count($projects) > 0)
					{
						if(in_array($row["posorder_ordernumber"], $projects))
						{
							$posorders_tmp[$row["posorder_id"]] = array("pipeline" => 2, "id"=>$row["posorder_id"]);
						}
					}
					else
					{
						$posorders_tmp[$row["posorder_id"]] = array("pipeline" => 2, "id"=>$row["posorder_id"]);
					}
				}
			}
		}
		else
		{
			$posorders_tmp[$row["posorder_id"]] = array("pipeline" => 2, "id"=>$row["posorder_id"]);
		}
	}

	foreach($posorders_tmp as $key=>$posorder)
	{
		$posorders[] = $posorder;
	}


	//new projects
	$posorders_tmp = array();
	$sql = "select posaddress_id, posorder_ordernumber, posorder_id, project_cost_type, posorder_product_line " .
	       "from posaddressespipeline " .
		   "left join posorderspipeline on posorder_posaddress = posaddress_id " .
		   "left join posareaspipeline on posarea_posaddress = posaddress_id " .
		   "left join countries on country_id = posaddress_country " . 
		   "left join postypes on postype_id = posorder_postype " .
		   "left join projects on project_order = posorder_order " .
   	       "left join project_costs on project_cost_order = posorder_order " .
		   "left join orders on order_id = project_order " .
		   "where posorder_type = 1 and posorder_parent_table = 'posaddressespipeline'" . 
		   " and " . $filter . " order by posaddress_name";
	
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		
		if($filter_product_lines != '()')
		{
			if(strpos($filter_product_lines, $row["posorder_product_line"]) > 0)
			{
				if($row["project_cost_type"] == 1 
					or $row["project_cost_type"] == 3 
					or $row["project_cost_type"] == 4
					or $row["project_cost_type"] == 6) //CER
				{
					if(count($projects) > 0)
					{
						if(in_array($row["posorder_ordernumber"], $projects))
						{
							$posorders_tmp[$row["posorder_id"]] = array("pipeline" => 1, "id"=>$row["posorder_id"]);
						}
					}
					else
					{
						$posorders_tmp[$row["posorder_id"]] = array("pipeline" => 1, "id"=>$row["posorder_id"]);
					}
				}
				elseif($row["project_cost_type"] == 2 or $row["project_cost_type"] == 5)
				{
					if(count($projects) > 0)
					{
						if(in_array($row["posorder_ordernumber"], $projects))
						{
							$posorders_tmp[$row["posorder_id"]] = array("pipeline" => 1, "id"=>$row["posorder_id"]);
						}
					}
					else
					{
						$posorders_tmp[$row["posorder_id"]] = array("pipeline" => 1, "id"=>$row["posorder_id"]);
					}
				}
			}
		}
		else
		{
			$posorders_tmp[$row["posorder_id"]] = array("pipeline" => 1, "id"=>$row["posorder_id"]);
		}
	}


	foreach($posorders_tmp as $key=>$posorder)
	{
		$posorders[] = $posorder;
	}
	

}


?>