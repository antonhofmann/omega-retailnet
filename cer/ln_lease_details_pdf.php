<?php
/********************************************************************

    ln_lease_details_pdf.php

    Print Lease Details

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-09-26
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-09-26
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";


$cer_version = 0;
$ln_version = 0;
if(param("cerversion"))
{
	$cer_version = param("cerversion");
}
if(param("lnversion"))
{
	$ln_version = param("lnversion");
}


require_once "include/get_functions.php";
require "../shared/func_posindex.php";
require "include/financial_functions.php";
require "include/get_project.php";

check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}


/********************************************************************
	prepare pdf
*********************************************************************/

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');
require_once('../include/fpdi/fpdi.php'); 


//Instanciation of inherited class
$pdf = new FPDI("P", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 23, 10);
$pdf->setPrintHeader(false);
$pdf->SetAutoPageBreak(false);

$pdf->Open();


include("ln_lease_details_detail_pdf.php");

// write pdf
$pdf->Output(BRAND . '_lease_details_' . $posname . '_' . $project_number . '.pdf');

?>