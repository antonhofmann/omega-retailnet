<?php
/********************************************************************

    lnr_03_pdf_detail.php

    Print Detail Form LNR-03.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-06-17
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-06-17
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.
*********************************************************************/

//get sellout data
$sellout_data = array();
$profitability_data = array();

$number_of_pos = 0;

$sql = "select *, CAST(ln_basicdata_lnr03_distance AS UNSIGNED) as di from ln_basicdata_inr03 " . 
       "inner join possellouts on possellout_id = ln_basicdata_lnr03_sellout_id ". 
	   "left join posaddresses on posaddress_id = possellout_posaddress_id " .
	   "left join places on place_id = posaddress_place_id " .
	   "left join posowner_types on posowner_type_id = posaddress_ownertype " . 
	   "left join postypes on postype_id = posaddress_store_postype " . 
       "where ln_basicdata_lnr03_ln_version = " . $ln_version . "  and ln_basicdata_lnr03_sellout_id > 0 and ln_basicdata_lnr03_lnbasicdata_id = " . $ln_basicdata["ln_basicdata_id"] . 
	   " order by di asc";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$posaddress = $row["place_name"] . ", " . $row["posaddress_address"];
	$type = $row["posowner_type_name"] . " " . $row["postype_name"];
	
	$sellout_data[] = array("posname"=>$row["posaddress_name"],
		"address"=>$posaddress, 
		"type"=>$type,"distance"=>$row["ln_basicdata_lnr03_distance"], 
		"watches"=>$row["possellout_watches_units"], 
		"bijoux"=>$row["possellout_bijoux_units"], 
		"watchesvalue"=>$row["possellout_watches_grossales"], 
		"bijouxvalue"=>$row["possellout_bijoux_grossales"], 
		"ptc"=>$row["ln_basicdata_lnr03_ptc"]);
	
	$number_of_pos++;
	
}


//get profitability data
$profitability_data = array();

$sql = "select * from ln_basicdata_inr03 " . 
       "inner join possellouts on possellout_id = ln_basicdata_lnr03_profitability_id ". 
	   "left join posaddresses on posaddress_id = possellout_posaddress_id " .
	   "left join places on place_id = posaddress_place_id " .
	   "left join posowner_types on posowner_type_id = posaddress_ownertype " . 
	   "left join postypes on postype_id = posaddress_store_postype " . 
       "where ln_basicdata_lnr03_ln_version = " . $ln_version . "  and ln_basicdata_lnr03_profitability_id > 0 and ln_basicdata_lnr03_lnbasicdata_id = " . $ln_basicdata["ln_basicdata_id"];


$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	
	//get lease expiration date
	$lease_end_date = '';
	$sql_l = "select poslease_enddate from posleases where poslease_posaddress = " . dbquote($row["possellout_posaddress_id"]) . " order by poslease_enddate DESC";

	
	$res_l = mysql_query($sql_l) or dberror($sql_l);
	if ($row_l = mysql_fetch_assoc($res_l))
	{
		$lease_end_date = to_system_date($row_l["poslease_enddate"]);
	}
	
	$profitability_data[$row['posaddress_id']] = array("posname"=>$row["posaddress_name"], "watches"=>$row["possellout_watches_units"], "bijoux"=>$row["possellout_bijoux_units"], "netsales"=>$row["possellout_net_sales"], "grossmargin"=>$row["possellout_grossmargin"], "expenses"=>$row["possellout_operating_expenses"], "income01"=>$row["possellout_operating_income_excl"], "wsmargin"=>$row["possellout_wholsale_margin"], "income02"=>$row["possellout_operating_income_incl"], "openingdate"=>to_system_date($row["posaddress_store_openingdate"]),
	"closingdate"=>to_system_date($row["posaddress_store_closingdate"]),
	"lease_expiration"=>$lease_end_date, "pct"=>$row["ln_basicdata_lnr03_ptc"]);




}

//set pdf parameters
$margin_top = 12;
$margin_left = 12;
$y = $margin_top;
$x = $margin_left+1;
$standard_h = 6;

$pdf->AddPage("L");

// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "B", 10);
$pdf->Cell(51, 8, "Swatch Group", 1);

$pdf->SetFont("arialn", "B", 14);
$pdf->Cell(179, 8, "LEASE NEGOTIATION - ADDITIONAL ANALYSIS", 1, "", "C");


$pdf->SetFont("arialn", "", 10);
$pdf->SetFillColor(248,251,167);
if($ln_version == 0 and $ln_basicdata["ln_basicdata_locked"] == 0)
{
	$pdf->Cell(20, 8, date("d.m.Y"), 1, "", "C", true);
}
else
{
	$pdf->Cell(20, 8, to_system_date($ln_basicdata["versiondate"]), 1, "", "C", true);
}
$pdf->Cell(20, 8, "LNR-03", 1, "", "C");

	
	$y = $pdf->GetY()+10;
	$y_at_start = $y - 1;

	
	
	// 5. Top 10 distribution analysis of current stores in the area
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(268, 6, "5. Top 10 distribution analysis of current stores in the area (" . $project["order_shop_address_company"] . ", " . $project["order_number"] . ")", 1, "", "L");

		
	$y = $pdf->GetY()+6;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(68, $standard_h, "Retailer Name / Boutique Name", 1, "", "L");
	$pdf->Cell(70, $standard_h, "Location (mall name, street name)", 1, "", "L");
	$pdf->Cell(34, $standard_h, "Distance from prop. POS", 1, "", "R");
	$pdf->Cell(34, $standard_h, "Type", 1, "", "L");
	$pdf->Cell(17, $standard_h, "Units sold", 1, "", "R");
	$pdf->Cell(25, $standard_h, "Value sold (k" . $currency_symbol . ")", 1, "", "R");
	$pdf->Cell(20, $standard_h, "Prop. to close", 1, "", "R");
	
	
	
	
	$y = $pdf->GetY()+6;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	
	$num_of_lines = 0;
	$i=1;
	foreach($sellout_data as $key=>$data)
	{
		if($i<14)
		{
			$pdf->Cell(68, $standard_h, $data["posname"], 1, "", "L", true);
			$pdf->Cell(70, $standard_h, $data["address"], 1, "", "L", true);
			$pdf->Cell(34, $standard_h, $data["distance"], 1, "", "R", true);
			$pdf->Cell(34, $standard_h, $data["type"], 1, "", "L", true);
			$pdf->Cell(17, $standard_h, $data["watches"] + $data["bijoux"], 1, "", "R", true);
			$pdf->Cell(25, $standard_h, number_format(round(($data["watchesvalue"] + $data["bijouxvalue"])/1000,0), 0, '.', "'"), 1, "", "R", true);

			if($data["ptc"] == 1)
			{
				$pdf->Cell(20, $standard_h, "close" , 1, "", "R", true);
			}
			else
			{
				$pdf->Cell(20, $standard_h, "" , 1, "", "R", true);
			}

			$y = $pdf->GetY()+$standard_h;
			$pdf->SetXY($margin_left+1,$y);
			$i++;
			$num_of_lines++;
		}	
	}

	if($number_of_pos < 10) {
		$pdf->SetTextColor(250,3,15);
		$pdf->Cell(40, $standard_h, "There are no further POS within a radius of 100km.", 0, "", "L");
		$pdf->SetTextColor(0,0,0);	
	}
	
	
	/*
	for($i=($i-1);$i<11;$i++)
	{
		$pdf->Cell(68, $standard_h, "", 1, "", "L", true);
		$pdf->Cell(70, $standard_h, "", 1, "", "L", true);
		$pdf->Cell(34, $standard_h, "", 1, "", "R", true);
		$pdf->Cell(34, $standard_h, "", 1, "", "L", true);
		$pdf->Cell(17, $standard_h, "", 1, "", "R", true);
		$pdf->Cell(25, $standard_h, "", 1, "", "R", true);
		$pdf->Cell(20, $standard_h, "", 1, "", "R", true);

		$y = $pdf->GetY()+$standard_h;
		$pdf->SetXY($margin_left+1,$y);
	}
	*/

	

	//6. Profitability analysis of Corporate stores in the country (to be completed for Corporate store application only) (4)


	if(count($sellout_data) + count($profitability_data) > 24) {
		
		//draw outer box
		$y = $pdf->GetY()-$margin_top - 8;
		$pdf->SetXY($margin_left,$y_at_start);
		$pdf->Cell(270, $y, "", 1);


		$pdf->AddPage("L");

		// Title first line
		$pdf->SetXY($margin_left,$margin_top);
		$pdf->SetFont("arialn", "B", 10);
		$pdf->Cell(51, 8, "Swatch Group", 1);

		$pdf->SetFont("arialn", "B", 14);
		$pdf->Cell(179, 8, "LEASE NEGOTIATION - ADDITIONAL ANALYSIS", 1, "", "C");


		$pdf->SetFont("arialn", "", 10);
		$pdf->SetFillColor(248,251,167);
		if($ln_version == 0 and $ln_basicdata["ln_basicdata_locked"] == 0)
		{
			$pdf->Cell(20, 8, date("d.m.Y"), 1, "", "C", true);
		}
		else
		{
			$pdf->Cell(20, 8, to_system_date($ln_basicdata["versiondate"]), 1, "", "C", true);
		}
		$pdf->Cell(20, 8, "LNR-03", 1, "", "C");

		$y = $pdf->GetY()+10;
		$pdf->SetXY($margin_left+1,$y);
			
	}
	else {
		$y = $pdf->GetY()+7;
		$y = $pdf->GetY()+7;
		$pdf->SetXY($margin_left+1,$y);
	}

	$last_y = $y;

	
	
	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(268, 6, "6. Profitability analysis of Corporate stores in the country (to be completed for Corporate store application only)", 1, "", "L");

	$y = $pdf->GetY()+6;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "B", 9);
	$pdf->MultiCell(68, 1.8*$standard_h, "Retailer Name / Boutique Name", 1, "L", false);
	$pdf->SetXY($margin_left+69,$y);
	$pdf->MultiCell(17, 1.8*$standard_h, "Units total", 1, "R", false);
	$pdf->SetXY($margin_left+69 + 1*17,$y);
	$pdf->MultiCell(17, 1.8*$standard_h, "Net sales", 1, "R", false);
	$pdf->SetXY($margin_left+69 + 2*17,$y);
	$pdf->MultiCell(17, 1.8*$standard_h, "Gross margin", 1, "R", false);
	$pdf->SetXY($margin_left+69 + 3*17,$y);
	$pdf->MultiCell(25, 1.8*$standard_h, "Indirect operating expenses", 1, "R", false);
	$pdf->SetXY($margin_left+69 + 1*25 + 3*17,$y);
	$pdf->MultiCell(25, 1.8*$standard_h, "Operating income", 1, "R", false);
	$pdf->SetXY($margin_left+69 + 2*25+ 3*17,$y);
	$pdf->MultiCell(25, 1.8*$standard_h, "Wholesale margin (%)", 1, "R", false);
	$pdf->SetXY($margin_left+69 + 3*25+ 3*17,$y);
	$pdf->MultiCell(25, 1.8*$standard_h, "Operating income incl. whol. Margin", 1, "R", false);
	$pdf->SetXY($margin_left+69 + 4*25+ 3*17,$y);
	$pdf->MultiCell(25, 1.8*$standard_h, "Opening date", 1, "L", false);
	$pdf->SetXY($margin_left+69 + 5*25+ 3*17,$y);
	$pdf->MultiCell(24, 1.8*$standard_h, "Lease Expiration", 1, "L", false);
	

	$y = $pdf->GetY();
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	
	$footnotes = array();
	$i=1;
	$k=11;

	$last_y = $y;

	if(count($profitability_data) > 10) {
		$k=24;
	}
	foreach($profitability_data as $key=>$data)
	{
		if($i<$k)
		{
		    $pdf->SetTextColor(0,0,0);
			if($key == $project["project_relocated_posaddress_id"] and $data['pct'] == 1) {
				$pdf->SetTextColor(15,9,241);
				$footnotes[1] = "*will be closed after relocation";
				$data["posname"] = $data["posname"] . "*";
			}
			elseif($key == $project["project_relocated_posaddress_id"] and $data['pct'] != 1) {
				$pdf->SetTextColor(15,9,241);
				$footnotes[2] = "**will be relocated";
				$data["posname"] = $data["posname"] . "**";
			}
			elseif($data['closingdate'] != NULL and $data['closingdate']<> '0000-00-00') {
				$pdf->SetTextColor(250,3,15);
				$footnotes[3] = "***closed";
				$data["posname"] = $data["posname"] . "***";
			}
			elseif($data['pct'] == 1) {
				$pdf->SetTextColor(250,3,15);
				$footnotes[4] = "****will be closed";
				$data["posname"] = $data["posname"] . "****";
			}
			
			
			$pdf->Cell(68, $standard_h, $data["posname"], 1, "", "L", true);
			$pdf->Cell(17, $standard_h, $data["watches"] + $data["bijoux"], 1, "", "R", true);
			$pdf->Cell(17, $standard_h, number_format(round($data["netsales"]/1000,0), 0, '.', "'"), 1, "", "R", true);
			$pdf->Cell(17, $standard_h, number_format(round($data["grossmargin"]/1000,0), 0, '.', "'"), 1, "", "R", true);
			$pdf->Cell(25, $standard_h, number_format(round($data["expenses"]/1000,0), 0, '.', "'"), 1, "", "R", true);
			$pdf->Cell(25, $standard_h, number_format(round($data["income01"]/1000,0), 0, '.', "'"), 1, "", "R", true);
			$pdf->Cell(25, $standard_h, number_format($data["wsmargin"], 0, '.', "'") ."%", 1, "", "R", true);
			$pdf->Cell(25, $standard_h, number_format(round($data["income02"]/1000,0), 0, '.', "'"), 1, "", "R", true);
			$pdf->Cell(25, $standard_h, $data["openingdate"], 1, "", "R", true);
			$pdf->Cell(24, $standard_h, $data["lease_expiration"], 1, "", "R", true);

			$y = $pdf->GetY()+$standard_h;
			$pdf->SetXY($margin_left+1,$y);
			$i++;
			$num_of_lines++;

			$last_y = $y;
		}
	
	}
	
	if($num_of_lines > 22 and count($profitability_data) > 23) {
		
		//draw outer box
		$y = $pdf->GetY()-$margin_top - 8;
		$pdf->SetXY($margin_left,$y_at_start);
		$pdf->Cell(270, $y, "", 1);

		//set pdf parameters
		$margin_top = 12;
		$margin_left = 12;
		$y = $margin_top;
		$x = $margin_left+1;
		$standard_h = 6;

		$pdf->AddPage("L");

		// Title first line
		$pdf->SetXY($margin_left,$margin_top);
		$pdf->SetFont("arialn", "B", 10);
		$pdf->Cell(51, 8, "Swatch Group", 1);

		$pdf->SetFont("arialn", "B", 14);
		$pdf->Cell(179, 8, "LEASE NEGOTIATION - ADDITIONAL ANALYSIS", 1, "", "C");


		$pdf->SetFont("arialn", "", 10);
		$pdf->SetFillColor(248,251,167);
		if($ln_version == 0 and $ln_basicdata["ln_basicdata_locked"] == 0)
		{
			$pdf->Cell(20, 8, date("d.m.Y"), 1, "", "C", true);
		}
		else
		{
			$pdf->Cell(20, 8, to_system_date($ln_basicdata["versiondate"]), 1, "", "C", true);
		}
		$pdf->Cell(20, 8, "LNR-03", 1, "", "C");

		$y = $pdf->GetY()+10;
		$pdf->SetXY($margin_left+1,$y);

		$pdf->SetFont("arialn", "B", 11);
		$pdf->Cell(268, 6, "6. Profitability analysis of Corporate stores in the country (to be completed for Corporate store application only)", 1, "", "L");

		$y = $pdf->GetY()+6;
		$pdf->SetXY($margin_left+1,$y);

		$pdf->SetFont("arialn", "B", 9);
		$pdf->MultiCell(68, 1.8*$standard_h, "Retailer Name / Boutique Name", 1, "L", false);
		$pdf->SetXY($margin_left+69,$y);
		$pdf->MultiCell(17, 1.8*$standard_h, "Units total", 1, "R", false);
		$pdf->SetXY($margin_left+69 + 1*17,$y);
		$pdf->MultiCell(17, 1.8*$standard_h, "Net sales", 1, "R", false);
		$pdf->SetXY($margin_left+69 + 2*17,$y);
		$pdf->MultiCell(17, 1.8*$standard_h, "Gross margin", 1, "R", false);
		$pdf->SetXY($margin_left+69 + 3*17,$y);
		$pdf->MultiCell(25, 1.8*$standard_h, "Indirect operating expenses", 1, "R", false);
		$pdf->SetXY($margin_left+69 + 1*25 + 3*17,$y);
		$pdf->MultiCell(25, 1.8*$standard_h, "Operating income", 1, "R", false);
		$pdf->SetXY($margin_left+69 + 2*25+ 3*17,$y);
		$pdf->MultiCell(25, 1.8*$standard_h, "Wholesale margin (%)", 1, "R", false);
		$pdf->SetXY($margin_left+69 + 3*25+ 3*17,$y);
		$pdf->MultiCell(25, 1.8*$standard_h, "Operating income incl. whol. Margin", 1, "R", false);
		$pdf->SetXY($margin_left+69 + 4*25+ 3*17,$y);
		$pdf->MultiCell(25, 1.8*$standard_h, "Opening date", 1, "L", false);
		$pdf->SetXY($margin_left+69 + 5*25+ 3*17,$y);
		$pdf->MultiCell(24, 1.8*$standard_h, "Lease Expiration", 1, "L", false);
		

		$y = $pdf->GetY();
		$pdf->SetXY($margin_left+1,$y);
		$pdf->SetFont("arialn", "", 9);

		$i=1;
		$k=48;

		$last_y = $y;
		
		foreach($profitability_data as $key=>$data)
		{
			if($i>23 and $i<$k)
			{
				$pdf->SetTextColor(0,0,0);
				if($key == $project["project_relocated_posaddress_id"] and $data['pct'] == 1) {
					$pdf->SetTextColor(15,9,241);
					$footnotes[1] = "*will be closed after relocation";
					$data["posname"] = $data["posname"] . "*";
				}
				elseif($key == $project["project_relocated_posaddress_id"] and $data['pct'] != 1) {
					$pdf->SetTextColor(15,9,241);
					$footnotes[2] = "**will be relocated";
					$data["posname"] = $data["posname"] . "**";
				}
				elseif($data['closingdate'] != NULL and $data['closingdate']<> '0000-00-00') {
					$pdf->SetTextColor(250,3,15);
					$footnotes[3] = "***closed";
					$data["posname"] = $data["posname"] . "***";
				}
				elseif($data['pct'] == 1) {
					$pdf->SetTextColor(250,3,15);
					$footnotes[4] = "****will be closed";
					$data["posname"] = $data["posname"] . "****";
				}
				
				$pdf->Cell(68, $standard_h, $data["posname"], 1, "", "L", true);
				$pdf->Cell(17, $standard_h, $data["watches"] + $data["bijoux"], 1, "", "R", true);
				$pdf->Cell(17, $standard_h, number_format(round($data["netsales"]/1000, 0), 0, '.', "'"), 1, "", "R", true);
				$pdf->Cell(17, $standard_h, number_format(round($data["grossmargin"]/1000, 0), 0, '.', "'"), 1, "", "R", true);
				$pdf->Cell(25, $standard_h, number_format(round($data["expenses"]/1000, 0), 0, '.', "'"), 1, "", "R", true);
				$pdf->Cell(25, $standard_h, number_format(round($data["income01"]/1000, 0), 0, '.', "'"), 1, "", "R", true);
				$pdf->Cell(25, $standard_h, number_format($data["wsmargin"], 0, '.', "'") ."%", 1, "", "R", true);
				$pdf->Cell(25, $standard_h, number_format(round($data["income02"]/1000, 0), 0, '.', "'"), 1, "", "R", true);
				$pdf->Cell(25, $standard_h, $data["openingdate"], 1, "", "R", true);
				$pdf->Cell(24, $standard_h, $data["lease_expiration"], 1, "", "R", true);

				$y = $pdf->GetY()+$standard_h;
				$pdf->SetXY($margin_left+1,$y);
				$num_of_lines++;
				
			}
			$i++;
		}
		
		//draw outer box
		$last_y = $pdf->GetY();
		
		$y = $pdf->GetY()-$margin_top - 8;
		$pdf->SetXY($margin_left,$y_at_start);
		$pdf->Cell(270, $y, "", 1);

	}

	//print footnote
	if(count($footnotes) > 0
		or $number_of_pos < 10) {

		$pdf->SetXY($margin_left, $last_y);
		if(array_key_exists(1, $footnotes)) {
			$pdf->SetTextColor(15,9,241);
			$pdf->Cell(40, $standard_h, $footnotes[1], 0, "", "L");
		}
		if(array_key_exists(2, $footnotes)) {
			$pdf->SetTextColor(15,9,241);
			$pdf->Cell(40, $standard_h, $footnotes[2], 0, "", "L");
		}
		if(array_key_exists(3, $footnotes)) {
			$pdf->SetTextColor(250,3,15);
			$pdf->Cell(40, $standard_h, $footnotes[3], 0, "", "L");
		}
		if(array_key_exists(4, $footnotes)) {
			$pdf->SetTextColor(250,3,15);
			$pdf->Cell(40, $standard_h, $footnotes[4], 0, "", "L");
		}
		$pdf->SetTextColor(0,0,0);
		$last_y = $pdf->GetY();
	
	}
	


	//7. Conditions of other SG brands in same mall / street
	$new_page = false;
	if($num_of_lines >= 18) {
		//draw outer box
		
		$y = $pdf->GetY()-$margin_top - 8;
		$pdf->SetXY($margin_left,$y_at_start);
		$pdf->Cell(270, $y, "", 1);

		$new_page = true;

		

		$pdf->AddPage("L");
		$y = $pdf->GetY()+10;
		$y_at_start = $y - 1;


		// Title first line
		$pdf->SetXY($margin_left,$margin_top);
		$pdf->SetFont("arialn", "B", 10);
		$pdf->Cell(51, 8, "Swatch Group", 1);

		$pdf->SetFont("arialn", "B", 14);
		$pdf->Cell(179, 8, "LEASE NEGOTIATION - ADDITIONAL ANALYSIS", 1, "", "C");


		$pdf->SetFont("arialn", "", 10);
		$pdf->SetFillColor(248,251,167);
		if($ln_version == 0 and $ln_basicdata["ln_basicdata_locked"] == 0)
		{
			$pdf->Cell(20, 8, date("d.m.Y"), 1, "", "C", true);
		}
		else
		{
			$pdf->Cell(20, 8, to_system_date($ln_basicdata["versiondate"]), 1, "", "C", true);
		}
		$pdf->Cell(20, 8, "LNR-03", 1, "", "C");

		$y = $pdf->GetY()+10;
		$pdf->SetXY($margin_left+1,$y);
	}
	else {
		if($last_y) {
			$y = $last_y + 7;
		}
		else {
			$y = $pdf->GetY()+7;
		}
	}
	
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(268, 6, "7. Conditions of other SG brands in same mall / street (" . $project["order_shop_address_company"] . ")", 1, "", "L");
	

			
	$y = $pdf->GetY()+6;
	$pdf->SetXY($x,$y);

	$pdf->SetFont("arialn", "B", 7);
	
	$pdf->MultiCell(34,8,"Brand",1, "L", false, 0);
	$pdf->MultiCell(15,8,"Legal Type",1, "L", false, 0);
	$pdf->MultiCell(25,8,"POS Type",1, "L", false, 0);
	$pdf->MultiCell(16,8,"Contract year signed",1, "L", false, 0);
	$pdf->MultiCell(16,8,"Gross surface (sqm)",1, "R", false, 0);
	$pdf->MultiCell(75,8,"Rental conditions",1, "L", false, 0);
	$pdf->MultiCell(11,8,"Sales year",1, "L", false, 0);
	$pdf->MultiCell(16,8,"Gross sales"  . "\n" . " (k" . $currency_symbol . ")",1, "R", false, 0);
	$pdf->MultiCell(60,8,"Notes",1, "L", false, 1);
	
	$pdf->SetFont("arialn", "", 7);
	
	//get data
	$has_data = false;
	$sql = "select * from ln_basicdata_lnr03_brands " . 
		   " left join postypes on postype_id = ln_basicdata_lnr03_brand_postype_id " .
		   " left join posowner_types on posowner_type_id = ln_basicdata_lnr03_brand_legaltype_id " .
		   " left join sg_brands on sg_brand_id = ln_basicdata_lnr03_brand_sg_brand_id " . 
		   " where ln_basicdata_lnr03_brand_cerbasicdata_id = " . $cer_basicdata["cer_basicdata_id"] . 
		   "  and ln_basicdata_lnr03_brand_cer_version = " . $ln_version .
		   " order by sg_brand_name";

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$has_data = true;

		$y = $pdf->GetY();
		$pdf->SetXY($margin_left+1,$y);

		$amount = round($row["ln_basicdata_lnr03_brand_sales_amount"]/1000, 0);
		$pdf->MultiCell(34,0,$row["sg_brand_name"],1, "L", true, 0);
		$pdf->MultiCell(15,0,$row["posowner_type_name"],1, "L", true, 0);
		$pdf->MultiCell(25,0,$row["postype_name"],1, "L", true, 0);
		$pdf->MultiCell(16,0,$row["ln_basicdata_lnr03_brand_contract_year"],1, "L", true, 0);
		$pdf->MultiCell(16,0,$row["ln_basicdata_lnr03_brand_gross_surface"],1, "R", true, 0);

		$x = $pdf->getX()+75;
		$pdf->MultiCell(75,0,$row["ln_basicdata_lnr03_brand_rental_conidtions"],1, "L", true, 1);
		$y1 = $pdf->GetY();
		


		$pdf->setXY($x, $y);

		$pdf->MultiCell(11,0,$row["ln_basicdata_lnr03_brand_sales_year"],1, "L", true, 0);
		$pdf->MultiCell(16,0,$amount,1, "R", true, 0);
		$pdf->MultiCell(60,0,$row["ln_basicdata_lnr03_brand_notes"],1, "L", true, 1);
		$y2 = $pdf->GetY();
		
		$y = $pdf->GetY();
		if($y1 > $y){$y = $y1;}
		if($y2 > $y){$y = $y2;}
			
		$pdf->SetXY($x,$y);

		
		
	}

	if($has_data == false)
	{	
		$y = $pdf->GetY()+1;
		$pdf->SetXY($x,$y);
		$pdf->MultiCell(190,0,"There are no other Swatch Group Brands in the same mall/street.",0, "L", false, 0);
		$y = $pdf->GetY()+3;
		$pdf->SetXY($x,$y);
	
	}



	//draw outer box
	if($new_page == false) {
		$y = $pdf->GetY()-$margin_top - 8;
		$pdf->SetXY($margin_left,$y_at_start);
		$pdf->Cell(270, $y, "", 1);
	}
	else {
		$y = $pdf->GetY()-$margin_top + 2;
		$pdf->SetXY($margin_left,$margin_top);
		$pdf->Cell(270, $y, "", 1);

	}

		

	
?>