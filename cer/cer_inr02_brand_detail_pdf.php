<?php
/********************************************************************

    cer_inr02_brand_detail_pdf.php

    Print Brand Detail in LN-, AF, and CER-Booklets

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*********************************************************************/


//page data
$margin_top = 8;
$margin_left = 12;
$y = $margin_top;
$standard_y = 3.5;

$years1 = $years;
$years2 = array();
if(count($years) > 12)
{
	$years1 = array_slice($years,0,12); 
	$years2 = array_slice($years,12);
}

//BRAND Details
if(count($cer_brands) > 0)
{
	foreach($cer_brands as $brand_id=>$brand_name)
	{

		$x1 = $margin_left;
		$x2 = $margin_left+32;
		$x3 = $margin_left+52;
		
		$y = $margin_top;
		$y = $y + 6;
		$y2 = $y + $standard_y;   // for column2
		$y3 = $y + $standard_y;   // for column3
		$y4 = $y + $standard_y;   // for column4
		$y5 = $y + $standard_y;   // for column5
		$y6 = $y + $standard_y;   // for column6
		
		$pdf->AddPage();
		// draw page box
		$pdf->SetXY($margin_left-2,$margin_top);
		$pdf->Cell(280, 195, "", 1);
		// Title first line
		$pdf->SetXY($margin_left,$margin_top);
		$pdf->SetFont("arialn", "B", 11);
		$pdf->Cell(255, 6,  "Retail Business Plan Overview for: " . $brand_name . " - " . $project_name, 0, "", "L");



		//parameter
		$x1c2 = $margin_left;
		$x2c2 = $margin_left + 25;
		
		
		$pdf->SetXY($x1c2,$y2);
		$pdf->SetFont("arialn", "", 8);
		
		$pdf->Cell(50, 4, "Sales Reduction Watches", 0, "", "L");

		$pdf->SetXY($x2c2,$y2);
		$pdf->Cell(20, 4, number_format($sales_reduction_watches[$brand_id],2) . "%", 0, "", "R");

		$y2 = $y2+ $standard_y;

		$pdf->SetXY($x1c2,$y2);
		$pdf->Cell(50, 4, "Sales Reduction Jewellery", 0, "", "L");

		$pdf->SetXY($x2c2,$y2);
		$pdf->Cell(20, 4, number_format($sales_reduction_bijoux[$brand_id],2) . "%", 0, "", "R");

		$y2 = $y2+ $standard_y;

		$pdf->SetXY($x1c2,$y2);
		$pdf->Cell(50, 4, "Sales Reduction Accessories", 0, "", "L");

		$pdf->SetXY($x2c2,$y2);
		$pdf->Cell(20, 4, number_format($sales_reduction_accessories[$brand_id],2) . "%", 0, "", "R");

		$y2 = $y2+ $standard_y;
		$pdf->SetXY($x1c2,$y2);
		$pdf->Cell(50, 4, "Sales Reduction Services", 0, "", "L");
		$pdf->SetXY($x2c2,$y2);
		$pdf->Cell(20, 4, number_format($sales_reduction_services[$brand_id],2) . "%", 0, "", "R");

	
		//parameter

		$x1c2 = $margin_left + 55;
		$x2c2 = $x1c2 + 30;
		
		$y2 = $y4;
		$pdf->SetXY($x1c2,$y2);
		$pdf->SetFont("arialn", "", 8);
		
		$pdf->SetXY($x1c2,$y2);
		$pdf->Cell(50, 4, "Wholsale Margin Watches", 0, "", "L");

		$pdf->SetXY($x2c2,$y2);
		$pdf->Cell(20, 4, number_format($wholesale_margin_watches[$brand_id],2) . "%", 0, "", "R");

		$y2 = $y2+ $standard_y;

		$pdf->SetXY($x1c2,$y2);
		$pdf->Cell(50, 4, "Wholsale Margin Bijoux", 0, "", "L");

		$pdf->SetXY($x2c2,$y2);
		$pdf->Cell(20, 4, number_format($wholesale_margin_bijoux[$brand_id],2) . "%", 0, "", "R");


		$y2 = $y2+ $standard_y;

		$pdf->SetXY($x1c2,$y2);
		$pdf->Cell(50, 4, "Wholsale Margin Accessories", 0, "", "L");

		$pdf->SetXY($x2c2,$y2);
		$pdf->Cell(20, 4, number_format($wholesale_margin_accessories[$brand_id],2) . "%", 0, "", "R");


		$y2 = $y2+ $standard_y;

		$pdf->SetXY($x1c2,$y2);
		$pdf->Cell(50, 4, "Wholsale Margin Services", 0, "", "L");

		$pdf->SetXY($x2c2,$y2);
		$pdf->Cell(20, 4, number_format($wholesale_margin_service[$brand_id],2) . "%", 0, "", "R");


		
		//parameters

		$x1c2 = $margin_left + 115;
		$x2c2 = $x1c2 + 30;
		
		$y2 = $y4;
		$pdf->SetXY($x1c2,$y2);
		$pdf->SetFont("arialn", "", 8);
		
		
		$pdf->Cell(50, 4, "Cost of Watches", 0, "", "L");

		$pdf->SetXY($x2c2,$y2);
		$pdf->Cell(20, 4, number_format($cost_watches[$brand_id],2) . "%", 0, "", "R");

		$y2 = $y2+ $standard_y;

		$pdf->SetXY($x1c2,$y2);
		$pdf->Cell(50, 4, "Cost of Jewellery", 0, "", "L");

		$pdf->SetXY($x2c2,$y2);
		$pdf->Cell(20, 4, number_format($cost_bijoux[$brand_id],2) . "%", 0, "", "R");

		$y2 = $y2+ $standard_y;
		$pdf->SetXY($x1c2,$y2);
		$pdf->Cell(50, 4, "Cost of Accessories", 0, "", "L");
		$pdf->SetXY($x2c2,$y2);
		$pdf->Cell(20, 4, number_format($cost_accessories[$brand_id],2) . "%", 0, "", "R");

		$y2 = $y2+ $standard_y;
		$pdf->SetXY($x1c2,$y2);
		$pdf->Cell(50, 4, "Cost of Services", 0, "", "L");
		$pdf->SetXY($x2c2,$y2);
		$pdf->Cell(20, 4, number_format($cost_services[$brand_id],2) . "%", 0, "", "R");

		$y2 = $y2+ 2*$standard_y;

		$y = $pdf->getY() + 2;
		

		//sales values
		$captions = array();
		
		$captions[] = "Watches Units";
		$captions[] = "Jewellery Units";
		$captions[] = "Watches av. Price";
		$captions[] = "Jewellery Av. Price";
		$captions[] = "Sales Value Watches";
		$captions[] = "Sales Value Jewellery";
		$captions[] = "Sales Value Accessories";
		$captions[] = "Sales Value Services";
		$captions[] = "Total Gross Sales";
		$captions[] = "Sales Reduction";
		$captions[] = "Total Net Sales";
		$captions[] = "Cost of Watches Sold";
		$captions[] = "Cost of Jewellery Sold";
		$captions[] = "Cost of Accessories Sold";
		$captions[] = "Cost of Services Sold";
		$captions[] = "Cost of Products Sold";
		$captions[] = "Total Gross Margin";
		$captions[] = "";
		$captions[] = "Wholsale Margin Watches";
		$captions[] = "Wholsale Margin Jewellery";
		$captions[] = "Wholsale Margin Accessories";
		$captions[] = "Wholsale Margin Services";
		
		$y = $y+ $standard_y;
		$y = $y+ $standard_y;
		$y = $y+ $standard_y;
		$y_start = $y;
		$standard_xoffset = 18;

		$pdf->SetFont("arialn", "", 8);
		foreach($captions as $key=>$caption)
		{
			$pdf->SetXY($x1,$y);
			$pdf->Cell(50, 4, $caption, 0, "", "L");
			$y = $y+ $standard_y;
		}
		$pdf->SetFont("arialn", "B", 8);
		$xi = $standard_xoffset;
		$y = $y_start - $standard_y;
		foreach($years1 as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4, $year, 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}
		$pdf->SetFont("arialn", "", 8);
		$xi = $standard_xoffset;
		$y = $y_start;
		foreach($years1 as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  $sales_units_watches_values_per_brand[$brand_id][$year], 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years1 as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  $sales_units_jewellery_values_per_brand[$brand_id][$year], 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}
		
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years1 as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  $average_price_watches_values_per_brand[$brand_id][$year], 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years1 as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  $average_price_jewellery_values_per_brand[$brand_id][$year], 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}
		
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years1 as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($sales_watches_values_per_brand[$brand_id][$year],2), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years1 as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($sales_jewellery_values_per_brand[$brand_id][$year],2), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}

		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years1 as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($sales_accessories_values_per_brand[$brand_id][$year],2), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}
		
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years1 as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($sales_customer_service_values_per_brand[$brand_id][$year],2), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years1 as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($total_gross_sales_values_per_brand[$brand_id][$year],2), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years1 as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($sales_reduction_values_per_brand[$brand_id][$year],2), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years1 as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($total_net_sales_values_per_brand[$brand_id][$year],2), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}
		
		
		// costs of products sold
		
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years1 as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			if(array_key_exists($year, $cost_of_production_watches_values_per_brand[$brand_id]))
			{
				$pdf->Cell(12, 4,  number_format($cost_of_production_watches_values_per_brand[$brand_id][$year],2), 0, "", "R");
			}
			else
			{
				$pdf->Cell(12, 4,  '0', 0, "", "R");
			}
				
			$xi = $xi + $standard_xoffset;
		}
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years1 as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			if(array_key_exists($year, $cost_of_production_jewellery_values_per_brand[$brand_id]))
			{
				$pdf->Cell(12, 4,  number_format($cost_of_production_jewellery_values_per_brand[$brand_id][$year],2), 0, "", "R");
			}
			else
			{
				$pdf->Cell(12, 4,  '0', 0, "", "R");
			}
			$xi = $xi + $standard_xoffset;
		}
		
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years1 as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			if(array_key_exists($year, $cost_of_production_accessories_values_per_brand[$brand_id]))
			{
				$pdf->Cell(12, 4,  number_format($cost_of_production_accessories_values_per_brand[$brand_id][$year],2), 0, "", "R");
			}
			else
			{
				$pdf->Cell(12, 4,  '0' , 0, "", "R");
			}
			$xi = $xi + $standard_xoffset;
		}

		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years1 as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			if(array_key_exists($year, $cost_of_production_service_values_per_brand[$brand_id]))
			{
				$pdf->Cell(12, 4,  number_format($cost_of_production_service_values_per_brand[$brand_id][$year],2), 0, "", "R");
			}
			else
			{
				$pdf->Cell(12, 4,  '0' , 0, "", "R");
			}
			$xi = $xi + $standard_xoffset;
		}
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years1 as $key=>$year)
		{
			$cost_of_products_sold = 0;
			if(array_key_exists($year, $cost_of_production_watches_values_per_brand[$brand_id]))
			{
				$cost_of_products_sold = $cost_of_products_sold + $cost_of_production_watches_values_per_brand[$brand_id][$year];
			}
			if(array_key_exists($year, $cost_of_production_jewellery_values_per_brand[$brand_id]))
			{
				$cost_of_products_sold = $cost_of_products_sold + $cost_of_production_jewellery_values_per_brand[$brand_id][$year];
			}
			if(array_key_exists($year, $cost_of_production_accessories_values_per_brand[$brand_id]))
			{
				$cost_of_products_sold = $cost_of_products_sold + $cost_of_production_accessories_values_per_brand[$brand_id][$year];
			}
			if(array_key_exists($year, $cost_of_production_service_values_per_brand[$brand_id]))
			{
				$cost_of_products_sold = $cost_of_products_sold + $cost_of_production_service_values_per_brand[$brand_id][$year];
			}

			
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($cost_of_products_sold,2), 0, "", "R");

			
			$xi = $xi + $standard_xoffset;
		}
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years1 as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($total_gross_margin_values_per_brand[$brand_id][$year],2), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}

		$xi = $standard_xoffset;
		$y = $y+ 2*$standard_y;
		foreach($years1 as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($wholesale_margin_of_watches_sold_per_brand[$brand_id][$year],2), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}
		
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years1 as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($wholesale_margin_of_jewellery_sold_per_brand[$brand_id][$year],2), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}

		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years1 as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($wholesale_margin_of_accessories_sold_per_brand[$brand_id][$year],2), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}

		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years1 as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($wholesale_margin_of_services_sold_per_brand[$brand_id][$year],2), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}



		if(count($years2) > 0)
		{
			//sales values
			$y = $pdf->getY();
			
			$y = $y+ $standard_y;
			$y = $y+ $standard_y;
			$y = $y+ $standard_y;
			$y_start = $y;
			$standard_xoffset = 18;

			$pdf->SetFont("arialn", "", 8);
			foreach($captions as $key=>$caption)
			{
				$pdf->SetXY($x1,$y);
				$pdf->Cell(50, 4, $caption, 0, "", "L");
				$y = $y+ $standard_y;
			}
			$pdf->SetFont("arialn", "B", 8);
			$xi = $standard_xoffset;
			$y = $y_start - $standard_y;
			foreach($years2 as $key=>$year)
			{
				$pdf->SetXY($x2+$xi,$y);
				$pdf->Cell(12, 4, $year, 0, "", "R");
				$xi = $xi + $standard_xoffset;
			}
			$pdf->SetFont("arialn", "", 8);
			$xi = $standard_xoffset;
			$y = $y_start;
			foreach($years2 as $key=>$year)
			{
				$pdf->SetXY($x2+$xi,$y);
				$pdf->Cell(12, 4,  $sales_units_watches_values_per_brand[$brand_id][$year], 0, "", "R");
				$xi = $xi + $standard_xoffset;
			}
			$xi = $standard_xoffset;
			$y = $y+ $standard_y;
			foreach($years2 as $key=>$year)
			{
				$pdf->SetXY($x2+$xi,$y);
				$pdf->Cell(12, 4,  $sales_units_jewellery_values_per_brand[$brand_id][$year], 0, "", "R");
				$xi = $xi + $standard_xoffset;
			}
			
			$xi = $standard_xoffset;
			$y = $y+ $standard_y;
			foreach($years2 as $key=>$year)
			{
				$pdf->SetXY($x2+$xi,$y);
				$pdf->Cell(12, 4,  $average_price_watches_values_per_brand[$brand_id][$year], 0, "", "R");
				$xi = $xi + $standard_xoffset;
			}
			$xi = $standard_xoffset;
			$y = $y+ $standard_y;
			foreach($years2 as $key=>$year)
			{
				$pdf->SetXY($x2+$xi,$y);
				$pdf->Cell(12, 4,  $average_price_jewellery_values_per_brand[$brand_id][$year], 0, "", "R");
				$xi = $xi + $standard_xoffset;
			}
			
			$xi = $standard_xoffset;
			$y = $y+ $standard_y;
			foreach($years2 as $key=>$year)
			{
				$pdf->SetXY($x2+$xi,$y);
				$pdf->Cell(12, 4,  number_format($sales_watches_values_per_brand[$brand_id][$year],2), 0, "", "R");
				$xi = $xi + $standard_xoffset;
			}
			$xi = $standard_xoffset;
			$y = $y+ $standard_y;
			foreach($years2 as $key=>$year)
			{
				$pdf->SetXY($x2+$xi,$y);
				$pdf->Cell(12, 4,  number_format($sales_jewellery_values_per_brand[$brand_id][$year],2), 0, "", "R");
				$xi = $xi + $standard_xoffset;
			}

			$xi = $standard_xoffset;
			$y = $y+ $standard_y;
			foreach($years2 as $key=>$year)
			{
				$pdf->SetXY($x2+$xi,$y);
				$pdf->Cell(12, 4,  number_format($sales_accessories_values_per_brand[$brand_id][$year],2), 0, "", "R");
				$xi = $xi + $standard_xoffset;
			}
			
			$xi = $standard_xoffset;
			$y = $y+ $standard_y;
			foreach($years2 as $key=>$year)
			{
				$pdf->SetXY($x2+$xi,$y);
				$pdf->Cell(12, 4,  number_format($sales_customer_service_values_per_brand[$brand_id][$year],2), 0, "", "R");
				$xi = $xi + $standard_xoffset;
			}
			$xi = $standard_xoffset;
			$y = $y+ $standard_y;
			foreach($years2 as $key=>$year)
			{
				$pdf->SetXY($x2+$xi,$y);
				$pdf->Cell(12, 4,  number_format($total_gross_sales_values_per_brand[$brand_id][$year],2), 0, "", "R");
				$xi = $xi + $standard_xoffset;
			}
			$xi = $standard_xoffset;
			$y = $y+ $standard_y;
			foreach($years2 as $key=>$year)
			{
				$pdf->SetXY($x2+$xi,$y);
				$pdf->Cell(12, 4,  number_format($sales_reduction_values_per_brand[$brand_id][$year],2), 0, "", "R");
				$xi = $xi + $standard_xoffset;
			}
			$xi = $standard_xoffset;
			$y = $y+ $standard_y;
			foreach($years2 as $key=>$year)
			{
				$pdf->SetXY($x2+$xi,$y);
				$pdf->Cell(12, 4,  number_format($total_net_sales_values_per_brand[$brand_id][$year],2), 0, "", "R");
				$xi = $xi + $standard_xoffset;
			}
			// costs of products sold
			
			
			$xi = $standard_xoffset;
			$y = $y+ $standard_y;
			foreach($years2 as $key=>$year)
			{
				$pdf->SetXY($x2+$xi,$y);
				if(array_key_exists($year, $cost_of_production_watches_values_per_brand[$brand_id]))
				{
					$pdf->Cell(12, 4,  number_format($cost_of_production_watches_values_per_brand[$brand_id][$year],2), 0, "", "R");
				}
				else
				{
					$pdf->Cell(12, 4,  '0', 0, "", "R");
				}
					
				$xi = $xi + $standard_xoffset;
			}
			$xi = $standard_xoffset;
			$y = $y+ $standard_y;
			foreach($years2 as $key=>$year)
			{
				$pdf->SetXY($x2+$xi,$y);
				if(array_key_exists($year, $cost_of_production_jewellery_values_per_brand[$brand_id]))
				{
					$pdf->Cell(12, 4,  number_format($cost_of_production_jewellery_values_per_brand[$brand_id][$year],2), 0, "", "R");
				}
				else
				{
					$pdf->Cell(12, 4,  '0', 0, "", "R");
				}
				$xi = $xi + $standard_xoffset;
			}
			
			$xi = $standard_xoffset;
			$y = $y+ $standard_y;
			foreach($years2 as $key=>$year)
			{
				$pdf->SetXY($x2+$xi,$y);
				if(array_key_exists($year, $cost_of_production_accessories_values_per_brand[$brand_id]))
				{
					$pdf->Cell(12, 4,  number_format($cost_of_production_accessories_values_per_brand[$brand_id][$year],2), 0, "", "R");
				}
				else
				{
					$pdf->Cell(12, 4,  '0' , 0, "", "R");
				}
				$xi = $xi + $standard_xoffset;
			}

			$xi = $standard_xoffset;
			$y = $y+ $standard_y;
			foreach($years2 as $key=>$year)
			{
				$pdf->SetXY($x2+$xi,$y);
				if(array_key_exists($year, $cost_of_production_service_values_per_brand[$brand_id]))
				{
					$pdf->Cell(12, 4,  number_format($cost_of_production_service_values_per_brand[$brand_id][$year],2), 0, "", "R");
				}
				else
				{
					$pdf->Cell(12, 4,  '0' , 0, "", "R");
				}
				$xi = $xi + $standard_xoffset;
			}
			$xi = $standard_xoffset;
			$y = $y+ $standard_y;
			foreach($years2 as $key=>$year)
			{
				$cost_of_products_sold = 0;
				if(array_key_exists($year, $cost_of_production_watches_values_per_brand[$brand_id]))
				{
					$cost_of_products_sold = $cost_of_products_sold + $cost_of_production_watches_values_per_brand[$brand_id][$year];
				}
				if(array_key_exists($year, $cost_of_production_jewellery_values_per_brand[$brand_id]))
				{
					$cost_of_products_sold = $cost_of_products_sold + $cost_of_production_jewellery_values_per_brand[$brand_id][$year];
				}
				if(array_key_exists($year, $cost_of_production_accessories_values_per_brand[$brand_id]))
				{
					$cost_of_products_sold = $cost_of_products_sold + $cost_of_production_accessories_values_per_brand[$brand_id][$year];
				}
				if(array_key_exists($year, $cost_of_production_service_values_per_brand[$brand_id]))
				{
					$cost_of_products_sold = $cost_of_products_sold + $cost_of_production_service_values_per_brand[$brand_id][$year];
				}

				
				$pdf->SetXY($x2+$xi,$y);
				$pdf->Cell(12, 4,  number_format($cost_of_products_sold,2), 0, "", "R");

				
				$xi = $xi + $standard_xoffset;
			}
			$xi = $standard_xoffset;
			$y = $y+ $standard_y;
			foreach($years2 as $key=>$year)
			{
				$pdf->SetXY($x2+$xi,$y);
				$pdf->Cell(12, 4,  number_format($total_gross_margin_values_per_brand[$brand_id][$year],2), 0, "", "R");
				$xi = $xi + $standard_xoffset;
			}

			$xi = $standard_xoffset;
			$y = $y+ 2*$standard_y;
			foreach($years2 as $key=>$year)
			{
				$pdf->SetXY($x2+$xi,$y);
				$pdf->Cell(12, 4,  number_format($wholesale_margin_of_watches_sold_per_brand[$brand_id][$year],2), 0, "", "R");
				$xi = $xi + $standard_xoffset;
			}
			
			$xi = $standard_xoffset;
			$y = $y+ $standard_y;
			foreach($years2 as $key=>$year)
			{
				$pdf->SetXY($x2+$xi,$y);
				$pdf->Cell(12, 4,  number_format($wholesale_margin_of_jewellery_sold_per_brand[$brand_id][$year],2), 0, "", "R");
				$xi = $xi + $standard_xoffset;
			}

			$xi = $standard_xoffset;
			$y = $y+ $standard_y;
			foreach($years2 as $key=>$year)
			{
				$pdf->SetXY($x2+$xi,$y);
				$pdf->Cell(12, 4,  number_format($wholesale_margin_of_accessories_sold_per_brand[$brand_id][$year],2), 0, "", "R");
				$xi = $xi + $standard_xoffset;
			}

			$xi = $standard_xoffset;
			$y = $y+ $standard_y;
			foreach($years2 as $key=>$year)
			{
				$pdf->SetXY($x2+$xi,$y);
				$pdf->Cell(12, 4,  number_format($wholesale_margin_of_services_sold_per_brand[$brand_id][$year],2), 0, "", "R");
				$xi = $xi + $standard_xoffset;
			}
		}

	}
}




$pdf->Output();

?>