<?php
/********************************************************************

    standardparameter.php

    Creation and mutation of parameter records records.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_full_access_to_cer");

/********************************************************************
    build form 
*********************************************************************/
$form = new Form("cer_standardparameters", "cer_standardparameters");

$form->add_label("cer_standardparameter_text", "Parameter");

$form->add_edit("cer_standardparameter_value", "Value*", NOTNULL, "", TYPE_DECIMAL, 12, 2);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();
$form->process();

$page = new Page("standardparameters");

$page->header();
$page->title("Edit Standard Parameter");
$form->render();
$page->footer();

?>
