<?php
/********************************************************************

    reject_submission.php

    Reject Submission: LN, AF, CER, INR-03

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2012-07-20
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2012-07-20
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";
require_once "../include/page_modal.php";

check_access("can_reject_submissions");

if(param("type") and param("pid"))
{
	$type = param("type");
	$pid = param("pid");
}
else
{
	exit;
}

if(param("cancel_action") == 1)
{
	if(array_key_exists("reject_action_" . user_id(), $_SESSION))
	{
		unset($_SESSION["reject_action_" . user_id()]);
	}
		
	?>
		<script language="javascript">
			window.parent.$.window.hideAll();
		</script>

	<?php
	exit;
}


/********************************************************************
    prepare all data needed
*********************************************************************/
$recipient_name = "";
$recipient_text = "";
$recipient_id = 0;
$recipient_email = "";

if($type == "ln")
{
	$page_title= "Reject Lease Negotiation Form";
	$basicdata = get_ln_basicdata(param("pid"));

	$submitted_by = get_user($basicdata["ln_basicdata_submitted_by"]);
	$resubmitted_by = get_user($basicdata["ln_basicdata_resubmitted_by"]);
	$rejected_by = get_user($basicdata["ln_basicdata_rejected_by"]);

	if($basicdata["ln_basicdata_resubmitted_by"])
	{
		$resubmitted_by = get_user($basicdata["ln_basicdata_resubmitted_by"]);
		$recipient_text = "LN resubmitted by";
		$recipient_name = $resubmitted_by["name"] . " " . $resubmitted_by["firstname"];
		$recipient_id = $resubmitted_by["id"]; 
		$recipient_email = $resubmitted_by["email"]; 
	}
	elseif($basicdata["ln_basicdata_submitted_by"])
	{
		$submitted_by = get_user($basicdata["ln_basicdata_submitted_by"]);
		$recipient_text = "LN submitted by";
		$recipient_name = " by " . $submitted_by["name"] . " " . $submitted_by["firstname"];
		$recipient_id = $submitted_by["id"]; 
		$recipient_email = $submitted_by["email"]; 
	}

	$mail_group = "Submisson of LN rejected";
	$subject = MAIL_SUBJECT_PREFIX . ": " . $mail_group ." - Project " . $project["order_number"] . ": " . $project["order_shop_address_company"];

	
}
elseif($type == "cer")
{
	$page_title= "Reject CER";

	$submitted_by = get_user($cer_basicdata["cer_basicdata_submitted_by"]);
	$resubmitted_by = get_user($cer_basicdata["cer_basicdata_resubmitted_by"]);
	$rejected_by = get_user($cer_basicdata["cer_basicdata_rejected_by"]);

	if($cer_basicdata["cer_basicdata_resubmitted_by"])
	{
		$resubmitted_by = get_user($cer_basicdata["cer_basicdata_resubmitted_by"]);
		$recipient_text = "CER resubmitted by";
		$recipient_name = $resubmitted_by["name"] . " " . $resubmitted_by["firstname"];
		$recipient_id = $resubmitted_by["id"]; 
		$recipient_email = $resubmitted_by["email"]; 
	}
	elseif($cer_basicdata["cer_basicdata_submitted_by"])
	{
		$submitted_by = get_user($cer_basicdata["cer_basicdata_submitted_by"]);
		$recipient_text = "CER submitted by";
		$recipient_name = " by " . $submitted_by["name"] . " " . $submitted_by["firstname"];
		$recipient_id = $submitted_by["id"]; 
		$recipient_email = $submitted_by["email"]; 
	}

	$mail_group = "Submission of CER rejected";
	$subject = MAIL_SUBJECT_PREFIX . ": " . $mail_group ." - Project " . $project["order_number"] . ": " . $project["order_shop_address_company"];
}
elseif($type == "af")
{
	$page_title= "Reject AF";
	$submitted_by = get_user($cer_basicdata["cer_basicdata_submitted_by"]);
	$resubmitted_by = get_user($cer_basicdata["cer_basicdata_resubmitted_by"]);
	$rejected_by = get_user($cer_basicdata["cer_basicdata_rejected_by"]);

	if($cer_basicdata["cer_basicdata_resubmitted_by"])
	{
		$resubmitted_by = get_user($cer_basicdata["cer_basicdata_resubmitted_by"]);
		$recipient_text = "AF resubmitted by";
		$recipient_name = $resubmitted_by["name"] . " " . $resubmitted_by["firstname"];
		$recipient_id = $resubmitted_by["id"]; 
		$recipient_email = $resubmitted_by["email"]; 
	}
	elseif($cer_basicdata["cer_basicdata_submitted_by"])
	{
		$submitted_by = get_user($cer_basicdata["cer_basicdata_submitted_by"]);
		$recipient_text = "AF submitted by";
		$recipient_name = " by " . $submitted_by["name"] . " " . $submitted_by["firstname"];
		$recipient_id = $submitted_by["id"]; 
		$recipient_email = $submitted_by["email"]; 
	}


	$mail_group = "Submission of AF rejected";
	$subject = MAIL_SUBJECT_PREFIX . ": " . $mail_group ." - Project " . $project["order_number"] . ": " . $project["order_shop_address_company"];
}
elseif($type == "inr03")
{
	$page_title= "Reject INR-03";
	$submitted_by = get_user($cer_basicdata["cer_basicdata_submitted_by"]);
	$resubmitted_by = get_user($cer_basicdata["cer_basicdata_resubmitted_by"]);
	$rejected_by = get_user($cer_basicdata["cer_basicdata_rejected_by"]);

	if($cer_basicdata["cer_basicdata_resubmitted_by"])
	{
		$resubmitted_by = get_user($cer_basicdata["cer_basicdata_resubmitted_by"]);
		$recipient_text = "INR-03 resubmitted by";
		$recipient_name = $resubmitted_by["name"] . " " . $resubmitted_by["firstname"];
		$recipient_id = $resubmitted_by["id"]; 
		$recipient_email = $resubmitted_by["email"]; 
	}
	elseif($cer_basicdata["cer_basicdata_submitted_by"])
	{
		$submitted_by = get_user($cer_basicdata["cer_basicdata_submitted_by"]);
		$recipient_text = "INR-03 submitted by";
		$recipient_name = " by " . $submitted_by["name"] . " " . $submitted_by["firstname"];
		$recipient_id = $submitted_by["id"]; 
		$recipient_email = $submitted_by["email"]; 
	}


	$mail_group = "Submission of INR-03 rejected";
	$subject = MAIL_SUBJECT_PREFIX . ": " . $mail_group ." - Project " . $project["order_number"] . ": " . $project["order_shop_address_company"];
}
elseif($type == "in03")
{
	
	$page_title= "Reject Request for Additional Funding";


	$sql = "select * from cer_refundings where cer_refunding_project = " . param("pid");
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$id = $row["cer_refunding_id"];

		$refunding_status = $row["cer_refunding_status"];
		$refunding_justification = $row["cer_refunding_justification"];
		$refunding_basicdata = $row["cer_refunding_effects"];

		$refunding_submission = to_system_date($row["cer_refunding_submissiondate"]);
		$refunding_submission_by = $row["cer_refunding_sumbitted_by"];
		$refunding_resubmission = to_system_date($row["cer_refunding_resumissiondate"]);
		$refunding_resubmission_by = $row["cer_refunding_resubmitted_by"];

		$refunding_rejected = to_system_date($row["cer_refunding_rejected"]);
		$refunding_rejected_by = $row["cer_refunding_rejected_by"];

		$attachment1 = $row["cer_refunding_attachment1"];
		$attachment2 = $row["cer_refunding_attachment2"];
		$attachment3 = $row["cer_refunding_attachment3"];
		$attachment4 = $row["cer_refunding_attachment4"];

		$refunding_locked_status = $row["cer_refunding_locked"];

		
	}

	if($refunding_resubmission_by)
	{
		$resubmitted_by = get_user($refunding_submission_by);
		$recipient_text = "Request resubmitted by";
		$recipient_name = $resubmitted_by["name"] . " " . $resubmitted_by["firstname"];
		$recipient_id = $resubmitted_by["id"]; 
		$recipient_email = $resubmitted_by["email"]; 
	}
	elseif($refunding_submission_by)
	{
		$submitted_by = get_user($refunding_submission_by);
		$recipient_text = "Request submitted by";
		$recipient_name = " by " . $submitted_by["name"] . " " . $submitted_by["firstname"];
		$recipient_id = $submitted_by["id"]; 
		$recipient_email = $submitted_by["email"]; 
	}

	$mail_group = "Submisison of IN-03 rejected";
	$subject = MAIL_SUBJECT_PREFIX . ": " . $mail_group ." - Project " . $project["order_number"] . ": " . $project["order_shop_address_company"];

}
else
{
	exit;
}



$project = get_project(param("pid"));
$client_address = get_address($project["order_client_address"]);


$client = $client_address["company"] . ", " .
		  $client_address["zip"] . " " .
          $client_address["place"] . ", " .
          $client_address["country_name"];

$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_address"] . ", " .
		$project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];





//get recipients
$cc_reciepients = array();
$cc_reciepients_ids = array();
$sql = "select user_id, concat(user_name , ' ', user_firstname) as user_name, " . 
	   "user_email, address_company, role_name " .
	   "from users " .
	   "left join user_roles on user_role_user = user_id " .
	   "left join addresses on address_id = user_address " . 
	   "left join roles on role_id = user_role_role " . 
	   "where user_active = 1 and user_role_role = 15 and address_id = " . $client_address["id"];

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	if(!in_array($row["user_id"], $cc_reciepients_ids))
	{
		$user = array();
		$user["name"] = $row["user_name"] . ' (' . $row["address_company"] . ')';
		$user["email"] = strtolower($row["user_email"]);
		$user["roles"] = "";
		$user["roles"] = $row["role_name"];
		
		$cc_reciepients[] = $user;
		$cc_reciepients_ids[] = $row["user_id"];
	}
}

//finance controllers
$sql = "select user_id, concat(user_name , ' ', user_firstname) as user_name, " . 
	   "user_email, address_company, role_name " .
	   "from users " .
	   "left join user_roles on user_role_user = user_id " .
	   "left join addresses on address_id = user_address " . 
	   "left join roles on role_id = user_role_role " . 
	   "where user_active = 1 and user_role_role = 22 ";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	if(!in_array($row["user_id"], $cc_reciepients_ids))
	{
		$user = array();
		$user["name"] = $row["user_name"] . ' (' . $row["address_company"] . ')';
		$user["email"] = strtolower($row["user_email"]);
		$user["roles"] = "";
		$user["roles"] = $row["role_name"];
		
		$cc_reciepients[] = $user;
		$cc_reciepients_ids[] = $row["user_id"];
	}
}

//project manager
$sql = "select user_id, concat(user_name , ' ', user_firstname) as user_name, " . 
	   "user_email, address_company, role_name " .
	   "from projects " .
	   "left join users on user_id = project_retail_coordinator " . 
	   "left join user_roles on user_role_user = user_id " .
	   "left join addresses on address_id = user_address " . 
	   "left join roles on role_id = user_role_role " . 
	   "where project_id = " . param("pid");

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	if(!in_array($row["user_id"], $cc_reciepients_ids))
	{
		$user = array();
		$user["name"] = $row["user_name"] . ' (' . $row["address_company"] . ')';
		$user["email"] = strtolower($row["user_email"]);
		$user["roles"] = "";
		$user["roles"] = $row["role_name"];
		
		$cc_reciepients[] = $user;
		$cc_reciepients_ids[] = $row["user_id"];
	}
}

//get submission notificants
$sql = 'select * from projecttype_newproject_notifications ' . 
	   'where projecttype_newproject_notification_on_lnresubmission = 1 ' . 
	   ' and projecttype_newproject_notification_country = ' . $project["order_shop_address_country"] . 
	   ' and projecttype_newproject_notification_postype = ' . $project['project_postype'];


$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	if($row["projecttype_newproject_notification_email"])
	{
		
		$sql = 'select * from users ' . 
			   'where user_email = ' . dbquote($row["projecttype_newproject_notification_email"]);

		$res_u = mysql_query($sql) or dberror($sql);
		if ($row_u = mysql_fetch_assoc($res_u))
		{
			if(!in_array($row_u["user_id"], $cc_reciepients_ids))
			{
				$cc_reciepients_ids[] = $row_u["user_id"];
				$cc_reciepients[] = array("name" => $row_u["user_name"] . " " . $row_u["user_firstname"], "email" => strtolower($row_u["user_email"]), "roles" => "Notification for Submissions");
			}
		}
		
		
	}

	if($row["projecttype_newproject_notification_emailcc1"])
	{
		
		$sql = 'select * from users ' . 
			   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc1"]);

		$res_u = mysql_query($sql) or dberror($sql);
		if ($row_u = mysql_fetch_assoc($res_u))
		{
			if(!in_array($row_u["user_id"], $cc_reciepients_ids))
			{
				$cc_reciepients_ids[] = $row_u["user_id"];
				$cc_reciepients[] = array("name" => $row_u["user_name"] . " " . $row_u["user_firstname"], "email" => strtolower($row_u["user_email"]), "roles" => "Notification for Submissions");
			}
		}
		
	}
	if($row["projecttype_newproject_notification_emailcc2"])
	{
		
		$sql = 'select * from users ' . 
			   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc2"]);

		$res_u = mysql_query($sql) or dberror($sql);
		if ($row_u = mysql_fetch_assoc($res_u))
		{
			if(!in_array($row_u["user_id"], $cc_reciepients_ids))
			{
				$cc_reciepients_ids[] = $row_u["user_id"];
				$cc_reciepients[] = array("name" => $row_u["user_name"] . " " . $row_u["user_firstname"], "email" => strtolower($row_u["user_email"]), "roles" => "Notification for Submissions");
			}
		}
		
		
	}
	if($row["projecttype_newproject_notification_emailcc3"])
	{
		
		$sql = 'select * from users ' . 
			   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc3"]);

		$res_u = mysql_query($sql) or dberror($sql);
		if ($row_u = mysql_fetch_assoc($res_u))
		{
			if(!in_array($row_u["user_id"], $cc_reciepients_ids))
			{
				$cc_reciepients_ids[] = $row_u["user_id"];
				$cc_reciepients[] = array("name" => $row_u["user_name"] . " " . $row_u["user_firstname"], "email" => strtolower($row_u["user_email"]), "roles" => "Notification for Submissions");
			}
		}
		
		
	}
	if($row["projecttype_newproject_notification_emailcc4"])
	{
		
		$sql = 'select * from users ' . 
			   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc4"]);

		$res_u = mysql_query($sql) or dberror($sql);
		if ($row_u = mysql_fetch_assoc($res_u))
		{
			if(!in_array($row_u["user_id"], $cc_reciepients_ids))
			{
				$cc_reciepients_ids[] = $row_u["user_id"];
				$cc_reciepients[] = array("name" => $row_u["user_name"] . " " . $row_u["user_firstname"], "email" => strtolower($row_u["user_email"]), "roles" => "Notification for Submissions");
			}
		}
		
	
	}
	if($row["projecttype_newproject_notification_emailcc5"])
	{
		
		$sql = 'select * from users ' . 
			   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc5"]);

		$res_u = mysql_query($sql) or dberror($sql);
		if ($row_u = mysql_fetch_assoc($res_u))
		{
			if(!in_array($row_u["user_id"], $cc_reciepients_ids))
			{
				$cc_reciepients_ids[] = $row_u["user_id"];
				$cc_reciepients[] = array("name" => $row_u["user_name"] . " " . $row_u["user_firstname"], "email" => strtolower($row_u["user_email"]), "roles" => "Notification for Submissions");
			}
		}
		
	
	}
	if($row["projecttype_newproject_notification_emailcc6"])
	{
		
		$sql = 'select * from users ' . 
			   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc6"]);

		$res_u = mysql_query($sql) or dberror($sql);
		if ($row_u = mysql_fetch_assoc($res_u))
		{
			if(!in_array($row_u["user_id"], $cc_reciepients_ids))
			{
				$cc_reciepients_ids[] = $row_u["user_id"];
				$cc_reciepients[] = array("name" => $row_u["user_name"] . " " . $row_u["user_firstname"], "email" => strtolower($row_u["user_email"]), "roles" => "Notification for Submissions");
			}
		}
		
	
	}
	if($row["projecttype_newproject_notification_emailcc7"])
	{
		
		$sql = 'select * from users ' . 
			   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc7"]);

		$res_u = mysql_query($sql) or dberror($sql);
		if ($row_u = mysql_fetch_assoc($res_u))
		{
			if(!in_array($row_u["user_id"], $cc_reciepients_ids))
			{
				$cc_reciepients_ids[] = $row_u["user_id"];
				$cc_reciepients[] = array("name" => $row_u["user_name"] . " " . $row_u["user_firstname"], "email" => strtolower($row_u["user_email"]), "roles" => "Notification for Submissions");
			}
		}
		
	}
}


$sender = get_user(user_id());
$cc_reciepients[] = array("name" => $sender["name"] . " " . $sender["firstname"], "email" => strtolower($sender["email"]), "roles" => "Copy to me");
$cc_reciepients_ids[] = user_id();



/********************************************************************
    save data
*********************************************************************/
$saved = 0;
$error = "";
if(param("save_form"))
{
	if(!$_POST["window_text"])
	{
		$error = "Please enter a mail text!";
	}
	elseif(count($_FILES) > 0 and $_FILES["cer_mail_attachment1_path"]["name"] != '' and !$_POST["cer_mail_attachment1_title"])
	{
		$error = "Please indicate a title for your attachment!";

	}
	elseif($_POST["type"] == "ln")
	{
		$fields = array();
		$fields[] = "ln_basicdata_rejected = " . dbquote(date("Y-m-d"));
		$fields[] = "ln_basicdata_rejected_by = " . dbquote(user_id());

		$sql = "update ln_basicdata set " . join(", ", $fields) . " where ln_basicdata_project = " . param("pid");
		mysql_query($sql) or dberror($sql);

		$saved = 1;
	}
	elseif($_POST["type"] == "af" or $_POST["type"] == "cer" or $_POST["type"] == "inr03")
	{
		$fields = array();
		$fields[] = "cer_basicdata_rejected = " . dbquote(date("Y-m-d"));
		$fields[] = "cer_basicdata_rejected_by = " . dbquote(user_id());

		$sql = "update cer_basicdata set " . join(", ", $fields) . " where cer_basicdata_version = 0 and cer_basicdata_project = " . param("pid");
		mysql_query($sql) or dberror($sql);

		$saved = 1;
	}
	elseif($_POST["type"] == "in03")
	{
		$fields = array();
		$fields[] = "cer_refunding_rejected = " . dbquote(date("Y-m-d"));
		$fields[] = "cer_refunding_rejected_by = " . dbquote(user_id());

		$sql = "update cer_refundings set " . join(", ", $fields) . " where cer_refunding_project = " . param("pid");
		mysql_query($sql) or dberror($sql);

		$saved = 1;
	}


	if($saved == 1)
	{
		//send mail
		$text = str_replace("\r\n", "\n", trim($_POST["window_text"]));

		if(count($_FILES) > 0 and $_FILES["cer_mail_attachment1_path"]["name"] != '' )
		{
			$file_path = '/files/cer/' . $project["project_number"];
			$path = combine_paths($_SERVER["DOCUMENT_ROOT"], $file_path);
			create_directory($path);
			$name = make_valid_filename($_FILES["cer_mail_attachment1_path"]["name"]);
			
			$name = make_unique_filename($name, $_SERVER["DOCUMENT_ROOT"]);
			$cer_file_path = '/files/cer/' . $project["project_number"] . '/' . $name;
			
			copy($_FILES["cer_mail_attachment1_path"]["tmp_name"], combine_paths($_SERVER["DOCUMENT_ROOT"], $cer_file_path));

		 }


		$mail = new PHPMailer();
		$mail->Subject = $subject;
		$mail->SetFrom($sender["email"], $sender["firstname"] . " " . $sender["name"]);
		$mail->AddReplyTo($sender["email"], $sender["firstname"] . " " . $sender["name"]);

		if($_POST["type"] == "ln")
		{
			$bodytext0 = str_replace("\r\n", "\n", $text) . "\n\n";
			$link ="ln_general.php?pid=" . param("pid");
			$bodytext = $bodytext0 . "Click below to have direct access to the project's LN:\n";
			$bodytext = $bodytext .  APPLICATION_URL ."/cer/" . $link . "\n\n";
		}
		elseif($_POST["type"] == "af" or $_POST["type"] == "cer" or $_POST["type"] == "inr03")
		{
			$bodytext0 = str_replace("\r\n", "\n", $text) . "\n\n";
			$link ="cer_project.php?pid=" . param("pid");
			$bodytext = $bodytext0 . "Click below to have direct access to the project's " . strtoupper($_POST["type"]) . ":\n";
			$bodytext = $bodytext .  APPLICATION_URL ."/cer/" . $link . "\n\n";
		}
		elseif($_POST["type"] == "in03")
		{
			$bodytext0 = str_replace("\r\n", "\n", $text) . "\n\n";
			$link ="cer_additional_funding.php?pid=" . param("pid");
			$bodytext = $bodytext0 . "Click below to have direct access to the project's " . strtoupper($_POST["type"]) . ":\n";
			$bodytext = $bodytext .  APPLICATION_URL ."/cer/" . $link . "\n\n";
		}

		$mail->Body = $bodytext;

		

		$rcpts = "";
		$rcptscc = "";
		$selected_reciepients = array();
		$mail->AddAddress($recipient_email, $recipient_name);
		$rcpts .= $recipient_email . "\n";


		$selected_reciepients[] = $recipient_email;

		foreach($cc_reciepients as $key=>$value)
		{
			if($_POST["R_" . $cc_reciepients_ids[$key]] == 1)
			{
				$mail->AddCC($value["email"], $value["name"]);
				$rcptscc .= $value["email"] . "\n";

				$selected_reciepients[] = $value["email"];

			}
		}

		if($rcptscc)
		{
			$rcpts .= "and CC-Mail to:" . "\n" . $rcptscc;
		}

		
		//get all ccmails
		if(array_key_exists('ccmails', $_POST))
		{
			
			$selected_ccreciepients = array();
			
			$ccmails =  explode(' ', preg_replace("'\r?\n'"," ",$_POST['ccmails'])); 
			foreach($ccmails as $ccmail) {
				if(is_email_address($ccmail)) {
					
					$selected_ccreciepients[strtolower($ccmail)] = strtolower($ccmail);
				}
			
			}

			foreach($selected_ccreciepients as $key=>$value)
			{
				if($value and !in_array($value, $selected_reciepients)) {
					$result = $mail->AddCC($value);
					$rcpts .= $value . "\n";
				}
			}
		}

		if(count($_FILES) > 0 and $_FILES["cer_mail_attachment1_path"]["name"] != '' )
		{
			
			$filepath = $_SERVER["DOCUMENT_ROOT"] . $cer_file_path;
			$mail->AddAttachment($filepath);
		}

		
		$mail->Send();

		
		//update mail history
		$fields = array();
		$values = array();

		$fields[] = "cer_mail_project";
		$values[] = param("pid");

		$fields[] = "cer_mail_group";
		$values[] = dbquote($mail_group);

		$fields[] = "cer_mail_text";
		$values[] = dbquote($text);
		

		$fields[] = "cer_mail_sender";
		$values[] = dbquote($sender["firstname"] . " " . $sender["name"]);

		$fields[] = "cer_mail_sender_email";
		$values[] = dbquote($sender["email"]);

		$fields[] = "cer_mail_reciepient";
		$values[] = dbquote($rcpts);


		if(count($_FILES) > 0)
		{
			$fields[] = "cer_mail_attachment1_title";
			$values[] = dbquote($_POST["cer_mail_attachment1_title"]);

			$fields[] = "cer_mail_attachment1_path";
			$values[] = dbquote($cer_file_path);
		}

		$fields[] = "date_created";
		$values[] = "now()";

		$fields[] = "date_modified";
		$values[] = "now()";

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$fields[] = "user_modified";
		$values[] = dbquote(user_login());

		$sql = "insert into cer_mails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
		
		mysql_query($sql) or dberror($sql);


		if(array_key_exists("reject_action_" . user_id(), $_SESSION))
		{
			unset($_SESSION["reject_action_" . user_id()]);
		}
		
		?>
			<script language="javascript">
				window.parent.$.window.hideAll();
				window.parent.location.reload();
			</script>

		<?php
	}
}


//get session data for overlay window
$message = "";
if(array_key_exists("reject_action_" . user_id(), $_SESSION))
{
	
	$data = $_SESSION["reject_action_" . user_id()];
	
	if(count($data["post"]) > 0)
	{
		$message = $data["post"]["window_text"];
	}
}


/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_mails", "cer_mails");

$form->add_label("shop_address", "POS Location", RENDER_HTML, $shop);
$form->add_label("client_address", "Client", RENDER_HTML, $client);
$form->add_hidden("pid", param("pid"));
$form->add_hidden("type", param("type"));
$form->add_hidden("recipient_id", $recipient_id);

$form->add_hidden("save_form", "1");
$form->add_hidden("cancel_action", "0");

$form->add_section("Mail Message");
$form->add_multiline("window_text", "Message*", 16, 0, $message, 0, "", 70);


$form->add_section("Attachments");
$form->add_edit("cer_mail_attachment1_title", "File Title");
$form->add_upload("cer_mail_attachment1_path", "Attachment", "/files/cer/". $project["order_number"]);


$form->add_section("Recipient*");
$form->add_checkbox("recipient", $recipient_name, 1, DISABLED, $recipient_text);

$form->add_section("CC Recipients*");

foreach($cc_reciepients as $key=>$user)
{
	$form->add_checkbox("R_" . $cc_reciepients_ids[$key], $user["name"], "", "", $user["roles"]);
}

$form->add_section("CC Recipients");
$form->add_modal_selector("ccmails", "Selected Recipients", 8);


//$form->add_input_submit("send", "Send Mail", 0);
$link =  "javascript:document.forms['main'].submit();";
$form->add_button("send", "Send Mail", $link);

$link =  "javascript:$('#cancel_action').val('1');document.forms['main'].submit();";
$form->add_button("cancel", "Cancel", $link);

if($error)
{
	$form->error($error);
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Page_Modal("cer_projects");



$page->header();
$page->title($page_title);
$form->render();


?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#ccmails_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/shared/select_mail_recipients.php'
    });
    return false;
  });
});
</script>

<?php

$page->footer();

?>