<?php
/********************************************************************

    system.php

    Main entry page for POS System-Data login.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-03-23
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-23
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_full_access_to_cer");


$page = new Page("system");

$page->header();
echo "<p>Welcome to the management section of " . APPLICATION_NAME . " CER.</p>";


$page->footer();


?>