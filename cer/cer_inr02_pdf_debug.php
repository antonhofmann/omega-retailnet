<?php
/********************************************************************

    cer_inr02_pdf_debug.php

    Print Debuggung Information PDF for CER Business Plan (Form IN-R02)

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();

$SUPPRESS_HEADERS = true;
define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 


require "../include/frame.php";

$cer_version = 0;
$ln_version = 0;
if(param("cerversion"))
{
	$cer_version = param("cerversion");
}
if(param("lnversion"))
{
	$ln_version = param("lnversion");
}


require "include/get_functions.php";
require "include/get_project.php";
require "../shared/func_posindex.php";
require "include/financial_functions.php";

check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');





// get all data needed from project
$sql = "select * " . 
       "from projects " . 
	   "left join orders on order_id = project_order " .
	   "left join countries on country_id = order_shop_address_country " .
	   "left join product_lines on product_line_id = project_product_line " .
	   "left join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join projectkinds on projectkind_id = project_projectkind " .
       "left join addresses on address_id = order_client_address " .
       "left join users on user_id = project_retail_coordinator " . 
	   "where project_id = " . param("pid");


$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	if($row["address_legal_entity_name"])
	{
		$legal_entity_name = $row["address_legal_entity_name"];
	}
	else
	{
		$legal_entity_name = $row["address_company"];
	}
	$address_country = $row["address_country"];
	
	$project_name = $row["order_shop_address_company"]. ", " . $row["order_shop_address_place"] . ", " . $row["country_name"];

	$pos_country = $row["country_name"];
	$pos_address = $row["order_shop_address_address"];
	$pos_city = $row["order_shop_address_place"];

	$project_manager = $row["user_name"] . " " . $row["user_firstname"];
	
	$project_kind = $row["projectkind_name"];

	if($row["address_legal_entity_name"])
	{
		$legal_entity = $row["address_legal_entity_name"];
	}
	else
	{
		$legal_entity = $row["address_company"];
	}

	$date_request = to_system_date($row["order_date"]);

	$order_number = $row["project_order"];

	$sales_surface = round($row["project_cost_sqms"], 0);

	$planned_opening_date = to_system_date($row["project_real_opening_date"]);
	

}

$client_currency = get_cer_currency(param("pid"), $cer_version);
$currency_symbol = $client_currency["symbol"];


$sql_s = "select cer_revenue_sales_reduction_watches, " .
		 "cer_revenue_sales_reduction_bijoux, cer_revenue_sales_reduction_accessories, " .
		 "cer_revenue_sales_reduction_cservice, cer_revenue_reduction_credit_cards, cer_revenue_wholesale_margin_watches,  " .
		 "cer_revenue_wholesale_margin_bijoux, cer_revenue_wholesale_margin_accessories, " . 
		 " cer_revenue_wholesale_margin_services " . 
		 "from cer_revenues " . 
		 "where cer_revenue_cer_version = " .  dbquote($cer_version) .  
		 " and cer_revenue_project = " . dbquote(param("pid"));

$res_s = mysql_query($sql_s) or dberror($sql_s);
if($row_s = mysql_fetch_assoc($res_s))
{
	$sales_reduction_watches = $row_s["cer_revenue_sales_reduction_watches"];
	$sales_reduction_bijoux = $row_s["cer_revenue_sales_reduction_bijoux"];
	$sales_reduction_accessories = $row_s["cer_revenue_sales_reduction_accessories"];
	$sales_reduction_services = $row_s["cer_revenue_sales_reduction_cservice"];
	//$sales_reduction_creditcard = $row_s["cer_revenue_reduction_credit_cards"];
	
	$wholesale_margin_watches = $row_s["cer_revenue_wholesale_margin_watches"];
	$wholesale_margin_bijoux = $row_s["cer_revenue_wholesale_margin_bijoux"];
	$wholesale_margin_accessories = $row_s["cer_revenue_wholesale_margin_accessories"];
	$wholesale_margin_services = $row_s["cer_revenue_wholesale_margin_services"];
}


include("include/in_financial_data.php");	



//set pdf parameters

$margin_top = 8;
$margin_left = 12;
$y = $margin_top;
$standard_y = 3.5;

// Create and setup PDF document

$pdf = new TCPDF("L", "mm", "A3", true, 'UTF-8', false);
$pdf->SetMargins(12, 8);
$pdf->setPrintHeader(false);


$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');

$pdf->Open();
$pdf->SetTitle("Retail Busines Plan Overview Form IN-R02");
$pdf->SetAuthor(BRAND . " Retailnet");
$pdf->SetDisplayMode(110);
$pdf->AddPage();
$pdf->SetAutoPageBreak(false, 0);

//page data

// draw page box
$pdf->SetXY($margin_left-2,$margin_top);
$pdf->Cell(406, 270, "", 1);
// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "B", 11);
$pdf->Cell(255, 6, "Debug Output for the Retail Business Plan Overview for Project " . $project["project_number"] . ": " . $project_name . ", Page 1", 0, "", "L");


	//investment
	
	$x1 = $margin_left;
	$x2 = $margin_left+32;
	$x3 = $margin_left+52;
	

	$y = $y + 6;
	$y2 = $y + $standard_y;   // for column2
	$y3 = $y + $standard_y;   // for column3
	$y4 = $y + $standard_y;   // for column4
	$y5 = $y + $standard_y;   // for column5
	$y6 = $y + $standard_y;   // for column6

	foreach($fixed_assets2 as $key=>$itype)
	{
		
		
		if(array_key_exists($itype, $amounts))
		{
			if(!$amounts[$itype]){$amounts[$itype] = "-";}
			if(!$depryears[$itype]){$depryears[$itype] = "-";}

			$y = $y + $standard_y;
			$pdf->SetXY($x1,$y);
			$pdf->SetFont("arialn", "", 8);
			$pdf->Cell(63, 4, substr($investment_names[$itype], 0, 25), 0, "", "L");

			$pdf->SetXY($x2,$y);
			$pdf->SetFont("arialn", "", 8);
			
			if($amounts[$itype] != 0)
			{
				if($itype == 11) {
					$amounts[$itype] = $amounts[$itype] - $merchandising_total -$transportation_total;
				}
				$pdf->Cell(20, 4, number_format($amounts[$itype],2), 0, "", "R");
				$pdf->Cell(5, 4, $depryears[$itype], 0, "", "R");
			}
			else
			{
				$pdf->Cell(20, 4, "-", 0, "", "R");
			}
		}
		else
		{
			$y = $y + $standard_y;
		}
	}


	$y = $y + $standard_y;
	$pdf->SetXY($x1,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(20, 4, "Total Fixed Assets", 0, "", "L");

	$pdf->SetXY($x2,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(20, 4, number_format($investment_total,0), 0, "", "R");

	
	$y = $y + $standard_y;
	$pdf->SetXY($x1,$y);
	$pdf->SetFont("arialn", "", 8);

	$pdf->Cell(20, 4, $intagible_name, 0, "", "L");

	$pdf->SetXY($x2,$y);
	$pdf->SetFont("arialn", "", 8);
	
	if($intagible_amount != 0)
	{
		$pdf->Cell(20, 4, number_format($intagible_amount,0), 0, "", "R");
		if(array_key_exists(15, $depryears))
		{
			$pdf->Cell(5, 4, $depryears[15], 0, "", "R");
		}
		else
		{
			$pdf->Cell(5, 4, "0", 0, "", "R");
		}
	}
	else
	{
		$pdf->Cell(20, 4, "-", 0, "", "R");
	}

	
	$y = $y + $standard_y;
	$pdf->SetXY($x1,$y);
	$pdf->SetFont("arialn", "", 8);
	
	if(array_key_exists(9, $investment_names))
	{
		$pdf->Cell(20, 3.5, $investment_names[9], 0, "", "L");
	}
	else
	{
		$pdf->Cell(20, 4, "", 0, "", "L");
	}

	$pdf->SetXY($x2,$y);
	$pdf->SetFont("arialn", "", 8);
	
	if(array_key_exists(9, $amounts))
	{
		if($amounts[9] > 0)
		{
			$pdf->Cell(20, 4, number_format($amounts[9],2), 0, "", "R");
			$pdf->Cell(5, 4, $depryears[9], 0, "", "R");
		}
		else
		{
			$pdf->Cell(20, 4, "-", 0, "", "R");
		}
	}
	else
	{
		$pdf->Cell(20, 4, "", 0, "", "R");
	}


	
	$y = $y + $standard_y;
	$pdf->SetXY($x1,$y);
	$pdf->SetFont("arialn", "", 8);
	
	if(array_key_exists(13, $investment_names))
	{
		$pdf->Cell(20, 4, $investment_names[13], 0, "", "L");
	}
	else
	{
		$pdf->Cell(20, 4, "", 0, "", "L");
	}

	
	$pdf->SetXY($x2,$y);
	$pdf->SetFont("arialn", "", 8);
	
	
	if(array_key_exists(13, $amounts))
	{
		if($amounts[13] > 0)
		{
			$pdf->Cell(20, 4, number_format($amounts[13],2), 0, "", "R");
			$pdf->Cell(5, 4, $depryears[13], 0, "", "R");
		}
		else
		{
			$pdf->Cell(20, 4, "-", 0, "", "R");
		}
	}
	else
	{
		$pdf->Cell(20, 4, "", 0, "", "R");
	}

	$y = $y+ $standard_y;
	$pdf->SetXY($margin_left,$y);
	
	$pdf->SetXY($margin_left,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(50, 4, "Total Project costs", 0, "", "L");

	$pdf->SetXY($x2,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(20, 4, number_format($cer_totals,2), 0, "", "R");



	$y = $y+ $standard_y;
	$pdf->SetXY($margin_left,$y);
	
	$pdf->SetXY($margin_left,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 4, "Later Renovation", 0, "", "L");

	$pdf->SetXY($x2,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 4, number_format($future_investment,2), 0, "", "R");


	//parameter
	$x1c2 = 73;
	$x2c2 = 98;
	
	
	$pdf->SetXY($x1c2,$y2);
	$pdf->SetFont("arialn", "", 8);
	
	$pdf->Cell(50, 4, "Sales Reduction Watches", 0, "", "L");

	$pdf->SetXY($x2c2,$y2);
	$pdf->Cell(20, 4, number_format($average_sales_reduction_watches,2) . "%", 0, "", "R");

	$y2 = $y2+ $standard_y;

	$pdf->SetXY($x1c2,$y2);
	$pdf->Cell(50, 4, "Sales Reduction Jewellery", 0, "", "L");

	$pdf->SetXY($x2c2,$y2);
	$pdf->Cell(20, 4, number_format($average_sales_reduction_bijoux,2) . "%", 0, "", "R");

	$y2 = $y2+ $standard_y;

	$pdf->SetXY($x1c2,$y2);
	$pdf->Cell(50, 4, "Sales Reduction Accessories", 0, "", "L");

	$pdf->SetXY($x2c2,$y2);
	$pdf->Cell(20, 4, number_format($average_sales_reduction_accessories,2) . "%", 0, "", "R");

	$y2 = $y2+ $standard_y;
	$pdf->SetXY($x1c2,$y2);
	$pdf->Cell(50, 4, "Sales Reduction Services", 0, "", "L");
	$pdf->SetXY($x2c2,$y2);
	$pdf->Cell(20, 4, number_format($average_sales_reduction_services,2) . "%", 0, "", "R");

	/*
	$y2 = $y2+ $standard_y;
	$pdf->SetXY($x1c2,$y2);
	$pdf->Cell(50, 4, "Sales Reduction Credit Cards", 0, "", "L");
	$pdf->SetXY($x2c2,$y2);
	$pdf->Cell(20, 4, number_format($average_sales_reduction_creditcard,2) . "%", 0, "", "R");
	*/

	$y2 = $y2+ 2*$standard_y;
	$pdf->SetXY($x1c2,$y2);
	$pdf->Cell(50, 4, "Fixed Growth Salaries", 0, "", "L");
	$pdf->SetXY($x2c2,$y2);
	$pdf->Cell(20, 4, number_format($cer_basicdata["cer_basicdata_salary_growth"],2) . "%", 0, "", "R");


	$y2 = $y2+ $standard_y;
	$pdf->SetXY($x1c2,$y2);
	$pdf->Cell(50, 4, "Global Commission", 0, "", "L");
	$pdf->SetXY($x2c2,$y2);
	$pdf->Cell(20, 4, number_format($cer_basicdata["cer_basicdata_commission"],2) . "%", 0, "", "R");

	$y2 = $y2+ $standard_y;
	$pdf->SetXY($x1c2,$y2);
	$pdf->Cell(50, 4, "Social Charges on Commission", 0, "", "L");
	$pdf->SetXY($x2c2,$y2);
	$pdf->Cell(20, 4, number_format($cer_basicdata["cer_basicdata_commission_tax_percent"],2) . "%", 0, "", "R");


	//parameter
	$x1c2 = 130;
	$x2c2 = 160;
	
	$y2 = $y4;
	$pdf->SetXY($x1c2,$y2);
	$pdf->SetFont("arialn", "", 8);
	
	$pdf->SetXY($x1c2,$y2);
	$pdf->Cell(50, 4, "Wholsale Margin Watches", 0, "", "L");

	$pdf->SetXY($x2c2,$y2);
	$pdf->Cell(20, 4, number_format($average_wholesale_margin_watches,2) . "%", 0, "", "R");

	$y2 = $y2+ $standard_y;

	$pdf->SetXY($x1c2,$y2);
	$pdf->Cell(50, 4, "Wholsale Margin Bijoux", 0, "", "L");

	$pdf->SetXY($x2c2,$y2);
	$pdf->Cell(20, 4, number_format($average_wholesale_margin_bijoux,2) . "%", 0, "", "R");


	$y2 = $y2+ $standard_y;

	$pdf->SetXY($x1c2,$y2);
	$pdf->Cell(50, 4, "Wholsale Margin Accessories", 0, "", "L");

	$pdf->SetXY($x2c2,$y2);
	$pdf->Cell(20, 4, number_format($average_wholesale_margin_accessories,2) . "%", 0, "", "R");


	$y2 = $y2+ $standard_y;

	$pdf->SetXY($x1c2,$y2);
	$pdf->Cell(50, 4, "Wholsale Margin Services", 0, "", "L");

	$pdf->SetXY($x2c2,$y2);
	$pdf->Cell(20, 4, number_format($average_wholesale_margin_services,2) . "%", 0, "", "R");


	
	//parameters
	$x1c2 = 192;
	$x2c2 = 223;
	
	$y2 = $y4;
	$pdf->SetXY($x1c2,$y2);
	$pdf->SetFont("arialn", "", 8);
	
	
	$pdf->Cell(50, 4, "Cost of Watches", 0, "", "L");

	$pdf->SetXY($x2c2,$y2);
	$pdf->Cell(20, 4, number_format($average_cost_watches,2) . "%", 0, "", "R");

	$y2 = $y2+ $standard_y;

	$pdf->SetXY($x1c2,$y2);
	$pdf->Cell(50, 4, "Cost of Jewellery", 0, "", "L");

	$pdf->SetXY($x2c2,$y2);
	$pdf->Cell(20, 4, number_format($average_cost_bijoux,2) . "%", 0, "", "R");

	$y2 = $y2+ $standard_y;
	$pdf->SetXY($x1c2,$y2);
	$pdf->Cell(50, 4, "Cost of Accessories", 0, "", "L");
	$pdf->SetXY($x2c2,$y2);
	$pdf->Cell(20, 4, number_format($average_cost_accessories,2) . "%", 0, "", "R");

	$y2 = $y2+ $standard_y;
	$pdf->SetXY($x1c2,$y2);
	$pdf->Cell(50, 4, "Cost of Services", 0, "", "L");
	$pdf->SetXY($x2c2,$y2);
	$pdf->Cell(20, 4, number_format($average_cost_services,2) . "%", 0, "", "R");

	$y2 = $y2+ 2*$standard_y;

	
	$pdf->SetXY($x1c2,$y2);
	$pdf->Cell(50, 4, "Discount Rate", 0, "", "L");

	$pdf->SetXY($x2c2,$y2);
	$pdf->Cell(20, 4, number_format(100*$discount_rate,2) . "%", 0, "", "R");



	//liquidation parameters
	$x1c4 = 256;
	$x2c4 = 283;

	$pdf->SetXY($x1c4,$y4);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 4, "Liquidation Revenue on Keymoney", 0, "", "L");

	$pdf->SetXY($x2c4,$y4);
	$pdf->Cell(20, 4, number_format($cer_basicdata["cer_bascidata_liquidation_keymoney"],2) . "%", 0, "", "R");

	$y4 = $y4+ $standard_y;

	$pdf->SetXY($x1c4,$y4);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 4, "Liquidation Revenue on Deposit", 0, "", "L");

	$pdf->SetXY($x2c4,$y4);
	$pdf->Cell(20, 4, number_format($cer_basicdata["cer_basicdata_liquidation_deposit"],2) . "%", 0, "", "R");

	$y4 = $y4+ $standard_y;

	$pdf->SetXY($x1c4,$y4);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 4, "Liquidation Revenue on Stock", 0, "", "L");

	$pdf->SetXY($x2c4,$y4);
	$pdf->Cell(20, 4, number_format($cer_basicdata["cer_bascidata_liquidation_stock"],2) . "%", 0, "", "R");

	$y4 = $y4+ $standard_y;

	$pdf->SetXY($x1c4,$y4);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 4, "Liquidation Revenue on Staff", 0, "", "L");

	$pdf->SetXY($x2c4,$y4);
	$pdf->Cell(20, 4, number_format($cer_basicdata["cer_bascicdate_liquidation_staff"],2) . "%", 0, "", "R");

	
	


	//residual values
	$y4 = $y4+ $standard_y;
	$y4 = $y4+ $standard_y;
	
	$pdf->SetXY($x1c4,$y4);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 4, "Residual Value of Fixed Assets", 0, "", "L");

	$pdf->SetXY($x2c4,$y4);
	$pdf->Cell(20, 4, number_format($cer_basicdata["cer_basicdata_residual_value"],2), 0, "", "R");

	$y4 = $y4+ $standard_y;

	$pdf->SetXY($x1c4,$y4);
	$pdf->SetFont("arialn", "", 8);
	
	$pdf->Cell(50, 4, "Residual Value of Keymoney", 0, "", "L");
	$pdf->SetXY($x2c4,$y4);
	$pdf->Cell(20, 4, number_format($cer_basicdata["cer_basicdata_residualkeymoney_value"],2), 0, "", "R");

	$y4 = $y4+ $standard_y;

	$pdf->SetXY($x1c4,$y4);
	$pdf->Cell(50, 4, "Recoverable Keymoney", 0, "", "L");
	$pdf->SetXY($x2c4,$y4);
	$pdf->Cell(20, 4, number_format($cer_basicdata["cer_basicdata_recoverable_keymoney"],2), 0, "", "R");


	//sales values
	$captions = array();
	
	$captions[] = "Watches Units";
	$captions[] = "Jewellery Units";
	$captions[] = "Watches av. Price";
	$captions[] = "Jewellery Av. Price";
	$captions[] = "Sales Value Watches";
	$captions[] = "Sales Value Jewellery";
	$captions[] = "Sales Value Accessories";
	$captions[] = "Sales Value Services";
	$captions[] = "Total Gross Sales";
	$captions[] = "Sales Reduction";
	$captions[] = "Total Net Sales";
	$captions[] = "";
	$captions[] = "Cost of Watches Sold";
	$captions[] = "Cost of Jewellery Sold";
	$captions[] = "Cost of Accessories Sold";
	$captions[] = "Cost of Services Sold";
	$captions[] = "Cost of Products Sold";
	$captions[] = "Total Gross Margin";
	$captions[] = "";
	$captions[] = "Marketing Expenses";
	$captions[] = "";


	foreach($expenses as $expense_name=>$value)
	{
		$captions[] = $expense_name;
	}
	$captions[] = "Depreciation on Investment";

	foreach($fixed_assets2 as $key=>$itype)
	{
		if(isset($debug_depreciation[$itype]))
		{
			$captions[] = "  " . $investment_names[$itype];
		}
	}

	$captions[] = "  Depreciation on Future Investment";
	
	$captions[] = "Total Depreciation on Investment";
	$captions[] = "";
	$captions[] = "Depreciation on Goodwill/Keymoney";

	$captions[] = "";

	$captions[] = "Total Expenses";

	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	$y_start = $y;
	$standard_xoffset = 20;

	$pdf->SetFont("arialn", "", 8);
	foreach($captions as $key=>$caption)
	{
		$pdf->SetXY($x1,$y);
		$pdf->Cell(50, 4, $caption, 0, "", "L");
		$y = $y+ $standard_y;
	}
	$pdf->SetFont("arialn", "B", 8);
	$xi = $standard_xoffset;
	$y = $y_start - $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4, $year, 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	$pdf->SetFont("arialn", "", 8);
	$xi = $standard_xoffset;
	$y = $y_start;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  $sales_units_watches_values[$year], 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  $sales_units_jewellery_values[$year], 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  $average_price_watches_values[$year], 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  $average_price_jewellery_values[$year], 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($sales_watches_values[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($sales_jewellery_values[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($sales_accessories_values[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($sales_customer_service_values[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($total_gross_sales_values[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($sales_reduction_values[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($total_net_sales_values[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	// costs of products sold
	
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  "", 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}


	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		if(array_key_exists($year, $cost_of_production_values['cost_watches_values']))
		{
			$pdf->Cell(12, 4,  number_format($cost_of_production_values['cost_watches_values'][$year],2), 0, "", "R");
		}
		else
		{
			$pdf->Cell(12, 4,  '0', 0, "", "R");
		}
			
		$xi = $xi + $standard_xoffset;
	}
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		if(array_key_exists($year, $cost_of_production_values['cost_sales_jewellery_values']))
		{
			$pdf->Cell(12, 4,  number_format($cost_of_production_values['cost_sales_jewellery_values'][$year],2), 0, "", "R");
		}
		else
		{
			$pdf->Cell(12, 4,  '0', 0, "", "R");
		}
		$xi = $xi + $standard_xoffset;
	}
	
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		if(array_key_exists($year, $cost_of_production_values['cost_sales_accessories_values']))
		{
			$pdf->Cell(12, 4,  number_format($cost_of_production_values['cost_sales_accessories_values'][$year],2), 0, "", "R");
		}
		else
		{
			$pdf->Cell(12, 4,  '0' , 0, "", "R");
		}
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		if(array_key_exists($year, $cost_of_production_values['cost_sales_customer_service_values']))
		{
			$pdf->Cell(12, 4,  number_format($cost_of_production_values['cost_sales_customer_service_values'][$year],2), 0, "", "R");
		}
		else
		{
			$pdf->Cell(12, 4,  '0' , 0, "", "R");
		}
		$xi = $xi + $standard_xoffset;
	}
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$cost_of_products_sold = 0;
		if(array_key_exists($year, $cost_of_production_values['cost_watches_values']))
		{
			$cost_of_products_sold = $cost_of_products_sold + $cost_of_production_values['cost_watches_values'][$year];
		}
		if(array_key_exists($year, $cost_of_production_values['cost_sales_jewellery_values']))
		{
			$cost_of_products_sold = $cost_of_products_sold + $cost_of_production_values['cost_sales_jewellery_values'][$year];
		}
		if(array_key_exists($year, $cost_of_production_values['cost_sales_accessories_values']))
		{
			$cost_of_products_sold = $cost_of_products_sold + $cost_of_production_values['cost_sales_accessories_values'][$year];
		}
		if(array_key_exists($year, $cost_of_production_values['cost_sales_customer_service_values']))
		{
			$cost_of_products_sold = $cost_of_products_sold + $cost_of_production_values['cost_sales_customer_service_values'][$year];
		}

		
		$pdf->SetXY($x2+$xi,$y);
		//$pdf->Cell(12, 4,  number_format($material_of_products_values[$year],2), 0, "", "R");
		$pdf->Cell(12, 4,  number_format($cost_of_products_sold,2), 0, "", "R");

		
		$xi = $xi + $standard_xoffset;
	}
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($total_gross_margin_values[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	
	// marketing expenses
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4, "", 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($marketing_expenses_values[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
		
	//expenses
	$y = $y+ $standard_y;
	foreach($expenses as $name=>$values)
	{
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($values as $year=>$value)
		{
			
			
			$pdf->SetXY($x2+$xi,$y);
			
			
			if($cer_basicdata["cer_basicdata_calculate_rent_avg"] == 1 and 
			   ($expense_types[$year][$name] == 2 or $expense_types[$year][$name] == 3 or $expense_types[$year][$name] == 16)) // rental cost
			{
				if($expense_types[$year][$name] == 2) // fixed rent
				{
					$pdf->Cell(12, 4,  number_format($fixedrents_values[$year],2, ".", "'"), 0, "", "R");
				}
				elseif($expense_types[$year][$name] == 16) // turnoverbased
				{
					$pdf->Cell(12, 4,  number_format($turnoverbasedrents_values[$year],2, ".", "'"), 0, "", "R");
				}
				elseif($expense_types[$year][$name] == 3) // aditional renal cost
				{
					$pdf->Cell(12, 4,  number_format($additionalrents_values[$year],2, ".", "'"), 0, "", "R");
				}
				else
				{
					$pdf->Cell(12, 4,  number_format($value,2, ".", "'"), 0, "", "R");
				}
			}
			else
			{
				$pdf->Cell(12, 4,  number_format($value,2), 0, "", "R");
			}

			$xi = $xi + $standard_xoffset;
		}

		
	}

	//Depreciation
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	
	foreach($fixed_assets2 as $key=>$itype)
	{
		if(isset($debug_depreciation[$itype]))
		{
			if(!$amounts[$itype]){$amounts[$itype] = "-";}
			if(!$depryears[$itype]){$depryears[$itype] = "-";}

			$xi = $standard_xoffset;
			foreach($years as $key=>$year)
			{
				$pdf->SetXY($x2+$xi,$y);
				if(isset($debug_depreciation[$itype][$year]))
				{
					$pdf->Cell(12, 4,  number_format($debug_depreciation[$itype][$year],2), 0, "", "R");
				}
				else
				{
					$pdf->Cell(12, 4, "0.00", 0, "", "R");
				}
				$xi = $xi + $standard_xoffset;
			}
			$y = $y+ $standard_y;
		}
	}


	$_depr_fin = $_depreciation["depreciation_on_future_investments"];
	$xi = $standard_xoffset;
	foreach($years as $key=>$year)
	{
		if(array_key_exists($year, $_depr_fin))
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($_depr_fin[$year],2), 0, "", "R");
			$xi = $xi + $standard_xoffset;
			
		}
		else
	    {
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  "0.00", 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}
	}


	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($depreciation[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$tmp = 0;
		if(array_key_exists(15, $debug_depreciation) and array_key_exists(17, $debug_depreciation))
		{
			
			if(array_key_exists($year, $debug_depreciation[15]) and array_key_exists($year, $debug_depreciation[17]))
			{
				$tmp = $tmp + $debug_depreciation[15][$year] + $debug_depreciation[17][$year];
			}
		}
		elseif(array_key_exists(15, $debug_depreciation))
		{
			$tmp = $tmp + $debug_depreciation[15][$year];
		}
		elseif(array_key_exists(17, $debug_depreciation))
		{
			$tmp = $tmp +$debug_depreciation[17][$year];
		}
		elseif($tmp > 0) {
			//nop
		}
		else
	    {
			$tmp = "0.00";
		}

		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($tmp,2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}


	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($total_indirect_expenses_values[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	

//page 2
$pdf->AddPage();
// draw page box
$pdf->SetXY($margin_left-2,$margin_top);
$pdf->Cell(406, 270, "", 1);
// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "B", 11);
$pdf->Cell(255, 6, "Debug Output for the Retail Business Plan Overview for: " . $project_name . ", Page 2", 0, "", "L");

	$captions = array();
	$captions[] = "Gross Margin";
	$captions[] = "Marekting Expenses";
	$captions[] = "Contribution";
	$captions[] = "Expenses";
	$captions[] = "Operating Income without Wholsale Margin";
	$captions[] = "";
	//$captions[] = "Material Costs of Products Sold";
	$captions[] = "Wholesale margin watches";
	$captions[] = "Wholesale margin jewellery";
	$captions[] = "Wholesale margin accessories";
	$captions[] = "Wholesale margin services";
	$captions[] = "Operating Income inkl. Wholsale Margin";
	$captions[] = "";

	$captions[] = "Liabilities";
	$captions[] = "Liabilities Change";
	$captions[] = "Stock";
	$captions[] = "Stock Change";

	$captions[] = "";
	$captions[] = "Operating Income without Wholsale Margin";
	$captions[] = "Depreciation on Goodwill/Keymoney";
	$captions[] = "Depreciation on Investment";
	$captions[] = "Variation Working Capital";
	$captions[] = "Initial Investment/Disinvestment";
	$captions[] = "Total Cash Flow";
	$captions[] = "";
	$captions[] = "Financial Expenses";
	$captions[] = "Income Taxes";
	$captions[] = "Total FCF";
	$captions[] = "Cash Flow";
	$captions[] = "";

	$captions[] = "Total Cash Flow Wholesale";
	$captions[] = "";

	$captions[] = "Net Present Value Retail";
	$captions[] = "Discounted Cash Flow ROI in %";
	$captions[] = "Pay Back Period";
	
	$captions[] = "";
	$captions[] = "Net Present Value Wholesale";
	$captions[] = "Discounted Cash Flow ROI in %";
	$captions[] = "Pay Back Period";
	
	$captions[] = "";
	$captions[] = "";
	$captions[] = "Pay Back Cummulated Retail Values";
	$captions[] = "Pay Back Period Retail Values";
	$captions[] = "";
	$captions[] = "Pay Back Cummulated Whole Sale Values";
	$captions[] = "Pay Back Period Whole Sale Values";

	$captions[] = "";
	$captions[] = "Interest Rate";
	$captions[] = "Inflation Rate";
	$captions[] = "Stock in Months";
	$captions[] = "Payment Terms in Months";


	$x1 = $margin_left;
	$x2 = $margin_left+32;
	$x3 = $margin_left+52;

	
	$y = $margin_top + 6;
	$y2 = $y + $standard_y;   // for column2
	
	$pdf->SetFont("arialn", "B", 8);
	$xi = $standard_xoffset;

	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4, $year, 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	
	
	$y = $y + $standard_y;
	$y_start = $y;
	$pdf->SetFont("arialn", "", 8);
	foreach($captions as $key=>$caption)
	{
		$pdf->SetXY($x1,$y);
		$pdf->Cell(50, 4, $caption, 0, "", "L");
		$y = $y+ $standard_y;
	}
	
	$y = $y_start - $standard_y;
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($total_gross_margin_values[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	
	// marketing expenses
	$xi = $standard_xoffset;
	//$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4, "", 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($marketing_expenses_values[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	

	//contribution II
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$tmp9 =  $total_gross_margin_values[$year] - $marketing_expenses_values[$year];
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($tmp9,2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($total_indirect_expenses_values[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($operating_income01_values[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	
	//material cost of products sold
	/*
	$y = $y+ $standard_y;
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($material_of_products_values[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	*/

	
	//wholesale margins
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	$xi = $standard_xoffset;
	foreach($years as $key=>$year)
	{
		$tmp = $wholesale_margin_of_watches_sold[$year];
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($tmp,2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$y = $y+ $standard_y;
	$xi = $standard_xoffset;
	foreach($years as $key=>$year)
	{
		$tmp = $wholesale_margin_of_jewellery_sold[$year];
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($tmp,2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$y = $y+ $standard_y;
	$xi = $standard_xoffset;
	foreach($years as $key=>$year)
	{
		$tmp = $wholesale_margin_of_accessories_sold[$year];
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($tmp,2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	
	$y = $y+ $standard_y;
	$xi = $standard_xoffset;
	foreach($years as $key=>$year)
	{
		$tmp = $wholesale_margin_of_services_sold[$year];
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($tmp,2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	
	/*
	$y = $y+ $standard_y;
	$xi = $standard_xoffset;
	foreach($years as $key=>$year)
	{
		$tmp11 = $material_of_products_values[$year]; // Wholesale Margin
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($tmp11,2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	*/

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($operating_income02_values[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}


	$y = $y+ $standard_y;
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($debugvar_liabilities[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($debugvar_liabilities_change[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($debugvar_stock[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($debugvar_stock_change[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}


	$y = $y+ $standard_y;
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($operating_income01_values[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		if(array_key_exists(15, $debug_depreciation) and array_key_exists(17, $debug_depreciation))
		{
			$tmp = "0.00";
			if(array_key_exists($year, $debug_depreciation[15]) and array_key_exists($year, $debug_depreciation[17]))
			{
				$tmp = $debug_depreciation[15][$year] + $debug_depreciation[17][$year];
			}
		}
		elseif(array_key_exists(15, $debug_depreciation))
		{
			$tmp = $debug_depreciation[15][$year];
		}
		elseif(array_key_exists(17, $debug_depreciation))
		{
			$tmp = $debug_depreciation[17][$year];
		}
		else
		{
			$tmp = "0.00";
		}
		

		$pdf->SetXY($x2+$xi,$y);
		//$pdf->Cell(12, 4,  number_format($tmp,2), 0, "", "R");
		$pdf->Cell(12, 4,  number_format($prepayed_rents[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($depreciation[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($debugvar_working_capital[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($debug_initial_investment[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($total_cash_flow_values[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}


	
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($debugvar_finacial_expenses[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($income_taxes_values[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($total_cash_flow_fcf_values[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($cash_flow_values[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($cash_flow_whole_sale_values[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	$pdf->SetXY($x2+$xi,$y);
	$pdf->Cell(12, 4,  number_format($net_present_value_retail,2), 0, "", "R");

	$formula = "NPV(" . $discount_rate . ", [";
	foreach($cfs as $key=>$value)
	{
		$formula .= number_format($value,2) . ", ";
	}
	$formula = substr($formula, 0, strlen($formula) - 2) . "]) - " . number_format($_initial_investment, 2);

	$xi = $xi + 14;
	$pdf->SetXY($x2+$xi,$y);
	$pdf->Cell(12, 4, $formula, 0, "", "L");

	
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$pdf->SetXY($x2+$xi,$y);
	if(abs($discounted_cash_flow_retail) < 10000)
	{
		$pdf->Cell(12, 4,  $discounted_cash_flow_retail, 0, "", "R");
	}


	$formula = "IRR([";
	foreach($cfs1 as $key=>$value)
	{
		$formula .= number_format($value, 2) . ", ";
	}
	$formula = substr($formula, 0, strlen($formula) - 2) . "], -0.1)";

	$xi = $xi + 14;
	$pdf->SetXY($x2+$xi,$y);
	$pdf->Cell(12, 4, $formula, 0, "", "L");


	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$pdf->SetXY($x2+$xi,$y);
	$pdf->Cell(12, 4,  number_format($pay_back_period_retail,2), 0, "", "R");

	/*
	$formula = "NPV(" . $discount_rate . ", [";
	foreach($cfs2 as $key=>$value)
	{
		$formula .= number_format($value, 2) . ", ";
	}
	$formula = substr($formula, 0, strlen($formula) - 2) . "]) - " . number_format($_initial_investment, 2);

	$xi = $xi + 14;
	$pdf->SetXY($x2+$xi,$y);
	$pdf->Cell(12, 4, $formula, 0, "", "L");
	*/
	
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	$pdf->SetXY($x2+$xi,$y);
	$pdf->Cell(12, 4,  number_format($net_present_value_wholesale,2), 0, "", "R");

	$formula = "NPV(" . $discount_rate . ", [";
	foreach($cfs2 as $key=>$value)
	{
		$formula .= number_format($value, 2) . ", ";
	}
	$formula = substr($formula, 0, strlen($formula) - 2) . "]) - " . number_format($_initial_investment, 2);

	$xi = $xi + 14;
	$pdf->SetXY($x2+$xi,$y);
	$pdf->Cell(12, 4, $formula, 0, "", "L");


	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$pdf->SetXY($x2+$xi,$y);
	if(abs($discounted_cash_flow_wholesale) < 10000)
	{
		$pdf->Cell(12, 4, $discounted_cash_flow_wholesale, 0, "", "R");
	}

	$formula = "IRR([";
	foreach($cfs3 as $key=>$value)
	{
		$formula .= number_format($value, 2) . ", ";
	}
	$formula = substr($formula, 0, strlen($formula) - 2) . "], -0.1)";

	$xi = $xi + 14;
	$pdf->SetXY($x2+$xi,$y);
	$pdf->Cell(12, 4, $formula, 0, "", "L");


	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$pdf->SetXY($x2+$xi,$y);
	$pdf->Cell(12, 4,  number_format($pay_back_period_wholesale,2), 0, "", "R");

	/*
	$formula = "NPV(" . $discount_rate . ", [";
	foreach($cfs2 as $key=>$value)
	{
		$formula .= number_format($value, 2) . ", ";
	}
	$formula = substr($formula, 0, strlen($formula) - 2) . "]) - " . number_format($_initial_investment, 2);

	$xi = $xi + 14;
	$pdf->SetXY($x2+$xi,$y);
	$pdf->Cell(12, 4, $formula, 0, "", "L");
    */
	


	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($pay_back_cummunlated_retail_values[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($pay_back_period_retail_values[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}


	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($pay_back_cummunlated_wholesale_values[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($pay_back_period_wholesale_values[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	$xi = $standard_xoffset;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4, number_format($cer_basicdata_interest_rates[$year],2) . "%", 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$y = $y+ $standard_y;
	$xi = $standard_xoffset;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4, number_format($cer_basicdata_inflation_rates[$year],2) . "%", 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$y = $y+ $standard_y;
	$xi = $standard_xoffset;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4, number_format($stock_data[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$y = $y+ $standard_y;
	$xi = $standard_xoffset;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4, number_format($payment_terms[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}


//page 3, BRAND Detail
$page = 2;
if(count($cer_brands) > 0)
{

	foreach($cer_brands as $brand_id=>$brand_name)
	{

		$page++;

		$x1 = $margin_left;
		$x2 = $margin_left+32;
		$x3 = $margin_left+52;
		
		$y = $margin_top;
		$y = $y + 6;
		$y2 = $y + $standard_y;   // for column2
		$y3 = $y + $standard_y;   // for column3
		$y4 = $y + $standard_y;   // for column4
		$y5 = $y + $standard_y;   // for column5
		$y6 = $y + $standard_y;   // for column6
		
		$pdf->AddPage();
		// draw page box
		$pdf->SetXY($margin_left-2,$margin_top);
		$pdf->Cell(406, 270, "", 1);
		// Title first line
		$pdf->SetXY($margin_left,$margin_top);
		$pdf->SetFont("arialn", "B", 11);
		$pdf->Cell(255, 6, $brand_name . " - Debug Output for the Retail Business Plan Overview for: " . $project_name . ", Page " . $page, 0, "", "L");



		//parameter
		$x1c2 = $margin_left;
		$x2c2 = $margin_left + 25;
		
		
		$pdf->SetXY($x1c2,$y2);
		$pdf->SetFont("arialn", "", 8);
		
		$pdf->Cell(50, 4, "Sales Reduction Watches", 0, "", "L");

		$pdf->SetXY($x2c2,$y2);
		$pdf->Cell(20, 4, number_format($sales_reduction_watches[$brand_id],2) . "%", 0, "", "R");

		$y2 = $y2+ $standard_y;

		$pdf->SetXY($x1c2,$y2);
		$pdf->Cell(50, 4, "Sales Reduction Jewellery", 0, "", "L");

		$pdf->SetXY($x2c2,$y2);
		$pdf->Cell(20, 4, number_format($sales_reduction_bijoux[$brand_id],2) . "%", 0, "", "R");

		$y2 = $y2+ $standard_y;

		$pdf->SetXY($x1c2,$y2);
		$pdf->Cell(50, 4, "Sales Reduction Accessories", 0, "", "L");

		$pdf->SetXY($x2c2,$y2);
		$pdf->Cell(20, 4, number_format($sales_reduction_accessories[$brand_id],2) . "%", 0, "", "R");

		$y2 = $y2+ $standard_y;
		$pdf->SetXY($x1c2,$y2);
		$pdf->Cell(50, 4, "Sales Reduction Services", 0, "", "L");
		$pdf->SetXY($x2c2,$y2);
		$pdf->Cell(20, 4, number_format($sales_reduction_services[$brand_id],2) . "%", 0, "", "R");

		/*
		$y2 = $y2+ $standard_y;
		$pdf->SetXY($x1c2,$y2);
		$pdf->Cell(50, 4, "Sales Reduction Credit Cards", 0, "", "L");
		$pdf->SetXY($x2c2,$y2);
		$pdf->Cell(20, 4, number_format($sales_reduction_creditcard[$brand_id],2) . "%", 0, "", "R");
		*/
	
		//parameter

		$x1c2 = $margin_left + 55;
		$x2c2 = $x1c2 + 30;
		
		$y2 = $y4;
		$pdf->SetXY($x1c2,$y2);
		$pdf->SetFont("arialn", "", 8);
		
		$pdf->SetXY($x1c2,$y2);
		$pdf->Cell(50, 4, "Wholsale Margin Watches", 0, "", "L");

		$pdf->SetXY($x2c2,$y2);
		$pdf->Cell(20, 4, number_format($wholesale_margin_watches[$brand_id],2) . "%", 0, "", "R");

		$y2 = $y2+ $standard_y;

		$pdf->SetXY($x1c2,$y2);
		$pdf->Cell(50, 4, "Wholsale Margin Bijoux", 0, "", "L");

		$pdf->SetXY($x2c2,$y2);
		$pdf->Cell(20, 4, number_format($wholesale_margin_bijoux[$brand_id],2) . "%", 0, "", "R");


		$y2 = $y2+ $standard_y;

		$pdf->SetXY($x1c2,$y2);
		$pdf->Cell(50, 4, "Wholsale Margin Accessories", 0, "", "L");

		$pdf->SetXY($x2c2,$y2);
		$pdf->Cell(20, 4, number_format($wholesale_margin_accessories[$brand_id],2) . "%", 0, "", "R");


		$y2 = $y2+ $standard_y;

		$pdf->SetXY($x1c2,$y2);
		$pdf->Cell(50, 4, "Wholsale Margin Services", 0, "", "L");

		$pdf->SetXY($x2c2,$y2);
		$pdf->Cell(20, 4, number_format($wholesale_margin_service[$brand_id],2) . "%", 0, "", "R");


		
		//parameters

		$x1c2 = $margin_left + 115;
		$x2c2 = $x1c2 + 30;
		
		$y2 = $y4;
		$pdf->SetXY($x1c2,$y2);
		$pdf->SetFont("arialn", "", 8);
		
		
		$pdf->Cell(50, 4, "Cost of Watches", 0, "", "L");

		$pdf->SetXY($x2c2,$y2);
		$pdf->Cell(20, 4, number_format($cost_watches[$brand_id],2) . "%", 0, "", "R");

		$y2 = $y2+ $standard_y;

		$pdf->SetXY($x1c2,$y2);
		$pdf->Cell(50, 4, "Cost of Jewellery", 0, "", "L");

		$pdf->SetXY($x2c2,$y2);
		$pdf->Cell(20, 4, number_format($cost_bijoux[$brand_id],2) . "%", 0, "", "R");

		$y2 = $y2+ $standard_y;
		$pdf->SetXY($x1c2,$y2);
		$pdf->Cell(50, 4, "Cost of Accessories", 0, "", "L");
		$pdf->SetXY($x2c2,$y2);
		$pdf->Cell(20, 4, number_format($cost_accessories[$brand_id],2) . "%", 0, "", "R");

		$y2 = $y2+ $standard_y;
		$pdf->SetXY($x1c2,$y2);
		$pdf->Cell(50, 4, "Cost of Services", 0, "", "L");
		$pdf->SetXY($x2c2,$y2);
		$pdf->Cell(20, 4, number_format($cost_services[$brand_id],2) . "%", 0, "", "R");

		$y2 = $y2+ 2*$standard_y;

		$y = $pdf->getY() + 10;
		

		//sales values
		$captions = array();
		
		$captions[] = "Watches Units";
		$captions[] = "Jewellery Units";
		$captions[] = "Watches av. Price";
		$captions[] = "Jewellery Av. Price";
		$captions[] = "Sales Value Watches";
		$captions[] = "Sales Value Jewellery";
		$captions[] = "Sales Value Accessories";
		$captions[] = "Sales Value Services";
		$captions[] = "Total Gross Sales";
		$captions[] = "Sales Reduction";
		$captions[] = "Total Net Sales";
		$captions[] = "";
		$captions[] = "Cost of Watches Sold";
		$captions[] = "Cost of Jewellery Sold";
		$captions[] = "Cost of Accessories Sold";
		$captions[] = "Cost of Services Sold";
		$captions[] = "Cost of Products Sold";
		$captions[] = "Total Gross Margin";
		$captions[] = "";
		$captions[] = "Wholsale Margin Watches";
		$captions[] = "Wholsale Margin Jewellery";
		$captions[] = "Wholsale Margin Accessories";
		$captions[] = "Wholsale Margin Services";
		
		$y = $y+ $standard_y;
		$y = $y+ $standard_y;
		$y = $y+ $standard_y;
		$y_start = $y;
		$standard_xoffset = 20;

		$pdf->SetFont("arialn", "", 8);
		foreach($captions as $key=>$caption)
		{
			$pdf->SetXY($x1,$y);
			$pdf->Cell(50, 4, $caption, 0, "", "L");
			$y = $y+ $standard_y;
		}
		$pdf->SetFont("arialn", "B", 8);
		$xi = $standard_xoffset;
		$y = $y_start - $standard_y;
		foreach($years as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4, $year, 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}
		$pdf->SetFont("arialn", "", 8);
		$xi = $standard_xoffset;
		$y = $y_start;
		foreach($years as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  $sales_units_watches_values_per_brand[$brand_id][$year], 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  $sales_units_jewellery_values_per_brand[$brand_id][$year], 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}
		
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  $average_price_watches_values_per_brand[$brand_id][$year], 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  $average_price_jewellery_values_per_brand[$brand_id][$year], 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}
		
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($sales_watches_values_per_brand[$brand_id][$year],2), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($sales_jewellery_values_per_brand[$brand_id][$year],2), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}

		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($sales_accessories_values_per_brand[$brand_id][$year],2), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}
		
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($sales_customer_service_values_per_brand[$brand_id][$year],2), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($total_gross_sales_values_per_brand[$brand_id][$year],2), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}


		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($sales_reduction_values_per_brand[$brand_id][$year],2), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}
		
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($total_net_sales_values_per_brand[$brand_id][$year],2), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}
		// costs of products sold
		
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  "", 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}


		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			if(array_key_exists($year, $cost_of_production_watches_values_per_brand[$brand_id]))
			{
				$pdf->Cell(12, 4,  number_format($cost_of_production_watches_values_per_brand[$brand_id][$year],2), 0, "", "R");
			}
			else
			{
				$pdf->Cell(12, 4,  '0', 0, "", "R");
			}
				
			$xi = $xi + $standard_xoffset;
		}
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			if(array_key_exists($year, $cost_of_production_jewellery_values_per_brand[$brand_id]))
			{
				$pdf->Cell(12, 4,  number_format($cost_of_production_jewellery_values_per_brand[$brand_id][$year],2), 0, "", "R");
			}
			else
			{
				$pdf->Cell(12, 4,  '0', 0, "", "R");
			}
			$xi = $xi + $standard_xoffset;
		}
		
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			if(array_key_exists($year, $cost_of_production_accessories_values_per_brand[$brand_id]))
			{
				$pdf->Cell(12, 4,  number_format($cost_of_production_accessories_values_per_brand[$brand_id][$year],2), 0, "", "R");
			}
			else
			{
				$pdf->Cell(12, 4,  '0' , 0, "", "R");
			}
			$xi = $xi + $standard_xoffset;
		}

		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			if(array_key_exists($year, $cost_of_production_service_values_per_brand[$brand_id]))
			{
				$pdf->Cell(12, 4,  number_format($cost_of_production_service_values_per_brand[$brand_id][$year],2), 0, "", "R");
			}
			else
			{
				$pdf->Cell(12, 4,  '0' , 0, "", "R");
			}
			$xi = $xi + $standard_xoffset;
		}
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years as $key=>$year)
		{
			$cost_of_products_sold = 0;
			if(array_key_exists($year, $cost_of_production_watches_values_per_brand[$brand_id]))
			{
				$cost_of_products_sold = $cost_of_products_sold + $cost_of_production_watches_values_per_brand[$brand_id][$year];
			}
			if(array_key_exists($year, $cost_of_production_jewellery_values_per_brand[$brand_id]))
			{
				$cost_of_products_sold = $cost_of_products_sold + $cost_of_production_jewellery_values_per_brand[$brand_id][$year];
			}
			if(array_key_exists($year, $cost_of_production_accessories_values_per_brand[$brand_id]))
			{
				$cost_of_products_sold = $cost_of_products_sold + $cost_of_production_accessories_values_per_brand[$brand_id][$year];
			}
			if(array_key_exists($year, $cost_of_production_service_values_per_brand[$brand_id]))
			{
				$cost_of_products_sold = $cost_of_products_sold + $cost_of_production_service_values_per_brand[$brand_id][$year];
			}

			
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($cost_of_products_sold,2), 0, "", "R");

			
			$xi = $xi + $standard_xoffset;
		}
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($total_gross_margin_values_per_brand[$brand_id][$year],2), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}

		$xi = $standard_xoffset;
		$y = $y+ 2*$standard_y;
		foreach($years as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($wholesale_margin_of_watches_sold_per_brand[$brand_id][$year],2), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}
		
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($wholesale_margin_of_jewellery_sold_per_brand[$brand_id][$year],2), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}

		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($wholesale_margin_of_accessories_sold_per_brand[$brand_id][$year],2), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}

		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($wholesale_margin_of_services_sold_per_brand[$brand_id][$year],2), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}

	}
}

/*80 percent scenario*/

$standard_xoffset = $standard_xoffset + 2;
	//page 3
	$pdf->AddPage();
	// draw page box
	$pdf->SetXY($margin_left-2,$margin_top);
	$pdf->Cell(406, 270, "", 1);
	// Title first line
	$pdf->SetXY($margin_left,$margin_top);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(255, 6, "Debug Output for the Retail Business Plan Overview for: " . $project_name . ", Page 2", 0, "", "L");

	$captions = array();
	$captions[] = "Total Gross Sales 80%";
	$captions[] = "Sales Reduction 80%";
	$captions[] = "Total Net Sales 80%";
	$captions[] = "Material Costs of Products Sold";
	$captions[] = "Gross Margin 80%";
	$captions[] = "";
	$captions[] = "Marketing Expenses";
	$captions[] = "";

	foreach($expenses as $expense_name=>$value)
	{
		$captions[] = $expense_name;
	}
	
	
	$captions[] = "";
	$captions[] = "Total Depreciation on Investment";
	$captions[] = "Depreciation on Goodwill/Keymoney";

	$captions[] = "";

	$captions[] = "Total Expenses";

	$captions[] = "";
	$captions[] = "WSM (scenario 80%)";
	$captions[] = "";

	$captions[] = "Operating Income without Wholsale Margin";
	$captions[] = "Operating Income incl. WSM (scenario 80%)";


	

	$x1 = $margin_left;
	$x2 = $margin_left+32;
	$x3 = $margin_left+52;

	
	$y = $margin_top + 6;
	$y2 = $y + $standard_y;   // for column2
	
	$pdf->SetFont("arialn", "B", 8);
	$xi = $standard_xoffset;

	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4, $year, 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	
	
	$y = $y + $standard_y;
	$y_start = $y;
	$pdf->SetFont("arialn", "", 8);
	foreach($captions as $key=>$caption)
	{
		$pdf->SetXY($x1,$y);
		$pdf->Cell(50, 4, $caption, 0, "", "L");
		$y = $y+ $standard_y;
	}
	
	
	$y = $y_start - $standard_y;
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($total_gross_sales_80_percent_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}


	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($sales_reduction_80_percent_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}


	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($total_net_sales_80_percent_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}



	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format(0.8*$material_of_products_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}


	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($total_gross_margin_80_percent_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

		
	// marketing expenses
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4, "", 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($marketing_expenses_values[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
		
	//expenses
	$y = $y+ $standard_y;
	foreach($expenses as $name=>$values)
	{
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($values as $year=>$value)
		{
			$pdf->SetXY($x2+$xi,$y);
			
			if($cer_basicdata["cer_basicdata_calculate_rent_avg"] == 1 and 
			   ($expense_types[$year][$name] == 2 
				or $expense_types[$year][$name] == 3 
				or $expense_types[$year][$name] == 16
				or $expense_types[$year][$name] == 18
				or $expense_types[$year][$name] == 19
				or $expense_types[$year][$name] == 20)) // rental cost
			{
				if($expense_types[$year][$name] == 2) // fixed rent
				{
					$pdf->Cell(12, 4,  number_format($fixedrents_values[$year],2, ".", "'"), 0, "", "R");
				}
				elseif($expense_types[$year][$name] == 16) // turnoverbased
				{
					$pdf->Cell(12, 4,  number_format($turnoverbasedrents_80_percent_values[$year],2, ".", "'"), 0, "", "R");
				}
				elseif($expense_types[$year][$name] == 18) // taxes
				{
					$pdf->Cell(12, 4,  number_format($taxes_80_percent_values[$year],2, ".", "'"), 0, "", "R");
				}
				elseif($expense_types[$year][$name] == 19) // passenger index
				{
					$pdf->Cell(12, 4,  number_format($passenger_index_80_percent_values[$year],2, ".", "'"), 0, "", "R");
				}
				elseif($expense_types[$year][$name] == 3) // aditional renal cost
				{
					$pdf->Cell(12, 4,  number_format($additionalrents_values[$year],2, ".", "'"), 0, "", "R");
				}
				else
				{
					$pdf->Cell(12, 4,  number_format($value,2, ".", "'"), 0, "", "R");
				}
			}
			else
			{
				if($expense_types[$year][$name] == 2 
					or $expense_types[$year][$name] == 3 
					or $expense_types[$year][$name] == 16
					or $expense_types[$year][$name] == 18
					or $expense_types[$year][$name] == 19
					or $expense_types[$year][$name] == 20) // rental cost
				{
					if($expense_types[$year][$name] == 2) // fixed rent
					{
						$pdf->Cell(12, 4,  number_format($fixedrents_values[$year],2, ".", "'"), 0, "", "R");
					}
					elseif($expense_types[$year][$name] == 16) // turnoverbased
					{
						if(array_key_exists($year, $turnoverbasedrents_80_percent_values)) {
							$pdf->Cell(12, 4,  number_format($turnoverbasedrents_80_percent_values[$year],2, ".", "'"), 0, "", "R");
						}
						else {
							$pdf->Cell(12, 4,  '-', 0, "", "R");
						}
					}
					elseif($expense_types[$year][$name] == 18) // taxes
					{
						if(array_key_exists($year, $taxes_80_percent_values)) {
							$pdf->Cell(12, 4,  number_format($taxes_80_percent_values[$year],2, ".", "'"), 0, "", "R");
						}
						else {
							$pdf->Cell(12, 4,  '-', 0, "", "R");
						}
					}
					elseif($expense_types[$year][$name] == 19 and array_key_exists($year, $passenger_index_80_percent_values)) // passenger index
					{
						$pdf->Cell(12, 4,  number_format($passenger_index_80_percent_values[$year],2, ".", "'"), 0, "", "R");
					}
					elseif($expense_types[$year][$name] == 3) // aditional renal cost
					{
						$pdf->Cell(12, 4,  number_format($additionalrents_values[$year],2, ".", "'"), 0, "", "R");
					}
					else
					{
						$pdf->Cell(12, 4,  number_format($value,2, ".", "'"), 0, "", "R");
					}
				}
				else
				{
					$pdf->Cell(12, 4,  number_format($value,2, ".", "'"), 0, "", "R");
				}
			}
			$xi = $xi + $standard_xoffset;
		}
	}
		
	//Depreciation
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	
	

	$xi = $standard_xoffset;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($depreciation[$year],2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		if(array_key_exists(15, $debug_depreciation) and array_key_exists(17, $debug_depreciation))
		{
			$tmp = "0.00";
			if(array_key_exists($year, $debug_depreciation[15]) and array_key_exists($year, $debug_depreciation[17]))
			{
				$tmp = $debug_depreciation[15][$year] + $debug_depreciation[17][$year];
			}
		}
		elseif(array_key_exists(15, $debug_depreciation))
		{
			$tmp = $debug_depreciation[15][$year];
		}
		elseif(array_key_exists(17, $debug_depreciation))
		{
			$tmp = $debug_depreciation[17][$year];
		}
		else
	    {
			$tmp = "0.00";
		}

		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($tmp,2), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	
	
	

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
				
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($total_indirect_expenses_80_percent_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}


	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($total_wholsale_margin_80_percent[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}


	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($operating_income01_80_percent_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($operating_income02_80_percent_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}


$file_name = BRAND . "_Business_Plan_Debugging_" . $project["order_number"] . ".pdf";

$pdf->Output($file_name);

?>