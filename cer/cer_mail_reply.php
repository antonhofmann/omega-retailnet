<?php
/********************************************************************

    cer_mail_reply.php

    Mail Box: Reply to a Mail

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";

check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}

set_referer("cer_mail_new.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
$client_address = get_address($project["order_client_address"]);


$client = $client_address["company"] . ", " .
		  $client_address["zip"] . " " .
          $client_address["place"] . ", " .
          $client_address["country_name"];

$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_address"] . ", " .
		$project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];



//get recipients of emails
//$recipients = get_address_users($client_address["id"], $project["project_retail_coordinator"], $project["order_retail_operator"], $project["project_local_retail_coordinator"], $project["project_hq_project_manager"]);

$recipients = get_address_users($client_address["id"], $project["project_retail_coordinator"], $project["order_retail_operator"], $project["project_local_retail_coordinator"], 0, $project["order_user"], $project["project_design_supervisor"]);
//get userdata
$sender = get_user(user_id());
$recipients[] = array("name" => $sender["firstname"] . " " . $sender["name"], "email" => strtolower($sender["email"]), "roles" => "Copy to me");


//get recipients of maildialogue
$other_recipients = '';
$mail_sender_email = '';
$dialogue_recipients = array();
$sql = "select cer_mail_sender_email, cer_mail_reciepient from cer_mails " . 
       "where cer_mail_project = " . dbquote(param("pid")) . 
	   " and cer_mail_id = " . dbquote(param("id"));

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$dialogue_recipients = explode(' ', preg_replace("'\r?\n'"," ", $row["cer_mail_reciepient"]));
	$mail_sender_email = $row["cer_mail_sender_email"];
}


/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_mails", "cer_mails");

$form->add_label("shop_address", "POS Location", RENDER_HTML, $shop);
$form->add_label("client_address", "Client", RENDER_HTML, $client);
include("include/project_head.php");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("id", param("id"));

$form->add_section("Original Mail Message");
$form->add_label("cer_mail_sender", "Sent by", 0);
$form->add_label("cer_mail_group", "Topic", 0);
$form->add_label("cer_mail_text", "Message", RENDER_HTML);

$form->add_section("Reply Mail Message");
$form->add_multiline("text", "Message*", 20, NOTNULL, "", "", "", 70);

$form->add_section("Attachments");
$form->add_edit("mail_attachment1_title", "File Title");
$form->add_upload("mail_attachment1_path", "Attachment", "/files/cer/". $project["order_number"]);

$form->add_section("Recipients*");
$existing_recipients = array();
foreach($recipients as $key=>$user)
{
	if($user["roles"] == 'Finance Controller')
	{
		$form->add_checkbox($user["email"], $user["name"], 1, DISABLED, $user["roles"]);
	}
	elseif(in_array(strtolower($user["email"]) , $dialogue_recipients))
	{
		$form->add_checkbox($user["email"], $user["name"], true, "", $user["roles"]);
	}
	elseif(strtolower($user["email"] == $mail_sender_email))
	{
		$form->add_checkbox($user["email"], $user["name"], true, "", $user["roles"]);
	}
	else
	{
		$form->add_checkbox($user["email"], $user["name"], "", "", $user["roles"]);
	}

	$existing_recipients[] = strtolower($user["email"]);
}

foreach($dialogue_recipients as $value) {
	
	if(is_email_address($value))
	{
		if(!in_array(strtolower($value), $existing_recipients) and $value) {
			$other_recipients .= strtolower($value) . "\n";
		}
	}
}


$form->add_section("CC Recipients");
$form->add_modal_selector("ccmails", "Selected Recipients", 8, RENDER_HTML, $other_recipients);

$form->add_button("send", "Send Mail");
$form->add_button("back", "Back");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if($form->button("back"))
{
	$link = "cer_mails.php?pid=" . param("pid");
	redirect($link);
}
elseif($form->button("send"))
{
	$reciepient_selected = 0;
	$selected_recipients = array();

	foreach($recipients as $key=>$user)
	{
		if($form->value($user["email"]) == 1 or $user["roles"] == "Finance Controller")
		{
			$reciepient_selected = 1;
			$selected_recipients[strtolower($user["email"])] = strtolower($user["email"]);
		}
	}


	if($reciepient_selected == 0)
	{
		$form->error("You must select at least one reciepient.");
	}

	
	if($form->validate() and $reciepient_selected == 1)
	{
		
		//get all ccmails
		$ccmails =  explode(' ', preg_replace("'\r?\n'"," ",$form->value('ccmails'))); 
		foreach($ccmails as $ccmail) {
			if(is_email_address($ccmail)) {
				
				$selected_ccrecipients[strtolower($ccmail)] = strtolower($ccmail);
			}
		
		}

		
		if($form_type == "INR03")
		{
			$subject = MAIL_SUBJECT_PREFIX . ": " . $form->value("cer_mail_group") . " - Project " . $project["order_number"] . ": INR-03 for " . $project["order_shop_address_company"];
		}
		elseif($form_type == "AF")
		{
			$subject = MAIL_SUBJECT_PREFIX . ": " . $form->value("cer_mail_group") . " - Project " . $project["order_number"] . ": AF for " . $project["order_shop_address_company"];
		}
		else
		{
			$subject = MAIL_SUBJECT_PREFIX . ": " . $form->value("cer_mail_group") . " - Project " . $project["order_number"] . ": CER for " . $project["order_shop_address_company"];
		}

		$text = str_replace("\r\n", "\n", trim($form->value("text")));

		foreach($selected_recipients as $key=>$email)
		{
			$sql = "select user_email_cc, user_email_deputy " . 
			       "from users " . 
				   "where user_email = " . dbquote($email);

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				if($row["user_email_cc"])
				{
					$selected_ccrecipients[strtolower($row["user_email_cc"])] = strtolower($row["user_email_cc"]);
				}
				if($row["user_email_deputy"])
				{
					$selected_ccrecipients[strtolower($row["user_email_deputy"])] = strtolower($row["user_email_deputy"]);
				}
			}
		}


		$mail = new PHPMailer();
		$mail->Subject = $subject;
		$mail->SetFrom($sender["email"], $sender["firstname"] . " " . $sender["name"]);
		$mail->AddReplyTo($sender["email"], $sender["firstname"] . " " . $sender["name"]);

		$bodytext0 = str_replace("\r\n", "\n", $text) . "\n\n";
		$link ="cer_project.php?pid=" . param("pid");
		$bodytext = $bodytext0 . "Click below to have direct access to the project:\n";
		$bodytext = $bodytext .  APPLICATION_URL ."/cer/" . $link . "\n\n";           
		$mail->Body = $bodytext;

		if($form->value("mail_attachment1_path"))
		{
			$file_name = str_replace("/files/cer/". $project["order_number"]. "/", "",$form->value("mail_attachment1_path"));
			
			$filepath = $_SERVER["DOCUMENT_ROOT"] . "/" . $form->value("mail_attachment1_path");
			
			//$file_data = fread(fopen($filepath,"r"),filesize($filepath));

			$mail->AddAttachment($filepath);
		}

	
		$rcpts = "";
		foreach($selected_recipients as $key=>$value)
		{
			$mail->AddAddress($value);
			$rcpts .= $value . "\n";
		}

		foreach($selected_ccrecipients as $key=>$value)
		{
			if(!in_array($value,$selected_recipients))
			{
				$mail->AddCC($value);
				$rcpts .= $value . "\n";
			}
		}

		$mail->Send();
		

		//update mail history
		$fields = array();
		$values = array();

		
		$fields[] = "cer_mail_parent";
		$values[] = param("id");

		$fields[] = "cer_mail_group";
		$values[] = dbquote($form->value("cer_mail_group"));

		$fields[] = "cer_mail_project";
		$values[] = param("pid");

		$fields[] = "cer_mail_text";
		$values[] = dbquote($text);

		$fields[] = "cer_mail_sender";
		$values[] = dbquote($sender["firstname"] . " " . $sender["name"]);

		$fields[] = "cer_mail_sender_email";
		$values[] = dbquote($sender["email"]);

		$fields[] = "cer_mail_reciepient";
		$values[] = dbquote($rcpts);

		if($form->value("mail_attachment1_path"))
		{
			$fields[] = "cer_mail_attachment1_title";
			$values[] = dbquote($form->value("mail_attachment1_title"));

			$fields[] = "cer_mail_attachment1_path";
			$values[] = dbquote($form->value("mail_attachment1_path"));
		}
		
		$fields[] = "date_created";
		$values[] = "now()";

		$fields[] = "date_modified";
		$values[] = "now()";

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$fields[] = "user_modified";
		$values[] = dbquote(user_login());

		$sql = "insert into cer_mails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
		
		mysql_query($sql) or dberror($sql);

		$link = "cer_mails.php?pid=" . param("pid");
		redirect($link);
	}
}


/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();

if($form_type == "INR03")
{
	$page->title("INR-03 - Retail Furniture in Third-party Store: Reply Mail");
}
elseif($form_type == "AF")
{
	$page->title("Application Form: Reply Mail");
}
else
{
	$page->title("Capital Expenditure Request: ReplyMail");
}
$form->render();


?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#ccmails_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/shared/select_mail_recipients.php'
    });
    return false;
  });
});
</script>

<?php

require "include/footer_scripts.php";
$page->footer();

?>