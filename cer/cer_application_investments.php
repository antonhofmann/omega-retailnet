<?php
/********************************************************************

    cer_application_investment.php

    Application Form: investment information
    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";
require_once "../shared/project_cost_functions.php";

check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}


/********************************************************************
    prepare all data needed
*********************************************************************/
$posdata = get_pos_data($project["project_order"]);
$posleases = get_pos_leasedata($posdata["posaddress_id"], $project["project_order"]);

$months = array();
for($i=1;$i<12;$i++) {
	$months[$i] = $i;
}

$currency = get_cer_currency(param("pid"));

//build missing investment records
$result = build_missing_cer_investment_records(param("pid"));


//update investments with default depreciation in case of a franchisee SIS
if($project['project_cost_type'] != 1
   and $cer_basicdata["cer_basicdata_firstyear_depr"] > 0
   and $cer_basicdata["cer_basicdata_firstmonth"] > 0) {

	$sql = "select cer_investment_id 
	        from cer_investments
			where cer_investment_cer_version = 0 
			and cer_investment_depr_years = 0
			and cer_investment_depr_months = 0
			and cer_investment_project = " . $project['project_id'];


	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$sql_u = "update cer_investments set 
				cer_investment_depr_years =  3, " . 
				"cer_investment_depr_months =  0 " .
				" where cer_investment_type<> 18 
				   and cer_investment_cer_version = 0 and cer_investment_project = " . $project['project_id'];
		
		$result = mysql_query($sql_u) or dberror($sql_u);
	}


}

if($project["order_budget_is_locked"] != 1) // budget locked
{
	$order_currency = get_order_currency($project["project_order"]);
	$budget = get_project_budget_totals(param("pid"), $order_currency);
	$result = update_investments_from_project_cost_sheet(param("pid"), $budget, 0);
}

//get investment data from the list of materials
/*
if($project["order_actual_order_state_code"] < '620') // budget approved
{
	if($form_type == "CER")
	{
		$result = update_investments_from_the_list_of_materials(param("pid"), $project["project_order"], $project["order_shop_address_country"], 1);
	}
	else
	{
		//This dopy workaround is necessary since franchisees do not use the tool for entering local
		//construction works in the project
		$sql = "select cer_investment_amount_cer_loc from cer_investments " . 
			   "where cer_investment_type = 1 " . 
			   "and cer_investment_cer_version = 0 and cer_investment_project = " . param("pid");

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$local_construction_works_for_franchisee_projects = $row["cer_investment_amount_cer_loc"];
		}
		$result = update_investments_from_the_list_of_materials(param("pid"), $project["project_order"], $project["order_shop_address_country"], 1);
		$sql = "update cer_investments set cer_investment_amount_cer_loc = '" . $local_construction_works_for_franchisee_projects . "' " .
			   "where cer_investment_type = 1 " . 
			   "and cer_investment_cer_version = 0 and cer_investment_project = " . param("pid");
		mysql_query($sql) or dberror($sql);

		echo ".";

	}
}
*/


$months = array();
for($i=1;$i<13;$i++)
{
	$months[$i] = $i;
}

$years = array();

for($i=$cer_basicdata["cer_basicdata_firstyear"];$i<=$cer_basicdata["cer_basicdata_lastyear"];$i++)
{
	$years[$i] = $i;
}

//get investments

$keymoney = get_pos_intangibles(param("pid"), 15);
$goodwill = get_pos_intangibles(param("pid"), 17);
$construction = get_pos_intangibles(param("pid"), 1);
$fixturing = get_pos_intangibles(param("pid"), 3);
$architectural = get_pos_intangibles(param("pid"), 5);
$equipment = get_pos_intangibles(param("pid"), 7);
$deposit = get_pos_intangibles(param("pid"), 9);
$other1 = get_pos_intangibles(param("pid"), 11);
$merchandising = get_pos_intangibles(param("pid"), 18);
$transportation = get_pos_intangibles(param("pid"), 19);
$other2 = get_pos_intangibles(param("pid"), 13);


$amounts = array();
$comments = array();
$depryears = array();
$deprmonths = array();
$cer_totals = 0;

$sql = "select * from cer_investments " .
       "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
	   "where cer_investment_cer_version = 0 and posinvestment_type_intangible = 0 and cer_investment_project = " . param("pid");

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	if($row["cer_investment_type"] == 1) //local construction work
	{
		$local_construction_works_for_franchisee_projects = $row["cer_investment_amount_cer_loc"];
	}
	$amounts[$row["cer_investment_id"]] = $row["cer_investment_amount_cer_loc"];
	$comments[$row["cer_investment_id"]] = $row["cer_investment_comment"];
	$depryears[$row["cer_investment_id"]] = $row["cer_investment_depr_years"];
	$deprmonths[$row["cer_investment_id"]] = $row["cer_investment_depr_months"];
	$cer_totals = $cer_totals + $row["cer_investment_amount_cer_loc"];
}

//get intangibles
$total_intagibles = 0;
$sql_insc = "select * from cer_investments " . 
			"left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
			"where cer_investment_cer_version = 0 and posinvestment_type_intangible =1 and cer_investment_project = " . param("pid");

$res = mysql_query($sql_insc) or dberror($sql_insc);
while ($row = mysql_fetch_assoc($res))
{
	$total_intagibles = $total_intagibles + $row["cer_investment_amount_cer_loc"];
}


$sql_list = "select * from cer_investments " .
            "left join posinvestment_types on posinvestment_type_id = cer_investment_type "; 
$list_filter = "cer_investment_cer_version = 0 and cer_investment_project = " . param("pid") . " and posinvestment_type_intangible = 0";

/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

include("include/project_head.php");

$form->add_hidden("pid", param("pid"));


$form->add_section("Business Plan Period");
$tmp = $cer_basicdata["cer_basicdata_firstyear"] . "-" . $cer_basicdata["cer_basicdata_firstmonth"] . " / " .
$cer_basicdata["cer_basicdata_lastyear"] . "-" . $cer_basicdata["cer_basicdata_lastmonth"] . " / " . $business_plan_period;
$form->add_label("business_plan_period", "From / To / Duration", 0, $tmp);


if(count($posleases) > 0)
{
	$form->add_section("Lease Period");
	$form->add_label("l_startdate", "Start Date", NOTNULL, to_system_date($posleases["poslease_startdate"]), TYPE_DATE);
	$form->add_label("l_enddate", "Expiry Date", NOTNULL, to_system_date($posleases["poslease_enddate"]), TYPE_DATE);
	$form->add_label("rental_duration", "Rental Duration", 0, $posleases["rental_duration"]);
}
$form->add_section("Total Investments in " . $currency["symbol"]);
$form->add_label("total_intangible", "Key Money and Goodwill in " . $currency["symbol"], 0, number_format($total_intagibles,2, ".", "'"));
$form->add_label("total_investment", "Investments in " . $currency["symbol"], 0, number_format($cer_totals,2, ".", "'"));
$form->add_label("total", "Total in " . $currency["symbol"], 0, number_format($cer_totals + $total_intagibles,2, ".", "'"));


if(($cer_basicdata["cer_basicdata_cer_locked"] == 0 and  has_access("has_access_to_his_cer")) or has_access("has_full_access_to_cer"))
{
	
	$form->add_section("Beginning of the Depreciation Period");
	$form->add_comment("Please use the month and the year of the agreed opening date.");
	$form->add_list("cer_basicdata_firstyear_depr", "First Year of Depreciation Period*", $years, NOTNULL, $cer_basicdata["cer_basicdata_firstyear_depr"]);
	$form->add_list("cer_basicdata_firstmonth_depr", "First Month of Depreciation Period*", $months, NOTNULL, $cer_basicdata["cer_basicdata_firstmonth_depr"]);

	
	$form->add_section("Key Money and Goodwill in " . $currency["symbol"]);
	$form->add_edit("keymoney", $keymoney["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $keymoney["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
	$form->add_edit("goodwill", $goodwill["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $goodwill["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
	$form->add_edit("keymoney_depr_years", "Depreciation Period in full Years", 0, $keymoney["cer_investment_depr_years"], TYPE_INT, 4);
	$form->add_list("keymoney_depr_months", "Depreciation Period, additional Months", $months, 0, $keymoney["cer_investment_depr_months"]);

	$form->add_edit("cer_basicdata_recoverable_keymoney", "Recoverable Keymoney in " . $currency["symbol"], 0, $cer_basicdata["cer_basicdata_recoverable_keymoney"], TYPE_DECIMAL, 12, 0);

	$form->add_section("Construction, Store Fixturing and Architectural Costs in " . $currency["symbol"]);
	
	if($form_type == "CER")
	{
		$form->add_edit("construction", $construction["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $construction["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
	}
	else
	{
		$form->add_edit("construction", $construction["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $local_construction_works_for_franchisee_projects, TYPE_DECIMAL, 12, 2);
	}

	$form->add_edit("construction_depr_years", "Depreciation Period in full Years", 0, $construction["cer_investment_depr_years"], TYPE_INT, 4);
	$form->add_list("construction_depr_months", "Depreciation Period, additional Months", $months, 0, $construction["cer_investment_depr_months"]);
	
	$form->add_edit("fixturing", $fixturing["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $fixturing["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
	

	$form->add_edit("fixturing_depr_years", "Depreciation Period in full Years", 0, $fixturing["cer_investment_depr_years"], TYPE_INT, 4);
	$form->add_list("fixturing_depr_months", "Depreciation Period, additional Months", $months, 0, $fixturing["cer_investment_depr_months"]);

	$form->add_edit("architectural", $architectural["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $architectural["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
	
	$form->add_edit("architectural_depr_years", "Depreciation Period in full Years", 0, $architectural["cer_investment_depr_years"], TYPE_INT, 4);
	$form->add_list("architectural_depr_months", "Depreciation Period, additional Months", $months, 0, $architectural["cer_investment_depr_months"]);

	$form->add_section("Other Costs in " . $currency["symbol"]);
	$form->add_edit("equipment", $equipment["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $equipment["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
	$form->add_edit("equipment_depr_years", "Depreciation Period in full Years", 0, $equipment["cer_investment_depr_years"], TYPE_INT, 4);
	$form->add_list("equipment_depr_months", "Depreciation Period, additional Months", $months, 0, $equipment["cer_investment_depr_months"]);

	$form->add_comment("");
	$form->add_edit("deposit", $deposit["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $deposit["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
	$form->add_comment("");
	$form->add_edit("other1", $other1["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other1["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
	$form->add_edit("other1_depr_years", "Depreciation Period in full Years", 0, $other1["cer_investment_depr_years"], TYPE_INT, 4);
	$form->add_list("other1_depr_months", "Depreciation Period, additional Months", $months, 0, $other1["cer_investment_depr_months"]);


	$form->add_comment("");
	$form->add_edit("merchandising", $merchandising["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $merchandising["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
	$form->add_edit("merchandising_depr_years", "Depreciation Period in full Years", 0, $merchandising["cer_investment_depr_years"], TYPE_INT, 4);
	$form->add_list("merchandising_depr_months", "Depreciation Period, additional Months", $months, 0, $merchandising["cer_investment_depr_months"]);


	$form->add_comment("");
	$form->add_edit("transportation", $transportation["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $transportation["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
	$form->add_edit("transportation_depr_years", "Depreciation Period in full Years", 0, $transportation["cer_investment_depr_years"], TYPE_INT, 4);
	$form->add_list("transportation_depr_months", "Depreciation Period, additional Months", $months, 0, $transportation["cer_investment_depr_months"]);


	$form->add_comment("");
	$form->add_edit("other2", $other2["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other2["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);

	
	$form->add_section("Business Partner's Contribution");
	$form->add_edit("cer_basicdata_franchsiee_investment_share", "Business Partner's Contribution in % is", 0, $cer_basicdata["cer_basicdata_franchsiee_investment_share"], TYPE_DECIMAL, 6, 2);

	
	if($project['project_projectkind'] == 5) {
		$form->add_section("Investments to Consider in Depreciation for a Later Renovation in " . $currency["symbol"]);
		$form->add_edit("cer_basicdata_future_investment", "Future Investment in " . $currency["symbol"], 0, $cer_basicdata["cer_basicdata_future_investment"], TYPE_INT, 12);
		$form->add_list("cer_basicdata_future_depr_start_year", "First Year of Depreciation", $years, 0, $cer_basicdata["cer_basicdata_future_depr_start_year"]);
		$form->add_list("cer_basicdata_future_depr_start_month", "First Month of Depreciation", $months, 0, $cer_basicdata["cer_basicdata_future_depr_start_month"]);

		$form->add_list("cer_basicdata_future_depr_end_year", "Last Year of Depreciation", $years, 0, $cer_basicdata["cer_basicdata_future_depr_end_year"]);
		$form->add_list("cer_basicdata_future_depr_end_month", "Last Month of Depreciation", $months, 0, $cer_basicdata["cer_basicdata_future_depr_end_month"]);

	}
	else {
		$form->add_hidden("cer_basicdata_future_investment", 0);
		$form->add_hidden("cer_basicdata_future_depr_start_year", 0);
		$form->add_hidden("cer_basicdata_future_depr_start_month", 0);
		$form->add_hidden("cer_basicdata_future_depr_end_year", 0);
		$form->add_hidden("cer_basicdata_future_depr_end_month", 0);
	}

	$form->add_section("Residual Value of Fixed Assets in " . $currency["symbol"] . " (only in case of Renovation or Take Over/Renovation Projects)");

	$form->add_edit("keymoney_depr_years", "Depreciation Period in full Years", 0, $keymoney["cer_investment_depr_years"], TYPE_INT, 4);
	$form->add_edit("cer_basicdata_residual_value", "Residual Value in " . $currency["symbol"], 0, $cer_basicdata["cer_basicdata_residual_value"], TYPE_INT, 12);
	
	$form->add_edit("cer_basicdata_residual_depryears", "Depreciation Period in full Years", 0, $cer_basicdata["cer_basicdata_residual_depryears"], TYPE_INT, 4);
	$form->add_list("cer_basicdata_residual_deprmonths", "Depreciation Period, additional Months", $months, 0, $cer_basicdata["cer_basicdata_residual_deprmonths"]);
	
			
	
	
	
	$form->add_section("Residual Value of Keymoney in " . $currency["symbol"] . " (only in case of Renovation or Take Over/Renovation Projects)");

	$form->add_edit("cer_basicdata_residualkeymoney_value", "Residual Value in " . $currency["symbol"], 0, $cer_basicdata["cer_basicdata_residualkeymoney_value"], TYPE_INT, 12);

	$form->add_edit("cer_basicdata_residualkeymoney_depryears", "Depreciation Period in full Years", 0, $cer_basicdata["cer_basicdata_residualkeymoney_depryears"], TYPE_INT, 4);
	$form->add_list("cer_basicdata_residualkeymoney_deprmonths", "Depreciation Period, additional Months", $months, 0, $cer_basicdata["cer_basicdata_residualkeymoney_deprmonths"]);

	
		
	if($form_type == "CER" and has_access("has_full_access_to_cer"))
	{
		
		
		$total_klapproved = 0;
		$form->add_section("KL approved Investments in " . $currency["symbol"]);
		$form->add_label("construction_approved", $construction["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $construction["cer_investment_amount_cer_loc_approved"]);
		$total_klapproved = $construction["cer_investment_amount_cer_loc_approved"];

		$form->add_label("fixturing_approved", $fixturing["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $fixturing["cer_investment_amount_cer_loc_approved"]);
		
		$form->add_label("architectural_approved", $architectural["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $architectural["cer_investment_amount_cer_loc_approved"]);
		
		$form->add_label("equipment_approved", $equipment["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $equipment["cer_investment_amount_cer_loc_approved"]);

		$form->add_label("other1_approved", $other1["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other1["cer_investment_amount_cer_loc_approved"]);

		$form->add_label("merchandising_approved", $merchandising["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $merchandising["cer_investment_amount_cer_loc_approved"]);


		$form->add_label("transportation_approved", $transportation["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $transportation["cer_investment_amount_cer_loc_approved"]);

		$form->add_label("other1_approved", $other1["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other1["cer_investment_amount_cer_loc_approved"]);

		$total_klapproved = $total_klapproved + $fixturing["cer_investment_amount_cer_loc_approved"] + $architectural["cer_investment_amount_cer_loc_approved"] + $equipment["cer_investment_amount_cer_loc_approved"] + $other1["cer_investment_amount_cer_loc_approved"] + $merchandising["cer_investment_amount_cer_loc_approved"] + $transportation["cer_investment_amount_cer_loc_approved"];

		$form->add_label("approved_total", "Total KL approved Investments in Fixed Assets in " . $currency["symbol"], 0, number_format($total_klapproved, 2, ".", "'"));


		$form->add_label("deposit_approved", $deposit["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $deposit["cer_investment_amount_cer_loc_approved"]);

		$form->add_label("other2_approved", $other2["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other2["cer_investment_amount_cer_loc_approved"]);

		//$form->add_label("deposite", $deposit["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $deposit["cer_investment_amount_cer_loc"]);
		//$form->add_label("noncapitalized", $other2["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other2["cer_investment_amount_cer_loc"]);
		
		$total_klapproved2 = $total_klapproved + $deposit["cer_investment_amount_cer_loc_approved"] + $other2["cer_investment_amount_cer_loc_approved"];

		$form->add_label("approved_total2", "Total Project Costs KL approved amount in " . $currency["symbol"], 0, number_format($total_klapproved2, 2, ".", "'"));
			
	}
	
	$form->add_button("form_save", "Save Data");
	
	if($cer_basicdata['cer_basicdata_cer_locked'] != 1) {
		$form->add_button("recalculate", "Update Investments from Project's Budget");
	}
}
else
{
	
	$form->add_section("Beginning of the Depreciation Period");
	$form->add_label("cer_basicdata_firstyear_depr", "First Year of Depreciation Period", 0,$cer_basicdata["cer_basicdata_firstyear_depr"]);
	$form->add_label("cer_basicdata_firstmonth_depr", "First Month of Depreciation Period",0,$cer_basicdata["cer_basicdata_firstmonth_depr"]);
	
	$form->add_section("Key Money and Goodwill in " . $currency["symbol"]);
	$form->add_label("keymoney", $keymoney["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $keymoney["cer_investment_amount_cer_loc"]);
	$form->add_label("goodwill", $goodwill["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $goodwill["cer_investment_amount_cer_loc"]);
	$form->add_label("keymoney_depr_years", "Depreciation Period in full Years", 0, $keymoney["cer_investment_depr_years"]);
	$form->add_label("keymoney_depr_months", "Depreciation Period, additional Months", 0, $keymoney["cer_investment_depr_months"]);

	$form->add_label("cer_basicdata_recoverable_keymoney", "Recoverable Keymoney in " . $currency["symbol"], 0, $cer_basicdata["cer_basicdata_recoverable_keymoney"]);

	$form->add_section("Construction, Store Fixturing and Architectural Costs in " . $currency["symbol"]);
	
	$form->add_label("construction", $construction["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $construction["cer_investment_amount_cer_loc"]);
	
	$form->add_label("construction_depr_years", "Depreciation Period in full Years", 0, $construction["cer_investment_depr_years"]);
	$form->add_label("construction_depr_months", "Depreciation Period, additional Months", 0, $construction["cer_investment_depr_months"]);

	$form->add_label("fixturing", $fixturing["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $fixturing["cer_investment_amount_cer_loc"]);

	

	$form->add_label("fixturing_depr_years", "Depreciation Period in full Years", 0, $fixturing["cer_investment_depr_years"]);
	$form->add_label("fixturing_depr_months", "Depreciation Period, additional Months", 0, $fixturing["cer_investment_depr_months"]);

	$form->add_label("architectural", $architectural["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $architectural["cer_investment_amount_cer_loc"]);
	$form->add_label("architectural_depr_years", "Depreciation Period in full Years", 0, $architectural["cer_investment_depr_years"]);
	$form->add_label("architectural_depr_months", "Depreciation Period, additional Months", 0, $architectural["cer_investment_depr_months"]);

	$form->add_section("Other Costs in " . $currency["symbol"]);
	$form->add_label("equipment", $equipment["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $equipment["cer_investment_amount_cer_loc"]);
	$form->add_label("equipment_depr_years", "Depreciation Period in full Years", 0, $equipment["cer_investment_depr_years"]);
	$form->add_label("equipment_depr_months", "Depreciation Period, additional Months", 0, $equipment["cer_investment_depr_months"]);

	$form->add_comment("");
	$form->add_label("deposit", $deposit["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $deposit["cer_investment_amount_cer_loc"]);
	$form->add_comment("");
	$form->add_label("other1", $other1["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other1["cer_investment_amount_cer_loc"]);
	$form->add_label("other1_depr_years", "Depreciation Period in full Years", 0, $other1["cer_investment_depr_years"]);
	$form->add_label("other1_depr_years", "Depreciation Period, additional Months", 0, $other1["cer_investment_depr_months"]);

	$form->add_comment("");
	$form->add_label("merchandising", $merchandising["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $merchandising["cer_investment_amount_cer_loc"]);
	$form->add_label("merchandising_depr_years", "Depreciation Period in full Years", 0, $merchandising["cer_investment_depr_years"]);
	$form->add_label("merchandising_depr_years", "Depreciation Period, additional Months", 0, $merchandising["cer_investment_depr_months"]);

	$form->add_comment("");
	$form->add_label("transportation", $transportation["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $transportation["cer_investment_amount_cer_loc"]);
	$form->add_label("transportation_depr_years", "Depreciation Period in full Years", 0, $transportation["cer_investment_depr_years"]);
	$form->add_label("transportation_depr_years", "Depreciation Period, additional Months", 0, $transportation["cer_investment_depr_months"]);

	$form->add_comment("");
	$form->add_label("other2", $other2["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other2["cer_investment_amount_cer_loc"]);

	$form->add_section("Business Partner's Contribution");
	$form->add_label("cer_basicdata_franchsiee_investment_share", "Business Partner Contribution in %", 0, $cer_basicdata["cer_basicdata_franchsiee_investment_share"]);


	$form->add_section("Investments to Consider in Depreciation for a Later Renovation in " . $currency["symbol"]);
	$form->add_label("cer_basicdata_future_investment", "Future Investment in " . $currency["symbol"], 0, $cer_basicdata["cer_basicdata_future_investment"]);
	$form->add_label("cer_basicdata_future_depr_start_year", "First Year of Depreciation", 0,$cer_basicdata["cer_basicdata_future_depr_start_year"]);
	$form->add_label("cer_basicdata_future_depr_start_month", "First Month of Depreciation", 0, $cer_basicdata["cer_basicdata_future_depr_start_month"]);

	$form->add_label("cer_basicdata_future_depr_end_year", "Last Year of Depreciation", 0,$cer_basicdata["cer_basicdata_future_depr_end_year"]);
	$form->add_label("cer_basicdata_future_depr_end_month", "Last Month of Depreciation", 0, $cer_basicdata["cer_basicdata_future_depr_end_month"]);
	
	$form->add_section("Residual Value of Fixed Assets in " . $currency["symbol"] . " (only in case of Renovation or Take Over/Renovation Projects)");
	$form->add_label("cer_basicdata_residual_value", "Residual Value in " . $currency["symbol"], 0, $cer_basicdata["cer_basicdata_residual_value"]);

	$form->add_label("cer_basicdata_residual_depryears", "Depreciation Period in full Years", 0, $cer_basicdata["cer_basicdata_residual_depryears"]);
	$form->add_label("cer_basicdata_residual_deprmonths", "Depreciation Period, additional Months", 0, $cer_basicdata["cer_basicdata_residual_deprmonths"]);

	
	
	$form->add_section("Residual Value of Keymoney in " . $currency["symbol"] . " (only in case of Renovation or Take Over/Renovation Projects)");

	$form->add_label("cer_basicdata_residualkeymoney_value", "Residual Value in " . $currency["symbol"], 0, $cer_basicdata["cer_basicdata_residualkeymoney_value"]);

	$form->add_label("cer_basicdata_residualkeymoney_depryears", "Depreciation Period in full Years", 0, $cer_basicdata["cer_basicdata_residualkeymoney_depryears"]);
	$form->add_label("cer_basicdata_residualkeymoney_deprmonths", "Depreciation Period, additional Months", 0, $cer_basicdata["cer_basicdata_residualkeymoney_deprmonths"]);


}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if($form->button("recalculate"))
{
	
	$order_currency = get_order_currency($project["project_order"]);
	$budget = get_project_budget_totals(param("pid"), $order_currency);
	$result = update_investments_from_project_cost_sheet(param("pid"), $budget, 0);
	
	$link = "cer_application_investments.php?pid=" . param("pid");
	redirect($link);
}
elseif($form->button("form_save"))
{
	if($form->validate())
	{
		//save intangibles type keymoney
		$fields = array();
	
		$value = dbquote($form->value("keymoney"));
		$fields[] = "cer_investment_amount_cer_loc = " . $value;

		$value = dbquote($form->value("keymoney_depr_years"));
		$fields[] = "cer_investment_depr_years = " . $value;

		$value = dbquote($form->value("keymoney_depr_months"));
		$fields[] = "cer_investment_depr_months = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $keymoney["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		//save intangibles type goodwill
		$fields = array();
    
		$value = dbquote($form->value("goodwill"));
		$fields[] = "cer_investment_amount_cer_loc = " . $value;

		$value = dbquote($form->value("keymoney_depr_years"));
		$fields[] = "cer_investment_depr_years = " . $value;

		$value = dbquote($form->value("keymoney_depr_months"));
		$fields[] = "cer_investment_depr_months = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $goodwill["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		//save investment type construction
		$fields = array();
    
		$value = dbquote($form->value("construction"));
		$fields[] = "cer_investment_amount_cer_loc = " . $value;

		$value = dbquote($form->value("construction_depr_years"));
		$fields[] = "cer_investment_depr_years = " . $value;

		$value = dbquote($form->value("construction_depr_months"));
		$fields[] = "cer_investment_depr_months = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $construction["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		//save investment type fixturing
		$fields = array();
    
		$value = dbquote($form->value("fixturing"));
		$fields[] = "cer_investment_amount_cer_loc = " . $value;

		
		$value = dbquote($form->value("fixturing_depr_years"));
		$fields[] = "cer_investment_depr_years = " . $value;

		$value = dbquote($form->value("fixturing_depr_months"));
		$fields[] = "cer_investment_depr_months = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $fixturing["cer_investment_id"];
		mysql_query($sql) or dberror($sql);


		//save investment type architectural
		$fields = array();
    
		$value = dbquote($form->value("architectural"));
		$fields[] = "cer_investment_amount_cer_loc = " . $value;

		
		$value = dbquote($form->value("architectural_depr_years"));
		$fields[] = "cer_investment_depr_years = " . $value;

		$value = dbquote($form->value("architectural_depr_months"));
		$fields[] = "cer_investment_depr_months = " . $value;

		
		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $architectural["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		//save investment type equipment
		$fields = array();
    
		$value = dbquote($form->value("equipment"));
		$fields[] = "cer_investment_amount_cer_loc = " . $value;

		
		$value = dbquote($form->value("equipment_depr_years"));
		$fields[] = "cer_investment_depr_years = " . $value;

		$value = dbquote($form->value("equipment_depr_months"));
		$fields[] = "cer_investment_depr_months = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $equipment["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		//save investment type deposit
		$fields = array();
    
		$value = dbquote($form->value("deposit"));
		$fields[] = "cer_investment_amount_cer_loc = " . $value;


		
		$value1 = "current_timestamp";
		$fields[] = "date_modified = " .$value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $deposit["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		//save investment type other costs
		$fields = array();
    
		$value = dbquote($form->value("other1"));
		$fields[] = "cer_investment_amount_cer_loc = " . $value;

		
		$value = dbquote($form->value("other1_depr_years"));
		$fields[] = "cer_investment_depr_years = " . $value;

		$value = dbquote($form->value("other1_depr_months"));
		$fields[] = "cer_investment_depr_months = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $other1["cer_investment_id"];
		mysql_query($sql) or dberror($sql);


		//save investment type merchandising costs
		$fields = array();
    
		$value = dbquote($form->value("merchandising"));
		$fields[] = "cer_investment_amount_cer_loc = " . $value;

		
		$value = dbquote($form->value("merchandising_depr_years"));
		$fields[] = "cer_investment_depr_years = " . $value;

		$value = dbquote($form->value("merchandising_depr_months"));
		$fields[] = "cer_investment_depr_months = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $merchandising["cer_investment_id"];
		mysql_query($sql) or dberror($sql);


		//save investment type transportation costs
		$fields = array();
    
		$value = dbquote($form->value("transportation"));
		$fields[] = "cer_investment_amount_cer_loc = " . $value;

		
		$value = dbquote($form->value("transportation_depr_years"));
		$fields[] = "cer_investment_depr_years = " . $value;

		$value = dbquote($form->value("transportation_depr_months"));
		$fields[] = "cer_investment_depr_months = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $transportation["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		//save investment type other non captalized costs
		$fields = array();
    
		$value = dbquote($form->value("other2"));
		$fields[] = "cer_investment_amount_cer_loc = " . $value;


		
		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $other2["cer_investment_id"];
		mysql_query($sql) or dberror($sql);


		//update cer_basic_data
		$fields = array();
    
		$value = dbquote($form->value("cer_basicdata_residual_value"));
		$fields[] = "cer_basicdata_residual_value = " . $value;

		$value = dbquote($form->value("cer_basicdata_firstyear_depr"));
		$fields[] = "cer_basicdata_firstyear_depr = " . $value;

		$value = dbquote($form->value("cer_basicdata_firstmonth_depr"));
		$fields[] = "cer_basicdata_firstmonth_depr = " . $value;
		
		$value = dbquote($form->value("cer_basicdata_residual_depryears"));
		$fields[] = "cer_basicdata_residual_depryears = " . $value;

		$value = dbquote($form->value("cer_basicdata_residual_deprmonths"));
		$fields[] = "cer_basicdata_residual_deprmonths = " . $value;

		$value = dbquote($form->value("cer_basicdata_residualkeymoney_value"));
		$fields[] = "cer_basicdata_residualkeymoney_value = " . $value;

		$value = dbquote($form->value("cer_basicdata_residualkeymoney_depryears"));
		$fields[] = "cer_basicdata_residualkeymoney_depryears = " . $value;

		$value = dbquote($form->value("cer_basicdata_residualkeymoney_deprmonths"));
		$fields[] = "cer_basicdata_residualkeymoney_deprmonths = " . $value;

		$value = dbquote($form->value("cer_basicdata_recoverable_keymoney"));
		$fields[] = "cer_basicdata_recoverable_keymoney = " . $value;

		$value = dbquote($form->value("cer_basicdata_franchsiee_investment_share"));
		$fields[] = "cer_basicdata_franchsiee_investment_share = " . $value;

		$value = dbquote($form->value("cer_basicdata_future_investment"));
		$fields[] = "cer_basicdata_future_investment = " . $value;

		$value = dbquote($form->value("cer_basicdata_future_depr_start_year"));
		$fields[] = "cer_basicdata_future_depr_start_year = " . $value;

		$value = dbquote($form->value("cer_basicdata_future_depr_start_month"));
		$fields[] = "cer_basicdata_future_depr_start_month = " . $value;
		
		$value = dbquote($form->value("cer_basicdata_future_depr_end_year"));
		$fields[] = "cer_basicdata_future_depr_end_year = " . $value;

		$value = dbquote($form->value("cer_basicdata_future_depr_end_month"));
		$fields[] = "cer_basicdata_future_depr_end_month = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " .$value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_basicdata set " . join(", ", $fields) . " where cer_basicdata_version = 0 and cer_basicdata_project = " . param("pid");
		mysql_query($sql) or dberror($sql);

		$sql = "update projects set " . 
			 "project_share_other = " . dbquote($form->value("cer_basicdata_franchsiee_investment_share")) . ", " .
			 "date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", " . 
			 "user_modified = " . dbquote(user_login()) . 
			 " where project_id = " . dbquote(param("pid"));

		$result = mysql_query($sql) or dberror($sql);


		$result = update_posorder_investments($project["project_id"], $project["project_order"]);


		$form->message("Your data has bee saved.");
	}
}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();

if($form_type == "INR03")
{
	$page->title("INR-03 - Retail Furniture in Third-party Store: Investment Information");
}
elseif($form_type == "AF")
{
	$page->title("Application Form: Investment Information");
}
else
{
	$page->title("Capital Expenditure Request: Investment Information");
}
require_once("include/tabs.php");
$form->render();

require "include/footer_scripts.php";
$page->footer();

?>