<?php
/********************************************************************

    cer_download.php

    Creation and mutation of downloads

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-03-29
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-03-29
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("cer_can_edit_downloads");

$form = new Form("cer_downloads", "Downloads");

$form->add_section();
$form->add_edit("cer_download_title", "Title", NOTNULL);
$form->add_upload("cer_download_path", "File", "/files/links");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("downloads");
$page->header();
$page->title(id() ? "Edit Download" : "Add Download");
$form->render();
$page->footer();

?>