<?php
/********************************************************************

    cer_booklet_pdf.php

    Print PDF CER all Forms.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-02-20
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-02-20
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();

$SUPPRESS_HEADERS = true;
define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 



require "../include/frame.php";


$cer_version = 0;
$ln_version = 0;
if(param("cerversion"))
{
	$cer_version = param("cerversion");
}
if(param("lnversion"))
{
	$ln_version = param("lnversion");
}


require "include/get_functions.php";
require "include/get_project.php";
require "include/financial_functions.php";
require "../shared/func_posindex.php";
require_once "../shared/project_cost_functions.php";

check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');
require_once('../include/fpdi/fpdi.php');
require_once('../include/SetaPDF/Autoload.php');


$order_number = $project["project_order"];

//save actual cer version temporarily
$tmp_cer_version = $cer_version;

//get data of latest ln version
if($tmp_cer_version > 0)
{
	$sql = "select cer_basicdata_version from cer_basicdata where cer_basicdata_project = " . dbquote(param("pid")) . " and cer_basicdata_version < " . $tmp_cer_version . " and cer_basicdata_version_context = 'ln' order by cer_basicdata_version desc";
}
else
{
	$sql = "select cer_basicdata_version from cer_basicdata where cer_basicdata_project = " . dbquote(param("pid")) . " and cer_basicdata_version_context = 'ln' order by cer_basicdata_version desc";
}

$res = mysql_query($sql) or dberror($sql);

if ($row = mysql_fetch_assoc($res))
{
	$cer_version = $row["cer_basicdata_version"];

	include("include/in_financial_data.php");
	$ln_amounts = $amounts;
	$ln_exchange_rate = $cer_basicdata["cer_basicdata_exchangerate"];
	$ln_exchange_rate_factor = $cer_basicdata["cer_basicdata_factor"];
	$ln_investment_total = $investment_total;
	$ln_intagible_amount = $intagible_amount;
	$ln_totals = $cer_totals;
	$ln_sales_units_watches_values = $sales_units_watches_values;
	$ln_sales_units_jewellery_values = $sales_units_jewellery_values;
	$ln_sales_accessories_values = $sales_accessories_values;
	$ln_sales_customer_service_values = $sales_customer_service_values;
	$ln_total_net_sales_values = $total_net_sales_values;
	$ln_operating_income02_values = $operating_income02_values;
	$ln_operating_income02_80_percent_values = $operating_income02_80_percent_values;

	$ln_number_of_months_first_year = $number_of_months_first_year;
	$ln_number_of_months_last_year = $number_of_months_last_year;
	$ln_wholesale_margin = $weighted_average_whole_sale_margin;

}
elseif(isset($number_of_months_first_year))
{
	$ln_amounts = array();
	$ln_exchange_rate = $cer_basicdata["cer_basicdata_exchangerate"];
	$ln_exchange_rate_factor = $cer_basicdata["cer_basicdata_factor"];
	$ln_investment_total = 0;
	$ln_intagible_amount = 0;
	$ln_totals =0;
	$ln_sales_units_watches_values = array();
	$ln_sales_units_jewellery_values = array();
	$ln_sales_accessories_values = array();
	$ln_sales_customer_service_values = array();
	$ln_total_net_sales_values = array();
	$ln_operating_income02_values = array();
	$ln_operating_income02_80_percent_values = array();

	$ln_number_of_months_first_year = $number_of_months_first_year;
	$ln_number_of_months_last_year = $number_of_months_last_year;
	$ln_wholesale_margin = "";
}
else
{
	$ln_amounts = array();
	$ln_exchange_rate = 0;
	$ln_exchange_rate_factor = 0;
	$ln_investment_total = 0;
	$ln_intagible_amount = 0;
	$ln_totals =0;
	$ln_sales_units_watches_values = array();
	$ln_sales_units_jewellery_values = array();
	$ln_sales_accessories_values = array();
	$ln_sales_customer_service_values = array();
	$ln_total_net_sales_values = array();
	$ln_operating_income02_values = array();
	$ln_operating_income02_80_percent_values = array();

	$ln_number_of_months_first_year = 0;
	$ln_number_of_months_last_year = 0;
	$ln_wholesale_margin = "";
}


//restore cer_version
$cer_version = $tmp_cer_version;
$cer_basicdata = get_cer_basicdata(param("pid"), $cer_version);
$exchange_rate = $cer_basicdata["cer_basicdata_exchangerate"];
$exchange_rate_factor = $cer_basicdata["cer_basicdata_factor"];
include("include/in_financial_data.php");


//get currencies
$currencies = array();
$sql = 'select currency_id, currency_symbol from currencies where currency_system = 1';

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$currencies["s"] = $row['currency_symbol'];
}


if($cer_basicdata['cer_basicdata_currency']) {
	$sql = 'select currency_id, currency_symbol from currencies where currency_id = ' . $cer_basicdata['cer_basicdata_currency'];

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res) and !in_array($row['currency_symbol'], $currencies))
	{
		$currencies["c1"] = $row['currency_symbol'];
	}
}

if($cer_basicdata['cer_basicdata_currency2']) {
	$sql = 'select currency_id, currency_symbol from currencies where currency_id = ' . $cer_basicdata['cer_basicdata_currency2'];

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res) and !in_array($row['currency_symbol'], $currencies))
	{
		$currencies["c2"] = $row['currency_symbol'];
	}
}


//get signed sheet
$cer_signatures = "";

$sql_sheets = "select cer_summary_cer_coversheet " .
       "from cer_summary ". 
	   " where cer_summary_cer_version = 0 and cer_summary_project = " . param("pid");

$res_sheets = mysql_query($sql_sheets) or dberror($sql_sheets);
if ($row_ssheets = mysql_fetch_assoc($res_sheets))
{
	$cer_signatures = $row_ssheets["cer_summary_cer_coversheet"];
}


require("include/cer_get_data_from_project.php");


// Create and setup PDF document

$pdf = new FPDI("P", "mm", "A4", true, 'UTF-8');
$pdf->setPrintHeader(false);


$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');

$pdf->Open();



//financial summary
if($cer_signatures)
{	
	$source_file = ".." . $cer_signatures;
	if(file_exists($source_file))
	{
		
		$layout = new FPDI();
		$num_of_pages = $pdf->setSourceFile($source_file);
		$num_of_pages = $layout->setSourceFile($source_file);
		for($i=1;$i<=$num_of_pages;$i++)
		{
			$tplIdx = $pdf->importPage($i);
			$layout->AddPage("L");
			$tplIdx = $layout->importPage($i);
			
			$tpl = $layout->useTemplate($tplIdx);
			//$pdf->AddPage("", array(0=>$tpl["w"], 1=>$tpl["h"]));

			if($tpl["w"] > $tpl["h"])
			{
				$pdf->AddPage("L", array(0=>$tpl["h"], 1=>$tpl["w"]));
			}
			else
			{
				$pdf->AddPage("P", array(0=>$tpl["w"], 1=>$tpl["h"]));
			}
			$tpl = $pdf->useTemplate($tplIdx);
		}
	}
}
else
{
	include("cer_in01_pdf_detail.php");
}


include("cer_in02A_detail_pdf.php");
include("cer_in02B_detail_pdf.php");

//LN versus CER
$ln_exchange_rate = 1;
$ln_exchange_rate_factor = 1;
include("cer_inr02c_pdf_detail.php");


//former lease conditions
if($project['project_projectkind'] == 5
		or $project['project_projectkind'] == 2
		or $project['project_projectkind'] == 3
		or $project['project_projectkind'] == 6) {
		include("ln_former_lease_conditions_pdf_detail.php");
}

//business plan in client's currency
include("cer_inr02_pdf_detail.php");

//business plan in system'currency
if(array_key_exists("s", $currencies) and array_key_exists("c1", $currencies) and $currencies["s"] != $currencies["c1"])
{
	$cid = 's';
	include("cer_inr02_pdf_detail.php");
}

//business plan in pos'currency
if(array_key_exists("c2", $currencies) and array_key_exists("c1", $currencies) and $currencies["c2"] != $currencies["c1"])
{
	$cid = 'c2';
	include("cer_inr02_pdf_detail.php");
}


//brand details
if(count($cer_brands) > 1)
{
	include("cer_inr02_brand_detail_pdf.php");
}


//project budget in local currency

include("../user/project_costs_budget_detail_pdf.php");


//project budget in CHF
if(isset($order_currency) and $order_currency["symbol"] != 'CHF')
{
	$sc = 1;
	include("../user/project_costs_budget_detail_pdf.php");
}


//project layout attachment pdf
$sql = "select order_file_path " . 
       "from projects " . 
	   "left join order_files on order_file_order = project_order " . 
       "where order_file_category = 8 and order_file_type= 13 and project_id = " .param("pid") . 
	   " order by order_files.date_created DESC";

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$source_file = ".." . $row["order_file_path"];
	if(file_exists($source_file))
	{
		
		$layout = new FPDI();
		$num_of_pages = $pdf->setSourceFile($source_file);
		$num_of_pages = $layout->setSourceFile($source_file);
		for($i=1;$i<=$num_of_pages;$i++)
		{
			$tplIdx = $pdf->importPage($i);
			$layout->AddPage("L");
			$tplIdx = $layout->importPage($i);
			
			$tpl = $layout->useTemplate($tplIdx);
			//$pdf->AddPage("", array(0=>$tpl["w"], 1=>$tpl["h"]));

			if($tpl["w"] > $tpl["h"])
			{
				$pdf->AddPage("L", array(0=>$tpl["h"], 1=>$tpl["w"]));
			}
			else
			{
				$pdf->AddPage("P", array(0=>$tpl["w"], 1=>$tpl["h"]));
			}

			/*
			if($tpl["w"] > $tpl["h"])
			{
				$pdf->AddPage("L", array(0=>$tpl["w"], 1=>$tpl["h"]));
			}
			else
			{
				$pdf->AddPage("P", array(0=>$tpl["w"], 1=>$tpl["h"]));
			}
			*/
			$tpl = $pdf->useTemplate($tplIdx);
		}
	}
}
else // layout as JPEG
{

	$sql = "select order_file_path " . 
		   "from projects " . 
		   "left join order_files on order_file_order = project_order " . 
		   "where order_file_category = 8 and order_file_type= 2 and project_id = " .param("pid") . 
		   " order by order_files.date_created DESC";

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$source_file = ".." . $row["order_file_path"];
		if(file_exists($source_file))
		{
			$pdf->AddPage("L");
			$margin_top = 16;
			$margin_left = 12;
			$pdf->SetXY($margin_left,$margin_top);

			$pdf->SetFont("arialn", "I", 9);
			$pdf->Cell(45, 10, "", 1);

			$pdf->Image('../pictures/logo.jpg',13,$margin_top + 1,22);

			$pdf->SetFont("arialn", "B", 11);
			$pdf->Cell(172, 10, "Layout: " . $project_name, 1, "", "L");
			$pdf->Cell(20, 10, date("d.m.Y"), 1, "", "C");

			$pdf->Image($source_file,20,40, 200);
		}
	}
}


//quotes for local works
//include("../user/project_offer_comparison_pdf_main.php");
include("../user/project_costs_bid_comparison_pdf_main.php");

//list of variables
include("cer_listofvars_pdf_detail.php");



//check if project has jpg attachments to be added to the booklet
$sql = "select order_file_path " . 
	   "from projects " . 
	   "left join order_files on order_file_order = project_order " . 
	   "where order_file_attach_to_cer_booklet = 1 and order_file_type= 2 and project_id = " .param("pid") . 
	   " order by order_files.date_created DESC";

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	
	
	$source_file = ".." . $row["order_file_path"];
	if(file_exists($source_file))
	{
		$pdf->AddPage("L");
		$margin_top = 16;
		$margin_left = 12;
		$pdf->SetXY($margin_left,$margin_top);

		/*
		$pdf->SetFont("arialn", "I", 9);
		$pdf->Cell(45, 10, "", 1);

		$pdf->Image('../pictures/brand_logo.jpg',13,$margin_top + 1,22);

		$pdf->SetFont("arialn", "B", 11);
		$pdf->Cell(172, 10, "Layout: " . $project_name, 1, "", "L");
		$pdf->Cell(20, 10, date("d.m.Y"), 1, "", "C");
		*/

		$pdf->Image($source_file,20,40, 200);
	}
}


//check if project has pdf attachments to be added to the booklet
$PDFmerger_was_used = false;
$pdf_print_output = true;

$sql = "select order_file_path " . 
       "from projects " . 
	   "left join order_files on order_file_order = project_order " . 
       "where order_file_attach_to_cer_booklet = 1 and order_file_type = 13 and project_id = " .param("pid") . 
	   " order by order_files.date_created DESC";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{

	$source_file = ".." . $row["order_file_path"];	if(file_exists($source_file))
	{
		
		if($pdf_print_output == true)
		{
			$pdfString = $pdf->output('', 'S');
			$tmp = SetaPDF_Core_Document::loadByString($pdfString);
			
			if($PDFmerger_was_used == false)
			{
				$merger = new SetaPDF_Merger();
			}

			$merger->addDocument($tmp);
			$pdf_print_output = false;
		}

		$merger->addFile(array(
			'filename' => $source_file,
			'copyLayers' => true
		));


		
		$PDFmerger_was_used = true;
		$pdf_print_output = false;

	}
	
}

$file_name = BRAND . "_CER_Booklet_" . str_replace(" ", "_", $project_name) . "_" . date("Y-m-d") . ".pdf";
if($PDFmerger_was_used == true)
{

	if($pdf_print_output == false)
	{
		$merger->merge();
		$document = $merger->getDocument();

		$document->setWriter(new SetaPDF_Core_Writer_Http($file_name, true));
		$document->save()->finish();
	}
	else
	{
	
		$pdfString = $pdf->output('', 'S');
		$tmp = SetaPDF_Core_Document::loadByString($pdfString);
		$merger->addDocument($tmp);
		
		$merger->merge();
		$document = $merger->getDocument();
		$document->setWriter(new SetaPDF_Core_Writer_Http($file_name, true));
		$document->save()->finish();
	}
}
else
{
	$pdf->Output($file_name, 'I');
}
?>