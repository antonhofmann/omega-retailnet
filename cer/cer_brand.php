<?php
/********************************************************************

    cer_brand.php

    Creation and mutation of brands involved in CER.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-11-22
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-11-22
    Version:        1.0.0

    Copyright (c) 2014 Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_full_access_to_cer");

/********************************************************************
    build form 
*********************************************************************/
$form = new Form("cer_brands", "Brand");


$form->add_edit("cer_brand_name", "Brand Name", NOTNULL);
$form->add_checkbox("cer_brand_active", "In use", "", 0, "Active");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();
$form->process();

if($form->button(FORM_BUTTON_SAVE)) {
	redirect("cer_brands.php");
}

$page = new Page("cer_brands");

$page->header();
$page->title("Edit Brand Involved in CER");
$form->render();
$page->footer();

?>
