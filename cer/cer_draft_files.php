<?php
/********************************************************************

    cer_draft_files.php

    Business Plan Draft: Attachments

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2011-03-20
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2011-03-20
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require "include/get_draft_functions.php";

check_access("has_access_to_cer_drafts");

set_referer("cer_draft_file.php");

$title = "";

if(param('did')) {
	param('id', param('did'));

	$basicdata = get_draft_basicdata(param("did"));
	$title = $basicdata["cer_basicdata_title"];
}

/********************************************************************
    prepare all data needed
*********************************************************************/
register_param("did");

$files = array();
$sql_files = "select cer_draft_file_id, cer_draft_file_path from cer_draft_files where cer_draft_file_draft = " . param("did");
$res = mysql_query($sql_files) or dberror($sql_files);

while ($row = mysql_fetch_assoc($res))
{
	
	$link = "http://" . $_SERVER["HTTP_HOST"] . "/" . $row["cer_draft_file_path"];

	
	$link = "<a href=\"" . $link. "\" target=\"_blank\"><img style=\"padding-top:3px;\"src=\"/pictures/view.gif\" border='0'/></a>";
	$files[$row["cer_draft_file_id"]] = $link;
}

/********************************************************************
    list
*********************************************************************/
// create sql
$sql = "select cer_draft_file_id, cer_draft_file_title " .
       "from cer_draft_files ";

$list_filter = "cer_draft_file_draft = " . param("did");

/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql, LIST_HAS_HEADER | LIST_HAS_FOOTER);
$list->set_title("Files");
$list->set_entity("cer_draft_files");
$list->set_order("cer_draft_file_title");
$list->set_filter($list_filter);   

$link = "cer_draft_file.php?did=" . param("did");

$list->add_hidden("did", param("did"));
$list->add_text_column("file", "", COLUMN_UNDERSTAND_HTML, $files);

$list->add_column("cer_draft_file_title", "Title", $link, "", "", COLUMN_NO_WRAP);

$list->add_button("new", "Add New File", $link);



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();

if($list->button("new"))
{
	redirect($link);
}


/********************************************************************
    create page
*********************************************************************/
$page = new Page("cer_drafts");
require "include/draft_page_actions.php";
$page->header();

$page->title($title . ": Files");

if(id() > 0) {
	require_once("include/tabs_draft.php");
}


$list->render();

require "include/draft_footer_scripts.php";
$page->footer();
?>