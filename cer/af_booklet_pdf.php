<?php
/********************************************************************

    af_booklet_pdf.php

    Print PDF AF all Forms.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-02-20
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-02-20
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();

$SUPPRESS_HEADERS = true;
define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 

require "../include/frame.php";
$cer_version = 0;
$ln_version = 0;
if(param("cerversion"))
{
	$cer_version = param("cerversion");
}
if(param("lnversion"))
{
	$ln_version = param("lnversion");
}


require "include/get_functions.php";
require "../shared/func_posindex.php";
require "include/financial_functions.php";
require "include/get_project.php";
require_once "../shared/project_cost_functions.php";



check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');
require_once('../include/fpdi/fpdi.php');
require_once('../include/SetaPDF/Autoload.php');





$key_points_text = $cer_basicdata["cer_summary_in01_description"];

$currencies = array();
$sql = 'select currency_id, currency_symbol from currencies where currency_system = 1';

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$currencies["s"] = $row['currency_symbol'];
}


if($cer_basicdata['cer_basicdata_currency']) {
	$sql = 'select currency_id, currency_symbol from currencies where currency_id = ' . $cer_basicdata['cer_basicdata_currency'];

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res) and !in_array($row['currency_symbol'], $currencies))
	{
		$currencies["c1"] = $row['currency_symbol'];
	}
}

if($cer_basicdata['cer_basicdata_currency2']) {
	$sql = 'select currency_id, currency_symbol from currencies where currency_id = ' . $cer_basicdata['cer_basicdata_currency2'];

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res) and !in_array($row['currency_symbol'], $currencies))
	{
		$currencies["c2"] = $row['currency_symbol'];
	}
}

$rental_conditions = '';

// get all data needed from project
$sql = "select * " . 
       "from projects " . 
	   "left join orders on order_id = project_order " .
	   "left join countries on country_id = order_shop_address_country " .
	   "left join product_lines on product_line_id = project_product_line " .
	   "left join productline_subclasses on productline_subclass_id = project_product_line_subclass " .
	   "left join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join projectkinds on projectkind_id = project_projectkind " .
       "left join addresses on address_id = order_client_address " .
	   "left join currencies on currency_id = country_currency " . 
       "left join postypes on postype_id = project_postype ".
	   "left join ln_basicdata on ln_basicdata_project = project_id ".
	   "where ln_basicdata_version = " . $ln_version . " and project_id = " . param("pid");


$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	$order_number = $row["project_order"];
	//POS Basic Data
	$posname = $row["order_shop_address_company"];

	if($row["order_shop_address_company2"])
    {
		$posname .= ", " . $row["order_shop_address_company2"];
	}

	
	$posaddress = $row["country_name"];

	if($row["order_shop_address_zip"])
    {
		$posaddress .= ", " . $row["order_shop_address_zip"] . " " . $row["order_shop_address_place"];
	}
	else
	{
		$posaddress .= ", " . $row["order_shop_address_place"];
	}

	$posaddress .= ", " . $row["order_shop_address_address"];
	
	if($row["order_shop_address_address2"])
    {
		$posaddress .= ", " . $row["order_shop_address_address2"];
	}

	if($row["productline_subclass_name"])
	{
		$postype = $row["projectkind_name"] . " - " . $row["project_costtype_text"] . " " . $row["postype_name"] . " - " . $row["product_line_name"] . " / " . $row["productline_subclass_name"];
	}
	else {
		$postype = $row["projectkind_name"] . " - " . $row["project_costtype_text"] . " " . $row["postype_name"] . " - " . $row["product_line_name"];
	}
	
	$planned_opening_date = to_system_date($row["project_real_opening_date"]);
	$sales_surface = $row["project_cost_sqms"];
	$total_surface = $row["project_cost_totalsqms"];
	$gross_surface = $row["project_cost_gross_sqms"];
	$other_surface = $row["project_cost_othersqms"] + $row["project_cost_backofficesqms"];


		
	$pix1 = ".." . $row["ln_basicdata_pix1"];
	$pix2 = ".." . $row["ln_basicdata_pix2"];
	$pix3 = ".." . $row["ln_basicdata_pix3"];

	$floor_plan = ".." . $row["ln_basicdata_floorplan"];
	
	
	
	
	
	//get rental conditions
	$posdata = get_pos_data($row["order_id"]);

	$leasedata = get_cer_lease_data($project["project_id"], $cer_basicdata["cer_basicdata_id"], $project["pipeline"], $cer_version);

	
	if(isset($leasedata["poslease_negotiated_conditions"]))
	{
		$rental_conditions = $leasedata["poslease_negotiated_conditions"];
	}


	//surfaces
	$surfaces = "Gross Surface sqms: " . $row["project_cost_gross_sqms"] . " / Total Surface sqms: " . $row["project_cost_totalsqms"] . " / Sales Surface sqms: " . $row["project_cost_sqms"];
	
	
	//neighbourhood
	if($project["pipeline"] == 0)
	{
		$pos_order_sql = "select * from posorders where posorder_order = " . $project["order_id"];
	}
	elseif($project["pipeline"] == 1)
	{
		$pos_order_sql = "select * from posorderspipeline where posorder_order = " . $project["order_id"];
	}
	
	$pos_type = 0;
	$neighbourhood = "";
	$neighbourhood_indicated = 0;
	$res = mysql_query($pos_order_sql) or dberror($pos_order_sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$neighbourhood = "Shop on Left Side: " . $row["posorder_neighbour_left"] . 
		                 " / Right Side: " . $row["posorder_neighbour_right"] . 
		                 " / Across Left: " . $row["posorder_neighbour_acrleft"] . 
		                 " / Across Right: " . $row["posorder_neighbour_acrright"] . "\r\n" . 
						 "Other Brands in Area: " . $row["posorder_neighbour_brands"];
		
		if($row["posorder_neighbour_left"]
		   and $row["posorder_neighbour_right"]
		   and $row["posorder_neighbour_acrleft"]
		   and $row["posorder_neighbour_acrright"]
		   and $row["posorder_neighbour_brands"])
		   {
		   		$neighbourhood_indicated = 1;	
		   }
		$pos_type = $row["posorder_postype"];
	}
	

	

}
else
{
	exit;
}



include("include/in_financial_data.php");	



// Create and setup PDF document

$pdf = new FPDI("P", "mm", "A4");
$pdf->setPrintHeader(false);


$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');

$pdf->Open();

//investmen approval form
include("af_inr03_pdf_detail.php");


//key points
$page_title = "Key Points";
//if($key_points_text)
//{
	$pdf->AddPage();
	$new_page = 0;


	//Logo
	$pdf->Image('../pictures/logo.jpg',10,8,33);
	//arialn bold 15
	$pdf->SetFont('arialn','B',18);
	//Move to the right
	$pdf->Cell(80);
	//Title
	$pdf->SetY(0);
	$pdf->Cell(0,33,$page_title,0,0,'R');
	//Line break
	$pdf->SetY(23);


	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190,7,$posname,1, 0, 'L', 1);
	$pdf->Ln();
	$pdf->SetFont('arialn','',10);
	$pdf->Cell(190,7,$posaddress,1, 0, 'L', 0);
	$pdf->Ln();
	$pdf->SetFont('arialn','',10);
	$pdf->Cell(190,7,$postype,1, 0, 'L', 0);
	$pdf->Ln();

	$x = $pdf->GetX();
	$y = $pdf->GetY() + 2;

	
	//picture 1
	if($pix1 != ".." and file_exists($pix1))
	{
		if(substr($pix1, strlen($pix1)-3, 3) == "jpg" or substr($pix1, strlen($pix1)-3, 3) == "JPG")
		{
			$imagesize = getimagesize($pix1);
			$w = $imagesize[0];
			$h = $imagesize[1];

			$imgratio=$w/$h;

			if ($imgratio>1)
			{
				if($w >= 190)
				{
					$scale_factor = 190/$w;
					$h = $h*$scale_factor;
					$w = $w*$scale_factor;
					$pdf->Image($pix1,$x,$y, $w, $h);
					$y1 = $h;
				}
				else
				{
					$pdf->Image($pix1,$x,$y);
					$y1 = ($h / 72) * 25.4;
				}
				
			}
			else
			{
				if($w >= 142)
				{
					$scale_factor = 142/$h;
					$h = $h*$scale_factor;
					$w = $w*$scale_factor;
					$pdf->Image($pix1,$x,$y, $w, $h);
					$y1 = $h;
				}
				else
				{
					$pdf->Image($pix1,$x,$y);
					$y1 = ($h / 72) * 25.4;
				}
				
			}

			$pdf->SetY($y1 + $pdf->GetY() + 5);
		}
	}


	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190,7,"Key points",1, 0, 'L', 1);
	$pdf->Ln();
	$pdf->SetFont('arialn','',10);
	$pdf->MultiCell(190,3.5, $key_points_text, 1, "T");

	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190,7,"Negotiated rental conditions",1, 0, 'L', 1);
	$pdf->Ln();
	$pdf->SetFont('arialn','',10);
	$pdf->MultiCell(190,3.5, $rental_conditions, 1, "T");


	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190,7,"Surfaces",1, 0, 'L', 1);
	$pdf->Ln();
	$pdf->SetFont('arialn','',10);
	$pdf->MultiCell(190,3.5, $surfaces, 1, "T");
	
	if($neighbourhood_indicated == 1 and ($pos_type == 1 or $pos_type == 3)) // store or kiosk
	{
		$pdf->SetFont('arialn','B',10);
		$pdf->Cell(190,7,"Neighbourhood",1, 0, 'L', 1);
		$pdf->Ln();
		$pdf->SetFont('arialn','',10);
		$pdf->MultiCell(190,3.5, $neighbourhood, 1, "T");
	}

	$pdf->Ln();

	//watches sold out in the past
	// renovation or tekover/renovation or lease renewal or new relocation project
	if(($project["project_projectkind"] == 2 
        or $project["project_projectkind"] == 3 
		or $project["project_projectkind"] == 5)
		or ($project["project_projectkind"] == 6 and $project["project_is_relocation_project"] == 1 and $project["project_relocated_posaddress_id"] > 0)
	   )

	{
		
		$sellout_ending_year = (int)substr($cer_basicdata["date_created"], 0, 4);

		if($sellout_ending_year < (int)substr($cer_basicdata["cer_basicdata_firstyear"], 0, 4))
		{
			$sellout_ending_year =(int)substr($cer_basicdata["cer_basicdata_firstyear"], 0, 4);
		}

		if($sellout_ending_year > date("Y"))
		{
			$sellout_ending_year = date("Y");
		}

		$sellout_starting_year =$sellout_ending_year - 3;

		$pdf->SetFont('arialn','B',9);
		$pdf->Cell(75,5,"",0, 0, 'L', 0);
		for($i=$sellout_starting_year;$i<=($sellout_ending_year);$i++)
		{
			if(array_key_exists($i, $sellouts_watches))
			{
				$pdf->Cell(23,5,$i . "(" . $sellouts_months[$i] . ")",1, 0, 'R', 0);
			}
			else
			{
				$pdf->Cell(23,5,$i,1, 0, 'R', 0);
			}
		}
		$pdf->Cell(23,5,"",1, 0, 'R', 0);
		
		$pdf->Ln();

		$pdf->SetFont('arialn','B',9);
		$x = $pdf->GetX();
		$pdf->Cell(190,10,"",1, 0, 'L', 0);
		$pdf->SetX($x);

		$pdf->Cell(75,10," ",0, 0, 'R', 0);
		for($i=0;$i<5;$i++)
		{
			$pdf->Cell(23,10," ",1, 0, 'R', 0);
		}

		$pdf->SetX($x);

		$pdf->Cell(75,5,"Watches units (sold)",0, 0, 'L', 0);
		
		$pdf->SetFont('arialn','',9);
		for($i=$sellout_starting_year;$i<=($sellout_ending_year);$i++)
		{
			if(array_key_exists($i, $sellouts_watches))
			{
				$pdf->Cell(23,5,number_format($sellouts_watches[$i], 0, ".", "'"),0, 0, 'R', 0);
			}
			else
			{
				$pdf->Cell(23,5,"",0, 0, 'R', 0);
			}
		}
		$pdf->Ln();

		$pdf->SetFont('arialn','B',9);
		$pdf->Cell(75,5,"Bjoux units (sold)",0, 0, 'L', 0);
		$pdf->SetFont('arialn','',9);
		for($i=$sellout_starting_year;$i<=($sellout_ending_year);$i++)
			{
			if(array_key_exists($i, $sellouts_bjoux))
			{
				$pdf->Cell(23,5,number_format($sellouts_bjoux[$i], 0, ".", "'"),0, 0, 'R', 0);
			}
			else
			{
				$pdf->Cell(23,5," ",0, 0, 'R', 0);
			}
		}
	}

	
	

//}	


//project layout attachment

$sql = "select order_file_path " . 
       "from projects " . 
	   "left join order_files on order_file_order = project_order " . 
       "where order_file_category = 8 and order_file_type= 13 and project_id = " .param("pid") . 
	   " order by order_files.date_created DESC";

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$source_file = ".." . $row["order_file_path"];
	if(file_exists($source_file))
	{
		
		$num_of_pages = $pdf->setSourceFile($source_file);
		for($i=1;$i<=$num_of_pages;$i++)
		{
			$tplidx = $pdf->ImportPage($i);
			$s = $pdf->getTemplatesize($tplidx);
			if($s["w"] > $s["h"])
			{
				$pdf->AddPage("L", array(0=>$s["h"], 1=>$s["w"]));
			}
			else
			{
				$pdf->AddPage("P", array(0=>$s["w"], 1=>$s["h"]));
			}
			$pdf->useTemplate($tplidx);
		}

		
	}           
}
else // layout as JPEG
{
	$sql = "select order_file_path " . 
		   "from projects " . 
		   "left join order_files on order_file_order = project_order " . 
		   "where order_file_category = 8 and order_file_type= 2 and project_id = " .param("pid") . 
		   " order by order_files.date_created DESC";

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$source_file = ".." . $row["order_file_path"];
		if(file_exists($source_file))
		{
			
			$pdf->AddPage("L");
			$margin_top = 16;
			$margin_left = 12;
			$pdf->SetXY($margin_left,$margin_top);

			$pdf->SetFont("arialn", "I", 9);
			$pdf->Cell(45, 10, "", 1);

			$pdf->Image('../pictures/logo.jpg',13,$margin_top + 1,22);

			$pdf->SetFont("arialn", "B", 11);
			$pdf->Cell(172, 10, "Layout: " . $project_name, 1, "", "L");
			$pdf->Cell(20, 10, date("d.m.Y"), 1, "", "C");

			$pdf->Image($source_file,20,40, 200);
		}
	}
}



if(file_exists($floor_plan))
{
	//PDF
	if(substr($floor_plan, strlen($floor_plan)-3, 3) == "pdf" or substr($floor_plan, strlen($floor_plan)-3, 3) == "PDF")
	{
		
		$num_of_pages = $pdf->setSourceFile($floor_plan);
		for($i=1;$i<=$num_of_pages;$i++)
		{
			$tplidx = $pdf->ImportPage($i);
			$s = $pdf->getTemplatesize($tplidx);
			if($s["w"] > $s["h"])
			{
				$pdf->AddPage("L", array(0=>$s["h"], 1=>$s["w"]));
			}
			else
			{
				$pdf->AddPage("P", array(0=>$s["w"], 1=>$s["h"]));
			}
			$pdf->useTemplate($tplidx);
		}
		
		
	}
	elseif(substr($floor_plan, strlen($floor_plan)-3, 3) == "jpg" or substr($floor_plan, strlen($floor_plan)-3, 3) == "JPG")
	{
		$pdf->AddPage("P");
		$pdf->Image('../pictures/logo.jpg',10,8,33);
		//arialn bold 15
		$pdf->SetFont('arialn','B',12);
		//Move to the right
		$pdf->Cell(80);
		//Title
		$pdf->SetY(0);
		$pdf->Cell(0,33,"Mall Map/Street Map",0,0,'R');
		//Line break
		$pdf->SetY(23);
		$pdf->Image($floor_plan,10,30, 180);

		//Position at 1.5 cm from bottom
		$pdf->SetY(282);
		//arialn italic 8
		$pdf->SetFont('arialn','I',8);
		//Page number
		$pdf->Cell(0,10, to_system_date(date("d.m.y")),0,0,'R');
	}
}


	//Picture 2
	if($pix2 != ".." and file_exists($pix2))
	{
		if(substr($pix2, strlen($pix2)-3, 3) == "jpg" or substr($pix2, strlen($pix2)-3, 3) == "JPG")
		{
			$pdf->AddPage("P", "A4");
			$pdf->Image('../pictures/logo.jpg',10,8,33);
			//arialn bold 15
			$pdf->SetFont('arialn','B',12);
			//Move to the right
			$pdf->Cell(80);
			//Title
			$pdf->SetY(0);
			$pdf->Cell(0,33,"Pictures",0,0,'R');
			//Line break
			$pdf->SetY(23);

			$x = $pdf->GetX();
			$y = $pdf->GetY() + 2;
			
			$pdf->Image($pix2,$x,$y, 0, 110);
			
			//Position at 1.5 cm from bottom
			$pdf->SetY(282);
			//arialn italic 8
			$pdf->SetFont('arialn','I',8);
			//Page number
			$pdf->Cell(0,10, to_system_date(date("d.m.y")),0,0,'R');
		}
	}


	//Picture 3
	if($pix3 != ".." and file_exists($pix3))
	{
		if(substr($pix3, strlen($pix3)-3, 3) == "jpg" or substr($pix3, strlen($pix3)-3, 3) == "JPG")
		{
			
			$y = 145;
			$pdf->Image($pix3,$x,$y, 0, 110);

			//Position at 1.5 cm from bottom
			$pdf->SetY(282);
			//arialn italic 8
			$pdf->SetFont('arialn','I',8);
			//Page number
			$pdf->Cell(0,10, to_system_date(date("d.m.y")),0,0,'R');
		}
	}


//quotes for local works
//include("../user/project_offer_comparison_pdf_main.php");


//business plan
include("cer_inr02_pdf_detail.php");

//business plan in system'currency
if(array_key_exists("s", $currencies) and array_key_exists("c1", $currencies) and $currencies["s"] != $currencies["c1"])
{
	$cid = 's';
	include("cer_inr02_pdf_detail.php");
}

//business plan in pos'currency
if(array_key_exists("c2", $currencies) and array_key_exists("c1", $currencies) and $currencies["c2"] != $currencies["c1"])
{
	$cid = 'c2';
	include("cer_inr02_pdf_detail.php");
}


//brand details
if(count($cer_brands) > 1)
{
	include("cer_inr02_brand_detail_pdf.php");
}

//project budget in local currency

include("../user/project_costs_budget_detail_pdf.php");


//project budget in CHF
if(isset($order_currency) and $order_currency["symbol"] != 'CHF')
{
	$sc = 1;
	include("../user/project_costs_budget_detail_pdf.php");
}

//list of variables
include("cer_listofvars_pdf_detail.php");


//check if project has pdf attachments to be added to the booklet
$PDFmerger_was_used = false;
$pdf_print_output = true;

$sql = "select order_file_path " . 
       "from projects " . 
	   "left join order_files on order_file_order = project_order " . 
       "where order_file_attach_to_cer_booklet = 1 and order_file_type = 13 and project_id = " .param("pid") . 
	   " order by order_files.date_created DESC";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{

	$source_file = ".." . $row["order_file_path"];	if(file_exists($source_file))
	{
		
		if($pdf_print_output == true)
		{
			$pdfString = $pdf->output('', 'S');
			$tmp = SetaPDF_Core_Document::loadByString($pdfString);
			
			if($PDFmerger_was_used == false)
			{
				$merger = new SetaPDF_Merger();
			}

			$merger->addDocument($tmp);
			$pdf_print_output = false;
		}

		$merger->addFile(array(
			'filename' => $source_file,
			'copyLayers' => true
		));


		
		$PDFmerger_was_used = true;
		$pdf_print_output = false;

	}
	
}

$file_name = BRAND . "_AF_Booklet_" . str_replace(" ", "_", $project_name) . "_" . date("Y-m-d") . ".pdf";
if($PDFmerger_was_used == true)
{

	if($pdf_print_output == false)
	{
		$merger->merge();
		$document = $merger->getDocument();

		$document->setWriter(new SetaPDF_Core_Writer_Http($file_name, true));
		$document->save()->finish();
	}
	else
	{
	
		$pdfString = $pdf->output('', 'S');
		$tmp = SetaPDF_Core_Document::loadByString($pdfString);
		$merger->addDocument($tmp);
		
		$merger->merge();
		$document = $merger->getDocument();
		$document->setWriter(new SetaPDF_Core_Writer_Http($file_name, true));
		$document->save()->finish();
	}
}
else
{
	$pdf->Output($file_name, 'I');
}
?>