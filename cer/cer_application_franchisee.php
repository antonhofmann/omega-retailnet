<?php
/********************************************************************

    cer_application_franchisee.php

    Application Form: franchisee information
    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";


check_access("has_access_to_cer");

if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}

/********************************************************************
    prepare all data needed
*********************************************************************/
$client_address = get_address($project["order_client_address"]);
$posdata = get_pos_data($project["project_order"]);
$franchisee_address = get_address($posdata["posaddress_franchisee_id"]);
$franchisor_address = get_address($posdata["posaddress_franchisor_id"]);

if(!array_key_exists("company", $franchisee_address))
{
	$franchisee_address["company"] = "";
	$franchisee_address["zip"] = "";
	$franchisee_address["place"] = "";
	$franchisee_address["country_name"] = "";

}

//get quantities of watches
$quantity_watches = array();

$sql = "select * from cer_revenues where cer_revenue_cer_version = 0 and cer_revenue_project = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if(array_key_exists($row["cer_revenue_year"], $quantity_watches))
	{
		$quantity_watches[$row["cer_revenue_year"]] = $quantity_watches[$row["cer_revenue_year"]] + $row["cer_revenue_quantity_watches"];
	}
	else
	{
		$quantity_watches[$row["cer_revenue_year"]] = $row["cer_revenue_quantity_watches"];
	}
}
ksort($quantity_watches);

// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ".
                 "order by country_name";

/*
//agreemant data

//calculate Agreement duration
if($posdata["posaddress_fagrstart"] != NULL and $posdata["posaddress_fagrstart"] != '0000-00-00')
{
	$starting_date = $posdata["posaddress_fagrstart"];
	$expiry_date = $posdata["posaddress_fagrend"];
}
else
{
	$fag_data = get_fag_data($project["project_planned_opening_date"]);
	$starting_date = $fag_data["starting_date"];
	$expiry_date = $fag_data["ending_date"];

	$sql = "update " . $posdata["table"] . " set " .
		   "posaddress_fagrstart = " . dbquote($starting_date) . ", " .
		   "posaddress_fagrend = " . dbquote($expiry_date) .
		   "where posaddress_id = " . $posdata["posaddress_id"];
	
	$result = mysql_query($sql) or dberror($sql);
}

$duration = "";
$sql = "select IF(TIMESTAMPDIFF(MONTH,posaddress_fagrstart,posaddress_fagrend) >0, TIMESTAMPDIFF(MONTH,posaddress_fagrstart,posaddress_fagrend), 'n.a.') as months from ". $posdata["table"] . " where posaddress_id = " . $posdata["posaddress_id"];

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$duration = $row["months"];
	$duration = floor($duration/12);

	$months = 1 + $row["months"] - $duration*12;


	if($months == 12)
	{
		$duration = $duration + 1 . " years";
	}
	
	elseif($months > 0)
	{
		$duration = $duration . " years and " . (1 + $row["months"] - $duration*12) . " months";
	}
}
*/




/********************************************************************
    build form
*********************************************************************/
$form = new Form("posaddresses", "posaddress");

$donotshowfranchisee = true;
include("include/project_head.php");


$form->add_hidden("pid", param("pid"));


if($project["project_cost_type"] != 6)
{
	$form->add_section("Franchisee");
}
else
{
	$form->add_section("Owner Company");
}

$form->add_label("franchisee_company", "Company", 0, $franchisee_address["company"]);
$form->add_label("franchisee_address", "City", 0, $franchisee_address["zip"] . " " . $franchisee_address["place"]);
$form->add_label("franchisee_country", "Country", 0, $franchisee_address["country_name"]);
/*
$form->add_label("franchisee_email", "Email", 0, $franchisee_address["email"]);
$form->add_label("franchisee_phone", "Phone", 0, $franchisee_address["phone"]);
$form->add_label("franchisee_contact", "Contact", 0, $franchisee_address["contact_name"]);
*/

if($project["project_actual_opening_date"] == NULL or $project["project_actual_opening_date"] == '0000-00-00')
{
	if (has_access("can_edit_franchisee_agreement_data"))
	{

		if(count($posdata) > 0 and array_key_exists("posaddress_fagagreement_type", $posdata) and  $posdata["posaddress_fagagreement_type"] > 0 and $project["project_projectkind"] ==  2) // renovation projects
		{

			/*
			$type = $posdata["posaddress_fagagreement_type"];
			$sent = $posdata["posaddress_fagrsent"];
			$signed = $posdata["posaddress_fagrsigned"];
			$start = $posdata["posaddress_fagrstart"];
			$end = $posdata["posaddress_fagrend"];
			$comment = $posdata["posaddress_fag_comment"];
			

			
			$type = "";
			$sent = "";
			$signed = "";
			$start = "";
			$end = "";
			$comment = "";;
			*/

			$type = $project["project_fagagreement_type"];
			$sent = $project["project_fagrsent"];
			$signed = $project["project_fagrsigned"];
			$start = $project["project_fagrstart"];
			$end = $project["project_fagrend"];
			$comment = $project["project_fag_comment"];
		}
		else
		{
			$type = $project["project_fagagreement_type"];
			$sent = $project["project_fagrsent"];
			$signed = $project["project_fagrsigned"];
			$start = $project["project_fagrstart"];
			$end = $project["project_fagrend"];
			$comment = $project["project_fag_comment"];

		}
		
		$form->add_section("Agreement");
		$form->add_list("project_fagagreement_type", "Agreement Type*", "select agreement_type_id, agreement_type_name from agreement_types", 0, $type);
		$form->add_checkbox("project_fagrsent", "Sent", $sent, 0, "Agreement");
		$form->add_checkbox("project_fagrsigned", "Signed", $signed, 0, "Agreement");
		$form->add_label("dummy", "");
		$form->add_edit("project_fagrstart", "Agreement Starting Date", 0, to_system_date($start), TYPE_DATE);
		$form->add_edit("project_fagrend", "Agreement Ending Date", 0, to_system_date($end), TYPE_DATE);
		$form->add_multiline("project_fag_comment", "Comment", 4, 0, $comment);
	}
	else
	{	
		if(count($posdata) > 0 and array_key_exists("posaddress_fagagreement_type", $posdata) and  $posdata["posaddress_fagagreement_type"] > 0)
		{
			$type = $posdata["posaddress_fagagreement_type"];
			$sent = $posdata["posaddress_fagrsent"];
			$signed = $posdata["posaddress_fagrsigned"];
			$start = $posdata["posaddress_fagrstart"];
			$end = $posdata["posaddress_fagrend"];
			$comment = $posdata["posaddress_fag_comment"];
		}
		else
		{
			$type = $project["project_fagagreement_type"];
			$sent = $project["project_fagrsent"];
			$signed = $project["project_fagrsigned"];
			$start = $project["project_fagrstart"];
			$end = $project["project_fagrend"];
			$comment = $project["project_fag_comment"];
		}
		
		$form->add_hidden("project_fagagreement_type", $type);
		$form->add_hidden("project_fagrsent", $sent);
		$form->add_hidden("project_fagrsigned", $signed);
		$form->add_hidden("project_fagrstart", to_system_date($start));
		$form->add_hidden("project_fagrend", to_system_date($end));
		$form->add_hidden("project_fag_comment",$comment);


	}
}
elseif (has_access("can_edit_franchisee_agreement_data"))
{
	if(count($posdata) >0  and array_key_exists("posaddress_fagagreement_type", $posdata) and  $posdata["posaddress_fagagreement_type"] > 0)
	{
		$type = $posdata["posaddress_fagagreement_type"];
		$sent = $posdata["posaddress_fagrsent"];
		$signed = $posdata["posaddress_fagrsigned"];
		$start = $posdata["posaddress_fagrstart"];
		$end = $posdata["posaddress_fagrend"];
		$comment = $posdata["posaddress_fag_comment"];
	}
	else
	{
		$type = $project["project_fagagreement_type"];
		$sent = $project["project_fagrsent"];
		$signed = $project["project_fagrsigned"];
		$start = $project["project_fagrstart"];
		$end = $project["project_fagrend"];
		$comment = $project["project_fag_comment"];
	}
	
	$form->add_section("Agreement");
	$form->add_list("project_fagagreement_type", "Agreement Type*", "select agreement_type_id, agreement_type_name from agreement_types", 0, $type);
	$form->add_checkbox("project_fagrsent", "Sent", $sent, 0, "Agreement");
	$form->add_checkbox("project_fagrsigned", "Signed", $signed, 0, "Agreement");
	$form->add_label("dummy", "");
	$form->add_edit("project_fagrstart", "Agreement Starting Date", 0, to_system_date($start), TYPE_DATE);
	$form->add_edit("project_fagrend", "Agreement Ending Date", 0, to_system_date($end), TYPE_DATE);
	$form->add_multiline("project_fag_comment", "Comment", 4, 0, $comment);
}



if($form_type == "AF" or $form_type == "INR03")
{
	$form->add_section("Relation");
	$form->add_checkbox("address_is_independent_retailer", "Company is an independent retailer", $franchisee_address["address_is_independent_retailer"], "", "Relation");
	
	
	
	$form->add_checkbox("cer_basicdata_franchsiee_already_partner", "Company is already a business partner of " . BRAND, $cer_basicdata["cer_basicdata_franchsiee_already_partner"], "", "Third Party Business Partner");

	$form->add_edit("address_company_is_partner_since", "Company is partner since", 0, to_system_date($franchisee_address["address_company_is_partner_since"]), TYPE_DATE);

	$form->add_edit("cer_basicdata_franchsiee_number_of_pos", "&nbsp;&nbsp;- with Numer of Stores", 0, $cer_basicdata["cer_basicdata_franchsiee_number_of_pos"], TYPE_INT, 8);

	$form->add_edit("cer_basicdata_franchsiee_brands", "&nbsp;&nbsp;- of the following Brands", 0, $cer_basicdata["cer_basicdata_franchsiee_brands"]);
}
else
{
	$form->add_hidden("address_is_independent_retailer");
	$form->add_hidden("address_company_is_partner_since");

	$form->add_hidden("cer_basicdata_franchsiee_already_partner");
	$form->add_hidden("cer_basicdata_franchsiee_number_of_pos");
	$form->add_hidden("cer_basicdata_franchsiee_brands");
}

$deletable = "";
if(has_access("can_delete_uploaded_files"))
{
	$deletable = DELETABLE;
}

if($form_type == "CER")
{
	$form->add_section("Mall Map/Street Map (PDF or JPG only)*:	");
	$form->add_upload("ln_basicdata_floorplan", "Mall Map/Street Map (PDF or JPG only)*", "/files/ln/". $ln_basicdata["project_number"], $deletable, $ln_basicdata["ln_basicdata_floorplan"], 1, "floorplan");

	$form->add_section("Territory Exclusivity");
	$form->add_comment("Please give a description clearly delimitating the exclusive territory of this POS location, covered by the Franchisee contract.");
}
elseif($form_type == "AF" or $form_type == "INR03")
{
	
	$form->add_section("Planning");
	$form->add_edit("project_drawing_submission_date", "Date of drawings to be submitted to retailer", NOTNULL, to_system_date($project["project_drawing_submission_date"]), TYPE_DATE);

	$form->add_edit("project_construction_startdate", "Date of construction works start", NOTNULL, to_system_date($project["project_construction_startdate"]), TYPE_DATE);

	
	
	
	$form->add_section("Picture");
	$form->add_upload("ln_basicdata_pix1", "Photo 1 (JPG only)", "/files/ln/". $ln_basicdata["project_number"], 0 | $deletable, $ln_basicdata["ln_basicdata_pix1"]);
	
	$form->add_section("Floor Plan");
	$form->add_upload("ln_basicdata_floorplan", "Shop floor plan (PDF)*", "/files/ln/". $ln_basicdata["project_number"], $deletable, $ln_basicdata["ln_basicdata_floorplan"], 1, "floorplan");

	$form->add_upload("ln_basicdata_floorplan2", "Shop floor plan (DWG)*", "/files/ln/". $ln_basicdata["project_number"], $deletable, $ln_basicdata["ln_basicdata_floorplan2"], 1, "floorplan2");


	$form->add_section("Elevation");
	$form->add_upload("ln_basicdata_elevation_plan", "Interior elevetion plan (PDF) showing neighbours*", "/files/ln/". $ln_basicdata["project_number"], $deletable, $ln_basicdata["ln_basicdata_elevation_plan"], 1, "elevationplan");

	$form->add_upload("ln_basicdata_elevation_plan2", "Interior elevetion plan (DWG)*", "/files/ln/". $ln_basicdata["project_number"], $deletable, $ln_basicdata["ln_basicdata_elevation_plan2"], 1, "elevationplan2");


	$form->add_upload("ln_basicdata_elevation_plan3", "Elevetion plan of the facade (PDF)", "/files/ln/". $ln_basicdata["project_number"], $deletable, $ln_basicdata["ln_basicdata_elevation_plan3"], 1, "elevationplan3");

	$form->add_upload("ln_basicdata_elevation_plan4", "Elevetion plan of the facade (DWG)", "/files/ln/". $ln_basicdata["project_number"], $deletable, $ln_basicdata["ln_basicdata_elevation_plan4"], 1, "elevationplan4");

	$form->add_section("Photos");
	$form->add_upload("ln_basicdata_photoreport", "Photo report (interior, exterior views) including neighbours (PDF)", "/files/ln/". $ln_basicdata["project_number"], $deletable, $ln_basicdata["ln_basicdata_photoreport"], 1, "photoreport");	

	$form->add_section("3D Renderings");
	$form->add_upload("ln_basicdata_3drenderings", "3D Renderings (PDF)", "/files/ln/". $ln_basicdata["project_number"], $deletable, $ln_basicdata["ln_basicdata_photoreport"], 1, "3drenderings");	
	

	$form->add_section("Territory Exclusivity");
	$form->add_comment("Please give a description clearly delimitating the exclusive territory of this POS location, covered by the Franchisee contract.");
}





if(has_access("has_access_to_his_cer") or has_access("has_full_access_to_cer"))
{
	$form->add_multiline("posaddress_fag_territory", "Description", 4, 0, $posdata["posaddress_fag_territory"]);
	
	
	if($posdata["posaddress_fag_city_pasted"])
	{
		$form->add_upload("posaddress_fag_city_pasted", "City Map (PDF or JPG only)*", "/files/cer/". $project["order_number"],  $deletable, $posdata["posaddress_fag_city_pasted"]);
	}
	else
	{
		$form->add_hidden("posaddress_fag_city_pasted");
	}
}
else
{
	$form->add_label("posaddress_fag_territory", "Agreement Content");
	
	if($posdata["posaddress_fag_city_pasted"])
	{
		$link = "<a href=\"#\" onClick=\"javascript:popup('" . $posdata["posaddress_fag_city_pasted"] . "', 640, 480);\">Show City Pasted</a>";
		$form->add_label("posaddress_fag_city_pasted", "City Pasted", RENDER_HTML, $link);
	}

}
/*
$form->add_section("Duration of the Agreement");
$form->add_label("end", "Planned POS Opening Date", 0, to_system_date($project["project_planned_opening_date"]));

$form->add_label("posaddress_fagrstart", "Agreement Starting Date", 0, to_system_date($starting_date));
$form->add_label("posaddress_fagrend", "Agreement Ending Date", 0,  to_system_date($expiry_date));
$form->add_label("posaddress_fargrduration", "Agreement Duration", 0, $duration);
*/

if(count($quantity_watches) > 0)
{
	$form->add_section("Minimum Quantities (only Watches as per Franchisee Contract)");
}
foreach($quantity_watches as $year=>$quantity)
{
	$form->add_label("units_" . $year, "Units in " . $year ,0 ,$quantity);
}


$form->add_section("Franchisor Address");
$form->add_label("franchisor_company", "Company", 0, $franchisor_address["company"]);
$form->add_label("franchisor_address", "City", 0, $franchisor_address["zip"] . " " . $franchisor_address["place"]);
$form->add_label("franchisor_country", "Country", 0, $franchisor_address["country_name"]);
/*
$form->add_label("franchisor_email", "Email", 0, $franchisor_address["email"]);
$form->add_label("franchisor_phone", "Phone", 0, $franchisor_address["phone"]);
$form->add_label("franchisor_contact", "Contact", 0, $franchisor_address["contact_name"]);
*/

if(($cer_basicdata["cer_basicdata_cer_locked"] == 0 and has_access("has_access_to_his_cer")) or has_access("has_full_access_to_cer"))
{
	$form->add_button("save", "Save Data");
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if($form->button("save"))
{
	
	$sql = "update addresses set address_is_independent_retailer = " . dbquote($form->value("address_is_independent_retailer")) . ", " . 
		"address_company_is_partner_since = " . dbquote(from_system_date($form->value("address_company_is_partner_since"))) . 
		   " where address_id = " . $posdata["posaddress_franchisee_id"];
	
	mysql_query($sql) or dberror($sql);
	
	
	$files_ok = true;
	
	
	if($form_type == "CER")
	{
		$floor_plan_ext = substr(strtolower($form->value('ln_basicdata_floorplan')), -3, 3);
		$city_pasted_ext = substr(strtolower($form->value('posaddress_fag_city_pasted')), -3, 3);
	
	
		if($floor_plan_ext and $floor_plan_ext != 'pdf') 
		{
			$form->error('Only PDF Files are allowed for uploads.');	
			$files_ok = false;
		}
		elseif($city_pasted_ext and $city_pasted_ext != 'pdf' and $city_pasted_ext != 'jpg') 
		{
			$form->error('Only PFD or JPG Files are allowed for uploads.');
			$files_ok = false;
		}
	}	
	elseif($form_type == "AF"  or $form_type == "INR03")
	{
		

		$pix1_ext = substr(strtolower($form->value('ln_basicdata_pix1')), -3, 3);
		$floor_plan_ext = substr(strtolower($form->value('ln_basicdata_floorplan')), -3, 3);
		$floor_plan2_ext = substr(strtolower($form->value('ln_basicdata_floorplan2')), -3, 3);
		$elevation_plan_ext = substr(strtolower($form->value('ln_basicdata_elevation_plan')), -3, 3);
		$elevation_plan2_ext = substr(strtolower($form->value('ln_basicdata_elevation_plan2')), -3, 3);
		$elevation_plan3_ext = substr(strtolower($form->value('ln_basicdata_elevation_plan3')), -3, 3);
		$elevation_plan4_ext = substr(strtolower($form->value('ln_basicdata_elevation_plan4')), -3, 3);
		$photoreport_ext = substr(strtolower($form->value('ln_basicdata_photoreport')), -3, 3);
		$renderings_ext = substr(strtolower($form->value('ln_basicdata_3drenderings')), -3, 3);

		if($pix1_ext and $pix1_ext != 'jpg') 
		{
			$form->error('Picture: Only JPG Files are allowed for the picture upload.');	
			$files_ok = false;
		}
		elseif($floor_plan_ext and $floor_plan_ext != 'pdf') 
		{
			$form->error('Floorplan: Only PDF Files are allowed for uploads.');	
			$files_ok = false;
		}
		elseif($floor_plan2_ext and $floor_plan2_ext != 'dwg') 
		{
			$form->error('Floorplan: Only DWG Files are allowed for uploads.');	
			$files_ok = false;
		}
		elseif($elevation_plan_ext and $elevation_plan_ext != 'pdf') 
		{
			$form->error('Interiro Elevation: Only PDF Files are allowed for uploads.');	
			$files_ok = false;
		}
		elseif($elevation_plan2_ext and $elevation_plan2_ext != 'dwg') 
		{
			$form->error('Interiro Elevation: Only DWG Files are allowed for uploads.');	
			$files_ok = false;
		}
		elseif($elevation_plan3_ext and $elevation_plan3_ext != 'pdf') 
		{
			$form->error('Elevation: Only PDF Files are allowed for uploads.');	
			$files_ok = false;
		}
		elseif($elevation_plan4_ext and $elevation_plan4_ext != 'dwg') 
		{
			$form->error('Elevation: Only DWG Files are allowed for uploads.');	
			$files_ok = false;
		}
		elseif($photoreport_ext and $photoreport_ext != 'pdf') 
		{
			$form->error('Photo Report: Only PDF Files are allowed for uploads.');	
			$files_ok = false;
		}
		elseif($renderings_ext and $renderings_ext != 'pdf') 
		{
			$form->error('3D Renderings: Only PDF Files are allowed for uploads.');	
			$files_ok = false;
		}
	}
	
	if($files_ok == true and $form->validate())
	{

		
			//update ln_basic_data
			$fields = array();

			$value = dbquote($form->value("ln_basicdata_floorplan"));
			$fields[] = "ln_basicdata_floorplan = " . $value;

			if($form_type == "AF"  or $form_type == "INR03")
			{
				
				$value = dbquote($form->value("ln_basicdata_pix1"));
				$fields[] = "ln_basicdata_pix1 = " . $value;
				
				$value = dbquote($form->value("ln_basicdata_elevation_plan"));
				$fields[] = "ln_basicdata_elevation_plan = " . $value;

				$value = dbquote($form->value("ln_basicdata_elevation_plan2"));
				$fields[] = "ln_basicdata_elevation_plan2 = " . $value;

				$value = dbquote($form->value("ln_basicdata_elevation_plan3"));
				$fields[] = "ln_basicdata_elevation_plan3 = " . $value;

				$value = dbquote($form->value("ln_basicdata_elevation_plan4"));
				$fields[] = "ln_basicdata_elevation_plan4 = " . $value;

				$value = dbquote($form->value("ln_basicdata_photoreport"));
				$fields[] = "ln_basicdata_photoreport = " . $value;

				$value = dbquote($form->value("ln_basicdata_3drenderings"));
				$fields[] = "ln_basicdata_3drenderings = " . $value;
			}

		$value1 = $_SESSION["user_login"];
		$fields[] = "user_modified = " . dbquote($value1);


		$sql = "update ln_basicdata set " . join(", ", $fields) . " where ln_basicdata_version = 0 and ln_basicdata_project = " . param("pid");
		mysql_query($sql) or dberror($sql);
		
		//update posaddress data

		$fields = array();
		
		$value = dbquote($form->value("posaddress_fag_territory"));
		$fields[] = "posaddress_fag_territory = " . $value;

		$value = dbquote($form->value("posaddress_fag_city_pasted"));
		$fields[] = "posaddress_fag_city_pasted = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update " . $posdata["table"] . " set " . join(", ", $fields) . " where posaddress_id = " . $posdata["posaddress_id"];
		mysql_query($sql) or dberror($sql);


		//update cer basic data

		$fields = array();
		
		$value = dbquote($form->value("cer_basicdata_franchsiee_already_partner"));
		$fields[] = "cer_basicdata_franchsiee_already_partner = " . $value;

		$value = dbquote($form->value("cer_basicdata_franchsiee_number_of_pos"));
		$fields[] = "cer_basicdata_franchsiee_number_of_pos = " . $value;

		$value = dbquote($form->value("cer_basicdata_franchsiee_brands"));
		$fields[] = "cer_basicdata_franchsiee_brands = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_basicdata set " . join(", ", $fields) . " where cer_basicdata_version = 0 and cer_basicdata_id = " . $cer_basicdata["cer_basicdata_id"];
		mysql_query($sql) or dberror($sql);


		//update project data
		if($form_type == "AF"  or $form_type == "INR03")
		{
			$sql = "update projects set " . 
				   "project_drawing_submission_date = " . dbquote(from_system_date($form->value("project_drawing_submission_date"))) . ", " .
				   "project_construction_startdate = " . dbquote(from_system_date($form->value("project_construction_startdate"))) . 
				   " where project_id = " . dbquote(param("pid"));
			mysql_query($sql) or dberror($sql);
		
		}


		//update agreement data
		$project_fields = array();

		$value = trim($form->value("project_fagagreement_type")) == "" ? "0" : dbquote($form->value("project_fagagreement_type"));
		$project_fields[] = "project_fagagreement_type = " . $value;

		$value = trim($form->value("project_fagrsent")) == "" ? "0" : dbquote($form->value("project_fagrsent"));
		$project_fields[] = "project_fagrsent = " . $value;

		$value = trim($form->value("project_fagrsigned")) == "" ? "0" : dbquote($form->value("project_fagrsigned"));
		$project_fields[] = "project_fagrsigned = " . $value;

		$value = trim($form->value("project_fagrstart")) == "" ? "NULL" : dbquote(from_system_date($form->value("project_fagrstart")));
		$project_fields[] = "project_fagrstart = " . $value;

		$value = trim($form->value("project_fagrend")) == "" ? "NULL" : dbquote(from_system_date($form->value("project_fagrend")));
		$project_fields[] = "project_fagrend = " . $value;

		$value = trim($form->value("project_fag_comment")) == "" ? "null" : dbquote($form->value("project_fag_comment"));
		$project_fields[] = "project_fag_comment = " . $value;

		$sql = "update projects set " . join(", ", $project_fields) . " where project_id = " . dbquote(param("pid"));
		mysql_query($sql) or dberror($sql);
		
		$form->message("Your data has bee saved.");
	}
}
  
/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();
if($form_type == "INR03")
{
	$page->title("INR-03 - Retail Furniture in Third-party Store: Franchisee Information");
}
elseif($form_type == "AF")
{
	$page->title("Application Form: Franchisee Information");
}
else
{
	$page->title("Capital Expenditure Request: Franchisee Information");
}

require_once("include/tabs.php");
$form->render();
?>


<div id="floorplan" style="display:none;">
    Please make sure that the mall/street map contains:
	<ul>
	<li>a clear indication of future <?php echo BRAND;?> Location</li>
	<li>a clear indication of who our direct neighbours are</li>
	<li>indication of major brands an brand mix of the shopping mall</li>
	</ul>
</div>


<?php

require "include/footer_scripts.php";
$page->footer();

?>