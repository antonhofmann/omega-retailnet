<?php
/********************************************************************

    draft_page_actions.php

    Defines the possible page actions depending on access rights

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-02-21
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-02-21
    Version:        1.0.1

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

$page->register_action('bpentry', 'Business Plan Data', "cer_draft.php?did=" . param("did"));

$page->register_action('fsummary', 'Financial Summary', "draft_financial_summary.php?did=" . param("did"));

$page->register_action('print', 'Print Business Plan', "cer_draft_print.php?did=" . param("did"));

$link = "'/cer/cer_inr02_extension_pdf.php?did=" . param("did") . "'";
$page->register_action('cerinr02pdf', 'Print Business Plan Extension',"javascript:popup(" . $link . ", 1024, 768);");

$page->register_action('submit_draft', 'Submit Draft', "cer_draft_submit.php?did=" . param("did"));

?>