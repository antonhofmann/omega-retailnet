<?php

// Financial Figures year 1 to 10
$pdf->SetXY($margin_left,$y);
$pdf->SetFont("arialn", "B", 10);
$pdf->Cell(63, 5, "4. Average Sell Out", 1, "", "L");
$pdf->SetXY($margin_left,$y+5);
$pdf->Cell(63, 95, "", 1, "", "L");

$x1 = $margin_left+65;
$x2 = $x1+21;
$x3 = $x2+21;
$x4 = $x3+21;
$x5 = $x4+21;
$x6 = $x5+21;
$x7 = $x6+21;
$x8 = $x7+21;
$x9 = $x8+21;
$x10 = $x9+21;
$x11 = $x10+21;

//column 1 to 5 Years
$i = 1;
foreach($years_page_row as $key=>$year)
{
	$xi = "x" . $i;
	//column 1
	$pdf->SetXY($$xi,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(19, 5, $year, 1, "", "C");
	$i++;

}

$y = $y + 4;



//column 1 to 10 Boxes
for($i=1;$i<11;$i++)
{
	$xi = "x" . $i;

	$pdf->SetXY($$xi,$y+1);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(19, 95, "", 1, "", "C");
}

//Head Counts

$y = $y + 2;
$x0 = $margin_left+6;

$pdf->SetXY($margin_left+3.2,$y);
$pdf->SetFont("arialn", "B", 8);
$pdf->Cell(50, 3.5, "Head Counts", 0, "", "L");

$y = $y + $standard_y;
$pdf->SetXY($x0,$y);
$pdf->SetFont("arialn", "", 8);
$pdf->Cell(50, 3.5, "Number of Sales Staff", 0, "", "L");

//print values for columns 1 to 5
$i = 1;

foreach($years_page_row as $key=>$year)
{
	$xi = "x" . $i;

	$pdf->SetXY($$xi,$y);
	if(array_key_exists($year, $number_of_sales_staff))
	{
		$pdf->Cell(19, 3.5, $number_of_sales_staff[$year], 0, "", "R");
	}
	else
	{
		$pdf->Cell(19, 3.5,"", 0, "", "R");
	}

	$i++;
}	


$y = $y + $standard_y;
$pdf->SetXY($x0,$y);
$pdf->SetFont("arialn", "", 8);
$pdf->Cell(50, 3.5, "Full Time Equivalents", 0, "", "L");

//print values for columns 1 to 5
$i = 1;
foreach($years_page_row as $key=>$year)
{
	$xi = "x" . $i;

	$pdf->SetXY($$xi,$y);
	if(array_key_exists($year, $ftes))
	{
		$pdf->Cell(19, 3.5, number_format($ftes[$year], 2, ".", "'"), 0, "", "R");
	}
	else
	{
		$pdf->Cell(19, 3.5, "", 0, "", "R");
	}

	$i++;
}

$y = $y + $standard_y;
$pdf->SetXY($x0,$y);
$pdf->SetFont("arialn", "", 8);
$pdf->Cell(50, 3.5, "Total Working Hours per Week for 1 FTE", 0, "", "L");

//print values for columns 1 to 5
$i = 1;
foreach($years_page_row as $key=>$year)
{
	$xi = "x" . $i;

	$pdf->SetXY($$xi,$y);
	if(array_key_exists($year, $cer_revenue_total_workinghours_per_week))
	{
		$pdf->Cell(19, 3.5, $cer_revenue_total_workinghours_per_week[$year], 0, "", "R");
	}
	else
	{
		$pdf->Cell(19, 3.5, "", 0, "", "R");
	}

	$i++;
}


$y = $y + $standard_y;
$pdf->SetXY($x0,$y);
$pdf->SetFont("arialn", "", 8);
$pdf->Cell(50, 3.5, "Total Hours per Week of whole Staff", 0, "", "L");

//print values for columns 1 to 5
$i = 1;
foreach($years_page_row as $key=>$year)
{
	$xi = "x" . $i;

	$pdf->SetXY($$xi,$y);
	if(array_key_exists($year, $total_working_hours_per_week_all_staff))
	{
		$pdf->Cell(19, 3.5, number_format($total_working_hours_per_week_all_staff[$year], 2, ".", "'"), 0, "", "R");
	}
	else
	{
		$pdf->Cell(19, 3.5, "", 0, "", "R");
	}

	$i++;
}


//Store Opening Hours

$y = $y + 4;
$x0 = $margin_left+6;

$pdf->SetXY($margin_left+3.2,$y);
$pdf->SetFont("arialn", "B", 8);
$pdf->Cell(50, 3.5, "Store Opening Hours", 0, "", "L");

$y = $y + $standard_y;
$pdf->SetXY($x0,$y);
$pdf->SetFont("arialn", "", 8);
$pdf->Cell(50, 3.5, "Total Hours Open per Week", 0, "", "L");

//print values for columns 1 to 5
$i = 1;

foreach($years_page_row as $key=>$year)
{
	$xi = "x" . $i;

	$pdf->SetXY($$xi,$y);
	if(array_key_exists($year, $cer_revenue_total_hours_open_per_week))
	{
		$pdf->Cell(19, 3.5, $cer_revenue_total_hours_open_per_week[$year], 0, "", "R");
	}
	else
	{
		$pdf->Cell(19, 3.5, "", 0, "", "R");
	}

	$i++;
}	


$y = $y + $standard_y;
$pdf->SetXY($x0,$y);
$pdf->SetFont("arialn", "", 8);
$pdf->Cell(50, 3.5, "Total Days Open per Year", 0, "", "L");

//print values for columns 1 to 5
$i = 1;
foreach($years_page_row as $key=>$year)
{
	$xi = "x" . $i;

	$pdf->SetXY($$xi,$y);
	if(array_key_exists($year, $cer_revenue_total_days_open_per_year))
	{
		$pdf->Cell(19, 3.5, $cer_revenue_total_days_open_per_year[$year], 0, "", "R");
	}
	else
	{
		$pdf->Cell(19, 3.5, "", 0, "", "R");
	}

	$i++;
}

$y = $y + $standard_y;
$pdf->SetXY($x0,$y);
$pdf->SetFont("arialn", "", 8);
$pdf->Cell(50, 3.5, "Total Hours Open per Year", 0, "", "L");

//print values for columns 1 to 5
$i = 1;
foreach($years_page_row as $key=>$year)
{
	$xi = "x" . $i;

	$pdf->SetXY($$xi,$y);
	if(array_key_exists($year, $total_hours_open_per_year))
	{
		$pdf->Cell(19, 3.5, number_format($total_hours_open_per_year[$year], 2, ".", "'"), 0, "", "R");
	}
	else
	{
		$pdf->Cell(19, 3.5, "", 0, "", "R");
	}

	$i++;
}


//Degree of Opening Hours

$y = $y + 4;
$x0 = $margin_left+6;

$pdf->SetXY($margin_left+3.2,$y);
$pdf->SetFont("arialn", "B", 8);
$pdf->Cell(50, 3.5, "Degree of Opening Hours", 0, "", "L");

$y = $y + $standard_y;
$pdf->SetXY($x0,$y);
$pdf->SetFont("arialn", "", 8);
$pdf->Cell(50, 3.5, "Average Staff per Opening Hour", 0, "", "L");

//print values for columns 1 to 5
$i = 1;

foreach($years_page_row as $key=>$year)
{
	$xi = "x" . $i;

	$pdf->SetXY($$xi,$y);
	if(array_key_exists($year, $average_staff_opening_hours))
	{
		$pdf->Cell(19, 3.5, number_format($average_staff_opening_hours[$year], 2, ".", "'"), 0, "", "R");
	}
	else
	{
		$pdf->Cell(19, 3.5, "", 0, "", "R");
	}

	$i++;
}


//Customer Frequency

$y = $y + 4;
$x0 = $margin_left+6;

$pdf->SetXY($margin_left+3.2,$y);
$pdf->SetFont("arialn", "B", 8);
$pdf->Cell(50, 3.5, "Customer Frequency", 0, "", "L");

$y = $y + $standard_y;
$pdf->SetXY($x0,$y);
$pdf->SetFont("arialn", "", 8);
$pdf->Cell(50, 3.5, "per Day", 0, "", "L");

//print values for columns 1 to 5
$i = 1;

foreach($years_page_row as $key=>$year)
{
	$xi = "x" . $i;

	$pdf->SetXY($$xi,$y);
	if(array_key_exists($year, $cer_revenue_customer_frequency))
	{
		$pdf->Cell(19, 3.5, $cer_revenue_customer_frequency[$year], 0, "", "R");
	}
	else
	{
		$pdf->Cell(19, 3.5, "", 0, "", "R");
	}

	$i++;
}


//Average Sell Out Watches

$y = $y + 4;
$x0 = $margin_left+6;

$pdf->SetXY($margin_left+3.2,$y);
$pdf->SetFont("arialn", "B", 8);
$pdf->Cell(50, 3.5, "Average Sell Out Watches", 0, "", "L");

$y = $y + $standard_y;
$pdf->SetXY($x0,$y);
$pdf->SetFont("arialn", "", 8);
$pdf->Cell(50, 3.5, "Per Day", 0, "", "L");

//print values for columns 1 to 5
$i = 1;

foreach($years_page_row as $key=>$year)
{
	$xi = "x" . $i;

	$pdf->SetXY($$xi,$y);
	
	if(array_key_exists($year, $average_sell_out_watches))
	{
		$pdf->Cell(19, 3.5, number_format($average_sell_out_watches[$year], 2, ".", "'"), 0, "", "R");
	}
	else
	{
		$pdf->Cell(19, 3.5, "", 0, "", "R");
	}

	$i++;
}	


$y = $y + $standard_y;
$pdf->SetXY($x0,$y);
$pdf->SetFont("arialn", "", 8);
$pdf->Cell(50, 3.5, "In Percent of Frequency", 0, "", "L");

//print values for columns 1 to 5
$i = 1;
foreach($years_page_row as $key=>$year)
{
	$xi = "x" . $i;

	$pdf->SetXY($$xi,$y);
	if(array_key_exists($year, $percent_sell_out_watches))
	{
		$pdf->Cell(19, 3.5, number_format(100*$percent_sell_out_watches[$year], 2, ".", "'") . "%", 0, "", "R");
	}
	else
	{
		$pdf->Cell(19, 3.5, "", 0, "", "R");
	}

	$i++;
}

$y = $y + $standard_y;
$pdf->SetXY($x0,$y);
$pdf->SetFont("arialn", "", 8);
$pdf->Cell(50, 3.5, "Per Square Meter", 0, "", "L");

//print values for columns 1 to 5
$i = 1;
foreach($years_page_row as $key=>$year)
{
	$xi = "x" . $i;

	$pdf->SetXY($$xi,$y);
	if(array_key_exists($year, $sqm_sell_out_watches))
	{
		$pdf->Cell(19, 3.5, number_format($sqm_sell_out_watches[$year], 2, ".", "'"), 0, "", "R");
	}
	else
	{
		$pdf->Cell(19, 3.5, "", 0, "", "R");
	}

	$i++;
}


//Average Sell Out Jewellery

$y = $y + 4;
$x0 = $margin_left+6;

$pdf->SetXY($margin_left+3.2,$y);
$pdf->SetFont("arialn", "B", 8);
$pdf->Cell(50, 3.5, "Average Sell Out Jewellery", 0, "", "L");

$y = $y + $standard_y;
$pdf->SetXY($x0,$y);
$pdf->SetFont("arialn", "", 8);
$pdf->Cell(50, 3.5, "Per Day", 0, "", "L");

//print values for columns 1 to 5
$i = 1;

foreach($years_page_row as $key=>$year)
{
	$xi = "x" . $i;

	$pdf->SetXY($$xi,$y);
	
	if(array_key_exists($year, $average_sell_out_jewellery))
	{
		$pdf->Cell(19, 3.5, number_format($average_sell_out_jewellery[$year], 2, ".", "'"), 0, "", "R");
	}
	else
	{
		$pdf->Cell(19, 3.5, "", 0, "", "R");
	}

	$i++;
}	


$y = $y + $standard_y;
$pdf->SetXY($x0,$y);
$pdf->SetFont("arialn", "", 8);
$pdf->Cell(50, 3.5, "In Percent of Frequency", 0, "", "L");

//print values for columns 1 to 5
$i = 1;
foreach($years_page_row as $key=>$year)
{
	$xi = "x" . $i;

	$pdf->SetXY($$xi,$y);
	if(array_key_exists($year, $percent_sell_out_jewellery))
	{
		$pdf->Cell(19, 3.5, number_format(100*$percent_sell_out_jewellery[$year], 2, ".", "'") . "%", 0, "", "R");
	}
	else
	{
		$pdf->Cell(19, 3.5, "", 0, "", "R");
	}

	$i++;
}

$y = $y + $standard_y;
$pdf->SetXY($x0,$y);
$pdf->SetFont("arialn", "", 8);
$pdf->Cell(50, 3.5, "Per Square Meter", 0, "", "L");

//print values for columns 1 to 5
$i = 1;
foreach($years_page_row as $key=>$year)
{
	$xi = "x" . $i;

	$pdf->SetXY($$xi,$y);
	if(array_key_exists($year, $sqm_sell_out_jewellery))
	{
		$pdf->Cell(19, 3.5, number_format($sqm_sell_out_jewellery[$year], 2, ".", "'"), 0, "", "R");
	}
	else
	{
		$pdf->Cell(19, 3.5, "", 0, "", "R");
	}

	$i++;
}

	
?>