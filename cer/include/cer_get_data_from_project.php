<?php

//set defaul values
$legal_entity_name = "";
$project_name = "";
$project_manager = "";
$requested_amount = "";
$budget_amount = "";
$currency_symbol = "";
$investment_amount = "";
$project_start = "";
$net_present_value = "";
$key_money = "";
$project_end = "";
$pay_back_period = "";
$deposit = "";
$post_compl_review = "";
$internal_reate_of_return ="";
$other_cost = "";
$project_kind = "";


$description = "";
$strategy = "";
$investment = "";
$benefits = "";
$alternative = "";
$risks = "";

$name1 = "";
$name2 = "";
$name3 = "";
$name4 = "";
$name5 = "";
$name6 = "";
$name7 = "";
$name8 = "";
$name9 = "";

$date1 = "";
$date2 = "";
$date3 = "";
$date4 = "";
$date5 = "";
$date6 = "";
$date7 = "";
$date8 = "";
$date9 = "";


$protocol_number1 = "";
$protocol_number1 = "";
$cer_number = "";

$opening_date = "";
$takeover_date = "";
$takeover = false;

$project_kind_new = "";
$project_kind_renovation = "";
$project_kind_relocation = "";



// get basic data
$cer_basicdata = get_cer_basicdata(param("pid"));


$approval_name1 = $cer_basicdata["cer_basicdata_approvalname1"];
$approval_name2 = $cer_basicdata["cer_basicdata_approvalname2"];
$approval_name3 = $cer_basicdata["cer_basicdata_approvalname3"];
$approval_name4 = $cer_basicdata["cer_basicdata_approvalname4"];
$approval_name5 = $cer_basicdata["cer_basicdata_approvalname5"];
$approval_name6 = $cer_basicdata["cer_basicdata_approvalname6"];
$approval_name7 = $cer_basicdata["cer_basicdata_approvalname7"];
$approval_name8 = $cer_basicdata["cer_basicdata_approvalname8"];
$approval_name9 = $cer_basicdata["cer_basicdata_approvalname9"];
$approval_name10 = $cer_basicdata["cer_basicdata_approvalname10"];

$approval_name11 = $cer_basicdata["cer_summary_in01_sig01"];
$approval_name12 = $cer_basicdata["cer_summary_in01_sig02"];
$approval_name13 = $cer_basicdata["cer_summary_in01_sig03"];

// get all data needed from cer_basic_data

$sql = "select * from cer_summary " . 
       "where cer_summary_project = " . param("pid") . 
	   " and cer_summary_cer_version = " . $cer_version;
$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{

	$description = $row["cer_summary_in01_description"];
	$strategy = $row["cer_summary_in01_strategy"];
	$investment = $row["cer_summary_in01_investment"];
	$benefits = $row["cer_summary_in01_benefits"];
	$alternative = $row["cer_summary_in01_alternative"];
	$risks = $row["cer_summary_in01_risks"];

	$name1 = $row["cer_summary_in01_sig01"];
	$name2 = $row["cer_summary_in01_sig02"];
	$name3 = $row["cer_summary_in01_sig03"];
	$name4 = $row["cer_summary_in01_sig04"];
	$name5 = $row["cer_summary_in01_sig05"];
	$name6 = $row["cer_summary_in01_sig06"];
	$name7 = $row["cer_summary_in01_sig07"];
	$name8 = $row["cer_summary_in01_sig08"];
	$name9 = $row["cer_summary_in01_sig09"];

	$name7 = $cer_basicdata["cer_basicdata_approvalname11"];

	$date1 = to_system_date($row["cer_summary_in01_date01"]);
	$date2 = to_system_date($row["cer_summary_in01_date02"]);
	$date3 = to_system_date($row["cer_summary_in01_date03"]);
	$date4 = to_system_date($row["cer_summary_in01_date04"]);
	$date5 = to_system_date($row["cer_summary_in01_date05"]);
	$date6 = to_system_date($row["cer_summary_in01_date06"]);
	$date7 = to_system_date($row["cer_summary_in01_date07"]);
	$date8 = to_system_date($row["cer_summary_in01_date08"]);
	$date9 = to_system_date($row["cer_summary_in01_date09"]);


	$protocol_number1 = $row["cer_summary_in01_prot01"];
	$protocol_number2 = $row["cer_summary_in01_prot02"];
	$cer_number = $row["cer_summary_in01_cernr"];

	
	if($row["cer_summary_in01_review_date"] and $row["cer_summary_in01_review_date"] != '0000-00-00')
	{
		$post_compl_review = substr($row["cer_summary_in01_review_date"], 5,2) . "/" . substr($row["cer_summary_in01_review_date"], 0,4);
	}
	
}



//get milestones
$date_ln_approval = "";
$date_cer_request = "";

$sql = "select project_milestone_milestone, project_milestone_date from project_milestones " . 
       "where project_milestone_milestone in(1, 13, 15) " . 
	   " and project_milestone_project = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($row["project_milestone_milestone"] == 1)
	{
		if($row["project_milestone_date"] == NULL or $row["project_milestone_date"] == '0000-00-00')
		{
			$date_cer_request = 'n.a.';
		}
		else
		{
			$date_cer_request = to_system_date($row["project_milestone_date"]);

		}
	}
	elseif($row["project_milestone_milestone"] == 15)
	{
		if($row["project_milestone_date"] != NULL and $row["project_milestone_date"] != '0000-00-00')
		{
			$date_cer_request = to_system_date($row["project_milestone_date"]);
		}
	}
	elseif($row["project_milestone_milestone"] == 13)
	{
		if($row["project_milestone_date"] == NULL or $row["project_milestone_date"] == '0000-00-00')
		{
			$date_ln_approval = 'n.a.';
		}
		else
		{
			$date_ln_approval = to_system_date($row["project_milestone_date"]);
		}
	}
}


// get all data needed from project
$sql = "select * " . 
       "from projects " . 
	   "left join orders on order_id = project_order " .
	   "left join countries on country_id = order_shop_address_country " .
	   "left join product_lines on product_line_id = project_product_line " .
	   "left join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join projectkinds on projectkind_id = project_projectkind " .
       "left join addresses on address_id = order_client_address " .
       "left join users on user_id = project_retail_coordinator " . 
	   "where project_id = " . param("pid");


$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	$legal_entity_name = $row["address_company"];
	
	$project_name = $row["order_shop_address_company"]. ", " . $row["order_shop_address_place"] . ", " . $row["country_name"];


	$project_manager = $row["user_firstname"] . " " . $row["user_name"];
	
	$project_kind = $row["projectkind_name"];

	if($row["project_projectkind"] == 4)
	{
		$takeover = true;
	}


	
	$factor = $cer_basicdata['cer_basicdata_factor'];
	if(!$cer_basicdata['cer_basicdata_factor'])
	{
		$factor = 1;
	}

	$exchange_rate = 1;
	if($cer_basicdata['cer_basicdata_exchangerate'] > 0)
	{
		$exchange_rate = $cer_basicdata['cer_basicdata_exchangerate'];
	}
	
	
	if($cer_basicdata['cer_basicdata_exchangerate']) {
		$budget_amount = $row["project_approximate_budget"]/$cer_basicdata['cer_basicdata_exchangerate']*$factor;
		$budget_amount = number_format($budget_amount, 0, "", "'");
	}
	else {
		$budget_amount = '';
	}

	
	if($row["order_date"]) {
		$project_start = substr($row["order_date"], 5,2) . "/" . substr($row["order_date"], 0,4);
	}
	else {
		$project_start = '';
	}
	

	$tmp = date('Y-m-d H:i:s', strtotime($row["project_real_opening_date"] . ' + 90 day'));

	
	$project_end = substr($tmp, 5,2) . "/" . substr($tmp, 0,4);
	$order_number = $row["project_order"];
	$address_country = $row["address_country"];

	
	$opening_date = to_system_date($row["project_real_opening_date"]);

	if($row["project_planned_takeover_date"] != NULL and $row["project_planned_takeover_date"] != '0000-00-00')
	{
		$takeover_date = to_system_date($row["project_planned_takeover_date"]);
	}

	if($row["project_projectkind"] == 1)  {
		$project_kind_new = "X";
	}
	elseif($project["project_projectkind"] == 6)  {
		$project_kind_relocation = "X";
	}
	elseif($row["project_projectkind"] == 2 or $row["project_projectkind"] == 3)  {
		$project_kind_renovation = "X";
	}


	//relocated POS-Name
	$relocated_pos_name = "";
	if($row["project_relocated_posaddress_id"] > 0)
	{
		$relocated_pos_data = get_relocated_pos_info($row["project_relocated_posaddress_id"]);

			$relocated_pos_name = $relocated_pos_data["posaddress_name"] .", " .
			$relocated_pos_data["posaddress_zip"] . " " .
			$relocated_pos_data["place_name"];
	}
	
	
}

$client_currency = get_cer_currency(param("pid"));
$currency_symbol = $client_currency["symbol"];
$client_address = get_address($project["order_client_address"]);