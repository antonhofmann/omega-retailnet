<?php
/********************************************************************

    calculate_average_bouildout_costs.php

    Calculate average bulding costs

    Created by:     Anton hofmann (anton.hofmann@codingroom.ch)
    Date created:   2017-05-12
    Modified by:    Anton hofmann (anton.hofmann@codingroom.ch)
    Date modified:  2017-05-12
    Version:        1.0.0

    Copyright (c) 20172, RADO, All Rights Reserved.

*********************************************************************/

require_once "../../include/frame.php";
require_once "../../include/page_modal.php";
require_once "get_functions.php";
check_access("has_access_to_cer");


/********************************************************************
    prepare all data needed
*********************************************************************/
if(param("lid")) {
	$lid = param("lid");
}
else
{
	exit;
}

if(param("pid")) {
	$pid = param("pid");
}
else
{
	exit;
}

$project = get_project($pid);

$calculation_modes = array();

$calculation_modes[1] = 'Caclulate Average Based on Approved Investments';
$calculation_modes[2] = 'Caclulate Average Based on Real Costs';

$sqms = array();
$cer_investments = array();
$ln_investments = array();
$project_investments = array();
$project_kinds = array();

//get saved values for this LN/CER
$sql_list = "select * from ln_basicdata_inr03 
         left join possellouts on possellout_id = ln_basicdata_lnr03_profitability_id
         left join posaddresses on posaddress_id = possellout_posaddress_id 
		 left join places on place_id = posaddress_place_id 
	     left join provinces on province_id = place_province 
	     left join countries on posaddress_country = country_id 
	     left join project_costtypes on project_costtype_id = posaddress_ownertype 
	     left join postypes on postype_id = posaddress_store_postype 
		 where ln_basicdata_lnr03_ln_version = 0
		 and posaddress_store_postype = " . $project['project_postype'] .
		 " and ln_basicdata_lnr03_lnbasicdata_id = " . $lid;

$res = mysql_query($sql_list) or dberror($sql_list);
while($row = mysql_fetch_assoc($res))
{

	$sqms[$row['ln_basicdata_lnr03_id']] = $row["posaddress_store_totalsurface"];

	$investment_data = get_pos_investment_data($row["posaddress_id"], $project['project_projectkind'], $project['project_postype']);


	$cer_investments[$row['ln_basicdata_lnr03_id']] = $investment_data['cer_total'];
	$ln_investments[$row['ln_basicdata_lnr03_id']] = $investment_data['ln_total'];
	$project_investments[$row['ln_basicdata_lnr03_id']] = $investment_data['project_total'];

	$latest_project = get_latest_pos_project($row["posaddress_id"]);

	if(count($latest_project) > 0) {
		$sql_p = "select projectkind_name 
			   from projects
			   left join projectkinds on projectkind_id = project_projectkind 
			   where project_id = " . dbquote($latest_project['project_id']);

		$res_p = mysql_query($sql_p) or dberror($sql_p);
		if($row_p = mysql_fetch_assoc($res_p))
		{
			$project_kinds[$row['ln_basicdata_lnr03_id']] = $row_p['projectkind_name'];
		}
		else {
			$project_kinds[$row['ln_basicdata_lnr03_id']] = 'n.a.';
		}
	}
}

/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql_list);
$list->set_title("POS Locations for LNR-03 Distribution Analysis Investments");
$list->set_entity("ln_basicdata_inr03");
$list->set_order("country_name, place_name, posaddress_name");
$list->add_column("place_name", "&nbsp;City", "", LIST_FILTER_FREE,'', COLUMN_UNDERSTAND_HTML|COLUMN_NO_WRAP);
$list->add_column("posaddress_name", "POS Name", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("postype_name", "POS Type", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);
$list->add_column("project_costtype_text", "Legal Type", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);
$list->add_text_column("pk", "Project Type", COLUMN_UNDERSTAND_HTML, $project_kinds);
$list->add_text_column("sqms", "Net Surface", COLUMN_UNDERSTAND_HTML, $sqms);
//$list->add_text_column("lni", "LN", COLUMN_UNDERSTAND_HTML, $ln_investments);
$list->add_text_column("ceri", "CER Approved", COLUMN_UNDERSTAND_HTML, $cer_investments);
$list->add_text_column("proi", "Real Cost&nbsp;", COLUMN_UNDERSTAND_HTML, $project_investments);

$list->populate();
$list->process();

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new form("cer_basicdata", "calculatabc");
$form->add_hidden("pid", $pid);
$form->add_hidden("lid", $lid);
$form->add_hidden("save_form", 1);

if(count($cer_investments) == 0) {
	$form->add_comment("Please first fill in the <a href='/cer/ln_profitability.php?context=ln&pid=" . $pid . "'>profitability analysis</a> of corporate POS locations<br >in case no POS locations are shown here.");
	$form->add_button("cancel", "Cancel", 0);
	
}
else {
	$form->add_section(" ");
	$form->add_list("calculation_mode", "Calculation Mode", $calculation_modes, NOTNULL);
	$form->add_input_submit("submit", "Update Average Buildout Costs", 0);
}



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$list->populate();

if(param("save_form"))
{
	$form->error('Please indicate a calculation mode!');
}
$form->populate();
$form->process();


/********************************************************************
     Render Page
 *********************************************************************/ 
$page_title = "Closest POS by proximity in the area";
$page = new Page_Modal("cer_basicdata");

$page->header();
$page->title($page_title);

$list->render();
$form->render();

if(param("save_form"))
{
	$error = 1;
	if(param('calculation_mode')) {
		
		$abc = 0;
		$tsqms = 0;
		$tinvestments = 0;
		if(param('calculation_mode') == 1) {

			foreach($cer_investments as $key=>$value) {
			
				if($value > 0 and $sqms[$key] > 0) {
					$tinvestments = $tinvestments + $value;
					$tsqms = $tsqms + $sqms[$key];

				}
			}

			if($tinvestments > 0) {
				$abc = round($tinvestments/$tsqms, 2);
			}
		}
		elseif(param('calculation_mode') == 2) {
			
			foreach($project_investments as $key=>$value) {
			
				if($value > 0 and $sqms[$key] > 0) {
					$tinvestments = $tinvestments + $value;
					$tsqms = $tsqms + $sqms[$key];

				}
			}

			if($tinvestments > 0) {
				$abc = round($tinvestments/$tsqms, 2);
			}
		}
		
		if($abc) {
			$sql = "update ln_basicdata set ln_basicdata_average_buildout_costs = " . dbquote($abc) . 
				   " where ln_basicdata_id = " . dbquote(param("lid"));

			$res = mysql_query($sql) or dberror($sql);
		}
		$error = 0;
	}
	
	if($error == 0) {
?>

	<script languege="javascript">
		var back_link = "/cer/ln_general.php?pid=<?php echo param('pid');?>"; 
		$.nyroModalRemove();
	</script>

<?php
	}
}
$page->footer();
