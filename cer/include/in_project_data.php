<?php

if(!isset($ln_version))
{
	$cer_version = 0;
	$ln_version = 0;
}
// get all data needed from project
if(param("pid") > 0 )
{
	$form_type = "CER";
	$sql = "select * " . 
		   "from projects " . 
		   "left join orders on order_id = project_order " .
		   "left join countries on country_id = order_shop_address_country " .
		   "left join product_lines on product_line_id = project_product_line " .
		   "left join project_costs on project_cost_order = project_order " .
		   "left join project_costtypes on project_costtype_id = project_cost_type " .
		   "left join projectkinds on projectkind_id = project_projectkind " .
		   "left join addresses on address_id = order_client_address " .
		   "left join users on user_id = project_retail_coordinator " . 
		   "left join postypes on postype_id = project_postype " . 
		   "where project_id = " . param("pid");


	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		
		if($row["address_legal_entity_name"])
		{
			$legal_entity_name = $row["address_legal_entity_name"];
		}
		else
		{
			$legal_entity_name = $row["address_company"];
		}
		$address_country = $row["address_country"];
		
		$project_name = $row["order_shop_address_company"]. ", " . $row["order_shop_address_place"] . ", " . $row["country_name"];

		$pos_country = $row["country_name"];
		$pos_address = $row["order_shop_address_address"];
		$pos_city = $row["order_shop_address_place"];

		$project_manager = $row["user_name"] . " " . $row["user_firstname"];
		
		//$project_kind = $row["projectkind_name"];

		$project_kind = str_replace('POS', "", $row["projectkind_name"]);


		$pos_type_name = $row["postype_name"];


		$legal_type_name = $row["project_costtype_text"];




		if($row["address_legal_entity_name"])
		{
			$legal_entity = $row["address_legal_entity_name"];
		}
		else
		{
			$legal_entity = $row["address_company"];
		}

		$date_request = to_system_date($row["order_date"]);

		$order_number = $row["project_order"];
		$project_number = $row["project_number"];

		$sales_surface = round($row["project_cost_sqms"], 0);

		$planned_opening_date = to_system_date($row["project_real_opening_date"]);
		

		//corporate, joint venture, cooperation 3rd party
		if($project["project_cost_type"] == 6) // other
		{
			$form_type = "INR03";
		}
		elseif($row["project_cost_type"] == 1 or $row["project_cost_type"] == 3 or $row["project_cost_type"] == 4)
		{
			$form_type = "CER";
		}
		else
		{
			$form_type = "AF";
		}


		// franchisee
		$franchisee_company = "";
		$sql_a = "select * from addresses where address_id = " . dbquote($row["order_franchisee_address_id"]);
		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if($row_a = mysql_fetch_assoc($res_a))
		{
			$franchisee_company = $row_a["address_company"];
		}


		//relocated POS-Name
		$relocated_pos_name = "";
		if($row["project_relocated_posaddress_id"] > 0)
		{
			$relocated_pos_data = get_relocated_pos_info($row["project_relocated_posaddress_id"]);

				$relocated_pos_name = $relocated_pos_data["posaddress_name"] .", " .
				$relocated_pos_data["posaddress_zip"] . " " .
				$relocated_pos_data["place_name"];
		}
	}

	$cer_basicdata = get_cer_basicdata(param("pid"), $cer_version);
}
else {

	$cer_basicdata = get_draft_basicdata(param("did"), $cer_version);

	$form_type = "CER";
	$legal_entity_name = '-';
	$address_country = $cer_basicdata["cer_basicdata_country"];
	$project_name = $cer_basicdata["cer_basicdata_title"];

	$pos_country = '-';
	$pos_address = '-';
	$pos_city = '-';

	$project_manager = '-';
	$project_kind = '-';

	$legal_entity = '-';
	$date_request = '-';
	$order_number = '-';
	$project_number = '-';

	$franchisee_company = "";

	$pos_type_name = '-';
	$relocated_pos_name = '';
	$legal_type_name = '-';
	
	//$sales_surface = round($row["project_cost_sqms"], 0);
	$sales_surface = round($cer_basicdata["cer_basicdata_sqms"], 0);

	$planned_opening_date = '-';
		
	//corporate, joint venture, cooperation 3rd party
	if($cer_basicdata["cer_basicdata_legal_type"] == 6) // other
	{
		$form_type = "INR03";
	}
	elseif($cer_basicdata["cer_basicdata_legal_type"] == 1 or $cer_basicdata["cer_basicdata_legal_type"] == 3 or $cer_basicdata["cer_basicdata_legal_type"] == 4)
	{
		$form_type = "CER";
	}
	else
	{
		$form_type = "AF";
	}
	
	//only for business plan drafts
	$project["project_projectkind"] = 0;
	$project["project_number"] = "draft";
}

if(param("pid") > 0 )
{
	$currency_symbol_page_bottom = get_currency_symbol($cer_basicdata["cer_basicdata_currency"]);
	$exchange_rate_page_bottom = $cer_basicdata["cer_basicdata_exchangerate"];
	if(array_key_exists('cid', $_GET)) {

		if(array_key_exists('cid', $_GET) and $_GET['cid'] == 's') // choose system currency
		{
			$currency_symbol = "CHF";
			$exchange_rate = $cer_basicdata["cer_basicdata_exchangerate"];
			$exchange_rate_factor = $cer_basicdata["cer_basicdata_factor"];

			
			$currency_symbol_page_bottom = get_currency_symbol($cer_basicdata["cer_basicdata_currency"]);;
			$exchange_rate_page_bottom = $cer_basicdata["cer_basicdata_exchangerate"];
			
		}
		elseif(array_key_exists('cid', $_GET) and $_GET['cid'] == 'c2') // choose currency of the POS country
		{
			$currency_symbol = get_currency_symbol($cer_basicdata["cer_basicdata_currency2"]);
			
			$exchange_rate1 = $cer_basicdata["cer_basicdata_exchangerate"];
			$exchange_rate_factor1 = $cer_basicdata["cer_basicdata_factor"];
			$exchange_rate2 = $cer_basicdata["cer_basicdata_exchangerate2"];
			$exchange_rate2_factor = $cer_basicdata["cer_basicdata_factor2"];

			$exchange_rate = $exchange_rate2_factor*$exchange_rate1/$exchange_rate2;
			$exchange_rate_factor = 1;

			$currency_symbol_page_bottom = get_currency_symbol($cer_basicdata["cer_basicdata_currency2"]);
			$exchange_rate_page_bottom = $cer_basicdata["cer_basicdata_exchangerate2"];

		}
		else // chose client's currency
		{
			$currency_symbol = get_currency_symbol($cer_basicdata["cer_basicdata_currency"]);
			$exchange_rate = 1;
			$exchange_rate_factor = 1;
		}
	}
	elseif(isset($cid)) {
		if($cid == 's' and isset($currency_source) and $currency_source == "LN") // choose system currency in LN exchange rate
		{
				$ln_basic_data = get_ln_basicdata(param("pid"), $ln_version);
				$currency_symbol = "CHF";
				$currency_symbol_page_bottom = get_currency_symbol($ln_basic_data["ln_basicdata_currency"]);;

				$exchange_rate = $ln_basic_data["ln_basicdata_exchangerate"];
				$exchange_rate_factor = $ln_basic_data["ln_basicdata_factor"];
				$exchange_rate_page_bottom = $ln_basic_data["ln_basicdata_exchangerate"];
		}
		elseif($cid == 's') // choose system currency
		{
			$currency_symbol = "CHF";
			$exchange_rate = $cer_basicdata["cer_basicdata_exchangerate"];
			$exchange_rate_factor = $cer_basicdata["cer_basicdata_factor"];

			
			$currency_symbol_page_bottom = get_currency_symbol($cer_basicdata["cer_basicdata_currency"]);
			$exchange_rate_page_bottom = $cer_basicdata["cer_basicdata_exchangerate"];
			
		}
		elseif($cid == 'c2') // choose currency of the POS country
		{
			$currency_symbol = get_currency_symbol($cer_basicdata["cer_basicdata_currency2"]);
			
			$exchange_rate1 = $cer_basicdata["cer_basicdata_exchangerate"];
			$exchange_rate_factor1 = $cer_basicdata["cer_basicdata_factor"];
			$exchange_rate2 = $cer_basicdata["cer_basicdata_exchangerate2"];
			$exchange_rate2_factor = $cer_basicdata["cer_basicdata_factor2"];

			$exchange_rate = $exchange_rate2_factor*$exchange_rate1/$exchange_rate2;
			$exchange_rate_factor = 1;

			$currency_symbol_page_bottom = get_currency_symbol($cer_basicdata["cer_basicdata_currency2"]);
			$exchange_rate_page_bottom = $cer_basicdata["cer_basicdata_exchangerate2"];

		}
	}
	else // coose client's currency
	{
		if(isset($currency_source) and $currency_source = "LN") // choose client's currency in LN exchange rate
		{
			$ln_basic_data = get_ln_basicdata(param("pid"), $ln_version);
			$currency_symbol_page_bottom = get_currency_symbol($ln_basic_data["ln_basicdata_currency"]);;
			$exchange_rate_page_bottom = $ln_basic_data["ln_basicdata_exchangerate"];

		}
	
		$currency_symbol = get_currency_symbol($cer_basicdata["cer_basicdata_currency"]);
		$exchange_rate = 1;
		$exchange_rate_factor = 1;

	}
}
else
{

	$currency_symbol_page_bottom = get_currency_symbol($cer_basicdata["cer_basicdata_currency"]);
	$currency_symbol = $currency_symbol_page_bottom;


	if(array_key_exists('cid', $_GET) and $_GET['cid'] == 's') // choose system currency
	{
		$currency_symbol = "CHF";
		$exchange_rate = $cer_basicdata["cer_basicdata_exchangerate"];
		$exchange_rate_factor = $cer_basicdata["cer_basicdata_factor"];

		
		$currency_symbol_page_bottom = get_currency_symbol($cer_basicdata["cer_basicdata_currency"]);
		$exchange_rate_page_bottom = $cer_basicdata["cer_basicdata_exchangerate"];
		
	}
	else
	{
				
		$exchange_rate = 1;
		$exchange_rate_page_bottom = $cer_basicdata["cer_basicdata_exchangerate"];
		$exchange_rate_factor = 1;
	}

	//$exchange_rate = 1;
	//$exchange_rate_page_bottom = 1;
	//$exchange_rate_factor = 1;
}


?>