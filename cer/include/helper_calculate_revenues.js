<script language="Javascript">

	

	function calculate_grid_values(revenue_ids)
	{
		revenue_ids.forEach(function(entry) {
			q = $('#__cer_revenues_cer_revenue_quantity_watches_' + entry).val();
			p = $('#__cer_revenues_cer_revenue_aveargeprice_watches_' + entry).val();
			$('#__cer_revenues_cer_revenue_watches_' + entry).html(q*p);
		});

		revenue_ids.forEach(function(entry) {
			q = $('#__cer_revenues_cer_revenue_quantity_jewellery_' + entry).val();
			p = $('#__cer_revenues_cer_revenue_aveargeprice_jewellery_' + entry).val();
			$('#__cer_revenues_cer_revenue_jewellery_' + entry).html(q*p);
		});


		revenue_ids.forEach(function(entry) {
			a1 = $('#__cer_revenues_cer_revenue_watches_' + entry).html();
			a2 = $('#__cer_revenues_cer_revenue_jewellery_' + entry).html();
			a3 = $('#__cer_revenues_cer_revenue_accessories_' + entry).val();
			a4 = $('#__cer_revenues_cer_revenue_customer_service_' + entry).val();
			$('#__cer_revenues_total_gross_sales_' + entry).html(1*a1+1*a2+1*a3+1*a4);
		});

		column_total = 0;
		revenue_ids.forEach(function(entry) {
			v = $('#__cer_revenues_cer_revenue_quantity_watches_' + entry).val();
			column_total = column_total + 1*v;
		});
		$('#__footer_cer_revenue_quantity_watches').html(numeral(column_total).format('0,0'));

		column_total = 0;
		revenue_ids.forEach(function(entry) {
			v = $('#__cer_revenues_cer_revenue_watches_' + entry).html();
			column_total = column_total + 1*v;
		});
		$('#__footer_cer_revenue_watches').html(numeral(column_total).format('0,0'));

		column_total = 0;
		revenue_ids.forEach(function(entry) {
			v = $('#__cer_revenues_cer_revenue_quantity_jewellery_' + entry).val();
			column_total = column_total + 1*v;
		});
		$('#__footer_cer_revenue_quantity_jewellery').html(numeral(column_total).format('0,0'));

		column_total = 0;
		revenue_ids.forEach(function(entry) {
			v = $('#__cer_revenues_cer_revenue_jewellery_' + entry).html();
			column_total = column_total + 1*v;
		});
		$('#__footer_cer_revenue_jewellery').html(numeral(column_total).format('0,0'));

		column_total = 0;
		revenue_ids.forEach(function(entry) {
			v = $('#__cer_revenues_cer_revenue_accessories_' + entry).val();
			column_total = column_total + 1*v;
		});
		$('#__footer_cer_revenue_accessories').html(numeral(column_total).format('0,0'));

		column_total = 0;
		revenue_ids.forEach(function(entry) {
			v = $('#__cer_revenues_cer_revenue_customer_service_' + entry).val();
			column_total = column_total + 1*v;
		});
		$('#__footer_cer_revenue_customer_service').html(numeral(column_total).format('0,0'));

		column_total = 0;
		revenue_ids.forEach(function(entry) {
			v = $('#__cer_revenues_total_gross_sales_' + entry).html();
			column_total = column_total + 1*v;
		});
		

		$('#__footer_total_gross_sales').html(numeral(column_total).format('0,0'));

	}
	
	
	
	var revenue_ids = new Array();
	var first_revenue_id = 99999999999999;

	var firstyear = <?php echo $cer_basicdata["cer_basicdata_firstyear"];?>;
	var firstmonth = <?php echo $cer_basicdata["cer_basicdata_firstmonth"];?>;
	var lastyear = <?php echo $cer_basicdata["cer_basicdata_lastyear"];?>;
	var lastmonth = <?php echo $cer_basicdata["cer_basicdata_lastmonth"];?>;

	var num_months_first_year = 13 - firstmonth;
	var num_months_last_year = lastmonth;
	var number_of_years = lastyear - firstyear;
	
	<?php 
		foreach($revenue_ids as $key=>$revenue_id)
		{
			if($key == 0)
			{
				echo 'first_revenue_id = 0;';
			}
			echo 'revenue_ids[' . $key . '] = ' . $revenue_id . ';';
			
		}
	?>
	
	

		
	$(document).ready(function(){
		  
		

		$('#cer_basicdata_revenue_p1').change(function() {
			
			var cer_basicdata_revenue_p1 = $("#cer_basicdata_revenue_p1").val();
			
			id0 = '#__cer_revenues_cer_revenue_quantity_watches_' + revenue_ids[0]; 
			var new_val0 = $(id0).val();
			
			id1 = '#__cer_revenues_cer_revenue_quantity_jewellery_' + revenue_ids[0];
			var new_val1 = $(id1).val();
			var counter = 1;
			<?php
			foreach($revenue_ids as $key=>$revenue_id)
			{
				if($key > 0)
				{
					echo 'id00 = "#__cer_revenues_cer_revenue_quantity_watches_' . $revenue_id .'";';
					echo 'if(cer_basicdata_revenue_p1 > 0){';
					
					echo 'if(counter == 1) {';
					echo 'new_val0 = 12*new_val0/num_months_first_year;';
					echo '}';
					echo 'else if(counter == number_of_years) {';
					echo 'new_val0 = num_months_last_year*new_val0/12;';
					echo '}';
					
					echo 'new_val0 = Math.round(1*new_val0 + (new_val0*cer_basicdata_revenue_p1/100));';
					echo '$(id00).val(new_val0);';
					echo '}';



					echo 'id01 = "#__cer_revenues_cer_revenue_quantity_jewellery_' . $revenue_id .'";';
					
					echo 'if(cer_basicdata_revenue_p1 > 0){';

					echo 'if(counter == 1) {';
					echo 'new_val1 = 12*new_val1/num_months_first_year;';
					echo '}';
					echo 'else if(counter == number_of_years) {';
					echo 'new_val1 = num_months_last_year*new_val1/12;';
					echo '}';

					echo 'new_val1 = Math.round(1*new_val1 + (new_val1*cer_basicdata_revenue_p1/100));';
					echo '$(id01).val(new_val1);';
					echo '}';

					echo 'counter++;';

				}
				
			}
			?>
		});

		$('#cer_basicdata_revenue_p2').change(function() {
			
			var cer_basicdata_revenue_p2 = $("#cer_basicdata_revenue_p2").val();

			id2 = '#__cer_revenues_cer_revenue_aveargeprice_watches_' + revenue_ids[0];
			var new_val2 = $(id2).val();

			id3 = '#__cer_revenues_cer_revenue_aveargeprice_jewellery_' + revenue_ids[0];
			var new_val3 = $(id3).val();
			var counter = 1;
			<?php 
			foreach($revenue_ids as $key=>$revenue_id)
			{
				if($key > 0)
				{
					echo 'id02 = "#__cer_revenues_cer_revenue_aveargeprice_watches_' . $revenue_id .'";';
					
					echo 'if(cer_basicdata_revenue_p2 > 0){';
					
					echo 'if(counter == 1) {';
					echo 'new_val2 = 12*new_val2/num_months_first_year;';
					echo '}';
					echo 'else if(counter == number_of_years) {';
					echo 'new_val2 = num_months_last_year*new_val2/12;';
					echo '}';
					
					echo 'new_val2 = Math.round(1*new_val2 + (new_val2*cer_basicdata_revenue_p2/100));';
					echo '$(id02).val(new_val2);';
					echo '};';


					echo 'id03 = "#__cer_revenues_cer_revenue_aveargeprice_jewellery_' . $revenue_id .'";';
					
					echo 'if(cer_basicdata_revenue_p2 > 0){';

					echo 'if(counter == 1) {';
					echo 'new_val3 = 12*new_val3/num_months_first_year;';
					echo '}';
					echo 'else if(counter == number_of_years) {';
					echo 'new_val3 = num_months_last_year*new_val3/12;';
					echo '}';

					echo 'new_val3 = Math.round(1*new_val3 + (new_val3*cer_basicdata_revenue_p2/100));';
					echo '$(id03).val(new_val3);';
					echo '};';

					echo 'counter++;';
				}
				
			}
			?>
		});


		$('#cer_basicdata_revenue_p3').change(function() {

			id4 = '#__cer_revenues_cer_revenue_accessories_' + revenue_ids[0]; 
			var cer_basicdata_revenue_p3 = $("#cer_basicdata_revenue_p3").val();
			var new_val4 = $(id4).val();
			var counter = 1;
			<?php 
			foreach($revenue_ids as $key=>$revenue_id)
			{
				if($key > 0)
				{
					echo 'id04 = "#__cer_revenues_cer_revenue_accessories_' . $revenue_id .'";';
					
					echo 'if(cer_basicdata_revenue_p3 > 0){';
					
					echo 'if(counter == 1) {';
					echo 'new_val4 = 12*new_val4/num_months_first_year;';
					echo '}';
					echo 'else if(counter == number_of_years) {';
					echo 'new_val4 = num_months_last_year*new_val4/12;';
					echo '}';

					echo 'new_val4 = Math.round(1*new_val4 + (new_val4*cer_basicdata_revenue_p3/100));';
					echo '$(id04).val(new_val4);';
					echo '};';

					echo 'counter++;';
				}
				
			}
			?>
		});


		$('#cer_basicdata_revenue_p4').change(function() {

			id5 = '#__cer_revenues_cer_revenue_customer_service_' + revenue_ids[0]; 
			var cer_basicdata_revenue_p4 = $("#cer_basicdata_revenue_p4").val();
			var new_val5 = $(id5).val();
			var counter = 1;
			<?php 
			foreach($revenue_ids as $key=>$revenue_id)
			{
				if($key > 0)
				{
					echo 'id05 = "#__cer_revenues_cer_revenue_customer_service_' . $revenue_id .'";';
					
					echo 'if(cer_basicdata_revenue_p4 > 0){';
					
					echo 'if(counter == 1) {';
					echo 'new_val5 = 12*new_val5/num_months_first_year;';
					echo '}';
					echo 'else if(counter == number_of_years) {';
					echo 'new_val5 = num_months_last_year*new_val5/12;';
					echo '}';
					
					echo 'new_val5 = Math.round(1*new_val5 + (new_val5*cer_basicdata_revenue_p4/100));';
					echo '$(id05).val(new_val5);';
					echo '};';

					echo 'counter++;';
				}
				
			}
			?>


		});
		


		if(first_revenue_id == 0)
		{
		  id0 = '#__cer_revenues_cer_revenue_quantity_watches_' + revenue_ids[0]; 
		  $(id0).change(function() {
				
				var cer_basicdata_revenue_p1 = $("#cer_basicdata_revenue_p1").val();
				var new_val0 = $(id0).val();
				var counter = 1;
				<?php 
				foreach($revenue_ids as $key=>$revenue_id)
				{
					if($key > 0)
					{
						echo 'id00 = "#__cer_revenues_cer_revenue_quantity_watches_' . $revenue_id .'";';
						echo 'if(cer_basicdata_revenue_p1 > 0){';
						
						echo 'if(counter == 1) {';
						echo 'new_val0 = 12*new_val0/num_months_first_year;';
						echo '}';
						echo 'else if(counter == number_of_years) {';
						echo 'new_val0 = num_months_last_year*new_val0/12;';
						echo '}';
						
						echo 'new_val0 = Math.round(1*new_val0 + (new_val0*cer_basicdata_revenue_p1/100));';
						echo '$(id00).val(new_val0);';
						echo '};';

						echo 'counter++;';
					}
					
				}
				?>

		  });

		  id1 = '#__cer_revenues_cer_revenue_quantity_jewellery_' + revenue_ids[0]; 
		  $(id1).change(function() {
				
				var cer_basicdata_revenue_p1 = $("#cer_basicdata_revenue_p1").val();
				var new_val1 = $(id1).val();
				var counter = 1;
				

				<?php 
				foreach($revenue_ids as $key=>$revenue_id)
				{
					if($key > 0)
					{
						echo 'id01 = "#__cer_revenues_cer_revenue_quantity_jewellery_' . $revenue_id .'";';
						
						echo 'if(cer_basicdata_revenue_p1 > 0){';
						
						echo 'if(counter == 1) {';
						echo 'new_val1 = 12*new_val1/num_months_first_year;';
						echo '}';
						echo 'else if(counter == number_of_years) {';
						echo 'new_val1 = num_months_last_year*new_val1/12;';
						echo '}';

						echo 'new_val1 = Math.round(1*new_val1 + (new_val1*cer_basicdata_revenue_p1/100));';
						echo '$(id01).val(new_val1);';
						echo '};';

						echo 'counter++;';
					}
					
				}
				?>
		  });



		  id2 = '#__cer_revenues_cer_revenue_aveargeprice_watches_' + revenue_ids[0]; 
		  $(id2).change(function() {
				
				var cer_basicdata_revenue_p2 = $("#cer_basicdata_revenue_p2").val();
				var new_val2 = $(id2).val();
				var counter = 1;
				

				<?php 
				foreach($revenue_ids as $key=>$revenue_id)
				{
					if($key > 0)
					{
						echo 'id02 = "#__cer_revenues_cer_revenue_aveargeprice_watches_' . $revenue_id .'";';
						
						echo 'if(cer_basicdata_revenue_p2 > 0){';

						echo 'if(counter == 1) {';
						echo 'new_val2 = 12*new_val2/num_months_first_year;';
						echo '}';
						echo 'else if(counter == number_of_years) {';
						echo 'new_val2 = num_months_last_year*new_val2/12;';
						echo '}';

						echo 'new_val2 = Math.round(1*new_val2 + (new_val2*cer_basicdata_revenue_p2/100));';
						echo '$(id02).val(new_val2);';
						echo '};';

						echo 'counter++;';
					}
					
				}
				?>
		  });


		  id3 = '#__cer_revenues_cer_revenue_aveargeprice_jewellery_' + revenue_ids[0]; 
		  $(id3).change(function() {
				
				var cer_basicdata_revenue_p2 = $("#cer_basicdata_revenue_p2").val();
				var new_val3 = $(id3).val();
				var counter = 1;
				

				<?php 
				foreach($revenue_ids as $key=>$revenue_id)
				{
					if($key > 0)
					{
						echo 'id03 = "#__cer_revenues_cer_revenue_aveargeprice_jewellery_' . $revenue_id .'";';
						
						echo 'if(cer_basicdata_revenue_p2 > 0){';
						
						echo 'if(counter == 1) {';
						echo 'new_val3 = 12*new_val3/num_months_first_year;';
						echo '}';
						echo 'else if(counter == number_of_years) {';
						echo 'new_val3 = num_months_last_year*new_val3/12;';
						echo '}';

						echo 'new_val3 = Math.round(1*new_val3 + (new_val3*cer_basicdata_revenue_p2/100));';
						echo '$(id03).val(new_val3);';
						echo '};';

						echo 'counter++;';
					}
					
				}
				?>
		  });


		  id4 = '#__cer_revenues_cer_revenue_accessories_' + revenue_ids[0]; 
		  $(id4).change(function() {
				
				var cer_basicdata_revenue_p3 = $("#cer_basicdata_revenue_p3").val();
				var new_val4 = $(id4).val();
				var counter = 1;
				<?php 
				foreach($revenue_ids as $key=>$revenue_id)
				{
					if($key > 0)
					{
						echo 'id04 = "#__cer_revenues_cer_revenue_accessories_' . $revenue_id .'";';
						
						echo 'if(cer_basicdata_revenue_p3 > 0){';

						echo 'if(counter == 1) {';
						echo 'new_val4 = 12*new_val4/num_months_first_year;';
						echo '}';
						echo 'else if(counter == number_of_years) {';
						echo 'new_val4 = num_months_last_year*new_val4/12;';
						echo '}';

						echo 'new_val4 = Math.round(1*new_val4 + (new_val4*cer_basicdata_revenue_p3/100));';
						echo '$(id04).val(new_val4);';
						echo '};';

						echo 'counter++;';
					}
					
				}
				?>

		  });


		  id5 = '#__cer_revenues_cer_revenue_customer_service_' + revenue_ids[0]; 
		  $(id5).change(function() {
				
				var cer_basicdata_revenue_p4 = $("#cer_basicdata_revenue_p4").val();
				var new_val5 = $(id5).val();
				var counter = 1;
				

				<?php 
				foreach($revenue_ids as $key=>$revenue_id)
				{
					if($key > 0)
					{
						echo 'id05 = "#__cer_revenues_cer_revenue_customer_service_' . $revenue_id .'";';
						
						echo 'if(cer_basicdata_revenue_p4 > 0){';

						echo 'if(counter == 1) {';
						echo 'new_val5 = 12*new_val5/num_months_first_year;';
						echo '}';
						echo 'else if(counter == number_of_years) {';
						echo 'new_val5 = num_months_last_year*new_val5/12;';
						echo '}';


						echo 'new_val5 = Math.round(1*new_val5 + (new_val5*cer_basicdata_revenue_p4/100));';
						echo '$(id05).val(new_val5);';
						echo '};';

						echo 'counter++;';
					}
					
				}
				?>
		  });
		



			id6 = '#__cer_revenues_cer_revenue_total_days_open_per_year_' + revenue_ids[0]; 
			$(id6).change(function() {
				var new_val6 = $(id6).val();
				<?php 
				foreach($revenue_ids as $key=>$revenue_id)
				{
					if($key > 0)
					{
						echo 'id06 = "#__cer_revenues_cer_revenue_total_days_open_per_year_' . $revenue_id .'";';
						
						echo 'if(new_val6 > 0){';
						echo '$(id06).val(new_val6);';
						echo '};';
					}
					
				}
				?>
			});

			id7 = '#__cer_revenues_cer_revenue_total_hours_open_per_week_' + revenue_ids[0]; 
			$(id7).change(function() {
				var new_val7 = $(id7).val();
				<?php 
				foreach($revenue_ids as $key=>$revenue_id)
				{
					if($key > 0)
					{
						echo 'id07 = "#__cer_revenues_cer_revenue_total_hours_open_per_week_' . $revenue_id .'";';
						
						echo 'if(new_val7 > 0){';
						echo '$(id07).val(new_val7);';
						echo '};';
					}
					
				}
				?>
			});

			id8 = '#__cer_revenues_cer_revenue_total_workinghours_per_week_' + revenue_ids[0]; 
			$(id8).change(function() {
				var new_val8 = $(id8).val();
				<?php 
				foreach($revenue_ids as $key=>$revenue_id)
				{
					if($key > 0)
					{
						echo 'id08 = "#__cer_revenues_cer_revenue_total_workinghours_per_week_' . $revenue_id .'";';
						
						echo 'if(new_val8 > 0){';
						echo '$(id08).val(new_val8);';
						echo '};';
					}
					
				}
				?>
			});
		}


		<?php 
		foreach($revenue_ids as $key=>$revenue_id)
		{
			echo 'id = "#__cer_revenues_cer_revenue_quantity_watches_' . $revenue_id .'";';
			echo '$(id).change(function() {calculate_grid_values(revenue_ids)});';

			echo 'id = "#__cer_revenues_cer_revenue_aveargeprice_watches_' . $revenue_id .'";';
			echo '$(id).change(function() {calculate_grid_values(revenue_ids)});';

			echo 'id = "#__cer_revenues_cer_revenue_quantity_jewellery_' . $revenue_id .'";';
			echo '$(id).change(function() {calculate_grid_values(revenue_ids)});';

			echo 'id = "#__cer_revenues_cer_revenue_aveargeprice_jewellery_' . $revenue_id .'";';
			echo '$(id).change(function() {calculate_grid_values(revenue_ids)});';

			echo 'id = "#__cer_revenues_cer_revenue_accessories_' . $revenue_id .'";';
			echo '$(id).change(function() {calculate_grid_values(revenue_ids)});';

			echo 'id = "#__cer_revenues_cer_revenue_customer_service_' . $revenue_id .'";';
			echo '$(id).change(function() {calculate_grid_values(revenue_ids)});';
		}
		?>
	});

</script>