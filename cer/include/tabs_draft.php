<?php


$page->add_tab("genral", "General", "cer_draft.php?did=" . param("did"), $target = "_self", $flags = 0);
$page->add_tab("investment", "Key Money/Investment", "cer_draft_investments.php?did=" . param("did"), $target = "_self", $flags = 0);
$page->add_tab("rental", "Rental Costs", "cer_draft_rental.php?did=" . param("did"), $target = "_self", $flags = 0);



if(count($cer_brands) > 0)
{
	$page->add_tab_list("brand_selector", "Revenues", "", param("brand"), 0, $cer_brands);
}
else
{
	foreach($cer_brands as $cer_brand_id=>$cer_brand_name)
	{
		$page->add_tab("revenues_" . $cer_brand_id, "Revenues " . $cer_brand_name, "cer_application_revenues.php?pid=" . param("pid"). "&brand=" . $cer_brand_id, $target = "_self", $flags = 0);
	}
}
if(has_access("has_access_to_human_resources"))
{
	$page->add_tab("salaries", "Human Resources", "cer_draft_salaries.php?did=" . param("did"), $target = "_self", $flags = 0);
}
$page->add_tab("expenses", "Expenses", "cer_draft_expenses.php?did=" . param("did"), $target = "_self", $flags = 0);
$page->add_tab("cash", "Calculation Parameters", "cer_draft_cashflow.php?did=" . param("did"), $target = "_self", $flags = 0);
$page->add_tab("files", "Files", "cer_draft_files.php?did=" . param("did"), $target = "_self", $flags = 0);
$page->tabs();
echo '<br />';


?>