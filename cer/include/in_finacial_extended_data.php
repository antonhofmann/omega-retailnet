<?php

$total_hours_open_per_year = array();
$average_staff_opening_hours = array();
$average_sell_out_watches = array();
$average_sell_out_jewellery = array();
$average_sell_out_fliflak = array();
$percent_sell_out_watches = array();
$percent_sell_out_jewellery = array();
$sqm_sell_out_watches = array();
$sqm_sell_out_jewellery = array();


//calculate total hour open per year
foreach($years_page_row as $key=>$year)
{
	$total_hours_open_per_year[$year] = 0;
	if(array_key_exists($year, $cer_revenue_total_hours_open_per_week))
	{
		if($year == $cer_basicdata["cer_basicdata_firstyear"])
		{
			$missing_months = $cer_basicdata["cer_basicdata_firstmonth"] -1;
			$num_of_weeks = 52 - ($missing_months/12) * 52;
		}
		elseif($year == $cer_basicdata["cer_basicdata_lastyear"])
		{
			$missing_months = 12 - $cer_basicdata["cer_basicdata_lastmonth"];
			$num_of_weeks = 52 - ($missing_months/12) * 52;
		}
		else
		{
			$num_of_weeks = 52;
		}

		$total_hours_open_per_year[$year] = $num_of_weeks*$cer_revenue_total_hours_open_per_week[$year];
	}

	
}

//calculate average staff opening hours
foreach($years_page_row as $key=>$year)
{
	$average_staff_opening_hours[$year] = 0;
	if(array_key_exists($year, $total_working_hours_per_week_all_staff)
		and array_key_exists($year, $cer_revenue_total_hours_open_per_week) and $cer_revenue_total_hours_open_per_week[$year] > 0)
	{
		$average_staff_opening_hours[$year] = $total_working_hours_per_week_all_staff[$year] / $cer_revenue_total_hours_open_per_week[$year];
	}
}


//calculate average sell outs watches
foreach($years_page_row as $key=>$year)
{
	$average_sell_out_watches[$year] = 0;
	$percent_sell_out_watches[$year] = 0;
	$sqm_sell_out_watches[$year] = 0;
	if(array_key_exists($year, $sales_units_watches_values)
		and array_key_exists($year, $cer_revenue_total_days_open_per_year) and $cer_revenue_total_days_open_per_year[$year] > 0)
	{
		$average_sell_out_watches[$year] = $sales_units_watches_values[$year] / $cer_revenue_total_days_open_per_year[$year];
	}

	if(array_key_exists($year, $average_sell_out_watches)
		and array_key_exists($year, $cer_revenue_customer_frequency) and $cer_revenue_customer_frequency[$year] > 0)
	{
		$percent_sell_out_watches[$year] = $average_sell_out_watches[$year] / $cer_revenue_customer_frequency[$year];
	}

	if(array_key_exists($year, $sales_units_watches_values) and $sales_surface > 0)
	{
		$sqm_sell_out_watches[$year] = $sales_units_watches_values[$year] / $sales_surface;
	}
}


//calculate average sell outs jewellery
foreach($years_page_row as $key=>$year)
{
	$average_sell_out_jewellery[$year] = 0;
	$percent_sell_out_jewellery[$year] = 0;
	$sqm_sell_out_jewellery[$year] = 0;
	if(array_key_exists($year, $sales_units_jewellery_values)
		and array_key_exists($year, $cer_revenue_total_days_open_per_year) and $cer_revenue_total_days_open_per_year[$year] > 0)
	{
		$average_sell_out_jewellery[$year] = $sales_units_jewellery_values[$year] / $cer_revenue_total_days_open_per_year[$year];
	}

	if(array_key_exists($year, $average_sell_out_jewellery)
		and array_key_exists($year, $cer_revenue_customer_frequency) and $cer_revenue_customer_frequency[$year] > 0)
	{
		$percent_sell_out_jewellery[$year] = $average_sell_out_jewellery[$year] / $cer_revenue_customer_frequency[$year];
	}

	if(array_key_exists($year, $sales_units_jewellery_values) and $sales_surface > 0)
	{
		$sqm_sell_out_jewellery[$year] = $sales_units_jewellery_values[$year] / $sales_surface;
	}
}