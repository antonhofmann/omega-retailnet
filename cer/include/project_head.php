<?php
// read project and order details
if(!isset($project))
{
	$project = get_project(param("pid"));
}
$order_state_name = get_actual_order_state_name($project["order_actual_order_state_code"], 1);



$client_address = get_address($project["order_client_address"]);

$franchisee_address = get_address($project["order_franchisee_address_id"]);

$client = $client_address["company"] . ", " .
          $client_address["zip"] . " " . $client_address["place"] . ", " .
          $client_address["country_name"];

$franchisee = $franchisee_address["company"] . ", " .
		  $franchisee_address["zip"] . " " . $franchisee_address["place"] . ", " .
          $franchisee_address["country_name"];


if(has_access("can_edit_posindex") or has_access("can_view_posindex"))
{
	$link = '<a href="/pos/poscompany.php?country=' . $project["order_franchisee_address_country"] . '&address_filter=a&id=' . $project["order_franchisee_address_id"]. '" target="_blank">' . $franchisee . '</a>';
	
	$franchisee = $link;
}
elseif(has_access("can_view_his_posindex") or has_access("can_edit_his_posindex"))
{

	$user = get_user(user_id());

	$country_filter = "";
	$tmp = array();
	$sql_c = "select * from country_access " .
		   "where country_access_user = " . user_id();


	$res = mysql_query($sql_c) or dberror($sql_c);

	while ($row = mysql_fetch_assoc($res))
	{            
		$tmp[] = $row["country_access_country"];
	}
	
	$tmp[] = $user["country"];
	if(in_array($project["order_franchisee_address_country"], $tmp))
	{
		$link = '<a href="/pos/poscompany.php?country=' . $project["order_franchisee_address_country"] . '&address_filter=a&id=' . $project["order_franchisee_address_id"]. '" target="_blank">' . $franchisee . '</a>';
	
		$franchisee = $link;
	}
}

if(($project["project_projectkind"] == 1 
     and $project["project_actual_opening_date"] != NULL 
	 and $project["project_actual_opening_date"] != '0000-00-00') 
	or $project["project_projectkind"] == 2 
    or $project["project_projectkind"] == 3
	or $project["project_projectkind"] == 4
	or $project["project_projectkind"] == 7) // renovation or takeover/renovation or takeover, lease renewal, equipment
{
	$renovated_pos_id = get_renovated_pos_info($project["project_order"]);
	if ($renovated_pos_id > 0)
	{
		 
		$shop_address = $project["order_shop_address_zip"] . " " .
						$project["order_shop_address_place"] . ", " .
						$project["order_shop_address_country_name"];

		$tmp = $shop_address;
		if (has_access("can_edit_pos_data") or has_access("can_view_pos_data"))
		{	
			$tmp = '<a href="../pos/posindex_pos.php?country=' . $project["order_shop_address_country"] . '&ltf=all&ostate=&province=&id=' . $renovated_pos_id . '" target="_blank">' . $project["order_shop_address_company"] . '</a>, ' . $shop_address;
		}
		$form->add_label("shop_address_label", "POS Location Address", RENDER_HTML, $tmp);
	}
	else
	{	
		$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];
		$form->add_label("shop_address_label", "POS Location Address", 0, $shop);
	}
}
else
{
	$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];
	$form->add_label("shop_address_label", "POS Location Address", 0, $shop);
}

if($project["project_projectkind"] == 6 and $project["project_relocated_posaddress_id"] > 0) // relocation
{
	$relocated_pos = get_relocated_pos_info($project["project_relocated_posaddress_id"]);

	if (count($relocated_pos) > 0)
	{
		
		$shop1 = $relocated_pos["posaddress_name"];
		$shop2 = ", " .
				$relocated_pos["posaddress_zip"] . " " .
				$relocated_pos["place_name"] . ", " .
				$relocated_pos["country_name"];
	
		if (has_access("can_edit_pos_data") or has_access("can_view_pos_data"))
		{ 
			$tmp = '<a href="../pos/posindex_pos.php?country=' . $project["order_shop_address_country"] . '&ltf=all&ostate=&province=&id=' . $project["project_relocated_posaddress_id"] . '" target="_blank">' . $shop1 . '</a>' . $shop2;
			
			$form->add_label("relocated_pos_label", "Relocated POS", RENDER_HTML, $tmp );
		}
		else
		{
			$form->add_label("relocated_pos_label", "Relocated POS", 0, $shop );
		}
	}
}


$form->add_label("client_address1_label", "Client", RENDER_HTML, $client);


if(!isset($donotshowfranchisee)) {

	if($project["project_cost_type"] != 6)
	{
		$form->add_label("franchisee_address_label", "Franchisee", RENDER_HTML, $franchisee);
	}
	else
	{
		$form->add_label("franchisee_address_label", "Owner Company", RENDER_HTML, $franchisee);
	}
}

$form->add_label("type3_label", "Project Legal Type / Project Kind", 0, $project["project_costtype_text"] . " / " . $project["projectkind_name"]);

if($project["possubclass_name"])
{
	$form->add_label("project_postype_label", "POS Type / Subclass", 0, $project["postype_name"] . " / " . $project["possubclass_name"]);
}
else
{
	$form->add_label("project_postype_label", "POS Type / Subclass", 0, $project["postype_name"]);
}

if($project["product_line_name"] and $project["productline_subclass_name"])
{
	$form->add_label("product_line_label", "Product Line / Subclass", 0, $project["product_line_name"] . " / " . $project["productline_subclass_name"]);
}
elseif($project["product_line_name"])
{
	$form->add_label("product_line_label", "Product Line / Subclass", 0, $project["product_line_name"]);
}
else
{
	$form->add_label("product_line_label", "Product Line / Subclass", 0, "");
}

$form->add_label("project_number_label", "Project Number  /Treatment State", RENDER_HTML, "<span class='text'><strong>" . $project["project_number"] . "</strong></span> / " . $project["project_state_text"]);


$form->add_label("status_label", "Project Status", 0, $project["order_actual_order_state_code"]  . " " . $order_state_name);

$form->add_label("staff_label", "Project Manager / Logistics Coordinator", 0, $project["project_manager"] . " / " . $project["operator"]);

$form->add_section(" ");

$form->add_label("submitted_by_label", "Project Starting Date", 0, to_system_date($project["order_date"])  . " owned by " . $project["submitted_by"]);

if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
{
	if($project["project_real_opening_date"] != NULL 
		and $project["project_real_opening_date"] != '0000-00-00'
	    and $project["project_actual_opening_date"] != NULL 
		and $project["project_actual_opening_date"] != '0000-00-00')
	{
		$form->add_label("real_opening_date_label", $project["projectkind_milestone_name_01"] . " / " . $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_real_opening_date"]) . " / " . to_system_date($project["project_actual_opening_date"]));
	}
	elseif($project["project_real_opening_date"] != NULL 
		and $project["project_real_opening_date"] != '0000-00-00')
	{
		$form->add_label("real_opening_date_label", $project["projectkind_milestone_name_01"] . " / " . $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_real_opening_date"]));
	}
	else
	{
		$form->add_label("real_opening_date_label", $project["projectkind_milestone_name_01"] . " / " . $project["projectkind_milestone_name_02"], 0, "");
	}
}
elseif($project["project_projectkind"] == 3) //Take Over and renovation
{
	$form->add_label("planned_takeover_date_label", "Client's Preferred Takeover Date", 0, to_system_date($project["project_planned_takeover_date"]));
	
	if($project["project_real_opening_date"] != NULL 
		and $project["project_real_opening_date"] != '0000-00-00'
	    and $project["project_actual_opening_date"] != NULL 
		and $project["project_actual_opening_date"] != '0000-00-00')
	{
		$form->add_label("planned_opening_date_label", "Client's Preferred / Agreed / Actual Opening Date", 0, to_system_date($project["project_planned_opening_date"]) . " / " . to_system_date($project["project_real_opening_date"]) . " / " . to_system_date($project["project_actual_opening_date"]));
	}
	elseif($project["project_real_opening_date"] != NULL 
		and $project["project_real_opening_date"] != '0000-00-00')
	{
		$form->add_label("planned_opening_date_label", "Client's Preferred / Agreed / Actual Opening Date", 0, to_system_date($project["project_planned_opening_date"]) . " / " . to_system_date($project["project_real_opening_date"]));
	}
	else
	{
		$form->add_label("planned_opening_date_label", "Client's Preferred / Agreed / Actual Opening Date", 0, to_system_date($project["project_planned_opening_date"]));
	}
}
else
{
	if($project["project_real_opening_date"] != NULL 
		and $project["project_real_opening_date"] != '0000-00-00'
	    and $project["project_actual_opening_date"] != NULL 
		and $project["project_actual_opening_date"] != '0000-00-00')
	{
		$form->add_label("planned_opening_date_label", "Client's Preferred / Agreed / Actual Opening Date", 0, to_system_date($project["project_planned_opening_date"]) . " / " . to_system_date($project["project_real_opening_date"]) . " / " . to_system_date($project["project_actual_opening_date"]));
	}
	elseif($project["project_real_opening_date"] != NULL 
		and $project["project_real_opening_date"] != '0000-00-00')
	{
		$form->add_label("planned_opening_date_label", "Client's Preferred / Agreed / Actual Opening Date", 0, to_system_date($project["project_planned_opening_date"]) . " / " . to_system_date($project["project_real_opening_date"]));
	}
	else
	{
		$form->add_label("planned_opening_date_label", "Client's Preferred / Agreed / Actual Opening Date", 0, to_system_date($project["project_planned_opening_date"]));
	}
}

$form->add_label("sqm_label", "Gross/Total/Sales/Other Surface in sqms", 0, $project["project_cost_gross_sqms"] . "/" . $project["project_cost_totalsqms"] . "/" . $project["project_cost_sqms"]. "/" . $project["project_cost_othersqms"]);

$form->add_section(" ");

$tmp =  $cer_basicdata["cer_basicdata_firstmonth"] . "/" . $cer_basicdata["cer_basicdata_firstyear"] . " - " .
$cer_basicdata["cer_basicdata_lastmonth"] . "/" . $cer_basicdata["cer_basicdata_lastyear"] . " / " . $business_plan_period2;
$form->add_label("business_plan_period_label", "Business Plan Period", 0, $tmp);

$posdata = get_pos_data($project["project_order"]);
$posleases = get_pos_leasedata($posdata["posaddress_id"], $project["project_order"]);
if(count($posleases) > 0)
{
	$form->add_label("l_startdate_label", 'Lease Start Date / Expiry Date / Rental Duration', 0, to_system_date($posleases["poslease_startdate"]) . " / " . to_system_date($posleases["poslease_enddate"]) . " / " . $posleases["rental_duration"]);
}

?>