<script language="Javascript">

	
	
	function difference_in_days(fy,fm,fd,ty,tm,td)
	{
		var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
		var firstDate = new Date(fy,fm,fd);
		var secondDate = new Date(ty,tm,td);

		var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
		return diffDays;
	}


	function NumberOfdaysInYear(year) {
		if(year % 4 === 0 && (year % 100 !== 0 || year % 400 === 0)) {
			// Leap year
			return 366;
		} else {
			// Not a leap year
			return 365;
		}
	}

	

	function getDaysInMonth(m, y) {
	   return /8|3|5|10/.test(--m)?30:m==1?(!(y%4)&&y%100)||!(y%400)?29:28:31;
	}


	
	
	function calculacte_additional_rental_cost_type(id, type)
	{
		var amount = $("#__record_cer_additional_rental_cost_amount_" + id).val();
		var unit = $("#__record_cer_additional_rental_cost_unit_" + id).val();
		var increase = $("#__record_cer_additional_rental_cost_increase_" + id).val();
		var period = $("#__record_cer_additional_rental_cost_period_" + id).val();


		<?php
		if(param("pid") > 0)
		{
		?>
			var surface_in_sqm = <?php echo $project["project_cost_gross_sqms"];?>;
			var surface_in_feet = <?php echo 10.7639104*$project["project_cost_gross_sqms"];?>;
		<?php
		}
		else
		{
		?>
			var surface_in_sqm = $("#cer_fixed_rent_pos_surface").val();
			var surface_in_feet = 10.7639104*surface_in_sqm;
		<?php
		}
		?>



		
		//calculate number of months in first year and last year
		var first_year = <?php echo $first_year;?>;
		var first_month = <?php echo $first_month;?>;
		var first_day = <?php echo $first_day;?>;
		var last_year = <?php echo $last_year;?>;
		var last_month = <?php echo $last_month;?>;
		var last_day = <?php echo $last_day;?>;
		
		var number_of_months_first_year = 13 - first_month;
		var number_of_months_last_year = last_month;

		var month_share_first_year = (1+getDaysInMonth(first_month, first_year)- first_day)/getDaysInMonth(first_month, first_year);
		number_of_months_first_year = number_of_months_first_year - 1 + month_share_first_year;
		
		var month_share_last_year = (last_day)/getDaysInMonth(last_month, last_year);
		number_of_months_last_year = number_of_months_last_year - 1 + month_share_last_year;


		//calculate total amount per month
		var update = true;
		var amount_per_month = 0;
		if(unit == 1) // square foot and month
		{
			var total_amount = surface_in_feet*amount;
			var amount_per_month = total_amount;
		}
		else if(unit == 2) // square meter and month
		{
			var total_amount = surface_in_sqm*amount;
			var amount_per_month = total_amount;
		}
		else if(unit == 3) // square foot and year
		{
			var amount_per_month = surface_in_feet*amount/12;
		}
		else if(unit == 4) // square meter and year
		{
			var amount_per_month = surface_in_sqm*amount/12;
		}
		else if(unit == 5) // total surface per month
		{
			var amount_per_month = amount;
		}
		else if(unit == 6) // total surface per year
		{
			var amount_per_month = amount/12;
		}

		var total_amount_per_year = 12*amount_per_month;

		
		//initialize array of all years
		var cost_per_year = new Array();
		<?php
		for($year=$first_year;$year<=$last_year;$year++)
		{
		?>
			cost_per_year[<?php echo $year;?>] = 0;
		<?php
		}
		?>
		
		
		
		
		
		//calculate amounts for each year
		
		<?php
		for($year=$first_year;$year<=$last_year;$year++)
		{
		?>
				
				if(increase > 0 && <?php echo $year;?>> first_year)
				{
					//increase_amount = total_amount_per_year*increase/100;
					//total_amount_per_year = total_amount_per_year + increase_amount;

					increase_amount = amount_per_month*increase/100;
					amount_per_month = 1*amount_per_month + 1*increase_amount;

				}

				if(first_year == last_year)
				{
					//tmp = (1+difference_in_days(first_year,first_month,first_day,last_year,last_month,last_day))/NumberOfdaysInYear(first_year);
					//cost_per_year[first_year] = tmp*total_amount_per_year;

					cost_per_year[first_year] = (last_month - first_month + 1)*amount_per_month;
				}
				else if(<?php echo $year;?> == first_year)
				{
					//tmp = (1+difference_in_days(first_year,first_month,first_day,first_year,12,31))/NumberOfdaysInYear(first_year);
					//cost_per_year[first_year] = tmp*total_amount_per_year;

					cost_per_year[first_year] = number_of_months_first_year*amount_per_month;

				}
				else if(<?php echo $year;?> == last_year)
				{
					
					var increase_correction = 0;
					if(increase > 0 && period == 2) // lease year, correct yearly increase to a part of the year
					{
						//total_amount_per_year = total_amount_per_year - increase_amount;

						increase_correction = (13-number_of_months_last_year)*increase_amount;
					}
					
					//tmp = (1+difference_in_days(last_year,1,1,last_year,last_month,last_day))/NumberOfdaysInYear(last_year);
					//cost_per_year[last_year] = tmp*(total_amount_per_year);
					
					cost_per_year[<?php echo $year;?>] = number_of_months_last_year*amount_per_month - increase_correction;
				}
				else
				{
					var increase_correction = 0;
					if(increase > 0 && period == 2) // lease year, correct yearly increase to a part of the year
					{
						//days_of_the_first_part = NumberOfdaysInYear(<?php echo $year;?>) - difference_in_days(<?php echo $year;?>,first_month,first_day,<?php echo $year;?>,12,31) - 1;
						//increase_correction = (increase_amount*days_of_the_first_part/NumberOfdaysInYear(<?php echo $year;?>));

						increase_correction = number_of_months_first_year*increase_amount;

					}
					//cost_per_year[<?php echo $year;?>] = total_amount_per_year - increase_correction;
					cost_per_year[<?php echo $year;?>] = 12*amount_per_month - increase_correction;
				}
		
		<?php
		}
		?>
		

		//update data fields
		if(update == true)
		{

			<?php
			for($year=$first_year;$year<=$last_year;$year++)
			{
			?>
				var tmp_id = "#__record_ac_amounts_<?php echo $year;?>_" + type;
			
				$(tmp_id).val(Math.round(cost_per_year[<?php echo $year;?>]));
			
			<?php
			}
			?>
		}
	}

	$(document).ready(function(){

		


		<?php
		  foreach($ac_fields as $cer_additional_rental_cost_id=>$additional_rental_cost_id)
		  {
		  ?>

				$("#__record_cer_additional_rental_cost_amount_<?php echo $cer_additional_rental_cost_id;?>" ).change(function() {
				
					calculacte_additional_rental_cost_type(<?php echo $cer_additional_rental_cost_id;?>, <?php echo $additional_rental_cost_id;?>)					
				});

				$("#__record_cer_additional_rental_cost_unit_<?php echo $cer_additional_rental_cost_id;?>" ).change(function() {
				
					calculacte_additional_rental_cost_type(<?php echo $cer_additional_rental_cost_id;?>, <?php echo $additional_rental_cost_id;?>)					
				});


				$("#__record_cer_additional_rental_cost_increase_<?php echo $cer_additional_rental_cost_id;?>" ).change(function() {
				
					calculacte_additional_rental_cost_type(<?php echo $cer_additional_rental_cost_id;?>, <?php echo $additional_rental_cost_id;?>)					
				});


				$("#__record_cer_additional_rental_cost_period_<?php echo $cer_additional_rental_cost_id;?>" ).change(function() {
					
					calculacte_additional_rental_cost_type(<?php echo $cer_additional_rental_cost_id;?>, <?php echo $additional_rental_cost_id;?>)					
				});

				
		<?php
		  }
		?>
	

	});


</script>