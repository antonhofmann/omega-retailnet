<?php
/********************************************************************

    project_page_actions.php

    Defines the possible page actions depending on access rights

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-25
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-09-27
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/


if(has_access("has_access_only_to_human_resources"))
{
	$page->register_action('cer', 'Human Resources', "cer_application_salaries.php?pid=" . param("pid"));
	
}
elseif(has_access("can_only_edit_staff_in_cer")
 or has_access("has_access_only_to_human_resources"))
{
	if($form_type == "CER")
	{
		$page->register_action('cer', 'Capital Expenditure Request Form', "cer_application_general.php?pid=" . param("pid"));
	}
	elseif($form_type == "AF")
	{
		$page->register_action('cer', 'Application Form', "cer_application_general.php?pid=" . param("pid"));
	}
	elseif($form_type == "INR03")
	{
		$page->register_action('cer', 'Request Form INR-03', "cer_application_general.php?pid=" . param("pid"));
	}
}
else
{
	$page->register_action('project', 'Overview', "cer_project.php?pid=" . param("pid"));
	$page->register_action('cermail', 'Mailbox', "cer_mails.php?pid=" . param("pid") . "&id=" . param("pid"));

	//add rejection actions
	
	if($cer_basicdata["cer_basicdata_cer_locked"] == 0 and $cer_basicdata["cer_basicdata_submitted"] != NULL and $cer_basicdata["cer_basicdata_submitted"] != '0000-00-00')
	{
		if(has_access("can_reject_submissions"))
		{
			
			if($cer_basicdata["cer_basicdata_resubmitted"] and $cer_basicdata["cer_basicdata_rejected"] > $cer_basicdata["cer_basicdata_resubmitted"])
			{

			}
			elseif(!$cer_basicdata["cer_basicdata_resubmitted"] and $cer_basicdata["cer_basicdata_rejected"] > $cer_basicdata["cer_basicdata_submitted"])
			{
			}
			else
			{
			
				$page->register_action('dummy', ' ', "");

				if($form_type == "INR03")
				{
					$link = "javascript:createWindowWithRemotingUrl2('Reject INR-03', 'reject_submission.php?pid=" . param("pid") . "&type=inr03');";
					$page->register_action('reject', 'Reject INR-03', $link);
				}
				elseif($form_type == "AF")
				{
					$link = "javascript:createWindowWithRemotingUrl2('Reject AF', 'reject_submission.php?pid=" . param("pid") . "&type=af');";
					$page->register_action('reject', 'Reject AF', $link);
				}
				else
				{
					$link = "javascript:createWindowWithRemotingUrl2('Reject CER', 'reject_submission.php?pid=" . param("pid") . "&type=cer');";
					$page->register_action('reject', 'Reject CER', $link);
				}
			}
		}
	}
	// end reject action


	if($posorder_present == 1)
	{
		
		$page->register_action('dummy', ' ', "");

		//corporate, joint venture, cooperation 3rd party
		if($form_type == "CER")
		{
			$page->register_action('cer', 'Capital Expenditure Request Form', "cer_application_general.php?pid=" . param("pid"));
			$page->register_action('ln', 'Lease Negotiation Form', "ln_general.php?pid=" . param("pid"));
			
			if($ln_basicdata["ln_no_ln_submission_needed"] == 0)
			{
				$page->register_action('ln_cer', 'CER versus LN', "cer_versus_ln.php?pid=" . param("pid"));
			}
			
			$page->register_action('refund', 'Request for Additional Funding', "cer_additional_funding.php?pid=" . param("pid"));
			$page->register_action('dummy', ' ', "");
		
			
			$page->register_action('fsummary', 'Financial Summary', "cer_financial_summary.php?pid=" . param("pid"));
			
		}
		elseif($form_type == "AF")
		{
			
			$page->register_action('cer', 'Application Form', "cer_application_general.php?pid=" . param("pid"));
			//$page->register_action('ln', 'Lease Negotiation Form', "ln_general.php?pid=" . param("pid"));
			//$page->register_action('refund', 'Request for Additional Funding', "cer_additional_funding.php?pid=" . param("pid"));
			$page->register_action('dummy', ' ', "");

			$page->register_action('refund', 'Request for Additional Funding', "cer_additional_funding.php?pid=" . param("pid"));
			$page->register_action('dummy', ' ', "");


			$page->register_action('fsummary', 'Financial Summary', "cer_financial_summary.php?pid=" . param("pid"));
		}
		elseif($form_type == "INR03")
		{
			
			$page->register_action('cer', 'Request Form INR-03', "cer_application_general.php?pid=" . param("pid"));
			//$page->register_action('ln', 'Lease Negotiation Form', "ln_general.php?pid=" . param("pid"));
			$page->register_action('refund', 'Request for Additional Funding', "cer_additional_funding.php?pid=" . param("pid"));
			$page->register_action('dummy', ' ', "");
			$page->register_action('fsummary', 'Financial Summary', "cer_financial_summary.php?pid=" . param("pid"));
		}

		
		if(has_access("has_full_access_to_cer"))
		{
			$page->register_action('edit_names', 'Edit Approval Names', "cer_edit_approval_names.php?pid=" . param("pid"));
		}

		
		if(has_access("has_full_access_to_cer")) // corporate only
		{
			$page->register_action('edit_cer', 'Edit SG Approvals', "project_edit_approved_amounts_data.php?pid=" . param("pid"));
			$page->register_action('dummy22', ' ', "");
		}
		
		$page->register_action('print', 'Print', "cer_print.php?pid=" . param("pid"));
		
			
		$page->register_action('dummy0', ' ', "");
		$page->register_action('project', 'Access Project', "/user/project_task_center.php?pid=" . param("pid"), "_blank"); 
		if (has_access("can_edit_milestones") and ($project["needs_cer"] == 1 
			or $project["needs_af"] == 1 or $project["needs_inr03"] == 1))
		{
			
			$page->register_action('edit_milestones', 'Project Milestones', "cer_edit_milestones.php?pid=" . param("pid")); 
		}
		elseif (has_access("can_view_milestones") and ($project["needs_cer"] == 1 
			or $project["needs_af"] == 1  or $project["needs_inr03"] == 1))
		{   
			$page->register_action('view_milestones', 'Project Milestones', "cer_view_milestones.php?pid=" . param("pid"));    
		}

		if (has_access("has_access_to_all_projects") or has_access("can_view_budget_in_projects"))
		{   
			$page->register_action('view_project_budget', 'View Budget', "/user/project_costs_overview.php?pid=" . param("pid"), "_blank");

			
		}

		if (has_access("can_edit_local_constrction_work") )
		{
			$url = "/user/project_costs_bids.php?pid=" . param("pid");
			$page->register_action('edit_bids', 'Edit Bids', $url, "_blank");
		}
		
	}
}

?>