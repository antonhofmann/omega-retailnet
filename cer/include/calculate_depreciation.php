<?php
/********************************************************************

    calculate_depreciation.php

    Calculate depreciation values for business plan

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-07-06
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-07-06

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

/********************************************************************
    get system currency informations
*********************************************************************/
function calculate_depreciation($cer_basicdata, $project_id = 0, $mode = 0, $version = 0)
{

	$return_values = array();
	$return_values["intangibles"] = 0;
	$return_values["intangibles_total_depreciation"] = 0;
	$return_values["intangibles_residual_value"] = 0;
	$return_values["investments_debug"] =0;

	if(!$project_id)
	{
	    return $return_values;
	}

	//get lease data
	$leasestart_year = 0;
	$leasestart_month = 0;
	$lease_end_year = 0;
	$lease_end_month = 0;
	$project_kind = 0;
	
	if($mode == 0)
	{
		$sql = "select project_order, project_projectkind from projects where project_id = " . dbquote($project_id);
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$project_kind = $row["project_projectkind"];
			$posdata = get_pos_data($row["project_order"]);
			$posleases = get_pos_leasedata($posdata["posaddress_id"], $row["project_order"]);
			if(count($posleases) > 0)
			{
				$leasestart_year = substr($posleases["poslease_startdate"], 0, 4);
				$leasestart_month = substr($posleases["poslease_startdate"], 5, 2);

				$lease_end_year = substr($posleases["poslease_enddate"], 0, 4);
				$leaseend_month = substr($posleases["poslease_enddate"], 5, 2);


			}
		}
	}



	$first_year = $cer_basicdata["cer_basicdata_firstyear"];
	$first_month = $cer_basicdata["cer_basicdata_firstmonth"];
	$last_year = $cer_basicdata["cer_basicdata_lastyear"];
	$last_month = $cer_basicdata["cer_basicdata_lastmonth"];

	$first_year_depr = $first_year;
	$first_month_depr = $first_month;
	if($cer_basicdata["cer_basicdata_firstyear_depr"] > 0)
	{
		$first_year_depr = $cer_basicdata["cer_basicdata_firstyear_depr"];
		$first_month_depr = $cer_basicdata["cer_basicdata_firstmonth_depr"];
	}

	//echo "-> " . $first_month_depr . "<br />";

	$residual_value_former_investment = $cer_basicdata["cer_basicdata_residual_value"];
	$residual_value_former_keymoney = $cer_basicdata["cer_basicdata_residualkeymoney_value"];

	$residual_value_former_investment_depricate_years = $cer_basicdata["cer_basicdata_residual_depryears"];
	$residual_value_former_investment_depricate_months = $cer_basicdata["cer_basicdata_residual_deprmonths"];
	
	$residual_value_former_keymoney_depricate_years = $cer_basicdata["cer_basicdata_residualkeymoney_depryears"];
	$residual_value_former_keymoney_depricate_months = $cer_basicdata["cer_basicdata_residualkeymoney_deprmonths"];

	if(!$last_year)
	{
		$last_year = $first_year + 5;
		$last_month = 12;
	}
	
	$years = array();

	for($y=$first_year;$y<=$last_year;$y++)
	{
		$years[] = $y;

	}

	$number_of_years = count($years);

	//calculate depreciation for investments
	$depreciation = array();
	$total_depreciation = 0;
	$total_depreciatable_investment = 0;
	$residual_value = 0;
	$depr_months_firstyear = 0;
	$depr_months_lasttyear = 0;
	$depr_lasttyear = 0;

	foreach($years as $key=>$year)
	{
		$depreciation[$year] = 0;
	}


	


	//get depreciable investment data
	$debug_depreciation = array();
	$total_depreciatable_investment_types = array();

	if($mode == 0)
	{
		$sql = "select * from cer_investments " .
			   "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
			   "where cer_investment_project = " . $project_id . 
			   " and cer_investment_cer_version = " . $version . 
			   " and posinvestment_type_intangible = 0 " . 
			   " and posinvestment_type_depreciatable = 1 " .
			   " and cer_investment_depr_years >= 0";

	}
	else
	{
		$sql = "select * from cer_draft_investments " .
			   "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
			   "where cer_investment_draft_id = " . $project_id . 
			   " and posinvestment_type_intangible = 0 " . 
			   " and posinvestment_type_depreciatable = 1 " .
			   " and cer_investment_depr_years >= 0";
	}
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$total_depreciatable_investment = $total_depreciatable_investment   + $row["cer_investment_amount_cer_loc"];
		$total_depreciatable_investment_types[$row["posinvestment_type_id"]] =  $row["cer_investment_amount_cer_loc"];
	}

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{	
		//echo $row["posinvestment_type_name"] . " " . $cer_basicdata["cer_extend_depreciation_period"] . " " . $row["cer_investment_depr_years"] . " " . $number_of_years . "<br />";
		
		
		if($cer_basicdata["cer_extend_depreciation_period"] == 0 and $row["cer_investment_depr_years"] > $number_of_years)
		{
			$months_count_of_all_years = (13-$first_month) + 12*($last_year-$first_year-1) + $last_month;

			
			$depr_months_firstyear = 13 - $first_month_depr;
			$depr_months_lasttyear = 12 - $depr_months_firstyear;


			$number_of_full_years = $row["cer_investment_depr_years"]-1;
			
			$number_of_full_years = floor($months_count_of_all_years/12);

			$depr_months_lasttyear = $months_count_of_all_years - $depr_months_firstyear - (12*$number_of_full_years);
		}
		elseif($cer_basicdata["cer_extend_depreciation_period"] == 1 and $row["cer_investment_depr_years"] > $number_of_years)
		{
			$months_count_of_all_years = ($row["cer_investment_depr_years"] * 12) + $row["cer_investment_depr_months"];

			$months_count_of_all_years = ($row["cer_investment_depr_years"] * 12) + $row["cer_investment_depr_months"];

			$depr_months_firstyear = 13 - $first_month_depr;
			$depr_months_lasttyear = 12 - $depr_months_firstyear;


			$number_of_full_years = $row["cer_investment_depr_years"]-1;
			
			$number_of_full_years = floor($months_count_of_all_years/12);

			$depr_months_lasttyear = $months_count_of_all_years - $depr_months_firstyear - (12*$number_of_full_years);

		}
		else
		{
			$months_count_of_all_years = ($row["cer_investment_depr_years"] * 12) + $row["cer_investment_depr_months"];

			$depr_months_firstyear = 13 - $first_month_depr;
			$depr_months_lasttyear = 12 - $depr_months_firstyear;


			$number_of_full_years = $row["cer_investment_depr_years"]-1;
			
			$number_of_full_years = floor($months_count_of_all_years/12);

			$depr_months_lasttyear = $months_count_of_all_years - $depr_months_firstyear - (12*$number_of_full_years);
		}


		$tmp_amount = $row["cer_investment_amount_cer_loc"];
		
		$debug_depreciation[$row["posinvestment_type_id"]] = array();

		if($months_count_of_all_years > 0)
		{
			
			$tmp = 0;
			
			//$tmp_depr_lastyear = $first_year_depr + $number_of_years-1;

			if($number_of_full_years == 0 and $depr_months_lasttyear > 0) {
				$tmp_depr_lastyear = $first_year_depr +  floor($residual_value_former_investment_depricate_years) + 1;
			}
			else
			{
				$tmp_depr_lastyear = $first_year_depr + $number_of_full_years;
				
				if($depr_months_lasttyear > 0) {
					$tmp_depr_lastyear = $tmp_depr_lastyear + 1;
				}
				elseif($depr_months_lasttyear <= 0) {
					$depr_months_lasttyear = 12 + $depr_months_lasttyear;
				}
			}


			for($i=$first_year_depr;$i<=$tmp_depr_lastyear;$i++)
			{
				if($i == $first_year_depr)
				{
					$tmp = $tmp_amount/$months_count_of_all_years * $depr_months_firstyear;
					
				}
				elseif($i == $tmp_depr_lastyear) // last year
				{

					$tmp = $depr_months_lasttyear * $tmp_amount/$months_count_of_all_years;

					//echo $number_of_full_years . " " . $months_count_of_all_years . " " . $depr_months_firstyear . " " . $depr_months_lasttyear . '<br />';
					//abc();

				}
				else
				{
					if($cer_basicdata["cer_extend_depreciation_period"] == 1 
						and $i == $cer_basicdata["cer_basicdata_lastyear"]
						and ($cer_basicdata["cer_basicdata_lastyear"] < $cer_basicdata["cer_basicdata_firstyear_depr"] + $number_of_full_years)
					   )
					{
						$tmp = $tmp_amount/$months_count_of_all_years * $cer_basicdata["cer_basicdata_lastmonth"];
					}
					else
					{
						$tmp = $tmp_amount/$months_count_of_all_years * 12;
					}
				}

				//echo $row["posinvestment_type_id"] . ' ' . $i . ' '. $tmp . ' ' . $tmp_depr_lastyear . '<br />';
				
				$debug_depreciation[$row["posinvestment_type_id"]][$i] = $tmp;
				$total_depreciatable_investment_types[$row["posinvestment_type_id"]] = $total_depreciatable_investment_types[$row["posinvestment_type_id"]] -  $tmp;
			
				$total_depreciation  = $total_depreciation + $tmp;

				//residual value
				if($i == $tmp_depr_lastyear and $debug_depreciation[$row["posinvestment_type_id"]][$i] > 0) // last year
				{
					$residual_value_investment_type = $total_depreciatable_investment_types[$row["posinvestment_type_id"]];
					$debug_depreciation[$row["posinvestment_type_id"]][$i] = $debug_depreciation[$row["posinvestment_type_id"]][$i] + $residual_value_investment_type;

					//echo $tmp_depr_lastyear . ' ' . $row["posinvestment_type_id"] . ' ' . $i . ' '. $residual_value_investment_type . '<br />';
					//abc();
				}
				

				
			}
			for($i=$tmp_depr_lastyear+1;$i<=$last_year+1;$i++)
			{
				$debug_depreciation[$row["posinvestment_type_id"]][$i] = 0;
			}
		}


		//residual value of fixed assets
		if($row["posinvestment_type_id"] == 3 and $residual_value_former_investment > 0 
			and ($residual_value_former_investment_depricate_years > 0 or $residual_value_former_investment_depricate_months > 0))
		{

			if($residual_value_former_investment_depricate_years > $number_of_years)
			{
				$months_count_of_all_years_residual = $number_of_years * 12;
			}
			else
			{
				$months_count_of_all_years_residual = ($residual_value_former_investment_depricate_years * 12) + $residual_value_former_investment_depricate_months;
			}


			$depr_months_firstyear_residual = 13 - $first_month_depr;
			$depr_months_lasttyear_residual = 12 - $depr_months_firstyear_residual;

			if($first_year_depr == $last_year)
			{
				$depr_months_firstyear_residual = 12;
				$depr_months_lasttyear_residual = 12;
			}

			$number_of_full_years_residual = $residual_value_former_investment_depricate_years-1;
		
			$number_of_full_years_residual = floor($months_count_of_all_years_residual/12);

			$depr_months_lasttyear_residual = $months_count_of_all_years_residual - $depr_months_firstyear_residual - (12*$number_of_full_years_residual);
			
			
			$total_depreciatable_investment_residual = $residual_value_former_investment;
			$tmp_amount_residual = $residual_value_former_investment;


			if($months_count_of_all_years_residual > 0)
			{
				$tmp_residual = 0;
				
				if($number_of_full_years_residual == 0 and $depr_months_lasttyear_residual > 0) {
					$tmp_depr_lastyear_residual = $first_year_depr +  floor($residual_value_former_investment_depricate_years) + 1;
				}
				else
				{
					$tmp_depr_lastyear_residual = $first_year_depr + $number_of_full_years_residual;

					if($tmp_depr_lastyear_residual > $last_year)
					{
						$tmp_depr_lastyear_residual = $last_year;
					}
					
					if($depr_months_lasttyear_residual > 0) {
						$tmp_depr_lastyear_residual = $tmp_depr_lastyear_residual + 1;
					}
					elseif($depr_months_lasttyear_residual <= 0) {
						$depr_months_lasttyear_residual = 12 + $depr_months_lasttyear_residual;
					}
				}

				for($i=$first_year_depr;$i<=$tmp_depr_lastyear_residual;$i++)
				{
					if($i == $first_year_depr)
					{
						$tmp_residual = $tmp_amount_residual/$months_count_of_all_years_residual * $depr_months_firstyear_residual;
						
					}
					elseif($i == $tmp_depr_lastyear_residual) // last year
					{

						$tmp_residual = $depr_months_lasttyear_residual * $tmp_amount_residual/$months_count_of_all_years_residual;
					}
					else
					{
						$tmp_residual = $tmp_amount_residual/$months_count_of_all_years_residual * 12;
					}


					if(array_key_exists($i, $debug_depreciation[$row["posinvestment_type_id"]]))  {
						$debug_depreciation[$row["posinvestment_type_id"]][$i] = $debug_depreciation[$row["posinvestment_type_id"]][$i] + $tmp_residual;
					}
					else
					{
						$debug_depreciation[$row["posinvestment_type_id"]][$i] = $tmp_residual;
					}
					
					
					$total_depreciatable_investment_residual = $total_depreciatable_investment_residual -  $tmp_residual;
				
					$total_depreciation  = $total_depreciation + $tmp_residual;

				
					
					if($i == $tmp_depr_lastyear_residual and $debug_depreciation[$row["posinvestment_type_id"]][$i] > 0) // last year
					{
						$debug_depreciation[$row["posinvestment_type_id"]][$i] = $debug_depreciation[$row["posinvestment_type_id"]][$i] + $total_depreciatable_investment_residual;
					}
					
					
				}

				if(isset($tmp_depr_lastyear) and !$tmp_depr_lastyear and $tmp_depr_lastyear_residual > 0)
				{
					$tmp_depr_lastyear = $tmp_depr_lastyear_residual;
				}
				
				if(isset($tmp_depr_lastyear)) {
					for($i=$tmp_depr_lastyear+1;$i<=$last_year+1;$i++)
					{
						$debug_depreciation[$row["posinvestment_type_id"]][$i] = 0;
					}
				}
			}
		}
	}


	for($i=$first_year;$i<=$last_year;$i++)
	{
		$depreciation[$i] = 0;
	}

	foreach($debug_depreciation as $invetsment_type=>$year_values)
	{
		foreach($year_values as $key=>$amount)
		{
			if(array_key_exists($key, $depreciation))
			{
				$depreciation[$key] = $depreciation[$key] + $amount;
			}
		}
	}



	//calculate depreciation on future investments
	$depr_months = array();
	$depr_amounts = array();
	
	if($cer_basicdata['cer_basicdata_future_investment'] > 0) {
		
		if($cer_basicdata['cer_basicdata_future_depr_start_year'] == 0) {
			$cer_basicdata['cer_basicdata_future_depr_start_year'] = $cer_basicdata['cer_basicdata_firstyear_depr'];
		}

		if($cer_basicdata['cer_basicdata_future_depr_start_month'] == 0) {
			$cer_basicdata['cer_basicdata_future_depr_start_month'] = $cer_basicdata['cer_basicdata_firstmonth_depr'];
		}

		
		$depr_amounts[$cer_basicdata['cer_basicdata_future_depr_start_year']] = 0;
		$depr_months[$cer_basicdata['cer_basicdata_future_depr_start_year']] = (13-$cer_basicdata['cer_basicdata_future_depr_start_month']);
		
		
		
		if($cer_basicdata['cer_basicdata_future_depr_end_year'] > 0
			and $cer_basicdata['cer_basicdata_future_depr_end_month']) {
			
			$number_of_months = $cer_basicdata['cer_basicdata_future_depr_end_month'];
			
			for ($i = ($cer_basicdata['cer_basicdata_future_depr_start_year']+1); $i < $cer_basicdata['cer_basicdata_future_depr_end_year']; $i++) {
				$number_of_months = $number_of_months + 12;
				$depr_amounts[$i] = 0;
				$depr_months[$i] = 12;
			}

			$number_of_months = $number_of_months + (13-$cer_basicdata['cer_basicdata_future_depr_start_month']);

			$depr_amounts[$cer_basicdata['cer_basicdata_future_depr_end_year']] = 0;
			$depr_months[$cer_basicdata['cer_basicdata_future_depr_end_year']] = $cer_basicdata['cer_basicdata_future_depr_end_month'];
		}
		else {
			
			$number_of_months = $cer_basicdata['cer_basicdata_lastmonth'];
		
			for ($i = ($cer_basicdata['cer_basicdata_future_depr_start_year']+1); $i < $cer_basicdata['cer_basicdata_lastyear']; $i++) {
				$number_of_months = $number_of_months + 12;
				$depr_amounts[$i] = 0;
				$depr_months[$i] = 12;
			}

			$number_of_months = $number_of_months + (13-$cer_basicdata['cer_basicdata_future_depr_start_month']);

			$depr_amounts[$cer_basicdata['cer_basicdata_lastyear']] = 0;
			$depr_months[$cer_basicdata['cer_basicdata_lastyear']] = $cer_basicdata['cer_basicdata_lastmonth'];
		}


		foreach($depr_months as $year=>$months) {
			$depr_amounts[$year] = round($months*($cer_basicdata['cer_basicdata_future_investment']/$number_of_months), 2);
		}
	}

	foreach($depr_amounts as $year=>$amount) {
		$depreciation[$year] = $depreciation[$year] + $amount;
	}



	$return_values["depreciation_on_future_investments"] = $depr_amounts;

	$return_values["investments"] = $depreciation;
	$return_values["investments_total_depreciation"] = $total_depreciation;
	$return_values["investments_residual_value"] = $total_depreciatable_investment - $total_depreciation;

	
	//calculate depreciation for intangible assets
	
	//calculate depreciation from lease start

	if($project_kind == 1
		or $project_kind == 4
		or $project_kind == 5
		or $project_kind == 6) //new, take over, lease renewal, relocation
	{
		if($leasestart_year == $first_year_depr and $leasestart_month > 0 and $leasestart_month < $first_month_depr)
		{
			$first_month_depr = $leasestart_month;
		}
	}
	
	$prepayed_rents = array();
	$total_depreciatable_intangibles = 0;
	$total_depreciation = 0;
	$depr_months_firstyear = 0;
	$depr_months_lasttyear = 0;
	$depr_lasttyear = 0;

	foreach($years as $key=>$year)
	{
		$prepayed_rents[$year] = 0;
	}

	$total_depreciatable_investment_types = array();

	//get depreciable intangible data
	if($mode == 0)
	{
		$sql = "select * from cer_investments " .
			   "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
			   "where cer_investment_project = " . $project_id . 
			   " and cer_investment_cer_version = " . $version . 
			   " and posinvestment_type_intangible = 1 " . 
			   " and posinvestment_type_depreciatable = 1 " .
			   " and cer_investment_depr_years >= 0";
	}
	else
	{
		$sql = "select * from cer_draft_investments " .
			   "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
			   "where cer_investment_draft_id = " . $project_id . 
			   " and posinvestment_type_intangible = 1 " . 
			   " and posinvestment_type_depreciatable = 1 " .
			   " and cer_investment_depr_years >= 0";
	}

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$total_depreciatable_intangibles = $total_depreciatable_intangibles   + $row["cer_investment_amount_cer_loc"];
		$total_depreciatable_investment_types[$row["posinvestment_type_id"]] =  $row["cer_investment_amount_cer_loc"];
	}


	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{	
		
		//echo $row["posinvestment_type_name"] . " " . $cer_basicdata["cer_extend_depreciation_period"] . " " . $row["cer_investment_depr_years"] . " " . $number_of_years . "<br />";

		if($cer_basicdata["cer_extend_depreciation_period"] == 0 and $row["cer_investment_depr_years"] > $number_of_years)
		{
			$months_count_of_all_years = (13-$first_month) + 12*($last_year-$first_year-1) + $last_month;

			
			$depr_months_firstyear = 13 - $first_month_depr;
			$depr_months_lasttyear = 12 - $depr_months_firstyear;


			$number_of_full_years = $row["cer_investment_depr_years"]-1;
			
			$number_of_full_years = floor($months_count_of_all_years/12);

			$depr_months_lasttyear = $months_count_of_all_years - $depr_months_firstyear - (12*$number_of_full_years);

			
		}
		elseif($cer_basicdata["cer_extend_depreciation_period"] == 1 and $row["cer_investment_depr_years"] > $number_of_years)
		{
			
			$months_count_of_all_years = ($row["cer_investment_depr_years"] * 12) + $row["cer_investment_depr_months"];

			$months_count_of_all_years = ($row["cer_investment_depr_years"] * 12) + $row["cer_investment_depr_months"];

			$depr_months_firstyear = 13 - $first_month_depr;
			$depr_months_lasttyear = 12 - $depr_months_firstyear;


			$number_of_full_years = $row["cer_investment_depr_years"]-1;
			
			$number_of_full_years = floor($months_count_of_all_years/12);



			if($number_of_full_years > $number_of_years)
			{
				$depr_months_lasttyear = $leaseend_month;
			}
			else
			{
				$depr_months_lasttyear = $months_count_of_all_years - $depr_months_firstyear - (12*$number_of_full_years);
			}
			//echo "First month: " . $first_month_depr . " Number of months dirst year: " . $depr_months_firstyear . " Number of months all years: " . $months_count_of_all_years . " Number of months in fist year: " . $depr_months_firstyear ." Number of full years: " . $number_of_full_years . " Number of months last year: " . $depr_months_lasttyear . "<br />";
		}
		else
		{
			$months_count_of_all_years = ($row["cer_investment_depr_years"] * 12) + $row["cer_investment_depr_months"];

			$depr_months_firstyear = 13 - $first_month_depr;
			$depr_months_lasttyear = 12 - $depr_months_firstyear;


			$number_of_full_years = $row["cer_investment_depr_years"]-1;
			
			$number_of_full_years = floor($months_count_of_all_years/12);

			$depr_months_lasttyear = $months_count_of_all_years - $depr_months_firstyear - (12*$number_of_full_years);
		}

		$tmp_amount = $row["cer_investment_amount_cer_loc"];
		$debug_depreciation[$row["posinvestment_type_id"]] = array();

		if($months_count_of_all_years > 0)
		{
			$tmp = 0;
			
			//$tmp_depr_lastyear = $first_year_depr + $number_of_years-1;

			if($number_of_full_years == 0 and $depr_months_lasttyear > 0) {
				$tmp_depr_lastyear = $first_year_depr +  floor($row["cer_investment_depr_years"]) + 1;

				//echo "1:" . $tmp_depr_lastyear . "<br />";
			}
			else
			{
				if($cer_basicdata["cer_extend_depreciation_period"] == 1 and $row["cer_investment_depr_years"] > $number_of_years)
				{
					

					//echo $lease_end_year . " " . $last_year . "<br />";
					if($lease_end_year <= $last_year)
					{
						$tmp_depr_lastyear = $lease_end_year;
					}
					else
					{
						$tmp_depr_lastyear = $last_year;
					}
					//echo "2:" . $tmp_depr_lastyear . "<br />";
					
				}
				else
				{
					$tmp_depr_lastyear = $first_year_depr + $number_of_full_years;
					//echo "3:" . $tmp_depr_lastyear . "<br />";
				}
				
			
				if($cer_basicdata["cer_extend_depreciation_period"] == 1 and $row["cer_investment_depr_years"] > $number_of_years)
				{
					$tmp_depr_lastyear = $first_year_depr +  $row["cer_investment_depr_years"];

					//echo $first_year_depr . " ".  $row["cer_investment_depr_years"] . "<br />";
				}
				else
				{
					if($depr_months_lasttyear > 0) {
						$tmp_depr_lastyear = $tmp_depr_lastyear + 1;
					}
					elseif($depr_months_lasttyear <= 0) {
						$depr_months_lasttyear = 12 + $depr_months_lasttyear;
					}
				}
			}

			

			//echo "last year: " . $tmp_depr_lastyear . "<br />";

			for($i=$first_year_depr;$i<=$tmp_depr_lastyear;$i++)
			{
				//echo $i . "<br />";
				if($i == $first_year_depr)
				{
					$tmp = $tmp_amount/$months_count_of_all_years * $depr_months_firstyear;
					
				}
				elseif($i == $tmp_depr_lastyear) // last year
				{

					$tmp = $depr_months_lasttyear * $tmp_amount/$months_count_of_all_years;
					

					//echo $tmp_amount . "  " . $number_of_full_years . " " . $months_count_of_all_years . " " . $depr_months_firstyear . " " . $depr_months_lasttyear . '<br />';

					//echo $tmp . "<br />";
				}
				else
				{
					$tmp = $tmp_amount/$months_count_of_all_years * 12;
				}

				//echo $row["posinvestment_type_id"] . ' ' . $i . ' '. $tmp . ' ' . $tmp_depr_lastyear . '<br />';
				
				$debug_depreciation[$row["posinvestment_type_id"]][$i] = $tmp;
				$total_depreciatable_investment_types[$row["posinvestment_type_id"]] = $total_depreciatable_investment_types[$row["posinvestment_type_id"]] -  $tmp;
			
				$total_depreciation  = $total_depreciation + $tmp;

				//residual value
				if($i == $tmp_depr_lastyear and $debug_depreciation[$row["posinvestment_type_id"]][$i] > 0) // last year
				{
					$residual_value_investment_type = $total_depreciatable_investment_types[$row["posinvestment_type_id"]];
					
					if($cer_basicdata["cer_extend_depreciation_period"] == 0)
					{
						$debug_depreciation[$row["posinvestment_type_id"]][$i] = $debug_depreciation[$row["posinvestment_type_id"]][$i] + $residual_value_investment_type;
					}

					//echo $tmp_depr_lastyear . ' ' . $row["posinvestment_type_id"] . ' ' . $i . ' '. $residual_value_investment_type . '<br />';
					//abc();
				}
				

				
			}
			for($i=$tmp_depr_lastyear+1;$i<=$last_year+1;$i++)
			{
				$debug_depreciation[$row["posinvestment_type_id"]][$i] = 0;
			}
		}
	}

	//residual value of key money
	// add to keymoney depreciation
	$debug_depreciation_rv = array();
	if($residual_value_former_keymoney > 0 
		and ($residual_value_former_keymoney_depricate_years > 0 or $residual_value_former_keymoney_depricate_months > 0))
	{

		if(!array_key_exists(15, $debug_depreciation_rv))
		{
			$debug_depreciation_rv[15] = array();
		}

		
		if($residual_value_former_keymoney_depricate_years > $number_of_years)
		{
			$months_count_of_all_years_residual = $number_of_years * 12;
		}
		else
		{
			$months_count_of_all_years_residual = ($residual_value_former_keymoney_depricate_years * 12) + $residual_value_former_keymoney_depricate_months;
		}

		$depr_months_firstyear_residual = 13 - $first_month_depr;
		$depr_months_lasttyear_residual = 12 - $depr_months_firstyear_residual;

		if($first_year_depr == $last_year)
		{
			$depr_months_firstyear_residual = 12;
			$depr_months_lasttyear_residual = 12;
		}

		$number_of_full_years_residual = $residual_value_former_keymoney_depricate_years-1;

		$number_of_full_years_residual = floor($months_count_of_all_years_residual/12);

		$depr_months_lasttyear_residual = $months_count_of_all_years_residual - $depr_months_firstyear_residual - (12*$number_of_full_years_residual);


		$total_depreciatable_keymoney_residual = $residual_value_former_keymoney;
		$tmp_amount_residual = $residual_value_former_keymoney;

		if($months_count_of_all_years_residual > 0)
		{
			$tmp_residual = 0;
			
			if($number_of_full_years_residual == 0 and $depr_months_lasttyear_residual > 0) {
				$tmp_depr_lastyear_residual = $first_year_depr +  floor($residual_value_former_keymoney_depricate_years) + 1;
			}
			else
			{
				$tmp_depr_lastyear_residual = $first_year_depr + $number_of_full_years_residual;

				if($tmp_depr_lastyear_residual > $last_year)
				{
					$tmp_depr_lastyear_residual = $last_year;
				}
				
				if($depr_months_lasttyear_residual > 0) {
					$tmp_depr_lastyear_residual = $tmp_depr_lastyear_residual + 1;
				}
				elseif($depr_months_lasttyear_residual <= 0) {
					$depr_months_lasttyear_residual = 12 + $depr_months_lasttyear_residual;
				}
			}

			for($i=$first_year_depr;$i<=$tmp_depr_lastyear_residual;$i++)
			{
				
				if(!array_key_exists($i, $debug_depreciation_rv[15]))
				{
					$debug_depreciation_rv[15][$i] = 0;
				}

				if($i == $first_year_depr)
				{
					$tmp_residual = $tmp_amount_residual/$months_count_of_all_years_residual * $depr_months_firstyear_residual;
					
				}
				elseif($i == $tmp_depr_lastyear_residual) // last year
				{

					$tmp_residual = $depr_months_lasttyear_residual * $tmp_amount_residual/$months_count_of_all_years_residual;
				}
				else
				{
					$tmp_residual = $tmp_amount_residual/$months_count_of_all_years_residual * 12;
				}

				$debug_depreciation_rv[15][$i] = $debug_depreciation_rv[15][$i] + $tmp_residual;
							
				
				$total_depreciatable_keymoney_residual = $total_depreciatable_keymoney_residual -  $tmp_residual;
			
				$total_depreciation  = $total_depreciation + $tmp_residual;

			
				
				if($i == $tmp_depr_lastyear_residual and $debug_depreciation_rv[15][$i] > 0) // last year
				{
					$debug_depreciation_rv[15][$i] = $debug_depreciation_rv[15][$i] + $total_depreciatable_keymoney_residual;
				}
				
			}

			for($i=$tmp_depr_lastyear_residual+1;$i<=$last_year+1;$i++)
			{
				$debug_depreciation_rv[15][$i] = 0;
			}
		}
	}

	
	
	//add depreciation on residual values
	foreach($debug_depreciation_rv as $invetsment_type=>$year_values)
	{
		foreach($year_values as $key=>$amount)
		{
			if(array_key_exists($invetsment_type, $debug_depreciation)
				and array_key_exists($key, $debug_depreciation[$invetsment_type]))
			{
				$debug_depreciation[$invetsment_type][$key] = $debug_depreciation[$invetsment_type][$key] + $amount;
			}
			else
			{
				$debug_depreciation[$invetsment_type][$key] = $amount;
			}
		}
	}
		
	
	for($i=$first_year;$i<=$last_year;$i++)
	{
		$prepayed_rents[$i] = 0;
	}

	foreach($debug_depreciation as $invetsment_type=>$year_values)
	{
		if($invetsment_type == 15 or $invetsment_type == 17) // key money and goodwill
		{
			foreach($year_values as $key=>$amount)
			{
				if(array_key_exists($key, $prepayed_rents))
				{
					$prepayed_rents[$key] = $prepayed_rents[$key] + $amount;
				}
			}
		}
	}


	$return_values["intangibles"] = $prepayed_rents;
	$return_values["intangibles_total_depreciation"] = $total_depreciatable_intangibles;
	$return_values["intangibles_residual_value"] = $total_depreciatable_intangibles - $total_depreciation;
	$return_values["investments_debug"] = $debug_depreciation;
	
    
    return $return_values;
}


?>