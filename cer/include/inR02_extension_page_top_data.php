<?php
// draw page box
$pdf->SetXY($margin_left-2,$margin_top);
$pdf->Cell(277, 202, "", 1);
// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "B", 11);

if(param("pid") > 0 )
{
	$pdf->Cell(235, 6, "Retail Business Plan Extension for Project " . $project_number . ": " . $project_name , 0, "", "L");
}
else
{
	$pdf->Cell(235, 6, "Retail Business Draft Extension: " . $project_name , 0, "", "L");
}

$pdf->SetFont("arialn", "", 9);
$pdf->Cell(20, 6, date("d.m.Y"), 1, "", "C");
$pdf->Cell(20, 6, "IN-R02", 1, "", "C");


// draw box for general information
$y = $margin_top + 6;
$pdf->SetXY($margin_left,$y);
$pdf->Cell(275, 17, "", 1);

	$y = $margin_top + 7;
	$pdf->SetXY($margin_left,$y);
	$pdf->SetFont("arialn", "B", 10);
	$pdf->Cell(20, 4, "1. General information", 0, "", "L");
	
	//row 1
	$y = $y + 4;
	$pdf->SetXY($margin_left,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(20, 4, "Brand: " . BRAND, 0, "", "L");

	$pdf->SetXY($margin_left+106,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(20, 4, "Legal Entity: " . $legal_entity, 0, "", "L");

	$pdf->SetXY($margin_left+190,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(20, 4, "Project Classification: " . $project_kind, 0, "", "L");

	//row 2
	$y = $y + 4;
	$pdf->SetXY($margin_left,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(20, 4, "Country: " . $pos_country, 0, "", "L");

	$pdf->SetXY($margin_left+106,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(20, 4, "Address: " . $pos_address, 0, "", "L");

	$pdf->SetXY($margin_left+190,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(20, 4, "City: " . $pos_city, 0, "", "L");

	//row 3
	$y = $y + 4;
	$pdf->SetXY($margin_left,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(20, 4, "Project Name: " . $project_name, 0, "", "L");

	$pdf->SetXY($margin_left+106,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(20, 4, "Project Manager: " . $project_manager, 0, "", "L");

	$pdf->SetXY($margin_left+190,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(20, 4, "Date Investment Request " . $date_request, 0, "", "L");



		
	
?>