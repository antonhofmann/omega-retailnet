<?php
/********************************************************************

    benchmark_page_actions.php

    Defines the possible page actions depending on access rights

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-25
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-09-27
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

if(id() > 0)
{	
	$url = "cer_benchmark_01_xls.php?id=" . id();
	$link = "javascript:popup('" . $url . "', 1024, 768)";
	$page->register_action('b01', 'Benchmark horizontal', $url);
	
	$url = "cer_benchmark_02_xls.php?id=" . id();
	$link = "javascript:popup('" . $url . "', 1024, 768)";
	$page->register_action('b02', 'Human Resources', $url);

	$url = "cer_benchmark_03_xls.php?id=" . id();
	$link = "javascript:popup('" . $url . "', 1024, 768)";
	$page->register_action('b03', 'Rental Details', $url);

	$url = "cer_benchmark_04_xls.php?id=" . id();
	$link = "javascript:popup('" . $url . "', 1024, 768)";
	$page->register_action('b04', 'Environment', $url);

	$url = "cer_benchmark_05_xls.php?id=" . id();
	$link = "javascript:popup('" . $url . "', 1024, 768)";
	$page->register_action('b05', 'Investments', $url);

	$url = "cer_benchmark_06_xls.php?id=" . id();
	$link = "javascript:popup('" . $url . "', 1024, 768)";
	$page->register_action('b06', 'Revenues', $url);

}

?>