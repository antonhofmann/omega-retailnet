<script language="Javascript">

	function round_decimal(num,decimals){
		return Math.round(num*Math.pow(10,decimals))/Math.pow(10,decimals);
	}


	function daysInMonth(month,year) {
		return new Date(year, month, 0).getDate();
	}


	function monthDiff(fy,fm,fd,ty,tm,td) {
		
		var number_of_months = 0;
		var number_of_full_months = 0;
		var month_part_first_year = 0;
		var month_part_last_year = 0;
		

		
		if(fy == ty)
		{
			if(fm == tm)
			{
				days_in_fist_month = daysInMonth(fm,fy);
				days = 1*td - 1*fd + 1;
				month_part_first_year = days/days_in_fist_month;
				month_part_last_year = 0;
			}
			else if(1*tm == (1*fm + 1))
			{
				days_in_fist_month = daysInMonth(fm,fy);
				days_in_last_month = daysInMonth(tm,ty);

				month_part_first_year = (days_in_fist_month-1*fd+1)/days_in_fist_month;
				month_part_last_year = 1*td/days_in_last_month;
			}
			else
			{
				days_in_fist_month = daysInMonth(fm,fy);
				days_in_last_month = daysInMonth(tm,ty);

				month_part_first_year = (days_in_fist_month-1*fd+1)/days_in_fist_month;
				month_part_last_year = 1*td/days_in_last_month;

				number_of_full_months = (1*tm) - (1*fm+1);

				

			}
		}
		else if(ty == (1*fy + 1))
		{
			days_in_fist_month = daysInMonth(fm,fy);
			month_part_first_year = (days_in_fist_month-1*fd+1)/days_in_fist_month;
			
			days_in_last_month = daysInMonth(tm,ty);
			month_part_last_year = 1*td/days_in_last_month;
			
			
			if(fm < 12)
			{
				month_part_first_year = month_part_first_year + 12-1*fm;
			}
			
			
			if(tm > 1)
			{
				month_part_last_year = month_part_last_year + 1*tm - 1;
				
			}
		}
		else
		{
			days_in_fist_month = daysInMonth(fm,fy);
			month_part_first_year = (days_in_fist_month-fd+1)/days_in_fist_month;
			
			days_in_last_month = daysInMonth(tm,ty);
			month_part_last_year = td/days_in_last_month;

			

			if(fm < 12)
			{
				month_part_first_year = month_part_first_year + 12-1*fm;
			}
			
			
			if(tm > 1)
			{
				month_part_last_year = month_part_last_year + 1*tm - 1;
				
			}

			number_of_full_months = 12*(ty-fy-1);
		}
		
		number_of_months = number_of_full_months + month_part_first_year + month_part_last_year;
		
		return number_of_months;
		
		

	}

		
	$(document).ready(function(){
		  
		  
		<?php
		if(param("pid") > 0)
		{
		?>
			var surface_in_sqm = <?php echo $project["project_cost_gross_sqms"];?>;
			var surface_in_feet = <?php echo 10.7639104*$project["project_cost_gross_sqms"];?>;
		<?php
		}
		else
		{
		?>
			var surface_in_sqm = $("#cer_fixed_rent_pos_surface").val();
			var surface_in_feet = 10.7639104*surface_in_sqm;
		<?php
		}
		?>

		  var units = new Array();
		  var surfaces = new Array();
		  var surfaces2 = new Array();
		  units = <?php echo json_encode($units);?>;
		  surfaces = <?php echo json_encode($surfaces);?>;
		  surfaces2 = <?php echo json_encode($surfaces2);?>;

		  surface_in_feet = surface_in_feet.toFixed(2);

		  var new_unit = "";


		  <?php
		  foreach($from_months as $key=>$value)
		  {
		  ?>
		  
		  
		  $("#__record_cer_fixed_rent_amount_<?php echo $key;?>" ).change(function() {
				
			var amount = $("#__record_cer_fixed_rent_amount_<?php echo $key;?>").val();
			$("#__record_fixed_rent_amount2_<?php echo $key;?>").html(amount);
			$("#__record_fixed_rent_amount3_<?php echo $key;?>").html(amount);
			$("#__record_fixed_rent_amount4_<?php echo $key;?>").html(amount);
				
		  });



		  $("#__record_cer_fixed_rent_unit_<?php echo $key;?>").change(function() {
				
				new_unit = $("#__record_cer_fixed_rent_unit_<?php echo $key;?>").val();
				$("#unit2").html(surfaces[new_unit]);
				$("#unit3").html(surfaces2[new_unit]);

				<?php
				foreach($from_months as $key1=>$value)
				{
				?>
					$("#__record_cer_fixed_rent_unit_<?php echo $key1;?>").val(new_unit);
				<?php
				}
				?>
		  });
		  
		  
		  $("#__record_cer_fixed_rent_from_year_<?php echo $key;?>" ).change(function() {
				
			var fy = $("#__record_cer_fixed_rent_from_year_<?php echo $key;?>").val();
			var fm = $("#__record_cer_fixed_rent_from_month_<?php echo $key;?>").val();
			var fd = $("#__record_cer_fixed_rent_from_day_<?php echo $key;?>").val();

			var ty = $("#__record_cer_fixed_rent_to_year_<?php echo $key;?>").val();
			var tm = $("#__record_cer_fixed_rent_to_month_<?php echo $key;?>").val();
			var td = $("#__record_cer_fixed_rent_to_day_<?php echo $key;?>").val();

			if(fy > 0 && fm > 0 && fd > 0 && ty > 0 && tm > 0 && td > 0)
			{
				number_of_months = monthDiff(fy,fm,fd,ty,tm,td);

				if(number_of_months >=0)
				{
					$("#__record_cer_fixed_rent_number_of_months_<?php echo $key;?>").val(number_of_months);

					var fdate = fd + "." + fm + "." + fy;
					$("#__record_fdate2_<?php echo $key;?>").html(fdate);
					$("#__record_fdate3_<?php echo $key;?>").html(fdate);
					$("#__record_fdate4_<?php echo $key;?>").html(fdate);

					var fdate = td + "." + tm + "." + ty;
					$("#__record_tdate2_<?php echo $key;?>").html(fdate);
					$("#__record_tdate3_<?php echo $key;?>").html(fdate);
					$("#__record_tdate4_<?php echo $key;?>").html(fdate);
				}
			}
				
		  });
		  
		  $("#__record_cer_fixed_rent_from_month_<?php echo $key;?>" ).change(function() {
				
			var fy = $("#__record_cer_fixed_rent_from_year_<?php echo $key;?>").val();
			var fm = $("#__record_cer_fixed_rent_from_month_<?php echo $key;?>").val();
			var fd = $("#__record_cer_fixed_rent_from_day_<?php echo $key;?>").val();

			var ty = $("#__record_cer_fixed_rent_to_year_<?php echo $key;?>").val();
			var tm = $("#__record_cer_fixed_rent_to_month_<?php echo $key;?>").val();
			var td = $("#__record_cer_fixed_rent_to_day_<?php echo $key;?>").val();

			if(fy > 0 && fm > 0 && fd > 0 && ty > 0 && tm > 0 && td > 0)
			{
				number_of_months = monthDiff(fy,fm,fd,ty,tm,td);

				if(number_of_months >=0)
				{
					$("#__record_cer_fixed_rent_number_of_months_<?php echo $key;?>").val(number_of_months);

					var fdate = fd + "." + fm + "." + fy;
					$("#__record_fdate2_<?php echo $key;?>").html(fdate);
					$("#__record_fdate3_<?php echo $key;?>").html(fdate);
					$("#__record_fdate4_<?php echo $key;?>").html(fdate);

					var fdate = td + "." + tm + "." + ty;
					$("#__record_tdate2_<?php echo $key;?>").html(fdate);
					$("#__record_tdate3_<?php echo $key;?>").html(fdate);
					$("#__record_tdate4_<?php echo $key;?>").html(fdate);
				}
			}
			
		  });

		  $("#__record_cer_fixed_rent_from_day_<?php echo $key;?>" ).change(function() {
				
			var fy = $("#__record_cer_fixed_rent_from_year_<?php echo $key;?>").val();
			var fm = $("#__record_cer_fixed_rent_from_month_<?php echo $key;?>").val();
			var fd = $("#__record_cer_fixed_rent_from_day_<?php echo $key;?>").val();

			var ty = $("#__record_cer_fixed_rent_to_year_<?php echo $key;?>").val();
			var tm = $("#__record_cer_fixed_rent_to_month_<?php echo $key;?>").val();
			var td = $("#__record_cer_fixed_rent_to_day_<?php echo $key;?>").val();

			if(fy > 0 && fm > 0 && fd > 0 && ty > 0 && tm > 0 && td > 0)
			{
				number_of_months = monthDiff(fy,fm,fd,ty,tm,td);

				if(number_of_months >=0)
				{
					$("#__record_cer_fixed_rent_number_of_months_<?php echo $key;?>").val(number_of_months);

					var fdate = fd + "." + fm + "." + fy;
					$("#__record_fdate2_<?php echo $key;?>").html(fdate);
					$("#__record_fdate3_<?php echo $key;?>").html(fdate);
					$("#__record_fdate4_<?php echo $key;?>").html(fdate);

					var fdate = td + "." + tm + "." + ty;
					$("#__record_tdate2_<?php echo $key;?>").html(fdate);
					$("#__record_tdate3_<?php echo $key;?>").html(fdate);
					$("#__record_tdate4_<?php echo $key;?>").html(fdate);
				}
			}
			
		  });

		  
		  $("#__record_cer_fixed_rent_to_year_<?php echo $key;?>" ).change(function() {
				
			var fy = $("#__record_cer_fixed_rent_from_year_<?php echo $key;?>").val();
			var fm = $("#__record_cer_fixed_rent_from_month_<?php echo $key;?>").val();
			var fd = $("#__record_cer_fixed_rent_from_day_<?php echo $key;?>").val();

			var ty = $("#__record_cer_fixed_rent_to_year_<?php echo $key;?>").val();
			var tm = $("#__record_cer_fixed_rent_to_month_<?php echo $key;?>").val();
			var td = $("#__record_cer_fixed_rent_to_day_<?php echo $key;?>").val();
			
			if(fy > 0 && fm > 0 && fd > 0 && ty > 0 && tm > 0 && td > 0)
			{
				number_of_months = monthDiff(fy,fm,fd,ty,tm,td);

				if(number_of_months >=0)
				{
					$("#__record_cer_fixed_rent_number_of_months_<?php echo $key;?>").val(number_of_months);

					var fdate = fd + "." + fm + "." + fy;
					$("#__record_fdate2_<?php echo $key;?>").html(fdate);
					$("#__record_fdate3_<?php echo $key;?>").html(fdate);
					$("#__record_fdate4_<?php echo $key;?>").html(fdate);

					var fdate = td + "." + tm + "." + ty;
					$("#__record_tdate2_<?php echo $key;?>").html(fdate);
					$("#__record_tdate3_<?php echo $key;?>").html(fdate);
					$("#__record_tdate4_<?php echo $key;?>").html(fdate);
				}
			}
			
		  });

		  $("#__record_cer_fixed_rent_to_month_<?php echo $key;?>" ).change(function() {
				
			var fy = $("#__record_cer_fixed_rent_from_year_<?php echo $key;?>").val();
			var fm = $("#__record_cer_fixed_rent_from_month_<?php echo $key;?>").val();
			var fd = $("#__record_cer_fixed_rent_from_day_<?php echo $key;?>").val();

			var ty = $("#__record_cer_fixed_rent_to_year_<?php echo $key;?>").val();
			var tm = $("#__record_cer_fixed_rent_to_month_<?php echo $key;?>").val();
			var td = $("#__record_cer_fixed_rent_to_day_<?php echo $key;?>").val();
			
			if(fy > 0 && fm > 0 && fd > 0 && ty > 0 && tm > 0 && td > 0)
			{
				number_of_months = monthDiff(fy,fm,fd,ty,tm,td);

				if(number_of_months >=0)
				{
					$("#__record_cer_fixed_rent_number_of_months_<?php echo $key;?>").val(number_of_months);

					var fdate = fd + "." + fm + "." + fy;
					$("#__record_fdate2_<?php echo $key;?>").html(fdate);
					$("#__record_fdate3_<?php echo $key;?>").html(fdate);
					$("#__record_fdate4_<?php echo $key;?>").html(fdate);

					var fdate = td + "." + tm + "." + ty;
					$("#__record_tdate2_<?php echo $key;?>").html(fdate);
					$("#__record_tdate3_<?php echo $key;?>").html(fdate);
					$("#__record_tdate4_<?php echo $key;?>").html(fdate);
				}
			}
			
		  });


		  $("#__record_cer_fixed_rent_to_day_<?php echo $key;?>" ).change(function() {
				
			var fy = $("#__record_cer_fixed_rent_from_year_<?php echo $key;?>").val();
			var fm = $("#__record_cer_fixed_rent_from_month_<?php echo $key;?>").val();
			var fd = $("#__record_cer_fixed_rent_from_day_<?php echo $key;?>").val();

			var ty = $("#__record_cer_fixed_rent_to_year_<?php echo $key;?>").val();
			var tm = $("#__record_cer_fixed_rent_to_month_<?php echo $key;?>").val();
			var td = $("#__record_cer_fixed_rent_to_day_<?php echo $key;?>").val();
			
			if(fy > 0 && fm > 0 && fd > 0 && ty > 0 && tm > 0 && td > 0)
			{
				number_of_months = monthDiff(fy,fm,fd,ty,tm,td);

				if(number_of_months >=0)
				{
					$("#__record_cer_fixed_rent_number_of_months_<?php echo $key;?>").val(number_of_months);

					var fdate = fd + "." + fm + "." + fy;
					$("#__record_fdate2_<?php echo $key;?>").html(fdate);
					$("#__record_fdate3_<?php echo $key;?>").html(fdate);
					$("#__record_fdate4_<?php echo $key;?>").html(fdate);

					var fdate = td + "." + tm + "." + ty;
					$("#__record_tdate2_<?php echo $key;?>").html(fdate);
					$("#__record_tdate3_<?php echo $key;?>").html(fdate);
					$("#__record_tdate4_<?php echo $key;?>").html(fdate);
				}
			}
			
		  });


		  $("#__record_cer_fixed_rent_from_year_sg_<?php echo $key;?>" ).change(function() {
				
			var fy = $("#__record_cer_fixed_rent_from_year_sg_<?php echo $key;?>").val();
			var fm = $("#__record_cer_fixed_rent_from_month_sg_<?php echo $key;?>").val();
			var fd = $("#__record_cer_fixed_rent_from_day_sg_<?php echo $key;?>").val();

			var ty = $("#__record_cer_fixed_rent_to_year_sg_<?php echo $key;?>").val();
			var tm = $("#__record_cer_fixed_rent_to_month_sg_<?php echo $key;?>").val();
			var td = $("#__record_cer_fixed_rent_to_day_sg_<?php echo $key;?>").val();

			if(fy > 0 && fm > 0 && fd > 0 && ty > 0 && tm > 0 && td > 0)
			{
				number_of_months = monthDiff(fy,fm,fd,ty,tm,td);

				if(number_of_months >=0)
				{
					$("#__record_cer_fixed_rent_number_of_months_sg_<?php echo $key;?>").val(number_of_months);

				}
			}
				
		  });
		  
		  $("#__record_cer_fixed_rent_from_month_sg_<?php echo $key;?>" ).change(function() {
				
			var fy = $("#__record_cer_fixed_rent_from_year_sg_<?php echo $key;?>").val();
			var fm = $("#__record_cer_fixed_rent_from_month_sg_<?php echo $key;?>").val();
			var fd = $("#__record_cer_fixed_rent_from_day_sg_<?php echo $key;?>").val();

			var ty = $("#__record_cer_fixed_rent_to_year_sg_<?php echo $key;?>").val();
			var tm = $("#__record_cer_fixed_rent_to_month_sg_<?php echo $key;?>").val();
			var td = $("#__record_cer_fixed_rent_to_day_sg_<?php echo $key;?>").val();

			if(fy > 0 && fm > 0 && fd > 0 && ty > 0 && tm > 0 && td > 0)
			{
				number_of_months = monthDiff(fy,fm,fd,ty,tm,td);

				if(number_of_months >=0)
				{
					$("#__record_cer_fixed_rent_number_of_months_sg_<?php echo $key;?>").val(number_of_months);
				}
			}
			
		  });

		  $("#__record_cer_fixed_rent_from_day_sg_<?php echo $key;?>" ).change(function() {
				
			var fy = $("#__record_cer_fixed_rent_from_year_sg_<?php echo $key;?>").val();
			var fm = $("#__record_cer_fixed_rent_from_month_sg_<?php echo $key;?>").val();
			var fd = $("#__record_cer_fixed_rent_from_day_sg_<?php echo $key;?>").val();

			var ty = $("#__record_cer_fixed_rent_to_year_sg_<?php echo $key;?>").val();
			var tm = $("#__record_cer_fixed_rent_to_month_sg_<?php echo $key;?>").val();
			var td = $("#__record_cer_fixed_rent_to_day_sg_<?php echo $key;?>").val();
			if(fy > 0 && fm > 0 && fd > 0 && ty > 0 && tm > 0 && td > 0)
			{
				number_of_months = monthDiff(fy,fm,fd,ty,tm,td);

				if(number_of_months >=0)
				{
					$("#__record_cer_fixed_rent_number_of_months_sg_<?php echo $key;?>").val(number_of_months);
				}
			}
			
		  });

		  
		  $("#__record_cer_fixed_rent_to_year_sg_<?php echo $key;?>" ).change(function() {
				
			var fy = $("#__record_cer_fixed_rent_from_year_sg_<?php echo $key;?>").val();
			var fm = $("#__record_cer_fixed_rent_from_month_sg_<?php echo $key;?>").val();
			var fd = $("#__record_cer_fixed_rent_from_day_sg_<?php echo $key;?>").val();

			var ty = $("#__record_cer_fixed_rent_to_year_sg_<?php echo $key;?>").val();
			var tm = $("#__record_cer_fixed_rent_to_month_sg_<?php echo $key;?>").val();
			var td = $("#__record_cer_fixed_rent_to_day_sg_<?php echo $key;?>").val();
			
			if(fy > 0 && fm > 0 && fd > 0 && ty > 0 && tm > 0 && td > 0)
			{
				number_of_months = monthDiff(fy,fm,fd,ty,tm,td);

				if(number_of_months >=0)
				{
					$("#__record_cer_fixed_rent_number_of_months_sg_<?php echo $key;?>").val(number_of_months);
				}
			}
			
		  });

		  $("#__record_cer_fixed_rent_to_month_sg_<?php echo $key;?>" ).change(function() {
				
			var fy = $("#__record_cer_fixed_rent_from_year_sg_<?php echo $key;?>").val();
			var fm = $("#__record_cer_fixed_rent_from_month_sg_<?php echo $key;?>").val();
			var fd = $("#__record_cer_fixed_rent_from_day_sg_<?php echo $key;?>").val();

			var ty = $("#__record_cer_fixed_rent_to_year_sg_<?php echo $key;?>").val();
			var tm = $("#__record_cer_fixed_rent_to_month_sg_<?php echo $key;?>").val();
			var td = $("#__record_cer_fixed_rent_to_day_sg_<?php echo $key;?>").val();
			
			if(fy > 0 && fm > 0 && fd > 0 && ty > 0 && tm > 0 && td > 0)
			{
				number_of_months = monthDiff(fy,fm,fd,ty,tm,td);

				if(number_of_months >=0)
				{
					$("#__record_cer_fixed_rent_number_of_months_sg_<?php echo $key;?>").val(number_of_months);

				}
			}
			
		  });


		  $("#__record_cer_fixed_rent_to_day_sg_<?php echo $key;?>" ).change(function() {
				
			var fy = $("#__record_cer_fixed_rent_from_year_sg_<?php echo $key;?>").val();
			var fm = $("#__record_cer_fixed_rent_from_month_sg_<?php echo $key;?>").val();
			var fd = $("#__record_cer_fixed_rent_from_day_sg_<?php echo $key;?>").val();

			var ty = $("#__record_cer_fixed_rent_to_year_sg_<?php echo $key;?>").val();
			var tm = $("#__record_cer_fixed_rent_to_month_sg_<?php echo $key;?>").val();
			var td = $("#__record_cer_fixed_rent_to_day_sg_<?php echo $key;?>").val();
			
			if(fy > 0 && fm > 0 && fd > 0 && ty > 0 && tm > 0 && td > 0)
			{
				number_of_months = monthDiff(fy,fm,fd,ty,tm,td);

				if(number_of_months >=0)
				{
					$("#__record_cer_fixed_rent_number_of_months_sg_<?php echo $key;?>").val(number_of_months);

				}
			}
			
		  });


		  <?php
		  }
		  foreach($tob_from_months as $key=>$value)
		  {
		  ?>
		  
		  
		  $("#__record_cer_rent_percent_from_sale_from_year_<?php echo $key;?>" ).change(function() {
				
				var fy = $("#__record_cer_rent_percent_from_sale_from_year_<?php echo $key;?>").val();
				var fm = $("#__record_cer_rent_percent_from_sale_from_month_<?php echo $key;?>").val();
				var fd = $("#__record_cer_rent_percent_from_sale_from_day_<?php echo $key;?>").val();

				var ty = $("#__record_cer_rent_percent_from_sale_to_year_<?php echo $key;?>").val();
				var tm = $("#__record_cer_rent_percent_from_sale_to_month_<?php echo $key;?>").val();
				var td = $("#__record_cer_rent_percent_from_sale_to_day_<?php echo $key;?>").val();

				if(fy > 0 && fm > 0 && fd > 0 && ty > 0 && tm > 0 && td > 0)
				{
					number_of_months = monthDiff(fy,fm,fd,ty,tm,td);

					if(number_of_months >=0)
					{
						$("#__record_cer_rent_percent_from_sale_number_of_months_<?php echo $key;?>").val(number_of_months);
					}
				}
					
		  });
			  
		  $("#__record_cer_rent_percent_from_sale_from_month_<?php echo $key;?>" ).change(function() {
				
			var fy = $("#__record_cer_rent_percent_from_sale_from_year_<?php echo $key;?>").val();
			var fm = $("#__record_cer_rent_percent_from_sale_from_month_<?php echo $key;?>").val();
			var fd = $("#__record_cer_rent_percent_from_sale_from_day_<?php echo $key;?>").val();

			var ty = $("#__record_cer_rent_percent_from_sale_to_year_<?php echo $key;?>").val();
			var tm = $("#__record_cer_rent_percent_from_sale_to_month_<?php echo $key;?>").val();
			var td = $("#__record_cer_rent_percent_from_sale_to_day_<?php echo $key;?>").val();

			if(fy > 0 && fm > 0 && fd > 0 && ty > 0 && tm > 0 && td > 0)
			{
				number_of_months = monthDiff(fy,fm,fd,ty,tm,td);

				if(number_of_months >=0)
				{
					$("#__record_cer_rent_percent_from_sale_number_of_months_<?php echo $key;?>").val(number_of_months);
				}
			}
			
		  });

		  $("#__record_cer_rent_percent_from_sale_from_day_<?php echo $key;?>" ).change(function() {
				
			var fy = $("#__record_cer_rent_percent_from_sale_from_year_<?php echo $key;?>").val();
			var fm = $("#__record_cer_rent_percent_from_sale_from_month_<?php echo $key;?>").val();
			var fd = $("#__record_cer_rent_percent_from_sale_from_day_<?php echo $key;?>").val();

			var ty = $("#__record_cer_rent_percent_from_sale_to_year_<?php echo $key;?>").val();
			var tm = $("#__record_cer_rent_percent_from_sale_to_month_<?php echo $key;?>").val();
			var td = $("#__record_cer_rent_percent_from_sale_to_day_<?php echo $key;?>").val();

			if(fy > 0 && fm > 0 && fd > 0 && ty > 0 && tm > 0 && td > 0)
			{
				number_of_months = monthDiff(fy,fm,fd,ty,tm,td);

				if(number_of_months >=0)
				{
					$("#__record_cer_rent_percent_from_sale_number_of_months_<?php echo $key;?>").val(number_of_months);
				}
			}
			
		  });

		  
		  $("#__record_cer_rent_percent_from_sale_to_year_<?php echo $key;?>" ).change(function() {
				
			var fy = $("#__record_cer_rent_percent_from_sale_from_year_<?php echo $key;?>").val();
			var fm = $("#__record_cer_rent_percent_from_sale_from_month_<?php echo $key;?>").val();
			var fd = $("#__record_cer_rent_percent_from_sale_from_day_<?php echo $key;?>").val();

			var ty = $("#__record_cer_rent_percent_from_sale_to_year_<?php echo $key;?>").val();
			var tm = $("#__record_cer_rent_percent_from_sale_to_month_<?php echo $key;?>").val();
			var td = $("#__record_cer_rent_percent_from_sale_to_day_<?php echo $key;?>").val();
			
			if(fy > 0 && fm > 0 && fd > 0 && ty > 0 && tm > 0 && td > 0)
			{
				number_of_months = monthDiff(fy,fm,fd,ty,tm,td);

				if(number_of_months >=0)
				{
					$("#__record_cer_rent_percent_from_sale_number_of_months_<?php echo $key;?>").val(number_of_months);
				}
			}
			
		  });

		  $("#__record_cer_rent_percent_from_sale_to_month_<?php echo $key;?>" ).change(function() {
				
			var fy = $("#__record_cer_rent_percent_from_sale_from_year_<?php echo $key;?>").val();
			var fm = $("#__record_cer_rent_percent_from_sale_from_month_<?php echo $key;?>").val();
			var fd = $("#__record_cer_rent_percent_from_sale_from_day_<?php echo $key;?>").val();

			var ty = $("#__record_cer_rent_percent_from_sale_to_year_<?php echo $key;?>").val();
			var tm = $("#__record_cer_rent_percent_from_sale_to_month_<?php echo $key;?>").val();
			var td = $("#__record_cer_rent_percent_from_sale_to_day_<?php echo $key;?>").val();
			
			if(fy > 0 && fm > 0 && fd > 0 && ty > 0 && tm > 0 && td > 0)
			{
				number_of_months = monthDiff(fy,fm,fd,ty,tm,td);

				if(number_of_months >=0)
				{
					$("#__record_cer_rent_percent_from_sale_number_of_months_<?php echo $key;?>").val(number_of_months);
				}
			}
			
		  });


		  $("#__record_cer_rent_percent_from_sale_to_day_<?php echo $key;?>" ).change(function() {
				
			var fy = $("#__record_cer_rent_percent_from_sale_from_year_<?php echo $key;?>").val();
			var fm = $("#__record_cer_rent_percent_from_sale_from_month_<?php echo $key;?>").val();
			var fd = $("#__record_cer_rent_percent_from_sale_from_day_<?php echo $key;?>").val();

			var ty = $("#__record_cer_rent_percent_from_sale_to_year_<?php echo $key;?>").val();
			var tm = $("#__record_cer_rent_percent_from_sale_to_month_<?php echo $key;?>").val();
			var td = $("#__record_cer_rent_percent_from_sale_to_day_<?php echo $key;?>").val();
			
			if(fy > 0 && fm > 0 && fd > 0 && ty > 0 && tm > 0 && td > 0)
			{
				number_of_months = monthDiff(fy,fm,fd,ty,tm,td);

				if(number_of_months >=0)
				{
					$("#__record_cer_rent_percent_from_sale_number_of_months_<?php echo $key;?>").val(number_of_months);
				}
			}
			
		  });


		  <?php
		  }
		  ?>

	});

</script>