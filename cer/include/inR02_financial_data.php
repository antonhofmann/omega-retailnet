<?php
// Financial Figures
$y = $y + 5;
$pdf->SetXY($margin_left,$y);
$pdf->SetFont("arialn", "B", 10);
$pdf->Cell(63, 5, "3. Financial Figures", 1, "", "L");
$pdf->SetXY($margin_left,$y+5);
$pdf->Cell(63, 92, "", 1, "", "L");

	$x1 = $margin_left+65;
	$x2 = $x1+42;
	$x3 = $x2+42;
	$x4 = $x3+42;
	$x5 = $x4+42;

	//column 1 to 5 Years
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;
		//column 1
		$pdf->SetXY($$xi,$y);
		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(40, 5, $year, 1, "", "C");
		$i++;

	}

	$y = $y + 3.5;
	
	//column 1 to 5 Currency
	for($i=1;$i<6;$i++)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y+1);
		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(20, 3.5, "in T" . $currency_symbol, 1, "", "C");

		$pdf->SetXY($$xi+20,$y+1);
		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(20, 3.5, "in %", 1, "", "C");
	}

	//column 1 to 5 Boxes
	for($i=1;$i<6;$i++)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y+1);
		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(20, 92.5, "", 1, "", "C");
		$pdf->SetXY($$xi+20,$y+1);
		$pdf->Cell(20, 92.5, "", 1, "", "C");
	}
	
	//Sales

	$y = $y + 2;
	$x0 = $margin_left+6;

	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(50, 3.5, "Sales in Units", 0, "", "L");

	$y = $y + $standard_y;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 3.5, BRAND . " Watches", 0, "", "L");

	//print values for columns 1 to 5
	$i = 1;

	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 3.5, $sales_units_watches_values[$year], 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, 3.5, $sales_units_watches_growth[$year], 0, "", "R");
		$i++;
	}	


	$y = $y + $standard_y;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 3.5, BRAND . " Jewellery", 0, "", "L");

	//print values for columns 1 to 5
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 3.5, $sales_units_jewellery_values[$year], 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		//$pdf->Cell(20, 3.5, round($sales_units_jewellery_shares[$year], 1), 0, "", "R");
		$i++;
	}
	
	
	$y = $y + $standard_y;
	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(50, 3.5, "Average Price", 0, "", "L");

	$y = $y + $standard_y;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 3.5, BRAND . " Watches  - in " . $currency_symbol, 0, "", "L");

	//print values for columns 1 to 5
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 3.5, round($exchange_rate*$average_price_watches_values[$year]/$exchange_rate_factor,2), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, 3.5, $average_price_watches_shares[$year], 0, "", "R");
		$i++;
	}	

	$y = $y + $standard_y;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 3.5, BRAND . " Jewellery - in " . $currency_symbol, 0, "", "L");

	//print values for columns 1 to 5
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 3.5, round($exchange_rate*$average_price_jewellery_values[$year]/$exchange_rate_factor,2), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, 3.5, $average_price_jewellery_shares[$year], 0, "", "R");
		$i++;
	}
	
		$y = $y + $standard_y;
	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(50, 3.5, "Sales Value", 0, "", "L");

	$y = $y + $standard_y;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 3.5, BRAND . " Watches", 0, "", "L");

	//print values for columns 1 to 5
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 3.5, round($exchange_rate*$sales_watches_values[$year]/$exchange_rate_factor/1000,0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, 3.5, round($sales_watches_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}	


	$y = $y + $standard_y;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 3.5, BRAND . " Jewellery", 0, "", "L");

	//print values for columns 1 to 5
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 3.5, round($exchange_rate*$sales_jewellery_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, 3.5, round($sales_jewellery_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}


	$y = $y + $standard_y;

	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 3.5, "Customer Services", 0, "", "L");


	//print values for columns 1 to 5
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 3.5, round($exchange_rate*$sales_customer_service_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, 3.5, round($sales_customer_service_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}
	

	$y = $y + $standard_y;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 3.5, "Others", 0, "", "L");


	//print values for columns 1 to 5
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 3.5, round($exchange_rate*$sales_accessories_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, 3.5, round($sales_accessories_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}

	

	$y = $y + $standard_y;

	$pdf->SetXY($margin_left,$y);
	$pdf->Cell(63, 3.5, "", 1, "", "L");

	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "B", 8);
	
	$pdf->Cell(60, 3.5, "Total Gross Sales", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 3.5, round($exchange_rate*$total_gross_sales_values[$year]/$exchange_rate_factor/1000, 0), 1, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, 3.5, round($total_gross_sales_shares[$year], 1). "%", 1, "", "R");
		$i++;
	}	

	$y = $y + 3.5;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 3.5, "Sales Reduction", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 3.5, -round($exchange_rate*$sales_reduction_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, 3.5, -round($sales_reduction_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}	
	
	$y = $y +  $standard_y;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(50, 3.5, "Total Net Sales", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 3.5, round($exchange_rate*$total_net_sales_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, 3.5, round($total_net_sales_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}	

	$y = $y +  $standard_y;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 3.5, "Material of Products Sold", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 3.5, -round($exchange_rate*$material_of_products_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, 3.5, -round($material_of_products_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}	

	$y = $y + $standard_y;
	
	$pdf->SetXY($margin_left,$y);
	$pdf->SetFont("arialn", "B", 3);
	$pdf->Cell(63, 3.5, "", 1, "", "L");

	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(50, 3.5, "Total Gross Margin", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 3.5, round($exchange_rate*$total_gross_margin_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, 3.5, round($total_goss_margin_shares[$year], 1) . "%", 0, "", "R");
		

		$pdf->SetXY($$xi,$y);
		$pdf->SetFont("arialn", "B", 3);
		$pdf->Cell(20, 3.5, "", 1, "", "L");
		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, 3.5, "", 1, "", "L");
		
		
		$pdf->SetFont("arialn", "B", 8);
		$i++;
	}	

	$y = $y + $standard_y;
	
	$pdf->SetXY($margin_left,$y);
	$pdf->SetFont("arialn", "B", 3);
	$pdf->Cell(63, 3.5, "", 1, "", "L");

	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(50, 3.5, "Marketing Expenses", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$tmp =  $marketing_expenses_values[$year];
		$pdf->Cell(20, 3.5, -1 * round($exchange_rate*$tmp/$exchange_rate_factor/1000, 0), 1, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, 3.5, round($marketing_expenses_shares[$year], 1) . "%", 1, "", "R");
		$i++;
	}	

	$y = $y + 4;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 3.5, "Indirect Salaries", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 3.5, -round($exchange_rate*$indirect_salaries_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, 3.5, -round($indirect_salaries_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}	

	$y = $y + $standard_y;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 3.5, "Rents", 0, "", "L");

	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 3.5, -round($exchange_rate*$rents_total_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, 3.5, -round($rents_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}	

	$y = $y + $standard_y;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 3.5, "Aux. Mat., energy, maintenance", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 3.5, -round($exchange_rate*$auxmat_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, 3.5, -round($auxmat_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}
	
	$y = $y + $standard_y;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 3.5, "Sales & administration expenses", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 3.5, -round($exchange_rate*$sales_admin_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, 3.5, -round($sales_admin_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}	

	$y = $y + $standard_y;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 3.5, "Depreciation on fixed assets", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 3.5, -round($exchange_rate*$depreciation_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, 3.5, -round($depreciation_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}
	
	$y = $y + $standard_y;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 3.5, "Depreciation on Goodwill/Keymoney", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 3.5, -round($exchange_rate*$prepayed_rent_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, 3.5, -round($prepayed_rent_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}	

	$y = $y + $standard_y;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 3.5, "Other indirect Expenses", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 3.5, -round($exchange_rate*$other_expenses_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, 3.5, -round($other_expenses_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}	

	$y = $y + $standard_y;
	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(50, 3.5, "Total Indirect Expenses", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 3.5, -round($exchange_rate*$total_indirect_expenses_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, 3.5, -round($total_indirect_expenses_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}	
	
	$y = $y + 3.5;
	
	$pdf->SetFont("arialn", "B", 8);
	$pdf->SetXY($margin_left,$y);
	$pdf->Cell(63, 3.5, "", 1, "", "L");

	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(50, 3.5, "Operating Income w/o Wholesale Margin", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 3.5, round($exchange_rate*$operating_income01_values[$year]/$exchange_rate_factor/1000, 0), 1, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, 3.5, round($operating_income01_shares[$year], 1) . "%", 1, "", "R");
		$i++;
	}	

	
	$y = $y + $standard_y+1;

	//$pdf->SetXY($margin_left,$y);
	//$pdf->SetFont("arialn", "B", 8);
	//$pdf->Cell(63, 3.5, "", 1, "", "L");

	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(50, 3.5, "Operating Income incl. Wholesale Margin", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 3.5, round($exchange_rate*$operating_income02_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, 3.5, round($operating_income02_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}

	
	$y = $y + $standard_y;
	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(50, 3.5, "Operating Income incl. WSM: scenario 80%", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 3.5, round($exchange_rate*$operating_income02_80_percent_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, 3.5, round($operating_income02_80_percent_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}
	
	//summary at bottom of page

	$y = $y + 4.5;

	$pdf->SetXY($margin_left,$y);
	$pdf->SetFont("arialn", "", 3);
	$pdf->Cell(63, 7, "", 1, "", "L");

	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 3.5, "Break Even Retail Margin ", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;
		
		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 7, "", 1, "", "R");

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 3.5, round($break_even_retail_margin[$year], 0), 0, "", "R");
		$i++;
	}

	$y = $y + $standard_y;
	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 3.5, "Break Even Wholesale Margin", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 3.5, round($break_even_wholesale_margin[$year], 0), 0, "", "R");
		$i++;
	}


	$y = $y + $standard_y+0.7;

	$pdf->SetXY($margin_left,$y);
	$pdf->Cell(63, 3.5, "", 1, "", "L");

	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 3.5, "Cash Flow", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, 3.5, round($exchange_rate*$cash_flow_values[$year]/$exchange_rate_factor/1000, 0), 1, "", "R");
		$i++;
	}	
?>