<?php
/********************************************************************

    get_draft_funtions.php

    Various utility functions to get information from tables.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-02-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-02-16

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

///get all brands
$cer_brands = array();
$first_brand = 1;
$sql = "select DISTINCT cer_revenue_brand_id, cer_brand_name " . 
       " from cer_draft_revenues " . 
	   " left join cer_brands on cer_brand_id = cer_revenue_brand_id " . 
	   " where cer_revenue_draft_id = " . dbquote(param("did")) . 
	   " order by cer_brand_name";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$cer_brands[$row["cer_revenue_brand_id"]] = $row["cer_brand_name"];
}
$tmp = array_keys($cer_brands);
if(count($tmp) > 0)
{
	$first_brand = $tmp[0];
	asort($cer_brands);
}

/********************************************************************
    get currency
*********************************************************************/
function get_currency_symbol($currency_id)
{
    $currency = array();
    
	$sql = "select * from currencies " . 
		   "where currency_id = " . $currency_id;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$currency["id"] = $row["currency_id"];
		$currency["symbol"] = $row["currency_symbol"];
	}

    else
    {
        $currency["id"] = 0;
        $currency["symbol"] = "";
    }
    
    return $currency["symbol"];
}


/********************************************************************
    get project and order data 
*********************************************************************/
function get_project($id)
{
    $project = array();

    $sql = $sql = "select * " . 
       "from projects " . 
	   "left join orders on order_id = project_order " .
	   "left join countries on country_id = order_shop_address_country " .
	   "left join product_lines on product_line_id = project_product_line " .
	    "left join productline_subclasses on productline_subclass_id = project_product_line_subclass " .
	   "left join postypes on postype_id = project_postype ".
	   "left join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join projectkinds on projectkind_id = project_projectkind " .
       "left join addresses on address_id = order_client_address " .
	   "left join currencies on currency_id = country_currency " . 
       "left join users on user_id = project_retail_coordinator " . 
	   "where project_id = " . $id;


    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $project = $row;
    }

	
	$project["order_franchisee_address_country_name"] = "";
	$sql = "select country_name ".
           "from countries ".
           "where country_id  = " . dbquote($project["order_franchisee_address_country"]);

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $project["order_franchisee_address_country_name"] = $row["country_name"];
    }

    
	$project["order_shop_address_country_name"] = "";

	$sql = "select country_name ".
           "from countries ".
           "where country_id  = " . dbquote($project["order_shop_address_country"]);

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $project["order_shop_address_country_name"] = $row["country_name"];
    }

    $sql = "select country_name ".
           "from countries ".
           "where country_id  = " . dbquote($project["order_billing_address_country"]);

    $res = mysql_query($sql) or dberror($sql);

	$project["order_billing_address_country_name"] = "";
    if ($row = mysql_fetch_assoc($res))
    {
        $project["order_billing_address_country_name"] = $row["country_name"];
    }
	
	//get pos address from POS Index
	$sql = "select posorder_posaddress from posorders where posorder_order = " . $project["order_id"];
	$res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $project["posaddress_id"] = $row["posorder_posaddress"];
    }
	else
	{
		$project["posaddress_id"] = "";
	}

	//postypes CER/AF
	$project["needs_cer"] = 0;
	$project["needs_af"] = 0;

	$sql = "select posproject_type_needs_cer, posproject_type_needs_af from posproject_types " . 
	       "where posproject_type_postype = " . $project["project_postype"] .  
		   " and posproject_type_projectcosttype = " . $project["project_cost_type"] . 
		   " and posproject_type_projectkind = " . $project["project_projectkind"];

	$res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $project["needs_cer"] = $row["posproject_type_needs_cer"];
		$project["needs_af"] = $row["posproject_type_needs_af"];
    }
    

    return $project;
}

/********************************************************************
    gbuld missing investment records
*********************************************************************/
function build_missing_draft_investment_records($draft_id)
{
	$sql = "select * from posinvestment_types where posinvestment_type_active = 1";
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$sql_i = "select cer_investment_id from cer_draft_investments " .
				 "where cer_investment_draft_id = " .  $draft_id . 
				 " and cer_investment_type = " . $row["posinvestment_type_id"];


		$res_i = mysql_query($sql_i) or dberror($sql_i);
		if (!$row_i = mysql_fetch_assoc($res_i))
		{
			$sql = "insert into cer_draft_investments (" .
				   "cer_investment_draft_id, " .
				   "cer_investment_type, " .
				   "user_created, date_created) values (".
				   $draft_id . ", " .
				   $row["posinvestment_type_id"] . ", " . 
				   dbquote(user_login()) . ", " . 
				   "current_timestamp)";


			$result = mysql_query($sql) or dberror($sql);
		}
	}
	
	return true;
}

/********************************************************************
    get intangibles
*********************************************************************/
function get_intangibles($draft_id, $investment_type)
{
	$intangibles = array();
	
	$sql = "select * from cer_draft_investments " .
		   "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
		   "where cer_investment_draft_id = " . $draft_id .
		   " and cer_investment_type = $investment_type ";

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$intangibles = $row;
	}
	return $intangibles;
}


/********************************************************************
    get draft_basicdata
*********************************************************************/
function get_draft_basicdata($draft_id)
{

	$basic_data = array();

    $sql = "select * from cer_drafts " . 
	       "left join currencies on currency_id = cer_basicdata_currency " . 
	       "where cer_basicdata_id = " . dbquote($draft_id);
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $basic_data = $row;
    }

	return $basic_data;

}

/********************************************************************
    get cer currency
*********************************************************************/
function get_draft_currency($draft_id)
{
    $currency = array();
    
	$sql = "select * from cer_drafts " . 
		   "left join currencies on currency_id = cer_basicdata_currency " . 
		   "where cer_basicdata_id = " . $draft_id;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$currency["id"] = $row["cer_basicdata_currency"];
		$currency["symbol"] = $row["currency_symbol"];
		$currency["exchange_rate"] = $row["cer_basicdata_exchangerate"];
		$currency["factor"] = $row["cer_basicdata_factor"];
	}

    else
    {
        $currency["id"] = 0;
        $currency["symbol"] = "";
        $currency["exchange_rate"] = 0;
        $currency["factor"] = 0;
    }
    
    return $currency;
}


/*************************************************************************************
    check if revenue data exists
*************************************************************************************/
function check_revenues($draft_id, $first_year, $last_year, $selected_brands = array())
{

	if(count($selected_brands) > 0)
	{

		foreach($selected_brands as $brand_id=>$brand_name)
		{
		
			$existing_years = array();

			for($year = $first_year;$year <= $last_year; $year++)
			{
				$existing_years[] = $year;

				$sql = "select count(cer_revenue_id) as num_recs " .
						"from cer_draft_revenues " . 
						"where cer_revenue_year =  " . $year .
						" and cer_revenue_draft_id = " . $draft_id . 
						" and cer_revenue_brand_id = " . $brand_id;

				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				if ($row["num_recs"] == 0)
				{
					$fields = array();
					$values = array();

					$fields[] = "cer_revenue_draft_id";
					$values[] = $draft_id;

					$fields[] = "cer_revenue_brand_id";
					$values[] = $brand_id;

					$fields[] = "cer_revenue_year";
					$values[] = dbquote($year);
					
					$fields[] = "date_created";
					$values[] = "now()";

					$fields[] = "date_modified";
					$values[] = "now()";

					$fields[] = "user_created";
					$values[] = dbquote(user_login());

					$fields[] = "user_modified";
					$values[] = dbquote(user_login());

					$sql = "insert into cer_draft_revenues (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					
					mysql_query($sql) or dberror($sql);
				}
			}

			//delete records not enclosed in the business plan period
			$sql = "select distinct cer_revenue_year " .
				   "from cer_draft_revenues " .
				   " where cer_revenue_draft_id = " . $draft_id . 
						" and cer_revenue_brand_id = " . $brand_id;
			$res = mysql_query($sql) or dberror($sql);
			while ($row = mysql_fetch_assoc($res))
			{
				if(!in_array($row["cer_revenue_year"], $existing_years))
				{
					$sql = "delete from cer_draft_revenues " . 
					   "where cer_revenue_year = " . $row["cer_revenue_year"] . 
					   " and cer_revenue_draft_id = " . $draft_id . 
						" and cer_revenue_brand_id = " . $brand_id;

					$result = mysql_query($sql) or dberror($sql);
				}
			
			}
		}
	}

	return true;
}


/********************************************************************
    get pos data
*********************************************************************/
function get_pos_data($order_id)
{
	$posdata = array();
	$project_is_in_pipeline = 0;

	//check if project is in pipeline
	$sql = "select count(posorder_id) as num_recs " . 
		   "from posorderspipeline " . 
	       "where posorder_order = " . $order_id;

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	if ($row["num_recs"] > 0)
	{
		$project_is_in_pipeline = 1;
	}

	if($project_is_in_pipeline == 1)
	{
		$parent_table = "";
		$sql = "select * from posorderspipeline " . 
			   "where posorder_order = " . $order_id;


		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$parent_table = $row["posorder_parent_table"];
		}
		
		$sql = "select * from posorderspipeline " . 
			   "left join " . $parent_table . " on posaddress_id = posorder_posaddress " . 
			   "where posorder_order = " . $order_id;

		
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			
			/*
			if(!$row["posaddress_id"])
			{
				$sql = "select * from posorderspipeline " . 
					   "left join posaddresses on posaddress_id = posorder_posaddress " . 
					   "where posorder_order = " . $order_id;
				$res = mysql_query($sql) or dberror($sql);

				if ($row = mysql_fetch_assoc($res))
				{
					$posdata = $row;
					$posdata["table"] = "posaddresses";
					$posdata["table_leases"] = "posleasepipeline";
				}
			}
			else
			{
				$posdata = $row;
				$posdata["table"] = "posaddressespipeline";
				$posdata["table_leases"] = "posleasepipeline";
			}
			*/

			$posdata = $row;
			$posdata["table"] = $parent_table;
			$posdata["table_leases"] = "posleasepipeline";
			
		}
		
		if(count($posdata) > 0)
		{

			$areas = "";
			$sql = "select * from posareaspipeline " .
				   "left join posareatypes on posareatype_id = posarea_area " . 
				   "where posarea_posaddress = " . dbquote($posdata["posaddress_id"]);
			
			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{
				$areas .= $row["posareatype_name"] . ", ";
			}
			
			$areas = substr($areas, 0, strlen($areas)-2);
			$posdata["posareas"] = $areas;
		}
	}
	else
	{
		$sql = "select * from posorders " . 
			   "left join posaddresses on posaddress_id = posorder_posaddress " . 
			   "where posorder_order = " . $order_id;

		
		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$posdata = $row;
			$posdata["table"] = "posaddresses";
			$posdata["table_leases"] = "posleases";
		}

		if(count($posdata) > 0)
		{
			$areas = "";
			$sql = "select * from posareas " .
				   "left join posareatypes on posareatype_id = posarea_area " . 
				   "where posarea_posaddress = " . dbquote($posdata["posaddress_id"]);
			
			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{
				$areas .= $row["posareatype_name"] . ", ";
			}

			$areas = substr($areas, 0, strlen($areas)-2);

			$posdata["posareas"] = $areas;
		}

	}
	
	if(count($posdata) > 0)
	{
		$sql = "select country_id, country_name ".
			   "from countries ".
			   "where country_id = " . dbquote($posdata["posaddress_country"]);

		$res = mysql_query($sql);
		if ($res)
		{
			$row = mysql_fetch_assoc($res);
			$posdata["country_name"] = $row['country_name'];
		}
	}

	return $posdata;
}

/********************************************************************
    get pos lease data
*********************************************************************/
function get_pos_leasedata($posaddress_id, $project_order = 0 )
{
	$posleasedata = array();
	$project_is_in_pipeline = 0;

	//check if project is in pipeline
	$sql = "select count(posorder_id) as num_recs " . 
		   "from posorderspipeline " . 
	       "where posorder_order = " . $project_order;

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	if ($row["num_recs"] > 0)
	{
		$project_is_in_pipeline = 1;
	}

	if($project_is_in_pipeline == 1)
	{
		$sql = "select * from posleasespipeline " . 
			   " left join poslease_types on poslease_type_id = poslease_lease_type " .
			   " where poslease_posaddress = " . $posaddress_id .
			   " and poslease_order = " . $project_order .
			   " order by poslease_startdate DESC ";

		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$posleasedata = $row;
			$posleasedata["table"] = "posleasespipeline";
		}
	}
	else
	{
		$sql = "select * from posleases " .
			   "left join poslease_types on poslease_type_id = poslease_lease_type " .
			   "where poslease_posaddress = " . $posaddress_id .
			   " and poslease_order = " . $project_order .
			   " order by poslease_startdate DESC ";

		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$posleasedata = $row;
			$posleasedata["table"] = "posleases";
		}
	
	}



	$rental_duration = "";
	if(count($posleasedata) > 0)
	{
		if($posleasedata["poslease_startdate"] != NULL 
			and $posleasedata["poslease_startdate"] != '0000-00-00' 
			and $posleasedata["poslease_enddate"] != NULL
			and $posleasedata["poslease_enddate"] != '0000-00-00')
		{
			$diff = abs(strtotime($posleasedata["poslease_enddate"]) - strtotime($posleasedata["poslease_startdate"]));
			$years = floor($diff / (365*60*60*24));
			$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));

			if($months == 12)
			{
				$months = 0;
				$years++;
			}

			$rental_duration = $years . " years and " . $months . " months";
		}
	}

	if(count($posleasedata) > 0)
	{
		$posleasedata["rental_duration"] = $rental_duration;
	}

	return $posleasedata;

}

/********************************************************************
    check if expense data exists
*********************************************************************/
function check_expenses($draft_id, $first_year, $last_year)
{
	$existing_years = array();

	for($year = $first_year;$year <= $last_year; $year++)
	{
		$existing_years[] = $year;
	}

	
	//check if expenses for all years ar present
	$sql = "select count(cer_expense_type_id) as num_recs " . 
		   "from cer_expense_types " .
		   "where cer_expense_type_active = 1 " . 
		  "order by cer_expense_type_sortorder";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);

	$num_of_expenses = $row["num_recs"];
	$num_of_years = $last_year - $first_year + 1;
	$num_of_expenses_that_should_be = $num_of_expenses * $num_of_years;


	$sql_e = "select count(cer_expense_id) as num_recs " .
		     "from cer_draft_expenses " . 
		     "where cer_expense_draft_id = " . $draft_id;

	$res_e = mysql_query($sql_e) or dberror($sql_e);
	$row_e = mysql_fetch_assoc($res_e);
	
	if($row_e['num_recs'] < $num_of_expenses_that_should_be)
	{
		for($year = $first_year;$year <= $last_year; $year++)
		{
			//create cer_expense records
			$sql = "select * from cer_expense_types " .
				   "where cer_expense_type_active = 1 " . 
				   "order by cer_expense_type_sortorder";
			$res = mysql_query($sql) or dberror($sql);
			while ($row = mysql_fetch_assoc($res))
			{
				
				$sql_e = "select count(cer_expense_id) as num_recs " .
					   "from cer_draft_expenses " . 
					   "where cer_expense_type =  " . $row["cer_expense_type_id"] .
					   " and cer_expense_year = " . dbquote($year) . 
					   " and cer_expense_draft_id = " . $draft_id;

				$res_e = mysql_query($sql_e) or dberror($sql_e);
				$row_e = mysql_fetch_assoc($res_e);
				if ($row_e["num_recs"] == 0)
				{
				
					$fields = array();
					$values = array();

					$fields[] = "cer_expense_draft_id";
					$values[] = $draft_id;

					$fields[] = "cer_expense_type";
					$values[] = dbquote($row["cer_expense_type_id"]);
					
					$fields[] = "cer_expense_year";
					$values[] = $year;

					$fields[] = "date_created";
					$values[] = "now()";

					$fields[] = "date_modified";
					$values[] = "now()";

					$fields[] = "user_created";
					$values[] = dbquote(user_login());

					$fields[] = "user_modified";
					$values[] = dbquote(user_login());

					$sql = "insert into cer_draft_expenses (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					
					mysql_query($sql) or dberror($sql);
				}
				
			}
		}
	}

	//delete records not enclosed in the business plan period
	$sql = "select distinct cer_expense_year " .
		   "from cer_draft_expenses " .
		   " where cer_expense_draft_id = " . $draft_id;

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		if(!in_array($row["cer_expense_year"], $existing_years))
		{
			$sql = "delete from cer_draft_expenses " . 
			   "where cer_expense_year = " . $row["cer_expense_year"] . 
			   " and cer_expense_draft_id = " . $draft_id;

			$result = mysql_query($sql) or dberror($sql);
		}
	
	}

	return true;
}


/********************************************************************
    check if stock data exists
*********************************************************************/
function check_stocks($draft_id, $first_year, $last_year)
{

	$existing_years = array();

	for($year = $first_year;$year <= $last_year; $year++)
	{
		$existing_years[] = $year;
	
		$sql = "select count(cer_stock_id) as num_recs " .
				"from cer_draft_stocks " . 
				"where cer_stock_year = " . $year . 
			    " and cer_stock_draft_id = " . $draft_id;

		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);
		if ($row["num_recs"] == 0)
		{
			$fields = array();
			$values = array();

			$fields[] = "cer_stock_draft_id";
			$values[] = $draft_id;

			$fields[] = "cer_stock_year";
			$values[] = dbquote($year);
			
			$fields[] = "date_created";
			$values[] = "now()";

			$fields[] = "date_modified";
			$values[] = "now()";

			$fields[] = "user_created";
			$values[] = dbquote(user_login());

			$fields[] = "user_modified";
			$values[] = dbquote(user_login());

			$sql = "insert into cer_draft_stocks (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			
			mysql_query($sql) or dberror($sql);
		}
	}

	//delete records not enclosed in the business plan period
	$sql = "select distinct cer_stock_year " .
		   "from cer_draft_stocks " .
		   " where cer_stock_draft_id = " . $draft_id;

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		if(!in_array($row["cer_stock_year"], $existing_years))
		{
			$sql = "delete from cer_draft_stocks " . 
			   "where cer_stock_year = " . $row["cer_stock_year"] . 
			   " and cer_stock_draft_id = " . $draft_id;

			$result = mysql_query($sql) or dberror($sql);
		}
	
	}

	return true;
}


/********************************************************************
    check if paymentterma data exists
*********************************************************************/
function check_paymentterms($draft_id, $first_year, $last_year)
{

	$existing_years = array();

	for($year = $first_year;$year <= $last_year; $year++)
	{
		$existing_years[] = $year;

		$sql = "select count(cer_paymentterm_id) as num_recs " .
				"from cer_draft_paymentterms " . 
				"where cer_paymentterm_year = " . $year . 
			    " and cer_paymentterm_draft_id = " . $draft_id;

		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);
		if ($row["num_recs"] == 0)
		{
			$fields = array();
			$values = array();

			$fields[] = "cer_paymentterm_draft_id";
			$values[] = $draft_id;

			$fields[] = "cer_paymentterm_year";
			$values[] = dbquote($year);
			
			$fields[] = "date_created";
			$values[] = "now()";

			$fields[] = "date_modified";
			$values[] = "now()";

			$fields[] = "user_created";
			$values[] = dbquote(user_login());

			$fields[] = "user_modified";
			$values[] = dbquote(user_login());

			$sql = "insert into cer_draft_paymentterms (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			mysql_query($sql) or dberror($sql);
		}
	}

	//delete records not enclosed in the business plan period
	$sql = "select distinct cer_paymentterm_year " .
		   "from cer_draft_paymentterms " .
		   " where cer_paymentterm_draft_id = " . $draft_id;

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		if(!in_array($row["cer_paymentterm_year"], $existing_years))
		{
			$sql = "delete from cer_draft_paymentterms " . 
			   "where cer_paymentterm_year = " . $row["cer_paymentterm_year"] . 
			   " and cer_paymentterm_draft_id = " . $draft_id;

			$result = mysql_query($sql) or dberror($sql);
		}
	
	}

	return true;
}


/********************************************************************
    caclulate cost of products sold
*********************************************************************/
function update_cost_of_products_sold($draft_id, $version = 0)
{
	
	//get all brands involved
	$brand_ids = array();
	$sql = "select DISTINCT cer_revenue_brand_id " . 
		   " from  cer_draft_revenues " .
		   " where cer_revenue_draft_id = " . $draft_id;

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$brand_ids[$row["cer_revenue_brand_id"]] = $row["cer_revenue_brand_id"];
	}
	
	
	$years = array();
	$revenue_watches = array();
	$revenue_jewellery = array();
	$revenue_services = array();

	$cer_basicdata = get_draft_basicdata($draft_id);

	//caclulate net sales, based on net sales values, calculated backwards up to gross sales
	$total_gross_sales_values = array();
	$total_net_sales_values = array();
	$gross_sales_watches_values = array();
	$gross_sales_jewellery_values = array();
	$gross_sales_customer_service_values = array();

	$net_sales_watches_values = array();
	$net_sales_jewellery_values = array();
	$net_sales_accessories_values = array();
	$net_sales_customer_service_values = array();

	$cost_watches_values = array();
	$cost_sales_jewellery_values = array();
	$cost_sales_accessories_values = array();
	$cost_sales_customer_service_values = array();

	//get sales planning
	foreach($brand_ids as $key=>$brand_id)
	{
	
		$sql = "select * from cer_draft_revenues " .
			   "where cer_revenue_draft_id = " . $draft_id . 
			   " and cer_revenue_brand_id = " . $brand_id;

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$year = $row["cer_revenue_year"];
			$years[$year] = $year;
			
			/*
			$sales_reduction_watches = $row["cer_revenue_sales_reduction_watches"] + $row["cer_revenue_reduction_credit_cards"];
			$sales_reduction_bijoux = $row["cer_revenue_sales_reduction_bijoux"] + $row["cer_revenue_reduction_credit_cards"];
			$sales_reduction_accessories = $row["cer_revenue_sales_reduction_accessories"] + $row["cer_revenue_reduction_credit_cards"];
			$sales_reduction_services = $row["cer_revenue_sales_reduction_cservice"] + $row["cer_revenue_reduction_credit_cards"];
			*/

			$sales_reduction_watches = $row["cer_revenue_sales_reduction_watches"];
			$sales_reduction_bijoux = $row["cer_revenue_sales_reduction_bijoux"];
			$sales_reduction_accessories = $row["cer_revenue_sales_reduction_accessories"];
			$sales_reduction_services = $row["cer_revenue_sales_reduction_cservice"];

			$cost_watches = $row["cer_revenue_cost_watches"] + $row["cer_revenue_reduction_credit_cards"];
			$cost_bijoux = $row["cer_revenue_cost_jewellery"] + $row["cer_revenue_reduction_credit_cards"];
			$cost_accessories = $row["cer_revenue_cost_accessories"] + $row["cer_revenue_reduction_credit_cards"];
			$cost_services = $row["cer_revenue_cost_service"] + $row["cer_revenue_reduction_credit_cards"];

			if(array_key_exists($year, $gross_sales_watches_values))
			{
				$gross_sales_watches_values[$year] = $gross_sales_watches_values[$year] + $row["cer_revenue_watches"];
				$gross_sales_jewellery_values[$year] = $gross_sales_jewellery_values[$year] + $row["cer_revenue_jewellery"];
				$gross_sales_accessories_values[$year] = $gross_sales_accessories_values[$year] + $row["cer_revenue_accessories"];
				$gross_sales_customer_service_values[$year] = $gross_sales_customer_service_values[$year] + $row["cer_revenue_customer_service"];

				$net_sales_watches_values[$year] = $net_sales_watches_values[$year] + $row["cer_revenue_watches"] - ($row["cer_revenue_watches"]*$sales_reduction_watches/100);
				$net_sales_jewellery_values[$year] = $net_sales_jewellery_values[$year] + $row["cer_revenue_jewellery"] - ($row["cer_revenue_jewellery"]*$sales_reduction_bijoux/100); 
				$net_sales_accessories_values[$year] = $net_sales_accessories_values[$year] + $row["cer_revenue_accessories"] - ($row["cer_revenue_accessories"]*$sales_reduction_accessories/100);
				$net_sales_customer_service_values[$year] = $net_sales_customer_service_values[$year] + $row["cer_revenue_customer_service"]- ($row["cer_revenue_customer_service"]*$sales_reduction_services/100);
			}
			else
			{
				$gross_sales_watches_values[$year] = $row["cer_revenue_watches"];
				$gross_sales_jewellery_values[$year] =$row["cer_revenue_jewellery"];
				$gross_sales_accessories_values[$year] = $row["cer_revenue_accessories"];
				$gross_sales_customer_service_values[$year] = $row["cer_revenue_customer_service"];

				$net_sales_watches_values[$year] = $row["cer_revenue_watches"] - ($row["cer_revenue_watches"]*$sales_reduction_watches/100);
				$net_sales_jewellery_values[$year] = $row["cer_revenue_jewellery"] - ($row["cer_revenue_jewellery"]*$sales_reduction_bijoux/100); 
				$net_sales_accessories_values[$year] = $row["cer_revenue_accessories"] - ($row["cer_revenue_accessories"]*$sales_reduction_accessories/100);
				$net_sales_customer_service_values[$year] = $row["cer_revenue_customer_service"]- ($row["cer_revenue_customer_service"]*$sales_reduction_services/100);
			}
		}
	}


	foreach($years as $key=>$year) {

		if($cer_basicdata["cer_basicdata_cost_on_net_sales"] == 1)
		{
			$cost_watches_values[$year] = ($net_sales_watches_values[$year] * $cost_watches / 100);

			$cost_sales_jewellery_values[$year] = ($net_sales_jewellery_values[$year] * $cost_bijoux / 100);

			$cost_sales_accessories_values[$year] = ($net_sales_accessories_values[$year] * $cost_accessories / 100);

			$cost_sales_customer_service_values[$year] = ($net_sales_customer_service_values[$year] * $cost_services / 100);
		}
		else
		{
			$cost_watches_values[$year] = ($gross_sales_watches_values[$year] * $cost_watches / 100);
			
			$cost_sales_jewellery_values[$year] = ($gross_sales_jewellery_values[$year] * $cost_bijoux / 100);

			$cost_sales_accessories_values[$year] = ($gross_sales_accessories_values[$year] * $cost_accessories / 100);

			$cost_sales_customer_service_values[$year] = ($gross_sales_customer_service_values[$year] * $cost_services / 100);
		}

		$cost_of_production[$year] = $cost_watches_values[$year] + $cost_sales_jewellery_values[$year] + $cost_sales_accessories_values[$year] + $cost_sales_customer_service_values[$year];

		
		$fields = array();

		$value = dbquote($cost_of_production[$year]);
		$fields[] = "cer_expense_amount = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}

		$sql = "update cer_draft_expenses set " . join(", ", $fields) . " where cer_expense_draft_id = " .$draft_id . " and cer_expense_type = 15 and cer_expense_year = " . dbquote($year);
		mysql_query($sql) or dberror($sql);
	}


	$cost_of_production_values = array('cost_watches_values'=>$cost_watches_values, 'cost_sales_jewellery_values'=>$cost_sales_jewellery_values, 
		'cost_sales_accessories_values'=>$cost_sales_accessories_values,
		'cost_sales_customer_service_values'=>$cost_sales_customer_service_values);
	return $cost_of_production_values;
}



/********************************************************************
    calculate inflated amounts
*********************************************************************/
function get_inflation_rate($draft_id, $year)
{
	$irate = 0;
	//get inflation rates
	$inflationrates = array();
	$sql = "select cer_basicdata_inflationrates, cer_basicdata_firstyear, cer_basicdata_lastyear " .
		   "from cer_drafts " . 
		   "where cer_basicdata_id = " . $draft_id;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$cer_basicdata_inflation_rates = unserialize($row["cer_basicdata_inflationrates"]);
		
		//get the first matching inflation rate
		$inflation_rate_tmp = 0;
		foreach($cer_basicdata_inflation_rates as $year=>$rate)
		{
			if($row["cer_basicdata_firstyear"] > $year)
			{
				$inflation_rate_tmp = $rate;
			}
			elseif($row["cer_basicdata_firstyear"] == $year)
			{
				$inflation_rate_tmp = $cer_basicdata_inflation_rates[$row["cer_basicdata_firstyear"]];
			}
		
		}
		/*
		if(!array_key_exists($row["cer_basicdata_firstyear"],$cer_basicdata_inflation_rates))
		{
			$inflation_rate_tmp = $cer_basicdata_inflation_rates[$row["cer_basicdata_firstyear"]];
		}
		else
		{
			$inflation_rate_tmp = 0;
		}
		*/

		for($y=$row["cer_basicdata_firstyear"];$y<=$row["cer_basicdata_lastyear"];$y++)
		{
			$years[] = $y;

			if(!array_key_exists($y,$cer_basicdata_inflation_rates))
			{
				$cer_basicdata_inflation_rates[$y] = $inflation_rate_tmp;
			}
			else
			{
				$inflation_rate_tmp = $cer_basicdata_inflation_rates[$y];
			}
		}
	}

	return $cer_basicdata_inflation_rates[$year];
}

/********************************************************************
    calculate inflated amounts
*********************************************************************/
function calculate_inflated_amounts($draft_id, $amounts, $starting_year, $starting_month)
{
	$amount_base = 0;
	foreach($amounts as $year=>$amount)
	{
		
		if($amount_base == 0)
		{
			if($year == $starting_year)
			{
				//calculate amount for 12 Months
				$amount_base = 12*$amount/(13-$starting_month);
			}
			else
			{
				$amount_base = $amount;
			}
		}

	
		if($starting_year < $year)
		{
			$irate = get_inflation_rate($draft_id, $year);
			$s = $amount_base + ($amount_base * $irate / 100);

			//echo $amount_base . ":" . $s . "<br>";
			$amounts[$year] = $s;
			$amount_base = $s;

			//echo "1Y: " . $year . " amount: " . $s . " irate: " . $irate . " amount base: " . $amount_base ."<br>";
		}
		else
		{
			//$irate = get_inflation_rate($draft_id, $year);
			$irate = 0;
			$number_of_months = 13 - $starting_month;
			$s = $number_of_months*$amount_base/12;
			//$s = $s + ($s * $irate / 100);
			$amounts[$year] = $s;
			
			$sn = $amount_base + ($amount_base * $irate / 100);
			$amount_base = $sn;

			
		}
		//echo "2Y: " . $year . " amount: " . $s . " irate: " . $irate . " amount base: " . $amount_base ."<br>";
	
	}

	return $amounts;
}


/********************************************************************
    calculate fixed growing amounts
*********************************************************************/
function calculate_fixed_growth_amounts($amounts, $starting_year, $starting_month, $percentage)
{
	$amount_base = 0;
	foreach($amounts as $year=>$amount)
	{
		
		if($amount_base == 0)
		{
			if($year == $starting_year)
			{
				//calculate amount for 12 Months
				$amount_base = 12*$amount/(13-$starting_month);
			}
			else
			{
				$amount_base = $amount;
			}
		}

	
		if($starting_year < $year)
		{
			$irate = $percentage;
			$s = $amount_base + ($amount_base * $irate / 100);
			$amounts[$year] = $s;
			$amount_base = $s;
		}
		else
		{
			$irate = 0;
			$number_of_months = 13 - $starting_month;
			$s = $number_of_months*$amount_base/12;
			$amounts[$year] = $s;
			
			$sn = $amount_base + ($amount_base * $irate / 100);
			$amount_base = $sn;

			
		}
	}

	return $amounts;
}

/********************************************************************
    calculate forcasted salaries
*********************************************************************/
function calculate_forcasted_salaries($draft_id, $years, $country)
{
	$basic_data = get_draft_basicdata($draft_id);
	$salaries = array();
	$sql = "select * from cer_draft_salaries where cer_salary_draft_id = " . $draft_id;
	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$salaries = array();
		foreach($years as $key=>$year)
		{
			$salaries[$year] = 0;
		}
		
		foreach($salaries as $year=>$value)
		{
			if($row["cer_salary_year_starting"] < $year)
			{
				$s = $row["cer_salary_total"] * $row["cer_salary_headcount_percent"] / 100;
				$s = $s + $salaries[$year];
				$salaries[$year] = $s;
			}
			elseif($row["cer_salary_year_starting"] == $year) // only a part of the year
			{
				$s = $row["cer_salary_total"] * $row["cer_salary_headcount_percent"] / 100;
				$s = ($s / 12 ) * (13 - $row["cer_salary_month_starting"]);
				$s = $s + $salaries[$year];
				$salaries[$year] = $s;

			}
		}
		
		if($basic_data["cer_basicdata_salary_growth"] > 0)
		{
			//fixed growth
			$salaries = calculate_fixed_growth_amounts($salaries, $row["cer_salary_year_starting"], $row["cer_salary_month_starting"], $basic_data["cer_basicdata_salary_growth"]);
		}
		else
		{
			//calculate inflation
			$salaries = calculate_inflated_amounts($draft_id, $salaries, $row["cer_salary_year_starting"], $row["cer_salary_month_starting"]);
		}

		

		//last year
		$salaries[$basic_data["cer_basicdata_lastyear"]] = $salaries[$basic_data["cer_basicdata_lastyear"]]/ 12 * $basic_data["cer_basicdata_lastmonth"];

		$salary_rows[] = $salaries;

	}

	if(count($salaries) > 0)
	{
		// add all salary rows
		$salaries = array();
		foreach($salary_rows as $key=>$salary_row)
		{
			foreach($salary_row as $year=>$salary)
			{
				if(array_key_exists($year, $salaries))
				{
					$s = $salaries[$year] + $salary;
					$salaries[$year] = $s;
				}
				else
				{
					$salaries[$year] = $salary;
				}
			}
		}

	
		//get the starting year an month
		
		$year_starting = 0;
		$month_starting = 0;

		$sql = "select min(cer_salary_year_starting) as firstyear " .
			   "from cer_draft_salaries " . 
			   "where cer_salary_draft_id = " . $draft_id;

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$year_starting = $row["firstyear"];
		}

		$sql = "select min(cer_salary_month_starting) as firstmonth " . 
			   "from cer_draft_salaries " . 
			   "where cer_salary_draft_id = " . $draft_id . 
			   " and cer_salary_year_starting = " . $year_starting;
		
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$month_starting = $row["firstmonth"];
		}


		// update expenses with salaries

		$i = 1;
		foreach($salaries as $year=>$value)
		{
			$sql = "update cer_draft_expenses SET " . 
			       "cer_expense_amount = " . dbquote(round($value,0)) . 
				   " where cer_expense_draft_id = " . $draft_id . 
			       " and cer_expense_type = 1 " .
			       " and cer_expense_year =  " . dbquote($year);

			$result = mysql_query($sql) or dberror($sql);
		}



		
	}

	return true;
}




/********************************************************************
    get the field values of a user
*********************************************************************/
function get_user($id)
{
    $user = array();

    if ($id == '')
    {
        $user["firstname"] = "";
        $user["name"] = "";
        $user["contact"] = "";
        $user["phone"] = "";
        $user["fax"] = "";
        $user["email"] = "";
        $user["cc"] = "";
        $user["deputy"] = "";
        $user["address"] = "";
		$user["country"] = "";
    }
    else
    {
        $sql = "select * from users left join addresses on address_id = user_address where user_id = " . $id;
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $user["firstname"] = $row["user_firstname"];
            $user["name"] = $row["user_name"];
            $user["contact"] = $row["user_name"] . " " . $row["user_firstname"];
            $user["phone"] = $row["user_phone"];
            $user["fax"] = $row["user_fax"];
            $user["email"] = $row["user_email"];
            $user["cc"] = $row["user_email_cc"];
            $user["deputy"] = $row["user_email_deputy"];
            $user["address"] = $row["user_address"];
			$user["country"] = $row["address_country"];
        }
    }
    return $user;
}

/*************************************************************************
   get all the roles of a user
**************************************************************************/
function get_user_roles($user_id)
{
    $user_roles = array();
    
    $sql = "select user_role_role ".
           "from user_roles ".
           "left join roles on user_role_role = role_id ".
           "where user_role_user = " . $user_id;

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $user_roles[] = $row["user_role_role"];
    }

    return $user_roles;
}

/*************************************************************************
   get the role of a user in the context of an order or project
   contains the orders the user has access to because of his role
**************************************************************************/
function get_user_specific_order_list($user_id, $user_roles = array())
{
    
    $order_ids = array();
    $condition = "";
    $user_address = 0;
	$project_access_filter = "";

    // get user's address
    $sql = "select user_address, user_can_only_see_his_projects from users ".
           "where user_id = " . user_id();

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {            
        $user_address = $row["user_address"];
		if($row["user_can_only_see_his_projects"] == 1) {
			$project_access_filter = " and order_user = " . $user_id . " ";
		}
    }


    //get users country
    $user_country = 0;
    $sql = "select address_country from addresses " .
           "where address_id = " . $user_address;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $user_country = $row["address_country"];
    }


    // check client
    $sql = "select order_id " .
           "from orders ".
           "where order_user = " . $user_id . 
           "   and order_client_address = " . $user_address;

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {            
        $order_ids[] = $row["order_id"];
    }


	// retail operator
    $sql = "select order_id " .
           "from orders ".
           "where order_retail_operator = " . $user_id;

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {            
        $order_ids[] = $row["order_id"];
    }


    // local agent
    if (has_access("has_access_to_all_projects_of_his_country"))
    {
		$country_filter = "";
        $sql = "select * from country_access " .
               "where country_access_user = " . user_id();


        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {            
            $country_filter.= " or address_country = " . $row["country_access_country"];
        }

        if($country_filter)
        {
            $country_filter = "address_country  = " . $user_country . $country_filter;
        }
        else
        {
            $country_filter = "address_country  = " . $user_country;
        }
        
        $sql = "select project_order " .
               "from projects ".
               "left join orders on order_id = project_order " .
               "left join addresses on address_id = order_client_address " .
               "where " . $country_filter . 
			   $project_access_filter;

        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {            
            $order_ids[] = $row["project_order"];
        }
    }

	//others
	if(has_access("cer_has_full_access_to_his_projects"))
	{
		$sql = "select project_order " .
               "from projects ".
               "left join orders on order_id = project_order " .
               "left join addresses on address_id = order_client_address " .
               "where (order_shop_address_country = " . $user_country . " or address_country  = " . $user_country . ") " . 
			   $project_access_filter;;

        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {            
            $order_ids[] = $row["project_order"];
        }

	}

    
    //projects
    
	// retail coordinator, design contractor, design supervisor
	if(in_array(7, $user_roles)) // design contractor
	{
		$sql = "select project_order " .
			   "from projects ".
			   "left join orders on order_id = project_order " .
			   "where order_actual_order_state_code >= 220 " . 
			   "   and (project_retail_coordinator = " . $user_id . 
			   "   or project_design_contractor = " . $user_id .
			   "   or project_design_supervisor = " . $user_id . 
			   ")";
	}
	else
	{

	$sql = "select project_order " .
		   "from projects ".
		   "where project_retail_coordinator = " . $user_id . 
		   "   or project_design_contractor = " . $user_id .
		   "   or project_design_supervisor = " . $user_id;
	}
	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{            
		$order_ids[] = $row["project_order"];
	}

   
	// check supplier or forwarder
	if (has_access("can_view_empty_orders") and has_access("can_view_order_before_booklet_approval"))
	{
		$sql = "select distinct order_item_order " .
			   "from order_items ".
			   "where order_item_supplier_address = " .$user_address . 
			   "   or order_item_forwarder_address = " . $user_address;
	}
	elseif (has_access("can_view_empty_orders") and !has_access("can_view_order_before_booklet_approval"))
	{
		$sql = "select distinct order_item_order " .
			   "from order_items ".
			   "where (order_item_supplier_address = " .$user_address .
			   "   or order_item_forwarder_address = " . $user_address . ")".
			   "   and order_item_show_to_suppliers = 1";
	}
	elseif (!has_access("can_view_empty_orders") and has_access("can_view_order_before_booklet_approval"))
	{
		$sql = "select distinct order_item_order " .
			   "from order_items ".
			   "where (order_item_supplier_address = " .$user_address . 
			   "   or order_item_forwarder_address = " . $user_address . ")" .
			   "   and (order_item_not_in_budget = 0 ".
			   "   or order_item_not_in_budget is null)";
	}
	elseif (!has_access("can_view_empty_orders") and !has_access("can_view_order_before_booklet_approval"))
	{
		$sql = "select distinct order_item_order " .
			   "from order_items ".
			   "where (order_item_supplier_address = " .$user_address . 
			   "   or order_item_forwarder_address = " . $user_address . ")" .
			   "   and (order_item_not_in_budget = 0 ".
			   "   or order_item_not_in_budget is null)" .
			   "   and order_item_show_to_suppliers = 1";
	}

	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{            
		$order_ids[] = $row["order_item_order"];
	}

    $condition = "(" . implode(",", $order_ids) .")";
    
    return $condition;

}

/********************************************************************
    get cer_basicdata
*********************************************************************/
function get_cer_basicdata($draft_id)
{

	$basic_data = array();

    $sql = "select * from cer_drafts " . 
	       "left join currencies on currency_id = cer_basicdata_currency " . 
	       "where cer_basicdata_id = " . dbquote($draft_id);
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $basic_data = $row;
    }

	return $basic_data;

}


/********************************************************************
    get_gross_sale_values
*********************************************************************/
function get_gross_sale_values($draft_id = 0, $scenario_80_percent = false)
{
	
	// get gross sales values
	$total_sales = array();
	$sql =  "select * from cer_draft_revenues " .
			"where cer_revenue_draft_id = " . $draft_id .
		    " order by cer_revenue_year";

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$sales_watches_values = $row["cer_revenue_watches"];
		$sales_jewellery_values = $row["cer_revenue_jewellery"];
		$sales_customer_service_values = $row["cer_revenue_customer_service"];
		$sales_accessories_values = $row["cer_revenue_accessories"];

		if(array_key_exists($row["cer_revenue_year"], $total_sales))
		{
			$tmp = 1*$sales_watches_values + 1*$sales_jewellery_values + 1*$sales_customer_service_values + 1*$sales_accessories_values;
			$total_sales[$row["cer_revenue_year"]] = $total_sales[$row["cer_revenue_year"]] + $tmp;
		}
		else
		{
			$total_sales[$row["cer_revenue_year"]] = 1*$sales_watches_values + 1*$sales_jewellery_values + 1*$sales_customer_service_values + 1*$sales_accessories_values;
		}

		if($scenario_80_percent == true)
		{
			$total_sales[$row["cer_revenue_year"]] = 0.8*$total_sales[$row["cer_revenue_year"]];
		}

	}

	return $total_sales;
}


/********************************************************************
    get_net_sale_values
*********************************************************************/
function get_net_sale_values($draft_id = 0, $scenario_80_percent = false)
{
	
	$cer_basicdata = get_cer_basicdata($draft_id);
	// get gross sales values
	$total_net_sales = array();
	

	//get sales planning
	$sql = "select * from cer_draft_revenues " .
		   "where cer_revenue_draft_id = " . $draft_id;

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$year = $row["cer_revenue_year"];

		/*
		$sales_reduction_watches = $row["cer_revenue_sales_reduction_watches"] + $row["cer_revenue_reduction_credit_cards"];
		$sales_reduction_bijoux = $row["cer_revenue_sales_reduction_bijoux"] + $row["cer_revenue_reduction_credit_cards"];
		$sales_reduction_accessories = $row["cer_revenue_sales_reduction_accessories"] + $row["cer_revenue_reduction_credit_cards"];
		$sales_reduction_services = $row["cer_revenue_sales_reduction_cservice"] + $row["cer_revenue_reduction_credit_cards"];
		*/

		$sales_reduction_watches = $row["cer_revenue_sales_reduction_watches"];
		$sales_reduction_bijoux = $row["cer_revenue_sales_reduction_bijoux"];
		$sales_reduction_accessories = $row["cer_revenue_sales_reduction_accessories"];
		$sales_reduction_services = $row["cer_revenue_sales_reduction_cservice"];

		if($scenario_80_percent == true)
		{
			$net_sales_watches = 0.8*$row["cer_revenue_watches"] - (0.8*$row["cer_revenue_watches"]*$sales_reduction_watches/100);
			$net_sales_jewellery = 0.8*$row["cer_revenue_jewellery"] - (0.8*$row["cer_revenue_jewellery"]*$sales_reduction_bijoux/100); 
			$net_sales_accessories = 0.8*$row["cer_revenue_accessories"] - (0.8*$row["cer_revenue_accessories"]*$sales_reduction_accessories/100);
			$net_sales_customer_service = 0.8*$row["cer_revenue_customer_service"]- (0.8*$row["cer_revenue_customer_service"]*$sales_reduction_services/100);
		}
		else {		
			$net_sales_watches = $row["cer_revenue_watches"] - ($row["cer_revenue_watches"]*$sales_reduction_watches/100);
			$net_sales_jewellery = $row["cer_revenue_jewellery"] - ($row["cer_revenue_jewellery"]*$sales_reduction_bijoux/100); 
			$net_sales_accessories = $row["cer_revenue_accessories"] - ($row["cer_revenue_accessories"]*$sales_reduction_accessories/100);
			$net_sales_customer_service = $row["cer_revenue_customer_service"]- ($row["cer_revenue_customer_service"]*$sales_reduction_services/100);
		}

		if(array_key_exists($row["cer_revenue_year"], $total_net_sales))
		{
			$total_net_sales[$year] = $total_net_sales[$year] + $net_sales_watches + $net_sales_jewellery + $net_sales_accessories + $net_sales_customer_service;
		}
		else
		{
			$total_net_sales[$year] = $net_sales_watches + $net_sales_jewellery + $net_sales_accessories + $net_sales_customer_service;
		}
	}

	return $total_net_sales;
}


/********************************************************************
    get_total_rents
*********************************************************************/
function get_total_rents($draft_id = 0)
{

	$total_rents = array();
	$sql = "select * " .
		   "from cer_draft_expenses " .
		   "where cer_expense_type IN (2,16, 20) " . 
		   " and cer_expense_draft_id = " . $draft_id .
		   " order by cer_expense_year";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		if(array_key_exists($row["cer_expense_year"], $total_rents))
		{
			$total_rents[$row["cer_expense_year"]] = $total_rents[$row["cer_expense_year"]] + $row["cer_expense_amount"];
		}
		else
		{
			$total_rents[$row["cer_expense_year"]] = $row["cer_expense_amount"];
		}
	}

	return $total_rents;
}


/********************************************************************
    get_fixed_rents
*********************************************************************/
function get_fixed_rents($draft_id = 0)
{

	$fixed_rents = array();
	$sql = "select cer_expense_year, sum(cer_expense_amount) as cer_expense_amount " .
		   "from cer_draft_expenses " .
		   "where cer_expense_type in(2,20) " . 
		   " and cer_expense_draft_id = " . $draft_id .
		   " group by cer_expense_year " . 
		   " order by cer_expense_year ";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$fixed_rents[$row["cer_expense_year"]] = $row["cer_expense_amount"];
	}

	return $fixed_rents;
}


/********************************************************************
    update turnover based rental cost
*********************************************************************/
function update_turnoverbased_rental_cost($draft_id = 0, $scenario_80_percent = false)
{
		
	$cer_basicdata = get_cer_basicdata($draft_id);

	
	if($cer_basicdata["cer_basicdata_tob_from_net_sales"] == 1)
	{
		$total_sales = get_net_sale_values($draft_id, $scenario_80_percent);
	}
	else
	{
		$total_sales = get_gross_sale_values($draft_id, $scenario_80_percent);
	}
	$fixed_rents = get_fixed_rents($draft_id);

	

	//calculate monthly turn over per year and month
	$monthly_total_sales = array();
	$first_year = $cer_basicdata["cer_basicdata_firstyear"];
	$first_month = $cer_basicdata["cer_basicdata_firstmonth"];

	$last_year = $cer_basicdata["cer_basicdata_lastyear"];
	$last_month = $cer_basicdata["cer_basicdata_lastmonth"];


	if($first_year == $last_year)
	{
		$num_of_months_first_year = $last_month - $first_month + 1;
	}
	else
	{
		$num_of_months_first_year = 13 - $first_month;
	}

	foreach($total_sales as $year=>$total_sale)
	{
		if($year == $first_year)
		{
			 $monthly_total_sales[$year] = $total_sale/$num_of_months_first_year;
		}
		elseif($year == $last_year)
		{
			 $monthly_total_sales[$year] = $total_sale/$last_month;
		}
		else
		{
			$monthly_total_sales[$year] = $total_sale/12;
		}
	}

	
	
	$debug[] = "<strong>CER Percen from Sales Data</strong>";


	// Correct first and last year by decimal month shares
	// becasue busness splan period does not contain days
	$sql = "select * from cer_draft_rent_percent_from_sales " . 
		   "where  cer_rent_percent_from_sale_draft_id = " . $draft_id . 
		   " and cer_rent_percent_from_sale_percent > 0 " . 
		   " order by cer_rent_percent_from_sale_from_year, " . 
		   " cer_rent_percent_from_sale_from_month, cer_rent_percent_from_sale_from_day ";

	$res = mysql_query($sql) or dberror($sql);

	while($row = mysql_fetch_assoc($res))
	{
		
		$tob_from_year = $row["cer_rent_percent_from_sale_from_year"];
		$tob_from_month = $row["cer_rent_percent_from_sale_from_month"];
		$tob_from_day = $row["cer_rent_percent_from_sale_from_day"];
		$tob_to_year = $row["cer_rent_percent_from_sale_to_year"];
		$tob_to_month = $row["cer_rent_percent_from_sale_to_month"];
		$tob_to_day = $row["cer_rent_percent_from_sale_to_day"];

		$num_of_months_year_1 = 0;
		$num_of_months_year_2 = 0;

		$debug[] = "TOB to Year and Last Year of Business Plan: " . $tob_to_year . "->" . $last_year;

		if($tob_from_year == $first_year)
		{
					
			//first year
			if($tob_from_day == 1)
			{
				$date1 = $tob_from_year . "-" . $tob_from_month . "-00";
			}
			else
			{
				$date1 = $tob_from_year . "-" . $tob_from_month . "-" . $tob_from_day;	
			}
			$date2 = $tob_from_year . "-12-31";

			$number_of_days = cal_days_in_month(CAL_GREGORIAN, $tob_from_month, $tob_from_year);
			
			
			$d1 = new DateTime($date1);
			$d2 = new DateTime($date2);

			$part = 0;
			$number_of_months = $d1->diff($d2)->m;
			$part = ($number_of_days - $tob_from_day)/$number_of_days;
			
			if( $tob_from_day > 1)
			{
				$num_of_months_year_1 = $number_of_months + $part;
			}
			else
			{
				$num_of_months_year_1 = $number_of_months;
			}

			//make a correction by partial months
			if($tob_from_day> 30 and $tob_from_year == $first_year and $num_of_months_year_1 > 0)
			{
				$monthly_total_sales[$first_year] = $total_sales[$first_year]/$num_of_months_year_1;


				$debug[] = "<br /><strong>Monthly correction< by partial months</strong>";
				$debug[] = $first_year . ": " . $total_sales[$first_year] . "/" . $num_of_months_year_1 . "=" . $monthly_total_sales[$first_year];
			}
		}
		
		if($tob_to_year == $last_year)
		{
			$number_of_days = cal_days_in_month(CAL_GREGORIAN, $tob_to_month, $tob_to_year);

			$part = 0;
			$number_of_months = $tob_to_month;
			$part = $tob_to_day/$number_of_days;
		
			if( $tob_to_day < $number_of_days)
			{
				$num_of_months_year_2 = ($number_of_months-1) + $part;
			}
			else
			{
				$num_of_months_year_2 = $number_of_months;
			}

			//make a correction by partial months
			if($tob_to_year == $last_year and $num_of_months_year_2 > 0)
			{
				 //$monthly_total_sales[$last_year] = $total_sales[$last_year]/$num_of_months_year_2;
			}
		}
	}


	//prepare data from cer_rent_percent_from_sales
	$debug[] = "<br /><strong>Break Point Data</strong>";
	$tob_data_break_points = array();

	if($cer_basicdata["cer_basicdata_tob_from_breakpoint_difference"] == 1)
	{
		$sql = "select * from cer_draft_rent_percent_from_sales " . 
			   "where cer_rent_percent_from_sale_draft_id = " . $draft_id . 
			   " and cer_rent_percent_from_sale_percent > 0 " . 
			   " order by cer_rent_percent_from_sale_from_year ASC, " . 
			   " cer_rent_percent_from_sale_from_month ASC, cer_rent_percent_from_sale_from_day ASC, cer_rent_percent_from_sale_amount DESC ";
	}
	else
	{
		$sql = "select * from cer_draft_rent_percent_from_sales " . 
			   "where cer_rent_percent_from_sale_draft_id = " . $draft_id . 
			   " and cer_rent_percent_from_sale_percent > 0 " . 
			   " order by cer_rent_percent_from_sale_from_year ASC, " . 
			   " cer_rent_percent_from_sale_from_month ASC, cer_rent_percent_from_sale_from_day ASC, cer_rent_percent_from_sale_amount ASC ";
	}

	$res = mysql_query($sql) or dberror($sql);
	$old_timeperiod = "";
	while($row = mysql_fetch_assoc($res))
	{
		$tob_from_year = $row["cer_rent_percent_from_sale_from_year"];
		$tob_from_month = $row["cer_rent_percent_from_sale_from_month"];
		$tob_from_day = $row["cer_rent_percent_from_sale_from_day"];
		$tob_to_year = $row["cer_rent_percent_from_sale_to_year"];
		$tob_to_month = $row["cer_rent_percent_from_sale_to_month"];
		$tob_to_day = $row["cer_rent_percent_from_sale_to_day"];

		
		$timeperiod = $tob_from_year . $tob_from_month . $tob_from_day . $tob_to_year . $tob_to_month . $tob_to_day;

		
		if($old_timeperiod != $timeperiod)
		{
			$old_timeperiod = $timeperiod;
			$number_of_breakpoints_in_timeperiod = 1;
		}
		else
		{
			$number_of_breakpoints_in_timeperiod++;
		}
		$tob_data_break_points[] = array("timeperiod"=>$timeperiod, "breakpoint"=>$row["cer_rent_percent_from_sale_amount"], "percent"=>$row["cer_rent_percent_from_sale_percent"], "number_of_breakpoints_in_timeperiod"=>$number_of_breakpoints_in_timeperiod);
	}

	foreach($tob_data_break_points as $key=>$breakpoint)
	{
		foreach($breakpoint as $key=>$value)
		{
			$debug[] = $key . ": " . $value;
		}
		$debug[] = "<br />";
	}
	

	//calculate turnoverbased rents

	$turn_over_based_rents = array();
	$turn_over_in_contract_periods = array();
	$tax_rates = array();
	$passenger_rates = array();

	$record_count = 1;

	$sql = "select DISTINCT cer_rent_percent_from_sale_from_year, cer_rent_percent_from_sale_from_month, " .
		   "cer_rent_percent_from_sale_from_day, cer_rent_percent_from_sale_to_year, cer_rent_percent_from_sale_to_month, " . 
		   "cer_rent_percent_from_sale_to_day, cer_rent_percent_from_sale_tax_rate, cer_rent_percent_from_sale_passenger_rate " . 
	       "from cer_draft_rent_percent_from_sales " . 
		   "where cer_rent_percent_from_sale_draft_id = " . $draft_id . 
		   " and cer_rent_percent_from_sale_percent > 0 " . 
		   " order by cer_rent_percent_from_sale_from_year, " . 
		   " cer_rent_percent_from_sale_from_month, cer_rent_percent_from_sale_from_day ";

	$res = mysql_query($sql) or dberror($sql);

	while($row = mysql_fetch_assoc($res))
	{
		$debug[] = "<br /><br />Record " . $record_count . " in table cer_rent_percent_from_sales ";
		$record_count++;
		
		$tob_from_year = $row["cer_rent_percent_from_sale_from_year"];
		$tob_from_month = $row["cer_rent_percent_from_sale_from_month"];
		$tob_from_day = $row["cer_rent_percent_from_sale_from_day"];
		$tob_to_year = $row["cer_rent_percent_from_sale_to_year"];
		$tob_to_month = $row["cer_rent_percent_from_sale_to_month"];
		$tob_to_day = $row["cer_rent_percent_from_sale_to_day"];

		$tax_rate = (float)($row["cer_rent_percent_from_sale_tax_rate"]/100);
		$passenger_rate = (float)($row["cer_rent_percent_from_sale_passenger_rate"]/100);

		$timeperiod = $tob_from_year . $tob_from_month . $tob_from_day . $tob_to_year . $tob_to_month . $tob_to_day;

		//echo "<br />" . $timeperiod . "<br />";


		$num_of_months_year_1 = 0;
		$num_of_months_year_2 = 0;
		

		//if($tob_to_year > $tob_from_year)
		//{

			
			//first year
			$debug[] = "<br /><br /><strong>First Year</strong>";
			$date1 = $tob_from_year . "-" . $tob_from_month . "-" . $tob_from_day;
			$date1 = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $date1) ) ));

			if(1*$tob_from_month == 1 and 1*$tob_from_day == 1)
			{
				$date1 = date('Y-m-d',strtotime ( $date1));
			}
			else
			{
				$date1 = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $date1) ) ));
			}
			
			if($tob_to_year > $tob_from_year)
			{
				$date2 = $tob_from_year . "-12-31";
			}
			else
			{
				if($tob_to_month == 2 and ($tob_to_day == 28 or $tob_to_day == 29))
				{
					$date2 = $tob_to_year . "-" . $tob_to_month . "-31";
				}
				else
				{
					$date2 = $tob_to_year . "-" . $tob_to_month . "-" . $tob_to_day;
				}
			}
			
			if( $tob_from_month > 0 and $tob_from_year > 0) {
				$number_of_days = cal_days_in_month(CAL_GREGORIAN, $tob_from_month, $tob_from_year);
			}
			else {
				$number_of_days = 0;
			}

			$debug[] = "Number of Days: " . $number_of_days;
			$debug[] = "Datum von bis: " . $date1 . "->" . $date2;
			
			$d1 = new DateTime($date1);
			$d2 = new DateTime($date2);

			$part = 0;
			//$number_of_months = $d1->diff($d2)->m;
			$number_of_months = $d1->diff($d2)->m + ($d1->diff($d2)->y*12);
			
			if($number_of_days > 0) {
				$part = ($number_of_days - $tob_from_day)/$number_of_days;
			}

			$debug[] = "Number of months: " . $number_of_months;
			
			if( $tob_from_day > 1)
			{
				$num_of_months_year_1 = $number_of_months + $part;
			}
			else
			{
				$num_of_months_year_1 = $number_of_months;
			}

			//work around to get full number of months
			if($tob_from_month == '01' and 
				$num_of_months_year_1 == $number_of_months 
				and $tob_from_day == '01' and $tob_to_day > 29)
			{
				$number_of_months++;
				$num_of_months_year_1++;
			}

			if($num_of_months_year_1 > 12)
			{
				$num_of_months_year_1 = 12;
			}

			//workaround first year
			if($tob_from_year == $first_year and $tob_from_month > 1 and $tob_from_day > 1)
			{
				$num_of_months_year_1 = $number_of_months + 1;
			}
			elseif($tob_from_year == $first_year and $last_year > $first_year)
			{
				$num_of_months_year_1 = $number_of_months;
			}

			if($num_of_months_year_1 > 12)
			{
				$num_of_months_year_1 = 12;
			}
			
			$debug[] = "Number of months: " . $number_of_months;

			$debug[] = "Number of decimal months: " . $num_of_months_year_1;

			$debug[] = "Montly sales per year and month: ";
			foreach($monthly_total_sales as $key=>$value)
			{
				$debug[] = $key . "->" . $value;
			}

			
			if(array_key_exists($tob_from_year, $monthly_total_sales))
			{
				$turn_over_in_contract_period = $num_of_months_year_1 * $monthly_total_sales[$tob_from_year];
								
				$debug[] = "Turn over contract period: " . $turn_over_in_contract_period;
				
				
				
				if(array_key_exists($tob_from_year,$turn_over_in_contract_periods ))
				{
					$turn_over_in_contract_periods[$tob_from_year] = $turn_over_in_contract_periods[$tob_from_year] + $turn_over_in_contract_period;
				}
				else
				{
					$turn_over_in_contract_periods[$tob_from_year] = $turn_over_in_contract_period;	
				}

				$debug[] = "TOB from year and tunrover in year: " . $tob_from_year . " " . $turn_over_in_contract_periods[$tob_from_year];

				
				$rent = 0;
				
				if($tob_from_year > $first_year)
				{
					$base_amount = $turn_over_in_contract_period;
					$debug[] = "Base Amount Turn Over starting: " . $tob_from_year ."/" . $first_year . ": " . $base_amount;
				}
				else
				{
					//$base_amount = $turn_over_in_contract_periods[$tob_from_year];
					//not clear why we first hat the above line
					$base_amount = $turn_over_in_contract_period;
					$debug[] = "Base Amount Turn Over starting: " . $tob_from_year ."/" . $first_year . ": " . $base_amount;
				}


				
				foreach($tob_data_break_points as $key=>$break_point_data)
				{
					$debug[] = "Timeperiod breakpoint and time period and starting year and first year: " . $break_point_data["timeperiod"] . "->" . $timeperiod . "->" . $tob_from_year . "->" .$first_year;
					
					if($break_point_data["timeperiod"] == $timeperiod)
					{
						if($tob_from_year > $first_year)
						{
							$partial_break_point = $num_of_months_year_1*$break_point_data["breakpoint"]/12;

							$debug[] = "Base amount and partial break point: " . $base_amount . "->" . $partial_break_point;
							if($base_amount > $partial_break_point) //break points
							{
								if($cer_basicdata["cer_basicdata_tob_from_breakpoint_difference"] == 1)
								{
									$difference = $base_amount - $partial_break_point;
									$rent = $rent + $break_point_data["percent"]*($difference)/100;
									$base_amount = $base_amount - $difference;
								}
								else
								{
									if($partial_break_point > 0 and $num_of_months_year_1 < 12)
									{
										//$rent = $partial_break_point*$base_amount/100;
										//$rent = $break_point_data["percent"]*$partial_break_point/100;
										$rent = $break_point_data["percent"]*$base_amount/100;


										$debug[] = "01 Partial breakpoint and base amount anc rent " . $partial_break_point . "->" . $base_amount . "->" . $rent;
									}
									else
									{
										$rent = $break_point_data["percent"]*$base_amount/100;

										$debug[] = "01 Rent " . $break_point_data["percent"] . "*" . $base_amount . "=" . $rent;
									}

								}
							}
						}
						else
						{
							$debug[] = "Base amount and break point: " . $base_amount . "->" . $break_point_data["breakpoint"];
							if($base_amount > $break_point_data["breakpoint"]) //break points
							{
								if($cer_basicdata["cer_basicdata_tob_from_breakpoint_difference"] == 1)
								{
									$difference = $base_amount - $break_point_data["breakpoint"];
									$rent = $rent + $break_point_data["percent"]*($difference)/100;
									$base_amount = $base_amount - $difference;
								}
								else
								{
									$rent = $break_point_data["percent"]*$base_amount/100;
									$debug[] = "02 Rent " . $break_point_data["percent"] . "*" . $base_amount . "=" . $rent;
								}
							}
						}
					}

				}
				
				if(array_key_exists($tob_from_year,$turn_over_based_rents ))
				{
					$turn_over_based_rents[$tob_from_year] = $turn_over_based_rents[$tob_from_year] + $rent;
				}
				else
				{
					$turn_over_based_rents[$tob_from_year] = $rent;
				}

				$tax_rates[$tob_from_year] = $tax_rate;
				$passenger_rates[$tob_from_year] = $passenger_rate;

				$debug[] = "<strong>->TOB year and tob rent: </strong>" . $tob_from_year . "->" . $turn_over_based_rents[$tob_from_year];

				 
			}


			//in between years
			$debug[] = "<strong>In between years</strong>";
			for($year = $tob_from_year+1;$year <$tob_to_year;$year++)
			{
				$debug[] = "<br />Year " . $year;
				if(array_key_exists($year, $monthly_total_sales))
				{
					$turn_over_in_contract_period = $total_sales[$year];
					if(array_key_exists($year,$turn_over_in_contract_periods ))
					{
						$turn_over_in_contract_periods[$year] = $turn_over_in_contract_periods[$year] + $turn_over_in_contract_period;
					}
					else
					{
						$turn_over_in_contract_periods[$year] = $turn_over_in_contract_period;	
					}
					
					$rent = 0;

					$base_amount = $turn_over_in_contract_periods[$year];
					foreach($tob_data_break_points as $key=>$break_point_data)
					{
						
						$debug[] = "Breakpoint data: " . $break_point_data["timeperiod"] . "->" . $timeperiod;
						if($break_point_data["timeperiod"] == $timeperiod)
						{
							$debug[] = "Base amount and breakpoint " . $base_amount . "->" . $break_point_data["breakpoint"];
						
							if($base_amount > $break_point_data["breakpoint"]) //break points
							{
								if($cer_basicdata["cer_basicdata_tob_from_breakpoint_difference"] == 1)
								{
									$difference = $base_amount - $break_point_data["breakpoint"];
									$rent = $rent + $break_point_data["percent"]*($difference)/100;
									$base_amount = $base_amount - $difference;
								}
								else
								{
									$rent = $break_point_data["percent"]*$base_amount/100;

									$debug[] = "Breakpoint and base amount and Rent " . $break_point_data["percent"] . "->" . $base_amount . "->" . $rent . "<br />";
								}
							}
						}

					}
					
					if(array_key_exists($year,$turn_over_based_rents ))
					{
						$turn_over_based_rents[$year] = $turn_over_based_rents[$year] + $rent;
					}
					else
					{
						$turn_over_based_rents[$year] = $rent;
					}

					$tax_rates[$year] = $tax_rate;
					$passenger_rates[$year] = $passenger_rate;
				}
			}


			//last year
			$debug[] = "<strong>Last year</strong>";
			if($tob_to_year > $tob_from_year)
			{
			
				$number_of_days = cal_days_in_month(CAL_GREGORIAN, $tob_to_month, $tob_to_year);
						

				$part = 0;
				$number_of_months = $tob_to_month;
				$part = $tob_to_day/$number_of_days;
				

				
			
				if( $tob_to_day < $number_of_days)
				{
					$num_of_months_year_2 = ($number_of_months-1) + $part;
				}
				else
				{
					$num_of_months_year_2 = $number_of_months;
				}

				//workaround last year
				if($tob_to_year == $last_year)
				{
					$num_of_months_year_2 = $last_month;
				}

				if($number_of_records_with_last_year == $record_count)
				{
					$num_of_months_year_2 = $last_month;

					$debug[] = "<br />number of months2 last year: " . $num_of_months_year_2;
				}
				elseif($number_of_records_with_last_year == $record_count
					and $tob_from_year == $last_year)
				{
					$num_of_months_year_2 = $last_month;
				}

				$debug[] = "Number of months: " . $num_of_months_year_2;

			
				if(array_key_exists($tob_to_year, $monthly_total_sales))
				{
					$turn_over_in_contract_period = $num_of_months_year_2 * $monthly_total_sales[$tob_to_year];

					$debug[] = "Turn over in contract period: " . $num_of_months_year_2 . "*" .  $monthly_total_sales[$tob_to_year] ."=" . $turn_over_in_contract_period;
					
					if(array_key_exists($tob_to_year,$turn_over_in_contract_periods ))
					{
						$turn_over_in_contract_periods[$tob_to_year] = $turn_over_in_contract_periods[$tob_to_year] + $turn_over_in_contract_period;
					}
					else
					{
						$turn_over_in_contract_periods[$tob_to_year] = $turn_over_in_contract_period;	
					}

					if($last_year == $tob_to_year)
					{
						$turn_over_in_contract_period = $num_of_months_year_2 * $monthly_total_sales[$last_year];
						$turn_over_in_contract_periods[$last_year] = $turn_over_in_contract_period;	
					}

					$rent = 0;
					$base_amount = $turn_over_in_contract_periods[$tob_to_year];

					
					
					foreach($tob_data_break_points as $key=>$break_point_data)
					{
						$debug[] = "Breackpont time period: " . $break_point_data["timeperiod"] . "->" . $timeperiod;
						if($break_point_data["timeperiod"] == $timeperiod)
						{
							
							$debug[] = " TOB Year and last year: " . $tob_to_year . "->" . $last_year;
							if($tob_to_year == $last_year
								and $tob_to_month == 12)
							{
								$debug[] = "Base amount and breakpoint: " . $base_amount . "->" . $break_point_data["breakpoint"];
							
								if($base_amount > $break_point_data["breakpoint"]) //break points
								{
									if($cer_basicdata["cer_basicdata_tob_from_breakpoint_difference"] == 1)
									{
										$difference = $base_amount - $break_point_data["breakpoint"];
										$rent = $rent + $break_point_data["percent"]*($difference)/100;
										$base_amount = $base_amount - $difference;

										$debug[] =  "03 Rent " . $rent;
										$debug[] = "Breakpoint percent and base amount: " .  $break_point_data["percent"] . "->" . $base_amount;
									}
									else
									{
										$rent = $break_point_data["percent"]*$base_amount/100;
										$debug[] =  "04 Rent " . $rent;
										$debug[] = "Breakpoint percent and base amount: " .  $break_point_data["percent"] . "->" . $base_amount;
									}
								}
							}
							else // parial breakpoint ins overlapping years
							{
								if($last_year == $tob_to_year) {
									$partial_breakpoint = $break_point_data["breakpoint"];
								}
								else {
									$partial_breakpoint = $num_of_months_year_2*$break_point_data["breakpoint"]/12;
								}
								
								$debug[] = "Base amount and pratial breakpoint: " . $base_amount . "->" . $partial_breakpoint;
								
								if($base_amount > $partial_breakpoint) //break points
								{
									if($cer_basicdata["cer_basicdata_tob_from_breakpoint_difference"] == 1)
									{
										$difference = $base_amount - $partial_breakpoint;
										$rent = $rent + $break_point_data["percent"]*($difference)/100;
										$base_amount = $base_amount - $difference;

										$debug[] =  "05 Rent " . $rent;
										$debug[] = "Breakpoint percent and base amount: " .  $break_point_data["percent"] . "->" . $base_amount;
									}
									else
									{
										if(isset($partial_break_point) 
											and $partial_break_point > 0
											and $num_of_months_year_2 < 12)
										{
											//$rent = $partial_breakpoint*$base_amount/100;
											//$rent = $break_point_data["percent"]*($partial_breakpoint)/100;
											$rent = $break_point_data["percent"]*($base_amount)/100;
										}
										else
										{
											$rent = $break_point_data["percent"]*($base_amount)/100;
										}
										
										$debug[] =  "06 Rent " . $rent;
										$debug[] = "Breakpoint percent and base amount: " .  $break_point_data["percent"] . "->" . $base_amount;
									}
								}
							}
						}
					}

					if(array_key_exists($tob_to_year,$turn_over_based_rents ))
					{
						$turn_over_based_rents[$tob_to_year] = $turn_over_based_rents[$tob_to_year] + $rent;
					}
					else
					{
						$turn_over_based_rents[$tob_to_year] = $rent;
					}

					$tax_rates[$tob_to_year] = $tax_rate;
					$passenger_rates[$tob_to_year] = $passenger_rate;

					$debug[] = "<strong>->TOB year and tob rent: </strong>" . $tob_to_year . "->" . $turn_over_based_rents[$tob_to_year];
					//abc();
				}
			}
			
		//}
	}

	

	//update expenses
	if($scenario_80_percent == false)
	{
		$sql = "update cer_draft_expenses SET " . 
			   "cer_expense_amount = 0 " .
			   " where cer_expense_type = 16 " . 
			   " and cer_expense_draft_id = " . $draft_id;

					
		$result = mysql_query($sql) or dberror($sql);

		foreach($turn_over_based_rents as $year=>$rent)
		{
			
			//calculate rent on the basis of whatever is higher
			// TOB-Rents are not added to fixed rents
			if($cer_basicdata["cer_basicdata_add_tob_rents"] == 0)
			{
				if($rent > $fixed_rents[$year])
				{
					$rent = $rent - $fixed_rents[$year];
				}
				else
				{
					$rent = 0;
				}
			}
			
			//update expenses
			$sql = "update cer_draft_expenses SET " . 
				   "cer_expense_amount = " . $rent . 
				   " where cer_expense_type = 16 " . 
				   " and cer_expense_draft_id = " . $draft_id . 
				   " and cer_expense_year = " . dbquote($year);

					
			$result = mysql_query($sql) or dberror($sql);


			//update taxrates
			$amount = 0;
			$sql = "select cer_expense_amount " .
				   " from cer_draft_expenses " . 
				   " where cer_expense_type = 18 " . 
				   " and cer_expense_draft_id = " . $draft_id . 
				   " and cer_expense_year = " . dbquote($year);

			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				$amount = $row["cer_expense_amount"];
			}

			$amount = $amount + $tax_rates[$year]*$rent;
			

			$sql = "update cer_draft_expenses SET " . 
				   "cer_expense_amount = " . $amount . 
				   " where cer_expense_type = 18 " . 
				   " and cer_expense_draft_id = " . $draft_id .  
				   " and cer_expense_year = " . dbquote($year);

					
			$result = mysql_query($sql) or dberror($sql);


			//update passenger index amount
			$amount = 0;
			$sql = "select cer_expense_amount " .
				   " from cer_draft_expenses " .
				   " where cer_expense_type = 19 " . 
				   " and cer_expense_draft_id = " . $draft_id .  
				   " and cer_expense_year = " . dbquote($year);

			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				$amount = $row["cer_expense_amount"];
			}

			$amount = $amount + $passenger_rates[$year]*$rent;
			

			$sql = "update cer_draft_expenses SET " . 
				   "cer_expense_amount = " . $amount . 
				   " where cer_expense_type = 19 " . 
				   " and cer_expense_draft_id = " . $draft_id .  
				   " and cer_expense_year = " . dbquote($year);

					
			$result = mysql_query($sql) or dberror($sql);

			echo $sql . "<br />";

		}

		/*
		foreach($debug as $key=>$value)
		{
			echo $value . "<br />";
		}
		cde();
		*/
	}
	else
	{
		$tob_rents_80_percent = array();

		$taxes_80_percent = array();
		$passenger_index_80_percent = array();

		$taxes_on_fixed_rents = array();
		$passenger_index_on_fixed_rents = array();

		if(count($turn_over_based_rents) > 0)
		{
			foreach($turn_over_based_rents as $year=>$rent)
			{
				
				//get taxes on fixed rents
				//01 get existing turnover based rent
				$sql_r = "select cer_expense_amount " . 
						   " from cer_draft_expenses " . 
						   " where cer_expense_type = 16 " . 
						   " and cer_expense_draft_id = " . $draft_id . 
						   " and cer_expense_year = " . dbquote($year);

						
				$res_r = mysql_query($sql_r) or dberror($sql_r);

				if ($row_r = mysql_fetch_assoc($res_r))
				{
					$tmp_tob_rent = $row_r["cer_expense_amount"];
					$original_tax = $tax_rates[$year]*$tmp_tob_rent;
					$original_passenger_index = $passenger_rates[$year]*$tmp_tob_rent;

					//02 get original total tax
					$sql_r = "select cer_expense_amount " . 
							   " from cer_draft_expenses " . 
							   " where cer_expense_type = 18 " . 
							   " and cer_expense_draft_id = " . $draft_id . 
							   " and cer_expense_year = " . dbquote($year);

							
					$res_r = mysql_query($sql_r) or dberror($sql_r);

					if ($row_r = mysql_fetch_assoc($res_r))
					{
						$original_total_tax = $row_r["cer_expense_amount"];
						$taxes_on_fixed_rents[$year] = $original_total_tax - $original_tax;
					}
					else
					{
						$taxes_on_fixed_rents[$year] = 0;
					}


					//03 get original total passenger index
					$sql_r = "select cer_expense_amount " . 
							   " from cer_draft_expenses " . 
							   " where cer_expense_type = 19 " . 
							   " and cer_expense_draft_id = " . $draft_id .  
							   " and cer_expense_year = " . dbquote($year);

							
					$res_r = mysql_query($sql_r) or dberror($sql_r);

					if ($row_r = mysql_fetch_assoc($res_r))
					{
						$original_total_passenger_index = $row_r["cer_expense_amount"];
						$passenger_index_on_fixed_rents[$year] = $original_total_passenger_index - $original_passenger_index;
					}
					else
					{
						$taxes_on_fixed_rents[$year] = 0;
					}
				}
				else
				{
					$taxes_on_fixed_rents[$year] = 0;
					$passenger_index_on_fixed_rents = 0;
				}



				
				//calculate rent on the basis of whatever is higher
				// TOB-Rents are not added to fixed rents
				if($cer_basicdata["cer_basicdata_add_tob_rents"] == 0)
				{
					if($rent > $fixed_rents[$year])
					{
						$rent = $rent - $fixed_rents[$year];
					}
					else
					{
						$rent = 0;
					}
				}

				$tob_rents_80_percent[$year] = $rent;
				
				//taxrates
				$amount = $tax_rates[$year]*$rent;
				$taxes_80_percent[$year] = $amount + $taxes_on_fixed_rents[$year];
			
					
				//passenger index
				$amount = $passenger_rates[$year]*$rent;
				$passenger_index_80_percent[$year] = $amount + $passenger_index_on_fixed_rents[$year];
			}
		
			return array("tob_rents"=>$tob_rents_80_percent, "taxes"=>$taxes_80_percent, "passenger_index"=>$passenger_index_80_percent);
		}
		else
		{
			foreach($fixed_rents as $year=>$rent)
			{
				
				//get taxes on fixed rents
				$sql_r = "select cer_expense_amount " . 
						   " from cer_draft_expenses " . 
						   " where cer_expense_type = 18 " . 
							   " and cer_expense_draft_id = " . $draft_id .  
							   " and cer_expense_year = " . dbquote($year);

						
				$res_r = mysql_query($sql_r) or dberror($sql_r);

				if ($row_r = mysql_fetch_assoc($res_r))
				{
					$taxes_80_percent[$year] = $row_r["cer_expense_amount"];
				}
				else
				{
					$taxes_80_percent[$year] = 0;
				}


				//get passenger index on fixed rents
				$sql_r = "select cer_expense_amount " . 
						   " from cer_draft_expenses " . 
						   " where cer_expense_type = 19 " . 
							   " and cer_expense_draft_id = " . $draft_id .  
							   " and cer_expense_year = " . dbquote($year);

						
				$res_r = mysql_query($sql_r) or dberror($sql_r);

				if ($row_r = mysql_fetch_assoc($res_r))
				{
					$passenger_index_80_percent[$year] = $row_r["cer_expense_amount"];
				}
				else
				{
					$passenger_index_80_percent[$year] = 0;
				}
					
			}
		
			return array("tob_rents"=>$tob_rents_80_percent, "taxes"=>$taxes_80_percent, "passenger_index"=>$passenger_index_80_percent);
		}
	}

	return true;
}


/********************************************************************
    update commissions
*********************************************************************/
function update_commissions($draft_id)
{
	
	$revenue_watches = array();
	$revenue_jewellery = array();
	$revenue_services = array();

	$cer_basicdata = get_cer_basicdata($draft_id);

	$global_commission_percent = $cer_basicdata["cer_basicdata_commission"];
	$global_commission_tax_percent = $cer_basicdata["cer_basicdata_commission_tax_percent"];


	$sql_salaries = "select * from cer_draft_salaries " . 
		            "where cer_salary_draft_id = " . $draft_id;


	//caclulate net sales, based on net sales values, calculated backwards up to gross sales
	$total_gross_sales_values = array();
	$total_net_sales_values = array();
	$gross_sales_watches_values = array();
	$gross_sales_jewellery_values = array();
	$gross_sales_customer_service_values = array();

	$net_sales_watches_values = array();
	$net_sales_jewellery_values = array();
	$net_sales_customer_service_values = array();

	//get sales planning
	$sql = "select * from cer_draft_revenues " .
		   "where cer_revenue_draft_id = " . $draft_id;

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$year = $row["cer_revenue_year"];
		
		/*
		$sales_reduction_watches = $row["cer_revenue_sales_reduction_watches"] + $row["cer_revenue_reduction_credit_cards"];
		$sales_reduction_bijoux = $row["cer_revenue_sales_reduction_bijoux"] + $row["cer_revenue_reduction_credit_cards"];
		$sales_reduction_accessories = $row["cer_revenue_sales_reduction_accessories"] + $row["cer_revenue_reduction_credit_cards"];
		$sales_reduction_services = $row["cer_revenue_sales_reduction_cservice"] + $row["cer_revenue_reduction_credit_cards"];
		*/

		$sales_reduction_watches = $row["cer_revenue_sales_reduction_watches"];
		$sales_reduction_bijoux = $row["cer_revenue_sales_reduction_bijoux"];
		$sales_reduction_accessories = $row["cer_revenue_sales_reduction_accessories"];
		$sales_reduction_services = $row["cer_revenue_sales_reduction_cservice"];
		
		$gross_sales_watches_values[$year] = $row["cer_revenue_watches"];
		$gross_sales_jewellery_values[$year] = $row["cer_revenue_jewellery"];
		$gross_sales_accessories_values[$year] = $row["cer_revenue_accessories"];
		$gross_sales_customer_service_values[$year] = $row["cer_revenue_customer_service"];

		
		$net_sales_watches_values[$year] = $gross_sales_watches_values[$year] - ($gross_sales_watches_values[$year]*$sales_reduction_watches/100);
		
		$net_sales_jewellery_values[$year] = $gross_sales_jewellery_values[$year] - ($gross_sales_jewellery_values[$year]*$sales_reduction_bijoux/100); 
		
		$net_sales_accessories_values[$year] = $gross_sales_accessories_values[$year] - ($gross_sales_accessories_values[$year]*$sales_reduction_accessories/100);

		
		$net_sales_customer_service_values[$year] = $gross_sales_customer_service_values[$year]- ($gross_sales_customer_service_values[$year]*$sales_reduction_services/100);


		
		
		$commission_watches_values[$year] = 0;
		$commission_jewellery_values[$year] = 0;
		$commission_accessories_values[$year] = 0;
		$commission_customer_service_values[$year] = 0;
		$commission[$year] = 0;


		$res_s = mysql_query($sql_salaries) or dberror($sql_salaries);
		while ($row_s = mysql_fetch_assoc($res_s))
		{
			
			
			if($cer_basicdata["cer_basicdata_commission_from_net_sales"] == 1)
			{
				if($row_s["cer_salary_commission_percent"] > 0)
				{
					$commission_watches_values[$year] = $net_sales_watches_values[$year] * $row_s["cer_salary_commission_percent"] / 100;
					$commission_jewellery_values[$year] = $net_sales_jewellery_values[$year] * $row_s["cer_salary_commission_percent"] / 100;
					$commission_accessories_values[$year] = $net_sales_accessories_values[$year] * $row_s["cer_salary_commission_percent"] / 100;
					$commission_customer_service_values[$year] = $net_sales_customer_service_values[$year] * $row_s["cer_salary_commission_percent"] / 100;

					$total_commission = $commission_watches_values[$year] + $commission_jewellery_values[$year] + $commission_accessories_values[$year] + $commission_customer_service_values[$year];
					$commission[$year] = $commission[$year] + $total_commission;
				}
				if($row_s["cer_salary_commission_social_charges_percent"] > 0)
				{
					$commission[$year] = $commission[$year] + ($total_commission* $row_s["cer_salary_commission_social_charges_percent"] / 100);
				}
			}
			else
			{
				if($row_s["cer_salary_commission_percent"] > 0)
				{
					$commission_watches_values[$year] = $gross_sales_watches_values[$year] * $row_s["cer_salary_commission_percent"] / 100;
					$commission_jewellery_values[$year] = $gross_sales_jewellery_values[$year] * $row_s["cer_salary_commission_percent"] / 100;
					$commission_accessories_values[$year] = $gross_sales_accessories_values[$year] * $row_s["cer_salary_commission_percent"] / 100;
					$commission_customer_service_values[$year] = $gross_sales_customer_service_values[$year] * $row_s["cer_salary_commission_percent"] / 100;
					
					$total_commission = $commission_watches_values[$year] + $commission_jewellery_values[$year] + $commission_accessories_values[$year] + $commission_customer_service_values[$year];
					$commission[$year] = $commission[$year] + $total_commission;
				}

				//echo $year . " " . $commission[$year] . "<br />";

				if($row_s["cer_salary_commission_social_charges_percent"] > 0)
				{
					$commission[$year] = $commission[$year] + ($total_commission* $row_s["cer_salary_commission_social_charges_percent"] / 100);
				}
			}
		}


		//calculate global commission
		if($global_commission_percent > 0)
		{
			if($cer_basicdata["cer_basicdata_commission_from_net_sales"] == 1)
			{
				$commission_watches_values[$year] = $net_sales_watches_values[$year] * $global_commission_percent / 100;
				$commission_jewellery_values[$year] = $net_sales_jewellery_values[$year] * $global_commission_percent / 100;
				$commission_accessories_values[$year] = $net_sales_accessories_values[$year] * $global_commission_percent / 100;
				$commission_customer_service_values[$year] = $net_sales_customer_service_values[$year] * $global_commission_percent / 100;
				
				$total_commission = $commission_watches_values[$year] + $commission_jewellery_values[$year] + $commission_accessories_values[$year] + $commission_customer_service_values[$year];
				$commission[$year] = $commission[$year] + $total_commission;

				if($global_commission_tax_percent > 0)
				{
					$commission[$year] = $commission[$year] + ($total_commission* $global_commission_tax_percent / 100);
				}
			}
			else
			{
				$commission_watches_values[$year] = $gross_sales_watches_values[$year] * $global_commission_percent / 100;
				$commission_jewellery_values[$year] = $gross_sales_jewellery_values[$year] * $global_commission_percent / 100;
				$commission_accessories_values[$year] = $gross_sales_accessories_values[$year] * $global_commission_percent / 100;
				$commission_customer_service_values[$year] = $gross_sales_customer_service_values[$year] * $global_commission_percent / 100;
				
				$total_commission = $commission_watches_values[$year] + $commission_jewellery_values[$year] + $commission_accessories_values[$year] + $commission_customer_service_values[$year];
				$commission[$year] = $commission[$year] + $total_commission;

				if($global_commission_tax_percent > 0)
				{
					$commission[$year] = $commission[$year] + ($total_commission* $global_commission_tax_percent / 100);
				}
			}
		}

		

		$fields = array();

		$value = dbquote($commission[$year]);
		$fields[] = "cer_expense_amount = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}

		
		$sql = "update cer_draft_expenses set " . join(", ", $fields) . " where cer_expense_draft_id = " .$draft_id . " and cer_expense_type = 17 and cer_expense_year = " . dbquote($row["cer_revenue_year"]);
		mysql_query($sql) or dberror($sql);

	}
	
	return true;
	

}

/********************************************************************
    get cer_basicdata
*********************************************************************/
function get_project_cer_basicdata($draft_id)
{

	$basic_data = array();

    $sql = "select * ,cer_basicdata.date_created as version_date " . 
		   "from cer_basicdata " . 
	       "left join currencies on currency_id = cer_basicdata_currency " . 
		   "left join cer_summary on cer_summary_project = cer_basicdata_project " . 
	       "where cer_basicdata_version = 0 and cer_basicdata_project = " . dbquote($draft_id);
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $basic_data = $row;
    }


	return $basic_data;

}

?>