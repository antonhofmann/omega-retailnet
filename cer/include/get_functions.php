<?php
/********************************************************************

    get_functions.php

    Various utility functions to get information from tables.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-26
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-26

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

/********************************************************************
    get system currency informations
*********************************************************************/
function get_system_currency_fields()
{
    $system_currency = array();

    $sql = "select * from currencies where currency_system=1";
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $system_currency["id"] = $row["currency_id"];
        $system_currency["symbol"] = $row["currency_symbol"];
        $system_currency["exchange_rate"] = $row["currency_exchange_rate"];
        $system_currency["factor"] = $row["currency_factor"];
    }
    
    return $system_currency;
}

/********************************************************************
    get currency informations
*********************************************************************/
function get_currency($currency_id)
{
    $currency = array();

    $sql = "select * from currencies where currency_id = " . $currency_id;
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $currency["id"] = $currency_id;
        $currency["symbol"] = $row["currency_symbol"];
        $currency["exchange_rate"] = $row["currency_exchange_rate"];
        $currency["factor"] = $row["currency_factor"];
    }
    else
    {
        $currency["id"] = 0;
        $currency["symbol"] = "";
        $currency["exchange_rate"] = 0;
        $currency["factor"] = 0;
    }
    
    return $currency;
}


/********************************************************************
    get currency
*********************************************************************/
function get_currency_symbol($currency_id)
{
    $currency = array();
    
	$sql = "select * from currencies " . 
		   "where currency_id = " . $currency_id;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$currency["id"] = $row["currency_id"];
		$currency["symbol"] = $row["currency_symbol"];
	}

    else
    {
        $currency["id"] = 0;
        $currency["symbol"] = "";
    }
    
    return $currency["symbol"];
}

/********************************************************************
    get id of cer_basicdata
*********************************************************************/
function build_missing_cer_investment_records($pid, $version = 0)
{
	$sql = "select * from posinvestment_types where posinvestment_type_active = 1";
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$sql_i = "select cer_investment_id from cer_investments " .
				 "where cer_investment_cer_version = " . $version . " and cer_investment_project = " .  $pid . 
				 " and cer_investment_type = " . $row["posinvestment_type_id"];

		$res_i = mysql_query($sql_i) or dberror($sql_i);
		if (!$row_i = mysql_fetch_assoc($res_i))
		{
			$sql = "insert into cer_investments (" .
				   "cer_investment_cer_version, " .
				   "cer_investment_project, " .
				   "cer_investment_type, " .
				   "user_created, date_created) values (".
				   $version  . ", " .
				   param("pid") . ", " .
				   $row["posinvestment_type_id"] . ", " . 
				   dbquote(user_login()) . ", " . 
				   "current_timestamp)";

			$result = mysql_query($sql) or dberror($sql);
		}
	}
	
	return true;
}



/********************************************************************
    check if cer_summary exists
*********************************************************************/
function build_missing_cer_records($pid)
{
	$sql = "select count(cer_summary_id) as num_recs " . 
       "from cer_summary " . 
	   "where cer_summary_cer_version = 0 and cer_summary_project = " . $pid;

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	if ($row["num_recs"] == 0)
	{
		//create cer_revenue records
	
		$fields = array();
		$values = array();

		$fields[] = "cer_summary_project";
		$values[] = param("pid");

	
		$fields[] = "date_created";
		$values[] = "now()";

		$fields[] = "date_modified";
		$values[] = "now()";

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$fields[] = "user_modified";
		$values[] = dbquote(user_login());

		$sql = "insert into cer_summary (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
		
		mysql_query($sql) or dberror($sql);

	}

	// cer basic data
	$currency_id = 0;
	$exchange_rate = 0;
	$factor = 0;
	$country_id = 0;
	$order_state = 0;

	$sql = "select currency_id, currency_exchange_rate, currency_factor, country_id, " .
		   "order_actual_order_state_code " . 
		   "from projects " . 
		   "left join orders on order_id = project_order " . 
		   "left join addresses on address_id = order_client_address " .
		   "left join countries on country_id = address_country " . 
		   "left join currencies on currency_id = country_currency " . 
		   "where project_id = " . $pid;

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$currency_id = $row["currency_id"];
		$exchange_rate = $row["currency_exchange_rate"];
		$factor = $row["currency_factor"];
		$country_id = $row["country_id"];
		$order_state = $row["order_actual_order_state_code"];
	}


	//get currency data
	$currency_id2 = 0;
	$exchange_rate2 = 0;
	$factor2 = 0;

	$sql = "select currency_id, currency_exchange_rate, currency_factor " . 
		   "from projects " . 
		   "left join orders on order_id = project_order " . 
		   "left join countries on country_id = order_shop_address_country " . 
		   "left join currencies on currency_id = country_currency " . 
		   "where project_id = " . $pid;

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$currency_id2 = $row["currency_id"];
		$exchange_rate2 = $row["currency_exchange_rate"];
		$factor2 = $row["currency_factor"];
	}

	
	//update cer_basic_data
	$sql = "select count(cer_basicdata_id) as num_recs " . 
			"from cer_basicdata " . 
			"where cer_basicdata_version = 0 and cer_basicdata_project = " . $pid;

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	if ($row["num_recs"] == 0)
	{
		
		$fields = array();
		$values = array();

		$fields[] = "cer_basicdata_project";
		$values[] = param("pid");

		$fields[] = "cer_basicdata_currency";
		$values[] = dbquote($currency_id);

		$fields[] = "cer_basicdata_factor";
		$values[] = dbquote($factor);


		$fields[] = "cer_basicdata_currency2";
		$values[] = dbquote($currency_id2);

		$fields[] = "cer_basicdata_factor2";
		$values[] = dbquote($factor2);


		$fields[] = "date_created";
		$values[] = "now()";

		$fields[] = "date_modified";
		$values[] = "now()";

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$fields[] = "user_modified";
		$values[] = dbquote(user_login());

		$sql = "insert into cer_basicdata (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
		
		mysql_query($sql) or dberror($sql);

	}


	$sql = "select * from posinvestment_types where posinvestment_type_active = 1";
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$sql_i = "select cer_investment_id from cer_investments " .
				 "where cer_investment_cer_version = 0 and cer_investment_project = " .  $pid . 
				 " and cer_investment_type = " . $row["posinvestment_type_id"];

		$res_i = mysql_query($sql_i) or dberror($sql_i);
		if (!$row_i = mysql_fetch_assoc($res_i))
		{
			$sql = "insert into cer_investments (" .
				   "cer_investment_project, " .
				   "cer_investment_type, " .
				   "user_created, date_created) values (".
				   param("pid") . ", " .
				   $row["posinvestment_type_id"] . ", " . 
				   dbquote(user_login()) . ", " . 
				   "current_timestamp)";

			$result = mysql_query($sql) or dberror($sql);
		}
	}
	
	return true;

}

/********************************************************************
    get id of cer_basicdata
*********************************************************************/
function get_id_of_cer_basicdata($pid, $version = 0)
{
	$sql = "select cer_basicdata_id from cer_basicdata where cer_basicdata_version = " . $version. " and cer_basicdata_project = " . $pid;
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	return $row["cer_basicdata_id"];
}


/********************************************************************
    get cer_basicdata
*********************************************************************/
function get_cer_basicdata($project_id, $version = 0)
{

	$basic_data = array();

    $sql = "select *, cer_basicdata.date_created as versiondate from cer_basicdata " . 
	       "left join currencies on currency_id = cer_basicdata_currency " . 
		   "left join cer_summary on cer_summary_project = cer_basicdata_project " . 
	       "where cer_basicdata_version = " . $version . " and cer_basicdata_project = " . dbquote($project_id);
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $basic_data = $row;
    }

	return $basic_data;

}


/********************************************************************
    get ln_basicdata
*********************************************************************/
function get_ln_basicdata($project_id = 0, $version = 0)
{

	$basic_data = array();

    $sql = "select *, ln_basicdata.date_created as versiondate from ln_basicdata " . 
	       "left join projects on project_id = ln_basicdata_project " . 
		   "where ln_basicdata_version = " . $version . " and ln_basicdata_project = " . dbquote($project_id);
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$basic_data = $row;
    }
	else
	{
		
		//get currency data
		$currency_id = 0;
		$exchange_rate = 0;
		$factor = 0;

		$sql = "select currency_id, currency_exchange_rate, currency_factor, country_id, " .
			   "order_actual_order_state_code " . 
			   "from projects " . 
			   "left join orders on order_id = project_order " . 
			   "left join addresses on address_id = order_client_address " .
			   "left join countries on country_id = address_country " . 
			   "left join currencies on currency_id = country_currency " . 
			   "where project_id = " . $project_id;

		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$currency_id = $row["currency_id"];
			$exchange_rate = $row["currency_exchange_rate"];
			$factor = $row["currency_factor"];
			$country_id = $row["country_id"];
			$order_state = $row["order_actual_order_state_code"];
		}


		//get currency data
		$currency_id = 0;
		$exchange_rate = 0;
		$factor = 0;

		$sql = "select currency_id, currency_exchange_rate, currency_factor " . 
			   "from projects " . 
			   "left join orders on order_id = project_order " . 
			   "left join countries on country_id = order_shop_address_country " . 
			   "left join currencies on currency_id = country_currency " . 
			   "where project_id = " . $project_id;

		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$currency_id = $row["currency_id"];
			$exchange_rate = $row["currency_exchange_rate"];
			$factor = $row["currency_factor"];
		}



		$fields = array();
		$values = array();

		$fields[] = "ln_basicdata_project";
		$values[] = dbquote($project_id);

		$fields[] = "ln_basicdata_version";
		$values[] = dbquote($version);

		$fields[] = "ln_basicdata_currency";
		$values[] = dbquote($currency_id);

		$fields[] = "ln_basicdata_exchangerate";
		$values[] = dbquote($exchange_rate);

		$fields[] = "ln_basicdata_exr_date";
		$values[] = dbquote(date("Y-m-d"));

		$fields[] = "ln_basicdata_factor";
		$values[] = dbquote($factor);

		$fields[] = "date_created";
		$values[] = "now()";

		$fields[] = "date_modified";
		$values[] = "now()";

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$fields[] = "user_modified";
		$values[] = dbquote(user_login());

		$sql = "insert into ln_basicdata (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
		
		mysql_query($sql) or dberror($sql);

		 $sql = "select * from ln_basicdata " . 
			   "left join projects on project_id = ln_basicdata_project " . 
			   "where ln_basicdata_version = " . $version . " and ln_basicdata_project = " . dbquote($project_id);
		$res = mysql_query($sql) or dberror($sql);

		$basic_data =mysql_fetch_assoc($res);
	
	}


	if($basic_data["ln_basicdata_former_pos_id"] > 0) {
		$sql = "select postype_name, project_costtype_text 
		        from posaddresses
				left join postypes on posaddress_store_postype = postype_id 
				left join project_costtypes on project_costtype_id = posaddress_ownertype
				where posaddress_id = " . $basic_data["ln_basicdata_former_pos_id"];
		
		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res)) {
			$basic_data['former_pos_postype_name'] = $row['postype_name'];
			$basic_data['former_pos_project_costtype_text'] = $row['project_costtype_text'];
		}
	}

	return $basic_data;

}

/********************************************************************
    check if basic data exists
*********************************************************************/
function check_basic_data($pid, $version = 0)
{
	
	//get standardparameters
	$sparams = array();
	$sql = "select * from cer_standardparameters";
	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$sparams[$row["cer_standardparameter_id"]] = $row["cer_standardparameter_value"];
	}


	//get currency data
	$currency_id = 0;
	$exchange_rate = 0;
	$factor = 0;
	$country_id = 0;
	$order_state = 0;

	$sql = "select currency_id, currency_exchange_rate, currency_factor, country_id, " .
		   "order_actual_order_state_code " . 
		   "from projects " . 
		   "left join orders on order_id = project_order " . 
		   "left join addresses on address_id = order_client_address " .
		   "left join countries on country_id = address_country " . 
		   "left join currencies on currency_id = country_currency " . 
		   "where project_id = " . $pid;

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$currency_id = $row["currency_id"];
		$exchange_rate = $row["currency_exchange_rate"];
		$factor = $row["currency_factor"];
		$country_id = $row["country_id"];
		$order_state = $row["order_actual_order_state_code"];
	}


	//get currency data
	$currency_id2 = 0;
	$exchange_rate2 = 0;
	$factor2 = 0;

	$sql = "select currency_id, currency_exchange_rate, currency_factor " . 
		   "from projects " . 
		   "left join orders on order_id = project_order " . 
		   "left join countries on country_id = order_shop_address_country " . 
		   "left join currencies on currency_id = country_currency " . 
		   "where project_id = " . $pid;

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$currency_id2 = $row["currency_id"];
		$exchange_rate2 = $row["currency_exchange_rate"];
		$factor2 = $row["currency_factor"];
	}

	//get inflationrates
	$inflationrates = array();
	$sql = "select * from cer_inflationrates " . 
		   "where inflationrate_country = " . $country_id . 
		   " order by inflationrate_year";

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$inflationrates[$row["inflationrate_year"]] = $row["inflationrate_rate"];
	}

	$inflationrates = serialize($inflationrates);
	
	//get interestrates
	$interestrates = array();

	$sql = "select * " .
		   "from cer_interestrates where interestrate_country = " . $country_id . 
		   " order by interestrate_year ASC";

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$interestrates[$row["interestrate_year"]] = $row["interestrate_rate"];
	}

	$interestrates = serialize($interestrates);

	
	//update cer_basic_data

	$sql = "select count(cer_basicdata_id) as num_recs " . 
			"from cer_basicdata " . 
			"where cer_basicdata_project = " . $pid;

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	if ($row["num_recs"] == 0)
	{
		
		$starting_year = date("Y");
		$starting_month = date("m");
		
		$fields = array();
		$values = array();

		$fields[] = "cer_basicdata_version";
		$values[] = dbquote($version);

		$fields[] = "cer_basicdata_version_context";
		$values[] = dbquote("cer");

		$fields[] = "cer_basicdata_version_user_id";
		$values[] = user_id();

		$fields[] = "cer_basicdata_project";
		$values[] = param("pid");

		$fields[] = "cer_basicdata_currency";
		$values[] = dbquote($currency_id);

		$fields[] = "cer_basicdata_exchangerate";
		$values[] = dbquote($exchange_rate);

		$fields[] = "cer_basicdata_factor";
		$values[] = dbquote($factor);


		$fields[] = "cer_basicdata_currency2";
		$values[] = dbquote($currency_id2);

		$fields[] = "cer_basicdata_exchangerate2";
		$values[] = dbquote($exchange_rate2);

		$fields[] = "cer_basicdata_factor2";
		$values[] = dbquote($factor2);


		$fields[] = "cer_basicdata_exr_date";
		$values[] = dbquote(date("Y-m-d"));

		$fields[] = "cer_basicdata_firstyear";
		$values[] = dbquote($starting_year);

		$fields[] = "cer_basicdata_firstmonth";
		$values[] = dbquote($starting_month);


		$fields[] = "cer_basicdata_dicount_rate";
		$values[] = dbquote($sparams[1]);

		$fields[] = "cer_bascidata_liquidation_keymoney";
		$values[] = dbquote($sparams[2]);

		$fields[] = "cer_basicdata_liquidation_deposit";
		$values[] = dbquote($sparams[3]);

		$fields[] = "cer_bascidata_liquidation_stock";
		$values[] = dbquote($sparams[4]);

		$fields[] = "cer_basicdata_inflationrates";
		$values[] = dbquote($inflationrates);

		$fields[] = "cer_basic_data_interesrates";
		$values[] = dbquote($interestrates);

		$fields[] = "cer_bascicdate_liquidation_staff";
		$values[] = dbquote($sparams[5]);

		$fields[] = "date_created";
		$values[] = "now()";

		$fields[] = "date_modified";
		$values[] = "now()";

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$fields[] = "user_modified";
		$values[] = dbquote(user_login());

		$sql = "insert into cer_basicdata (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

		mysql_query($sql) or dberror($sql);


		//update business partner's investment share
		$sql = "select sum(costsheet_budget_amount) as budget_total
				from costsheets
				where costsheet_project_id = " . dbquote($pid);

		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);
		
		$budget_total = $row["budget_total"];

		$sql = "select sum(costsheet_partner_contribution*costsheet_budget_amount/100) as partner_total
				from costsheets
				where costsheet_partner_contribution > 0 
				   and costsheet_project_id = " . dbquote($pid);

		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);
		
		$partner_total = $row["partner_total"];
		
		$share = 0;
		if($budget_total > 0) {
			$share = round(100*($partner_total/$budget_total), 2);
		}


		if($share > 0) {
			
			$sql = "update cer_basicdata set " . 
				 "cer_basicdata_franchsiee_investment_share = " . dbquote($share) . ", " .
				 "date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", " . 
				 "user_modified = " . dbquote(user_login()) . 
				 " where cer_basicdata_version = 0 and cer_basicdata_project = " . dbquote($pid);

			$result = mysql_query($sql) or dberror($sql);
		}
		else {
			$sql = "select  project_share_other 
			       from projects where project_id = " . dbquote($pid);
			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res)) {
				$sql = "update cer_basicdata set " . 
					 "cer_basicdata_franchsiee_investment_share = " . dbquote($row["project_share_other"]) . ", " .
					 "date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", " . 
					 "user_modified = " . dbquote(user_login()) . 
					 " where cer_basicdata_version = 0 and cer_basicdata_project = " . dbquote($pid);

				$result = mysql_query($sql) or dberror($sql);
			}
		}

	}
	else
	{
		return true;
		//do not update
		
		//update only if project state <= 620

		if($order_state <= "620")
		{
			$fields = array();
		
			$fields[] = "cer_basicdata_currency = " . dbquote($currency_id);

			$fields[] = "cer_basicdata_exchangerate = " . dbquote($exchange_rate);

			$fields[] = "cer_basicdata_exr_date = " . dbquote(date("Y-m-d"));;

			$fields[] = "cer_basicdata_factor = " . dbquote($factor);

			$fields[] = "cer_basicdata_inflationrates = " . dbquote($inflationrates);

			$fields[] = "cer_basic_data_interesrates = " . dbquote($interestrates);


			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_basicdata set " . join(", ", $fields) . " where cer_basicdata_version = " . $version . " and cer_basicdata_project = " . $pid;
			mysql_query($sql) or dberror($sql);

		}
	
	}

	return true;

}


/********************************************************************
    check if expense data exists
*********************************************************************/
function check_expenses($project_id, $first_year, $last_year, $version = 0)
{
	$existing_years = array();

	for($year = $first_year;$year <= $last_year; $year++)
	{
		$existing_years[] = $year;
	}

	
	//check if expenses for all years ar present
	$sql = "select count(cer_expense_type_id) as num_recs " . 
		   "from cer_expense_types " .
		   "where cer_expense_type_active = 1 " . 
		  "order by cer_expense_type_sortorder";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);

	$num_of_expenses = $row["num_recs"];
	$num_of_years = $last_year - $first_year + 1;
	$num_of_expenses_that_should_be = $num_of_expenses * $num_of_years;


	$sql_e = "select count(cer_expense_id) as num_recs " .
		     "from cer_expenses " . 
		     "where cer_expense_project = " . $project_id . 
		     " and cer_expense_cer_version = " . $version;

	$res_e = mysql_query($sql_e) or dberror($sql_e);
	$row_e = mysql_fetch_assoc($res_e);
	
	if($row_e['num_recs'] < $num_of_expenses_that_should_be)
	{
		for($year = $first_year;$year <= $last_year; $year++)
		{
			//create cer_expense records
			$sql = "select * from cer_expense_types " .
				   "where cer_expense_type_active = 1 " . 
				   "order by cer_expense_type_sortorder";
			$res = mysql_query($sql) or dberror($sql);
			while ($row = mysql_fetch_assoc($res))
			{
				
				$sql_e = "select count(cer_expense_id) as num_recs " .
					   "from cer_expenses " . 
					   "where cer_expense_type =  " . $row["cer_expense_type_id"] .
					   " and cer_expense_year = " . dbquote($year) . 
					   " and cer_expense_project = " . $project_id . 
					   " and cer_expense_cer_version = " . $version;

				$res_e = mysql_query($sql_e) or dberror($sql_e);
				$row_e = mysql_fetch_assoc($res_e);
				if ($row_e["num_recs"] == 0)
				{
				
					$fields = array();
					$values = array();

					$fields[] = "cer_expense_project";
					$values[] = $project_id;

					$fields[] = "cer_expense_type";
					$values[] = dbquote($row["cer_expense_type_id"]);
					
					$fields[] = "cer_expense_year";
					$values[] = $year;

					$fields[] = "date_created";
					$values[] = "now()";

					$fields[] = "date_modified";
					$values[] = "now()";

					$fields[] = "user_created";
					$values[] = dbquote(user_login());

					$fields[] = "user_modified";
					$values[] = dbquote(user_login());

					$sql = "insert into cer_expenses (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					
					mysql_query($sql) or dberror($sql);
				}
				
			}
		}
	}
	

	//delete records not enclosed in the business plan period
	$sql = "select distinct cer_expense_year " .
		   "from cer_expenses " .
		   " where cer_expense_project = " . $project_id . 
		   " and cer_expense_cer_version = " . $version;

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		if(!in_array($row["cer_expense_year"], $existing_years))
		{
			$sql = "delete from cer_expenses " . 
			   "where cer_expense_year = " . $row["cer_expense_year"] . 
			   " and cer_expense_project = " . $project_id .
			   " and cer_expense_cer_version = " . $version;

			$result = mysql_query($sql) or dberror($sql);
		}
	
	}

	return true;
}

/******************************************************************************************************
    check if revenue data exists
******************************************************************************************************/
function check_revenues($project_id, $first_year, $last_year, $version = 0, $selected_brands = array())
{
	if(count($selected_brands) > 0)
	{
		foreach($selected_brands as $brand_id=>$brand_name)
		{
			$existing_years = array();

			for($year = $first_year;$year <= $last_year; $year++)
			{
				$existing_years[] = $year;

				$sql = "select count(cer_revenue_id) as num_recs " .
						"from cer_revenues " . 
						"where cer_revenue_cer_version = " . $version . " and cer_revenue_year =  " . $year .
						" and cer_revenue_project = " . $project_id . 
						" and cer_revenue_brand_id = " . $brand_id;

				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				if ($row["num_recs"] == 0)
				{
					$fields = array();
					$values = array();

					$fields[] = "cer_revenue_brand_id";
					$values[] = $brand_id;

					$fields[] = "cer_revenue_project";
					$values[] = $project_id;

					$fields[] = "cer_revenue_cer_version";
					$values[] = $version;

					$fields[] = "cer_revenue_year";
					$values[] = dbquote($year);
					
					$fields[] = "date_created";
					$values[] = "now()";

					$fields[] = "date_modified";
					$values[] = "now()";

					$fields[] = "user_created";
					$values[] = dbquote(user_login());

					$fields[] = "user_modified";
					$values[] = dbquote(user_login());

					$sql = "insert into cer_revenues (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					
					mysql_query($sql) or dberror($sql);
				}
			}

			//delete records not enclosed in the business plan period
			$sql = "select distinct cer_revenue_year " .
				   "from cer_revenues " .
				   " where cer_revenue_cer_version = " . $version . 
				   " and cer_revenue_project = " . $project_id . 
				   " and cer_revenue_brand_id = " . $brand_id;

			$res = mysql_query($sql) or dberror($sql);
			while ($row = mysql_fetch_assoc($res))
			{
				if(!in_array($row["cer_revenue_year"], $existing_years))
				{
					$sql = "delete from cer_revenues " . 
					   "where cer_revenue_year = " . $row["cer_revenue_year"] . 
					   " and cer_revenue_cer_version = " . $version . 
					   " and cer_revenue_project = " . $project_id . 
					   " and cer_revenue_brand_id = " . $brand_id;

					$result = mysql_query($sql) or dberror($sql);
				}
			
			}
		}
	}

	return true;
}

/********************************************************************
    check if stock data exists
*********************************************************************/
function check_stocks($project_id, $first_year, $last_year, $version = 0)
{

	$existing_years = array();

	for($year = $first_year;$year <= $last_year; $year++)
	{
		$existing_years[] = $year;
	
		$sql = "select count(cer_stock_id) as num_recs " .
				"from cer_stocks " . 
				"where cer_stock_cer_version = " . $version . " and cer_stock_year = " . $year . 
			    " and cer_stock_project = " . $project_id;

		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);
		if ($row["num_recs"] == 0)
		{
			$fields = array();
			$values = array();

			$fields[] = "cer_stock_project";
			$values[] = $project_id;

			$fields[] = "cer_stock_cer_version";
			$values[] = $version;

			$fields[] = "cer_stock_year";
			$values[] = dbquote($year);
			
			$fields[] = "date_created";
			$values[] = "now()";

			$fields[] = "date_modified";
			$values[] = "now()";

			$fields[] = "user_created";
			$values[] = dbquote(user_login());

			$fields[] = "user_modified";
			$values[] = dbquote(user_login());

			$sql = "insert into cer_stocks (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			
			mysql_query($sql) or dberror($sql);
		}
	}

	//delete records not enclosed in the business plan period
	$sql = "select distinct cer_stock_year " .
		   "from cer_stocks " .
		   " where cer_stock_cer_version = " . $version . " and cer_stock_project = " . $project_id;

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		if(!in_array($row["cer_stock_year"], $existing_years))
		{
			$sql = "delete from cer_stocks " . 
			   "where cer_stock_year = " . $row["cer_stock_year"] . 
			   " and cer_stock_cer_version = " . $version . " and cer_stock_project = " . $project_id;

			$result = mysql_query($sql) or dberror($sql);
		}
	
	}

	return true;
}


/********************************************************************
    check if paymentterma data exists
*********************************************************************/
function check_paymentterms($project_id, $first_year, $last_year, $version = 0)
{

	$existing_years = array();

	for($year = $first_year;$year <= $last_year; $year++)
	{
		$existing_years[] = $year;

		$sql = "select count(cer_paymentterm_id) as num_recs " .
				"from cer_paymentterms " . 
				"where cer_paymentterm_cer_version = " . $version . " and cer_paymentterm_year = " . $year . 
			    " and cer_paymentterm_project = " . $project_id;

		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);
		if ($row["num_recs"] == 0)
		{
			$fields = array();
			$values = array();

			$fields[] = "cer_paymentterm_project";
			$values[] = $project_id;

			$fields[] = "cer_paymentterm_cer_version";
			$values[] = $version;
			
			$fields[] = "cer_paymentterm_year";
			$values[] = dbquote($year);
			
			$fields[] = "date_created";
			$values[] = "now()";

			$fields[] = "date_modified";
			$values[] = "now()";

			$fields[] = "user_created";
			$values[] = dbquote(user_login());

			$fields[] = "user_modified";
			$values[] = dbquote(user_login());

			$sql = "insert into cer_paymentterms (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			mysql_query($sql) or dberror($sql);
		}
	}

	//delete records not enclosed in the business plan period
	$sql = "select distinct cer_paymentterm_year " .
		   "from cer_paymentterms " .
		   " where cer_paymentterm_cer_version = " . $version . " and cer_paymentterm_project = " . $project_id;

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		if(!in_array($row["cer_paymentterm_year"], $existing_years))
		{
			$sql = "delete from cer_paymentterms " . 
			   "where cer_paymentterm_cer_version = " . $version . " and cer_paymentterm_year = " . $row["cer_paymentterm_year"] . 
			   " and cer_paymentterm_project = " . $project_id;

			$result = mysql_query($sql) or dberror($sql);
		}
	
	}

	return true;
}

/********************************************************************
    get id of cer_summary
*********************************************************************/
function get_id_of_cer_summary($pid, $version = 0)
{
	$sql = "select cer_summary_id from cer_summary where cer_summary_cer_version = " . $version . " and cer_summary_project = " . $pid;
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	return $row["cer_summary_id"];
}


/********************************************************************
    check if cer_summary exists
*********************************************************************/
function check_cer_summary($pid, $version = 0)
{
	$sql = "select count(cer_summary_id) as num_recs " . 
       "from cer_summary " . 
	   "where cer_summary_cer_version = " . $version . " and  cer_summary_project = " . $pid;

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	if ($row["num_recs"] == 0)
	{
		//create cer_revenue records
	
		$fields = array();
		$values = array();

		$fields[] = "cer_summary_project";
		$values[] = param("pid");

		$fields[] = "cer_summary_cer_version";
		$values[] = $version;

		$fields[] = "date_created";
		$values[] = "now()";

		$fields[] = "date_modified";
		$values[] = "now()";

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$fields[] = "user_modified";
		$values[] = dbquote(user_login());

		$sql = "insert into cer_summary (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
		
		mysql_query($sql) or dberror($sql);

	}

	return true;

}

/********************************************************************
    get the field values of an address
*********************************************************************/
function get_address($id)
{
    $address = array();

    if ($id == '')
    {
            $address["id"] = 0;
			$address["shortcut"] = "";
            $address["company"] = "";
			$address["legal_entity_name"] = "";
            $address["company2"] = "";
            $address["address"] = "";
            $address["address2"] = "";
            $address["zip"] = "";
            $address["place"] = "";
            $address["country"] = "";
            $address["country_name"] = "";
            $address["currency"] = "";
            $address["phone"] = "";
            $address["fax"] = "";
            $address["email"] = "";
            $address["contact"] = "";
            $address["client_type"] = "";
			$address["legal_entity"] = "";
			$address["retailer"] = 0;
			$address["contact_name"] = "";
			$address["address_is_independent_retailer"] = 0;
			$address["address_company_is_partner_since"] = 0;
			$address["address_is_hq_agent"] = 0;
			$address["address_sapnr"] = '';
			
			
    }
    else
    {
        $sql = "select * from addresses " . 
		       " left join places on place_id = address_place_id " . 
			   " where address_id = " . $id;
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $address["id"] = $row["address_id"];
			$address["shortcut"] = $row["address_shortcut"];
			$address["legal_entity_name"] = $row["address_legal_entity_name"];
            $address["company"] = $row["address_company"];
            $address["company2"] = $row["address_company2"];
            $address["address"] = $row["address_address"];
            $address["address2"] = $row["address_address2"];
            $address["zip"] = $row["address_zip"];
            $address["place"] = $row["place_name"];
            $address["country"] = $row["address_country"];
            $address["country_name"] = "";
            $address["currency"] = $row["address_currency"];
            $address["phone"] = $row["address_phone"];
            $address["fax"] = $row["address_fax"];
            $address["email"] = $row["address_email"];
            $address["contact"] = $row["address_contact"];
            $address["client_type"] = $row["address_client_type"];
			$address["legal_entity"] = $row["address_legal_entity_name"];
			$address["retailer"] = $row["address_is_independent_retailer"];
			$address["contact_name"] = $row["address_contact_name"];
			$address["address_is_independent_retailer"] = $row["address_is_independent_retailer"];
			$address["address_company_is_partner_since"] = $row["address_company_is_partner_since"];
			$address["address_is_hq_agent"] = $row["address_is_hq_agent"];
			$address["address_sapnr"] = $row["address_sapnr"];

            $sql = "select country_id, country_name ".
                   "from countries ".
                   "where country_id = " . dbquote($address["country"]);

            $res = mysql_query($sql);
            if ($res)
            {
                $row = mysql_fetch_assoc($res);
                $address["country_name"] = $row['country_name'];
            }

        }
    }
    return $address;
}


/********************************************************************
    get the field values of an address
*********************************************************************/
function get_address_users($address_id, $rtc_id, $rto_id, $rtcl_id, $hqpm_id, $client_user, $design_supervisor)
{

	//get country
	$country_id = 0;
	$sql = "select address_country from addresses " . 
		   "where address_id = ". $address_id;
	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$country_id = $row["address_country"];
	}
	
	$uids = array();
	$users = array();
	
	/*
	$sql = "select user_id, concat(user_name , ' ', user_firstname) as user_name, " . 
		   "user_email, address_company " .
		   "from users " .
		   "left join addresses on address_id = user_address " .
		   "where user_address = " . $address_id . 
		   " and user_active = 1 ";
	
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		if(!in_array($row["user_id"], $uids))
		{
		
			$user = array();
			$user["name"] = $row["user_name"] . ' (' . $row["address_company"] . ')';
			$user["email"] = strtolower($row["user_email"]);
			$user["roles"] = "";
			
			
			$sql = "select role_name " . 
				   "from user_roles " . 
				   "left join roles on role_id = user_role_role " . 
				   "where user_role_user = " . $row["user_id"] . 
				   " and (role_id = 4 " .
				   " or role_id = 10 " .
				   " or role_id = 15 " .
				   " or role_id = 16 " .
			       " or role_id = 33)";;
			
			$res1 = mysql_query($sql) or dberror($sql);
			while ($row1 = mysql_fetch_assoc($res1))
			{
				$user["roles"] = $user["roles"] . $row1["role_name"] . ", ";
			}
			
			$user["roles"] = substr($user["roles"], 0, strlen($user["roles"])-2);

			if($user["roles"])
			{
				$users[] = $user;
				$uids[] = $row["user_id"];
			}
		}
	}
	*/
	

	//get retail coordinator, operator, client hq-pm
	$sql = "select user_id, concat(user_name , ' ', user_firstname) as user_name, " . 
		   "user_email, address_company " .
		   "from users " .
		   "left join addresses on address_id = user_address " .
		   "where user_id = " . dbquote($rtc_id) .
		   " or user_id = " . dbquote($rto_id) .
		   " or user_id = " . dbquote($rtcl_id) .
		   " or user_id = " . dbquote($client_user) .
		   " or user_id = " . dbquote($hqpm_id);
	
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		if(!in_array($row["user_id"], $uids))
		{
			$user = array();
			$user["name"] = $row["user_name"] . ' (' . $row["address_company"] . ')';
			$user["email"] = strtolower($row["user_email"]);
			$user["roles"] = "";
			$user["user_id"] = $row["user_id"];
			
			$sql = "select role_name " . 
				   "from user_roles " . 
				   "left join roles on role_id = user_role_role " . 
				   "where user_role_user = " . $row["user_id"] . 
				   " and role_id IN(2, 4, 3, 10, 15, 16)";
			
			$res1 = mysql_query($sql) or dberror($sql);
			while ($row1 = mysql_fetch_assoc($res1))
			{
				$user["roles"] = $user["roles"] . $row1["role_name"] . ", ";
			}
			
			$user["roles"] = substr($user["roles"], 0, strlen($user["roles"])-2);

			if($user["roles"])
			{
				$users[] = $user;
				$uids[] = $row["user_id"];
			}
		}
	}


	//get design supervisor

	$sql = "select user_id, concat(user_name , ' ', user_firstname) as user_name, " . 
		   "user_email, address_company " .
		   "from user_roles " .
		   "left join users on user_id = user_role_user " . 
		   "left join addresses on address_id = user_address " . 
		   "where user_id = " . dbquote($design_supervisor) . " and user_active = 1";
	
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{

		if(!in_array($row["user_id"], $uids))
		{
			$user = array();
			$user["name"] = $row["user_name"] . ' (' . $row["address_company"] . ')';
			$user["email"] = strtolower($row["user_email"]);
			$user["roles"] = "";
			$user["user_id"] = $row["user_id"];
			
			$sql = "select role_name " . 
				   "from user_roles " . 
				   "left join roles on role_id = user_role_role " . 
				   "where user_role_user = " . $row["user_id"];
			
			$res1 = mysql_query($sql) or dberror($sql);
			while ($row1 = mysql_fetch_assoc($res1))
			{
				$user["roles"] = $user["roles"] . $row1["role_name"] . ", ";
			}
			
			$user["roles"] = substr($user["roles"], 0, strlen($user["roles"])-2);

			if($user["roles"])
			{
				$users[] = $user;
				$uids[] = $row["user_id"];
				
			}
		}
	}

	


	//get other persons
	$sql = "select user_id, concat(user_name , ' ', user_firstname) as user_name, " . 
		   "user_email, address_company " .
		   "from users " .
		   "left join addresses on address_id = user_address " . 
		   "where user_cer_reciepient = 1 and user_active = 1 and address_country = " . $country_id;

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		if(!in_array($row["user_id"], $uids))
		{
			$user = array();
			$user["name"] = $row["user_name"] . ' (' . $row["address_company"] . ')';
			$user["email"] = strtolower($row["user_email"]);
			$user["roles"] = "";
			$user["user_id"] = $row["user_id"];
			
			$sql = "select role_name " . 
				   "from user_roles " . 
				   "left join roles on role_id = user_role_role " . 
				   "where role_id < 30 and user_role_user = " . $row["user_id"];
			
			$res1 = mysql_query($sql) or dberror($sql);
			while ($row1 = mysql_fetch_assoc($res1))
			{
				$user["roles"] = $user["roles"] . $row1["role_name"] . ", ";
			}
			
			$user["roles"] = substr($user["roles"], 0, strlen($user["roles"])-2);

			if($user["roles"])
			{
				$users[] = $user;
				$uids[] = $row["user_id"];
			}
		}
	}

	//get finance controlles

	$sql = "select user_id, concat(user_name , ' ', user_firstname) as user_name, " . 
		   "user_email, address_company " .
		   "from user_roles " .
		   "left join users on user_id = user_role_user " . 
		   "left join addresses on address_id = user_address " . 
		   "where user_role_role = 22 and user_active = 1";
	
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		if(!in_array($row["user_id"], $uids))
		{
			$user = array();
			$user["name"] = $row["user_name"] . ' (' . $row["address_company"] . ')';
			$user["email"] = strtolower($row["user_email"]);
			$user["roles"] = "";
			$user["user_id"] = $row["user_id"];
			
			$sql = "select role_name " . 
				   "from user_roles " . 
				   "left join roles on role_id = user_role_role " . 
				   "where user_role_user = " . $row["user_id"] . 
				   " and role_id = 22";
			
			$res1 = mysql_query($sql) or dberror($sql);
			while ($row1 = mysql_fetch_assoc($res1))
			{
				$user["roles"] = $user["roles"] . $row1["role_name"] . ", ";
			}
			
			$user["roles"] = substr($user["roles"], 0, strlen($user["roles"])-2);

			if($user["roles"])
			{
				$users[] = $user;
				$uids[] = $row["user_id"];
			}
		}
	}


	


	return $users;
}

/********************************************************************
    get the field values of a user
*********************************************************************/
function get_user($id)
{
    $user = array();

   if ($id == '' or $id == 0)
   {
        $user["id"] = 0;
		$user["firstname"] = "";
        $user["name"] = "";
        $user["contact"] = "";
        $user["phone"] = "";
        $user["fax"] = "";
        $user["email"] = "";
        $user["cc"] = "";
        $user["deputy"] = "";
        $user["address"] = "";
		$user["country"] = "";
    }
    else
    {
        $sql = "select * from users left join addresses on address_id = user_address where user_id = " . $id;
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $user["id"] = $id;
			$user["firstname"] = $row["user_firstname"];
            $user["name"] = $row["user_name"];
            $user["contact"] = $row["user_name"] . " " . $row["user_firstname"];
            $user["phone"] = $row["user_phone"];
            $user["fax"] = $row["user_fax"];
            $user["email"] = $row["user_email"];
            $user["cc"] = $row["user_email_cc"];
            $user["deputy"] = $row["user_email_deputy"];
            $user["address"] = $row["user_address"];
			$user["country"] = $row["address_country"];
        }
    }
    return $user;
}


/********************************************************************
    get cer currency
*********************************************************************/
function get_cer_currency($pid, $version = 0)
{

	$currency = array();
    
	$sql = "select * from cer_basicdata " . 
		   "left join currencies on currency_id = cer_basicdata_currency " . 
		   "where cer_basicdata_version = " . $version . " and cer_basicdata_project = " . $pid;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$currency["id"] = $row["cer_basicdata_currency"];
		$currency["symbol"] = $row["currency_symbol"];
		$currency["exchange_rate"] = $row["cer_basicdata_exchangerate"];
		$currency["factor"] = $row["cer_basicdata_factor"];
	}

    else
    {
        $currency["id"] = 0;
        $currency["symbol"] = "";
        $currency["exchange_rate"] = 0;
        $currency["factor"] = 0;
    }
    
    return $currency;
}


/********************************************************************
    get ln currency
*********************************************************************/
function get_ln_currency($pid = 0, $version = 0)
{
    $currency = array();
    
	$sql = "select * from ln_basicdata " . 
		   "left join currencies on currency_id = ln_basicdata_currency " . 
		   "where ln_basicdata_version = " . $version . " and ln_basicdata_project = " . $pid;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$currency["id"] = $row["ln_basicdata_currency"];
		$currency["symbol"] = $row["currency_symbol"];
		$currency["exchange_rate"] = $row["ln_basicdata_exchangerate"];
		$currency["factor"] = $row["ln_basicdata_factor"];
	}

    else
    {
        $currency["id"] = 0;
        $currency["symbol"] = "";
        $currency["exchange_rate"] = 0;
        $currency["factor"] = 0;
    }
    
    return $currency;
}

/********************************************************************
    get project and order data 
*********************************************************************/
function get_project($id)
{
    $project = array();

    $sql = $sql = "select * " . 
       "from projects " . 
	   "left join orders on order_id = project_order " .
	   "left join countries on country_id = order_shop_address_country " .
	   "left join product_lines on product_line_id = project_product_line " .
	   "left join productline_subclasses on productline_subclass_id = project_product_line_subclass " .
	   "left join postypes on postype_id = project_postype ".
	   "left join possubclasses on possubclass_id = project_pos_subclass " .
	   "left join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join projectkinds on projectkind_id = project_projectkind " .
	   "left join project_states on project_state_id = project_state ".
       "left join addresses on address_id = order_client_address " .
	   "left join currencies on currency_id = country_currency " . 
       "left join users on user_id = project_retail_coordinator " .
	   "where project_id = " . $id;


    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $project = $row;

		$sql1 = "select user_name, user_firstname from users " . 
			   "where user_id = " . $row["order_user"];

		$res1 = mysql_query($sql1) or dberror($sql1);

		if ($row1 = mysql_fetch_assoc($res1))
		{
			$project["submitted_by"] = $row1["user_name"] . " " . $row1["user_firstname"];
		}
		else
		{
			$project["submitted_by"] = "";
		}
    }
	

	$project["project_manager"] = "";
	$sql = "select concat(user_name, ' ', user_firstname) as username from users where user_id =" . dbquote($project["project_retail_coordinator"]);
	$res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $project["project_manager"] = $row["username"];
    }
	
	$project["operator"] = "";
	$sql = "select concat(user_name, ' ', user_firstname) as username from users where user_id =" . dbquote($project["order_retail_operator"]);
	$res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $project["operator"] = $row["username"];
    }

	
	$project["order_franchisee_address_country_name"] = "";
	$sql = "select country_name ".
           "from countries ".
           "where country_id  = " . dbquote($project["order_franchisee_address_country"]);

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $project["order_franchisee_address_country_name"] = $row["country_name"];
    }

    
	$project["order_shop_address_country_name"] = "";

	$sql = "select country_name ".
           "from countries ".
           "where country_id  = " . dbquote($project["order_shop_address_country"]);

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $project["order_shop_address_country_name"] = $row["country_name"];
    }

    $sql = "select country_name ".
           "from countries ".
           "where country_id  = " . dbquote($project["order_billing_address_country"]);

    $res = mysql_query($sql) or dberror($sql);

	$project["order_billing_address_country_name"] = "";
    if ($row = mysql_fetch_assoc($res))
    {
        $project["order_billing_address_country_name"] = $row["country_name"];
    }
	
	//get pos address from POS Index
	$sql = "select posorder_posaddress from posorders where posorder_order = " . $project["order_id"];
	$res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $project["posaddress_id"] = $row["posorder_posaddress"];
		$project["pipeline"] = 0;
		
    }
	else
	{
		
		$sql = "select posorder_posaddress from posorderspipeline where posorder_order = " . $project["order_id"];
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$project["posaddress_id"] = $row["posorder_posaddress"];
			$project["pipeline"] = 1;
		}
		else
		{
			$project["posaddress_id"] = "";
			$project["pipeline"] = 0;
		}
	}

	//postypes CER/AF
	$project["needs_cer"] = 0;
	$project["needs_af"] = 0;
	$project["needs_inr03"] = 0;


	$sql = "select posproject_type_needs_cer, posproject_type_needs_af, posproject_type_needs_inr03 " . 
		   "from posproject_types " . 
	       "where posproject_type_postype = " . $project["project_postype"] .  
		   " and posproject_type_projectcosttype = " . $project["project_cost_type"] . 
		   " and posproject_type_projectkind = " . $project["project_projectkind"];

	$res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $project["needs_cer"] = $row["posproject_type_needs_cer"];
		$project["needs_af"] = $row["posproject_type_needs_af"];
		$project["needs_inr03"] = $row["posproject_type_needs_inr03"];
    }
    

    return $project;
}

/********************************************************************
    get posorder
*********************************************************************/
function get_posorder_id($order_id)
{
	$posorder = array();

	//check if project is in pipeline
	$sql = "select *  " . 
		   "from posorderspipeline " . 
	       "where posorder_order = " . $order_id;

	$res = mysql_query($sql) or dberror($sql);
	
	if($row = mysql_fetch_assoc($res))
	{
		$posorder = $row;
	}
	else
	{
		$sql = "select *  " . 
		   "from posorders " . 
	       "where posorder_order = " . $order_id;

		$res = mysql_query($sql) or dberror($sql);
		
		if($row = mysql_fetch_assoc($res))
		{
			$posorder = $row;
		}
	
	}

	return $posorder;
}

/********************************************************************
    get pos data
*********************************************************************/
function get_pos_data($order_id)
{
	$posdata = array();
	$project_is_in_pipeline = 0;

	//check if project is in pipeline
	$sql = "select count(posorder_id) as num_recs " . 
		   "from posorderspipeline " . 
	       "where posorder_order = " . $order_id;

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	if ($row["num_recs"] > 0)
	{
		$project_is_in_pipeline = 1;
	}

	if($project_is_in_pipeline == 1)
	{
		$parent_table = "";
		$sql = "select * from posorderspipeline " . 
			   "where posorder_order = " . $order_id;


		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$parent_table = $row["posorder_parent_table"];
		}
		
		$sql = "select * from posorderspipeline " . 
			   "left join " . $parent_table . " on posaddress_id = posorder_posaddress " . 
			   "where posorder_order = " . $order_id;

		
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			
			/*
			if(!$row["posaddress_id"])
			{
				$sql = "select * from posorderspipeline " . 
					   "left join posaddresses on posaddress_id = posorder_posaddress " . 
					   "where posorder_order = " . $order_id;
				$res = mysql_query($sql) or dberror($sql);

				if ($row = mysql_fetch_assoc($res))
				{
					$posdata = $row;
					$posdata["table"] = "posaddresses";
					$posdata["table_leases"] = "posleasepipeline";
				}
			}
			else
			{
				$posdata = $row;
				$posdata["table"] = "posaddressespipeline";
				$posdata["table_leases"] = "posleasepipeline";
			}
			*/

			$posdata = $row;
			$posdata["table"] = $parent_table;
			$posdata["table_leases"] = "posleasepipeline";
			
		}
		
		if(count($posdata) > 0)
		{

			$areas = "";
			$sql = "select * from posareaspipeline " .
				   "left join posareatypes on posareatype_id = posarea_area " . 
				   "where posarea_posaddress = " . dbquote($posdata["posaddress_id"]);
			
			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{
				$areas .= $row["posareatype_name"] . ", ";
			}
			
			$areas = substr($areas, 0, strlen($areas)-2);
			$posdata["posareas"] = $areas;
		}
	}
	else
	{
		$sql = "select * from posorders " . 
			   "left join posaddresses on posaddress_id = posorder_posaddress " . 
			   "where posorder_order = " . $order_id;

		
		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$posdata = $row;
			$posdata["table"] = "posaddresses";
			$posdata["table_leases"] = "posleases";
		}

		if(count($posdata) > 0)
		{
			$areas = "";
			$sql = "select * from posareas " .
				   "left join posareatypes on posareatype_id = posarea_area " . 
				   "where posarea_posaddress = " . dbquote($posdata["posaddress_id"]);
			
			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{
				$areas .= $row["posareatype_name"] . ", ";
			}

			$areas = substr($areas, 0, strlen($areas)-2);

			$posdata["posareas"] = $areas;
		}

	}
	
	if(count($posdata) > 0)
	{
		$sql = "select country_id, country_name, currency_symbol ".
			   "from countries ".
			   " left join currencies on currency_id = country_currency " . 
			   "where country_id = " . dbquote($posdata["posaddress_country"]);

		$res = mysql_query($sql);
		if ($res)
		{
			$row = mysql_fetch_assoc($res);
			$posdata["country_name"] = $row['country_name'];
			$posdata["currency_symbol"] = $row['currency_symbol'];
		}
	}

	return $posdata;
}

/********************************************************************
    get pos lease data
*********************************************************************/
function get_pos_leasedata($posaddress_id, $project_order = 0 )
{
	$posleasedata = array();
	$project_is_in_pipeline = 0;

	//check if project is in pipeline
	$sql = "select count(posorder_id) as num_recs " . 
		   "from posorderspipeline " . 
	       "where posorder_order = " . $project_order;

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	if ($row["num_recs"] > 0)
	{
		$project_is_in_pipeline = 1;
	}

	if($project_is_in_pipeline == 1)
	{
		$sql = "select * from posleasespipeline " . 
			   " left join poslease_types on poslease_type_id = poslease_lease_type " .
			   " where poslease_posaddress = " . dbquote($posaddress_id) .
			   " and poslease_order = " . dbquote($project_order) .
			   " order by poslease_startdate DESC ";

		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$posleasedata = $row;
			$posleasedata["table"] = "posleasespipeline";
		}
	}
	else
	{
		$sql = "select * from posleases " .
			   "left join poslease_types on poslease_type_id = poslease_lease_type " .
			   "where poslease_posaddress = " . dbquote($posaddress_id) .
			   " and poslease_order = " . dbquote($project_order) .
			   " order by poslease_startdate DESC ";

		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$posleasedata = $row;
			$posleasedata["table"] = "posleases";
		}
	
	}



	$rental_duration = "";
	if(count($posleasedata) > 0)
	{
		if($posleasedata["poslease_startdate"] != NULL 
			and $posleasedata["poslease_startdate"] != '0000-00-00' 
			and $posleasedata["poslease_enddate"] != NULL
			and $posleasedata["poslease_enddate"] != '0000-00-00')
		{
			$diff = abs(strtotime($posleasedata["poslease_enddate"]) - strtotime($posleasedata["poslease_startdate"]));
			$years = floor($diff / (365*60*60*24));
			$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));

			if($months == 12)
			{
				$months = 0;
				$years++;
			}

			$rental_duration = $years . " years and " . $months . " months";
		}
	}

	if(count($posleasedata) > 0)
	{
		$posleasedata["rental_duration"] = $rental_duration;
	}

	//latest rental review date
	$sql = "select poslease_rent_review_date from posleases " .
		   "where poslease_posaddress = " . dbquote($posaddress_id) .
		   " and poslease_rent_review_date is not null " . 
		   "  and poslease_rent_review_date <> '0000-00-00' " .
		   " order by poslease_rent_review_date DESC ";

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res)
		and count($posleasedata) > 0)
	{
		$posleasedata["rent_review_date"] = to_system_date($row['poslease_rent_review_date']);
	}

	return $posleasedata;
}


/********************************************************************
    get intangibles
*********************************************************************/
function get_pos_intangibles($project_id, $investment_type, $version = 0)
{
	$posintangibles = array();
	
	$sql = "select * from cer_investments " .
		   "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
		   "where cer_investment_cer_version = " . $version . " and cer_investment_project = " . $project_id .
		   " and cer_investment_type = $investment_type ";

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$posintangibles = $row;
	}
	return $posintangibles;
}


/********************************************************************
    get the address of an order specified by an address_type
*********************************************************************/
function get_order_address($address_type, $order_id)
{
    $order_address = array();

    $sql = "select * ".
           "from order_addresses ".
           "where order_address_order  = " . $order_id . " and order_address_type = " . $address_type;

	$res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $order_address["id"] = $row["order_address_id"];
        $order_address["company"] = $row["order_address_company"];
        $order_address["company2"] = $row["order_address_company2"];
        $order_address["address"] = $row["order_address_address"];
        $order_address["address2"] = $row["order_address_address2"];
        $order_address["zip"] = $row["order_address_zip"];
        $order_address["place"] = $row["order_address_place"];
        $order_address["country"] = $row["order_address_country"];
        $order_address["country_name"] = "";
        $order_address["phone"] = $row["order_address_phone"];
        $order_address["fax"] = $row["order_address_fax"];
        $order_address["email"] = $row["order_address_email"];
        $order_address["contact"] = $row["order_address_contact"];


        $sql = "select country_id, country_name ".
               "from countries ".
               "where country_id = " . dbquote($order_address["country"]);

        $res = mysql_query($sql);
        if ($res)
        {
           $row = mysql_fetch_assoc($res);
           $order_address["country_name"] = $row['country_name'];
        }

    }
	else
	{
		$order_address["id"] = "";
        $order_address["company"] = "";
        $order_address["company2"] = "";
        $order_address["address"] = "";
        $order_address["address2"] = "";
        $order_address["zip"] = "";
        $order_address["place"] = "";
        $order_address["country"] = "";
        $order_address["country_name"] = "";
        $order_address["phone"] = "";
        $order_address["fax"] = "";
        $order_address["email"] = "";
        $order_address["contact"] = "";
	
	}

    return $order_address;
}


/*************************************************************************
   get actual order state name
**************************************************************************/
function get_actual_order_state_name($code, $type)
{
    $name = "";

    $sql = "select order_state_name ".
           "from order_states ".
           "left join order_state_groups on order_state_group_id = order_state_group ".
           "where order_state_group_order_type = " . $type .
           "   and order_state_code = '" . $code . "'";

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $name = $row["order_state_name"];
    }

    return $name;
}

/*************************************************************************
   get project state name
**************************************************************************/
function get_project_state_name($id)
{
    $name = "";

    $sql = "select project_state_text ".
           "from project_states ".
           "where project_state_id = " . $id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $name = $row["project_state_text"];
    }

    return $name;
}


/********************************************************************
    calculate inflated amounts
*********************************************************************/
function get_inflation_rate($project_id, $year, $version = 0)
{
	$irate = 0;
	//get inflation rates
	$inflationrates = array();
	$sql = "select cer_basicdata_inflationrates, cer_basicdata_firstyear, cer_basicdata_lastyear " .
		   "from cer_basicdata " . 
		   "where cer_basicdata_version = " . $version . " and cer_basicdata_project = " . $project_id;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$cer_basicdata_inflation_rates = unserialize($row["cer_basicdata_inflationrates"]);
		
		//get the first matching inflation rate
		$inflation_rate_tmp = 0;
		foreach($cer_basicdata_inflation_rates as $year=>$rate)
		{
			if($row["cer_basicdata_firstyear"] > $year)
			{
				$inflation_rate_tmp = $rate;
			}
			elseif($row["cer_basicdata_firstyear"] == $year)
			{
				$inflation_rate_tmp = $cer_basicdata_inflation_rates[$row["cer_basicdata_firstyear"]];
			}
		
		}
		/*
		if(!array_key_exists($row["cer_basicdata_firstyear"],$cer_basicdata_inflation_rates))
		{
			$inflation_rate_tmp = $cer_basicdata_inflation_rates[$row["cer_basicdata_firstyear"]];
		}
		else
		{
			$inflation_rate_tmp = 0;
		}
		*/

		for($y=$row["cer_basicdata_firstyear"];$y<=$row["cer_basicdata_lastyear"];$y++)
		{
			$years[] = $y;

			if(!array_key_exists($y,$cer_basicdata_inflation_rates))
			{
				$cer_basicdata_inflation_rates[$y] = $inflation_rate_tmp;
			}
			else
			{
				$inflation_rate_tmp = $cer_basicdata_inflation_rates[$y];
			}
		}
	}
	
		
	return $cer_basicdata_inflation_rates[$year];
}

/********************************************************************
    calculate inflated amounts
*********************************************************************/
function calculate_inflated_amounts($project_id, $amounts, $starting_year, $starting_month, $version = 0)
{
	$amount_base = 0;
	foreach($amounts as $year=>$amount)
	{
		
		if($amount_base == 0)
		{
			if($year == $starting_year)
			{
				//calculate amount for 12 Months
				$amount_base = 12*$amount/(13-$starting_month);
			}
			else
			{
				$amount_base = $amount;
			}
		}

	
		if($starting_year < $year)
		{
			$irate = get_inflation_rate($project_id, $year, $version);
			$s = $amount_base + ($amount_base * $irate / 100);

			//echo $amount_base . ":" . $s . "<br>";
			$amounts[$year] = $s;
			$amount_base = $s;

			//echo "1Y: " . $year . " amount: " . $s . " irate: " . $irate . " amount base: " . $amount_base ."<br>";
		}
		else
		{
			//$irate = get_inflation_rate($project_id, $year, $version);
			$irate = 0;
			$number_of_months = 13 - $starting_month;
			$s = $number_of_months*$amount_base/12;
			//$s = $s + ($s * $irate / 100);
			$amounts[$year] = $s;
			
			$sn = $amount_base + ($amount_base * $irate / 100);
			$amount_base = $sn;

			
		}
		//echo "2Y: " . $year . " amount: " . $s . " irate: " . $irate . " amount base: " . $amount_base ."<br>";
	
	}

	return $amounts;
}


/********************************************************************
    calculate fixed growing amounts
*********************************************************************/
function calculate_fixed_growth_amounts($amounts, $starting_year, $starting_month, $percentage)
{
	$amount_base = 0;
	foreach($amounts as $year=>$amount)
	{
		
		if($amount_base == 0)
		{
			if($year == $starting_year)
			{
				//calculate amount for 12 Months
				$amount_base = 12*$amount/(13-$starting_month);
			}
			else
			{
				$amount_base = $amount;
			}
		}

	
		if($starting_year < $year)
		{
			$irate = $percentage;
			$s = $amount_base + ($amount_base * $irate / 100);
			$amounts[$year] = $s;
			$amount_base = $s;
		}
		else
		{
			$irate = 0;
			$number_of_months = 13 - $starting_month;
			$s = $number_of_months*$amount_base/12;
			$amounts[$year] = $s;
			
			$sn = $amount_base + ($amount_base * $irate / 100);
			$amount_base = $sn;

			
		}
	}

	return $amounts;
}


/********************************************************************
    calculate forcasted salaries
*********************************************************************/
function calculate_forcasted_salaries($project_id, $years, $country, $version = 0)
{
	$cer_basicdata = get_cer_basicdata($project_id, $version);
	$salaries = array();
	$sql = "select * from cer_salaries where cer_salary_cer_version = " . $version . " and cer_salary_project = " . $project_id;
	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$salaries = array();
		foreach($years as $key=>$year)
		{
			$salaries[$year] = 0;
		}
		
		foreach($salaries as $year=>$value)
		{
			if($row["cer_salary_year_starting"] < $year)
			{
				$s = $row["cer_salary_total"] * $row["cer_salary_headcount_percent"] / 100;
				$s = $s + $salaries[$year];
				$salaries[$year] = $s;
			}
			elseif($row["cer_salary_year_starting"] == $year) // only a part of the year
			{
				$s = $row["cer_salary_total"] * $row["cer_salary_headcount_percent"] / 100;
				$s = ($s / 12 ) * (13 - $row["cer_salary_month_starting"]);
				$s = $s + $salaries[$year];
				$salaries[$year] = $s;

			}
		}

		if($cer_basicdata["cer_basicdata_salary_growth"] > 0)
		{
			//fixed growth
			$salaries = calculate_fixed_growth_amounts($salaries, $row["cer_salary_year_starting"], $row["cer_salary_month_starting"], $cer_basicdata["cer_basicdata_salary_growth"]);
		}
		else
		{
			//calculate inflation
			//$salaries = calculate_inflated_amounts($project_id, $salaries, $row["cer_salary_year_starting"], $row["cer_salary_month_starting"], $version);
		}

		//last year
		$salaries[$cer_basicdata["cer_basicdata_lastyear"]] = $salaries[$cer_basicdata["cer_basicdata_lastyear"]]/ 12 * $cer_basicdata["cer_basicdata_lastmonth"];

		$salary_rows[] = $salaries;

	}

	if(count($salaries) > 0)
	{
		// add all salary rows
		$salaries = array();
		foreach($salary_rows as $key=>$salary_row)
		{
			foreach($salary_row as $year=>$salary)
			{
				if(array_key_exists($year, $salaries))
				{
					$s = $salaries[$year] + $salary;
					$salaries[$year] = $s;
				}
				else
				{
					$salaries[$year] = $salary;
				}
			}
		}

	
		//get the starting year an month
		
		$year_starting = 0;
		$month_starting = 0;

		$sql = "select min(cer_salary_year_starting) as firstyear " .
			   "from cer_salaries " . 
			   "where cer_salary_cer_version = " . $version . " and cer_salary_project = " . $project_id;

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$year_starting = $row["firstyear"];
		}

		$sql = "select min(cer_salary_month_starting) as firstmonth " . 
			   "from cer_salaries " . 
			   "where cer_salary_cer_version = " . $version . " and cer_salary_project = " . $project_id . 
			   " and cer_salary_year_starting = " . $year_starting;
		
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$month_starting = $row["firstmonth"];
		}


		// update expenses with salaries

		$i = 1;
		foreach($salaries as $year=>$value)
		{
			$sql = "update cer_expenses SET " . 
			       "cer_expense_amount = " . dbquote(round($value,0)) . 
				   " where cer_expense_cer_version = " . $version . " and cer_expense_project = " . $project_id . 
			       " and cer_expense_type = 1 " .
			       " and cer_expense_year =  " . dbquote($year);

			$result = mysql_query($sql) or dberror($sql);
		}
	}

	return true;
}


/********************************************************************
    calculate forcasted amounts
*********************************************************************/
function calculate_forcasted_expenses($expense_id, $years, $project_id, $version = 0)
{
	$sql = "select * from cer_expenses " . 
		   "where cer_expense_id = " . $expense_id;

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$amounts = array();

		foreach($years as $key=>$year)
		{
		
			$amounts[$year] = 0;

			if($row["cer_expense_year_starting"] < $year)
			{
				$s = $row["cer_expense_amount_per_year"];
				$s = $s + $amounts[$year];
				$amounts[$year] = $s;
			}
			elseif($row["cer_expense_year_starting"] == $year) // only a part of the year
			{
				$s = $row["cer_expense_amount_per_year"];
				$s = ($s / 12 ) * (13 - $row["cer_expense_month_starting"]);
				$s = $s + $amounts[$year];
				$amounts[$year] = $s;

			}
			//calculate inflation
			$amounts = calculate_inflated_amounts($project_id, $amounts, $row["cer_expense_year_starting"], $row["cer_expense_month_starting"], $version);
		}

	}

	if(count($amounts) > 0)
	{
		$sql = "update cer_expenses SET ";
	
		$i = 1;
		foreach($amounts as $year=>$value)
		{
			$sql .= "cer_expense_amount" . $i . " = " . dbquote($value) . ", cer_expense_year" . $i . " = " . dbquote($year);
			if($i < 5)
			{
				$sql .= ", ";
			}
			$i++;
		}



		$sql .= " where cer_expense_id = " . $expense_id;

		$result = mysql_query($sql) or dberror($sql);
	
	}

	return true;
	
}

/**********************************************************************************************************************
    get investments from the list of materials
**********************************************************************************************************************/
function update_investments_from_the_list_of_materials($project_id, $order_id, $country_id, $update_mode, $version = 0)
{
	$amounts = array();
	$comments = array();

	//get the local currency of shop
	$currency = get_cer_currency($project_id, $version);

	$sql = "select * from cer_investments " .
		   "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
		   "where cer_investment_cer_version = " . $version . " and cer_investment_project = " . $project_id;

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		
		if($row["posinvestment_type_cms_sumcostgroups"])
		{
			$group_total = 0;
			$cost_groups = explode(",", $row["posinvestment_type_cms_sumcostgroups"]);
			foreach($cost_groups as $key=>$cost_group)
			{
				$sql_i = "select sum(order_item_system_price*order_item_quantity) as total_in_system_currency " . 
						 "from order_items ".
						 "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
						 "   and order_item_order = " . $order_id . 
						 "   and order_item_cost_group =  " . dbquote($cost_group);
				
				$res_i = mysql_query($sql_i) or dberror($sql_i);
				if ($row_i = mysql_fetch_assoc($res_i))
				{
					
					if($currency["exchange_rate"] > 0)
					{
						$group_total = $group_total + ($row_i["total_in_system_currency"] / $currency["exchange_rate"])*$currency["factor"];
					}
				}

				if($update_mode == 1)
				{
					$amounts[$row["cer_investment_id"]] = $group_total;
					$comments[$row["cer_investment_id"]] = "inserted from the list of materials";
				}
				else
				{
					if($row["cer_investment_amount_cer_loc"] == 0)
					{
						$amounts[$row["cer_investment_id"]] = $group_total;
						$comments[$row["cer_investment_id"]] = "inserted from the list of materials";
					}
				}
			}
		}
	}
	
	foreach($amounts as $id=>$value)
	{
		if($value)
		{
			$fields = array();
		
			$value = dbquote($value);
			$fields[] = "cer_investment_amount_cer_loc = " . $value;

			$value = dbquote($comments[$id]);
			$fields[] = "cer_investment_comment = " . $value;

			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $id;

			mysql_query($sql) or dberror($sql);
		}

	}

	/*
	//update kl approved investments
	$sql = "update cer_investments set cer_investment_amount_cer_loc_approved = cer_investment_amount_cer_loc " . 
			"where cer_investment_amount_cer_loc_approved = 0 and cer_investment_type in(1, 3, 5, 7, 11) and cer_investment_project = " . $project_id;

	$result = mysql_query($sql) or dberror($sql);
	*/
}


/******************************************************************************************************************************
    get prroved investments from the list of materials
*******************************************************************************************************************************/
function update_approved_investments_from_the_list_of_materials($project_id, $order_id, $country_id, $update_mode, $version = 0)
{
	$amounts = array();
	$comments = array();

	//get the local currency of shop
	$currency = get_cer_currency($project_id, $version);

	$sql = "select * from cer_investments " .
		   "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
		   "where cer_investment_cer_version = " . $version . " and cer_investment_project = " . $project_id;

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		
		if($row["posinvestment_type_cms_sumcostgroups"])
		{
			$group_total = 0;
			$cost_groups = explode(",", $row["posinvestment_type_cms_sumcostgroups"]);

			foreach($cost_groups as $key=>$cost_group)
			{
				$sql_i = "select sum(order_item_system_price_freezed*order_item_quantity_freezed) as total_in_system_currency " . 
						 "from order_items ".
						 "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
						 "   and order_item_order = " . $order_id . 
						 "   and order_item_cost_group =  " . dbquote($cost_group);

				$res_i = mysql_query($sql_i) or dberror($sql_i);
				if ($row_i = mysql_fetch_assoc($res_i))
				{
					if($currency["exchange_rate"] > 0)
					{
						$group_total = $group_total + ($row_i["total_in_system_currency"] / $currency["exchange_rate"])*$currency["factor"];
					}
				}

				if($update_mode == 1)
				{
					$amounts[$row["cer_investment_id"]] = $group_total;
					$comments[$row["cer_investment_id"]] = "inserted from the list of materials";
				}
				else
				{
					if($row["cer_investment_amount_cer_loc"] == 0)
					{
						$amounts[$row["cer_investment_id"]] = $group_total;
						$comments[$row["cer_investment_id"]] = "inserted from the list of materials";
					}
				}
			}
		}
	}
	
	foreach($amounts as $id=>$value)
	{
		$fields = array();
    
		$value = dbquote($value);
		$fields[] = "cer_investment_amount_cer_loc = " . $value;

		$value = dbquote($comments[$id]);
		$fields[] = "cer_investment_comment = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $id;
		mysql_query($sql) or dberror($sql);

	}


	//update kl approved investments
	/*
	$sql = "update cer_investments set cer_investment_amount_cer_loc_approved = cer_investment_amount_cer_loc " . 
			"where cer_investment_cer_version = " . $version . " and cer_investment_amount_cer_loc_approved = 0 and cer_investment_type in(1, 3, 5, 7, 11) and cer_investment_project = " . $project_id;

	$result = mysql_query($sql) or dberror($sql);
	*/

}


/**************************************************************************************
    get investments from the project's cost sheet
**************************************************************************************/
function update_investments_from_project_cost_sheet($project_id, $budget, $version = 0)
{

	//get the local currency of shop
	$cer_currency = get_cer_currency($project_id, $version);


	$update_investments = false;
	$total = 0;
	foreach($budget["cer_group_totals_by_posinvestment_type_chf"] as $posinvestment_type_id=>$value)
	{
		$total = $total + $value;
	}

	if($total > 0) {
	
		$comment = "inserted from budget";	
		foreach($budget["cer_group_totals_by_posinvestment_type_chf"] as $posinvestment_type_id=>$value)
		{
			
			//echo $posinvestment_type_id . " " . $value . "<br />";

			
			if($cer_currency["exchange_rate"] > 0)
			{
				$value = ($value / $cer_currency["exchange_rate"])*$cer_currency["factor"];
			}
			else
			{
				$value = 0;
			}
			
				
			$fields = array();
			
			$fields[] = "cer_investment_amount_cer_loc = " . $value;

			$value = dbquote($comment);
			$fields[] = "cer_investment_comment = " . $value;

			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_cer_version = " . $version . " and cer_investment_project = " . $project_id. " and cer_investment_type = '" . $posinvestment_type_id . "'";

			//echo $sql . "<br />";

			mysql_query($sql) or dberror($sql);
		}
	}

	/*
	//update kl approved investments
	$sql = "update cer_investments set cer_investment_amount_cer_loc_approved = cer_investment_amount_cer_loc " . 
			"where cer_investment_cer_version = " . $version . " and cer_investment_amount_cer_loc_approved = 0 and cer_investment_type in(1, 3, 5, 7, 11) and cer_investment_project = " . $project_id;

	$result = mysql_query($sql) or dberror($sql);
	*/
}

/********************************************************************
    caclulate cost of products sold
*********************************************************************/
function update_cost_of_products_sold($project_id, $version = 0)
{
	
	//get all brands involved
	$brand_ids = array();
	$sql = "select DISTINCT cer_revenue_brand_id " . 
		   " from  cer_revenues " .
		   " where cer_revenue_cer_version = " . $version . 
		   " and cer_revenue_project = " . $project_id;


	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$brand_ids[$row["cer_revenue_brand_id"]] = $row["cer_revenue_brand_id"];
	}
	
	
	$years = array();
	$revenue_watches = array();
	$revenue_jewellery = array();
	$revenue_services = array();

	$cost_watches_values = array();
	$cost_sales_jewellery_values = array();
	$cost_sales_accessories_values = array();
	$cost_sales_customer_service_values = array();

	$cost_of_production = array();

	$cer_basicdata = get_cer_basicdata($project_id, $version);

	

	//get sales planning
	foreach($brand_ids as $key=>$brand_id)
	{
	
		$total_gross_sales_values = array();
		$total_net_sales_values = array();
		$gross_sales_watches_values = array();
		$gross_sales_jewellery_values = array();
		$gross_sales_customer_service_values = array();

		$net_sales_watches_values = array();
		$net_sales_jewellery_values = array();
		$net_sales_accessories_values = array();
		$net_sales_customer_service_values = array();

		

		
		$sql = "select * from cer_revenues " .
			   "where cer_revenue_cer_version = " . $version . " and cer_revenue_project = " . $project_id . 
			   " and cer_revenue_brand_id = " . $brand_id;


		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$year = $row["cer_revenue_year"];
			$years[$year] = $year;

			/*
			$sales_reduction_watches = $row["cer_revenue_sales_reduction_watches"] + $row["cer_revenue_reduction_credit_cards"];
			$sales_reduction_bijoux = $row["cer_revenue_sales_reduction_bijoux"] + $row["cer_revenue_reduction_credit_cards"];
			$sales_reduction_accessories = $row["cer_revenue_sales_reduction_accessories"] + $row["cer_revenue_reduction_credit_cards"];
			$sales_reduction_services = $row["cer_revenue_sales_reduction_cservice"] + $row["cer_revenue_reduction_credit_cards"];
			*/

			$sales_reduction_watches = $row["cer_revenue_sales_reduction_watches"];
			$sales_reduction_bijoux = $row["cer_revenue_sales_reduction_bijoux"];
			$sales_reduction_accessories = $row["cer_revenue_sales_reduction_accessories"];
			$sales_reduction_services = $row["cer_revenue_sales_reduction_cservice"];

			$cost_watches = $row["cer_revenue_cost_watches"] + $row["cer_revenue_reduction_credit_cards"];
			$cost_bijoux = $row["cer_revenue_cost_jewellery"] + $row["cer_revenue_reduction_credit_cards"];
			$cost_accessories = $row["cer_revenue_cost_accessories"] + $row["cer_revenue_reduction_credit_cards"];
			$cost_services = $row["cer_revenue_cost_service"] + $row["cer_revenue_reduction_credit_cards"];

			/*
			if(array_key_exists($year, $gross_sales_watches_values))
			{
				$gross_sales_watches_values[$year] = $gross_sales_watches_values[$year] + $row["cer_revenue_watches"];
				$gross_sales_jewellery_values[$year] = $gross_sales_jewellery_values[$year] + $row["cer_revenue_jewellery"];
				$gross_sales_accessories_values[$year] = $gross_sales_accessories_values[$year] + $row["cer_revenue_accessories"];
				$gross_sales_customer_service_values[$year] = $gross_sales_customer_service_values[$year] + $row["cer_revenue_customer_service"];

				$net_sales_watches_values[$year] = $net_sales_watches_values[$year] + $row["cer_revenue_watches"] - ($row["cer_revenue_watches"]*$sales_reduction_watches/100);
				$net_sales_jewellery_values[$year] = $net_sales_jewellery_values[$year] + $row["cer_revenue_jewellery"] - ($row["cer_revenue_jewellery"]*$sales_reduction_bijoux/100); 
				$net_sales_accessories_values[$year] = $net_sales_accessories_values[$year] + $row["cer_revenue_accessories"] - ($row["cer_revenue_accessories"]*$sales_reduction_accessories/100);
				$net_sales_customer_service_values[$year] = $net_sales_customer_service_values[$year] + $row["cer_revenue_customer_service"]- ($row["cer_revenue_customer_service"]*$sales_reduction_services/100);
			}
			else
			{
				$gross_sales_watches_values[$year] = $row["cer_revenue_watches"];
				$gross_sales_jewellery_values[$year] =$row["cer_revenue_jewellery"];
				$gross_sales_accessories_values[$year] = $row["cer_revenue_accessories"];
				$gross_sales_customer_service_values[$year] = $row["cer_revenue_customer_service"];

				$net_sales_watches_values[$year] = $row["cer_revenue_watches"] - ($row["cer_revenue_watches"]*$sales_reduction_watches/100);
				$net_sales_jewellery_values[$year] = $row["cer_revenue_jewellery"] - ($row["cer_revenue_jewellery"]*$sales_reduction_bijoux/100); 
				$net_sales_accessories_values[$year] = $row["cer_revenue_accessories"] - ($row["cer_revenue_accessories"]*$sales_reduction_accessories/100);
				$net_sales_customer_service_values[$year] = $row["cer_revenue_customer_service"]- ($row["cer_revenue_customer_service"]*$sales_reduction_services/100);
			}
			*/


			$gross_sales_watches_values[$year] = $row["cer_revenue_watches"];
			$gross_sales_jewellery_values[$year] =$row["cer_revenue_jewellery"];
			$gross_sales_accessories_values[$year] = $row["cer_revenue_accessories"];
			$gross_sales_customer_service_values[$year] = $row["cer_revenue_customer_service"];

			$net_sales_watches_values[$year] = $row["cer_revenue_watches"] - ($row["cer_revenue_watches"]*$sales_reduction_watches/100);
			$net_sales_jewellery_values[$year] = $row["cer_revenue_jewellery"] - ($row["cer_revenue_jewellery"]*$sales_reduction_bijoux/100); 
			$net_sales_accessories_values[$year] = $row["cer_revenue_accessories"] - ($row["cer_revenue_accessories"]*$sales_reduction_accessories/100);
			$net_sales_customer_service_values[$year] = $row["cer_revenue_customer_service"]- ($row["cer_revenue_customer_service"]*$sales_reduction_services/100);

						
			if(array_key_exists($year, $cost_watches_values))
			{
				if($cer_basicdata["cer_basicdata_cost_on_net_sales"] == 1)
				{
					$cost_watches_values[$year] =  $cost_watches_values[$year] + ($net_sales_watches_values[$year] * $cost_watches / 100);

					$cost_sales_jewellery_values[$year] = $cost_sales_jewellery_values[$year] + ($net_sales_jewellery_values[$year] * $cost_bijoux / 100);

					$cost_sales_accessories_values[$year] = $cost_sales_accessories_values[$year] + ($net_sales_accessories_values[$year] * $cost_accessories / 100);

					$cost_sales_customer_service_values[$year] = $cost_sales_customer_service_values[$year] + ($net_sales_customer_service_values[$year] * $cost_services / 100);
				}
				else
				{
					$cost_watches_values[$year] = $cost_watches_values[$year] + ($gross_sales_watches_values[$year] * $cost_watches / 100);
					
					$cost_sales_jewellery_values[$year] = $cost_sales_jewellery_values[$year] + ($gross_sales_jewellery_values[$year] * $cost_bijoux / 100);

					$cost_sales_accessories_values[$year] = $cost_sales_accessories_values[$year] + ($gross_sales_accessories_values[$year] * $cost_accessories / 100);

					$cost_sales_customer_service_values[$year] = $cost_sales_customer_service_values[$year] + ($gross_sales_customer_service_values[$year] * $cost_services / 100);
				}
			}
			else
			{
				if($cer_basicdata["cer_basicdata_cost_on_net_sales"] == 1)
				{
					$cost_watches_values[$year] = ($net_sales_watches_values[$year] * $cost_watches / 100);

					$cost_sales_jewellery_values[$year] = ($net_sales_jewellery_values[$year] * $cost_bijoux / 100);

					$cost_sales_accessories_values[$year] = ($net_sales_accessories_values[$year] * $cost_accessories / 100);

					$cost_sales_customer_service_values[$year] = ($net_sales_customer_service_values[$year] * $cost_services / 100);
				}
				else
				{
					$cost_watches_values[$year] = ($gross_sales_watches_values[$year] * $cost_watches / 100);
					
					$cost_sales_jewellery_values[$year] = ($gross_sales_jewellery_values[$year] * $cost_bijoux / 100);

					$cost_sales_accessories_values[$year] = ($gross_sales_accessories_values[$year] * $cost_accessories / 100);

					$cost_sales_customer_service_values[$year] = ($gross_sales_customer_service_values[$year] * $cost_services / 100);
				}
			}
		
		}
	}

	foreach($years as $key=>$year)
	{
		$cost_of_production[$year] = $cost_watches_values[$year] + $cost_sales_jewellery_values[$year] + $cost_sales_accessories_values[$year] + $cost_sales_customer_service_values[$year];
		
		$fields = array();

		$value = dbquote($cost_of_production[$year]);
		$fields[] = "cer_expense_amount = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}

		$sql = "update cer_expenses set " . join(", ", $fields) . " where cer_expense_cer_version = " . $version . " and cer_expense_project = " .$project_id . " and cer_expense_type = 15 and cer_expense_year = " . dbquote($year);
		mysql_query($sql) or dberror($sql);
	}


	$cost_of_production_values = array('cost_watches_values'=>$cost_watches_values, 'cost_sales_jewellery_values'=>$cost_sales_jewellery_values, 
		'cost_sales_accessories_values'=>$cost_sales_accessories_values,
		'cost_sales_customer_service_values'=>$cost_sales_customer_service_values);
	
	return $cost_of_production_values;
}


/**
* DATEADD
* Returns a new Unix timestamp value based on adding an interval to the specified date.
* @param  string  $datepart is the parameter that specifies on which part of the date to return a new value.
* @param  integer $number is the value used to increment datepart. If you specify a value that is not an integer, the fractional part of the value is discarded.
* @param  integer $date     a Unix timestamp value.     
* @return integer a Unix timestamp.
*/
function DATEADD($datepart, $number, $date)
{
	$number = intval($number);
	switch (strtolower($datepart)) {
		case 'yy':
		case 'yyyy':
		case 'year':
			$d = getdate($date);
			$d['year'] += $number;
			if (($d['mday'] == 29) && ($d['mon'] == 2) && (date('L', mktime(0, 0, 0, 1, 1, $d['year'])) == 0)) $d['mday'] = 28;
			return mktime($d['hours'], $d['minutes'], $d['seconds'], $d['mon'], $d['mday'], $d['year']);
			break;
		case 'm':
		case 'mm':
		case 'month':
			$d = getdate($date);
			$d['mon'] += $number;
			while($d['mon'] > 12) {
				$d['mon'] -= 12;
				$d['year']++;
			}
			while($d['mon'] < 1) {
				$d['mon'] += 12;
				$d['year']--;
			}
			$l = date('t', mktime(0,0,0,$d['mon'],1,$d['year']));
			if ($d['mday'] > $l) $d['mday'] = $l;
			return mktime($d['hours'], $d['minutes'], $d['seconds'], $d['mon'], $d['mday'], $d['year']);
			break;
		case 'd':
		case 'dd':
		case 'day':
			return ($date + $number * 86400); 
			break;
		default:
			die("Unsupported operation");
	}
}

/**
* DATEDIFF
* Returns the number of date and time boundaries crossed between two specified dates.
* @param  string  $datepart  is the parameter that specifies on which part of the date to calculate the difference.
* @param  integer $startdate is the beginning date (Unix timestamp) for the calculation.
* @param  integer $enddate   is the ending date (Unix timestamp) for the calculation.     
* @return integer the number between the two dates.
*/
function DATEDIFF($datepart, $startdate, $enddate)
{
	switch (strtolower($datepart)) {
		case 'yy':
		case 'yyyy':
		case 'year':
			$di = getdate($startdate);
			$df = getdate($enddate);
			return $df['year'] - $di['year'];
			break;
		case 'q':
		case 'qq':
		case 'quarter':
			die("Unsupported operation");
			break;
		case 'n':
		case 'mi':
		case 'minute':
			return ceil(($enddate - $startdate) / 60); 
			break;
		case 'hh':
		case 'hour':
			return ceil(($enddate - $startdate) / 3600); 
			break;
		case 'd':
		case 'dd':
		case 'day':
			return ceil(($enddate - $startdate) / 86400); 
			break;
		case 'wk':
		case 'ww':
		case 'week':
			return ceil(($enddate - $startdate) / 604800); 
			break;
		case 'm':
		case 'mm':
		case 'month':
			$di = getdate($startdate);
			$df = getdate($enddate);
			return ($df['year'] - $di['year']) * 12 + ($df['mon'] - $di['mon']);
			break;
		default:
			die("Unsupported operation");
	}
}


/********************************************************************
    calculate franchisee agreement data
*********************************************************************/
function get_fag_data($date)
{
	

	if(substr($date, 8,2) < 15)
	{
		$fag_data["starting_date"] = substr($date, 0,7) . "-01";
	}
	else
	{
		if(substr($date, 5,2) == 12)
		{
			$year = substr($date, 0,4) + 1;
			$fag_data["starting_date"] = $year . "-01-01";
		}
		else
		{
			$month = substr($date, 5, 2) + 1;
			if($month < 10){$month = "0" . $month;}

			$fag_data["starting_date"] = substr($date, 0,5) . $month . "-01";	
		}
	}

	$year = substr($fag_data["starting_date"], 0,5) + 3;
	$month = '12';
	$day = '31';

		
	$fag_data["ending_date"] = $year . "-" . $month . "-" . $day;
	return $fag_data;
}


/********************************************************************
    get currency informations assigned to an order
*********************************************************************/
function get_order_currency($order_id)
{
    $currency = array();

    $sql = "select * from orders where order_id = " . dbquote($order_id);
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $currency_id = $row["order_client_currency"];
        $currency["exchange_rate"] = $row["order_client_exchange_rate"];
    }


    if ($currency_id > 0)
    {
        $sql = "select * from currencies where currency_id = " . $currency_id;
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $currency["id"] = $currency_id;
            $currency["symbol"] = $row["currency_symbol"];
            //$currency["exchange_rate"] = $row["currency_exchange_rate"];
            $currency["factor"] = $row["currency_factor"];
        }
    }
    else
    {
        $currency["id"] = 0;
        $currency["symbol"] = "";
        $currency["exchange_rate"] = 0;
        $currency["factor"] = 0;
    }
    
    return $currency;
}

/*************************************************************************
   build line numbers for budget (position numbers)
**************************************************************************/
function get_budet_line_numbers($order_id)
{
    $line_numbers = array();

    // build budget position numbers for standard items
    $sql_1 = "select order_item_id, order_item_text, order_item_quantity, ".
             "    order_item_po_number, item_id, order_item_system_price, ".
             "    (order_item_quantity * order_item_system_price) as total_price, ".
             "    order_item_client_price, ".
             "    item_code, ".
             "    category_priority, category_name, ".
             "    address_shortcut, item_type_id, item_type_name, ".
             "    item_type_priority, order_item_type ".
             "from order_items ".
             "left join items on order_item_item = item_id ".
             "left join categories on order_item_category = category_id ".
             "left join addresses on order_item_supplier_address = address_id ".
             "left join item_types on order_item_type = item_type_id ";
    $sql_2 = "where (order_item_type = " . ITEM_TYPE_STANDARD . " or order_item_type = " . ITEM_TYPE_SERVICES . ") ";
    $sql_3 = "and order_item_order = " . $order_id . " ".
             "and (order_item_not_in_budget = 0 or order_item_not_in_budget is null)  " .
             "order by category_priority, item_code";

    $sql_order_items = $sql_1 . $sql_2 . $sql_3;

    $line_numbers_tmp = array();
    $i = 1;

    $res = mysql_query($sql_order_items) or dberror($sql_order_items);
    while ($row = mysql_fetch_assoc($res))
    {
        if ($i < 10)
        {
            $p = "00" . $i;
        }
        else if ($i < 100)
        {
            $p = "0" . $i;
        }
        else
        {
            $p = $i;
        }
        $line_numbers_tmp[$row["order_item_id"]] = $p;
        $i++;
    }

    $line_numbers[ITEM_TYPE_STANDARD] = $line_numbers_tmp;

    // build budget position numbers for special items
    $sql_2 = "where order_item_type = " . ITEM_TYPE_SPECIAL . " ";
    $sql_3 = "and order_item_order = " . $order_id . " ".
             "and (order_item_not_in_budget = 0 or order_item_not_in_budget is null)  " .
             "order by item_code";


    $sql_order_items = $sql_1 . $sql_2 . $sql_3;

    $line_numbers_tmp = array();

    $res = mysql_query($sql_order_items) or dberror($sql_order_items);
    while ($row = mysql_fetch_assoc($res))
    {
        if ($i < 10)
        {
            $p = "00" . $i;
        }
        else if ($i < 100)
        {
            $p = "0" . $i;
        }
        else
        {
            $p = $i;
        }
        $line_numbers_tmp[$row["order_item_id"]] = $p;
        $i++;
    }

    $line_numbers[ITEM_TYPE_SPECIAL] = $line_numbers_tmp;

    // build budget position numbers for local construction cost  positions
    $sql_2 = "where order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST . " ";
    $sql_3 = "and order_item_order = " . $order_id;
 
    $sql_order_items = $sql_1 . $sql_2 . $sql_3;

    $line_numbers_tmp = array();

    $res = mysql_query($sql_order_items) or dberror($sql_order_items);
    while ($row = mysql_fetch_assoc($res))
    {
        if ($i < 10)
        {
            $p = "00" . $i;
        }
        else if ($i < 100)
        {
            $p = "0" . $i;
        }
        else
        {
            $p = $i;
        }
        $line_numbers_tmp[$row["order_item_id"]] = $p;
        $i++;
    }

    
    $line_numbers[ITEM_TYPE_LOCALCONSTRUCTIONCOST] = $line_numbers_tmp;

    // build budget position numbers for exclusions
    $sql_2 = "where order_item_type = " . ITEM_TYPE_COST_ESTIMATION . " ";
    $sql_3 = "and order_item_order = " . $order_id;

    $sql_order_items = $sql_1 . $sql_2 . $sql_3;

    $line_numbers_tmp = array();

    $res = mysql_query($sql_order_items) or dberror($sql_order_items);
    while ($row = mysql_fetch_assoc($res))
    {
        if ($i < 10)
        {
            $p = "00" . $i;
        }
        else if ($i < 100)
        {
            $p = "0" . $i;
        }
        else
        {
            $p = $i;
        }
        $line_numbers_tmp[$row["order_item_id"]] = $p;
        $i++;
    }

    $line_numbers[ITEM_TYPE_COST_ESTIMATION] = $line_numbers_tmp;

    // build budget position numbers for exclusions
    $sql_2 = "where order_item_type = " . ITEM_TYPE_EXCLUSION . " ";
    $sql_3 = "and order_item_order = " . $order_id;

    $sql_order_items = $sql_1 . $sql_2 . $sql_3;

    $line_numbers_tmp = array();

    $res = mysql_query($sql_order_items) or dberror($sql_order_items);
    while ($row = mysql_fetch_assoc($res))
    {
        if ($i < 10)
        {
            $p = "00" . $i;
        }
        else if ($i < 100)
        {
            $p = "0" . $i;
        }
        else
        {
            $p = $i;
        }
        $line_numbers_tmp[$row["order_item_id"]] = $p;
        $i++;
    }

    $line_numbers[ITEM_TYPE_EXCLUSION] = $line_numbers_tmp;

    // build budget position numbers for notifications
    $sql_2 = "where order_item_type = " . ITEM_TYPE_NOTIFICATION . " ";
    $sql_3 = "and order_item_order = " . $order_id;

    $sql_order_items = $sql_1 . $sql_2 . $sql_3;

    $line_numbers_tmp = array();

    $res = mysql_query($sql_order_items) or dberror($sql_order_items);
    while ($row = mysql_fetch_assoc($res))
    {
        if ($i < 10)
        {
            $p = "00" . $i;
        }
        else if ($i < 100)
        {
            $p = "0" . $i;
        }
        else
        {
            $p = $i;
        }
        $line_numbers_tmp[$row["order_item_id"]] = $p;
        $i++;
    }

    $line_numbers[ITEM_TYPE_NOTIFICATION] = $line_numbers_tmp;

    return $line_numbers;
}

/*************************************************************************
   build group totals for catalog items
**************************************************************************/
function get_group_total_of_standard_items($order_id, $field)
{
    $group_totals = array();
    
    $sql = "select category_priority, ".
           "sum(order_item_quantity * " . $field . ") AS group_total ".
           "from order_items LEFT JOIN categories ON order_item_category = category_id ".
           "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) " . 
		   "and order_item_order=" . $order_id . 
		   " and (order_item_type = " . ITEM_TYPE_STANDARD . " or order_item_type = " . ITEM_TYPE_SERVICES . ") ".
           "group by category_priority, category_name ".
           "order by category_priority";

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $group_totals[$row["category_priority"]] = $row["group_total"];
    }
    return $group_totals;
}

/*************************************************************************
    get the grand totals of a list of order_items of a certain item type
**************************************************************************/
function get_order_item_type_total($order_id, $item_type)
{
    $totals = array();
    $totals["in_system_currency"] = 0;
    $totals["in_order_currency"] = 0;

	if($item_type == ITEM_TYPE_STANDARD)
	{
		$filter = " (order_item_type=" . ITEM_TYPE_STANDARD . " or order_item_type = " . ITEM_TYPE_SERVICES . ")";
	}
	else
	{
		$filter = " order_item_type=" . $item_type;
	}


    $sql = "select order_item_order, order_item_type, order_item_not_in_budget, ".
           "    sum(order_item_system_price * if(order_item_quantity>0, order_item_quantity, 1)) as total_in_system_currency, ".
           "    sum(order_item_client_price * if(order_item_quantity>0, order_item_quantity, 1)) as total_in_order_currency ".
           "from order_items ".
           "where order_item_order=" . $order_id . 
           "   and order_item_not_in_budget = 0 or order_item_not_in_budget is null ".
           "group by order_item_order, order_item_type ".
           "having " . $filter;

	$res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $totals["in_system_currency"] = $totals["in_system_currency"] + $row["total_in_system_currency"];
        $totals["in_order_currency"] = $totals["in_order_currency"] + $row["total_in_order_currency"];
    }


    return $totals;
}
/*************************************************************************
   build group totals for catalog items, budget freezed
**************************************************************************/
function get_group_total_of_standard_items_freezed($order_id, $field)
{
    $group_totals = array();
    
    $sql = "select category_priority, ".
           "sum(order_item_quantity_freezed * " . $field . ") AS group_total ".
           "from order_items LEFT JOIN categories ON order_item_category = category_id ".
           "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) and order_item_order=" . $order_id . 
		   " AND (order_item_type= " . ITEM_TYPE_STANDARD . " or order_item_type= " . ITEM_TYPE_SERVICES . ") " .
           "group by category_priority, category_name ".
           "order by category_priority";

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $group_totals[$row["category_priority"]] = $row["group_total"];
    }
    return $group_totals;
}

/****************************************************************************************
    get the grand totals of a list of order_items of a certain item type budget freezed
*****************************************************************************************/
function get_order_item_type_total_freezed($order_id, $item_type)
{
    $totals = array();
    $totals["in_system_currency"] = 0;
    $totals["in_order_currency"] = 0;


	if($item_type == ITEM_TYPE_STANDARD)
	{
		$filter = " (order_item_type=" . ITEM_TYPE_STANDARD . " or order_item_type = " . ITEM_TYPE_SERVICES . ")";
	}
	else
	{
		$filter = " order_item_type=" . $item_type;
	}


    $sql = "select order_item_order, order_item_type, order_item_not_in_budget, ".
           "    sum(order_item_system_price_freezed * if(order_item_quantity_freezed>0, order_item_quantity_freezed, 1)) as total_in_system_currency, ".
           "    sum(order_item_client_price_freezed * if(order_item_quantity_freezed>0, order_item_quantity_freezed, 1)) as total_in_order_currency ".
           "from order_items ".
           "where order_item_not_in_budget = 0 or order_item_not_in_budget is null ".
           "group by order_item_order, order_item_type ".
           "having order_item_order=" . $order_id .
           "    and " . $filter;

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $totals["in_system_currency"] = $totals["in_system_currency"] + $row["total_in_system_currency"];
        $totals["in_order_currency"] = $totals["in_order_currency"] + $row["total_in_order_currency"];
    }


    return $totals;
}


/*************************************************************************
   get all the roles of a user
**************************************************************************/
function get_user_roles($user_id)
{
    $user_roles = array();
    
    $sql = "select user_role_role ".
           "from user_roles ".
           "left join roles on user_role_role = role_id ".
           "where user_role_user = " . $user_id;

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $user_roles[] = $row["user_role_role"];
    }

    return $user_roles;
}


/*************************************************************************
   get the role of a user in the context of an order or project
   contains the orders the user has access to because of his role
**************************************************************************/
function get_user_specific_order_list($user_id, $user_roles = array())
{
    
    $order_ids = array();
    $condition = "";
    $user_address = 0;
	$project_access_filter = "";

    // get user's address
    $sql = "select user_address, user_can_only_see_his_projects from users ".
           "where user_id = " . user_id();

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {            
        $user_address = $row["user_address"];
		if($row["user_can_only_see_his_projects"] == 1) {
			$project_access_filter = " and order_user = " . $user_id . " ";
		}
    }


    //get users country
    $user_country = 0;
    $sql = "select address_country from addresses " .
           "where address_id = " . $user_address;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $user_country = $row["address_country"];
    }


    // check client
    $sql = "select order_id " .
           "from orders ".
           "where order_user = " . $user_id . 
           "   and order_client_address = " . $user_address;

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {            
        $order_ids[] = $row["order_id"];
    }


	// retail operator
    $sql = "select order_id " .
           "from orders ".
           "where order_retail_operator = " . $user_id;

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {            
        $order_ids[] = $row["order_id"];
    }


    // local agent
    if (has_access("has_access_to_all_projects_of_his_country"))
    {
		$country_filter = "";
        $sql = "select * from country_access " .
               "where country_access_user = " . user_id();


        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {            
            $country_filter.= " or address_country = " . $row["country_access_country"];
        }

        if($country_filter)
        {
            $country_filter = "address_country  = " . $user_country . $country_filter;
        }
        else
        {
            $country_filter = "address_country  = " . $user_country;
        }
        
        $sql = "select project_order " .
               "from projects ".
               "left join orders on order_id = project_order " .
               "left join addresses on address_id = order_client_address " .
               "where " . $country_filter . 
			   $project_access_filter;

        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {            
            $order_ids[] = $row["project_order"];
        }
    }

	//others
	if(has_access("cer_has_full_access_to_his_projects"))
	{
		$sql = "select project_order " .
               "from projects ".
               "left join orders on order_id = project_order " .
               "left join addresses on address_id = order_client_address " .
               "where (order_shop_address_country = " . $user_country . " or address_country  = " . $user_country . ") " . 
			   $project_access_filter;;

        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {            
            $order_ids[] = $row["project_order"];
        }

	}

    
    //projects
    
	// retail coordinator, design contractor, design supervisor
	if(in_array(7, $user_roles)) // design contractor
	{
		$sql = "select project_order " .
			   "from projects ".
			   "left join orders on order_id = project_order " .
			   "where order_actual_order_state_code >= 220 " . 
			   "   and (project_retail_coordinator = " . $user_id . 
			   "   or project_design_contractor = " . $user_id .
			   "   or project_design_supervisor = " . $user_id . 
			   ")";
	}
	else
	{

	$sql = "select project_order " .
		   "from projects ".
		   "where project_retail_coordinator = " . $user_id . 
		   "   or project_design_contractor = " . $user_id .
		   "   or project_design_supervisor = " . $user_id;
	}
	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{            
		$order_ids[] = $row["project_order"];
	}

   
	// check supplier or forwarder
	if (has_access("can_view_empty_orders") and has_access("can_view_order_before_booklet_approval"))
	{
		$sql = "select distinct order_item_order " .
			   "from order_items ".
			   "where order_item_supplier_address = " .$user_address . 
			   "   or order_item_forwarder_address = " . $user_address;
	}
	elseif (has_access("can_view_empty_orders") and !has_access("can_view_order_before_booklet_approval"))
	{
		$sql = "select distinct order_item_order " .
			   "from order_items ".
			   "where (order_item_supplier_address = " .$user_address .
			   "   or order_item_forwarder_address = " . $user_address . ")".
			   "   and order_item_show_to_suppliers = 1";
	}
	elseif (!has_access("can_view_empty_orders") and has_access("can_view_order_before_booklet_approval"))
	{
		$sql = "select distinct order_item_order " .
			   "from order_items ".
			   "where (order_item_supplier_address = " .$user_address . 
			   "   or order_item_forwarder_address = " . $user_address . ")" .
			   "   and (order_item_not_in_budget = 0 ".
			   "   or order_item_not_in_budget is null)";
	}
	elseif (!has_access("can_view_empty_orders") and !has_access("can_view_order_before_booklet_approval"))
	{
		$sql = "select distinct order_item_order " .
			   "from order_items ".
			   "where (order_item_supplier_address = " .$user_address . 
			   "   or order_item_forwarder_address = " . $user_address . ")" .
			   "   and (order_item_not_in_budget = 0 ".
			   "   or order_item_not_in_budget is null)" .
			   "   and order_item_show_to_suppliers = 1";
	}

	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{            
		$order_ids[] = $row["order_item_order"];
	}

    $condition = "(" . implode(",", $order_ids) .")";
    
    return $condition;

}



/********************************************************************
    get_province_name
*********************************************************************/
function get_province_name($place_id)
{
	$province_name = "";

	$sql = "select province_canton " . 
	       "from places " .
		   "left join provinces on province_id = place_province " . 
		   "where place_id = " . dbquote($place_id);

	$res = mysql_query($sql) or dberror($sql);
	
	if($row = mysql_fetch_assoc($res))
	{
		$province_name = $row["province_canton"];
	}

	return $province_name;
}


/********************************************************************
    getproject milestone
*********************************************************************/
function get_project_milestone($project_id, $milestone_id)
{
    $milestone = array();
    
	$sql = "select * from project_milestones " . 
		   "where project_milestone_project = " . $project_id .
		   " and project_milestone_milestone = " . $milestone_id;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$milestone = $row;
	}
    
    return $milestone;
}


/********************************************************************
    get latest project from POS
*********************************************************************/

function get_latest_pos_project($pos_id)
{

	//check if there is a new project in progress for this POS
	$sql = 'select posorder_id, project_id, order_actual_order_state_code, project_postype, ' . 
	       'order_id, order_client_address  ' . 
		   'from posorderspipeline ' .
		   'left join orders on order_id = posorder_order ' .
		   'left join projects on project_order = order_id  ' .
		   'where posorder_parent_table = "posaddresses" ' .
		   ' and posorder_order > 0 ' .
		   ' and posorder_type = 1 ' .
		   ' and posorder_posaddress = ' . $pos_id;

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		return $row;
	}


	//check if there is an ongoing project
	$sql = 'select posorder_id, project_id, project_number, posorder_order, project_postype, ' . 
	       'order_actual_order_state_code, order_id,  order_client_address  ' . 
		   'from posorders ' . 
		   'left join orders on order_id = posorder_order ' .
		   'left join projects on project_order = order_id  ' .
		   'where posorder_posaddress = ' . $pos_id . 
		   ' and (project_shop_closingdate is NULL or project_shop_closingdate = "0000-00-00") ' .
	       ' and order_actual_order_state_code < 820 ' .
		   ' and posorder_order > 0 ' .
		   ' and posorder_type = 1 ' .
		   ' order by project_id DESC ';

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		return $row;
	}

	
	
	$sql = 'select posorder_id, project_id, project_number, posorder_order, project_postype, ' . 
	       'order_actual_order_state_code, order_id, order_client_address  ' . 
		   'from posorders ' . 
		   'left join orders on order_id = posorder_order ' .
		   'left join projects on project_order = order_id  ' .
		   'where posorder_posaddress = ' . $pos_id . 
	       ' and project_actual_opening_date is not NULL and project_actual_opening_date <> "0000-00-00" ' .
		   ' and posorder_order > 0 ' .
		   ' and posorder_type = 1 ' .
		   ' and project_projectkind in (1,2, 3, 4) ' .
		   ' order by project_id DESC ';

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		return $row;
	}
	

	return array();

}


/********************************************************************
    get system currency informations
*********************************************************************/
function check_sellout_data($project_order,$cer_basicdata_firstyear)
{
	$sellout_data_ok = true;

	//get pos_id
	$sql = "select posorder_posaddress " . 
		   "from posorders " . 
		   "where posorder_order = " . dbquote($project_order);

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$pos_id = $row["posorder_posaddress"];

	$pos_data = get_poslocation($pos_id, "posaddresses");
	$has_project = update_posdata_from_posorders($pos_id);

	$opening_date = $pos_data["posaddress_store_openingdate"];
	$closing_date = $pos_data["posaddress_store_closingdate"];

	$opening_year = substr($opening_date, 0, 4);
	$closing_year = substr($closing_date, 0, 4);
	$actual_year = date("Y");


	//check if sellout records exist else create
	$bp_starting_date = $cer_basicdata_firstyear;

	if($bp_starting_date > date("Y"))
	{
		$bp_starting_date = date("Y");
	}

	if($opening_year + 3 > $bp_starting_date )
	{
		$starting_year = $opening_year;
	}
	else
	{
		$starting_year = $bp_starting_date - 3;
	}

	if($closing_year > 0)
	{
		$ending_year = $closing_year;
	}
	else
	{
		$ending_year = $actual_year;
	}

	if ($starting_year and $ending_year) {
	
		for($year=$starting_year;$year<=$ending_year;$year++)
		{
			$sql = "select possellout_watches_units " . 
				   "from possellouts " . 
				   "where possellout_posaddress_id = " . dbquote($pos_id) . 
				   " and possellout_year = " .  dbquote($year);
			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);
			
			if($row["possellout_watches_units"] == 0)
			{
				$sellout_data_ok = false;
			}
		}
	} else {
		$sellout_data_ok = false;
	}

	return $sellout_data_ok;
}


/********************************************************************
    get_gross_sale_values
*********************************************************************/
function get_gross_sale_values($project_id = 0, $version = 0, $scenario_80_percent = false, $add_whsm_to_gross_sales = 0)
{
	
	// get gross sales values
	$total_sales = array();
	$sql =  "select * from cer_revenues " .
			"where cer_revenue_cer_version = " . $version . " and cer_revenue_project = " . $project_id .
		    " order by cer_revenue_year";;

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		
		if($add_whsm_to_gross_sales == 1) {
			//average prices in business plan are wholesale prices
			//but we like retail prices here
			$sales_watches_values = $row["cer_revenue_watches"] + $row["cer_revenue_watches"]*($row['cer_revenue_wholesale_margin_watches']/100);
			
			$sales_jewellery_values = $row["cer_revenue_jewellery"] + $row["cer_revenue_jewellery"]*($row['cer_revenue_wholesale_margin_bijoux']/100);
			
			$sales_customer_service_values = $row["cer_revenue_customer_service"] + $row["cer_revenue_customer_service"]*($row['cer_revenue_wholesale_margin_accessories']/100);
			
			$sales_accessories_values = $row["cer_revenue_accessories"] + $row["cer_revenue_accessories"]*($row['cer_revenue_wholesale_margin_services']/100);
		}
		else {
			$sales_watches_values = $row["cer_revenue_watches"];
			$sales_jewellery_values = $row["cer_revenue_jewellery"];
			$sales_customer_service_values = $row["cer_revenue_customer_service"];
			$sales_accessories_values = $row["cer_revenue_accessories"];
		}

		if(array_key_exists($row["cer_revenue_year"], $total_sales))
		{
			$tmp = 1*$sales_watches_values + 1*$sales_jewellery_values + 1*$sales_customer_service_values + 1*$sales_accessories_values;
			$total_sales[$row["cer_revenue_year"]] = $total_sales[$row["cer_revenue_year"]] + $tmp;
		}
		else
		{
			$total_sales[$row["cer_revenue_year"]] = 1*$sales_watches_values + 1*$sales_jewellery_values + 1*$sales_customer_service_values + 1*$sales_accessories_values;
		}

		if($scenario_80_percent == true)
		{
			$total_sales[$row["cer_revenue_year"]] = 0.8*$total_sales[$row["cer_revenue_year"]];
		}

	}


	return $total_sales;
}


/********************************************************************
    get_net_sale_values
*********************************************************************/
function get_net_sale_values($project_id = 0, $version = 0, $scenario_80_percent = false, $add_whsm_to_gross_sales = 0)
{
	$cer_basicdata = get_cer_basicdata($project_id, $version);
	// get gross sales values
	$total_net_sales = array();
	

	//get sales planning
	$sql = "select * from cer_revenues " .
		   "where cer_revenue_cer_version = " . $version . " and cer_revenue_project = " . $project_id;

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$year = $row["cer_revenue_year"];

		$sales_reduction_watches = $row["cer_revenue_sales_reduction_watches"];
		$sales_reduction_bijoux = $row["cer_revenue_sales_reduction_bijoux"];
		$sales_reduction_accessories = $row["cer_revenue_sales_reduction_accessories"];
		$sales_reduction_services = $row["cer_revenue_sales_reduction_cservice"];

		
		if($add_whsm_to_gross_sales == 1) {
			//average prices in business plan are wholesale prices
			//but we like retail prices here
			$row["cer_revenue_watches"] = $row["cer_revenue_watches"] + $row["cer_revenue_watches"]*($row['cer_revenue_wholesale_margin_watches']/100);
			
			$row["cer_revenue_jewellery"] = $row["cer_revenue_jewellery"] + $row["cer_revenue_jewellery"]*($row['cer_revenue_wholesale_margin_bijoux']/100);
			
			$row["cer_revenue_customer_service"] = $row["cer_revenue_customer_service"] + $row["cer_revenue_customer_service"]*($row['cer_revenue_wholesale_margin_accessories']/100);
			
			$row["cer_revenue_accessories"] = $row["cer_revenue_accessories"] + $row["cer_revenue_accessories"]*($row['cer_revenue_wholesale_margin_services']/100);
		
		}
		
		if($scenario_80_percent == true)
		{
			$net_sales_watches = 0.8*$row["cer_revenue_watches"] - (0.8*$row["cer_revenue_watches"]*$sales_reduction_watches/100);
			$net_sales_jewellery = 0.8*$row["cer_revenue_jewellery"] - (0.8*$row["cer_revenue_jewellery"]*$sales_reduction_bijoux/100); 
			$net_sales_accessories = 0.8*$row["cer_revenue_accessories"] - (0.8*$row["cer_revenue_accessories"]*$sales_reduction_accessories/100);
			$net_sales_customer_service = 0.8*$row["cer_revenue_customer_service"]- (0.8*$row["cer_revenue_customer_service"]*$sales_reduction_services/100);
		}
		else {		
			$net_sales_watches = $row["cer_revenue_watches"] - ($row["cer_revenue_watches"]*$sales_reduction_watches/100);
			$net_sales_jewellery = $row["cer_revenue_jewellery"] - ($row["cer_revenue_jewellery"]*$sales_reduction_bijoux/100); 
			$net_sales_accessories = $row["cer_revenue_accessories"] - ($row["cer_revenue_accessories"]*$sales_reduction_accessories/100);
			$net_sales_customer_service = $row["cer_revenue_customer_service"]- ($row["cer_revenue_customer_service"]*$sales_reduction_services/100);
		}

		if(array_key_exists($row["cer_revenue_year"], $total_net_sales))
		{
			$total_net_sales[$year] = $total_net_sales[$year] + $net_sales_watches + $net_sales_jewellery + $net_sales_accessories + $net_sales_customer_service;
		}
		else
		{
			$total_net_sales[$year] = $net_sales_watches + $net_sales_jewellery + $net_sales_accessories + $net_sales_customer_service;
		}

	}

	return $total_net_sales;
}

/********************************************************************
    get_total_rents
*********************************************************************/
function get_total_rents($project_id = 0, $version = 0)
{

	$total_rents = array();
	$sql = "select * " .
		   "from cer_expenses " .
		   "where cer_expense_cer_version = " . $version . " and cer_expense_type IN (2,16, 20) " . 
		   " and cer_expense_project = " . $project_id .
		   " order by cer_expense_year";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		if(array_key_exists($row["cer_expense_year"], $total_rents))
		{
			$total_rents[$row["cer_expense_year"]] = $total_rents[$row["cer_expense_year"]] + $row["cer_expense_amount"];
		}
		else
		{
			$total_rents[$row["cer_expense_year"]] = $row["cer_expense_amount"];
		}
	}

	return $total_rents;
}

/********************************************************************
    get_fixed_rents
*********************************************************************/
function get_fixed_rents($project_id = 0, $version = 0)
{

	$fixed_rents = array();
	$sql = "select cer_expense_year, sum(cer_expense_amount) as cer_expense_amount " .
		   "from cer_expenses " .
		   "where cer_expense_cer_version = " . $version . 
		   " and cer_expense_type in (2,20) " . 
		   " and cer_expense_project = " . $project_id .
		   " group by cer_expense_year " . 
		   " order by cer_expense_year";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$fixed_rents[$row["cer_expense_year"]] = $row["cer_expense_amount"];
	}

	return $fixed_rents;
}


/***********************************************************************************************************************
    update turnover based rental cost
************************************************************************************************************************/
function update_turnoverbased_rental_cost($project_id = 0, $version = 0, $scenario_80_percent = false)
{

	$cer_basicdata = get_cer_basicdata($project_id, $version);

	$add_whsm_to_gross_sales = 0;
	if($cer_basicdata["cer_basicdata_calculate_tob_incl_whsm"] == 1)
	{
		$add_whsm_to_gross_sales = 1;
	}


	if($cer_basicdata["cer_basicdata_tob_from_net_sales"] == 1)
	{
		$total_sales = get_net_sale_values($project_id, $version, $scenario_80_percent, $add_whsm_to_gross_sales);
	}
	else
	{
		$total_sales = get_gross_sale_values($project_id, $version, $scenario_80_percent, $add_whsm_to_gross_sales);
	}
	$fixed_rents = get_fixed_rents($project_id, $version);


	//calculate monthly turn over per year and month
	$monthly_total_sales = array();
	$first_year = $cer_basicdata["cer_basicdata_firstyear"];
	$first_month = $cer_basicdata["cer_basicdata_firstmonth"];

	$last_year = $cer_basicdata["cer_basicdata_lastyear"];
	$last_month = $cer_basicdata["cer_basicdata_lastmonth"];


	if($first_year == $last_year)
	{
		$num_of_months_first_year = $last_month - $first_month + 1;
	}
	else
	{
		$num_of_months_first_year = 13 - $first_month;
	}

	$debug[] = '<br />Calculation of monthly sales';
	
	foreach($total_sales as $year=>$total_sale)
	{
		if($year == $first_year)
		{
			 $monthly_total_sales[$year] = $total_sale/$num_of_months_first_year;
			 $debug[] = "<br />" . $year . " - " . $total_sale . " - " . $num_of_months_first_year;
		}
		elseif($year == $last_year)
		{
			 $monthly_total_sales[$year] = $total_sale/$last_month;
			 $debug[] = "<br />" . $year . " - " . $total_sale . " - " . $last_month;
		}
		else
		{
			$monthly_total_sales[$year] = $total_sale/12;
			$debug[] = "<br />" . $year . " - " . $total_sale . " - " . 12;
		}
	}

	$debug[] = "<strong>CER Percen from Sales Data</strong>";

	// Correct first and last year by decimal month shares
	// becasue busness splan period does not contain days
	$sql = "select * from cer_rent_percent_from_sales " . 
		   "where  cer_rent_percent_from_sale_cer_version = " . $version . " and cer_rent_percent_from_sale_project = " . $project_id . 
		   " and cer_rent_percent_from_sale_percent > 0 " . 
		   " order by cer_rent_percent_from_sale_from_year, " . 
		   " cer_rent_percent_from_sale_from_month, cer_rent_percent_from_sale_from_day ";

	//count how many times the business plan's last year appears in the records
	$number_of_records_with_last_year = 0;
	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		if($row["cer_rent_percent_from_sale_to_year"] == $last_year)
		{
			$number_of_records_with_last_year++;
		}
	}

	$res = mysql_query($sql) or dberror($sql);

	while($row = mysql_fetch_assoc($res))
	{
		
		$tob_from_year = $row["cer_rent_percent_from_sale_from_year"];
		$tob_from_month = $row["cer_rent_percent_from_sale_from_month"];
		$tob_from_day = $row["cer_rent_percent_from_sale_from_day"];
		$tob_to_year = $row["cer_rent_percent_from_sale_to_year"];
		$tob_to_month = $row["cer_rent_percent_from_sale_to_month"];
		$tob_to_day = $row["cer_rent_percent_from_sale_to_day"];

		$num_of_months_year_1 = 0;
		$num_of_months_year_2 = 0;

		$debug[] = "TOB to Year and Last Year of Business Plan: " . $tob_to_year . "->" . $last_year;

		if($tob_from_year == $first_year)
		{
			//first year
			if($tob_from_day == 1)
			{
				$date1 = $tob_from_year . "-" . $tob_from_month . "-00";
			}
			else
			{
				$date1 = $tob_from_year . "-" . $tob_from_month . "-" . $tob_from_day;	
			}
			$date2 = $tob_from_year . "-12-31";

			$number_of_days = cal_days_in_month(CAL_GREGORIAN, $tob_from_month, $tob_from_year);
			
			
			$d1 = new DateTime($date1);
			$d2 = new DateTime($date2);

			$part = 0;
			$number_of_months = $d1->diff($d2)->m;
			$part = ($number_of_days - $tob_from_day)/$number_of_days;
			
			if( $tob_from_day > 1)
			{
				$num_of_months_year_1 = $number_of_months + $part;
			}
			else
			{
				$num_of_months_year_1 = $number_of_months;
			}

			//make a correction by partial months
			if($tob_from_day> 30 and $tob_from_year == $first_year and $num_of_months_year_1 > 0)
			{
				$monthly_total_sales[$first_year] = $total_sales[$first_year]/$num_of_months_year_1;

				$debug[] = "<br /><strong>Monthly correction< by partial months</strong>";
				$debug[] = $first_year . ": " . $total_sales[$first_year] . "/" . $num_of_months_year_1 . "=" . $monthly_total_sales[$first_year];
			}
		}
		
		if($tob_to_year == $last_year)
		{
			$number_of_days = cal_days_in_month(CAL_GREGORIAN, $tob_to_month, $tob_to_year);

			$part = 0;
			$number_of_months = $tob_to_month;
			$part = $tob_to_day/$number_of_days;
		
			if( $tob_to_day < $number_of_days)
			{
				$num_of_months_year_2 = ($number_of_months-1) + $part;
			}
			else
			{
				$num_of_months_year_2 = $number_of_months;
			}

			//make a correction by partial months
			if($tob_to_year == $last_year and $num_of_months_year_2 > 0)
			{
				 //$monthly_total_sales[$last_year] = $total_sales[$last_year]/$num_of_months_year_2;
			}

		}
	}

	//prepare data from cer_rent_percent_from_sales
	$debug[] = "<br /><strong>Break Point Data</strong>";
	$tob_data_break_points = array();
	
	if($cer_basicdata["cer_basicdata_tob_from_breakpoint_difference"] == 1)
	{
		$sql = "select * from cer_rent_percent_from_sales " . 
			   "where cer_rent_percent_from_sale_cer_version = " . $version . " and cer_rent_percent_from_sale_project = " . $project_id . 
			   " and cer_rent_percent_from_sale_percent > 0 " . 
			   " order by cer_rent_percent_from_sale_from_year ASC, " . 
			   " cer_rent_percent_from_sale_from_month ASC, cer_rent_percent_from_sale_from_day ASC, cer_rent_percent_from_sale_amount DESC ";
	}
	else
	{
		$sql = "select * from cer_rent_percent_from_sales " . 
			   "where cer_rent_percent_from_sale_cer_version = " . $version . " and cer_rent_percent_from_sale_project = " . $project_id . 
			   " and cer_rent_percent_from_sale_percent > 0 " . 
			   " order by cer_rent_percent_from_sale_from_year ASC, " . 
			   " cer_rent_percent_from_sale_from_month ASC, cer_rent_percent_from_sale_from_day ASC, cer_rent_percent_from_sale_amount ASC ";
	}

	$res = mysql_query($sql) or dberror($sql);
	$old_timeperiod = "";
	while($row = mysql_fetch_assoc($res))
	{
		$tob_from_year = $row["cer_rent_percent_from_sale_from_year"];
		$tob_from_month = $row["cer_rent_percent_from_sale_from_month"];
		$tob_from_day = $row["cer_rent_percent_from_sale_from_day"];
		$tob_to_year = $row["cer_rent_percent_from_sale_to_year"];
		$tob_to_month = $row["cer_rent_percent_from_sale_to_month"];
		$tob_to_day = $row["cer_rent_percent_from_sale_to_day"];

		$timeperiod = $tob_from_year . $tob_from_month . $tob_from_day . $tob_to_year . $tob_to_month . $tob_to_day;

		
		if($old_timeperiod != $timeperiod)
		{
			$old_timeperiod = $timeperiod;
			$number_of_breakpoints_in_timeperiod = 1;
		}
		else
		{
			$number_of_breakpoints_in_timeperiod++;
		}

		$tob_data_break_points[] = array("timeperiod"=>$timeperiod, "breakpoint"=>$row["cer_rent_percent_from_sale_amount"], "percent"=>$row["cer_rent_percent_from_sale_percent"], "number_of_breakpoints_in_timeperiod"=>$number_of_breakpoints_in_timeperiod);
	}

	
	foreach($tob_data_break_points as $key=>$breakpoint)
	{
		foreach($breakpoint as $key=>$value)
		{
			$debug[] = $key . ": " . $value;
		}
		$debug[] = "<br />";
	}
	
	

	//calculate turnoverbased rents
	$debug[] = "<br /><strong>Turnover Based Rents</strong>";
	$turn_over_based_rents = array();
	$turn_over_in_contract_periods = array();
	$tax_rates = array();
	$passenger_rates = array();

	$record_count = 1;

	$sql = "select DISTINCT cer_rent_percent_from_sale_from_year, cer_rent_percent_from_sale_from_month, " .
		   "cer_rent_percent_from_sale_from_day, cer_rent_percent_from_sale_to_year, cer_rent_percent_from_sale_to_month, " . 
		   "cer_rent_percent_from_sale_to_day, cer_rent_percent_from_sale_tax_rate, cer_rent_percent_from_sale_passenger_rate " . 
	       "from cer_rent_percent_from_sales " . 
		   "where cer_rent_percent_from_sale_cer_version = " . $version . " and cer_rent_percent_from_sale_project = " . $project_id . 
		   " and cer_rent_percent_from_sale_percent > 0 " . 
		   " order by cer_rent_percent_from_sale_from_year, " . 
		   " cer_rent_percent_from_sale_from_month, cer_rent_percent_from_sale_from_day, cer_rent_percent_from_sale_passenger_rate ";

	$debug[] = "<br /><br />" . $sql . "<br /><br />";

	$res = mysql_query($sql) or dberror($sql);

	while($row = mysql_fetch_assoc($res))
	{
		$debug[] = "<br /><br /><strong>Record " . $record_count . " in table cer_rent_percent_from_sales </strong />";

		$tob_from_year = $row["cer_rent_percent_from_sale_from_year"];
		$tob_from_month = $row["cer_rent_percent_from_sale_from_month"];
		$tob_from_day = $row["cer_rent_percent_from_sale_from_day"];
		$tob_to_year = $row["cer_rent_percent_from_sale_to_year"];
		$tob_to_month = $row["cer_rent_percent_from_sale_to_month"];
		$tob_to_day = $row["cer_rent_percent_from_sale_to_day"];

		$tax_rate = (float)($row["cer_rent_percent_from_sale_tax_rate"]/100);
		$passenger_rate = (float)($row["cer_rent_percent_from_sale_passenger_rate"]/100);


		$timeperiod = $tob_from_year . $tob_from_month . $tob_from_day . $tob_to_year . $tob_to_month . $tob_to_day;

		$debug[] = "<br /><strong>For Time Period: ". $timeperiod . "</strong>";



		$num_of_months_year_1 = 0;
		$num_of_months_year_2 = 0;
		

		//if($tob_to_year > $tob_from_year)
		//{

			
			//first year
			$debug[] = "<br /><br /><strong>First Year</strong>";
			$date1 = $tob_from_year . "-" . $tob_from_month . "-" . $tob_from_day;

			$debug[] = "First Year Date from : " .$date1;


			$date1 = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $date1) ) ));

			if(1*$tob_from_month == 1 and 1*$tob_from_day == 1)
			{
				$date1 = date('Y-m-d',strtotime ( $date1));
			}
			else
			{
				$date1 = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $date1) ) ));
			}

			if($tob_to_year > $tob_from_year)
			{
				$date2 = $tob_from_year . "-12-31";
			}
			else
			{
				if($tob_to_month == 2 and ($tob_to_day == 28 or $tob_to_day == 29))
				{
					$date2 = $tob_to_year . "-" . $tob_to_month . "-31";
				}
				else
				{
					$date2 = $tob_to_year . "-" . $tob_to_month . "-" . $tob_to_day;
				}
			}
			
			$number_of_days = 0;

			if ($tob_from_month && $tob_from_year) {
				$number_of_days = cal_days_in_month(CAL_GREGORIAN, $tob_from_month, $tob_from_year);
			}

			$debug[] = "Number of Days in month: " . $number_of_days;
			$debug[] = "Datum von bis: " . $date1 . "->" . $date2;
			
			$d1 = new DateTime($date1);
			$d2 = new DateTime($date2);

			$part = 0;
			//$number_of_months = $d1->diff($d2)->m;
			$number_of_months = $d1->diff($d2)->m + ($d1->diff($d2)->y*12);
			
			$debug[] = "Number of months: " . $number_of_months;

			if ($number_of_days) {
				
				if($tob_from_year == $tob_to_year
					and $tob_from_month == $tob_to_month) {
					$part = ($tob_to_day - $tob_from_day + 1)/$number_of_days;
					$debug[] = "Calc part 01: " . $tob_to_day . " - " . $tob_from_day . "+ 1/ " .  $number_of_days . " = " . $part;
				}
				else {
					$part = ($number_of_days - $tob_from_day)/$number_of_days;
					$debug[] = "Calc part 02: " . $tob_to_day . " - " . $tob_from_day . "+ 1/ " .  $number_of_days . " = " . $part;
				}
			}

			$debug[] = "Part: " . $part;
			

			if( $tob_from_day > 1 
				and $tob_from_year == $tob_to_year
				and ($tob_from_month + 1) == $tob_to_month)
			{
				$num_of_months_year_1 = $number_of_months;
			}
			elseif( $tob_from_day > 1)
			{
				$debug[] = "Number of months tob_from_day > 1: " . $number_of_months . "->" . $part;
				$num_of_months_year_1 = $number_of_months + $part;

			}
			else
			{
				$num_of_months_year_1 = $number_of_months;
			}

			//work around to get full number of months
			if($tob_from_month == '01' and 
				$num_of_months_year_1 == $number_of_months 
				and $tob_from_day == '01' and $tob_to_day > 29
				and $tob_from_month < $tob_to_month)
			{
				$number_of_months++;
				$num_of_months_year_1++;

				$debug[] = "--->work around: " . $tob_from_month ." " . $num_of_months_year_1  . " " . $number_of_months . " " . $tob_from_day . " " . $tob_to_day;
			}

			if($number_of_months > 12)
			{
				$number_of_months = 12;
			}

			//workaround first year
			if($tob_from_year == $first_year 
				and $first_month > $tob_from_month)
			{
				$num_of_months_year_1 = $number_of_months;
			}
			elseif($tob_from_year == $first_year 
				and $tob_from_month > 1 
				and $tob_from_day > 1)
			{
				$num_of_months_year_1 = $number_of_months + 1;
			}
			elseif($tob_from_year == $first_year and $last_year > $first_year)
			{
				$num_of_months_year_1 = $number_of_months;
				
			}

			if($num_of_months_year_1 > 12)
			{
				$num_of_months_year_1 = 12;
			}

			if($tob_from_year == $tob_to_year
					and $tob_from_month == $tob_to_month
				    ) {
					$num_of_months_year_1 = $part;
			}


			$debug[] = "Number of months: " . $number_of_months;
			$debug[] = "Number of decimal months: " . $num_of_months_year_1;
			$debug[] = "Montly sales per year and month: ";
			foreach($monthly_total_sales as $key=>$value)
			{
				$debug[] = $key . "->" . $value;
			}
			
			if(array_key_exists($tob_from_year, $monthly_total_sales))
			{
				$turn_over_in_contract_period = $num_of_months_year_1 * $monthly_total_sales[$tob_from_year];
								
				$debug[] = "Turn over contract period: " . $num_of_months_year_1 . "*" . $monthly_total_sales[$tob_from_year] . " = " . $turn_over_in_contract_period;
				
				
				
				if(array_key_exists($tob_from_year,$turn_over_in_contract_periods ))
				{
					$turn_over_in_contract_periods[$tob_from_year] = $turn_over_in_contract_periods[$tob_from_year] + $turn_over_in_contract_period;
				}
				else
				{
					$turn_over_in_contract_periods[$tob_from_year] = $turn_over_in_contract_period;	
				}

				$debug[] = "TOB from year and tunrover in year: " . $tob_from_year . " " . $turn_over_in_contract_periods[$tob_from_year];

				

				
				$rent = 0;
				
				if($tob_from_year > $first_year)
				{
					$base_amount = $turn_over_in_contract_period;
					$debug[] = "Base Amount Turn Over starting: " . $tob_from_year ."/" . $first_year . ": " . $base_amount;
				}
				else
				{
					//$base_amount = $turn_over_in_contract_periods[$tob_from_year];
					//not clear why we first had the above line
					$base_amount = $turn_over_in_contract_period;
					$debug[] = "Base Amount Turn Over starting: " . $tob_from_year ."/" . $first_year . ": " . $base_amount;
				}
				
				
				foreach($tob_data_break_points as $key=>$break_point_data)
				{
					$debug[] = "Timeperiod breakpoint and time period and starting year and first year: " . $break_point_data["timeperiod"] . "->" . $timeperiod . "->" . $tob_from_year . "->" .$first_year;

					if($break_point_data["timeperiod"] == $timeperiod)
					{
						if($tob_from_year > $first_year
							or ($tob_from_year == $first_year and $tob_from_month > 1))
						{
							
							if($tob_from_year == $first_year and $part < 1) {
								$partial_break_point = $part*$num_of_months_year_1*$break_point_data["breakpoint"]/12;
							}
							else {
								$partial_break_point = $num_of_months_year_1*$break_point_data["breakpoint"]/12;
							}

							$debug[] = "-> num_of_months_year_1/parial breakcpoint " . $num_of_months_year_1 . "/" . $break_point_data["breakpoint"] . "/" . $partial_break_point;

							$debug[] = "Base amount and partial break point: " . $base_amount . "->" . $partial_break_point;
							if($base_amount > $partial_break_point) //break points
							{
								if($cer_basicdata["cer_basicdata_tob_from_breakpoint_difference"] == 1)
								{
									$debug[] = "<strong>->rent: </strong>" . $year . "->" . $rent;
									
									$difference = $base_amount - $partial_break_point;
									$rent = $rent + $break_point_data["percent"]*($difference)/100;
									$base_amount = $base_amount - $difference;
									
									$debug[] = "<strong>->Difference: </strong>" . $year . "->" . $rent . "->" . $break_point_data["percent"] . "->" . $difference  . "->" . $base_amount;
								}
								else
								{
									if($partial_break_point > 0 and $num_of_months_year_1 < 12)
									{
										//$rent = $partial_break_point*$base_amount/100;
										//$rent = $break_point_data["percent"]*$partial_break_point/100;
										$rent = $break_point_data["percent"]*$base_amount/100;
										
										
										$debug[] = "01 Partial breakpoint and base amount anc rent " . $partial_break_point . "->" . $base_amount . "->" . $rent;
									}
									else
									{
										$rent = $break_point_data["percent"]*$base_amount/100;

										$debug[] = "01 Rent " . $break_point_data["percent"] . "*" . $base_amount . "=" . $rent;
									}
									
								}
							}
						}
						else
						{
							$debug[] = "Base amount and break point: " . $base_amount . "->" . $break_point_data["breakpoint"];
							if($base_amount > $break_point_data["breakpoint"]) //break points
							{
								if($cer_basicdata["cer_basicdata_tob_from_breakpoint_difference"] == 1)
								{
									$debug[] = "<strong>->rent: </strong>" . $year . "->" . $rent;
									$difference = $base_amount - $break_point_data["breakpoint"];
									$rent = $rent + $break_point_data["percent"]*($difference)/100;
									$base_amount = $base_amount - $difference;

									$debug[] = "<strong>->Difference: </strong>" . $year . "->" . $rent . "->" . $break_point_data["percent"] . "->" . $difference  . "->" . $base_amount;
								}
								else
								{
									$rent = $break_point_data["percent"]*$base_amount/100;
									$debug[] = "02 Rent " . $break_point_data["percent"] . "*" . $base_amount . "=" . $rent;
								}
							}
						}
					}

				}
				
				if(array_key_exists($tob_from_year,$turn_over_based_rents ))
				{
					$turn_over_based_rents[$tob_from_year] = $turn_over_based_rents[$tob_from_year] + $rent;
				}
				else
				{
					$turn_over_based_rents[$tob_from_year] = $rent;
				}

				$tax_rates[$tob_from_year] = $tax_rate;
				$passenger_rates[$tob_from_year] = $passenger_rate;

				$debug[] = "<strong>->TOB year and tob rent: </strong>" . $tob_from_year . "->" . $turn_over_based_rents[$tob_from_year];

				 
			}


			//in between years
			$debug[] = "<strong><br /><br />In between years</strong>";
			for($year = $tob_from_year+1;$year <$tob_to_year;$year++)
			{
				$debug[] = "<br />Year " . $year;
				if(array_key_exists($year, $monthly_total_sales))
				{
					$turn_over_in_contract_period = $total_sales[$year];
					
					$debug[] = "Total Sales: " . $year . ": " . $total_sales[$year];
					if(array_key_exists($year,$turn_over_in_contract_periods ))
					{
						$turn_over_in_contract_periods[$year] = $turn_over_in_contract_periods[$year] + $turn_over_in_contract_period;
					}
					else
					{
						$turn_over_in_contract_periods[$year] = $turn_over_in_contract_period;	
					}
					
					$rent = 0;

					$base_amount = $turn_over_in_contract_periods[$year];
					foreach($tob_data_break_points as $key=>$break_point_data)
					{
						$debug[] = "Breakpoint data: " . $break_point_data["timeperiod"] . "->" . $timeperiod;
						if($break_point_data["timeperiod"] == $timeperiod)
						{
							$debug[] = "Base amount and breakpoint " . $base_amount . "->" . $break_point_data["breakpoint"];

							if($base_amount > $break_point_data["breakpoint"]) //break points
							{
								if($cer_basicdata["cer_basicdata_tob_from_breakpoint_difference"] == 1)
								{
									$difference = $base_amount - $break_point_data["breakpoint"];
									$rent = $rent + $break_point_data["percent"]*($difference)/100;
									$base_amount = $base_amount - $difference;
								}
								else
								{
									$rent = $break_point_data["percent"]*$base_amount/100;
								}

								$debug[] = "Breakpoint and base amount and Rent " . $break_point_data["percent"] . "->" . $base_amount . "->" . $rent . "<br />";
							}
						}

					}
					
					if(array_key_exists($year,$turn_over_based_rents ))
					{
						$turn_over_based_rents[$year] = $turn_over_based_rents[$year] + $rent;
					}
					else
					{
						$turn_over_based_rents[$year] = $rent;
					}

					$tax_rates[$year] = $tax_rate;
					$passenger_rates[$year] = $passenger_rate;
				}
			}


			//last year
			$debug[] = "<strong><br /><br />Last year</strong>";
			if($tob_to_year > $tob_from_year)
			{
			
				$number_of_days = cal_days_in_month(CAL_GREGORIAN, $tob_to_month, $tob_to_year);
						

				$part = 0;
				$number_of_months = $tob_to_month;
				$part = $tob_to_day/$number_of_days;
				

				$debug[] = "<br />to year: " . $tob_to_year . " to month: " . $tob_to_month . " to day: " . $tob_to_day . " number of months: " . $number_of_months . " part: " . $number_of_days . " part: " . $part;
			
				if( $tob_to_day < $number_of_days)
				{
					$num_of_months_year_2 = ($number_of_months-1) + $part;
				}
				else
				{
					$num_of_months_year_2 = $number_of_months;
				}

				//workaround last year
				if($tob_from_year == $last_year and $tob_to_year == $last_year)
				{
					$num_of_months_year_2 = $last_month;
					$debug[] = "<br />number of months2: " . $num_of_months_year_2;
				}

				$debug[] = "<br />number_of_records_with_last_year and  record_count: " . $number_of_records_with_last_year . '/'. $record_count;

				if($number_of_records_with_last_year == $record_count
					and $tob_to_year == $last_year)
				{
					$num_of_months_year_2 = $last_month;

					$debug[] = "<br />number of months2 last year: " . $num_of_months_year_2;
				}
				elseif($number_of_records_with_last_year == $record_count
					and $tob_from_year == $last_year)
				{
					$num_of_months_year_2 = $last_month;

					$debug[] = "<br />number of months2 last year: " . $num_of_months_year_2;
				}

				$debug[] = "Number of months: " . $num_of_months_year_2;

				if(array_key_exists($tob_to_year, $monthly_total_sales))
				{
					$turn_over_in_contract_period = $num_of_months_year_2 * $monthly_total_sales[$tob_to_year];

					$debug[] = "Turn over in contract period: " . $num_of_months_year_2 . "*" .  $monthly_total_sales[$tob_to_year] ."=" . $turn_over_in_contract_period;
					
					if(array_key_exists($tob_to_year,$turn_over_in_contract_periods ))
					{
						$turn_over_in_contract_periods[$tob_to_year] = $turn_over_in_contract_periods[$tob_to_year] + $turn_over_in_contract_period;
					}
					else
					{
						$turn_over_in_contract_periods[$tob_to_year] = $turn_over_in_contract_period;	
					}

					if($last_year == $tob_to_year)
					{
						$turn_over_in_contract_period = $num_of_months_year_2 * $monthly_total_sales[$last_year];
						$turn_over_in_contract_periods[$last_year] = $turn_over_in_contract_period;	
					}

					$rent = 0;
					$base_amount = $turn_over_in_contract_periods[$tob_to_year];

					foreach($tob_data_break_points as $key=>$break_point_data)
					{
						$debug[] = "Breackpont time period: " . $break_point_data["timeperiod"] . "->" . $timeperiod;
						if($break_point_data["timeperiod"] == $timeperiod)
						{
							
							$debug[] = " TOB Year and last year: " . $tob_to_year . "->" . $last_year;
							if($tob_to_year == $last_year
								and $tob_to_month == 12)
							{
								$debug[] = "Base amount and breakpoint: " . $base_amount . "->" . $break_point_data["breakpoint"];
							
								if($base_amount > $break_point_data["breakpoint"]) //break points
								{
									if($cer_basicdata["cer_basicdata_tob_from_breakpoint_difference"] == 1)
									{
										$difference = $base_amount - $break_point_data["breakpoint"];
										$rent = $rent + $break_point_data["percent"]*($difference)/100;
										$base_amount = $base_amount - $difference;

										$debug[] =  "<strong>03 Rent</strong> " . $rent;
										$debug[] = "Breakpoint percent and base amount: " .  $break_point_data["percent"] . "->" . $base_amount . "->" . $difference;
									}
									else
									{
										$rent = $break_point_data["percent"]*$base_amount/100;
										$debug[] =  "04 Rent: " . $break_point_data["percent"] . "*" . $base_amount . "/100=" . $rent;
										$debug[] = "Breakpoint percent and base amount: " .  $break_point_data["percent"] . "->" . $base_amount;
									}
								}
							}
							else // parial breakpoint in overlapping years
							{
								if($last_year == $tob_to_year and $last_year == 12) {
									$partial_breakpoint = $break_point_data["breakpoint"];
								}
								else {
									$partial_breakpoint = $num_of_months_year_2*$break_point_data["breakpoint"]/12;
								}
								
								$debug[] = "Base amount and pratial breakpoint: " . $base_amount . "->" . $partial_breakpoint;
								
								if($base_amount > $partial_breakpoint) //break points
								{
									if($cer_basicdata["cer_basicdata_tob_from_breakpoint_difference"] == 1)
									{
										$difference = $base_amount - $partial_breakpoint;
										$rent = $rent + $break_point_data["percent"]*($difference)/100;
										$base_amount = $base_amount - $difference;

										$debug[] =  "05 Rent " . $rent;
										$debug[] = "Breakpoint percent and base amount: " .  $break_point_data["percent"] . "->" . $base_amount;
									}
									else
									{
										if(isset($partial_break_point) 
											and $partial_break_point > 0
											and $num_of_months_year_2 < 12)
										{
											//$rent = $partial_breakpoint*$base_amount/100;
											//$rent = $break_point_data["percent"]*($partial_breakpoint)/100;
											$rent = $break_point_data["percent"]*($base_amount)/100;
										}
										else
										{
											$rent = $break_point_data["percent"]*($base_amount)/100;
										}
										
										$debug[] =  "06 Rent " . $rent;
										$debug[] = "Breakpoint percent and base amount: " .  $break_point_data["percent"] . "->" . $base_amount;
									}
								}
							}
						}
					}

					if(array_key_exists($tob_to_year,$turn_over_based_rents ))
					{
						$turn_over_based_rents[$tob_to_year] = $turn_over_based_rents[$tob_to_year] + $rent;
					}
					else
					{
						$turn_over_based_rents[$tob_to_year] = $rent;
					}

					$tax_rates[$tob_to_year] = $tax_rate;
					$passenger_rates[$tob_to_year] = $passenger_rate;

					$debug[] = "<strong>->TOB year and tob rent: </strong>" . $tob_to_year . "->" . $turn_over_based_rents[$tob_to_year];
					//abc();
				}
			}

			$record_count++;
			
		//}
	}

	//update expenses
	if($scenario_80_percent == false)
	{
		$sql = "update cer_expenses SET " . 
			   "cer_expense_amount = 0 " .
			   " where cer_expense_cer_version = " . $version . " and cer_expense_type = 16 " . 
			   " and cer_expense_project = " . $project_id;

					
		$result = mysql_query($sql) or dberror($sql);

		foreach($turn_over_based_rents as $year=>$rent)
		{
			
			//calculate rent on the basis of whatever is higher
			// TOB-Rents are not added to fixed rents
			if($cer_basicdata["cer_basicdata_add_tob_rents"] == 0)
			{
				if($rent > $fixed_rents[$year])
				{
					//echo $year . "  " . $rent . "  " .  $fixed_rents[$year] . "<br />"; 
					
					$rent = $rent - $fixed_rents[$year];

					
				}
				else
				{
					$rent = 0;
				}
			}
			
			//update expenses
			$sql = "update cer_expenses SET " . 
				   "cer_expense_amount = " . $rent . 
				   " where cer_expense_cer_version = " . $version . " and cer_expense_type = 16 " . 
				   " and cer_expense_project = " . $project_id . 
				   " and cer_expense_year = " . dbquote($year);

					
			$result = mysql_query($sql) or dberror($sql);


			//update taxrates
			$amount = 0;
			$sql = "select cer_expense_amount " .
				   " from cer_expenses " . 
				   " where cer_expense_cer_version = " . $version . " and cer_expense_type = 18 " . 
				   " and cer_expense_project = " . $project_id . 
				   " and cer_expense_year = " . dbquote($year);

			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				$amount = $row["cer_expense_amount"];
			}

			$amount = $amount + $tax_rates[$year]*$rent;
			

			$sql = "update cer_expenses SET " . 
				   "cer_expense_amount = " . $amount . 
				   " where cer_expense_cer_version = " . $version . " and cer_expense_type = 18 " . 
				   " and cer_expense_project = " . $project_id . 
				   " and cer_expense_year = " . dbquote($year);

					
			$result = mysql_query($sql) or dberror($sql);


			//update passenger index amount
			$amount = 0;
			$sql = "select cer_expense_amount " .
				   " from cer_expenses " .
				   " where cer_expense_cer_version = " . $version . " and cer_expense_type = 19 " . 
				   " and cer_expense_project = " . $project_id . 
				   " and cer_expense_year = " . dbquote($year);

			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				$amount = $row["cer_expense_amount"];
			}

			$amount = $amount + $passenger_rates[$year]*$rent;
			

			$sql = "update cer_expenses SET " . 
				   "cer_expense_amount = " . $amount . 
				   " where cer_expense_cer_version = " . $version . " and cer_expense_type = 19 " . 
				   " and cer_expense_project = " . $project_id . 
				   " and cer_expense_year = " . dbquote($year);

					
			$result = mysql_query($sql) or dberror($sql);
		}


		
		/*
		foreach($debug as $key=>$value)
		{
			echo $value . "<br />";
		}
		die;
		*/
		
		
	}
	else
	{
		$tob_rents_80_percent = array();

		$taxes_80_percent = array();
		$passenger_index_80_percent = array();

		$taxes_on_fixed_rents = array();
		$passenger_index_on_fixed_rents = array();

		if(count($turn_over_based_rents) > 0)
		{
			foreach($turn_over_based_rents as $year=>$rent)
			{
				
				//get taxes on fixed rents
				//01 get existing turnover based rent
				$sql_r = "select cer_expense_amount " . 
						   " from cer_expenses " . 
						   " where cer_expense_cer_version = " . $version . " and cer_expense_type = 16 " . 
						   " and cer_expense_project = " . $project_id . 
						   " and cer_expense_year = " . dbquote($year);

						
				$res_r = mysql_query($sql_r) or dberror($sql_r);

				if ($row_r = mysql_fetch_assoc($res_r))
				{
					$tmp_tob_rent = $row_r["cer_expense_amount"];
					$original_tax = $tax_rates[$year]*$tmp_tob_rent;
					$original_passenger_index = $passenger_rates[$year]*$tmp_tob_rent;

					//02 get original total tax
					$sql_r = "select cer_expense_amount " . 
							   " from cer_expenses " . 
							   " where cer_expense_cer_version = " . $version . " and cer_expense_type = 18 " . 
							   " and cer_expense_project = " . $project_id . 
							   " and cer_expense_year = " . dbquote($year);

							
					$res_r = mysql_query($sql_r) or dberror($sql_r);

					if ($row_r = mysql_fetch_assoc($res_r))
					{
						$original_total_tax = $row_r["cer_expense_amount"];
						$taxes_on_fixed_rents[$year] = $original_total_tax - $original_tax;
					}
					else
					{
						$taxes_on_fixed_rents[$year] = 0;
					}


					//03 get original total passenger index
					$sql_r = "select cer_expense_amount " . 
							   " from cer_expenses " . 
							   " where cer_expense_cer_version = " . $version . " and cer_expense_type = 19 " . 
							   " and cer_expense_project = " . $project_id . 
							   " and cer_expense_year = " . dbquote($year);

							
					$res_r = mysql_query($sql_r) or dberror($sql_r);

					if ($row_r = mysql_fetch_assoc($res_r))
					{
						$original_total_passenger_index = $row_r["cer_expense_amount"];
						$passenger_index_on_fixed_rents[$year] = $original_total_passenger_index - $original_passenger_index;
					}
					else
					{
						$taxes_on_fixed_rents[$year] = 0;
					}
				}
				else
				{
					$taxes_on_fixed_rents[$year] = 0;
					$passenger_index_on_fixed_rents = 0;
				}



				
				//calculate rent on the basis of whatever is higher
				// TOB-Rents are not added to fixed rents
				if($cer_basicdata["cer_basicdata_add_tob_rents"] == 0)
				{
					if($rent > $fixed_rents[$year])
					{
						$rent = $rent - $fixed_rents[$year];
					}
					else
					{
						$rent = 0;
					}
				}

				$tob_rents_80_percent[$year] = $rent;
				
				//taxrates
				$amount = $tax_rates[$year]*$rent;
				$taxes_80_percent[$year] = $amount + $taxes_on_fixed_rents[$year];
			
					
				//passenger index
				$amount = $passenger_rates[$year]*$rent;
				$passenger_index_80_percent[$year] = $amount + $passenger_index_on_fixed_rents[$year];
			}
		
			return array("tob_rents"=>$tob_rents_80_percent, "taxes"=>$taxes_80_percent, "passenger_index"=>$passenger_index_80_percent);
		}
		else // taxes on fixed rents
		{
			foreach($fixed_rents as $year=>$rent)
			{
				
				//get taxes on fixed rents
				$sql_r = "select cer_expense_amount " . 
						   " from cer_expenses " . 
						   " where cer_expense_cer_version = " . $version . " and cer_expense_type = 18 " . 
						   " and cer_expense_project = " . $project_id . 
						   " and cer_expense_year = " . dbquote($year);

						
				$res_r = mysql_query($sql_r) or dberror($sql_r);

				if ($row_r = mysql_fetch_assoc($res_r))
				{
					$taxes_80_percent[$year] = $row_r["cer_expense_amount"];
				}
				else
				{
					$taxes_80_percent[$year] = 0;
				}


				//get passenger index on fixed rents
				$sql_r = "select cer_expense_amount " . 
						   " from cer_expenses " . 
						   " where cer_expense_cer_version = " . $version . " and cer_expense_type = 19 " . 
						   " and cer_expense_project = " . $project_id . 
						   " and cer_expense_year = " . dbquote($year);

						
				$res_r = mysql_query($sql_r) or dberror($sql_r);

				if ($row_r = mysql_fetch_assoc($res_r))
				{
					$passenger_index_80_percent[$year] = $row_r["cer_expense_amount"];
				}
				else
				{
					$passenger_index_80_percent[$year] = 0;
				}
					
			}
		
			return array("tob_rents"=>$tob_rents_80_percent, "taxes"=>$taxes_80_percent, "passenger_index"=>$passenger_index_80_percent);
		}
	}
	


	return true;
}


/**************************************************************************************************************************
    update beginning period of depreciation
**************************************************************************************************************************/
function update_beginning_period_of_depreciation($project_id = 0, $planned_opening_date, $agreed_opening_date, $version = 0)
{
	 
	 $sql = "select cer_basicdata_id,cer_basicdata_firstyear, " . 
			  "cer_basicdata_firstmonth, cer_basicdata_firstyear_depr, cer_basicdata_firstmonth_depr " . 
			  "from cer_basicdata " . 
			  "where cer_basicdata_version = " . $version . " and cer_basicdata_project = " . $project_id;


	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		
		$firstyear_depr = $row["cer_basicdata_firstyear"];
		$firstmonth_depr = $row["cer_basicdata_firstmonth"];

		$update = false;
		
		if(!$row["cer_basicdata_firstyear_depr"])
		{
			if($agreed_opening_date != NULL 
				and $agreed_opening_date != '0000-00-00'
			    and substr($agreed_opening_date, 0, 4) >= $firstyear_depr)
			{
				$firstyear_depr = substr($agreed_opening_date, 0, 4);
				$firstmonth_depr = substr($agreed_opening_date, 5, 2);
				$update = true;
			}
			elseif($planned_opening_date != NULL 
				and $planned_opening_date != '0000-00-00'
			    and substr($planned_opening_date, 0, 4) >= $firstyear_depr)
			{
				$firstyear_depr = substr($planned_opening_date, 0, 4);
				$firstmonth_depr = substr($planned_opening_date, 5, 2);
				$update = true;
			}
		}
		elseif($agreed_opening_date != NULL 
			and $agreed_opening_date != '0000-00-00'
		    and substr($agreed_opening_date, 0, 4) >= $firstyear_depr)
		{
			if($row["cer_basicdata_firstyear_depr"] == substr($planned_opening_date, 0, 4)
				and $row["cer_basicdata_firstmonth_depr"] == substr($planned_opening_date, 5, 2))
			{	
			    $firstyear_depr = substr($agreed_opening_date, 0, 4);
			    $firstmonth_depr = substr($agreed_opening_date, 5, 2);
				$update = true;
			}
			elseif($row["cer_basicdata_firstyear_depr"] == $row["cer_basicdata_firstyear"]
				and $row["cer_basicdata_firstmonth_depr"] == $row["cer_basicdata_firstmonth"])
			{	
			    $firstyear_depr = substr($agreed_opening_date, 0, 4);
			    $firstmonth_depr = substr($agreed_opening_date, 5, 2);
				$update = true;
			}
		}

		if($update == true)
		{

			$sql = "Update cer_basicdata set " . 
				   "cer_basicdata_firstyear_depr = " . dbquote($firstyear_depr) . ", " .
				   "cer_basicdata_firstmonth_depr = " . dbquote($firstmonth_depr) . 
				   " where cer_basicdata_version = " . $version . " and cer_basicdata_id = " . $row["cer_basicdata_id"];

			$result = mysql_query($sql) or dberror($sql);
		}

		return true;
	}
	return false;
}

/******************************************************************************
    update commissions
********************************************************************************/
function update_commissions($project_id, $version = 0)
{
	
	$revenue_watches = array();
	$revenue_jewellery = array();
	$revenue_accessories = array();
	$revenue_services = array();

	$cer_basicdata = get_cer_basicdata($project_id, $version);

	$global_commission_percent = $cer_basicdata["cer_basicdata_commission"];
	$global_commission_tax_percent = $cer_basicdata["cer_basicdata_commission_tax_percent"];


	$sql_salaries = "select * from cer_salaries " . 
		            "where cer_salary_cer_version = 0 and cer_salary_project = " . $project_id;


	//caclulate net sales, based on net sales values, calculated backwards up to gross sales
	$total_gross_sales_values = array();
	$total_net_sales_values = array();
	$gross_sales_watches_values = array();
	$gross_sales_jewellery_values = array();
	$gross_sales_accessories_values = array();
	$gross_sales_customer_service_values = array();

	$net_sales_watches_values = array();
	$net_sales_jewellery_values = array();
	$net_sales_accessories_values = array();
	$net_sales_customer_service_values = array();


	//get sales planning
	$sql = "select * from cer_revenues " .
		   "where cer_revenue_cer_version = " . $version . " and cer_revenue_project = " . $project_id;

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$year = $row["cer_revenue_year"];

		/*
		$sales_reduction_watches = $row["cer_revenue_sales_reduction_watches"] + $row["cer_revenue_reduction_credit_cards"];
		$sales_reduction_bijoux = $row["cer_revenue_sales_reduction_bijoux"] + $row["cer_revenue_reduction_credit_cards"];
		$sales_reduction_accessories = $row["cer_revenue_sales_reduction_accessories"] + $row["cer_revenue_reduction_credit_cards"];
		$sales_reduction_services = $row["cer_revenue_sales_reduction_cservice"] + $row["cer_revenue_reduction_credit_cards"];
		*/

		$sales_reduction_watches = $row["cer_revenue_sales_reduction_watches"];
		$sales_reduction_bijoux = $row["cer_revenue_sales_reduction_bijoux"];
		$sales_reduction_accessories = $row["cer_revenue_sales_reduction_accessories"];
		$sales_reduction_services = $row["cer_revenue_sales_reduction_cservice"];
		
		$gross_sales_watches_values[$year] = $row["cer_revenue_watches"];
		$gross_sales_jewellery_values[$year] = $row["cer_revenue_jewellery"];
		$gross_sales_accessories_values[$year] = $row["cer_revenue_accessories"];
		$gross_sales_customer_service_values[$year] = $row["cer_revenue_customer_service"];

		
		$net_sales_watches_values[$year] = $gross_sales_watches_values[$year] - ($gross_sales_watches_values[$year]*$sales_reduction_watches/100);
		
		$net_sales_jewellery_values[$year] = $gross_sales_jewellery_values[$year] - ($gross_sales_jewellery_values[$year]*$sales_reduction_bijoux/100); 
		
		$net_sales_accessories_values[$year] = $gross_sales_accessories_values[$year] - ($gross_sales_accessories_values[$year]*$sales_reduction_accessories/100);

		
		$net_sales_customer_service_values[$year] = $gross_sales_customer_service_values[$year]- ($gross_sales_customer_service_values[$year]*$sales_reduction_services/100);



		$commission_watches_values[$year] = 0;
		$commission_jewellery_values[$year] = 0;
		$commission_accessories_values[$year] = 0;
		$commission_customer_service_values[$year] = 0;
		$commission[$year] = 0;


		$res_s = mysql_query($sql_salaries) or dberror($sql_salaries);
		while ($row_s = mysql_fetch_assoc($res_s))
		{
			
			
			if($cer_basicdata["cer_basicdata_commission_from_net_sales"] == 1)
			{
				//if($row_s["cer_salary_commission_percent"] > 0)
				//{
					$commission_watches_values[$year] = $net_sales_watches_values[$year] * $row_s["cer_salary_commission_percent"] / 100;
					$commission_jewellery_values[$year] = $net_sales_jewellery_values[$year] * $row_s["cer_salary_commission_percent"] / 100;
					$commission_accessories_values[$year] = $net_sales_accessories_values[$year] * $row_s["cer_salary_commission_percent"] / 100;
					$commission_customer_service_values[$year] = $net_sales_customer_service_values[$year] * $row_s["cer_salary_commission_percent"] / 100;

					$total_commission = $commission_watches_values[$year] + $commission_jewellery_values[$year] + $commission_accessories_values[$year] + $commission_customer_service_values[$year];
					$commission[$year] = $commission[$year] + $total_commission;
				//}
				//if($row_s["cer_salary_commission_social_charges_percent"] > 0)
				//{
					$commission[$year] = $commission[$year] + ($total_commission* $row_s["cer_salary_commission_social_charges_percent"] / 100);
				//}
			}
			else
			{
				//if($row_s["cer_salary_commission_percent"] > 0)
				//{
					$commission_watches_values[$year] = $gross_sales_watches_values[$year] * $row_s["cer_salary_commission_percent"] / 100;
					$commission_jewellery_values[$year] = $gross_sales_jewellery_values[$year] * $row_s["cer_salary_commission_percent"] / 100;
					$commission_accessories_values[$year] = $gross_sales_accessories_values[$year] * $row_s["cer_salary_commission_percent"] / 100;
					$commission_customer_service_values[$year] = $gross_sales_customer_service_values[$year] * $row_s["cer_salary_commission_percent"] / 100;
					
					$total_commission = $commission_watches_values[$year] + $commission_jewellery_values[$year] + $commission_accessories_values[$year] + $commission_customer_service_values[$year];
					$commission[$year] = $commission[$year] + $total_commission;
				//}

				//echo $year . " " . $commission[$year] . "<br />";

				//if($row_s["cer_salary_commission_social_charges_percent"] > 0)
				//{
					$commission[$year] = $commission[$year] + ($total_commission* $row_s["cer_salary_commission_social_charges_percent"] / 100);
				//}
			}
		}


		//calculate global commission
		//if($global_commission_percent > 0)
		//{
			if($cer_basicdata["cer_basicdata_commission_from_net_sales"] == 1)
			{
				$commission_watches_values[$year] = $net_sales_watches_values[$year] * $global_commission_percent / 100;
				$commission_jewellery_values[$year] = $net_sales_jewellery_values[$year] * $global_commission_percent / 100;
				$commission_accessories_values[$year] = $net_sales_accessories_values[$year] * $global_commission_percent / 100;
				$commission_customer_service_values[$year] = $net_sales_customer_service_values[$year] * $global_commission_percent / 100;
				
				$total_commission = $commission_watches_values[$year] + $commission_jewellery_values[$year] + $commission_accessories_values[$year] + $commission_customer_service_values[$year];
				$commission[$year] = $commission[$year] + $total_commission;

				if($global_commission_tax_percent > 0)
				{
					$commission[$year] = $commission[$year] + ($total_commission* $global_commission_tax_percent / 100);
				}
			}
			else
			{
				$commission_watches_values[$year] = $gross_sales_watches_values[$year] * $global_commission_percent / 100;
				$commission_jewellery_values[$year] = $gross_sales_jewellery_values[$year] * $global_commission_percent / 100;
				$commission_accessories_values[$year] = $gross_sales_accessories_values[$year] * $global_commission_percent / 100;
				$commission_customer_service_values[$year] = $gross_sales_customer_service_values[$year] * $global_commission_percent / 100;
				
				$total_commission = $commission_watches_values[$year] + $commission_jewellery_values[$year] + $commission_accessories_values[$year] + $commission_customer_service_values[$year];
				$commission[$year] = $commission[$year] + $total_commission;

				if($global_commission_tax_percent > 0)
				{
					$commission[$year] = $commission[$year] + ($total_commission* $global_commission_tax_percent / 100);
				}
			}
		//}

		

		

		$fields = array();

		$value = dbquote($commission[$year]);
		$fields[] = "cer_expense_amount = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}

		
		$sql = "update cer_expenses set " . join(", ", $fields) . " where cer_expense_cer_version = " . $version . " and cer_expense_project = " .$project_id . " and cer_expense_type = 17 and cer_expense_year = " . dbquote($row["cer_revenue_year"]);
		mysql_query($sql) or dberror($sql);

	}
	
	return true;

}


/********************************************************************
    get POS Info of relocated POS
*********************************************************************/
function get_relocated_pos_info($pos_id)
{
	$pod_data = array();

	$sql = "select posaddress_name, posaddress_zip, place_name, country_name, " .
		   "posaddress_store_grosssurface, posaddress_store_totalsurface, posaddress_store_retailarea " . 
		   "from posaddresses " . 
		   "left join places on place_id = posaddress_place_id " .
		   "left join countries on country_id = posaddress_country " .
		   "where posaddress_id = " . $pos_id;

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$pos_data = $row;
	}

	return $pos_data;
}



/********************************************************************
    get POS Info of relocated POS
*********************************************************************/
function get_renovated_pos_info($order_id)
{
	$pos_id = 0;

	$sql = "select posaddress_id " .
		   "from posorders " .
		   "left join posaddresses on posaddress_id = posorder_posaddress " . 
		   "where posorder_order = " . $order_id;

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$pos_id = $row["posaddress_id"];
	}

	return $pos_id;
}


/********************************************************************
    Get LN lease data
*********************************************************************/
function get_ln_lease_data($project_id, $ln_basicdata_id, $project_pipeline, $version = 0)
{
	$poslease = array();
	if($version == 0)
	{
	
		if($project_pipeline == 0)
		{
			$sql_i = "select * from posleases " . 
					 "where poslease_order = " . dbquote($project_id) . 
					 " order by poslease_id DESC";
			
		}
		elseif($project_pipeline == 1)
		{
			$sql_i = "select * from posleasespipeline " . 
					 "where poslease_order = " . dbquote($project_id) . 
					 " order by poslease_id DESC";
		}
		$res_i = mysql_query($sql_i) or dberror($sql_i);
		if ($row_i = mysql_fetch_assoc($res_i))
		{
			$poslease = $row_i;
		}
	}
	else
	{
		$sql_i = "select * from ln_version_leases " . 
					 "where poslease_ln_basicdata_id = " . $ln_basicdata_id . 
					 " order by poslease_id DESC";

		$res_i = mysql_query($sql_i) or dberror($sql_i);
		if ($row_i = mysql_fetch_assoc($res_i))
		{
			$poslease = $row_i;
		}
	}

	return $poslease;

}


/********************************************************************
    Get CER lease data
*********************************************************************/
function get_cer_lease_data($project_id, $cer_basicdata_id, $project_pipeline, $version = 0)
{
	$poslease = array();
	if($version == 0)
	{
	
		if($project_pipeline == 0)
		{
			$sql_i = "select * from posleases " . 
					 "where poslease_order = " . dbquote($project_id) . 
					 " order by poslease_id DESC";
			
		}
		elseif($project_pipeline == 1)
		{
			$sql_i = "select * from posleasespipeline " . 
					 "where poslease_order = " . dbquote($project_id) . 
					 " order by poslease_id DESC";
		}
		$res_i = mysql_query($sql_i) or dberror($sql_i);
		if ($row_i = mysql_fetch_assoc($res_i))
		{
			$poslease = $row_i;
		}
	}
	else
	{
		$sql_i = "select * from cer_version_leases " . 
					 "where poslease_cer_basicdata_id = " . $cer_basicdata_id . 
					 " order by poslease_id DESC";

		$res_i = mysql_query($sql_i) or dberror($sql_i);
		if ($row_i = mysql_fetch_assoc($res_i))
		{
			$poslease = $row_i;
		}
	}

	return $poslease;

}


/********************************************************************
    Get latest LN Version for CER
*********************************************************************/
function get_latest_ln_version($projectd = 0)
{
	$cer_version = array();
	$cer_version["cer_basicdata_version"] = 0;

	$sql = "select ln_basicdata_id, ln_basicdata_version from ln_basicdata " . 
		   " where ln_basicdata_project = " . $projectd . 
		   " order by ln_basicdata_version DESC";

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$cer_version["ln_basicdata_id"] = $row["ln_basicdata_id"];
		$sql = "select cer_basicdata_version from cer_basicdata " . 
			   " where cer_basicdata_project = " . $projectd . 
			   " and cer_basicdata_version = " . $row["ln_basicdata_version"] . 
			   " and cer_basicdata_version_context = 'ln'";

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$cer_version["cer_basicdata_version"] = $row["cer_basicdata_version"];
		}
	}
	
	return $cer_version;
}


/********************************************************************
    create CER Version
*********************************************************************/
function create_cer_version($project_id, $context = "", $project_pipeline = 0)
{

	//version 0 is always the actual version in use
	//versions higher than 0 are archived versions from earlier time
	
	$new_cer_version = 0;

	$sql = "select cer_basicdata_version " . 
		   "from cer_basicdata " . 
		   "where cer_basicdata_project = " . dbquote($project_id) . 
		   "order by cer_basicdata_version DESC";
	
	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$new_version = (int)$row["cer_basicdata_version"] + 1;
		$new_cer_version = $new_version;
		
		
		//create archive version of CER havin version 0

		
		//basic data
		$sql = "select * from cer_basicdata " . 
			   "where cer_basicdata_version = 0 ". 
			   " and cer_basicdata_project = " . dbquote($project_id);

		
		$res = mysql_query($sql) or dberror($sql);	   
		$row = mysql_fetch_assoc($res);

		$cer_basicdata_id = $row["cer_basicdata_id"];


		$fields = 'cer_basicdata_version,';
		$values = $new_version . ',';

		$fields .= 'cer_basicdata_version_context,';
		$values .= dbquote($context) . ',';

		$fields .= 'cer_basicdata_version_user_id,';
		$values .= user_id() . ',';

		foreach($row as $field_name=>$value)
		{
			if($field_name != 'cer_basicdata_id' 
				and $field_name != 'cer_basicdata_version'
			    and $field_name != 'cer_basicdata_version_context'
				and $field_name != 'cer_basicdata_version_user_id'
				and $field_name != 'user_created'
				and $field_name != 'date_created'
				and $field_name != 'user_modified'
				and $field_name != 'date_modified'
			) {
				$fields .= $field_name . ',';
				$values .= dbquote($value) . ',';
			}
			
		}

		$fields .= 'user_created,';
		$values .= dbquote(user_login()) . ',';

		$fields .= 'date_created,';
		$values .= dbquote(date("Y-m-d H:i:s")) . ',';

		$fields = substr($fields, 0, strlen($fields)-1);
		$values = substr($values, 0, strlen($values)-1);

		$sql_i = 'insert into cer_basicdata (' . $fields . ') VALUES (' . $values . ')';
		$result = mysql_query($sql_i) or dberror($sql_i);

		$new_cer_id = mysql_insert_id();

		//expenses
		$sql = 'Select * from cer_expenses ' . 
			   'where cer_expense_project =' . dbquote($project_id) . 
			   ' and cer_expense_cer_version = 0';


		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = 'cer_expense_cer_version,';
			$values = $new_version . ',';
			foreach($row as $field_name=>$value)
			{
				if($field_name != 'cer_expense_id' 
					and $field_name != 'cer_expense_cer_version'
				    and $field_name != 'user_created'
					and $field_name != 'date_created'
					and $field_name != 'user_modified'
					and $field_name != 'date_modified') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields .= 'user_created,';
			$values .= dbquote(user_login()) . ',';

			$fields .= 'date_created,';
			$values .= dbquote(date("Y-m-d H:i:s")) . ',';

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_expenses (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}



		//new additional rental costs
		$sql = 'Select * from cer_additional_rental_costs ' . 
			   'where cer_additional_rental_cost_project =' . dbquote($project_id) . 
			   ' and cer_additional_rental_cost_cer_version = 0';


		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = 'cer_additional_rental_cost_cer_version,';
			$values = $new_version . ',';
			foreach($row as $field_name=>$value)
			{
				if($field_name != 'cer_additional_rental_cost_id' 
					and $field_name != 'cer_additional_rental_cost_cer_version'
				    and $field_name != 'user_created'
					and $field_name != 'date_created'
					and $field_name != 'user_modified'
					and $field_name != 'date_modified') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields .= 'user_created,';
			$values .= dbquote(user_login()) . ',';

			$fields .= 'date_created,';
			$values .= dbquote(date("Y-m-d H:i:s")) . ',';

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_additional_rental_costs (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		$sql = 'Select * from cer_additional_rental_cost_amounts ' . 
			   'where cer_additional_rental_cost_amount_project =' . dbquote($project_id) . 
			   ' and cer_additional_rental_cost_amount_version = 0';


		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = 'cer_additional_rental_cost_amount_version,';
			$values = $new_version . ',';
			foreach($row as $field_name=>$value)
			{
				if($field_name != 'cer_additional_rental_cost_amount_id' 
					and $field_name != 'cer_additional_rental_cost_amount_version'
				    and $field_name != 'user_created'
					and $field_name != 'date_created'
					and $field_name != 'user_modified'
					and $field_name != 'date_modified') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields .= 'user_created,';
			$values .= dbquote(user_login()) . ',';

			$fields .= 'date_created,';
			$values .= dbquote(date("Y-m-d H:i:s")) . ',';

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_additional_rental_cost_amounts (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}

		//end new additional rental costs



		//fixed rents
		$sql = 'Select * from cer_fixed_rents ' . 
			   'where cer_fixed_rent_project_id =' . dbquote($project_id) . 
			   ' and cer_fixed_rent_cer_version = 0';
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = 'cer_fixed_rent_cer_version,';
			$values = $new_version . ',';
			foreach($row as $field_name=>$value)
			{
				if($field_name != 'cer_fixed_rent_id' 
					and $field_name != 'cer_fixed_rent_cer_version'
				    and $field_name != 'user_created'
					and $field_name != 'date_created'
					and $field_name != 'user_modified'
					and $field_name != 'date_modified') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields .= 'user_created,';
			$values .= dbquote(user_login()) . ',';

			$fields .= 'date_created,';
			$values .= dbquote(date("Y-m-d H:i:s")) . ',';
			
			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_fixed_rents (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}

		
		//investments
		$sql = 'Select * from cer_investments ' . 
			   'where cer_investment_project =' . dbquote($project_id) . 
			   ' and cer_investment_cer_version = 0';
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = 'cer_investment_cer_version,';
			$values = $new_version . ',';
			foreach($row as $field_name=>$value)
			{
				if($field_name != 'cer_investment_id' 
					and $field_name != 'cer_investment_cer_version'
				    and $field_name != 'user_created'
					and $field_name != 'date_created'
					and $field_name != 'user_modified'
					and $field_name != 'date_modified') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields .= 'user_created,';
			$values .= dbquote(user_login()) . ',';

			$fields .= 'date_created,';
			$values .= dbquote(date("Y-m-d H:i:s")) . ',';
			
			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_investments (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//payment termns
		$sql = 'Select * from cer_paymentterms ' . 
			   'where cer_paymentterm_project =' . dbquote($project_id) . 
			   ' and cer_paymentterm_cer_version = 0';
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = 'cer_paymentterm_cer_version,';
			$values = $new_version . ',';
			foreach($row as $field_name=>$value)
			{
				if($field_name != 'cer_paymentterm_id' 
					and $field_name != 'cer_paymentterm_cer_version'
				    and $field_name != 'user_created'
					and $field_name != 'date_created'
					and $field_name != 'user_modified'
					and $field_name != 'date_modified') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields .= 'user_created,';
			$values .= dbquote(user_login()) . ',';

			$fields .= 'date_created,';
			$values .= dbquote(date("Y-m-d H:i:s")) . ',';
			
			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_paymentterms (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}

		
		//turnover based rentals
		$sql = 'Select * from cer_rent_percent_from_sales ' . 
			   'where cer_rent_percent_from_sale_project =' . dbquote($project_id) . 
			   ' and cer_rent_percent_from_sale_cer_version = 0';
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = 'cer_rent_percent_from_sale_cer_version,';
			$values = $new_version . ',';
			foreach($row as $field_name=>$value)
			{
				if($field_name != 'cer_rent_percent_from_sale_id' 
					and $field_name != 'cer_rent_percent_from_sale_cer_version'
				    and $field_name != 'user_created'
					and $field_name != 'date_created'
					and $field_name != 'user_modified'
					and $field_name != 'date_modified') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields .= 'user_created,';
			$values .= dbquote(user_login()) . ',';

			$fields .= 'date_created,';
			$values .= dbquote(date("Y-m-d H:i:s")) . ',';
			
			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_rent_percent_from_sales (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}

		//revenues
		$sql = 'Select * from cer_revenues ' . 
			   'where cer_revenue_project =' . dbquote($project_id) . 
			   ' and cer_revenue_cer_version = 0';
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = 'cer_revenue_cer_version,';
			$values = $new_version . ',';
			foreach($row as $field_name=>$value)
			{
				if($field_name != 'cer_revenue_id' 
					and $field_name != 'cer_revenue_cer_version'
				    and $field_name != 'user_created'
					and $field_name != 'date_created'
					and $field_name != 'user_modified'
					and $field_name != 'date_modified') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			
			$fields .= 'user_created,';
			$values .= dbquote(user_login()) . ',';

			$fields .= 'date_created,';
			$values .= dbquote(date("Y-m-d H:i:s")) . ',';
			
			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_revenues (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//salaries
		$sql = 'Select * from cer_salaries ' . 
			   'where cer_salary_project =' . dbquote($project_id) . 
			   ' and cer_salary_cer_version = 0';
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = 'cer_salary_cer_version,';
			$values = $new_version . ',';
			foreach($row as $field_name=>$value)
			{
				if($field_name != 'cer_salary_id' 
					and $field_name != 'cer_salary_cer_version'
				    and $field_name != 'user_created'
					and $field_name != 'date_created'
					and $field_name != 'user_modified'
					and $field_name != 'date_modified') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields .= 'user_created,';
			$values .= dbquote(user_login()) . ',';

			$fields .= 'date_created,';
			$values .= dbquote(date("Y-m-d H:i:s")) . ',';
			
			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_salaries (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//stocks
		$sql = 'Select * from cer_stocks ' . 
			   'where cer_stock_project =' . dbquote($project_id) . 
			   ' and cer_stock_cer_version = 0';
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = 'cer_stock_cer_version,';
			$values = $new_version . ',';
			foreach($row as $field_name=>$value)
			{
				if($field_name != 'cer_stock_id' 
					and $field_name != 'cer_stock_cer_version'
				    and $field_name != 'user_created'
					and $field_name != 'date_created'
					and $field_name != 'user_modified'
					and $field_name != 'date_modified') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields .= 'user_created,';
			$values .= dbquote(user_login()) . ',';

			$fields .= 'date_created,';
			$values .= dbquote(date("Y-m-d H:i:s")) . ',';
			
			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_stocks (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//cer summary
		$sql = "select * from cer_summary " . 
			   "where cer_summary_cer_version = 0 ". 
			   " and cer_summary_project = " . dbquote($project_id);

		
		$res = mysql_query($sql) or dberror($sql);	   
		$row = mysql_fetch_assoc($res);


		$fields = 'cer_summary_cer_version,';
		$values = $new_version . ',';


		foreach($row as $field_name=>$value)
		{
			if($field_name != 'cer_summary_id' 
				and $field_name != 'cer_summary_cer_version'
				and $field_name != 'user_created'
				and $field_name != 'date_created'
				and $field_name != 'user_modified'
				and $field_name != 'date_modified'
			) {
				$fields .= $field_name . ',';
				$values .= dbquote($value) . ',';
			}
			
		}

		$fields .= 'user_created,';
		$values .= dbquote(user_login()) . ',';

		$fields .= 'date_created,';
		$values .= dbquote(date("Y-m-d H:i:s")) . ',';

		$fields = substr($fields, 0, strlen($fields)-1);
		$values = substr($values, 0, strlen($values)-1);

		$sql_i = 'insert into cer_summary (' . $fields . ') VALUES (' . $values . ')';
		$result = mysql_query($sql_i) or dberror($sql_i);




		// Save lease data version
		$poslease = get_cer_lease_data($project_id, $cer_basicdata_id, $project_pipeline, 0);

		$fields = 'poslease_cer_basicdata_id,';
		$values = $new_cer_id . ',';


		foreach($poslease as $field_name=>$value)
		{
			if($field_name != 'poslease_id'
				and $field_name != 'poslease_posaddress'
				and $field_name != 'poslease_order'
				and $field_name != 'poslease_breakpoint_percent'
				and $field_name != 'poslease_breakpoint_amount'
				and $field_name != 'poslease_breakpoint_percent2'
				and $field_name != 'poslease_breakpoint_amount2'
				and $field_name != 'poslease_breakpoint_percent3'
				and $field_name != 'poslease_breakpoint_amount3'
				and $field_name != 'user_created'
				and $field_name != 'date_created'
				and $field_name != 'user_modified'
				and $field_name != 'date_modified'
			) {
				$fields .= $field_name . ',';
				$values .= dbquote($value) . ',';
			}
			
		}

		$fields .= 'user_created,';
		$values .= dbquote(user_login()) . ',';

		$fields .= 'date_created,';
		$values .= dbquote(date("Y-m-d H:i:s")) . ',';

		$fields = substr($fields, 0, strlen($fields)-1);
		$values = substr($values, 0, strlen($values)-1);

		$sql_i = 'insert into cer_version_leases (' . $fields . ') VALUES (' . $values . ')';
		$result = mysql_query($sql_i) or dberror($sql_i);


		//update cer data with ln_data in case the context is ln
		if($context == "ln")
		{
			$ln_basicdata = get_ln_basicdata($project_id);
			
			$fields = array();

			$value = dbquote($ln_basicdata["ln_basicdata_currency"]);
			$fields[] = "cer_basicdata_currency = " . $value;

			$value = dbquote($ln_basicdata["ln_basicdata_exchangerate"]);
			$fields[] = "cer_basicdata_exchangerate = " . $value;

			$value = dbquote($ln_basicdata["ln_basicdata_factor"]);
			$fields[] = "cer_basicdata_factor = " . $value;

			
			$sql = "update cer_basicdata set " . join(", ", $fields) . " where cer_basicdata_id = " .$new_cer_id;
			mysql_query($sql) or dberror($sql);
		}
	}


	//create LN-version
	if($context == 'ln' and $new_cer_version > 0)
	{
		
		
		//LN Basic DATA
		$sql = "select ln_basicdata_id, ln_basicdata_version " . 
			   "from ln_basicdata " . 
			   "where ln_basicdata_project = " . dbquote($project_id) . 
			   "order by ln_basicdata_version DESC";
		
		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$ln_basicdata_id = $row["ln_basicdata_id"];
			$new_version = (int)$row["ln_basicdata_version"] + 1;

			
			
			//create archive version of LN having version 0

			
			//basic data
			$sql = "select * from ln_basicdata " . 
				   "where ln_basicdata_version = 0 ". 
				   " and ln_basicdata_project = " . dbquote($project_id);

			
			$res = mysql_query($sql) or dberror($sql);	   
			$row = mysql_fetch_assoc($res);


			$fields = 'ln_basicdata_version,';
			$values = $new_version . ',';

			
			$fields .= 'ln_basicdata_version_user_id,';
			$values .= user_id() . ',';


			$fields .= 'ln_basicdata_version_cer_version,';
			$values .= $new_cer_version . ',';

			foreach($row as $field_name=>$value)
			{
				if($field_name != 'ln_basicdata_id' 
					and $field_name != 'ln_basicdata_version'
					and $field_name != 'ln_basicdata_version_user_id'
					and $field_name != 'ln_basicdata_version_cer_version'
					and $field_name != 'user_created'
					and $field_name != 'date_created'
					and $field_name != 'user_modified'
					and $field_name != 'date_modified'
				) {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
				
			}

			$fields .= 'user_created,';
			$values .= dbquote(user_login()) . ',';

			$fields .= 'date_created,';
			$values .= dbquote(date("Y-m-d H:i:s")) . ',';

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into ln_basicdata (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);

			$new_ln_id = mysql_insert_id();


			//LN Basic DATA IN03
			$sql = "select ln_basicdata_lnr03_ln_version " . 
				   "from ln_basicdata_inr03 " . 
				   "where ln_basicdata_lnr03_lnbasicdata_id = " . dbquote($ln_basicdata_id) . 
				   "order by ln_basicdata_lnr03_ln_version DESC";

			
			$res = mysql_query($sql) or dberror($sql);

			if ($row = mysql_fetch_assoc($res))
			{
				$new_version = (int)$row["ln_basicdata_lnr03_ln_version"] + 1;
				//create archive version of LN having version 0

				
				//basic data
				
				$sql = "select * from ln_basicdata_inr03 " . 
					   "where ln_basicdata_lnr03_lnbasicdata_id = " . dbquote($ln_basicdata_id);

				
				$res = mysql_query($sql) or dberror($sql);	   
				while ($row = mysql_fetch_assoc($res))
				{
					$fields = 'ln_basicdata_lnr03_lnbasicdata_id,';
					$values = $new_ln_id . ',';

					$fields .= 'ln_basicdata_lnr03_ln_version,';
					$values .= $new_version . ',';

					foreach($row as $field_name=>$value)
					{
						if($field_name != 'ln_basicdata_lnr03_id' 
							and $field_name != 'ln_basicdata_lnr03_lnbasicdata_id'
							and $field_name != 'ln_basicdata_lnr03_ln_version'
							and $field_name != 'user_created'
							and $field_name != 'date_created'
							and $field_name != 'user_modified'
							and $field_name != 'date_modified'
						) {
							$fields .= $field_name . ',';
							$values .= dbquote($value) . ',';
						}
						
					}

					$fields .= 'user_created,';
					$values .= dbquote(user_login()) . ',';

					$fields .= 'date_created,';
					$values .= dbquote(date("Y-m-d H:i:s")) . ',';

					$fields = substr($fields, 0, strlen($fields)-1);
					$values = substr($values, 0, strlen($values)-1);

					$sql_i = 'insert into ln_basicdata_inr03 (' . $fields . ') VALUES (' . $values . ')';
					$result = mysql_query($sql_i) or dberror($sql_i);
				}
			}



			//LN POS Lease Version
			$poslease = get_ln_lease_data($project_id, $ln_basicdata_id, $project_pipeline, 0);

			$fields = 'poslease_ln_basicdata_id,';
			$values = $new_ln_id . ',';


			foreach($poslease as $field_name=>$value)
				{
					if($field_name != 'poslease_id'
					    and $field_name != 'poslease_posaddress'
						and $field_name != 'poslease_order'
						and $field_name != 'poslease_breakpoint_percent'
						and $field_name != 'poslease_breakpoint_amount'
						and $field_name != 'poslease_breakpoint_percent2'
						and $field_name != 'poslease_breakpoint_amount2'
						and $field_name != 'poslease_breakpoint_percent3'
						and $field_name != 'poslease_breakpoint_amount3'
						and $field_name != 'user_created'
						and $field_name != 'date_created'
						and $field_name != 'user_modified'
						and $field_name != 'date_modified'
					) {
						$fields .= $field_name . ',';
						$values .= dbquote($value) . ',';
					}
					
				}

				$fields .= 'user_created,';
				$values .= dbquote(user_login()) . ',';

				$fields .= 'date_created,';
				$values .= dbquote(date("Y-m-d H:i:s")) . ',';

				$fields = substr($fields, 0, strlen($fields)-1);
				$values = substr($values, 0, strlen($values)-1);

				$sql_i = 'insert into ln_version_leases (' . $fields . ') VALUES (' . $values . ')';
				$result = mysql_query($sql_i) or dberror($sql_i);
		}
	}

	return $new_cer_version;

}

/********************************************************************
    ccalculate additional rental costs
*********************************************************************/
function calculate_additional_rental_costs($project_id = 0, $lease_start_date = '', $lease_end_date = '', $total_sqm = 0)
{
	
	$first_year = substr($lease_start_date, 0, 4);
	$first_month = substr($lease_start_date, 5, 2);
	$first_day = substr($lease_start_date, 8, 2);
	$last_year = substr($lease_end_date, 0, 4);
	$last_month = substr($lease_end_date, 5, 2);
	$last_day = substr($lease_end_date, 8, 2);


	$surface_in_sqm = $total_sqm;
	$surface_in_feet = 10.7639104*$total_sqm;


	$number_of_months_first_year = 13 - $first_month;
	$number_of_months_last_year = $last_month;



	$month_share_first_year = (1+cal_days_in_month(CAL_GREGORIAN, $first_month, $first_year)- $first_day)/cal_days_in_month(CAL_GREGORIAN, $first_month, $first_year);

	$number_of_months_first_year = $number_of_months_first_year - 1 + $month_share_first_year;
		
	$month_share_last_year = (1+cal_days_in_month(CAL_GREGORIAN, $last_month, $last_year))/cal_days_in_month(CAL_GREGORIAN, $last_month, $last_year);
	$number_of_months_last_year = $number_of_months_last_year - 1 + $month_share_last_year;

	
	//delete all additional rentals cost amounts not i n the period

	$sql = "Delete from cer_additional_rental_cost_amounts " . 
			" where cer_additional_rental_cost_amount_version = 0 " . 
			" and cer_additional_rental_cost_amount_project = " . dbquote($project_id);


	$result = mysql_query($sql) or dberror($sql);
	

	$sql_l = "select cer_additional_rental_cost_id, cer_additional_rental_cost_amount, " . 
			 "cer_additional_rental_cost_increase, cer_additional_rental_cost_unit, cer_additional_rental_cost_period, " .
			 "additional_rental_cost_type_id, additional_rental_cost_type_name " . 
			 " from cer_additional_rental_costs " . 
			 " left join additional_rental_cost_types on additional_rental_cost_type_id = cer_additional_rental_cost_type_id";

	$sql_l .= " where cer_additional_rental_cost_project = " . dbquote($project_id);
	$sql_l .= " and cer_additional_rental_cost_cer_version = 0 ";


	$res = mysql_query($sql_l) or dberror($sql_l);
	while($row = mysql_fetch_assoc($res))
	{
				
		$costype_id = $row["additional_rental_cost_type_id"];

		$amount_per_month = 0;
		if($row["cer_additional_rental_cost_unit"] == 1) // square foot and month
		{
			$total_amount = $surface_in_feet*$row["cer_additional_rental_cost_amount"];
			$amount_per_month = $total_amount;
		}
		else if($row["cer_additional_rental_cost_unit"] == 2) // square meter and month
		{
			$total_amount = $surface_in_sqm*$row["cer_additional_rental_cost_amount"];
			$amount_per_month = $total_amount;
		}
		else if($row["cer_additional_rental_cost_unit"] == 3) // square foot and year
		{
			$amount_per_month = $surface_in_feet*$row["cer_additional_rental_cost_amount"]/12;
		}
		else if($row["cer_additional_rental_cost_unit"] == 4) // square meter and year
		{
			$amount_per_month = $surface_in_sqm*$row["cer_additional_rental_cost_amount"]/12;
		}
		else if($row["cer_additional_rental_cost_unit"] == 5) // total surface per month
		{
			$amount_per_month = $row["cer_additional_rental_cost_amount"];
		}
		else if($row["cer_additional_rental_cost_unit"] == 6) // total surface per year
		{
			$amount_per_month = $row["cer_additional_rental_cost_amount"]/12;
		}

		$total_amount_per_year = 12*$amount_per_month;
		$increase = $row["cer_additional_rental_cost_increase"];
		$period = $row["cer_additional_rental_cost_period"];

		for($year=$first_year;$year<=$last_year;$year++)
		{
			$cost_per_year = 0;

			
			if($increase > 0 and $year > $first_year)
			{
				$increase_amount = $amount_per_month*$increase/100;
				$amount_per_month = 1*$amount_per_month + 1*$increase_amount;

			}

			if($first_year == $last_year)
			{
				$cost_per_year = ($number_of_months_last_year - $number_of_months_first_year)*$amount_per_month;
			}
			else if($year == $first_year)
			{
				$cost_per_year = $number_of_months_first_year*$amount_per_month;

			}
			else if($year == $last_year)
			{
				$increase_correction = 0;
				if($increase > 0 and $period == 2) // lease year, correct yearly increase to a part of the year
				{

					$increase_correction = (13-$number_of_months_last_year)*$increase_amount;
				}
				
				$cost_per_year = $last_month*$amount_per_month - $increase_correction;
			}
			else
			{
				$increase_correction = 0;
				if($increase > 0 and $period == 2) // lease year, correct yearly increase to a part of the year
				{
					$increase_correction = $number_of_months_first_year*$increase_amount;

				}
				$cost_per_year = 12*$amount_per_month - $increase_correction;
			}

			$cost_per_year = round($cost_per_year, 0);
			

			//update calculated amounts for additional rental costs
			
			
			$fields = array();
			$values = array();


			$fields[] = "cer_additional_rental_cost_amount_project";
			$values[] = dbquote($project_id);


			$fields[] = "cer_additional_rental_cost_amount_version";
			$values[] = 0;


			$fields[] = "cer_additional_rental_cost_amount_costtype_id";
			$values[] = dbquote($costype_id);

			$fields[] = "cer_additional_rental_cost_amount_year";
			$values[] = dbquote($year);

			$fields[] = "cer_additional_rental_cost_amount_amount";
			$values[] = dbquote($cost_per_year);

			
			$fields[] = "user_created";
			$values[] = dbquote(date("Y-m-d H:i:s"));

			$sql = "insert into cer_additional_rental_cost_amounts (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			

			mysql_query($sql) or dberror($sql);


			//echo "<br />" . $sql . "<br />";
		}
	}
	

	//update additional rental costs in expenses
	for($year=$first_year;$year<=$last_year;$year++)
	{
		$sql = "select sum(cer_additional_rental_cost_amount_amount) as amount " . 
			   "from cer_additional_rental_cost_amounts " . 
			   " where cer_additional_rental_cost_amount_version = 0 " . 
			   " and cer_additional_rental_cost_amount_project = " .dbquote($project_id) . 
			   " and cer_additional_rental_cost_amount_year = " . $year;
			   
		$res = mysql_query($sql) or dberror($sql);
		while($row = mysql_fetch_assoc($res))
		{
			$fields = array();
	
			$fields[] = "cer_expense_amount = " . dbquote($row["amount"]);

			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			$fields[] = "user_modified = " . dbquote(user_login());

			$sql = "update cer_expenses set " . join(", ", $fields) . 
			" where cer_expense_cer_version = 0 and cer_expense_project = " . dbquote($project_id) . 
			" and cer_expense_type = 3 " . 
			" and cer_expense_year = " . dbquote($year);

			mysql_query($sql) or dberror($sql);
		}
	}

}


/********************************************************************
    get project design objective items ids data 
*********************************************************************/
function get_project_design_objectives($id)
{
    $project_design_objective_item_ids = array();
    
    $sql = "select project_item_item, design_objective_group_id, design_objective_group_name, design_objective_item_name ".
           "from project_items ".
           "left join design_objective_items on project_item_item = design_objective_item_id ".
           "left join design_objective_groups on design_objective_item_group = design_objective_group_id ".
           "where project_item_project  = " . $id ." ".
           "order by design_objective_group_priority, design_objective_item_priority";

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
         $project_design_objective_item_ids[$row["design_objective_group_name"]] = $row["design_objective_item_name"];
    }

    return  $project_design_objective_item_ids;
}



/********************************************************************
    get predecessor project 
*********************************************************************/
function get_predecessor_project($pos_id, $order_id)
{
	//check if there is a new project in progress for this POS
	$sql = 'select posorder_id,  posorder_opening_date ' . 
		   'from posorders ' .
		   ' where (posorder_order <> ' . dbquote($order_id) . 
		   ' or posorder_order is null) ' . 
		   ' and posorder_type = 1 ' .
		   ' and posorder_posaddress = ' . dbquote($pos_id) . 
		   " and posorder_opening_date is not null " . 
		   " and posorder_opening_date <> '0000-00-00' " . 
		   " order by posorder_opening_date DESC ";


	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		return $row;
	}


	
	

	return array();
}

/********************************************************************
    check if there is a difference between ln and cer
*********************************************************************/
function is_there_a_difference_between_cer_and_ln($project_id = 0)
{
	$state = array();
	//check if there are versions
	$sql = "select count(cer_basicdata_id) as num_recs " .
		   " from cer_basicdata ".
		   " where cer_basicdata_project = " . dbquote($project_id);

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);

	if($row["num_recs"] < 2)
	{
		$state['state'] = false;

		$state['total_revenues_cer'] = 0;
		$state['total_revenues_ln'] = 0;

		$state['total_expenses_cer'] = 0;
		$state['total_expenses_ln'] = 0;

		$state['total_investments_cer'] = 0;
		$state['total_investments_ln'] = 0;

		return $state;
	}
	
	//get data of actual cer
	

	$sql = "select sum(cer_revenue_watches) as w, " . 
		   " sum(cer_revenue_jewellery) as j, " .  
	       " sum(cer_revenue_accessories) as f, " . 
		   " sum(cer_revenue_customer_service) as c " . 
		   " from cer_revenues " .
		   " where cer_revenue_cer_version = 0 " . 
		   " and cer_revenue_project = " . dbquote($project_id);

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$total_revenues_cer = $row["w"] + $row["j"] + $row["f"] +  $row["c"];
	}


	$sql = "select sum(cer_expense_amount) as e " . 
		   " from cer_expenses " .
		   " where cer_expense_cer_version = 0 " . 
		   " and cer_expense_project = " . dbquote($project_id);

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$total_expenses_cer = $row["e"];
	}


	$sql = "select sum(cer_investment_amount_cer_loc) as i " . 
		   " from cer_investments " .
		   " where cer_investment_cer_version = 0 " . 
		   " and cer_investment_project = " . dbquote($project_id);

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$total_investments_cer = $row["i"];
	}

	//get latest ln_version
	$total_revenues_ln = 0;
	$total_expenses_ln = 0;
	$total_investments_ln = 0;

	$sql = "select cer_basicdata_version " .
		   " from cer_basicdata ".
		   " where cer_basicdata_project = " . dbquote($project_id) .
		   " and cer_basicdata_version > 0 and cer_basicdata_version_context = 'ln' " .
		   " order by cer_basicdata_version desc";

	$res = mysql_query($sql) or dberror($sql);
	
	if($row = mysql_fetch_assoc($res))
	{
		$ln_version = $row["cer_basicdata_version"];
		$sql = "select sum(cer_revenue_watches) as w, " . 
			   " sum(cer_revenue_jewellery) as j, " .  
			   " sum(cer_revenue_accessories) as f, " . 
			   " sum(cer_revenue_customer_service) as c " . 
			   " from cer_revenues " .
			   " where cer_revenue_cer_version =  " . $ln_version .
			   " and cer_revenue_project = " . dbquote($project_id);

		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$total_revenues_ln = $row["w"] + $row["j"] + $row["f"] +  $row["c"];
		}


		$sql = "select sum(cer_expense_amount) as e " . 
			   " from cer_expenses " .
			   " where cer_expense_cer_version = " . $ln_version .
			   " and cer_expense_project = " . dbquote($project_id);

		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$total_expenses_ln = $row["e"];
		}


		$sql = "select sum(cer_investment_amount_cer_loc) as i " . 
			   " from cer_investments " .
			   " where cer_investment_cer_version = " . $ln_version .
			   " and cer_investment_project = " . dbquote($project_id);

		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$total_investments_ln = $row["i"];
		}
	}
	
	$state['total_revenues_cer'] = $total_revenues_cer;
	$state['total_revenues_ln'] = $total_revenues_ln;

	$state['total_expenses_cer'] = $total_expenses_cer;
	$state['total_expenses_ln'] = $total_expenses_ln;

	$state['total_investments_cer'] = $total_investments_cer;
	$state['total_investments_ln'] = $total_investments_ln;

	$cer_amount = round(($total_revenues_cer - $total_expenses_cer)/1000, 0);
	$ln_amount = round(($total_revenues_ln - $total_expenses_ln)/1000, 0);


	if((1*$ln_amount > 1*$cer_amount)
		or round($total_investments_ln/1000, 0) < round($total_investments_cer/1000, 0))
	{
		$state['state'] = true;
		return $state;
	}

	$state['state'] = false;
	return $state;

}

/********************************************************************
    get agreed opening date of a former CER Version
*********************************************************************/
function get_version_agreed_opening_date($project_id = 0 , $agreed_opening_date = 0, $version = 0, $context = '')
{
	
	//get submission date
	$submission_date = '';
	if($context != 'ln' and $context != 'cer')
	{
		return $agreed_opening_date;
	}
	elseif($context == 'ln')
	{
		$sql = 'SELECT ln_basicdata_submitted, ln_basicdata_resubmitted ' . 
			   'FROM ln_basicdata ' . 
			   'WHERE ln_basicdata_project = ' . dbquote($project_id) . 
			   ' and ln_basicdata_version_cer_version = ' . dbquote($version) .  
			   ' ORDER BY ln_basicdata_version DESC';

		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			if($row['ln_basicdata_resubmitted'] > $row['ln_basicdata_submitted'])
			{
				$submission_date = $row['ln_basicdata_resubmitted'];
			}
			else
			{
				$submission_date = $row['ln_basicdata_submitted'];
			}
		}
	}
	elseif($context == 'cer')
	{

		$sql = 'SELECT cer_basicdata_submitted, cer_basicdata_resubmitted ' . 
			   'FROM cer_basicdata ' . 
			   'WHERE cer_basicdata_project = ' . dbquote($project_id) . 
			   ' and cer_basicdata_version = ' . dbquote($version) .  
			   ' ORDER BY cer_basicdata_version DESC';

		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			if($row['cer_basicdata_resubmitted'] > $row['cer_basicdata_submitted'])
			{
				$submission_date = $row['cer_basicdata_resubmitted'];
			}
			else
			{
				$submission_date = $row['cer_basicdata_submitted'];
			}
		}

	}

	
	if($submission_date == '')
	{
		return $agreed_opening_date;
	}
	
	$sql = 'select projecttracking_oldvalue from projecttracking '. 
		   ' where projecttracking_field = "project_real_opening_date" '. 
		   ' and projecttracking_project_id = ' . dbquote($project_id) . 
		   ' and str_to_date(projecttracking_oldvalue,"%d.%m.%Y") <= ' . dbquote($submission_date) . 
		   ' order by projecttracking_oldvalue DESC';

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		return $row["projecttracking_oldvalue"];
	}

	return $agreed_opening_date;

}

/********************************************************************
    get brand manager
*********************************************************************/
function get_brand_manager($address_id = 0, $country_id = 0)
{
	$user_data = array();
	
	
	$sql = "select user_id, user_email, user_firstname, user_name " .
		   "from user_roles " . 
		   "left join users on user_id = user_role_user " .
		   "left join addresses on address_id = user_address " . 
		   " where user_role_role = 15 " . 
		   " and address_id = " . dbquote($address_id) . 
		   " and user_active = 1";

	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$user_data = $row;
		return $user_data;
	}
	
	//sub or agent
	$sql = "select user_id, user_email, user_firstname, user_name " .
		   "from user_roles " . 
		   "left join users on user_id = user_role_user " .
		   "left join addresses on address_id = user_address " . 
		   " where user_role_role = 15 " . 
		   " and address_country = " . dbquote($country_id) . 
		   " and user_active = 1 " . 
		   " and address_client_type in (1, 2)";
	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$user_data = $row;
		return $user_data;
	}


	//address has no brand manager
	$sql = "select user_id, user_email, user_firstname, user_name " .
		   "from country_access " . 
		   "left join users on user_id = country_access_user " .
		   "left join user_roles on user_role_user = user_id " . 
		   "left join addresses on address_id = user_address " . 
		   " where user_role_role = 15 " . 
		   " and address_country = " . dbquote($country_id) . 
		   " and user_active = 1";

	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$user_data = $row;
	}

	return $user_data;
}

/********************************************************************
    get head of controlling
*********************************************************************/
function get_head_of_controlling($address_id = 0, $country_id = 0)
{
	$user_data = array();
	
	
	$sql = "select user_id, user_email, user_firstname, user_name " .
		   "from user_roles " . 
		   "left join users on user_id = user_role_user " .
		   "left join addresses on address_id = user_address " . 
		   " where user_role_role = 62 " . 
		   " and address_id = " . dbquote($address_id) . 
		   " and user_active = 1";

	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$user_data = $row;
		return $user_data;
	}
	
	//sub or agent
	$sql = "select user_id, user_email, user_firstname, user_name " .
		   "from user_roles " . 
		   "left join users on user_id = user_role_user " .
		   "left join addresses on address_id = user_address " . 
		   " where user_role_role = 62 " . 
		   " and address_country = " . dbquote($country_id) . 
		   " and user_active = 1 " . 
		   " and address_client_type in (1, 2)";
	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$user_data = $row;
		return $user_data;
	}


	return $user_data;
}


/********************************************************************
    get finance manager
*********************************************************************/
function get_finance_manager($address_id = 0, $country_id = 0)
{
	$user_data = array();
	
	$sql = "select user_id, user_email, user_firstname, user_name " .
		   "from user_roles " . 
		   "left join users on user_id = user_role_user " .
		   "left join addresses on address_id = user_address " . 
		   " where user_role_role = 45 " . 
		   " and address_id = " . dbquote($address_id) . 
		   " and user_active = 1";

	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$user_data = $row;
		return $user_data;
	}

	//sub or agent
	$sql = "select user_id, user_email, user_firstname, user_name " .
		   "from user_roles " . 
		   "left join users on user_id = user_role_user " .
		   "left join addresses on address_id = user_address " . 
		   " where user_role_role = 45 " . 
		   " and address_country = " . dbquote($country_id) . 
		   " and user_active = 1 " . 
		   " and address_client_type in (1, 2)";

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$user_data = $row;
		return $user_data;
	}


	//address has no brand manager
	$sql = "select user_id, user_email, user_firstname, user_name " .
		   "from country_access " . 
		   "left join users on user_id = country_access_user " .
		   "left join user_roles on user_role_user = user_id " . 
		   "left join addresses on address_id = user_address " . 
		   " where user_role_role = 45 " . 
		   " and address_country = " . dbquote($country_id) . 
		   " and user_active = 1";

	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$user_data = $row;
	}

	return $user_data;
}


/********************************************************************
    get country manager
*********************************************************************/
function get_country_manager($address_id = 0, $country_id = 0)
{
	$user_data = array();
	
	$sql = "select user_id, user_email, user_firstname, user_name " .
		   "from user_roles " . 
		   "left join users on user_id = user_role_user " .
		   "left join addresses on address_id = user_address " . 
		   " where user_role_role = 61 " . 
		   " and address_id = " . dbquote($address_id) . 
		   " and user_active = 1";

	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$user_data = $row;
		return $user_data;
	}

	//sub or agent
	$sql = "select user_id, user_email, user_firstname, user_name " .
		   "from user_roles " . 
		   "left join users on user_id = user_role_user " .
		   "left join addresses on address_id = user_address " . 
		   " where user_role_role = 61 " . 
		   " and address_country = " . dbquote($country_id) . 
		   " and user_active = 1 " . 
		   " and address_client_type in (1, 2)";

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$user_data = $row;
		return $user_data;
	}


	//address has no county manager
	$sql = "select user_id, user_email, user_firstname, user_name " .
		   "from country_access " . 
		   "left join users on user_id = country_access_user " .
		   "left join user_roles on user_role_user = user_id " . 
		   "left join addresses on address_id = user_address " . 
		   " where user_role_role = 61 " . 
		   " and address_country = " . dbquote($country_id) . 
		   " and user_active = 1";

	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$user_data = $row;
	}

	return $user_data;
}


/********************************************************************
    get head of controlling
*********************************************************************/
function get_retail_manager_country($address_id = 0, $country_id = 0)
{
	$user_data = array();
	
	
	$sql = "select user_id, user_email, user_firstname, user_name " .
		   "from user_roles " . 
		   "left join users on user_id = user_role_user " .
		   "left join addresses on address_id = user_address " . 
		   " where user_role_role = 16 " . 
		   " and address_id = " . dbquote($address_id) . 
		   " and user_active = 1";

	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$user_data = $row;
		return $user_data;
	}
	
	//sub or agent
	$sql = "select user_id, user_email, user_firstname, user_name " .
		   "from user_roles " . 
		   "left join users on user_id = user_role_user " .
		   "left join addresses on address_id = user_address " . 
		   " where user_role_role = 16 " . 
		   " and address_country = " . dbquote($country_id) . 
		   " and user_active = 1 " . 
		   " and address_client_type in (1, 2)";
	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$user_data = $row;
		return $user_data;
	}


	return $user_data;
}

/********************************************************************
    get head of controlling
*********************************************************************/
function get_regional_sales_manager($country_id = 0)
{
	$user_data = array();
	

	$sql = "select user_firstname, user_email, user_name from users " . 
		     "left join user_roles on user_role_user = user_id " . 
		     "left join country_access on country_access_user = user_id " . 
		     "where country_access_country  = " . dbquote($country_id) .
		     " and user_active = 1 and user_role_role = 33";

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$user_data = $row;
		return $user_data;
	}

	return $user_data;
}


/********************************************************************
    get pos_investments
*********************************************************************/
function get_pos_investment_data($pos_id = 0, $project_kind = 0, $pos_type = 0)
{
	
	$project_total = 0;
	$ln_total = 0;
	$cer_total = 0;
	
	//get pos data
	$sql = "select posaddress_id from posaddresses
	       where posaddress_id = " . $pos_id;

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
			
		//get the latest project of the same kind
		$sql = 'select project_id, posorder_order, project_postype 
			   from posorders 
			   left join orders on order_id = posorder_order 
			   left join projects on project_order = order_id  
			   where posorder_posaddress = ' . $pos_id . 
			   ' and project_actual_opening_date is not NULL and project_actual_opening_date <> "0000-00-00" 
			    and posorder_order > 0 
			    and posorder_type = 1 
			    and project_projectkind in (' . $project_kind . ') 
				and project_postype in (' . $pos_type . ') 
			    order by project_id DESC ';

		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$project_data = $row;
			//get cms real costs
			$sql = "select sum(costsheet_real_amount) as total_real_cost
			       from costsheets where costsheet_is_in_cms = 1
				   and costsheet_is_in_cms = 1
				   and costsheet_project_id = " . $project_data['project_id'];
			
			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				$project_total = $row['total_real_cost'];
			}


			//get ln investments
			$sql = "select sum(cer_investment_amount_cer_loc) as total_investments
			       from cer_investments 
				   left join cer_basicdata on cer_basicdata_project =  cer_investment_project
				   where cer_basicdata_version_context = 'ln'
				   and cer_investment_project = " .  $project_data['project_id'] .
				   " order by cer_basicdata_id DESC";
			
			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				$ln_total = $row['total_investments'];
			}


			//get cer investments
			$sql = "select sum(cer_investment_amount_cer_loc_approved) as total_investments
			       from cer_investments 
				   where cer_investment_cer_version = 0
				   and cer_investment_project = " .  $project_data['project_id'];
			
			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				$cer_total = $row['total_investments'];
			}


		}

		

		if($project_total == 0 and $cer_total == 0) {
			//get the latest ongoing project of the same kind
			$sql = 'select project_id, posorder_order, project_postype 
				   from posorders 
				   left join orders on order_id = posorder_order 
				   left join projects on project_order = order_id  
				   where posorder_posaddress = ' . $pos_id . 
				   ' and order_actual_order_state_code < "890" 
					and posorder_order > 0 
					and posorder_type = 1 
					and project_projectkind in (' . $project_kind . ') 
					and project_postype in (' . $pos_type . ') 
					order by project_id DESC ';



			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				$project_data = $row;
				//get cms real costs
				$sql = "select sum(costsheet_real_amount) as total_real_cost
			            from costsheets where costsheet_is_in_cms = 1
				        and costsheet_is_in_cms = 1
				        and costsheet_project_id = " . $project_data['project_id'];
				
				$res = mysql_query($sql) or dberror($sql);
				if($row = mysql_fetch_assoc($res))
				{
					$project_total = $row['total_real_cost'];
				}


				//get ln investments
				$sql = "select sum(cer_investment_amount_cer_loc) as total_investments
					   from cer_investments 
					   left join cer_basicdata on cer_basicdata_project =  cer_investment_project
					   where cer_basicdata_version_context = 'ln'
					   and cer_investment_project = " .  $project_data['project_id'] .
					   " order by cer_basicdata_id DESC";
				
				$res = mysql_query($sql) or dberror($sql);
				if($row = mysql_fetch_assoc($res))
				{
					$ln_total = $row['total_investments'];
				}


				//get cer investments
				$sql = "select sum(cer_investment_amount_cer_loc_approved) as total_investments
					   from cer_investments 
					   where cer_investment_cer_version = 0
					   and cer_investment_project = " .  $project_data['project_id'];
				
				$res = mysql_query($sql) or dberror($sql);
				if($row = mysql_fetch_assoc($res))
				{
					$cer_total = $row['total_investments'];
				}


			}
		}

		if($project_total == 0 and $cer_total == 0) {
			//get the latest ongoing project of the same kind from pipeline
			$sql = 'select project_id, posorder_order, project_postype 
				   from posorderspipeline 
				   left join orders on order_id = posorder_order 
				   left join projects on project_order = order_id  
				   where posorder_posaddress = ' . $pos_id . 
				   ' and order_actual_order_state_code < "890" 
					and posorder_order > 0 
					and posorder_type = 1 
					and project_projectkind in (' . $project_kind . ') 
					and project_postype in (' . $pos_type . ') 
					order by project_id DESC ';

			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				$project_data = $row;
				//get cms real costs
				$sql = "select sum(costsheet_real_amount) as total_real_cost
			            from costsheets where costsheet_is_in_cms = 1
				        and costsheet_is_in_cms = 1
				        and costsheet_project_id = " . $project_data['project_id'];
				
				$res = mysql_query($sql) or dberror($sql);
				if($row = mysql_fetch_assoc($res))
				{
					$project_total = $row['total_real_cost'];
				}


				//get ln investments
				$sql = "select sum(cer_investment_amount_cer_loc) as total_investments
					   from cer_investments 
					   left join cer_basicdata on cer_basicdata_project =  cer_investment_project
					   where cer_basicdata_version_context = 'ln'
					   and cer_investment_project = " .  $project_data['project_id'] .
					   " order by cer_basicdata_id DESC";
				
				$res = mysql_query($sql) or dberror($sql);
				if($row = mysql_fetch_assoc($res))
				{
					$ln_total = $row['total_investments'];
				}


				//get cer investments
				$sql = "select sum(cer_investment_amount_cer_loc_approved) as total_investments
					   from cer_investments 
					   where cer_investment_cer_version = 0
					   and cer_investment_project = " .  $project_data['project_id'];
				
				$res = mysql_query($sql) or dberror($sql);
				if($row = mysql_fetch_assoc($res))
				{
					$cer_total = $row['total_investments'];
				}


			}
		}
	}

	return array('project_total'=>$project_total, 'ln_total'=>$ln_total, 'cer_total'=>$cer_total);
}

?>