<?php

$debug_output = array();
//fixed rents
$ids = array();

if(param("pid") > 0) //project
{
	$sql = "select * " .
		   "from cer_expenses " .
		   "left join cer_expense_types on cer_expense_type_id = cer_expense_type " . 
		   "where cer_expense_cer_version = 0 and cer_expense_type = 2 " . 
		   " and cer_expense_project = " . param("pid") .
		   " order by cer_expense_year";
}
else //draft
{
	$sql = "select * " .
		   "from cer_draft_expenses " .
		   "left join cer_expense_types on cer_expense_type_id = cer_expense_type " . 
		   "where cer_expense_type = 2 " . 
		   " and cer_expense_draft_id = " . param("did") .
		   " order by cer_expense_year";

}

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$ids[$row["cer_expense_year"]] = $row["cer_expense_id"];
}


//tax
$ids2 = array();

if(param("pid") > 0) //project
{
	$sql = "select * " .
		   "from cer_expenses " .
		   "left join cer_expense_types on cer_expense_type_id = cer_expense_type " . 
		   "where cer_expense_cer_version = 0 and cer_expense_type = 18 " . 
		   " and cer_expense_project = " . param("pid") .
		   " order by cer_expense_year";
}
else //draft
{
	$sql = "select * " .
		   "from cer_draft_expenses " .
		   "left join cer_expense_types on cer_expense_type_id = cer_expense_type " . 
		   "where cer_expense_type = 18 " . 
		   " and cer_expense_draft_id = " . param("did") .
		   " order by cer_expense_year";

}

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$ids2[$row["cer_expense_year"]] = $row["cer_expense_id"];
}

//passenger index amounts
$ids3 = array();

if(param("pid") > 0) //project
{
	$sql = "select * " .
		   "from cer_expenses " .
		   "left join cer_expense_types on cer_expense_type_id = cer_expense_type " . 
		   "where cer_expense_cer_version = 0 and cer_expense_type = 19 " . 
		   " and cer_expense_project = " . param("pid") .
		   " order by cer_expense_year";
}
else //draft
{
	$sql = "select * " .
		   "from cer_draft_expenses " .
		   "left join cer_expense_types on cer_expense_type_id = cer_expense_type " . 
		   "where cer_expense_type = 19 " . 
		   " and cer_expense_draft_id = " . param("did") .
		   " order by cer_expense_year";

}

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$ids3[$row["cer_expense_year"]] = $row["cer_expense_id"];
}

$amounts_per_month = array();
$parts_of_months = array();
$increase_per_year = array();
$index_per_year = array();


$increased_amounts = array();
$total_increased_amounts = array();
$total_increase_rates = array();
$total_full_increase_rates = array();
$last_amounts_payed = array();

$tax_rates = array();
$passenger_rates = array();


foreach($price_records as $key=>$record)
{
	$fixed_rent_id = $key;
	$fy = $record["fy"];
	$fm = $record["fm"];
	$fd = $record["fd"];

	$ty = $record["ty"];
	$tm = $record["tm"];
	$td = $record["td"];
	
	$unit = $record["unit"];
	$surface = $record["surface"];

	$index_rate = 0;
	$increase_rate = 0;
	$inflation_rate = 0;

	
	if($record["indexrate"] > 0)
	{
		$index_rate =  (float)($record["indexrate"]/100);
	}

	if($record["increaserate"] > 0)
	{
		$increase_rate = (float)($record["increaserate"]/100);
	}

	if($record["inflationrate"] > 0)
	{
		$inflation_rate = (float)($record["inflationrate"]/100);
	}
	



	$tax_rate =  (float)($record["taxrate"]/100);
	$passenger_rate = (float)($record["passengerrate"]/100);

	
	
	//set parameters fro index rate
	
	$index_start_year = $record["index_start_year"];
	$index_start_month = $record["index_start_month"];
	$index_start_day = $record["index_start_day"];



	$part_of_index_month = 1;
	
	if($index_start_day > 1)
	{
		$number_of_days_in_month = cal_days_in_month(CAL_GREGORIAN, $index_start_month, $index_start_year);
		$part_of_index_month = ($number_of_days_in_month-$index_start_day+1)/$number_of_days_in_month;
	}

		
	if($index_start_year == $fy)
	{
		if($index_start_month < $fm){$index_start_month = $fm;}
		
		$monthcount_index_rate = 12 - ($index_start_month - $fm);
	}
	else
	{
		$monthcount_index_rate = 13 - $index_start_month;
	}
	
	$inflator = 0;



	//set parameters fro icrease rate
	$increase_start_year = $record["increase_start_year"];
	$increase_start_month = $record["increase_start_month"];
	$increase_start_day = $record["increase_start_day"];
	$part_of_increase_month = 1;
	if($increase_start_day > 1)
	{
		$number_of_days_in_month = cal_days_in_month(CAL_GREGORIAN, $increase_start_month, $increase_start_year);
		$part_of_increase_month = ($number_of_days_in_month-$increase_start_day+1)/$number_of_days_in_month;
	}
		
	if($increase_start_year == $fy)
	{
		if($increase_start_month < $fm){$increase_start_month = $fm;}
		
		$monthcount_increase_rate = 12 - ($increase_start_month - $fm);
	}
	else
	{
		$monthcount_increase_rate = 13 - $increase_start_month;
	}
	$inflator2 = 0;


	//set parameters fro icrease rate
	$inflation_start_year = $record["inflation_start_year"];
	$inflation_start_month = $record["inflation_start_month"];
	$inflation_start_day = $record["inflation_start_day"];
	$part_of_inflation_month = 1;
	if($inflation_start_day > 1)
	{
		$number_of_days_in_month = cal_days_in_month(CAL_GREGORIAN, $inflation_start_month, $inflation_start_year);
		$part_of_inflation_month = ($number_of_days_in_month-$inflation_start_day+1)/$number_of_days_in_month;
	}
		
	if($inflation_start_year == $fy)
	{
		if($inflation_start_month < $fm){$inflation_start_month = $fm;}
		
		$monthcount_inflation_rate = 12 - ($inflation_start_month - $fm);
	}
	else
	{
		$monthcount_inflation_rate = 13 - $inflation_start_month;
	}
	$inflator3 = 0;
	
	

	$price = 0;

	if($unit == 1 or $unit == 2) //price per month and surface unit
	{
		$price = $record["amount"] * $record["surface"];
	}
	elseif($unit == 3 or $unit == 4) //price per year and surface unit
	{
		$price = ($record["amount"] * $record["surface"])/12;
	}
	elseif($unit == 5) //price per month and total surface
	{
		if($surface > 0)
		{
			$price = ($record["amount"] * $record["surface"])/$surface;
		}
	}
	elseif($unit == 6) //price per year and total surface
	{
		if($surface > 0)
		{
			$price = ($record["amount"] * $record["surface"])/12/$surface;
		}
	}


	for($year = $fy; $year <= $ty; $year++)
	{
		if($year == $fy and $fy == $ty)
		{
			for($month = $fm; $month <= $tm; $month++)
			{
				if($month < $tm)
				{
					$part_of_month = 1;
					$number_of_days_in_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);
					if($month == $fm and $fd !=1)
					{
						$part_of_month = ($number_of_days_in_month-$fd+1)/$number_of_days_in_month;
					}
				}
				else
				{
					$part_of_month = 1;
					$number_of_days_in_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);
					if($month == $tm and $td != $number_of_days_in_month)
					{
						$part_of_month = ($td)/$number_of_days_in_month;
					}
				}

				if(array_key_exists($year . " " . $month, $amounts_per_month))
				{
					$amounts_per_month[$year . " " . $month] = $amounts_per_month[$year . " " . $month] + $price*$part_of_month;
				}
				else
				{
					$amounts_per_month[$year . " " . $month] = $price*$part_of_month;
				}

				//$increased_amounts[$year . " " . $month] = $amounts_per_month[$year . " " . $month];
				$increased_amounts[$year . " " . $month] = $price;
				$total_increase_rates[$year . " " . $month] = 0;
				$total_full_increase_rates[$year . " " . $month] = 0;
				$tax_rates[$year . " " . $month] = $tax_rate;
				$passenger_rates[$year . " " . $month] = $passenger_rate;

				
				$parts_of_months[$year . " " . $month] = $part_of_month;
				$debug_output[$year . " " . $month] = array("amount"=>$amounts_per_month[$year . " " . $month], "part_of_month"=>$part_of_month, "price"=>$price); 
			}

		}
		elseif($fy+1 == $ty and $year == $fy )
		{
			for($month = $fm; $month <= 12; $month++)
			{
				$part_of_month = 1;
				$number_of_days_in_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);
				if($month == $fm and $fd !=1)
				{
					$part_of_month = ($number_of_days_in_month-$fd+1)/$number_of_days_in_month;
				}
				
				if(array_key_exists($year . " " . $month, $amounts_per_month))
				{
					$amounts_per_month[$year . " " . $month] = $amounts_per_month[$year . " " . $month] + $price*$part_of_month;
				}
				else
				{
					$amounts_per_month[$year . " " . $month] = $price*$part_of_month;
				}

				$parts_of_months[$year . " " . $month] = $part_of_month;

				//$increased_amounts[$year . " " . $month] = $amounts_per_month[$year . " " . $month];
				$increased_amounts[$year . " " . $month] = $price;

				$total_increase_rates[$year . " " . $month] = 0;
				$total_full_increase_rates[$year . " " . $month] = 0;
				$tax_rates[$year . " " . $month] = $tax_rate;
				$passenger_rates[$year . " " . $month] = $passenger_rate;


				$debug_output[$year . " " . $month] = array("amount"=>$amounts_per_month[$year . " " . $month], "part_of_month"=>$part_of_month, "price"=>$price); 
			}
		}
		elseif($fy+1 == $ty and $year == $ty )
		{
			for($month = 1; $month <= $tm; $month++)
			{
				$part_of_month = 1;
				$number_of_days_in_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);
				if($month == $tm and $td != $number_of_days_in_month)
				{
					$part_of_month = ($td)/$number_of_days_in_month;
				}
				
				if(array_key_exists($year . " " . $month, $amounts_per_month))
				{

					$amounts_per_month[$year . " " . $month] = $amounts_per_month[$year . " " . $month] + $price*$part_of_month;
				}
				else
				{
					$amounts_per_month[$year . " " . $month] = $price*$part_of_month;
				}

				//$increased_amounts[$year . " " . $month] = $amounts_per_month[$year . " " . $month];
				$increased_amounts[$year . " " . $month] = $price;
				$total_increase_rates[$year . " " . $month] = 0;
				$total_full_increase_rates[$year . " " . $month] = 0;
				$tax_rates[$year . " " . $month] = $tax_rate;
				$passenger_rates[$year . " " . $month] = $passenger_rate;

				$parts_of_months[$year . " " . $month] = $part_of_month;
				$debug_output[$year . " " . $month] = array("amount"=>$amounts_per_month[$year . " " . $month], "part_of_month"=>$part_of_month, "price"=>$price); 
			}
		}
		elseif($year == $fy)
		{
			for($month = $fm; $month <= 12; $month++)
			{
				$part_of_month = 1;
				$number_of_days_in_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);
				if($month == $fm and $fd !=1)
				{
					$part_of_month = ($number_of_days_in_month-$fd+1)/$number_of_days_in_month;
				}

				if(array_key_exists($year . " " . $month, $amounts_per_month))
				{
					$amounts_per_month[$year . " " . $month] = $amounts_per_month[$year . " " . $month] + $price*$part_of_month;
				}
				else
				{
					$amounts_per_month[$year . " " . $month] = $price*$part_of_month;
				}

				//$increased_amounts[$year . " " . $month] = $amounts_per_month[$year . " " . $month];
				$increased_amounts[$year . " " . $month] = $price;
				$total_increase_rates[$year . " " . $month] = 0;
				$total_full_increase_rates[$year . " " . $month] = 0;
				$tax_rates[$year . " " . $month] = $tax_rate;
				$passenger_rates[$year . " " . $month] = $passenger_rate;

				$parts_of_months[$year . " " . $month] = $part_of_month;
				$debug_output[$year . " " . $month] = array("amount"=>$amounts_per_month[$year . " " . $month], "part_of_month"=>$part_of_month, "price"=>$price); 
			}
		}
		elseif($year > $fy and $year < $ty)
		{
			for($month = 1; $month <= 12; $month++)
			{
				$part_of_month = 1;
				if(array_key_exists($year . " " . $month, $amounts_per_month))
				{
					$amounts_per_month[$year . " " . $month] = $amounts_per_month[$year . " " . $month]+$price;
				}
				else
				{
					$amounts_per_month[$year . " " . $month] = $price;
				}

				//$increased_amounts[$year . " " . $month] = $amounts_per_month[$year . " " . $month];
				$increased_amounts[$year . " " . $month] = $price;
				$total_increase_rates[$year . " " . $month] = 0;
				$total_full_increase_rates[$year . " " . $month] = 0;
				$tax_rates[$year . " " . $month] = $tax_rate;
				$passenger_rates[$year . " " . $month] = $passenger_rate;

				$parts_of_months[$year . " " . $month] = $part_of_month;
				$debug_output[$year . " " . $month] = array("amount"=>$amounts_per_month[$year . " " . $month], "part_of_month"=>1, "price"=>$price); 
			}
		}
		else
		{
			for($month = 1; $month <= $tm; $month++)
			{
				$part_of_month = 1;
				$number_of_days_in_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);
				if($month == $tm and $td != $number_of_days_in_month)
				{
					$part_of_month = ($td)/$number_of_days_in_month;
				}
				
				if(array_key_exists($year . " " . $month, $amounts_per_month))
				{
					$amounts_per_month[$year . " " . $month] = $amounts_per_month[$year . " " . $month] + $price*$part_of_month;
				}
				else
				{
					$amounts_per_month[$year . " " . $month] = $price*$part_of_month;
				}

				//$increased_amounts[$year . " " . $month] = $amounts_per_month[$year . " " . $month];
				$increased_amounts[$year . " " . $month] = $price;
				$total_increase_rates[$year . " " . $month] = 0;
				$total_full_increase_rates[$year . " " . $month] = 0;
				$tax_rates[$year . " " . $month] = $tax_rate;
				$passenger_rates[$year . " " . $month] = $passenger_rate;

				$parts_of_months[$year . " " . $month] = $part_of_month;
				$debug_output[$year . " " . $month] = array("amount"=>$amounts_per_month[$year . " " . $month], "part_of_month"=>$part_of_month, "price"=>$price); 
			}
		}
	}


		
	//calculate total icrease rates per month
	$start_indexing = 0;
	$start_increaseing = 0;
	$start_inflateing = 0;
	for($year = $fy; $year <= $ty; $year++)
	{
		
		$start_month = $fm;
		$end_month = $tm;
		if($year == $fy and $fy == $ty)
		{
			$start_month = $fm;
			$end_month = $tm;
		}
		elseif($fy+1 == $ty and $year == $fy )
		{
			$start_month = $fm;
			$end_month = 12;
		}
		elseif($fy+1 == $ty and $year == $ty )
		{
			$start_month = 1;
			$end_month = $tm;
		}
		elseif($year == $fy)
		{
			$start_month = $fm;
			$end_month = 12;
		}
		elseif($year > $fy and $year < $ty)
		{
			$start_month = 1;
			$end_month = 12;
		}
		else
		{
			$start_month = 1;
			$end_month = $tm;
		}

		for($month = $start_month; $month <= $end_month; $month++)
		{
			if($year == $index_start_year and $month == $index_start_month)
			{
				$start_indexing = 1;
				$month_count_index = 0;
			}

			if($start_indexing == 1)
			{
				
				if($month_count_index == 0)
				{
					if($year == $index_start_year 
						and $index_start_month == $month 
						and $part_of_index_month < $parts_of_months[$year . " " . $month])
					{
						$total_increase_rates[$year . " " . $month] = $total_increase_rates[$year . " " . $month] + ($index_rate * $part_of_index_month);
					}
					else
					{
						$total_increase_rates[$year . " " . $month] = $total_increase_rates[$year . " " . $month] + $index_rate;
					}
					$total_full_increase_rates[$year . " " . $month] = $total_full_increase_rates[$year . " " . $month] + $index_rate;
				}
				
				$month_count_index++;
				if($month_count_index == 12)
				{
					$month_count_index = 0;
				}

			}

			//increases
			if($year == $increase_start_year and $month == $increase_start_month)
			{
				$start_increaseing = 1;
				$month_count_increase = 0;
			}

			if($start_increaseing == 1)
			{
				
				if($month_count_increase == 0)
				{
					if($year == $increase_start_year 
						and $increase_start_month == $month 
						and $part_of_increase_month < $parts_of_months[$year . " " . $month] )
					{
						$total_increase_rates[$year . " " . $month] = $total_increase_rates[$year . " " . $month] + ($increase_rate * $part_of_increase_month);
					}
					else
					{
						$total_increase_rates[$year . " " . $month] = $total_increase_rates[$year . " " . $month] + $increase_rate;
					}
					$total_full_increase_rates[$year . " " . $month] = $total_full_increase_rates[$year . " " . $month] + $increase_rate;
				}

				$month_count_increase++;
				if($month_count_increase == 12)
				{
					$month_count_increase = 0;
				}
			}

			//inflation
			if($year == $inflation_start_year and $month == $inflation_start_month)
			{
				$start_inflateing = 1;
				$month_count_inflation = 0;
			}

			if($start_inflateing == 1)
			{
				
				if($month_count_inflation == 0)
				{
					if($year == $inflation_start_year 
						and $inflation_start_month == $month 
						and $part_of_inflation_month < $parts_of_months[$year . " " . $month] )
					{
						$total_increase_rates[$year . " " . $month] = $total_increase_rates[$year . " " . $month] + ($inflation_rate * $part_of_inflation_month);
					}
					else
					{
						$total_increase_rates[$year . " " . $month] = $total_increase_rates[$year . " " . $month] + $inflation_rate;
					}
					$total_full_increase_rates[$year . " " . $month] = $total_full_increase_rates[$year . " " . $month] + $inflation_rate;
				}
				
				$month_count_inflation++;
				if($month_count_inflation == 12)
				{
					$month_count_inflation = 0;
				}
			}
		}
	}
	

	/*
	foreach($total_increase_rates as $key=>$value)
	{
		echo $key . " " . $value . "<br />";
	}
	*/

	
	
	//calculate increases
	$month_count = 0;
	$last_amount = 0;
	$start_increaseing = 0;
	for($year = $fy; $year <= $ty; $year++)
	{
		$increase_amount = 0;
		
		
		$start_month = $fm;
		$end_month = $tm;
		if($year == $fy and $fy == $ty)
		{
			$start_month = $fm;
			$end_month = $tm;
		}
		elseif($fy+1 == $ty and $year == $fy )
		{
			$start_month = $fm;
			$end_month = 12;
		}
		elseif($fy+1 == $ty and $year == $ty )
		{
			$start_month = 1;
			$end_month = $tm;
		}
		elseif($year == $fy)
		{
			$start_month = $fm;
			$end_month = 12;
		}
		elseif($year > $fy and $year < $ty)
		{
			$start_month = 1;
			$end_month = 12;
		}
		else
		{
			$start_month = 1;
			$end_month = $tm;
		}

		for($month = $start_month; $month <= $end_month; $month++)
		{
			
			if($start_increaseing == 0)
			{
				$start_increaseing = 1;
				$last_amount = $increased_amounts[$year . " " . $month];
			}
			$increase_amount = $last_amount*$total_increase_rates[$year . " " . $month];
				
			$month_count++;
			
			$debug_output[$year . " " . $month]["increase_rate"] = $total_increase_rates[$year . " " . $month];
			$debug_output[$year . " " . $month]["last_uncreased_amount"] = $last_amount;
			$debug_output[$year . " " . $month]["month_count"] = $month_count;
			
			$increased_amounts[$year . " " . $month] = $parts_of_months[$year . " " . $month]*($last_amount + $increase_amount);
			$last_amounts_payed[$fixed_rent_id] = $last_amount + $increase_amount;
			
			$tmp_increased_amount = $last_amount + $last_amount*$total_full_increase_rates[$year . " " . $month];
			
			if(array_key_exists($year . " " . $month, $total_increased_amounts))
			{
				$total_increased_amounts[$year . " " . $month] = $total_increased_amounts[$year . " " . $month] + $increased_amounts[$year . " " . $month];
			}
			else
			{
				$total_increased_amounts[$year . " " . $month] = $increased_amounts[$year . " " . $month];
			}
			$debug_output[$year . " " . $month]["last_increased_amount"] = $total_increased_amounts[$year . " " . $month];
			//if($increased_amounts[$year . " " . $month] > $last_amount)
			if($increased_amounts[$year . " " . $month] != $last_amount)
			{
				$month_count = 0;
				//$last_amount = $increased_amounts[$year . " " . $month];
				$last_amount = $tmp_increased_amount;
				
			}
		}
	}


	//update last calculated amout for fixed rental record amounts
	if($unit == 3 or $unit == 4) //price per year and surface unit
	{
		if($surface > 0)
		{
			$last_amounts_payed[$fixed_rent_id] = $last_amounts_payed[$fixed_rent_id]*12/$surface;
		}
	}
	elseif($unit == 6) //price per year and total surface
	{
		if($surface > 0)
		{
			$last_amounts_payed[$fixed_rent_id] = $last_amounts_payed[$fixed_rent_id]*12;
		}
	}
	if(param("pid") > 0) //project
	{
		$sql = "update cer_fixed_rents " . 
			   "set cer_fixed_rent_last_amount_payed = " . dbquote($last_amounts_payed[$fixed_rent_id]) . 
				" where cer_fixed_rent_id = " . $fixed_rent_id;
	}
	else // draft
	{
		$sql = "update cer_draft_fixed_rents " . 
			   "set cer_fixed_rent_last_amount_payed = " . dbquote($last_amounts_payed[$fixed_rent_id]) . 
				" where cer_fixed_rent_id = " . $fixed_rent_id;
	}
	mysql_query($sql) or dberror($sql);

}


foreach($amounts_per_month as $key=>$amount)
{
	$increase_amount = 0;

	if(array_key_exists($key, $total_increased_amounts))
	{
		$increase_amount = $total_increased_amounts[$key] - $amounts_per_month[$key];
		$debug_output[$key]["increased_amount"] = $total_increased_amounts[$key];
	}
	$amounts_per_month[$key] = $amounts_per_month[$key] + $increase_amount;
	
	$debug_output[$key]["final_amount"] = "<strong>" . $amounts_per_month[$key] . "</strong>";
}


//caclulate taxes
$tax_amounts = array();
foreach($amounts_per_month as $key=>$amount)
{
	$tax_amounts[$key] = $amounts_per_month[$key]*$tax_rates[$key];
	$debug_output[$key]["tax_rate"] = $tax_rates[$key];
	$debug_output[$key]["tax"] = $tax_amounts[$key];
}

//calculate passenger index rate
$passenger_amounts = array();
foreach($amounts_per_month as $key=>$amount)
{
	$passenger_amounts[$key] = $amounts_per_month[$key]*$passenger_rates[$key];
	$debug_output[$key]["passenger_rate"] = $passenger_rates[$key];
	$debug_output[$key]["passenger_amount"] = $passenger_amounts[$key];
}



//caclulate fixed rent per year
$rents_per_year = array();
$taxes_per_year = array();
$passenger_rates_per_year = array();
foreach($ids as $year=>$expense_id)
{
	$rents_per_year[$year] = 0;
	$taxes_per_year[$year] = 0;
	$passenger_rates_per_year[$year] = 0;
}


foreach($amounts_per_month as $key=>$amount)
{
	$year = substr($key, 0, 4);
	$month = substr($key, 5, strlen($key) - 1);
	$rents_per_year[$year] = $rents_per_year[$year] + $amount;
	$taxes_per_year[$year] = $taxes_per_year[$year] + $tax_amounts[$key];
	$passenger_rates_per_year[$year] = $passenger_rates_per_year[$year] + $passenger_amounts[$key];
}

foreach($ids as $year=>$expense_id)
{
	$rents_per_year[$year] = round($rents_per_year[$year], 0);
	$taxes_per_year[$year] = round($taxes_per_year[$year], 0);
	$passenger_rates_per_year[$year] = round($passenger_rates_per_year[$year], 0);
}


if(param("pid") > 0) //project
{
	$sql = "update cer_expenses SET " . 
		   "cer_expense_amount = 0 " .
		   " where cer_expense_type in (2, 18, 19) " . 
		   " and cer_expense_cer_version = 0 and cer_expense_project = " . param("pid");
}
else
{
	$sql = "update cer_draft_expenses SET " . 
		   "cer_expense_amount = 0 " .
		   " where cer_expense_type in (2, 18, 19) " . 
		   " and cer_expense_draft_id = " . param("did");
}
				
$result = mysql_query($sql) or dberror($sql);


//update fixed rents
foreach($ids as $year=>$id)
{
	
	$fields = array();

	if($rents_per_year[$year] > 0) // only update if there are values, prevent from overwriting existing values
	{
		$value = dbquote($rents_per_year[$year]);
		$fields[] = "cer_expense_amount = " . $value;

		
		$value = "current_timestamp";
		$fields[] = "date_modified = " . $value;

		if (isset($_SESSION["user_login"]))
		{
			$value = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value);
		}

		
		if(param("pid") > 0) //project
		{

			$sql = "update cer_expenses set " . join(", ", $fields) . 
					" where cer_expense_id = " . $id;
		}
		else // draft
		{
			$sql = "update cer_draft_expenses set " . join(", ", $fields) . 
					" where cer_expense_id = " . $id;
		}
		mysql_query($sql) or dberror($sql);


		
	}
}

//update tax amounts
foreach($ids2 as $year=>$id)
{
	
	$fields = array();

	
	$value = dbquote($taxes_per_year[$year]);
	$fields[] = "cer_expense_amount = " . $value;

	
	$value = "current_timestamp";
	$fields[] = "date_modified = " . $value;

	if (isset($_SESSION["user_login"]))
	{
		$value = $_SESSION["user_login"];
		$fields[] = "user_modified = " . dbquote($value);
	}

	
	if(param("pid") > 0) //project
	{

		$sql = "update cer_expenses set " . join(", ", $fields) . 
				" where cer_expense_id = " . $id;
	}
	else // draft
	{
		$sql = "update cer_draft_expenses set " . join(", ", $fields) . 
				" where cer_expense_id = " . $id;
	}
	mysql_query($sql) or dberror($sql);
}

//update passenger index amounts
foreach($ids3 as $year=>$id)
{
	
	$fields = array();

	
	$value = dbquote($passenger_rates_per_year[$year]);
	$fields[] = "cer_expense_amount = " . $value;

	
	$value = "current_timestamp";
	$fields[] = "date_modified = " . $value;

	if (isset($_SESSION["user_login"]))
	{
		$value = $_SESSION["user_login"];
		$fields[] = "user_modified = " . dbquote($value);
	}

	
	if(param("pid") > 0) //project
	{

		$sql = "update cer_expenses set " . join(", ", $fields) . 
				" where cer_expense_id = " . $id;
	}
	else // draft
	{
		$sql = "update cer_draft_expenses set " . join(", ", $fields) . 
				" where cer_expense_id = " . $id;
	}

	//echo $sql . "<br />";
	mysql_query($sql) or dberror($sql);
}



//DEBUG OUTPUT
/*
echo '<table>';
foreach($debug_output as $key=>$output)
{
	echo '<table><tr><td colspan="2">';
	echo "<strong>" . $key . "</strong><br />";
	echo '</td></tr>';
	foreach($output as $key2=>$value)
	{
		echo "<tr><td>" . $key2 . "</td><td>" . $value . "</td></tr>";
	}
	echo '<table><tr><td colspan="2">&nbsp;</td></tr>';
}
echo "</table>";
die;
*/


