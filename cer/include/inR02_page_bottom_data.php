<?php
$y = $y + $standard_y+0.8;
$x2 = $x2 - 22;
$x3 = $x3 - 42;
$x4 = $x4 - 42;

	$pdf->SetXY($margin_left,$y);
	$pdf->SetFont("arialn", "B", 3);
	$pdf->Cell(63, 3.5, "", 1, "", "L");

	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(50, 3.5, "Profitablility:", 0, "", "L");
	
	$pdf->SetXY($margin_left,$y+3.5);
	$pdf->Cell(63, 11, " ", 1, "", "L");

	$pdf->SetXY($x1-1,$y);
	$pdf->Cell(20, 3.5, "Retail Margin", 0, "", "L");

	$pdf->SetXY($x2-1,$y);
	$pdf->Cell(20, 3.5, "Wholesale Margin", 0, "", "L");
	

	$pdf->SetXY($x2,$y+3.5);
	$pdf->SetFont("arialn", "B", 3);
	$pdf->Cell(20, 11, " ", 1, "", "L");

	$pdf->SetXY($x1,$y+3.5);
	$pdf->Cell(20, 11, " ", 1, "", "L");

	$y = $y + 3.5;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 3.5, "Net Present Value", 0, "", "L");
	
	
	$pdf->SetXY($x3,$y);
	$pdf->SetFont("arialn", "B", 3);
	$pdf->Cell(40, 3.5, " ", 1, "", "L");
	$pdf->SetXY($x3,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(40, 3.5, "Discount Rate", 0, "", "L");

	$pdf->SetXY($x4,$y);
	$pdf->SetFont("arialn", "B", 3);
	$pdf->Cell(20, 3.5, " ", 1, "", "L");
	$pdf->SetXY($x4,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, (100*$discount_rate) . "%", 0, "", "R");

	$y = $y + 3.5;
	$pdf->SetXY($x3,$y);
	$pdf->SetFont("arialn", "B", 3);
	$pdf->Cell(40, 3.5, " ", 1, "", "L");
	$pdf->SetXY($x3,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(40, 3.5, "Exchange Rate " . $currency_symbol_page_bottom, 0, "", "L");
	
	$pdf->SetXY($x4,$y);
	$pdf->SetFont("arialn", "B", 3);
	$pdf->Cell(20, 3.5, " ", 1, "", "L");
	$pdf->SetXY($x4,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, ($exchange_rate_page_bottom), 0, "", "R");
	$y = $y - 3.5;

	
	$pdf->SetXY($x1,$y);
	$pdf->Cell(20, 3.5, round($exchange_rate*$net_present_value_retail/$exchange_rate_factor/1000, 0), 0, "", "R");

	$pdf->SetXY($x2,$y);
	$pdf->Cell(20, 3.5, round($exchange_rate*$net_present_value_wholesale/$exchange_rate_factor/1000, 0), 0, "", "R");

	$y = $y + $standard_y;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 3.5, "Discounted Cash Flow Return on Investment in %", 0, "", "L");

	if(abs($discounted_cash_flow_retail) < 10000)
	{
		$discounted_cash_flow_retail = round($discounted_cash_flow_retail,2) . "%";
	}
	else
	{
		$discounted_cash_flow_retail = "";
	}
	
	$pdf->SetXY($x1,$y);
	$pdf->Cell(20, 3.5, $discounted_cash_flow_retail, 0, "", "R");

	
	if(abs($discounted_cash_flow_wholesale) < 10000)
	{
		$discounted_cash_flow_wholesale = round($discounted_cash_flow_wholesale,2) . "%";
	}
	else
	{
		$discounted_cash_flow_wholesale = "";
	}
	
	$pdf->SetXY($x2,$y);
	$pdf->Cell(20, 3.5, $discounted_cash_flow_wholesale, 0, "", "R");

	$y = $y + $standard_y;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 3.5, "Pay back period in years", 0, "", "L");

	
	if($pay_back_period_retail)
	{
		$pay_back_period_retail = number_format(round($pay_back_period_retail,2), 2);
	}
	else
	{
		$pay_back_period_retail = "Invest. Period";
	}

	$pdf->SetXY($x1,$y);
	$pdf->Cell(20, 3.5, $pay_back_period_retail, 0, "", "R");

	$pdf->SetXY($x2,$y);
	$pdf->Cell(20, 3.5, number_format(round($pay_back_period_wholesale, 2), 2), 0, "", "R");	




	// sellouts out in the past
	
	$y = $y - 9;
	
	//watches sold out in the past
	// renovation or tekover/renovation or lease renewal or new relocation project
	if(($project["project_projectkind"] == 2 
        or $project["project_projectkind"] == 3 
		or $project["project_projectkind"] == 5)
		or ($project["project_projectkind"] == 6 and $project["project_is_relocation_project"] == 1 and $project["project_relocated_posaddress_id"] > 0)
	   )

	{
		
		$x = $x4 + 22;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 3);
		$pdf->Cell(20,3.5,"",1, 0, 'R', 0);
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(20,3.5,"Sellouts",0, 0, 'R', 0);

		$pdf->SetXY($x,$y +3.5);
		$pdf->SetFont("arialn", "B", 3);
		$pdf->Cell(20,3.5,"",1, 0, 'R', 0);
		$pdf->SetXY($x,$y +3.5);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(20,3.5,"Watches",0, 0, 'R', 0);
		
		
		$pdf->SetXY($x,$y + 7);
		$pdf->SetFont("arialn", "B", 3);
		$pdf->Cell(20,3.5,"",1, 0, 'R', 0);
		$pdf->SetXY($x,$y + 7);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(20,3.5,"Bijoux",0, 0, 'R', 0);
		
				
		
		$x = $x4 + 42;
		$pdf->SetXY($x,$y);
		$sellout_ending_year = (int)substr($cer_basicdata["date_created"], 0, 4);

		if($sellout_ending_year < (int)substr($cer_basicdata["cer_basicdata_firstyear"], 0, 4))
		{
			$sellout_ending_year =(int)substr($cer_basicdata["cer_basicdata_firstyear"], 0, 4);
		}

		if($sellout_ending_year > date("Y"))
		{
			$sellout_ending_year = date("Y");
		}

		$sellout_starting_year =$sellout_ending_year - 3;

		
		for($i=$sellout_starting_year;$i<=($sellout_ending_year);$i++)
		{
			$x = $pdf->getX();
			$pdf->SetFont("arialn", "B", 3);
			$pdf->Cell(20,3.5,"",1, 0, 'R', 0);
			$pdf->SetFont("arialn", "B", 8);
			$pdf->SetXY($x,$y);
			if(array_key_exists($i, $sellouts_watches))
			{
				$pdf->Cell(20,3.5,$i . "(" . $sellouts_months[$i] . ")",0, 0, 'R', 0);
			}
			else
			{
				$pdf->Cell(20,3.5,$i ,0, 0, 'R', 0);
			}


			
		}
		
		$pdf->SetFont("arialn", "", 8);
		$x = $x4 + 42;
		$pdf->SetXY($x,$y+3.5);
		for($i=$sellout_starting_year;$i<=($sellout_ending_year);$i++)
		{
			$x = $pdf->getX();
			$pdf->SetFont("arialn", "B", 3);
			$pdf->Cell(20,3.5,"",1, 0, 'R', 0);
			$pdf->SetFont("arialn", "B", 8);
			$pdf->SetXY($x,$y+3.5);

			if(array_key_exists($i, $sellouts_watches))
			{
				$pdf->Cell(20,3.5,number_format($sellouts_watches[$i], 0, ".", "'"),0, 0, 'R', 0);
			}
			else
			{
				$pdf->Cell(20,3.5,"" ,0, 0, 'R', 0);
			}
			
		}

		$x = $x4 + 42;
		$pdf->SetXY($x,$y+7);
		for($i=$sellout_starting_year;$i<=($sellout_ending_year);$i++)
			{
			
			$x = $pdf->getX();
			$pdf->SetFont("arialn", "B", 3);
			$pdf->Cell(20,3.5,"",1, 0, 'R', 0);
			$pdf->SetFont("arialn", "B", 8);
			$pdf->SetXY($x,$y+7);
			
			if(array_key_exists($i, $sellouts_bjoux))
			{
				$pdf->Cell(20,3.5,number_format($sellouts_bjoux[$i], 0, ".", "'"),0, 0, 'R', 0);
			}
			else
			{
				$pdf->Cell(20,3.5,"" ,0, 0, 'R', 0);
			}
			
		}

	}

	
?>