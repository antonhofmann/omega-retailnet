

<script language="javascript">
	
	$('#brand_selector').change(function() {
		window.location = "cer_application_revenues.php?pid=<?php echo param('pid');?>&brand=" + $(this).val();
	});
	
</script>

<?php

if(array_key_exists("reject_action_" . user_id(), $_SESSION))
	{
		
		if($_SESSION["reject_action_" .  user_id()]['post']['type'] == 'ln')
		{
			?>
			<script language="javascript">
				createWindowWithRemotingUrl2('Reject Lease Negotioation Form', 'reject_submission.php?pid=<?php echo param("pid");?>&type=ln');
				overlayWindow.minimize();
			</script>

			<?php
		}
		elseif($_SESSION["reject_action_" .  user_id()]['post']['type'] == 'af')
		{
			?>
			<script language="javascript">
				createWindowWithRemotingUrl2('Reject AF', 'reject_submission.php?pid=<?php echo param("pid");?>&type=af');
				overlayWindow.minimize();
			</script>

			<?php
		}
		elseif($_SESSION["reject_action_" .  user_id()]['post']['type'] == 'inr03')
		{
			?>
			<script language="javascript">
				createWindowWithRemotingUrl2('Reject INR-03', 'reject_submission.php?pid=<?php echo param("pid");?>&type=inr03');
				overlayWindow.minimize();
			</script>

			<?php
		}
		elseif($_SESSION["reject_action_" .  user_id()]['post']['type'] == 'in03')
		{
			?>
			<script language="javascript">
				createWindowWithRemotingUrl2('Reject Request for Additional Funding', 'reject_submission.php?pid=<?php echo param("pid");?>&type=in03');
				overlayWindow.minimize();
			</script>

			<?php
		}
		elseif($_SESSION["reject_action_" .  user_id()]['post']['type'] == 'cer')
		{
			?>
			<script language="javascript">
				createWindowWithRemotingUrl2('Reject CER', 'reject_submission.php?pid=<?php echo param("pid");?>&type=cer');
				overlayWindow.minimize();
			</script>

			<?php
		}
		elseif (preg_match("/funding/", $_SERVER["SCRIPT_FILENAME"]))
		{
			?>
			<script language="javascript">
				createWindowWithRemotingUrl2('Reject Request for Additional Funding', 'reject_submission.php?pid=<?php echo param("pid");?>&type=in03');
				overlayWindow.minimize();
			</script>

			<?php
		}
		elseif (preg_match("/ln_general/", $_SERVER["SCRIPT_FILENAME"]))
		{
			?>
			<script language="javascript">
				createWindowWithRemotingUrl2('Reject Lease Negotioation Form', 'reject_submission.php?pid=<?php echo param("pid");?>&type=ln');
				overlayWindow.minimize();
			</script>

			<?php
		}
		elseif($form_type == "AF")
		{
			?>
			<script language="javascript">
				createWindowWithRemotingUrl2('Reject AF', 'reject_submission.php?pid=<?php echo param("pid");?>&type=af');
				overlayWindow.minimize();
			</script>

			<?php
		}
		elseif($form_type == "INR03")
		{
			?>
			<script language="javascript">
				createWindowWithRemotingUrl2('Reject INR-03', 'reject_submission.php?pid=<?php echo param("pid");?>&type=inr03');
				overlayWindow.minimize();
			</script>

			<?php
		}
		else
		{
			?>
			<script language="javascript">
				createWindowWithRemotingUrl2('Reject CER', 'reject_submission.php?pid=<?php echo param("pid");?>&type=cer');
				overlayWindow.minimize();
			</script>

			<?php
		}

	}
