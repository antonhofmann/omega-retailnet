<?php



if(!isset($ln_version))
{
	$cer_version = 0;
	$ln_version = 0;
}

$project = get_project(param("pid"));
//update depreciation period in cer_basicdata
$result = update_beginning_period_of_depreciation(param("pid"), $project["project_planned_opening_date"], $project["project_real_opening_date"]);

$cer_basicdata = get_cer_basicdata(param("pid"), $cer_version);

//check if we hav a posorder
$posorder_present = 0;
$sql = "select count(posorder_id) as num_recs " . 
       "from posorders " . 
	   "where posorder_order = " . $project["order_id"];

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	if($row["num_recs"] > 0)
	{
		$posorder_present = 1;
	}
}

if($posorder_present == 0)
{
	$sql = "select count(posorder_id) as num_recs " . 
		   "from posorderspipeline " . 
		   "where posorder_order = " . $project["order_id"];

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		if($row["num_recs"] > 0)
		{
			$posorder_present = 1;
		}
	}
}

$form_type = "";
//corporate, joint venture, cooperation 3rd party
if($project["project_cost_type"] == 6) // other
{
	$form_type = "INR03";
}
elseif($project["project_cost_type"] == 1 or $project["project_cost_type"] == 3 or $project["project_cost_type"] == 4)
{
	$form_type = "CER";
}
else
{
	$form_type = "AF";
}


$business_plan_period = "";
$business_plan_period2 = "";
if($cer_basicdata["cer_basicdata_firstyear"] 
   and $cer_basicdata["cer_basicdata_lastyear"] 
   and $cer_basicdata["cer_basicdata_firstmonth"]
   and $cer_basicdata["cer_basicdata_lastmonth"])
{
	/*
	$date1 = $cer_basicdata["cer_basicdata_firstyear"] . "-" . $cer_basicdata["cer_basicdata_firstmonth"] . "-01";
	$date2 = $cer_basicdata["cer_basicdata_lastyear"] . "-" . $cer_basicdata["cer_basicdata_lastmonth"] . "-28";
	$diff = abs(strtotime($date2) - strtotime($date1));
	$num_years = floor($diff / (365*60*60*24));
	$num_months = floor(($diff - $num_years * 365*60*60*24) / (30*60*60*24));

	if($num_months == 12)
	{
		$num_months = 0;
		$num_years++;
	}

	$business_plan_period = $num_years . " years and " . $num_months . " months";
    */


	$duration_in_years = $cer_basicdata["cer_basicdata_lastyear"] - $cer_basicdata["cer_basicdata_firstyear"];
	$tmp1 = 13 - $cer_basicdata["cer_basicdata_firstmonth"] + $cer_basicdata["cer_basicdata_lastmonth"];
	$tmp2 = ($duration_in_years - 1)*12;
	$duration_in_years = round(($tmp1 + $tmp2) / 12, 1);

	$duration_in_years_and_months = floor(($tmp1 + $tmp2) / 12);
	$duration_in_years_and_months = $duration_in_years_and_months . " years and " . (($tmp1 + $tmp2) - ($duration_in_years_and_months*12)) . " months";

	$business_plan_period = $duration_in_years_and_months;
	$business_plan_period2 = $duration_in_years_and_months;

	
}


$ln_basicdata = get_ln_basicdata(param("pid"), $ln_version);


//get all brands
$first_brand = '';
$cer_brands = array();
$sql = "select DISTINCT cer_revenue_brand_id, cer_brand_name " . 
       " from cer_revenues " . 
	   " left join cer_brands on cer_brand_id = cer_revenue_brand_id " . 
	   " where cer_revenue_cer_version = " . $cer_version . 
	   " and cer_revenue_project = " . param("pid") . 
	   " order by cer_brand_name";


$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$cer_brands[$row["cer_revenue_brand_id"]] = $row["cer_brand_name"];
}
if(count($cer_brands) > 0)
{
	$tmp = array_keys($cer_brands);
	$first_brand = $tmp[0];
	asort($cer_brands);
}
else
{
	$sql = "select DISTINCT cer_brand_id, cer_brand_name " . 
		   " from cer_brands " . 
		   " where cer_brand_active = 1" . 
		   " order by cer_brand_name";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$cer_brands[$row["cer_brand_id"]] = $row["cer_brand_name"];
	}

	if(count($cer_brands) > 0)
	{
		$tmp = array_keys($cer_brands);
		$first_brand = $tmp[0];
		asort($cer_brands);
	}
}
?>