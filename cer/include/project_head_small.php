<?php
// read project and order details
if(!isset($project))
{
	$project = get_project(param("pid"));
}
$order_state_name = get_actual_order_state_name($project["order_actual_order_state_code"], 1);



$client_address = get_address($project["order_client_address"]);

$franchisee_address = get_address($project["order_franchisee_address_id"]);

$client = $client_address["company"] . ", " .
		  $client_address["zip"] . " " .
          $client_address["place"] . ", " .
          $client_address["country_name"];


$franchisee = $franchisee_address["company"] . ", " .
		  $franchisee_address["zip"] . " " .
          $franchisee_address["place"] . ", " .
          $franchisee_address["country_name"];


$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_address"] . ", " .
		$project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];


$form->add_label("shop_address", "POS Location", RENDER_HTML, $shop);
$form->add_label("client_address", "Client", RENDER_HTML, $client);
$form->add_label("legal_entity", "Legal Entity", RENDER_HTML, $client_address["legal_entity"]);

if(!isset($donotshowfranchisee)) {

	if($project["project_cost_type"] != 6)
	{
		$form->add_label("franchisee_address", "Franchisee", RENDER_HTML, $franchisee);
	}
	else
	{
		$form->add_label("franchisee_address", "Owner Company", RENDER_HTML, $franchisee);
	}
}

$form->add_label("project_number", "Project Number", 0, $project["project_number"]);
$form->add_lookup("project_postype", "POS Type", "postypes", "postype_name", 0, $project["project_postype"]);
$form->add_lookup("project_pos_subclass", "POS Type Subclass", "possubclasses", "possubclass_name", NOTNULL, $project["project_pos_subclass"]);

$form->add_label("type3", "Project Legal Type", 0, $project["project_costtype_text"]);
$form->add_label("type2", "Project Kind", 0, $project["projectkind_name"]);


?>