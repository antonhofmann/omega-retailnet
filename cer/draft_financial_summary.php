<?php
/********************************************************************

    draft_financial_summary.php

    Show financial summary of the business plan draft

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-02-21
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-02-21
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_draft_functions.php";
require "include/financial_functions.php";

check_access("has_access_to_cer_drafts");

/********************************************************************
    prepare all data needed
*********************************************************************/

$currency = get_draft_currency(param("did"));
$basicdata = get_draft_basicdata(param("did"));


include("include/in_financial_data.php");


if($discounted_cash_flow_retail)
{
	$discounted_cash_flow_retail = round($discounted_cash_flow_retail,2) . "%";
}
else
{
	$discounted_cash_flow_retail = "";
}

if($discounted_cash_flow_wholesale)
{
	$discounted_cash_flow_wholesale = round($discounted_cash_flow_wholesale,2) . "%";
}
else
{
	$discounted_cash_flow_wholesale = "";
}

if($pay_back_period_retail)
{
	$pay_back_period_retail = number_format(round($pay_back_period_retail,2), 2);
}
else
{
	$pay_back_period_retail = "Invest. Period";
}

if($pay_back_period_wholesale)
{
	$pay_back_period_wholesale = number_format(round($pay_back_period_wholesale,2), 2);
}
else
{
	$pay_back_period_wholesale = "";
}


$summary = array();
for($i=1;$i<22;$i++)
{
	$y = $first_year + $i -1;
	if(in_array($y, $years))
	{
		$summary[$i][1] = round($total_gross_sales_values[$y]/1000, 0);
		$summary[$i][2] = -1 * round($sales_reduction_values[$y]/1000, 0);
		$summary[$i][3] = "<strong>" . round($total_net_sales_values[$y]/1000, 0)  . "</strong>";
		$summary[$i][4] = -1 * round($material_of_products_values[$y]/1000, 0)  . "</strong>";
		$summary[$i][5] = "<strong>" . round($total_gross_margin_values[$y]/1000, 0)  . "</strong>";
		$tmp =  $marketing_expenses_values[$y];
		$summary[$i][6] = -1*round($tmp/1000, 0);
		$summary[$i][7] = -1*round($total_indirect_expenses_values[$y]/1000, 0);
		$summary[$i][8] = "<strong>" . round($operating_income01_values[$y]/1000, 0)  . "</strong>";
		$summary[$i][9] = round($operating_income02_values[$y]/1000, 0);
		$summary[$i][10] = "<strong>" . round($cash_flow_values[$y]/1000, 0)  . "</strong>";
	}
}



/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_drafts", "cer_drafts");

$form->add_section("Profitability Retail Margin");
$form->add_label("npv1", "Net Present Value in " . $currency["symbol"], 0, round($net_present_value_retail/1000, 2));
$form->add_label("roi1", "Discounted Cash Flow Return on Investment in %", 0, $discounted_cash_flow_retail);
$form->add_label("payback1", "Pay back period in years", 0, $pay_back_period_retail);

$form->add_section("Profitability Wholesale Margin");
$form->add_label("npv2", "Net Present Value in " . $currency["symbol"] , 0, round($net_present_value_wholesale/1000, 2));
$form->add_label("roi2", "Discounted Cash Flow Return on Investment in %", 0, $discounted_cash_flow_wholesale);
$form->add_label("payback2", "Pay back period in years", 0, $pay_back_period_wholesale);


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();


/********************************************************************
    build list of selected figures
*********************************************************************/
$sql_list1 = "select distinct cer_financialsummary_caption_id, cer_financialsummary_caption_caption " .
             "from cer_financialsummary_captions";

$list1_filter = "cer_expense_project = " . param("pid");

$list1 = new ListView($sql_list1, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list1->set_title("Business Plan Key Figures in  " . $currency["symbol"]);
$list1->set_entity("cer_financialsummary_captions");
//$list1->set_filter($list1_filter);

$list1->add_column("cer_financialsummary_caption_caption", "Type", "", "", "", COLUMN_NO_WRAP | COLUMN_UNDERSTAND_HTML);

for($i=1;$i<22;$i++)
{
	$y = $first_year + $i -1;
	if(in_array($y, $years))
	{
		$list1->add_text_column("y1", "$y", COLUMN_ALIGN_RIGHT| COLUMN_UNDERSTAND_HTML,$summary[$i]);
	}
}

/********************************************************************
    Populate list and process button clicks
*********************************************************************/ 
$list1->populate();


/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_drafts");


require "include/draft_page_actions.php";


$page->header();

$page->title($basicdata['cer_basicdata_title'] . ": Financial Summary");

$form->render();
$list1->render();
$page->footer();

?>