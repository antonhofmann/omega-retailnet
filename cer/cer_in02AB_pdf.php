<?php
/********************************************************************

    cer_in02A_pdf.php

    Print Lease Negotiation Application Form.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-06-17
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-06-17
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();

$SUPPRESS_HEADERS = true;
define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 


require "../include/frame.php";

$cer_version = 0;
$ln_version = 0;
if(param("cerversion"))
{
	$cer_version = param("cerversion");
}
if(param("lnversion"))
{
	$ln_version = param("lnversion");
}


require "include/get_functions.php";
require "include/get_project.php";
require "../shared/func_posindex.php";
require "include/financial_functions.php";



check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');



// data needed
$project = get_project(param("pid"));
$order_number  = $project["project_order"];



$client_currency = get_cer_currency(param("pid"), $cer_version);


$project_manager="";
$tmp = get_user($project["project_retail_coordinator"]);

if(count($tmp) > 0)
{
	$project_manager = $tmp["name"] . " " . $tmp["firstname"];
}



$client_address = get_address($project["order_client_address"]);

$currency_symbol = $client_currency["symbol"];





$postype = $project["projectkind_name"] . ", " . $project["project_costtype_text"] . ", " . $project["postype_name"] . ", " . $project["possubclass_name"];



//get keymoney and deposit
$tmp = get_pos_intangibles(param("pid"), 15, $cer_version);
if(count($tmp) > 0)
{
	$keymoney_full = $tmp ["cer_investment_amount_cer_loc"];
	$keymoney = round($keymoney_full/1000, 0);
	

	$tmp = get_pos_intangibles(param("pid"), 9, $cer_version);
	$deposit = $tmp ["cer_investment_amount_cer_loc"];
	$deposit = round($deposit/1000, 0);
}
else
{
	$keymoney = "";
	$deposit = "";
}



$currency_symbol = get_currency_symbol($ln_basicdata["ln_basicdata_currency"]);


//approval names
$approval_name1 = $cer_basicdata["cer_basicdata_approvalname1"];
$approval_name2 = $cer_basicdata["cer_basicdata_approvalname2"];
$approval_name3 = $cer_basicdata["cer_basicdata_approvalname3"];
$approval_name4 = $cer_basicdata["cer_basicdata_approvalname4"];
$approval_name5 = $cer_basicdata["cer_basicdata_approvalname5"];
$approval_name6 = $cer_basicdata["cer_basicdata_approvalname6"];
$approval_name7 = $cer_basicdata["cer_basicdata_approvalname7"];
$approval_name8 = $cer_basicdata["cer_basicdata_approvalname8"];
$approval_name9 = $cer_basicdata["cer_basicdata_approvalname9"];
$approval_name10 = $cer_basicdata["cer_basicdata_approvalname10"];

$approval_name11 = $cer_basicdata["cer_summary_in01_sig01"];
$approval_name12 = $cer_basicdata["cer_summary_in01_sig02"];
$approval_name13 = $cer_basicdata["cer_summary_in01_sig03"];




//documents and pictures
$pix1 = ".." . $ln_basicdata["ln_basicdata_pix1"];

// Create and setup PDF document

$pdf = new TCPDF("L", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 20, 10);
$pdf->setPrintHeader(false);

$pdf->SetTitle("Retail Lease Negotiation");
$pdf->SetAuthor( BRAND . " Retailnet");
$pdf->SetDisplayMode(100);
$pdf->SetAutoPageBreak(false, 0);


$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');
$pdf->Open();



include("cer_in02A_detail_pdf.php");
include("cer_in02B_detail_pdf.php");

$file_name = BRAND . "_INR-02_" . $project["project_number"] . ".pdf";

$pdf->Output($file_name);

?>