<?php
/********************************************************************

    ln_former_lease_conditions_pdf_detail.php

    Print former leae conditions

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2018-04-03
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2018-04-03
    Version:        1.0.0

    Copyright (c) 2018, OMEGA, All Rights Reserved.
*********************************************************************/

/********************************************************************
    prepare all data needed
*********************************************************************/
$posdata = get_pos_data($project["project_order"]);

if($project["project_relocated_posaddress_id"] > 0) {
	$_rl_pos_data =  get_poslocation($project["project_relocated_posaddress_id"]);
	$posdata['posaddress_store_openingdate'] = $_rl_pos_data['posaddress_store_openingdate'];
}


$client_address = get_address($project["order_client_address"]);
$project_name = $project["project_number"] . ": " . $project["order_shop_address_company"]. ", " . $project["order_shop_address_place"] . ", " . $project["country_name"];

$currency = get_cer_currency($project["project_id"]);

//get possellouts
//count years in sellouts
if($project["project_relocated_posaddress_id"] > 0) {
	$sql = "select max(possellout_year) as last_year from possellouts 
			where possellout_posaddress_id = " .  dbquote($project["project_relocated_posaddress_id"]);
}
else {
	$sql = "select max(possellout_year) as last_year from possellouts 
			where possellout_posaddress_id = " .  dbquote($project["posaddress_id"]);
}
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);
$last_year = $row['last_year'];
$previous_year = $cer_basicdata["cer_basicdata_firstyear"];

$sellouts = array();
$sellouts_months=array();
if($previous_year > date('Y')) {
	$previous_year = date('Y');
}

if(!$last_year) {
	$year1 = "";
	$year2 = "";
	$year3 = "";
}
elseif($last_year == $previous_year) {
	$year1 = $previous_year;
	$year2 = $previous_year - 1;
	$year3 = $previous_year - 2;
}
elseif($last_year < $previous_year) {
	$year1 = $previous_year;
	$year2 = $last_year;
	$year3 = $last_year - 1;
}
elseif($last_year > $previous_year) {
	$year1 = $previous_year;
	$year2 = $previous_year -1;
	$year3 = $previous_year - 2;
}


if($project['project_relocated_posaddress_id'] > 0) {
	$sql = "select * from possellouts 
			where possellout_posaddress_id = " .  dbquote($project["project_relocated_posaddress_id"]) . 
			" and possellout_year in ('" . $year1 . "', '" . $year2 . "', '" . $year3 ."')
			order by possellout_year desc ";
}
else {
	$sql = "select * from possellouts 
			where possellout_posaddress_id = " .  dbquote($project["posaddress_id"]) . 
			" and possellout_year in ('" . $year1 . "', '" . $year2 . "', '" . $year3 ."')
			order by possellout_year desc ";
}

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$sellouts[$row['possellout_year']] = $row;
	$sellouts_months[$row['possellout_year']] = $row["possellout_month"];
}

//add missing years
if(!array_key_exists($year1, $sellouts)) {
	$sellouts[$year1] = array("possellout_month"=>"", "possellout_watches_units"=>"", "possellout_watches_grossales"=>"", "possellout_bijoux_units"=>"", "possellout_bijoux_grossales"=>"", "possellout_net_sales"=>"", "possellout_grossmargin"=>"", "possellout_operating_expenses"=>"", "possellout_operating_income_excl"=>"", "possellout_wholsale_margin"=>"", "possellout_operating_income_incl"=>"");
	$sellouts_months[$year1] = "";
}
if(!array_key_exists($year2, $sellouts)) {
	$sellouts[$year2] = array("possellout_month"=>"", "possellout_watches_units"=>"", "possellout_watches_grossales"=>"", "possellout_bijoux_units"=>"", "possellout_bijoux_grossales"=>"", "possellout_net_sales"=>"", "possellout_grossmargin"=>"", "possellout_operating_expenses"=>"", "possellout_operating_income_excl"=>"", "possellout_wholsale_margin"=>"", "possellout_operating_income_incl"=>"");
	$sellouts_months[$year2] = "";
}
if(!array_key_exists($year3, $sellouts)) {
	$sellouts[$year3] = array("possellout_month"=>"", "possellout_watches_units"=>"", "possellout_watches_grossales"=>"", "possellout_bijoux_units"=>"", "possellout_bijoux_grossales"=>"", "possellout_net_sales"=>"", "possellout_grossmargin"=>"", "possellout_operating_expenses"=>"", "possellout_operating_income_excl"=>"", "possellout_wholsale_margin"=>"", "possellout_operating_income_incl"=>"");
	$sellouts_months[$year3] = "";
}

/********************************************************************
    print PDF
*********************************************************************/
//set pdf parameters

$margin_top = 16;
$margin_left = 12;
$y = $margin_top;

$pdf->SetTitle("Former Lease Conditions and Profitability");
$pdf->SetAuthor(BRAND . " Retailnet");
$pdf->SetDisplayMode(150);
$pdf->AddPage("P", "A4");
$pdf->SetAutoPageBreak(false, 0);

// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "I", 9);
$pdf->Cell(45, 10, "", 1);

$pdf->Image('../pictures/logo.jpg',13,$margin_top + 1,22);

$pdf->SetFont("arialn", "B", 9);
$pdf->Cell(122, 10, "Former Lease Conditions and Profitability", 1, "", "C");
$pdf->Cell(20, 10, date("d.m.Y"), 1, "", "C");


// draw first box
$pdf->SetXY($margin_left,$y+8);

	// print project and investment infos
	$y = $y+11;
	$x = $margin_left;

	$y = $y+7;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, $project_name, 1, "", "L");

	
	//client
	$y = $y+7;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, "1. " . BRAND . " Subsidiary or Agent", 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Company Name:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	
	if($client_address["company2"])
	{
		$pdf->Cell(142, 5, $client_address["company"] .  ", " . $client_address["company2"], 1, "", "L");
	}
	else
	{
		$pdf->Cell(142, 5, $client_address["company"], 1, "", "L");
	}



	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Country:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, $client_address["country_name"], 1, "", "L");
	$pdf->Ln();



	//Business Premises
	$y = $y+7;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, "2. POS Location", 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "POS Name:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, $posdata["posaddress_name"], 1, "", "L");


	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Address:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);

	if($posdata["posaddress_address2"])
	{
		$pdf->Cell(142, 5, $posdata["posaddress_address"] .  ", " . $posdata["posaddress_address2"], 1, "", "L");
	}
	else
	{
		$pdf->Cell(142, 5, $posdata["posaddress_address"], 1, "", "L");
	}

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Zip and City:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, $posdata["posaddress_zip"] . " " . $posdata["posaddress_place"], 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Country:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, $posdata["country_name"], 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Legal Type/POS Type:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, $project["project_costtype_text"] ." " . $project["postype_name"], 1, "", "L");


	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Project:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, $project["project_number"], 1, "", "L");

	
	$y = $y+7;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, "3. " . BRAND . " Former Lease Conditions", 1, "", "L");

	if($ln_basicdata["ln_basicdata_former_pos_id"] > 0) {
		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(45, 5, "Former Legal Type/POS Type:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(142, 5, $ln_basicdata["former_pos_project_costtype_text"] ." " . $ln_basicdata["former_pos_postype_name"], 1, "", "L");
	}

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Original Opening Date", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, to_system_date($posdata["posaddress_store_openingdate"]), 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Latest Renovation Date", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, to_system_date($ln_basicdata["ln_basicdata_pos_last_renovation_date"]), 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Last yearly fixed rent in " . $currency['symbol'], 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, number_format($ln_basicdata["ln_basicdata_last_fixed_rent"], 0, ".", "'"), 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Last yearly additional rent in " . $currency['symbol'], 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, number_format($ln_basicdata["ln_basicdata_last_additional_rent"], 0, ".", "'"), 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Last turnover based percentage", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, number_format($ln_basicdata["ln_basicdata_last_tob_rent"], 2) . "%", 1, "", "L");

	$y = $y+7;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, "Description of changes in the environment since the POS was opened", 1, "", "L");
	$pdf->ln();
	$pdf->SetX($x);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(187,0,$ln_basicdata["ln_basicdata_changes_in_enviroment"],1, "L", false, 1);
	$pdf->ln();

	
	$y = $pdf->getY();
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, "5. Profitability", 1, "", "L");


	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(46, 5, "Year", 1, "", "L");
	$pdf->Cell(47, 5, $year1 . "(" . $sellouts_months[$year1] . ")", 1, "", "L");
	$pdf->Cell(47, 5, $year2. "(" . $sellouts_months[$year2] . ")", 1, "", "L");
	$pdf->Cell(47, 5, $year3. "(" . $sellouts_months[$year3] . ")", 1, "", "L");


	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(46, 5, "Number of months selling", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(47, 5, $sellouts[$year1]['possellout_month'], 1, "", "L");
	$pdf->Cell(47, 5, $sellouts[$year2]['possellout_month'], 1, "", "L");
	$pdf->Cell(47, 5, $sellouts[$year3]['possellout_month'], 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(46, 5, "Watches Units sold", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(47, 5, $sellouts[$year1]['possellout_watches_units'], 1, "", "L");
	$pdf->Cell(47, 5, $sellouts[$year2]['possellout_watches_units'], 1, "", "L");
	$pdf->Cell(47, 5, $sellouts[$year3]['possellout_watches_units'], 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(46, 5, "Watches Gross Sales", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(47, 5, number_format((int)$sellouts[$year1]['possellout_watches_grossales'], 2, '.', "'"), 1, "", "L");
	$pdf->Cell(47, 5, number_format((int)$sellouts[$year2]['possellout_watches_grossales'], 2, '.', "'"), 1, "", "L");
	$pdf->Cell(47, 5, number_format((int)$sellouts[$year3]['possellout_watches_grossales'], 2, '.', "'"), 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(46, 5, "Bijoux Units sold", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(47, 5, $sellouts[$year1]['possellout_bijoux_units'], 1, "", "L");
	$pdf->Cell(47, 5, $sellouts[$year2]['possellout_bijoux_units'], 1, "", "L");
	$pdf->Cell(47, 5, $sellouts[$year3]['possellout_bijoux_units'], 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(46, 5, "Bijoux Gross Sales", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(47, 5, number_format((int)$sellouts[$year1]['possellout_bijoux_grossales'], 2, '.', "'"), 1, "", "L");
	$pdf->Cell(47, 5, number_format((int)$sellouts[$year2]['possellout_bijoux_grossales'], 2, '.', "'"), 1, "", "L");
	$pdf->Cell(47, 5, number_format((int)$sellouts[$year3]['possellout_bijoux_grossales'], 2, '.', "'"), 1, "", "L");

	if($project['project_projectkind'] != 3
			and $project['project_projectkind'] != 4) {
		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(46, 5, "Net Sales", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(47, 5, number_format((int)$sellouts[$year1]['possellout_net_sales'], 2, '.', "'"), 1, "", "L");
		$pdf->Cell(47, 5, number_format((int)$sellouts[$year2]['possellout_net_sales'], 2, '.', "'"), 1, "", "L");
		$pdf->Cell(47, 5, number_format((int)$sellouts[$year3]['possellout_net_sales'], 2, '.', "'"), 1, "", "L");
	

		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(46, 5, "Gross Margin", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(47, 5, number_format((int)$sellouts[$year1]['possellout_grossmargin'], 2, '.', "'"), 1, "", "L");
		$pdf->Cell(47, 5, number_format((int)$sellouts[$year2]['possellout_grossmargin'], 2, '.', "'"), 1, "", "L");
		$pdf->Cell(47, 5, number_format((int)$sellouts[$year3]['possellout_grossmargin'], 2, '.', "'"), 1, "", "L");

		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(46, 5, "Indirect operating expenses", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(47, 5, number_format((int)$sellouts[$year1]['possellout_operating_expenses'], 2, '.', "'"), 1, "", "L");
		$pdf->Cell(47, 5, number_format((int)$sellouts[$year2]['possellout_operating_expenses'], 2, '.', "'"), 1, "", "L");
		$pdf->Cell(47, 5, number_format((int)$sellouts[$year3]['possellout_operating_expenses'], 2, '.', "'"), 1, "", "L");

		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(46, 5, "Operating Income", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(47, 5, number_format((int)$sellouts[$year1]['possellout_operating_income_excl'], 2, '.', "'"), 1, "", "L");
		$pdf->Cell(47, 5, number_format((int)$sellouts[$year2]['possellout_operating_income_excl'], 2, '.', "'"), 1, "", "L");
		$pdf->Cell(47, 5, number_format((int)$sellouts[$year3]['possellout_operating_income_excl'], 2, '.', "'"), 1, "", "L");

		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(46, 5, "Wholesale margin (%)", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(47, 5, number_format((int)$sellouts[$year1]['possellout_wholsale_margin'], 2, '.', "'") . "%", 1, "", "L");
		$pdf->Cell(47, 5, number_format((int)$sellouts[$year2]['possellout_wholsale_margin'], 2, '.', "'") . "%", 1, "", "L");
		$pdf->Cell(47, 5, number_format((int)$sellouts[$year3]['possellout_wholsale_margin'], 2, '.', "'") . "%", 1, "", "L");

		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(46, 5, "Operating income incl. Whole Sale Margin", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(47, 5, number_format((int)$sellouts[$year1]['possellout_operating_income_incl'], 2, '.', "'"), 1, "", "L");
		$pdf->Cell(47, 5, number_format((int)$sellouts[$year2]['possellout_operating_income_incl'], 2, '.', "'"), 1, "", "L");
		$pdf->Cell(47, 5, number_format((int)$sellouts[$year3]['possellout_operating_income_incl'], 2, '.', "'"), 1, "", "L");
	}


?>