<?php
/********************************************************************

    cer_draft_print.php

    Print business plan

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-11-05
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-11-05
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_draft_functions.php";

check_access("has_access_to_cer");

/********************************************************************
    prepare all data needed
*********************************************************************/

$basicdata = get_draft_basicdata(param("did"));


$title = $basicdata["cer_basicdata_title"];

$currencies = array();
$sql = 'select currency_id, currency_symbol from currencies where currency_system = 1';

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$currencies["s"] = $row['currency_symbol'];
}


if($basicdata['cer_basicdata_currency']) {
	$sql = 'select currency_id, currency_symbol from currencies where currency_id = ' . $basicdata['cer_basicdata_currency'];

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res) and !in_array($row['currency_symbol'], $currencies))
	{
		$currencies["c1"] = $row['currency_symbol'];
	}
}

if($basicdata['cer_basicdata_currency2']) {
	$sql = 'select currency_id, currency_symbol from currencies where currency_id = ' . $basicdata['cer_basicdata_currency2'];

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res) and !in_array($row['currency_symbol'], $currencies))
	{
		$currencies["c2"] = $row['currency_symbol'];
	}
}


/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_basicdata", "basicdata");


$form->add_hidden("did", param("did"));

$form->add_hidden("did", param("did"));
$form->add_section("Currency Selection");
$form->add_comment("Please indicate in which currency you would like to have the business plan draft printed.");
$form->add_list("currency", "Currency", $currencies, SUBMIT, 'c1');


$link = "'/cer/cer_inr02_pdf.php?did=" . param("did") . "&cid=" . param("currency") . "'";
$form->add_button("print", "Print business plan", "javascript:popup(" . $link . ", 1024, 768);");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_drafts");
require "include/draft_page_actions.php";
$page->header();



$page->title($title . ": Print Business Plan Draft");

if(id() > 0) {
	require_once("include/tabs_draft.php");
}

$form->render();
$page->footer();

?>