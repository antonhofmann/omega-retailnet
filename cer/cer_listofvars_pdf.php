<?php
/********************************************************************

    cer_listofvars_pdf.php

    Print PDF for List of Variables

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-09-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-09-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();


$SUPPRESS_HEADERS = true;
define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 


require "../include/frame.php";


$cer_version = 0;
$ln_version = 0;
if(param("cerversion"))
{
	$cer_version = param("cerversion");
}
if(param("lnversion"))
{
	$ln_version = param("lnversion");
}


require "include/get_functions.php";
require "include/financial_functions.php";
require "include/get_project.php";

check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');
require_once('../include/fpdi/fpdi.php'); 





// Create and setup PDF document

$pdf = new FPDI("P", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 23, 10);
$pdf->setPrintHeader(false);

$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');

$pdf->Open();

include("cer_listofvars_pdf_detail.php");

$file_name = BRAND . "_list_of_variables_" . $project["project_number"] . "_.pdf";
$pdf->Output($file_name);
?>