<?php
/********************************************************************

    af_booklet_pdf.php

    Print PDF AF all Forms.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-02-20
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-02-20
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();

$SUPPRESS_HEADERS = true;
define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 

require "../include/frame.php";
require "include/get_functions.php";
require "include/financial_functions.php";

check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');
require_once('../include/fpdi/fpdi.php'); 



//get currencies
$cer_basicdata = get_cer_basicdata(param("pid"));


$currencies = array();
$sql = 'select currency_id, currency_symbol from currencies where currency_system = 1';

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$currencies["s"] = $row['currency_symbol'];
}


if($cer_basicdata['cer_basicdata_currency']) {
	$sql = 'select currency_id, currency_symbol from currencies where currency_id = ' . $cer_basicdata['cer_basicdata_currency'];

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res) and !in_array($row['currency_symbol'], $currencies))
	{
		$currencies["c1"] = $row['currency_symbol'];
	}
}

if($cer_basicdata['cer_basicdata_currency2']) {
	$sql = 'select currency_id, currency_symbol from currencies where currency_id = ' . $cer_basicdata['cer_basicdata_currency2'];

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res) and !in_array($row['currency_symbol'], $currencies))
	{
		$currencies["c2"] = $row['currency_symbol'];
	}
}


// Create and setup PDF document

$pdf = new FPDI("P", "mm", "A4");
$pdf->setPrintHeader(false);


$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');

$pdf->Open();




//project layout attachment
$source_file = "..../files/orders/211.091.803/phoenix_market_city,_bangalore,_560048_bangalore,_india_model_(1).pdf";
if(file_exists($source_file))
{
	$num_of_pages = $pdf->setSourceFile($source_file);

	for($i=1;$i<=$num_of_pages;$i++)
	{
		$tplidx = $pdf->ImportPage($i);
        $s = $pdf->getTemplatesize($tplidx);
		if($s["w"] > $s["h"])
		{
			$pdf->AddPage("L", array(0=>$s["h"], 1=>$s["w"]));
		}
		else
		{
			$pdf->AddPage("P", array(0=>$s["w"], 1=>$s["h"]));
		}
        $pdf->useTemplate($tplidx);
	}

}           



$source_file = '..../files/ln/211.091.803/full_page_fax_print.pdf';
if(file_exists($source_file))
{
	$num_of_pages = $pdf->setSourceFile($source_file);
	for($i=1;$i<=$num_of_pages;$i++)
	{
		$tplidx = $pdf->ImportPage($i);
        $s = $pdf->getTemplatesize($tplidx);

		if($s["w"] > $s["h"])
		{
			$pdf->AddPage("L", array(0=>$s["h"], 1=>$s["w"]));
		}
		else
		{
			$pdf->AddPage("P", array(0=>$s["w"], 1=>$s["h"]));
		}


        $pdf->useTemplate($tplidx);
	}
}

$pdf->Output();
?>