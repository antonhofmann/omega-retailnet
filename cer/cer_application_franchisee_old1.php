<?php
/********************************************************************

    cer_application_franchisee.php

    Application Form: franchisee information
    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";

check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}

/********************************************************************
    prepare all data needed
*********************************************************************/
$client_address = get_address($project["order_client_address"]);
$posdata = get_pos_data($project["project_order"]);

//get quantities of watches
//get revenue values
$quantity_watches = array();

$sql = "select * from cer_revenues where cer_revenue_cer_version = 0 and cer_revenue_project = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$quantity_watches[$row["cer_revenue_year"]] = $row["cer_revenue_quantity_watches"];
}

// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ".
                 "order by country_name";

/********************************************************************
    build form
*********************************************************************/
$form = new Form("posaddresses", "posaddress");

include("include/project_head.php");


$form->add_hidden("pid", param("pid"));

$form->add_section("Franchisee");

$form->add_label("franchisee", $franchisee_address["company"]);

/*
$form->add_comment("Please indicate the franchisee address.");

$sql = "select address_id, concat(address_company, ', ', address_place) as company from addresses " . 
	   "where address_canbefranchisee = 1 and address_active = 1 and address_parent = " . $client_address["id"] .
	   " order by address_company";

$form->add_list("franchisee_address_id", "Franchisee", $sql, SUBMIT, $posdata["posaddress_franchisee_id"]);

if($posdata["posaddress_franchisee_id"] > 0)
{
	$franchisee_address = get_address($posdata["posaddress_franchisee_id"]);
	$form->add_comment("Please check the franchisee address and correct if necessary.");
	$form->add_edit("franchisee_address_company", "Company*",NOTNULL , $franchisee_address["company"], TYPE_CHAR);
	$form->add_edit("franchisee_address_company2", "", 0, $franchisee_address["company2"], TYPE_CHAR);
	$form->add_edit("franchisee_address_legal_entity_name", "Legal Form of Company",0 , $franchisee_address["legal_entity"], TYPE_CHAR);
	$form->add_edit("franchisee_address_address", "Address*",NOTNULL , $franchisee_address["address"], TYPE_CHAR);
	$form->add_edit("franchisee_address_address2", "", 0, $franchisee_address["address2"], TYPE_CHAR);
	$form->add_edit("franchisee_address_zip", "ZIP*",NOTNULL , $franchisee_address["zip"], TYPE_CHAR, 20);
	$form->add_edit("franchisee_address_place", "City*",NOTNULL , $franchisee_address["place"], TYPE_CHAR, 20);
	$form->add_list("franchisee_address_country", "Country*", $sql_countries, NOTNULL, $franchisee_address["country"]);
	$form->add_edit("franchisee_address_phone", "Phone*",NOTNULL , $franchisee_address["phone"], TYPE_CHAR, 20);
	$form->add_edit("franchisee_address_fax", "Fax",0 , $franchisee_address["fax"], TYPE_CHAR, 20);
	$form->add_edit("franchisee_address_email", "Email",0 , $franchisee_address["email"], TYPE_CHAR);
	$form->add_edit("franchisee_address_contact", "Contact*",NOTNULL ,$franchisee_address["contact"], TYPE_CHAR);
	$form->add_checkbox("franchisee_address_swatch_retailer", "Company is a present " . BRAND . " Retailer", $franchisee_address["retailer"], "", "Relation");
}
else
{
	$form->add_comment("Please check the franchisee address and correct if necessary.");
	$form->add_edit("franchisee_address_company", "Company*",NOTNULL , "", TYPE_CHAR);
	$form->add_edit("franchisee_address_company2", "", 0, "", TYPE_CHAR);
	$form->add_edit("franchisee_address_legal_entity_name", "Legal Form of Company",0 , "", TYPE_CHAR);
	$form->add_edit("franchisee_address_address", "Address*",NOTNULL , "", TYPE_CHAR);
	$form->add_edit("franchisee_address_address2", "", 0, "", TYPE_CHAR);
	$form->add_edit("franchisee_address_zip", "ZIP*",NOTNULL , "", TYPE_CHAR, 20);
	$form->add_edit("franchisee_address_place", "City*",NOTNULL , "", TYPE_CHAR, 20);
	$form->add_list("franchisee_address_country", "Country*", $sql_countries, NOTNULL);
	$form->add_edit("franchisee_address_phone", "Phone*",NOTNULL , "", TYPE_CHAR, 20);
	$form->add_edit("franchisee_address_fax", "Fax",0 , "", TYPE_CHAR, 20);
	$form->add_edit("franchisee_address_email", "Email",0 , "", TYPE_CHAR);
	$form->add_edit("franchisee_address_contact", "Contact*",NOTNULL ,"", TYPE_CHAR);
	$form->add_checkbox("franchisee_address_swatch_retailer", "Company is a present " . BRAND . " Retailer", "", "", "Relation");
}
*/

$form->add_checkbox("franchisee_address_swatch_retailer", "Company is a present " . BRAND . " Retailer", "", "", "Relation");

$form->add_section("Territory Exclusivity");
$form->add_comment("Please give a description clearly delimitating the exclusive franchisee territory covered by the franchisee contract and provide a detailed city pasted under annex 1. ");
$form->add_multiline("posaddress_fag_territory", "Agreement Content*", 4, NOTNULL, $posdata["posaddress_fag_territory"]);

$form->add_upload("posaddress_fag_city_pasted", "City Pasted", "/files/cer/project_". param("pid"), NOTNULL | $deletable, $posdata["posaddress_fag_city_pasted"]);

$form->add_section("Minimum Quantities (only Watches as per Franchisee Contract)");
foreach($quantity_watches as $year=>$quantity)
{
	$form->add_label("units_" . $year, "Units in " . $year ,0 ,$quantity);
}


$form->add_section("Franchisor Address");


$form->add_button("form_save", "Save Data");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/*
if($form->button("franchisee_address_id"))
{


	if ($form->value("franchisee_address_id"))
	{
		$sql = "select * from addresses where address_id = " . $form->value("franchisee_address_id");
		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$form->value("franchisee_address_company", $row["address_company"]);
			$form->value("franchisee_address_company2",  $row["address_company2"]);
			$form->value("franchisee_address_legal_entity_name",  $row["address_legal_entity_name"]);
			$form->value("franchisee_address_address",  $row["address_address"]);
			$form->value("franchisee_address_address2",  $row["address_address2"]);
			$form->value("franchisee_address_zip",  $row["address_zip"]);
			$form->value("franchisee_address_place",  $row["address_place"]);
			$form->value("franchisee_address_country",  $row["address_country"]);
			$form->value("franchisee_address_phone",  $row["address_phone"]);
			$form->value("franchisee_address_fax",  $row["address_fax"]);
			$form->value("franchisee_address_email",  $row["address_email"]);
			$form->value("franchisee_address_contact",  $row["address_contact_name"]);
			$form->value("franchisee_address_swatch_retailer",  $row["address_swatch_retailer"]);
		}
	}
	else
	{
			$form->value("franchisee_address_company", "");
			$form->value("franchisee_address_company2",  "");
			$form->value("franchisee_address_legal_entity_name","");
			$form->value("franchisee_address_address",  "");
			$form->value("franchisee_address_address2",  "");
			$form->value("franchisee_address_zip",  "");
			$form->value("franchisee_address_place",  "");
			$form->value("franchisee_address_country",  0);
			$form->value("franchisee_address_phone",  "");
			$form->value("franchisee_address_fax",  "");
			$form->value("franchisee_address_email",  "");
			$form->value("franchisee_address_contact",  "");
			$form->value("franchisee_address_swatch_retailer",  false);
	}
}
else
*/
if($form->button("form_save"))
{
	if($form->validate())
	{
		
		/*
		//save franchisee data
		if($form->value("franchisee_address_id") > 0) //update address
		{
			$fields = array();
			$values = array();

			$value = dbquote($form->value("franchisee_address_legal_entity_name"));
			$fields[] = "address_legal_entity_name = " . $value;

			$value = dbquote($form->value("franchisee_address_company"));
			$fields[] = "address_company = " . $value;

			$value = dbquote($form->value("franchisee_address_company2"));
			$fields[] = "address_company2 = " . $value;

			$value = dbquote($form->value("franchisee_address_address"));
			$fields[] = "address_address = " . $value;

			$value = dbquote($form->value("franchisee_address_address2"));
			$fields[] = "address_address2 = " . $value;

			$value = dbquote($form->value("franchisee_address_zip"));
			$fields[] = "address_zip = " . $value;

			$value = dbquote($form->value("franchisee_address_place"));
			$fields[] = "address_place = " . $value;

			$value = dbquote($form->value("franchisee_address_country"));
			$fields[] = "address_country = " . $value;

			$value = dbquote($form->value("franchisee_address_phone"));
			$fields[] = "address_phone = " . $value;

			$value = dbquote($form->value("franchisee_address_fax"));
			$fields[] = "address_fax = " . $value;

			$value = dbquote($form->value("franchisee_address_email"));
			$fields[] = "address_email = " . $value;

			$value = dbquote($form->value("franchisee_address_contact"));
			$fields[] = "address_contact_name = " . $value;

			$value = dbquote($form->value("franchisee_address_swatch_retailer"));
			$fields[] = "address_swatch_retailer = " . $value;

			$value = dbquote(date("Y-m-d"));
			$fields[] = "date_modified = " . $value;

			$value = dbquote(user_login());
			$fields[] = "user_created = " . $value;

			$sql = "update addresses set " . join(", ", $fields) . " where address_id = " . $form->value("franchisee_address_id");
			mysql_query($sql) or dberror($sql);

			$franchisee_id = $form->value("franchisee_address_id");
			
		
		}
		else // insert news address
		{
			$fields = array();
			$values = array();

			$fields[] = "address_shortcut";
			$values[] = dbquote(substr(strtolower($form->value("franchisee_address_company")), 0, 15));

			$fields[] = "address_legal_entity_name";
			$values[] = dbquote($form->value("franchisee_address_legal_entity_name"));

			$fields[] = "address_company";
			$values[] = dbquote($form->value("franchisee_address_company"));

			$fields[] = "address_company2";
			$values[] = dbquote($form->value("franchisee_address_company2"));

			$fields[] = "address_address";
			$values[] = dbquote($form->value("franchisee_address_address"));

			$fields[] = "address_address2";
			$values[] = dbquote($form->value("franchisee_address_address2"));

			$fields[] = "address_zip";
			$values[] = dbquote($form->value("franchisee_address_zip"));

			$fields[] = "address_place";
			$values[] = dbquote($form->value("franchisee_address_place"));

			$fields[] = "address_country";
			$values[] = dbquote($form->value("franchisee_address_country"));

			$fields[] = "address_phone";
			$values[] = dbquote($form->value("franchisee_address_phone"));

			$fields[] = "address_fax";
			$values[] = dbquote($form->value("franchisee_address_fax"));

			$fields[] = "address_email";
			$values[] = dbquote($form->value("franchisee_address_email"));

			$fields[] = "address_contact_name";
			$values[] = dbquote($form->value("franchisee_address_contact"));

			$fields[] = "address_swatch_retailer";
			$values[] = dbquote($form->value("franchisee_address_swatch_retailer"));

			$fields[] = "address_type";
			$values[] = 7;

			$fields[] = "address_canbefranchisee";
			$values[] = 1;

			$fields[] = "address_showinposindex";
			$values[] = 1;

			$fields[] = "address_active";
			$values[] = 1;

			$fields[] = "address_parent";
			$values[] = $project["order_client_address"];

			$fields[] = "address_checked";
			$values[] = "0";

			$sql = "insert into addresses (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			mysql_query($sql) or dberror($sql);

			$franchisee_id = mysql_insert_id();
		
		}

		*/


		//update posaddress data

		$fields = array();
		
		/*
		$value = dbquote($franchisee_id);
		$fields[] = "posaddress_franchisee_id = " . $value;
        */


		$value = dbquote($form->value("posaddress_fag_territory"));
		$fields[] = "posaddress_fag_territory = " . $value;

		$value = dbquote($form->value("posaddress_fag_quantity1"));
		$fields[] = "posaddress_fag_quantity1 = " . $value;

		$value = dbquote($form->value("posaddress_fag_quantity2"));
		$fields[] = "posaddress_fag_quantity2 = " . $value;

		$value = dbquote($form->value("posaddress_fag_quantity3"));
		$fields[] = "posaddress_fag_quantity3 = " . $value;

		$value = dbquote($form->value("posaddress_fag_quantity4"));
		$fields[] = "posaddress_fag_quantity4 = " . $value;

		$value = dbquote($form->value("posaddress_fag_quantity5"));
		$fields[] = "posaddress_fag_quantity5 = " . $value;

		$value = dbquote($form->value("posaddress_fag_city_pasted"));
		$fields[] = "posaddress_fag_city_pasted = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update " . $posdata["table"] . " set " . join(", ", $fields) . " where posaddress_id = " . $posdata["posaddress_id"];
		mysql_query($sql) or dberror($sql);

		$form->message("Your data has bee saved.");

	
	}

}
  
/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();
if($form_type == "AF")
{
	$page->title("Application Form: Franchisee Information");
}
else
{
	$page->title("Capital Expenditure Request: Franchisee Information");
}
require_once("include/tabs.php");
$form->render();

require "include/footer_scripts.php";
$page->footer();

?>