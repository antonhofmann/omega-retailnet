<?php
/********************************************************************

    cer_in02B_detail_pdf.php

    Print Detail Form IN-R02B.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-06-17
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-06-17
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.
*********************************************************************/
include("include/in_financial_data.php");

//set pdf parameters
$margin_top = 12;
$margin_left = 12;
$y = $margin_top;
$x = $margin_left+1;
$standard_h = 6;
$x1 = $margin_left+1;
$x2 = $margin_left+61;

$y_pos_for_boxes = array();

$years_page1 = array();
$j= 1;
foreach($years as $key=>$year)
{
	if($j < 6)
	{
		$years_page1[] = $years[$key];
	}
	$j++;
}


$sellouts_last_full_year = "";
if(array_key_exists($sellout_ending_year, $sellouts_watches))
{
	$sellouts_last_full_year = $sellouts_watches[$sellout_ending_year];
}

$pdf->AddPage("L", "A4");

// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "B", 10);
$pdf->Cell(51, 8, "Swatch Group", 1);

$pdf->SetFont("arialn", "B", 14);
$pdf->Cell(159, 8, "RETAIL BUSINESS PLAN OVERVIEW", 1, "", "C");


$pdf->SetFont("arialn", "", 10);

$pdf->SetFillColor(224,224,224);
$pdf->Cell(20, 8, "DETAIL", 1, "", "C", true);
$pdf->SetFillColor(248,251,167);
if($cer_basicdata["cer_basicdata_version"] == 0 and $cer_basicdata["cer_basicdata_cer_locked"] == 0)
{
	$pdf->Cell(20, 8, date("d.m.Y"), 1, "", "C", true);
}
else
{
	$pdf->Cell(20, 8, to_system_date($cer_basicdata["versiondate"]), 1, "", "C", true);
}
$pdf->Cell(20, 8, "INR-02B", 1, "", "C");

	
	$y = $pdf->GetY()+10;
	$y_at_start = $y - 1;

	

	// 3. Sales figures
	
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(268, 6, "3. Sales figures " . " (" . $project['order_number'] . ")", 1, "", "L");

	$y = $pdf->GetY()+6;
	$pdf->SetXY($margin_left+1,$y);
	
	$pdf->Cell(68, 5, "" , 1, "", "L");

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(40, 5, $year , 1, "", "R");
		$i++;
	}
	for($k=$i;$k<5;$k++)
	{
		$pdf->Cell(40, 5, "" , 1, "", "R");
	}



	$y = $pdf->GetY()+6;
	$y_pos_for_boxes[1] = $y;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(34, 5, "Sales in units" , 0, "", "L");
	$pdf->Cell(34, 5, "Watches" , 0, "", "L");



	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, $sales_units_watches_values[$year] , 0, "", "R", true);
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$i++;
	}
	for($k=$i;$k<5;$k++)
	{
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$pdf->Cell(20, 5, "" , 0, "", "R");
	}

	
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(34, 5, "" , 0, "", "L");
	$pdf->Cell(34, 5, "Jewellery" , 0, "", "L");

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, $sales_units_jewellery_values[$year] , 0, "", "R", true);
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$i++;
	}
	for($k=$i;$k<5;$k++)
	{
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$pdf->Cell(20, 5, "" , 0, "", "R");
	}

	$y = $pdf->GetY()+5;
	$y_pos_for_boxes[2] = $y;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(34, 5, "Average Price (" . $currency_symbol . ")" , 0, "", "L");
	$pdf->Cell(34, 5, "Watches" , 0, "", "L");

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, number_format(round($average_price_watches_values[$year],0), 0, '.', "'") , 0, "", "R", true);
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$i++;
	}
	for($k=$i;$k<5;$k++)
	{
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$pdf->Cell(20, 5, "" , 0, "", "R");
	}


	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(34, 5, "" , 0, "", "L");
	$pdf->Cell(34, 5, "Jewellery" , 0, "", "L");

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, number_format(round($average_price_jewellery_values[$year], 0), 0, '.', "'") , 0, "", "R", true);
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$i++;
	}
	for($k=$i;$k<5;$k++)
	{
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$pdf->Cell(20, 5, "" , 0, "", "R");
	}




	$y = $pdf->GetY()+5;
	$y_pos_for_boxes[3] = $y;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(34, 5, "Gross sales (k" .  $currency_symbol . ")" , 0, "", "L");
	$pdf->Cell(34, 5, "Watches" , 0, "", "L");

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, number_format(round($sales_watches_values[$year]/1000, 0), 0, '.', "'") , 0, "", "R", true);
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$i++;
	}
	for($k=$i;$k<5;$k++)
	{
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$pdf->Cell(20, 5, "" , 0, "", "R");
	}


	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(34, 5, "" , 0, "", "L");
	$pdf->Cell(34, 5, "Jewellery" , 0, "", "L");

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, number_format(round($sales_jewellery_values[$year]/1000,0), 0, '.', "'") , 0, "", "R", true);
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$i++;
	}
	for($k=$i;$k<5;$k++)
	{
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$pdf->Cell(20, 5, "" , 0, "", "R");
	}


	


	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(34, 5, "" , 0, "", "L");
	$pdf->Cell(34, 5, "Customer Service" , 0, "", "L");

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, number_format(round($sales_customer_service_values[$year]/1000,0), 0, '.', "'") , 0, "", "R", true);
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$i++;
	}
	for($k=$i;$k<5;$k++)
	{
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$pdf->Cell(20, 5, "" , 0, "", "R");
	}


	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(34, 5, "" , 0, "", "L");
	$pdf->Cell(34, 5, "Others" , 0, "", "L");

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, number_format(round($sales_accessories_values[$year]/1000,0), 0, '.', "'") , 0, "", "R", true);
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$i++;
	}
	for($k=$i;$k<5;$k++)
	{
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$pdf->Cell(20, 5, "" , 0, "", "R");
	}


	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(68, 5, "Total gross sales (k" .  $currency_symbol . ")" , 1, "", "L");

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, number_format(round($total_gross_sales_values[$year]/1000,0), 0, '.', "'") , 1, "", "R", false);
		$pdf->Cell(20, 5, "", "", "R");
		$i++;
	}
	for($k=$i;$k<5;$k++)
	{
		$pdf->Cell(20, 5, "" , 1, "", "R");
		$pdf->Cell(20, 5, "" , 0, "", "R");
	}

	//draw boxes
	$y_tmp = $y;
	foreach($y_pos_for_boxes as $key=>$y_pos)
	{
		$pdf->SetXY($margin_left+1,$y_pos);
		if($key == 1)
		{
			$pdf->Cell(68, 10, "" , 1);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 45, "" , 1);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 45, "" , 1);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 45, "" , 1);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 45, "" , 1);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 45, "" , 1);
		}
		elseif($key == 2)
		{
			$pdf->Cell(68, 10, "" , 1);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 45, "" , 0);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 45, "" , 0);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 45, "" , 0);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 45, "" , 0);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 45, "" , 0);
		}
		elseif($key == 3)
		{
			$pdf->Cell(68, 20, "" , 1);
			$pdf->Cell(20, 20, "" , 1);
			$pdf->Cell(20, 45, "" , 0);
			$pdf->Cell(20, 20, "" , 1);
			$pdf->Cell(20, 45, "" , 0);
			$pdf->Cell(20, 20, "" , 1);
			$pdf->Cell(20, 45, "" , 0);
			$pdf->Cell(20, 20, "" , 1);
			$pdf->Cell(20, 45, "" , 0);
			$pdf->Cell(20, 20, "" , 1);
			$pdf->Cell(20, 45, "" , 0);
		}

	}
	$pdf->SetY($y_tmp);
	$y_pos_for_boxes = array();


	// 4. Finacncial figures
	$y = $pdf->GetY()+6;
	$pdf->SetXY($margin_left+1,$y);
	
	
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 10);
	$pdf->Cell(268, 6, "4. Financial figures", 1, "", "L");

	$y = $pdf->GetY()+6;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 10);
	$pdf->Cell(68, 5, "(all figures in k" . $currency_symbol . ")" , 1, "", "C");
	
	$pdf->SetFont("arialn", "B", 10);
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(40, 5, $year , 1, "", "R");
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(40, 5, "" , 1, "", "R");
	}



	$y = $pdf->GetY()+5;
	$y_pos_for_boxes[1] = $y;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(68, 5, "Total gross sales" , 0, "", "L");

	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, number_format(round($total_gross_sales_values[$year]/1000,0), 0, '.', "'") , 0, "", "R", true);
		$pdf->Cell(20, 5, number_format(round($total_gross_sales_shares[$year], 1), 0, '.', "'"). "%" , 0, "", "R");
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$pdf->Cell(20, 5, "" , 0, "", "R");
	}


	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(68, 5, "Sales Reduction" , 0, "", "L");

	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, number_format(-1*round($sales_reduction_values[$year]/1000, 0), 0, '.', "'") , 0, "", "R", true);
		$pdf->Cell(20, 5, number_format(-round($sales_reduction_shares[$year], 1), 0, '.', "'") . "%" , 0, "", "R");
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$pdf->Cell(20, 5, "" , 0, "", "R");
	}

	


	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(68, 5, "Net sales" , 1, "", "L");

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, number_format(round($total_net_sales_values[$year]/1000,0), 0, '.', "'") , 1, "", "R", true);
		$pdf->Cell(20, 5, number_format(round($total_net_sales_shares[$year], 1), 0, '.', "'") . "%" , 1, "", "R");
		$i++;
	}
	for($k=$i;$k<5;$k++)
	{
		$pdf->Cell(20, 5, "" , 1, "", "R");
		$pdf->Cell(20, 5, "" , 1, "", "R");
	}

	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(68, 5, "Total costs of products sold" , 1, "", "L");

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, number_format(-1*round($material_of_products_values[$year]/1000,0), 0, '.', "'") , 1, "", "R", true);
		$pdf->Cell(20, 5, number_format(-round($material_of_products_shares[$year], 1), 0, '.', "'") . "%" , 1, "", "R");
		$i++;
	}
	for($k=$i;$k<5;$k++)
	{
		$pdf->Cell(20, 5, "" , 1, "", "R");
		$pdf->Cell(20, 5, "" , 1, "", "R");
	}


	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(68, 5, "Gross margin" , 1, "", "L");

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, number_format(round($total_gross_margin_values[$year]/1000,0), 0, '.', "'") , 1, "", "R", true);
		$pdf->Cell(20, 5, number_format(round($total_goss_margin_shares[$year], 1), 0, '.', "'") . "%" , 1, "", "R");
		$i++;
	}
	for($k=$i;$k<5;$k++)
	{
		$pdf->Cell(20, 5, "" , 1, "", "R");
		$pdf->Cell(20, 5, "" , 1, "", "R");
	}


	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(68, 5, "Total marketing investments" , 1, "", "L");

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, number_format(-1*round($marketing_expenses_values[$year]/1000,0), 0, '.', "'") , 1, "", "R", true);
		$pdf->Cell(20, 5, number_format(-round($marketing_expenses_shares[$year], 1), 0, '.', "'") . "%" , 1, "", "R");
		$i++;
	}
	for($k=$i;$k<5;$k++)
	{
		$pdf->Cell(20, 5, "" , 1, "", "R");
		$pdf->Cell(20, 5, "" , 1, "", "R");
	}


	$y = $pdf->GetY()+5;
	$y_pos_for_boxes[2] = $y;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(68, 5, "Indirect salaries & social benefits" , 0, "", "L");

	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, number_format(-1*round($indirect_salaries_values[$year]/1000,0), 0, '.', "'") , 0, "", "R", true);
		$pdf->Cell(20, 5, number_format(-round($indirect_salaries_shares[$year], 1), 0, '.', "'") . "%" , 0, "", "R");
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$pdf->Cell(20, 5, "" , 0, "", "R");
	}

	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(68, 5, "Rents, amortization key money" , 0, "", "L");

	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, number_format(-1*round($rents_total_values[$year]/1000,0), 0, '.', "'") , 0, "", "R", true);
		$pdf->Cell(20, 5, number_format(-round($rents_shares[$year], 1), 0, '.', "'") . "%" , 0, "", "R");
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$pdf->Cell(20, 5, "" , 0, "", "R");
	}

	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(68, 5, "Aux. material, energy, maintenance" , 0, "", "L");

	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, number_format(-1*round($auxmat_values[$year]/1000,0), 0, '.', "'") , 0, "", "R", true);
		$pdf->Cell(20, 5, number_format(-round($auxmat_shares[$year], 1), 0, '.', "'") . "%" , 0, "", "R");
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$pdf->Cell(20, 5, "" , 0, "", "R");
	}

	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(68, 5, "Sales & administration expenses" , 0, "", "L");

	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, number_format(-1*round($sales_admin_values[$year]/1000,0), 0, '.', "'") , 0, "", "R", true);
		$pdf->Cell(20, 5, number_format(-round($sales_admin_shares[$year], 1), 0, '.', "'") . "%" , 0, "", "R");
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$pdf->Cell(20, 5, "" , 0, "", "R");
	}

	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(68, 5, "Depreciation on fixed assets" , 0, "", "L");

	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, number_format(-1*round($depreciation_values[$year]/1000,0), 0, '.', "'") , 0, "", "R", true);
		$pdf->Cell(20, 5, number_format(-round($depreciation_shares[$year], 1), 0, '.', "'") . "%" , 0, "", "R");
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$pdf->Cell(20, 5, "" , 0, "", "R");
	}

	/*
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(68, 5, "Depreciation on Goodwill/Keymoney" , 0, "", "L");

	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, number_format(-1*round($prepayed_rent_values[$year]/1000,0), 0, '.', "'") , 0, "", "R", true);
		$pdf->Cell(20, 5, number_format(-round($prepayed_rent_shares[$year], 1), 0, '.', "'") . "%" , 0, "", "R");
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$pdf->Cell(20, 5, "" , 0, "", "R");
	}
	*/


	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(68, 5, "Other indirect expenses" , 0, "", "L");

	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, number_format(-1*round($other_expenses_values[$year]/1000,0), 0, '.', "'") , 0, "", "R", true);
		$pdf->Cell(20, 5, number_format(-round($other_expenses_shares[$year], 1), 0, '.', "'") . "%" , 0, "", "R");
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$pdf->Cell(20, 5, "" , 0, "", "R");
	}

	
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(68, 5, "Total indirect distribution & sales expenses" , 1, "", "L");
	
	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, number_format(-1*round($total_indirect_expenses_values[$year]/1000,0), 0, '.', "'") , 1, "", "R", true);
		$pdf->Cell(20, 5, number_format(-round($total_indirect_expenses_shares[$year], 1), 0, '.', "'") . "%" , 1, "", "R");
		$i++;
	}
	for($k=$i;$k<5;$k++)
	{
		$pdf->Cell(20, 5, "" , 1, "", "R");
		$pdf->Cell(20, 5, "" , 1, "", "R");
	}

	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(68, 5, "Operating income (excl. wholesale margin)" , 1, "", "L");

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, number_format(round($operating_income01_values[$year]/1000,0), 0, '.', "'") , 1, "", "R", true);
		$pdf->Cell(20, 5, number_format(round($operating_income01_shares[$year], 1), 0, '.', "'") . "%" , 1, "", "R");
		$i++;
	}
	for($k=$i;$k<5;$k++)
	{
		$pdf->Cell(20, 5, "" , 1, "", "R");
		$pdf->Cell(20, 5, "" , 1, "", "R");
	}

	
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(43, 5, "Wholesale margin" , 1, "", "L");
	$pdf->Cell(25, 5, $weighted_average_whole_sale_margin . "%" , 1, "", "L");

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$tmp = $wholesale_margin_of_watches_sold[$year] + $wholesale_margin_of_jewellery_sold[$year] +$wholesale_margin_of_accessories_sold[$year] + $wholesale_margin_of_services_sold[$year];
		
		$pdf->Cell(20, 5, number_format(round($tmp/1000, 0), 0, '.', "'") , 1, "", "R", true);
		$pdf->Cell(20, 5, "" , 1, "", "R");
		$i++;
	}
	for($k=$i;$k<5;$k++)
	{
		$pdf->Cell(20, 5, "" , 1, "", "R");
		$pdf->Cell(20, 5, "" , 1, "", "R");
	}


	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(68, 5, "Operating income (incl. wholesale margin)" , 1, "", "L");


	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, number_format(round($operating_income02_values[$year]/1000,0), 0, '.', "'") , 1, "", "R", true);
		$pdf->Cell(20, 5, number_format(round($operating_income02_shares[$year], 1), 0, '.', "'") . "%" , 1, "", "R");
		$i++;
	}
	for($k=$i;$k<5;$k++)
	{
		$pdf->Cell(20, 5, "" , 1, "", "R");
		$pdf->Cell(20, 5, "" , 1, "", "R");
	}



	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(68, 5, "Operating income (incl. WSM): scenario 80%" , 1, "", "L");


	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, number_format(round($operating_income02_80_percent_values[$year]/1000,0), 0, '.', "'") , 1, "", "R", true);
		$pdf->Cell(20, 5, number_format(round($operating_income02_80_percent_shares[$year], 1), 0, '.', "'") . "%" , 1, "", "R");
		$i++;
	}
	for($k=$i;$k<5;$k++)
	{
		$pdf->Cell(20, 5, "" , 1, "", "R");
		$pdf->Cell(20, 5, "" , 1, "", "R");
	}


	$y = $pdf->GetY()+5;
	$y_pos_for_boxes[3] = $y;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(68, 5, "Break-even based on retail margin (units)" , 0, "", "L");


	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, number_format(round($break_even_retail_margin[$year], 0), 0, '.', "'") , 0, "", "R", true);
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$pdf->Cell(20, 5, "" , 0, "", "R");
	}


	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(68, 5, "Break-even based on retail + wholesale margin (units)" , 0, "", "L");


	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(20, 5, number_format(round($break_even_wholesale_margin[$year],0), 0, '.', "'") , 0, "", "R", true);
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(20, 5, "" , 0, "", "R");
		$pdf->Cell(20, 5, "" , 0, "", "R");
	}
	

	//draw boxes
	$y_tmp = $y;
	foreach($y_pos_for_boxes as $key=>$y_pos)
	{
		$pdf->SetXY($margin_left+1,$y_pos);
		if($key == 1)
		{
			$pdf->Cell(68, 10, "" , 1);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 10, "" , 1);
		}
		elseif($key == 2)
		{
			$pdf->Cell(68, 35, "" , 1);
			$pdf->Cell(20, 35, "" , 1);
			$pdf->Cell(20, 35, "" , 1);
			$pdf->Cell(20, 35, "" , 1);
			$pdf->Cell(20, 35, "" , 1);
			$pdf->Cell(20, 35, "" , 1);
			$pdf->Cell(20, 35, "" , 1);
			$pdf->Cell(20, 35, "" , 1);
			$pdf->Cell(20, 35, "" , 1);
			$pdf->Cell(20, 35, "" , 1);
			$pdf->Cell(20, 35, "" , 1);
		}
		elseif($key == 3)
		{
			$pdf->Cell(68, 10, "" , 1);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 10, "" , 1);
			$pdf->Cell(20, 10, "" , 1);
		}

	}
	$pdf->SetY($y_tmp);





	//draw outer box
	$y = $pdf->GetY()-$margin_top;
	$pdf->SetXY($margin_left,$y_at_start);
	$pdf->Cell(270, $y, "", 1);




	

		
?>