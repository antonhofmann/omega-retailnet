<?php
/********************************************************************

    cer_mail_delete.php

    Mail Box: Delete a Mail

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-11-17
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-11-17
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";

check_access("has_full_access_to_cer");

/********************************************************************
    prepare all data needed
*********************************************************************/
$client_address = get_address($project["order_client_address"]);


$client = $client_address["company"] . ", " .
		  $client_address["zip"] . " " .
          $client_address["place"] . ", " .
          $client_address["country_name"];

$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_address"] . ", " .
		$project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];


//get reciepients of emails
//$reciepients = get_address_users($client_address["id"], $project["project_retail_coordinator"], $project["order_retail_operator"], $project["project_local_retail_coordinator"], $project["project_hq_project_manager"]);

$reciepients = get_address_users($client_address["id"], $project["project_retail_coordinator"], $project["order_retail_operator"], $project["project_local_retail_coordinator"], 0, $project["order_user"], $project["project_design_supervisor"]);
//get userdata
$sender = get_user(user_id());
$reciepients[] = array("name" => $sender["firstname"] . " " . $sender["name"], "email" => $sender["email"], "roles" => "Copy to me");


/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_mails", "cer_mails");

$form->add_label("shop_address", "POS Location", RENDER_HTML, $shop);
$form->add_label("client_address", "Client", RENDER_HTML, $client);
include("include/project_head.php");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("id", param("id"));

$form->add_section("Original Mail Message");
$form->add_label("cer_mail_sender", "Sent by", 0);
$form->add_label("cer_mail_group", "Topic", 0);
$form->add_label("cer_mail_text", "Message", RENDER_HTML);

$form->add_button("delete", "Delete Mail");
$form->add_button("back", "Back");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if($form->button("back"))
{
	$link = "cer_mails.php?pid=" . param("pid");
	redirect($link);
}
elseif($form->button("delete"))
{
		$sql = "delete from cer_mails where cer_mail_id = " . id();
		mysql_query($sql) or dberror($sql);
		$link = "cer_mails.php?pid=" . param("pid");
		redirect($link);
}


/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();

if($form_type == "INR03")
{
	$page->title("INR-03 - Retail Furniture in Third-party Store: Delete Mail");
}
elseif($form_type == "AF")
{
	$page->title("Application Form: Delete Mail");
}
else
{
	$page->title("Capital Expenditure Request: Delete Mail");
}
$form->render();

$page->footer();

?>