<?php
/********************************************************************

	cer_draft_revenues.php

    Application Form: revenues
    
	Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-02-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-02-16
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_draft_functions.php";

check_access("has_access_to_cer_drafts");
set_referer("cer_draft_revenue.php");


if(!param("brand"))
{
	$link = "cer_draft.php?did=" . param("did");
	redirect($link);
}


/********************************************************************
    prepare all data needed
*********************************************************************/

$currency = get_draft_currency(param("did"));
$cer_basicdata = get_draft_basicdata(param("did"));

$years = array();

$sql = "select * " .
       "from cer_draft_expenses " . 
	   "where cer_expense_type = 1 and cer_expense_draft_id = " . param("did");

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$years[] = $row["cer_expense_year"];
}

//get revenue values
$quantity_watches = array();
$aveargeprice_watches = array();
$quantity_jewellery = array();
$aveargeprice_jewellery = array();
$revenue_accessories = array();
$revenue_customerservice = array();
$sales_reduction = array();
$wholesale_margin = array();

$customer_frequencies = array();
$days_open_per_year = array();
$working_hours_per_week = array();
$opening_hours_per_week = array();


$sales_reduction["watches"] = "";
$sales_reduction["bijoux"] = "";
$sales_reduction["accessories"] = "";
$sales_reduction["service"] = "";
$sales_reduction["creditcards"] = "";

$wholesale_margin["watches"] = "";
$wholesale_margin["bijoux"] = "";
$wholesale_margin["accessories"] = "";
$wholesale_margin["service"] = "";

$cost_of_products_sold["watches"] = "";
$cost_of_products_sold["bijoux"] = "";
$cost_of_products_sold["accessories"] = "";
$cost_of_products_sold["service"] = "";

$sql = "select * from cer_draft_revenues " . 
       "where cer_revenue_draft_id = " . param("did") . 
	   " and cer_revenue_brand_id = " . param("brand");

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$quantity_watches[$row["cer_revenue_id"]] = $row["cer_revenue_quantity_watches"];
	$aveargeprice_watches[$row["cer_revenue_id"]] = $row["cer_revenue_aveargeprice_watches"];
	$quantity_jewellery[$row["cer_revenue_id"]] = $row["cer_revenue_quantity_jewellery"];
	$aveargeprice_jewellery[$row["cer_revenue_id"]] = $row["cer_revenue_aveargeprice_jewellery"];

	$revenue_accessories[$row["cer_revenue_id"]] = $row["cer_revenue_accessories"];
	$revenue_customerservice[$row["cer_revenue_id"]] = $row["cer_revenue_customer_service"];

	if($row["cer_revenue_sales_reduction_watches"] > 0)
	{
		$sales_reduction["watches"] = $row["cer_revenue_sales_reduction_watches"];
		$sales_reduction["bijoux"] = $row["cer_revenue_sales_reduction_bijoux"];
		$sales_reduction["accessories"] = $row["cer_revenue_sales_reduction_accessories"];
		$sales_reduction["service"] = $row["cer_revenue_sales_reduction_cservice"];
		$sales_reduction["creditcards"] = $row["cer_revenue_reduction_credit_cards"];

		$wholesale_margin["watches"] = $row["cer_revenue_wholesale_margin_watches"];
		$wholesale_margin["bijoux"] = $row["cer_revenue_wholesale_margin_bijoux"];
		$wholesale_margin["accessories"] = $row["cer_revenue_wholesale_margin_accessories"];
		$wholesale_margin["service"] = $row["cer_revenue_wholesale_margin_services"];

		$cost_of_products_sold["watches"] = $row["cer_revenue_cost_watches"];
		$cost_of_products_sold["bijoux"] = $row["cer_revenue_cost_jewellery"];
		$cost_of_products_sold["accessories"] = $row["cer_revenue_cost_accessories"];
		$cost_of_products_sold["service"] = $row["cer_revenue_cost_service"];
	}
}

$sql = "select * from cer_draft_revenues " . 
       "where cer_revenue_draft_id = " . param("did") . 
	   " and cer_revenue_brand_id = " . dbquote($first_brand);

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	
	$customer_frequencies[$row["cer_revenue_id"]] = $row["cer_revenue_customer_frequency"];

	$days_open_per_year[$row["cer_revenue_id"]] = $row["cer_revenue_total_days_open_per_year"];
	$opening_hours_per_week[$row["cer_revenue_id"]] = $row["cer_revenue_total_hours_open_per_week"];
	$working_hours_per_week[$row["cer_revenue_id"]] = $row["cer_revenue_total_workinghours_per_week"];
}


$sql_list1 = "select cer_revenue_id, cer_revenue_year, " .
             "cer_revenue_quantity_watches, cer_revenue_aveargeprice_watches, " . 
			 "cer_revenue_quantity_jewellery, cer_revenue_aveargeprice_jewellery, " .
             "cer_revenue_watches, cer_revenue_jewellery, cer_revenue_accessories , " . 
			 "cer_revenue_customer_service " .
             "from cer_draft_revenues ";

$list1_filter = "cer_revenue_draft_id = " . param("did") . " and cer_revenue_brand_id = " . param("brand");
$list3_filter = "cer_revenue_draft_id = " . param("did") . " and cer_revenue_brand_id = " . dbquote($first_brand);


//get list_totals
$revenue_ids = array();
$list_totals = array();
$list_totals[1] = 0;
$list_totals[2] = 0;
$list_totals[3] = 0;
$list_totals[4] = 0;
$list_totals[7] = 0;
$list_totals[8] = 0;
$list_totals[9] = 0;
$total_gross_sales = array();

$sql = $sql_list1 . " where " . $list1_filter;
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$list_totals[1] = $list_totals[1] + $row["cer_revenue_quantity_watches"];
	$list_totals[2] = $list_totals[2] + $row["cer_revenue_watches"];
	$list_totals[3] = $list_totals[3] + $row["cer_revenue_quantity_jewellery"];
	$list_totals[4] = $list_totals[4] + $row["cer_revenue_jewellery"];
	$list_totals[7] = $list_totals[7] + $row["cer_revenue_accessories"];
	$list_totals[8] = $list_totals[8] + $row["cer_revenue_customer_service"];

	$total_gross_sales[$row["cer_revenue_id"]] = $row["cer_revenue_watches"] + $row["cer_revenue_jewellery"] + $row["cer_revenue_accessories"] + $row["cer_revenue_customer_service"];

	$revenue_ids[] = $row["cer_revenue_id"];
}
$list_totals[9] = array_sum($total_gross_sales);


// get all expenses for for each year
$years = array();
$year_amounts = array();

$sql  = "select cer_expense_type, cer_expense_year, cer_expense_amount " .
		"from cer_draft_expenses " .
		"where cer_expense_type = 14 and cer_expense_draft_id = " . param("did");

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$years[$row["cer_expense_year"]] = $row["cer_expense_year"];
	$year_amounts[$row["cer_expense_year"]] = $row["cer_expense_amount"];
}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_drafts", "cer_drafts");

$form->add_hidden("did", param("did"));
$form->add_hidden("brand", param("brand"));
$form->add_section("Revenue Parameters " . $cer_brands[param("brand")]);


$form->add_edit("sales_reduction_watches", "Sales reduction watches in % of gross sales", 0, $sales_reduction["watches"], TYPE_DECIMAL, 12,2,1, "sales_reduction");

$form->add_edit("sales_reduction_bijoux", "Sales reduction jewellery in % of gross sales", 0, $sales_reduction["bijoux"], TYPE_DECIMAL, 12,2,1, "sales_reduction");

$form->add_edit("sales_reduction_accessories", "Sales reduction accessories in % of gross sales", 0, $sales_reduction["accessories"], TYPE_DECIMAL, 12,2,1, "sales_reduction");

$form->add_edit("sales_reduction_cservice", "Sales reduction customer service in % of gross sales", 0, $sales_reduction["service"], TYPE_DECIMAL, 12,2,1, "sales_reduction");

$form->add_edit("sales_reduction_creditcards", "Sales reduction credit cards % of gross sales", 0, $sales_reduction["creditcards"], TYPE_DECIMAL, 6,2,1, "sales_reduction");

$form->add_edit("wholesale_margin_watches", "Wholesale margin watches in %", 0, $wholesale_margin["watches"], TYPE_DECIMAL, 12,2, 1, "whole_sale_margin");

$form->add_edit("wholesale_margin_bijoux", "Wholesale margin jewellery in %", 0, $wholesale_margin["bijoux"], TYPE_DECIMAL, 12,2, 1, "whole_sale_margin");

$form->add_edit("wholesale_margin_accessories", "Wholesale margin accessories in %", 0, $wholesale_margin["accessories"], TYPE_DECIMAL, 12,2, 1, "whole_sale_margin");

$form->add_edit("wholesale_margin_services", "Wholesale margin customer service in %", 0, $wholesale_margin["service"], TYPE_DECIMAL, 12,2, 1, "whole_sale_margin");


$form->add_section("Cost of Products Sold in Percent of Sales");
$form->add_comment("Cost of products and services sold are: Purchase Price to Swatch Group or Agents - Retail Purchase Price.");
$form->add_comment("Please check the following checkbox in case calculation should be based on NET SALES. <br />Otherwise the calculation will be based on GROSS SALES.");

$form->add_checkbox("cer_basicdata_cost_on_net_sales", "cost of products are to be calculated on the base of NET SALES VALUES", $cer_basicdata["cer_basicdata_cost_on_net_sales"], "", "Calculation Mode");
$form->add_comment("");

$form->add_edit("cost_watches", "Cost of watches sold in % of sales", 0, $cost_of_products_sold["watches"], TYPE_DECIMAL, 12,2);
$form->add_edit("cost_jewellery", "Cost of jewellery sold in % of sales", 0, $cost_of_products_sold["bijoux"], TYPE_DECIMAL, 12,2);
$form->add_edit("cost_accessories", "Cost of accessories sold in % of sales", 0, $cost_of_products_sold["accessories"], TYPE_DECIMAL, 12,2);
$form->add_edit("cost_service", "Cost of customer service sold in % of sales", 0, $cost_of_products_sold["service"], TYPE_DECIMAL, 12,2);

$form->add_section("Revenue Calculation Helper");
$form->add_comment("Enter a percentage in case you like the tabel of revenues to be autocompleted after entering a value for the first year. <br />Values of the following years will be calculated on the basis of your inputs.");
$form->add_edit("cer_basicdata_revenue_p1", "Annual percentage increase in quantities", 0, $cer_basicdata["cer_basicdata_revenue_p1"], TYPE_DECIMAL, 6,2);
$form->add_edit("cer_basicdata_revenue_p2", "Annual percentage increase in average price", 0, $cer_basicdata["cer_basicdata_revenue_p2"], TYPE_DECIMAL, 6,2);
$form->add_edit("cer_basicdata_revenue_p3", "Annual percentage increase in gross sales on accessories", 0, $cer_basicdata["cer_basicdata_revenue_p3"], TYPE_DECIMAL, 6,2);
$form->add_edit("cer_basicdata_revenue_p4", "Annual percentage increase in gross sales on services", 0, $cer_basicdata["cer_basicdata_revenue_p4"], TYPE_DECIMAL, 6,2);

$form->add_button("form_save", "Save Data");


/********************************************************************
    build list of standard revenues
*********************************************************************/
$list1 = new ListView($sql_list1, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list1->set_title("Sales Revenues " . $cer_brands[param("brand")] . " in " . $currency["symbol"]);
$list1->set_entity("cer_draft_revenues");
$list1->set_filter($list1_filter);
$list1->set_order("cer_revenue_year");

$list1->add_column("cer_revenue_year", "Year");

$list1->add_number_edit_column("cer_revenue_quantity_watches", "Quantity\nWatches", "4",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $quantity_watches);
$list1->add_number_edit_column("cer_revenue_aveargeprice_watches", "Average Price\nWatches in " . $currency["symbol"], "12",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $aveargeprice_watches);

$list1->add_column("cer_revenue_watches", "Gross Sales\nWatches", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK | COLUMN_ALIGN_RIGHT);

$list1->add_number_edit_column("cer_revenue_quantity_jewellery", "Quantity\nJewellery", "4",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $quantity_jewellery);
$list1->add_number_edit_column("cer_revenue_aveargeprice_jewellery", "Average Price\nJewellery in " . $currency["symbol"], "12",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $aveargeprice_jewellery);
$list1->add_column("cer_revenue_jewellery", "Gross Sales\nJewellery", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK | COLUMN_ALIGN_RIGHT);

$list1->add_number_edit_column("cer_revenue_accessories", "Gross Sales\nAccessories", "12",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $revenue_accessories);

$list1->add_number_edit_column("cer_revenue_customer_service", "Gross Sales\nCustomer Service", "12",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $revenue_customerservice);

$list1->set_footer("cer_revenue_year", "Total");
$list1->set_footer("cer_revenue_quantity_watches", number_format($list_totals[1] ,0));
$list1->set_footer("cer_revenue_watches", number_format($list_totals[2] ,0));
$list1->set_footer("cer_revenue_quantity_jewellery", number_format($list_totals[3] ,0));
$list1->set_footer("cer_revenue_jewellery", number_format($list_totals[4] ,0));

$list1->set_footer("cer_revenue_accessories", number_format($list_totals[7] ,0));

$list1->set_footer("cer_revenue_customer_service", number_format($list_totals[8] ,0));
$list1->add_text_column("total_gross_sales", "Total Gross\nSales Values",COLUMN_NO_WRAP | COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $total_gross_sales);

$list1->set_footer("total_gross_sales", number_format($list_totals[9] ,0));

$list1->add_button("list1_save", "Save Values");


/********************************************************************
    build list of standard revenues
*********************************************************************/
$list3 = new ListView($sql_list1, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list3->set_title("Other Sales Relevant Information");
$list3->set_entity("cer_draft_revenues");
$list3->set_filter($list3_filter);
$list3->set_order("cer_revenue_year");

$list3->add_column("cer_revenue_year", "Year");


$list3->add_number_edit_column("cer_revenue_total_days_open_per_year", "Total Days\n Open\nper Year", "12",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $days_open_per_year);

$list3->add_number_edit_column("cer_revenue_total_hours_open_per_week", "Total Hours\n Open\nper Week", "12",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $opening_hours_per_week);

$list3->add_number_edit_column("cer_revenue_total_workinghours_per_week", "Total Working\nHours per Week \nfor 1 Employee\n (working 100%)", "20",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $working_hours_per_week);


$list3->add_number_edit_column("cer_revenue_customer_frequency", "Customer\nFrequency\nper Day", "12",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $customer_frequencies);

$list3->add_button("list3_save", "Save Values");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

$list1->populate();
$list1->process();

$list3->populate();
$list3->process();


if($form->button("form_save"))
{
	if($form->validate())
	{
		
		//update cer_basicdata
		$fields = array();
    
		$value = dbquote($form->value("cer_basicdata_revenue_p1"));
		$fields[] = "cer_basicdata_revenue_p1 = " . $value;

		$value = dbquote($form->value("cer_basicdata_revenue_p2"));
		$fields[] = "cer_basicdata_revenue_p2 = " . $value;

		$value = dbquote($form->value("cer_basicdata_revenue_p3"));
		$fields[] = "cer_basicdata_revenue_p3 = " . $value;

		$value = dbquote($form->value("cer_basicdata_revenue_p4"));
		$fields[] = "cer_basicdata_revenue_p4 = " . $value;

		$value = dbquote($form->value("cer_basicdata_cost_on_net_sales"));
		$fields[] = "cer_basicdata_cost_on_net_sales = " . $value;


		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_drafts set " . join(", ", $fields) . " where cer_basicdata_id = " . param("did");
		$result = mysql_query($sql) or dberror($sql);



		//update cer_revenues
		$sql = "select cer_revenue_id " . 
			   "from cer_draft_revenues " . 
			   "where cer_revenue_draft_id = " . param("did") . 
			   " and cer_revenue_brand_id = " . param("brand");

		$res = mysql_query($sql) or dberror($sql);
		while($row = mysql_fetch_assoc($res))
		{
			$fields = array();
		
			$value = dbquote($_POST["sales_reduction_watches"]);
			$fields[] = "cer_revenue_sales_reduction_watches = " . $value;

			$value = dbquote($_POST["sales_reduction_bijoux"]);
			$fields[] = "cer_revenue_sales_reduction_bijoux = " . $value;

			$value = dbquote($_POST["sales_reduction_accessories"]);
			$fields[] = "cer_revenue_sales_reduction_accessories = " . $value;

			$value = dbquote($_POST["sales_reduction_cservice"]);
			$fields[] = "cer_revenue_sales_reduction_cservice = " . $value;

			$value = dbquote($_POST["sales_reduction_creditcards"]);
			$fields[] = "cer_revenue_reduction_credit_cards = " . $value;

			$value = dbquote($_POST["wholesale_margin_watches"]);
			$fields[] = "cer_revenue_wholesale_margin_watches = " . $value;

			$value = dbquote($_POST["wholesale_margin_bijoux"]);
			$fields[] = "cer_revenue_wholesale_margin_bijoux = " . $value;

			$value = dbquote($_POST["wholesale_margin_accessories"]);
			$fields[] = "cer_revenue_wholesale_margin_accessories = " . $value;

			$value = dbquote($_POST["wholesale_margin_services"]);
			$fields[] = "cer_revenue_wholesale_margin_services = " . $value;

			$value = dbquote($_POST["cost_watches"]);
			$fields[] = "cer_revenue_cost_watches = " . $value;

			$value = dbquote($_POST["cost_jewellery"]);
			$fields[] = "cer_revenue_cost_jewellery = " . $value;

			$value = dbquote($_POST["cost_accessories"]);
			$fields[] = "cer_revenue_cost_accessories = " . $value;

			$value = dbquote($_POST["cost_service"]);
			$fields[] = "cer_revenue_cost_service = " . $value;


			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql_u = "update cer_draft_revenues set " . join(", ", $fields) . 
				     " where cer_revenue_id = " . $row["cer_revenue_id"];
			$result = mysql_query($sql_u) or dberror($sql_u);

		}

		$form->message("Your data has bee saved.");
	}

		
}
elseif($list1->button("list1_save"))
{

	$v1 = $list1->values("cer_revenue_quantity_watches");
	$v2 = $list1->values("cer_revenue_aveargeprice_watches");
	$v3 = $list1->values("cer_revenue_quantity_jewellery");
	$v4 = $list1->values("cer_revenue_aveargeprice_jewellery");
	$v5 = $list1->values("cer_revenue_accessories");
	$v6 = $list1->values("cer_revenue_customer_service");

	foreach ($v1 as $key=>$value)
    {
        
            $fields = array();
    
            $value = dbquote($v1[$key]);
			$fields[] = "cer_revenue_quantity_watches = " . $value;

			$value = dbquote($v2[$key]);
			$fields[] = "cer_revenue_aveargeprice_watches = " . $value;

			$value = dbquote($v3[$key]);
			$fields[] = "cer_revenue_quantity_jewellery = " . $value;

			$value = dbquote($v4[$key]);
			$fields[] = "cer_revenue_aveargeprice_jewellery = " . $value;


			$value = dbquote($v1[$key]*$v2[$key]);
			$fields[] = "cer_revenue_watches = " . $value;

			$value = dbquote($v3[$key]*$v4[$key]);
			$fields[] = "cer_revenue_jewellery = " . $value;

			$value = dbquote($v5[$key]);
			$fields[] = "cer_revenue_accessories = " . $value;

			$value = dbquote($v6[$key]);
			$fields[] = "cer_revenue_customer_service = " . $value;


            $value1 = "current_timestamp";
            $fields[] = "date_modified = " . $value1;
    
            if (isset($_SESSION["user_login"]))
            {
                $value1 = $_SESSION["user_login"];
                $fields[] = "user_modified = " . dbquote($value1);
            }
       
            $sql = "update cer_draft_revenues set " . join(", ", $fields) . " where cer_revenue_id = " . $key;
            mysql_query($sql) or dberror($sql);
	}

	$reuslt = update_cost_of_products_sold(param("did"));

	$form->message("Your data has bee saved.");
	$link = "cer_draft_revenues.php?did=" . param("did");
	redirect($link);
}
elseif($list3->button("list3_save"))
{

	$v1 = $list3->values("cer_revenue_customer_frequency");
	$v2= $list3->values("cer_revenue_total_days_open_per_year");
	$v3= $list3->values("cer_revenue_total_workinghours_per_week");
	$v4= $list3->values("cer_revenue_total_hours_open_per_week");

	foreach ($v1 as $key=>$value)
    {
        
            $fields = array();
    
            
			$value = dbquote($v1[$key]);
			$fields[] = "cer_revenue_customer_frequency = " . $value;

			$value = dbquote($v2[$key]);
			$fields[] = "cer_revenue_total_days_open_per_year = " . $value;

			$value = dbquote($v3[$key]);
			$fields[] = "cer_revenue_total_workinghours_per_week = " . $value;

			$value = dbquote($v4[$key]);
			$fields[] = "cer_revenue_total_hours_open_per_week = " . $value;

            $value1 = "current_timestamp";
            $fields[] = "date_modified = " . $value1;
    
            if (isset($_SESSION["user_login"]))
            {
                $value1 = $_SESSION["user_login"];
                $fields[] = "user_modified = " . dbquote($value1);
            }
       
            $sql = "update cer_draft_revenues set " . join(", ", $fields) . " where cer_revenue_id = " . $key;
            mysql_query($sql) or dberror($sql);
	}

	$form->message("Your data has bee saved.");
	
}

  
/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_drafts");

require "include/draft_page_actions.php";
$page->header();
$page->title($cer_basicdata['cer_basicdata_title'] . ": Revenues " . $cer_brands[param("brand")]);

require_once("include/tabs_draft.php");
$form->render();
echo "<br />";

$list1->render();


echo "<br />";
echo "<br />";
$list3->render();

?>

<div id="customer_service" style="display:none;">
    Customer service in percent of gross sales
</div>

<div id="sales_reduction" style="display:none;">
    Sales reduction in percent of gross sales
</div>

<div id="whole_sale_margin" style="display:none;">
    Wholesale margin in percent on costs of products sold
</div> 

<div id="additional_whole_sale_margin" style="display:none;">
    Additional margin should only be introduced if the POS is an agent POS because a third party won't earn any wholesale margin.
</div> 



<?php
require "include/draft_footer_scripts.php";
require "include/helper_calculate_draft_revenues.js";

$page->footer();

?>