<?php
/********************************************************************

    welcome.php

    Entry page for the cer section.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-26
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-26
    Version:        1.1.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";


check_access("has_access_to_cer");

$select_country = false;

$country_filter = "";
$tmp = array();
$sql = "select * from country_access " .
	   "where country_access_user = " . user_id();


$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{            
	$tmp[] = $row["country_access_country"];
}

if(count($tmp) > 0) {
	$country_filter = " country_id IN (" . implode(",", $tmp) . ") ";
	$select_country = true;
}



if(has_access("has_full_access_to_cer") 
	or has_access("can_view_all_cer_date") or $select_country == true)
{

	set_referer("cer_projects.php");

	/********************************************************************
	prepare all data needed
	*********************************************************************/
	//create sqls for the project selection

	
	
	
	if($country_filter)
	{
		if(has_access("has_access_to_retail_only")) {
			$country_filter .= " and project_cost_type in (1) ";
		}

		if(has_access("has_access_to_wholesale")) {
			$country_filter .= " and project_cost_type in (2, 6) ";
		}
		
		$sql_years = "select distinct left(order_date, 4) as year " .
					  "from orders ".
			          "inner join project_costs on project_cost_order = order_id " .
					  "where order_actual_order_state_code <= '900' and (order_archive_date = '0000-00-00' or order_archive_date is null) " . 
					  "order by year DESC";



		
		$sql_product_lines = "select distinct product_line_id, product_line_name ".
							 "from projects ".
							 "left join product_lines on  project_product_line = product_line_id ".
							 "left join orders on project_order = order_id ".
			                 "inner join project_costs on project_cost_order = order_id " .
			                 "left join countries on order_shop_address_country = countries.country_id ".
							 "where order_actual_order_state_code <= '900' " .
			                 "and (order_archive_date = '0000-00-00' or order_archive_date is null) " . 
			                 " and " . $country_filter .
							 " order by product_line_name";

		$sql_countries = "select distinct country_id, country_name ".
						 "from projects ".
						 "left join orders on project_order = order_id ".
			             "inner join project_costs on project_cost_order = order_id " .
						 "left join countries on order_shop_address_country = countries.country_id ".
						 "where order_actual_order_state_code <= '900' " . 
			             " and (order_archive_date = '0000-00-00' or order_archive_date is null) " . 
			             " and " . $country_filter .
						 "order by country_name";
	}
	else
	{
		
		$legal_type_filter = '';

		if(has_access("has_access_to_retail_only")) {
			$legal_type_filter .= " and project_cost_type in (1) ";
		}

		if(has_access("has_access_to_wholesale")) {
			$legal_type_filter .= " and project_cost_type in (2, 6) ";
		}

		$sql_years = "select distinct left(order_date, 4) as year " .
					  "from orders ".
			           "inner join project_costs on project_cost_order = order_id " .
					  "where order_actual_order_state_code <= '900' and (order_archive_date = '0000-00-00' or order_archive_date is null) " .  $legal_type_filter . 
					  "order by year DESC";



		$sql_product_lines = "select distinct product_line_id, product_line_name ".
							 "from projects ".
							 "left join product_lines on  project_product_line = product_line_id ".
							 "left join orders on project_order = order_id ".
			                 "inner join project_costs on project_cost_order = order_id " .
							 "where order_actual_order_state_code <= '900' and (order_archive_date = '0000-00-00' or order_archive_date is null) " . $legal_type_filter .
							 " order by product_line_name";

		$sql_countries = "select distinct country_id, country_name ".
						 "from projects ".
						 "left join orders on project_order = order_id ".
						 "left join countries on order_shop_address_country = countries.country_id ".
			             "inner join project_costs on project_cost_order = order_id " .
						 "where order_actual_order_state_code <= '900' and (order_archive_date = '0000-00-00' or order_archive_date is null) " . $legal_type_filter .
						 "order by country_name";
	}


	/********************************************************************
	build form
	*********************************************************************/
	$form = new Form("orders", "orders");

	$form->add_list("from_year", "From Year", $sql_years, 0);
	$form->add_list("to_year", "To Year", $sql_years, 0);
	$form->add_section(" ");
	$form->add_list("productline", "Product Line", $sql_product_lines, 0);
	$form->add_list("country", "Country", $sql_countries, 0);
	$form->add_edit("projectnumber", "Project Number");

	$form->add_button("proceed", "Show Projects");


	/********************************************************************
		Populate form and process button clicks
	*********************************************************************/ 
	$form->populate();
	$form->process();

	if ($form->button("proceed"))
	{
		$y1 = $form->value("from_year");
		$y2 = $form->value("to_year");
		$p = $form->value("productline");
		$c = $form->value("country");
		$pn = $form->value("projectnumber");

		$link = "cer_projects.php?y1=". $y1 . "&y2=" . $y2 . "&p=" . $p . "&c=" . $c . "&pn=" . $pn;

		redirect($link);
	}

	/********************************************************************
		render page
	*********************************************************************/ 
	$page = new Page("cer_projects");

	$page->header();
	$page->title("CER/AF: Project Selection");
	$form->render();

	?>

	<script type="text/javascript">
		
		
		var selectedInput = null;
		$(document).ready(function(){
		  $("#projectnumber").focus();

		  $('input').focus(function() {
					selectedInput = this;
				});

		});

		document.onkeydown = process_key;
		
		function process_key(e)
		{
		  if( !e ) 
		  {
			if( window.event ) 
			{
			  e = window.event;
			} 
			else 
			{
			  return;
			}
		  }

		  if(e.keyCode==13)
		  {
			  button('proceed');
		  }
		}
	</script>

	<?php

	$page->footer();
}
elseif(has_access("has_access_to_his_cer") or has_access("can_view_his_cer_data") or has_access("cer_has_full_access_to_his_projects"))
{
	$link = "cer_projects.php";
	redirect($link);
}
else
{
	redirect("noaccess.php");	
}
?>