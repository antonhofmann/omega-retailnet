<?php
/********************************************************************

    ln_general.php

    Application Form: lease negiotiation form
    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-11-06
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-11-06
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";
require_once "../shared/func_posindex.php";

check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}

/********************************************************************
    prepare all data needed
*********************************************************************/
$submitted_by = get_user($ln_basicdata["ln_basicdata_submitted_by"]);
$resubmitted_by = get_user($ln_basicdata["ln_basicdata_resubmitted_by"]);
$rejected_by = get_user($ln_basicdata["ln_basicdata_rejected_by"]);

$project = get_project(param("pid"));

$client_currency = get_cer_currency(param("pid"), $cer_version);
$currency_symbol = $client_currency["symbol"];

$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_address"] . ", " .
		$project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];

$project_detail = $project["project_number"] . ", " . $project["project_costtype_text"] . " " . $project["postype_name"] . ", " . $project["product_line_name"] . ", " . $project["projectkind_name"];


$choices = array();

$choices[0] = "No";
$choices[1] = "Yes";

$submission_possible = 0;


//check if submission is possible (only if business plan is filled in
$submission_possible = 0;

$revenues = 0;
$sql = "select sum(cer_revenue_watches) as total " .
       "from cer_revenues " .
	   " where cer_revenue_cer_version = 0 and cer_revenue_project = " . param("pid");

$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	$revenues = $row["total"];
}

$expenses = 0;
$sql = "select sum(cer_expense_amount) as total " .
       "from cer_expenses " .
	   " where cer_expense_cer_version = 0 and cer_expense_project = " . param("pid");

$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	$expenses = $row["total"];
}


$salaries = 0;
$sql = "select sum(cer_salary_fixed_salary) as total " .
       "from cer_salaries " .
	   " where cer_salary_cer_version = 0 and cer_salary_project = " . param("pid");

$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	$salaries = $row["total"];
}

$posdata = get_pos_data($project["project_order"]);
$posleases = get_pos_leasedata($posdata["posorder_posaddress"], $posdata["posorder_order"]);
if(!isset($posleases["poslease_startdate"]) or $posleases["poslease_startdate"] == NULL or $posleases["poslease_startdate"] == '0000-00-00')
{
	$submission_possible = 0;
}
else
{
	$submission_possible = 1;
	
}


if($submission_possible == 1)
{
	$construction = get_pos_intangibles(param("pid"), 1);
	$fixturing = get_pos_intangibles(param("pid"), 3);
	$keymoney = get_pos_intangibles(param("pid"), 15);

	if((array_key_exists("cer_investment_amount_cer_loc", $construction) and $construction["cer_investment_amount_cer_loc"] > 0) or (array_key_exists("cer_investment_amount_cer_loc", $fixturing) and $fixturing["cer_investment_amount_cer_loc"] > 0)  or (array_key_exists("cer_investment_amount_cer_loc", $keymoney) and $keymoney["cer_investment_amount_cer_loc"] > 0))
	{
		$submission_possible = 1;
	}
	else
	{
		$submission_possible = 0;
	}
}


if($revenues > 0 and $submission_possible == 1)
{
	$submission_possible = 1;
}
else
{
	$submission_possible = 0;
}


if($salaries > 0 and $submission_possible == 1)
{
	$submission_possible = 1;
}
else
{
	$submission_possible = 0;
}

if($expenses > 0 and $submission_possible == 1)
{
	$submission_possible = 1;
}
else
{
	$submission_possible = 0;
}


$sellout_necessary = 0;
$sellout_data_ok = true;
$project_id = param("pid");
$project_order = $project["project_order"];

if(($project["project_projectkind"] == 2 
        or $project["project_projectkind"] == 3 
		or $project["project_projectkind"] == 5)) // renovation or tekover/renovation or lease renewal
{
	$sellout_necessary = 1;
}
elseif($project["project_projectkind"] == 6 and $project["project_is_relocation_project"] == 1 and $project["project_relocated_posaddress_id"] > 0) // relocation
{
	$latest_project = get_latest_pos_project($project["project_relocated_posaddress_id"]);
	
	if(count($latest_project) > 0)
	{
		$sellout_necessary = 1;
		$project_id = $latest_project["project_id"];
		$project_order = $latest_project["order_id"];
	}
}

if($sellout_necessary == 1)
{
	$sellout_data_ok = check_sellout_data($project_order, $cer_basicdata["cer_basicdata_firstyear"]);
}


if($project['project_projectkind'] != 5) // lease reneval
{
	if(!$ln_basicdata["ln_basicdata_remarks"] 
	)
	{
		$submission_possible = 0;
	}
}
else {

	if(!$ln_basicdata["ln_basicdata_remarks1"] 
	or !$ln_basicdata["ln_basicdata_remarks2"]
	or !$ln_basicdata["ln_basicdata_remarks3"]
	or !$ln_basicdata["ln_basicdata_remarks4"]
	)
	{
		$submission_possible = 0;
	}
}

//AF/LN Supporting Documents
$sql_attachment = "select distinct order_file_id, order_file_visited, ".
                  "    order_file_title, order_file_description, ".
                  "    order_file_path, file_type_name, ".
                  "    order_files.date_created, ".
                  "    order_file_category_name, order_file_category_priority, ".
                  "    concat(user_name, ' ', user_firstname) as owner_fullname ".
                  "from order_files  " .
                  "left join order_file_categories on order_file_category_id = order_file_category ".
                  "left join users on user_id = order_file_owner ".
                  "left join file_types on order_file_type = file_type_id ";

$list1_filter = "order_file_category_id = 16 and order_file_order = " . $project["project_order"];


/********************************************************************
    build form
*********************************************************************/
$form = new Form("ln_basicdata", "ln_basicdata");

$form->add_hidden("pid", param("pid"));



include("include/project_head.php");



$form->add_section("POS locations of " . BRAND . " in the area");
$form->add_edit("ln_basicdata_numofpos_in_area", "Number of POS (for the same Brand) in the same city area", "" , $ln_basicdata['ln_basicdata_numofpos_in_area'], TYPE_INT, 8);
$form->add_edit("ln_basicdata_numofcpos_in_area", " - of which " . BRAND . " Corporate Stores ", "" , $ln_basicdata['ln_basicdata_numofcpos_in_area'], TYPE_INT, 8);
$form->add_edit("ln_basicdata_numoffpos_in_area", " - of which " . BRAND . " Franchise Stores ", "" , $ln_basicdata['ln_basicdata_numoffpos_in_area'], TYPE_INT, 8);



$form->add_section('Average Buildout Costs');
$form->add_comment("Please indicate the average buildout costs (" . $currency_symbol . "/m2) of corporate POS for " . BRAND . " <strong>in the same area</strong>.<br />Take similar corporate POS (e.g. compare airport stores to airport stores, SIS to SIS etc.)<br />If not enough other POS in the same city, please provide the information for the same market or same country.<br />Click to <a id='calculateabc' href='javascript:void(0);'>calculate the average buildout costs</a>.");
		
$form->add_edit("ln_basicdata_average_buildout_costs", "Average Buildout Costs in " . $currency_symbol , 0, $ln_basicdata["ln_basicdata_average_buildout_costs"], TYPE_DECIMAL, 10,2);


if($project['project_projectkind'] != 5) // lease reneval
{
	$form->add_section("Key Points");
	$form->add_multiline("ln_basicdata_remarks", "Key Points", 10, 0, $ln_basicdata["ln_basicdata_remarks"]);
	$form->add_hidden("ln_basicdata_remarks1");
	$form->add_hidden("ln_basicdata_remarks2");
	$form->add_hidden("ln_basicdata_remarks3");
	$form->add_hidden("ln_basicdata_remarks4");
}
else {
	
	$form->add_hidden("ln_basicdata_remarks");

	$form->add_section("Reasons/Commitment");
	$form->add_comment("Indicate the reasons of the lease renewal request (location, performance, ...)");
	$form->add_multiline("ln_basicdata_remarks1", "Reasons*", 6, NOTNULL, $ln_basicdata["ln_basicdata_remarks1"]);


	$form->add_comment("Justify projected sales in view of pas performance (make also reference to other lease renewals)");
	$form->add_multiline("ln_basicdata_remarks2", "Justification*", 6, NOTNULL, $ln_basicdata["ln_basicdata_remarks2"]);

	$form->add_comment("Commitment that Sales targets are very likely to be achieved)");
	$form->add_multiline("ln_basicdata_remarks3", "Commitment by the Brand Manager*", 6, NOTNULL, $ln_basicdata["ln_basicdata_remarks3"]);
	$form->add_multiline("ln_basicdata_remarks4", "Commitment by the Country Manager*", 6, NOTNULL, $ln_basicdata["ln_basicdata_remarks4"]);

	
}

$form->add_section("General Remarks");
$form->add_multiline("ln_basicdata_rent", "General Remarks", 8, 0, $ln_basicdata["ln_basicdata_rent"]);

$passed_rent = "";
if($ln_basicdata["ln_basicdata_passedrental"] > 0) {
	$passed_rent = $ln_basicdata["ln_basicdata_passedrental"];
}


if($project["project_projectkind"] == 2 or $project["project_projectkind"] == 3 or $project["project_projectkind"] == 4 )
{
	$form->add_edit("ln_basicdata_passedrental", "Rent of last full year in " . $cer_basicdata['currency_symbol'], "" , $passed_rent, TYPE_DECIMAL, 12, 0);
}
elseif($project["project_projectkind"] == 6 and $project["project_is_relocation_project"] == 1)
{
	$form->add_edit("ln_basicdata_passedrental", "Rent of last full year in " . $cer_basicdata['currency_symbol'], "" , $passed_rent, TYPE_DECIMAL, 12, 0);
}
else
{
	$form->add_hidden("ln_basicdata_passedrental");
}


$form->add_section("Attachments");

$deletable = "";
if(has_access("can_delete_uploaded_files"))
{
	$deletable = DELETABLE;
}

$form->add_upload("ln_basicdata_floorplan", "Mall Map/Street Map (PDF only)*", "/files/ln/". $ln_basicdata["project_number"], NOTNULL | $deletable, $ln_basicdata["ln_basicdata_floorplan"], 1, "floorplan");

$form->add_upload("ln_basicdata_mall_all_floors", "Mall Map all floors only in case of a Mall (PDF only)", "/files/ln/". $ln_basicdata["project_number"], 0 | $deletable, $ln_basicdata["ln_basicdata_mall_all_floors"], 1, "floorplan");

$form->add_upload("ln_basicdata_store_layout", "Store Layout (PDF only)", "/files/ln/". $ln_basicdata["project_number"], 0 | $deletable, $ln_basicdata["ln_basicdata_store_layout"], 1, "floorplan");

$form->add_upload("ln_basicdata_pix1", "Photo 1 (JPG only)*", "/files/ln/". $ln_basicdata["project_number"], NOTNULL | $deletable, $ln_basicdata["ln_basicdata_pix1"]);
$form->add_upload("ln_basicdata_pix2", "Photo 2 (JPG only)*", "/files/ln/". $ln_basicdata["project_number"], NOTNULL | $deletable, $ln_basicdata["ln_basicdata_pix2"]);
$form->add_upload("ln_basicdata_pix3", "Photo 3 (JPG only)*", "/files/ln/". $ln_basicdata["project_number"], NOTNULL | $deletable, $ln_basicdata["ln_basicdata_pix3"]);
$form->add_upload("ln_basicdata_draft_aggreement", "Draft Lease Agreement (PDF only)", "/files/ln/". $ln_basicdata["project_number"], 0 | $deletable, $ln_basicdata["ln_basicdata_draft_aggreement"]);


/*
if($ln_basicdata["ln_basicdata_approvedby_cm"] == 1 and $ln_basicdata["ln_basicdata_approved_by_ekl"] == 1)
{
	$form->add_section("Approvals");
	$form->add_label("approved1", "Approved by Country Manager", 0, "Yes");
	$form->add_label("approved2", "Approved by EKL responsible", 0, "Yes");
	$form->add_hidden("ln_basicdata_approvedby_cm", $ln_basicdata["ln_basicdata_approvedby_cm"]);
	$form->add_hidden("ln_basicdata_approved_by_ekl", $ln_basicdata["ln_basicdata_approved_by_ekl"]);
}
else
{
	$form->add_section("Approvals");
	$form->add_radiolist("ln_basicdata_approvedby_cm", "Approved by Country Manager", $choices, 0, $ln_basicdata["ln_basicdata_approvedby_cm"]);
	$form->add_radiolist("ln_basicdata_approved_by_ekl", "Approved by EKL responsible", $choices, 0, $ln_basicdata["ln_basicdata_approved_by_ekl"]);
}
*/

$form->add_section("Approvals");
$form->add_radiolist("ln_basicdata_approvedby_cm", "Approved by Country Manager", $choices, 0, $ln_basicdata["ln_basicdata_approvedby_cm"]);
$form->add_radiolist("ln_basicdata_approved_by_ekl", "Approved by EKL responsible", $choices, 0, $ln_basicdata["ln_basicdata_approved_by_ekl"]);




//if($ln_basicdata["ln_basicdata_approvedby_cm"] == 1 and $ln_basicdata["ln_basicdata_approved_by_ekl"] == 1)
//if((param("ln_basicdata_approvedby_cm") == 1 and param("ln_basicdata_approved_by_ekl") == 1) or ($ln_basicdata["ln_basicdata_approvedby_cm"] == 1 and //$ln_basicdata["ln_basicdata_approved_by_ekl"] == 1))
//{
	
	$form->add_section("Submission");

	if(has_access("can_edit_ln_workflow_data")) {
		$form->add_checkbox("ln_no_ln_submission_needed", "No LN submission needed", $ln_basicdata["ln_no_ln_submission_needed"], 0, "LN Submission");
	}
	else
	{
		$form->add_hidden("ln_no_ln_submission_needed");
	}
	
	$by = "";
	if($submitted_by["name"])
	{
		$by = " by " . $submitted_by["name"] . " " . $submitted_by["firstname"];
	}
	
	$form->add_label("date",  "Submission Date", 0,  to_system_date($ln_basicdata["ln_basicdata_submitted"]) . $by);

	$by = "";
	if($resubmitted_by["name"])
	{
		$by = " by " . $resubmitted_by["name"] . " " . $resubmitted_by["firstname"];
	}

	$form->add_label("date1",  "Date of latest Resubmission", 0,  to_system_date($ln_basicdata["ln_basicdata_resubmitted"]). $by);

	$by = "";
	if($rejected_by["name"])
	{
		$by = " by " . $rejected_by["name"] . " " . $rejected_by["firstname"];
	}

	$form->add_label("date2",  "Date of Rejection", 0,  to_system_date($ln_basicdata["ln_basicdata_rejected"]). $by);

	
	
	if($submission_possible == 0) {
		$form->add_comment("Please be aware that submission of LN only is possible in case of a completed business plan.");
	}

	$can_submit = false;
	if($project['project_projectkind'] != 5
		and $ln_basicdata["ln_basicdata_remarks"]
		and $ln_basicdata["ln_basicdata_floorplan"]
	    and $ln_basicdata["ln_basicdata_pix1"]
	    and $ln_basicdata["ln_basicdata_pix2"]
	    and $ln_basicdata["ln_basicdata_pix3"]) {
		
		$can_submit = true;
		
	}
	elseif ($ln_basicdata["ln_basicdata_remarks1"] 
	   and $ln_basicdata["ln_basicdata_remarks2"]
	   and $ln_basicdata["ln_basicdata_remarks3"]
	   and $ln_basicdata["ln_basicdata_remarks4"]
	   and $ln_basicdata["ln_basicdata_floorplan"]
	   and $ln_basicdata["ln_basicdata_pix1"]
	   and $ln_basicdata["ln_basicdata_pix2"]
	   and $ln_basicdata["ln_basicdata_pix3"])
	{
		$can_submit = true;
	}
	
	/*temporarily deactivated*/
	if(1==1 or $can_submit == true)
	{
		
		if($ln_basicdata["ln_basicdata_submitted"] == NULL or $ln_basicdata["ln_basicdata_submitted"] == "0000-00-00")
		{
			$button_Text = "Submit Lease Negotiation";
		}
		else
		{
			$button_Text = "Resubmit Lease Negotiation";
			$submission_possible = 1;
			$sellout_necessary = 0;
		}
		$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" /><a href=\"#\" onClick=\"button('submit')\">" .  $button_Text . "</a>";

		/*temporarily deactivated*/
		if($ln_basicdata["ln_basicdata_locked"] == 0  
			and (1==1 or $submission_possible == 1)) {
			
			if(($ln_basicdata["ln_basicdata_locked"] == 0 and has_access("has_access_to_his_cer")) or has_access("has_full_access_to_cer"))
			{
				/*temporarily deactivated*/
				/*
				if($sellout_necessary == 1 and $sellout_data_ok == true)
				{
					$form->add_label("submit", "Submission", RENDER_HTML, $link);
				}
				elseif($sellout_necessary == 0)
				{
					$form->add_label("submit", "Submission", RENDER_HTML, $link);
				}
				else
				{
					$form->add_comment("Please be aware that submission of LN only is possible in case of a completed business plan.");
				}
				*/
				$form->add_label("submit", "Submission", RENDER_HTML, $link);
			}

		}
	}
//}



//if(($ln_basicdata["ln_basicdata_approvedby_cm"] == 0 or $ln_basicdata["ln_basicdata_approved_by_ekl"] == 0) and $ln_basicdata["ln_basicdata_submitted"] != NULL and $ln_basicdata["ln_basicdata_submitted"] != '0000-00-00')
//{
if($ln_basicdata["ln_basicdata_submitted"] != NULL and $ln_basicdata["ln_basicdata_submitted"] != '0000-00-00')
{
	if(has_access("can_reject_submissions"))
	{

		if($ln_basicdata["ln_basicdata_resubmitted"] and $ln_basicdata["ln_basicdata_rejected"] > $ln_basicdata["ln_basicdata_resubmitted"])
		{

		}
		elseif(!$ln_basicdata["ln_basicdata_resubmitted"] and $ln_basicdata["ln_basicdata_rejected"] > $ln_basicdata["ln_basicdata_submitted"])
		{
		}
		else
		{
			$link = "javascript:createWindowWithRemotingUrl2('Reject Lease Negotiation Form', 'reject_submission.php?pid=" . param("pid") . "&type=ln');";
			
			if($ln_basicdata["ln_basicdata_locked"] == 0) {
				$form->add_button("reject", "Reject LN", $link);
			}
		}
	}
}

if(($ln_basicdata["ln_basicdata_locked"] == 0 and has_access("has_access_to_his_cer")) or has_access("has_full_access_to_cer"))
{
	
	if (has_access("can_add_attachments_in_projects"))
	{
		$form->add_button("add_attachment", "Add Supporting Documents", "", IS_UPLOAD_BUTTON);
	}
	
	$form->add_button("save", "Save Data");

}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();



/********************************************************************
    List of AF/LN supporting documents
*********************************************************************/
$list1 = new ListView($sql_attachment);

if($project["project_cost_type"] == 1)
{
	$list1->set_title("LN Supporting Documents");
}
else
{
	$list1->set_title("AF Supporting Documents");
}

$list1->set_entity("order_files");
$list1->set_filter($list1_filter);
$list1->set_order("order_files.date_created DESC");

$link = "http://" . $_SERVER["HTTP_HOST"] . "/include/openfile.php?id={order_file_id}";
$list1->add_column("order_file_title", "Title", $link, "", "", COLUMN_NO_WRAP);
$list1->add_column("file_type_name", "Type", "", "", "", COLUMN_NO_WRAP);



if(param("s") == 1)
{
	$form->message("Your form was submitted successfully.");
}

if($form->button("save") or $form->button("submit"))
{
	
	//$valid = $form->validate();
	$valid = true;


	if($form->value("ln_basicdata_floorplan") and strpos(strtolower($form->value("ln_basicdata_floorplan")), 'pdf') == 0)
	{
		$form->error("The Mall Map/Street Map must be a PDF-File.");
	}
	elseif($form->value("ln_basicdata_mall_all_floors") and strpos(strtolower($form->value("ln_basicdata_mall_all_floors")), 'pdf') == 0)
	{
		$form->error("The Mall Map/Street Map of all floors must be a PDF-File.");
	}
	elseif($form->value("ln_basicdata_store_layout") and strpos(strtolower($form->value("ln_basicdata_store_layout")), 'pdf') == 0)
	{
		$form->error("The Store Layout must be a PDF-File.");
	}
	elseif($form->value("ln_basicdata_pix1") and strpos(strtolower($form->value("ln_basicdata_pix1")), 'jpg') == 0)
	{
		$form->error("Pictures mus be JPG-Files.");
	}
	elseif($form->value("ln_basicdata_pix2") and strpos(strtolower($form->value("ln_basicdata_pix2")), 'jpg') == 0)
	{
		$form->error("Pictures mus be JPG-Files.");
	}
	elseif($form->value("ln_basicdata_pix3") and strpos(strtolower($form->value("ln_basicdata_pix3")), 'jpg') == 0)
	{
		$form->error("Pictures mus be JPG-Files.");
	}
	elseif($form->value("ln_basicdata_draft_aggreement") and strpos(strtolower($form->value("ln_basicdata_draft_aggreement")), 'pdf') == 0)
	{
		$form->error("The Draft Lease Agreement must be a PDF-File.");
	}
	elseif(strlen($form->value("ln_basicdata_remarks")) > 600)
	{
		$form->error("Description of key points is to long. The maximum number of characters is 600 characters. Plese shorten the description.");
	}
	else
	{
	
		
		//update ln_basic_data
		$fields = array();

		$value = dbquote($form->value("ln_basicdata_passedrental"));
		$fields[] = "ln_basicdata_passedrental = " . $value;

		$value = dbquote($form->value("ln_basicdata_numofpos_in_area"));
		$fields[] = "ln_basicdata_numofpos_in_area = " . $value;

		$value = dbquote($form->value("ln_basicdata_numofcpos_in_area"));
		$fields[] = "ln_basicdata_numofcpos_in_area = " . $value;

		$value = dbquote($form->value("ln_basicdata_numoffpos_in_area"));
		$fields[] = "ln_basicdata_numoffpos_in_area = " . $value;

		$value = dbquote($form->value("ln_basicdata_remarks"));
		$fields[] = "ln_basicdata_remarks = " . $value;

		$value = dbquote($form->value("ln_basicdata_remarks1"));
		$fields[] = "ln_basicdata_remarks1 = " . $value;

		$value = dbquote($form->value("ln_basicdata_remarks2"));
		$fields[] = "ln_basicdata_remarks2 = " . $value;

		$value = dbquote($form->value("ln_basicdata_remarks3"));
		$fields[] = "ln_basicdata_remarks3 = " . $value;

		$value = dbquote($form->value("ln_basicdata_remarks4"));
		$fields[] = "ln_basicdata_remarks4 = " . $value;

		$value = dbquote($form->value("ln_basicdata_average_buildout_costs"));
		$fields[] = "ln_basicdata_average_buildout_costs = " . $value;

		$value = dbquote($form->value("ln_basicdata_rent"));
		$fields[] = "ln_basicdata_rent = " . $value;

		$value = dbquote($form->value("ln_basicdata_floorplan"));
		$fields[] = "ln_basicdata_floorplan = " . $value;

		$value = dbquote($form->value("ln_basicdata_mall_all_floors"));
		$fields[] = "ln_basicdata_mall_all_floors = " . $value;

		$value = dbquote($form->value("ln_basicdata_store_layout"));
		$fields[] = "ln_basicdata_store_layout = " . $value;

		$value = dbquote($form->value("ln_basicdata_pix1"));
		$fields[] = "ln_basicdata_pix1 = " . $value;

		$value = dbquote($form->value("ln_basicdata_pix2"));
		$fields[] = "ln_basicdata_pix2 = " . $value;

		$value = dbquote($form->value("ln_basicdata_pix3"));
		$fields[] = "ln_basicdata_pix3 = " . $value;

		$value = dbquote($form->value("ln_basicdata_draft_aggreement"));
		$fields[] = "ln_basicdata_draft_aggreement = " . $value;

		$value = dbquote($form->value("ln_basicdata_approvedby_cm"));
		$fields[] = "ln_basicdata_approvedby_cm = " . $value;

		$value = dbquote($form->value("ln_basicdata_approved_by_ekl"));
		$fields[] = "ln_basicdata_approved_by_ekl = " . $value;

		$value = dbquote($form->value("ln_no_ln_submission_needed"));
		$fields[] = "ln_no_ln_submission_needed = " . $value;


		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}

		$sql = "update ln_basicdata set " . join(", ", $fields) . " where ln_basicdata_version = 0 and ln_basicdata_project = " . param("pid");
		mysql_query($sql) or dberror($sql);

		$send_mail = false;
		if($form->button("submit"))
		{

			
			if(!$form->value("ln_basicdata_remarks1")
				or !$form->value("ln_basicdata_remarks2")
				or !$form->value("ln_basicdata_remarks3")
				or !$form->value("ln_basicdata_remarks4")
				or !$form->value("ln_basicdata_floorplan")
				or !$form->value("ln_basicdata_pix1")
				or !$form->value("ln_basicdata_pix2")
				or !$form->value("ln_basicdata_pix3")
				)
			{
				$form->error("Please enter all mandatory fields before submission/resubmission.");
			}
			else
			{
				
				
				//update ln_basic_data
				$fields = array();

				$value = date("Y-m-d");
				
				if($ln_basicdata["ln_basicdata_submitted"] == NULL or $ln_basicdata["ln_basicdata_submitted"] == "0000-00-00")
				{
					$fields[] = "ln_basicdata_submitted = " . dbquote($value);
					$fields[] = "ln_basicdata_submitted_by = " . dbquote(user_id());
				}
				else
				{
					$fields[] = "ln_basicdata_resubmitted = " . dbquote($value);
					$fields[] = "ln_basicdata_resubmitted_by = " . dbquote(user_id());
				}
				
				$sql = "update ln_basicdata set " . join(", ", $fields) . " where ln_basicdata_version = 0 and ln_basicdata_project = " . param("pid");
				mysql_query($sql) or dberror($sql);


				//update project milestones
				$fields = array();


				$value = date("Y-m-d");
				$fields[] = "project_milestone_date = " . dbquote($value);

				$value = user_login();
				$fields[] = "user_created = " . dbquote($value);

				$value = date("Y-m-d");
				$fields[] = "date_created = " . dbquote($value);
				
				$value = user_login();
				$fields[] = "user_modified = " . dbquote($value);

				$value = date("Y-m-d");
				$fields[] = "date_modified = " . dbquote($value);
				
				
				if($ln_basicdata["ln_basicdata_submitted"] == NULL or $ln_basicdata["ln_basicdata_submitted"] == "0000-00-00")
				{
					$sql = "update project_milestones set " . join(", ", $fields) . 
						   " where project_milestone_project = " . param("pid") . 
						   " and project_milestone_milestone = 16";
				}
				else
				{
					$sql = "select count(project_milestone_id) as num_recs from project_milestones " . 
							" where project_milestone_project = " . param("pid") . 
							" and project_milestone_milestone = 18";

					 $res = mysql_query($sql) or dberror($sql);
					 if ($row = mysql_fetch_assoc($res))
					 {
						 if($row["num_recs"] == 1) 
						 {
							 $sql = "update project_milestones set " . join(", ", $fields) . 
									" where project_milestone_project = " . param("pid") . 
									" and project_milestone_milestone = 18";
						 }
						 else
						 {
							 $sql = "insert into project_milestones (project_milestone_project, " . 
									"project_milestone_milestone, project_milestone_date, " . 
									"project_milestone_date_comment, user_created, date_created) Values (" . 
									dbquote(param("pid")) . ", " .
									"18, " .
									dbquote(date("Y-m-d")) . ", " .
									"'LN Resubmission', " .
									dbquote(user_login()) . ", " .
									dbquote(date("Y-m-d")) . ")";

						 }
					 }
					 
					
					
				}

				mysql_query($sql) or dberror($sql);

				$send_mail = true;
			}

		}
		
		
		if($send_mail == true)
		{
			
			//get recipients of project submission alerts
			$recipient_user_ids = array();

			
			if($ln_basicdata["ln_basicdata_submitted"] == NULL or $ln_basicdata["ln_basicdata_submitted"] == "0000-00-00")
			{
				$sql = 'select * from projecttype_newproject_notifications ' . 
					   'where projecttype_newproject_notification_on_lnsubmission = 1 ' . 
					   ' and projecttype_newproject_notification_country = ' . $project["order_shop_address_country"] . 
					   ' and projecttype_newproject_notification_postype = ' . $project['project_postype'];
			}
			else
			{
				$sql = 'select * from projecttype_newproject_notifications ' . 
					   'where projecttype_newproject_notification_on_lnresubmission = 1 ' . 
					   ' and projecttype_newproject_notification_country = ' . $project["order_shop_address_country"] . 
					   ' and projecttype_newproject_notification_postype = ' . $project['project_postype'];
			}

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				if($row["projecttype_newproject_notification_email"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_email"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_email"]] = $row_u["user_id"];
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_email"]] = 0;
					}
					$mailaddress = 1;
					
				}

				if($row["projecttype_newproject_notification_emailcc1"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc1"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc1"]] = $row_u["user_id"];
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc1"]] = 0;
					}
				}
				if($row["projecttype_newproject_notification_emailcc2"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc2"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc2"]] = $row_u["user_id"];
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc2"]] = 0;
					}
					
				}
				if($row["projecttype_newproject_notification_emailcc3"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc3"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc3"]] = $row_u["user_id"];
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc3"]] = 0;
					}
					
				}
				if($row["projecttype_newproject_notification_emailcc4"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc4"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc4"]] = $row_u["user_id"];
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc4"]] = 0;
					}
				
				}
				if($row["projecttype_newproject_notification_emailcc5"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc5"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc5"]] = $row_u["user_id"];
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc5"]] = 0;
					}
				
				}
				if($row["projecttype_newproject_notification_emailcc6"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc6"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc6"]] = $row_u["user_id"];
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc6"]] = 0;
					}
				
				}
				if($row["projecttype_newproject_notification_emailcc7"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc7"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc7"]] = $row_u["user_id"];
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc7"]] = 0;
					}
				}
			}

			//send mail notification

			$sql = "select order_number, order_shop_address_place, country_name, order_shop_address_company, " .
				   "project_postype, project_projectkind, project_cost_type " . 
				   "from projects " .
				   "left join orders on order_id = project_order " .
				   "left join countries on country_id = order_shop_address_country " .
				   "left join project_costs on project_cost_order = order_id " .
				   "where project_id = " .  dbquote(param("pid"));

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$user_data = get_user(user_id());
				
				if($ln_basicdata["ln_basicdata_submitted"] == NULL or $ln_basicdata["ln_basicdata_submitted"] == "0000-00-00")
				{
					$subject = "LN submission alert - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];
				}
				else
				{
					$subject = "LN resubmission alert - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_place"] . ", " . $row["order_shop_address_company"];
				}

				$subject_rmas = $subject;
			
				$sql = "select * from milestones " . 
					   "where milestone_id = 16";
						
				$reciepient_cc = "";
				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					$sender_email = $user_data["email"];
					$sender_name = $user_data["firstname"] . " " . $user_data["name"];
					$bodytext = "The lease negotiation form was submitted by " . $sender_name . ".\n";  

					$link ="lnr_booklet_pdf.php?pid=" . param("pid");
					$bodytext .= "\nClick below to download the booklet:\n";
					$bodytext .= APPLICATION_URL ."/cer/" . $link . "\n\n";           
					
					
					$bodytext .= "Thank you very much.\nKind regards, " . $sender_name;

					$mail = new Mail();
					$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
					$mail->set_sender($sender_email, $sender_name);
					
					//send mail notification to project notification reciepients
					$recipient_email = $row["milestone_email"];
					$mail->add_recipient($recipient_email);
					
					if($row["milestone_ccmail"])
					{
						$mail->add_cc($row["milestone_ccmail"]);
						$reciepient_cc .= $row["milestone_ccmail"]. "\n";
					}
					if($row["milestone_ccmail2"])
					{
						$mail->add_cc($row["milestone_ccmail2"]);
						$reciepient_cc .= $row["milestone_ccmail2"]. "\n";
					}

					//add submission recipeint alerts
					foreach($recipient_user_ids as $key=>$user_id)
					{
						if($key != $row["milestone_email"] and $key!= $row["milestone_ccmail"] and $key!= $row["milestone_ccmail2"])
						{
							$mail->add_cc($key);
							$reciepient_cc .= $key. "\n";
						}
					}

					$mail->add_text($bodytext);

					$result = $mail->send();

					$rctps = $recipient_email . "\n" . "and CC-Mail to:" . "\n" . $reciepient_cc;

					if($result == 1)
					{
						$fields = array();
						$values = array();

						$fields[] = "cer_mail_project";
						$values[] = dbquote(param("pid"));

						$fields[] = "cer_mail_group";
						$values[] = dbquote($subject);

						$fields[] = "cer_mail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "cer_mail_sender";
						$values[] = dbquote($sender_name);

						$fields[] = "cer_mail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "cer_mail_reciepient";
						$values[] = dbquote($rctps);

						$fields[] = "date_created";
						$values[] = "now()";

						$fields[] = "date_modified";
						$values[] = "now()";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into cer_mails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

						mysql_query($sql) or dberror($sql);
					}
				}
			}

			redirect("ln_general.php?pid=" . param("pid") . "&s=1");
		}

		if($form->button("save"))
		{
			$link = "ln_general.php?pid=" . param("pid");
			redirect($link);
			//$form->message("Your data has been saved.");
		}
		
	}
}


/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();
$page->title("Lease Negotiation Form: General Information");


require_once("include/tabs_ln.php");

$form->render();

echo "<p>&nbsp;</p>";
$list1->render();

?>

<div id="floorplan" style="display:none;">
    Please make sure that the mall/street Map plan contains:
	<ul>
	<li>a clear indication of future <?php echo BRAND;?> Location</li>
	<li>a clear indication of who our direct neighbours are</li>
	<li>indication of major brands an brand mix of the shopping mall</li>
	</ul>
</div>


<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#modal_file_upload').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/cer/ajx_upload_ln_supporting_document.php?pid=<?php echo param("pid");?>'
    });
    return false;
  });


  $('#calculateabc').click(function(e) {
		e.preventDefault();
		$.nyroModalManual({
		  url: 'include/calculate_average_bouildout_costs.php?lid=<?php echo $ln_basicdata["ln_basicdata_id"];?>&pid=<?php echo param("pid");?>'
		});
		return false;
	  });
  
});
</script>


<?php

require "include/footer_scripts.php";


$page->footer();

?>