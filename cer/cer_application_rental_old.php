<?php
/********************************************************************

    cer_application_rental.php

    Application Form: rental information
    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";

check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}

/********************************************************************
    prepare all data needed
*********************************************************************/
$client_address = get_address($project["order_client_address"]);
$posdata = get_pos_data($project["project_order"]);

$currency = get_cer_currency(param("pid"));
$posleases = get_pos_leasedata($posdata["posaddress_id"], $project["project_order"]);


$bp_from = "";
$bp_to = "";
$business_plan_period = "";

if($cer_basicdata["cer_basicdata_firstyear"] 
   and $cer_basicdata["cer_basicdata_lastyear"] 
   and $cer_basicdata["cer_basicdata_firstmonth"]
   and $cer_basicdata["cer_basicdata_lastmonth"])
{
	
	$bp_from = date('F', mktime(0,0,0,$cer_basicdata["cer_basicdata_firstmonth"],1)) . " " . $cer_basicdata["cer_basicdata_firstyear"];
	$bp_to = date('F', mktime(0,0,0,$cer_basicdata["cer_basicdata_lastmonth"],1))  . " " . $cer_basicdata["cer_basicdata_lastyear"];

	/*
	$date1 = $cer_basicdata["cer_basicdata_firstyear"] . "-" . $cer_basicdata["cer_basicdata_firstmonth"] . "-01";
	$date2 = $cer_basicdata["cer_basicdata_lastyear"] . "-" . $cer_basicdata["cer_basicdata_lastmonth"] . "-28";
	$diff = abs(strtotime($date2) - strtotime($date1));
	$num_years = floor($diff / (365*60*60*24));
	$num_months = floor(($diff - $num_years * 365*60*60*24) / (30*60*60*24));

	if($num_months == 12)
	{
		$num_months = 0;
		$num_years++;
	}


	$business_plan_period = $num_years . " years and " . $num_months . " months";
    */

	$duration_in_years = $cer_basicdata["cer_basicdata_lastyear"] - $cer_basicdata["cer_basicdata_firstyear"];
	$tmp1 = 13 - $cer_basicdata["cer_basicdata_firstmonth"] + $cer_basicdata["cer_basicdata_lastmonth"];
	$tmp2 = ($duration_in_years - 1)*12;
	$duration_in_years = round(($tmp1 + $tmp2) / 12, 1);

	$duration_in_years_and_months = floor(($tmp1 + $tmp2) / 12);
	$duration_in_years_and_months = $duration_in_years_and_months . " years and " . (($tmp1 + $tmp2) - ($duration_in_years_and_months*12)) . "months";

	$business_plan_period = $duration_in_years_and_months;

}

//build missing investment records
$result = build_missing_cer_investment_records(param("pid"));


$yes_no = array();
$yes_no[0] = "No";
$yes_no[1] = "Yes";


//calculate annual rent
$average_annual_rent = "";
$rental_duration = "";


if(count($posleases) > 0)
{
	if($posleases["poslease_startdate"] != NULL 
		and $posleases["poslease_startdate"] != '0000-00-00' 
		and $posleases["poslease_enddate"] != NULL
		and $posleases["poslease_enddate"] != '0000-00-00')
	{
		$diff = abs(strtotime($posleases["poslease_enddate"]) - strtotime($posleases["poslease_startdate"]));
		$years = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));

		if($months == 12)
		{
			$months = 0;
			$years++;
		}

		$rental_duration = $years . " years and " . $months . " months";

		$duration_in_years = $years + ($months/12);
	}
	
	if($duration_in_years > 0)
	{
		$total_lease_commitment = 0;
		$sql_cer = "select * from cer_expenses " .
					  "where cer_expense_project = " . param("pid") .
					  " and cer_expense_type IN(2, 3, 16, 18, 19, 20)";


		$res_cer = mysql_query($sql_cer) or dberror($sql_cer);
		while ($row_cer = mysql_fetch_assoc($res_cer))
		{
			$total_lease_commitment = $total_lease_commitment + 1*$row_cer["cer_expense_amount"];
		}

		if($duration_in_years > 0)
		{
			$average_annual_rent = number_format(round($total_lease_commitment / $duration_in_years, 0), 0, ".", "'");
		}
	}
}


//calculate annual rent in % of gross sales values
$total_rents = get_total_rents(param("pid"));
$total_sales = get_gross_sale_values(param("pid"));

foreach($total_sales as $year=>$value)
{
	$sales_percents[$year] = "";
	if($value > 0)
	{
		$sales_percents[$year] = round(100*$total_rents[$year]/$value, 2);
	}
}

/*
$sql = "select * " .
	   "from cer_rent_percent_from_sales " .
	   "where cer_rent_percent_from_sale_project = " . param("pid") .
	   " order by cer_rent_percent_from_sale_year";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	//$sales_percent_ids[$row["cer_rent_percent_from_sale_year"]] = $row["cer_rent_percent_from_sale_id"];
	$sales_percents[$row["cer_rent_percent_from_sale_year"]] = round($row["cer_rent_percent_from_sale_percent"], 2);
}

*/


// turn over based rents, update data
$result = check_expenses(param("pid"), $cer_basicdata["cer_basicdata_firstyear"], $cer_basicdata["cer_basicdata_lastyear"]);
$result = update_turnoverbased_rental_percentages(param("pid"));

$sql = "select * " .
	   "from cer_rent_percent_from_sales " .
	   "where cer_rent_percent_from_sale_project = " . param("pid") .
	   " order by cer_rent_percent_from_sale_year";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$sales_percent_ids[$row["cer_rent_percent_from_sale_year"]] = $row["cer_rent_percent_from_sale_id"];
	$sales_percents[$row["cer_rent_percent_from_sale_year"]] = $row["cer_rent_percent_from_sale_percent"];
}


$total_rents = get_total_rents(param("pid"));
$total_sales = get_gross_sale_values(param("pid"));

foreach($total_sales as $year=>$value)
{
	$total_sales_percents[$year] = "";
	if($value > 0)
	{
		$total_sales_percents[$year] = round(100*$total_rents[$year]/$value, 2);
	}
}


$sql_pos = "select posaddress_id, " . 
           " concat(product_line_name, ': ', posaddress_place, ', ', posaddress_name) as posname " . 
           "from posaddresses " .
		   "left join countries on country_id = posaddress_country " .
		   "left join product_lines on product_line_id = posaddress_store_furniture " . 
		   "where posaddress_country = " . $client_address["country"] . 
		   " and posaddress_store_postype = " . $project["project_postype"] .
		   " order by product_line_name,posname";

/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

include("include/project_head.php");

$form->add_hidden("pid", param("pid"));



if(($cer_basicdata["cer_basicdata_cer_locked"] == 0 and has_access("has_access_to_his_cer")) or has_access("has_full_access_to_cer"))
{
	$form->add_section("Benchmark Info");
	$form->add_comment("Please indicate an existing POS similar to this one in case there is any.");
	$form->add_list("posaddress_best_benchmark_pos", "Similar POS Location", $sql_pos, 0, $posdata["posaddress_best_benchmark_pos"]);
	

	$form->add_section("Business Plan Period");
	$tmp = $cer_basicdata["cer_basicdata_firstyear"] . "-" . $cer_basicdata["cer_basicdata_firstmonth"] . " / " .
	$cer_basicdata["cer_basicdata_lastyear"] . "-" . $cer_basicdata["cer_basicdata_lastmonth"] . " / " . $business_plan_period;
	$form->add_label("business_plan_period", "From / To / Duration", 0, $tmp);


	$form->add_section("Lease Details");

	if(count($posleases) > 0)
	{
				
		$form->add_comment("There is a contract in the system for this POS location as follows:");
		$form->add_list("poslease_lease_type", "Lease Type*",
				"select poslease_type_id, poslease_type_name from poslease_types order by poslease_type_id", NOTNULL, $posleases["poslease_lease_type"]);
		$form->add_edit("poslease_startdate", "Start Date*", NOTNULL, to_system_date($posleases["poslease_startdate"]), TYPE_DATE);
		$form->add_edit("poslease_enddate", "Expiry Date*", NOTNULL, to_system_date($posleases["poslease_enddate"]), TYPE_DATE);
		$form->add_label("rental_duration", "Rental Duration", 0, $rental_duration);
		$form->add_edit("poslease_extensionoption", "Extension Option to Date", 0, to_system_date($posleases["poslease_extensionoption"]), TYPE_DATE, "", "", 1, "extenstion_option_info");
		$form->add_edit("poslease_exitoption", "Exit Option to Date", 0, to_system_date($posleases["poslease_exitoption"]), TYPE_DATE, "", "", 1, "exit_option_info");
		$form->add_edit("poslease_termination_time", "Termination deadline*", NOTNULL, $posleases["poslease_termination_time"], TYPE_INT, 8, 0, 1, "termination");
		
		$form->add_edit("cer_basicdata_deadline_property", "Deadline for property", 0, to_system_date($cer_basicdata["cer_basicdata_deadline_property"]), TYPE_DATE, "", "", 1, "deadline_for_property_info");


		$form->add_edit("poslease_handoverdate", "Handover Date (Key)", 0, to_system_date($posleases["poslease_handoverdate"]), TYPE_DATE);
		$form->add_edit("poslease_firstrentpayed", "First Rent Payed on", 0, to_system_date($posleases["poslease_firstrentpayed"]), TYPE_DATE);

		$form->add_edit("poslease_freeweeks", "Rent Free Period in Weeks*", 0, $posleases["poslease_freeweeks"], TYPE_INT, 6);
		$form->add_list("poslease_hasfixrent", "Contract contains fixed rent*", $yes_no, 0, $posleases["poslease_hasfixrent"]);

		$form->add_section("Total Annual Rent in % of Sales Values");
		$form->add_comment("Please check the following option if turnover based rents are to be calculated on the base of NET SALES VALUES. Otherwise they will be calculated on the base of GROSS SALES VALUES.");
		
		$form->add_checkbox("cer_basicdata_tob_from_net_sales", "turnover based rents are to be calculated on the base of NET SALES VALUES", $cer_basicdata["cer_basicdata_tob_from_net_sales"], "", "Calculation Mode");

		$form->add_comment("Please indicate the total annual rent as a percentage from sales.");

		foreach($sales_percents as $year=>$sales_percent)
		{
			$form->add_edit("p" . $sales_percent_ids[$year], "Percent " . $year, 0, $sales_percent, TYPE_DECIMAL, 12,9);
		}

		
		
		$form->add_comment("Please check the following option if turnover based rents are to be paid in addition to fixed rents. Otherwise rents are calculated on a 'whatever is higher base'.");
		$form->add_checkbox("cer_basicdata_add_tob_rents", "turnover based rents are to be paid in addition to fixed rents", $cer_basicdata["cer_basicdata_add_tob_rents"], "", "Calculation Mode");

		$form->add_section("Breakpoint Turnover Based Rent");
		$form->add_comment("Please fill the following fields in case the rental contract contains breakpoints for turnover based rents. Indicate the percentage of the rent from turnover that has to be payed in case turnover exceeds the corresponding breakpoint.");
		
		$form->add_edit("poslease_breakpoint_amount", "Turnover Breakpoint 1", 0, $posleases["poslease_breakpoint_amount"], TYPE_INT, 12);
		$form->add_edit("poslease_breakpoint_percent", "Percentage of Turn Over", 0, $posleases["poslease_breakpoint_percent"], TYPE_DECIMAL, 5,2);

		
		$form->add_edit("poslease_breakpoint_amount2", "Turnover Breakpoint 2", 0, $posleases["poslease_breakpoint_amount2"], TYPE_INT, 12);
		$form->add_edit("poslease_breakpoint_percent2", "Percentage of Turn Over", 0, $posleases["poslease_breakpoint_percent2"], TYPE_DECIMAL, 5,2);

		
		$form->add_edit("poslease_breakpoint_amount3", "Turnover Breakpoint 3", 0, $posleases["poslease_breakpoint_amount3"], TYPE_INT, 12);
		$form->add_edit("poslease_breakpoint_percent3", "Percentage of Turn Over", 0, $posleases["poslease_breakpoint_percent3"], TYPE_DECIMAL, 5,2);
		
		$form->add_section("Additional Rental Costs");
			
		$form->add_checkbox("poslease_indexclause_in_contract", "Index Clause in Contract", $posleases["poslease_indexclause_in_contract"], "", "Index");
		$form->add_checkbox("poslease_isindexed", "contains tacit renewal clause", $posleases["poslease_isindexed"], "", "Contract");

		$form->add_edit("poslease_indexrate", "Index Rate in %", 0, $posleases["poslease_indexrate"], TYPE_DECIMAL, 6, 2);
		
		$form->add_edit("poslease_average_increase", "Average yearly Increase in %", 0, $posleases["poslease_average_increase"], TYPE_DECIMAL, 12, 2);
		$form->add_edit("poslease_realestate_fee", "Real Estate Fee in % of Keymoney", 0, $posleases["poslease_realestate_fee"], TYPE_DECIMAL, 12, 2);
		$form->add_edit("poslease_annual_charges", "Annual Charges in " . $currency["symbol"], 0, $posleases["poslease_annual_charges"], TYPE_INT, 12);
		$form->add_edit("poslease_other_fees", "Other Fees", 0, $posleases["poslease_other_fees"], TYPE_INT, 12);


		

		
		$form->add_section("Negotiation Details");
		$form->add_edit("poslease_negotiator", "Negotiator of Key Money and Lease*", NOTNULL, $posleases["poslease_negotiator"]);
		$form->add_multiline("poslease_landlord_name", "Landlord (Negotiation Partner)*", 4, NOTNULL, $posleases["poslease_landlord_name"]);
		$form->add_multiline("poslease_negotiated_conditions", "Negotiated Rental Conditions*", 4, NOTNULL, $posleases["poslease_negotiated_conditions"], 1, "condition_info");


		
		
	}
	else
	{
		$form->add_list("poslease_lease_type", "Lease Type*",
				"select poslease_type_id, poslease_type_name from poslease_types order by poslease_type_id", NOTNULL);
		$form->add_edit("poslease_startdate", "Start Date*", NOTNULL, "", TYPE_DATE);
		$form->add_edit("poslease_enddate", "Expiry Date*", NOTNULL, "", TYPE_DATE);
		$form->add_edit("poslease_extensionoption", "Extension Option to Date", 0, "", TYPE_DATE, "", "", 1, "extenstion_option_info");
		$form->add_edit("poslease_exitoption", "Exit Option to Date", 0, "", TYPE_DATE, "", "", 1, "exit_option_info");
		$form->add_edit("poslease_termination_time", "Termination deadline*", NOTNULL, "", TYPE_INT, 10, 0, 1, "termination");

		
		$form->add_edit("cer_basicdata_deadline_property", "Deadline for property", 0, "", TYPE_DATE, "", "", 1, "deadline_for_property_info");


		$form->add_edit("poslease_handoverdate", "Handover Date (Key)", 0, "", TYPE_DATE);
		$form->add_edit("poslease_firstrentpayed", "First Rent Payed on", 0, "", TYPE_DATE);
		$form->add_edit("poslease_freeweeks", "Rent Free Period in Weeks*", 0, "", TYPE_INT, 6);
		$form->add_list("poslease_hasfixrent", "Contract contains fixed rent*", $yes_no, 0, 1);

		
		$form->add_section("Total Annual Rent in % of Sales Values");

		$form->add_comment("Please check the following option if turnover based rents are to be calculated on the base of NET SALES VALUES. Otherwise they will be calculated on the base of GROSS SALES VALUES.");
		$form->add_checkbox("cer_basicdata_tob_from_net_sales", "turnover based rents are to be calculated on the base of NET SALES VALUES", $cer_basicdata["cer_basicdata_tob_from_net_sales"], "", "Calculation Mode");

		$form->add_comment("Please indicate the total annual rent as a percentage from sales.");
		foreach($sales_percents as $year=>$sales_percent)
		{
			$form->add_edit("p" . $sales_percent_ids[$year], "Percent " . $year, 0, $sales_percent, TYPE_DECIMAL, 12,9);
		}


		$form->add_comment("Please check the following option if turnover based rents are to be paid in addition to fixed rents. Otherwise rents are calculated on a 'whatever is higher base'.");
		$form->add_checkbox("cer_basicdata_add_tob_rents", "turnover based rents are to be paid in addition to fixed rents", $cer_basicdata["cer_basicdata_add_tob_rents"], "", "Calculation Mode");

		$form->add_section("Breakpoint Turnover Based Rent");
		$form->add_comment("Please fill the following fields in case the rental contract contains breakpoints for turnover based rents. Indicate the percentage of the rent from turnover that has to be payed in case turnover exceeds the corresponding breakpoint.");
		
		$form->add_edit("poslease_breakpoint_amount", "Turnover Breakpoint 1", 0, 0, TYPE_INT, 12);
		$form->add_edit("poslease_breakpoint_percent", "Percentage of Turn Over", 0, 0, TYPE_DECIMAL, 5,2);

		
		$form->add_edit("poslease_breakpoint_amount2", "Turnover Breakpoint 2", 0, 0, TYPE_INT, 12);
		$form->add_edit("poslease_breakpoint_percent2", "Percentage of Turn Over", 0, 0, TYPE_DECIMAL, 5,2);

		
		$form->add_edit("poslease_breakpoint_amount3", "Turnover Breakpoint 3", 0, 0, TYPE_INT, 12);
		$form->add_edit("poslease_breakpoint_percent3", "Percentage of Turn Over", 0, 0, TYPE_DECIMAL, 5,2);

		
		$form->add_section("Additional Rental Costs");
			
		$form->add_checkbox("poslease_indexclause_in_contract", "Index Clause in Contract", "", "", "Index");
		$form->add_checkbox("poslease_isindexed", "contains tacit renewal clause", "", "", "Contract");

		$form->add_edit("poslease_indexrate", "Index Rate in %", 0, "", TYPE_DECIMAL, 6, 2);

		$form->add_edit("poslease_average_increase", "Average yearly Increase in %", 0, "", TYPE_DECIMAL, 12, 2);
		$form->add_edit("poslease_realestate_fee", "Real Estate Fee in % of Keymoney", 0, "", TYPE_DECIMAL, 12, 2);
		$form->add_edit("poslease_annual_charges", "Annual Charges in " . $currency["symbol"], 0, "", TYPE_INT, 6, 2);
		$form->add_edit("poslease_other_fees", "Other Fees", 0, "");

		
		$form->add_section("Negotiation Details");
		$form->add_edit("poslease_negotiator", "Negotiator of Key Money and Lease*", NOTNULL, "");
		$form->add_multiline("poslease_landlord_name", "Landlord (Negotiation Partner)*", 4, NOTNULL, "");
		$form->add_multiline("poslease_negotiated_conditions", "Negotiated Rental Conditions*", 4, NOTNULL, "", 1, "condition_info");

		

	}

	$form->add_button("form_save", "Save Data");
}
else
{
	

	if(count($posleases) > 0)
	{
		
		$rental_duration = "";
		if($posleases["poslease_startdate"] != NULL 
			and $posleases["poslease_startdate"] != '0000-00-00' 
			and $posleases["poslease_enddate"] != NULL
			and $posleases["poslease_enddate"] != '0000-00-00')
		{
			$diff = abs(strtotime($posleases["poslease_enddate"]) - strtotime($posleases["poslease_startdate"]));
			$years = floor($diff / (365*60*60*24));
			$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));

			if($months == 12)
			{
				$months = 0;
				$years++;
			}

			$rental_duration = $years . " years and " . $months . " months";


		}
		
		$form->add_section("Benchmark Info");

		$pos_name = "concat(posaddress_name, ', ', posaddress_place)";
		$form->add_lookup("posaddress_best_benchmark_pos", "Similar POS Location", "posaddresses", $pos_name, 0, dbquote($posdata["posaddress_best_benchmark_pos"]));

		$form->add_section("Business Plan Period");
		$tmp = $cer_basicdata["cer_basicdata_firstyear"] . "-" . $cer_basicdata["cer_basicdata_firstmonth"] . " / " .
		$cer_basicdata["cer_basicdata_lastyear"] . "-" . $cer_basicdata["cer_basicdata_lastmonth"] . " / " . $business_plan_period;
		$form->add_label("business_plan_period", "From / To / Duration", 0, $tmp);


		
		$form->add_section("Lease Details");

		$form->add_comment("There is a contract in the system for this POS location as follows:");
		$form->add_lookup("poslease_lease_type", "Lease Type", "poslease_types", "poslease_type_name", 0, $posleases["poslease_lease_type"]);
		
		$form->add_label("poslease_startdate", "Start Date", 0, to_system_date($posleases["poslease_startdate"]));
		$form->add_label("poslease_enddate", "Expiry Date", 0, to_system_date($posleases["poslease_enddate"]));
		$form->add_label("rental_duration", "Rental Duration", 0, $rental_duration);
		$form->add_label("poslease_extensionoption", "Extension Option to Date", 0, to_system_date($posleases["poslease_extensionoption"]));
		$form->add_label("poslease_exitoption", "Exit Option to Date", 0, to_system_date($posleases["poslease_exitoption"]));
		$form->add_label("poslease_termination_time", "Terminationdeadline", 0, $posleases["poslease_termination_time"]);

		
		$form->add_label("cer_basicdata_deadline_property", "Deadline for property", 0, to_system_date($cer_basicdata["cer_basicdata_deadline_property"]), TYPE_DATE);

		$form->add_label("poslease_handoverdate", "Handover Date (Key)", 0, to_system_date($posleases["poslease_handoverdate"]));
		$form->add_label("poslease_firstrentpayed", "First Rent Payed on", 0, to_system_date($posleases["poslease_firstrentpayed"]));

		$form->add_label("poslease_freeweeks", "Rent Free Period in Weeks", 0, to_system_date($posleases["poslease_freeweeks"]));
		$form->add_label("poslease_hasfixrent", "Contract contains fixed rent", $yes_no[$posleases["poslease_hasfixrent"]]);


		/*
		$form->add_section("Rental Costs");
		$form->add_label("annual_rent", "Average Annual Rent in " . $currency["symbol"], 0, $average_annual_rent);

		foreach($sales_percents as $year=>$sales_percent)
		{
			$form->add_label("salespercent_" . $year, "Annual Rent in % of Gross Sales " . $year, 0, $sales_percent);
		}
		*/


		$form->add_comment("Please check the following option if turnover based rents are to be paid in addition to fixed rents. Otherwise rents are calculated on a 'whatever is higher base'.");
		$form->add_checkbox("cer_basicdata_add_tob_rents", "turnover based rents are to be paid in addition to fixed rents", $cer_basicdata["cer_basicdata_add_tob_rents"], "", "Calculation Mode");


		$form->add_section("Total Annual Rent in % of Sales Values");

		$form->add_comment("Please check the following option if turnover based rents are to be calculated on the base of NET SALES VALUES. Otherwise they will be calculated on the base of GROSS SALES VALUES.");
		$form->add_checkbox("cer_basicdata_tob_from_net_sales", "turnover based rents are to be calculated on the base of NET SALES VALUES", $cer_basicdata["cer_basicdata_tob_from_net_sales"], "", "Calculation Mode");

		$form->add_comment("Please indicate the total annual rent as a percentage from sales.");
		foreach($sales_percents as $year=>$sales_percent)
		{
			$form->add_label("p" . $sales_percent_ids[$year], "Percent " . $year, 0, $sales_percent);
		}


		$form->add_comment("Please check the following option if turnover based rents are to be paid in addition to fixed rents. Otherwise rents are calculated on a 'whatever is higher base'.");
		$form->add_checkbox("cer_basicdata_add_tob_rents", "turnover based rents are to be paid in addition to fixed rents", $cer_basicdata["cer_basicdata_add_tob_rents"], "", "Calculation Mode");


		$form->add_section("Breakpoint Turnover Based Rent");
		$form->add_comment("Please fill the following fields in case the rental contract contains breakpoints for turnover based rents. Indicate the percentage of the rent from turnover that has to be payed in case turnover exceeds the corresponding breakpoint.");
		
		$form->add_label("poslease_breakpoint_amount", "Turnover Breakpoint 1", 0, $posleases["poslease_breakpoint_amount"]);
		$form->add_label("poslease_breakpoint_percent", "Percentage of Turn Over", 0, $posleases["poslease_breakpoint_percent"]);

		
		$form->add_label("poslease_breakpoint_amount2", "Turnover Breakpoint 2", 0, $posleases["poslease_breakpoint_amount2"]);
		$form->add_label("poslease_breakpoint_percent2", "Percentage of Turn Over", 0, $posleases["poslease_breakpoint_percent2"]);

		
		$form->add_label("poslease_breakpoint_amount3", "Turnover Breakpoint 3", 0, $posleases["poslease_breakpoint_amount3"]);
		$form->add_label("poslease_breakpoint_percent3", "Percentage of Turn Over", 0, $posleases["poslease_breakpoint_percent3"]);
		
		$form->add_section("Additional Rental Costs");
			
		$form->add_checkbox("poslease_indexclause_in_contract", "Index Clause in Contract", $posleases["poslease_indexclause_in_contract"], "", "Index");
		$form->add_checkbox("poslease_isindexed", "contains tacit renewal clause", $posleases["poslease_isindexed"], "", "Contract");

		$form->add_label("poslease_indexrate", "Index Rate in %", 0, $posleases["poslease_indexrate"]);

		$form->add_label("poslease_average_increase", "Average yearly Increase in %", 0, $posleases["poslease_average_increase"]);
		$form->add_label("poslease_realestate_fee", "Real Estate Fee in % of Keymoney", 0, $posleases["poslease_realestate_fee"]);
		$form->add_label("poslease_annual_charges", "Annual Charges in " . $currency["symbol"], 0, $posleases["poslease_annual_charges"]);
		$form->add_label("poslease_other_fees", "Other Fees", 0, $posleases["poslease_other_fees"]);
		
		$form->add_section("Negotiation Details");
		$form->add_label("poslease_negotiator", "Negotiator of Key Money and Lease", 0, $posleases["poslease_negotiator"]);
		$form->add_label("poslease_landlord_name", "Landlord (Negotiation Partner)", 0, $posleases["poslease_landlord_name"]);
		$form->add_label("poslease_negotiated_conditions", "Negotiated Rental Conditions", 0, $posleases["poslease_negotiated_conditions"], 1, "condition_info");
		
	}
	else
	{
		$form->add_section("Benchmark Info");

		$pos_name = "concat(posaddress_name, ', ', posaddress_place)";
		$form->add_lookup("posaddress_best_benchmark_pos", "Similar POS Location", "posaddresses", $pos_name, 0, dbquote($posdata["posaddress_best_benchmark_pos"]));

		$form->add_section("Business Plan Period");
		$tmp = $cer_basicdata["cer_basicdata_firstyear"] . "-" . $cer_basicdata["cer_basicdata_firstmonth"] . " / " .
		$cer_basicdata["cer_basicdata_lastyear"] . "-" . $cer_basicdata["cer_basicdata_lastmonth"] . " / " . $business_plan_period;
		$form->add_label("business_plan_period", "From / To / Duration", 0, $tmp);


		$form->add_section("Lease Details");
		
		$form->add_label("poslease_lease_type", "Lease Type");
		$form->add_label("poslease_startdate", "Start Date");
		$form->add_label("poslease_enddate", "Expiry Date");
		$form->add_label("poslease_extensionoption", "Extension Option to Date");
		$form->add_label("poslease_exitoption", "Exit Option to Date");
		$form->add_label("poslease_termination_time", "Termination deadline");

		$form->add_label("cer_basicdata_deadline_property", "Deadline for property");

		$form->add_label("poslease_handoverdate", "Handover Date (Key)", 0, to_system_date($posleases["poslease_handoverdate"]));
		$form->add_label("poslease_firstrentpayed", "First Rent Payed on", 0, to_system_date($posleases["poslease_firstrentpayed"]));

		$form->add_label("poslease_freeweeks", "Rent Free Period in Weeks", 0, to_system_date($posleases["poslease_freeweeks"]));
		$form->add_label("poslease_hasfixrent", "Contract contains fixed rent", $yes_no[$posleases["poslease_hasfixrent"]]);


		

		/*
		$form->add_section("Rental Costs");
		$form->add_label("annual_rent", "Average Annual Rent in " . $currency["symbol"], 0, $average_annual_rent);

		foreach($sales_percents as $year=>$sales_percent)
		{
			$form->add_label("salespercent_" . $year, "Annual Rent in % of Gross Sales " . $year, 0, $sales_percent);
		}
		*/


		$form->add_comment("Please check the following option if turnover based rents are to be paid in addition to fixed rents. Otherwise rents are calculated on a 'whatever is higher base'.");
		$form->add_checkbox("cer_basicdata_add_tob_rents", "turnover based rents are to be paid in addition to fixed rents", $cer_basicdata["cer_basicdata_add_tob_rents"], "", "Calculation Mode");


		$form->add_section("Total Annual Rent in % of Sales Values");

		$form->add_comment("Please check the following option if turnover based rents are to be calculated on the base of NET SALES VALUES. Otherwise they will be calculated on the base of GROSS SALES VALUES.");
		$form->add_checkbox("cer_basicdata_tob_from_net_sales", "turnover based rents are to be calculated on the base of NET SALES VALUES", $cer_basicdata["cer_basicdata_tob_from_net_sales"], "", "Calculation Mode");

		$form->add_comment("Please indicate the total annual rent as a percentage from sales.");
		foreach($sales_percents as $year=>$sales_percent)
		{
			$form->add_edit("p" . $sales_percent_ids[$year], "Percent " . $year, 0, $sales_percent, TYPE_DECIMAL, 12,9);
		}


		$form->add_comment("Please check the following option if turnover based rents are to be paid in addition to fixed rents. Otherwise rents are calculated on a 'whatever is higher base'.");
		$form->add_checkbox("cer_basicdata_add_tob_rents", "turnover based rents are to be paid in addition to fixed rents", $cer_basicdata["cer_basicdata_add_tob_rents"], "", "Calculation Mode");

		$form->add_section("Breakpoint Turnover Based Rent");
		$form->add_comment("Please fill the following fields in case the rental contract contains breakpoints for turnover based rents. Indicate the percentage of the rent from turnover that has to be payed in case turnover exceeds the corresponding breakpoint.");
		
		$form->add_label("poslease_breakpoint_amount", "Turnover Breakpoint 1", 0, 0);
		$form->add_label("poslease_breakpoint_percent", "Percentage of Turn Over", 0,0);

		
		$form->add_label("poslease_breakpoint_amount2", "Turnover Breakpoint 2", 0, 0);
		$form->add_label("poslease_breakpoint_percent2", "Percentage of Turn Over", 0,0);

		
		$form->add_label("poslease_breakpoint_amount3", "Turnover Breakpoint 3", 0, 0);
		$form->add_label("poslease_breakpoint_percent3", "Percentage of Turn Over", 0,0);

		
		$form->add_section("Additional Rental Costs");
			
		$form->add_checkbox("poslease_indexclause_in_contract", "Index Clause in Contract", "", "", "Index");
		$form->add_checkbox("poslease_isindexed", "contains tacit renewal clause","", "", "Contract");
		$form->add_label("poslease_indexrate", "Index Rate in %", 0, "");

		$form->add_label("poslease_average_increase", "Average yearly Increase in %", 0);
		$form->add_label("poslease_realestate_fee", "Real Estate Fee in % of Keymoney", 0);
		$form->add_label("poslease_annual_charges", "Annual Charges in " . $currency["symbol"], 0);
		$form->add_label("poslease_other_fees", "Other Fees", 0);


		$form->add_section("Negotiation Details");
		$form->add_label("poslease_negotiator", "Negotiator of Key Money and Lease");
		$form->add_label("poslease_landlord_name", "Landlord (Negotiation Partner)");
		$form->add_label("poslease_negotiated_conditions", "Negotiated Rental Conditions");

	}
}




/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if($form->button("form_save"))
{
	if($form->value("poslease_hasfixrent") == '')
	{
		$form->error("Please indicate if the contract includes fixed rents.");
	}
	elseif($form->validate())
	{

		// save pos data
		$fields = array();
    
		$value = dbquote($form->value("posaddress_best_benchmark_pos"));
		$fields[] = "posaddress_best_benchmark_pos = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update " . $posdata["table"] . " set " . join(", ", $fields) . " where posaddress_id = " . $posdata["posaddress_id"];
		mysql_query($sql) or dberror($sql);

		//save data to posleases
		
		if(count($posleases) > 0) // update existing record
		{
			
			$fields = array();
		
			$value = dbquote($form->value("poslease_lease_type"));
			$fields[] = "poslease_lease_type = " . $value;

			//$value = dbquote($form->value("poslease_anual_rent"));
			//$fields[] = "poslease_anual_rent = " . $value;

			//$value = dbquote($form->value("poslease_salespercent"));
			//$fields[] = "poslease_salespercent = " . $value;

			$value = dbquote($form->value("poslease_indexclause_in_contract"));
			$fields[] = "poslease_indexclause_in_contract = " . $value;

			$value = dbquote($form->value("poslease_isindexed"));
			$fields[] = "poslease_isindexed = " . $value;

			$value = dbquote($form->value("poslease_indexrate"));
			$fields[] = "poslease_indexrate = " . $value;

			$value = dbquote($form->value("poslease_average_increase"));
			$fields[] = "poslease_average_increase = " . $value;

			$value = dbquote($form->value("poslease_realestate_fee"));
			$fields[] = "poslease_realestate_fee = " . $value;

			$value = dbquote($form->value("poslease_annual_charges"));
			$fields[] = "poslease_annual_charges = " . $value;

			$value = dbquote($form->value("poslease_other_fees"));
			$fields[] = "poslease_other_fees = " . $value;
			
			$value = dbquote(from_system_date($form->value("poslease_startdate")));
			$fields[] = "poslease_startdate = " . $value;

			$value = dbquote(from_system_date($form->value("poslease_enddate")));
			$fields[] = "poslease_enddate = " . $value;

			$value = dbquote(from_system_date($form->value("poslease_extensionoption")));
			$fields[] = "poslease_extensionoption = " . $value;

			$value = dbquote(from_system_date($form->value("poslease_exitoption")));
			$fields[] = "poslease_exitoption = " . $value;


			$value = dbquote(from_system_date($form->value("poslease_handoverdate")));
			$fields[] = "poslease_handoverdate = " . $value;

			$value = dbquote(from_system_date($form->value("poslease_firstrentpayed")));
			$fields[] = "poslease_firstrentpayed = " . $value;

			$value = dbquote($form->value("poslease_freeweeks"));
			$fields[] = "poslease_freeweeks = " . $value;

			$value = dbquote($form->value("poslease_hasfixrent"));
			$fields[] = "poslease_hasfixrent = " . $value;

			$value = dbquote($form->value("poslease_breakpoint_percent"));
			$fields[] = "poslease_breakpoint_percent = " . $value;

			$value = dbquote($form->value("poslease_breakpoint_amount"));
			$fields[] = "poslease_breakpoint_amount = " . $value;

			$value = dbquote($form->value("poslease_breakpoint_percent2"));
			$fields[] = "poslease_breakpoint_percent2 = " . $value;

			$value = dbquote($form->value("poslease_breakpoint_amount2"));
			$fields[] = "poslease_breakpoint_amount2 = " . $value;


			$value = dbquote($form->value("poslease_breakpoint_percent3"));
			$fields[] = "poslease_breakpoint_percent3 = " . $value;

			$value = dbquote($form->value("poslease_breakpoint_amount3"));
			$fields[] = "poslease_breakpoint_amount3 = " . $value;

			$value = dbquote($form->value("poslease_negotiator"));
			$fields[] = "poslease_negotiator = " . $value;

			$value = dbquote($form->value("poslease_landlord_name"));
			$fields[] = "poslease_landlord_name = " . $value;

			$value = dbquote($form->value("poslease_negotiated_conditions"));
			$fields[] = "poslease_negotiated_conditions = " . $value;

			$value = dbquote($form->value("poslease_termination_time"));
			$fields[] = "poslease_termination_time = " . $value;

			
			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update " . $posleases["table"] . " set " . join(", ", $fields) . " where poslease_id = " . $posleases["poslease_id"];
			mysql_query($sql) or dberror($sql);

		}
		else //insert new record
		{

			$fields = array();
			$values = array();

			$fields[] = "poslease_posaddress";
			$values[] = dbquote($posdata["posaddress_id"]);

			$fields[] = "poslease_order";
			$values[] = dbquote($project["project_order"]);

			$fields[] = "poslease_lease_type";
			$values[] = dbquote($form->value("poslease_lease_type"));
			
			//$fields[] = "poslease_anual_rent";
			//$values[] = dbquote($form->value("poslease_anual_rent"));

			//$fields[] = "poslease_salespercent";
			//$values[] = dbquote($form->value("poslease_salespercent"));

			$fields[] = "poslease_indexclause_in_contract";
			$values[] = dbquote($form->value("poslease_indexclause_in_contract"));

			$fields[] = "poslease_isindexed";
			$values[] = dbquote($form->value("poslease_isindexed"));

			$fields[] = "poslease_indexrate";
			$values[] = dbquote($form->value("poslease_indexrate"));

			$fields[] = "poslease_average_increase";
			$values[] = dbquote($form->value("poslease_average_increase"));

			$fields[] = "poslease_realestate_fee";
			$values[] = dbquote($form->value("poslease_realestate_fee"));

			$fields[] = "poslease_annual_charges";
			$values[] = dbquote($form->value("poslease_annual_charges"));

			$fields[] = "poslease_other_fees";
			$values[] = dbquote($form->value("poslease_other_fees"));

						
			$fields[] = "poslease_startdate";
			$values[] = dbquote(from_system_date($form->value("poslease_startdate")));

			$fields[] = "poslease_enddate";
			$values[] = dbquote(from_system_date($form->value("poslease_enddate")));

			$fields[] = "poslease_extensionoption";
			$values[] = dbquote(from_system_date($form->value("poslease_extensionoption")));

			$fields[] = "poslease_exitoption";
			$values[] = dbquote(from_system_date($form->value("poslease_exitoption")));

			$fields[] = "poslease_handoverdate";
			$values[] = dbquote(from_system_date($form->value("poslease_handoverdate")));

			$fields[] = "poslease_firstrentpayed";
			$values[] = dbquote(from_system_date($form->value("poslease_firstrentpayed")));

			$fields[] = "poslease_freeweeks";
			$values[] = dbquote($form->value("poslease_freeweeks"));

			$fields[] = "poslease_hasfixrent";
			$values[] = dbquote($form->value("poslease_hasfixrent"));

			$fields[] = "poslease_breakpoint_percent";
			$values[] = dbquote($form->value("poslease_breakpoint_percent"));

			$fields[] = "poslease_breakpoint_amount";
			$values[] = dbquote($form->value("poslease_breakpoint_amount"));

			$fields[] = "poslease_breakpoint_percent2";
			$values[] = dbquote($form->value("poslease_breakpoint_percent2"));

			$fields[] = "poslease_breakpoint_amount2";
			$values[] = dbquote($form->value("poslease_breakpoint_amount2"));

			$fields[] = "poslease_breakpoint_percent3";
			$values[] = dbquote($form->value("poslease_breakpoint_percent3"));

			$fields[] = "poslease_breakpoint_amount3";
			$values[] = dbquote($form->value("poslease_breakpoint_amount3"));
			
			$fields[] = "poslease_negotiator";
			$values[] = dbquote($form->value("poslease_negotiator"));

			$fields[] = "poslease_landlord_name";
			$values[] = dbquote($form->value("poslease_landlord_name"));

			$fields[] = "poslease_negotiated_conditions";
			$values[] = dbquote($form->value("poslease_negotiated_conditions"));

			$fields[] = "poslease_termination_time";
			$values[] = dbquote($form->value("poslease_termination_time"));

			$fields[] = "date_created";
			$values[] = "now()";

			$fields[] = "date_modified";
			$values[] = "now()";

			$fields[] = "user_created";
			$values[] = dbquote(user_login());

			$fields[] = "user_modified";
			$values[] = dbquote(user_login());

			if($posdata["table_leases"] == "posleasepipeline")
			{
				$sql = "insert into posleasespipeline (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			}
			else
			{
				$sql = "insert into posleases (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			}
			
			mysql_query($sql) or dberror($sql);
		
		}

		//update cer_basic_data
		$fields = array();
    
		$value = dbquote(from_system_date($form->value("cer_basicdata_deadline_property")));
		$fields[] = "cer_basicdata_deadline_property = " . $value;

		$value = dbquote($form->value("cer_basicdata_add_tob_rents"));
		$fields[] = "cer_basicdata_add_tob_rents = " . $value;

		$value = dbquote($form->value("cer_basicdata_tob_from_net_sales"));
		$fields[] = "cer_basicdata_tob_from_net_sales = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_basicdata set " . join(", ", $fields) . " where cer_basicdata_project = " . param("pid");
		mysql_query($sql) or dberror($sql);



		//update turnoverbased rental cost
		foreach($sales_percent_ids as $year=>$id)
		{
			$fields = array();
	
			$value = dbquote($form->value("p" . $sales_percent_ids[$year]));
			$fields[] = "cer_rent_percent_from_sale_percent = " . $value;

			$value = "current_timestamp";
			$fields[] = "date_modified = " . $value;

			if (isset($_SESSION["user_login"]))
			{
				$value = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value);
			}
	   
			$sql = "update cer_rent_percent_from_sales set " . join(", ", $fields) . " where cer_rent_percent_from_sale_id = " . $id;
			mysql_query($sql) or dberror($sql);

	
		}

		$result = update_turnoverbased_rental_cost(param("pid"), $form->value("cer_basicdata_add_tob_rents"), $cer_basicdata["cer_basicdata_tob_from_net_sales"]);

		
		$form->message("Your data has bee saved.");
	}
}
  
/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");
require "include/project_page_actions.php";

$page->header();
if($form_type == "AF")
{
	$page->title("Application Form: Rental details/Key Money");
}
else
{
	$page->title("Capital Expenditure Request: Rental details/Key Money");
}
require_once("include/tabs.php");
$form->render();
?>

<div id="condition_info" style="display:none;">
    Please indicate the negotiated rental condidtions. Negotations might have been about:
	<ul>
	<li>Participation of landlord</li>
	<li>Guarantess made to landlord</li>
	<li>Conditions of landlord</li>
	<li>Fixed rent per measurement unit (m2/sqft) per month or year</li>
	<li>Special other conditions</li>
	</ul>
</div> 

<div id="termination" style="display:none;">
    Please provide the number of months prior to termination date that notice must be given to landlord of tenant's intention to extend or terminate.
</div> 

<div id="extenstion_option_info" style="display:none;">
    This date shall indicate until when a contract can be extended after its expiry.
</div> 

<div id="exit_option_info" style="display:none;">
    This date shall indicate the date you can terminate the contract before its expiry.
</div> 

<div id="deadline_for_property_info" style="display:none;">
    This date shall indicate until when you have to give final answer to your landlord.
</div> 

<?php

require "include/footer_scripts.php";
$page->footer();

?>