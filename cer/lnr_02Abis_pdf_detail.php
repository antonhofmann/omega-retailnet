<?php
/********************************************************************

    lnr_02Abis_pdf_detail.php

    Print Detail Form LNR02Abis.

    Created by:     Anton Hofmann (anton.hofmann@codingroom.ch)
    Date created:   2018-01-09
    Modified by:    Anton Hofmann (anton.hofmann@codingroom.ch)
    Date modified:  2018-01-09
    Version:        1.0.0

    Copyright (c) 2018, OMEGA, All Rights Reserved.
*********************************************************************/
include("include/in_financial_data.php");


//set pdf parameters
$margin_top = 12;
$margin_left = 10;
$y = $margin_top;
$x = $margin_left+1;
$standard_h = 6;
$x1 = $margin_left+1;
$x2 = $margin_left+61;

$years_page1 = array();
$j= 1;
foreach($years as $key=>$year)
{
	if($j < 7)
	{
		$years_page1[] = $years[$key];
	}
	$j++;
}

$pdf->AddPage("P");

// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "B", 10);
$pdf->Cell(40, 8, "Swatch Group", 1);

$pdf->SetFont("arialn", "B", 13);

$pdf->Cell(110, 8, "LEASE RENEWAL REQUEST - BUSINESS PLAN", 1, "", "C");


$pdf->SetFont("arialn", "", 10);
$pdf->SetFillColor(248,251,167);
if($ln_version == 0 and $ln_basicdata["ln_basicdata_locked"] == 0)
{
	$pdf->Cell(20, 8, date("d.m.Y"), 1, "", "C", true);
}
else
{
	$pdf->Cell(20, 8, to_system_date($ln_basicdata["versiondate"]), 1, "", "C", true);
}
$pdf->Cell(20, 8, "LNR-02Abis", 1, "", "C");

	
	$y = $pdf->GetY()+10;
	$y_at_start = $y - 1;

	
	
	// 2. Investment
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(188, 6, "2. Investment (" . $project["order_shop_address_company"] . ", " . $project['order_number']. ")", 1, "", "L");

	$y = $pdf->GetY()+6;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(60, 5, "Investment in Fixed Assets - Cost", 1, "", "L");
	$pdf->Cell(15, 5, $currency_symbol, 1, "", "R");
	$pdf->Cell(15, 5, "CHF", 1, "", "R");
	$pdf->Cell(98, 5, "City information", 1, "", "L");
	
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+91,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(78, 5, "Number of POS (for the same Brand) in the same city area", 1, "", "L");
	$pdf->Cell(20, 5, $ln_basicdata["ln_basicdata_numofpos_in_area"], 1, "", "L", true);
	
	
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+91,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(78, 5, "", 1, "", "L");
	$pdf->SetXY($margin_left+91,$y);
	$pdf->Cell(20, 5, "of which :", 0, "", "L");
	$pdf->Cell(58, 5, "- Swatch Group Corporate Stores", 0, "", "L");
	$pdf->Cell(20, 5, $ln_basicdata["ln_basicdata_numofcpos_in_area"], 1, "", "L", true);
	
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+91,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(78, 5, "", 1, "", "L");
	$pdf->SetXY($margin_left+91,$y);
	$pdf->Cell(20, 5, "", 0, "", "L");
	$pdf->Cell(58, 5, "- Swatch Group Franchise Stores", 0, "", "L");
	$pdf->Cell(20, 5, $ln_basicdata["ln_basicdata_numoffpos_in_area"], 1, "", "L", true);
	
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+91,$y);
	$pdf->Cell(98, 5, "HR information", 1, "", "L");
	
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+91,$y);
	$pdf->Cell(20, 5, "Headcount:", 0, "", "L");
	$pdf->Cell(20, 5, $head_counts, 1, "", "L");
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+91,$y);
	$pdf->Cell(20, 5, "FTE:", 0, "", "L");
	$pdf->Cell(20, 5, $ftes, 1, "", "L");

	
	$y = $pdf->GetY()-25;
	$pdf->SetXY($margin_left+1,$y);
	

	//draw investment boxes
	$pdf->Cell(60, 25, "", 1);
	$pdf->SetXY($x2,$y);
	$pdf->Cell(15, 25, "", 1);
	
	
	//box city information
	
	$pdf->SetXY($margin_left+91,$y);
	$pdf->Cell(98, 30, "", 1);


	//list investments
	$pdf->SetFont("arialn", "", 9);
		
	$y = $y - 5;
	
	foreach($fixed_assets as $key=>$itype)
	{
		if(array_key_exists($itype, $amounts))
		{
			if(!$amounts[$itype]){
				$amounts[$itype] = "";
			}
			
			
			$y = $y + 5;
			$pdf->SetXY($x1,$y);
			$pdf->SetFont("arialn", "", 9);

			$pdf->Cell($x, 5, $investment_names[$itype] , 0, "", "L");

			$pdf->SetXY($x2,$y);
			$pdf->SetFont("arialn", "", 9);
			
			if($amounts[$itype] != "-" and $amounts[$itype] != 0)
			{
				$pdf->Cell(15, 5, number_format(round($amounts[$itype]/ 1000,0), 0, '.', "'"), 0, "", "R");
			}
			else
			{
				$pdf->Cell(15, 5, "-", 0, "", "R");
			}


			if($amounts[$itype] != "-" and $amounts[$itype] != 0)
			{	
				$tmp = $ln_basicdata["ln_basicdata_exchangerate"]*$amounts[$itype]/$ln_basicdata["ln_basicdata_factor"];
				$pdf->Cell(15, 5, number_format(round($tmp/ 1000,0), 0, '.', "'"), 0, "", "R");
			}
			else
			{
				$pdf->Cell(15, 5, "-", 0, "", "R");
			}

			
		}
		else
		{
			$y = $y + 5;
		}
	}
	
	$y = $pdf->GetY()+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(60, 5, "Total Investment", 1, "", "L");

	$pdf->Cell(15, 5, number_format(round($investment_total/1000, 0), 0, '.', "'"), 1, "", "R");
	
	$tmp = $ln_basicdata["ln_basicdata_exchangerate"]*$investment_total/$ln_basicdata["ln_basicdata_factor"];
	$pdf->Cell(15, 5, number_format(round($tmp/ 1000,0), 0, '.', "'"), 1, "", "R");

	$y = $pdf->GetY()+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(60, 5, "Total Investment per sqm (total surface)", 1, "", "L");
	

	$tmp = $investment_total/$gross_surface;
	$pdf->Cell(15, 5, number_format(round($tmp/1000, 0), 0, '.', "'"), 1, "", "R");
	
	$tmp = $ln_basicdata["ln_basicdata_exchangerate"]*$investment_total/$ln_basicdata["ln_basicdata_factor"];
	$tmp = $tmp/$gross_surface;
	$pdf->Cell(15, 5, number_format(round($tmp/1000, 0), 0, '.', "'"), 1, "", "R");
	
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(78, 5, "Exch. rate (" . $currency_symbol . "/CHF)", 1, "", "L");
	$pdf->Cell(20, 5, $ln_basicdata["ln_basicdata_exchangerate"], 1, "", "L");



	//future investments
	if($cer_basicdata['cer_basicdata_future_investment'] > 0) {
		
		$y = $pdf->GetY()+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 9);
		$pdf->Cell(60, 5, "Planned Future Investment for Renovation", 1, "", "L");
		

		$tmp = $cer_basicdata['cer_basicdata_future_investment'];
		$pdf->Cell(15, 5, number_format(round($tmp/1000, 0), 0, '.', "'"), 1, "", "R");

		$tmp = $ln_basicdata["ln_basicdata_exchangerate"]*$cer_basicdata['cer_basicdata_future_investment']/$ln_basicdata["ln_basicdata_factor"];
		$pdf->Cell(15, 5, number_format(round($tmp/ 1000,0), 0, '.', "'"), 1, "", "R");


		if($cer_basicdata['cer_basicdata_future_depr_end_year'] > 0 
			and $cer_basicdata['cer_basicdata_future_depr_end_month'] > 0) {

			$tmp = $cer_basicdata['cer_basicdata_future_depr_start_month'] . "/" . $cer_basicdata['cer_basicdata_future_depr_start_year'];
			$tmp = "Depreciation considered strating from "  . $tmp;

			$tmp .= " and ending in "  . $cer_basicdata['cer_basicdata_future_depr_end_month'] . "/" . $cer_basicdata['cer_basicdata_future_depr_end_year'];

		}
		else {
			$tmp = $cer_basicdata['cer_basicdata_future_depr_start_month'] . "/" . $cer_basicdata['cer_basicdata_future_depr_start_year'];
			$tmp = "Depreciation considered strating from "  . $tmp;
		}
		$pdf->Cell(98, 5, $tmp, 1, "", "L");
	}



	$y = $pdf->GetY()+6;
	$pdf->SetXY($margin_left+1,$y);


	// 3. Financial Figures
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(188, 6, "3. Financial figures " . $currency_symbol, 1, "", "L");
	

	$pdf->SetFont("arialn", "B", 9);
	$y = $pdf->GetY()+6;
	$y_tmp = $y;
	$pdf->SetXY($margin_left+1,$y);	
	$pdf->Cell(70, 5, "" , 1, "", "L");

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(15, 5, $year , 1, "", "R");
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(15, 5, "" , 1, "", "R");
	}

	$x_for_sellout_table = $pdf->GetX();
	$y_for_sellout_table = $pdf->GetY();

	$pdf->SetFont("arialn", "", 9);
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);	
	//draw boxes
	$pdf->Cell(70, 8, "" , 1, "", "L");
	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(15, 8, "" , 1, "", "R", true);
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(15, 8, "" , 1, "", "R", true);
	}



	$pdf->SetXY($margin_left+1,$y);	
	$pdf->Cell(70,4, "Watches units", 0, "L");

	

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(15, 4, $sales_units_watches_values[$year] , 0, "", "R");
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(15, 4, "" , 0, "", "R");
	}


	

	$y = $pdf->GetY()+4;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->Cell(70,4, "Bijoux units", 0, "L");

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(15, 4, $sales_units_jewellery_values[$year] , 0, "", "R");
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(15, 4, "" , 0, "", "R");
	}

	
	$pdf->SetFont("arialn", "B", 9);
	$y = $pdf->GetY()+4;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->Cell(70,5, "Gross sales", 1, "L");


	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(15, 5, number_format(round($total_gross_sales_values[$year]/1000, 0), 0, '.', "'"), 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(15, 5, "" , 0, "", "R");
	}


	$pdf->SetFont("arialn", "", 9);
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);	
	//draw box
	$pdf->Cell(70, 10, "" , 1, "", "L");
	$pdf->SetXY($margin_left+1,$y);	
	$pdf->Cell(70,5, "Sales reductions", 0, "L");


	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$tmp = $sales_reduction_values[$year];
		
		$pdf->Cell(15, 5, number_format(round($tmp/1000, 0), 0, '.', "'"), 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(15, 5, "" , 0, "", "R");
	}
	
	$y = $pdf->GetY()+5;
	$pdf->SetFont("arialn", "B", 9);
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->Cell(70,5, "Net sales", 1, "L");

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(15, 5, number_format(round($total_net_sales_values[$year]/1000, 0), 0, '.', "'"), 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(15, 5, "" , 0, "", "R");
	}


	
	$pdf->SetFont("arialn", "", 9);
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);	
	//draw box
	$pdf->Cell(70, 8, "" , 1, "", "L");
	$pdf->SetXY($margin_left+1,$y);	
	$pdf->Cell(70,4, "Gross margin", 0, "L");


	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(15, 4, number_format(round($total_gross_margin_values[$year]/1000, 0), 0, '.', "'"), 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(15, 4, "" , 0, "", "R");
	}
	
	$y = $pdf->GetY()+4;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(70,4, "Gross margin % (of sales)", 0, "L");

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{

		$pdf->Cell(15, 4, number_format(round($total_goss_margin_shares[$year], 1), 0, '.', "'") . "%", 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(15, 4, "" , 0, "", "R");
	}


	$pdf->SetFont("arialn", "", 9);
	$y = $pdf->GetY()+4;
	$pdf->SetXY($margin_left+1,$y);	
	//draw box
	$pdf->Cell(70, 15, "" , 1, "", "L");
	$pdf->SetXY($margin_left+1,$y);	
	$pdf->Cell(70,5, "Indirect salaries & social benefits", 0, "L");


	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$tmp = -1*($indirect_salaries_values[$year]);
		$pdf->Cell(15, 5, number_format(round($tmp/1000, 0), 0, '.', "'"), 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(15, 5, "" , 0, "", "R");
	}


	$pdf->SetFont("arialn", "", 9);
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);	
	$pdf->Cell(70,5, "Rents, amortization key money", 0, "L");


	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$tmp = -1*($rents_total_values[$year] + $prepayed_rent_values[$year]);
		$pdf->Cell(15, 5, number_format(round($tmp/1000, 0), 0, '.', "'"), 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(15, 5, "" , 0, "", "R");
	}
	
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->Cell(70,5, "% (of sales)", 0, "L");


	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$tmp = $rents_total_values[$year] + $prepayed_rent_values[$year];
		if($total_net_sales_values[$year] > 0)
		{
			$tmp = $tmp/$total_net_sales_values[$year];
			$tmp = number_format(round(100*$tmp, 2), 0, '.', "'") . "%";
		}
		else
		{
			$tmp = "";
		}
		$pdf->Cell(15, 5, $tmp, 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(15, 5, "" , 0, "", "R");
	}

	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->Cell(70,5, "Depreciation on fixed assets", 0, "L");

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$tmp = -1*$depreciation_values[$year];
		$pdf->Cell(15, 5, number_format(round($tmp/1000, 0), 0, '.', "'"), 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(15, 5, "" , 0, "", "R");
	}

	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->Cell(70,5, "Other expenses", 0, "L");


	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$tmp = -1*($auxmat_values[$year] + $sales_admin_values[$year] + $other_expenses_values[$year]+ $marketing_expenses[$year]) ;
		$pdf->Cell(15, 5, number_format(round($tmp/1000, 0), 0, '.', "'"), 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(15, 5, "" , 0, "", "R");
	}

	
	$pdf->SetFont("arialn", "B", 9);
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->Cell(70,5, "Operating income (excl. wholesale margin)", 1, "L");


	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(15, 5, number_format(round($operating_income01_values[$year]/1000, 0), 0, '.', "'"), 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(15, 5, "" , 0, "", "R");
	}

	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->Cell(58,5, "Operating income (incl. wholesale margin)", 1, "L");
	$pdf->SetFont("arialn", "", 9);

	
	$pdf->Cell(12,5, $weighted_average_whole_sale_margin . "%", 1, "", "R", true);
	$pdf->SetFont("arialn", "B", 9);
	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(15, 5, number_format(round($operating_income02_values[$year]/1000, 0), 0, '.', "'"), 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(15, 5, "" , 0, "", "R");
	}


	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->Cell(70,5, "Operating income (incl. WSM scenario 80%)", 1, "L");
	$pdf->SetFont("arialn", "", 9);

	$pdf->SetFont("arialn", "B", 9);
	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(15, 5, number_format(round($operating_income02_80_percent_values[$year]/1000, 0), 0, '.', "'"), 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(15, 5, "" , 0, "", "R");
	}


	$pdf->SetFont("arialn", "", 9);
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->Cell(70,5, "Break even units (excl. wholesale margin)", 1, "L");

	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(15, 5, number_format(round($break_even_retail_margin[$year], 0), 0, '.', "'"), 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(15, 5, "" , 0, "", "R");
	}

	$pdf->SetFont("arialn", "", 9);
	$y = $pdf->GetY()+5;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->Cell(70,5, "Break even units (incl. wholesale margin)", 1, "L");

	
	$i = 0;
	foreach($years_page1 as $key=>$year)
	{
		$pdf->Cell(15, 5, number_format(round($break_even_wholesale_margin[$year], 0), 0, '.', "'"), 1, "", "R", true); 
		$i++;
	}
	for($k=$i;$k<$i;$k++)
	{
		$pdf->Cell(15, 5, "" , 0, "", "R");
	}
	
	if($ln_basicdata["ln_basicdata_rent"]) {
		$y = $pdf->GetY()+6;
		$pdf->SetXY($margin_left+1,$y);
		$pdf->SetFont("arialn", "", 9);
		$pdf->Cell(188, 4, "General Remarks", 1, "", "L");
		$pdf->Ln();
		$y = $pdf->GetY();
		$pdf->SetXY($margin_left+1,$y);
		$pdf->MultiCell(188,0, $ln_basicdata["ln_basicdata_rent"], 1, "T", true);
	}
	else {
		$pdf->SetY($pdf->GetY()+5);
	}

	
	
	//4. Reasons/Commitment

	$y = $pdf->GetY()+1;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(188, 6, "4. Reasons/Commitment", 1, "", "L");


	$y = $pdf->GetY()+6;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(188, 4, "Reasons of the lease renewal request (location, performance, ...)", 1, "", "L");
	$pdf->Ln();
	$y = $pdf->GetY();
	$pdf->SetXY($margin_left+1,$y);
	$pdf->MultiCell(188,0, $ln_basicdata['ln_basicdata_remarks1'], 1, "T", true);

	$y = $pdf->GetY();
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(188, 4, "Justification projected sales in view of pas performance (make also reference to other lease renewals)", 1, "", "L");
	$pdf->Ln();
	$y = $pdf->GetY();
	$pdf->SetXY($margin_left+1,$y);
	$pdf->MultiCell(188,0, $ln_basicdata['ln_basicdata_remarks2'], 1, "T", true);

	$y = $pdf->GetY();
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(188, 4, "Commitment by the Brand Manager that Sales targets are very likely to be achieved)", 1, "", "L");
	$pdf->Ln();
	$y = $pdf->GetY();
	$pdf->SetXY($margin_left+1,$y);
	$pdf->MultiCell(188,0, $ln_basicdata['ln_basicdata_remarks3'], 1, "T", true);

	$y = $pdf->GetY();
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(188, 4, "Commitment by the Country Manager that Sales targets are very likely to be achieved)", 1, "", "L");
	$pdf->Ln();
	$y = $pdf->GetY();
	$pdf->SetXY($margin_left+1,$y);
	$pdf->MultiCell(188,0, $ln_basicdata['ln_basicdata_remarks4'], 1, "T", true);


	
	// 5. Approvals
	$y = $pdf->GetY()+1;
	
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(188, 6, "5. Approvals", 1, "", "L");

	
	$y = $pdf->GetY()+6;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "B", 9);
	$pdf->MultiCell(45,9, "", 1, "L");
	$pdf->SetXY($margin_left+1,$y);
	$pdf->MultiCell(15,9, "Country", 0, "T");
	$pdf->SetFont("arialn", "", 9);
	$pdf->SetXY($margin_left+15,$y);
	$pdf->MultiCell(30,9, "Country Manager " . "\r\n" . $approval_name11, 0, "T");
	$pdf->SetXY($margin_left+45,$y);
	$pdf->MultiCell(50,9, "", 1, "T", true);

	$pdf->SetFont("arialn", "B", 9);
	$pdf->SetXY($margin_left+95,$y);
	$pdf->MultiCell(45,9, "", 1, "L");
	$pdf->SetXY($margin_left+95,$y);
	$pdf->MultiCell(15,9, "Brand", 0, "T");
	$pdf->SetFont("arialn", "", 9);
	$pdf->SetXY($margin_left+105, $y);
	$pdf->MultiCell(30,9, "Retail Controller" . "\r\n" . $approval_name9, 0, "T");
	$pdf->SetXY($margin_left+140,$y);
	$pdf->MultiCell(49,9, "", 1, "T", true);


	$y = $pdf->GetY();
	$pdf->SetXY($margin_left+1,$y);

	
	$pdf->SetFont("arialn", "B", 9);
	$pdf->MultiCell(45,9, "", 1, "L");
	$pdf->SetXY($margin_left+1,$y);
	$pdf->MultiCell(15,9, "", 0, "T");
	$pdf->SetFont("arialn", "", 9);
	$pdf->SetXY($margin_left+15,$y);
	$pdf->MultiCell(30,9, "Finance/Service Center " . "\r\n" .  $approval_name12, 0, "T");
	$pdf->SetXY($margin_left+45,$y);
	$pdf->MultiCell(50,9, "", 1, "T", true);

	$pdf->SetFont("arialn", "B", 9);
	$pdf->SetXY($margin_left+95,$y);
	$pdf->MultiCell(45,9, "", 1, "L");
	$pdf->SetXY($margin_left+95,$y);
	$pdf->MultiCell(15,9, "", 0, "T");
	$pdf->SetFont("arialn", "", 9);
	$pdf->SetXY($margin_left+105,$y);
	$pdf->MultiCell(30,9, "VP Sales" . "\r\n" . $approval_name3, 0, "T");
	$pdf->SetXY($margin_left+140,$y);
	$pdf->MultiCell(49,9, "", 1, "T", true);


	//china projects
	if($project['order_client_address'] == 42
		or $project['order_client_address'] == 7) {
		$y = $pdf->GetY();
		$pdf->SetXY($margin_left+1,$y);


		$pdf->SetFont("arialn", "B", 9);
		$pdf->MultiCell(45,9, "", 1, "L");
		$pdf->SetXY($margin_left+1,$y);
		$pdf->MultiCell(15,9, "", 0, "T");
		$pdf->SetFont("arialn", "", 9);
		$pdf->SetXY($margin_left+15,$y);
		$pdf->MultiCell(30,9, "Head of Controlling " . "\r\n" . $approval_name16, 0, "T");
		$pdf->SetXY($margin_left+45,$y);
		$pdf->MultiCell(50,9, "", 1, "T", true);

	}
	
	
	$y = $pdf->GetY();

	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "B", 9);
	$pdf->MultiCell(45,9, "", 1, "L");
	$pdf->SetXY($margin_left+1,$y);
	$pdf->MultiCell(15,9, "", 0, "T");
	$pdf->SetFont("arialn", "", 9);
	$pdf->SetXY($margin_left+15,$y);
	$pdf->MultiCell(30,9, "Brand Manager " . "\r\n" . $approval_name13, 0, "T");
	$pdf->SetXY($margin_left+45,$y);
	$pdf->MultiCell(50,9, "", 1, "T", true);

	//china projects
	if($project['order_client_address'] == 42
		or $project['order_client_address'] == 7) {
			$y = $pdf->GetY()-18;
	}

	$pdf->SetFont("arialn", "B", 9);
	$pdf->SetXY($margin_left+95,$y);
	$pdf->MultiCell(45,9, "", 1, "L");
	$pdf->SetXY($margin_left+95,$y);
	$pdf->MultiCell(15,9, "", 0, "T");
	$pdf->SetFont("arialn", "", 9);
	$pdf->SetXY($margin_left+105,$y);
	$pdf->MultiCell(30,9, "VP Finance/CFO" . "\r\n" . $approval_name7, 0, "T");
	$pdf->SetXY($margin_left+140,$y);
	$pdf->MultiCell(49,9, "", 1, "T", true);

	
	$y = $pdf->GetY();
	$pdf->SetXY($margin_left+1,$y);

	if($project['order_client_address'] != 42
		and $project['order_client_address'] != 7) {
		
		$pdf->SetFont("arialn", "B", 9);
		$pdf->MultiCell(45,9, "", 1, "L");
		$pdf->SetXY($margin_left+1,$y);
		$pdf->MultiCell(15,9, "", 0, "T");
		$pdf->SetFont("arialn", "", 9);
		$pdf->SetXY($margin_left+15,$y);
		$pdf->MultiCell(30,9, "", 0, "T");
		$pdf->SetXY($margin_left+45,$y);
		$pdf->MultiCell(50,9, "", 1, "T", true);
	}

	$pdf->SetFont("arialn", "B", 9);
	$pdf->SetXY($margin_left+95,$y);
	$pdf->MultiCell(45,9, "", 1, "L");
	$pdf->SetXY($margin_left+95,$y);
	$pdf->MultiCell(15,9, "", 0, "T");
	$pdf->SetFont("arialn", "", 9);
	$pdf->SetXY($margin_left+105,$y);
	$pdf->MultiCell(30,9, "President" . "\r\n" . $approval_name2, 0, "T");
	$pdf->SetXY($margin_left+140,$y);
	$pdf->MultiCell(49,9, "", 1, "T", true);
	

	

	//draw outer box
	$y = $pdf->GetY()-$margin_top - 8;
	$pdf->SetXY($margin_left,$y_at_start);
	$pdf->Cell(190, $y, "", 1);


	/*
	$pdf->SetFont("arialn", "", 9);
	$pdf->SetXY($x_textbox,$y_textbox);
	$pdf->MultiCell(70,10, $ln_basicdata["ln_basicdata_rent"], 0, "L");
	*/
		
?>