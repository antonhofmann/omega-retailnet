<?php
/********************************************************************

    ln_profitability.php

    Application Form: lease negiotiation form
    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-06-17
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-06-17
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";
require_once "../shared/func_posindex.php";

check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}

/********************************************************************
    prepare all data needed
*********************************************************************/

//get place_id of actual project
if($project["pipeline"] == 0)
{
	$pos_data = get_poslocation($project["posaddress_id"], "posaddresses");
}
elseif($project["pipeline"] == 1)
{
	$pos_data = get_poslocation_from_pipeline($project["posaddress_id"], $project["order_id"]);
}

$place_id = $pos_data["place_id"];
$province_id = $pos_data["province_id"];
$country_id = $pos_data["posaddress_country"];
$sellout_ids = array();
$profitability_ids = array();
$client_id = $pos_data["posaddress_client_id"];
$parent_id = $pos_data["address_parent"];

$include_sellouts_in_ln = array();
$include_profitability_in_ln = array();

//sellouts

$sql_list1 =    "select DISTINCT posaddress_id, posaddress_name, project_costtype_text, " . 
                "province_canton, place_name " . 
                "from posaddresses " . 
				"left join possellouts on possellout_posaddress_id = posaddress_id " .
				"left join places on place_id = posaddress_place_id " .
				"left join provinces on province_id = place_province " . 
				"left join project_costtypes on project_costtype_id = posaddress_ownertype";
/*
$list1_filter = "posaddress_country = " . $country_id . 
                " and posaddress_id <> " . $pos_data["posaddress_id"]  . 
				" and possellout_posaddress_id > 0 and 
				(possellout_month = 12 
					or 
					(possellout_year = " . dbquote(date('Y')) . " 
						and year(posaddress_store_openingdate) = " . dbquote(date('Y')) . "
					)
				) "; 

$list1_filter = "posaddress_country = " . $country_id . 
			" and possellout_posaddress_id > 0"; 
*/	

$list1_filter = "(posaddress_client_id = " . $client_id . " or posaddress_client_id = " . dbquote($parent_id). ") " .
				" and possellout_posaddress_id > 0"; 

$list1_order = "project_costtype_text, place_name, posaddress_name";
$list1_group = "";



$sql = $sql_list1 . " where " . $list1_filter;


//get sellouts values
$year_values = array();
$watches_values = array();
$bijoux_values = array();
$distances = array();
$ptc = array();
$ptc_dates = array();


$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	
	$sql_p = "select possellout_id, possellout_year, possellout_month, " . 
		     "possellout_watches_units, possellout_bijoux_units " . 
		     "from possellouts " .
		     " inner join posaddresses on posaddress_id = possellout_posaddress_id " . 
		     " where 
					(possellout_month = 12 
					or 
					(possellout_year = " . dbquote(date('Y')) . " 
						and year(posaddress_store_openingdate) = " . dbquote(date('Y')) . "
					)
				)
			  and possellout_posaddress_id = " . $row["posaddress_id"] . 
		     " order by possellout_year DESC";

		
	$res_p = mysql_query($sql_p) or dberror($sql_p);
	if ($row_p = mysql_fetch_assoc($res_p))
	{
		$year_values[$row["posaddress_id"]] = $row_p["possellout_year"];
		$watches_values[$row["posaddress_id"]] = $row_p["possellout_watches_units"];
		$bijoux_values[$row["posaddress_id"]] = $row_p["possellout_bijoux_units"];
		$sellout_ids[$row["posaddress_id"]] = $row_p["possellout_id"];	
		
		
		$sql_l = "select ln_basicdata_lnr03_distance, ln_basicdata_lnr03_ptc, ln_basicdata_lnr03_ptc_date " . 
				 "from ln_basicdata_inr03 " .
				 " where (ln_basicdata_lnr03_ln_version = 0 or ln_basicdata_lnr03_ln_version is null) and ln_basicdata_lnr03_sellout_id = " . $row_p["possellout_id"] .
				 " AND ln_basicdata_lnr03_lnbasicdata_id = " . dbquote($ln_basicdata['ln_basicdata_id']);


		$res_l = mysql_query($sql_l) or dberror($sql_l);
		if ($row_l = mysql_fetch_assoc($res_l))
		{
			$distances[$row["posaddress_id"]] = $row_l["ln_basicdata_lnr03_distance"];
			$ptc[$row["posaddress_id"]] = $row_l["ln_basicdata_lnr03_ptc"];

			$ptc_dates[$row["posaddress_id"]] = to_system_date($row_l["ln_basicdata_lnr03_ptc_date"]);


			$ptc2[$row["posaddress_id"]] = $row_l["ln_basicdata_lnr03_ptc"];

			$ptc_dates2[$row["posaddress_id"]] = to_system_date($row_l["ln_basicdata_lnr03_ptc_date"]);
		}
		else {
			$distances[$row["posaddress_id"]] = "";
			$ptc[$row["posaddress_id"]] = "";

			$ptc_dates[$row["posaddress_id"]] = "";
		}

		
	}

}

//profitability
$sql = $sql_list1 . " where " . $list1_filter;
//get sellouts values
$year2_values = array();
$net_sales = array();
$grossmargin_values = array();
$expenses_values = array();
$operating_income01_values = array();
$operating_income02_values = array();
$ptc2 = array();
$ptc_dates2 = array();

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	
	$sql_p = "select * " . 
		     "from possellouts " . 
		     " inner join posaddresses on posaddress_id = possellout_posaddress_id " . 
		     " where 
					(possellout_month = 12 
					or 
					(possellout_year = " . dbquote(date('Y')) . " 
						and year(posaddress_store_openingdate) = " . dbquote(date('Y')) . "
					)
				) 
		      and possellout_posaddress_id = " . $row["posaddress_id"] . 
		     " order by possellout_year DESC";


	$sql_p = "select * " . 
		     "from possellouts " . 
		     " inner join posaddresses on posaddress_id = possellout_posaddress_id " . 
		     " where 
					(possellout_month = 12 
					or 
					(possellout_year = " . dbquote(date('Y')) . " 
						and year(posaddress_store_openingdate) = " . dbquote(date('Y')) . "
					)
				)
				and possellout_posaddress_id = " . $row["posaddress_id"] . 
		     " order by possellout_year DESC";
	
	
	$res_p = mysql_query($sql_p) or dberror($sql_p);
	if ($row_p = mysql_fetch_assoc($res_p))
	{
		$year2_values[$row["posaddress_id"]] = $row_p["possellout_year"];
		$net_sales[$row["posaddress_id"]] = $row_p["possellout_net_sales"];
		$grossmargin_values[$row["posaddress_id"]] = $row_p["possellout_grossmargin"];
		
		$expenses_values[$row["posaddress_id"]] = $row_p["possellout_operating_expenses"];
		$operating_income01_values[$row["posaddress_id"]] = $row_p["possellout_operating_income_excl"];
		$operating_income02_values[$row["posaddress_id"]] = $row_p["possellout_operating_income_incl"];
		$profitability_ids[$row["posaddress_id"]] = $row_p["possellout_id"];
		//$ptc2[$row["posaddress_id"]] = "";
		//$ptc_dates2[$row["posaddress_id"]] = "";
		
		
	}
}


if(!param("save"))
{
	$sql = "select possellout_posaddress_id from ln_basicdata_inr03 " . 
		   "left join possellouts on possellout_id = ln_basicdata_lnr03_sellout_id " . 
		   " inner join posaddresses on posaddress_id = possellout_posaddress_id " . 
		   " where 
					(possellout_month = 12 
					or 
					(possellout_year = " . dbquote(date('Y')) . " 
						and year(posaddress_store_openingdate) = " . dbquote(date('Y')) . "
					)
				) 
			and  ln_basicdata_lnr03_ln_version = 0 and ln_basicdata_lnr03_sellout_id > 0 " . 
		   "and ln_basicdata_lnr03_lnbasicdata_id = " . $ln_basicdata["ln_basicdata_id"];

	
		     

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$include_sellouts_in_ln[$row["possellout_posaddress_id"]] = 1;
	}


	$sql = "select possellout_id, possellout_posaddress_id, 
	        ln_basicdata_lnr03_ptc, ln_basicdata_lnr03_ptc_date " . 
		   "from ln_basicdata_inr03 " . 
		   "left join possellouts on possellout_id = ln_basicdata_lnr03_profitability_id " . 
		   " inner join posaddresses on posaddress_id = possellout_posaddress_id " . 
		   " where 
					(possellout_month = 12 
					or 
					(possellout_year = " . dbquote(date('Y')) . " 
						and year(posaddress_store_openingdate) = " . dbquote(date('Y')) . "
					)
				) 
			and ln_basicdata_lnr03_ln_version = 0 and ln_basicdata_lnr03_profitability_id > 0 " . 
		   "and ln_basicdata_lnr03_lnbasicdata_id = " . $ln_basicdata["ln_basicdata_id"];

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$include_profitability_in_ln[$row["possellout_posaddress_id"]] = 1;
		$ptc2[$row["possellout_posaddress_id"]] = $row["ln_basicdata_lnr03_ptc"];
		$ptc_dates2[$row["possellout_posaddress_id"]] = to_system_date($row["ln_basicdata_lnr03_ptc_date"]);
	}

}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("ln_basicdata", "ln_basicdata");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("save", 1);


include("include/project_head_small.php");


/********************************************************************
    build list of standard expenses
*********************************************************************/
$list1 = new ListView($sql_list1, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list1->set_title("POS Locations for LNR-03 Distribution Analysis");
$list1->set_entity("possellouts");
$list1->set_filter($list1_filter);
$list1->set_order($list1_order);



$list1->add_checkbox_column("include_sellouts_in_ln", "", "", $include_sellouts_in_ln);
$list1->add_column("project_costtype_text", "Legal Type", "");
$list1->add_column("place_name", "Place", "");
$list1->add_column("posaddress_name", "POS Location", "");

$list1->add_edit_column("distances", "Distance from proposed store", 10, "", $distances);
$list1->add_text_column("year", "Years", COLUMN_ALIGN_RIGHT, $year_values);
$list1->add_text_column("watches", "Watches Units", COLUMN_ALIGN_RIGHT, $watches_values);
$list1->add_text_column("bijoux", "Bijoux Units", COLUMN_ALIGN_RIGHT, $bijoux_values);
$list1->add_checkbox_column("ptc", "Proposition to Close", COLUMN_ALIGN_CENTER, $ptc);
$list1->add_date_edit_column("ptc_dates", "Planned Closing Date", 10, 0, $ptc_dates);
$list1->add_hidden("pid", param("pid"));


if(count($year_values) > 0)
{
	if(($ln_basicdata["ln_basicdata_locked"] == 0 and has_access("has_access_to_his_cer")) or has_access("has_full_access_to_cer"))
	{
		$list1->add_button("save", "Save Data");
	}
}



/********************************************************************
    build list of standard expenses
*********************************************************************/
$list2 = new ListView($sql_list1, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list2->set_title("POS Locations for LNR-03 Profitability Analysis of Corporate Stores");
$list2->set_entity("possellouts");
$list2->set_filter($list1_filter);
$list2->set_order($list1_order);


$list2->add_checkbox_column("include_profitability_in_ln", "", "", $include_profitability_in_ln);
$list2->add_column("project_costtype_text", "Legal Type", "");
$list2->add_column("place_name", "Place", "");
$list2->add_column("posaddress_name", "POS Location", "");
$list2->add_text_column("year2", "Years", COLUMN_ALIGN_RIGHT, $year2_values);
$list2->add_text_column("netsales", "Net Sales", COLUMN_ALIGN_RIGHT, $net_sales);
$list2->add_text_column("grossmargin", "Grossmargin", COLUMN_ALIGN_RIGHT, $grossmargin_values);
$list2->add_text_column("expenses", "Expenses", COLUMN_ALIGN_RIGHT | COLUMN_BREAK, $expenses_values);
$list2->add_text_column("operatingincome01", "Operating Income\nexcl. WM", COLUMN_ALIGN_RIGHT | COLUMN_BREAK, $operating_income01_values);
$list2->add_text_column("operatingincome02", "Operating Income\nincl. WM", COLUMN_ALIGN_RIGHT | COLUMN_BREAK, $operating_income02_values);
$list2->add_checkbox_column("ptc2", "Proposition to Close", COLUMN_ALIGN_CENTER, $ptc2);
$list2->add_date_edit_column("ptc_dates2", "Planned Closing Date", 10, 0, $ptc_dates2);


$list2->add_hidden("pid", param("pid"));

if(count($year_values) > 0)
{
	if(($ln_basicdata["ln_basicdata_locked"] == 0 and has_access("has_access_to_his_cer")) or has_access("has_full_access_to_cer"))
	{
		$list2->add_button("save", "Save Data");
	}
}



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


$list1->process();
$list1->populate();


$list2->process();
$list2->populate();


if($list1->button("save") or $list2->button("save"))
{
	$error = 0;
	$sql = 	"delete  from ln_basicdata_inr03 " . 
		    " where ln_basicdata_lnr03_ln_version = 0 and ln_basicdata_lnr03_lnbasicdata_id = " . $ln_basicdata["ln_basicdata_id"];

	$result = mysql_query($sql) or dberror($sql);

	foreach($list1->values("include_sellouts_in_ln") as $key => $value)
    {
        $sellout_id = (int)str_replace('__possellouts_include_in_ln', '', $key);
		$tmp = $list1->values("distances");
		$distance = $tmp[$key];


		
		
		$tmp = $list1->values("ptc");
		$ptc = 0;
		if(array_key_exists($key, $tmp)) {
			$ptc = $_POST['__possellouts_ptc_' . $key];
		}

		$tmp = $list1->values("ptc_dates");
		$planned_closing_date = from_system_date($tmp[$key]);

		if($planned_closing_date) {
			$ptc = 1;
		}

		echo $value . " " . $sellout_id . "<br />";
		if($ptc == 1 and !$planned_closing_date) {
			$form->error("Please indicate the planned closing date!");
			$error = 1;
		}
		else {
			if($value > 0 and array_key_exists($sellout_id, $sellout_ids))
			{
				$sql = "insert into ln_basicdata_inr03 (" .
					   "ln_basicdata_lnr03_ln_version, " . 
					   "ln_basicdata_lnr03_lnbasicdata_id, " . 
					   "ln_basicdata_lnr03_sellout_id, " .
					   "ln_basicdata_lnr03_distance, " .
					   "ln_basicdata_lnr03_ptc, " .
					   "ln_basicdata_lnr03_ptc_date, " .
					   "user_created, date_created) " .
					   " values (0," .
					   $ln_basicdata["ln_basicdata_id"] . ", " .
					   $sellout_ids[$sellout_id] . ", " .
					   dbquote($distance) . ", " .
					   dbquote($ptc) . ", " .
					   dbquote($planned_closing_date) . ", " .
					   "'" . user_login() . "', " .
					   "'" . date("Y-m-d") . "')";
				$result = mysql_query($sql) or dberror($sql);

				if($planned_closing_date) {
					
					$sql = "update posaddresses set posaddress_store_planned_closingdate = " . 
						   dbquote($planned_closing_date) . 
						   " where posaddress_id = " . dbquote($key);
					$result = mysql_query($sql) or dberror($sql);
				
				}
			}
		}
    }

	foreach($list2->values("include_profitability_in_ln") as $key => $value)
    {
        $profitability_id = (int)str_replace('__possellouts_include_profitability_in_ln', '', $key);

		
		
		$tmp = $list1->values("ptc");
		$ptc2 = 0;
		if(array_key_exists($key, $tmp)) {
			$ptc2 = $_POST['__possellouts_ptc_' . $key];
		}

		$tmp = $list1->values("ptc_dates");
		$planned_closing_date = from_system_date($tmp[$key]);

		if($ptc2 == 0) {
		
			$tmp = $list2->values("ptc2");
			$ptc2 = 0;
			if(array_key_exists($key, $tmp)) {
				$ptc2 = $_POST['__possellouts_ptc2_' . $key];
			}

			$tmp = $list2->values("ptc_dates2");
			$planned_closing_date = from_system_date($tmp[$key]);
		}

		if($planned_closing_date) {
			$ptc2 = 1;
		}


		
		if($ptc2 == 1 and !$planned_closing_date) {
			$form->error("Please indicate the planned closing date!");
			$error = 1;
		}
		else {
		
			if($value > 0 and array_key_exists($profitability_id, $profitability_ids))
			{
				$sql = "insert into ln_basicdata_inr03 (" .
					   "ln_basicdata_lnr03_ln_version, " . 
					   "ln_basicdata_lnr03_lnbasicdata_id, " . 
					   "ln_basicdata_lnr03_profitability_id, " .
					   "ln_basicdata_lnr03_ptc, " .
					   "ln_basicdata_lnr03_ptc_date, " .
					   "user_created, date_created) " .
					   " values (0," .
					   $ln_basicdata["ln_basicdata_id"] . ", " .
					   $profitability_ids[$profitability_id] . ", " .
					   dbquote($ptc2) . ", " .
					   dbquote($planned_closing_date) . ", " .
					   "'" . user_login() . "', " .
					   "'" . date("Y-m-d") . "')";
				$result = mysql_query($sql) or dberror($sql);

				
				if($planned_closing_date) {
					
					$sql = "update posaddresses set posaddress_store_planned_closingdate = " . 
						   dbquote($planned_closing_date) . 
						   " where posaddress_id = " . dbquote($key);
					$result = mysql_query($sql) or dberror($sql);
				
				}
				//echo $sql;
			}
		}
    }

	if($error == 0) {
		$link = "ln_profitability.php?context=ln&pid=" . param("pid");
		redirect($link);
	}
	
}




/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();
$page->title("Lease Negotiation Form: Sellouts and Profitability");


require_once("include/tabs_ln.php");

$form->render();
$list1->render();
echo '<p>&nbsp;</p>';
$list2->render();


$page->footer();

?>