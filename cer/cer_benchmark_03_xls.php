<?php
/********************************************************************

    cer_benchmark_03_xls.php

    Generate Excel File: CER BenchmarkRental Details

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-10-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-10-15
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_access_to_cer_benchmarks");

require_once "../include/xls/Writer.php";
require_once "include/get_functions.php";

/********************************************************************
    prepare Data Needed
*********************************************************************/

include("cer_benchmark_filter.php");

/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$filename = "benchmark_" . $benchmark_shortcut . "_rental_details_" . date("Ymd") . ".xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename); 
$xls->setVersion(8);
$sheet = $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

//formats
$f_header =& $xls->addFormat();
$f_header->setSize(12);
$f_header->setAlign('left');
$f_header->setBold();

$f_title =& $xls->addFormat();
$f_title->setSize(10);
$f_title->setAlign('left');
$f_title->setBold();

$f_group_title =& $xls->addFormat();
$f_group_title->setSize(10);
$f_group_title->setAlign('left');
$f_group_title->setBold();

$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);

$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);


$f_unused =& $xls->addFormat();
$f_unused->setSize(8);
$f_unused->setBorder(1);
$f_unused->setPattern(2);
$f_unused->setBgColor('silver');

$f_border_left=& $xls->addFormat();
$f_border_left->setTop(1);
$f_border_left->setBottom(1);
$f_border_left->setLeft(1);
$f_border_left->setSize(10);
$f_border_left->setAlign('left');
$f_border_left->setBold();

$f_border_right=& $xls->addFormat();
$f_border_right->setTop(1);
$f_border_right->setBottom(1);
$f_border_right->setRight(1);


$f_border_middle=& $xls->addFormat();
$f_border_middle->setTop(1);
$f_border_middle->setBottom(1);

//captions
$c01 = 0;
$captions = array();
$captions[] = "Project Number";
$sheet->setColumn($c01, $c01, 10);
$c01++;

$captions[] = "Project State";
$sheet->setColumn($c01, $c01, 6);
$c01++;

$captions[] = "Country";
$sheet->setColumn($c01, $c01, 14);
$c01++;

$captions[] = "POS Location Address";
$sheet->setColumn($c01, $c01, 60);
$c01++;

$captions[] = "Product Line";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Product Line Subclass";
$sheet->setColumn($c01, $c01, 8);
$c01++;


$captions[] = "Opening Date";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Legal Type";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Project Kind";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Project Type";
$sheet->setColumn($c01, $c01, 12);
$c01++;

$captions[] = "Pre rental costs to be paid";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Turnover based rental costs per year";
$sheet->setColumn($c01, $c01, 6);
$c01++;

$captions[] = "Additional rental costs per year";
$sheet->setColumn($c01, $c01, 6);
$c01++;

$captions[] = "Annual rental costs";
$sheet->setColumn($c01, $c01, 6);
$c01++;

$captions[] = "Total rental period in years";
$sheet->setColumn($c01, $c01, 6);
$c01++;


$captions[] = "Gross surface rented in sqms";
$sheet->setColumn($c01, $c01, 6);
$c01++;

$captions[] = "Total surface rented in sqms";
$sheet->setColumn($c01, $c01, 6);
$c01++;

$captions[] = "Sales surface in sqms";
$sheet->setColumn($c01, $c01, 6);
$c01++;

$captions[] = "Other dedicated surface (toilets, office, etc.)";
$sheet->setColumn($c01, $c01, 6);

$captions[] = "Contract date rental agreement";
$sheet->setColumn($c01, $c01, 6);

$captions[] = "Expiry date rental agreement";
$sheet->setColumn($c01, $c01, 6);

$captions[] = "Extension option";
$sheet->setColumn($c01, $c01, 6);

$captions[] = "Exit option";
$sheet->setColumn($c01, $c01, 6);



/********************************************************************
    write all captions
*********************************************************************/
$sheet->write(0, 0, $benchmark_title, $f_header);


if($base_currency == "chf")
{
	$sheet->write(1, 0, "Rental Details in CHF (" . date("d.m.Y, H:m:s") . ")", $f_title);
}
else
{
	$sheet->write(1, 0, "Rental Details in LOC (" . date("d.m.Y, H:m:s") . ")", $f_title);
}


$sheet->write(3,10,"Rental costs", $f_border_left);
$sheet->write(3,11,"", $f_border_middle);
$sheet->write(3,12,"", $f_border_middle);
$sheet->write(3,13,"", $f_border_right);

$sheet->write(3,14,"", "");
$sheet->write(3,15,"Rental space", $f_border_left);
$sheet->write(3,16,"", $f_border_middle);
$sheet->write(3,17,"", $f_border_right);

$sheet->write(3,18,"Contract details", $f_border_left);
$sheet->write(3,19,"", $f_border_middle);
$sheet->write(3,20,"", $f_border_right);


$sheet->setRow(4, 165);
$sheet->writeRow(4, 0, $captions, $f_caption);

/********************************************************************
    write all data
*********************************************************************/
$list_totals = array();
$num_of_projects = 0;

$row_index = 5;
$c01 = 0;

foreach($posorders as $key=>$posorder)
{
	if($posorder["pipeline"] == 1)
	{
		$sql = "select * " . 
			   "from posaddressespipeline " . 
			   "left join posorderspipeline on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " .
			   "left join orders on order_id = project_order " .
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "left join project_costs on project_cost_order = posorder_order " .
			   "where posorder_type = 1 and posorder_id = " . $posorder["id"];
	}
	elseif($posorder["pipeline"] == 2)
	{
		$sql = "select * " . 
			   "from posaddresses " . 
			   "left join posorderspipeline on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " . 
			   "left join orders on order_id = project_order " .
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "left join project_costs on project_cost_order = posorder_order " .
			   "where posorder_type = 1 and posorder_id = " . $posorder["id"];
	}
	else
	{
		$sql = "select * " . 
			   "from posaddresses " . 
			   "left join posorders on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " . 
			   "left join orders on order_id = project_order " .
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "left join project_costs on project_cost_order = posorder_order " .
			   "where posorder_type = 1 and posorder_id = " . $posorder["id"];
	}


	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$num_of_projects++;

		$exchange_rate = 1;
		$factor = 1;


		$cer_basicdata = get_cer_basicdata($row["project_id"]);

		if(count($cer_basicdata) > 0)
		{
			$exchange_rate = $cer_basicdata["cer_basicdata_exchangerate"];
			$factor = $cer_basicdata["cer_basicdata_factor"];
			if(!$factor) {$factor = 1;}
		}

		if($base_currency == "chf")
		{
		}
		else
		{
			$exchange_rate = 1;
			$factor = 1;
		}

		$sheet->write($row_index, $c01, $row["posorder_ordernumber"], $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["order_actual_order_state_code"], $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["country_name"], $f_normal);
		$c01++;
		$tmp = $row["posaddress_place"] . ", " . $row["posaddress_name"] . ", " . $row["posaddress_address"];
		$sheet->write($row_index, $c01, $tmp, $f_normal);
		$c01++;
		
		$sheet->write($row_index, $c01, $row["product_line_name"], $f_normal);
		$c01++;
		
		$sheet->write($row_index, $c01, $row["productline_subclass_name"], $f_normal);
		$c01++;
		

		$sheet->write($row_index, $c01, to_system_date($row["posaddress_store_openingdate"]), $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["project_costtype_text"], $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["projectkind_name"], $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["postype_name"], $f_normal);
		$c01++;

		//get intagibles
		$intagibles = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and (cer_investment_type = 15 or cer_investment_type = 17)";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$intagibles = $row_s["total"] * $exchange_rate / $factor;
		}
		$sheet->write($row_index, $c01, $intagibles, $f_number);
		$c01++;

		if(array_key_exists("intagibles", $list_totals))
		{
			$list_totals["intagibles"] = $list_totals["intagibles"] + $intagibles;
		}
		else
		{
			$list_totals["intagibles"] = $intagibles;
		}

		//get duration of business plan in months
		$duration_in_months = 0;
		$sql_s = "select cer_basicdata_firstyear, cer_basicdata_firstmonth, cer_basicdata_lastyear, cer_basicdata_lastmonth " . 
			     "from cer_basicdata " . 
			     "where cer_basicdata_version = 0 and cer_basicdata_project = " . dbquote($row["project_id"]);

		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$first_year = $row_s["cer_basicdata_firstyear"];
			$first_month = $row_s["cer_basicdata_firstmonth"];
			$last_year = $row_s["cer_basicdata_lastyear"];
			$last_month = $row_s["cer_basicdata_lastmonth"];

			$duration_in_months = 13 - $first_month;
			$duration_in_months = $duration_in_months + $last_month;

			$tmp = $last_year - $first_year - 1;
			$tmp = $tmp*12;

			$duration_in_months = $duration_in_months + $tmp;
			

		}
		

		//get turnover based rental costs
		$turnoverbased_rental_costs = 0;
		$sql_s = "select sum(cer_expense_amount) as total " . 
				 "from cer_expenses " . 
				 "where cer_expense_cer_version = 0 and cer_expense_project = " . dbquote($row["project_id"]) . 
			     " and cer_expense_type = 16 ";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$turnoverbased_rental_costs = $row_s["total"] * $exchange_rate / $factor;

			if($duration_in_months > 0)
			{
				$turnoverbased_rental_costs = round(12 * $turnoverbased_rental_costs / $duration_in_months, 0);
			}
		}


		$sheet->write($row_index, $c01, $turnoverbased_rental_costs, $f_number);
		$c01++;

		if(array_key_exists("turnoverbased_rental_costs", $list_totals))
		{
			$list_totals["turnoverbased_rental_costs"] = $list_totals["turnoverbased_rental_costs"] + $turnoverbased_rental_costs;
		}
		else
		{
			$list_totals["turnoverbased_rental_costs"] = $turnoverbased_rental_costs;
		}

		//get additional rental costs
		$additional_rental_costs = 0;
		$sql_s = "select sum(cer_expense_amount) as total " . 
				 "from cer_expenses " . 
				 "where cer_expense_cer_version = 0 and cer_expense_project = " . dbquote($row["project_id"]) . 
			     " and cer_expense_type in (3,18,19, 20) ";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$additional_rental_costs = $row_s["total"] * $exchange_rate / $factor;

			if($duration_in_months > 0)
			{
				$additional_rental_costs = round(12 * $additional_rental_costs / $duration_in_months, 0);
			}
		}


		$sheet->write($row_index, $c01, $additional_rental_costs, $f_number);
		$c01++;

		if(array_key_exists("additional_rental_costs", $list_totals))
		{
			$list_totals["additional_rental_costs"] = $list_totals["additional_rental_costs"] + $additional_rental_costs;
		}
		else
		{
			$list_totals["additional_rental_costs"] = $additional_rental_costs;
		}


		//get annual rental costs
		$annual_rental_costs = 0;
		$sql_s = "select sum(cer_expense_amount) as total " . 
				 "from cer_expenses " . 
				 "where cer_expense_cer_version = 0 and cer_expense_project = " . dbquote($row["project_id"]) . 
			     " and cer_expense_type in (2,20) ";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$annual_rental_costs = $row_s["total"] * $exchange_rate / $factor;;

			if($duration_in_months > 0)
			{
				$annual_rental_costs = round(12 * $annual_rental_costs / $duration_in_months, 0);
			}
		}


		$sheet->write($row_index, $c01, $annual_rental_costs, $f_number);
		$c01++;

		if(array_key_exists("annual_rental_costs", $list_totals))
		{
			$list_totals["annual_rental_costs"] = $list_totals["annual_rental_costs"] + $annual_rental_costs;
		}
		else
		{
			$list_totals["annual_rental_costs"] = $annual_rental_costs;
		}

		//get the lease duration in years
		$starting_date = 0;
		$ending_date = 0;
		$extension_option = 0;
		$exit_option = 0;

		$sql_s = "select poslease_startdate, poslease_enddate, poslease_extensionoption, poslease_exitoption " . 
				 "from posleases " . 
				 "where poslease_posaddress = " . dbquote($row["posaddress_id"]);

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		while($row_s = mysql_fetch_assoc($res_s))
		{
			if($starting_date == 0)
			{
				$starting_date = $row_s["poslease_startdate"];
			}
			elseif($row_s["poslease_startdate"] < $starting_date)
			{
				$starting_date = $row_s["poslease_startdate"];
			}

			if($ending_date == 0)
			{
				$ending_date = $row_s["poslease_enddate"];
			}
			elseif($row_s["poslease_enddate"] > $ending_date)
			{
				$ending_date = $row_s["poslease_enddate"];
			}

			if($extension_option == 0)
			{
				$extension_option = $row_s["poslease_extensionoption"];
			}
			elseif($row_s["poslease_extensionoption"] > $extension_option)
			{
				$extension_option = $row_s["poslease_extensionoption"];
			}

			if($exit_option == 0)
			{
				$exit_option = $row_s["poslease_exitoption"];
			}
			elseif($row_s["poslease_exitoption"] > $exit_option)
			{
				$exit_option = $row_s["poslease_exitoption"];
			}
		}
		if($starting_date == 0 and $ending_date == 0)
		{
			$lease_period = 0;
		}
		else
		{
			$start = strtotime($starting_date) ;
			$end = strtotime($ending_date) ;

			$lease_period = $end - $start;
			$lease_period = $lease_period / 86400;
			$lease_period = round($lease_period/365, 2);

		}

		$sheet->write($row_index, $c01, $lease_period, $f_number);
		$c01++;

		if(array_key_exists("lease_period", $list_totals))
		{
			$list_totals["lease_period"] = $list_totals["lease_period"] + $lease_period;
		}
		else
		{
			$list_totals["lease_period"] = $lease_period;
		}
		

		//get space in square meters

		$gross_space = $row["project_cost_gross_sqms"];
		$sheet->write($row_index, $c01, number_format($gross_space, 2), $f_number);
		$c01++;


		if(array_key_exists("gross_space", $list_totals))
		{
			$list_totals["gross_space"] = $list_totals["gross_space"] + $gross_space;
		}
		else
		{
			$list_totals["gross_space"] = $gross_space;
		}

		$total_space = $row["project_cost_totalsqms"];
		$sheet->write($row_index, $c01, number_format($total_space, 2), $f_number);
		$c01++;

		if(array_key_exists("total_space", $list_totals))
		{
			$list_totals["total_space"] = $list_totals["total_space"] + $total_space;
		}
		else
		{
			$list_totals["total_space"] = $total_space;
		}
		
		$retail_area = $row["project_cost_sqms"];
		$sheet->write($row_index, $c01, number_format($retail_area, 2), $f_number);
		$c01++;

		if(array_key_exists("retail_area", $list_totals))
		{
			$list_totals["retail_area"] = $list_totals["retail_area"] + $retail_area;
		}
		else
		{
			$list_totals["retail_area"] = $retail_area;
		}



		$back_office = $row["posaddress_store_backoffice"];
		$sheet->write($row_index, $c01, number_format($back_office, 2), $f_number);
		$c01++;

		if(array_key_exists("back_office", $list_totals))
		{
			$list_totals["back_office"] = $list_totals["back_office"] + $back_office;
		}
		else
		{
			$list_totals["back_office"] = $back_office;
		}
		
		
		$sheet->write($row_index, $c01, to_system_date($starting_date), $f_number);
		$c01++;

		$sheet->write($row_index, $c01, to_system_date($ending_date), $f_number);
		$c01++;

		$sheet->write($row_index, $c01, to_system_date($extension_option), $f_number);
		$c01++;

		$sheet->write($row_index, $c01, to_system_date($exit_option), $f_number);
		$c01++;



		$c01 = 0;
		$row_index++;

	}
}

if(count($posorders) > 0)
{
	$c01 = 9;
	$row_index++;
	$sheet->write($row_index, $c01,"Sum", $f_normal);
	$c01++;
	$sheet->write($row_index, $c01,$list_totals["intagibles"], $f_number);
	$c01++;

	$sheet->write($row_index, $c01,$list_totals["turnoverbased_rental_costs"], $f_number);
	$c01++;
	$sheet->write($row_index, $c01,$list_totals["additional_rental_costs"], $f_number);
	$c01++;
	$sheet->write($row_index, $c01,$list_totals["annual_rental_costs"], $f_number);
	$c01++;
	$sheet->write($row_index, $c01,$list_totals["lease_period"], $f_number);
	$c01++;

	$sheet->write($row_index, $c01,number_format($list_totals["gross_space"], 2), $f_number);
	$c01++;

	$sheet->write($row_index, $c01,number_format($list_totals["total_space"], 2), $f_number);
	$c01++;
	$sheet->write($row_index, $c01,number_format($list_totals["retail_area"], 2), $f_number);
	$c01++;
	$sheet->write($row_index, $c01,number_format($list_totals["back_office"], 2), $f_number);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;
	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;
	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;
	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;

	$c01 = 9;
	$row_index++;
	$sheet->write($row_index, $c01,"Average", $f_normal);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["intagibles"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	
	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["turnoverbased_rental_costs"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["additional_rental_costs"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;


	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["annual_rental_costs"]/$num_of_projects, 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["lease_period"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;


	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["gross_space"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;


	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["total_space"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["retail_area"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($num_of_projects > 0)
	{
		$avg = round($list_totals["back_office"]/$num_of_projects, 2);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;
	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;
	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;
	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;


	$c01 = 9;
	$row_index++;
	$row_index++;
	$sheet->write($row_index, $c01,"Average per sqm", $f_normal);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["intagibles"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	
	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["turnoverbased_rental_costs"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;

	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["additional_rental_costs"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;


	$avg = 0;
	if($list_totals["retail_area"] > 0)
	{
		$avg = round($list_totals["annual_rental_costs"]/$list_totals["retail_area"], 0);
	}
	$sheet->write($row_index, $c01,$avg, $f_number);
	$c01++;


	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;
	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;
	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;
	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;
	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;
	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;
	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;
	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;
	$sheet->write($row_index, $c01,"", $f_unused);
	$c01++;
}
$xls->close(); 

?>