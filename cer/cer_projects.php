<?php
/********************************************************************

    welcome.php

    Entry page for the cer section.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-26
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-26
    Version:        1.1.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("has_access_to_cer");
/********************************************************************
prepare all data needed
*********************************************************************/
// create sql for the list


$sub_select_filter = " (select count(posproject_type_id) " . 
              "from posproject_types " . 
			  "where posproject_type_postype = postype_id " . 
			  " and posproject_type_projectcosttype = project_costtype_id " . 
			  " and posproject_type_projectkind = project_projectkind " . 
			  " and (posproject_type_needs_cer = 1 or posproject_type_needs_af = 1 or posproject_type_needs_inr03 = 1)) > 0 ";


$sql = "select distinct project_id, project_number, project_order, " .
	   "left(projects.date_created, 10), ".
	   "product_line_name, postype_name, " . 
	   "concat(order_shop_address_place,', ', order_shop_address_company), country_name, ".
	   "    concat(user_name,' ',user_firstname), ".
	   "    order_id, order_actual_order_state_code, project_costtype_text, projectkind_code, ".
	   "    cer_basicdata_submitted " . 
	   "from projects ".
	   "left join orders on project_order = order_id ".
	   "left join project_costs on project_cost_order = order_id " .
	   "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join product_lines on project_product_line = product_line_id ".
	   "left join projectkinds on projectkind_id = project_projectkind ".
	   "left join postypes on postype_id = project_postype ".
	   "left join countries on order_shop_address_country = countries.country_id ".
	   "left join users on project_retail_coordinator = users.user_id " . 
	   "left join cer_basicdata on cer_basicdata_project = project_id";


$list_filter = $sub_select_filter . " and order_actual_order_state_code <= '820' " . 
			   "and (order_archive_date = '0000-00-00' or order_archive_date is null) and (cer_basicdata_version = 0 or cer_basicdata_version is null) ";


$list_filter = $sub_select_filter . " and order_actual_order_state_code <= '820' " . 
			   "and (order_archive_date = '0000-00-00' or order_archive_date is null) and (cer_basicdata_version = 0 or cer_basicdata_version is null) and project_product_line > 0";

if(param("y1") > 0)
{
	$list_filter .= "    and left(order_date,4) >= " . param("y1");
	register_param("y1");
}
if(param("y2") > 0)
{
	$list_filter .= "    and left(order_date,4) <= " . param("y2");
	register_param("y2");
}

if(param("p") > 0)
{
	$list_filter .= "    and product_line_id = " . param("p");
	register_param("p");
}

if(param("c") > 0)
{
	$list_filter .= "    and country_id = " . param("c");
	register_param("c");
}

if(param("pn") != 0)
{
	$list_filter .= "    and order_number like '%" . param("pn") . "%' ";
	register_param("pn");
}


$country_filter = "";
$tmp = array();
$sql_c = "select * from country_access " .
		 "where country_access_user = " . user_id();


$res_c = mysql_query($sql_c) or dberror($sql_c);

while ($row_c = mysql_fetch_assoc($res_c))
{            
	$tmp[] = $row_c["country_access_country"];
}

if(count($tmp) > 0) {
	$country_filter = " country_id IN (" . implode(",", $tmp) . ") ";
}

if(has_access("has_full_access_to_cer"))
{
	$milestones = array();

	$sql_m = "select project_id, max(milestone_code) as milsetone " . 
			 "from projects " .
		     "inner join orders on project_order = order_id ".
		     "left join project_milestones on project_milestone_project = project_id " .
		     "left join milestones on milestone_id = project_milestone_milestone " .
			 "where order_actual_order_state_code <= '900' " . 
			 "and (order_archive_date = '0000-00-00' or order_archive_date is null) " . 
		     "and milestone_use_in_CER = 1 " . 
		     "and project_milestone_date is not NULL and project_milestone_date <> '0000-00-00'" . 
		     "group by project_id ";

	$res = mysql_query($sql_m) or dberror($sql_m);
	while ($row = mysql_fetch_assoc($res))
	{
		
		$link = "<a href=\"#\" onCLick=\"javascript:popup('cer_show_milestones.php?pid=" . $row["project_id"] . "', 800, 600)\">" . $row["milsetone"] . "</>";
		$milestones[$row["project_id"]] = $link;
	}
}
elseif(has_access("cer_has_full_access_to_his_projects"))
{
	$user_data = get_user(user_id());
	$list_filter .= " and order_client_address = " . $user_data["address"] . " ";

}
elseif(has_access("has_access_to_his_cer") or has_access("can_view_his_cer_data"))
{

	

	
	$user_data = get_user(user_id());

	$user_roles = get_user_roles(user_id());
	$order_list = get_user_specific_order_list(user_id(), $user_roles = array());
	if($order_list == "()")
	{
		$list_filter .= " and (project_order = 0) ";
	}
	else
	{
		$list_filter .= " and (project_order IN " . $order_list . ") ";
	}
}

if(has_access("has_access_to_retail_only")) {
	$list_filter .= " and project_cost_type in (1) ";
}

if(has_access("has_access_to_wholesale")) {
	$list_filter .= " and project_cost_type in (2, 6) ";
}

if(has_access("has_access_only_to_human_resources"))
{
	$list_filter .= " and project_cost_type in (1, 2) ";
}

if($country_filter) 
{
	if(has_access("has_access_to_retail_only")) {
		$country_filter .= " and project_cost_type in (1) ";
	}

	if(has_access("has_access_to_wholesale")) {
		$country_filter .= " and project_cost_type in (2, 6) ";
	}

	if(has_access("has_access_only_to_human_resources"))
	{
		$country_filter .= " and project_cost_type in (1, 2) ";
	}
	
	$list_filter = $list_filter . " or (" . $country_filter . " and " .  $sub_select_filter . " and order_actual_order_state_code <= '820' and (order_archive_date = '0000-00-00' or order_archive_date is null) and (cer_basicdata_version = 0  or cer_basicdata_version is null))";
}





//image column
if(!param("show_project"))
{
	$submission_states = array();
	$sql_s = $sql . " where " . $list_filter;
	$res = mysql_query($sql_s) or dberror($sql_s);
	while($row = mysql_fetch_assoc($res))
	{
		$ok = true;
		if($row["cer_basicdata_submitted"] == '0000-00-00' or $row["cer_basicdata_submitted"] == NULL)
		{
			$ok = false;
		}
		
		if($ok == true)
		{
			$submission_states[$row["project_id"]] = "/pictures/ok.gif";
		}
		else
		{
			$submission_states[$row["project_id"]] = "/pictures/not_ok.gif";
		}
	}
}


//count projects
 $sql_count = "select count(project_id) as num_recs ".
			   "from projects ".
			   "inner join orders on project_order = order_id ".
			   "left join project_costs on project_cost_order = order_id " .
			   "left join project_costtypes on project_costtype_id = project_cost_type " .
			   "left join product_lines on project_product_line = product_line_id ".
			   "left join postypes on postype_id = project_postype ".
			   "inner join countries on order_shop_address_country = countries.country_id ".
			   "left join users on project_retail_coordinator = users.user_id " .
			    "left join cer_basicdata on cer_basicdata_project = project_id";


$sql_count = $sql_count . " where " . $list_filter;
$res = mysql_query($sql_count);
$row = mysql_fetch_assoc($res);


if($row["num_recs"] == 1) {
	
	
	$sql_count = "select project_id ".
			   "from projects ".
			   "inner join orders on project_order = order_id ".
			   "left join project_costs on project_cost_order = order_id " .
			   "left join project_costtypes on project_costtype_id = project_cost_type " .
			   "left join product_lines on project_product_line = product_line_id ".
			   "left join postypes on postype_id = project_postype ".
			   "inner join countries on order_shop_address_country = countries.country_id ".
			   "left join users on project_retail_coordinator = users.user_id "  .
		       "left join cer_basicdata on cer_basicdata_project = project_id";

	$sql_count = $sql_count . " where " . $list_filter;
	$res = mysql_query($sql_count);
	$row = mysql_fetch_assoc($res);


	$link = "cer_project.php?pid=" . $row["project_id"] . "&id=" . $row["project_id"];
	redirect($link);
}

/********************************************************************
	Create List
*********************************************************************/ 
$list = new ListView($sql);

$list->set_entity("projects");
$list->set_order("order_actual_order_state_code ASC, left(projects.date_created, 10) desc");
$list->set_filter($list_filter);   

if(has_access("can_only_edit_staff_in_cer")
	or has_access("has_access_only_to_human_resources"))
{
	$list->add_column("project_number", "Project No.", "cer_application_salaries.php?pid={project_id}", "", "", COLUMN_NO_WRAP);
}
else
{
	$list->add_column("project_number", "Project No.", "cer_project.php?pid={project_id}", "", "", COLUMN_NO_WRAP);
}


if(has_access("has_full_access_to_cer"))
{
	$list->add_text_column("milestone", "Milestone", COLUMN_UNDERSTAND_HTML | COLUMN_ALIGN_RIGHT, $milestones);
}

if(!param("show_project"))
{
	$list->add_image_column("", "", 0, $submission_states);
}

$list->add_column("project_costtype_text", "Legal Type", "", 0, "", COLUMN_NO_WRAP);
$list->add_column("projectkind_code", "Kind", "", 0, "", COLUMN_NO_WRAP);

$list->add_column("product_line_name", "Product Line", "", 0, "", COLUMN_NO_WRAP);
$list->add_column("postype_name", "POS Type", "", 0, "", COLUMN_NO_WRAP);

$list->add_column("order_actual_order_state_code", "Project\nStatus", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK);
$list->add_column("left(projects.date_created, 10)", "Submitted", "", "", "", COLUMN_NO_WRAP);
$list->add_column("country_name", "Country", "", 0, "");
$list->add_column("concat(order_shop_address_place,', ', order_shop_address_company)", "POS Address");
$list->add_column("concat(user_name,' ',user_firstname)", "Project Manager", "", 0, "");

$list->add_hidden("show_project", 1);

$list->add_button("back", "Back");

/********************************************************************
	Populate list and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();

if($list->button("back"))
{
	redirect("welcome.php");
}

/********************************************************************
	render page
*********************************************************************/ 
$page = new Page("cer_projects");

$page->header();
$page->title("CER/AF: Projects");
$list->render();
$page->footer();

?>