<?php
/********************************************************************

    cer_inr02_pdf_detail.php

    Print PDF for CER Business Plan (Form IN-R02)

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*********************************************************************/
include("include/in_project_data.php");
include("include/in_financial_data.php");	

//set pdf parameters

$margin_top = 5;
$margin_left = 12;
$y = $margin_top;
$standard_y = 3.5;

$pdf->SetTitle("Retail Busines Plan Overview Form IN-R02");
$pdf->SetAuthor(BRAND . "Retailnet");
$pdf->SetDisplayMode(110);
$pdf->AddPage("L", "A4");
$pdf->SetAutoPageBreak(false, 0);

//page data

include "include/inR02_page_top_data.php";


$years_page1 = array();
$j= 1;
foreach($years as $key=>$year)
{
	if($j < 6)
	{
		$years_page1[] = $years[$key];
	}
	$j++;
}

include "include/inR02_financial_data.php";	
include "include/inR02_page_bottom_data.php";

if(count($years) > 5)
{
	$pdf->AddPage("L", "A4");
	include "include/inR02_page_top_data.php";

	$years_page1 = array();
	$j= 1;
	foreach($years as $key=>$year)
	{
		if($j > 5 and $j < 11)
		{
			$years_page1[] = $years[$key];
		}
		$j++;
	}

	include "include/inR02_financial_data.php";	
	include "include/inR02_page_bottom_data.php";
}

if(count($years) > 10)
{
	$pdf->AddPage("L", "A4");
	include "include/inR02_page_top_data.php";

	$years_page1 = array();
	$j= 1;
	foreach($years as $key=>$year)
	{
		if($j > 10 and $j < 16)
		{
			$years_page1[] = $years[$key];
		}
		$j++;
	}

	include "include/inR02_financial_data.php";	
	include "include/inR02_page_bottom_data.php";
}

if(count($years) > 15)
{
	$pdf->AddPage("L", "A4");
	include "include/inR02_page_top_data.php";

	$years_page1 = array();
	$j= 1;
	foreach($years as $key=>$year)
	{
		if($j > 15 and $j < 21)
		{
			$years_page1[] = $years[$key];
		}
		$j++;
	}

	include "include/inR02_financial_data.php";	
	include "include/inR02_page_bottom_data.php";
}

if(count($years) > 20)
{
	$pdf->AddPage("L", "A4");
	include "include/inR02_page_top_data.php";

	$years_page1 = array();
	$j= 1;
	foreach($years as $key=>$year)
	{
		if($j > 20 and $j < 22)
		{
			$years_page1[] = $years[$key];
		}
		$j++;
	}

	include "include/inR02_financial_data.php";	
	include "include/inR02_page_bottom_data.php";
}


?>