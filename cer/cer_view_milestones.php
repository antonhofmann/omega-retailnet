<?php
/********************************************************************

    cer_view_milestones.php

    View MileStones of the Projecs

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-12-12
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-12-12
    Version:        1.0.

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";

check_access("can_view_milestones");

register_param("pid");

/********************************************************************
    prepare all data needed
*********************************************************************/

$client_address = get_address($project["order_client_address"]);

$client = $client_address["company"] . ", " .
		  $client_address["zip"] . " " .
          $client_address["place"] . ", " .
          $client_address["country_name"];

$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_address"] . ", " .
		$project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];



/********************************************************************
    Create Project Milestones
*********************************************************************/ 
    
$sql = "select * from milestones " .
	   "where milestone_active = 1 " .
	   "order by milestone_code ";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$sql_m = "select count(project_milestone_id) as num_recs from project_milestones " .
		     "where project_milestone_project = " . $project["project_id"] . 
	         "   and project_milestone_milestone = " . $row["milestone_id"];
	
	
	$res_m = mysql_query($sql_m) or dberror($sql_m);
	$row_m = mysql_fetch_assoc($res_m);

	if($row_m["num_recs"] == 0)
	{
		$fields = array();
		$values = array();

		$fields[] = "project_milestone_project";
		$values[] = $project["project_id"];

		$fields[] = "project_milestone_milestone";
		$values[] = $row["milestone_id"];
		
		$fields[] = "date_created";
		$values[] = dbquote(date("Y-m-d"));

		$fields[] = "date_modified";
		$values[] = dbquote(date("Y-m-d"));

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$fields[] = "user_modified";
		$values[] = dbquote(user_login());

		$sql = "insert into project_milestones (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

		mysql_query($sql) or dberror($sql);
	}
}

/********************************************************************
    Caclucalte dates and diffeneces in dates
*********************************************************************/
$dates = array();
$comments = array();
$datecomments = array();
$daysconsumed = array();
$daysaccumulated = array();
$lastdate = "";
$milestone_turnaround = "";


//get trunaround
$date_min = "0000-00-00";
$sql_m = "select min(project_milestone_date) as date_min " .
         "from project_milestones " .
         " where project_milestone_date <> '0000-00-00' " . 
		 " and project_milestone_date is not NULL " . 
		 " and project_milestone_project = " . $project["project_id"];
$res = mysql_query($sql_m) or dberror($sql_m);
if($row = mysql_fetch_assoc($res))
{
	$date_min = $row["date_min"];
}

$date_max = "0000-00-00";
$sql_m = "select max(project_milestone_date) as date_max " . 
         "from project_milestones " .
         " where project_milestone_date <> '0000-00-00' " . 
		 " and project_milestone_date is not NULL " . 
		 " and project_milestone_project = " . $project["project_id"];
$res = mysql_query($sql_m) or dberror($sql_m);
if($row = mysql_fetch_assoc($res))
{
	$date_max = $row["date_max"];
}

$milestone_turnaround = floor((strtotime($date_max) - strtotime($date_min)) / 86400);


$sql = "select project_milestone_id, project_milestone_date, project_milestone_date_comment, " .
       "project_milestone_comment, milestone_code, milestone_text " . 
       "from project_milestones " .
       " left join milestones on milestone_id = project_milestone_milestone ";

$list_filter = "project_milestone_project = " . $project["project_id"];


$sql_m = $sql . " where " . $list_filter . " order by milestone_code ";

$res = mysql_query($sql_m) or dberror($sql_m);

while($row = mysql_fetch_assoc($res))
{
	$dates[$row["project_milestone_id"]] = to_system_date($row["project_milestone_date"]);
	$datecomments[$row["project_milestone_id"]] = $row["project_milestone_date_comment"];
    $comments[$row["project_milestone_id"]] = $row["project_milestone_comment"];

	
	if($lastdate != "0000-00-00" and $lastdate != NULL and $row["project_milestone_date"] != "0000-00-00" and $row["project_milestone_date"] != NULL)
	{
		$thisdate = $row["project_milestone_date"];
		$difference_in_days = floor((strtotime($thisdate) - strtotime($date_min)) / 86400);
		
		
		
		$daysconsumed[$row["project_milestone_id"]] = $difference_in_days;
		
		
		//$milestone_turnaround = $milestone_turnaround + $difference_in_days;
		//$daysaccumulated[$row["project_milestone_id"]] = $milestone_turnaround;
	}

	if($row["project_milestone_date"] != "0000-00-00" and $row["project_milestone_date"] != NULL)
	{
		$lastdate = $row["project_milestone_date"];
	}
}



/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project");

$form->add_label("shop_address", "POS Location", RENDER_HTML, $shop);
$form->add_label("client_address", "Client", RENDER_HTML, $client);
include("include/project_head.php");

$form->add_hidden("pid", param("pid"));

$form->add_section(" ");
$form->add_label("start", "Project Starting Date", 0, to_system_date($project["order_date"]));

if($project["project_projectkind"] != 4 or $project["project_projectkind"] != 5) //Take Over and lease renewal
{
	$form->add_label("planned_opening_date", "Client's preferred Shop Opening Date", 0, to_system_date($project["project_planned_opening_date"]));
}

$form->add_label("real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]));
$form->add_label("actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]));


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();

/********************************************************************
    Create List for Milestones
*********************************************************************/ 
$list = new ListView($sql);

$list->set_title("Milestones");
$list->set_entity("project_milestones");
$list->set_filter($list_filter);
$list->set_order("milestone_code");


$list->add_column("milestone_code", "Code", "", "", "", COLUMN_NO_WRAP);   
$list->add_column("milestone_text", "Description", "", "", "", COLUMN_NO_WRAP);



$list->add_text_column("project_milestone_date", "Date", 0, $dates);
$list->add_text_column("daysconsumed", "Days", COLUMN_ALIGN_RIGHT, $daysconsumed);
//$list->add_text_column("daysaccumulated", "Sum", COLUMN_ALIGN_RIGHT, $daysaccumulated);
$list->add_text_column("project_milestone_date_comment", "Date Comment", 0, $datecomments);
$list->add_text_column("project_milestone_comment", "General Comment", 0, $comments);


$list->populate();
$list->process();



/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");

require "include/project_page_actions.php";

$page->header();

if($form_type == "INR03")
{
	$page->title("INR-03 - Retail Furniture in Third-party Store: Project Milestones");
}
elseif($form_type == "AF")
{
	$page->title("Application Form: Project Milestones");
}
else
{
	$page->title("Capital Expenditure Request: Project Milestones");
}

$form->render();

echo "<br>";
$list->render();

$page->footer();

?>