<?php
/********************************************************************

    cer_mail_new.php

    Mail Box: Send new Mail

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";

check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}

set_referer("cer_mail_new.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
$client_address = get_address($project["order_client_address"]);


$client = $client_address["company"] . ", " .
		  $client_address["zip"] . " " .
          $client_address["place"] . ", " .
          $client_address["country_name"];

$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_address"] . ", " .
		$project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];


//get reciepients of emails
//$reciepients = get_address_users($client_address["id"], $project["project_retail_coordinator"], $project["order_retail_operator"], $project["project_local_retail_coordinator"], $project["project_hq_project_manager"]); 

$reciepients = get_address_users($client_address["id"], $project["project_retail_coordinator"], $project["order_retail_operator"], $project["project_local_retail_coordinator"], 0, $project["order_user"], $project["project_design_supervisor"]); 

$sender = get_user(user_id());
$reciepients[] = array("name" => $sender["firstname"] . " " . $sender["name"], "email" => strtolower($sender["email"]), "roles" => "Copy to me");



/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_mails", "cer_mails");

$form->add_label("shop_address", "POS Location", RENDER_HTML, $shop);
$form->add_label("client_address", "Client", RENDER_HTML, $client);
include("include/project_head.php");

$form->add_hidden("pid", param("pid"));

$form->add_section("Mail Message");
$form->add_edit("cer_mail_group", "Topic*", NOTNULL);
$form->add_multiline("text", "Message*", 8, NOTNULL);

$form->add_section("Attachments");
$form->add_edit("cer_mail_attachment1_title", "File Title");
$form->add_upload("cer_mail_attachment1_path", "Attachment", "/files/cer/". $project["order_number"]);

$form->add_section("Recipients*");
foreach($reciepients as $key=>$user)
{
	if($user["roles"] == 'Finance Controller')
	{
		$form->add_checkbox($user["email"], $user["name"], 1, DISABLED, $user["roles"]);
	}
	else
	{
		$form->add_checkbox($user["email"], $user["name"], "", "", $user["roles"]);
	}
}

$form->add_section("CC Recipients");
$form->add_modal_selector("ccmails", "Selected Recipients", 8);

$form->add_button("send", "Send Mail");
$form->add_button("back", "Back");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if($form->button("back"))
{
	$link = "cer_mails.php?pid=" . param("pid");
	redirect($link);
}
elseif($form->button("send"))
{

	$reciepient_selected = 0;
	$selected_reciepients = array();
	$selected_ccreciepients = array();
	

	foreach($reciepients as $key=>$user)
	{
		if($form->value($user["email"]) == 1 or $user["roles"] == "Finance Controller")
		{
			$reciepient_selected = 1;
			$selected_reciepients[strtolower($user["email"])] = strtolower($user["email"]);
		}
	}

	if($reciepient_selected == 0)
	{
		$form->error("You must select at least one reciepient.");
	}

	
	if($form->validate() and $reciepient_selected == 1)
	{
		
		//get all ccmails
		$ccmails =  explode(' ', preg_replace("'\r?\n'"," ",$form->value('ccmails'))); 
		foreach($ccmails as $ccmail) {
			if(is_email_address($ccmail)) {
				
				$selected_ccreciepients[strtolower($ccmail)] = strtolower($ccmail);
			}
		
		}

		
		
		if($form_type == "INR03")
		{
			$subject = MAIL_SUBJECT_PREFIX . ": " . $form->value("cer_mail_group") . " - Project " . $project["order_number"] . ": INR-03 for " . $project["order_shop_address_company"];
		}
		elseif($form_type == "AF")
		{
			$subject = MAIL_SUBJECT_PREFIX . ": " . $form->value("cer_mail_group") . " - Project " . $project["order_number"] . ": AF for " . $project["order_shop_address_company"];
		}
		else
		{
			$subject = MAIL_SUBJECT_PREFIX . ": " . $form->value("cer_mail_group") . " - Project " . $project["order_number"] . ": CER for " . $project["order_shop_address_company"];
		}


		$text = str_replace("\r\n", "\n", trim($form->value("text")));

		foreach($selected_reciepients as $key=>$email)
		{
			
			$sql = "select user_email_cc, user_email_deputy " . 
			       "from users " . 
				   "where user_email = " . dbquote($email);

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				if($row["user_email_cc"])
				{
					$selected_ccreciepients[strtolower($row["user_email_cc"])] = strtolower($row["user_email_cc"]);
				}
				if($row["user_email_deputy"])
				{
					$selected_ccreciepients[strtolower($row["user_email_deputy"])] = strtolower($row["user_email_deputy"]);
				}
			}
		}
	

		$mail = new PHPMailer();
		$mail->Subject = $subject;
		$mail->SetFrom($sender["email"], $sender["firstname"] . " " . $sender["name"]);
		$mail->AddReplyTo($sender["email"], $sender["firstname"] . " " . $sender["name"]);

		$bodytext0 = str_replace("\r\n", "\n", $text) . "\n\n";
		$link ="cer_project.php?pid=" . param("pid");
		$bodytext = $bodytext0 . "Click below to have direct access to the project:\n";
		$bodytext = $bodytext .  APPLICATION_URL ."/cer/" . $link . "\n\n";           
		$mail->Body = $bodytext;

		if($form->value("cer_mail_attachment1_path"))
		{
			

			$file_name = "/files/cer/". $project["order_number"]. "/" . $form->value("cer_mail_attachment1_path");
			
			$filepath = $_SERVER["DOCUMENT_ROOT"] . $form->value("cer_mail_attachment1_path");
			
			$mail->AddAttachment($filepath);

			echo "<br /><br /><br />" . $filepath;
		}

		$mail->AddAddress("anton.hofmann@mediaparx.com");

		/*
		$rcpts = "";
		foreach($selected_reciepients as $key=>$value)
		{
			$mail->AddAddress($value);
			$rcpts .= $value . "\n";
		}


		foreach($selected_ccreciepients as $key=>$value)
		{
			if(!in_array($value, $selected_reciepients)) {
				$mail->AddCC($value);
				$rcpts .= $value . "\n";
			}
		}
		*/

		
		$mail->Send();

		//update mail history
		$fields = array();
		$values = array();

		$fields[] = "cer_mail_project";
		$values[] = param("pid");

		$fields[] = "cer_mail_group";
		$values[] = dbquote($form->value("cer_mail_group"));

		$fields[] = "cer_mail_text";
		$values[] = dbquote($text);

		$fields[] = "cer_mail_sender";
		$values[] = dbquote($sender["firstname"] . " " . $sender["name"]);

		$fields[] = "cer_mail_sender_email";
		$values[] = dbquote($sender["email"]);

		$fields[] = "cer_mail_reciepient";
		$values[] = dbquote($rcpts);

		if($form->value("cer_mail_attachment1_path"))
		{
			$fields[] = "cer_mail_attachment1_title";
			$values[] = dbquote($form->value("cer_mail_attachment1_title"));

			$fields[] = "cer_mail_attachment1_path";
			$values[] = dbquote($form->value("cer_mail_attachment1_path"));
		}
		
		$fields[] = "date_created";
		$values[] = "now()";

		$fields[] = "date_modified";
		$values[] = "now()";

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$fields[] = "user_modified";
		$values[] = dbquote(user_login());

		$sql = "insert into cer_mails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
		
		//mysql_query($sql) or dberror($sql);

		$link = "cer_mails.php?pid=" . param("pid");
		//redirect($link);
	}
}


/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();
if($form_type == "INR03")
{
	$page->title("INR-03 - Retail Furniture in Third-party Store: Send New Mail");
}
elseif($form_type == "AF")
{
	$page->title("Application Form: Send New Mail");
}
else
{
	$page->title("Capital Expenditure Request: Send New Mail");
}
$form->render();


?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#ccmails_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/shared/select_mail_recipients.php'
    });
    return false;
  });
});
</script>

<?php

$page->footer();

?>