<?php
/********************************************************************

    cer_application_franchisee.php

    Application Form: franchisee information
    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";

check_access("has_access_to_cer");

if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}

/********************************************************************
    prepare all data needed
*********************************************************************/
//$client_address = get_address($project["order_client_address"]);
$posdata = get_pos_data($project["project_order"]);
$franchisee_address = get_address($posdata["posaddress_franchisee_id"]);


//agreemant data

//calculate franchisee agreement duration
if($posdata["posaddress_fagrstart"] != NULL and $posdata["posaddress_fagrstart"] != '0000-00-00')
{
	$starting_date = $posdata["posaddress_fagrstart"];
	$expiry_date = $posdata["posaddress_fagrend"];
}
else
{
	/* no longer needed, since entered manually in the edit POS data dialogue
	$fag_data = get_fag_data($project["project_planned_opening_date"]);
	$starting_date = $fag_data["starting_date"];
	$expiry_date = $fag_data["ending_date"];

	$sql = "update " . $posdata["table"] . " set " .
		   "posaddress_fagrstart = " . dbquote($starting_date) . ", " .
		   "posaddress_fagrend = " . dbquote($expiry_date) .
		   "where posaddress_id = " . $posdata["posaddress_id"];
	
	$result = mysql_query($sql) or dberror($sql);
	*/

	$starting_date = "";
	$expiry_date = "";
}

$duration = "";
$sql = "select IF(TIMESTAMPDIFF(MONTH,posaddress_fagrstart,posaddress_fagrend) >0, TIMESTAMPDIFF(MONTH,posaddress_fagrstart,posaddress_fagrend), 'n.a.') as months from ". $posdata["table"] . " where posaddress_id = " . $posdata["posaddress_id"];

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$duration = $row["months"];
	$duration = floor($duration/12);

	$months = 1 + $row["months"] - $duration*12;


	if($months == 12)
	{
		$duration = $duration + 1 . " years";
	}
	
	elseif($months > 0)
	{
		$duration = $duration . " years and " . (1 + $row["months"] - $duration*12) . " months";
	}
}


/********************************************************************
    build form
*********************************************************************/
$form = new Form("posaddresses", "posaddress");

include("include/project_head.php");


$form->add_hidden("pid", param("pid"));

$form->add_section("Franchisee");

$form->add_label("franchisee_company", "Company", 0, $franchisee_address["company"]);
$form->add_label("franchisee_address", "City", 0, $franchisee_address["zip"] . " " . $franchisee_address["place"]);
$form->add_label("franchisee_country", "Country", 0, $franchisee_address["country_name"]);


$form->add_section("Duration of the Franchisee Agreement");
$form->add_label("end", "Planned POS Opening Date", 0, to_system_date($project["project_planned_opening_date"]));

$form->add_label("posaddress_fagrstart", "Agreement Starting Date", 0, to_system_date($starting_date));
$form->add_label("posaddress_fagrend", "Agreement Ending Date", 0,  to_system_date($expiry_date));
$form->add_label("posaddress_fargrduration", "Agreement Duration", 0, $duration);


$form->add_section("Pament Terms");
$form->add_comment("Please note that the following payment terms are applicable for all franchise projects regarding furniture and installation costs reinvoiced by " . BRAND . ".:");
$form->add_label("p1", "under 50'000 CHF", 0, "60 Days");
$form->add_label("p2", "between 50'000 - 100'000 CHF", 0, "90 Days");
$form->add_label("p3", "over 100'000 CHF", 0, "180 Days");

$form->add_comment("Special payment terms for store projects exceeding CHF 100'000 are only possible on special request and need prior discussion with and written approval from the " . BRAND . " finance department:");

$form->add_edit("cer_basicdata_specialterms", "Approved special terms", 0, $cer_basicdata["cer_basicdata_specialterms"]);

$form->add_button("form_save", "Save Data");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if($form->button("form_save"))
{
	if($form->validate())
	{
		//update basicdata

		$fields = array();
		
		$value = dbquote($form->value("cer_basicdata_specialterms"));
		$fields[] = "cer_basicdata_specialterms = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_basicdata set " . join(", ", $fields) . " where cer_basicdata_version = 0 and cer_basicdata_project = " . param("pid");
		mysql_query($sql) or dberror($sql);

		$form->message("Your data has bee saved.");

	
	}

}
  
/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();
$page->title("Application Form: Agreement and Payment Terms");
require_once("include/tabs.php");
$form->render();

require "include/footer_scripts.php";
$page->footer();

?>