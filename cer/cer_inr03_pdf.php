<?php
/********************************************************************

    cer_inr03_pdf.php

    Print PDF for Request Retail Furniture for Third-Party POS (Form INR-03).

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2016-08-12
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2016-08-12
    Version:        1.0.0

    Copyright (c) 2016, Swatch AG, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();

$SUPPRESS_HEADERS = true;
define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 


require "../include/frame.php";


$cer_version = 0;
$ln_version = 0;
if(param("cerversion"))
{
	$cer_version = param("cerversion");
}
if(param("lnversion"))
{
	$ln_version = param("lnversion");
}


require "include/get_functions.php";
require "include/get_project.php";
require "../shared/func_posindex.php";
require "include/financial_functions.php";
require_once "../shared/project_cost_functions.php";

check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}


require("include/cer_get_data_from_project.php");

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');
require_once('../include/fpdi/fpdi.php'); 



// Create and setup PDF document

$pdf = new FPDI("P", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 23, 10);
$pdf->setPrintHeader(false);


$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');
$pdf->Open();


//financial summary
include("cer_inr03_pdf_detail.php");


$file_name = BRAND . "_INR-03_" . $project["project_number"] . ".pdf";
$pdf->Output($file_name);

?>