<?php
/********************************************************************

    ln_from.pfd.php

    Print Lease Negotiation Form

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-11-06
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-11-06
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";

$cer_version = 0;
$ln_version = 0;

if(param("cerversion"))
{
	$cer_version = param("cerversion");
}
if(param("lnversion"))
{
	$ln_version = param("lnversion");
}

require_once "include/get_functions.php";
require "../shared/func_posindex.php";
require "include/financial_functions.php";
require "include/get_project.php";

check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}


$contract_starting_date = "";
$contract_ending_date = "";


/********************************************************************
    prepare all data needed
*********************************************************************/
if($ln_basicdata["ln_basicdata_factor"] == 0) {$ln_basicdata["ln_basicdata_factor"] = 1;}
$currency_symbol = get_currency_symbol($ln_basicdata["ln_basicdata_currency"]);

// get all data needed from project
$sql = "select * " . 
       "from projects " . 
	   "left join orders on order_id = project_order " .
	   "left join countries on country_id = order_shop_address_country " .
	   "left join product_lines on product_line_id = project_product_line " .
	   "left join productline_subclasses on productline_subclass_id = project_product_line_subclass " .
	   "left join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join projectkinds on projectkind_id = project_projectkind " .
       "left join addresses on address_id = order_client_address " .
	   "left join currencies on currency_id = country_currency " . 
       "left join postypes on postype_id = project_postype ".
	   "left join possubclasses on possubclass_id = project_pos_subclass ".
	   "left join ln_basicdata on ln_basicdata_project = project_id ".
	   "left join floors on floor_id = project_floor " . 
	   "where ln_basicdata_version = " . $ln_version . " and project_id = " . param("pid");


$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	$order_number = $row["project_order"];
	//POS Basic Data
	$posname = $row["order_shop_address_company"];

	if($row["order_shop_address_company2"])
    {
		$posname .= ", " . $row["order_shop_address_company2"];
	}

	
	$posaddress = $row["country_name"];
	$country_name = $row["country_name"];

	if($row["order_shop_address_zip"])
    {
		$posaddress .= ", " . $row["order_shop_address_zip"] . " " . $row["order_shop_address_place"];
	}
	else
	{
		$posaddress .= ", " . $row["order_shop_address_place"];
	}

	$city_name = $row["order_shop_address_place"];

	$posaddress .= ", " . $row["order_shop_address_address"];
	
	if($row["order_shop_address_address2"])
    {
		$posaddress .= ", " . $row["order_shop_address_address2"];
	}
	
	$planned_opening_date = to_system_date($row["project_real_opening_date"]);
	//$planned_opening_date = to_system_date($row["project_planned_opening_date"]);
	$sales_surface = $row["project_cost_sqms"];
	$total_surface = $row["project_cost_totalsqms"];
	$other_surface = $row["project_cost_othersqms"] + $row["project_cost_backofficesqms"];

	$page_title = "Lease Negotiation: " . $row["project_number"];
	$page_title = "Lease Negotiation ";

	
	if($relocated_pos) {

	$postype = "Relocation" . ", " . $project["project_costtype_text"] . ", " . $project["postype_name"] . ", " . $project["possubclass_name"];

		if($row["productline_subclass_name"])
		{
			$postype = "Relocation" . " - " . $row["project_costtype_text"] . " " . $row["postype_name"] . " - " . $row["product_line_name"] . " / " . $row["productline_subclass_name"];
		
		}
		else {
			$postype = "Relocation" . " - " . $row["project_costtype_text"] . " " . $row["postype_name"] . " - " . $row["product_line_name"];
		}
	}
	else {
		if($row["productline_subclass_name"])
		{
			$postype = $row["projectkind_name"] . " - " . $row["project_costtype_text"] . " " . $row["postype_name"] . " - " . $row["product_line_name"] . " / " . $row["productline_subclass_name"];
		
		}
		else {
			$postype = $row["projectkind_name"] . " - " . $row["project_costtype_text"] . " " . $row["postype_name"] . " - " . $row["product_line_name"];
		}
	}
	
	
	

	$postype = $row["project_costtype_text"] . " " . $row["postype_name"] . " " . $row["possubclass_name"];

	$project_kind = $row["projectkind_name"];

	$placement = $row["floor_name"];

	$deadline_for_property = to_system_date($cer_basicdata["cer_basicdata_deadline_property"]);

	//get keymoney and deposit
	$tmp = get_pos_intangibles(param("pid"), 15, $cer_version);
	if(count($tmp) > 0)
	{
		$keymoney = $tmp ["cer_investment_amount_cer_loc"];
		$keymoney = $keymoney*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
		$keymoney = round($keymoney/1000, 0);
		

		$tmp = get_pos_intangibles(param("pid"), 9, $cer_version);
		$depositposted = $tmp ["cer_investment_amount_cer_loc"];
		$depositposted = $depositposted*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
		$depositposted = round($depositposted/1000, 0);
	}
	else
	{
		$keymoney = "";
		$depositposted = "";
	}

	//get rents
	$first_full_year = $cer_basicdata["cer_basicdata_firstyear"];
	if($cer_basicdata["cer_basicdata_firstmonth"] > 1)
	{
		$first_full_year = $cer_basicdata["cer_basicdata_firstyear"]+1;
	}

	$last_full_year = $cer_basicdata["cer_basicdata_lastyear"];
	if($cer_basicdata["cer_basicdata_lastmonth"] < 12)
	{
		$last_full_year = $cer_basicdata["cer_basicdata_lastyear"]-1;
	}

	$fullrent_firstyear = 0;
	$fixedrent_firstyear = 0;
	$turnoverrent_firstyear = 0;
	$total_rent_first_year = 0;

	if($first_full_year > 0 and $cer_basicdata["cer_basicdata_firstyear"] < $cer_basicdata["cer_basicdata_lastyear"])
	{
		$sql_e = "select sum(cer_expense_amount) as cer_expense_amount from cer_expenses " . 
			   "where cer_expense_cer_version = " . $cer_version . " and cer_expense_project = " . param("pid") . 
			   " and cer_expense_type in (2,20) " . 
			   " and cer_expense_year = " . dbquote($first_full_year);

		$res_e = mysql_query($sql_e) or dberror($sql_e);
		if ($row_e = mysql_fetch_assoc($res_e))
		{
			$fixedrent_firstyear = $row_e["cer_expense_amount"];
		}

		$sql_e = "select cer_expense_amount from cer_expenses " . 
			   "where cer_expense_cer_version = " . $cer_version . " and cer_expense_project = " . param("pid") . 
			   " and cer_expense_type = 16 " . 
			   " and cer_expense_year = " . dbquote($first_full_year);

		$res_e = mysql_query($sql_e) or dberror($sql_e);
		if ($row_e = mysql_fetch_assoc($res_e))
		{
			$turnoverrent_firstyear = $row_e["cer_expense_amount"];
		}
	}
	elseif($cer_basicdata["cer_basicdata_firstyear"] == $cer_basicdata["cer_basicdata_lastyear"])
	{
		$sql_e = "select sum(cer_expense_amount) as cer_expense_amount from cer_expenses " . 
			   "where cer_expense_cer_version = " . $cer_version . " and cer_expense_project = " . param("pid") . 
			   " and cer_expense_type in (2,20) " . 
			   " and cer_expense_year = " . dbquote($cer_basicdata["cer_basicdata_firstyear"]);

		$res_e = mysql_query($sql_e) or dberror($sql_e);
		if ($row_e = mysql_fetch_assoc($res_e))
		{
			$fixedrent_firstyear = $row_e["cer_expense_amount"];
		}

		$sql_e = "select cer_expense_amount from cer_expenses " . 
			   "where cer_expense_cer_version = " . $cer_version . " and cer_expense_project = " . param("pid") . 
			   " and cer_expense_type = 16 " . 
			   " and cer_expense_year = " . dbquote($cer_basicdata["cer_basicdata_firstyear"]);


		$res_e = mysql_query($sql_e) or dberror($sql_e);
		if ($row_e = mysql_fetch_assoc($res_e))
		{
			$turnoverrent_firstyear = $row_e["cer_expense_amount"];
		}
	}

	$fullrent_firstyear = $fixedrent_firstyear + $turnoverrent_firstyear;

	$fullrent_firstyear = $fullrent_firstyear*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
	$fullrent_firstyear = round($fullrent_firstyear/1000, 0);

	$turnoverrent_firstyear = $turnoverrent_firstyear*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
	$turnoverrent_firstyear = round($turnoverrent_firstyear/1000, 0);
	
	$total_rent_first_year = $fixedrent_firstyear + $turnoverrent_firstyear;

	/*
	$fullrent_lastyear = 0;
	$fixedrent_lastyear = 0;
	$turnoverrent_lastyear = 0;

	if($last_full_year > 0)
	{
		$sql_e = "select sum(cer_expense_amount) as cer_expense_amount from cer_expenses " . 
			   "where cer_expense_cer_version = " . $cer_version . " and cer_expense_project = " . param("pid") . 
			   " and cer_expense_type in (2,20) " . 
			   " and cer_expense_year = " . $last_full_year;

		$res_e = mysql_query($sql_e) or dberror($sql_e);
		if ($row_e = mysql_fetch_assoc($res_e))
		{
			$fixedrent_lastyear = $row_e["cer_expense_amount"];
		}

		$sql_e = "select cer_expense_amount from cer_expenses " . 
			   "where cer_expense_cer_version = " . $cer_version . " and cer_expense_project = " . param("pid") . 
			   " and cer_expense_type = 16 " . 
			   " and cer_expense_year = " . $last_full_year;

		$res_e = mysql_query($sql_e) or dberror($sql_e);
		if ($row_e = mysql_fetch_assoc($res_e))
		{
			$turnoverrent_lastyear = $row_e["cer_expense_amount"];
		}
	}

	$fullrent_lastyear = $fixedrent_lastyear + $turnoverrent_lastyear;
	*/

	$fullrent_lastyear = $ln_basicdata["ln_basicdata_passedrental"];

	$fullrent_lastyear = $fullrent_lastyear*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
	$fullrent_lastyear = round($fullrent_lastyear/1000, 0);

	//$turnoverrent_lastyear = $turnoverrent_lastyear*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
	//$turnoverrent_lastyear = round($turnoverrent_lastyear/1000, 0);


	
	//Lease Consitions
	$poslease = get_ln_lease_data($project["project_id"], $ln_basicdata["ln_basicdata_id"], $project["pipeline"], $ln_version);

	if(count($poslease) > 0)
	{
		$landlord = $poslease["poslease_landlord_name"];
		$negotiated_rental_conditions = $poslease["poslease_negotiated_conditions"];
		$indexrate = $poslease["poslease_indexrate"] . "%";
		
		$real_estate_fee = $poslease["poslease_realestate_fee"] . "%";

		$free_weeks = $poslease["poslease_freeweeks"];
		$free_months = round($free_weeks/4, 1);


		$average_annual_rent = 0;
		$duration_in_years = 0;
		if($poslease["poslease_startdate"] != NULL 
			and $poslease["poslease_startdate"] != '0000-00-00' 
			and $poslease["poslease_enddate"] != NULL
			and $poslease["poslease_enddate"] != '0000-00-00')
			{
				$diff = abs(strtotime($poslease["poslease_enddate"]) - strtotime($poslease["poslease_startdate"]));
				$years = floor($diff / (365*60*60*24));
				$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));

				if($months == 12)
				{
					$months = 0;
					$years++;
				}

				$rental_duration = $years . " years and " . $months . " months";

				$duration_in_years = $years + ($months/12);

				$contract_starting_date = to_system_date($poslease["poslease_startdate"]);
				$contract_ending_date = to_system_date($poslease["poslease_enddate"]);

				$date1 = date(strtotime($poslease["poslease_startdate"]));
				$date2 = date(strtotime($poslease["poslease_enddate"]));
				
				$difference = $date2 - $date1;
				$rental_duration_in_months = round($difference / 86400 / 30, 1);
		}

			
		if($duration_in_years > 0)
		{
			$total_lease_commitment = 0;
			$sql_cer = "select * from cer_expenses " .
						  "where cer_expense_cer_version = " . $cer_version . " and cer_expense_project = " . param("pid") .
						  " and cer_expense_type IN(2, 3, 16, 18, 19, 20)";


			$res_cer = mysql_query($sql_cer) or dberror($sql_cer);
			while ($row_cer = mysql_fetch_assoc($res_cer))
			{
				$total_lease_commitment = $total_lease_commitment + 1*$row_cer["cer_expense_amount"];
			}


			if($duration_in_years > 0)
			{
				$average_annual_rent = round(($total_lease_commitment / $duration_in_years)/1000, 0);
			}
		}
	}
	else
	{
		$landlord = "";
		$negotiated_rental_conditions = "";
		$indexrate = "";
		$real_estate_fee = "";
		$average_annual_rent = "";
		$duration_in_years = "";
		$total_lease_commitment = "";
		$contract_starting_date = "";
		$contract_ending_date = "";
		$free_weeks = "";
		$free_months = "";
		$rental_duration_in_months = "";
	}


	//get averag increas in fixed rents
	$average_yearly_increase = "";
	$num_of_years = 0;
	$index_rate_total = 0;
	$increase_rate_total = 0;
	$inflation_rate_total = 0;
	$sql_e = "select cer_fixed_rent_index_rate, cer_fixed_rent_increas_rate, cer_fixed_rent_inflation_rate " . 
	         "from cer_fixed_rents " . 
			 "where cer_fixed_rent_cer_version = " . $cer_version " and cer_fixed_rent_project_id = " . param("pid");

	$res = mysql_query($sql_e) or dberror($sql_e);

	while($row = mysql_fetch_assoc($res))
	{
		$num_of_years++;
		$index_rate_total = $index_rate_total + $row["cer_fixed_rent_index_rate"];
		$increase_rate_total = $increase_rate_total + $row["cer_fixed_rent_increas_rate"];
		$inflation_rate_total = $inflation_rate_total + $row["cer_fixed_rent_inflation_rate"];
	}

	if($num_of_years > 0)
	{
		$tmp1 = "";
		$tmp2 = "";
		$tmp3 = "";
		if($index_rate_total > 0)
		{
			$tmp1 = "Index Rate: " . round($index_rate_total/$num_of_years, 2) . " ";
		}
		if($increase_rate_total > 0)
		{
			$tmp2 = "Increas Rate: " . round($increase_rate_total/$num_of_years, 2) . " ";
		}
		if($inflation_rate_total > 0)
		{
			$tmp3 = "Increas Rate: " . round($inflation_rate_total/$num_of_years, 2) . " ";
		}
		$average_yearly_increase = $tmp1 . $tmp2 . $tmp3;

	}



	//additional rental cost and other fees
	//get additional rental costs
	$annual_charges = 0;
	$num_of_years = 0;
	$sql_e = "select * from cer_expenses " .
			 "where cer_expense_cer_version = " . $cer_version . " and cer_expense_project = " . param("pid") . 
			 " and cer_expense_type in (3,18,19, 20, 20) " . 
			 " order by cer_expense_year";
	$res_e = mysql_query($sql_e) or dberror($sql_e);

	while($row_e = mysql_fetch_assoc($res_e))
	{
		$annual_charges = $annual_charges + $row_e["cer_expense_amount"];
		$num_of_years++;
	}

	if($num_of_years > 0)
	{
		$annual_charges = $annual_charges/$num_of_years;
		$annual_charges = 1*round($annual_charges/1000, 0);
	}

	//get areas and neighbourhood information
	$posareas = "";
	if($project["pipeline"] == 0)
	{
		$sql_i = "select * from posareas " . 
				 "left join posareatypes on posareatype_id = posarea_area " .
				 "where posarea_posaddress = " . $project["posaddress_id"];
		
	}
	elseif($project["pipeline"] == 1)
	{
		
		$sql_p = "select posorder_posaddress from posorderspipeline where posorder_order = " . $project["order_id"];
		$res_p = mysql_query($sql_p) or dberror($sql_p);

		if ($row_p = mysql_fetch_assoc($res_p))
		{
			$sql_i = "select * from posareaspipeline " . 
					 "left join posareatypes on posareatype_id = posarea_area " .
					 "where posarea_posaddress = " . $row_p["posorder_posaddress"];
		}
	}
	$res_i = mysql_query($sql_i) or dberror($sql_i);
	while ($row_i = mysql_fetch_assoc($res_i))
	{
		$posareas .= $row_i["posareatype_name"] . ", ";
	}
	$posareas = substr($posareas,0,strlen($posareas)-2);


	//neighbourhood
	$neighbourhoods = array();
	if($project["pipeline"] == 0)
	{
		$sql_e = "select * from posorders where posorder_order = " . $project["order_id"];
	}
	elseif($project["pipeline"] == 1)
	{
		$sql_e = "select * from posorderspipeline where posorder_order = " . $project["order_id"];
	}
	
	$res_e = mysql_query($sql_e) or dberror($sql_e);
	if ($row_e = mysql_fetch_assoc($res_e))
	{
		$neighbourhoods["Shop on left side"] = $row_e["posorder_neighbour_left"];
		$neighbourhoods["Shop on right side"] = $row_e["posorder_neighbour_right"];
		$neighbourhoods["Shop across left side"] = $row_e["posorder_neighbour_acrleft"];
		$neighbourhoods["Shop across right side"] = $row_e["posorder_neighbour_acrright"];
		$neighbourhoods["Other brands in area"] = $row_e["posorder_neighbour_brands"];
	}		



	$ln_remarks = $row["ln_basicdata_remarks"];
	$ln_brands = $row["ln_basicdata_brands"];
	$ln_size_remarks = $row["ln_basicdata_size_remarks"];
	$ln_area = $row["ln_basicdata_area"];
	$ln_rent = $row["ln_basicdata_rent"];
	$ln_availability = $row["ln_basicdata_availability"];

	

	$approvedby_cm = $row["ln_basicdata_approvedby_cm"];
	$approved_by_ekl = $row["ln_basicdata_approved_by_ekl"];

	
	$pix1 = ".." . $row["ln_basicdata_pix1"];
	$pix2 = ".." . $row["ln_basicdata_pix2"];
	$pix3 = ".." . $row["ln_basicdata_pix3"];

	$floor_plan = ".." . $row["ln_basicdata_floorplan"];
	$lease_agreement = ".." . $row["ln_basicdata_draft_aggreement"];


	//get names of roles
	$brand_manager = "";
	$sales_manager = "";
	$vp_sales = "";
	$president = "";

	$sql_u = "select cer_summary_in01_sig03 " . 
		     "from cer_summary " . 
		     "where cer_summary_cer_version = " . $cer_version . " and cer_summary_project = " . param("pid");
	
	$res_u = mysql_query($sql_u) or dberror($sql_u);
	if($row_u = mysql_fetch_assoc($res_u))
	{
		$brand_manager = $row_u["cer_summary_in01_sig03"];
	}

	$sql_u = "select user_firstname, user_name from users " . 
		     "left join addresses on address_id = user_address " . 
		     "left join user_roles on user_role_user = user_id " . 
		     "where address_country = " . dbquote($row["order_shop_address_country"]) .
		     " and user_active = 1 and user_role_role = 33";

	
	$res_u = mysql_query($sql_u) or dberror($sql_u);
	if($row_u = mysql_fetch_assoc($res_u))
	{
		$sales_manager = "\r\n" . $row_u["user_name"] . " " . $row_u["user_firstname"];
	}

	if(!$sales_manager)
	{
		$sql_u = "select user_firstname, user_name from addresses " . 
			     "left join country_access on country_access_country = address_country " .
			     "left join users on user_id = country_access_user " . 
			     "left join user_roles on user_role_user = user_id " . 
				 "where address_country = " . dbquote($row["order_shop_address_country"]) .
				 " and user_active = 1 and user_role_role = 33";
	
		$res_u = mysql_query($sql_u) or dberror($sql_u);
		if($row_u = mysql_fetch_assoc($res_u))
		{
			$sales_manager = "\r\n" . $row_u["user_name"] . " " . $row_u["user_firstname"];
		}
	}


	$approval_name1 = $cer_basicdata["cer_basicdata_approvalname1"];
	$president = $cer_basicdata["cer_basicdata_approvalname2"];
	$vp_sales = $cer_basicdata["cer_basicdata_approvalname3"];
	$retail_head = $cer_basicdata["cer_basicdata_approvalname4"];

	/*
	$sql = "select * from cer_approvalnames where cer_approvalname_id = 1";
	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$approval_name1 = $row["cer_approvalname_name1"];
		$president = $row["cer_approvalname_name2"];
		$vp_sales = $row["cer_approvalname_name3"];
		$retail_head = $row["cer_approvalname_name4"];
	}
	*/


    /*
	$cer_basicdata = get_cer_basicdata(param("pid"), $cer_version);

	$exchange_rate_factor = $cer_basicdata['cer_basicdata_factor'];
	if(!$cer_basicdata['cer_basicdata_factor'])
	{
		$exchange_rate_factor = 1;
	}

	$exchange_rate = 1;
	if($cer_basicdata['cer_basicdata_exchangerate'] > 0)
	{
		$exchange_rate = $cer_basicdata['cer_basicdata_exchangerate'];
	}
	*/

	
	$exchange_rate_factor = $ln_basicdata['ln_basicdata_factor'];
	if(!$ln_basicdata['ln_basicdata_factor'])
	{
		$exchange_rate_factor = 1;
	}

	$exchange_rate = 1;
	if($ln_basicdata['ln_basicdata_exchangerate'] > 0)
	{
		$exchange_rate = $ln_basicdata['ln_basicdata_exchangerate'];
	}

	

	include("include/in_financial_data.php");	

	/********************************************************************
		prepare pdf
	*********************************************************************/

	require_once('../include/tcpdf/config/lang/eng.php');
	require_once('../include/tcpdf/tcpdf.php');
	require_once('../include/fpdi/fpdi.php'); 


    //Instanciation of inherited class
	$pdf = new FPDI("P", "mm", "A4", true, 'UTF-8', false);
	$pdf->SetMargins(10, 23, 10);
	$pdf->setPrintHeader(false);
	$pdf->SetAutoPageBreak(false);

	$pdf->Open();


	$pdf->SetFillColor(220, 220, 220); 

	$pdf->AddFont('arialn','');
	$pdf->AddFont('arialn','B');
	$pdf->AddFont('arialn','I');
	$pdf->AddFont('arialn','BI');

	$pdf->AddPage();
	$new_page = 0;

	include("ln_form_pdf_detail.php");

	// write pdf
	$pdf->Output(BRAND . '_ln_form_' . $posname . '_' . $project["project_number"] . '.pdf');

}

?>