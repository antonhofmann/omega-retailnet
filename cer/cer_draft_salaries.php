<?php
/********************************************************************

	cer_draft_salaries.php

    Application Form: salaries
    
	Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2001-02-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2001-02-16
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_draft_functions.php";

check_access("has_access_to_cer_drafts");
set_referer("cer_draft_salary.php");


/********************************************************************
    prepare all data needed
*********************************************************************/
$cer_basicdata = get_draft_basicdata(param("did"));

$result = update_commissions(param("did"));

// create list values for the years
$years = array();
$first_year = $cer_basicdata["cer_basicdata_firstyear"];
$first_month = $cer_basicdata["cer_basicdata_firstmonth"];
$last_year = $cer_basicdata["cer_basicdata_lastyear"];

for($i = $first_year;$i <= $last_year;$i++)
{
	$years[$i] = $i;
}


$sql_list1 = "select cer_salary_id, cer_salary_year_starting, " .
             "cer_salary_month_starting, cer_salary_fixed_salary, cer_salary_bonus, " . 
			 "cer_salary_other, cer_salary_social_charges, cer_salary_total, cer_salary_headcount_percent,  " .
			 "TRUNCATE(cer_salary_total*cer_salary_headcount_percent/100,2 ) as cer_salary_part, " .
			 "cer_staff_type_name, concat(cer_salary_commission_percent, '%') as cer_salary_commission_percent, " . "concat(cer_salary_commission_social_charges_percent, '%') as  cer_salary_commission_social_charges_percent " . 
             "from cer_draft_salaries " . 
			 "left join cer_staff_types on cer_staff_type_id = cer_salary_staff_type ";

$list1_filter = "cer_salary_draft_id = " . param("did");


//get list_totals
$list_totals = array();
$list_totals[1] = 0;
$list_totals[2] = 0;
$list_totals[3] = 0;
$list_totals[4] = 0;
$list_totals[5] = 0;
$list_totals[6] = 0;

$sql = $sql_list1 . " where " . $list1_filter;
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$list_totals[1] = $list_totals[1] + $row["cer_salary_fixed_salary"];
	$list_totals[2] = $list_totals[2] + $row["cer_salary_bonus"];
	$list_totals[3] = $list_totals[3] + $row["cer_salary_other"];
	$list_totals[4] = $list_totals[4] + $row["cer_salary_social_charges"];
	$list_totals[5] = $list_totals[5] + $row["cer_salary_total"];
	$list_totals[6] = $list_totals[6] + $row["cer_salary_part"];

}

$currency = get_draft_currency(param("did"));


/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_drafts", "cer_drafts");
$form->add_hidden("did", param("did"));

$form->add_section("Salary Growth");

$form->add_edit("cer_basicdata_salary_growth", "Fixed grwoth of salaries in % per year", 0, $cer_basicdata["cer_basicdata_salary_growth"], TYPE_DECIMAL, 10,2, 1, "salary_growth");

$form->add_section("Commission");
$form->add_edit("cer_basicdata_commission", "Global Commission in % per year", 0, $cer_basicdata["cer_basicdata_commission"], TYPE_DECIMAL, 6,2);
$form->add_edit("cer_basicdata_commission_tax_percent", "Global Social Charges in % on Commission per year", 0, $cer_basicdata["cer_basicdata_commission_tax_percent"], TYPE_DECIMAL, 6,2);

$form->add_comment("Please check the following option if commissions are to be calculated on the base of NET SALES VALUES. <br />Otherwise they will be calculated on the base of GROSS SALES VALUES.");
$form->add_checkbox("cer_basicdata_commission_from_net_sales", "comissions are to be calculated on the base of NET SALES VALUES", $cer_basicdata["cer_basicdata_commission_from_net_sales"], "", "Calculation Mode");


$form->add_button("form_save", "Save Data");


/********************************************************************
    build list of standard expenses
*********************************************************************/
$list1 = new ListView($sql_list1, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list1->set_title("Salaries in " . $currency["symbol"]);
$list1->set_entity("cer_draft_salaries");
$list1->set_filter($list1_filter);

$list1->add_hidden("did", param("did"));

$link = "cer_draft_salary.php?did=" . param("did");
$list1->add_column("cer_staff_type_name", "Function", $link, "", "", COLUMN_NO_WRAP);
$list1->add_column("cer_salary_year_starting", "from Year", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list1->add_column("cer_salary_month_starting", "from Month", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list1->add_column("cer_salary_headcount_percent", "Fulltime %", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list1->add_column("cer_salary_fixed_salary", "Fixed Salary", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list1->add_column("cer_salary_bonus", "Bonus", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list1->add_column("cer_salary_other", "Other Salary", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list1->add_column("cer_salary_social_charges", "Social Charges", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list1->add_column("cer_salary_total", "Total Cost", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list1->add_column("cer_salary_part", "Part Time Cost", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

$list1->add_column("cer_salary_commission_percent", "Commission", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list1->add_column("cer_salary_commission_social_charges_percent", "Social Charges", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);



$list1->set_footer("cer_salary_function", "Total");
$list1->set_footer("cer_salary_fixed_salary", number_format($list_totals[1] ,0));
$list1->set_footer("cer_salary_bonus", number_format($list_totals[2] ,0));
$list1->set_footer("cer_salary_other", number_format($list_totals[3] ,0));
$list1->set_footer("cer_salary_social_charges", number_format($list_totals[4] ,0));
$list1->set_footer("cer_salary_total", number_format($list_totals[5] ,0));
$list1->set_footer("cer_salary_part", number_format($list_totals[6] ,0));
$list1->add_button("new", "Add New Position");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


if($form->button("form_save"))
{
	if($form->validate())
	{
		$fields = array();
    
		$value = dbquote($form->value("cer_basicdata_salary_growth"));
		$fields[] = "cer_basicdata_salary_growth = " . $value;

		$value = dbquote($form->value("cer_basicdata_commission_from_net_sales"));
		$fields[] = "cer_basicdata_commission_from_net_sales = " . $value;

		$value = dbquote($form->value("cer_basicdata_commission"));
		$fields[] = "cer_basicdata_commission = " . $value;

		$value = dbquote($form->value("cer_basicdata_commission_tax_percent"));
		$fields[] = "cer_basicdata_commission_tax_percent = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_drafts set " . join(", ", $fields) . " where cer_basicdata_id = " . param("did");
		mysql_query($sql) or dberror($sql);

		//update comissions
		$result = update_commissions(param("did"));

		$form->message("Your data has bee saved.");
	}
}

$list1->populate();
$list1->process();

if($list1->button("new"))
{
	$link = "cer_draft_salary.php?did=" . param("did");
	redirect($link);
}
  
/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_drafts");

require "include/draft_page_actions.php";
$page->header();
$page->title($cer_basicdata['cer_basicdata_title'] . ": Human Resources");

require_once("include/tabs_draft.php");
$form->render();

$list1->render();

?>

<div id="salary_growth" style="display:none;">
    If empty then salaries for each year will be calculated using your country's inflation rate (See Cash Flow Calculation).<br />
	Indicate a percentage if salaries are growing with a fixed rate each year.
</div>

 

<?php

require "include/draft_footer_scripts.php";
$page->footer();

?>