<?php
/********************************************************************

    cer_additional_funding_pdf

    Print PDF for Request for additional funding (Form IN-01).

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2012-03-06
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:   2012-03-06
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.
*********************************************************************/

require "../include/frame.php";
require "include/get_functions.php";
require "include/financial_functions.php";

check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}

$project = get_project(param("pid"));

//get all data
//set defaul values
$legal_entity_name = "";
$project_name = "";
$project_manager = "";
$cer_number = "";
$cer_approved_by = "";
$cer_approved_date = "";
$rfaf_number = "";


$protocol_number1 = "";
$protocol_number1 = "";

$legal_entity_name = "";
$project_name = "";
$project_manager = "";
$project_kind = "";


$approved_amount = "";
$additoinal_amount = "";
$currency_symbol = "";



$cer_basicdata = get_cer_basicdata(param("pid"));


$sql = "select * from cer_summary where cer_summary_cer_version = 0 and cer_summary_project = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$cer_number = $row["cer_summary_in01_cernr"];
	$cer_approved_by = $row["cer_summary_in01_approval_by"];
	$cer_approved_date = to_system_date($row["cer_summary_in01_approval_date"]);

	
	$rfaf_number = $row["cer_summary_in01_rfaf"];
	$protocol_number1 = $row["cer_summary_in03_prot01"];
	$protocol_number2 = $row["cer_summary_in03_prot02"];

}

//cer refunding data
$refunding_status = "";
$refunding_justification = "";
$refunding_basicdata = "";
$attachment1 = "";
$attachment2 = "";
$attachment3 = "";
$attachment4 = "";


$date1 = "";
$date2 = "";
$date3 = "";
$date4 = "";
$date5 = "";
$date6 = "";
$date7 = "";
$date8 = "";
$date9 = "";

$approval_name1 = "";
$approval_name2 = "";
$approval_name3 = "";
$approval_name4 = "";
$approval_name5 = "";
$approval_name6 = "";
$approval_name7 = "";
$approval_name8 = "";
$approval_name17 = "";

$share_business_partner = '';

$sql = "select * from cer_refundings where cer_refunding_project = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$id = $row["cer_refunding_id"];

	$refunding_status = $row["cer_refunding_status"];
	$refunding_justification = $row["cer_refunding_justification"];
	$refunding_basicdata = $row["cer_refunding_effects"];
	$attachment1 = ".." . $row["cer_refunding_attachment1"];
	$attachment2 = ".." . $row["cer_refunding_attachment2"];
	$attachment3 = ".." . $row["cer_refunding_attachment3"];
	$attachment4 = ".." . $row["cer_refunding_attachment4"];


	$approval_name1 = $row["cer_refunding_appronalname1"];
	$approval_name2 = $row["cer_refunding_appronalname2"];
	$approval_name3 = $row["cer_refunding_appronalname3"];
	$approval_name4 = $row["cer_refunding_appronalname4"];
	$approval_name5 = $row["cer_refunding_appronalname5"];
	$approval_name6 = $row["cer_refunding_appronalname6"];
	$approval_name7 = $row["cer_refunding_appronalname7"];
	$approval_name8 = $row["cer_refunding_appronalname8"];

	$approval_name17 = $cer_basicdata["cer_summary_in01_sig10"];

	$share_business_partner = $row["cer_refunding_share_businesspartner"];
}


// get all data needed from project
$share_business_partner_from_project = '';


$sql = "select * " . 
       "from projects " . 
	   "left join orders on order_id = project_order " .
	   "left join countries on country_id = order_shop_address_country " .
	   "left join product_lines on product_line_id = project_product_line " .
	   "left join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join projectkinds on projectkind_id = project_projectkind " .
       "left join addresses on address_id = order_client_address " .
       "left join users on user_id = project_retail_coordinator " . 
	   "where project_id = " . param("pid");


$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	
	if($row["address_legal_entity_name"])
	{
		$legal_entity_name = $row["address_legal_entity_name"];
	}
	else
	{
		$legal_entity_name = $row["address_company"];
	}
	
	$project_name = $row["order_shop_address_company"]. ", " . $row["order_shop_address_place"] . ", " . $row["country_name"];
	$project_manager = $row["user_name"] . " " . $row["user_firstname"];
	
	$project_kind = $row["projectkind_name"];


	$share_business_partner_from_project = $row['project_share_other'];
	
	
}

$client_currency = get_cer_currency(param("pid"));
$currency_symbol = $client_currency["symbol"];



$keymoney = get_pos_intangibles(param("pid"), 15);
$goodwill = get_pos_intangibles(param("pid"), 17);
$construction = get_pos_intangibles(param("pid"), 1);
$fixturing = get_pos_intangibles(param("pid"), 3);
$architectural = get_pos_intangibles(param("pid"), 5);
$equipment = get_pos_intangibles(param("pid"), 7);
$deposit = get_pos_intangibles(param("pid"), 9);
$other1 = get_pos_intangibles(param("pid"), 11);
$other2 = get_pos_intangibles(param("pid"), 13);
$merchandising = get_pos_intangibles(param("pid"), 18);
$transportation = get_pos_intangibles(param("pid"), 19);


$approved_amount = $keymoney["cer_investment_amount_cer_loc_approved"];
$approved_amount += $goodwill["cer_investment_amount_cer_loc_approved"];
$approved_amount += $construction["cer_investment_amount_cer_loc_approved"];
$approved_amount += $fixturing["cer_investment_amount_cer_loc_approved"];
$approved_amount += $architectural["cer_investment_amount_cer_loc_approved"];
$approved_amount += $equipment["cer_investment_amount_cer_loc_approved"];
$approved_amount += $deposit["cer_investment_amount_cer_loc_approved"];
$approved_amount += $other1["cer_investment_amount_cer_loc_approved"];
$approved_amount += $merchandising["cer_investment_amount_cer_loc_approved"];
$approved_amount += $transportation["cer_investment_amount_cer_loc_approved"];
$approved_amount += $other2["cer_investment_amount_cer_loc_approved"];

$approved_amount = round($approved_amount/1000)*1000;

$additional_amount = $keymoney["cer_investment_amount_additional_cer_loc"];
$additional_amount += $goodwill["cer_investment_amount_additional_cer_loc"];
$additional_amount += $construction["cer_investment_amount_additional_cer_loc"];
$additional_amount += $fixturing["cer_investment_amount_additional_cer_loc"];
$additional_amount += $architectural["cer_investment_amount_additional_cer_loc"];
$additional_amount += $equipment["cer_investment_amount_additional_cer_loc"];
$additional_amount += $deposit["cer_investment_amount_additional_cer_loc"];
$additional_amount += $other1["cer_investment_amount_additional_cer_loc"];
$additional_amount += $merchandising["cer_investment_amount_additional_cer_loc"];
$additional_amount += $transportation["cer_investment_amount_additional_cer_loc"];
$additional_amount += $other2["cer_investment_amount_additional_cer_loc"];

$total_amount = $additional_amount + $approved_amount ;

if($approved_amount > 0) {
	$delta = round(100*($total_amount -$approved_amount) / $approved_amount, 2);
}
else {
	$delta = 0;
}
if($delta > 0) {$delta = "+" . $delta. "%";}
elseif($delta < 0) {$delta = "-" . $delta. "%";}
elseif($delta == 0) {$delta = "+100%";}

//business partner amounts

$investment_business_partner = round($approved_amount*$cer_basicdata["cer_basicdata_franchsiee_investment_share"]/100, 0);

$additional_amount_business_partner = $share_business_partner*$additional_amount/100;
$new_total_business_partner = $additional_amount_business_partner+$investment_business_partner;

if($investment_business_partner > 0) {
	$delta2 = round(100*($new_total_business_partner - $investment_business_partner) / $investment_business_partner, 2);
}
else {
	$delta2 = 0;
}
if($delta2 > 0) {$delta2 = "+" . $delta2. "%";}
elseif($delta2 < 0) {$delta2 = "-" . $delta2. "%";}
elseif($delta2 == 0) {$delta2 = "+100%";}


if($additional_amount_business_partner > 0) {
	$delta4 = round(100*($additional_amount_business_partner) / $additional_amount, 2);
}
else {
	$delta4 = 0;
}
if($delta4 > 0) {$delta4 = "+" . $delta4. "%";}
elseif($delta4 < 0) {$delta4 = "-" . $delta42. "%";}
elseif($delta42 == 0) {$delta4 = "+0%";}


//corrected approved amount

$approved_amount2 = $approved_amount - $investment_business_partner;
$additional_amount2 = $additional_amount - $additional_amount_business_partner;
$total_amount2 = $additional_amount2 + $approved_amount2 ;

if($approved_amount2 > 0) {
	$delta3 = round(100*($total_amount2 -$approved_amount2) / $approved_amount2, 2);
}
else {
	$delta3 = 0;
}
if($delta3 > 0) {$delta3 = "+" . $delta3. "%";}
elseif($delta3 < 0) {$delta3 = "-" . $delta3. "%";}
elseif($delta3 == 0) {$delta3 = "+100%";}


//total share business partner
$share_business_partner_overall = 0;
if($total_amount > 0) {
	$share_business_partner_overall = round(100*($investment_business_partner+$additional_amount_business_partner)/($total_amount), 2);
}



$overall_amount_business_partner = $investment_business_partner+$additional_amount_business_partner;
$new_overall_total_business_partner = $investment_business_partner+$additional_amount_business_partner;

if($overall_amount_business_partner > 0) {
	$delta5 = round(100*($overall_amount_business_partner - $investment_business_partner) / $overall_amount_business_partner, 2);
}
else {
	$delta5 = 0;
}
if($delta5 > 0) {$delta5 = "+" . $delta5. "%";}
elseif($delta5 < 0) {$delta5 = "-" . $delta5. "%";}
elseif($delta5 == 0) {$delta5 = "+0%";}

//get signed sheet
$in03_signatures = "";

$sql_sheets = "select cer_summary_in03_coversheet " .
	   "from cer_summary ". 
	   " where cer_summary_cer_version = 0 and cer_summary_project = " . param("pid");

$res_sheets = mysql_query($sql_sheets) or dberror($sql_sheets);
if ($row_ssheets = mysql_fetch_assoc($res_sheets))
{
	$in03_signatures = $row_ssheets["cer_summary_in03_coversheet"];
}




//project

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');
require_once('../include/fpdi/fpdi.php'); 





// Create and setup PDF document

	$pdf = new FPDI("P", "mm", "A4", true, 'UTF-8', false);
	$pdf->SetMargins(10, 23, 10);
	$pdf->setPrintHeader(false);

	$pdf->AddFont('arialn','');
	$pdf->AddFont('arialn','B');
	$pdf->AddFont('arialn','I');
	$pdf->AddFont('arialn','BI');
	$pdf->Open();


	//set pdf parameters
	$margin_top = 16;
	$margin_left = 12;
	$y = $margin_top;

	
	if($in03_signatures)
	{
		$source_file = ".." . $in03_signatures;
		if(file_exists($source_file))
		{
			
			$layout = new FPDI();
			$num_of_pages = $pdf->setSourceFile($source_file);
			$num_of_pages = $layout->setSourceFile($source_file);
			for($i=1;$i<=$num_of_pages;$i++)
			{
				$tplIdx = $pdf->importPage($i);
				$layout->AddPage("L");
				$tplIdx = $layout->importPage($i);
				
				$tpl = $layout->useTemplate($tplIdx);
				//$pdf->AddPage("", array(0=>$tpl["w"], 1=>$tpl["h"]));

				if($tpl["w"] > $tpl["h"])
				{
					$pdf->AddPage("L", array(0=>$tpl["h"], 1=>$tpl["w"]));
				}
				else
				{
					$pdf->AddPage("P", array(0=>$tpl["w"], 1=>$tpl["h"]));
				}
				$tpl = $pdf->useTemplate($tplIdx);
			}
		}
	}
	else
	{
		$pdf->SetTitle("Request for Additional Funding (Form IN-03");
		$pdf->SetAuthor(BRAND . " Retail Net");
		$pdf->SetDisplayMode(150);
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(false, 0);

		// Title first line
		$pdf->SetXY($margin_left,$margin_top);
		$pdf->SetFont("arialn", "I", 9);
		$pdf->Cell(40, 8, "Swatch Group", 1);

		$pdf->SetFont("arialn", "B", 11);
		$pdf->Cell(82, 8, "Request for Additional Funding (RfAF)", 1, "", "C");

		$pdf->SetFont("arialn", "", 9);
		$pdf->SetFillColor(224,224,224);
		$pdf->Cell(25, 8, "Additional Credit", 1, "", "C", true);

		$pdf->SetFont("arialn", "", 9);
		$pdf->Cell(20, 8, date("d.m.Y"), 1, "", "C");
		$pdf->Cell(20, 8, "IN-03", 1, "", "C");


		// draw first box
		$pdf->SetXY($margin_left,$y+8);
		$pdf->Cell(187, 252, "", 1);


		// print project and investment infos
		$y = $y+9;
		$x = $margin_left+1;

		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(39, 5, "Legal Entity Name:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(146, 5, $legal_entity_name, 1, "", "L");


		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(39, 5, "Project Name (" . $project['order_number'] . "):", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(70, 5, substr($project_name, 0, 58), 1, "", "L");
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(28, 5, "Project Manager:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(48, 5, $project_manager, 1, "", "L");

		
		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(39, 5, "Original CER Nr.:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(31, 5, $cer_number, 1, "", "L");
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(39, 5, "Approved by:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(28, 5, $cer_approved_by, 1, "", "L");
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(18, 5, "Date:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(30, 5, $cer_approved_date, 1, "", "L");

		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(39, 5, "Approved Amount:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(31, 5, number_format($approved_amount, 0, '.', "'") . " " . $currency_symbol, 1, "", "L");
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(39, 5, "Additional Credit Request:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(28, 5, number_format($additional_amount, 0, '.', "'") . " " . $currency_symbol, 1, "", "L");
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(18, 5, "New Total:", 1, "", "L");
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(30, 5, number_format($total_amount, 0, '.', "'") . " " . $currency_symbol ." (" . $delta . ")", 1, "", "L");


		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(39, 5, "Approved with contribution:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(31, 5, number_format($approved_amount2, 0, '.', "'") . " " . $currency_symbol, 1, "", "L");
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(39, 5, "Additional Credit Request:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(28, 5, number_format($additional_amount2, 0, '.', "'") . " " . $currency_symbol, 1, "", "L");
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(18, 5, "New Total:", 1, "", "L");
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(30, 5, number_format($total_amount2, 0, '.', "'") . " " . $currency_symbol ." (" . $delta3 . ")", 1, "", "L");

		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(39, 5, "Share Business Partner %:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		
		if($share_business_partner) {
			$pdf->Cell(31, 5, number_format($share_business_partner, 2, '.', "'") . '%', 1, "", "L");
		}
		else {
			$pdf->Cell(31, 5, '', 1, "", "L");
		}
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(39, 5, "Share Business Partner:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(28, 5, number_format($additional_amount_business_partner, 0, '.', "'") . " " . $currency_symbol, 1, "", "L");
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(18, 5, "New Total:", 1, "", "L");
		$pdf->SetFont("arialn", "B", 8);
		//$pdf->Cell(30, 5, number_format($new_total_business_partner, 0, '.', "'") . " " . $currency_symbol ." (" . $delta2 . ")", 1, "", "L");
	    $pdf->Cell(30, 5, number_format($additional_amount_business_partner, 0, '.', "'") . " " . $currency_symbol ." (" . $delta4 . ")", 1, "", "L");


		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(39, 5, "Total Share Business Partner %:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(31, 5, number_format($share_business_partner_overall, 2, '.', "'") . '%', 1, "", "L");
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(39, 5, "Total Share Business Partner:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(28, 5, number_format($overall_amount_business_partner, 0, '.', "'") . " " . $currency_symbol, 1, "", "L");
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(18, 5, "New Total:", 1, "", "L");
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(30, 5, number_format($new_overall_total_business_partner, 0, '.', "'") . " " . $currency_symbol ." (" . $delta5 . ")", 1, "", "L");

	

		$pdf->SetXY($margin_left,$y+6);


		$y = $y+5;
		$pdf->SetXY($x-1,$y);
		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(102, 8, "Status of the project:", 0, "", "L");

		$y = $y+7;
		$pdf->SetXY($x,$y);
		$pdf->SetFillColor(248,251,167);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(185, 40, "", 1, "", "L", true);
		$pdf->SetXY($x,$y+1);
		$pdf->MultiCell(185,3.5, $refunding_status, 0, "T");

		
		$y = $y+40;
		$pdf->SetXY($x-1,$y);
		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(102, 8, "Justification for the request:", 0, "", "L");

		$y = $y+7;
		$pdf->SetXY($x,$y);
		$pdf->SetFillColor(248,251,167);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(185, 40, "", 1, "", "L", true);
		$pdf->SetXY($x,$y+1);
		$pdf->MultiCell(185,3.5, $refunding_justification, 0, "T");


		$y = $y+40;
		$pdf->SetXY($x-1,$y);
		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(102, 8, "Effects on the project:", 0, "", "L");

		$y = $y+7;
		$pdf->SetXY($x,$y);
		$pdf->SetFillColor(248,251,167);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(185, 40, "", 1, "", "L", true);
		$pdf->SetXY($x,$y+1);
		$pdf->MultiCell(185,3.5, $refunding_basicdata, 0, "T");


		$y = $y+45;
		$pdf->SetXY($x-1,$y);
		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(102, 8, "Signature of requesting legal entity (Country):", 0, "", "L");


		$y = $y+6;


		$pdf->SetXY($x,$y);
		$pdf->SetFont("freesans", "", 7);
		$pdf->Cell(29, 8, "Date: " . $date3, 1, "", "L");
		$pdf->SetXY($margin_left+30,$y);
		$pdf->MultiCell(63, 8, "Country Manager " . "\r\n" . $approval_name1, 1);
		$pdf->SetXY($margin_left+93,$y);
		$pdf->Cell(30, 8, "Date: " . $date4, 1, "", "L");
		$pdf->SetXY($margin_left+123,$y);
		//$pdf->Cell(62, 8, $approval_name4, 1);
		$pdf->Cell(62, 8, '', 1);

		$pdf->SetXY($margin_left+93,$y);
		$pdf->Cell(30, 8, "Date: " . $date2, 1, "", "L");
		$pdf->SetXY($margin_left+123,$y);
		$pdf->MultiCell(62, 8, "Finance/Service Center Manager " . "\r\n" . $approval_name2, 1);

		
		//china projects
		$y = $y+8;
		if($project['order_client_address'] == 42
			or $project['order_client_address'] == 7) {
		
			$pdf->SetXY($x,$y);
			$pdf->SetFont("freesans", "", 7);
			$pdf->Cell(29, 8, "Date: " . $date3, 1, "", "L");
			$pdf->SetXY($margin_left+30,$y);
			$pdf->MultiCell(63, 8, "Head of Controlling " . "\r\n" . $approval_name17, 1);
			$pdf->SetXY($margin_left+93,$y);
			$pdf->Cell(30, 8, "Date: " . $date4, 1, "", "L");
			$pdf->SetXY($margin_left+123,$y);
			//$pdf->Cell(62, 8, $approval_name4, 1);
			$pdf->Cell(62, 8, '', 1);

			$pdf->SetXY($margin_left+93,$y);
			$pdf->Cell(30, 8, "Date: " . $date2, 1, "", "L");
			$pdf->SetXY($margin_left+123,$y);
			$pdf->MultiCell(62, 8,"Brand Manager " . "\r\n" . $approval_name3, 1);
		}
		else {

			$pdf->SetXY($x,$y);
			$pdf->SetFont("freesans", "", 7);
			$pdf->Cell(29, 8, "Date: " . $date1, 1, "", "L");
			$pdf->SetXY($margin_left+30,$y);
			
			$pdf->MultiCell(63, 8, "Brand Manager " . "\r\n" . $approval_name3, 1);

			$pdf->SetXY($margin_left+93,$y);
			$pdf->Cell(30, 8, "Date: " . $date2, 1, "", "L");
			$pdf->SetXY($margin_left+123,$y);
			$pdf->MultiCell(62, 8, "", 1);
		}
		

		$y = $y+8;
		$pdf->SetXY($x-1,$y);
		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(102, 8, "Signature of requesting brand PC (" . BRAND . " HQ):", 0, "", "L");
		
		
		//old version before 14.3.2017
		if(!$approval_name4) {
			$y = $y+6;
			$pdf->SetXY($x,$y);
			$pdf->SetFont("freesans", "", 7);
			$pdf->Cell(29, 8, "Date: " . $date1, 1, "", "L");
			$pdf->SetXY($margin_left+30,$y);
			if($approval_name5)
			{
				$pdf->MultiCell(63, 8, "VP Finance " . "\r\n" . $approval_name5, 1);
			}
			elseif($approval_name6)
			{
				$pdf->MultiCell(63, 8, "VP Sales " . "\r\n" . $approval_name6, 1);
			}
			elseif($approval_name7)
			{
				$pdf->MultiCell(63, 8, "Merchandising Manager " . "\r\n" . $approval_name7, 1);
			}
			else
			{
				$pdf->MultiCell(63, 8, "", 1);
			}

			$pdf->SetXY($margin_left+93,$y);
			$pdf->Cell(30, 8, "Date: " . $date2, 1, "", "L");
			$pdf->SetXY($margin_left+123,$y);
			if($approval_name5 and $approval_name6)
			{
				$pdf->MultiCell(62, 8, "VP Sales " . "\r\n" . $approval_name6, 1);
			}
			elseif(($approval_name5 or $approval_name6) and $approval_name7)
			{
				$pdf->MultiCell(62, 8, "Merchandising Manager " . "\r\n" . $approval_name7, 1);
			}
			else
			{
				$pdf->MultiCell(62, 8, "", 1);
			}


			$y = $y+8;
			$pdf->SetXY($x,$y);
			$pdf->SetFont("freesans", "", 7);
			$pdf->Cell(29, 8, "Date: " . $date3, 1, "", "L");
			$pdf->SetXY($margin_left+30,$y);
			if($approval_name5 and $approval_name6 and $approval_name7)
			{
				$pdf->MultiCell(63, 8, "Merchandising Manager " . "\r\n" . $approval_name7, 1);
			}
			else
			{
				$pdf->MultiCell(63, 8, "", 1);
			}

			$pdf->SetXY($margin_left+93,$y);
			$pdf->Cell(30, 8, "Date: " . $date4, 1, "", "L");
			$pdf->SetXY($margin_left+123,$y);
			$pdf->MultiCell(62, 8, "", 1);
		}
		else {
			$y = $y+6;
			$pdf->SetXY($x,$y);
			$pdf->SetFont("freesans", "", 7);
			$pdf->Cell(29, 8, "Date: " . $date1, 1, "", "L");
			$pdf->SetXY($margin_left+30,$y);
			if($approval_name5)
			{
				$pdf->MultiCell(63, 8, "VP Finance " . "\r\n" . $approval_name5, 1);
			}
			elseif($approval_name6)
			{
				$pdf->MultiCell(63, 8, "VP Sales " . "\r\n" . $approval_name6, 1);
			}
			elseif($approval_name7)
			{
				$pdf->MultiCell(63, 8, "CEO " . "\r\n" . $approval_name7, 1);
			}
			else
			{
				$pdf->MultiCell(63, 8, "", 1);
			}

			$pdf->SetXY($margin_left+93,$y);
			$pdf->Cell(30, 8, "Date: " . $date2, 1, "", "L");
			$pdf->SetXY($margin_left+123,$y);
			if($approval_name5 and $approval_name6)
			{
				$pdf->MultiCell(62, 8, "VP Sales " . "\r\n" . $approval_name6, 1);
			}
			elseif(($approval_name5 or $approval_name6) and $approval_name7)
			{
				$pdf->MultiCell(62, 8, "CEO " . "\r\n" . $approval_name7, 1);
			}
			else
			{
				$pdf->MultiCell(62, 8, "", 1);
			}


			$y = $y+8;
			$pdf->SetXY($x,$y);
			$pdf->SetFont("freesans", "", 7);
			$pdf->Cell(29, 8, "Date: " . $date3, 1, "", "L");
			$pdf->SetXY($margin_left+30,$y);
			if($approval_name5 and $approval_name6 and $approval_name7)
			{
				$pdf->MultiCell(63, 8, "CEO " . "\r\n" . $approval_name7, 1);
			}
			else
			{
				$pdf->MultiCell(63, 8, "", 1);
			}

			$pdf->SetXY($margin_left+93,$y);
			$pdf->Cell(30, 8, "Date: " . $date4, 1, "", "L");
			$pdf->SetXY($margin_left+123,$y);
			$pdf->MultiCell(62, 8, "Retail Controller " . "\r\n" . $approval_name4, 1);
		}


		
			
		$y = $pdf->getY();
		$pdf->SetXY($x-1,$y);
		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(102, 8, "Swatch Group Approval:", 0, "", "L");

		$y = $y+6;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(50, 5, "Swatch Group Central Services", 1, "", "L");
		$pdf->Cell(11, 5, "Date: " . $date7, 1, "", "L");
		$pdf->Cell(32, 5, "", 1, "", "L");
		$pdf->Cell(15, 5, "Name:", 1, "", "L");
		$pdf->SetFont("freesans", "", 7);
		$pdf->Cell(77, 5, $approval_name8, 1, "", "L");


		$y = $y+6;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 7);
		$pdf->Cell(50, 5, "Swatch Group Group General Management (KL)", 1, "", "L");
		$pdf->Cell(11, 5, "Date: " . $date8, 1, "", "L");
		$pdf->Cell(32, 5, "", 1, "", "L");
		$pdf->Cell(15, 5, "Protocol no.:", 1, "", "L");
		$pdf->Cell(57, 5, $protocol_number1, 1, "", "L");
		$pdf->SetFillColor(224,224,224);
		$pdf->Cell(20, 5,"RfAF Number", 1, "", "L", true);

		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 7);
		$pdf->Cell(50, 5, "Board of Directors", 1, "", "L");
		$pdf->Cell(11, 5, "Date: " . $date9, 1, "", "L");
		$pdf->Cell(32, 5, "", 1, "", "L");
		$pdf->Cell(15, 5, "Protocol no.:", 1, "", "L");
		$pdf->Cell(57, 5, $protocol_number1, 1, "", "L");
		$pdf->SetFillColor(224,224,224);
		$pdf->Cell(20, 5, $rfaf_number, 1, "", "L", true);
	}


	
	//attachments

	$source_file = $attachment1;
	if(file_exists($source_file))
	{
		//PDF
		if(substr($source_file, strlen($source_file)-3, 3) == "pdf" or substr($source_file, strlen($source_file)-3, 3) == "PDF")
		{
			if(!isset($layout))
			{
				$layout = new FPDI();
			}
			$num_of_pages = $pdf->setSourceFile($source_file);
			$num_of_pages = $layout->setSourceFile($source_file);

			for($i=1;$i<=$num_of_pages;$i++)
			{
				/*
				$tplIdx = $pdf->importPage($i);
				$layout->AddPage("L");
				$tplIdx = $layout->importPage($i);
				
				$tpl = $layout->useTemplate($tplIdx);
				if($tpl["w"] > $tpl["h"])
				{
					$tpl = $layout->useTemplate($tplIdx, 0, 0, $tpl["w"], $tpl["h"], true);
					$pdf->AddPage("L");
				}
				else
				{
					$tpl = $layout->useTemplate($tplIdx, 0, 0, $tpl["w"], $tpl["h"], true);
					$pdf->AddPage("P");
				}
				$tpl = $pdf->useTemplate($tplIdx);
				*/




				$tplidx = $pdf->ImportPage($i);
				$s = $pdf->getTemplatesize($tplidx);
				if($s["w"] > $s["h"])
				{
					$pdf->AddPage("L", array(0=>$s["h"], 1=>$s["w"]));
				}
				else
				{
					$pdf->AddPage("P", array(0=>$s["w"], 1=>$s["h"]));
				}
				$pdf->useTemplate($tplidx);
			}
		}
	}

	$source_file = $attachment2;
	if(file_exists($source_file))
	{
		//PDF
		if(substr($source_file, strlen($source_file)-3, 3) == "pdf" or substr($source_file, strlen($source_file)-3, 3) == "PDF")
		{
			if(!isset($layout))
			{
				$layout = new FPDI();
			}
			$num_of_pages = $pdf->setSourceFile($source_file);
			$num_of_pages = $layout->setSourceFile($source_file);

			for($i=1;$i<=$num_of_pages;$i++)
			{
				/*
				$tplIdx = $pdf->importPage($i);
				$layout->AddPage("L");
				$tplIdx = $layout->importPage($i);
				
				$tpl = $layout->useTemplate($tplIdx);
				if($tpl["w"] > $tpl["h"])
				{
					$tpl = $layout->useTemplate($tplIdx, 0, 0, $tpl["w"], $tpl["h"], true);
					$pdf->AddPage("L");
				}
				else
				{
					$tpl = $layout->useTemplate($tplIdx, 0, 0, $tpl["w"], $tpl["h"], true);
					$pdf->AddPage("P");
				}
				$tpl = $pdf->useTemplate($tplIdx);
				*/


				$tplidx = $pdf->ImportPage($i);
				$s = $pdf->getTemplatesize($tplidx);
				if($s["w"] > $s["h"])
				{
					$pdf->AddPage("L", array(0=>$s["h"], 1=>$s["w"]));
				}
				else
				{
					$pdf->AddPage("P", array(0=>$s["w"], 1=>$s["h"]));
				}
				$pdf->useTemplate($tplidx);
			}
		}
	}

	$source_file = $attachment3;
	if(file_exists($source_file))
	{
		//PDF
		if(substr($source_file, strlen($source_file)-3, 3) == "pdf" or substr($source_file, strlen($source_file)-3, 3) == "PDF")
		{
			if(!isset($layout))
			{
				$layout = new FPDI();
			}
			$num_of_pages = $pdf->setSourceFile($source_file);
			$num_of_pages = $layout->setSourceFile($source_file);

			for($i=1;$i<=$num_of_pages;$i++)
			{
				/*
				$tplIdx = $pdf->importPage($i);
				$layout->AddPage("L");
				$tplIdx = $layout->importPage($i);
				
				$tpl = $layout->useTemplate($tplIdx);
				if($tpl["w"] > $tpl["h"])
				{
					$tpl = $layout->useTemplate($tplIdx, 0, 0, $tpl["w"], $tpl["h"], true);
					$pdf->AddPage("L");
				}
				else
				{
					$tpl = $layout->useTemplate($tplIdx, 0, 0, $tpl["w"], $tpl["h"], true);
					$pdf->AddPage("P");
				}
				$tpl = $pdf->useTemplate($tplIdx);
				*/


				$tplidx = $pdf->ImportPage($i);
				$s = $pdf->getTemplatesize($tplidx);
				if($s["w"] > $s["h"])
				{
					$pdf->AddPage("L", array(0=>$s["h"], 1=>$s["w"]));
				}
				else
				{
					$pdf->AddPage("P", array(0=>$s["w"], 1=>$s["h"]));
				}
				$pdf->useTemplate($tplidx);
			}
		}
	}

	$source_file = $attachment4;
	if(file_exists($source_file))
	{
		//PDF
		if(substr($source_file, strlen($source_file)-3, 3) == "pdf" or substr($source_file, strlen($source_file)-3, 3) == "PDF")
		{
			if(!isset($layout))
			{
				$layout = new FPDI();
			}
			$num_of_pages = $pdf->setSourceFile($source_file);
			$num_of_pages = $layout->setSourceFile($source_file);

			for($i=1;$i<=$num_of_pages;$i++)
			{
				$tplIdx = $pdf->importPage($i);
				$layout->AddPage("L");
				$tplIdx = $layout->importPage($i);
				
				$tpl = $layout->useTemplate($tplIdx);
				if($tpl["w"] > $tpl["h"])
				{
					$tpl = $layout->useTemplate($tplIdx, 0, 0, $tpl["w"], $tpl["h"], true);
					$pdf->AddPage("L");
				}
				else
				{
					$tpl = $layout->useTemplate($tplIdx, 0, 0, $tpl["w"], $tpl["h"], true);
					$pdf->AddPage("P");
				}
				$tpl = $pdf->useTemplate($tplIdx);
			}
		}
	}


	$file_name = BRAND . "_IN-03_" . $project["project_number"] . ".pdf";
$pdf->Output($file_name);
?>