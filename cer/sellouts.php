<?php
/********************************************************************

    sellouts.php

    Enter Sellout Data for the POS Location

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2012-08-16
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2012-08-16
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";
require "include/get_project.php";

check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}

$sellout_project = get_project(param("pid"));


if($sellout_project["project_is_relocation_project"] == 1 
   and $sellout_project["project_relocated_posaddress_id"] > 0)
{
	//get the original project from the pos location to be relocated
	
	
	$latest_project = get_latest_pos_project($sellout_project["project_relocated_posaddress_id"]);

	
	if(count($latest_project) > 0)
	{
		

		
		$pos_id = $sellout_project["project_relocated_posaddress_id"];
		$sellout_project = get_project($latest_project["project_id"]);

		//set temporary relocation project to true because latest project has no relocation info
		$sellout_project["project_is_relocation_project"] = 1;
		$shop = $sellout_project["order_shop_address_company"] . ", " .
				$sellout_project["order_shop_address_address"] . ", " .
				$sellout_project["order_shop_address_zip"] . " " .
				$sellout_project["order_shop_address_place"] . ", " .
				$sellout_project["order_shop_address_country_name"];
	}
	else
	{

		$pos_data = get_poslocation($sellout_project["project_relocated_posaddress_id"], "posaddresses");

		$shop = $pos_data["posaddress_name"] . ", " .
				$pos_data["posaddress_address"] . ", " .
				$pos_data["posaddress_zip"] . " " .
				$pos_data["place_name"] . ", " .
				$pos_data["country_name"];
		$pos_id = $sellout_project["project_relocated_posaddress_id"];

	}
}
else
{
	$shop = $sellout_project["order_shop_address_company"] . ", " .
			$sellout_project["order_shop_address_address"] . ", " .
			$sellout_project["order_shop_address_zip"] . " " .
			$sellout_project["order_shop_address_place"] . ", " .
			$sellout_project["order_shop_address_country_name"];



	//get pos_id
	$sql = "select posorder_posaddress " . 
		   "from posorders " . 
		   "where posorder_order = " . dbquote($sellout_project["project_order"]);

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$pos_id = $row["posorder_posaddress"];

	
}



$pos_data = get_poslocation($pos_id, "posaddresses");
$has_project = update_posdata_from_posorders($pos_id);

$opening_date = $pos_data["posaddress_store_openingdate"];
$closing_date = $pos_data["posaddress_store_closingdate"];

$opening_year = substr($opening_date, 0, 4);
$closing_year = substr($closing_date, 0, 4);
$actual_year = date("Y");


//check if sellout records exist else create
$bp_starting_date = $cer_basicdata["cer_basicdata_firstyear"];

if($actual_year == $bp_starting_date)
{
	$actual_year--;
}

if($bp_starting_date > date("Y"))
{
	$bp_starting_date = date("Y");
}

if($opening_year + 3 > $bp_starting_date )
{
	$starting_year = $opening_year;
}
else
{
	$starting_year = $bp_starting_date - 3;

}

if($closing_year > 0)
{
	$ending_year = $closing_year;
}
else
{
	$ending_year = $actual_year;
}

/*
$sql = "delete from possellouts" . 
	   " where possellout_posaddress_id = " . $pos_id . 
	   " and (possellout_watches_units = 0 or possellout_watches_units is NULL)";
$res = mysql_query($sql) or dberror($sql);
*/


for($year=$starting_year;$year<=$ending_year;$year++)
{
	$sql = "select count(possellout_id) as num_recs " . 
		   "from possellouts " . 
		   "where possellout_posaddress_id = " . dbquote($pos_id) . 
		   " and possellout_year = " . dbquote($year);

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	
	if($row["num_recs"] == 0)
	{
		$sql_i = "INSERT INTO possellouts (" . 
				 "possellout_posaddress_id, possellout_year, date_created, user_created" . 
				 ") VALUES (" . 
				 $pos_id . ", " . 
				 $year . ", " . 
				 dbquote(date("Y-m-d H:i:s")) . ", " . 
				 dbquote(user_login()) . 
				 ")";
		
		$result = mysql_query($sql_i) or dberror($sql_i);

	}
}


$watches = array();
$watches_gross_sales = array();
$months = array();
$delete_record = array();

$sql = "select * from possellouts where possellout_posaddress_id = " . $pos_id;
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$watches[$row["possellout_id"]] = $row["possellout_watches_units"];
	$watches_gross_sales[$row["possellout_id"]] = $row["possellout_watches_grossales"];
	
	if($row["possellout_month"] > 0)
	{
		$months[$row["possellout_id"]] = $row["possellout_month"];
	}
	elseif($row["possellout_year"] < date("Y"))
	{
		$months[$row["possellout_id"]] = 12;
	}
	else
	{
		$months[$row["possellout_id"]] = (int)date("m");
	}

	
	$link = "sellouts.php?pid=" . param("pid") . "&did=" . $row["possellout_id"] . "&context=" . param("context");

	$delete_record[$row["possellout_id"]] = '<a href="' . $link . '"><img src="/pictures/closed.gif" border="0"/></a>';
}


    

/********************************************************************
    Create Form
*********************************************************************/

$form = new Form("posaddresses", "posaddress");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("pos_id", $pos_id);
$form->add_hidden("context", param("context"));

$form->add_label("poslocation", "POS Location", 0, $shop);

/*
$form->add_section("Add a new month to the list for one of the following years:");
$form->add_button("y" . $starting_year, $starting_year);
for($year=$starting_year+1;$year<=$ending_year;$year++)
{
	$form->add_button("y" . $year, $year);
}
*/

$form->populate();
$form->process();

for($year=$starting_year;$year<=$ending_year;$year++)
{
	
	if($form->button("y" . $year))
	{

		$sql_i = "INSERT INTO possellouts (" . 
				 "possellout_posaddress_id, possellout_year, date_created, user_created" . 
				 ") VALUES (" . 
				 $pos_id . ", " . 
				 $year . ", " . 
				 dbquote(date("Y-m-d H:i:s")) . ", " . 
				 dbquote(user_login()) . 
				 ")";
		
		$result = mysql_query($sql_i) or dberror($sql_i);
		redirect("sellouts.php?pid=" . param("pid") . "&context=" . param("context"));
	}

}


/********************************************************************
    Create List
*********************************************************************/
$list = new ListView("select * from possellouts");

$list->set_title("Sellouts in Units");
$list->set_entity("possellouts");
$list->set_filter("possellout_posaddress_id = " . $pos_id . " and possellout_year < " . dbquote(date("Y")));
$list->set_order("possellout_year DESC, possellout_month DESC");

if(has_access("has_access_to_his_cer") or has_access("has_full_access_to_cer"))
{
	$list->add_column("possellout_year", "Year", "", 0 ,'', COLUMN_NO_WRAP);
	$list->add_number_edit_column("possellout_month", "Num of Months", "2", 0, $months);

	$list->add_number_edit_column("possellout_watches_units", "Watches", "10", 0, $watches);
	$list->add_number_edit_column("possellout_watches_grossales", "Gross Sales", "10", 0, $watches_gross_sales);

	//$list->add_text_column("delete", "", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML , $delete_record);
	$list->add_button("save", "Save");
}
else
{
	$list->add_column("possellout_year", "Year", "", 0 ,'', COLUMN_NO_WRAP);
	$list->add_column("possellout_month", "Num of Months", "", 0,'', COLUMN_NO_WRAP);
	$list->add_column("possellout_watches_units", "Watches", "", COLUMN_ALIGN_RIGHT);
	$list->add_column("possellout_watches_grossales", "Gross Sales", "", COLUMN_ALIGN_RIGHT);
}




$list->populate();
$list->process();



if($list->button("save"))
{
	$error = "";
	$error_text = "";
	foreach($list->columns as $key=>$column)
	{
	
		if($column["name"] == "possellout_month")
		{
			foreach($column["values"] as $possellout_id=>$month)
			{
				if($month) 
				{
					if($month >= 1 and $month <= 12)
					{
					
						$fields = array();
						$fields[] = "possellout_month = " . dbquote($month);
						$sql = "update possellouts set " . join(", ", $fields) . " where possellout_id = " . $possellout_id;
						mysql_query($sql) or dberror($sql);
					}
					else
					{
						$error_text = "The number of months mus be a number from 1 to 12!";
					}

				}

				$fields = array();
				$fields[] = "date_modified = " . dbquote(date("Y-m-d H:i:s"));
				$fields[] = "user_modified = " . dbquote(user_login());
				$sql = "update possellouts set " . join(", ", $fields) . " where possellout_id = " . $possellout_id;
				mysql_query($sql) or dberror($sql);
			}
		}
		elseif($column["name"] == "possellout_watches_units")
		{
			foreach($column["values"] as $possellout_id=>$watches)
			{
				
				if(!$watches or ($watches >= 0 and $watches <= 9999999999))
				{
					$fields = array();
					$fields[] = "possellout_watches_units = " . dbquote($watches);
					$sql = "update possellouts set " . join(", ", $fields) . " where possellout_id = " . $possellout_id;
					mysql_query($sql) or dberror($sql);
				}
				else
				{
					$error_text = "The amount of watches must be a number!";
				}
				
			}
		}
		elseif($column["name"] == "possellout_watches_grossales")
		{
			foreach($column["values"] as $possellout_id=>$gross_sale)
			{
				if($gross_sale) 
				{
					if($gross_sale >= 0 and $gross_sale <= 9999999999)
					{
						$fields = array();
						$fields[] = "possellout_watches_grossales = " . dbquote($gross_sale);
						$sql = "update possellouts set " . join(", ", $fields) . " where possellout_id = " . $possellout_id;
						mysql_query($sql) or dberror($sql);
					}
					else
					{
						$error_text = "The gross sale value must be a number!";
					}

				}
			}
		}
	}

	if($error_text)
	{
		$form->error($error_text);
	}
	else
	{
		$form->message("Your sellout data was saved.");
	}

}



$page = new Page("cer_projects");
require "include/project_page_actions.php";
$page->header();

if(param("context") and param("context") == 'ln')
{
	$page->title("Lease Negotiation Form: Sellouts");
	require_once("include/tabs_ln.php");
}
else
{
	if($form_type == "INR03")
	{
		$page->title("INR-03 - Retail Furniture in Third-party Store: Sellouts");
	}
	elseif($form_type == "AF")
	{
		$page->title("Application Form: Sellouts");
	}
	else
	{
		$page->title("Capital Expenditure Request: Sellouts");
	}
	require_once("include/tabs.php");
}


$form->render();
$list->render();

$page->footer();

?>
