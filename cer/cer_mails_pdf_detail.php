<?php
/********************************************************************

    cer_mails_pdf_detail.php

    Print Mailbox

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-06-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-06-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.
*********************************************************************/

/********************************************************************
    prepare all data needed
*********************************************************************/
$posdata = get_pos_data($project["project_order"]);


$project_name = $project["order_shop_address_company"]. ", " . $project["order_shop_address_place"] . ", " . $project["country_name"];
$project_number = $project["project_number"];

/********************************************************************
    print PDF
*********************************************************************/
//set pdf parameters

$margin_top = 16;
$margin_left = 12;
$y = $margin_top;

$pdf->SetTitle("Mailbox");
$pdf->SetAuthor(BRAND . " Retailnet");
$pdf->SetDisplayMode(150);
$pdf->AddPage("P");
$pdf->SetAutoPageBreak(true, 0);

// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "I", 9);
$pdf->Cell(45, 10, "", 1);

$pdf->Image('../pictures/logo.jpg',13,$margin_top + 1,22);


$pdf->SetFont("arialn", "B", 11);
$pdf->Cell(122, 10,  $project_name, 1, "", "L");
$pdf->Cell(20, 10, date("d.m.Y"), 1, "", "C");

$y = $y+11;
$x = $margin_left;
$pdf->SetXY($x,$y);

$pdf->SetFont("arialn", "B", 8);
$pdf->Cell(187, 5, "CER/AF - Mailbox for Project " . $project_number, 1, "", "L");

$group = "";
$sql = "select * from cer_mails " .
       "where cer_mail_project = " . param("pid") .
	   " order by cer_mail_group, date_created desc";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$time = substr($row["date_created"], 8, 2) . "." . substr($row["date_created"], 5, 2) . "." . substr($row["date_created"], 0, 4)  . "." . substr($row["date_created"], 11, strlen($row["date_created"]));

	$text = str_replace("\t", " ", $row["cer_mail_text"]) . $time;

	if($row["cer_mail_group"] != $group)
	{
		$group = $row["cer_mail_group"];
		$y = $pdf->GetY();
		$y = $y+7;
		$pdf->SetXY($x,$y);

		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(187, 5, $row["cer_mail_group"], 1, "", "L");


		$y = $y+7;
		$pdf->SetXY($x,$y);

		$pdf->SetFont("arialn", "", 8);
		$pdf->Write(3.5, $text, "", 0, "", 0, 0, false, false, 0);
		$y = $pdf->GetY();
		$y = $y+7;
		$pdf->Line($x, $y, $x+187,$y);
	}
	else
	{
		$y = $pdf->GetY();
		$y = $y+7;
		$pdf->SetXY($x,$y);

		$pdf->SetFont("arialn", "", 8);
		$pdf->Write(3.5, $text, "", 0, "", 0, 0, false, false, 0);
		$y = $pdf->GetY();
		$y = $y+7;
		$pdf->Line($x, $y, $x+187,$y);

	}
}


?>