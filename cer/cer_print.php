<?php
/********************************************************************

    cer_print.php

    Select object to be printed

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-06-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-06-14
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require "include/get_project.php";


check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}

/********************************************************************
    prepare all data needed
*********************************************************************/
$currencies = array();
$currency_id = 1;

if($cer_basicdata['cer_basicdata_currency']) {
	$sql = 'select currency_id, currency_symbol from currencies where currency_id = ' . $cer_basicdata['cer_basicdata_currency'];

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res) and !in_array($row['currency_symbol'], $currencies))
	{
		$currencies["c1"] = $row['currency_symbol'];
		$currency_id = $row['currency_id'];
	}
}




$url = "lnr_booklet_pdf.php?pid=" . param("pid") . "";
$link_lnr_booklet = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

$url = "cer_booklet_pdf.php?pid=" . param("pid") . "&v=2013";
$link_cer_booklet = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

$url = "lnr_booklet_pdf.php?pid=" . param("pid");
$link_af_booklet = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

$url = "cer_booklet_pdf.php?pid=" . param("pid") . "&v=0";
$link_cer_booklet_before_2013 = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";


$url = "inr03_pre_pdf.php?pid=" . param("pid") . "&booklet=1";
$link_inr03_pre_booklet = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

$url = "cer_inr03_pdf.php?pid=" . param("pid") . "&booklet=1";
$link_inr03_booklet = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";



if($form_type == "CER")
{
	$url = "cer_booklet_investments_pdf.php?pid=" . param("pid") . "&cerversion=0&v=2013";
}
elseif($form_type == "INR03")
{
	$url = "inr03_booklet_investments_pdf.php?pid=" . param("pid");
}
else
{
	$url = "af_booklet_investments_pdf.php?pid=" . param("pid") . "&cerversion=0";
}
$link_cer_booklet_investments = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";



$url = "cer_in01_pdf.php?pid=" . param("pid"). "&v=2013";
$link_in01 = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

$url = "cer_in02AB_pdf.php?pid=" . param("pid");;
$link_in02AB = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";


$url = "cer_in01_pdf.php?pid=" . param("pid"). "&v=0";
$link_summary_in01_before_2013 = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";


$url = "/cer/cer_inr02_pdf.php?pid=" . param("pid") . "&cid=s";
$link_bp01 = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";


$url = "/cer/cer_inr02_pdf.php?pid=" . param("pid") . "&cid=c1";
$link_bp02 = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";


$url = "/cer/cer_inr02_extension_pdf.php?pid=" . param("pid");
$link_bp03 = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

$url = "cer_inr02_pdf_debug.php?pid=" . param("pid");
$link_bp04 = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

$url = "lnr_pdf.php?pid=" . param("pid") . "";
$link_ln_form = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

$url = "ln_form_pdf.php?pid=" . param("pid");
$link_ln_form_before_2013 = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

$url = "cer_inr03_pdf.php?pid=" . param("pid");
$link_iaf = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

$url = "af_inr03_pdf.php?pid=" . param("pid");
$link_iaf_af = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

$url = "inr03_pre_pdf.php?pid=" . param("pid");
$link_iaf_inr03 = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";


$url = "cer_inr03_pdf.php?pid=" . param("pid");
$link_inr03 = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";


$url = "ln_lease_details_pdf.php?pid=" . param("pid");
$link_lease = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

$url = "cer_listofvars_pdf.php?pid=" . param("pid");
$link_lvars = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

$url = "ln_former_lease_conditions_pdf.php?pid=" . param("pid");
$link_flease = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";


$url = "/user/project_print_project_data.php?pid=" . param("pid");
$link_project_data = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";



$url = "cer_mails_pdf.php?pid=" . $project["project_id"];
$link_malbox = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";


$url = "cer_mails_pdf.php?pid=" . param("pid");
$link_malbox = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";


$url = "/user/project_costs_budget_pdf.php?pid=" . param("pid");
$link_budget = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";


$url = "/user/project_costs_bid_comparison_pdf.php?pid=" . param("pid");
$link_lw = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";



$url = "cer_additional_funding_pdf.php?pid=" . param("pid");
$link_in03 = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";


//get ln versions
$ln_versions = array();
$ln_version_date = "";
$ln_version = 0;
$sql = "select ln_basicdata_version, ln_basicdata_version_cer_version, ln_basicdata.date_created as date1 " . 
       "from ln_basicdata " .
	   "left join users on user_id = ln_basicdata_version_user_id " . 
	   "where ln_basicdata_version > 0 and ln_basicdata_project = " . param("pid") .
	   " order by ln_basicdata_version asc";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
		$url = "lnr_booklet_pdf.php?pid=" . param("pid") . "&lnversion=" . $row["ln_basicdata_version"] . "&cerversion=" . $row["ln_basicdata_version_cer_version"];
		$link_lnr_bookletv = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

		$tmp = to_system_date(substr($row["date1"], 0, 10)) . " " . substr($row["date1"], 10, 9);
		$ln_versions[$row["ln_basicdata_version"]] = array('date'=>$tmp, 'link0'=>$link_lnr_bookletv);

		if($ln_basicdata["ln_basicdata_locked"] == 1)
		{
			$link_lnr_booklet = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";
			$ln_version_date = $tmp;
			$ln_version = $row["ln_basicdata_version"];


			$url = "lnr_pdf.php?pid=" . param("pid") . "&lnversion=" . $row["ln_basicdata_version"] . "&cerversion=" . $row["ln_basicdata_version_cer_version"];
			$link_ln_form = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

		}
}

//get cer versions
// only show the latest version
$cer_versions = array();
$cer_version_date = "";
$cer_version = 0;
$sql = "select cer_basicdata_version, cer_basicdata_version_context, cer_basicdata.date_created as date1 " . 
       "from cer_basicdata " .
	   "left join users on user_id = cer_basicdata_version_user_id " . 
	   "where cer_basicdata_version > 0 " .
	   " and cer_basicdata_project = " . param("pid") .
	   " and cer_basicdata_version_context = 'cer' " . 
	   " order by cer_basicdata_version asc";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
		if($form_type == "CER")
		{
			$url = "cer_booklet_pdf.php?pid=" . param("pid") . "&cerversion=" . $row["cer_basicdata_version"]. "&v=2013";
		}
		elseif($form_type == "INR03")
		{
			$url = "cer_inr03_pdf.php?pid=" . param("pid") . "&cerversion=" . $row["cer_basicdata_version"]. "&booklet=1";
		}
		else
		{
			$url = "lnr_booklet_pdf.php?pid=" . param("pid") . "&cerversion=" . $row["cer_basicdata_version"];;
		}
		
		$link_cer_bookletv = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";
		$cer_version = $row["cer_basicdata_version"];

		$tmp = to_system_date(substr($row["date1"], 0, 10)) . " " . substr($row["date1"], 10, 9);
		$cer_versions[$row["cer_basicdata_version"]] = array('date'=>$tmp, 'link0'=>$link_cer_bookletv);

		if($cer_basicdata["cer_basicdata_cer_locked"] == 1)
		{	
			$link_cer_booklet = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";
			$cer_version_date = $tmp;
			$cer_version = $row["cer_basicdata_version"];
		}
}


$url = "cer_inr02c_pdf.php?pid=" . param("pid") . "&cid=s";
$link_inr02c_1 = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

$url = "cer_inr02c_pdf.php?pid=" . param("pid") . "&cid=c1";
$link_inr02c_2 = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new form("cer_basicdata", "cer_basicdata");
$form->add_hidden("pid", param("pid"));

include("include/project_head_small.php");

if($form_type == "CER")
{
	$form->add_section("Booklets");


	
	if($ln_version_date)
	{
		$form->add_label("lnr" . $ln_version_date, "LN Booklet " . $ln_version_date, RENDER_HTML, $link_lnr_booklet);
	}
	else
	{
		$form->add_label("lnr", "LN Booklet", RENDER_HTML, $link_lnr_booklet);
	}
	


	if(count($ln_versions) > 0)
	{
		foreach($ln_versions as $version=>$data)
		{
			if($data['date'] != $ln_version_date)
			{
				$form->add_label("lnr" . $data['date'], "LN Booklet " . $data['date'], RENDER_HTML, $data['link0']);
			}
			
		}
	}



	if($cer_version_date)
	{
		$form->add_label("cer" . $cer_version_date, "CER Booklet " . $cer_version_date, RENDER_HTML, $link_cer_booklet);
	}
	else
	{
		$form->add_label("cer_booklet", "CER Booklet", RENDER_HTML, $link_cer_booklet);
	}

	

	if(count($cer_versions) > 0)
	{
		foreach($cer_versions as $version=>$data)
		{
			if($data['date'] != $cer_version_date)
			{
				$form->add_label("cer" . $data['date'], "CER Booklet " . $data['date'], RENDER_HTML, $data['link0']);
			}
			
		}
	}

	if($link_cer_booklet_investments)
	{
		$form->add_label("cer_booklet_investments", "CER Investments", RENDER_HTML, $link_cer_booklet_investments);
	}

	$form->add_section("Request for Additional Funding");
	$form->add_label("cer_in03", "IN-03", RENDER_HTML, $link_in03);
	

	
	$form->add_section("Business Plan");
	$form->add_label("bp01", "Business Plan in CHF", RENDER_HTML, $link_bp01);
	$form->add_label("bp02", "Business Plan in " . $currencies["c1"], RENDER_HTML, $link_bp02);


	$form->add_label("inr02c_1", "CER versus LN Figures (INR-02C) in CHF", RENDER_HTML, $link_inr02c_1);
	$form->add_label("inr02c_2", "CER versus LN Figures (INR-02C) in " . $currencies["c1"], RENDER_HTML, $link_inr02c_2);


	$form->add_label("bp03", "Business Plan Extension ", RENDER_HTML, $link_bp03);
	
	
	if(has_access("can_edit_catalog"))
	{
		$form->add_label("bp04", "Business Plan Debugging ", RENDER_HTML, $link_bp04);
	}


	$form->add_section("Single Forms");
	
	if($ln_version_date)
	{
		$form->add_label("ln_form", "LN Application Form " . $ln_version_date, RENDER_HTML, $link_lnr_booklet);
	}
	else
	{
		$form->add_label("ln_form", "LN Application Form", RENDER_HTML, $link_ln_form);
	}
	
	if($cer_version_date)
	{
		$form->add_label("inr01", "IN-R01 " . $cer_version_date, RENDER_HTML, $link_in01);
		$form->add_label("in02AB", "INR-02A/INR-02B " . $cer_version_date, RENDER_HTML, $link_in02AB);
	}
	else
	{
		$form->add_label("inr01", "IN-R01", RENDER_HTML, $link_in01);
		$form->add_label("in02AB", "INR-02A/INR-02B", RENDER_HTML, $link_in02AB);
	}


	if (has_access("has_access_to_all_projects") or has_access("can_view_budget_in_projects"))
	{
		$form->add_section("Project");
		$form->add_label("project_data", "Project Data", RENDER_HTML, $link_project_data);
		$form->add_label("budget", "Project's Budget", RENDER_HTML, $link_budget);
		$form->add_label("local_work", "Ouotes for Local Work", RENDER_HTML, $link_lw);
	}


	$form->add_section("Other");
	$form->add_label("lvars", "List of Variables", RENDER_HTML, $link_lvars);

	if($project['project_projectkind'] == 5
		or $project['project_projectkind'] == 2
		or $project['project_projectkind'] == 3
		or $project['project_projectkind'] == 6) {
		$form->add_label("flease", "Former Lease Conditions", RENDER_HTML, $link_flease);
	}

	$form->add_label("lease", "Lease Details", RENDER_HTML, $link_lease);
	$form->add_label("cer_mailbox", "Mailbox", RENDER_HTML, $link_malbox);


	$page_title = "CER: Select Object to be Printed";

}

if($form_type == "AF")
{
	$form->add_section("Booklets");
	$form->add_label("cer_booklet", "Application Booklet", RENDER_HTML, $link_af_booklet);
	$form->add_label("inr03_booklet", "INR-03 Booklet", RENDER_HTML, $link_inr03_booklet);


	
	/*
	if($link_cer_booklet_investments)
	{
		$form->add_label("cer_booklet_investments", "Investments", RENDER_HTML, $link_cer_booklet_investments);
	}
	*/


	if(count($cer_versions) > 0)
	{
		$form->add_section("Booklets Older Application Versions");
		foreach($cer_versions as $version=>$data)
		{
			$form->add_label("cer" . $version, "Application Booklet " . $data['date'], RENDER_HTML, $data['link0']);
		}
	}

	if(count($cer_versions) > 0)
	{
		$form->add_section("Booklets Older INR-03 Versions");
		foreach($cer_versions as $version=>$data)
		{
			$form->add_label("inr03" . $version, "INR-03Booklet " . $data['date'], RENDER_HTML, $data['link0']);
		}
	}

	

	//$form->add_section("Request for Additional Funding");
	//$form->add_label("cer_in03", "IN-03", RENDER_HTML, $link_in03);

	
	$form->add_section("Business Plan");
	$form->add_label("bp01", "Business Plan in CHF", RENDER_HTML, $link_bp01);
	$form->add_label("bp02", "Business Plan in " . $currencies["c1"], RENDER_HTML, $link_bp02);
	$form->add_label("bp03", "Business Plan Extension ", RENDER_HTML, $link_bp03);
	
	
	if(has_access("can_edit_catalog"))
	{
		$form->add_label("bp04", "Business Plan Debugging ", RENDER_HTML, $link_bp04);
	}


	$form->add_section("Single Forms");
	$form->add_label("investment_approval_af", "Application Form", RENDER_HTML, $link_ln_form);
	$form->add_label("approval_form", "Request - Retail Furniture Third Party INR-03", RENDER_HTML, $link_inr03);


	if (has_access("has_access_to_all_projects") or has_access("can_view_budget_in_projects"))
	{
		$form->add_section("Project");
		$form->add_label("project_data", "Project Data", RENDER_HTML, $link_project_data);
		$form->add_label("budget", "Project's Budget", RENDER_HTML, $link_budget);
	}


	$form->add_section("Other");
	$form->add_label("lvars", "List of Variables", RENDER_HTML, $link_lvars);
	$form->add_label("cer_mailbox", "Mailbox", RENDER_HTML, $link_malbox);

	$page_title = "Application Form: Select Object to be Printed";
}


if($form_type == "INR03")
{
	$form->add_section("Booklets");
	$form->add_label("inr03_pre_booklet", "SIS Application Booklet", RENDER_HTML, $link_inr03_pre_booklet);

	$form->add_label("inr03_booklet", "INR-03 Booklet", RENDER_HTML, $link_inr03_booklet);


	if(count($cer_versions) > 0)
	{
		$form->add_section("Booklets Older INR-03 Versions");
		foreach($cer_versions as $version=>$data)
		{
			$form->add_label("inr03" . $version, "INR-03Booklet " . $data['date'], RENDER_HTML, $data['link0']);
		}
	}

	

	//$form->add_section("Request for Additional Funding");
	//$form->add_label("cer_in03", "IN-03", RENDER_HTML, $link_in03);

		
	if(has_access("can_edit_catalog"))
	{
		$form->add_section("Business Plan");
		$form->add_label("bp04", "Business Plan Debugging ", RENDER_HTML, $link_bp04);
	}


	$form->add_section("Single Forms");
	$form->add_label("preapproval_form", "SIS Application Form", RENDER_HTML, $link_iaf_inr03);
	$form->add_label("approval_form", "Request - Retail Furniture Third Party INR-03", RENDER_HTML, $link_inr03);


	if (has_access("has_access_to_all_projects") or has_access("can_view_budget_in_projects"))
	{
		$form->add_section("Project");
		$form->add_label("project_data", "Project Data", RENDER_HTML, $link_project_data);
		$form->add_label("budget", "Project's Budget", RENDER_HTML, $link_budget);
	}


	$form->add_section("Other");
	$form->add_label("lvars", "List of Variables", RENDER_HTML, $link_lvars);
	$form->add_label("cer_mailbox", "Mailbox", RENDER_HTML, $link_malbox);

	$page_title = "INR-03: Select Object to be Printed";
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();




/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page("cer_projects");

require "include/project_page_actions.php";

$page->header();
$page->title($page_title);


$form->render();


$page->footer();
?>