<?php
/********************************************************************

    inr03_inr03_pdf_detail.php

    Print PDF for Preapproval INR03

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2016-08-11
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2016-08-11
    Version:        1.0.0

    Copyright (c) 2016, Swatch AG, All Rights Reserved.
*********************************************************************/

/********************************************************************
    prepare all data needed
*********************************************************************/
$posdata = get_pos_data($project["project_order"]);

if($project['project_projectkind'] == 3 or $project['project_projectkind'] == 4) {
	$franchisee_address = get_address($project["order_franchisee_address_id"]);
}
else
{
	//$franchisee_address = get_address($posdata["posaddress_franchisee_id"]);
	$franchisee_address = get_address($project["order_franchisee_address_id"]);
}


$client_address = get_address($project["order_client_address"]);

//get quantities of watches
$units = array();
$units[0] = "";
$units[1] = "";
$units[2] = "";

$i = 0;
$sql = "select cer_revenue_year, sum(cer_revenue_quantity_watches) as total_watches " .
       "from cer_revenues " . 
       "where cer_revenue_cer_version = " . $cer_version . 
	   "  and cer_revenue_project = " . param("pid") .
	   " group by cer_revenue_year " . 
       " order by cer_revenue_year ASC ";
	   

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($project["project_fagrstart"] != NULL 
		   and $project["project_fagrstart"] != '0000-00-00')
	{
		if($row["cer_revenue_year"] >=  substr($project["project_fagrstart"], 0, 4))
		{
			$units[$i] = "Units in " . $row["cer_revenue_year"] . ": " . $row["total_watches"];
			$i++;
		}
	}
	else
	{
	    $units[$i] = "Units in " . $row["cer_revenue_year"] . ": " . $row["total_watches"];
		$i++;
	}
	
}


$project_name = $project["order_shop_address_company"]. ", " . $project["order_shop_address_place"] . ", " . $project["country_name"];


$design_objectives = get_project_design_objectives($project["project_id"]);

$posorder = get_posorder_id($project["project_order"]);

$neighbours = $posorder["posorder_neighbour_left"] . " " . $posorder["posorder_neighbour_right"] .  " " . $posorder["posorder_neighbour_acrleft"] .  " " . $posorder["posorder_neighbour_acrright"] .  " " . $posorder["posorder_neighbour_brands"];

$neighbours = str_replace("\r\n ", " ", $neighbours);
$neighbours = str_replace("\n ", " ", $neighbours);
$neighbours = str_replace("\r ", " ", $neighbours);
$neighbours = str_replace("  ", " ", $neighbours);
$neighbours = str_replace("  ", " ", $neighbours);
$neighbours = str_replace("  ", " ", $neighbours);



/********************************************************************
    print PDF
*********************************************************************/
//set pdf parameters

$margin_top = 16;
$margin_left = 12;
$y = $margin_top;

$pdf->SetTitle("SIS Form");
$pdf->SetAuthor(BRAND . " Retailnet");
$pdf->SetDisplayMode(150);
$pdf->AddPage("P", "A4");
$pdf->SetAutoPageBreak(false, 0);

// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "I", 9);
$pdf->Cell(45, 10, "", 1);

$pdf->Image('../pictures/logo.jpg',13,$margin_top + 1,22);

$pdf->SetFont("arialn", "B", 11);
$pdf->Cell(122, 10, "SHOP-IN-SHOP Application Form ", 1, "", "C");
$pdf->Cell(20, 10, date("d.m.Y"), 1, "", "C");


// draw first box
$pdf->SetXY($margin_left,$y+8);

	// print project and investment infos
	$y = $y+11;
	$x = $margin_left;

	//franchisee
	$y = $y+7;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(187, 5, "1. GENERAL INFORMATION", 0, "", "L");


	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Submitted by:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);


	$pdf->Cell(142, 5, $client_address["company"] . ', ' . $project["user_firstname"] . ' '. $project["user_name"], 1, "", "L");
	


	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Name of retailer:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	
	$pdf->Cell(142, 5, $franchisee_address["company"] . ', ' . $franchisee_address["place"] .  ' ' . $franchisee_address["country_name"], 1, "", "L");


	$posaddress = $posdata["posaddress_name"];

	if($posdata["posaddress_address"])
	{
		$posaddress .= ', '. $posdata["posaddress_address"];
	}

	$posaddress .= ', ' . $posdata["posaddress_zip"] . " " . $posdata["posaddress_place"];
	
	$posaddress .= ', ' . $posdata["country_name"];

	$posaddress .= ' (' . $project['project_number'] . ')';



	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "POS address:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, $posaddress, 1, "", "L");


	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	
	
	if($posdata["posaddress_email"] and $posdata["posaddress_contact_name"] and $posdata["posaddress_email"])
	{
		$pdf->Cell(45, 5, "Contact: Name / Phone / Email:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(142, 5, $posdata["posaddress_contact_name"] . ' / ' . $posdata["posaddress_phone"] . ' / ' . $posdata["posaddress_email"], 1, "", "L");
	}
	elseif($posdata["posaddress_email"] and $posdata["posaddress_contact_name"])
	{
		$pdf->Cell(45, 5, "Contact: Name / Phone / Email:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(142, 5, $posdata["posaddress_contact_name"] . ' / ' . $posdata["posaddress_phone"], 1, "", "L");
	}
	elseif($franchisee_address["email"])
	{
		$pdf->Cell(45, 5, "Contact: Name / Phone / Email:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(142, 5, $franchisee_address["contact_name"] . ' / ' . $franchisee_address["phone"] . ' / ' . $franchisee_address["email"], 1, "", "L");
	}
	else
	{
		$pdf->Cell(45, 5, "Contact: Name / Phone:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(142, 5, $franchisee_address["contact_name"] . ' / ' . $franchisee_address["phone"], 1, "", "L");
	}



	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "SIS location type:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, $posdata["posareas"], 1, "", "L");


	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "SIS size (sqm):", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, 'Gross surface: ' . $project["project_cost_gross_sqms"] . ' / Total surface: ' . $project["project_cost_totalsqms"] . ' / Sales surface: ' . $project["project_cost_sqms"], 1, "", "L");



	if($project["project_is_relocation_project"] == 1)
	{
		$scope = "Relocation";
	}
	else
	{
		$scope = $project["projectkind_name"];
	}

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Scope of work:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, $scope, 1, "", "L");

	$tmp_y = $pdf->getY();

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->Cell(187, 22, "", 1, "", "L");


	$y = $tmp_y+6;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(90, 5, "Proposed schedule:", 0, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 5, "Date of drawings to be submitted to retailer:", 0, "", "L");
	$pdf->Cell(20, 5, to_system_date($project["project_drawing_submission_date"]), 0, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->Cell(50, 5, "Date of construction works start:", 0, "", "L");
	$pdf->Cell(20, 5, to_system_date($project["project_construction_startdate"]), 0, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->Cell(50, 5, "Required completion / opening date: ", 0, "", "L");
	$pdf->Cell(20, 5, to_system_date($project["project_real_opening_date"]), 0, "", "L");
	


	$y = $y+10;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(187, 5, "2. REQUIREMENTS", 0, "", "L");

	$y = $y+7;
	$pdf->SetXY($x+2,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, "2.1 SPECIAL REQUIREMENTS FOR FURNITURE PRODUCTION", 0, "", "L");

	$y = $y+5;
	$pdf->SetXY($x+4,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 5, "2.1.1 Number of watches to be displayed:", 0, "", "L");
	$pdf->Cell(20, 5, $project["project_watches_displayed"], 0, "", "L");


	$y = $y+5;
	$pdf->SetXY($x+4,$y);
	$pdf->Cell(50, 5, "2.1.1 Number of watches to be displayed:", 0, "", "L");
	$pdf->Cell(20, 5, $project["project_watches_displayed"], 0, "", "L");

	$numbering = 2;
	foreach($design_objectives as $name=>$value)
	{
		$y = $y+5;
		$pdf->SetXY($x+4,$y);
		$pdf->Cell(50, 5, "2.1." . $numbering . "  " . $name, 0, "", "L");
		$pdf->Cell(20, 5, $value, 0, "", "L");

		$numbering++;
	}


	$y = $y+7;
	$pdf->SetXY($x+2,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, "2.2 NEIGHBOURING BRANDS", 0, "", "L");


	$y = $y+5;
	$pdf->SetXY($x+2,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(187, 5, $neighbours, 0, "", "L");


	$y = $y+10;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(187, 5, "3. CHECKLIST FOR REQUIRED DOCUMENTS", 0, "", "L");

	$y = $y+7;
	$pdf->SetXY($x+2,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(187, 5, "3.1  Shop floor plan in PDF and DWG formats (with overview of all brands)", 0, "", "L");

	$y = $y+5;
	$pdf->SetXY($x+2,$y);
	$pdf->Cell(187, 5, "3.2  Interior elevation plan in PDF and DWG formats showing neighbours", 0, "", "L");

	$y = $y+5;
	$pdf->SetXY($x+2,$y);
	$pdf->Cell(187, 5, "3.3  Elevation plan of facade in PDF and DWG formats (if applicable)", 0, "", "L");

	$y = $y+5;
	$pdf->SetXY($x+2,$y);
	$pdf->Cell(187, 5, "3.4  Photo report (interior views, external views), including neighbours", 0, "", "L");

	$y = $y+5;
	$pdf->SetXY($x+2,$y);
	$pdf->Cell(187, 5, "3.5  3D renderings", 0, "", "L");




	$y = $y+16;
	$pdf->SetXY($x,$y);
	$pdf->Cell(90, 5, "Prepared by: ", 0, "", "L");

	$pdf->Line($x+17, $y+5, $x+87,$y+5);

	$pdf->Cell(90, 5, "Approved by: ", 0, "", "L");

	$pdf->Line($x+108, $y+5, $x+178,$y+5);

	
	//add files for the booklet)
	if(array_key_exists("booklet", $_GET) and $_GET["booklet"] == 1)
	{
		$source_file = ".." . $ln_basicdata["ln_basicdata_floorplan"];

		if(file_exists($source_file))
		{
			if(substr($source_file, strlen($source_file)-3, 3) == "pdf" 
				or substr($source_file, strlen($source_file)-3, 3) == "PDF")
			{
				
				$num_of_pages = $pdf->setSourceFile($source_file);
				for($i=1;$i<=$num_of_pages;$i++)
				{
					$tplidx = $pdf->ImportPage($i);
					$s = $pdf->getTemplatesize($tplidx);
					if($s["w"] > $s["h"])
					{
						$pdf->AddPage("L", array(0=>$s["h"], 1=>$s["w"]));
					}
					else
					{
						$pdf->AddPage("P", array(0=>$s["w"], 1=>$s["h"]));
					}
					$pdf->useTemplate($tplidx);
				}
				
				
			}
		}

		$source_file = ".." . $ln_basicdata["ln_basicdata_elevation_plan"];

		if(file_exists($source_file))
		{
			if(substr($source_file, strlen($source_file)-3, 3) == "pdf" 
				or substr($source_file, strlen($source_file)-3, 3) == "PDF")
			{
				
				$num_of_pages = $pdf->setSourceFile($source_file);
				for($i=1;$i<=$num_of_pages;$i++)
				{
					$tplidx = $pdf->ImportPage($i);
					$s = $pdf->getTemplatesize($tplidx);
					if($s["w"] > $s["h"])
					{
						$pdf->AddPage("L", array(0=>$s["h"], 1=>$s["w"]));
					}
					else
					{
						$pdf->AddPage("P", array(0=>$s["w"], 1=>$s["h"]));
					}
					$pdf->useTemplate($tplidx);
				}
				
				
			}
		}

		$source_file = ".." . $ln_basicdata["ln_basicdata_elevation_plan3"];

		if(file_exists($source_file))
		{
			if(substr($source_file, strlen($source_file)-3, 3) == "pdf" 
				or substr($source_file, strlen($source_file)-3, 3) == "PDF")
			{
				
				$num_of_pages = $pdf->setSourceFile($source_file);
				for($i=1;$i<=$num_of_pages;$i++)
				{
					$tplidx = $pdf->ImportPage($i);
					$s = $pdf->getTemplatesize($tplidx);
					if($s["w"] > $s["h"])
					{
						$pdf->AddPage("L", array(0=>$s["h"], 1=>$s["w"]));
					}
					else
					{
						$pdf->AddPage("P", array(0=>$s["w"], 1=>$s["h"]));
					}
					$pdf->useTemplate($tplidx);
				}
				
				
			}
		}
		$source_file = ".." . $ln_basicdata["ln_basicdata_photoreport"];

		if(file_exists($source_file))
		{
			if(substr($source_file, strlen($source_file)-3, 3) == "pdf" 
				or substr($source_file, strlen($source_file)-3, 3) == "PDF")
			{
				
				$num_of_pages = $pdf->setSourceFile($source_file);
				for($i=1;$i<=$num_of_pages;$i++)
				{
					$tplidx = $pdf->ImportPage($i);
					$s = $pdf->getTemplatesize($tplidx);
					if($s["w"] > $s["h"])
					{
						$pdf->AddPage("L", array(0=>$s["h"], 1=>$s["w"]));
					}
					else
					{
						$pdf->AddPage("P", array(0=>$s["w"], 1=>$s["h"]));
					}
					$pdf->useTemplate($tplidx);
				}
				
				
			}

		}

		$source_file = ".." . $ln_basicdata["ln_basicdata_3drenderings"];

		if(file_exists($source_file))
		{
			if(substr($source_file, strlen($source_file)-3, 3) == "pdf" 
				or substr($source_file, strlen($source_file)-3, 3) == "PDF")
			{
				
				$num_of_pages = $pdf->setSourceFile($source_file);
				for($i=1;$i<=$num_of_pages;$i++)
				{
					$tplidx = $pdf->ImportPage($i);
					$s = $pdf->getTemplatesize($tplidx);
					if($s["w"] > $s["h"])
					{
						$pdf->AddPage("L", array(0=>$s["h"], 1=>$s["w"]));
					}
					else
					{
						$pdf->AddPage("P", array(0=>$s["w"], 1=>$s["h"]));
					}
					$pdf->useTemplate($tplidx);
				}
				
				
			}
		}
	}

	
	

	$file_name = "SIS_application_form_" . $project["order_number"];

	//$pdf->Output();

?>