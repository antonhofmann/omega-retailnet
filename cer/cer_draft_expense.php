<?php
/********************************************************************

    cer_draft_expense.php

    Add/edit New expense position

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-01-17
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-01-17
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require "include/get_draft_functions.php";

check_access("has_access_to_cer_drafts");

/********************************************************************
    prepare all data needed
*********************************************************************/
$currency = get_draft_currency(param("did"));
$basicdata = get_draft_basicdata(param("did"));


$expense_type_name = "";
$amounts = array();
$sales_percents = array();
$ids = array();
$sales_percent_ids = array();
$edidables = array();
$inflated = array();
$sql = "select * " .
       "from cer_draft_expenses " .
	   "left join cer_expense_types on cer_expense_type_id = cer_expense_type " . 
	   "where cer_expense_type = " . id() .
	   " and cer_expense_draft_id = " . param("did") .
	   " order by cer_expense_year";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$expense_type_name = $row["cer_expense_type_name"];
	$amounts[$row["cer_expense_year"]] = $row["cer_expense_amount"];
	$ids[$row["cer_expense_year"]] = $row["cer_expense_id"];
	$edidables[$row["cer_expense_year"]] = $row["cer_expense_type_editable"];
	$comments[$row["cer_expense_year"]] = $row["cer_expense_comment"];
}


$sql = "select * " .
	   "from cer_draft_rent_percent_from_sales " .
	   "where cer_rent_percent_from_sale_draft_id = " . param("did") .
	   " order by cer_rent_percent_from_sale_year";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$sales_percent_ids[$row["cer_rent_percent_from_sale_year"]] = $row["cer_rent_percent_from_sale_id"];
	$sales_percents[$row["cer_rent_percent_from_sale_year"]] = $row["cer_rent_percent_from_sale_percent"];
}

$total_rents = get_total_rents(param("did"));
$total_sales = get_gross_sale_values(param("did"));

foreach($total_sales as $year=>$value)
{
	$total_sales_percents[$year] = "";
	if($value > 0)
	{
		$total_sales_percents[$year] = round(100*$total_rents[$year]/$value, 2);
	}
}



/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_draft_expenses", "CER Expenses");

//$form->add_section("Expenses in " . $currency["symbol"]);

$form->add_hidden("did", param("did"));

$form->add_section($expense_type_name . " in " . $currency["symbol"]);

if(id() == 2) //fixed rental cost
{
	$form->add_comment("Please indicate <strong>ONLY FIXED</strong> rental cost. <strong>DO NOT</strong> add turnover based rental cost here!");
}
elseif(id() == 20) // reduction of rental expenses
{
	$form->add_comment('Please enter the reduction as <span class="error">minus values</span> (negative sign)!');
}

$form_is_editable = 1;
foreach($amounts as $year=>$amount)
{
	if($edidables[$year] == 1)
	{
		$form->add_edit("r" . $ids[$year], "Amount " . $year . " in " . $currency["symbol"], 0, $amount, TYPE_INT, 12);
		
		if(id() == 7 or id() == 10) // other expenses
		{
			$form->add_edit("c" . $ids[$year], "Comment ", 0, $comments[$year]);
		}
		else
		{
			$form->add_hidden("c" . $ids[$year]);
		}
	}
	else
	{
		$form->add_label("r" . $ids[$year], "Amount " . $year . " in " . $currency["symbol"], 0, $amount);
		$form_is_editable = 0;
	}
}

if($form_is_editable ==1)
{
	$form->add_button("save_form", "Save");
}
$form->add_button("back", "Back");

$form->populate();
$form->process();

if($form->button("save_form"))
{
	if($form->validate())
	{
		foreach($ids as $year=>$id)
		{
			$fields = array();
	
			$value = dbquote($form->value("r" . $ids[$year]));
			$fields[] = "cer_expense_amount = " . $value;

			$value = dbquote($form->value("c" . $ids[$year]));
			$fields[] = "cer_expense_comment = " . $value;

			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_draft_expenses set " . join(", ", $fields) . " where cer_expense_id = " . $id;
			mysql_query($sql) or dberror($sql);

	
		}
		
		/*
		if($expense_type != 3 and $expense_type != 6 and $expense_type != 9 and $expense_type != 11) //Annual fixed rents, Marketing Expenses, Contribution
		{
			$result = calculate_forcasted_expenses(id(), $years, $project["project_id"]);
		}
		*/

		$form->message("The data has been saved.");
		$link = "cer_draft_expenses.php?did=" . param("did");
		redirect($link);
	}
}
elseif($form->button("back"))
{
	$link = "cer_draft_expenses.php?did=" . param("did");
	redirect($link);
}

$page = new Page("cer_drafts");
require "include/draft_page_actions.php";
$page->header();
$page->title(id() ? $basicdata['cer_basicdata_title'] . ": Edit " . $expense_type_name : $basicdata['cer_basicdata_title'] . ": Add Expense Position");
require_once("include/tabs_draft.php");
$form->render();

require "include/draft_footer_scripts.php";
$page->footer();

?>