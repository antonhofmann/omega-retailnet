<?php
/********************************************************************

    cer_mails.php

    Mail Box

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";

check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}

set_referer("cer_mail_new.php");
set_referer("cer_mail_reply.php");


if( id()) {
	register_param("pid", id());
}

/********************************************************************
    prepare all data needed
*********************************************************************/
$client_address = get_address($project["order_client_address"]);

$client = $client_address["company"] . ", " .
		  $client_address["zip"] . " " .
          $client_address["place"] . ", " .
          $client_address["country_name"];

$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_address"] . ", " .
		$project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];

/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_summary", "cer_summary");

$form->add_label("shop_address", "POS Location", RENDER_HTML, $shop);
$form->add_label("client_address", "Client", RENDER_HTML, $client);
include("include/project_head.php");


$form->add_hidden("pid", param("pid"));

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    prepare data
*********************************************************************/
$sql_list1 = "select  cer_mail_id, cer_mail_text, cer_mail_attachment1_path, cer_mail_attachment1_title, " .
             "cer_mail_sender , cer_mail_group, concat('<strong>', cer_mail_group, '</strong><br/><br/>', cer_mail_text) as text, " . 
			 "cer_mail_reciepient , " .
			 "cer_mails.date_created as date_sent " . 
             "from cer_mails ";
$list1_filter = "cer_mail_project = " . param("pid");


$mail_links = array();
$mail_delete_links = array();
$sql_images = $sql_list1  . " where " . $list1_filter;

$res = mysql_query($sql_images) or dberror($sql_images);

while ($row = mysql_fetch_assoc($res))
{
	$link = "<a href=\"cer_mail_reply.php?pid=" . param("pid") . "&id=" .  $row["cer_mail_id"] . "\"><img src=\"/pictures/reply_mail.gif\" border='0'/></a>";
	$mail_links[$row["cer_mail_id"]] = $link;
	
	$link = "<a href=\"cer_mail_delete.php?pid=" . param("pid") . "&id=" .  $row["cer_mail_id"] . "\"><img src=\"/pictures/delete.gif\" border='0'/></a>";
	$mail_delete_links[$row["cer_mail_id"]] = $link;
}


/********************************************************************
    build list of selected mails
*********************************************************************/
$list1 = new ListView($sql_list1);

$list1->set_title("Mails Sent");
$list1->set_entity("projects");
$list1->set_filter($list1_filter);
$list1->set_order("date_created DESC");
//$list1->set_group("cer_mail_group");


$list1->add_hidden("pid", param("pid"));

$list1->add_text_column("reply", "", COLUMN_UNDERSTAND_HTML, $mail_links);
$list1->add_column("date_sent", "Date", "", "", "", COLUMN_NO_WRAP);
$list1->add_column("cer_mail_sender", "Sender", "", "", "", COLUMN_NO_WRAP);
$list1->add_column("cer_mail_reciepient", "Reciepients", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK);
//$list1->add_column("cer_mail_text", "Mailtext", "", "", "", COLUMN_BREAK);
$list1->add_column("text", "Mailtext", "", "", "", COLUMN_UNDERSTAND_HTML | COLUMN_BREAK);


$link = "http://" . $_SERVER["HTTP_HOST"] . "/";
$list1->add_column("cer_mail_attachment1_title", "Attachment", $link . "{cer_mail_attachment1_path}", "", "", COLUMN_NO_WRAP);
if(has_access("has_full_access_to_cer"))
{
	$list1->add_text_column("reply", "", COLUMN_UNDERSTAND_HTML, $mail_delete_links);
}

$list1->add_button("new_mail", "Send New Mail");

/********************************************************************
    Populate list and process button clicks
*********************************************************************/ 
$list1->populate();
$list1->process();

if($list1->button("new_mail"))
{
	$link = "cer_mail_new.php?pid=" . param("pid");
	redirect($link);
}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();

if($form_type == "INR03")
{
	$page->title("INR-03 - Retail Furniture in Third-party Store: Mailbox");
}
elseif($form_type == "AF")
{
	$page->title("Application Form: Mailbox");
}
else
{
	$page->title("Capital Expenditure Request: Mailbox");
}
$form->render();
$list1->render();
require "include/footer_scripts.php";
$page->footer();

?>