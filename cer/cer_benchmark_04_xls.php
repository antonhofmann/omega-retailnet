<?php
/********************************************************************

    cer_benchmark_04_xls.php

    Generate Excel File: CER BenchmarkRental Environment Details

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-10-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-10-15
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_access_to_cer_benchmarks");

require_once "../include/xls/Writer.php";
require_once "include/get_functions.php";

/********************************************************************
    prepare Data Needed
*********************************************************************/
$areas = array();
$posarea_totals = array();
$sql = "select * from posareatypes";
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$areas[$row["posareatype_id"]] = $row["posareatype_name"];
	$posarea_totals[$row["posareatype_id"]] = 0;
}
$num_areas = count($areas);

$areaperceptions = array();
$areaperceptions[] = "Class/Image Area";
$areaperceptions[] = "Tourist/Historical Area";
$areaperceptions[] = "Public Transportation";
$areaperceptions[] = "People Traffic Area";
$areaperceptions[] = "Parking Possibilities";
$areaperceptions[] = "Visibility from Pavement";
$areaperceptions[] = "Visibility from accross the Street";
$num_areaperceptions = count($areaperceptions);

$posareaperception_totals = array();
foreach($areaperceptions as $key=>$value)
{
	$posareaperception_totals[$key] = 0;
}


$neighbours = array();
$neighbours[] = "Shop on Left Side";
$neighbours[] = "Shop on Right Side";
$neighbours[] = "Shop Across Left Side";
$neighbours[] = "Shop Across Right Side";
$neighbours[] = "Other Brands in Area";
$num_neighbours = count($neighbours);


include("cer_benchmark_filter.php");

/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$filename = "benchmark_" . $benchmark_shortcut . "_environment_" . date("Ymd") . ".xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename); 
$xls->setVersion(8);
$sheet = $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

//formats
$f_header =& $xls->addFormat();
$f_header->setSize(12);
$f_header->setAlign('left');
$f_header->setBold();

$f_title =& $xls->addFormat();
$f_title->setSize(10);
$f_title->setAlign('left');
$f_title->setBold();

$f_group_title =& $xls->addFormat();
$f_group_title->setSize(10);
$f_group_title->setAlign('left');
$f_group_title->setBold();

$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);

$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);


$f_unused =& $xls->addFormat();
$f_unused->setSize(8);
$f_unused->setBorder(1);
$f_unused->setPattern(2);
$f_unused->setBgColor('silver');

$f_border_left=& $xls->addFormat();
$f_border_left->setTop(1);
$f_border_left->setBottom(1);
$f_border_left->setLeft(1);
$f_border_left->setSize(10);
$f_border_left->setAlign('left');
$f_border_left->setBold();

$f_border_right=& $xls->addFormat();
$f_border_right->setTop(1);
$f_border_right->setBottom(1);
$f_border_right->setRight(1);


$f_border_middle=& $xls->addFormat();
$f_border_middle->setTop(1);
$f_border_middle->setBottom(1);

//captions
$c01 = 0;
$captions = array();
$captions[] = "Project Number";
$sheet->setColumn($c01, $c01, 10);
$c01++;

$captions[] = "Project State";
$sheet->setColumn($c01, $c01, 6);
$c01++;

$captions[] = "Country";
$sheet->setColumn($c01, $c01, 14);
$c01++;


$captions[] = "POS Location Address";
$sheet->setColumn($c01, $c01, 60);
$c01++;

$captions[] = "Product Line";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Product Line Subclass";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Opening Date";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Legal Type";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Project Kind";
$sheet->setColumn($c01, $c01, 8);
$c01++;

$captions[] = "Project Type";
$sheet->setColumn($c01, $c01, 12);
$c01++;

foreach($areas as $key=>$value)
{
	$captions[] = $value;
	$sheet->setColumn($c01, $c01, 2);
	$c01++;
}

foreach($areaperceptions as $key=>$value)
{
	$captions[] = $value;
	$sheet->setColumn($c01, $c01, 3);
	$c01++;
}

foreach($neighbours as $key=>$value)
{
	$captions[] = $value;
	
	if($key == 4)
	{
		$sheet->setColumn($c01, $c01, 36);
	}
	else
	{
		$sheet->setColumn($c01, $c01, 12);
	}
	$c01++;
}


/********************************************************************
    write all captions
*********************************************************************/
$sheet->write(0, 0, $benchmark_title, $f_header);
$sheet->write(1, 0, "Environment Details (" . date("d.m.Y, H:m:s") . ")", $f_title);

$c1 = 8;
$c2 = $c1 + 1;
$c3 = $c1 + $num_areas - 1;
$count = $num_areas - 2;
$sheet->write(3,$c1,"Areas", $f_border_left);
for($i=0;$i<$count;$i++)
{
	$sheet->write(3,$c2+$i,"", $f_border_middle);
}
$sheet->write(3,$c3,"", $f_border_right);

$c1 = $c1 + $num_areas;
$c2 = $c1 + 1;
$c3 = $c1 + $num_areaperceptions - 1;
$count = $num_areaperceptions - 2;
$sheet->write(3,$c1,"Area perception", $f_border_left);
for($i=0;$i<$count;$i++)
{
	$sheet->write(3,$c2+$i,"", $f_border_middle);
}
$sheet->write(3,$c3,"", $f_border_right);

$c1 = $c1 + $num_areaperceptions;
$c2 = $c1 + 1;
$c3 = $c1 + $num_neighbours - 1;
$count = $num_neighbours - 2;
$sheet->write(3,$c1,"Neighbourhood", $f_border_left);
for($i=0;$i<$count;$i++)
{
	$sheet->write(3,$c2+$i,"", $f_border_middle);
}
$sheet->write(3,$c3,"", $f_border_right);


$sheet->setRow(4, 165);
$sheet->writeRow(4, 0, $captions, $f_caption);

/********************************************************************
    write all data
*********************************************************************/
$list_totals = array();
$num_of_projects = 0;

$row_index = 5;
$c01 = 0;

foreach($posorders as $key=>$posorder)
{
	if($posorder["pipeline"] == 1)
	{
		$sql = "select * " . 
			   "from posaddressespipeline " . 
			   "left join posorderspipeline on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " . 
			   "left join orders on order_id = project_order " .
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "where posorder_type = 1 and posorder_id = " . $posorder["id"];
	}
	elseif($posorder["pipeline"] == 2)
	{
		$sql = "select * " . 
			   "from posaddresses " . 
			   "left join posorderspipeline on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " . 
			   "left join orders on order_id = project_order " .
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "where posorder_type = 1 and posorder_id = " . $posorder["id"];
	}
	else
	{
		$sql = "select * " . 
			   "from posaddresses " . 
			   "left join posorders on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " . 
			   "left join orders on order_id = project_order " .
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "where posorder_type = 1 and posorder_id = " . $posorder["id"];
	}

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$num_of_projects++;
		$sheet->write($row_index, $c01, $row["posorder_ordernumber"], $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["order_actual_order_state_code"], $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["country_name"], $f_normal);
		$c01++;
		$tmp = $row["posaddress_place"] . ", " . $row["posaddress_name"] . ", " . $row["posaddress_address"];
		$sheet->write($row_index, $c01, $tmp, $f_normal);
		$c01++;
		
		
		$sheet->write($row_index, $c01, $row["product_line_name"], $f_normal);
		$c01++;
		
		$sheet->write($row_index, $c01, $row["productline_subclass_name"], $f_normal);
		$c01++;
		

		$sheet->write($row_index, $c01, to_system_date($row["posaddress_store_openingdate"]), $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["project_costtype_text"], $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["projectkind_name"], $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["postype_name"], $f_normal);
		$c01++;

		//areas
		$posareas = array();
		foreach($areas as $key=>$value)
		{
			$posareas[$key] = "";
		}
		
		if($posorder["pipeline"] == 1)
		{
			$sql_s = "select posarea_area " . 
					 "from posareaspipeline " . 
					 "where posarea_posaddress = " . $row["posaddress_id"];
		}
		else
		{
			$sql_s = "select posarea_area " . 
					 "from posareas " . 
					 "where posarea_posaddress = " . $row["posaddress_id"];
		}
		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		while($row_s = mysql_fetch_assoc($res_s))
		{
			$posareas[$row_s["posarea_area"]] = "x";
			$posarea_totals[$row_s["posarea_area"]] = $posarea_totals[$row_s["posarea_area"]] + 1;
		}

		foreach($posareas as $key=>$value)
		{
			$sheet->write($row_index, $c01, $value, $f_number);
			$c01++;
		}

		//area peceptions
		if($row["posaddress_perc_class"] > 0)
		{
			$posareaperception_totals[0] = $posareaperception_totals[0] + $row["posaddress_perc_class"];
			$sheet->write($row_index, $c01, $row["posaddress_perc_class"], $f_number);
			$c01++;

		}
		else
		{
			$sheet->write($row_index, $c01, "", $f_number);
			$c01++;
		}

		if($row["posaddress_perc_tourist"] > 0)
		{
			$posareaperception_totals[1] = $posareaperception_totals[1] + $row["posaddress_perc_tourist"];
			$sheet->write($row_index, $c01, $row["posaddress_perc_tourist"], $f_number);
			$c01++;

		}
		else
		{
			$sheet->write($row_index, $c01, "", $f_number);
			$c01++;
		}

		if($row["posaddress_perc_transport"] > 0)
		{
			$posareaperception_totals[2] = $posareaperception_totals[2] + $row["posaddress_perc_transport"];
			$sheet->write($row_index, $c01, $row["posaddress_perc_transport"], $f_number);
			$c01++;

		}
		else
		{
			$sheet->write($row_index, $c01, "", $f_number);
			$c01++;
		}

		if($row["posaddress_perc_people"] > 0)
		{
			$posareaperception_totals[3] = $posareaperception_totals[3] + $row["posaddress_perc_people"];
			$sheet->write($row_index, $c01, $row["posaddress_perc_people"], $f_number);
			$c01++;

		}
		else
		{
			$sheet->write($row_index, $c01, "", $f_number);
			$c01++;
		}

		if($row["posaddress_perc_parking"] > 0)
		{
			$posareaperception_totals[4] = $posareaperception_totals[4] + $row["posaddress_perc_parking"];
			$sheet->write($row_index, $c01, $row["posaddress_perc_parking"], $f_number);
			$c01++;

		}
		else
		{
			$sheet->write($row_index, $c01, "", $f_number);
			$c01++;
		}

		if($row["posaddress_perc_visibility1"] > 0)
		{
			$posareaperception_totals[5] = $posareaperception_totals[5] + $row["posaddress_perc_visibility1"];
			$sheet->write($row_index, $c01, $row["posaddress_perc_visibility1"], $f_number);
			$c01++;

		}
		else
		{
			$sheet->write($row_index, $c01, "", $f_number);
			$c01++;
		}

		if($row["posaddress_perc_visibility2"] > 0)
		{
			$posareaperception_totals[6] = $posareaperception_totals[6] + $row["posaddress_perc_visibility2"];
			$sheet->write($row_index, $c01, $row["posaddress_perc_visibility2"], $f_number);
			$c01++;

		}
		else
		{
			$sheet->write($row_index, $c01, "", $f_number);
			$c01++;
		}

		//Neighbourhood
		$sheet->write($row_index, $c01, $row["posorder_neighbour_left"], $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["posorder_neighbour_right"], $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["posorder_neighbour_acrleft"], $f_normal);
		$c01++;

		$sheet->write($row_index, $c01, $row["posorder_neighbour_acrright"], $f_normal);
		$c01++;
		
		$other = str_replace("\r\n", "", $row["posorder_neighbour_brands"]);
		$other = str_replace("\r", "",$other);
		$other = str_replace("\n", "",$other);

		$sheet->write($row_index, $c01, $other, $f_normal);
		$c01++;

		

		$c01 = 0;
		$row_index++;

	}
}

if(count($posorders) > 0)
{
	$c01 = 7;
	$row_index++;
	$sheet->write($row_index, $c01,"Sum", $f_normal);
	$c01++;

	foreach($posarea_totals as $key=>$value)
	{
		$sheet->write($row_index, $c01,$value, $f_number);
		$c01++;
	}

	foreach($areaperceptions as $key=>$value)
	{
		$sheet->write($row_index, $c01,"", $f_unused);
		$c01++;
	}

	foreach($neighbours as $key=>$value)
	{
		$sheet->write($row_index, $c01,"", $f_unused);
		$c01++;
	}



	$c01 = 7;
	$row_index++;
	$sheet->write($row_index, $c01,"Average", $f_normal);
	$c01++;

	foreach($areas as $key=>$value)
	{
		$sheet->write($row_index, $c01,"", $f_unused);
		$c01++;
	}

	foreach($posareaperception_totals as $key=>$value)
	{
		$avg = 0;
		if($num_of_projects > 0)
		{
			$avg = round($value/$num_of_projects, 1);
		}
		$sheet->write($row_index, $c01, number_format($avg, 1), $f_number);
		$c01++;
	}

	foreach($neighbours as $key=>$value)
	{
		$sheet->write($row_index, $c01,"", $f_unused);
		$c01++;
	}
}

$xls->close(); 

?>