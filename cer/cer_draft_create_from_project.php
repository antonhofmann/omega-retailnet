<?php
/********************************************************************

    cer_draft_create_from_project.php

    Create Draft from existing projects

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-03-17
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-03-17
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require "include/get_draft_functions.php";
check_access("has_access_to_cer_drafts");


/********************************************************************
    prepare all data needed
*********************************************************************/


$sql_countries = "select DISTINCT country_id, country_name " . 
				 "from cer_basicdata " . 
				 " inner join projects on project_id = cer_basicdata_project " .
				 " inner join orders on order_id = project_order " . 
				 " inner join countries on country_id = order_shop_address_country " . 
				 " order by country_name";


$list_filter = " where order_user = " . user_id();

if(has_access("cer_has_full_access_to_his_projects")) //only his projects
{

	$list_filter = " where order_user = " . user_id();
	//get the team members
	$user_list = '';
	$sql = 'select address_id from addresses ' .
		   'left join users on user_address = address_id ' . 
		   'where user_id = ' . user_id();

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$sql = 'select user_id from users ' .
			   'where user_address = ' . $row["address_id"];

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$user_list .= $row['user_id'] . ',';
		}

		$user_list = substr($user_list, 0, strlen($user_list)-1);


		$sql = "select cer_basicdata_id, cer_basicdata_title, concat(user_name, ' ', user_firstname) as uname,  " .
		       "cer_drafts.date_created as cdate, cer_drafts.date_modified as mdate " . 
		       "from cer_drafts " . 
		       "left join users on user_id = cer_basicdata_user_id";
		$list_filter = 'where order_user IN (' . $user_list . ')';
	}
}
elseif(has_access("has_access_to_his_cer")) //only his projects
{
	$list_filter = " where order_user = " . user_id();
}
else
{
	if(param("country_id")) {
		$list_filter = " where country_id = " . param("country_id");
	}
}


$sql_cers = "select cer_basicdata_id, concat(order_shop_address_place, ': ', order_shop_address_company, " . 
            "'(', order_number, ')') as project_name " . 
			"from cer_basicdata " . 
			" inner join projects on project_id = cer_basicdata_project " .
			" inner join orders on order_id = project_order " . 
			" inner join countries on country_id = order_shop_address_country " .
			$list_filter . 
			" and cer_basicdata_version = 0 and cer_basicdata_firstyear > 0 and cer_basicdata_lastyear > 0 " . 
			" order by project_name ";


$sql_countries = "select DISTINCT country_id, country_name " . 
				 "from cer_basicdata " . 
				 " inner join projects on project_id = cer_basicdata_project " .
				 " inner join orders on order_id = project_order " . 
				 " inner join countries on country_id = order_shop_address_country " . 
				 " where cer_basicdata_version = 0 
				  order by country_name";

if(has_access("has_access_to_his_cer")) //only his projects
{
	$sql_countries = "select DISTINCT country_id, country_name " . 
				 "from cer_basicdata " . 
				 " inner join projects on project_id = cer_basicdata_project " .
				 " inner join orders on order_id = project_order " . 
				 " inner join countries on country_id = order_shop_address_country " . 
				 " where cer_basicdata_version = 0 
	               and order_user = " . user_id() .
				 " order by country_name";
}
else {
	$sql_countries = "select DISTINCT country_id, country_name " . 
				 "from cer_basicdata " . 
				 " inner join projects on project_id = cer_basicdata_project " .
				 " inner join orders on order_id = project_order " . 
				 " inner join countries on country_id = order_shop_address_country " . 
				 " where cer_basicdata_version = 0 
				  order by country_name";
}


$years = array();

$s = date("Y");

$l = $s+21;
for($i=$s;$i<$l;$i++)
{
	$years[$i] = $i;
}


$form = new Form("cer_drafts", "CER/AF Draft");

$form->add_section("Draft");
$form->add_hidden("cer_basicdata_user_id", user_id());
$form->add_edit("cer_basicdata_title", "Title of the Draft*", NOTNULL);


if(has_access("has_access_to_drafts_of_his_country")) {
	$form->add_list("cer_af_id", "CER/AF*", $sql_cers, NOTNULL);
}
else
{
	$form->add_list("country_id", "Country*", $sql_countries, NOTNULL | SUBMIT);

	if(param("country_id")) {
		$form->add_list("cer_af_id", "CER/AF*", $sql_cers, NOTNULL);
	}

}
	
$form->add_button("create_draft", "Create Draft");

$form->add_list("first_year", "First Year*", $years, NOTNULL);

$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();
$form->process();


if($form->button("create_draft") and $form->validate()) {
	
	$id = 0;

	$year_offset = 0;
	
	$sql = "select * from cer_basicdata  " . 
		   " inner join projects on project_id = cer_basicdata_project " .
		   " inner join orders on order_id = project_order " . 
		   " inner join project_costs on project_cost_order = order_id " . 
		   "where cer_basicdata_version = 0 and cer_basicdata_id = " . $form->value("cer_af_id");


	$res = mysql_query($sql) or dberror($sql);
	if ($row_p = mysql_fetch_assoc($res))
	{

		$year_offset = $form->value("first_year") - $row_p['cer_basicdata_firstyear'];

		$exchange_rate = $row_p['cer_basicdata_exchangerate'];
		$factor = $row_p['cer_basicdata_factor'];

		$sql_c = "select * from currencies where currency_id = " . $row_p['cer_basicdata_currency'];
		$res_c = mysql_query($sql_c) or dberror($sql_c);
		if ($row_c = mysql_fetch_assoc($res_c))
		{
			
			$exchange_rate = $row_c['currency_exchange_rate'];
			$factor = $row_c['currency_factor'];
		
		}

		
		$fields = '';
		$values = '';
		
		$fields .= 'cer_basicdata_title' . ',';
		$values .= dbquote($form->value("cer_basicdata_title")) . ',';

		$fields .= 'cer_basicdata_user_id' . ',';
		$values .= user_id() . ',';


		$fields .= 'cer_basicdata_legal_type' . ',';
		$values .= dbquote($row_p['project_cost_type']) . ',';

		$fields .= 'cer_basicdata_country' . ',';
		$values .= dbquote($row_p['order_shop_address_country']) . ',';

		$fields .= 'cer_basicdata_currency' . ',';
		$values .= dbquote($row_p['cer_basicdata_currency']) . ',';

		$fields .= 'cer_basicdata_exchangerate' . ',';
		$values .= dbquote($exchange_rate) . ',';

		$fields .= 'cer_basicdata_factor' . ',';
		$values .= dbquote($factor) . ',';

		$fields .= 'cer_basicdata_firstyear' . ',';
		$values .= dbquote(($row_p['cer_basicdata_firstyear'] + $year_offset)) . ',';

		$fields .= 'cer_basicdata_firstmonth' . ',';
		$values .= dbquote($row_p['cer_basicdata_firstmonth']) . ',';

		$fields .= 'cer_basicdata_lastyear' . ',';
		$values .= dbquote(($row_p['cer_basicdata_lastyear'] + $year_offset)) . ',';

		$fields .= 'cer_basicdata_lastmonth' . ',';
		$values .= dbquote($row_p['cer_basicdata_lastmonth']) . ',';

		
		$fields .= 'cer_basicdata_dicount_rate' . ',';
		$values .= dbquote($row_p['cer_basicdata_dicount_rate']) . ',';

		$fields .= 'cer_bascidata_liquidation_keymoney' . ',';
		$values .= dbquote($row_p['cer_bascidata_liquidation_keymoney']) . ',';

		$fields .= 'cer_basicdata_liquidation_deposit' . ',';
		$values .= dbquote($row_p['cer_basicdata_liquidation_deposit']) . ',';

		$fields .= 'cer_bascidata_liquidation_stock' . ',';
		$values .= dbquote($row_p['cer_bascidata_liquidation_stock']) . ',';

		$fields .= 'cer_bascicdate_liquidation_staff' . ',';
		$values .= dbquote($row_p['cer_bascicdate_liquidation_staff']) . ',';

		$fields .= 'cer_basicdata_residual_value' . ',';
		$values .= dbquote($row_p['cer_basicdata_residual_value']) . ',';

		$fields .= 'cer_basicdata_firstyear_depr' . ',';
		$values .= dbquote($year_offset+$row_p['cer_basicdata_firstyear_depr']) . ',';

		$fields .= 'cer_basicdata_firstmonth_depr' . ',';
		$values .= dbquote($row_p['cer_basicdata_firstmonth_depr']) . ',';

		$fields .= 'cer_basicdata_residual_depryears' . ',';
		$values .= dbquote($row_p['cer_basicdata_residual_depryears']) . ',';

		$fields .= 'cer_basicdata_residual_deprmonths' . ',';
		$values .= dbquote($row_p['cer_basicdata_residual_deprmonths']) . ',';

		$fields .= 'cer_basicdata_residualkeymoney_value' . ',';
		$values .= dbquote($row_p['cer_basicdata_residualkeymoney_value']) . ',';

		$fields .= 'cer_basicdata_residualkeymoney_depryears' . ',';
		$values .= dbquote($row_p['cer_basicdata_residualkeymoney_depryears']) . ',';

		$fields .= 'cer_basicdata_residualkeymoney_deprmonths' . ',';
		$values .= dbquote($row_p['cer_basicdata_residualkeymoney_deprmonths']) . ',';

		$fields .= 'cer_basicdata_recoverable_keymoney' . ',';
		$values .= dbquote($row_p['cer_basicdata_recoverable_keymoney']) . ',';

		$fields .= 'cer_basicdata_inflationrates' . ',';
		$values .= dbquote($row_p['cer_basicdata_inflationrates']) . ',';

		$fields .= 'cer_basic_data_interesrates' . ',';
		$values .= dbquote($row_p['cer_basic_data_interesrates']) . ',';

		$fields .= 'cer_basicdata_calculate_rent_avg' . ',';
		$values .= dbquote($row_p['cer_basicdata_calculate_rent_avg']) . ',';

		$fields .= 'cer_basicdata_add_tob_rents' . ',';
		$values .= dbquote($row_p['cer_basicdata_add_tob_rents']) . ',';

		$fields .= 'cer_extend_depreciation_period' . ',';
		$values .= dbquote($row_p['cer_extend_depreciation_period']) . ',';

		$fields .= 'cer_basicdata_salary_growth' . ',';
		$values .= dbquote($row_p['cer_basicdata_salary_growth']) . ',';

		$fields .= 'cer_basicdata_rental_increase_mode' . ',';
		$values .= dbquote($row_p['cer_basicdata_rental_increase_mode']) . ',';

		$fields .= 'cer_basicdata_rental_index_mode' . ',';
		$values .= dbquote($row_p['cer_basicdata_rental_index_mode']) . ',';

		$fields .= 'cer_basicdata_tob_from_net_sales' . ',';
		$values .= dbquote($row_p['cer_basicdata_tob_from_net_sales']) . ',';

		$fields .= 'cer_basicdata_commission_from_net_sales' . ',';
		$values .= dbquote($row_p['cer_basicdata_commission_from_net_sales']) . ',';

		$fields .= 'cer_basicdata_commission' . ',';
		$values .= dbquote($row_p['cer_basicdata_commission']) . ',';

		$fields .= 'cer_basicdata_commission_tax_percent' . ',';
		$values .= dbquote($row_p['cer_basicdata_commission_tax_percent']) . ',';

		$fields .= 'cer_basicdata_cost_on_net_sales' . ',';
		$values .= dbquote($row_p['cer_basicdata_cost_on_net_sales']) . ',';

		$fields .= 'cer_basicdata_tob_from_breakpoint_difference' . ',';
		$values .= dbquote($row_p['cer_basicdata_tob_from_breakpoint_difference']) . ',';

		$fields .= 'cer_basicdata_revenue_p1' . ',';
		$values .= dbquote($row_p['cer_basicdata_revenue_p1']) . ',';

		$fields .= 'cer_basicdata_revenue_p2' . ',';
		$values .= dbquote($row_p['cer_basicdata_revenue_p2']) . ',';

		$fields .= 'cer_basicdata_revenue_p3' . ',';
		$values .= dbquote($row_p['cer_basicdata_revenue_p3']) . ',';

		$fields .= 'cer_basicdata_revenue_p4' . ',';
		$values .= dbquote($row_p['cer_basicdata_revenue_p4']) . ',';


		/*
		$fields .= 'cer_basicdata_future_investment' . ',';
		$values .= dbquote($row_p['cer_basicdata_future_investment']) . ',';

		$fields .= 'cer_basicdata_future_depr_start_year' . ',';
		$values .= dbquote($row_p['cer_basicdata_future_depr_start_year']) . ',';

		$fields .= 'cer_basicdata_future_depr_start_month' . ',';
		$values .= dbquote($row_p['cer_basicdata_future_depr_start_month']) . ',';
		*/

		if($row_p["project_order"] > 0)
		{

			$posdata = get_pos_data($row_p["project_order"]);
			$posleases = get_pos_leasedata($posdata["posaddress_id"], $row_p["project_order"]);
			
			if(count($posleases) > 0)
			{
				$fields .= 'cer_basicdata_lease_type' . ',';
				$values .= '1,';

				
				$tmp = $year_offset + 1*substr($posleases['poslease_startdate'], 0, 4);
				$tmp = $tmp . substr($posleases['poslease_startdate'], 4, 6);
				$fields .= 'cer_basicdata_lease_startdate' . ',';
				$values .= dbquote($tmp) . ',';

				$tmp = $year_offset + 1*substr($posleases['poslease_enddate'], 0, 4);
				$tmp = $tmp . substr($posleases['poslease_startdate'], 4, 6);
				$fields .= 'cer_basicdata_lease_enddate' . ',';
				$values .= dbquote($tmp) . ',';
				
				$fields .= 'cer_basicdata_indexclause_in_contract' . ',';
				$values .= dbquote($posleases['poslease_indexclause_in_contract']) . ',';

				$fields .= 'cer_basicdata_average_increase' . ',';
				$values .= dbquote($posleases['poslease_average_increase']) . ',';

				$fields .= 'cer_basicdata_realestate_fee' . ',';
				$values .= dbquote($posleases['poslease_realestate_fee']) . ',';

				$fields .= 'cer_basicdata_indexrate' . ',';
				$values .= dbquote($posleases['poslease_indexrate']) . ',';

				$fields .= 'cer_basicdata_annual_charges' . ',';
				$values .= dbquote($posleases['poslease_annual_charges']) . ',';

				$fields .= 'cer_basicdata_other_fees' . ',';
				$values .= dbquote($posleases['poslease_other_fees']) . ',';

			}
		}
		

		$fields .= 'user_created' . ',';
		$values .= dbquote(user_id()) . ',';

		$fields .= 'date_created' . ',';
		$values .= dbquote(date("Y-m-d H:i:s")) . ',';

		$fields = substr($fields, 0, strlen($fields)-1);
		$values = substr($values, 0, strlen($values)-1);

		

		$sql_i = 'insert into cer_drafts (' . $fields . ') VALUES (' . $values . ')';
		$result = mysql_query($sql_i) or dberror($sql_i);

		$id = mysql_insert_id();


		

	}

	if($id > 0)
	{
		//expenses
		$sql = 'Select * from cer_expenses ' . 
			   'where cer_expense_cer_version = 0 and cer_expense_project =' . $row_p['project_id'];
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				if($field_name == 'cer_expense_project') {
					$fields .= 'cer_expense_draft_id,';
					$values .= $id . ',';
				}
				elseif($field_name == 'cer_expense_year')
				{
					$fields .= $field_name . ',';
					$values .= dbquote(($value + $year_offset)) . ',';
					
				}
				elseif($field_name == 'cer_expense_cer_version')
				{
					//NOP
				}
				elseif($field_name != 'cer_expense_id') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_draft_expenses (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}

		//new additional rental costs
		//additional rental costs
		$sql = 'Select * from cer_additional_rental_costs ' . 
			   'where cer_additional_rental_cost_cer_version = 0 and cer_additional_rental_cost_project =' . $row_p['project_id'];
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				if($field_name == 'cer_additional_rental_cost_project') {
					$fields .= 'cer_additional_rental_cost_draft_id,';
					$values .= $id . ',';
				}
				elseif($field_name == 'cer_additional_rental_cost_cer_version')
				{
					//NOP
				}
				elseif($field_name != 'cer_additional_rental_cost_id') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_draft_additional_rental_costs (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		}


		$sql = 'Select * from cer_additional_rental_cost_amounts ' . 
			   'where cer_additional_rental_cost_amount_version = 0 and cer_additional_rental_cost_amount_project =' . $row_p['project_id'];
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				if($field_name == 'cer_additional_rental_cost_amount_project') {
					$fields .= 'cer_additional_rental_cost_amount_draft_id,';
					$values .= $id . ',';
				}
				elseif($field_name == 'cer_additional_rental_cost_amount_year')
				{
					$fields .= $field_name . ',';
					$values .= dbquote(($value + $year_offset)) . ',';
					
				}
				elseif($field_name == 'cer_additional_rental_cost_amount_version')
				{
					//NOP
				}
				elseif($field_name != 'cer_additional_rental_cost_amount_id') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_draft_additional_rental_cost_amounts (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		}

		//end new additional rental costs


		//turnover based rentals
		$sql = 'Select * from cer_rent_percent_from_sales ' . 
			   'where cer_rent_percent_from_sale_cer_version = 0 and cer_rent_percent_from_sale_project =' . $row_p['project_id'];
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				if($field_name == 'cer_rent_percent_from_sale_project') {
					$fields .= 'cer_rent_percent_from_sale_draft_id,';
					$values .= $id . ',';
				}
				elseif($field_name == 'cer_rent_percent_from_sale_year')
				{
					$fields .= $field_name . ',';
					$values .= dbquote(($value + $year_offset)) . ',';
					
				}
				elseif($field_name == 'cer_rent_percent_from_sale_cer_version')
				{
					//NOP
				}
				elseif($field_name != 'cer_rent_percent_from_sale_id') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_draft_rent_percent_from_sales (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//fixed rents
		$sql = 'Select * from cer_fixed_rents ' . 
			   'where cer_fixed_rent_cer_version = 0 and cer_fixed_rent_project_id =' . $row_p['project_id'];
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				if($field_name == 'cer_fixed_rent_project_id') {
					$fields .= 'cer_fixed_rent_draft_id,';
					$values .= $id . ',';
				}
				elseif($field_name == 'cer_fixed_rent_from_year')
				{
					$fields .= $field_name . ',';
					$values .= dbquote(($value + $year_offset)) . ',';
					
				}
				elseif($field_name == 'cer_fixed_rent_to_year')
				{
					$fields .= $field_name . ',';
					$values .= dbquote(($value + $year_offset)) . ',';
					
				}
				elseif($field_name == 'cer_fixed_rent_cer_version')
				{
					//NOP
				}
				elseif($field_name != 'cer_fixed_rent_id') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_draft_fixed_rents (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//investments
		$sql = 'Select * from cer_investments ' . 
			   'where cer_investment_cer_version = 0 and cer_investment_project =' . $row_p['project_id'];
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				if($field_name == 'cer_investment_project') {
					$fields .= 'cer_investment_draft_id,';
					$values .= $id . ',';
				}
				elseif($field_name == 'cer_investment_amount_cer_loc_approved'
				       or $field_name == 'cer_investment_amount_additional_cer_loc_approved')
				{
					//do nothing
				}
				elseif($field_name == 'cer_investment_cer_version')
				{
					//NOP
				}
				elseif($field_name != 'cer_investment_id') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_draft_investments (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//payments
		$sql = 'Select * from cer_paymentterms ' . 
			   'where cer_paymentterm_cer_version = 0 and cer_paymentterm_project =' . $row_p['project_id'];
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				if($field_name == 'cer_paymentterm_project') {
					$fields .= 'cer_paymentterm_draft_id,';
					$values .= $id . ',';
				}
				elseif($field_name == 'cer_paymentterm_year')
				{
					$fields .= $field_name . ',';
					$values .= dbquote(($value + $year_offset)) . ',';
					
				}
				elseif($field_name == 'cer_paymentterm_cer_version')
				{
					//NOP
				}
				elseif($field_name != 'cer_paymentterm_id') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_draft_paymentterms (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//revenues
		$sql = 'Select * from cer_revenues ' . 
			   'where cer_revenue_cer_version = 0 and cer_revenue_project =' . $row_p['project_id'];
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				if($field_name == 'cer_revenue_project') {
					$fields .= 'cer_revenue_draft_id,';
					$values .= $id . ',';
				}
				elseif($field_name == 'cer_revenue_year')
				{
					$fields .= $field_name . ',';
					$values .= dbquote(($value + $year_offset)) . ',';
					
				}
				elseif($field_name == 'cer_revenue_cer_version')
				{
					//NOP
				}
				elseif($field_name != 'cer_revenue_id') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_draft_revenues (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//salaries
		$sql = 'Select * from cer_salaries ' . 
			   'where cer_salary_cer_version = 0 and cer_salary_project =' . $row_p['project_id'];
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				if($field_name == 'cer_salary_project') {
					$fields .= 'cer_salary_draft_id,';
					$values .= $id . ',';
				}
				elseif($field_name == 'cer_salary_year_starting')
				{
					$fields .= $field_name . ',';
					$values .= dbquote(($value + $year_offset)) . ',';
					
				}
				elseif($field_name == 'cer_salary_cer_version')
				{
					//NOP
				}
				elseif($field_name != 'cer_salary_id') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_draft_salaries (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//stocks
		$sql = 'Select * from cer_stocks ' . 
			   'where cer_stock_cer_version = 0 and cer_stock_project =' . $row_p['project_id'];

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = '';
			$values = '';
			foreach($row as $field_name=>$value)
			{
				if($field_name == 'cer_stock_project') {
					$fields .= 'cer_stock_draft_id,';
					$values .= $id . ',';
				}
				elseif($field_name == 'cer_stock_year')
				{
					$fields .= $field_name . ',';
					$values .= dbquote(($value + $year_offset)) . ',';
					
				}
				elseif($field_name == 'cer_stock_cer_version')
				{
					//NOP
				}
				elseif($field_name != 'cer_stock_id') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_draft_stocks (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);

			echo $sql_i . "<br />";
		
		}

		//adapt all timedepenent data
		//get standardparameters
		$sparams = array();
		$sql = "select * from cer_standardparameters";
		$res = mysql_query($sql) or dberror($sql);
		while($row = mysql_fetch_assoc($res))
		{
			$sparams[$row["cer_standardparameter_id"]] = $row["cer_standardparameter_value"];
		}

		//get inflationrates
		$inflationrates = array();
		$sql = "select * from cer_inflationrates " . 
			   "where inflationrate_country = " . $row_p['order_shop_address_country'] . 
			   " order by inflationrate_year";

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$inflationrates[$row["inflationrate_year"]] = $row["inflationrate_rate"];
		}

		$inflationrates = serialize($inflationrates);
		
		//get interestrates
		$interestrates = array();

		$sql = "select * " .
			   "from cer_interestrates where interestrate_country = " . $row_p['order_shop_address_country']  . 
			   " order by interestrate_year ASC";

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$interestrates[$row["interestrate_year"]] = $row["interestrate_rate"];
		}

		$interestrates = serialize($interestrates);

		//update draft
		$fields = array();


		$fields[] = "cer_basicdata_dicount_rate  = " . dbquote($sparams[1]);
		$fields[] = "cer_bascidata_liquidation_keymoney = " . dbquote($sparams[2]);
		$fields[] = "cer_basicdata_liquidation_deposit = " . dbquote($sparams[3]);
		$fields[] = "cer_bascidata_liquidation_stock = " . dbquote($sparams[4]);
		$fields[] = "cer_bascicdate_liquidation_staff = " . dbquote($sparams[5]);
		$fields[] = "cer_basicdata_inflationrates = " . dbquote($inflationrates);
		$fields[] = "cer_basic_data_interesrates = " . dbquote($interestrates);

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}

		$sql = "update cer_drafts set " . join(", ", $fields) . " where cer_basicdata_id = " . $id;
		mysql_query($sql) or dberror($sql);

		$years = array();

		$fy = $row_p['cer_basicdata_firstyear'] + $year_offset;
		$ly = $row_p['cer_basicdata_lastyear'] + $year_offset;

	
		for($i = $fy;$i<=$ly;$i++)
		{
			
			$years[] = $i;
		}

		$result = calculate_forcasted_salaries($id, $years, $row_p['order_shop_address_country']);

	}

	$link = '/cer/cer_drafts.php?selected_country=' . param("country_id");
	redirect($link);
	
}


$page = new Page("cer_drafts");
require "include/draft_page_actions.php";
$page->header();



$page->title("Create Draft from existing Project");

if(id() > 0) {
	require_once("include/tabs_draft.php");
}

$form->render();
$page->footer();

?>