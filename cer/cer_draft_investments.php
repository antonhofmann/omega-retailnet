<?php
/********************************************************************

    cer_draft_investments.php

    Application Form: investment information
    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-02-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-02-16
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";

check_access("has_access_to_cer_drafts");

require "include/get_draft_functions.php";


/********************************************************************
    prepare all data needed
*********************************************************************/
$basicdata = get_draft_basicdata(param("did"));
$currency = get_draft_currency(param("did"));



$months = array();
for($i=1;$i<13;$i++)
{
	$months[$i] = $i;
}

$years = array();

for($i=$basicdata["cer_basicdata_firstyear"];$i<=$basicdata["cer_basicdata_lastyear"];$i++)
{
	$years[$i] = $i;
}


//get investments

$keymoney = get_intangibles(param("did"), 15);
$goodwill = get_intangibles(param("did"), 17);
$construction = get_intangibles(param("did"), 1);
$fixturing = get_intangibles(param("did"), 3);
$architectural = get_intangibles(param("did"), 5);
$equipment = get_intangibles(param("did"), 7);
$deposit = get_intangibles(param("did"), 9);
$other1 = get_intangibles(param("did"), 11);
$merchandising = get_intangibles(param("did"), 18);
$transportation = get_intangibles(param("did"), 19);
$other2 = get_intangibles(param("did"), 13);

$amounts = array();
$depryears = array();
$deprmonths = array();
$cer_totals = 0;
$total_intagibles = 0;

$sql = "select * from cer_draft_investments " .
       "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
	   "where posinvestment_type_intangible = 0 and cer_investment_draft_id = " . param("did");

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	if($row["cer_investment_type"] == 1) //local construction work
	{
		$local_construction_works_for_franchisee_projects = $row["cer_investment_amount_cer_loc"];
	}
	$amounts[$row["cer_investment_id"]] = $row["cer_investment_amount_cer_loc"];
	$depryears[$row["cer_investment_id"]] = $row["cer_investment_depr_years"];
	$deprmonths[$row["cer_investment_id"]] = $row["cer_investment_depr_months"];
	$cer_totals = $cer_totals + $row["cer_investment_amount_cer_loc"];

	if($row['posinvestment_type_intangible'] == 1) {
		$total_intagibles = $total_intagibles + $row["cer_investment_amount_cer_loc"];
	}
}


/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_drafts", "cer_drafts");


$form->add_hidden("did", param("did"));
$form->add_section("Total Investments in " . $currency["symbol"]);
$form->add_label("total_intangible", "Key Money and Goodwill in " . $currency["symbol"], 0, number_format($total_intagibles,2, ".", "'"));
$form->add_label("total_investment", "Investments in " . $currency["symbol"], 0, number_format($cer_totals,2, ".", "'"));
$form->add_label("total", "Total in " . $currency["symbol"], 0, number_format($cer_totals + $total_intagibles,2, ".", "'"));


$form->add_section("Beginning of the Depreciation Period");
$form->add_comment("Please use the month and the year of the agreed opening date.");
$form->add_list("cer_basicdata_firstyear_depr", "First Year of Depreciation Period*", $years, NOTNULL, $basicdata["cer_basicdata_firstyear_depr"]);
$form->add_list("cer_basicdata_firstmonth_depr", "First Month of Depreciation Period*", $months, NOTNULL, $basicdata["cer_basicdata_firstmonth_depr"]);


$form->add_section("Key Money and Goodwill in " . $currency["symbol"]);
$form->add_edit("keymoney", $keymoney["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $keymoney["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
$form->add_edit("goodwill", $goodwill["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $goodwill["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
$form->add_edit("keymoney_depr_years", "Depreciation Period in full Years", 0, $keymoney["cer_investment_depr_years"], TYPE_INT, 4);
$form->add_list("keymoney_depr_months", "Depreciation Period, additional Months", $months, 0, $keymoney["cer_investment_depr_months"]);
$form->add_edit("cer_basicdata_recoverable_keymoney", "Recoverable Keymoney in " . $currency["symbol"], 0, $basicdata["cer_basicdata_recoverable_keymoney"], TYPE_DECIMAL, 12, 0);

$form->add_section("Construction, Store Fixturing and Architectural Costs in " . $currency["symbol"]);

$form->add_edit("construction", $construction["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $construction["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);


$form->add_edit("construction_depr_years", "Depreciation Period in full Years", 0, $construction["cer_investment_depr_years"], TYPE_INT, 4);
$form->add_list("construction_depr_months", "Depreciation Period, additional Months", $months, 0, $construction["cer_investment_depr_months"]);

$form->add_edit("fixturing", $fixturing["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $fixturing["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);


$form->add_edit("fixturing_depr_years", "Depreciation Period in full Years", 0, $fixturing["cer_investment_depr_years"], TYPE_INT, 4);
$form->add_list("fixturing_depr_months", "Depreciation Period, additional Months", $months, 0, $fixturing["cer_investment_depr_months"]);

$form->add_edit("architectural", $architectural["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $architectural["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);

$form->add_edit("architectural_depr_years", "Depreciation Period in full Years", 0, $architectural["cer_investment_depr_years"], TYPE_INT, 4);
$form->add_list("architectural_depr_months", "Depreciation Period, additional Months", $months, 0, $architectural["cer_investment_depr_months"]);

$form->add_section("Other Costs in " . $currency["symbol"]);
$form->add_edit("equipment", $equipment["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $equipment["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
$form->add_edit("equipment_depr_years", "Depreciation Period in full Years", 0, $equipment["cer_investment_depr_years"], TYPE_INT, 4);
$form->add_list("equipment_depr_months", "Depreciation Period, additional Months", $months, 0, $equipment["cer_investment_depr_months"]);

$form->add_comment("");
$form->add_edit("deposit", $deposit["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $deposit["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
$form->add_comment("");
$form->add_edit("other1", $other1["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other1["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
$form->add_edit("other1_depr_years", "Depreciation Period in full Years", 0, $other1["cer_investment_depr_years"], TYPE_INT, 4);
$form->add_list("other1_depr_months", "Depreciation Period, additional Months", $months, 0, $other1["cer_investment_depr_months"]);


$form->add_comment("");
$form->add_edit("merchandising", $merchandising["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $merchandising["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
$form->add_edit("merchandising_depr_years", "Depreciation Period in full Years", 0, $merchandising["cer_investment_depr_years"], TYPE_INT, 4);
$form->add_list("merchandising_depr_months", "Depreciation Period, additional Months", $months, 0, $merchandising["cer_investment_depr_months"]);


$form->add_comment("");
$form->add_edit("transportation", $transportation["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $transportation["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
$form->add_edit("transportation_depr_years", "Depreciation Period in full Years", 0, $transportation["cer_investment_depr_years"], TYPE_INT, 4);
$form->add_list("transportation_depr_months", "Depreciation Period, additional Months", $months, 0, $transportation["cer_investment_depr_months"]);


$form->add_comment("");
$form->add_edit("other2", $other2["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other2["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);


/*
$form->add_section("Investments to Consider in Depreciation for a Later Renovation in " . $currency["symbol"]);
$form->add_edit("cer_basicdata_future_investment", "Future Investment in " . $currency["symbol"], 0, $basicdata["cer_basicdata_future_investment"], TYPE_INT, 12);
$form->add_list("cer_basicdata_future_depr_start_year", "First Year of Depreciation", $years, 0, $basicdata["cer_basicdata_future_depr_start_year"]);
$form->add_list("cer_basicdata_future_depr_start_month", "First Month of Depreciation", $months, 0, $basicdata["cer_basicdata_future_depr_start_month"]);
*/

$form->add_section("Residual Value of Fixed Assets in " . $currency["symbol"] . " (only in case of Renovation or Take Over/Renovation Projects)");
$form->add_edit("cer_basicdata_residual_value", "Residual Value in " . $currency["symbol"], 0, $basicdata["cer_basicdata_residual_value"], TYPE_INT, 12);

$form->add_edit("cer_basicdata_residual_depryears", "Depreciation Period in full Years", 0, $basicdata["cer_basicdata_residual_depryears"], TYPE_INT, 4);
$form->add_list("cer_basicdata_residual_deprmonths", "Depreciation Period, additional Months", $months, 0, $basicdata["cer_basicdata_residual_deprmonths"]);


$form->add_section("Residual Value of Keymoney in " . $currency["symbol"] . " (only in case of Renovation or Take Over/Renovation Projects)");

$form->add_edit("cer_basicdata_residualkeymoney_value", "Residual Value in " . $currency["symbol"], 0, $basicdata["cer_basicdata_residualkeymoney_value"], TYPE_INT, 12);

$form->add_edit("cer_basicdata_residualkeymoney_depryears", "Depreciation Period in full Years", 0, $basicdata["cer_basicdata_residualkeymoney_depryears"], TYPE_INT, 4);
$form->add_list("cer_basicdata_residualkeymoney_deprmonths", "Depreciation Period, additional Months", $months, 0, $basicdata["cer_basicdata_residualkeymoney_deprmonths"]);

$form->add_button("form_save", "Save Data");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if($form->button("form_save"))
{
	if($form->validate())
	{
		//save intangibles type keymoney
		$fields = array();
	
		$value = dbquote($form->value("keymoney"));
		$fields[] = "cer_investment_amount_cer_loc = " . $value;

		$value = dbquote($form->value("keymoney_depr_years"));
		$fields[] = "cer_investment_depr_years = " . $value;

		$value = dbquote($form->value("keymoney_depr_months"));
		$fields[] = "cer_investment_depr_months = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_draft_investments set " . join(", ", $fields) . " where cer_investment_id = " . $keymoney["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		//save intangibles type goodwill
		$fields = array();
    
		$value = dbquote($form->value("goodwill"));
		$fields[] = "cer_investment_amount_cer_loc = " . $value;

		$value = dbquote($form->value("keymoney_depr_years"));
		$fields[] = "cer_investment_depr_years = " . $value;

		$value = dbquote($form->value("keymoney_depr_months"));
		$fields[] = "cer_investment_depr_months = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_draft_investments set " . join(", ", $fields) . " where cer_investment_id = " . $goodwill["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		//save investment type construction
		$fields = array();
    
		$value = dbquote($form->value("construction"));
		$fields[] = "cer_investment_amount_cer_loc = " . $value;

		$value = dbquote($form->value("construction_depr_years"));
		$fields[] = "cer_investment_depr_years = " . $value;

		$value = dbquote($form->value("construction_depr_months"));
		$fields[] = "cer_investment_depr_months = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_draft_investments set " . join(", ", $fields) . " where cer_investment_id = " . $construction["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		//save investment type fixturing
		$fields = array();
    
		$value = dbquote($form->value("fixturing"));
		$fields[] = "cer_investment_amount_cer_loc = " . $value;

		
		$value = dbquote($form->value("fixturing_depr_years"));
		$fields[] = "cer_investment_depr_years = " . $value;

		$value = dbquote($form->value("fixturing_depr_months"));
		$fields[] = "cer_investment_depr_months = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_draft_investments set " . join(", ", $fields) . " where cer_investment_id = " . $fixturing["cer_investment_id"];
		mysql_query($sql) or dberror($sql);


		//save investment type architectural
		$fields = array();
    
		$value = dbquote($form->value("architectural"));
		$fields[] = "cer_investment_amount_cer_loc = " . $value;

		
		$value = dbquote($form->value("architectural_depr_years"));
		$fields[] = "cer_investment_depr_years = " . $value;

		$value = dbquote($form->value("architectural_depr_months"));
		$fields[] = "cer_investment_depr_months = " . $value;

		
		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_draft_investments set " . join(", ", $fields) . " where cer_investment_id = " . $architectural["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		//save investment type equipment
		$fields = array();
    
		$value = dbquote($form->value("equipment"));
		$fields[] = "cer_investment_amount_cer_loc = " . $value;

		
		$value = dbquote($form->value("equipment_depr_years"));
		$fields[] = "cer_investment_depr_years = " . $value;

		$value = dbquote($form->value("equipment_depr_months"));
		$fields[] = "cer_investment_depr_months = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_draft_investments set " . join(", ", $fields) . " where cer_investment_id = " . $equipment["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		//save investment type deposit
		$fields = array();
    
		$value = dbquote($form->value("deposit"));
		$fields[] = "cer_investment_amount_cer_loc = " . $value;


		
		$value1 = "current_timestamp";
		$fields[] = "date_modified = " .$value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_draft_investments set " . join(", ", $fields) . " where cer_investment_id = " . $deposit["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		//save investment type other costs
		$fields = array();
    
		$value = dbquote($form->value("other1"));
		$fields[] = "cer_investment_amount_cer_loc = " . $value;

		
		$value = dbquote($form->value("other1_depr_years"));
		$fields[] = "cer_investment_depr_years = " . $value;

		$value = dbquote($form->value("other1_depr_months"));
		$fields[] = "cer_investment_depr_months = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_draft_investments set " . join(", ", $fields) . " where cer_investment_id = " . $other1["cer_investment_id"];
		mysql_query($sql) or dberror($sql);




		//save investment type merchandising costs
		$fields = array();
    
		$value = dbquote($form->value("merchandising"));
		$fields[] = "cer_investment_amount_cer_loc = " . $value;

		
		$value = dbquote($form->value("merchandising_depr_years"));
		$fields[] = "cer_investment_depr_years = " . $value;

		$value = dbquote($form->value("merchandising_depr_months"));
		$fields[] = "cer_investment_depr_months = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_draft_investments set " . join(", ", $fields) . " where cer_investment_id = " . $merchandising["cer_investment_id"];
		mysql_query($sql) or dberror($sql);


		//save investment type transportation costs
		$fields = array();
    
		$value = dbquote($form->value("transportation"));
		$fields[] = "cer_investment_amount_cer_loc = " . $value;

		
		$value = dbquote($form->value("transportation_depr_years"));
		$fields[] = "cer_investment_depr_years = " . $value;

		$value = dbquote($form->value("transportation_depr_months"));
		$fields[] = "cer_investment_depr_months = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_draft_investments set " . join(", ", $fields) . " where cer_investment_id = " . $transportation["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		//save investment type other non captalized costs
		$fields = array();
    
		$value = dbquote($form->value("other2"));
		$fields[] = "cer_investment_amount_cer_loc = " . $value;


		
		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_draft_investments set " . join(", ", $fields) . " where cer_investment_id = " . $other2["cer_investment_id"];
		mysql_query($sql) or dberror($sql);


		//update cer_basic_data
		$fields = array();
    
		$value = dbquote($form->value("cer_basicdata_firstyear_depr"));
		$fields[] = "cer_basicdata_firstyear_depr = " . $value;

		$value = dbquote($form->value("cer_basicdata_firstmonth_depr"));
		$fields[] = "cer_basicdata_firstmonth_depr = " . $value;


		$value = dbquote($form->value("cer_basicdata_residual_value"));
		$fields[] = "cer_basicdata_residual_value = " . $value;

		$value = dbquote($form->value("cer_basicdata_residual_depryears"));
		$fields[] = "cer_basicdata_residual_depryears = " . $value;

		$value = dbquote($form->value("cer_basicdata_residual_deprmonths"));
		$fields[] = "cer_basicdata_residual_deprmonths = " . $value;

		$value = dbquote($form->value("cer_basicdata_residualkeymoney_value"));
		$fields[] = "cer_basicdata_residualkeymoney_value = " . $value;

		$value = dbquote($form->value("cer_basicdata_residualkeymoney_depryears"));
		$fields[] = "cer_basicdata_residualkeymoney_depryears = " . $value;

		$value = dbquote($form->value("cer_basicdata_residualkeymoney_deprmonths"));
		$fields[] = "cer_basicdata_residualkeymoney_deprmonths = " . $value;

		$value = dbquote($form->value("cer_basicdata_recoverable_keymoney"));
		$fields[] = "cer_basicdata_recoverable_keymoney = " . $value;

		/*
		$value = dbquote($form->value("cer_basicdata_future_investment"));
		$fields[] = "cer_basicdata_future_investment = " . $value;

		$value = dbquote($form->value("cer_basicdata_future_depr_start_year"));
		$fields[] = "cer_basicdata_future_depr_start_year = " . $value;

		$value = dbquote($form->value("cer_basicdata_future_depr_start_month"));
		$fields[] = "cer_basicdata_future_depr_start_month = " . $value;
		*/

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " .$value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_drafts set " . join(", ", $fields) . " where cer_basicdata_id = " . param("did");
		mysql_query($sql) or dberror($sql);

		$form->message("Your data has bee saved.");
	}
}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_drafts");

require "include/draft_page_actions.php";
$page->header();
$page->title($basicdata['cer_basicdata_title'] . ": Investment Information");

require_once("include/tabs_draft.php");
$form->render();

require "include/draft_footer_scripts.php";
$page->footer();

?>