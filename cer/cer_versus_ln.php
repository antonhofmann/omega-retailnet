<?php
/********************************************************************

    cer_verus_ln.php

    CER versus LN, Data and Comments
    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2016-05-23
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2016-05-23
    Version:        1.0.0

    Copyright (c) 2016, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";

check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}


/********************************************************************
    prepare all data needed
*********************************************************************/
//check if business plan can be edited
$can_edit_business_plan = false;
require_once("include/check_if_business_plan_is_editable.php");


$state = is_there_a_difference_between_cer_and_ln(param("pid"));


$pdf_link = "javascript:popup('cer_inr02c_pdf.php?pid=" . param("pid") . "&cid=c1',1024,768);";


//check if LN was approved, milestone id 13
$ln_ok1 = true;
if($ln_basicdata['ln_no_ln_submission_needed'] != 1 
	and $ln_basicdata['ln_basicdata_submitted'] !=  NULL and substr($ln_basicdata['ln_basicdata_submitted'], 0, 4) != '0000') 
{
	$milestone = get_project_milestone($cer_basicdata["cer_basicdata_project"], 13);
	if(count($milestone) > 0 and ($milestone['project_milestone_date'] == NULL or $milestone['project_milestone_date'] == '0000-00-00'))
	{
		$ln_ok1 = false;
	}
}

$investments = number_format($state['total_investments_ln'],0, ".", "'");
$investments .= " / ";
$investments .= number_format($state['total_investments_cer'],0, ".", "'");
$investments .= " / ";
$investments .= number_format($state['total_investments_cer'] - $state['total_investments_ln'],0, ".", "'");

$revenues = number_format($state['total_revenues_ln'],0, ".", "'");
$revenues .= " / ";
$revenues .= number_format($state['total_revenues_cer'],0, ".", "'");
$revenues .= " / ";
$revenues .= number_format($state['total_revenues_cer'] - $state['total_revenues_ln'],0, ".", "'");

$expenses = number_format($state['total_expenses_ln'],0, ".", "'");
$expenses .= " / ";
$expenses .= number_format($state['total_expenses_cer'],0, ".", "'");
$expenses .= " / ";
$expenses .= number_format($state['total_expenses_cer'] - $state['total_expenses_ln'],0, ".", "'");


//ln versus cer
if($ln_basicdata["ln_no_ln_submission_needed"] == 0)
{
	$state = is_there_a_difference_between_cer_and_ln(param("pid"));
	$difference_between_cer_and_ln = $state['state'];

	$delta_limit_in_percent = 0.05;
	$investments_delta = round($state['total_investments_cer'] - $state['total_investments_ln'],0);

	$investments_delta_limit = 100;
	
	if($investments_delta == 0)
	{
		$investments_delta_limit = 0;
	}
	elseif($state['total_investments_cer'])
	{
		$investments_delta_limit = abs($state['total_investments_cer'] - $state['total_investments_ln']) / $state['total_investments_cer'];
		$investments_delta_limit = round(100*$investments_delta_limit, 0);

		if($investments_delta < 0 or abs($investments_delta_limit) < 100*$delta_limit_in_percent)
		{
			$investments_delta_limit = 0;
		}
	}



	$revenues_delta = round($state['total_revenues_cer'] - $state['total_revenues_ln'],0);

	$revenues_delta_limit = 100;
	if($revenues_delta == 0)
	{
		$revenues_delta_limit = 0;
	}
	elseif($state['total_revenues_cer'])
	{
		$revenues_delta_limit = abs($state['total_revenues_cer'] - $state['total_revenues_ln']) / $state['total_revenues_cer'];
		$revenues_delta_limit = round(100*$revenues_delta_limit, 0);

		if(abs($revenues_delta_limit) < 100*$delta_limit_in_percent)
		{
			$revenues_delta_limit = 0;
		}
	}

	$expenses_delta = round($state['total_expenses_cer'] - $state['total_expenses_ln'],0);

	$expenses_delta_limit = 100;
	if($expenses_delta == 0)
	{
		$expenses_delta_limit = 0;
	}
	elseif($state['total_expenses_cer'])
	{
		$expenses_delta_limit = abs($state['total_expenses_cer'] - $state['total_expenses_ln']) / $state['total_expenses_cer'];
		$expenses_delta_limit = round(100*$expenses_delta_limit, 0);

		if(abs($expenses_delta_limit) < 100*$delta_limit_in_percent)
		{
			$expenses_delta_limit = 0;
		}
	}
}
else
{
	$investments_delta_limit = 0;
	$revenues_delta_limit = 0;
	$expenses_delta_limit = 0;
}



/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_basicdata", "cer_basicdata");

include("include/project_head.php");

$form->add_section("Businessplan Data");

$form->add_label("bd01", 'Investments LN / CER / Difference', 0, $investments);
$form->add_label("bd02", 'Totel Revenues LN / CER / Difference', 0, $revenues);
$form->add_label("bd03", 'Totel Cost and Expenses LN / CER / Difference', 0, $expenses);



if($ln_ok1 == true and ($investments_delta_limit > 0 or $revenues_delta_limit != 0 or $expenses_delta_limit > 0))
{
	//$form->add_section("Justification");
	$form->add_section("Comments on deviation (INR-02B) versus LNR (LNR-02A)");
	$form->add_comment("The data you are providing in the CER differ from the one you provided in the LN. Please explain us the reasons.");

	$form->add_comment("Click <a href=\"" . $pdf_link . "\">here</a> the see the <a href=\"" . $pdf_link . "\">CER versus LN data</a>.");
}


$form->add_hidden("pid", param("pid"));


/*
if($can_edit_business_plan == true)
{
	$form->add_multiline("cer_basicdata_cer_versus_ln_comment", "Comments", 10, NOTNULL, $cer_basicdata["cer_basicdata_cer_versus_ln_comment"]);

	$form->add_button("save", "Save Data");
}
else
{
	$form->add_label("cer_basicdata_cer_versus_ln_comment", "Comments", NOTNULL, $cer_basicdata["cer_basicdata_cer_versus_ln_comment"]);
}
*/

if(($ln_ok1 == true and $investments_delta_limit > 0) or $cer_basicdata["cer_basicdata_cer_versus_ln_comment"])
{
	$form->add_comment('The total amount of investments in your CER differs from the amount indicated in the LN by <span class="error">' . number_format($investments_delta,0, ".", "'") . '</span>' . ' ' . $cer_basicdata["currency_symbol"] . '.');
	//$form->add_multiline("cer_basicdata_cer_versus_ln_comment", "Justification for Investments", 10, 0, $cer_basicdata["cer_basicdata_cer_versus_ln_comment"]);
}
else
{
	//$form->add_hidden("cer_basicdata_cer_versus_ln_comment", $cer_basicdata["cer_basicdata_cer_versus_ln_comment"]);
}

if(($ln_ok1 == true and $revenues_delta_limit != 0) or $cer_basicdata["cer_basicdata_cer_versus_ln_comment2"])
{
	$form->add_comment('The total amount of gross sales in your LN differs from the amount indicated in the CER by <span class="error">' . number_format($revenues_delta,0, ".", "'") . '</span>' . ' ' . $cer_basicdata["currency_symbol"] . '.');
	//$form->add_multiline("cer_basicdata_cer_versus_ln_comment2", "Justification for Revenues", 10, 0, $cer_basicdata["cer_basicdata_cer_versus_ln_comment2"]);
}
else
{
	//$form->add_hidden("cer_basicdata_cer_versus_ln_comment2", $cer_basicdata["cer_basicdata_cer_versus_ln_comment2"]);
}

if(($ln_ok1 == true and $expenses_delta_limit > 0) or $cer_basicdata["cer_basicdata_cer_versus_ln_comment3"])
{
	$form->add_comment('The total amount of expenses in your CER differs from the amount indicated in the LN by <span class="error">' . number_format($expenses_delta,0, ".", "'") . '</span>' . ' ' . $cer_basicdata["currency_symbol"] . '.');
	//$form->add_multiline("cer_basicdata_cer_versus_ln_comment3", "Justification for Expenses", 10, 0, $cer_basicdata["cer_basicdata_cer_versus_ln_comment3"]);
}
else
{
	//$form->add_hidden("cer_basicdata_cer_versus_ln_comment3", $cer_basicdata["cer_basicdata_cer_versus_ln_comment3"]);
}

if($ln_ok1 == true and ($investments_delta_limit > 0 or $revenues_delta_limit != 0 or $expenses_delta_limit > 0))
{
	
	
	$form->add_multiline("cer_basicdata_cer_versus_ln_comment4", "Country Comment", 10, 0, $cer_basicdata["cer_basicdata_cer_versus_ln_comment4"]);

	$form->add_multiline("cer_basicdata_cer_versus_ln_comment5", "Brand HQ Comment", 10, 0, $cer_basicdata["cer_basicdata_cer_versus_ln_comment5"]);

	$form->add_button("save", "Save Data");
}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if($form->button("save") and $form->validate())
{
	/*
	$sql = "update cer_basicdata SET " .
		   " cer_basicdata_cer_versus_ln_comment = " . dbquote($form->value("cer_basicdata_cer_versus_ln_comment")) . ", " .
		   " cer_basicdata_cer_versus_ln_comment2 = " . dbquote($form->value("cer_basicdata_cer_versus_ln_comment2")) . ", " .
		   " cer_basicdata_cer_versus_ln_comment3 = " . dbquote($form->value("cer_basicdata_cer_versus_ln_comment3")) .
		   " where cer_basicdata_version = 0 " . 
		   " and cer_basicdata_project = " . dbquote(param("pid"));
	*/

	$sql = "update cer_basicdata SET " .
		   " cer_basicdata_cer_versus_ln_comment4 = " . dbquote($form->value("cer_basicdata_cer_versus_ln_comment4")) . ", " .
		   " cer_basicdata_cer_versus_ln_comment5 = " . dbquote($form->value("cer_basicdata_cer_versus_ln_comment5")) .
		   " where cer_basicdata_version = 0 " . 
		   " and cer_basicdata_project = " . dbquote(param("pid"));

	$result = mysql_query($sql) or dberror($sql);
	$form->message("Your data has been saved.");
}



/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();


$page->title("CER versus LN");


$form->render();


require "include/footer_scripts.php";
$page->footer();

?>