<?php
/********************************************************************

    cer_downloads.php

    Lists CER Downloads

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-03-29
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-03-29
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_access_to_cer");
set_referer("cer_download.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
//get all data from interest rates

$year_months = array();
$rates = array();

$sql = "select cer_download_id, cer_download_title, cer_download_path " . 
       "from cer_downloads ";


$downloads = array();

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
	$link = "http://" . $_SERVER["HTTP_HOST"] . "/" . $row["cer_download_path"];
	$link = "<a href=\"" . $link . "\" target=\"_blank\"><img src=\"/pictures/document_view.gif\" border='0'/></a>";

	
	$downloads[$row["cer_download_id"]] = $link;
}


/********************************************************************
    build list 
*********************************************************************/

$list = new ListView($sql);

$list->set_entity("cer_downloads");
$list->set_order("cer_download_title");

if(has_access("cer_can_edit_downloads"))
{
	$list->add_column("cer_download_title", "Title", "cer_download.php");
}
else
{
	$list->add_column("cer_download_title", "Title");
}
$list->add_text_column("downloads", "", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML, $downloads);

if(has_access("cer_can_edit_downloads"))
{
	$list->add_button(LIST_BUTTON_NEW, "New", "cer_download.php");
}

$list->populate();
$list->process();

$page = new Page("downloads");

$page->header();
$page->title("Downloads");
$list->render();
$page->footer();
?>
