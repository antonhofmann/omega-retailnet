<?php
/********************************************************************

    cer_application_brands.php

    Application Form: other swatch group brands in the area
    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2016-10-20
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2016-10-20
    Version:        1.0.0

    Copyright (c) 2016, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";
require_once "../shared/func_posindex.php";

check_access("has_access_to_cer");

/********************************************************************
    prepare all data needed
*********************************************************************/

$posdata = get_pos_data($project["project_order"]);
$currency_symbol = $posdata["currency_symbol"];

$sql_legal_types = "select posowner_type_id, posowner_type_name from posowner_types order by posowner_type_name";
$sql_pos_types = "select postype_id, postype_name from postypes order by postype_name";

$sql_sg_brands = "select sg_brand_id, sg_brand_name from sg_brands where sg_brand_active = 1 order by sg_brand_name";



$sql = "select * from ln_basicdata_lnr03_brands_dummies ";

$brands = array();
$legal_types = array();
$pos_types = array();

$contract_years = array();
$sales_surfaces = array();
$sale_years = array();
$sale_amounts = array();
$contract_years = array();
$rental_conditions = array();
$notes = array();

$signature_years = array();
$selling_years = array();


$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$brands[$row["ln_basicdata_lnr03_brand_id"]] = '';
	$legal_types[$row["ln_basicdata_lnr03_brand_id"]] = '';
	$pos_types[$row["ln_basicdata_lnr03_brand_id"]] = '';

	$contract_years[$row["ln_basicdata_lnr03_brand_id"]] = '';
	$rental_conditions[$row["ln_basicdata_lnr03_brand_id"]] = '';
	$sales_surfaces[$row["ln_basicdata_lnr03_brand_id"]] = '';
	$sale_years[$row["ln_basicdata_lnr03_brand_id"]] = '';
	$sale_amounts[$row["ln_basicdata_lnr03_brand_id"]] = '';

	$notes[$row["ln_basicdata_lnr03_brand_id"]] = '';
}

//get real data
$sql_rd = "select * from ln_basicdata_lnr03_brands " . 
       " where ln_basicdata_lnr03_brand_cerbasicdata_id = " . dbquote($cer_basicdata["cer_basicdata_id"]) . 
			   " and ln_basicdata_lnr03_brand_cer_version = 0";



$i = 1;
$res = mysql_query($sql_rd) or dberror($sql_rd);
while($row = mysql_fetch_assoc($res))
{
	$brands[$i] = $row["ln_basicdata_lnr03_brand_sg_brand_id"];
	$legal_types[$i] = $row["ln_basicdata_lnr03_brand_legaltype_id"];
	$pos_types[$i] = $row["ln_basicdata_lnr03_brand_postype_id"];

	$contract_years[$i] = $row["ln_basicdata_lnr03_brand_contract_year"];
	$rental_conditions[$i] = $row["ln_basicdata_lnr03_brand_rental_conidtions"];
	$sales_surfaces[$i] = $row["ln_basicdata_lnr03_brand_gross_surface"];
	$sale_years[$i] = $row["ln_basicdata_lnr03_brand_sales_year"];
	$sale_amounts[$i] = $row["ln_basicdata_lnr03_brand_sales_amount"];
	$notes[$i] = $row["ln_basicdata_lnr03_brand_notes"];
	$i++;

	$selling_years[$row["ln_basicdata_lnr03_brand_sales_year"]] = $row["ln_basicdata_lnr03_brand_sales_year"];
}



for($i=(date("Y")-1);$i>(date("Y")-4);$i--)
{
	$selling_years[$i] = $i;
}
asort($selling_years);


/********************************************************************
	build form
*********************************************************************/
$form = new Form("ln_basicdata_lnr03_brands", "CER Basic Data");

include("include/project_head.php");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("context", param("context"));


$form->add_section("Other Swatch Group Brands in Same Mall or Street");
$form->add_checkbox("cer_basicdata_no_sg_brands", "There are no other Swatch Group Brands in the same mall/street.", $cer_basicdata["cer_basicdata_no_sg_brands"], SUBMIT, "Other Swatch Group Brands");


if($cer_basicdata["cer_basicdata_no_sg_brands"] == 0)
{
	$form->add_comment("Please indicate for all other Swatch Group Brands located in the same mall/street as " . $project["order_shop_address_company"] . ".");

	if($project["project_cost_type"] != 1)
	{
		$form->add_comment("Indicate at least the brand, the legal type and the POS type in case you do not have access to the rental and turnover data of a brand.");
	}
}


$form->process();

if($form->button("cer_basicdata_no_sg_brands"))
{
	$value = 0;
	if(array_key_exists("cer_basicdata_no_sg_brands", $_POST))
	{
		$value = $_POST["cer_basicdata_no_sg_brands"];
	}
	

	$sql_u = "update cer_basicdata set cer_basicdata_no_sg_brands = " . dbquote($value) . 
		   " where cer_basicdata_version = 0 and cer_basicdata_project = " . dbquote(param("pid"));

	$res = mysql_query($sql_u) or dberror($sql_u);

	$link = "cer_application_brands.php?pid=" . param("pid") . "&context=" . param("context");
	redirect($link);
}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$form->populate();

/********************************************************************
	build list of pos locations
*********************************************************************/
$list = new ListView($sql);

$list->set_entity("ln_basicdata_lnr03_brands");

$list->add_list_column("brand_id", "Brand", $sql_sg_brands, 0, $brands);

$list->add_list_column("legaltype_id", "Legal Type", $sql_legal_types, 0, $legal_types);
$list->add_list_column("postype_id", "POS Type", $sql_pos_types, 0, $pos_types);

$list->add_edit_column("contract_year", "Year of <br />Contract  <br />Signature", 10, COLUMN_UNDERSTAND_HTML, $contract_years);



$list->add_edit_column("gross_surface", "Gross <br />Surface (sqm)", 8, COLUMN_UNDERSTAND_HTML, $sales_surfaces);
$list->add_edit_column("rental_conidtions", "Rental Conditions: fixed rent per <br />measurement unit and/or turnover <br />percentage on gross or net sales", 35, COLUMN_UNDERSTAND_HTML, $rental_conditions);

$list->add_list_column("sales_year", "Last Full <br />Year Selling", $selling_years, COLUMN_UNDERSTAND_HTML, $sale_years);

$list->add_edit_column("sales_amount", "Gross Sales<br />Last Full Year<br /> in " . $currency_symbol, 12, COLUMN_UNDERSTAND_HTML, $sale_amounts);
$list->add_edit_column("brand_notes", "Remark", 30, "", $notes);



if($project["project_state"] != 2)
{
	$list->add_button("save", "Save List");
}


$list->populate();
$list->process();



/********************************************************************
save list
*********************************************************************/
if($list->button("save"))
{
	$brands = array();
	$legal_types = array();
	$pos_types = array();

	$contract_years = array();
	$sales_surfaces = array();
	$sale_years = array();
	$sale_amounts = array();
	$contract_years = array();
	$rental_conditions = array();
	$notes = array();

	
	
	foreach($list->columns as $key=>$column)
	{
		if($column["name"] == "brand_id")
		{
			foreach($column["values"] as $id=>$value)
			{
				$brands[] = $value;

			}
		}

		if($column["name"] == "legaltype_id")
		{
			foreach($column["values"] as $id=>$value)
			{
				$legal_types[] = $value;
			}
		}

		if($column["name"] == "postype_id")
		{
			foreach($column["values"] as $id=>$value)
			{
				$pos_types[] = $value;

			}
		}

		if($column["name"] == "contract_year")
		{
			foreach($column["values"] as $id=>$value)
			{
				$contract_years[] = $value;

			}
		}

		if($column["name"] == "gross_surface")
		{
			foreach($column["values"] as $id=>$value)
			{
				$sales_surfaces[] = str_replace(",", ".", $value);

			}
		}

		if($column["name"] == "rental_conidtions")
		{
			foreach($column["values"] as $id=>$value)
			{
				$rental_conditions[] = $value;

			}
		}

		if($column["name"] == "sales_year")
		{
			foreach($column["values"] as $id=>$value)
			{
				$sale_years[] = $value;

			}
		}

		if($column["name"] == "sales_amount")
		{
			foreach($column["values"] as $id=>$value)
			{
				$sale_amounts[] = $value;

			}
		}

		if($column["name"] == "brand_notes")
		{
			foreach($column["values"] as $id=>$value)
			{
				$notes[] = $value;
			}
		}
	}


	//validate data
	$error = 0;
	$errors[1] = "Gross Sales Surface must be numeric!";
	$errors[2] = "Gross Sales must be numeric without decimals!";
	$errors[3] = "Entries of at least on line are not complete!";

	foreach($sales_surfaces as $key=>$value)
	{
		if($value and !is_real_value($value))
		{
			$error = 1;
		}
	}
	
	if($error == 0)
	{
		foreach($sale_amounts as $key=>$value)
		{
			if($value and !is_int_value($value, 12))
			{
				$error = 2;
			}
		}
	}

	if(1==3 and $error == 0 and $project["project_cost_type"] == 1)
	{
		
		foreach($brands as $key=>$value)
		{
		
			if($value > 0)
			{
				if($legal_types[$key] == 0
					or $pos_types[$key] == 0
					or $contract_years[$key] == 0
					or $sales_surfaces[$key] == ''
					or $sale_years[$key] == 0
					or $pos_types[$key] == 0
					or $sale_amounts[$key] == 0
						or $rental_conditions[$key] == ''
					)
				{
					$error = 3;
				}
			}
		}
	
	}


	if($error == 0)
	{
		
		foreach($brands as $key=>$value)
		{
		
			if($value > 0)
			{
				if($legal_types[$key] == 0
					or $pos_types[$key] == 0
					)
				{
					$error = 3;
				}
			}
		}
	
	}


	if($error > 0)
	{
		$form->error($errors[$error]);
	}
	else
	{
		$sql = "delete from ln_basicdata_lnr03_brands " . 
			   " where ln_basicdata_lnr03_brand_cerbasicdata_id = " . dbquote($cer_basicdata["cer_basicdata_id"]) . 
			   " and ln_basicdata_lnr03_brand_cer_version = 0";

		$result = mysql_query($sql) or dberror($sql);

	
		foreach($brands as $key=>$value)
		{	
			if($value > 0)
			{
				$fields = array();
				$values = array();

				$fields[] = "ln_basicdata_lnr03_brand_cerbasicdata_id";
				$values[] = dbquote($cer_basicdata["cer_basicdata_id"]);

				$fields[] = "ln_basicdata_lnr03_brand_cer_version";
				$values[] = 0;

				$fields[] = "ln_basicdata_lnr03_brand_sg_brand_id";
				$values[] = dbquote($value);

				$fields[] = "ln_basicdata_lnr03_brand_legaltype_id";
				$values[] = dbquote($legal_types[$key]);

				$fields[] = "ln_basicdata_lnr03_brand_postype_id";
				$values[] = dbquote($pos_types[$key]);

				$fields[] = "ln_basicdata_lnr03_brand_contract_year";
				$values[] = dbquote($contract_years[$key]);

				$fields[] = "ln_basicdata_lnr03_brand_rental_conidtions";
				$values[] = dbquote($rental_conditions[$key]);

				$fields[] = "ln_basicdata_lnr03_brand_gross_surface";
				$values[] = dbquote($sales_surfaces[$key]);

				$fields[] = "ln_basicdata_lnr03_brand_sales_year";
				$values[] = dbquote($sale_years[$key]);

				$fields[] = "ln_basicdata_lnr03_brand_sales_amount";
				$values[] = dbquote($sale_amounts[$key]);

				$fields[] = "ln_basicdata_lnr03_brand_notes";
				$values[] = dbquote($notes[$key]);

				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				$fields[] = "date_created";
				$values[] = dbquote(date("Y-m-d H:i:s"));


				$sql = "insert into ln_basicdata_lnr03_brands (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				$result = mysql_query($sql) or dberror($sql);
			}
			
		}
	}

	
}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();


if($project["project_projectkind"] == 8)
{
	if($form_type == "AF")
	{
		$page->title("Application Form for PopUps: Conditions of other SG brands in same mall / street");
	}
	else
	{
		$page->title("Capital Expenditure Request for PopUps: Conditions of other SG brands in same mall / street");
	}
}
else
{
	if($form_type == "AF")
	{
		$page->title("AF/Business Plan: Conditions of other SG brands in same mall / street");
	}
	else
	{
		$page->title("Capital Expenditure Request: Conditions of other SG brands in same mall / street");
	}
}


if(param("context") == 'ln')
{
	require_once("include/tabs_ln.php");
}
else
{
	require_once("include/tabs.php");
}

$form->render();

if($cer_basicdata["cer_basicdata_no_sg_brands"] == 0)
{
	$list->render();
}



require "include/footer_scripts.php";
$page->footer();

?>