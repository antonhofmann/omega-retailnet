<?php
/********************************************************************

    cer_inr02_pdf.php

    Print PDF for CER Business Plan (Form IN-R02)

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();

$SUPPRESS_HEADERS = true;
define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 


require "../include/frame.php";


$cer_version = 0;
$ln_version = 0;
if(param("cerversion"))
{
	$cer_version = param("cerversion");
}
if(param("lnversion"))
{
	$ln_version = param("lnversion");
}


require "../shared/func_posindex.php";


if(param("pid") > 0 )
{
	require "include/get_functions.php";
	require "include/get_project.php";
}
else
{
	require "include/get_draft_functions.php";
}

require "include/financial_functions.php";


check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');


// Create and setup PDF document

$pdf = new TCPDF("L", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 10, 5);
$pdf->setPrintHeader(false);


$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');
$pdf->Open();

include("cer_inr02_pdf_detail.php");


$pdf->Output(BRAND . '_business_plan_' . $project_name . '_' . $project["project_number"] . '.pdf');



?>