<?php
/********************************************************************

	cer_application_expenses.php

    Application Form: expenses
    
	Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";
check_access("has_access_to_cer");

if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}
set_referer("cer_application_expense.php");


/********************************************************************
    prepare all data needed
*********************************************************************/

$posdata = get_pos_data($project["project_order"]);
$posleases = get_pos_leasedata($posdata["posaddress_id"], $project["project_order"]);

//update comissions
$result = update_commissions(param("pid"));

$currency = get_cer_currency(param("pid"));

$result = update_cost_of_products_sold(param("pid"));

// get all years
$years = array();
$year_amounts = array();
$sql  = "select DISTINCT cer_expense_year " .
		"from cer_expenses " . 
		"where cer_expense_cer_version = 0 and cer_expense_project = " . param("pid") . 
		" order by cer_expense_year";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$year_amounts[$row["cer_expense_year"]] = array();
	$years[] = $row["cer_expense_year"];
}



// get all expenses for for each year
$sql  = "select cer_expense_type, cer_expense_year, cer_expense_amount " .
		"from cer_expenses " .
		"where cer_expense_cer_version = 0 and cer_expense_type != 9 and cer_expense_project = " . param("pid");

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$year_amounts[$row["cer_expense_year"]][$row["cer_expense_type"]] = $row["cer_expense_amount"];
}

//update salaries
$result = calculate_forcasted_salaries(param("pid"), $years, $project["order_shop_address_country"]);


//prepare sql for list
$sql_list1 = "select DISTINCT cer_expense_type_id, cer_expense_type_group_name, cer_expense_type_name " .
             "from cer_expenses " . 
			 "left join cer_expense_types on cer_expense_type_id = cer_expense_type ";

if($form_type == "CER")
{
	$list1_filter = "cer_expense_cer_version = 0 and cer_expense_type <> 9 and cer_expense_project = " . param("pid") . " and cer_expense_type <> 14";
}
else
{
	$list1_filter = "cer_expense_cer_version = 0 and cer_expense_type <> 9 " .
		            "and cer_expense_project = " . param("pid") . 
		            " and cer_expense_type <> 14 " . 
		            " and cer_expense_type <> 11 " . 
		            " and cer_expense_type <> 12 ";
}

//$list1_filter = "cer_expense_type <> 9 and cer_expense_project = " . param("pid") . " and cer_expense_type <> 14";

//get list_totals
$list_totals = array();
foreach($years as $key=>$year)
{
	$list_totals[$year] = 0;
}

$sql = "select cer_expense_year, cer_expense_amount " .
       "from cer_expenses " . 
	   " where " . $list1_filter;
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$list_totals[$row["cer_expense_year"]] = $list_totals[$row["cer_expense_year"]] + $row["cer_expense_amount"];
}


/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_basicdata", "cer_basicdata");
include("include/project_head.php");

$form->add_hidden("pid", param("pid"));

$form->add_section("Business Plan Period");
$tmp = $cer_basicdata["cer_basicdata_firstyear"] . "-" . $cer_basicdata["cer_basicdata_firstmonth"] . " / " .
$cer_basicdata["cer_basicdata_lastyear"] . "-" . $cer_basicdata["cer_basicdata_lastmonth"] . " / " . $business_plan_period;
$form->add_label("business_plan_period", "From / To / Duration", 0, $tmp);

if(count($posleases) > 0)
{
	$form->add_section("Lease Period");
	$form->add_label("l_startdate", "Start Date", NOTNULL, to_system_date($posleases["poslease_startdate"]), TYPE_DATE);
	$form->add_label("l_enddate", "Expiry Date", NOTNULL, to_system_date($posleases["poslease_enddate"]), TYPE_DATE);
	$form->add_label("rental_duration", "Rental Duration", 0, $posleases["rental_duration"]);
}




/********************************************************************
    build list of standard expenses
*********************************************************************/
$list1 = new ListView($sql_list1, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list1->set_title("Expenses in " . $currency["symbol"]);
$list1->set_entity("cer_expenses");
$list1->set_filter($list1_filter);
$list1->set_group("cer_expense_type_group_name");
$list1->set_order("cer_expense_type_sortorder");

$list1->add_hidden("pid", param("pid"));

$link = "cer_application_expense.php?pid=" . param("pid");
if(($cer_basicdata["cer_basicdata_cer_locked"] == 0 and has_access("has_access_to_his_cer")) or has_access("has_full_access_to_cer"))
{
	$list1->add_column("cer_expense_type_name", "Type", $link, "", "", COLUMN_NO_WRAP);
}
else
{
	$list1->add_column("cer_expense_type_name", "Type", "", "", "", COLUMN_NO_WRAP);
}

foreach($years as $key=>$year)
{
	$list1->add_text_column("y" . $year, "$year", COLUMN_ALIGN_RIGHT, $year_amounts[$year]);
	$list1->set_footer("y" . $year, number_format($list_totals[$year] ,0));

}
$list1->set_footer("cer_expense_type_name", "Total");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

$list1->populate();
$list1->process();


  
/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();

if($form_type == "INR03")
{
	$page->title("INR-03 - Retail Furniture in Third-party Store: Expenses");
}
elseif($form_type == "AF")
{
	$page->title("Application Form: Expenses");
}
else
{
	$page->title("Capital Expenditure Request: Expenses");
}
require_once("include/tabs.php");
$form->render();

$list1->render();

require "include/footer_scripts.php";
$page->footer();

?>