<?php
/********************************************************************

    cer_booklet_investments_pdf.php

    Print PDF CER only Budget Forms.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2015-07-17
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2015-07-17
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();

$SUPPRESS_HEADERS = true;
define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 



require "../include/frame.php";


$cer_version = 0;
$ln_version = 0;
if(param("cerversion"))
{
	$cer_version = param("cerversion");
}
if(param("lnversion"))
{
	$ln_version = param("lnversion");
}


require "include/get_functions.php";
require "include/get_project.php";
require "include/financial_functions.php";
require "../shared/func_posindex.php";
require_once "../shared/project_cost_functions.php";

check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');
require_once('../include/fpdi/fpdi.php');
require_once('../include/SetaPDF/Autoload.php');




//get currencies
$currencies = array();
$sql = 'select currency_id, currency_symbol from currencies where currency_system = 1';

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$currencies["s"] = $row['currency_symbol'];
}


if($cer_basicdata['cer_basicdata_currency']) {
	$sql = 'select currency_id, currency_symbol from currencies where currency_id = ' . $cer_basicdata['cer_basicdata_currency'];

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res) and !in_array($row['currency_symbol'], $currencies))
	{
		$currencies["c1"] = $row['currency_symbol'];
	}
}

if($cer_basicdata['cer_basicdata_currency2']) {
	$sql = 'select currency_id, currency_symbol from currencies where currency_id = ' . $cer_basicdata['cer_basicdata_currency2'];

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res) and !in_array($row['currency_symbol'], $currencies))
	{
		$currencies["c2"] = $row['currency_symbol'];
	}
}


//get signed sheet
$cer_signatures = "";

$sql_sheets = "select cer_summary_cer_coversheet " .
       "from cer_summary ". 
	   " where cer_summary_cer_version = 0 and cer_summary_project = " . param("pid");

$res_sheets = mysql_query($sql_sheets) or dberror($sql_sheets);
if ($row_ssheets = mysql_fetch_assoc($res_sheets))
{
	$cer_signatures = $row_ssheets["cer_summary_cer_coversheet"];
}


require("include/cer_get_data_from_project.php");


// Create and setup PDF document

$pdf = new FPDI("P", "mm", "A4", true, 'UTF-8');
$pdf->setPrintHeader(false);


$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');

$pdf->Open();



//financial summary
if($cer_signatures)
{	
	$source_file = ".." . $cer_signatures;
	if(file_exists($source_file))
	{
		
		$layout = new FPDI();
		$num_of_pages = $pdf->setSourceFile($source_file);
		$num_of_pages = $layout->setSourceFile($source_file);
		for($i=1;$i<=$num_of_pages;$i++)
		{
			$tplIdx = $pdf->importPage($i);
			$layout->AddPage("L");
			$tplIdx = $layout->importPage($i);
			
			$tpl = $layout->useTemplate($tplIdx);
			//$pdf->AddPage("", array(0=>$tpl["w"], 1=>$tpl["h"]));

			if($tpl["w"] > $tpl["h"])
			{
				$pdf->AddPage("L", array(0=>$tpl["h"], 1=>$tpl["w"]));
			}
			else
			{
				$pdf->AddPage("P", array(0=>$tpl["w"], 1=>$tpl["h"]));
			}
			$tpl = $pdf->useTemplate($tplIdx);
		}
	}
}
else
{
	include("cer_in01_pdf_detail.php");
}


//project budget in local currency

include("../user/project_costs_budget_detail_pdf.php");


//project budget in CHF
if(isset($order_currency) and $order_currency["symbol"] != 'CHF')
{
	$sc = 1;
	include("../user/project_costs_budget_detail_pdf.php");
}



$file_name = BRAND . "_CER_Booklet_" . str_replace(" ", "_", $project_name) . "_" . date("Y-m-d") . ".pdf";
$pdf->Output($file_name, 'I');

?>