<?php
/********************************************************************

    ajx_upload_ln_supporting_docuemnt.php

    Upload file

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2014-02-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2014-02-24
    Version:        1.0.0

    Copyright (c) 2014, OMEGA SA, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../include/page_modal.php";
require "include/get_functions.php";

if(has_access("has_access_to_his_cer") or has_access("has_full_access_to_cer"))
{
	$project = get_project(param("pid"));
	$order_number = $project["order_number"];
	$order_id = $project["order_id"];
	
	$error = 0;
	$title = "";
	$description = "";

	/********************************************************************
		prepare all data needed
	*********************************************************************/


	/********************************************************************
		save data
	*********************************************************************/
	if(param("save_form") and count($_FILES) == 1)
	{
		$error = 0;
		
		$file = $_FILES["order_file_path"];
		$title = $_POST["order_file_title"];
		$description = $_POST["order_file_description"];

		
		$ext = "";
		if(count($_FILES) > 0)
		{
			$ext = strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
		}

		

		if(!$_POST["order_file_title"] 
			or !$_POST["order_file_description"] 
			or $file["name"] == '')
		{
			$error = 1;
		}
		elseif($ext != 'pdf' and $ext != 'jpg')
		{
			$error = 2;
		}
		elseif ($file["name"])
		{
			$order_file_path = "/files/orders/$order_number";
			$path = "/files/orders/$order_number";
			$path = combine_paths($_SERVER["DOCUMENT_ROOT"], $path);
            //$path = preg_replace("/\\//", "\\", $path);
			$dir = $path;

			$name = make_valid_filename($file["name"]);
			$name = combine_paths($path, $name);
			$name = make_unique_filename($name, $_SERVER["DOCUMENT_ROOT"]);

			
			if(!file_exists($dir))
			{
				create_directory($dir);
			}
			if(file_exists($name))
			{
				unlink($name);
			}
			copy($file["tmp_name"], $name);

			
			$order_file_path = $order_file_path . "/" . basename($name);

			
			$file_type = "";
			
			$sql = "select file_type_id from file_types " . 
				   "where file_type_extension like '%" . $ext . "%'";
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$file_type = $row["file_type_id"];
			}

			if($file_type)
			{
				$fields = 'order_file_order,';
				$values = dbquote($_POST["oid"]) . ',';

				$fields .= 'order_file_title,';
				$values .= dbquote($_POST["order_file_title"]) . ',';

				$fields .= 'order_file_description,';
				$values .= dbquote($_POST["order_file_description"]) . ',';

				$fields .= 'order_file_path,';
				$values .= dbquote($order_file_path) . ',';

				$fields .= 'order_file_category,';
				$values .= '16,';

				$fields .= 'order_file_type,';
				$values .= dbquote($file_type) . ',';

				$fields .= 'order_file_owner,';
				$values .= user_id() . ',';

				$fields .= 'user_created,';
				$values .= dbquote(user_login()) . ',';

				$fields .= 'date_created,';
				$values .= dbquote(date("Y-m-d H:i:s")) . ',';

				$fields = substr($fields, 0, strlen($fields)-1);
				$values = substr($values, 0, strlen($values)-1);

				$sql_i = 'insert into order_files (' . $fields . ') VALUES (' . $values . ')';


				$result = mysql_query($sql_i) or dberror($sql_i);
			}
			else
			{
				$error = 3;
			}

			
		}
		else
		{
			$error = 1;
		}



	}

	/********************************************************************
		Create Form
	*********************************************************************/ 

	$form = new form("order_files", "Order Files");
	$form->add_hidden("save_form", "1");
	$form->add_hidden("pid", param("pid"));
	$form->add_hidden("oid", $order_id);


	$form->add_edit("order_file_title", "Title*", 0, $title, TYPE_CHAR);
	$form->add_multiline("order_file_description", "Description*", 4, 0, $description);
	$form->add_upload("order_file_path", "File", "/files/orders/$order_number", 0);

	if($error == 1)
	{
		$form->error("Alle fields are mandotory.");
	}
	elseif($error == 2)
	{
		$form->error("Only files of type PDF and JPG are allowed!");
	}
	elseif($error == 3)
	{
		$form->error("The file type could not be recognized. <br />Only files of type PDF and JPG are allowed!");
	}

	$form->add_input_submit("submit", "Upload File", 0);

	/********************************************************************
		Populate form and process button clicks
	*********************************************************************/ 
	$form->populate();
	$form->process();

	/********************************************************************
		Process buttons
	*********************************************************************/ 

	/********************************************************************
		 Render Page
	 *********************************************************************/ 
	$page = new Page_Modal("query_generator");

	//require "include/benchmark_page_actions.php";

	$page->header();
	$page->title("Upload LN Supporting Document");

	$form->render();
}
else
{
?>	
	<script languege="javascript">
		var back_link = "ln_general.php?pid=<?php echo param("pid");?>"; 
		$.nyroModalRemove();
	</script>

<?php
}
if(param("save_form") and $error == 0)
{
?>

<script languege="javascript">
var back_link = "ln_general.php?pid=<?php echo param("pid");?>"; 
$.nyroModalRemove();
</script>

<?php
}
$page->footer();
