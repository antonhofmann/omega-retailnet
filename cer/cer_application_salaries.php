<?php
/********************************************************************

	cer_application_salaries.php

    Application Form: salaries
    
	Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";


check_access("has_access_to_cer");


if(!has_access("has_access_to_human_resources"))
{
	redirect("noaccess.php");
}

set_referer("cer_application_salary.php");


/********************************************************************
    prepare all data needed
*********************************************************************/
$posdata = get_pos_data($project["project_order"]);
$posleases = get_pos_leasedata($posdata["posaddress_id"], $project["project_order"]);

//update comissions
$result = update_commissions(param("pid"));

// create list values for the years
$years = array();
$first_year = $cer_basicdata["cer_basicdata_firstyear"];
$first_month = $cer_basicdata["cer_basicdata_firstmonth"];
$last_year = $cer_basicdata["cer_basicdata_lastyear"];

for($i = $first_year;$i <= $last_year;$i++)
{
	$years[$i] = $i;
}


$sql_list1 = "select cer_salary_id, cer_salary_year_starting, " .
             "cer_salary_month_starting, cer_salary_fixed_salary, cer_salary_bonus, " . 
			 "cer_salary_other, cer_salary_social_charges, cer_salary_total, cer_salary_headcount_percent,  " .
			 "TRUNCATE(cer_salary_total*cer_salary_headcount_percent/100,2 ) as cer_salary_part, " .
			 "cer_staff_type_name, concat(cer_salary_commission_percent, '%') as cer_salary_commission_percent, " . "concat(cer_salary_commission_social_charges_percent, '%') as  cer_salary_commission_social_charges_percent " . 
             "from cer_salaries " . 
			 "left join cer_staff_types on cer_staff_type_id = cer_salary_staff_type ";

$list1_filter = "cer_salary_cer_version = 0 and cer_salary_project = " . param("pid");


//get list_totals
$list_totals = array();
$list_totals[1] = 0;
$list_totals[2] = 0;
$list_totals[3] = 0;
$list_totals[4] = 0;
$list_totals[5] = 0;
$list_totals[6] = 0;

$sql = $sql_list1 . " where " . $list1_filter;
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$list_totals[1] = $list_totals[1] + $row["cer_salary_fixed_salary"];
	$list_totals[2] = $list_totals[2] + $row["cer_salary_bonus"];
	$list_totals[3] = $list_totals[3] + $row["cer_salary_other"];
	$list_totals[4] = $list_totals[4] + $row["cer_salary_social_charges"];
	$list_totals[5] = $list_totals[5] + $row["cer_salary_total"];
	$list_totals[6] = $list_totals[6] + $row["cer_salary_part"];

}

$currency = get_cer_currency(param("pid"));


/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_basicdata", "cer_basicdata");
include("include/project_head.php");

$form->add_hidden("pid", param("pid"));

$form->add_section("Business Plan Period");
$tmp = $cer_basicdata["cer_basicdata_firstyear"] . "-" . $cer_basicdata["cer_basicdata_firstmonth"] . " / " .
$cer_basicdata["cer_basicdata_lastyear"] . "-" . $cer_basicdata["cer_basicdata_lastmonth"] . " / " . $business_plan_period;
$form->add_label("business_plan_period", "From / To / Duration", 0, $tmp);

if(count($posleases) > 0)
{
	$form->add_section("Lease Period");
	$form->add_label("l_startdate", "Start Date", NOTNULL, to_system_date($posleases["poslease_startdate"]), TYPE_DATE);
	$form->add_label("l_enddate", "Expiry Date", NOTNULL, to_system_date($posleases["poslease_enddate"]), TYPE_DATE);
	$form->add_label("rental_duration", "Rental Duration", 0, $posleases["rental_duration"]);
}

if(($cer_basicdata["cer_basicdata_cer_locked"] == 0 
	and (has_access("has_access_to_his_cer")
			or has_access("can_only_edit_staff_in_cer")
			 or has_access("has_access_only_to_human_resources")
		)
	)
	or has_access("has_full_access_to_cer")
)
{
	$form->add_section("Growth of Salaries");
	$form->add_edit("cer_basicdata_salary_growth", "Fixed grwoth of salaries in % per year", 0, $cer_basicdata["cer_basicdata_salary_growth"], TYPE_DECIMAL, 10,2, 1, "salary_growth");

	$form->add_section("Commission");

	$form->add_edit("cer_basicdata_commission", "Global Commission in % per year", 0, $cer_basicdata["cer_basicdata_commission"], TYPE_DECIMAL, 6,2);
	$form->add_edit("cer_basicdata_commission_tax_percent", "Global Social Charges in % on Commission per year", 0, $cer_basicdata["cer_basicdata_commission_tax_percent"], TYPE_DECIMAL, 6,2);

	$form->add_comment("Please check the following option if commissions are to be calculated on the base of NET SALES VALUES. <br />Otherwise they will be calculated on the base of GROSS SALES VALUES.");

	$form->add_checkbox("cer_basicdata_commission_from_net_sales", "comissions are to be calculated on the base of NET SALES VALUES", $cer_basicdata["cer_basicdata_commission_from_net_sales"], "", "Calculation Mode");


	

	$form->add_button("form_save", "Save Data");
}
else
{
	$form->add_section("Growth of Salaries");
	$form->add_label("cer_basicdata_salary_growth", "Fixed grwoth of salaries in % per year", 0, $cer_basicdata["cer_basicdata_salary_growth"]);
	$form->add_label("cer_basicdata_commission_tax_percent", "Global Social Charges in % on Commission per year", 0, $cer_basicdata["cer_basicdata_commission_tax_percent"]);
	
	$form->add_section("Commission");
	$form->add_label("cer_basicdata_commission", "Global Commission in % per year", 0, $cer_basicdata["cer_basicdata_commission"]);
	$form->add_checkbox("cer_basicdata_commission_from_net_sales", "comissions are to be calculated on the base of NET SALES VALUES", $cer_basicdata["cer_basicdata_commission_from_net_sales"], "", "Calculation Mode");

	
}






/********************************************************************
    build list of standard expenses
*********************************************************************/
$list1 = new ListView($sql_list1, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list1->set_title("Salaries in " . $currency["symbol"]);
$list1->set_entity("cer_salaries");
$list1->set_filter($list1_filter);

$list1->add_hidden("pid", param("pid"));

$link = "cer_application_salary.php?pid=" . param("pid");
if(($cer_basicdata["cer_basicdata_cer_locked"] == 0 
	and (has_access("has_access_to_his_cer")
			or has_access("can_only_edit_staff_in_cer")
			 or has_access("has_access_only_to_human_resources")
		)
	)
	or has_access("has_full_access_to_cer")
)
{
	$list1->add_column("cer_staff_type_name", "Function", $link, "", "", COLUMN_NO_WRAP);
}
else
{
	$list1->add_column("cer_staff_type_name", "Function", "", "", "", COLUMN_NO_WRAP);
}
$list1->add_column("cer_salary_year_starting", "from Year", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list1->add_column("cer_salary_month_starting", "from Month", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list1->add_column("cer_salary_headcount_percent", "Fulltime %", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list1->add_column("cer_salary_fixed_salary", "Fixed Salary", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list1->add_column("cer_salary_bonus", "Bonus", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list1->add_column("cer_salary_other", "Other Salary", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list1->add_column("cer_salary_social_charges", "Social Charges", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list1->add_column("cer_salary_total", "Total Cost", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list1->add_column("cer_salary_part", "Part Time Cost", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

$list1->add_column("cer_salary_commission_percent", "Commission", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list1->add_column("cer_salary_commission_social_charges_percent", "Social Charges", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);


$list1->set_footer("cer_salary_function", "Total");
$list1->set_footer("cer_salary_fixed_salary", number_format($list_totals[1] ,0));
$list1->set_footer("cer_salary_bonus", number_format($list_totals[2] ,0));
$list1->set_footer("cer_salary_other", number_format($list_totals[3] ,0));
$list1->set_footer("cer_salary_social_charges", number_format($list_totals[4] ,0));
$list1->set_footer("cer_salary_total", number_format($list_totals[5] ,0));
$list1->set_footer("cer_salary_part", number_format($list_totals[6] ,0));


if(($cer_basicdata["cer_basicdata_cer_locked"] == 0 
	and (has_access("has_access_to_his_cer")
			or has_access("can_only_edit_staff_in_cer")
			 or has_access("has_access_only_to_human_resources")
		)
	)
	or has_access("has_full_access_to_cer")
)
{
	$list1->add_button("new", "Add New Position");
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if($form->button("form_save"))
{

	if($form->validate())
	{
		$fields = array();
    
		$value = dbquote($form->value("cer_basicdata_salary_growth"));
		$fields[] = "cer_basicdata_salary_growth = " . $value;

		$value = dbquote($form->value("cer_basicdata_commission_from_net_sales"));
		$fields[] = "cer_basicdata_commission_from_net_sales = " . $value;

		$value = dbquote($form->value("cer_basicdata_commission"));
		$fields[] = "cer_basicdata_commission = " . $value;

		$value = dbquote($form->value("cer_basicdata_commission_tax_percent"));
		$fields[] = "cer_basicdata_commission_tax_percent = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_basicdata set " . join(", ", $fields) . " where cer_basicdata_version = 0 and cer_basicdata_project = " . param("pid");
		mysql_query($sql) or dberror($sql);


		//update comissions
		$result = update_commissions(param("pid"));

		calculate_forcasted_salaries(param("pid"), $years, $project["order_shop_address_country"]);


		$form->message("Your data has bee saved.");
	}
}

$list1->populate();
$list1->process();

if($list1->button("new"))
{
	$link = "cer_application_salary.php?pid=" . param("pid");
	redirect($link);
}
  
/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();

if($form_type == "INR03")
{
	$page->title("INR-03 - Retail Furniture in Third-party Store: Human Resources");
}
elseif($form_type == "AF")
{
	$page->title("Application Form: Human Resources");
}
else
{
	$page->title("Capital Expenditure Request: Human Resources");
}
require_once("include/tabs.php");
$form->render();

$list1->render();

?>

<div id="salary_growth" style="display:none;">
    If empty then salaries for each year will be calculated using your country's inflation rate (See Cash Flow Calculation).<br />
	Indicate a percentage if salaries are growing with a fixed rate each year.
</div>



<?php


require "include/footer_scripts.php";
$page->footer();

?>