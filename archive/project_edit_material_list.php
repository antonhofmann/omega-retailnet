<?php
/********************************************************************

    project_edit_material_list.php

    Edit List of Materials

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-09
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-11-13
    Version:        1.2.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_edit_list_of_materials_in_projects");

register_param("pid");
set_referer("project_add_catalog_item_categories.php");
set_referer("project_edit_material_list_edit_item.php");
set_referer("project_add_catalog_item_special_item_list.php");
set_referer("project_add_special_item_individual.php");
set_referer("project_add_catalog_item_cost_estimation_list.php");
set_referer("project_add_cost_estimation_individual.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

// get System currency
$system_currency = get_system_currency_fields();

// create sql for oder items
$sql_order_items = "select order_item_id, order_item_text, order_item_quantity, order_item_order_revisions, ".
                   "    order_item_po_number, order_item_system_price, item_id, order_item_supplier_address, ".
                   "    if(order_item_not_in_budget=1 , 'no', 'yes') as order_item_in_budget, ".
                   "    TRUNCATE(order_item_quantity * order_item_system_price, 2) as total_price, ".
                   "    if(item_code <>'', item_code, item_type_name) as item_shortcut, ".
                   "    category_priority, category_name, order_item_supplier_freetext, ".
                   "    addresses.address_shortcut as supplier_company, ".
                   "    addresses_1.address_shortcut as forwarder_company, ".
                   "    item_type_id, item_type_name, ".
                   "    item_type_priority, concat(item_category_name, ', ', category_name) as cat_name, " . 
				   "    order_item_type, unit_name  ".
                   "from order_items ".
                   "left join items on order_item_item = item_id ".
                   "left join categories on order_item_category = category_id ".
                   "left join item_categories on item_category_id = item_category " .
                   "left join addresses on order_item_supplier_address = addresses.address_id ".
                   "left join addresses as addresses_1 ".
                   "     on order_item_forwarder_address = addresses_1.address_id ".
                   "left join item_types on order_item_type = item_type_id " .
				   "left join units on unit_id = item_unit";


$where_clause = " where (order_item_type = " . ITEM_TYPE_STANDARD . 
                "    or order_item_type = " . ITEM_TYPE_SPECIAL . 
				"    or order_item_type = " . ITEM_TYPE_SERVICES .") ".
                "    and order_item_order = " . $project["project_order"];


$sql_suppliers = "select DISTINCT address_id, address_shortcut " . 
                 "from order_items " . 
				 "left join suppliers on supplier_item = order_item_item " . 
				 "left join addresses on address_id = supplier_address " . 
				 "where address_shortcut <> '' and order_item_order = " . $project["project_order"] . 
				 " order by address_shortcut";

$item_suppliers = array();

$quantities = array();
$revisions = array();

$res = mysql_query($sql_order_items . $where_clause) or dberror($sql_order_items . $where_clause);

$num_of_standard_items = 0;
$num_of_special_items = 0;
while ($row = mysql_fetch_assoc($res))
{
    $quantities[$row["order_item_id"]] = $row["order_item_quantity"];
	$revisions[$row["order_item_id"]] = $row["order_item_order_revisions"];

    if($row["order_item_type"] == ITEM_TYPE_STANDARD or $row["order_item_type"] == ITEM_TYPE_SERVICES)
    {
        $num_of_standard_items++;
    }
    elseif($row["order_item_type"] == ITEM_TYPE_SPECIAL)
    {
        $num_of_special_items++;
    }

	$item_suppliers[$row["order_item_id"]] = $row["order_item_supplier_address"];
}


$group_totals = get_group_total_of_standard_items($project["project_order"], "order_item_system_price");
$standard_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_STANDARD);
$special_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_SPECIAL);
$cost_estimation_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_COST_ESTIMATION);
$local_construction_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_LOCALCONSTRUCTIONCOST);


$grand_total_in_system_currency = number_format($standard_item_total["in_system_currency"] + $special_item_total["in_system_currency"],2);

$total_cost = $standard_item_total["in_system_currency"] + $special_item_total["in_system_currency"] + $cost_estimation_item_total["in_system_currency"] + $local_construction_item_total["in_system_currency"];


// create sql for the furniture height listbox
$sql_ceiling_heights = "select ceiling_height_id, ceiling_height_height ".
					   "from ceiling_heights";

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project");

$form->add_section("Project");
$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid",$project["project_order"]);

require_once "include/project_head_small.php";


$form->add_section("Budget Totals");

$form->add_label("grand_total1", "Standard/Special Items in " . $system_currency["symbol"], 0, $grand_total_in_system_currency);


$form->add_label("grand_total6", "Estimation Local Construction Cost in " . $system_currency["symbol"], 0, number_format($local_construction_item_total["in_system_currency"],2));

$form->add_label("grand_total2", "Estimation of Add. Cost in " . $system_currency["symbol"], 0, number_format($cost_estimation_item_total["in_system_currency"],2));
$form->add_label("grand_total3", "Total Cost " . $system_currency["symbol"], 0, number_format($total_cost,2));

if ($project["order_budget_is_locked"] == 1)
{
    $form->error = "Budget is locked, quantities and prices can not be changed anymore.";
}

$form->add_section("Furniture Height");
$form->add_list("furniture_height", "Furniture Height in mm*", $sql_ceiling_heights, SUBMIT, $project["project_furniture_height"]);
$form->add_edit("project_furniture_height_mm", "", 0, $project["project_furniture_height_mm"], TYPE_INT, 4);
$form->add_button("save_form", "Save Furniture Height");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if($form->button("save_form"))
{
	$sql = "Update projects set " . 
		   "project_furniture_height = " . dbquote($form->value("furniture_height")) . ", " . 
		   "project_furniture_height_mm = " . dbquote($form->value("project_furniture_height_mm")) .
		   " where project_id = " . dbquote($project["project_id"]);
	$result = mysql_query($sql) or dberror($sql);

	$sql = "Update posorderspipeline set " . 
		   "posorder_furniture_height = " . dbquote($form->value("furniture_height")) . ", " .
		   "posorder_furniture_height_mm = " . dbquote($form->value("project_furniture_height_mm")) .
		   "where posorder_ordernumber = " . dbquote($project["project_order"]);
	$result = mysql_query($sql) or dberror($sql);

	$sql = "Update posorders set " . 
		   "posorder_furniture_height = " . dbquote($form->value("furniture_height")) . ", " .
		   "posorder_furniture_height_mm = " . dbquote($form->value("project_furniture_height_mm")) .
		   "where posorder_ordernumber = " . dbquote($project["project_order"]);
	$result = mysql_query($sql) or dberror($sql);


	$form->message("The furniture height has been saved.");
}
elseif($form->button("furniture_height")) {
	$sql= "select ceiling_height_id, ceiling_height_height ".
		  "from ceiling_heights " . 
		  "where ceiling_height_id = " . dbquote($form->value("furniture_height"));

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$form->value('project_furniture_height_mm', $row['ceiling_height_height']);
	}
	
}

/********************************************************************
    Create List for Catalog Items
*********************************************************************/ 
$list1 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list1->set_title("Catalog Items");
$list1->set_entity("order_item");
$list1->set_filter("(order_item_type = " . ITEM_TYPE_STANDARD . " or order_item_type = " . ITEM_TYPE_SERVICES . ") and order_item_order = " . $project["project_order"]);
$list1->set_order("supplier_company, item_code");
$list1->set_group("cat_name");

$link="project_edit_material_list_edit_item.php?pid=" . param("pid");

$list1->add_column("item_shortcut", "Item Code", $link, "", "", COLUMN_NO_WRAP);

$list1->add_column("order_item_in_budget", "in Budget", "", "", "", COLUMN_NO_WRAP | COLUMN_ALIGN_CENTER);
$list1->add_column("order_item_text", "Name");

if ($project["order_budget_is_locked"] == 1)
{
    $list1->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
}
else
{
    $list1->add_edit_column("item_entry_field", "Quantity", "4", 0, $quantities);
}

$list1->add_column("unit_name", "Unit", "", "", "", COLUMN_NO_WRAP);

$list1->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

$list1->add_column("total_price", "Total " . $system_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

$list1->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);

$list1->add_list_column("item_suppliers", "Supplier", $sql_suppliers, NOTNULL, $item_suppliers);

$list1->add_column("forwarder_company", "Forwarder");



// set group totals
foreach ($group_totals as $key=>$value)
{
    $list1->set_group_footer("total_price", $key , number_format($value,2));
}

$list1->set_footer("item_shortcut", "Total");
$list1->set_footer("total_price", number_format($standard_item_total["in_system_currency"],2));


if ($project["order_budget_is_locked"] == 1)
{
    $list1->add_button("nothing", "");
}
else
{
    if($num_of_standard_items > 0)
    {
        $list1->add_button("save_items", "Save");
    }
    $list1->add_button("add_items", "Add Catalog Items");
    $list1->add_button("delete_items_1", "Delete All Catalog Items");
}

/********************************************************************
    Create List for Special Items
*********************************************************************/ 
$list2 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list2->set_title("Special Items");
$list2->set_entity("order_item");
$list2->set_filter("order_item_type = " . ITEM_TYPE_SPECIAL . " and order_item_order = " . $project["project_order"]);
$list2->set_order("supplier_company, order_item_text");

$link="project_edit_material_list_edit_item.php?pid=" . param("pid");

$list2->add_column("item_shortcut", "Item Code", $link, "", "", COLUMN_NO_WRAP);

$list2->add_column("order_item_in_budget", "in Budget", "", "", "", COLUMN_NO_WRAP | COLUMN_ALIGN_CENTER);
$list2->add_column("order_item_text", "Name");

if ($project["order_budget_is_locked"] == 1)
{
    $list2->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
}
else
{
    $list2->add_edit_column("item_entry_field", "Quantity", "4", 0, $quantities);
}

$list2->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);

$list2->add_column("total_price", "Total " . $system_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);


$list2->add_column("order_item_po_number", "P.O. Number");
$list2->add_column("supplier_company", "Supplier");
$list2->add_column("forwarder_company", "Forwarder");


$list2->set_footer("item_shortcut", "Total");
$list2->set_footer("total_price", number_format($special_item_total["in_system_currency"],2));


if ($project["order_budget_is_locked"] == 1)
{
    $list2->add_button("nothing", "");
}
else
{
    if($num_of_special_items > 0)
    {
        $list2->add_button("save_items", "Save");
    }
    //$list2->add_button("add_special_items_1", "Add Special Items from Catalog");
    $list2->add_button("add_special_items_2", "Add Special Item");
    $list2->add_button("delete_items_2", "Delete All Special Items");
}


/********************************************************************
    Create List for Local Construction Cost Positions
*********************************************************************/ 
$list4 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list4->set_title("Local Construction Cost");
$list4->set_entity("order_item");
$list4->set_filter("order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST . " and order_item_order = " . $project["project_order"]);
$list4->set_order("item_code, order_item_id");

$link="project_edit_material_list_edit_item.php?pid=" . param("pid");

if ($project["order_budget_is_locked"] == 1)
{
    $list4->add_column("item_shortcut", "Item Code");
}
else
{
    $list4->add_column("item_shortcut", "Item Code", $link, "", "", COLUMN_NO_WRAP);
}

$list4->add_column("order_item_text", "Name");
$list4->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);


$list4->set_footer("item_shortcut", "Total");
$list4->set_footer("order_item_system_price", number_format($local_construction_item_total["in_system_currency"],2));

if ($project["order_budget_is_locked"] == 1)
{
    $list4->add_button("nothing", "");
}
else
{
    //$list4->add_button("add_lccost_1", "Add Local Construction Cost from Catalog");
    $list4->add_button("add_lccost_2", "Add Local Construction Cost Estimation");
    $list4->add_button("delete_items_4", "Delete All Local Construction Cost Positions");
}

$list4->add_column("order_item_supplier_freetext", "Supplier Info");

/********************************************************************
    Create List for Cost Estimation Positions
*********************************************************************/ 
$list3 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list3->set_title("Cost Estimation");
$list3->set_entity("order_item");
$list3->set_filter("order_item_type = " . ITEM_TYPE_COST_ESTIMATION . " and order_item_order = " . $project["project_order"]);
$list3->set_order("item_code");

$link="project_edit_material_list_edit_item.php?pid=" . param("pid");

if ($project["order_budget_is_locked"] == 1)
{
    $list3->add_column("item_shortcut", "Item Code");
}
else
{
    $list3->add_column("item_shortcut", "Item Code", $link, "", "", COLUMN_NO_WRAP);
}

$list3->add_column("order_item_text", "Name");
$list3->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);


$list3->set_footer("item_shortcut", "Total");
$list3->set_footer("order_item_system_price", number_format($cost_estimation_item_total["in_system_currency"],2));

if ($project["order_budget_is_locked"] == 1)
{
    $list3->add_button("nothing", "");
}
else
{
    $list3->add_button("add_cost_1", "Add Cost Estimation from Catalog");
    $list3->add_button("add_cost_2", "Add Individual Cost Estimation");
    $list3->add_button("delete_items_3", "Delete All Cost Estimation Positions");
}

$list3->add_column("order_item_supplier_freetext", "Supplier Info");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list1->populate();
$list1->process();

if ($list1->button("add_items"))
{
    $link = "project_add_catalog_item_categories.php?pid=" . param("pid") . "&oid=" . $project["project_order"]; 
    redirect($link);
}
elseif ($list1->button("save_items"))
{
	
	// check if quntity has changed and set order_date to NULL
	foreach ($list1->values("item_entry_field") as $key=>$value)
    {
		if($quantities[$key] > 0 and $quantities[$key] != $value)
		 {
			 $revision = $revisions[$key] + 1;
			 $sql = "update order_items set " .
				   "order_item_ordered = NULL, " .
				   "order_item_order_revisions = " . $revision .
				   " where order_item_id = " . $key;

			$res = mysql_query($sql) or dberror($sql);
			
		 }
	}
	
	project_update_order_items($list1);
	$link = "project_edit_material_list.php?pid=" . param("pid");
	redirect($link);
}
elseif ($list1->button("delete_items_1"))
{
    //order_delete_all_items($project["project_order"], 1);
    $link = "project_edit_material_list_delete_items.php?pid=" . param("pid") . "&oid=" . param("oid") . "&t=1";
    redirect($link);
}

$list2->populate();
$list2->process();


if ($list2->button("add_special_items_1"))
{
    $link = "project_add_catalog_item_special_item_list.php?pid=" . param("pid") . "&oid=" . param("oid");
    redirect($link);
}
else if ($list2->button("add_special_items_2"))
{
    $link = "project_add_special_item_individual.php?pid=" . param("pid") . "&oid=" . $project["project_order"]; 
    redirect($link);
}
elseif ($list2->button("save_items"))
{
    
	// check if quntity has changed and set order_date to NULL
	foreach ($list2->values("item_entry_field") as $key=>$value)
    {
		if($quantities[$key] > 0 and $quantities[$key] != $value)
		 {
			 $revision = $revisions[$key] + 1;
			 $sql = "update order_items set " .
				   "order_item_ordered = NULL, " .
				   "order_item_order_revisions = " . $revision .
				   " where order_item_id = " . $key;

			 $res = mysql_query($sql) or dberror($sql);
		 }
	}
	project_update_order_items($list2);
}
elseif ($list2->button("delete_items_2"))
{
    $link = "project_edit_material_list_delete_items.php?pid=" . param("pid") . "&oid=" . param("oid") . "&t=2";
    redirect($link);
    //order_delete_all_items($project["project_order"], 2);
}

$list3->populate();
$list3->process();


if ($list3->button("add_cost_1"))
{
    $link = "project_add_catalog_item_cost_estimation_list.php?pid=" . param("pid") . "&oid=" . param("oid");
    redirect($link);
}
else if ($list3->button("add_cost_2"))
{
    $link = "project_add_cost_estimation_individual.php?pid=" . param("pid") . "&oid=" . $project["project_order"]; 
    redirect($link);
}
elseif ($list3->button("delete_items_3"))
{
    $link = "project_edit_material_list_delete_items.php?pid=" . param("pid") . "&oid=" . param("oid") . "&t=3";
    redirect($link);
    //order_delete_all_items($project["project_order"], 3);
}


$list4->populate();
$list4->process();


if ($list4->button("add_lccost_1"))
{
    $link = "project_add_catalog_item_lc_cost_estimation_list.php?pid=" . param("pid") . "&oid=" . param("oid");
    redirect($link);
}
else if ($list4->button("add_lccost_2"))
{
    $link = "project_add_lc_cost_estimation_individual.php?pid=" . param("pid") . "&oid=" . $project["project_order"]; 
    redirect($link);
}
elseif ($list4->button("delete_items_4"))
{
    $link = "project_edit_material_list_delete_items.php?pid=" . param("pid") . "&oid=" . param("oid") . "&t=6";
    redirect($link);
    //order_delete_all_items($project["project_order"], 6);
}



/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Edit List of Materials");
$form->render();
echo "<br>";
$list1->render();
echo "<br>";
$list2->render();
echo "<br>";
$list4->render();
echo "<br>";
$list3->render();
echo "<br>";

$page->footer();

?>