<?php
/********************************************************************


    project_edit_attachment.php


    Add comments to a project


    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-10-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-06-18
    Version:        1.0.1


    Copyright (c) 2002, Swatch AG, All Rights Reserved.


*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_edit_attachment_data_in_projects");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));


// get company's address
$client_address = get_address($project["order_client_address"]);


// buld sql for attachment categories
$sql_attachment_categories = "select order_file_category_id, order_file_category_name ".
                             "from order_file_categories ".
                             "where order_file_category_type = 2 ".
                             "order by order_file_category_priority";


// buld sql for file types
$sql_file_types = "select file_type_id, file_type_name ".
                  "from file_types ".
                  "order by file_type_name";




// get addresses involved in the project
$companies = get_involved_companies($project["project_order"], id());

// determine users that have already got an email announcing the new attachment
$old_recipients = array();
foreach($companies as $key=>$value)
{
    if($value["access"] == 1)
    {
        $old_recipients[] = $value["user"];
    }
}


// checkbox names
$check_box_names = array();


// get file owner
$owner = get_file_owner(id());


/********************************************************************
    Create Form
*********************************************************************/ 


$form = new Form("order_files", "file");


$form->add_section("Project");
$form->add_hidden("pid", param("pid"));
$form->add_hidden("order_file_order", $project["project_order"]);
$form->add_hidden("order_file_owner", user_id());


require_once "include/project_head_small.php";


$form->add_section("Attachment");
$form->add_list("order_file_category", "Category*", $sql_attachment_categories, NOTNULL);
$form->add_edit("order_file_title", "Title*", NOTNULL, "", TYPE_CHAR);
$form->add_multiline("order_file_description", "Description*", 4);


if (has_access("can_set_attachment_accessibility_in_projects"))
{
    $form->add_section("Accessibility");
    $form->add_comment("Please indicate who is allowed to view this attachment.");


    $num_checkboxes=1;
    foreach ($companies as $key=>$value_array)
    {
        if(!array_key_exists('role', $value_array)) {
			$value_array["role"] = "";
		}
		$form->add_checkbox("A" . $num_checkboxes, $value_array["name"], $value_array["access"], 0, $value_array["role"]);
        $check_box_names["A" . $num_checkboxes] = $value_array["user"];
        $num_checkboxes++;
    }
}

$order_number = $project["order_number"];


$form->add_section("File");
$form->add_list("order_file_type", "File Type*", $sql_file_types, NOTNULL);
$form->add_upload("order_file_path", "File", "/files/orders/$order_number", NOTNULL);


$form->add_button(FORM_BUTTON_SAVE, "Save");


if(has_access("can_delete_attachment_in_projects")  or $owner == user_id())
{
    $form->add_button("delete", "Delete");
}

$form->add_button(FORM_BUTTON_BACK, "Back");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


if ($form->button(FORM_BUTTON_SAVE))
{
    // check if a recipient was selected
    if (has_access("can_set_attachment_accessibility_in_projects"))
    {
        $no_recipient = 1;
    }
    else
    {
        $no_recipient = 0;
    }


    foreach ($form->items as $item)
    {
        if ($item["type"] == "checkbox" and $item["value"] == "1")
        {
            $no_recipient = 0;
        }
    }


	$invalid_file_extension = false;
	
	if($form->value("order_file_category") == 11)
	{
		$ext = pathinfo($form->value("order_file_path"), PATHINFO_EXTENSION);
		
		if($ext != 'jpg' and $ext != 'JPG')
		{
			$invalid_file_extension = true;
		}
	}


    if ($form->validate() and $no_recipient == 0 and $invalid_file_extension == false)
    {
		update_attachment_accessibility_info(id(), $form,  $check_box_names);

		
        $link = "project_view_attachments.php?pid=" . param("pid");
        redirect($link);
    }
    else
    {
        if($invalid_file_extension == true)
		{
			$form->error("Please upload only files of the type 'jpg'.");
		}
		else
		{
			$form->error("Please select a least one person to have access to the attachment.");
		}
    }
}
elseif ($form->button("delete"))
{
    delete_attachment(id());
    $link = "project_view_attachments.php?pid=" . param("pid");
    redirect($link);
}




$page = new Page("projects");


require "include/project_page_actions.php";


$page->header();
$page->title("Edit Attachment Data");
$form->render();
$page->footer();


?>