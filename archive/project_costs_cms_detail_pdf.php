<?php
/********************************************************************

    project_costs_cms_detail_pdf.php

    View project cost monitoring sheet in a PDF

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-01-09
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2014, OMEGA SA, All Rights Reserved.

*********************************************************************/


function print_page_header($pdf, $page_title)
{
	$pdf->AddPage("l","A4", $page_title);
	$pdf->SetY(8);
	$pdf->Image('../pictures/logo.jpg',10,8,33);
	$pdf->SetFont('arialn','B',12);
	$pdf->Cell(80);
	$pdf->Cell(0,10, $page_title, 0, 0, 'R');
	$pdf->Ln(5);
	$pdf->SetFont('arialn','I',8);
	$pdf->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$pdf->PageNo() ." ",0,0,'R');
	$pdf->Ln(15);
}


/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());

// read project and order details
$project = get_project(param("pid"));

// get orders's currency
$order_currency = get_order_currency($project["project_order"]);

$page_title = "Cost Monitoring Sheet - Project " . $project["project_number"];

$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];


$order_state_name = get_actual_order_state_name($project["order_actual_order_state_code"], 1);
$retail_coordinator = get_user($project["project_retail_coordinator"]);
$retail_operator = get_user($project["order_retail_operator"]);

$project_cost_gross_sqms = $project["project_cost_gross_sqms"];
$project_cost_total_sqms = $project["project_cost_totalsqms"];
$project_cost_sqms = $project["project_cost_sqms"];




//get budget totals
$budget = get_project_budget_totals(param("pid"));


//get cost groups
$costgroups = get_cost_groups();

//get cer totals
$cer_totals = get_project_cer_totals(param("pid"), $costgroups);


//get cost sub groups
$cost_sub_groups = array();
$sql = "select costsheet_id, costsheet_pcost_group_id, costsheet_pcost_subgroup_id, " . 
	   "costsheet_budget_amount, costsheet_budget_approved_amount, costsheet_real_amount, " . 
	   "costsheet_code, costsheet_text, costsheet_comment, " .
	   "concat(pcost_subgroup_code, ' ', pcost_subgroup_name) as subgroup  " .
	   "from costsheets " .
	   " left join pcost_subgroups on pcost_subgroup_id = costsheet_pcost_subgroup_id " . 
	   " where costsheet_project_id = " . param("pid") . 
	   " and costsheet_is_in_budget = 1 " . 
	   " order by subgroup ";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$cost_sub_groups[$row["costsheet_pcost_group_id"]][$row["costsheet_pcost_subgroup_id"]] = $row["subgroup"];
}


//get all cost positions
$costsheet_positions = array();
$sql = "select costsheet_id, costsheet_pcost_group_id, costsheet_pcost_subgroup_id, " . 
	   "costsheet_budget_amount, costsheet_sg_approved_amount as costsheet_budget_approved_amount, costsheet_real_amount, " . 
	   "costsheet_code, costsheet_text, costsheet_comment2, costsheet_company " .
	   "from costsheets " .
	   " where costsheet_project_id = " . param("pid") . 
	   " order by LENGTH(costsheet_code), costsheet_code ";
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$tmp = array();
	$tmp['code'] = $row["costsheet_code"];
	$tmp['text'] = trim(str_replace("\r\n", ' ', $row["costsheet_text"]));
	$tmp['company'] = trim(str_replace("\r\n", ' ', $row["costsheet_company"]));
	$tmp['comment'] = trim(str_replace("\r\n", ' ', $row["costsheet_comment2"]));
	$tmp['budget_amount'] = $row["costsheet_budget_amount"];
	$tmp['budget_approved_amount'] = $row["costsheet_budget_approved_amount"];
	$tmp['real_amount'] = $row["costsheet_real_amount"];

	$costsheet_positions[$row["costsheet_pcost_group_id"]][$row["costsheet_pcost_subgroup_id"]][$row["costsheet_id"]] = $tmp;
}

/********************************************************************
    prepare pdf Data
*********************************************************************/

$captions1 = array();
$captions1[] = "Project Number:";
$captions1[] = "Product Line / Type / POS Type Subclass:";
$captions1[] = "Project Starting Date / Opening Date:";
$captions1[] = "Status:";
$captions1[] = "Project Manager / Retail Operator:";
$captions1[] = "Project Legal Type / Gross, Total and Sales Surface in sqms:";
$captions1[] = "";
$captions1[] = "";

$data1 = array();
$data1[] = $project["project_number"];
$data1[] = $project["product_line_name"] . " / " . $project["postype_name"] . " / " . $project["possubclass_name"];
$data1[] = to_system_date($project["order_date"]) . " / " .  to_system_date($project["project_actual_opening_date"]);
$data1[] = $project["order_actual_order_state_code"] . " " . substr($order_state_name, 0, 30) . '...';
$data1[] = $retail_coordinator["firstname"] . " " . $retail_coordinator["name"] . " / " . $retail_operator["firstname"] . " " . $retail_operator["name"];
$data1[] = $project["project_costtype_text"] . " / " . $project_cost_gross_sqms .", " . $project_cost_total_sqms . ", " . $project_cost_sqms. " sqms";
$data1[] = "";
$data1[] = "";

$captions2 = array();
$captions2[] = "Amounts in " . $order_currency["symbol"];
$captions2[] = "Totals:";

$i = 0;
foreach($costgroups as $group_id=>$group_name)
{
	if($i<5)
	{
		$captions2[] = $group_name;
		$i++;
	}
}

$captions2[] = "Amounts per m2 of sales surface:";
    
$data2 = array();
$tmp = array();

if($project["project_cost_type"] == 1) // corporate
{
	$tmp[] = "HQ Approved";
	$tmp[] = "SG approved";
	$tmp[] = "Real Cost";
	$tmp[] = "Difference";
	$tmp[] = "% SG/Real";
}
else
{
	$tmp[] = "HQ Approved";
	$tmp[] = "SG approved";
	$tmp[] = "Real Cost";
	$tmp[] = "Difference";
	$tmp[] = "% SG/Real";
}
$data2[] = $tmp;




	
//totals
$approved_budget = number_format($budget["approved_budget_total"], 2);
$cer_approved_total = number_format($budget["sg_approved_budget_total"], 2);
$real_cost = number_format($budget["real_total"], 2);

$difference1 = $budget["real_total"] - $cer_totals["investments_approved_total"] - $cer_totals["investments_additional_approved_total"];
$difference1 = number_format($difference1, 2);


$difference1 = $budget["real_total"] - $budget["sg_approved_budget_total"];
$difference1 = number_format($difference1, 2);

if($budget["sg_approved_budget_total"] > 0)
{
	$difference2 = ($budget["real_total"] - $budget["sg_approved_budget_total"]) /$budget["sg_approved_budget_total"];
	
	$difference2 = number_format(round(100*$difference2, 2), 2, ".", "'") . '%';
}
else
{
	$difference2 = "100.00%";
}

$data2[] = array($approved_budget, $cer_approved_total, $real_cost, $difference1, $difference2);


//group totals
$i = 0;
foreach($costgroups as $group_id=>$group_name)
{
	if($i<5 and array_key_exists($group_id, $budget["group_approved_totals"]))
	{
		$approved_budget = number_format($budget["group_approved_totals"][$group_id], 2);
		$cer_approved_total = number_format($budget["group_sg_approved_totals"][$group_id], 2);
		$real_cost = number_format($budget["group_real_totals"][$group_id], 2);


		$difference1 = $budget["group_real_totals"][$group_id] - $budget["group_sg_approved_totals"][$group_id];
		$difference1 = number_format($difference1, 2);

		if($budget["group_sg_approved_totals"][$group_id] > 0)
		{
			$difference2 = ($budget["group_real_totals"][$group_id] - $budget["group_sg_approved_totals"][$group_id]) / $budget["group_sg_approved_totals"][$group_id];
			
			$difference2 = number_format(round(100*$difference2, 2), 2, ".", "'") . '%';
		}
		else
		{
			$difference2 = "100.00%";
		}


		$data2[] = array($approved_budget, $cer_approved_total,$real_cost, $difference1, $difference2);
		
		$i++;
	}
}

//per sqm of sales area
$approved_budget = "";
$cer_approved_total = "";
$cer_additional_total = "";
$real_cost = "";

if($project_cost_sqms > 0)
{
	$approved_budget = number_format($budget["approved_budget_total"]/$project_cost_sqms, 2);
	$cer_approved_total = number_format($budget["sg_approved_budget_total"]/$project_cost_sqms, 2);
	$real_cost = number_format($budget["real_total"]/$project_cost_sqms, 2);
}
$data2[] = array($approved_budget, $cer_approved_total, $real_cost, "", "");
	
	
	


/********************************************************************
   output data
*********************************************************************/

$pdf->SetAutoPageBreak(true, 5);

$pdf->SetLineWidth(0.1);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

print_page_header($pdf, $page_title);


// output project header informations


// output project header informations
$pdf->SetFont('arialn','B',10);
$pdf->Cell(278,5, $project["postype_name"] .": " . $shop,1);
$pdf->Cell(13,5," ",0);
$pdf->Ln();

$pdf->SetFont('arialn','',8);
for($i=0;$i<8;$i++)
{
    
	$pdf->Cell(58,4,$captions1[$i],1);
	$pdf->Cell(50,4,$data1[$i],1);

	$pdf->Cell(2,4," ",0);

	$pdf->Cell(58,4,$captions2[$i],1);
    
    
	if(array_key_exists($i, $data2))
	{
		$line = $data2[$i];

		for($k=0;$k<7;$k++)
		{
			if(!array_key_exists($k, $line))
			{
				$line[$k] = "";
			}
		}
	}
	else
	{
		$line[0] = "";
		$line[1] = "";
		$line[2] = "";
		$line[3] = "";
		$line[4] = "";

	}


	
    
	$pdf->Cell(22,4,$line[0],1, 0, "R");
    
	$pdf->Cell(22,4,$line[1],1, 0, "R");
    $pdf->Cell(22,4,$line[2],1, 0, "R");
	
	$pdf->Cell(22,4,$line[3],1, 0, "R");

    $pdf->Cell(22,4,$line[4],1, 0, "R");

	

    $pdf->Ln();
}



$pdf->Ln();

$pdf->Ln();






//print cost group details
$pdf->SetFillColor(240, 240, 240);
//$pdf->setCellHeightRatio(0.8);


$group_page_break_controller = "";
$number_of_groups = count($costgroups);
$group_counter = 0;

foreach($costgroups as $group_id=>$group_name)
{
	
	$y = $pdf->getY();
	if($y >= 185)
	{
		print_page_header($pdf, $page_title);
	}
	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(110 ,5, $group_name, 1, 0, 'L', 1);
	$pdf->Cell(35 ,5, "Company", 1, 0, 'L', 1);
	$pdf->Cell(75 ,5, "Comments", 1, 0, 'L', 1);
	$pdf->Cell(24 ,5, "Approved " . $order_currency["symbol"] , 1, 0, 'R', 1);
	$pdf->Cell(24 ,5, "Real " . $order_currency["symbol"] , 1, 0, 'R', 1);
	$pdf->Cell(10 ,5, '%' , 1, 0, 'R', 1);
	$pdf->Ln();

	if(array_key_exists($group_id, $cost_sub_groups))
	{
		foreach($cost_sub_groups[$group_id] as $sub_group_id=>$subgroup_name)
		{
			if(array_key_exists($group_id, $budget["subgroup_totals_by_id"]))
			{
				if(array_key_exists($group_id, $costsheet_positions))
				{
					$y = $pdf->getY();
					if($y >= 185)
					{
						print_page_header($pdf, $page_title);
					}

					
					$pdf->SetFont('arialn','B',8);
					$pdf->Cell(110 ,4, $subgroup_name, 1, 0, 'L', 1);
					$pdf->Cell(35 ,4, "", 1, 0, 'L', 1);
					$pdf->Cell(75 ,4, "", 1, 0, 'L', 1);
					$pdf->Cell(24 ,4, number_format($budget["subgroup_approved_totals_by_id"][$group_id][$sub_group_id], 2) , 1, 0, 'R', 1);

					$pdf->Cell(24 ,4, number_format($budget["subgroup_real_totals_by_id"][$group_id][$sub_group_id], 2) , 1, 0, 'R', 1);
					
					$share = "";
					if($budget["subgroup_real_totals_by_id"][$group_id][$sub_group_id] > 0 and $budget["real_total"] > 0)
					{
						$share = number_format(round(100*($budget["subgroup_real_totals_by_id"][$group_id][$sub_group_id] / $budget["real_total"]), 2), 2)  . '%';
					}
					$pdf->Cell(10 ,4, $share , 1, 0, 'R', 1);

					$pdf->Ln();

					$pdf->SetFont('arialn','',8);

					foreach($costsheet_positions[$group_id][$sub_group_id] as $key=>$data)
					{

						
						
						$num_lines = $pdf->getStringHeight(100, $data['text'])/3.528;
						$num_lines2 = $pdf->getStringHeight(75, $data['comment'])/3.528;
						
						//add new page if necessary
						$y = $pdf->getY();
						if($y >= 185 or $y+$num_lines > 185 or $y+$num_lines2 > 185)
						{
							print_page_header($pdf, $page_title);
						}
						
						//output data
						$pdf->Cell(10 ,4, $data['code'], 0, 0, 'L', 0, 0);
						$pdf->MultiCell(100, '', $data['text'],0,'L', false, 0);
						$pdf->MultiCell(35, 0,$data['company'],0,'L', false, 0);
						$pdf->MultiCell(75, 0,$data['comment'],0,'L', false, 0);
						$pdf->Cell(24 ,4, number_format($data['budget_approved_amount'], 2), 0, 0, 'R');
						$pdf->Cell(24 ,4, number_format($data['real_amount'], 2), 0, 0, 'R');

						$share = "";
						if($data['real_amount']> 0 and $budget["real_total"] > 0)
						{
							$share = number_format(round(100*($data['real_amount'] / $budget["real_total"]), 2), 2) . '%';
						}
						$pdf->Cell(10 ,4, $share , 0, 0, 'R', 0);
						
						
						
						if($num_lines2 > $num_lines){$num_lines = $num_lines2;}
						for($i=0;$i<$num_lines;$i++)
						{
							$pdf->Ln();
						}
						$pdf->Line(10,$pdf->GetY(), 287, $pdf->GetY());
						
					}
				}
			}
		}

		$group_counter++;
		if($number_of_groups > $group_counter and $group_page_break_controller != $group_id)
		{
			print_page_header($pdf, $page_title);
			$pdf->setY($pdf->getY()-15);
			$group_page_break_controller = $group_id;
		}
	}


	$pdf->Ln();
	$pdf->Ln();
}




?>