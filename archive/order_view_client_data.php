<?php
/********************************************************************

    order_view_client_data.php

    See the order details.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-03
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-19
    Version:        1.0.4

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
//require_once "include/order_functions.php";

check_access("can_view_client_data_in_orders");

set_referer("orders.php");
set_referer("order_new.php");

/********************************************************************
    prepare all data needed
*********************************************************************/

// read project and order details
if (param("oid"))
{
    $order = get_order(param("oid"));
}
else
{    
    $order = get_order(id());
}


// get company's address
$client_address = get_address($order["order_client_address"]);

// read design_item_ids from project items
$project_design_objective_item_ids = get_project_design_objective_item_ids(id());

// read information from order_addresses
$delivery_address = get_order_address(2, $order["order_id"]);


//get province
$billing_address_province_name = "";
if($order["order_billing_address_place_id"])
{
	$billing_address_province_name = get_province_name($order["order_billing_address_place_id"]);
}


$delivery_address_province_name = "";
if($delivery_address["place_id"])
{
	$delivery_address_province_name = get_province_name($delivery_address["place_id"]);
}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("orders", "order", 600);

$form->add_label("order_number", "Order Number", 0, $order["order_number"]);

$order_state_name = get_actual_order_state_name($order["order_actual_order_state_code"], 2);
$form->add_label("status", "Status", 0, $order["order_actual_order_state_code"] . " " . $order_state_name);

if ($order["order_retail_operator"])
{
    $form->add_lookup("retail_operator", "Retail Operator", "users", "concat(user_name, ' ', user_firstname)", 0, $order["order_retail_operator"]);
}
else
{
    $form->add_label("retail_operator", "Retail Operator");
}


// Client Address


$form->add_section("Client Address");
$form->add_label("client_address_company", "Company", 0, $client_address["company"]);


if ($client_address["company2"])
{
    $form->add_label("client_address_company2", "", 0, $client_address["company2"]);
}


$form->add_label("client_address_address", "Address", 0, $client_address["address"]);


if ($client_address["address2"])
{
    $form->add_label("client_address_address2", "", 0, $client_address["address2"]);
}


$form->add_label("client_address_place", "City", 0, $client_address["zip"] . " " . $client_address["place"]);
$form->add_lookup("client_address_country", "", "countries", "country_name", 0, $client_address["country"]);
$line = "concat(user_name, ' ', user_firstname)";
$form->add_lookup("client_address_contact", "Contact", "users", $line , 0, $order["order_user"]);
$form->add_lookup("clientaddress_phone", "Phone", "users", "user_phone", 0, $order["order_user"]);
$form->add_lookup("client_address_fax", "Fax", "users", "user_fax", 0, $order["order_user"]);
$form->add_lookup("client_address_email", "Email", "users", "user_email", 0, $order["order_user"]);



// Notify address

$form->add_section("Notify Address");
$form->add_label("billing_address_company", "Company", 0, $order["order_billing_address_company"]);


if ($order["order_billing_address_company2"])
{
    $form->add_label("billing_address_company2", "", 0, $order["order_billing_address_company2"]);
}


$form->add_label("billing_address_address", "Address", 0, $order["order_billing_address_address"]);


if ($order["order_billing_address_address2"])
{
    $form->add_label("billing_address_address2", "", 0, $order["order_billing_address_address2"]);
}


$form->add_label("billing_address_place", "City", 0, $order["order_billing_address_zip"] . " " . $order["order_billing_address_place"]);
$form->add_label("billing_address_province_name", "Province", 0, $billing_address_province_name);
$form->add_label("billing_address_country_name", "Country", 0, $order["order_billing_address_country_name"]);


$form->add_label("billing_address_phone", "Phone", 0, $order["order_billing_address_phone"]);
$form->add_label("billing_address_fax", "Fax", 0, $order["order_billing_address_fax"]);
$form->add_label("billing_address_email", "Email", 0, $order["order_billing_address_email"]);


// Delivery Address


$form->add_section("Delivery Address");
$form->add_label("delivery_address_company", "Company", 0, $delivery_address["company"]);


if ($delivery_address["company2"])
{
    $form->add_label("delivery_address_company2", "", 0, $delivery_address["company2"]);
}


$form->add_label("delivery_address_address", "Address", 0, $delivery_address["address"]);


if ($delivery_address["address2"])
{
    $form->add_label("delivery_address_address2", "", 0, $delivery_address["address2"]);
}


$form->add_label("delivery_address_place", "City", 0, $delivery_address["zip"]. " " .
                 $delivery_address["place"]);


$form->add_label("delivery_address_province_name", "Province", 0, $delivery_address_province_name);
$form->add_lookup("delivery_address_country", "", "countries", "country_name", 0, $delivery_address["country"]);


$form->add_label("delivery_address_phone", "Phone", 0, $delivery_address["phone"]);
$form->add_label("delivery_address_fax", "Fax", 0, $delivery_address["fax"]);
$form->add_label("delivery_address_email", "Email", 0, $delivery_address["email"]);


// POS Location
$form->add_section("POS Location Address");
$form->add_label("shop_address_company", "Company", 0, $order["order_shop_address_company"]);


if ($order["order_shop_address_company2"])
{
    $form->add_label("shop_address_company2", "", 0, $order["order_shop_address_company2"]);
}


$form->add_label("shop_address_address", "Address", 0, $order["order_shop_address_address"]);


if ($order["order_shop_address_address2"])
{
    $form->add_label("shop_address_address2", "", 0, $order["order_shop_address_address2"]);
}


$form->add_label("shop_address_place", "City", 0, $order["order_shop_address_zip"] . " " . 
                 $order["order_shop_address_place"]);


$form->add_label("shop_address_country_name", "Country", 0, $order["order_shop_address_country_name"]);

$form->add_label("shop_address_phone", "Phone", 0, $order["order_shop_address_phone"]);
$form->add_label("shop_address_fax", "Fax", 0, $order["order_shop_address_fax"]);
$form->add_label("shop_address_email", "Email", 0, $order["order_shop_address_email"]);

// Location info


$form->add_section("Location Info");
$form->add_lookup("voltage_name", "Voltage", "voltages", "voltage_name", 0, $order["order_voltage"]);


// Traffic checklist


$form->add_section("Preferences and Traffic Checklist");
$form->add_label("preferred_delivery_date", "Preferred Arrival Date", 0, to_system_date($order["order_preferred_delivery_date"]));

$form->add_lookup("preferred_transportation_arranged", "Transportation arranged by", "transportation_types", "transportation_type_name", 0, $order["order_preferred_transportation_arranged"]);

$form->add_lookup("preferred_transportation_mode", "Transportation mode", "transportation_types", "transportation_type_name", 0, $order["order_preferred_transportation_mode"]);



/*
$value="no";
if ($order["order_packaging_retraction"])
{           
    $value="yes";
}
$form->add_label("packing_retraction", "Packaging Retraction Desired", 0, $value);
*/


$value="no";
if ($order["order_pedestrian_mall_approval"])
{
    $value="yes";
}
$form->add_label("pedestrian_mall_approval", "Pedestrian Area Approval Needed", 0, $value);


$value="no";
if ($order["order_full_delivery"])
{
    $value="yes";
}
$form->add_label("full_delivery", "Full Delivery Desired", 0, $value);


$form->add_label("delivery_comments", "Delivery Comments", 0, $order["order_delivery_comments"]);

$form->add_section("Insurance");
if($order["order_insurance"] == 1)
{
	$form->add_label("order_insurance", "Insurance by " . BRAND . "/Forwarder", 0, "covered");
}
else
{
	$form->add_label("order_insurance", "Insurance by " . BRAND . "/Forwarder", 0, "not covered");
}



// General Comments


$form->add_section("General Comments");
$form->add_label("general_comments", "General Comments", 0, $order["order_special_item_request"]);




/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");


require ("include/order_page_actions.php");


$page->header();
$page->title("View Client Data");
$form->render();
$page->footer();


?>