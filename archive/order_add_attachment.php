<?php
/********************************************************************
    order_add_attachment.php
    
    add attachment to an order

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-26
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-18
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.


*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_add_attachments_in_orders");


/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$order = get_order(param("oid"));

// get company's address
$client_address = get_address($order["order_client_address"]);

// buld sql for attachment categories
$sql_attachment_categories = "select order_file_category_id, order_file_category_name ".
                             "from order_file_categories ".
                             "where order_file_category_type = 1 ".
                             "order by order_file_category_priority";

// buld sql for file types
$sql_file_types = "select file_type_id, file_type_name ".
                  "from file_types ".
                  "order by file_type_name";

// get addresses involved in the project
$companies = get_involved_companies(param('oid'), 0);

// checkbox names
$check_box_names = array();

/********************************************************************
    Create Form
********************************************************************/ 
$form = new Form("order_files", "file", 640);
$form->add_section("Order");
$form->add_hidden("oid", param('oid'));
$form->add_hidden("order_file_order", param('oid'));
$form->add_hidden("order_file_owner", user_id());

require_once "include/order_head_small.php";

$form->add_section("Attachment");
$form->add_list("order_file_category", "Category*", $sql_attachment_categories, NOTNULL);
$form->add_edit("order_file_title", "Title*", NOTNULL, "", TYPE_CHAR);
$form->add_multiline("order_file_description", "Description*", 4);


$form->add_hidden("supplier1", 0);
$form->add_hidden("supplier2", 0);

if (has_access("can_set_attachment_accessibility_in_orders"))
{
    $form->add_section("Accessibility");
    $form->add_comment("Please indicate who is allowed to view this attachment.");


    $num_checkboxes=1;
    foreach ($companies as $key=>$value_array)
    {
        if(!array_key_exists('role', $value_array)) {
			$value_array["role"] = "";
		}

		$form->add_checkbox("A" . $num_checkboxes, $value_array["name"], "", 0, $value_array["role"]);
        $check_box_names["A" . $num_checkboxes] = $value_array["user"];
        $num_checkboxes++;
    }
}


$order_number = $order["order_number"];


$form->add_section("File");
$form->add_list("order_file_type", "File Type*", $sql_file_types, NOTNULL);
$form->add_upload("order_file_path", "File", "/files/orders/$order_number", NOTNULL);


$form->add_section("CC Recipients");
$form->add_modal_selector("ccmails", "Selected Recipients", 8);


$form->add_button(FORM_BUTTON_SAVE, "Save Attachment");
$form->add_button(FORM_BUTTON_BACK, "Back");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


if ($form->button(FORM_BUTTON_SAVE))
{
    
    $attachment_id = mysql_insert_id();

    // check if a recipient was selected
    if (has_access("can_set_attachment_accessibility_in_orders"))
    {
        $no_recipient = 1;
    }
    else
    {
        $no_recipient = 0;
    }


    foreach ($form->items as $item)
    {
        if ($item["type"] == "checkbox" and $item["value"] == "1")
        {
            $no_recipient = 0;
        }
    }
    
    
    if ($form->validate() and $no_recipient == 0)
    {
        save_attachment_accessibility_info($form,  $check_box_names);


        // send email notifocation to the retail staff
        $order_id = param("oid");


        $sql = "select order_id, order_number, ".
               "users.user_email as recepient, users.user_address as address_id, " .
               "users1.user_email as sender, concat(users1.user_name, ' ', users1.user_firstname) as user_fullname ".
               "from orders ".
               "left join users on order_retail_operator = users.user_id ".
               "left join users as users1 on " . user_id() . "= users1.user_id ".
               "where order_id = " . $order_id;


        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res) and $row["recepient"])
        {
            $subject = MAIL_SUBJECT_PREFIX . ": New attachment was added - Order " . $row["order_number"];
            $sender_email = $row["sender"];
            $sender_name =  $row["user_fullname"];


            $mail = new Mail();
            $mail->set_subject($subject);
            $mail->set_sender($sender_email, $sender_name);


            $bodytext0 = "A new attachment has been added by " . $sender_name . ":";
			$bodytext1 = "A new attachment has been added by " . $sender_name . " for:";


			$reciepients = array();
			$reciepients_cc = array();
			$reciepients_dp = array();
			$reciepients[strtolower($row["recepient"])] = strtolower($row["recepient"]);

            foreach ($check_box_names as $key=>$value)
            {
                if ($form->value($key))
                {
					foreach($companies as $key1=>$company)
					{
						
						if($value == $company["user"])
						{
							$sql = "select user_email, user_email_cc, user_email_deputy ".
								   "from users ".
								   "where (user_id = " . $company["user"] . 
								   "   and user_active = 1)";
							
							$res1 = mysql_query($sql) or dberror($sql);
							if ($row1 = mysql_fetch_assoc($res1))
							{
								$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
								$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
								$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
							}
						}
					}
                }
            }

			//get all ccmails
			$ccmails =  explode(' ', preg_replace("'\r?\n'"," ",$form->value('ccmails'))); 
			foreach($ccmails as $ccmail) {
				if(is_email_address($ccmail)) {
					
					$reciepients_cc[strtolower($ccmail)] = strtolower($ccmail);
				}
			
			}

			foreach($reciepients as $key=>$email)
			{
				$bodytext1 = $bodytext1 . "\n" . $email;
				$mail->add_recipient($email);
			}


			foreach($reciepients_cc as $key=>$email)
			{
				$bodytext1 = $bodytext1 . "\n" . $email;
				$mail->add_cc($reciepients_cc[$email]);
			}

			foreach($reciepients_dp as $key=>$email)
			{
				$bodytext1 = $bodytext1 . "\n" . $email;
				$mail->add_cc($reciepients_dp[$email]);
			}

            
            $bodytext0.= "\n\n<--\n";
            $bodytext0.= $form->value("order_file_title") . "\n";
            
			/*
			if($form->value("order_file_description"))
            {
                $bodytext0.= $form->value("order_file_description") . "\n\n";
            }
            $bodytext0.= "download the file here:\n" . APPLICATION_URL . $form->value("order_file_path");
            $bodytext0.= "\n-->";
			*/

            $link ="order_view_attachment.php?oid=" . $order["order_id"] . "&id=" .  $attachment_id;
            $bodytext = $bodytext0 . "\n\nclick below to have direct access to the order:\n";
            $bodytext = $bodytext .  APPLICATION_URL . "/archive/" . $link . "\n\n";           
            $mail->add_text($bodytext);
            $mail->send();


			$bodytext1.= "\n\n<--\n";
            $bodytext1.= $form->value("order_file_title") . "\n";
            
			/*
			if($form->value("order_file_description"))
            {
                $bodytext1.= $form->value("order_file_description") . "\n\n";
            }
            $bodytext1.= "download the file here:\n" . APPLICATION_URL . $form->value("order_file_path");
            $bodytext1.= "\n-->";
			*/

            $link ="order_view_attachment.php?oid=" . $order["order_id"] . "&id=" .  $attachment_id;
            $bodytext = $bodytext1 . "\n\nclick below to have direct access to the order:\n";
            $bodytext = $bodytext .  APPLICATION_URL . "/archive/" . $link . "\n\n";   

            append_mail($order["order_id"], "" , user_id(), $bodytext, "", 2);
        }
        $link = "order_view_attachments.php?oid=" . param("oid");
        redirect($link);
    }
    else
    {
        $form->error("Please select a least one person to have access to the attachment.");
    }
}






$page = new Page("orders");


require "include/order_page_actions.php";


$page->header();
$page->title("Add an Attachment");
$form->render();

?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#ccmails_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/shared/select_mail_recipients.php'
    });
    return false;
  });
});
</script>

<?php

$page->footer();


?>