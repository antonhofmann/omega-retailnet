<?php
/********************************************************************


    project_edit_comment.php


    edit comments of a project


    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-02-28
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-06-18
    Version:        1.0.0


    Copyright (c) 2002, Swatch AG, All Rights Reserved.


*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";


check_access("can_edit_comment_data_in_projects");


/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));


// get company's address
$client_address = get_address($project["order_client_address"]);


// bulid sql for comment categoreis
$sql_comment_categories = "select comment_category_id, comment_category_name ".
                          "from comment_categories ".
                          "where comment_category_order_type = 2 ".
                          "order by comment_category_priority ";




// get addresses involved in the project
$companies = get_involved_companies_2($project["project_order"], id());


// determine users that have already got an email announcing the new attachment
$old_recipients = array();
foreach($companies as $key=>$value)
{
    if($value["access"] == 1)
    {
        $old_recipients[] = $value["id"];
    }
}


// checkbox names
$check_box_names = array();


/********************************************************************
    Create Form
*********************************************************************/ 


$form = new Form("comments", "comment");


$form->add_section("Project");
$form->add_hidden("pid", param('pid'));
$form->add_hidden("order_id", $project["project_order"]);


require_once "include/project_head_small.php";


$form->add_section("Comment");
$form->add_list("comment_category", "Category*", $sql_comment_categories, NOTNULL);
$form->add_multiline("comment_text", "Comment", 4, NOTNULL);


if (has_access("can_set_comment_accessibility_in_projects"))
{
    $form->add_section("Accessibility");
    $form->add_comment("Please indicate who is allowed to read this comment.");

	
    $num_checkboxes = 1;
    foreach ($companies as $key=>$value_array)
    {
        if(!array_key_exists('role', $value_array)) {
			$value_array["role"] = "";
		}
		$form->add_checkbox("A" . $num_checkboxes, $value_array["name"], $value_array["access"], 0, $value_array["role"]);
        $check_box_names[$value_array["id"]] = "A" . $num_checkboxes;
        $num_checkboxes++;
    }
	
}

$form->add_section("CC Recipients");
$form->add_modal_selector("ccmails", "Selected Recipients", 8);


$form->add_button(FORM_BUTTON_SAVE, "Save");


if(has_access("can_delete_comment_in_projects"))
{
    $form->add_button("delete", "Delete");
}


$form->add_button(FORM_BUTTON_BACK, "Back");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


if ($form->button(FORM_BUTTON_SAVE))
{
    // check if a recipient was selected
    if (has_access("can_set_comment_accessibility_in_projects"))
    {
        $no_recipient = 1;
    }
    else
    {
        $no_recipient = 0;
    }


    foreach ($form->items as $item)
    {
        if ($item["type"] == "checkbox" and $item["value"] == "1")
        {
            $no_recipient = 0;
        }
    }


    if ($form->validate() and $no_recipient == 0)
    {
        update_comment_accessibility_info(id(), $form,  $check_box_names);


        // send email notifocation to the retail staff
        $order_id = $project["project_order"];


        $sql = "select order_id, order_number, " .
               "order_shop_address_company, order_shop_address_place, " .
               "project_retail_coordinator, country_name, ".
               "users.user_email as recepient1, users.user_address as address_id1, " .
               "users2.user_email as recepient2, users2.user_address as address_id2, " .
               "users1.user_email as sender, concat(users1.user_name, ' ', users1.user_firstname) as user_fullname ".
               "from orders ".
               "left join projects on project_order = " . $order_id . " " .
               "left join countries on country_id = order_shop_address_country " .
               "left join users on order_retail_operator = users.user_id ".
               "left join users as users1 on " . user_id() . "= users1.user_id ".
               "left join users as users2 on project_retail_coordinator = users2.user_id ".
               "where order_id = " . $order_id;

        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res) and $row["recepient2"])
        {
            $subject = MAIL_SUBJECT_PREFIX . ": Comment was updated - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];


            $sender_email = $row["sender"];
            $sender_name =  $row["user_fullname"];


            $mail = new Mail();
            $mail->set_subject($subject);
            $mail->set_sender($sender_email, $sender_name);


            $bodytext0 = "A comment has been updated by " . $sender_name . ":";
			$bodytext1 = "A comment has been updated by " . $sender_name . " for:";


            $recipeint_added = 0;
			$reciepients = array();
			$reciepients_cc = array();
			$reciepients_dp = array();

            foreach ($check_box_names as $key=>$value)
            {
                if ($form->value($value) and !in_array($key, $old_recipients))
                {
					foreach($companies as $key1=>$company)
					{
						
						if($key == $company["id"])
						{
							$sql = "select user_email, user_email_cc, user_email_deputy ".
								   "from users ".
								   "where (user_id = " . $company["user"] . 
								   "   and user_active = 1)";
							
							$res1 = mysql_query($sql) or dberror($sql);
							if ($row1 = mysql_fetch_assoc($res1))
							{
								$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
								$reciepients_cc[strtolower($row1["user_email"])] = strtolower($row1["user_email_cc"]);
								$reciepients_dp[strtolower($row1["user_email"])] = strtolower($row1["user_email_deputy"]);
								$recipeint_added = 1;
							}
						}
					}
                }
            }

			//get all ccmails
			$ccmails =  explode(' ', preg_replace("'\r?\n'"," ",$form->value('ccmails'))); 
			foreach($ccmails as $ccmail) {
				if(is_email_address($ccmail)) {
					
					$reciepients_cc[strtolower($ccmail)] = strtolower($ccmail);
				}
			
			}

			if($recipeint_added == 1)
            {
				foreach($reciepients as $key=>$email)
				{
					$bodytext1 = $bodytext1 . "\n" . $email;
					$mail->add_recipient($email);
				}


				foreach($reciepients_cc as $key=>$email)
				{
					$bodytext1 = $bodytext1 . "\n" . $email;
					$mail->add_cc($reciepients_cc[$email]);
				}

				foreach($reciepients_dp as $key=>$email)
				{
					$bodytext1 = $bodytext1 . "\n" . $email;
					$mail->add_cc($reciepients_dp[$email]);
				}
			}
			else
			{
				foreach($reciepients_cc as $key=>$email)
				{
					$bodytext1 = $bodytext1 . "\n" . $email;
					$mail->add_recipient($reciepients_cc[$email]);
					$recipeint_added = 1;
				}
			}

			$bodytext0.= "\n\n<--\n";
            $bodytext0.= $form->value("comment_text") . "\n";
            $bodytext0.= "-->";


            
            $link ="project_view_comments.php?pid=" . param("pid");
            $bodytext = $bodytext0 . "\n\nclick below to have direct access to the project:\n";
            $bodytext = $bodytext .  APPLICATION_URL . "/archive/" . $link . "\n\n";           
            $mail->add_text($bodytext);
            $mail->send();


			$bodytext1.= "\n\n<--\n";
            $bodytext1.= $form->value("comment_text") . "\n";
            $bodytext1.= "-->";

			$link ="project_view_comments.php?pid=" . param("pid");
            $bodytext = $bodytext1 . "\n\nclick below to have direct access to the project:\n";
            $bodytext = $bodytext .  APPLICATION_URL . "/archive/" . $link . "\n\n";          

            if($recipeint_added == 1)
            {
                append_mail($project["project_order"], "" , user_id(), $bodytext, "", 1);
            }
        }

        $link = "project_view_comments.php?pid=" . param("pid");
        redirect($link);
    }
    else
    {
        $form->error("Please select a least one person to have access to the comment.");
    }
}
elseif ($form->button("delete"))
{
    delete_comment(id());
    $link = "project_view_comments.php?pid=" . param("pid");
    redirect($link);
}




$page = new Page("projects");


require "include/project_page_actions.php";


$page->header();
$page->title("Edit Comment");
$form->render();

?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#ccmails_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/shared/select_mail_recipients.php'
    });
    return false;
  });
});
</script>

<?php

$page->footer();


?>