<?php
/********************************************************************


    project_add_comment.php


    Add comments to a project


    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-25
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-18
    Version:        1.0.1


    Copyright (c) 2002, Swatch AG, All Rights Reserved.


*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";


check_access("can_add_comments_in_projects");


/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));


// get company's address
$client_address = get_address($project["order_client_address"]);


// bulid sql for comment categoreis
$sql_comment_categories = "select comment_category_id, comment_category_name ".
                          "from comment_categories ".
                          "where comment_category_order_type = 2 ".
                          "order by comment_category_priority ";




// get addresses involved in the project
$companies = get_involved_companies($project["project_order"], 0);

//add additional email reciepients
$sql = "select * from additional_mail_recipients " .
       "left join users on user_id = additional_mail_recipient_user " . 
	   "left join addresses on address_id = user_address " . 
       "where additional_mail_recipient_context = 'add_new_comment'";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$user_name = $row["user_name"] . " " . $row["user_firstname"] . " (" . $row["address_company"] . ")";
	$companies[$row["address_id"]] = array("id"=>$row["address_id"], "name"=>$user_name, "user"=>$row["additional_mail_recipient_user"], "access"=>1);
}

// checkbox names
$check_box_names = array();
$check_box_list = array();

/********************************************************************
    Create Form
*********************************************************************/ 


$form = new Form("comments", "comment");


$form->add_section("Project");
$form->add_hidden("pid", param('pid'));
$form->add_hidden("order_id", $project["project_order"]);


require_once "include/project_head_small.php";


$form->add_section("Comment");
$form->add_list("comment_category", "Category*", $sql_comment_categories, NOTNULL);
$form->add_multiline("comment_text", "Comment", 4, NOTNULL);

$form->add_section("Mail Allerts");
$form->add_checkbox("send_mails", "do not send mails", 1, "", "Mails");

if (has_access("can_set_comment_accessibility_in_projects"))
{
    $form->add_section("Accessibility");
    $form->add_comment("Please indicate who is allowed to read this comment.");


    $num_checkboxes = 1;
	$selected_companies = array();
    foreach ($companies as $key=>$value_array)
    {
        
		if(!array_key_exists('role', $value_array)) {
			$value_array["role"] = "";
		}
		$form->add_checkbox("A" . $num_checkboxes, $value_array["name"], "", 0, $value_array["role"]);
		
		$check_box_list[$value_array["id"]] = "A" . $num_checkboxes;

		$check_box_names["A" . $num_checkboxes] = array("company_id"=>$value_array["id"], "checkbox_name"=>"A" . $num_checkboxes);

		$selected_companies["A" . $num_checkboxes] = $value_array;

		$num_checkboxes++;
    }
}

$form->add_section("CC Recipients");
$form->add_modal_selector("ccmails", "Selected Recipients", 8);

$form->add_button("save", "Save Comment");
$form->add_button(FORM_BUTTON_BACK, "Back");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


if ($form->button("save"))
{
    // check if a recipient was selected
    if (has_access("can_set_comment_accessibility_in_projects"))
    {
        $no_recipient = 1;
    }
    else
    {
        $no_recipient = 0;
    }

	$send_mail = 1;
	if($form->value("send_mails") == 1)
	{
		$no_recipient = 1;
		$send_mail = 0;
	}
	

	
    foreach ($form->items as $item)
    {
        if ($item["type"] == "checkbox" and $item["value"] == "1")
        {
            $no_recipient = 0;
        }
    }
	

    if(strlen($form->value("comment_text")) > 600)
    {
        $form->error("The max. number of characters of a comment can be 600 characters only!");
    }
    elseif($form->validate() and $no_recipient == 0)
    {

		$comment_id = save_comment($form, $check_box_list);

		if($send_mail == 1)
		{
			// send email notifocation to the retail staff
			$order_id = $project["project_order"];


			$sql = "select order_id, order_number, " .
				   "order_shop_address_company, order_shop_address_place, " .
				   "project_retail_coordinator, country_name, ".
				   "users.user_email as recepient1, users.user_address as address_id1, " .
				   "users2.user_email as recepient2, users2.user_address as address_id2, " .
				   "users1.user_email as sender, concat(users1.user_name, ' ', users1.user_firstname) as user_fullname ".
				   "from orders ".
				   "left join projects on project_order = " . $order_id . " " .
				   "left join countries on country_id = order_shop_address_country " .
				   "left join users on order_retail_operator = users.user_id ".
				   "left join users as users1 on " . user_id() . "= users1.user_id ".
				   "left join users as users2 on project_retail_coordinator = users2.user_id ".
				   "where order_id = " . $order_id;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res) and $row["recepient2"])
			{
				
				$subject = MAIL_SUBJECT_PREFIX . ": New comment was added - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];

				$sender_email = $row["sender"];
				$sender_name =  $row["user_fullname"];


				$mail = new PHPMailer();
				$mail->Subject = $subject;

				$mail->SetFrom($sender_email, $sender_name);
				$mail->AddReplyTo($sender_email, $sender_name);

				$bodytext0 = "A new comment has been added by " . $sender_name . ":";

				$bodytext1 = "A new comment has been added by " . $sender_name . " for:";

				$tmp="";

				$reciepients = array();
				$reciepients_cc = array();
				$reciepients_dp = array();
				$reciepients[strtolower($row["recepient1"])] = strtolower($row["recepient1"]);
				$reciepients[strtolower($row["recepient2"])] = strtolower($row["recepient2"]);


				foreach ($check_box_names as $key=>$check_box_name)
				{
				
					if ($form->value($check_box_name["checkbox_name"]) == 1)
					{
						$sql = "select user_email, user_email_cc, user_email_deputy ".
							   "from users ".
							   "where (user_id = " . $selected_companies[$key]["user"] . 
							   "   and user_active = 1)";

					
						$res1 = mysql_query($sql) or dberror($sql);
						if ($row1 = mysql_fetch_assoc($res1))
						{
							$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
							$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
							$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
						}
							
					}
				}

				//get all ccmails
				$ccmails =  explode(' ', preg_replace("'\r?\n'"," ",$form->value('ccmails'))); 
				foreach($ccmails as $ccmail) {
					if(is_email_address($ccmail)) {
						
						$reciepients_cc[strtolower($ccmail)] = strtolower($ccmail);
					}
				
				}

				foreach($reciepients as $key=>$email)
				{
					$bodytext1 = $bodytext1 . "\n" . $email;
					$mail->AddAddress($email);
				}

				foreach($reciepients_cc as $key=>$email)
				{
					$bodytext1 = $bodytext1 . "\n" . $email;
					$mail->AddCC($reciepients_cc[$email]);
				}

				foreach($reciepients_dp as $key=>$email)
				{
					$bodytext1 = $bodytext1 . "\n" . $email;
					$mail->AddCC($reciepients_dp[$email]);
				}


				$bodytext0.= "\n\n<--\n";
				$bodytext0.= $form->value("comment_text") . "\n";
				$bodytext0.= "-->";

				
				$link ="project_view_comment.php?pid=" . param("pid")  . "&id=" . $comment_id;
				$bodytext = $bodytext0 . "\n\nclick below to have direct access to the project:\n";
				$bodytext = $bodytext .  APPLICATION_URL . "/archive/" . $link;
				
				$mail->Body = $bodytext;
				$mail->Send();

				
				$bodytext1.= "\n\n<--\n";
				$bodytext1.= $form->value("comment_text") . "\n";
				$bodytext1.= "-->";

				
				$bodytext = $bodytext1 . "\n\nclick below to have direct access to the project:\n";
				$bodytext = $bodytext .  APPLICATION_URL . "/archive/" . $link . "\n\n";   

				append_mail($project["project_order"], "" , user_id(), $bodytext, "", 1);
			}
		}

        $link = "project_view_comments.php?pid=" . param("pid");
        redirect($link);
    }
    else
    {
        $form->error("Please select a least one person to have access to the comment.");
    }
}


$page = new Page("projects");


require "include/project_page_actions.php";


$page->header();
$page->title("Add a Comment");
$form->render();

?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#ccmails_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/shared/select_mail_recipients.php'
    });
    return false;
  });
});
</script>

<?php

$page->footer();


?>