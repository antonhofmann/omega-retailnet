<?php
/********************************************************************
    project_add_lc_cost_estimation_individual.php

    Add individual local construction cost positions to an order

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-07
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-07
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_edit_list_of_materials_in_projects");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

/********************************************************************
    build form
*********************************************************************/
$form = new Form("order_items", "order_item");

$form->add_section("Project");
$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid",$project["project_order"]);
$form->add_hidden("order_item_id", id());
$form->add_hidden("order_item_order", $project["project_order"]);
$form->add_hidden("order_item_type", ITEM_TYPE_LOCALCONSTRUCTIONCOST);

require_once "include/project_head_small.php";

$form->add_section("Item Information");
$form->add_multiline("order_item_text", "Description*", 4, NOTNULL);

$form->add_edit("order_item_supplier_freetext", "Supplier Info");

$sql = "select project_cost_groupname_id, project_cost_groupname_name " . 
       "from project_cost_groupnames " . 
	   "where project_cost_groupname_active = 1 order by project_cost_groupname_id";

$form->add_list("order_item_cost_group", "Cost Group",$sql, NOTNULL);


$form->add_edit("order_item_system_price", "Price in *" . get_system_currency_symbol(), NOTNULL);

$form->add_button("save_data", "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


if ($form->button("save_data"))
{
    if ($form->validate())
    {
        project_add_special_item_save($form);
        $link = "project_edit_material_list.php?pid=" . param("pid"); 
        redirect($link);
    }
}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");


require "include/project_page_actions.php";


$page->header();
$page->title("Add Individual Local Construction Cost Estimation");
$form->render();
$page->footer();


?>