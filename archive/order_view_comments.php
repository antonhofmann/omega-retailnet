<?php
/********************************************************************

    order_view_comments.php

    List of comments made

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-25
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-12-03
    Version:        1.0.6

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_view_comments_in_orders");

register_param("oid");


/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$order = get_order(param("oid"));

// get company's address
$client_address = get_address($order["order_client_address"]);

// get user data
$user_data = get_user(user_id());

// build sql for comment entries
$sql_comments = "select distinct ".
                "    comment_id, comment_text, comments.date_created, ".
                "    comment_category_name, comment_category_priority, ".
                "    concat(user_name, ' ', user_firstname) as user_fullname ".
                "from comments ".
                "left join users on user_id = comment_user ".                
                "left join comment_categories on comment_category_id = comment_category ".
                "left join comment_addresses on comment_address_comment = comment_id";

// build filter for the list of comments
$list1_filter = "comment_category_order_type = 1 and comment_order = " . param('oid');

if (!has_access("has_access_to_all_comments_in_orders"))
{
    $list1_filter = $list1_filter . " and (comment_address_address = " . $user_data["address"] . " or comment_user = " . user_id() . ")";
}


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("comments", "comment", 640);

$form->add_section("Order");
$form->add_hidden("oid", param('oid'));

require_once "include/order_head_small.php";


/********************************************************************
    Create List
*********************************************************************/ 

$list1 = new ListView($sql_comments);

$list1->set_entity("comments");
$list1->set_filter($list1_filter);
$list1->set_order("comments.date_created DESC");
$list1->set_group("comment_category_priority", "comment_category_name");

$list1->add_column("date_created", "Date/Time", "", "", "", COLUMN_NO_WRAP);


$list1->add_column("user_fullname", "Made by", "", "", "", COLUMN_NO_WRAP);
$list1->add_column("comment_text", "Comment", "", "", "", COLUMN_BREAK);


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list1->populate();
$list1->process();



$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("Comments");
$form->render();
$list1->render();
$page->footer();

?>