<?php
/********************************************************************

    order_task_center.php

    List Flow Hitory and Actions

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-11-23
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-11-23
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_use_taskcentre_in_orders");

register_param("oid");
set_referer("order_perform_action.php");


/********************************************************************
    prepare all data needed
*********************************************************************/
// read order details
$order = get_order(param("oid"));


// get company's address
$client_address = get_address($order["order_client_address"]);


//get user roles
$user_roles = get_user_roles(user_id());
$user = get_user(user_id());
$user_name = $user["firstname"] . " " . $user["name"];


if(in_array(1, $user_roles)) // Administrator
{
    //history
    $sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name, " .
              "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
              "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
              "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y') as performed " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id " . "left join actual_order_states on actual_order_state_order = " . param("oid") . " and actual_order_state_state = order_state_id " .
              "left join users on users.user_id = actual_order_state_user " .
              "left join order_mails on order_mail_order = " . param("oid") . " " .
              "and order_mail_order_state = order_state_id " .
              "left join users as tousers on tousers.user_id = order_mail_user " .
              "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

    $filter_hi = "order_state_group_order_type = 2 " .
                 "and users.user_name <> '' ";
}
elseif(in_array(2, $user_roles)) // retrail operator
{
    //history
    $sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name, " .
              "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
              "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
              "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y') as performed " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id " . "left join actual_order_states on actual_order_state_order = " . param("oid") . " and actual_order_state_state = order_state_id " .
              "left join users on users.user_id = actual_order_state_user " .
              "left join order_mails on order_mail_order = " . param("oid") . " " .
              "and order_mail_order_state = order_state_id " .
              "left join users as tousers on tousers.user_id = order_mail_user " .
              "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

    $filter_hi = "order_state_group_order_type = 2 " .
                 "and users.user_name <> '' ";

}
elseif(in_array(4, $user_roles)) // client
{

    //history
    $sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name, " .
              "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
              "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
              "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y') as performed " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id " . "left join actual_order_states on actual_order_state_order = " . param("oid") . " and actual_order_state_state = order_state_id " .
              "left join users on users.user_id = actual_order_state_user " .
              "left join order_mails on order_mail_order = " . param("oid") . " " .
              "and order_mail_order_state = order_state_id " .
              "left join users as tousers on tousers.user_id = order_mail_user " .
              "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

    $filter_hi = "order_state_group_order_type = 2 " .
                 "and users.user_name <> '' " .
                 "and (order_state_performer = 4 " .
                 "or notification_recipient_role = 4) ";
}
elseif(in_array(5, $user_roles) or in_array(29, $user_roles)) // supplier
{
   //history
    $sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name, " .
              "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
              "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
              "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y') as performed " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id " . "left join actual_order_states on actual_order_state_order = " . param("oid") . " and actual_order_state_state = order_state_id " .
              "left join users on users.user_id = actual_order_state_user " .
              "left join order_mails on order_mail_order = " . param("oid") . " " .
              "and order_mail_order_state = order_state_id " .
              "left join users as tousers on tousers.user_id = order_mail_user " .
              "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

    $filter_hi = "order_state_group_order_type = 2 " .
                 "and users.user_name <> '' " .
                 "and (tousers.user_id = " . user_id() .
                 "  or users.user_id = " . user_id() . ")";
}
elseif(in_array(6, $user_roles)) // forwarder
{
    //history
    $sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name, " .
              "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
              "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
              "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y') as performed " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id " . "left join actual_order_states on actual_order_state_order = " . param("oid") . " and actual_order_state_state = order_state_id " .
              "left join users on users.user_id = actual_order_state_user " .
              "left join order_mails on order_mail_order = " . param("oid") . " " .
              "and order_mail_order_state = order_state_id " .
              "left join users as tousers on tousers.user_id = order_mail_user " .
              "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

    $filter_hi = "order_state_group_order_type = 2 " .
                 "and users.user_name <> '' " .
                 "and (tousers.user_id = " . user_id() .
                 "  or users.user_id = " . user_id() . ")";
}
else // all others
{

    //history
    $sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name, " .
              "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
              "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
              "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y') as performed " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id " . "left join actual_order_states on actual_order_state_order = " . param("oid") . " and actual_order_state_state = order_state_id " .
              "left join users on users.user_id = actual_order_state_user " .
              "left join order_mails on order_mail_order = " . param("oid") . " " .
              "and order_mail_order_state = order_state_id " .
              "left join users as tousers on tousers.user_id = order_mail_user " .
              "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

    $filter_hi = "order_state_group_order_type = 2 " .
                 "and users.user_name <> '' ";

}





/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("orders", "task_center", 640);
$form->add_hidden("oid", param("oid"));

$form->add_section("Order");
if(!$order["order_retail_operator"])
{
    $form->error("No Retail Operator has been assigned!");
}

require_once "include/order_head_small.php";


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();



/********************************************************************
    Create History
*********************************************************************/ 
$history = new ListView($sql_hi, LIST_HAS_HEADER);

$history->set_title("History");
$history->set_entity("order_state");
$history->set_filter($filter_hi);
$history->set_order("order_state_code, actual_order_states.date_created ASC");
$history->set_group("order_state_group_code", "order_state_group_name");

$history->add_column("order_state_code", "Step");
$history->add_column("order_state_action_name", "Action");

$history->add_column("fullname", "Performed by");
$history->add_column("performed", "Date");
$history->add_column("tofullname", "Recipient");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$history->populate();
$history->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("Task Centre");
$form->render();

$history->render();


$page->footer();

?>