<?php
/********************************************************************

    order_view_order_swatch_budget.php

    View Order's Budget Sheet

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-12-03
    Version:        1.1.3

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_edit_budget_in_orders");

register_param("oid");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read order details
$order = get_order(param("oid"));

// get company's address
$client_address = get_address($order["order_client_address"]);

// get system currency
$system_currency = get_system_currency_fields();

// get orders's currency
$order_currency = get_order_currency(param('oid'));


$sql_order_items = "select order_item_id, order_item_text, order_item_quantity, ".
                 "    order_item_po_number, item_id, order_item_system_price, ".
                 "    TRUNCATE(order_item_quantity * order_item_system_price, 2) as total_price, ".
                 "    order_item_client_price, ".
                 "    item_code, if(item_code <>'', item_code, item_type_name) as item_shortcut, ".
                 "    concat_ws(': ', product_line_name, category_name) as group_head, " .
                 "    category_priority, category_name, ".
                 "    address_shortcut, item_type_id, item_type_name, ".
                 "    item_type_priority, order_item_type ".
                 "from order_items ".
                 "left join items on order_item_item = item_id ".
                 "left join categories on order_item_category = category_id ".
                 "left join product_lines on product_lines.product_line_id = categories.category_product_line ".
                 "left join addresses on order_item_supplier_address = address_id ".
                 "left join item_types on order_item_type = item_type_id ";

// build line numbers for budget positions
$line_numbers = get_budet_line_numbers(param('oid'));

// build group_totals of standard item groups
$group_totals = get_group_total_of_standard_items(param('oid'), "order_item_system_price");


// build totals
$standard_item_total = get_order_item_type_total(param('oid'),  ITEM_TYPE_STANDARD);
$special_item_total = get_order_item_type_total(param('oid'),  ITEM_TYPE_SPECIAL);
$cost_estimation_item_total = get_order_item_type_total(param('oid'),  ITEM_TYPE_COST_ESTIMATION);

$grand_total_in_system_currency = number_format($standard_item_total["in_system_currency"] + $special_item_total["in_system_currency"],2);
$grand_total_in_order_currency = number_format($standard_item_total["in_order_currency"] + $special_item_total["in_order_currency"],2);
$total_cost = $standard_item_total["in_system_currency"] + $special_item_total["in_system_currency"] + $cost_estimation_item_total["in_system_currency"];


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("orders", "order");

$form->add_section("Order");

require_once "include/order_head_small.php";

// Budget totals

$form->add_section("Budget Totals");
$form->add_label("grand_total1", "Standard/Special Items in " . $system_currency["symbol"], 0, $grand_total_in_system_currency);
$form->add_label("grand_total2", "Estimation of Add. Cost " . $order_currency["symbol"], 0, number_format($cost_estimation_item_total["in_order_currency"],2));
$form->add_label("grand_total3", "Total Cost " . $order_currency["symbol"], 0, number_format($total_cost,2));


/********************************************************************
    Create Standard Item List
*********************************************************************/ 
$list1 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list1->set_title("Catalog Items");
$list1->set_entity("order_item");
$list1->set_filter("(order_item_not_in_budget = 0 or order_item_not_in_budget is null) and order_item_order = " . param('oid') .  " and order_item_type = " . ITEM_TYPE_STANDARD);
$list1->set_order("category_priority, item_code");
$list1->set_group("category_priority", "group_head");

$list1->add_hidden("oid", param('oid'));

$list1->add_text_column("pos", "No.", 0, $line_numbers[ITEM_TYPE_STANDARD]);

$list1->add_column("item_shortcut", "Item Code");   

$list1->add_column("order_item_text", "Name");
$list1->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
$list1->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list1->add_column("total_price", "Total " .  $system_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);


// set group totals
foreach ($group_totals as $key=>$value)
{
    $list1->set_group_footer("total_price", $key ,  number_format($value,2));
}

$list1->set_footer("item_shortcut", "Total");
$list1->set_footer("total_price", number_format($standard_item_total["in_system_currency"],2));


/********************************************************************
    Create Special Item List
*********************************************************************/ 
$list2 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list2->set_title("Special Items");
$list2->set_entity("order_item");
$list2->set_filter("(order_item_not_in_budget = 0 or order_item_not_in_budget is null)  and order_item_order = " . param('oid') .  " and order_item_type = " . ITEM_TYPE_SPECIAL);
$list2->set_order("item_code");

$list2->add_hidden("oid",param('oid'));


$list2->add_text_column("pos", "No.", 0, $line_numbers[ITEM_TYPE_SPECIAL]);

$list2->add_column("item_shortcut", "Item Code");   

$list2->add_column("order_item_text", "Name");
$list2->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
$list2->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list2->add_column("total_price", "Total " . $system_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

$list2->set_footer("item_code", "Total");
$list2->set_footer("total_price", number_format($special_item_total["in_system_currency"],2));



/********************************************************************
    Create Cost Estimation Positions
*********************************************************************/ 
$list3 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list3->set_title("Cost Estimation");
$list3->set_entity("order_item");
$list3->set_filter("order_item_order = " . param('oid') .  " and order_item_type = " . ITEM_TYPE_COST_ESTIMATION);

$list3->add_hidden("oid", param('oid'));

$list3->add_text_column("pos", "No.", 0, $line_numbers[ITEM_TYPE_COST_ESTIMATION]);

$list3->add_column("item_shortcut", "Item Code");   

$list3->add_column("order_item_text", "Text");
$list3->add_column("order_item_system_price", "Estimated Cost " . $system_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);

$list3->set_footer("item_code", "Total");
$list3->set_footer("order_item_system_price", number_format($cost_estimation_item_total["in_system_currency"],2));


/********************************************************************
    Create Exclusion Positions
*********************************************************************/ 
$list4 = new ListView($sql_order_items, LIST_HAS_HEADER);

$list4->set_title("Exclusions");
$list4->set_entity("order_item");
$list4->set_filter("order_item_order = " . param('oid') .  " and order_item_type = " . ITEM_TYPE_EXCLUSION);

$list4->add_hidden("oid", param('oid'));

$list4->add_text_column("pos", "No.", 0, $line_numbers[ITEM_TYPE_EXCLUSION]);

$list4->add_column("item_shortcut", "Item Code");   

$list4->add_column("order_item_text", "Exclusions");

$list4->add_button("nothing", "");


/********************************************************************
    Create Notification Positions
*********************************************************************/ 
$list5 = new ListView($sql_order_items, LIST_HAS_HEADER);

$list5->set_title("Notifications");
$list5->set_entity("order_item");
$list5->set_filter("order_item_order = " . param('oid') .  " and order_item_type = " . ITEM_TYPE_NOTIFICATION);

$list5->add_hidden("oid", param('oid'));

$list5->add_text_column("pos", "No.", 0, $line_numbers[ITEM_TYPE_NOTIFICATION]);

$list5->add_column("item_shortcut", "Item Code");   
$list5->add_column("order_item_text", "Notifications");
$list5->add_button("nothing", "");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list1->populate();
$list1->process();

$list2->populate();
$list2->process();

$list3->populate();
$list3->process();

$list4->populate();
$list4->process();

$list5->populate();
$list5->process();


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("View Budget");

$form->render();
$list1->render();
$list2->render();

// render list1 to 5 only when list of materials is present
if ($grand_total_in_system_currency > 0)
{
    $list3->render();
    $list4->render();
    $list5->render();
}

$page->footer();

?>