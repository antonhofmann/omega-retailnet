<?php
/********************************************************************

    project_print_project_data.php

    Print Project Request

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-10-18
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-10-18
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";
check_access("can_view_client_data_in_projects");


/********************************************************************
    prepare all data needed
*********************************************************************/

$system_currency = get_system_currency_fields();

$project = get_project(param("pid"));

if($project["pipeline"] == 0)
{
	$pos_data = get_poslocation($project["posaddress_id"], "posaddresses");
}
elseif($project["pipeline"] == 1)
{
	$pos_data = get_poslocation_from_pipeline($project["posaddress_id"], $project["order_id"]);
}


if (param("pid"))
{
	global $page_title;
	$page_title = "Project Overview: " . $project["project_number"];

	
	//POS Basic Data
	$posname = $project["order_shop_address_company"];

	if($project["order_shop_address_company2"])
    {
		$posname .= ", " . $project["order_shop_address_company2"];
	}

	$posaddress = $project["order_shop_address_address"];
	
	if($project["order_shop_address_address2"])
    {
		$posaddress .= ", " . $project["order_shop_address_address2"];
	}
	if($project["order_shop_address_zip"])
    {
		$posaddress .= ", " . $project["order_shop_address_zip"] . " " . $project["order_shop_address_place"];
	}
	else
	{
		$posaddress .= ", " . $project["order_shop_address_place"];
	}
	if($project["order_shop_address_country_name"])
    {
		$posaddress .= ", " . $project["order_shop_address_country_name"];
	}

		

	$poslegaltype = $project["project_costtype_text"];
	
	$projectkind = $project["projectkind_name"];

	if($project["project_projectkind"] == 6 and $project["project_relocated_posaddress_id"] > 0) // relocation
	{
		$relocated_pos = get_relocated_pos_info($project["project_relocated_posaddress_id"]);
		$projectkind .= " of " . $relocated_pos["posaddress_name"];
	
	}


	$posphone = $project["order_shop_address_phone"];
	$posfax = $project["order_shop_address_fax"];
	$posemail = $project["order_shop_address_email"];
	$poswebsite = "";
	$plannedopeningdate = to_system_date($project["project_planned_opening_date"]);
	$realisticopeningdate = to_system_date($project["project_real_opening_date"]);

	$posopeningdate = to_system_date($project["project_actual_opening_date"]);
	$posclosingdate = to_system_date($project["project_shop_closingdate"]);

	$projectbudget = number_format($project["project_approximate_budget"], 2);

	$voltagechoice = "";
	$sql = "select voltage_name from voltages where voltage_id = " . dbquote($project["order_voltage"]);
	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$voltagechoice = $row["voltage_name"];
	}

	$furniture_height =$project["project_furniture_height_mm"];

	$locationtype = "";
	$sql = "select location_type_name from location_types where location_type_id = " . dbquote($project["project_location_type"]);
	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$locationtype = $row["location_type_name"];
	}

	if($project["project_location"])
	{
		$locationtype .= "/" .  $project["project_location"];
	}

	
	$designobjectives = array();

	// read design_item_ids from project items
	$sql = "select project_item_item, design_objective_group_name, design_objective_item_name ".
		   "from project_items ".
		   "left join design_objective_items on project_item_item = design_objective_item_id ".
		   "left join design_objective_groups on design_objective_item_group = design_objective_group_id ".
		   "where project_item_project  = " . param("pid") ." ".
		   "order by design_objective_group_priority, design_objective_item_priority";


	$res = mysql_query($sql) or dberror($sql);


	$i=1;
	$group="";
	while ($row = mysql_fetch_assoc($res))
	{
		if ($group != $row["design_objective_group_name"])
		{
			$designobjectives[$row["design_objective_group_name"]] = $row["design_objective_item_name"];
			$group = $row["design_objective_group_name"];
		}
		else
		{
			$designobjectives[$row["design_objective_group_name"]] = $designobjectives[$row["design_objective_group_name"]] . ", " . $row["design_objective_item_name"];
		}
		$i++;
	}
	

	$watches = $project["project_watches_displayed"];
	$watchesstored = $project["project_watches_stored"];
	$bijoux = $project["project_bijoux_displayed"];
	$bijouxstored = $project["project_bijoux_stored"];

	$postype = $project["postype_name"];
	$possubclass = $project["possubclass_name"];
	
	if($project["productline_subclass_name"])
	{
		$posfurniture = $project["product_line_name"] . " / " . $project["productline_subclass_name"];
	}
	else
	{
		$posfurniture = $project["product_line_name"];
	}


	//delivery_address
	$delivery_address = get_order_address(2, $project["project_order"]);
	$deliveryaddress = "";

	$delivery_address_province_name = "";
	if($delivery_address["place_id"])
	{
		$delivery_address_province_name = get_province_name($delivery_address["place_id"]);
	}

	

	$deliveryaddress = $delivery_address["company"];
	if($delivery_address["address"])
	{
		$deliveryaddress .= ", " . $delivery_address["address"];
	}
	if($delivery_address["zip"])
	{
		$deliveryaddress .= ", " . $delivery_address["zip"] . " " . $delivery_address["place"];
	}
	else
	{
		$deliveryaddress .= ", " . $delivery_address["place"];
	}
	
	if($delivery_address_province_name) {
		$deliveryaddress .= ", " . $delivery_address_province_name;
	}
	if($delivery_address["country_name"])
	{
		$deliveryaddress .= ", " . $delivery_address["country_name"];
	}

	$comments = $project["order_delivery_comments"];
	if($comments)
	{
		$comments .= "\r\n" . $project["project_comments"];
	}
	else
	{
		$comments = $project["project_comments"];
	}



	$prefferedarrival = to_system_date($project["order_preferred_delivery_date"]);


	if($project["order_insurance"] == 1)
	{
		$insurance = "Insurance covered by " . BRAND . "/Forwarder";
	}
	else
	{
		$insurance = "Insurance not covered by " . BRAND . "/Forwarder";
	}
	
	
	$transportation_arranged = "";
	
	$sql = "select transportation_type_name from transportation_types where transportation_type_id = " . $project["order_preferred_transportation_arranged"];
	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$transportation_arranged = $row["transportation_type_name"];
	}
	
	$transportation_mode = "";
	$sql = "select transportation_type_name from transportation_types where transportation_type_id = " . $project["order_preferred_transportation_mode"];
	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$transportation_mode = $row["transportation_type_name"];
	}

	$conditions = "";
	/*
	if($project["order_packaging_retraction"] == 1)
	{
		$conditions = "Packaging Retraction Desired";
	}
	*/

	if($project["order_pedestrian_mall_approval"] == 1 and $conditions)
	{
		$conditions .= ", Pedestrian Area Approval Needed";
	}
	else
	{
		$conditions = "Pedestrian Area Approval Needed";
	}
	if($project["order_full_delivery"] == 1 and $conditions)
	{
		$conditions .= ", Full Delivery";
	}
	else
	{
		$conditions = "Full Delivery";
	}



	

	//notify address

	$billing_address_province_name = "";
	if($project["order_billing_address_place_id"])
	{
		$billing_address_province_name = get_province_name($project["order_billing_address_place_id"]);
	}


	$notifyaddress = "";
	$notifyaddress = $project["order_billing_address_company"];
	if($project["order_billing_address_address"])
	{
		$notifyaddress .= ", " . $project["order_billing_address_address"];
	}
	if($project["order_billing_address_zip"])
	{
		$notifyaddress .= ", " . $project["order_billing_address_zip"] . " " . $project["order_billing_address_place"];
	}
	else
	{
		$notifyaddress .= ", " . $project["order_billing_address_place"];
	}
	
	
	if($billing_address_province_name) {
	
		$notifyaddress .= ", " . $billing_address_province_name;
	}
	if($project["order_billing_address_country_name"])
	{
		$notifyaddress .= ", " . $project["order_billing_address_country_name"];
	}


	$posareas = "";
	if(count($pos_data) > 0)
	{
		if($project["pipeline"] == 0)
		{
			$sql_i = "select * from posareas " . 
					 "left join posareatypes on posareatype_id = posarea_area " .
					 "where posarea_posaddress = " . $project["posaddress_id"];
			
		}
		elseif($project["pipeline"] == 1)
		{
			$sql_i = "select * from posareaspipeline " . 
					 "left join posareatypes on posareatype_id = posarea_area " .
					 "where posarea_posaddress = " . $project["posaddress_id"];
		}
		$res_i = mysql_query($sql_i) or dberror($sql_i);
		while ($row_i = mysql_fetch_assoc($res_i))
		{
			$posareas .= $row_i["posareatype_name"] . ", ";
		}
		$posareas = substr($posareas,0,strlen($posareas)-2);

		
		
		//square meters
		$sql_m =  "select * from project_costs " .
        "where project_cost_order = " . $project["project_order"];

		$res_m = mysql_query($sql_m) or dberror($sql_m);
		$row_m = mysql_fetch_assoc($res_m);
		
		$posgrosssqm = $row_m["project_cost_gross_sqms"] ? number_format($row_m["project_cost_gross_sqms"], 2) : "";
		$postotalsqm = $row_m["project_cost_totalsqms"] ? number_format($row_m["project_cost_totalsqms"], 2) : "";
		$posretailsqm = $row_m["project_cost_sqms"] ? number_format($row_m["project_cost_sqms"], 2) : "";
		$posofficesqm = $row_m["project_cost_backofficesqms"] ? number_format($row_m["project_cost_backofficesqms"], 2) : "";
		$posnumfloors = $row_m["project_cost_numfloors"] ? number_format($row_m["project_cost_numfloors"], 0) : "";
		$posfloor1sqm = $row_m["project_cost_floorsurface1"] ? number_format($row_m["project_cost_floorsurface1"], 2) : "";
		$posfloor2sqm = $row_m["project_cost_floorsurface2"] ? number_format($row_m["project_cost_floorsurface2"], 2) : "";
		$posfloor3sqm = $row_m["project_cost_floorsurface3"] ? number_format($row_m["project_cost_floorsurface3"], 2) : "";
		
		//area perception
		$posearetypes = array();
		$posearetypes["Class/Image Area"] = $pos_data["posaddress_perc_class"];
		$posearetypes["Tourist/Historical Area"] = $pos_data["posaddress_perc_tourist"];
		$posearetypes["Public Transportation"] = $pos_data["posaddress_perc_transport"];
		$posearetypes["People Traffic Area"] = $pos_data["posaddress_perc_people"];
		$posearetypes["Parking Possibilities"] = $pos_data["posaddress_perc_parking"];
		$posearetypes["Visibility from Pavement"] = $pos_data["posaddress_perc_visibility1"];
		$posearetypes["Visibility from accross the Street"] = $pos_data["posaddress_perc_visibility2"];

		//neighbourhood
		$neighbourhoods = array();
		if($project["pipeline"] == 0)
		{
			$sql = "select * from posorders where posorder_order = " . $project["order_id"];
		}
		elseif($project["pipeline"] == 1)
		{
			$sql = "select * from posorderspipeline where posorder_order = " . $project["order_id"];
		}
		
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$neighbourhoods["Shop on Left Side"] = $row["posorder_neighbour_left"];
			$neighbourhoods["Shop on Right Side"] = $row["posorder_neighbour_right"];
			$neighbourhoods["Shop Across Left Side"] = $row["posorder_neighbour_acrleft"];
			$neighbourhoods["Shop Across Right Side"] = $row["posorder_neighbour_acrright"];
			$neighbourhoods["Other Brands in Area"] = $row["posorder_neighbour_brands"];
		}		


		//franchisee agreement
		$posagreement = $pos_data["agreement_type_name"];

		if($posagreement)
		{
			$posagreement .= ": ";
		}

		if($pos_data["posaddress_fagrsent"] == 1 and $pos_data["posaddress_fagrsigned"] == 1)
		{
			$posagreement .= "Agreement sent and signed";
		}
		elseif($pos_data["posaddress_fagrsent"] == 1)
		{
			$posagreement .= "Agreement sent but not signed";
		}
		elseif($pos_data["posaddress_fagrsigned"] == 1)
		{
			$posagreement .= "Agreement signed but not sent";
		}

		//calculate franchisee agreement duration
		$duration = "";
		
		$duration = $pos_data["dmonths"];
		if($duration != "n.a.")
		{
			$duration = floor($duration/12);

			$months = 1 + $pos_data["dmonths"] - $duration*12;


			if($months == 12)
			{
				$duration = $duration + 1 . " years";
			}
			
			elseif($months > 0)
			{
				$duration = $duration . " years and " . (1 + $pos_data["dmonths"] - $duration*12) . " months";
			}
		}

		/*
		$posagreementstart = to_system_date($pos_data["posaddress_fagrstart"]);
		$posagreementend = to_system_date($pos_data["posaddress_fagrend"]);
		$posagreementduration = $posagreementstart . " - " . $posagreementend . " (Duration: " . $duration . ")";
		$posagreementcancellation = to_system_date($pos_data["posaddress_fagcancellation"]);
		
		$posaddress_fag_comment = $pos_data["posaddress_fag_comment"];
		*/

		$posagreementstart = to_system_date($project["project_fagrstart"]);
		$posagreementend = to_system_date($project["project_fagrend"]);
		$posagreementduration = $posagreementstart . " - " . $posagreementend . " (Duration: " . $duration . ")";
		$posagreementcancellation = to_system_date($pos_data["posaddress_fagcancellation"]);
		
		$posaddress_fag_comment = $project["project_fag_comment"];
		

		$postakeoverdate = to_system_date($pos_data["posaddress_takeover_date"]);
		$postakeoveramount = $pos_data["posaddress_takeover_amount"] ? number_format($pos_data["posaddress_takeover_amount"], 2) : "";
		if($postakeoveramount)
		{
			$postakeoveramount .= " " . $pos_data["currency_symbol"];
		}
		$postakeoverdetail = "";
		if($postakeoverdate)
		{
			$postakeoverdetail = $postakeoverdate;
		}
		if($pos_data["posaddress_takeover_amount"] > 0)
		{
			$postakeoverdetail = $postakeoverdate . " for " . $postakeoveramount;
		}
	}
	else
	{
		$posgrosssqm = "";
		$postotalsqm = "";
		$posretailsqm = $project["project_cost_sqms"] ? number_format($project["project_cost_sqms"], 2) : "";
		$posofficesqm = "";
		$posnumfloors = "";
		$posfloor1sqm = "";
		$posfloor2sqm = "";
		$posfloor3sqm = "";
		
		//area perception
		$posearetypes = array();

		//neighbourhood
		$neighbourhoods = array();
		
		//franchisee agreement
		$posagreement = $project["agreement_type_name"];

		if($posagreement)
		{
			$posagreement .= ": ";
		}

		if($project["project_fagrsent"] == 1 and $project["project_fagrsigned"] == 1)
		{
			$posagreement .= "Agreement sent and signed";
		}
		elseif($project["project_fagrsent"] == 1)
		{
			$posagreement .= "Agreement sent but not signed";
		}
		elseif($project["project_fagrsigned"] == 1)
		{
			$posagreement .= "Agreement signed but not sent";
		}

		//calculate franchisee agreement duration
		$duration = "";
		
		$duration = $project["dmonths"];
		if($duration != "n.a.")
		{
			$duration = floor($duration/12);

			$months = 1 + $project["dmonths"] - $duration*12;


			if($months == 12)
			{
				$duration = $duration + 1 . " years";
			}
			
			elseif($months > 0)
			{
				$duration = $duration . " years and " . (1 + $project["dmonths"] - $duration*12) . " months";
			}
		}

		$posagreementstart = to_system_date($project["project_fagrstart"]);
		$posagreementend = to_system_date($project["project_fagrend"]);
		$posagreementduration = $posagreementstart . " - " . $posagreementend . " (Duration: " . $duration . ")";
		$posagreementcancellation = "";

		$posaddress_fag_comment = $project["project_fag_comment"];

	
	}


	//Client Data
	$posclient = "";
	$sql_i = "select * from addresses " . 
		     "left join countries on country_id = address_country " .
	         "where address_id = " . $project["order_client_address"];

	$res_i = mysql_query($sql_i) or dberror($sql_i);
	if ($row_i = mysql_fetch_assoc($res_i))
	{
		$posclient = $row_i["address_company"];
		if($row_i["address_address"])
		{
			$posclient .= ", " . $row_i["address_address"];
		}
		if($row_i["address_zip"])
		{
			$posclient .= ", " . $row_i["address_zip"] . " " . $row_i["address_place"];
		}
		else
		{
			$posclient .= ", " . $row_i["address_place"];
		}
		if($row_i["country_name"])
		{
			$posclient .= ", " . $row_i["country_name"];
		}
	}


	//Franchisor Data
	$posfranchisor = "";
	
	if(count($pos_data) > 0)
	{
		$sql_i = "select * from addresses " . 
				 "left join countries on country_id = address_country " .
				 "where address_id = " . $pos_data["posaddress_franchisor_id"];
	}
	else
	{
		$sql_i = "select * from addresses " . 
				 "left join countries on country_id = address_country " .
				 "where address_id = 13";
	}

	$res_i = mysql_query($sql_i) or dberror($sql_i);
	if ($row_i = mysql_fetch_assoc($res_i))
	{
		$posfranchisor = $row_i["address_company"];
		if($row_i["address_address"])
		{
			$posfranchisor .= ", " . $row_i["address_address"];
		}
		if($row_i["address_address2"])
		{
			$posfranchisor .= ", " . $row_i["address_address2"];
		}
		if($row_i["address_zip"])
		{
			$posfranchisor .= ", " . $row_i["address_zip"] . " " . $row_i["address_place"];
		}
		else
		{
			$posfranchisor .= ", " . $row_i["address_place"];
		}
		if($row_i["country_name"])
		{
			$posfranchisor .= ", " . $row_i["country_name"];
		}
	}

	//Franchisee Data
	$posfranchisee = "";

	if(count($pos_data) > 0)
	{
		$sql_i = "select * from addresses " . 
				 "left join countries on country_id = address_country " .
				 "where address_id = " . dbquote($pos_data["posaddress_franchisee_id"]);
	}
	else
	{
		$sql_i = "select * from addresses " . 
				 "left join countries on country_id = address_country " .
				 "where address_id = " . dbquote($project["order_franchisee_address_id"]);
	}

	$res_i = mysql_query($sql_i) or dberror($sql_i);
	if ($row_i = mysql_fetch_assoc($res_i))
	{
		$posfranchisee = $row_i["address_company"];
		if($row_i["address_address"])
		{
			$posfranchisee .= ", " . $row_i["address_address"];
		}
		if($row_i["address_address2"])
		{
			$posfranchisee .= ", " . $row_i["address_address2"];
		}
		if($row_i["address_zip"])
		{
			$posfranchisee .= ", " . $row_i["address_zip"] . " " . $row_i["address_place"];
		}
		else
		{
			$posfranchisee .= ", " . $row_i["address_place"];
		}
		if($row_i["country_name"])
		{
			$posfranchisee .= ", " . $row_i["country_name"];
		}
	}

	//Former Franchisee Data
	$posformerfranchisee = "";
	if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 3)
	{
			
			//Franchisee Data
			$sql_i = "select address_company, address_address, address_address2, address_zip, address_place, country_name " . 
					 "from posorders " .
				     "left join orders on order_id = posorder_order " .
					 "left join addresses on address_id = order_franchisee_address_id " .					       
				     "left join countries on country_id = address_country " .
					 "where posorder_type = 1 and (posorder_project_kind = 1 or posorder_project_kind = 2) " . 
					 " and posorder_posaddress = " . $project["posaddress_id"] . 
					 " and order_actual_order_state_code = '820' " . 
					 " order by posorder_id DESC";	


			$res_i = mysql_query($sql_i) or dberror($sql_i);
			if ($row_i = mysql_fetch_assoc($res_i))
			{
				$posformerfranchisee = $row_i["address_company"];
				if($row_i["address_address"])
				{
					$posformerfranchisee .= ", " . $row_i["address_address"];
				}
				if($row_i["address_address2"])
				{
					$posformerfranchisee .= ", " . $row_i["address_address2"];
				}
				if($row_i["address_zip"])
				{
					$posformerfranchisee .= ", " . $row_i["address_zip"] . " " . $row_i["address_place"];
				}
				else
				{
					$posformerfranchisee .= ", " . $row_i["address_place"];
				}
				if($row_i["country_name"])
				{
					$posformerfranchisee .= ", " . $row_i["country_name"];
				}
			}
	}


	

	/********************************************************************
		prepare pdf
	*********************************************************************/

	require_once('../include/tcpdf/config/lang/eng.php');
	require_once('../include/tcpdf/tcpdf.php');


	class MYPDF extends TCPDF
	{
		//Page header
		function Header()
		{
			global $page_title;
			//Logo
			$this->Image('../pictures/omega_logo.jpg',10,8,33);
			//arialn bold 15
			$this->SetFont('arialn','B',12);
			//Move to the right
			$this->Cell(80);
			//Title
			$this->Cell(0,33,$page_title,0,0,'R');
			//Line break
			$this->Ln(20);

		}

		//Page footer
		function Footer()
		{
			//Position at 1.5 cm from bottom
			$this->SetY(-15);
			//arialn italic 8
			$this->SetFont('arialn','I',8);
			//Page number
			$this->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$this->PageNo().'/'.$this->getAliasNbPages(),0,0,'R');
		}

	}

	//Instanciation of inherited class
	$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);
	$pdf->SetMargins(10, 23, 10);

	$pdf->Open();

	$pdf->SetFillColor(220, 220, 220); 
	$pdf->AddFont('arialn');
	$pdf->AddFont('arialn', 'B');

	$pdf->AddPage();
	$new_page = 0;

	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190,7,$posname,1, 0, 'L', 1);
	$pdf->Ln();
	$pdf->SetFont('arialn','',10);
	$pdf->Cell(190,7,$posaddress,1, 0, 'L', 0);
	$pdf->Ln();
	$pdf->Ln();

	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Phone",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$posphone,1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Fax",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$posfax,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Email",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$posemail,1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Website",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$poswebsite,1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Environment",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(163, 5,$posareas,1, 0, 'L', 0);
	$pdf->Ln();
	$pdf->Ln();

	
	if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
	{
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(68, 5,"",1, 0, 'L', 0);
	}
	else
	{
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"Planned Opening",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(68, 5,$plannedopeningdate,1, 0, 'L', 0);
	}
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,$project["projectkind_milestone_shortname_01"],1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$realisticopeningdate,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,$project["projectkind_milestone_shortname_02"],1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$posopeningdate,1, 0, 'L', 0);
	
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Closing Date",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$posclosingdate,1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Legal Type",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$poslegaltype,1, 0, 'L', 0);
	
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Project Kind",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$projectkind,1, 0, 'L', 0);
	$pdf->Ln();
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"POS Type",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$postype,1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Furniture",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$posfurniture,1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"POS Type Subcl.",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$possubclass,1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Aprox. Budget",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$projectbudget,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->Ln();

	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190, 5,"Surfaces in Square Meters",0, 0, 'L', 0);

	$pdf->Ln();
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Gross",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$posgrosssqm,1, 0, 'L', 0);


	$pdf->Ln();
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Total",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$postotalsqm,1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Retail",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$posretailsqm,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Office",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$posofficesqm,1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"1st Floor",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$posfloor1sqm,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"2nd Floor",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$posfloor2sqm,1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"3rd Floor",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$posfloor3sqm,1, 0, 'L', 0);
	$pdf->Ln();
	$pdf->Ln();

	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190, 5,"Ownership",0, 0, 'L', 0);
	$pdf->Ln();
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Client",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(163, 5,$posclient,1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Franchisor",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(163, 5,$posfranchisor,1, 0, 'L', 0);
	$pdf->Ln();


	if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 3)
	{
		if($posformerfranchisee)
		{
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(27, 5,"Former Franchisee",1, 0, 'L', 0);
			$pdf->SetFont('arialn','',8);
			$pdf->Cell(163, 5,$posformerfranchisee,1, 0, 'L', 0);
			$pdf->Ln();
		}
	}

	$pdf->SetFont('arialn','B',8);
	if($project["project_cost_type"] != 6)
	{
		$pdf->Cell(27, 5,"Franchisee",1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(27, 5,"Owner Company",1, 0, 'L', 0);
	}
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(163, 5,$posfranchisee,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Agreement",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(163, 5,$posagreement,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Duration",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(163, 5,$posagreementduration,1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Comment",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->MultiCell(163, 5,$posaddress_fag_comment,1,'L', 0);
	$pdf->Ln();

	if($pdf->GetY() > 240)
	{
		$pdf->AddPage();
	}
	
	if($project["project_projectkind"] != 4 and $project["project_projectkind"] != 5) // take over, lease renewal
	{
		$pdf->SetFont('arialn','B',10);
		$pdf->Cell(190, 5,"POS Information",0, 0, 'L', 0);
		$pdf->Ln();
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"Voltage Choice",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(68, 5,$voltagechoice,1, 0, 'L', 0);
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"Location Info",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(68, 5,$locationtype,1, 0, 'L', 0);
		$pdf->Ln();
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"Furniture Height",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
			
		if($furniture_height)
		{
			$furniture_height = $furniture_height . " mm";
		}
		$pdf->Cell(163, 5,$furniture_height,1, 0, 'L', 0);
		$pdf->Ln();
		
		
		foreach($designobjectives as $group=>$objectives)
		{
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(27, 5,$group,1, 0, 'L', 0);
			$pdf->SetFont('arialn','',8);
			$pdf->Cell(163, 5,$objectives,1, 0, 'L', 0);
			$pdf->Ln();
		}

		$pdf->Ln();
		
		if($pdf->GetY() > 240)
		{
			$pdf->AddPage();
		}

		$pdf->SetFont('arialn','B',10);
		$pdf->Cell(190, 5,"Capacity Request by Client",0, 0, 'L', 0);
		$pdf->Ln();
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"Watches displayed",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(68, 5,$watches,1, 0, 'L', 0);
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"Watches Stored",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(68, 5,$watchesstored,1, 0, 'L', 0);
		$pdf->Ln();

		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"Bijoux displayed",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(68, 5,$bijoux,1, 0, 'L', 0);
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"Bijoux Stored",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(68, 5,$bijouxstored,1, 0, 'L', 0);
		$pdf->Ln();
	}

	if($pdf->GetY() > 240)
	{
		$pdf->AddPage();
	}

	if(count($posearetypes) > 0)
	{
		$pdf->Ln();
		$pdf->SetFont('arialn','B',10);
		$pdf->Cell(190, 5,"Area Perception",0, 0, 'L', 0);
		$pdf->Ln();

		foreach($posearetypes as $name=>$value)
		{
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(50, 5,$name,1, 0, 'L', 0);
			$pdf->SetFont('arialn','',8);

			$pdf->Cell(140, 5,"",1, 0, 'L', 0);
			$pdf->SetX(62);
			$x = $pdf->GetX();
			$y = $pdf->GetY();
			
			$k = 6 - $value;

			for($i=1;$i<=$k;$i++)
			{
				$pdf->Image('../pictures/rating_star.jpg',$x, $y+1, 3);
				$x = $x+4;
				$pdf->SetX($x);
			}
			
			$pdf->Ln();
		}

		
		if($pdf->GetY() > 240)
		{
			$pdf->AddPage();
			$new_page = 1;
		}
	}
	

	if(count($neighbourhoods) > 0)
	{
		if($new_page == 0)
		{
			$pdf->Ln();
		}
		
		$pdf->SetFont('arialn','B',10);
		$pdf->Cell(190, 5,"Neighbourhood",0, 0, 'L', 0);
		$pdf->Ln();

		foreach($neighbourhoods as $name=>$value)
		{
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(50, 5,$name,1, 0, 'L', 0);
			$pdf->SetFont('arialn','',8);

			$pdf->Cell(140, 5,$value,1, 0, 'L', 0);
			$pdf->Ln();
		}

		
		if($new_page == 0)
		{
			if($pdf->GetY() > 240)
			{
				$pdf->AddPage();
				$new_page = 1;
			}
		}
	
	}

	if($project["project_projectkind"] != 4 and $project["project_projectkind"] != 5) // take over, lease renewal
	{
		$pdf->Ln();
		$pdf->SetFont('arialn','B',10);
		$pdf->Cell(190, 5,"Addresses",0, 0, 'L', 0);
		$pdf->Ln();
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"Delivery",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(163, 5,$deliveryaddress,1, 0, 'L', 0);
		$pdf->Ln();
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"Notify",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(163, 5,$notifyaddress,1, 0, 'L', 0);
		$pdf->Ln();
		
		$pdf->Ln();
		$pdf->SetFont('arialn','B',10);
		$pdf->Cell(190, 5,"Delivery",0, 0, 'L', 0);
		$pdf->Ln();
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"Preferred Arrival",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(18, 5,$prefferedarrival,1, 0, 'L', 0);
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"Transportation",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(48, 5,$transportation_arranged . " " . $transportation_mode,1, 0, 'L', 0);
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"Insurance",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(43, 5,$insurance,1, 0, 'L', 0);

		
		$pdf->Ln();

		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"Remarks",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(163, 5,$conditions,1, 0, 'L', 0);
		$pdf->Ln();

		if($comments)
		{
			$pdf->MultiCell(190, 5, $comments, 1);
		}

		$pdf->Ln();
		$pdf->Ln();
	}


	//Google Map
	if($pdf->getY() > 170)
	{
		$pdf->AddPage();
	}

	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190, 5, "Maps" ,0, 0, 'L', 0);
	$pdf->Ln();
	$pdf->Ln();

	if(isset($pos_data["posaddress_google_lat"]) and isset($pos_data["posaddress_google_long"]))
	{
		$url = STATIC_MAPS_HOST;
		$url .= '?center=' . $pos_data["posaddress_google_lat"] . ',' . $pos_data["posaddress_google_long"];
		$url .= '&zoom=16';
		$url .= '&size=640x640';
		$url .= '&maptype=roadmap' . "\\";
		$url .= '&markers=color:red%7Clabel:P%7C' . $pos_data["posaddress_google_lat"] . ',' . $pos_data["posaddress_google_long"];
		$url .= '&format=jpg';
		$url .= '&key=' . GOOGLE_API_KEY;
		$url .= '&sensor=false';

				
		$tmpfilename1 = "map1" . time() . ".jpg";


		if(!$context) {
			
			$mapImage = @file_get_contents($url);
		}
		else
		{
			$mapImage = @file_get_contents($url, false, $context);
		}

		if($mapImage) {
			$fh = fopen(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1, 'w') or die("can't open file");
			fwrite($fh, $mapImage);
			fclose($fh);
		
			if(file_exists(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1)) {	
				$pdf->Image(TMP_FILE_DIR_RELATIVE . $tmpfilename1, 10, $pdf->getY(), 90, 90);
			}
		}

		$url = STATIC_MAPS_HOST;
		$url .= '?center=' . $pos_data["posaddress_google_lat"] . ',' . $pos_data["posaddress_google_long"];
		$url .= '&zoom=14';
		$url .= '&size=640x640';
		$url .= '&maptype=roadmap' . "\\";
		$url .= '&markers=color:red%7Clabel:P%7C' . $pos_data["posaddress_google_lat"] . ',' . $pos_data["posaddress_google_long"];
		$url .= '&format=jpg';
		$url .= '&key=' . GOOGLE_API_KEY;
		$url .= '&sensor=false';

		
		$tmpfilename2 = "map2" . time() . ".jpg";


		//$mapImage2 = imagecreatefromjpeg($url);

		if(!$context) {
			
			$mapImage2 = @file_get_contents($url);
		}
		else
		{
			$mapImage2 = @file_get_contents($url, false, $context);
		}

		if($mapImage2) {
			$fh = fopen(TMP_FILE_DIR_ABSOLUTE . $tmpfilename2, 'w') or die("can't open file");
			fwrite($fh, $mapImage2);
			fclose($fh);
			
			if(file_exists(TMP_FILE_DIR_ABSOLUTE . $tmpfilename2)) {
				$pdf->Image(TMP_FILE_DIR_RELATIVE . $tmpfilename2, 110, $pdf->getY(), 90, 90);
			}
		}
	}

	// write pdf
	$pdf->Output();

	if(file_exists(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1))
	{
		unlink(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1);
	}

	if(file_exists(TMP_FILE_DIR_ABSOLUTE . $tmpfilename2))
	{
		unlink(TMP_FILE_DIR_ABSOLUTE . $tmpfilename2);
	}

	

	// write pdf
	$file_name = "Project_overview_" . $project["order_number"];
	$pdf->Output($file_name);

}

?>