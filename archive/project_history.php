<?php
/********************************************************************

    project_history.php

    View the history of a project in detail

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-11-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-02-16
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_view_history_in_projects");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);


// get data for history list
$sql_order_states = "select order_state_code, order_state_name, ".
                    "actual_order_states.date_modified as date_modified, ".
                    "concat(user_name, ' ', user_firstname) as user_fullname ".
                    "from actual_order_states ".
                    "left join order_states on actual_order_state_state = order_state_id ".
                    "left join users on actual_order_state_user = user_id";

$list1_filter = "actual_order_state_order = " . $project["project_order"];

// get dates from dates
$sql_dates = "select order_item_id, order_item_text, ".
             "concat(if(item_code <>'', item_code, item_type_name), ' ', order_item_text) as group_head,  ".
             "adr_sup.address_company as supplier, adr_frw.address_company as forwarder, ".
             "date_type_name, dates.date_modified as date_modified ".
             "from orders ".
             "left join order_items on order_item_order = order_id ".
             "left join items on order_item_item = item_id ".
             "left join addresses as adr_sup on order_item_supplier_address = adr_sup.address_id ".
             "left join addresses as adr_frw on order_item_forwarder_address = adr_frw.address_id ".
             "left join dates on date_order_item = order_item_id ".
             "left join date_types on date_type = date_type_id " .
             "left join item_types on order_item_type = item_type_id";

$list2_filter = "(order_item_not_in_budget is null or order_item_not_in_budget = 0) " .
                "   and order_id = " . $project["project_order"] . 
                "   and order_item_type <= " . ITEM_TYPE_SPECIAL;

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "history");

$form->add_section("Project");
$form->add_hidden("pid", param("pid"));

require_once "include/project_head_small.php";


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Create History of steps
*********************************************************************/ 
$list1 = new ListView($sql_order_states, LIST_HAS_HEADER);

$list1->set_title("History: Project Steps");
$list1->set_entity("order_state");
$list1->set_filter($list1_filter);
$list1->set_order("actual_order_states.date_modified");

$list1->add_column("date_modified", "Modification Date");
$list1->add_column("order_state_code", "Step");
$list1->add_column("order_state_name", "Action", "", "", "", COLUMN_NO_WRAP);
$list1->add_column("user_fullname", "Performed by", "", "", "", COLUMN_NO_WRAP);



/********************************************************************
    Create History of dates
*********************************************************************/ 
$list2 = new ListView($sql_dates, LIST_HAS_HEADER);

$list2->set_title("History: Dates");
$list2->set_entity("order_dates");
$list2->set_filter($list2_filter);
$list2->set_order("dates.date_modified, date_type_name, adr_sup.address_company, adr_frw.address_company");
$list2->set_group("group_head");


$list2->add_column("date_modified", "Date");
$list2->add_column("date_type_name", "Date Type");
$list2->add_column("supplier", "Supplier");
$list2->add_column("forwarder", "Forwarder");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$list1->populate();
$list1->process();

$list2->populate();
$list2->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("History");
$form->render();

$list1->render();
$list2->render();

$page->footer();

?>