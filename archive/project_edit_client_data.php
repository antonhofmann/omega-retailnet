<?php
/********************************************************************

    project_edit_client_data.php

    Edit project's  data as entered by the client.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-07
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-19
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "../shared/func_posindex.php";

check_access("can_edit_client_data_in_projects");


/********************************************************************
    prepare all data needed
*********************************************************************/


// Vars
$error = "unspecified";
$design_objectives_listbox_names = array();
$design_objectives_checklist_names = array();

// read project and order details
$project = get_project(param("pid"));


//check if the cost monitoring sheet exists already
$sql = "select count(project_cost_id) as num_recs " .
       "from project_costs " .
       "where project_cost_order = "  . $project["project_order"]; 
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

if($row["num_recs"] == 0)
{
    $fields = array();
    $values = array();

    $fields[] = "project_cost_order";
    $values[] = $project["project_order"];
    
    $fields[] = "date_created";
    $values[] = dbquote(date("Y-m-d"));

    $fields[] = "date_modified";
    $values[] = dbquote(date("Y-m-d"));

    $fields[] = "user_created";
    $values[] = dbquote(user_login());

    $fields[] = "user_modified";
    $values[] = dbquote(user_login());

    $sql = "insert into project_costs (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

    mysql_query($sql) or dberror($sql);
}

//update order items with predefined cost_groups
$sql = "select order_item_id, order_item_item, item_cost_group, item_costmonitoring_group " . 
       "from order_items " . 
	   "left join items on item_id = order_item_item " .
	   "where order_item_order = " . $project["project_order"] . 
	   "  and order_item_not_in_budget <> 1 " .
	   "  and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION . " or order_item_type >= " . ITEM_TYPE_LOCALCONSTRUCTIONCOST . ") ";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$sql_u = "update order_items Set order_item_cost_group =  " . dbquote($row["item_cost_group"]) . " " . 
		     " where (order_item_cost_group is NULL or order_item_cost_group = 0) " .
		     "  and order_item_id = " . $row["order_item_id"];

	mysql_query($sql_u) or dberror($sql_u);

	$sql_u = "update order_items Set order_items_costmonitoring_group =  " . dbquote($row["item_costmonitoring_group"]) . " " . 
		     " where (order_items_costmonitoring_group is NULL or order_items_costmonitoring_group = 0) " .
		     "  and order_item_id = " . $row["order_item_id"];

	mysql_query($sql_u) or dberror($sql_u);
}


// get users' company address
$address = get_address($project["order_client_address"]);

// get client user data
$user = get_user($project["order_user"]);

//project legal types
if($address["client_type"] == 1) // client is agent, only franchisee as option
{
    $sql_project_cost_types = "select * from project_costtypes where project_costtype_id IN (2,6)";
}
else
{
    $sql_project_cost_types = "select * from project_costtypes where project_costtype_id IN (1,2,6)";    
}


$order_state_name = get_actual_order_state_name($project["order_actual_order_state_code"], 1);

$project_state_name = get_project_state_name($project["project_state"]);

// get orders's currency
$currency = get_order_currency($project["project_order"]);

// read design_item_ids from project items
$project_design_objective_item_ids = get_project_design_objective_item_ids(param("pid"));

$delivery_address = get_order_address(2, $project["project_order"]);


// create sql for the client listbox
$sql_address = "select address_id, address_company ".
               "from addresses ".
               "where address_type = 1 or address_type = 4 ".
               "order by address_company";


// create array for the franchisee address listbox
//$franchisee_addresses = get_franchisee_addresses($project["order_client_address"]);

// create array for the billing address listbox
//$billing_addresses = get_billing_addresses($project["order_client_address"]);

// create array for the delivery address listbox
//$delivery_addresses = get_delivery_addresses($project["order_client_address"]);

// create sql for the client's contact listbox
if (!param("client_address_id"))
{
    $sql_address_user = "select user_id, concat(user_name, ' ', user_firstname) ".
                        "from users ".
                        "where user_id = " . $project["order_user"] . " or (user_active = 1 and user_address = ". $project["order_client_address"] . ") ".
                        "order by user_name";
}
else
{
    $sql_address_user = "select user_id, concat(user_name, ' ', user_firstname) ".
                        "from users ".
                        "where user_id = " . $project["order_user"] . " or (user_active = 1 and user_address = ". param("client_address_id") . ") ".
                        "order by user_name";
}


// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ".
                 "order by country_name";

// create sql for the location type listbox
$sql_location_types = "select location_type_id, location_type_name ".
                      "from location_types ".
                      "order by location_type_name";

// create sql for the voltage listbox
$sql_voltages = "select voltage_id, voltage_name ".
                "from voltages";

// create sql for the transportation type listbox
$sql_transportation_modes = "select transportation_type_id, transportation_type_name ".
                            "from transportation_types ".
                            "where transportation_type_visible = 1 " .
							" and transportation_type_code = 'mode' " . 
                            "order by transportation_type_name";


$sql_transportation_arranged = "select transportation_type_id, transportation_type_name ".
								"from transportation_types ".
								"where transportation_type_visible = 1 " .
								" and transportation_type_code = 'arranged' " . 
								"order by transportation_type_name";


// create sql for the project type listbox
/*
$sql_pos_types = "select postype_id, postype_name ".
                     "from product_line_pos_types ".
                     "left join postypes on postype_id = product_line_pos_type_pos_type ".
                     "where product_line_pos_type_product_line = " . $project["project_product_line"] . " " .
                     "order by postype_name";
*/

$sql_pos_types = "select postype_id, postype_name ".
                     "from postypes  " .
                     "order by postype_name";


//get addresses from pos index
$sql_posaddresses = "select posaddress_id, concat(posaddress_name, ', ', posaddress_place) as posaddress " .
                    "from posaddresses " . 
					"where posaddress_country = " . $project["order_shop_address_country"] . 
					" order by posaddress";

//pos subclasses
if(param("project_postype"))
{
	$sql_pos_subclasses = "select possubclass_id, possubclass_name " .
		              "from postype_subclasses " .
		              "left join possubclasses on possubclass_id = postype_subclass_subclass " .
		              "left join product_line_pos_types on  product_line_pos_type_id = postype_subclass_pl_pos_type " . 
		              "where  product_line_pos_type_pos_type = " . param("project_postype") .
		              " and product_line_pos_type_product_line is null " .
	                  " order by possubclass_name";
}
elseif($project["project_postype"])
{
	$sql_pos_subclasses = "select possubclass_id, possubclass_name " .
		              "from postype_subclasses " .
		              "left join possubclasses on possubclass_id = postype_subclass_subclass " .
		              "left join product_line_pos_types on  product_line_pos_type_id = postype_subclass_pl_pos_type " . 
		              "where  product_line_pos_type_pos_type = " . $project["project_postype"] .
		              " and product_line_pos_type_product_line is null " .
	                  " order by possubclass_name";
}
else
{
	$sql_pos_subclasses = "select possubclass_id, possubclass_name " .
		              "from postype_subclasses " .
		              "left join possubclasses on possubclass_id = postype_subclass_subclass " .
		              "left join product_line_pos_types on  product_line_pos_type_id = postype_subclass_pl_pos_type " . 
		              "where  product_line_pos_type_pos_type = 0 " .
		              " and product_line_pos_type_product_line =  0" .
	                  " order by possubclass_name";
}


//project legal types
if(param("project_cost_type"))
{
	if(param("project_cost_type") == 1) // Corporate
	{
		$sql_project_kinds = "select projectkind_id, projectkind_name " . 
			                 "from projectkinds " . 
			                 "where projectkind_id > 0 ";
	}
	else
	{
		$sql_project_kinds = "select projectkind_id, projectkind_name " . 
			                 "from projectkinds " . 
			                 "where projectkind_id > 0 and projectkind_id < 3";
	}
}
else
{
	if($project["project_cost_type"] == 1) // Corporate
	{
		$sql_project_kinds = "select projectkind_id, projectkind_name " . 
			                 "from projectkinds " . 
			                 "where projectkind_id > 0 ";
	}
	else
	{
		$sql_project_kinds = "select projectkind_id, projectkind_name " . 
			                 "from projectkinds " . 
			                 "where projectkind_id > 0 and projectkind_id < 3";
	}
}

// create sql for the furniture height listbox
$sql_ceiling_heights = "select ceiling_height_id, ceiling_height_height ".
                "from ceiling_heights";

//get pos data
if($project["pipeline"] == 0)
{
	$pos_data = get_poslocation($project["posaddress_id"], "posaddresses");
}
elseif($project["pipeline"] == 1)
{
	$pos_data = get_poslocation_from_pipeline($project["posaddress_id"], $project["order_id"]);
}


//get province
$billing_address_province_name = "";
if($project["order_billing_address_place_id"])
{
	$billing_address_province_name = get_province_name($project["order_billing_address_place_id"]);
}

$delivery_address_province_name = "";
if($delivery_address["place_id"])
{
	$delivery_address_province_name = get_province_name($delivery_address["place_id"]);
}

/********************************************************************
    Get Cost Monitoring Sheet Budget Data
*********************************************************************/ 
$sql =  "select * from project_costs " .
        "where project_cost_order = " . $project["project_order"];

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

$project_cost_sqms = $row["project_cost_sqms"];

/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

$form->add_section("Project");
$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid", $project["project_order"]);



if (has_access("can_edit_product_line"))
{
    
    $form->add_label("project_number", "Project Number*", 0, $project["order_number"]);
    
	$form->add_lookup("product_line", "Product Line", "product_lines", "product_line_name", NOTNULL, $project["project_product_line"]);
	$form->add_lookup("product_line_subclass", "Product Line Subclass", "productline_subclasses", "productline_subclass_name", 0, $project["project_product_line_subclass"]);
	//$form->add_list("product_line", "Product Line*", $sql_product_line, SUBMIT | NOTNULL, $project["project_product_line"]);
    
	//$form->add_lookup("project_postype", "POS Type", "postypes", "postype_name", NOTNULL, $project["project_postype"]);
	$form->add_list("project_postype", "POS Type*", $sql_pos_types, NOTNULL | SUBMIT, $project["project_postype"]);
	
	$form->add_list("project_pos_subclass", "POS Type Subclass", $sql_pos_subclasses, 0, $project["project_pos_subclass"]);
	$form->add_list("project_cost_type", "Project Legal Type", $sql_project_cost_types, 0, $project["project_cost_type"]);
	
	
	//$form->add_label("project_projectkind_label", "Project Kind", 0, $project["projectkind_name"]);
	//$form->add_hidden("project_projectkind", $project["project_projectkind"]);
	$form->add_list("project_projectkind", "Project Kind*", $sql_project_kinds, SUBMIT | NOTNULL, $project["project_projectkind"]);
}
else
{
	  

	$form->add_label("project_number", "Project Number*", 0, $project["order_number"]);
	$form->add_lookup("product_line", "Product Line", "product_lines", "product_line_name", NOTNULL, $project["project_product_line"]);
	$form->add_lookup("product_line_subclass", "Product Line Subclass", "productline_subclasses", "productline_subclass_name", 0, $project["project_product_line_subclass"]);
	$form->add_lookup("project_postype", "POS Type", "postypes", "postype_name", NOTNULL, $project["project_postype"]);
	$form->add_lookup("project_pos_subclass", "POS Type Subclass", "possubclasses", "possubclass_name", NOTNULL, $project["project_pos_subclass"]);

	$form->add_label("type3", "Project Legal Type", 0, $project["project_costtype_text"]);
	$form->add_label("type2", "Project Kind", 0, $project["projectkind_name"]);
	$form->add_hidden("project_cost_type");
	$form->add_hidden("project_projectkind");

}

if (has_access("can_edit_status_in_projects"))
{
    $sql = "select distinct order_state_code " .
           "from order_states " .
           "order by order_state_code";
    $form->add_list("status", "Project State", $sql, NOTNULL, $project["order_actual_order_state_code"]);

	/*
	$sql = "select project_state_id, project_state_text from " .
           " project_states";
    $form->add_list("project_state", "Treatment State", $sql, NOTNULL, $project["project_state"]);
    */

	$form->add_hidden("project_state", $project["project_state"]);
	$form->add_label("status2", "Treatment State", 0, $project_state_name);
}
else
{
    $form->add_hidden("project_state", $project["project_state"]);

    $form->add_hidden("status", $project["order_actual_order_state_code"]);

    $form->add_label("status1", "Project State", 0, $project["order_actual_order_state_code"]  . " " . $order_state_name);

    $form->add_label("status2", "Treatment State", 0, $project_state_name);
}

$form->add_section("Client");
$form->add_list("client_address_id", "Client*", $sql_address, SUBMIT | NOTNULL, $project["order_client_address"]);
$form->add_list("client_address_user_id", "Contact*", $sql_address_user, NOTNULL, $project["order_user"]);

if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) // take over, lease renewal
{

	$form->add_hidden("billing_address_company", $project["order_billing_address_company"]);
	$form->add_hidden("billing_address_company2", $project["order_billing_address_company2"]);
	$form->add_hidden("billing_address_address", $project["order_billing_address_address"]);
	$form->add_hidden("billing_address_address2",$project["order_billing_address_address2"]);
	$form->add_hidden("billing_address_zip", $project["order_billing_address_zip"]);
	$form->add_hidden("billing_address_place", $project["order_billing_address_place"]);
	$form->add_hidden("billing_address_country", $project["order_billing_address_country"]);
	$form->add_hidden("billing_address_phone", $project["order_billing_address_phone"]);
	$form->add_hidden("billing_address_fax", $project["order_billing_address_fax"]);
	$form->add_hidden("billing_address_email", $project["order_billing_address_email"]);
	$form->add_hidden("billing_address_contact", $project["order_billing_address_contact"]);


	$form->add_hidden("delivery_address_company", $delivery_address["company"]);
	$form->add_hidden("delivery_address_company2", $delivery_address["company2"]);
	$form->add_hidden("delivery_address_address", $delivery_address["address"]);
	$form->add_hidden("delivery_address_address2", $delivery_address["address2"]);
	$form->add_hidden("delivery_address_zip", $delivery_address["zip"]);
	$form->add_hidden("delivery_address_place", $delivery_address["place"]);
	$form->add_hidden("delivery_address_country", $delivery_address["country"]);
	$form->add_hidden("delivery_address_phone", $delivery_address["phone"]);
	$form->add_hidden("delivery_address_fax", $delivery_address["fax"]);
	$form->add_hidden("delivery_address_email", $delivery_address["email"]);
	$form->add_hidden("delivery_address_contact", $delivery_address["contact"]);
}
else
{
	$form->add_section("Notify Address");
	$form->add_label("billing_address_company", "Company*", NOTNULL, $project["order_billing_address_company"], TYPE_CHAR);
	$form->add_label("billing_address_company2", "", 0, $project["order_billing_address_company2"], TYPE_CHAR);
	$form->add_label("billing_address_address", "Address*", NOTNULL, $project["order_billing_address_address"], TYPE_CHAR);
	$form->add_label("billing_address_address2", "", 0, $project["order_billing_address_address2"], TYPE_CHAR);
	$form->add_label("billing_address_zip", "ZIP*", NOTNULL, $project["order_billing_address_zip"], TYPE_CHAR, 20);
	$form->add_label("billing_address_place", "City*", NOTNULL, $project["order_billing_address_place"], TYPE_CHAR, 20);
	$form->add_label("billing_address_province_name", "Province", 0, $billing_address_province_name);
	$form->add_lookup("billing_address_country", "Country", "countries", "country_name", 0, $project["order_billing_address_country"]);
	$form->add_label("billing_address_phone", "Phone*", NOTNULL, $project["order_billing_address_phone"], TYPE_CHAR, 20);
	$form->add_label("billing_address_fax", "Fax", 0, $project["order_billing_address_fax"], TYPE_CHAR, 20);
	$form->add_label("billing_address_email", "Email", 0, $project["order_billing_address_email"], TYPE_CHAR);
	$form->add_label("billing_address_contact", "Contact*", NOTNULL, $project["order_billing_address_contact"], TYPE_CHAR);


	$form->add_section("Delivery Address (consignee address)");
	$form->add_label("delivery_address_company", "Company*", NOTNULL, $delivery_address["company"], TYPE_CHAR);
	$form->add_label("delivery_address_company2", "", 0, $delivery_address["company2"], TYPE_CHAR);
	$form->add_label("delivery_address_address", "Address*", NOTNULL, $delivery_address["address"], TYPE_CHAR);
	$form->add_label("delivery_address_address2", "", 0, $delivery_address["address2"], TYPE_CHAR);
	$form->add_label("delivery_address_zip", "ZIP*", NOTNULL, $delivery_address["zip"], TYPE_CHAR, 20);
	$form->add_label("delivery_address_place", "City*", NOTNULL, $delivery_address["place"], TYPE_CHAR, 20);
	$form->add_label("delivery_address_province_name", "Province", 0, $delivery_address_province_name);
	$form->add_lookup("delivery_address_country", "Country", "countries", "country_name", 0, $delivery_address["country"]);
	$form->add_label("delivery_address_phone", "Phone*", NOTNULL, $delivery_address["phone"], TYPE_CHAR, 20);
	$form->add_label("delivery_address_fax", "Fax", 0, $delivery_address["fax"], TYPE_CHAR, 20);
	$form->add_label("delivery_address_email", "Email", 0, $delivery_address["email"], TYPE_CHAR);
	$form->add_label("delivery_address_contact", "Contact*", NOTNULL, $delivery_address["contact"], TYPE_CHAR);
}

$form->add_section("POS Location Address");
$form->add_label("shop_address_company", "Project Name*", 0, $project["order_shop_address_company"]);
$form->add_label("shop_address_company2", "", 0, $project["order_shop_address_company2"]);
$form->add_label("shop_address_address", "Address*", 0, $project["order_shop_address_address"]);
$form->add_label("shop_address_address2", "", 0, $project["order_shop_address_address2"]);
$form->add_label("shop_address_zip", "ZIP*", 0, $project["order_shop_address_zip"]);
$form->add_label("shop_address_place", "City*", 0, $project["order_shop_address_place"]);
$form->add_lookup("shop_address_country_name", "Country", "countries", "country_name", 0, $project["order_shop_address_country"]);
$form->add_hidden("shop_address_country", "Country*", 0, $project["order_shop_address_country"]);
$form->add_label("shop_address_phone", "Phone", 0, $project["order_shop_address_phone"]);
$form->add_label("shop_address_fax", "Fax", 0, $project["order_shop_address_fax"]);
$form->add_label("shop_address_email", "Email", 0, $project["order_shop_address_email"]);

if($project["project_projectkind"] == 6) // new POS
{
	$form->add_section("Relocation Info");
	$form->add_comment("Please indicate if this new POS is a relocation of an existing POS within the same mall or city.");
	$form->add_checkbox("project_is_relocation_project", "Yes the POS is relocated", $project["project_is_relocation_project"], "", "Relocation");

	$form->add_list("project_relocated_posaddress_id", "POS being relocated", $sql_posaddresses, 0, $project["project_relocated_posaddress_id"]);
}
else
{
	$form->add_hidden("project_is_relocation_project", 0);
	$form->add_hidden("project_relocated_posaddress_id", 0);
}

if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) // take over, lease renewal
{
	$form->add_hidden("shop_sqms", $project_cost_sqms);
	$form->add_hidden("location_type", $project["project_location_type"]);
	$form->add_hidden("location_type_other", $project["project_location"]);
	$form->add_hidden("voltage", $project["order_voltage"]);


	$form->add_hidden("furniture_height");
	$form->add_hidden("project_furniture_height_mm");
	
	
	$form->add_hidden("watches_displayed",$project["project_watches_displayed"]);
	$form->add_hidden("watches_stored", $project["project_watches_stored"]);
	$form->add_hidden("bijoux_displayed",$project["project_bijoux_displayed"]);
	$form->add_hidden("bijoux_stored", $project["project_bijoux_stored"]);

	$form->add_hidden("preferred_delivery_date", to_system_date($project["order_preferred_delivery_date"]));
	$form->add_hidden("preferred_transportation_arranged", $project["order_preferred_transportation_arranged"]);
	$form->add_hidden("preferred_transportation_mode", $project["order_preferred_transportation_mode"]);

	$form->add_hidden( "pedestrian_mall_approval", $project["order_pedestrian_mall_approval"]);
	$form->add_hidden( "full_delivery", $project["order_full_delivery"]);
	$form->add_hidden("delivery_comments", $project["order_delivery_comments"]);

	$form->add_hidden("order_insurance", $project["order_insurance"]);

	$form->add_hidden("approximate_budget", $project["project_approximate_budget"]);
	$form->add_hidden("planned_opening_date", to_system_Date($project["project_planned_opening_date"]));


	$form->add_hidden("comments", $project["project_comments"]);
}
else
{
	$form->add_section("Sales Surface");
	$form->add_edit("shop_sqms", "Sales Surface sqms", 0, $project_cost_sqms, TYPE_DECIMAL, 10,2);


	$form->add_section("Location Info");
	$form->add_list("location_type", "Location Type*", $sql_location_types, 0, $project["project_location_type"]);
	$form->add_edit("location_type_other", "Other Location Type*", 0, $project["project_location"], TYPE_CHAR, 20);
	$form->add_list("voltage", "Voltage Choice*", $sql_voltages, NOTNULL, $project["order_voltage"]);


	$form->add_section("Furniture Height");
	$form->add_list("furniture_height", "Furniture Height in mm*", $sql_ceiling_heights, SUBMIT, $project["project_furniture_height"]);
	$form->add_edit("project_furniture_height_mm", "", 0, $project["project_furniture_height_mm"], TYPE_INT, 4);
	

	if($project["date_created"] < '2010-12-05')
	{
		// count design_objective_groups old version when design objectives were depending on the product line
		$sql_design_objective_group = "select design_objective_group_id, design_objective_group_name, " .
									  "    design_objective_group_multiple " .
									  "from design_objective_groups " .
									  "where design_objective_group_product_line=  " . $project["project_product_line"] . " " .
									  "order by design_objective_group_priority";

		$res = mysql_query($sql_design_objective_group) or dberror($sql_design_objective_group);
		$number_of_design_objective_groups = mysql_num_rows($res);
	}
	else
	{
		// count design_objective_groups new version, design objectives depending on the postype
		$sql_design_objective_group = "select design_objective_group_id, design_objective_group_name, " .
									  "    design_objective_group_multiple " .
									  "from design_objective_groups " .
									  "where design_objective_group_postype =  " . $project["project_postype"] . " " .
									  "order by design_objective_group_priority";

		$res = mysql_query($sql_design_objective_group) or dberror($sql_design_objective_group);
		$number_of_design_objective_groups = mysql_num_rows($res);
	}

	if ($number_of_design_objective_groups > 0)
	{
		$form->add_section("Design Objectives");
		$form->add_comment("Please indicate your basic needs and requirements to assist out design process. ".
		"\n A Project Manager will ensure that the design is consistent with the ".
		BRAND . " Retail objectives."); 

		$i = 1;
		
		while ($row = mysql_fetch_assoc($res))
		{
			$sql_design_objective_item = "select design_objective_item_id, design_objective_item_name ".
										 "from design_objective_items ".
										 "where design_objective_item_group=" .    $row["design_objective_group_id"] . " ".
										 "order by design_objective_item_priority";

			if ($row["design_objective_group_multiple"] == 0) 
			{
				$value = "";
				foreach ($project_design_objective_item_ids as $key=>$element)
				{
				  if ($element == $row["design_objective_group_name"])
				  {
					  $value = $key;
				  }
				}

				$form->add_list("design_objective_items" . $i, $row["design_objective_group_name"] . "*", $sql_design_objective_item, NOTNULL, $value);
				$design_objectives_listbox_names[] = "design_objective_items" . $i;
			}
			else 
			{
				$values = array();
				foreach ($project_design_objective_item_ids as $key=>$element)
				{
					if ($element == $row["design_objective_group_name"])
					{
						$values[] = $key;
					}
					next($project_design_objective_item_ids);
				}
				$form->add_checklist("design_objective_items" . $i, $row["design_objective_group_name"] . "*", "project_items", $sql_design_objective_item, NOTNULL, $values);
				$design_objectives_checklist_names[] = "design_objective_items" . $i;
			}
			
			$i++;   
		}
	}

	$form->add_section("Capacity Request by Client");
	$form->add_edit("watches_displayed", "Watches Displayed", 0, $project["project_watches_displayed"], TYPE_INT, 20);
	$form->add_edit("watches_stored", "Watches Stored", 0, $project["project_watches_stored"], TYPE_INT, 20);
	$form->add_edit("bijoux_displayed", "Bijoux Displayed", 0, $project["project_bijoux_displayed"], TYPE_INT, 20);
	$form->add_edit("bijoux_stored", "Bijoux Stored", 0, $project["project_bijoux_stored"], TYPE_INT, 20);

	$form->add_section("Preferences and Traffic Checklist");
	$form->add_comment("Please enter the date in the form of dd.mm.yyyy.");
	$form->add_edit("preferred_delivery_date", "Preferred Arrival Date*", NOTNULL, to_system_date($project["order_preferred_delivery_date"]), TYPE_DATE, 20);
	
	
	
	$form->add_list("preferred_transportation_arranged", "Transportation arranged by*", $sql_transportation_arranged, NOTNULL, $project["order_preferred_transportation_arranged"]);

	$form->add_list("preferred_transportation_mode", "Transportation mode*", $sql_transportation_modes, NOTNULL, $project["order_preferred_transportation_mode"]);

	//$form->add_radiolist( "packaging_retraction", "Packaging Retraction Desired", array(0 => "no", 1 => "yes"), 0, $project["order_packaging_retraction"]);

	$form->add_comment("Please indicate if there is a special approval needed for delivery into ".
					   "a pedestrian area."); 
	$form->add_radiolist( "pedestrian_mall_approval", "Pedestrian Area Approval Needed",
		array(0 => "no", 1 => "yes"), 0, $project["order_pedestrian_mall_approval"]);
	$form->add_comment("Please indicate if partial delivery is possible or full delivery is required."); 
	$form->add_radiolist( "full_delivery", "Full Delivery",
		array(0 => "no", 1 => "yes"), 0, $project["order_full_delivery"]);
	$form->add_comment("Please indicate any other circumstances/restrictions concerning delivery and traffic."); 
	$form->add_multiline("delivery_comments", "Delivery Comments", 4, 0, $project["order_delivery_comments"]);

	$form->add_section("Insurance");
	$form->add_radiolist("order_insurance", array(1=>"Insurance by " . BRAND . "/Forwarder", 0=>""), array(1=>"covered",0=>"not covered"), VERTICAL, $project["order_insurance"]);


	$form->add_section("Other Information");
	$form->add_comment("Please use only figures to indicate the budget ".
					   "and enter the date in the form of dd.mm.yyyy.");
	$form->add_edit("approximate_budget", "Approximate Budget in " . $currency["symbol"] . "*", NOTNULL, $project["project_approximate_budget"], TYPE_DECIMAL, 20, 2);
	$form->add_edit("planned_opening_date", "Planned Opening Date*", NOTNULL, to_system_Date($project["project_planned_opening_date"]), TYPE_DATE, 20);


	$form->add_section("General Comments");
	$form->add_multiline("comments", "Comments", 4, 0, $project["project_comments"]);
}

$form->add_button("save", "Save Data");



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("product_line"))
{
    delete_design_objective_items(param("pid"));
    project_update_project_product_line(param("pid"), $form->value("product_line"));
    $link = "project_edit_client_data.php?pid=" . param("pid") . "&change_of_product_line=1";
    redirect($link);
}
elseif ($form->button("client_address_id"))
{
}
elseif($form->button("furniture_height")) {
	$sql= "select ceiling_height_id, ceiling_height_height ".
		  "from ceiling_heights " . 
		  "where ceiling_height_id = " . dbquote($form->value("furniture_height"));

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$form->value('project_furniture_height_mm', $row['ceiling_height_height']);
	}
	
}
else if ($form->button("delivery_address_id"))
{
    // set delivery address

    $form->value("delivery_address_company", "");
    $form->value("delivery_address_company2",  "");
    $form->value("delivery_address_address",  "");
    $form->value("delivery_address_address2",  "");
    $form->value("delivery_address_zip",  "");
    $form->value("delivery_address_place",  "");
    $form->value("delivery_address_country",  0);
    $form->value("delivery_address_phone",  "");
    $form->value("delivery_address_fax",  "");
    $form->value("delivery_address_email",  "");
    $form->value("delivery_address_contact",  "");


    if ($form->value("delivery_address_id"))
    {
       $sql = "select * from order_addresses where order_address_id = " . $form->value("delivery_address_id");
        $res = mysql_query($sql) or dberror($sql);

        if ($row = mysql_fetch_assoc($res))
        {
            $form->value("delivery_address_company", $row["order_address_company"]);
            $form->value("delivery_address_company2",  $row["order_address_company2"]);
            $form->value("delivery_address_address",  $row["order_address_address"]);
            $form->value("delivery_address_address2",  $row["order_address_address2"]);
            $form->value("delivery_address_zip",  $row["order_address_zip"]);
            $form->value("delivery_address_place",  $row["order_address_place"]);
            $form->value("delivery_address_country",  $row["order_address_country"]);
            $form->value("delivery_address_phone",  $row["order_address_phone"]);
            $form->value("delivery_address_fax",  $row["order_address_fax"]);
            $form->value("delivery_address_email",  $row["order_address_email"]);
            $form->value("delivery_address_contact",  $row["order_address_contact"]);
        }
    }   
}
else if ($form->button("save"))
{
// add validation ruels

    if($project["project_projectkind"] != 4 and $project["project_projectkind"] != 5) // take over, lease renewal
	{
		if (!$form->value("location_type") and !$form->value("location_type_other"))
		{
			$form->add_validation("{location_type}", "The location type must not be empty.");
		}
	}    
    if ($form->validate())
    {
        project_update_client_data($form,$design_objectives_listbox_names,$design_objectives_checklist_names);
        $form->message("Your changes have been saved.");
    }
}
/*
else if ($form->button("project_cost_type"))
{
	if($form->value("project_cost_type") == 2) // Franchisee Store
	{
		$form->value("franchisee_address_company", "");
		$form->value("franchisee_address_company2",  "");
		$form->value("franchisee_address_address",  "");
		$form->value("franchisee_address_address2",  "");
		$form->value("franchisee_address_zip",  "");
		$form->value("franchisee_address_place",  "");
		$form->value("franchisee_address_country",  0);
		$form->value("franchisee_address_phone",  "");
		$form->value("franchisee_address_fax",  "");
		$form->value("franchisee_address_email",  "");
		$form->value("franchisee_address_contact",  "");
	}
}
else if ($form->button("franchisee_address_id"))
{
    
	if($form->value("project_cost_type") == 2) // Franchisee Store
	{
		// set new franchisee address
		$form->value("franchisee_address_company", "");
		$form->value("franchisee_address_company2",  "");
		$form->value("franchisee_address_address",  "");
		$form->value("franchisee_address_address2",  "");
		$form->value("franchisee_address_zip",  "");
		$form->value("franchisee_address_place",  "");
		$form->value("franchisee_address_country",  0);
		$form->value("franchisee_address_phone",  "");
		$form->value("franchisee_address_fax",  "");
		$form->value("franchisee_address_email",  "");
		$form->value("franchisee_address_contact",  "");

		if ($form->value("franchisee_address_id"))
		{
			$sql = "select * from orders where order_id = " . $form->value("franchisee_address_id");
			$res = mysql_query($sql) or dberror($sql);

			if ($row = mysql_fetch_assoc($res))
			{
				$form->value("franchisee_address_company", $row["order_franchisee_address_company"]);
				$form->value("franchisee_address_company2",  $row["order_franchisee_address_company2"]);
				$form->value("franchisee_address_address",  $row["order_franchisee_address_address"]);
				$form->value("franchisee_address_address2",  $row["order_franchisee_address_address2"]);
				$form->value("franchisee_address_zip",  $row["order_franchisee_address_zip"]);
				$form->value("franchisee_address_place",  $row["order_franchisee_address_place"]);
				$form->value("franchisee_address_country",  $row["order_franchisee_address_country"]);
				$form->value("franchisee_address_phone",  $row["order_franchisee_address_phone"]);
				$form->value("franchisee_address_fax",  $row["order_franchisee_address_fax"]);
				$form->value("franchisee_address_email",  $row["order_franchisee_address_email"]);
				$form->value("franchisee_address_contact",  $row["order_franchisee_address_contact"]);
			}
		}
	}
}
elseif ($form->button("billing_address_id"))
{
    // set new billing address

    $form->value("billing_address_company", "");
    $form->value("billing_address_company2",  "");
    $form->value("billing_address_address",  "");
    $form->value("billing_address_address2",  "");
    $form->value("billing_address_zip",  "");
    $form->value("billing_address_place",  "");
    $form->value("billing_address_country",  0);
    $form->value("billing_address_phone",  "");
    $form->value("billing_address_fax",  "");
    $form->value("billing_address_email",  "");
    $form->value("billing_address_contact",  "");

    if ($form->value("billing_address_id"))
    {
        $sql = "select * from orders where order_id = " . $form->value("billing_address_id");
        $res = mysql_query($sql) or dberror($sql);

        if ($row = mysql_fetch_assoc($res))
        {
            $form->value("billing_address_company", $row["order_billing_address_company"]);
            $form->value("billing_address_company2",  $row["order_billing_address_company2"]);
            $form->value("billing_address_address",  $row["order_billing_address_address"]);
            $form->value("billing_address_address2",  $row["order_billing_address_address2"]);
            $form->value("billing_address_zip",  $row["order_billing_address_zip"]);
            $form->value("billing_address_place",  $row["order_billing_address_place"]);
            $form->value("billing_address_country",  $row["order_billing_address_country"]);
            $form->value("billing_address_phone",  $row["order_billing_address_phone"]);
            $form->value("billing_address_fax",  $row["order_billing_address_fax"]);
            $form->value("billing_address_email",  $row["order_billing_address_email"]);
            $form->value("billing_address_contact",  $row["order_billing_address_contact"]);
        }
    }
}
*/


    
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Edit Request");
$form->render();
$page->footer();



?>