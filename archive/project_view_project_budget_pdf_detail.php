<?php
/********************************************************************

    project_view_project_budget_pdf_detail.php

    View project budget in a PDF

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2003-10-03
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-07
    Version:        1.1.0

    Copyright (c) 2003, Swatch AG, All Rights Reserved.

*********************************************************************/

/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());

// read project and order details
$project = get_project(param("pid"));

// get orders's currency
$order_currency = get_order_currency($project["project_order"]);

$page_title = "Project Budget - Project " . $project["project_number"];

$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];


$order_state_name = get_actual_order_state_name($project["order_actual_order_state_code"], 1);
$retail_coordinator = get_user($project["project_retail_coordinator"]);
$retail_operator = get_user($project["order_retail_operator"]);

$invoice_address = "";

$billing_address_province_name = "";
if($project["order_billing_address_place_id"])
{
	$billing_address_province_name = get_province_name($project["order_billing_address_place_id"]);
}

$invoice_address = $invoice_address . $project["order_billing_address_company"];
if ($project["order_billing_address_company2"])
{
    $invoice_address = $invoice_address . ", " . $project["order_billing_address_company2"];
}

$invoice_address = $invoice_address . ", " . $project["order_billing_address_address"];

if ($project["order_billing_address_address2"])
{
    $invoice_address = $invoice_address . ", " . $project["order_billing_address_address2"];
}

$invoice_address = $invoice_address . ", " . $project["order_billing_address_zip"] . " " . $project["order_billing_address_place"];

if($billing_address_province_name) {
	$invoice_address .= ", " . $billing_address_province_name;
}
$invoice_address = $invoice_address . ", " . $project["order_billing_address_country_name"];
$invoice_address = $invoice_address . ", Phone: " . $project["order_billing_address_phone"];

// get company's address
$client_address = get_address($project["order_client_address"]);
$client = $client_address["company"] . ", " .
          $client_address["zip"] . " " . $client_address["place"] . ", " .
          $client_address["country_name"];



//calculate budget group totals
$grouptotals_investment = array();
$grouptotals_investment[2] = 0;
$grouptotals_investment[6] = 0;
$grouptotals_investment[7] = 0;
$grouptotals_investment[8] = 0;
$grouptotals_investment[9] = 0;
$grouptotals_investment[10] = 0;
$grouptotals_investment[11] = 0;


$sql = "select order_item_cost_group, order_item_type, ".
       "order_item_system_price, order_item_quantity,  ".
       "order_item_system_price_freezed, order_item_quantity_freezed ".
       "from order_items ".
       "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
       "   and order_item_order=" . $project["project_order"] . 
       "   and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION .
	   "   or order_item_type = " . ITEM_TYPE_SERVICES .
       "   or order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST .
       " ) " .
	   " and order_item_cost_group > 0 " . 
	   " order by order_item_cost_group";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
	{
		if($row["order_item_quantity"] > 0)
		{
			$grouptotals_investment[$row["order_item_cost_group"]] = $grouptotals_investment[$row["order_item_cost_group"]] + $row["order_item_system_price"] * $row["order_item_quantity"];

		}
		else
		{
			$grouptotals_investment[$row["order_item_cost_group"]] = $grouptotals_investment[$row["order_item_cost_group"]] + $row["order_item_system_price"];
		}
	}
	else
	{
		if($row["order_item_quantity_freezed"] > 0)
		{
			$grouptotals_investment[$row["order_item_cost_group"]] = $grouptotals_investment[$row["order_item_cost_group"]] + $row["order_item_system_price_freezed"] * $row["order_item_quantity_freezed"];
		}
		else
		{
			$grouptotals_investment[$row["order_item_cost_group"]] = $grouptotals_investment[$row["order_item_cost_group"]] + $row["order_item_system_price_freezed"];
		}
	}
}

//budget
$project_cost_construction = $grouptotals_investment[7];
$project_cost_fixturing = $grouptotals_investment[10] + $grouptotals_investment[6] + $grouptotals_investment[2];
$project_cost_architectural = $grouptotals_investment[9];
$project_cost_equipment = $grouptotals_investment[11];
$project_cost_other = + $grouptotals_investment[8];
$budget_total = $project_cost_construction + $project_cost_fixturing + $project_cost_architectural + $project_cost_equipment + $project_cost_other;



// prepare SQLs

// create sql for oder items
if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
{
	$sql_order_items = "select order_item_id, order_item_text, order_item_quantity, ".
                       "    order_item_po_number, item_id, order_item_client_price, ".
                       "    TRUNCATE(order_item_quantity * order_item_client_price, 2) as total_price, ".
                       "    order_item_client_price, ".
                       "    if(item_code <> '', item_code, item_type_name) as item_shortcut, ".
                       "    category_priority, category_name, ".
                       "    address_shortcut, item_type_id, ".
                       "    item_type_priority, order_item_type, ".
                       "    concat(item_category_sortorder, ' ', item_category_name, ', ', category_name) as cat_name  ".
                       "from order_items ".
                       "left join items on order_item_item = item_id ".
                       "left join categories on order_item_category = category_id ".
                       "left join item_categories on item_category_id = item_category " .
                       "left join addresses on order_item_supplier_address = address_id ".
                       "left join item_types on order_item_type = item_type_id";

    $list_filter1 = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
                      "   and order_item_order = " . $project["project_order"] .
                      "   and order_item_cost_group IN (10,6, 2)";

    $list_filter2 = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
                      "   and order_item_order = " . $project["project_order"] .
                      "   and order_item_cost_group = 9";

    $list_filter3 = "order_item_order = " . $project["project_order"] .
                      "   and order_item_cost_group = 11";

	$list_filter7 = "order_item_order = " . $project["project_order"] .
                      "   and order_item_cost_group = 8";

    $list_filter6 = "order_item_order = " . $project["project_order"] .
                      "   and  order_item_cost_group = 7";
}
else
{
    $sql_order_items = "select order_item_id, order_item_text, order_item_quantity_freezed, ".
                       "    order_item_po_number, item_id, order_item_client_price_freezed, ".
                       "    TRUNCATE(order_item_quantity_freezed * order_item_client_price_freezed, 2) as total_price, ".
                       "    order_item_client_price_freezed, ".
                       "    if(item_code <> '', item_code, item_type_name) as item_shortcut, ".
                       "    category_priority, category_name, ".
                       "    address_shortcut, item_type_id, ".
                       "    item_type_priority, order_item_type, ".
                       "    concat(item_category_sortorder, ' ', item_category_name, ', ', category_name) as cat_name  ".
                       "from order_items ".
                       "left join items on order_item_item = item_id ".
                       "left join categories on order_item_category = category_id ".
                       "left join item_categories on item_category_id = item_category " .
                       "left join addresses on order_item_supplier_address = address_id ".
                       "left join item_types on order_item_type = item_type_id";

    $list_filter1 = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
                      "   and order_item_order = " . $project["project_order"] .
                      "   and order_item_cost_group IN (10,6, 2) " . 
		              " and order_item_in_freezed_budget = 1";
	
    $list_filter2 = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
                      "   and order_item_order = " . $project["project_order"] .
                      "   and order_item_cost_group = 9 and order_item_in_freezed_budget = 1";

    $list_filter3 = "order_item_order = " . $project["project_order"] .
                      "   and order_item_cost_group = 11 and order_item_in_freezed_budget = 1";

	$list_filter7 = "order_item_order = " . $project["project_order"] .
                      "   and order_item_cost_group = 8 and order_item_in_freezed_budget = 1";

    $list_filter6 = "order_item_order = " . $project["project_order"] .
                      "   and order_item_cost_group = 7 and order_item_in_freezed_budget = 1";



}



$list_filter4 = "order_item_order = " . $project["project_order"] .
                      "   and order_item_type = " . ITEM_TYPE_EXCLUSION;

$list_filter5 = "order_item_order = " . $project["project_order"] .
                      "   and order_item_type = " . ITEM_TYPE_NOTIFICATION;


// count number of items
$num_recs1 = 0;
$num_recs2 = 0;
$num_recs3 = 0;
$num_recs4 = 0;
$num_recs5 = 0;
$num_recs6 = 0;
$num_recs7 = 0;

// fixturing
$sql = "select count(order_item_id) as num_recs ".
       "from order_items where " . $list_filter1;

$res = mysql_query($sql) or dberror($sql);

if ($row = mysql_fetch_assoc($res))
{
    $num_recs1 = $row["num_recs"];
}

// architectural
$sql = "select count(order_item_id) as num_recs ".
       "from order_items where " . $list_filter2;

$res = mysql_query($sql) or dberror($sql);

if ($row = mysql_fetch_assoc($res))
{
    $num_recs2 = $row["num_recs"];
}

// local construction cost estimation
$sql = "select count(order_item_id) as num_recs ".
       "from order_items where " . $list_filter6;

$res = mysql_query($sql) or dberror($sql);

if ($row = mysql_fetch_assoc($res))
{
    $num_recs6 = $row["num_recs"];
}

// equipment
$sql = "select count(order_item_id) as num_recs ".
       "from order_items where " . $list_filter3;

$res = mysql_query($sql) or dberror($sql);

if ($row = mysql_fetch_assoc($res))
{
    $num_recs3 = $row["num_recs"];
}

// exclusions
$sql = "select count(order_item_id) as num_recs ".
       "from order_items where " . $list_filter4;

$res = mysql_query($sql) or dberror($sql);

if ($row = mysql_fetch_assoc($res))
{
    $num_recs4 = $row["num_recs"];
}

// notifications
$sql = "select count(order_item_id) as num_recs ".
       "from order_items where " . $list_filter5;

$res = mysql_query($sql) or dberror($sql);

if ($row = mysql_fetch_assoc($res))
{
    $num_recs5 = $row["num_recs"];
}

// equipment
$sql = "select count(order_item_id) as num_recs ".
       "from order_items where " . $list_filter7;

$res = mysql_query($sql) or dberror($sql);

if ($row = mysql_fetch_assoc($res))
{
    $num_recs7 = $row["num_recs"];
}


// build line numbers for budget positions

if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
{
    // build group_totals of standard items
    $group_totals = get_group_total_of_standard_items($project["project_order"], "order_item_client_price");

    // build totals
    $standard_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_STANDARD);
    $special_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_SPECIAL);
    
    $cost_estimation_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_COST_ESTIMATION);

    $local_construction_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_LOCALCONSTRUCTIONCOST);
}
else
{
    // build group_totals of standard items
    $group_totals = get_group_total_of_standard_items_freezed($project["project_order"], "order_item_client_price_freezed");

    // build totals
    $standard_item_total = get_order_item_type_total_freezed($project["project_order"],  ITEM_TYPE_STANDARD);
    $special_item_total = get_order_item_type_total_freezed($project["project_order"],  ITEM_TYPE_SPECIAL);
    $cost_estimation_item_total = get_order_item_type_total_freezed($project["project_order"],  ITEM_TYPE_COST_ESTIMATION);

    $local_construction_item_total = get_order_item_type_total_freezed($project["project_order"],  ITEM_TYPE_LOCALCONSTRUCTIONCOST);
}

$total_cost = $standard_item_total["in_order_currency"] + $special_item_total["in_order_currency"] + $cost_estimation_item_total["in_order_currency"] + $local_construction_item_total["in_order_currency"];


// prepare output fields
$captions1 = array();
$captions1[] = "Project Number:";
if($project["productline_subclass_name"])
{
	$captions1[] = "Product Line / Subclass / Type:";
}
else
{
	$captions1[] = "Product Line / Type:";
}
$captions1[] = "Project Starting Date / Agreed Opening Date:";
$captions1[] = "Status:";
$captions1[] = "Project Manager / Retail Operator:";

$captions2 = array();
$captions2[] = "Construction/Building in " .$order_currency["symbol"];
$captions2[] = "Store Fixturing in " .$order_currency["symbol"];
$captions2[] = "Architectural Cost in " .$order_currency["symbol"];
$captions2[] = "Equipment in " .$order_currency["symbol"];
$captions2[] = "Other Cost in " .$order_currency["symbol"];
$captions2[] = "Total Cost";

$captions3 = array();
$captions3[] = "Client:";
$captions3[] = "Shop:";
$captions3[] = "Notify Address:";

$data1 = array();
$data1[] = $project["project_number"];

if($project["productline_subclass_name"])
{
	$data1[] = $project["product_line_name"] . " / " . $project["productline_subclass_name"] . " / " . $project["postype_name"];
}
else
{
	$data1[] = $project["product_line_name"] . " / " . $project["postype_name"];	
}
$data1[] = to_system_date($project["order_date"]) . " / " .  to_system_date($project["project_real_opening_date"]);
$data1[] = $project["order_actual_order_state_code"] . " " . $order_state_name;
$data1[] = $retail_coordinator["firstname"] . " " . $retail_coordinator["name"] . " / " . $retail_operator["firstname"] . " " . $retail_operator["name"];

$data2 = array();
$data2[] = number_format($project_cost_construction, 2);
$data2[] = number_format($project_cost_fixturing, 2);
$data2[] = number_format($project_cost_architectural, 2);
$data2[] = number_format($project_cost_equipment, 2);
$data2[] = number_format($project_cost_other, 2);
$data2[] = number_format($budget_total,2);

$data3 = array();
$data3[] = $client;
$data3[] = $shop;
$data3[] = $invoice_address;


// item data
$captions4 = array();
$captions4[1] = "Item";
$captions4[2] = "Description";
$captions4[3] = "Quantity";
$captions4[4] = "Cost in " . $order_currency["symbol"];
$captions4[5] = "Total in " . $order_currency["symbol"];



//Instanciation of inherited class

$pdf->SetAutoPageBreak(true, 5);

$pdf->SetLineWidth(0.1);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->AddPage("l", "A4", $page_title);
$pdf->SetY(8);

$pdf->Image('../pictures/omega_logo.jpg',10,8,33);
$pdf->SetFont('arialn','B',12);
$pdf->Cell(80);
$pdf->Cell(0,10, $page_title, 0, 0, 'R');
$pdf->Ln(5);
$pdf->SetFont('arialn','I',8);
$pdf->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$pdf->PageNo() ." ",0,0,'R');
$pdf->Ln(15);

// output project header informations
$pdf->SetFont('arialn','B',12);
$pdf->Cell(140,5,"Project Details",1);
$pdf->Cell(5,5," ",0);
$pdf->Cell(75,5,"Project Cost",1);
$pdf->Cell(5,5," ",0);
$pdf->Cell(50,5,"Budget Approval",1);

$pdf->Ln();


$pdf->SetFont('arialn','',9);

foreach($captions1 as $key=>$value)
{
    $pdf->Cell(70,5,$captions1[$key],1);
    $pdf->Cell(70,5,$data1[$key],1);

    $pdf->Cell(5,5," ",0);

    if($key <=4)
    {
        
        $pdf->Cell(45,5,$captions2[$key],1);
        $pdf->Cell(30,5,$data2[$key], 1, 0, 'R');
    }
    

    if($key == 0)
    {
        $pdf->Cell(5,5," ",0);
        $pdf->Cell(50,5, 'Name, Date and Signature' ,1);
        $pdf->Ln();
    }
    elseif($key == 1)
    {
        $pdf->Cell(5,5," ",0);
        $pdf->Cell(50,5, '' ,1);
        $pdf->Ln();
    }
    elseif($key == 2)
    {
        $pdf->Cell(5,5," ",0);
        $pdf->Cell(50,5, '' ,1);
        $pdf->Ln();
    }
    elseif($key == 3)
    {
        $pdf->Cell(5,15," ",0);
        $pdf->Cell(50,15, " " ,1);
        $pdf->Ln();
        $pdf->SetY($pdf->GetY()-10); 
    }

}
$pdf->Ln();
$pdf->Cell(145,5,'',0);
$pdf->SetFont('arialn','B',9);
$pdf->Cell(45,5,$captions2[5],1);
$pdf->Cell(30,5,$data2[5], 1, 0, 'R');

$pdf->SetFont('arialn','',9);

$pdf->Ln();
$pdf->Ln();

foreach($captions3 as $key=>$value)
{
    $pdf->SetFont('arialn','B',9);
    $pdf->Cell(40,5,$captions3[$key],1);
    $pdf->SetFont('arialn','',9);
    $pdf->MultiCell(235,5,$data3[$key],1);
}

$pdf->Ln();

// Budget Positions Local Construction Cost Estimation
if($num_recs6 > 0)
{
    $y = $pdf->getY();
    if($y >= 160)
    {
        $pdf->AddPage("l", "A4", $page_title);
		$pdf->SetY(8);
		$pdf->Image('../pictures/omega_logo.jpg',10,8,33);
		$pdf->SetFont('arialn','B',12);
		$pdf->Cell(80);
		$pdf->Cell(0,10, $page_title, 0, 0, 'R');
		$pdf->Ln(5);
		$pdf->SetFont('arialn','I',8);
		$pdf->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$pdf->PageNo() ." ",0,0,'R');
		$pdf->Ln(15);
    }
    $pdf->SetFont('arialn','B',9);
    $pdf->SetFillColor(220, 220, 220); 
    $pdf->Cell(275,5, "Construction/Building", 1, 0, 'L', 1);
    $pdf->Ln();

    $pdf->SetFillColor(240, 240, 240); 
    $pdf->Cell(50,5, $captions4[1], 1, 0, 'L', 1);
    $pdf->Cell(195,5, $captions4[2], 1, 0, 'L', 1);
    $pdf->Cell(30,5, $captions4[5], 1, 0, 'R', 1);
    $pdf->Ln();

    $pdf->SetFont('arialn','',9);


    $sql = $sql_order_items . " where " . $list_filter6 .
           " order by order_item_type, order_item_id, category_priority, item_shortcut";

    $res = mysql_query($sql) or dberror($sql);

    $type_total = 0;

    while ($row = mysql_fetch_assoc($res))
    {
        $pdf->Cell(50,5, $row["item_shortcut"], 1);
        $pdf->Cell(195,5, substr($row["order_item_text"], 0, 140), 1);
        
        if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
        {
            $pdf->Cell(30,5, number_format($row["order_item_client_price"], 2), 1, 0, 'R');
            $pdf->Ln();
            $type_total = $type_total + $row["order_item_client_price"];
        }
        else
        {
            $pdf->Cell(30,5, number_format($row["order_item_client_price_freezed"], 2), 1, 0, 'R');
            $pdf->Ln();
            $type_total = $type_total + $row["order_item_client_price_freezed"];
        }
    }

    $pdf->SetFont('arialn','B',9);
    $pdf->Cell(275,5, 'Total Construction/Building in ' . $order_currency["symbol"] . ' ' . number_format($type_total, 2) , 1, 0, 'R', 1);

    $pdf->Ln();
    $pdf->Ln();
}


// Budget Positions Fixturing/Furniture
if($num_recs1 > 0)
{
    
	$y = $pdf->getY();
	if($y >= 160)
	{
		$pdf->AddPage("l", "A4", $page_title);
		$pdf->SetY(8);
		$pdf->Image('../pictures/omega_logo.jpg',10,8,33);
		$pdf->SetFont('arialn','B',12);
		$pdf->Cell(80);
		$pdf->Cell(0,10, $page_title, 0, 0, 'R');
		$pdf->Ln(5);
		$pdf->SetFont('arialn','I',8);
		$pdf->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$pdf->PageNo() ." ",0,0,'R');
		$pdf->Ln(15);
	}

	$pdf->SetFont('arialn','B',9);
    $pdf->SetFillColor(220, 220, 220); 
    $pdf->Cell(275,5, "Store Fixturing/Furniture", 1, 0, 'L', 1);
    $pdf->Ln();

    $pdf->SetFillColor(240, 240, 240); 
    $pdf->Cell(50,5, $captions4[1], 1, 0, 'L', 1);
    $pdf->Cell(160,5, $captions4[2], 1, 0, 'L', 1);
    $pdf->Cell(15,5, $captions4[3], 1, 0, 'R', 1);
    $pdf->Cell(20,5, $captions4[4], 1, 0, 'R', 1);
    $pdf->Cell(30,5, $captions4[5], 1, 0, 'R', 1);
    $pdf->Ln();

    $pdf->SetFont('arialn','',9);


    $sql = $sql_order_items . " where " . $list_filter1 .
           " order by order_item_type, item_category_sortorder, cat_name, item_shortcut";

    $res = mysql_query($sql) or dberror($sql);

	$type_total = 0;

    $cat = "";

    while ($row = mysql_fetch_assoc($res))
    {
        $y = $pdf->getY();
		if($y >= 160)
		{
			$pdf->AddPage("l", "A4", $page_title);
			$pdf->SetY(8);
			$pdf->Image('../pictures/omega_logo.jpg',10,8,33);
			$pdf->SetFont('arialn','B',12);
			$pdf->Cell(80);
			$pdf->Cell(0,10, $page_title, 0, 0, 'R');
			$pdf->Ln(5);
			$pdf->SetFont('arialn','I',8);
			$pdf->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$pdf->PageNo() ." ",0,0,'R');
			$pdf->Ln(15);
		}


   	    $pdf->SetFont('arialn','',9);
		if($cat != $row["cat_name"])
        {
            $cat = $row["cat_name"];
            $pdf->SetFont('arialn','B',9);
            $pdf->Cell(275,5, $row["cat_name"], 1);
            $pdf->Ln();
            $pdf->SetFont('arialn','',9);

        }
        
        $pdf->Cell(50,5, $row["item_shortcut"], 1);
        $pdf->Cell(160,5, substr($row["order_item_text"], 0, 100), 1);
        
        
        if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
        {
            $pdf->Cell(15,5, $row["order_item_quantity"], 1, 0, 'R');
            $pdf->Cell(20,5, number_format($row["order_item_client_price"], 2), 1, 0, 'R');
        }
        else
        {
            $pdf->Cell(15,5, $row["order_item_quantity_freezed"], 1, 0, 'R');
            $pdf->Cell(20,5, number_format($row["order_item_client_price_freezed"], 2), 1, 0, 'R');
        }
        
        $pdf->Cell(30,5, number_format($row["total_price"], 2), 1, 0, 'R');
        $pdf->Ln();
        $type_total = $type_total + $row["total_price"];
    }

    $pdf->SetFont('arialn','B',9);
    $pdf->Cell(275,5, 'Total Store Fixturing/Furniture in ' . $order_currency["symbol"] . ' ' . number_format($type_total, 2) , 1, 0, 'R', 1);

    $pdf->Ln();
    $pdf->Ln();
}

// Budget Positions Architectural Cost
if($num_recs2 > 0)
{
    $y = $pdf->getY();
    if($y >= 160)
    {
		$pdf->AddPage("l", "A4", $page_title);
		$pdf->SetY(8);
		$pdf->Image('../pictures/omega_logo.jpg',10,8,33);
		$pdf->SetFont('arialn','B',12);
		$pdf->Cell(80);
		$pdf->Cell(0,10, $page_title, 0, 0, 'R');
		$pdf->Ln(5);
		$pdf->SetFont('arialn','I',8);
		$pdf->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$pdf->PageNo() ." ",0,0,'R');
		$pdf->Ln(15);
    }
    $pdf->SetFont('arialn','B',9);
    $pdf->SetFillColor(220, 220, 220); 
    $pdf->Cell(275,5, "Architectural Cost", 1, 0, 'L', 1);
    $pdf->Ln();

    $pdf->SetFillColor(240, 240, 240); 
    $pdf->Cell(50,5, $captions4[1], 1, 0, 'L', 1);
    $pdf->Cell(160,5, $captions4[2], 1, 0, 'L', 1);
    $pdf->Cell(15,5, $captions4[3], 1, 0, 'R', 1);
    $pdf->Cell(20,5, $captions4[4], 1, 0, 'R', 1);
    $pdf->Cell(30,5, $captions4[5], 1, 0, 'R', 1);
    $pdf->Ln();

    $pdf->SetFont('arialn','',9);


    $sql = $sql_order_items . " where " . $list_filter2 .
           " order by order_item_type, category_priority, item_shortcut";

    $res = mysql_query($sql) or dberror($sql);

    $type_total = 0;

    while ($row = mysql_fetch_assoc($res))
    {
        $pdf->Cell(50,5, $row["item_shortcut"], 1);
        $pdf->Cell(160,5, substr($row["order_item_text"], 0, 100), 1);
        
        if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
        {
            $pdf->Cell(15,5, $row["order_item_quantity"], 1, 0, 'R');
            $pdf->Cell(20,5, number_format($row["order_item_client_price"], 2), 1, 0, 'R');
        }
        else
        {
            $pdf->Cell(15,5, $row["order_item_quantity_freezed"], 1, 0, 'R');
            $pdf->Cell(20,5, number_format($row["order_item_client_price_freezed"], 2), 1, 0, 'R');
        }
        
        $pdf->Cell(30,5, number_format($row["total_price"], 2), 1, 0, 'R');
        $pdf->Ln();
        $type_total = $type_total + $row["total_price"];
    }

    $pdf->SetFont('arialn','B',9);
    $pdf->Cell(275,5, 'Total Architecturl Cost in ' . $order_currency["symbol"] . ' ' . number_format($type_total, 2) , 1, 0, 'R', 1);

    $pdf->Ln();
    $pdf->Ln();
}




// Budget Equipment
if($num_recs3 > 0)
{
    $y = $pdf->getY();
    if($y >= 160)
    {
        $pdf->AddPage("l","A4", $page_title);
		$pdf->SetY(8);
		$pdf->Image('../pictures/omega_logo.jpg',10,8,33);
		$pdf->SetFont('arialn','B',12);
		$pdf->Cell(80);
		$pdf->Cell(0,10, $page_title, 0, 0, 'R');
		$pdf->Ln(5);
		$pdf->SetFont('arialn','I',8);
		$pdf->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$pdf->PageNo() ." ",0,0,'R');
		$pdf->Ln(15);
    }
    $pdf->SetFont('arialn','B',9);
    $pdf->SetFillColor(220, 220, 220); 
    $pdf->Cell(275,5, "Equipment", 1, 0, 'L', 1);
    $pdf->Ln();

    $pdf->SetFillColor(240, 240, 240); 
    $pdf->Cell(50,5, $captions4[1], 1, 0, 'L', 1);
    $pdf->Cell(195,5, $captions4[2], 1, 0, 'L', 1);
    $pdf->Cell(30,5, $captions4[5], 1, 0, 'R', 1);
    $pdf->Ln();

    $pdf->SetFont('arialn','',9);


    $sql = $sql_order_items . " where " . $list_filter3 .
           " order by order_item_type, category_priority, item_shortcut";

    $res = mysql_query($sql) or dberror($sql);

    $type_total = 0;

    while ($row = mysql_fetch_assoc($res))
    {
        $pdf->Cell(50,5, $row["item_shortcut"], 1);
        $pdf->Cell(195,5, substr($row["order_item_text"], 0, 140), 1);
        
        if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
        {
            $pdf->Cell(30,5, number_format($row["order_item_client_price"], 2), 1, 0, 'R');
            $pdf->Ln();
            $type_total = $type_total + $row["order_item_client_price"];
        }
        else
        {
            $pdf->Cell(30,5, number_format($row["order_item_client_price_freezed"], 2), 1, 0, 'R');
            $pdf->Ln();
            $type_total = $type_total + $row["order_item_client_price_freezed"];
        }
    }

    $pdf->SetFont('arialn','B',9);
    $pdf->Cell(275,5, 'Total Equipment in ' . $order_currency["symbol"] . ' ' . number_format($type_total, 2) , 1, 0, 'R', 1);

    $pdf->Ln();
    $pdf->Ln();
}


// Budget Other Cost
if($num_recs7 > 0)
{
    $y = $pdf->getY();
    if($y >= 160)
    {
        $pdf->AddPage("l","A4", $page_title);
		$pdf->SetY(8);
		$pdf->Image('../pictures/omega_logo.jpg',10,8,33);
		$pdf->SetFont('arialn','B',12);
		$pdf->Cell(80);
		$pdf->Cell(0,10, $page_title, 0, 0, 'R');
		$pdf->Ln(5);
		$pdf->SetFont('arialn','I',8);
		$pdf->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$pdf->PageNo() ." ",0,0,'R');
		$pdf->Ln(15);
    }
    $pdf->SetFont('arialn','B',9);
    $pdf->SetFillColor(220, 220, 220); 
    $pdf->Cell(275,5, "Other Cost", 1, 0, 'L', 1);
    $pdf->Ln();

    $pdf->SetFillColor(240, 240, 240); 
    $pdf->Cell(50,5, $captions4[1], 1, 0, 'L', 1);
    $pdf->Cell(195,5, $captions4[2], 1, 0, 'L', 1);
    $pdf->Cell(30,5, $captions4[5], 1, 0, 'R', 1);
    $pdf->Ln();

    $pdf->SetFont('arialn','',9);


    $sql = $sql_order_items . " where " . $list_filter7 .
           " order by order_item_type, category_priority, item_shortcut";

    $res = mysql_query($sql) or dberror($sql);

    $type_total = 0;

    while ($row = mysql_fetch_assoc($res))
    {
        $pdf->Cell(50,5, $row["item_shortcut"], 1);
        $pdf->Cell(195,5, substr($row["order_item_text"], 0, 140), 1);
        
        
		if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
        {
            $tmp = $row["order_item_client_price"];
			if($row["order_item_quantity"] > 0)
			{
				$tmp = $row["order_item_quantity"] * $row["order_item_client_price"];
			}
			
			$pdf->Cell(30,5, number_format($tmp, 2), 1, 0, 'R');
            $pdf->Ln();
            $type_total = $type_total + $tmp;
        }
        else
        {
            
			$tmp = $row["order_item_client_price_freezed"];
			if($row["order_item_quantity_freezed"] > 0)
			{
				$tmp = $row["order_item_quantity_freezed"] * $row["order_item_client_price_freezed"];
			}

			$pdf->Cell(30,5, number_format($tmp, 2), 1, 0, 'R');
            $pdf->Ln();
            $type_total = $type_total + $tmp;
        }
    }

    $pdf->SetFont('arialn','B',9);
    $pdf->Cell(275,5, 'Total Other Cost in ' . $order_currency["symbol"] . ' ' . number_format($type_total, 2) , 1, 0, 'R', 1);

    $pdf->Ln();
    $pdf->Ln();
}

// Budget Exclusions
if($num_recs4 > 0)
{
    $y = $pdf->getY();
    if($y >= 160)
    {
        $pdf->AddPage("l", "A4", $page_title);
		$pdf->SetY(8);
		$pdf->Image('../pictures/omega_logo.jpg',10,8,33);
		$pdf->SetFont('arialn','B',12);
		$pdf->Cell(80);
		$pdf->Cell(0,10, $page_title, 0, 0, 'R');
		$pdf->Ln(5);
		$pdf->SetFont('arialn','I',8);
		$pdf->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$pdf->PageNo() ." ",0,0,'R');
		$pdf->Ln(15);
    }

    $pdf->SetFont('arialn','B',9);
    $pdf->SetFillColor(220, 220, 220); 
    $pdf->Cell(275,5, "Exclusions", 1, 0, 'L', 1);
    $pdf->Ln();


    $pdf->SetFont('arialn','',9);


    $sql = $sql_order_items . " where " . $list_filter4 .
           " order by order_item_type, category_priority, item_shortcut";

    $res = mysql_query($sql) or dberror($sql);

    $type_total = 0;

    while ($row = mysql_fetch_assoc($res))
    {
        $pdf->Cell(50,5, $row["item_shortcut"], 1);
        $pdf->MultiCell(225,5, $row["order_item_text"], 1);
    }

    $pdf->Ln();
    $pdf->Ln();
}

// Notifications
if($num_recs5 > 0)
{
    $y = $pdf->getY();
    if($y >= 160)
    {
        $pdf->AddPage("l", "A4", $page_title);
		$pdf->SetY(8);
		$pdf->Image('../pictures/omega_logo.jpg',10,8,33);
		$pdf->SetFont('arialn','B',12);
		$pdf->Cell(80);
		$pdf->Cell(0,10, $page_title, 0, 0, 'R');
		$pdf->Ln(5);
		$pdf->SetFont('arialn','I',8);
		$pdf->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$pdf->PageNo() ." ",0,0,'R');
		$pdf->Ln(15);
    }

    $pdf->SetFont('arialn','B',9);
    $pdf->SetFillColor(220, 220, 220); 
    $pdf->Cell(275,5, "Notifications", 1, 0, 'L', 1);
    $pdf->Ln();


    $pdf->SetFont('arialn','',9);


    $sql = $sql_order_items . " where " . $list_filter5 .
           " order by order_item_type, category_priority, item_shortcut";

    $res = mysql_query($sql) or dberror($sql);

    $type_total = 0;

    while ($row = mysql_fetch_assoc($res))
    {
        $pdf->Cell(50,5, $row["item_shortcut"], 1);
        $pdf->MultiCell(225,5, $row["order_item_text"], 1);
    }

    $pdf->Ln();
    $pdf->Ln();
}


?>