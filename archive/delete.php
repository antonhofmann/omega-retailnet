<?php
/********************************************************************
    delete.php

    Displays the various views used to to delete a record.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-29
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-10-17
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.


*********************************************************************/
require_once "../include/frame.php";

check_access();

$delete = get_session_value("delete_object");
$form = new Form();

if ($form->button("delete"))
{
    if (delete_record($delete->table, $delete->id))
    {
        $form->add_comment($delete->confirmation);
        $form->add_button("success", "Back");
    }
    else
    {
        $form->add_comment($delete->failure);
        $form->add_button("failure", "Back");
    }
}
else if ($form->button("success"))
{
    redirect($delete->back_on_success);
}
else if ($form->button("failure"))
{
    redirect($delete->back_on_failure);
}
else if ($form->button("abort"))
{
    redirect($delete->back_on_abort);
}
else
{
    $form->add_comment($delete->question);
    $form->add_button("delete", "Yes");
    $form->add_button("abort", "No");
}


$page = new Page();
$page->header();
$page->title("Delete " . ucwords($delete->entity));
$form->render();
$page->footer();
?>