<?php
/********************************************************************

    project_flow_control.php

    List Flow Hitory and Actions

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-10-03
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-12-03
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_use_taskcentre_in_projects");

register_param("pid");


/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);


// get order states for history
$sql_order_states = "select order_state_code, order_state_group_code, ".
                    "    order_state_group_name, order_state_action_name, ".
                    "    order_state_name, role_name ".
                    "from order_state_groups ".
                    "left join order_states on order_state_group_id = order_state_group " .
                    "left join roles on order_state_performer = role_id ";

$list3_filter = "order_state_group_order_type = 1";


// get actual order states
$order_states = get_order_states($project["project_order"], 1);

// prepare icon
$icons = array();
$icons[$project["order_actual_order_state_code"]] = "/pictures/actualorderstate_now.gif";

// check if user is a supplier or a forwarder and if he already has performed the action
$user_data = get_user(user_id());

$sql = "select count(order_item_id) as num_recs from order_items ".
       "where order_item_order = " . $project["project_order"] .
       "   and order_item_supplier_address = " . $user_data["address"] .
       "   or order_item_forwarder_address = " . $user_data["address"];

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{       
    $num_recs = $row["num_recs"];
}

$can_see_icon = 0;
if($num_recs == 0)
{
    $can_see_icon = 1;
}


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "flow control");

$form->add_section("Project");
$form->add_hidden("pid", param("pid"));

require_once "include/project_head_small.php";




/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Create History List
*********************************************************************/ 
$list3 = new ListView($sql_order_states, LIST_HAS_SEPARATOR | LIST_HAS_HEADER);

$list3->set_title("History: Project Steps (Indicating the Latest Actions)");
$list3->set_entity("order_state");
$list3->set_filter($list3_filter);
$list3->set_order("order_state_code");
$list3->set_group("order_state_group_code", "order_state_group_name");

$list3->add_image_column("actual_step", "Now", 0, $icons);
$list3->add_column("order_state_code", "Step");
$list3->add_column("order_state_action_name", "Action", "", "", "", COLUMN_NO_WRAP);
$list3->add_text_column("performer", "Sender", COLUMN_NO_WRAP | COLUMN_BREAK, $order_states["performers"]);
$list3->add_text_column("user", "Sent by", COLUMN_NO_WRAP | COLUMN_BREAK, $order_states["users_done"]);
$list3->add_text_column("date", "Date", COLUMN_NO_WRAP | COLUMN_BREAK, $order_states["done_dates"]);
$list3->add_text_column("recipient", "Recipient", COLUMN_NO_WRAP | COLUMN_BREAK, $order_states["recepients"]);

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$list3->populate();
$list3->process();

/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Task Centre");
$form->render();

if (has_access("can_view_history_in_projects"))
{   
    $list3->render();
}

$page->footer();

?>