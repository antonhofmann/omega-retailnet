<?php
/********************************************************************

    project_edit_project_sheet_2004.php

    Edit budget sheet (new project sheet vaild from 27.9.04).

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-09
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-09-27
    Version:        1.3.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_edit_project_sheet");
set_referer("project_financial_justification.php");

if (!isset($_REQUEST["id"]))
{
    $_REQUEST["id"] = param("pid");
}
else
{
    param("pid", $_REQUEST["id"]);
}

/********************************************************************
    prepare all data needed
*********************************************************************/

// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

// get clients's currency
$currency = get_address_currency($project["order_client_address"]);

// get system's currency
$system_currency = get_system_currency_fields();

// get suppliers's address
if ($project["order_supplier_address"])
{
    $supplier_address = get_address($project["order_supplier_address"]);
}

// sql for supplier's list box
$sql_suppliers = "select DISTINCT address_id, address_company ".
                 "from order_items ".
				 "left join addresses on address_id = order_item_supplier_address " .
                 "where order_item_order = " . $project["project_order"] . 
				 " and order_item_supplier_address > 0" . 
				 " order by address_company";

// sql for project requesters
// oho ---------------------------> MUST BE CHANGED TO MATCH USER-LIST IN EXCEL-SHEET!!!
$sql_requesters = "select concat(user_name, ' ', left(user_firstname,1), '.'), " .
                  "   concat(user_name, ' ', user_firstname) as text " .
                  "from user_roles ".
                  "left join users on users.user_id = user_roles.user_role_user ".
                  "where user_active = 1 and user_role_role in (2,3, 4) " .
                  "order by user_name";


$retail_coordinator = get_user($project["project_retail_coordinator"]);
$project_leader = substr($retail_coordinator["firstname"], 0, 1) . ". " . $retail_coordinator["name"];

// sql for business-units select
$sql_bu = "select business_unit_id, business_unit_name " .
          "from business_units where business_unit_application IN ('all', 'projects') and (business_unit_valid_to > '2004-09-27' or business_unit_valid_to is null) order by business_unit_name";


$business_unit = "";
if($project["project_business_unit"])
{
	$business_unit = $project["project_business_unit"];
}
else
{
	$sql = "select postype_business_unit from postypes where postype_id = " . $project["project_postype"];
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$business_unit = $row["postype_business_unit"];
	}
}


// sql for cost center
$sql_cc = "select cost_center_name, cost_center_name as text " .
          "from cost_centers";

$project_cost_center = "10220 / Retail"; //default

// sql for account_number
$sql_acn = "select account_number_name, account_number_name as text " .
          "from account_numbers";
$account_number_name = "E444 / 786000 Retail Furniture"; //default

// sql for closing date strings
$sql_cd = "select closing_date_name, closing_date_name as text " .
          "from closing_dates";


// sql for project sheet productlines
$sql_pspl = "select ps_product_line_name, ps_product_line_name as text " .
          "from ps_product_lines";
$ps_product_line_name = "SXX Brand";

// sql for project sheet spent for
$sql_pssf = "select ps_spent_for_name, ps_spent_for_name as text " .
          "from ps_spent_fors";

$project_ps_spent_for = "Marketing Projects (PRO)";

//Pos Name
$country_name = "";

$sql = "select country_name from countries where country_id = " . $project["order_shop_address_country"];
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$country_name = $row["country_name"];
}

$pos_name = $country_name . ", " . $project["order_shop_address_place"] . ", " . $project["order_shop_address_company"];

//project budget
//cost, budget situation from the list of materials

//get positions to be excluded from internal budget

/*
$local_production = 0;
$sql_c = "select sum(order_item_quantity*order_item_system_price) as total " . 
       "from order_items " . 
	   "where order_item_exclude_from_ps = 1 " . 
	   " and order_item_order = " .  $project["project_order"];

$res_c = mysql_query($sql_c) or dberror($sql_c);
if ($row_c = mysql_fetch_assoc($res_c))
{
	$local_production = $row_c["total"];
}


$list_total = 0;

$sql_c = "select project_cost_budget " .
         "from project_costs " . 
		 "where project_cost_order = " . $project["project_order"];


$res_c = mysql_query($sql_c) or dberror($sql_c);
if ($row_c = mysql_fetch_assoc($res_c))
{
	$list_total = $row_c["project_cost_budget"];

	$list_total = $list_total - $local_production;

	if($list_total > 0)
	{
		$sql_u = "update projects set project_planned_amount_current_year = " . $list_total . 
			     "   where project_id = " . $project["project_id"];
		$result = mysql_query($sql_u) or dberror($sql_u);
	}
}
*/

$line = 1;
$list_total = 0;

$sql_c = "select sum(order_item_quantity*order_item_system_price) as total " . 
       "from order_items " . 
	   "where order_item_cost_group in (2, 6) " . 
	   " and order_item_order = " .  $project["project_order"];

$res_c = mysql_query($sql_c) or dberror($sql_c);
if ($row_c = mysql_fetch_assoc($res_c))
{
	$list_total = $row_c["total"];
	if($list_total > 0)
	{
		$sql_u = "update projects set project_planned_amount_current_year = " . $list_total . 
			     "   where project_id = " . $project["project_id"];
		$result = mysql_query($sql_u) or dberror($sql_u);
	}
}


//check if standard financial justifications are present
$sql = "select count(project_financial_justification_id) as num_recs ". 
       "from project_financial_justifications " .
       "where project_financial_justification_project = " . id();

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);
if ($row["num_recs"] == 0)
{
	$rtc = get_user($project["project_retail_coordinator"]);
	$rto = get_user($project["order_retail_operator"]);
	
	$address = get_address($project["order_client_address"]);
	$cli = $address["company"] . ", " . $address["place"]  . ", " . $address["country_name"] ;
	
	//create standard financial justificateions
	$fj[1] = $project["product_line_name"] . " " . 
		     $project["postype_name"] . " in " . 
		     $country_name . " in " . 
		     $project["order_shop_address_place"] . ", " .
		     $project["order_shop_address_company"] . ", " .
			 $project["order_shop_address_address"];
	

	$fj[2] = "Project Coordinator: " . $rtc["firstname"] . " " . $rtc["name"];
	$fj[3] = "Project Operator: " . $rto["firstname"] . " " . $rto["name"];
	$fj[4] = "Project will be reinvoced 100% to: " . $cli;

	foreach($fj as $key=>$value)
	{
		$sql_i = "insert into project_financial_justifications (".
			     "project_financial_justification_project, " . 
				 "project_financial_justification_description, " .
				 "project_financial_justification_priority, " .
				 "user_created, " .
				 "date_created) values (" .
			     id() . ", " . 
			     dbquote($value) . ", " . 
			     $key . ", " . 
			     dbquote(user_login()) . ", " .
			     "now())";
				 $result = mysql_query($sql_i) or dberror($sql_i);
	}

}

$result = project_sheet_update_cost_groups($project["project_order"]);

/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

$form->add_section("Project");
$form->add_hidden("pid", param("pid"));

require_once "include/project_head_small.php";

$form->add_label("spacer7", "");

$form->add_edit("project_sheet_name", "Project Sheet Title", NOTNULL, "Project Sheet " . date("Y"));
$form->add_edit("project_name", "Project Name*", NOTNULL,  $pos_name);

$form->add_list("project_business_unit", "Business Unit*", $sql_bu, SUBMIT | NOTNULL, $business_unit);


//$form->add_label("project_approval_person8", "Cost Center Resp.",0);
//$form->add_date("project_approval_date8", "Dead Line for Approval");

$form->add_label("spacer8", "");

$form->add_list("project_approval_person9", "Project Leader*", $sql_requesters,  0, $project_leader);
$form->add_date("project_approval_date9", "Dead Line for Approval");


$form->add_section("Accounting Information");

$form->add_list("project_cost_center", "Cost Center*", $sql_cc, NOTNULL, $project_cost_center);

$form->add_list("project_account_number", "Account Number*", $sql_acn, 0 , $account_number_name);


$form->add_section("Budget Information");

$form->add_date("project_opening_date", "Opening Date");

//$form->add_date("project_closing_date", "Closing Date");

$form->add_list("project_closing_date_string", "Closing Date", $sql_cd, 0 , $project["project_closing_date_string"]);

$form->add_list("project_ps_product_line", "Product Line", $sql_pspl, 0 , $ps_product_line_name);

$form->add_list("project_ps_spent_for", "Spent For", $sql_pssf, 0 , $project_ps_spent_for);

$form->add_label("spacer0", "");
$form->add_label("project_planned_amount_current_year", "Planned Amount current Year in ". $currency["symbol"], 0, $list_total);
//$form->add_edit("project_planned_amount_current_year", "Planned Amount current Year in ". $currency["symbol"], 0, $list_total);
$form->add_edit("project_planned_amount_next_year", "Planned Amount next Year in ". $currency["symbol"]);

$share_options = array(0 => 0, 25 => 25, 50 => 50, 75 => 75, 100 => 100);
$form->add_list("project_share_company", "Share " . BRAND . " in %", $share_options, "", 0); 
$form->add_list("project_share_other", "Share Other in %", $share_options, "", 100); 

$form->add_section("Budget Situation");
$form->add_edit("project_budget_total", "Budget Total in ". $system_currency["symbol"]);
$form->add_edit("project_budget_committed", "Budget Committed in ". $system_currency["symbol"]);
$form->add_edit("project_budget_spent", "Already Spent in ". $system_currency["symbol"]);
$form->add_edit("project_budget_after_project", "Open to Commit after this Project in ". $system_currency["symbol"]);

$approval_names = array();



if (param("project_business_unit"))
{
    $sql = "select concat(user_name, ' ', left(user_firstname,1), '.') as username " .
           "from business_units " .
           "left join users on users.user_id = business_units.business_unit_responsible " .
           "where business_unit_id = " . param("project_business_unit");

    $res = mysql_query($sql) or dberror($sql);
    if (($res) && (mysql_num_rows($res) > 0))
    {
        $row = mysql_fetch_assoc($res);

        if (!in_array($row['username'], $approval_names))
        {
            $approval_names["project_approval_person8"] = $row['username'];
        }
        else
        {
            $approval_names["project_approval_person8"] = "";
        }
    }
}



$form->add_section("Approvals");
$form->add_edit("project_approval_person1", "Person 1");
$form->add_date("project_approval_date1", "Dead Line for Approval");
$form->add_label("spacer1", "");
$form->add_edit("project_approval_person2", "Person 2");
$form->add_date("project_approval_date2", "Dead Line for Approval");
$form->add_label("spacer2", "");
$form->add_edit("project_approval_person3", "Person 3");
$form->add_date("project_approval_date3", "Dead Line for Approval");
$form->add_label("spacer3", "");
$form->add_edit("project_approval_person4", "Person 4");
$form->add_date("project_approval_date4", "Dead Line for Approval");
$form->add_label("spacer4", "");
$form->add_edit("project_approval_person5", "Person 5");
$form->add_date("project_approval_date5", "Dead Line for Approval");
$form->add_label("spacer5", "");
$form->add_edit("project_approval_person6", "Person 6");
$form->add_date("project_approval_date6", "Dead Line for Approval");
$form->add_label("spacer6", "");
$form->add_edit("project_approval_person7", "Person 7");
$form->add_date("project_approval_date7", "Dead Line for Approval");
$form->add_label("spacer7", "");



// $form->add_multiline("project_approval_description", "Financial Justification", 6);

$form->add_section("Main Supplier");
$form->add_list("order_supplier_address", "Supplier*", $sql_suppliers, SUBMIT, $project["order_supplier_address"]);
if ($project["order_supplier_address"])
{
    $form->add_label("supplier_company", "Company", 0, $supplier_address["company"]);
    $form->add_label("supplier_address", "Address", 0, $supplier_address["address"]);
    $form->add_label("supplier_place", "City", 0, $supplier_address["zip"] . " " . $supplier_address["place"]);
    $form->add_lookup("supplier_country", "Country", "countries", "country_name", 0, $supplier_address["country"]);

    if ($supplier_address["contact"])
    {
        $line = "concat(user_name, ' ', user_firstname)";
        $form->add_lookup("supplier_contact_user", "Contact", "users", $line, 0, $supplier_address["contact"]);
        $form->add_lookup("supplier_contact_phone", "Phone", "users", "user_phone", 0, $supplier_address["contact"]);
        $form->add_lookup("supplier_contact_email", "Email", "users", "user_email", 0, $supplier_address["contact"]);
    }
}
else
{
    $form->add_label("supplier_company", "Company");
    $form->add_label("supplier_address", "Address");
    $form->add_label("supplier_place", "City");
    $form->add_label("supplier_country", "Country");
    $form->add_label("supplier_contact", "Contact");
    $form->add_label("supplier_phone", "Phone");
    $form->add_label("supplier_email", "Email");
}

if(has_access("can_edit_catalog"))
{
	$form->add_section("List of Project Sheets");
	$form->add_checkbox("project_is_on_project_sheet_list", "include this project in the list", "", "", "List");
	$form->add_section(" ");
	$form->add_section(" ");
}

$form->add_button("add_justification", "Add Financial Jusitification", 
                  "project_financial_justification.php?project=" . id(), OPTIONAL);

$form->add_subtable("project_financial_justifications", "Financial Justification",
                    "project_financial_justification.php", "Description",
    "select project_financial_justification_id, ". 
    "    project_financial_justification_description as Description, " .
    "    project_financial_justification_cost as Cost " .
    "from project_financial_justifications " .
    "where project_financial_justification_project = " . id() . " " .
    "order by project_financial_justification_priority ", "project_financial_justification_priority");

$form->add_button(FORM_BUTTON_SAVE, "Save Data");
$link = "http://" . $_SERVER["HTTP_HOST"] . "/user/project_sheet_pdf_2004.php?id=" . $project["project_order"];
$form->add_button("print_pdf", "Print Project Sheet", $link);

/********************************************************************
    Populate form and process button clicks
*********************************************************************/

if(param("project_sheet_name") or $project["project_sheet_name"])
{
	$form->populate();
}

$form->process();

$form->value("project_name", $pos_name);
if(!$project["project_sheet_name"])
{

	for($i = 1; $i < 9; $i++)
    {
        $approval_names["project_approval_person" . $i] = "";
    }

    $sql = "select budget_approval_user " .
           "from budget_approvals " .
           "where budget_approval_budget_low < ". intval($list_total) . " " .
           "order by budget_approval_priority asc";

    $res = mysql_query($sql);
    $approval_line = 1;
    while($row = mysql_fetch_assoc($res))
    {
        if (!in_array($row['budget_approval_user'], $approval_names))
        {
            while ($approval_line < 9)
            {
                if (empty($approval_names["project_approval_person" . $approval_line]))
                {
                    $approval_names["project_approval_person" . $approval_line] = 
                        $row['budget_approval_user'];
                    break;
                }
                else
                {
                    $approval_line++;
                }
            }
        }
    }

	// assign approval-names

	foreach($approval_names as $key => $name)
	{
			param($key, $name);
	}
	
	$form->value("project_approval_person1", $approval_names["project_approval_person1"]);
	$form->value("project_approval_person2", $approval_names["project_approval_person2"]);
	$form->value("project_approval_person3", $approval_names["project_approval_person3"]);
	$form->value("project_approval_person4", $approval_names["project_approval_person4"]);
	$form->value("project_approval_person5", $approval_names["project_approval_person5"]);
	$form->value("project_approval_person6", $approval_names["project_approval_person6"]);
	$form->value("project_approval_person7", $approval_names["project_approval_person7"]);
}


if ($form->button("order_supplier_address"))
{
    $form->save();
    $sql = "update orders ".
           "set order_supplier_address = " . $form->value("order_supplier_address") . " ".
           "where order_id = ". $project["project_order"];

    $res = mysql_query($sql) or dberror($sql);
    
    redirect ("project_edit_project_sheet_2004.php?pid=" . param("pid"));
}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Edit Project Sheet");
$form->render();
$page->footer();

?>