<?php
/********************************************************************

    project_view_traffic_data.php

    View Data concerning delivery and traffic

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-11-18
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-11-17
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_view_delivery_schedule_in_projects");

register_param("pid");
set_referer("project_edit_traffic_data_item.php");


/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());
$user_roles = get_user_roles(user_id());

// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

// create sql for oder items
$sql_order_items = "select order_item_id, " .
				   "    order_item_id, order_item_text, order_item_quantity, order_item_order,".
                   "    order_item_po_number, order_item_shipment_code, ".
                   "    if(order_item_item <>'', item_code, item_type_name) as item_shortcut, ".
                   "    concat(order_item_quantity, ' ', if(order_item_item <>'', item_code, item_type_name), '\n', if(order_item_po_number <>'', order_item_po_number, ''), '\n', if(order_item_shipment_code <>'', order_item_shipment_code, '')) as item_po, " .
                   "    addresses.address_company as forwarder_company, ".
                   "    addresses_1.address_company as supplier_company ".
				   "from order_items ".
                   "left join items on order_item_item = item_id ".
                   "left join orders on order_item_order = order_id ".
                   "left join projects on order_id = project_order ".
                   "left join item_types on order_item_type = item_type_id ".
                   "left join addresses on order_item_forwarder_address = addresses.address_id ".
                   "left join addresses AS addresses_1 on order_item_supplier_address = addresses_1.address_id ";


// get order_item_dates
 $sql_order_item_dates = $sql_order_items . " " .
                         "where (order_item_type = " . ITEM_TYPE_STANDARD .
                         "   or order_item_type = " . ITEM_TYPE_SPECIAL . " ) ".
                         "   and order_item_order = " . $project["project_order"] . " " .
                         "order by forwarder_company, order_item_po_number, order_item_type, item_shortcut";


$dates = get_order_dates_from_dates($sql_order_item_dates);

$dates1 = array();
foreach($dates["EXRP"] as $key=>$value)
{
     $dates1[$key] = $value . "\n" . $dates["PICK"][$key];
}

$dates2 = array();
foreach($dates["EXAR"] as $key=>$value)
{
     $dates2[$key] = $value . "\n" . $dates["ACAR"][$key];
}

// get item warehouse addresses
$warehouse_addresses = get_order_adresses($project["project_order"], 4);

//get item delivery addresses
$delivery_addresses = get_order_adresses($project["project_order"], 2);

/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

$form->add_hidden("pid", param("pid"));

require_once "include/project_head_small.php";




// Notify address
if(!in_array(5, $user_roles) and !in_array(29, $user_roles)) // supplier or warehouse
{
	
	$billing_address_province_name = "";
	if($project["order_billing_address_place_id"])
	{
		$billing_address_province_name = get_province_name($project["order_billing_address_place_id"]);
	}

	$form->add_section("Notify Address");
	$form->add_label("billing_address_company", "Company", 0, $project["order_billing_address_company"]);

	if ($project["order_billing_address_company2"])
	{
		$form->add_label("billing_address_company2", "", 0, $project["order_billing_address_company2"]);
	}

	$form->add_label("billing_address_address", "Address", 0, $project["order_billing_address_address"]);

	if ($project["order_billing_address_address2"])
	{
		$form->add_label("billing_address_address2", "", 0, $project["order_billing_address_address2"]);
	}
	

	$form->add_label("billing_address_place", "City", 0, $project["order_billing_address_zip"] . " " . $project["order_billing_address_place"]);

	$form->add_label("billing_address_province_name", "Province", 0, $billing_address_province_name);

	$form->add_label("billing_address_country", "Country", 0, $project["order_billing_address_country_name"]);

	$form->add_label("billing_address_phone", "Phone", 0, $project["order_billing_address_phone"]);
	$form->add_label("billing_address_fax", "Fax", 0, $project["order_billing_address_fax"]);
	$form->add_label("billing_address_email", "Email", 0, $project["order_billing_address_email"]);
}

$form->add_section("Preferences and Traffic Checklist");
$form->add_label("preferred_delivery_date", "Preferred Arrival Date", 0, to_system_date($project["order_preferred_delivery_date"]));


$form->add_lookup("preferred_transportation_arranged", "Transportation arranged by", "transportation_types", "transportation_type_name", 0, $project["order_preferred_transportation_arranged"]);

$form->add_lookup("preferred_transportation_mode", "Transportation mode", "transportation_types", "transportation_type_name", 0, $project["order_preferred_transportation_mode"]);

/*
$value="no";
if ($project["order_packaging_retraction"])
{
    $value="yes";
}
$form->add_label("order_packaging_retraction", "Packaging Retraction Desired", 0, $value);
*/

$value="no";
if ($project["order_pedestrian_mall_approval"])
{
    $value="yes";
}
$form->add_label("pedestrian_mall_approval", "Pedestrian Area Approval Needed", 0, $value);

$value="no";
if ($project["order_full_delivery"])
{
    $value="yes";
}
$form->add_label("full_delivery", "Full Delivery Desired", 0, $value);

$form->add_label("delivery_comments", "Delivery Comments", 0, $project["order_delivery_comments"]);

$form->add_section("Insurance");
if($project["order_insurance"] == 1)
{
	$form->add_label("order_insurance", "Insurance by " . BRAND . "/Forwarder", 0, "covered");
}
else
{
	$form->add_label("order_insurance", "Insurance by " . BRAND . "/Forwarder", 0, "not covered");
}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Create Item List
*********************************************************************/ 
$list = new ListView($sql_order_items, LIST_HAS_SEPARATOR | LIST_HAS_HEADER);

$list->set_title("Items up to Delivery");
$list->set_entity("order_item");

if(isset($_GET["s"]))
{
	$list->set_group("supplier_company");
	$list->set_order("supplier_company");
}
else
{
	$list->set_group("forwarder_company");
	$list->set_order("forwarder_company");
}

if (has_access("has_access_to_all_traffic_data_in_projects") 
    or in_array(15, $user_roles)
	or in_array(16, $user_roles)
	or in_array(33, $user_roles))
{
    $list->set_filter("(order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
                      "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
                      "   and order_item_order = " . $project["project_order"] .
                      "   and order_item_forwarder_address > 0 ");
}
else
{
	 $list->set_filter("(order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
                       "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
                       "   and order_item_order = " . $project["project_order"] .
                       "   and (order_item_supplier_address = " . $user_data["address"] . " " .
                       "   or order_item_forwarder_address = " . $user_data["address"] . " " .
                       "   or order_retail_operator = " . user_id() . " " .
                       "   or project_retail_coordinator = " . user_id()  . " " .
                       "   or project_design_contractor = " . user_id()  . " " .
                       "   or project_design_supervisor = " . user_id()  . " " .
                       "   or order_client_address = " . $user_data["address"] . ") ");

}


$list->add_column("item_po", "Item Code\nP.O. Number\nShipment Code", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK);

$list->add_text_column("dates_1", "Ready 4 Pickup\nPickup Date", COLUMN_NO_WRAP | COLUMN_BREAK, $dates1);
$list->add_text_column("dates_2", "Expected Arrival\nArrival", COLUMN_NO_WRAP | COLUMN_BREAK, $dates2);

$list->add_text_column("pickup_address", "Supplier's Warehouse\nPick Up Address\nContact\nPhone", COLUMN_NO_WRAP | COLUMN_BREAK , $warehouse_addresses);
$list->add_text_column("delivery_address", "Delivery Address\nContact\nPhone", COLUMN_NO_WRAP | COLUMN_BREAK , $delivery_addresses);

if(isset($_GET["s"]))
{
	$list->add_button("gbyf", "Group List by Forwarder");
}
else
{
	$list->add_button("gbys", "Group List by Supplier");
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();


if ($list->button("gbys"))
{
     $link = "project_view_traffic_data.php?pid=" . param("pid") . "&s=1"; 
     redirect($link);
}
elseif ($list->button("gbyf"))
{
     $link = "project_view_traffic_data.php?pid=" . param("pid"); 
     redirect($link);
}


/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("View Delivery Schedule");
$form->render();
$list->render();

$page->footer();

?>