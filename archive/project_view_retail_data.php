<?php
/********************************************************************

    project_view_retail_data.php

    View Retail Assignements.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-08
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-12-04
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/order_state_constants.php";

if(!has_access("can_view_client_data_in_projects"))
{
	check_access("can_edit_retail_data");
}

register_param("pid");


/********************************************************************
    prepare all data needed
*********************************************************************/

// read project and order details
$project = get_project(param("pid"));
$order_state_name = get_actual_order_state_name($project["order_actual_order_state_code"], 1);

// get Action parameter
$action_parameter_rto = get_action_parameter(RETAIL_OPERATOR_ASSIGNED, 1);
$action_parameter_rtc = get_action_parameter(RETAIL_COORDINATOR_ASSIGNED, 1);

// get company's address
$client_address = get_address($project["order_client_address"]);

/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

$form->add_section("Project");
$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid", $project["project_order"]);

//show project information
$form->add_label("project_number", "Project Number", 0, $project["project_number"]);
$form->add_lookup("product_line", "Product Line", "product_lines", "product_line_name", 0, $project["project_product_line"]);
$form->add_lookup("product_line_subclass", "Product Line Subclass", "productline_subclasses", "productline_subclass_name", 0, $project["project_product_line_subclass"]);
$form->add_lookup("project_postype", "POS Type", "postypes", "postype_name", 0, $project["project_postype"]);
$form->add_lookup("project_pos_subclass", "POS Type Subclass", "possubclasses", "possubclass_name", NOTNULL, $project["project_pos_subclass"]);

$form->add_label("type3", "Project Legal Type", 0, $project["project_costtype_text"]);
$form->add_label("type2", "Project Kind", 0, $project["projectkind_name"]);

$form->add_label("order_date", "Project Starting Date", 0, to_system_date($project["order_date"]));
$form->add_label("planned_opening_date", "Client's preferred Shop Opening Date", 0, to_system_date($project["project_planned_opening_date"]));
$form->add_label("real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]));

$form->add_label("actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]));


$form->add_label("status", "Status", 0, $project["order_actual_order_state_code"]  . " " . $order_state_name);

$client = $client_address["company"] . ", " .
          $client_address["zip"] . " " . $client_address["place"] . ", " .
          $client_address["country_name"];




$form->add_label("client_address", "Client", 0, $client);


$franchisee = $project["order_franchisee_address_company"] . ", " .
        $project["order_franchisee_address_zip"] . " " .
        $project["order_franchisee_address_place"] . ", " .
        $project["order_franchisee_address_country_name"];

if($project["project_cost_type"] != 6)
{
	$form->add_label("franchisee_address", "Franchisee", 0, $franchisee);		
}
else
{
	$form->add_label("franchisee_address", "Owner Company", 0, $franchisee);	
}

$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];
        
$form->add_label("shop_address", "POS Location Address", 0, $shop);


$form->add_label("shop_address", "POS Location Address", 0, $shop);


$form->add_section("Project Management");
$line = "concat(user_name, ' ', user_firstname)";
if ($project["project_retail_coordinator"])
{
    $form->add_lookup("retail_coordinator", "Project Manager", "users", $line, 0, $project["project_retail_coordinator"]);
}
else
{
    $form->add_label("project_retail_coordinator", "Project Manager");
}

$line = "concat(user_name, ' ', user_firstname)";
if ($project["project_local_retail_coordinator"])
{
    $form->add_lookup("local_retail_coordinator", "Local Project Manager", "users", $line, 0, $project["project_local_retail_coordinator"]);
}
else
{
    $form->add_label("project_local_retail_coordinator", "Local Project Manager");
}

/*
if ($project["project_hq_project_manager"])
{
    $form->add_lookup("hq_project_manager", "HQ Project Manager", "users", $line, 0, $project["project_hq_project_manager"]);
}
else
{
    $form->add_label("hq_project_manager", "HQ Project Manager");
}
*/

if ($project["order_retail_operator"])
{
    $form->add_lookup("retail_operator", "Retail Operator", "users", $line, 0, $project["order_retail_operator"]);
}
else
{
    $form->add_label("order_retail_operator", "Retail Operator");
}


$form->add_section("Design Staff");
if ($project["project_design_contractor"])
{
    $form->add_lookup("contractor", "Design Contractor", "users", $line, 0, $project["project_design_contractor"]);
}
else
{
    $form->add_label("order_retail_operator", "Design Contractor");
}

if ($project["project_design_supervisor"])
{
    $form->add_lookup("design_supervisor", "Design Supervisor", "users", $line, 0, $project["project_design_supervisor"]);
}
else
{
    $form->add_label("design_supervisor", "Design Supervisor");
}


$form->add_section("Confirmation of Delivery by");
$form->add_lookup("delivery_confirmation_by", "Person", "users", $line, 0, $project["order_delivery_confirmation_by"]);

if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
{
	$form->add_section("Project Dates");
}
else
{
	$form->add_section("POS Opening Date");
}

$form->add_label("project_real_opening_date", $project["projectkind_milestone_name_01"], NOTNULL, to_system_date($project["project_real_opening_date"]));

$form->add_section("Miscellanous");
$form->add_checkbox("project_no_planning", "project does not need architectural planning", $project["project_no_planning"], 0, "Planning");
$form->add_checkbox("project_is_local_production", "project is locally realized (local production)", $project["project_is_local_production"], 0, "Local Production");
$form->add_checkbox("project_is_special_production", "project is a special individual project", $project["project_is_special_production"], 0, "Special Project");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

    
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("View Retail Data");
$form->render();
$page->footer();

?>