<?php
/********************************************************************

    projects_archive.php

    Archive: Selection of the year.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-11-22
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-12-03
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("has_access_to_archive");

set_referer("projects_archive_projects.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());


$country_filter = "";
$tmp = array();
$sql = "select * from country_access " .
	   "where country_access_user = " . user_id();


$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{            
	$tmp[] = $row["country_access_country"];
}

if(count($tmp) > 0) {
	$country_filter = " country_id IN (" . implode(",", $tmp) . ") ";
}


$sql_years = "select distinct left(order_date, 4) as year " .
              "from orders ".
              "where order_type = 1 " .
              "    and order_archive_date <> 0 ".
              "    and order_archive_date is not null " .
              "order by year";


// create sql for product lines
if (has_access("has_access_to_all_projects"))
{
    $sql_product_lines = "select distinct product_line_id, product_line_name ".
                         "from projects ".
                         "left join product_lines on  project_product_line = product_line_id ".
                         "left join orders on project_order = order_id ".
                         "where order_archive_date is not null ".
                         "    and order_archive_date <> '0000-00-00' " .
                         " order by product_line_name";
}
else
{
    $sql_product_lines = "select distinct product_line_id, product_line_name ".
                         "from projects ".
                         "left join product_lines on  project_product_line = product_line_id ".
                         "left join orders on project_order = order_id ".
		                 "inner join project_costs on project_cost_order = order_id " .
                         "left join addresses on order_client_address = address_id ".
                         "left join order_items on order_item_order = order_id ".
		                 "left join countries on order_shop_address_country = countries.country_id ".
                         "where order_archive_date is not null ".
                         "    and order_archive_date <> '0000-00-00' " .
                         "    and (";
    
    $filter_tmp = "(order_item_supplier_address = " . $user_data["address"] . " " .
                  "    or order_item_forwarder_address = " . $user_data["address"] . " " .
                  "    or order_retail_operator = " . user_id() . " " .
                  "    or project_retail_coordinator = " . user_id()  . " " .
                  "    or project_design_contractor = " . user_id()  . " " .
                  "    or project_design_supervisor = " . user_id()   . " " .
                  "    or order_client_address = " . $user_data["address"] . ") ";
   
    
	if(has_access("has_access_to_retail_only")) {
		$filter_tmp .= " and project_cost_type in (1) ";
	}

	if(has_access("has_access_to_wholesale")) {
		$filter_tmp .= " and project_cost_type in (2, 6) ";
	}

	if($country_filter == "")
	{
		$sql_product_lines = $sql_product_lines . $filter_tmp . ") order by product_line_name";
	}
	else
	{
		$sql_product_lines = $sql_product_lines . $filter_tmp . ") or (" . $country_filter .") order by product_line_name";
		//$sql_countries = $sql_countries . $filter_tmp . ") or (" . $country_filter . ") order by country_name";
	}
}

// create sql for country list
if (has_access("has_access_to_all_projects"))
{
    $sql_countries = "select distinct country_id, country_name ".
                     "from projects ".
                     "left join orders on project_order = order_id ".
                     "left join countries on order_shop_address_country = countries.country_id ".
                     "where order_archive_date is not null ".
                     "    and order_archive_date <> '0000-00-00' " .
		             " and order_type = 1 " . 
                     "order by country_name";
}
else
{
    $sql_countries = "select distinct country_id, country_name ".
                     "from projects ".
                     "left join orders on project_order = order_id ".
                     "left join countries on order_shop_address_country = countries.country_id ".
                     "left join order_items on order_item_order = order_id ".
		             "inner join project_costs on project_cost_order = order_id " .
                     "where order_archive_date is not null ".
                     "    and order_archive_date <> '0000-00-00' " .
		             " and order_type = 1 " . 
                     "    and (";
    
    $filter_tmp = "(order_item_supplier_address = " . $user_data["address"] . " " .
                  "    or order_item_forwarder_address = " . $user_data["address"] . " " .
                  "    or order_retail_operator = " . user_id() . " " .
                  "    or project_retail_coordinator = " . user_id()  . " " .
                  "    or project_design_contractor = " . user_id()  . " " .
                  "    or project_design_supervisor = " . user_id()   . " " .
                  "    or order_client_address = " . $user_data["address"] . ") ";


	if(has_access("has_access_to_retail_only")) {
		$filter_tmp .= " and project_cost_type in (1) ";
	}

	if(has_access("has_access_to_wholesale")) {
		$filter_tmp .= " and project_cost_type in (2, 6) ";
	}

	if($country_filter == "")
	{
		$sql_countries = $sql_countries . $filter_tmp . ") order by country_name";
	}
	else
	{
		$sql_countries = $sql_countries . $filter_tmp . ") or (" . $country_filter . ") order by country_name";
	}
   
    
}


/********************************************************************
    build form
*********************************************************************/
$form = new Form("orders", "orders");

$form->add_section("Range Selection");
$form->add_comment("Please select a range of years.");
$form->add_list("from_year", "From Year", $sql_years, 0);
$form->add_list("to_year", "To Year", $sql_years, 0);
$form->add_section(" ");
$form->add_list("productline", "Product Line", $sql_product_lines, 0);
$form->add_list("country", "Country", $sql_countries, 0);
$form->add_edit("projectnumber", "Project Number");

$form->add_button("proceed", "Show Projects");



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("proceed"))
{
    $y1 = $form->value("from_year");
    $y2 = $form->value("to_year");
    $p = $form->value("productline");
    $c = $form->value("country");
	$pn = $form->value("projectnumber");

    $link = "projects_archive_projects.php?y1=". $y1 . "&y2=" . $y2 . "&p=" . $p . "&c=" . $c . "&pn=" . $pn;
    redirect($link);
}


   
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

//$page->register_action('orders', 'Orders', "orders_archive.php");
$page->register_action('projects', 'Projects', "projects_archive.php");
$page->register_action('welcome', 'Quit Archive', "../user/welcome.php");


$page->header();
$page->title("Projects: Archive");
$form->render();

?>

<script type="text/javascript">
    
	
	var selectedInput = null;
	$(document).ready(function(){
	  $("#projectnumber").focus();

	  $('input').focus(function() {
				selectedInput = this;
			});

	});

	document.onkeydown = process_key;
	
	function process_key(e)
	{
	  if( !e ) 
	  {
		if( window.event ) 
		{
		  e = window.event;
		} 
		else 
		{
		  return;
		}
	  }

	  if(selectedInput.name == "projectnumber" && e.keyCode==13)
	  {
		button('proceed');
	  }
	}
</script>

<?php
$page->footer();

?>