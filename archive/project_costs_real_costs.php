<?php
/********************************************************************

    project_costs_real_costs.php

    View or edit the real costs of a project

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-01-09
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2012, OMEGA SA, All Rights Reserved.

*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/project_cost_functions.php";
require_once "include/save_functions.php";

if(!has_access("can_view_budget_in_projects") and !has_access("can_edit_list_of_materials_in_projects"))
{
	$link = "project_task_center.php?pid=" . param("pid");
}


/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));
// get company's address
$client_address = get_address($project["order_client_address"]);

$currency_symbol = get_currency_symbol($project["order_client_currency"]);


/********************************************************************
    check if project cost positions are present and create if not
*********************************************************************/

$sql = "select count(costsheet_id) as num_recs from costsheets " . 
       " where costsheet_project_id = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

if($row["num_recs"] == 0) //make the user select a cost template
{
	$link = "project_costs_overview.php?pid=" . param("pid");
	redirect($link);
}

/********************************************************************
    get budget data from cost sheet
*********************************************************************/

$result = copy_approved_ammounts(param("pid"));
$code_data = array();
$currency_data = array();
$comment_data = array();
$real_data = array();
$sg_approved_data = array();


$sql = "select costsheet_id, costsheet_code, costsheet_real_amount, costsheet_sg_approved_amount, costsheet_comment2 " . 
       "from costsheets " . 
	   "where costsheet_project_id = " . param("pid");


$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$currency_data[$row["costsheet_id"]] = $currency_symbol;
	$comment_data[$row["costsheet_id"]] = $row["costsheet_comment2"];
	$real_data[$row["costsheet_id"]] = $row["costsheet_real_amount"];
	$code_data[$row["costsheet_id"]] = $row["costsheet_code"];
	$sg_approved_data[$row["costsheet_id"]] = $row["costsheet_sg_approved_amount"];
}




//get cms approval person coordinator
$approval_person_name = "";
$sql = "select user_id, concat(user_name, ' ' , user_firstname) as uname " .
       "from users " . 
	   "where user_id = " . dbquote($project["project_cms_approver"]);

$res = mysql_query($sql) or dberror($sql);

if($row = mysql_fetch_assoc($res))
{
	$approval_person_name = $row["uname"];
}

//get cms completion person coordinator
$person_to_complete_cms_name = "";
$sql = "select user_id, concat(user_name, ' ' , user_firstname) as uname " .
       "from users " . 
	   "where user_id = " . dbquote($project["order_retail_operator"]);

$res = mysql_query($sql) or dberror($sql);

if($row = mysql_fetch_assoc($res))
{
	$person_to_complete_cms_name = $row["uname"];
}


//get status data
$sql =  "select * from project_costs " .
        "where project_cost_order = " . $project["project_order"];

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

$cms_approved = $row["project_cost_cms_approved"];
$cms_approved_by = $row["project_cost_cms_approved_by"];
$cms_approved_date = $row["project_cost_cms_approved_date"];

$cms_completed = $row["project_cost_cms_completed"];
$cms_completed_by = $row["project_cost_cms_completed_by"];
$cms_completion_date = $row["project_cost_cms_completion_date"];

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("comments", "comment");


$form->add_section("Project");
$form->add_hidden("pid", param('pid'));
$form->add_hidden("order_id", $project["project_order"]);


require_once "include/project_head_small.php";



$form->add_section("CMS Completion");
if(has_access("can_complete_cms") and $cms_completed == 0)
{
	$form->add_comment("Please approve this CMS after you have completed the CMS.");
	$form->add_label("completor1", "Person to complete the CMS", 0, $person_to_complete_cms_name);
	$form->add_checkbox("completionstate", "completed", $cms_completed, 0, "Completion State");
	$form->add_lookup("completor2", "Person having approved", "users", "concat(user_name, ' ', user_firstname)", 0, dbquote($cms_completed_by));
	$form->add_label("completiondate", "Date of completion", 0, to_system_date($cms_completion_date));
}
else
{
	$form->add_comment("The CMS was completed as follows:");
	$form->add_label("cmsrto", "Person to complete the CMS", 0, $person_to_complete_cms_name);
	if($cms_completed == 1)
	{
		$form->add_label("completionstate", "Completions State", 0, "completed");
	}
	else
	{
		$form->add_label("approvalstate", "Completions State", 0, "not completed");
	}
	$form->add_lookup("completor2", "Person having completed", "users", "concat(user_name, ' ', user_firstname)", 0, dbquote($cms_completed_by));
	$form->add_label("completiondate", "Date of approval", 0, to_system_date($cms_completion_date));
}

$form->add_section("Approval");
if(has_access("can_approve_cms") and $cms_completed == 1 and $cms_approved == 0)
{
	$form->add_comment("Please approve this CMS after you have checked that everything is ok.");
	$form->add_label("approver1", "Person to approve", 0, $approval_person_name);
	$form->add_checkbox("approvalstate", "approved", $cms_approved, 0, "Approval State");
	
	$form->add_lookup("approver2", "Person having approved", "users", "concat(user_name, ' ', user_firstname)", 0, dbquote($cms_approved_by));
	$form->add_label("approvaldate", "Date of approval", 0, to_system_date($cms_approved_date));

}
else
{
	$form->add_comment("The CMS was approved as follows:");
	$form->add_label("approver1", "Person to approve", 0, $approval_person_name);
	if($cms_approved == 1)
	{
		$form->add_label("approvalstate", "Approval State", 0, "approved");
	}
	else
	{
		$form->add_label("approvalstate", "Approval State", 0, "not approved");
	}
	$form->add_lookup("approver2", "Person having approved", "users", "concat(user_name, ' ', user_firstname)", 0, dbquote($cms_approved_by));
	$form->add_label("approvaldate", "Date of approval", 0, to_system_date($cms_approved_date));

}



$link = "javascript:popup('/user/project_costs_cms_pdf.php?pid=" . param("pid") . "', 800, 600);";
$form->add_button("print_cms", "Print Cost Monitoring Sheet", $link);
$form->add_button("save_form", "Save");

$form->add_button("update_from_budget", "Update Real Costs from Budget");

$form->populate();
$form->process();

/********************************************************************
    Process form
*********************************************************************/ 

if ($form->button("save_form"))
{
    
    if ($form->validate())
    {
		$approval_fields = "";

		if($form->value("approvalstate") == 1 or $form->value("approvalstate") == 'approved') //CMS was approved
		{
			$approval_fields = "project_cost_cms_approved = 1, ";
			$approval_fields .= "project_cost_cms_approved_by = '" . user_id() . "', ";
			$approval_fields .= "project_cost_cms_approved_date = '" . date("Y.m.d") . "', ";

			//put the project to the records
			
			if($project["project_actual_opening_date"] != NULL and $project["project_actual_opening_date"] != '0000-00-00')
			{
				if($project["order_actual_order_state_code"] < '820')
				{
					append_order_state($project["project_order"], '820', 1, 1);
					set_archive_date($project["project_order"]);
				}
			}

			if($form->value("approvalstate") == 1) //CMS was approved
			{
				// send email notification for project types
				$sql = "select project_number, project_postype, project_product_line, " .
					   "order_shop_address_company, order_shop_address_place, country_name, " .
					   "user_firstname, user_name, user_email " . 
					   "from projects " . 
					   "left join orders on order_id = project_order " .
					   "left join countries on country_id = order_shop_address_country " .
					   "left join users on user_id =  " . user_id() .
					   "   where project_id = " . $project["project_id"];

				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{        
					
					// send mail to cost manager
					$recipients = array();
					$project_postype = $row["project_postype"];
					$project_product_line = $row["project_product_line"];
					
					$subject = MAIL_SUBJECT_PREFIX . ": Project was put to the records - Project " . $row["project_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];

					$sender_email = $row["user_email"];
					$sender_name =  $row["user_name"] . " " . $row["user_firstname"];

					$bodytext0 = "The project was put to the records by by " . $sender_name;

					$mailaddress = 0;

					$mail = new Mail();
					$mail->set_subject($subject);
					$mail->set_sender($sender_email, $sender_name);

					$sql = "select * from postype_notifications " .
						   "where postype_notification_postype = " . $project_postype . 
						   " and postype_notification_prodcut_line = " . $project_product_line;

					$res = mysql_query($sql) or dberror($sql);
					while ($row = mysql_fetch_assoc($res))
					{
						
						if($row["postype_notification_email3"])
						{
							$sql_u = "select user_id from users " .
									 "where user_email = '" . $row["postype_notification_email3"] .  "'";
							
							$res_u = mysql_query($sql_u) or dberror($sql_u);
							
							if($row_u = mysql_fetch_assoc($res_u))
							{
								$recepient_id = $row_u["user_id"];
								$recipients[] = $row_u["user_id"];
							}
							else
							{
								$recepient_id = '0';
							}

							$mail->add_recipient($row["postype_notification_email3"]);
							
							$mailaddress = 1;
						}
					}
					
					$link ="project_view_client_data.php?pid=" . $project["project_id"];
					$bodytext = $bodytext0 . "\nclick below to have direct access to the project:\n";
					$bodytext = $bodytext .  APPLICATION_URL . "/archive/" . $link . "\n\n";           
					$mail->add_text($bodytext);
					
					if($mailaddress == 1)
					{
						$mail->send();
						foreach($recipients as $key=>$value)
						{
							append_mail($project["project_order"], $value, user_id(), $bodytext, MOVED_TO_THE_RECORDS, 1);
						}
						
					}
				}
			}

			//delete all tasks for this user and this project
			$sql = "delete from tasks where task_order = " . dbquote($project["project_order"]) . 
				   " and task_user = " . dbquote($project["project_cms_approver"]);

			$result = mysql_query($sql) or dberror($sql);

		}
		

		if($form->value("completionstate") == 1) //CMS was completed
		{
			
			$approval_fields = "project_cost_cms_completed = 1, ";
			$approval_fields .= "project_cost_cms_completed_by = '" . user_id() . "', ";
			$approval_fields .= "project_cost_cms_completion_date = '" . date("Y.m.d") . "', ";

			//send mail to cms approval person if actual opening date was entered
			$sql = "select * from users " .
				   "where user_id = " . dbquote($project["project_cms_approver"]);
			
			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				$recipient = $row["user_email"];
				$reciepient_user_id = $row["user_id"];

				$sql = "select user_id, user_email, concat(user_name, ' ', user_firstname) as username ".
					   "from users ".
					   "where (user_id = '" . user_id() . "' " . 
					   "   and user_active = 1)";
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				$sender_id = $row["user_id"];
				$sender_email = $row["user_email"];
				$sender_name = $row["username"];
				

				$subject = "CMS approval needed - Project " . $project["order_number"] . ", " . $project["order_shop_address_country_name"] . ", " . $project["order_shop_address_company"];

				$mail = new Mail();
				$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
				$mail->set_sender($sender_email, $sender_name);
				$mail->add_recipient($recipient);

				$bodytext0 = $sender_name . " has confirmed the completion of the cost monitoring sheet for the project. Please check and approve the Cost Monitoring Sheet.";
				$link ="project_edit_cost_monitoring.php?pid=" . param("pid");
				$bodytext = $bodytext0 . "\nClick below to have direct access to the project:\n";
				$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
				$mail->add_text($bodytext);
				
				$result = $mail->send();

				append_mail($project["project_order"], $reciepient_user_id, user_id(), $bodytext0, "910", 1);
				append_task($project["project_order"], $reciepient_user_id, $bodytext0, $link, "",  user_id(), '910', 1);

			}
		}
		

        
		if($approval_fields)
		{
			$sql = "update project_costs set " .
				   substr($approval_fields, 0, strlen($approval_fields)- 2) . 
				   " where project_cost_order = " . $project["project_order"];
		
		}
		$result = mysql_query($sql) or dberror($sql);
		
		
		//$link = "project_costs_real_costs.php?pid=" . param("pid"); 
		//redirect($link);
    }
}
elseif ($form->button("update_from_budget")) {

	$sql = "update costsheets set costsheet_real_amount = costsheet_budget_amount " . 
		   " where costsheet_project_id = " . param("pid") . 
		   " and costsheet_real_amount = 0 ";
	$result = mysql_query($sql) or dberror($sql);
	
	$form->message("The real cost were update from the budget.");

	$link = "project_costs_real_costs.php?pid=" . param("pid");
	redirect($link);

}



/********************************************************************
    Compose Cost Sheet
*********************************************************************/ 
$budget = get_project_budget_totals(param("pid"));

//add all cost groups and cost sub groups

$list_names = array();
$save_button_names = array();
$add_button_names = array();
$remove_button_names = array();
$group_ids = array();
$group_titles = array();

$sql = "select DISTINCT costsheet_pcost_group_id, pcost_group_code, pcost_group_name " .
	   "from costsheets " .
	   "left join pcost_groups on pcost_group_id = costsheet_pcost_group_id " . 
	   "order by pcost_group_code";


$sql2 = "select DISTINCT costsheet_pcost_group_id, pcost_group_code, pcost_group_name " .
	   "from costsheets " .
	   "left join pcost_groups on pcost_group_id = costsheet_pcost_group_id " .
	   " where costsheet_project_id = " . param("pid") . " and costsheet_is_in_cms = 1" .
	   " order by pcost_group_code";



$res = mysql_query($sql2) or dberror($sql2);
while ($row = mysql_fetch_assoc($res))
{
	$listname = "list" . $row["pcost_group_code"];
	$save_buttonname = "save_cost" . $row["pcost_group_code"];
	$add_buttonname = "add_new" . $row["pcost_group_code"];
	$remove_buttonname = "remove_cost" . $row["pcost_group_code"];
	$list_names[] = $listname;
	$save_button_names[] = $save_buttonname;
	$add_button_names[] = $add_buttonname;
	$remove_button_names[] = $remove_buttonname;
	$group_ids[] = $row["costsheet_pcost_group_id"];
	$group_titles[] = $row["pcost_group_name"];
	
	$sql = "select costsheet_id, costsheet_code, costsheet_text, costsheet_budget_amount, " . 
		   "costsheet_budget_approved_amount, costsheet_comment2, " .
		   "concat(pcost_subgroup_code, ' ', pcost_subgroup_name) as subgroup  " . 
		   "from costsheets " .
		   " left join pcost_subgroups on pcost_subgroup_id = costsheet_pcost_subgroup_id"; 
	
	$list_filter = "costsheet_project_id = " . param("pid") . " and costsheet_pcost_group_id = " . $row["costsheet_pcost_group_id"] . " and costsheet_is_in_cms = 1";


	$toggler = '<div class="toggler_pointer" id="l' . $row["costsheet_pcost_group_id"] . '_on"><span class="fa fa-minus-square toggler"></span>' .$row["pcost_group_name"] . '</div>';
	
	$$listname = new ListView($sql, LIST_HAS_HEADER | LIST_HAS_FOOTER);

	$$listname->set_entity("costsheets");
	$$listname->set_order("LENGTH(costsheet_code), COALESCE(costsheet_code,'Z')");
	$$listname->set_group("subgroup");
	$$listname->set_filter($list_filter);
	$$listname->set_title($toggler);

	$$listname->add_edit_column("costsheet_code", "Code", 4, 0, $code_data);

	$$listname->add_column("costsheet_budget_amount", "Budget", "", "", "", COLUMN_ALIGN_RIGHT);
	$$listname->add_column("costsheet_budget_approved_amount", "Approved", "", "", "", COLUMN_ALIGN_RIGHT);
	$$listname->add_edit_column("costsheet_sg_approved_amount", "SG Approved", 12, 0, $sg_approved_data);
	$$listname->add_edit_column("costsheet_real_amount", "Real Costs", 12, 0, $real_data);
	$$listname->add_text_column("currency", "", 0, $currency_data);

	$$listname->add_edit_column("costsheet_comment2", "Comment", 30, 0, $comment_data, 'textarea', 3);


	$$listname->add_column("costsheet_text", "Text");

	foreach($budget["subgroup_totals"] as $subgroup=>$budget_sub_group_total)
	{
		$$listname->set_group_footer("costsheet_budget_amount",  $subgroup , number_format($budget_sub_group_total, 2));
	}

	
	foreach($budget["subgroup_approved_totals"] as $subgroup=>$budget_sub_group_total)
	{
		$$listname->set_group_footer("costsheet_budget_approved_amount",  $subgroup , number_format($budget_sub_group_total, 2));
	}
		

	$$listname->set_footer("costsheet_budget_amount", number_format($budget["group_totals"][$row["costsheet_pcost_group_id"]], 2));
	$$listname->set_footer("costsheet_budget_approved_amount", number_format($budget["group_approved_totals"][$row["costsheet_pcost_group_id"]], 2));
	
	
	
	$link = "javascript:add_new_cost_position(" . param("pid") .", " . $row["costsheet_pcost_group_id"] .")";
	$$listname->add_button($add_buttonname, "Add More Cost Positions", $link);
	$$listname->add_button($remove_buttonname, "Remove Empty Positions");

	$$listname->add_button($save_buttonname, "Save List");

	$$listname->populate();
	$$listname->process();
}


foreach($save_button_names as $key=>$buttonname)
{
	if($form->button($buttonname) or $form->button("save_form"))
	{
		$list = "";
		foreach($list_names as $key=>$listname)
		{
			$list = $listname;
		}

		
		foreach ($$list->values("costsheet_code") as $key=>$value)
		{
			$fields = array();

			$fields[] = "costsheet_code = " . dbquote($value);

			$sql = "update costsheets set " . join(", ", $fields) . " where costsheet_id = " . $key;
			mysql_query($sql) or dberror($sql);
		}

		foreach ($$list->values("costsheet_real_amount") as $key=>$value)
		{
			$value = trim(str_replace("'", "", $value));
			if(is_numeric($value))
			{
				$fields = array();

				$fields[] = "costsheet_real_amount = " . dbquote($value);

				$sql = "update costsheets set " . join(", ", $fields) . " where costsheet_id = " . $key;
				mysql_query($sql) or dberror($sql);
			}
			
		}


		foreach ($$list->values("costsheet_sg_approved_amount") as $key=>$value)
		{
			$value = trim(str_replace("'", "", $value));
			if(is_numeric($value))
			{
				$fields = array();

				$fields[] = "costsheet_sg_approved_amount = " . dbquote($value);

				$sql = "update costsheets set " . join(", ", $fields) . " where costsheet_id = " . $key;
				mysql_query($sql) or dberror($sql);
			}
			
		}

		foreach ($$list->values("costsheet_comment2") as $key=>$value)
		{
			$fields = array();

			$fields[] = "costsheet_comment2 = " . dbquote($value);
			$fields[] = "date_modified = " . dbquote(date("Y-m-d H:i:s"));
			$fields[] = "user_modified = " . dbquote(user_login());

			$sql = "update costsheets set " . join(", ", $fields) . " where costsheet_id = " . $key;
			mysql_query($sql) or dberror($sql);
			
		}

		$result = update_posorder_investments($project["project_id"], $project["project_order"]);
		$form->message("Your data has been saved.");
	}
}

foreach($remove_button_names as $key=>$buttonname)
{
	if($form->button($buttonname))
	{
		

		$sql = "delete from costsheets " .
		       "where (costsheet_text is null or costsheet_text = '') and costsheet_budget_amount = 0 " . 
			   "and costsheet_real_amount = 0 and costsheet_is_in_budget = 0 and costsheet_project_id = " . param("pid");

		mysql_query($sql) or dberror($sql);

		
		$form->message("The empty positions were removed.");
	}
}


$page = new Page("projects");


require "include/project_page_actions.php";


$page->header();
$page->title("Project Costs - Real Costs");

require_once("include/costsheet_tabs.php");
$form->render();

foreach($list_names as $key=>$listname)
{
	
	if(array_key_exists("costsheet", $_SESSION) and array_key_exists($listname, $_SESSION["costsheet"]))
	{
		if($_SESSION["costsheet"][$listname] == 0)
		{
			echo '<p>&nbsp;</p>';
			$toggler = '<div class="toggler_pointer" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
			echo $toggler;
			
			echo '<div style="display:none;" id="' . $listname . '">';
			$$listname->render();
			echo '</div>';
		}
		else
		{
			echo '<p>&nbsp;</p>';
			$toggler = '<div class="toggler_pointer toggler_pointer_off" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
			echo $toggler;
			
			echo '<div id="' . $listname . '">';
			$$listname->render();
			echo '</div>';
		}
	}
	else
	{
		echo '<p>&nbsp;</p>';
		$toggler = '<div class="toggler_pointer toggler_pointer_off" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
		echo $toggler;
		
		echo '<div id="' . $listname . '">';
		$$listname->render();
		echo '</div>';
	}
}



?>
<script language="javascript">
jQuery(document).ready(function($) {
	<?php
	foreach($list_names as $key=>$listname)
	{
	?>
		$('#l<?php echo $group_ids[$key];?>_on').click(function()  {
			$('#<?php echo $listname;?>').css('display', 'none');
			$('#l<?php echo $group_ids[$key];?>_off').css('display', 'block');

			$.ajax({
				type: "POST",
				data: "listname=<?php echo $listname;?>&visibility=0",
				url: "../shared/ajx_costsheet_liststaes.php",
				success: function(msg){
				}
			});
		});
		
		$('#l<?php echo $group_ids[$key];?>_off').click(function()  {
			$('#<?php echo $listname;?>').css('display', 'block');
			$('#l<?php echo $group_ids[$key];?>_off').css('display', 'none');

			$.ajax({
				type: "POST",
				data: "listname=<?php echo $listname;?>&visibility=1",
				url: "../shared/ajx_costsheet_liststaes.php",
				success: function(msg){
				}
			});
		});
	<?php
	}
	?>
});


function add_new_cost_position(pid, gid)
{		
	url = '/user/project_costs_add_positions.php?pid=' + pid + '&gid=' + gid + '&mode=cms';
	$.nyroModalManual({
	  url: url
	});

}


</script>

<?php

$page->footer();


?>