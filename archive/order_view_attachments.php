<?php
/********************************************************************

    order_view_attachments.php

    List of attachments of a project

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-26
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-12-03
    Version:        1.0.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_view_attachments_in_orders");

register_param("oid");


/********************************************************************
    prepare all data needed
*********************************************************************/
// read order details
$order = get_order(param("oid"));

// get company's address
$client_address = get_address($order["order_client_address"]);

// get user data
$user_data = get_user(user_id());
$user_roles = get_user_roles(user_id());

// build sql for attachment entries
$sql_attachment = "select distinct order_file_id, order_file_visited, ".
                  "    order_file_title, order_file_description, ".
                  "    order_file_path, file_type_name, ".
                  "    order_files.date_created, ".
                  "    order_file_category_name, order_file_category_priority, ".
                  "    concat(user_name, ' ', user_firstname) as owner_fullname ".
                  "from order_files ".
                  "left join order_file_categories on order_file_category_id = order_file_category ".
                  "left join users on user_id = order_file_owner ".
                  "left join file_types on order_file_type = file_type_id ";
                  

// build filter for the list of attachments
$list1_filter = "order_file_order = " . param('oid');


if (has_access("has_access_to_all_attachments_in_orders"))
{
	$sql_attachment = "select distinct order_file_id, order_file_visited, ".
                  "    order_file_title, order_file_description, ".
                  "    order_file_path, file_type_name, ".
                  "    order_files.date_created, ".
                  "    order_file_category_name, order_file_category_priority, ".
                  "    concat(user_name, ' ', user_firstname) as owner_fullname ".
                  "from order_files  " .
                  "left join order_file_categories on order_file_category_id = order_file_category ".
                  "left join users on user_id = order_file_owner ".
                  "left join file_types on order_file_type = file_type_id ";
}
else
{
	$file_ids = array();
	$sql_oder_addresses = 'select order_file_address_file from order_file_addresses ' .
		                  'where order_file_address_address = ' . $user_data["address"];

	$res = mysql_query($sql_oder_addresses) or dberror($sql_oder_addresses);
    while ($row = mysql_fetch_assoc($res)) {
		$file_ids[] = $row['order_file_address_file'];
	}
	
	$file_filter = "";
	if(count($file_ids) > 0) {
		$file_filter = ' or order_file_id IN (' . implode(',', $file_ids) . ')';
	}
	
	
	$sql_attachment = "select distinct order_file_id, order_file_visited, ".
                  "    order_file_title, order_file_description, ".
                  "    order_file_path, file_type_name, ".
                  "    order_files.date_created, ".
                  "    order_file_category_name, order_file_category_priority, ".
                  "    concat(user_name, ' ', user_firstname) as owner_fullname ".
                  "from order_files " . 
                  "left join order_file_categories on order_file_category_id = order_file_category ".
                  "left join users on user_id = order_file_owner ".
                  "left join file_types on order_file_type = file_type_id ";
	
	$list1_filter = $list1_filter . " and (order_file_owner = " . user_id() . $file_filter . ")";
}

$file_category_filter = "";
$categroy_restrictions = get_file_category_restirctions($user_roles);

if(count($categroy_restrictions) > 0)
{
	$file_category_filter = " and order_file_category IN (" . implode(",", $categroy_restrictions). ") ";
}

$list1_filter .=  $file_category_filter;

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("order_files", "order_file", 640);

$form->add_section("Order");
$form->add_hidden("oid", param('oid'));

require_once "include/order_head_small.php";


/********************************************************************
    Create List
*********************************************************************/ 

$list1 = new ListView($sql_attachment);

$list1->set_entity("order_files");
$list1->set_filter($list1_filter);
$list1->set_order("order_files.date_created DESC");
$list1->set_group("order_file_category_priority", "order_file_category_name");

$list1->add_column("date_created", "Date/Time", "", "", "", COLUMN_NO_WRAP);

$list1->add_column("owner_fullname", "Made by", "", "", "", COLUMN_NO_WRAP);
$link = "http://" . $_SERVER["HTTP_HOST"] . "/include/openfile.php?id={order_file_id}";
$list1->add_column("order_file_title", "Title", $link, "", "", COLUMN_NO_WRAP);
$list1->add_column("file_type_name", "Type", "", "", "", COLUMN_NO_WRAP);
$list1->add_column("order_file_description", "Description");


  if(in_array(1, $user_roles) or in_array(2, $user_roles) or in_array(3, $user_roles)) // Administrator and Retail Operator and Retail Coordinator
{
	if (has_access("can_add_attachments_in_orders"))
	{
		$list1->add_button("add_attachment", "Add Attachment");
	}    
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list1->populate();
$list1->process();


if ($list1->button("add_attachment"))
{
    $link = "order_add_attachment.php?oid=" . param("oid");
    redirect ($link);
}


$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("Attachments");
$form->render();
$list1->render();
$page->footer();

?>