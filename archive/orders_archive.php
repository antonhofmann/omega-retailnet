<?php
/********************************************************************

    orders_archive.php

    Archive: Selection of the year.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-11-22
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-12-03
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("has_access_to_archive");

set_referer("orders_archive_orders.php");

$user_data = get_user(user_id());

/********************************************************************
    prepare all data needed
*********************************************************************/

$sql_years = "select distinct left(order_date, 4) as year " .
              "from orders ".
              "where order_type = 2 " .
              "    and order_archive_date <> 0 ".
              "    and order_archive_date is not null " .
              "order by year";


// create sql for country list
if (has_access("has_access_to_all_orders"))
{
    $sql_countries = "select distinct country_id, country_name ".
                     "from orders ".
                     "left join countries on order_shop_address_country = countries.country_id ".
                     "where order_archive_date is not null ".
                     "    and order_archive_date <> '0000-00-00' " .
		             "   and  order_type = 2 ".
                     "order by country_name";
}
else
{
    $sql_countries = "select distinct country_id, country_name ".
                     "from orders ".
                     "left join countries on order_shop_address_country = countries.country_id ".
                     "left join order_items on order_item_order = order_id ".
                     "where order_archive_date is not null ".
                     "    and order_archive_date <> '0000-00-00' " .
		             "   and  order_type = 2 ".
                     "    and (";
    
    $filter_tmp = "order_item_supplier_address = " . $user_data["address"] . " " .
                  "    or order_item_forwarder_address = " . $user_data["address"] . " " .
                  "    or order_retail_operator = " . user_id() . " " .
                  "    or order_client_address = " . $user_data["address"];


	$country_filter = "";
	$tmp = array();
	$sql = "select * from country_access " .
		   "where country_access_user = " . user_id();


	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{            
		$tmp[] = $row["country_access_country"];
	}

	if(count($tmp) > 0) {
		$country_filter = " country_id IN (" . implode(",", $tmp) . ") ";
	}


	if($country_filter == "")
	{
		$sql_countries = $sql_countries . $filter_tmp . ") order by country_name";
	}
	else
	{
		$sql_countries = $sql_countries . $filter_tmp . ") or (" . $country_filter . ") order by country_name";
	}
}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("orders", "orders");

$form->add_section("Range Selection");
$form->add_comment("Please select a range of years.");
$form->add_list("from_year", "From Year", $sql_years, 0);
$form->add_list("to_year", "To Year", $sql_years, 0);

$form->add_list("country", "Country", $sql_countries, 0);
$form->add_edit("ordernumber", "Order Number");

$form->add_button("proceed", "Show Selected Orders");



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("proceed"))
{
    $y1 = $form->value("from_year");
    $y2 = $form->value("to_year");
	$c = $form->value("country");
	$on = $form->value("ordernumber");
    $link = "orders_archive_orders.php?y1=". $y1 . "&y2=" . $y2 . "&c=" . $c . "&on=" . $on;
    redirect($link);
}


   
/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

$page->register_action('orders', 'Orders', "orders_archive.php");
$page->register_action('projects', 'Projects', "projects_archive.php");
$page->register_action('welcome', 'Quit Archive', "../user/welcome.php");

$page->header();
$page->title("Orders: Archive");
$form->render();

?>

<script type="text/javascript">
    
	
	var selectedInput = null;
	$(document).ready(function(){
	  $("#ordernumber").focus();

	  $('input').focus(function() {
				selectedInput = this;
			});

	});

	document.onkeydown = process_key;
	
	function process_key(e)
	{
	  if( !e ) 
	  {
		if( window.event ) 
		{
		  e = window.event;
		} 
		else 
		{
		  return;
		}
	  }

	  if(selectedInput.name == "ordernumber" && e.keyCode==13)
	  {
		button('proceed');
	  }
	}
</script>

<?php

$page->footer();

?>