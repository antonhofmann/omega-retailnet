<?php
/********************************************************************

    order_view_material_list_view_item.php

    View item position in material list.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-12
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-12-03
    Version:        1.1.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_view_list_of_materials_in_orders");

register_param("oid");
register_param("id");


/********************************************************************
    prepare all data needed
*********************************************************************/

// read project and order details
$order = get_order(param("oid"));

// get order_item's information
$order_item = get_order_item(id());

// get company's address
$client_address = get_address($order["order_client_address"]);

// get system currency
$system_currency = get_system_currency_fields();

// get orders's currency
$order_currency = get_order_currency(param("oid"));

// get supplier's currency
$supplier_currency_symbol = "";
if($order_item["item"] and $order_item["supplier"])
{
    $supplier_currency_symbol = get_item_currency_symbol($order_item["supplier"], $order_item["item"]);
}
if (!$supplier_currency_symbol)
{
    $supplier_currency_symbol = get_currency_symbol($order_item["supplier_currency"]);
}


// get pick up date
$item_pick_up_date = get_last_order_item_date(id(), "PICK");


/********************************************************************
    build form
*********************************************************************/
$form = new Form("order_items", "item from list of materials");

$form->add_section("Order");


require_once "include/order_head_small.php";

$form->add_section("Item Information");

$form->add_label("order_item_text", "Description");

if ($order_item["type"] == ITEM_TYPE_STANDARD or $order_item["type"] == ITEM_TYPE_SPECIAL)
{
    $form->add_label("order_item_quantity", "Quantity*");
}

if ($order_item["type"] == ITEM_TYPE_STANDARD)
{
    $form->add_label("order_item_system_price", "Selling Price in " . $system_currency["symbol"] . "*");
    $form->add_label("order_item_client_price", "Client's Price in " . $order_currency["symbol"] . "*");

    $form->add_label("order_item_supplier_system_price", "Supplier's Price in " . $system_currency["symbol"], 0, $order_item["supplier_system_price"]);

    $form->add_label("order_item_supplier_price", "Supplier's Price in " . $supplier_currency_symbol);
}

else if ($order_item["type"] == ITEM_TYPE_SPECIAL)
{
    $form->add_label("order_item_system_price", "Selling Price in " . $system_currency["symbol"]);

    $form->add_label("order_item_client_price", "Client's Price in " . $order_currency["symbol"] . "*");

    $form->add_label("order_item_supplier_system_price", "Supplier's Price in " . $system_currency["symbol"], 0, $order_item["supplier_system_price"]);

    $form->add_label("order_item_supplier_price", "Supplier's Price in " . $supplier_currency_symbol);
}

if ($order_item["type"] == ITEM_TYPE_STANDARD or $order_item["type"] == ITEM_TYPE_SPECIAL)
{
    // accounting informatin

    $form->add_section("Accounting Information");
    $form->add_label("order_item_cost_unit_number", "Cost Unit Number");
    $form->add_label("order_item_po_number", "P.O. Number");
    
    if($item_pick_up_date["last_date"])
    {
        $form->add_section("Supplier");
        $form->add_lookup("order_item_supplier_address", "Supplier", "addresses", "address_company", $flags = 0, $order_item["supplier"]);
    }

    $form->add_label("order_item_production_time", "Production Time");
    $form->add_label("order_item_supplier_item_code", "Supplier's Item Code");
    $form->add_label("order_item_offer_number", "Supplier's Offer Number");
    $form->add_checkbox("order_item_no_offer_required", "does not require a supplier's offer", $order_item["no_offer_required"]);

    // Traffic


    if($item_pick_up_date["last_date"])
    {    
        $form->add_section("Traffic Information");
        $form->add_lookup("order_item_transportation", "Transportation Type", "transportation_types", "transportation_type_name", 0, $order_item["transportation"]);
        $form->add_lookup("order_item_forwarder_address", "Forwarder", "addresses", "address_company", 0, $order_item["forwarder"]);
        $form->add_label("order_item_staff_for_discharge", "Staff for Discharge");
    }
   
    
    
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

   
/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("List of Materials: View Item Position");
$form->render();
$page->footer();

?>