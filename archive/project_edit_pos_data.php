<?php
/********************************************************************

    project_edit_pos_data.php

    Edit POS Data

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-02-18
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-02-18
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "../shared/func_posindex.php";

check_access("can_edit_pos_data");


/********************************************************************
    prepare all data needed
*********************************************************************/
$user_roles = get_user_roles(user_id());

// read project and order details
$project = get_project(param("pid"));


$tracking_info = array();
$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
	   "concat(user_name, ' ', user_firstname) as user_name " . 
	   "from projecttracking " . 
       "left join users on user_id = projecttracking_user_id " . 
       "where projecttracking_project_id = " . param("pid") . 
	   " and projecttracking_field = 'project_real_opening_date' " . 
	   " order by projecttracking_time";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$tracking_info[] = array("projecttracking_oldvalue"=>$row["projecttracking_oldvalue"],
		"projecttracking_newvalue"=>$row["projecttracking_newvalue"],
		"projecttracking_comment"=>$row["projecttracking_comment"],
		"projecttracking_time"=>$row["projecttracking_time"],
		"user_name"=>$row["user_name"]
		);
}

// get company's address
$client_address = get_address($project["order_client_address"]);


$table = "posaddresses";
$table2 = "posareas";

if($project["pipeline"] == 0)
{
	$pos_data = get_poslocation($project["posaddress_id"], "posaddresses");
	$pos_order_sql = "select * from posorders where posorder_order = " . $project["order_id"];
}
elseif($project["pipeline"] == 1)
{
	$pos_data = get_poslocation_from_pipeline($project["posaddress_id"], $project["order_id"]);
	$table = "posaddressespipeline";
	$table2 = "posareaspipeline";

	$pos_order_sql = "select * from posorderspipeline where posorder_order = " . $project["order_id"];
}

//$pos_data = get_poslocation($project["posaddress_id"], "posaddresses");
//$pos_order_sql = "select * from posorders where posorder_order = " . $project["order_id"];


//sales
$sql_distribution_channels = "select mps_distchannel_id, concat(mps_distchannel_group, ' - ', mps_distchannel_name , ' - ', mps_distchannel_code) as channel from mps_distchannels order by mps_distchannel_group, mps_distchannel_name, mps_distchannel_code ";
$sql_turnovertypes_watches = "select mps_turnoverclass_id, mps_turnoverclass_code from mps_turnoverclasses where mps_turnoverclass_group_id = 1 order by mps_turnoverclass_code";
$sql_turnovertypes_bijoux = "select mps_turnoverclass_id, mps_turnoverclass_code from mps_turnoverclasses where mps_turnoverclass_group_id = 2 order by mps_turnoverclass_code";


/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

$form->add_section("Project");
$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid", $project["project_order"]);
$form->add_hidden("pipeline", $project["pipeline"]);


//Project Head
$order_state_name = get_actual_order_state_name($project["order_actual_order_state_code"], 1);

$form->add_label("project_number", "Project Number", 0, $project["project_number"]);
$form->add_lookup("product_line", "Product Line", "product_lines", "product_line_name", 0, $project["project_product_line"]);
$form->add_lookup("product_line_subclass", "Product Line Subclass", "productline_subclasses", "productline_subclass_name", 0, $project["project_product_line_subclass"]);
$form->add_lookup("project_postype", "POS Type", "postypes", "postype_name", 0, $project["project_postype"]);
$form->add_lookup("project_pos_subclass", "POS Type Subclass", "possubclasses", "possubclass_name", NOTNULL, $project["project_pos_subclass"]);

$form->add_label("type3", "Project Legal Type", 0, $project["project_costtype_text"]);
$form->add_label("type2", "Project Kind", 0, $project["projectkind_name"]);

$form->add_label("order_date", "Project Starting Date", 0, to_system_date($project["order_date"]));


if($project["project_projectkind"] != 4 and $project["project_projectkind"] != 5) //Take Over and lease renewal
{
	$form->add_label("planned_opening_date", "Client's preferred POS Opening Date", 0, to_system_date($project["project_planned_opening_date"]));
}

$form->add_label("real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]));
$form->add_label("actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]));

$form->add_label("status", "Status", 0, $project["order_actual_order_state_code"]  . " " . $order_state_name);


$line = "concat(user_name, ' ', user_firstname)";
if ($project["project_retail_coordinator"])
{
    $form->add_lookup("retail_coordinator", "Project Manager", "users", $line, 0, $project["project_retail_coordinator"]);
}
else
{
    $form->add_label("project_retail_coordinator", "Project Manager");
}


if ($project["order_retail_operator"])
{
    $form->add_lookup("retail_operator", "Retail Operator", "users", $line, 0, $project["order_retail_operator"]);
}
else
{
    $form->add_label("order_retail_operator", "Retail Operator");
}


$client = $client_address["company"] . ", " .
          $client_address["zip"] . " " . $client_address["place"] . ", " .
          $client_address["country_name"];


$form->add_label("client_address", "Client", 0, $client);

//End Project Head

$form->add_section("POS Location Address");

if(count($pos_data) > 0)
{
	
	$form->add_hidden("posaddress_id", $pos_data["posaddress_id"]);
	
	$form->add_label("shop_address_company", "Project Name", 0, $pos_data["posaddress_name"]);
	$form->add_label("shop_address_company2", "", 0, $pos_data["posaddress_name2"]);
	$form->add_label("shop_address_address", "Address", 0, $pos_data["posaddress_address"]);
	$form->add_label("shop_address_address2", "", 0, $pos_data["posaddress_address2"]);
	$form->add_label("shop_address_zip", "ZIP*", 0, $pos_data["posaddress_zip"]);
	$form->add_label("shop_address_place", "City", 0 , $pos_data["place_name"]);
	$form->add_label("province", "Province", 0, $pos_data["province_canton"]);

	$form->add_label("country", "Country", 0, $pos_data["country_name"]);
	
		
	
	if (has_access("can_edit_treatment_state"))
	{
		$form->add_section("Treatment State");
		
		
		if($project["project_actual_opening_date"] and $project["project_actual_opening_date"]<>"0000-00-00")
		{
			$form->add_lookup("project_state", "Treatment State", "project_states", "project_state_text", 0, $project["project_state"]);
		}
		else
		{
			$sql = "select project_state_id, project_state_text from " .
				   " project_states where project_state_selectable = 1  or project_state_id = " . dbquote($project["project_state"]);
			$form->add_list("project_state", "Treatment State", $sql, 0, $project["project_state"]);
		}
		
		
	}
	else
	{
		$form->add_hidden("project_state", $project["project_state"]);
	}

	if(in_array(4, $user_roles) and has_access("can_edit_pos_calendar_data"))
	{
		if(in_array(2, $user_roles) or in_array(3, $user_roles) or in_array(10, $user_roles))
		{
			
			if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
			{
				$form->add_section("Project Dates");
			}
			else
			{
				$form->add_section("POS Opening Dates");
			}

			if(count($tracking_info) > 0) {
				$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"],  DISABLED, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20, 0, 1, "changehistory");
			}
			else
			{
				$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], DISABLED, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20);
			}


			$form->add_edit("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]), TYPE_DATE, 20);

			$form->add_section("POS Closing Dates");
			$form->add_edit("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]), TYPE_DATE, 20);
		}
		elseif($project["order_actual_order_state_code"] >= '800')
		{
			if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
			{
				$form->add_section("Project Dates");
			}
			else
			{
				$form->add_section("POS Opening Dates");
			}
			
			if(count($tracking_info) > 0) {
				$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], DISABLED, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20, 0, 1, "changehistory");
			
			}
			else
			{
				$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], DISABLED, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20);
			}


			$form->add_edit("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]), TYPE_DATE, 20);


			$form->add_section("POS Closing Dates");
			$form->add_edit("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]), TYPE_DATE, 20);
		}
		else
		{
			if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
			{
				$form->add_section("Project Dates");
			}
			else
			{
				$form->add_section("POS Opening Dates");
			}

			$form->add_label("shop_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]));
			$form->add_label("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]));

			$form->add_label("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]));
		}
	}
	elseif (has_access("can_edit_pos_calendar_data"))
	{
		
		if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
		{
			$form->add_section("Project Dates");
		}
		else
		{
			$form->add_section("POS Opening Dates");
		}
		if(count($tracking_info) > 0) {
			$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], DISABLED, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20, 0, 1, "changehistory");
		
		}
		else
		{
			$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], DISABLED, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20);
		}

		
		$form->add_edit("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]), TYPE_DATE, 20);


		$form->add_section("POS Closing Dates");
		$form->add_edit("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]), TYPE_DATE, 20);
	}
	else
	{
		if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
		{
			$form->add_section("Project Dates");
		}
		else
		{
			$form->add_section("POS Opening Dates");
		}

		$form->add_label("shop_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]));
		$form->add_label("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]));

		$form->add_label("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]));
	}

	$form->add_hidden("old_shop_actual_opening_date", to_system_date($project["project_actual_opening_date"]));
	$form->add_hidden("old_shop_real_opening_date", to_system_date($project["project_real_opening_date"]));

	$form->add_button("save", "Save Data");
}
else
{
	$form->add_comment("The project has not been assigned to a POS yet.");


	$form->add_hidden("posaddress_id",0);
	
	$form->add_label("shop_address_company", "Project Name", 0, $project["order_shop_address_company"]);
	$form->add_label("shop_address_company2", "", 0, $project["order_shop_address_company2"]);
	$form->add_label("shop_address_address", "Address", 0, $project["order_shop_address_address"]);
	$form->add_label("shop_address_address2", "", 0, $project["order_shop_address_address2"]);
	$form->add_label("shop_address_zip", "ZIP*", 0, $project["order_shop_address_zip"]);
	$form->add_label("shop_address_place", "City", 0 , $project["order_shop_address_place"]);
	$form->add_label("order_shop_address_country_name", "Country", 0, $project["order_shop_address_country_name"]);
	
		
	
	if (has_access("can_edit_treatment_state"))
	{
		$form->add_section("Treatment State");
		
		
		if($project["project_actual_opening_date"] and $project["project_actual_opening_date"]<>"0000-00-00")
		{
			$form->add_lookup("project_state", "Treatment State", "project_states", "project_state_text", 0, $project["project_state"]);
		}
		else
		{
			$sql = "select project_state_id, project_state_text from " .
				   " project_states  where project_state_selectable = 1";
			$form->add_list("project_state", "Treatment State", $sql, 0, $project["project_state"]);
		}
		
		
	}
	else
	{
		$form->add_hidden("project_state", $project["project_state"]);
	}

	if(in_array(4, $user_roles) and has_access("can_edit_pos_calendar_data"))
	{
		if(in_array(2, $user_roles) or in_array(3, $user_roles) or in_array(10, $user_roles))
		{
			
			if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
			{
				$form->add_section("Project Dates");
			}
			else
			{
				$form->add_section("POS Opening Dates");
			}

			if(count($tracking_info) > 0) {
				$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], DISABLED, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20, 0, 1, "changehistory");
			}
			else
			{
				$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], DISABLED, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20);
			}


			$form->add_edit("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]), TYPE_DATE, 20);

			$form->add_section("POS Closing Dates");
			$form->add_edit("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]), TYPE_DATE, 20);
		}
		elseif($project["order_actual_order_state_code"] >= '800')
		{
			if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
			{
				$form->add_section("Project Dates");
			}
			else
			{
				$form->add_section("POS Opening Dates");
			}
			
			if(count($tracking_info) > 0) {
				$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], DISABLED, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20, 0, 1, "changehistory");
			
			}
			else
			{
				$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], DISABLED, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20);
			}


			$form->add_edit("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]), TYPE_DATE, 20);


			$form->add_section("POS Closing Dates");
			$form->add_edit("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]), TYPE_DATE, 20);
		}
		else
		{
			if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
			{
				$form->add_section("Project Dates");
			}
			else
			{
				$form->add_section("POS Opening Dates");
			}

			$form->add_label("shop_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]));
			$form->add_label("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]));

			$form->add_label("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]));
		}
	}
	elseif (has_access("can_edit_pos_calendar_data"))
	{
		
		if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
		{
			$form->add_section("Project Dates");
		}
		else
		{
			$form->add_section("POS Opening Dates");
		}
		if(count($tracking_info) > 0) {
			$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], DISABLED, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20, 0, 1, "changehistory");
		
		}
		else
		{
			$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], DISABLED, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20);
		}

		
		$form->add_edit("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]), TYPE_DATE, 20);


		$form->add_section("POS Closing Dates");
		$form->add_edit("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]), TYPE_DATE, 20);
	}
	else
	{
		if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
		{
			$form->add_section("Project Dates");
		}
		else
		{
			$form->add_section("POS Opening Dates");
		}

		$form->add_label("shop_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]));
		$form->add_label("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]));

		$form->add_label("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]));
	}

	$form->add_hidden("old_shop_actual_opening_date", to_system_date($project["project_actual_opening_date"]));
	$form->add_hidden("old_shop_real_opening_date", to_system_date($project["project_real_opening_date"]));

	$form->add_button("save", "Save Data");
}



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("save"))
{
    if($form->value("shop_actual_opening_date"))
	{
		$form->add_validation("from_system_date({shop_actual_opening_date}) <=  " . dbquote(date("Y-m-d", strtotime("+3 days"))), "The actual POS opening date must be a date in the past or can only be at maximum three days in the future!");

	}

	if($form->value("shop_closing_date"))
	{
		$form->add_validation("from_system_date({shop_closing_date}) <=  " . dbquote(date("Y-m-d", strtotime("+3 days"))), "The POS closing date must be a date in the past or can only be at maximum three days in the future!");

	}
		

	if ($form->validate())
    {
	
		
		$project_fields = array();
		// update record in table projects

		if($form->value("shop_closing_date"))
		{
			$value = 5;
			$project_fields[] = "project_state = " . $value;
		}
		elseif($form->value("shop_actual_opening_date"))
		{
			$project_fields[] = "project_state = 4"; //open
		}
		else
		{
			$value = trim($form->value("project_state")) == "" ? "null" : dbquote($form->value("project_state"));
			$project_fields[] = "project_state = " . $value;
		}

		
		$value = trim($form->value("shop_real_opening_date")) == "" ? "null" : dbquote(from_system_date($form->value("shop_real_opening_date")));
		$project_fields[] = "project_real_opening_date = " . $value;

		$value = trim($form->value("shop_actual_opening_date")) == "" ? "null" : dbquote(from_system_date($form->value("shop_actual_opening_date")));
		$project_fields[] = "project_actual_opening_date = " . $value;

		$value = trim($form->value("shop_closing_date")) == "" ? "null" : dbquote(from_system_date($form->value("shop_closing_date")));
		$project_fields[] = "project_shop_closingdate = " . $value;

		$value = "current_timestamp";
		$project_fields[] = "date_modified = " . $value;
		
		if (isset($_SESSION["user_login"]))
		{
			$value = dbquote($_SESSION["user_login"]);
			$project_fields[] = "user_modified = " . $value;
		}


		$sql = "update projects set " . join(", ", $project_fields) . " where project_id = " . $form->value("pid");
		mysql_query($sql) or dberror($sql);


		if($form->value("posaddress_id") > 0)
		{
			//update posorder
			$sql = "select posorder_id " . 
				   "from posorders " . 
				   "where posorder_order = " . $form->value("oid");
			
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
			
			
				$sql = "Update posorders set " . 
					   "posorder_opening_date = " . dbquote(from_system_date($form->value("shop_actual_opening_date"))) . ", " .
					   "posorder_closing_date = " . dbquote(from_system_date($form->value("shop_closing_date"))) . 
					   " where posorder_id = " . $row["posorder_id"];

				mysql_query($sql) or dberror($sql);
			}
			elseif($form->value("shop_actual_opening_date"))
			{ 
				//check if project's order still is in piepeline
				$sql = "select * " . 
					   "from posorderspipeline " . 
					   "where posorder_order = " . $form->value("oid");

				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					
					$pos_id = $row["posorder_posaddress"];
					$pos_id_pipeline = $row["posorder_posaddress"];
					
					//check if posaddress is in pipelien
					if($row["posorder_parent_table"] = 'posaddressespipeline')
					{
						//transfer posaddress
						$posorder_pos_id = $row["posorder_posaddress"];

						$sql1 = "select * from posaddressespipeline where posaddress_id = " . $posorder_pos_id;
						$res1 = mysql_query($sql1) or dberror($sql1);
						if ($row1 = mysql_fetch_assoc($res1))
						{
							//pos address
							$fields = array();
							$values = array();

							$fields[] = "posaddress_client_id";
							$values[] = dbquote($row1["posaddress_client_id"]);

							$fields[] = "posaddress_ownertype";
							$values[] = dbquote($row1["posaddress_ownertype"]);

							$fields[] = "posaddress_franchisor_id";
							$values[] = dbquote($row1["posaddress_franchisor_id"]);

							$fields[] = "posaddress_franchisee_id";
							$values[] = dbquote($row1["posaddress_franchisee_id"]);

							$fields[] = "posaddress_name";
							$values[] = dbquote($row1["posaddress_name"]);

							$fields[] = "posaddress_name2";
							$values[] = dbquote($row1["posaddress_name2"]);

							$fields[] = "posaddress_address";
							$values[] = dbquote($row1["posaddress_address"]);

							$fields[] = "posaddress_address2";
							$values[] = dbquote($row1["posaddress_address2"]);

							$fields[] = "posaddress_zip";
							$values[] = dbquote($row1["posaddress_zip"]);

							$fields[] = "posaddress_place";
							$values[] = dbquote($row1["posaddress_place"]);

							$fields[] = "posaddress_place_id";
							$values[] = dbquote($row1["posaddress_place_id"]);

							$fields[] = "posaddress_country";
							$values[] = dbquote($row1["posaddress_country"]);
							
							$fields[] = "posaddress_phone";
							$values[] = dbquote($row1["posaddress_phone"]);

							$fields[] = "posaddress_fax";
							$values[] = dbquote($row1["posaddress_fax"]);

							$fields[] = "posaddress_email";
							$values[] = dbquote($row1["posaddress_email"]);

							$fields[] = "posaddress_google_lat";
							$values[] = dbquote($row1["posaddress_google_lat"]);

							$fields[] = "posaddress_google_long";
							$values[] = dbquote($row1["posaddress_google_long"]);

							$fields[] = "posaddress_google_precision";
							$values[] = dbquote($row1["posaddress_google_precision"]);

							$fields[] = "posaddress_store_postype";
							$values[] = dbquote($row1["posaddress_store_postype"]);

							$fields[] = "posaddress_store_subclass";
							$values[] = dbquote($row1["posaddress_store_subclass"]);

							$fields[] = "posaddress_store_furniture";
							$values[] = dbquote($row1["posaddress_store_furniture"]);

							$fields[] = "posaddress_store_grosssurface";
							$values[] = dbquote($row1["posaddress_store_grosssurface"]);

							$fields[] = "posaddress_store_totalsurface";
							$values[] = dbquote($row1["posaddress_store_totalsurface"]);

							$fields[] = "posaddress_store_retailarea";
							$values[] = dbquote($row1["posaddress_store_retailarea"]);

							$fields[] = "posaddress_store_backoffice";
							$values[] = dbquote($row1["posaddress_store_backoffice"]);

							$fields[] = "posaddress_store_numfloors";
							$values[] = dbquote($row1["posaddress_store_numfloors"]);

							$fields[] = "posaddress_store_floorsurface1";
							$values[] = dbquote($row1["posaddress_store_floorsurface1"]);

							$fields[] = "posaddress_store_floorsurface2";
							$values[] = dbquote($row1["posaddress_store_floorsurface2"]);

							$fields[] = "posaddress_store_floorsurface3";
							$values[] = dbquote($row1["posaddress_store_floorsurface3"]);


							$fields[] = "posaddress_fagagreement_type";
							$values[] = dbquote($row1["posaddress_fagagreement_type"]);

							$fields[] = "posaddress_fagrsent";
							$values[] = dbquote($row1["posaddress_fagrsent"]);

							$fields[] = "posaddress_fagrsigned";
							$values[] = dbquote($row1["posaddress_fagrsigned"]);

							$fields[] = "posaddress_fagrstart";
							$values[] = dbquote($row1["posaddress_fagrstart"]);

							$fields[] = "posaddress_fagrend";
							$values[] = dbquote($row1["posaddress_fagrend"]);

							$fields[] = "posaddress_fag_comment";
							$values[] = dbquote($row1["posaddress_fag_comment"]);

							$fields[] = "posaddress_perc_class";
							$values[] = dbquote($row1["posaddress_perc_class"]);

							$fields[] = "posaddress_perc_tourist";
							$values[] = dbquote($row1["posaddress_perc_tourist"]);

							$fields[] = "posaddress_perc_transport";
							$values[] = dbquote($row1["posaddress_perc_transport"]);

							$fields[] = "posaddress_perc_people";
							$values[] = dbquote($row1["posaddress_perc_people"]);

							$fields[] = "posaddress_perc_parking";
							$values[] = dbquote($row1["posaddress_perc_parking"]);

							$fields[] = "posaddress_perc_visibility1";
							$values[] = dbquote($row1["posaddress_perc_visibility1"]);

							$fields[] = "posaddress_perc_visibility2";
							$values[] = dbquote($row1["posaddress_perc_visibility2"]);

							$fields[] = "posaddress_export_to_web";
							$values[] = dbquote($row1["posaddress_export_to_web"]);

							$sql = "insert into posaddresses (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
							mysql_query($sql) or dberror($sql);

							$new_posaddress_id = mysql_insert_id();
							$pos_id = mysql_insert_id();

							$sql = "delete from posaddressespipeline where posaddress_id = " . $posorder_pos_id;
							mysql_query($sql) or dberror($sql);


							//transfer posleases
							$sql = "select * from posleasespipeline where poslease_posaddress = " . dbquote($pos_id_pipeline);

							$res = mysql_query($sql) or dberror($sql);
							while ($row = mysql_fetch_assoc($res))
							{
								$fields = array();
								$values = array();


								$fields[] = "poslease_posaddress";
								$values[] = dbquote($pos_id);

								$fields[] = "poslease_order";
								$values[] = dbquote($row["poslease_ordere"]);

								$fields[] = "poslease_lease_type";
								$values[] = dbquote($row["poslease_lease_type"]);
								
								$fields[] = "poslease_anual_rent";
								$values[] = dbquote($row["poslease_anual_rent"]);

								$fields[] = "poslease_salespercent";
								$values[] = dbquote($row["poslease_salespercent"]);

								$fields[] = "poslease_indexclause_in_contract";
								$values[] = dbquote($row["poslease_indexclause_in_contract"]);

								$fields[] = "poslease_isindexed";
								$values[] = dbquote($row["poslease_isindexed"]);

								$fields[] = "poslease_indexrate";
								$values[] = dbquote($row["poslease_indexrate"]);

								$fields[] = "poslease_average_increase";
								$values[] = dbquote($row["poslease_average_increase"]);

								$fields[] = "poslease_realestate_fee";
								$values[] = dbquote($row["poslease_realestate_fee"]);

								$fields[] = "poslease_annual_charges";
								$values[] = dbquote($row["poslease_annual_charges"]);

								$fields[] = "poslease_other_fees";
								$values[] = dbquote($row["poslease_other_fees"]);
								
								$fields[] = "poslease_startdate";
								$values[] = dbquote($row["poslease_startdate"]);

								$fields[] = "poslease_enddate";
								$values[] = dbquote($row["poslease_enddate"]);

								$fields[] = "poslease_extensionoption";
								$values[] = dbquote($row["poslease_extensionoption"]);

								$fields[] = "poslease_handoverdate";
								$values[] = dbquote($row["poslease_handoverdate"]);

								$fields[] = "poslease_firstrentpayed";
								$values[] = dbquote($row["poslease_firstrentpayed"]);

								$fields[] = "poslease_exitoption";
								$values[] = dbquote($row["poslease_exitoption"]);

								$fields[] = "poslease_negotiator";
								$values[] = dbquote($row["poslease_negotiator"]);

								$fields[] = "poslease_landlord_name";
								$values[] = dbquote($row["poslease_landlord_name"]);

								$fields[] = "poslease_negotiated_conditions";
								$values[] = dbquote($row["poslease_negotiated_conditions"]);

								$fields[] = "poslease_termination_time";
								$values[] = dbquote($row["poslease_termination_time"]);

								$fields[] = "date_created";
								$values[] = dbquote(date("Y-m-d"));

								$fields[] = "date_modified";
								$values[] = dbquote(date("Y-m-d"));

								$fields[] = "user_created";
								$values[] = dbquote(user_login());

								$fields[] = "user_modified";
								$values[] = dbquote(user_login());

								$sql = "insert into posleases (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
								mysql_query($sql) or dberror($sql);
						
							}

							$sql = "delete from posleasespipeline where poslease_posaddress = " . dbquote($pos_id_pipeline);
							mysql_query($sql) or dberror($sql);


							//posareas
							$sql = "select * from posareaspipeline where posarea_posaddress = " . $pos_id_pipeline;

							$res = mysql_query($sql) or dberror($sql);
							while ($row = mysql_fetch_assoc($res))
							{
								$fields = array();
								$values = array();

								$fields[] = "posarea_posaddress";
								$values[] = dbquote($pos_id);

								$fields[] = "posarea_area";
								$values[] = dbquote($row["posarea_area"]);

								$fields[] = "date_created";
								$values[] = dbquote(date("Y-m-d"));

								$fields[] = "date_modified";
								$values[] = dbquote(date("Y-m-d"));

								$fields[] = "user_created";
								$values[] = dbquote(user_login());

								$fields[] = "user_modified";
								$values[] = dbquote(user_login());

								$sql = "insert into posareas (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
								mysql_query($sql) or dberror($sql);
							}

							$sql = "delete from posareaspipeline where posarea_posaddress = " . $pos_id_pipeline;
							mysql_query($sql) or dberror($sql);
						}
					}
					
					//transfer posorder
					$fields = array();
					$values = array();

					$fields[] = "posorder_posaddress";
					$values[] = dbquote($pos_id);

					$fields[] = "posorder_order";
					$values[] = dbquote($row["posorder_order"]);

					$fields[] = "posorder_type";
					$values[] = dbquote($row["posorder_type"]);

					$fields[] = "posorder_ordernumber";
					$values[] = dbquote($row["posorder_ordernumber"]);

					$fields[] = "posorder_year";
					$values[] = dbquote($row["posorder_year"]);

					$fields[] = "posorder_product_line";
					$values[] = dbquote($row["posorder_product_line"]);

					$fields[] = "posorder_product_line_subclass";
					$values[] = dbquote($row["posorder_product_line_subclass"]);

					$fields[] = "posorder_postype";
					$values[] = dbquote($row["posorder_postype"]);

					$fields[] = "posorder_subclass";
					$values[] = dbquote($row["posorder_subclass"]);

					$fields[] = "posorder_project_kind";
					$values[] = dbquote($row["posorder_project_kind"]);

					$fields[] = "posorder_legal_type";
					$values[] = dbquote($row["posorder_legal_type"]);

					$fields[] = "posorder_system_currency";
					$values[] = dbquote($row["posorder_system_currency"]);

					$fields[] = "posorder_budget_approved_sc";
					$values[] = dbquote($row["posorder_budget_approved_sc"]);

					$fields[] = "posorder_real_cost_sc";
					$values[] = dbquote($row["posorder_real_cost_sc"]);

					$fields[] = "posorder_client_currency";
					$values[] = dbquote($row["posorder_client_currency"]);

					$fields[] = "posorder_budget_approved_cc";
					$values[] = dbquote($row["posorder_budget_approved_cc"]);

					$fields[] = "posorder_real_cost_cc";
					$values[] = dbquote($row["posorder_real_cost_cc"]);

					
					$fields[] = "posorder_neighbour_left";
					$values[] = dbquote($row["posorder_neighbour_left"]);

					$fields[] = "posorder_neighbour_right";
					$values[] = dbquote($row["posorder_neighbour_right"]);

					$fields[] = "posorder_neighbour_acrleft";
					$values[] = dbquote($row["posorder_neighbour_acrleft"]);

					$fields[] = "posorder_neighbour_acrright";
					$values[] = dbquote($row["posorder_neighbour_acrright"]);

					$fields[] = "posorder_neighbour_brands";
					$values[] = dbquote($row["posorder_neighbour_brands"]);

					$fields[] = "posorder_neighbour_comment";
					$values[] = dbquote($row["posorder_neighbour_comment"]);

					$fields[] = "posorder_currency_symbol";
					$values[] = dbquote($row["posorder_currency_symbol"]);

					$fields[] = "posorder_exchangerate";
					$values[] = dbquote($row["posorder_exchangerate"]);

					$fields[] = "posorder_remark";
					$values[] = dbquote($row["posorder_remark"]);

					$fields[] = "posorder_project_locally_produced";
					$values[] = dbquote($row["posorder_project_locally_produced"]);

					$fields[] = "date_created";
					$values[] = dbquote(date("Y-m-d"));

					$fields[] = "date_modified";
					$values[] = dbquote(date("Y-m-d"));

					$fields[] = "user_created";
					$values[] = dbquote(user_login());

					$fields[] = "user_modified";
					$values[] = dbquote(user_login());
					
					$sql = "insert into posorders (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					mysql_query($sql) or dberror($sql);


					$sql = "delete from posorderspipeline where posorder_order = " . $form->value("oid");
					mysql_query($sql) or dberror($sql);

				}
			}


			$result = update_posdata_from_posorders($form->value("posaddress_id"));

			if($form->value("shop_actual_opening_date") and $form->value("posaddress_id") > 0)
			{
				update_store_locator($form->value("posaddress_id"));
			}
		}

						
        $form->message("Your changes have been saved.");

		
		//$link = "project_edit_pos_data.php?pid=" . param("pid"); 
        //redirect($link);

    }
}

    
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Edit POS-Data");
$form->render();

?>
<div id="changehistory" style="display:none;">
    <strong>Changes of the agreed opening date</strong>
	<table class="table_tracking">
	<tr>
	<td class="label">User</td>
	<td class="label">Time</td>
	<td class="label">Old Value</td>
	<td class="label">New Value</td>
	<td class="label">Comment</td>
	</tr>

	<?php
		foreach($tracking_info as $key=>$values)
		{
			echo '<tr class="tr_tracking"><td class="td_tracking_nobr">' . $values['user_name'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_time'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_oldvalue'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_newvalue'] . '</td>';
			echo '<td class="td_tracking">' . $values['projecttracking_comment'] . '</td></tr>';
		}
	?>
	
	</table>
</div> 

<?php
echo "<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>";
$page->footer();



?>