<?php
/********************************************************************

    project_costs_costsheet_bids.php

    View or edit bids for a project

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-01-09
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2012, OMEGA SA, All Rights Reserved.

*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/project_cost_functions.php";

check_access("can_edit_local_constrction_work");

if(!param("pid"))
{
	$link = "welcome.php";
	redirect($link);
}



/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));
// get company's address
$client_address = get_address($project["order_client_address"]);

$currency_symbol = get_currency_symbol($project["order_client_currency"]);


/********************************************************************
    check if project cost positions are present and create if not
*********************************************************************/

$sql = "select count(costsheet_id) as num_recs from costsheets " . 
       " where costsheet_project_id = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

if($row["num_recs"] == 0) //make the user select a cost template
{
	$link = "project_costs_overview.php?pid=" . param("pid");
	redirect($link);
}


/********************************************************************
    check if bid positions are present 
*********************************************************************/
$has_positions = true;

$sql = "select count(costsheet_bid_position_id) as num_recs from costsheet_bid_positions " . 
       " where costsheet_bid_position_costsheet_bid_id = " . id();

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

if($row["num_recs"] == 0) 
{
	$has_positions = false;
}

/********************************************************************
	prepare data
*********************************************************************/ 
$sql_cost_groups = "select DISTINCT costsheet_pcost_group_id, " . 
		               "concat(pcost_group_code, ' ', pcost_group_name) as costgroup " .
		               "from costsheets " .
					   "left join pcost_groups on pcost_group_id = costsheet_pcost_group_id " .
					   "where costsheet_project_id = " . param("pid") . 
					   " order by costgroup";

/********************************************************************
	Create Form
*********************************************************************/ 

$form = new Form("costsheet_bids", "costsheet_bids");


$form->add_section("Project");
$form->add_hidden("pid", param('pid'));


require_once "include/project_head_small.php";

$form->add_section("General Information");
$form->add_hidden("costsheet_bid_project_id", param("pid"));
$form->add_label("costsheet_bid_company", "Company*");
$form->add_label("costsheet_bid_date", "Date*");
$form->add_label("costsheet_bid_remark", "Remarks");






$link = "javascript:popup('/user/project_costs_bid_pdf.php?pid=" . param("pid") . "&bid=" . id() . "', 800, 600);";
$form->add_button("print_bid", "Print Bid", $link);



$form->add_button("back", "Back");

if($project["order_budget_is_locked"] == 1)
{
	$form->error("Budget is locked, no more changes can be made!");
}

$form->populate();
$form->process();


if($form->button("back"))
{
	$link = "project_costs_bids.php?pid=" . param("pid");
	redirect($link);
}

/********************************************************************
	Create list of cost positions
*********************************************************************/ 
$list_names = array();
$checkbox_ids = array();
$delete_ids = array();
$list_has_positions = array();
if($has_positions == true)
{	
	//get data 
	$bid_totals = get_project_bid_totals(id());


	$code_data = array();
	$bid_data = array();
	$currency_data = array();
	$text_data = array();
	$comment_data = array();
	$in_budget = array();
	

	$sql = "select costsheet_bid_position_id, costsheet_bid_position_pcost_group_id, " . 
		   "costsheet_bid_position_code, costsheet_bid_position_text, " . 
		   "costsheet_bid_position_amount, costsheet_bid_position_comment, costsheet_bid_position_is_in_budget " . 
		   "from costsheet_bid_positions " .
		   "where costsheet_bid_position_costsheet_bid_id =" .id();

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$code_data[$row["costsheet_bid_position_id"]] = $row["costsheet_bid_position_code"];
		$text_data[$row["costsheet_bid_position_id"]] = $row["costsheet_bid_position_text"];
		$bid_data[$row["costsheet_bid_position_id"]] = $row["costsheet_bid_position_amount"];
		$currency_data[$row["costsheet_bid_position_id"]] = $currency_symbol;
		$comment_data[$row["costsheet_bid_position_id"]] = $row["costsheet_bid_position_comment"];
		$in_budget[$row["costsheet_bid_position_id"]] = $row["costsheet_bid_position_is_in_budget"];
		$checkbox_ids[$row["costsheet_bid_position_pcost_group_id"]][$row["costsheet_bid_position_id"]] = "__costsheet_bid_positions_costsheet_bid_position_is_in_budget_" . $row["costsheet_bid_position_id"];
		$delete_ids[$row["costsheet_bid_position_pcost_group_id"]][$row["costsheet_bid_position_id"]] = "__costsheet_bid_positions_select_to_delete_" . $row["costsheet_bid_position_id"];
		
		$list_has_positions[$row["costsheet_bid_position_pcost_group_id"]] = true;
	}

	


	//add all cost groups and cost sub groups
	$save_button_names = array();
	$add_button_names = array();
	$remove_button_names = array();
	$remove_button2_names = array();
	$select_button_names = array();
	$group_ids = array();
	$group_titles = array();
	

	$sql = "select DISTINCT costsheet_bid_position_pcost_group_id, pcost_group_code, pcost_group_name " .
		   "from costsheet_bid_positions " .
		   "left join pcost_groups on pcost_group_id = costsheet_bid_position_pcost_group_id " . 
		   "where costsheet_bid_position_costsheet_bid_id = " . id() . 
		   " order by pcost_group_code";


	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$listname = "list" . $row["pcost_group_code"];
		$list_names[] = $listname;
		$group_ids[] = $row["costsheet_bid_position_pcost_group_id"];
		$group_titles[] = $row["pcost_group_name"];

		
		$sql = "select costsheet_bid_position_id, costsheet_bid_position_code, costsheet_bid_position_text, " . 
			   "costsheet_bid_position_amount, costsheet_bid_position_comment, costsheet_bid_position_is_in_budget, " . 
			   "concat(pcost_subgroup_code, ' ', pcost_subgroup_name) as subgroup " . 
			   "from costsheet_bid_positions " .
			   "left join pcost_groups on pcost_group_id = costsheet_bid_position_pcost_group_id " .
			   " left join pcost_subgroups on pcost_subgroup_id = costsheet_bid_position_pcost_subgroup_id"; 

		$list_filter = "costsheet_bid_position_costsheet_bid_id = " . id() . " and costsheet_bid_position_pcost_group_id = " . $row["costsheet_bid_position_pcost_group_id"];
		
		$toggler = '<div class="toggler_pointer" id="l' . $row["costsheet_bid_position_pcost_group_id"] . '_on"><span class="fa fa-minus-square toggler"></span>' .$row["pcost_group_name"] . '</div>';
		
		$$listname = new ListView($sql, LIST_HAS_HEADER | LIST_HAS_FOOTER);

		$$listname->set_entity("costsheet_bid_positions");
		$$listname->set_order("LENGTH(costsheet_bid_position_code), COALESCE(costsheet_bid_position_code,'Z')");
		$$listname->set_group("subgroup");
		$$listname->set_filter($list_filter);
		$$listname->set_title($toggler);

		
		$$listname->add_checkbox_column("costsheet_bid_position_is_in_budget", "", COLUMN_UNDERSTAND_HTML, $in_budget);
		$$listname->add_text_column("costsheet_bid_position_code", "Code", COLUMN_ALIGN_LEFT, $code_data);
		$$listname->add_text_column("costsheet_bid_position_text", "Text", COLUMN_ALIGN_LEFT, $text_data);
		$$listname->add_text_column("costsheet_bid_position_amount", "Costs", COLUMN_ALIGN_RIGHT, $bid_data);
		$$listname->add_text_column("currency", "", 0, $currency_data);
		$$listname->add_text_column("costsheet_bid_position_comment", "Comment", COLUMN_ALIGN_LEFT, $comment_data);
		
	
		if(array_key_exists($row["costsheet_bid_position_pcost_group_id"], $list_has_positions))
		{
			foreach($bid_totals["subgroup_totals"] as $subgroup=>$bid_sub_group_total)
			{
				$$listname->set_group_footer("costsheet_bid_position_text", $subgroup , "Subgroup Total");
				$$listname->set_group_footer("costsheet_bid_position_amount",  $subgroup , number_format($bid_sub_group_total, 2));
			}
					
			$$listname->set_footer("costsheet_bid_position_code", "Total");
			$$listname->set_footer("costsheet_bid_position_amount", number_format($bid_totals["group_totals"][$row["costsheet_bid_position_pcost_group_id"]], 2));
			
		}

		$$listname->populate();
		$$listname->process();
	}



	
}

$page = new Page("projects");


require "include/project_page_actions.php";

$page->header();
$page->title(id() ? "Project Costs - Edit Bid" : "Project Costs - Add Bid");

require_once("include/costsheet_tabs.php");
$form->render();


foreach($list_names as $key=>$listname)
{
	
	if(array_key_exists("costsheet", $_SESSION) and array_key_exists($listname, $_SESSION["costsheet"]))
	{
		if($_SESSION["costsheet"][$listname] == 0)
		{
			echo '<p>&nbsp;</p>';
			$toggler = '<div class="toggler_pointer" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
			echo $toggler;
			
			echo '<div style="display:none;" id="' . $listname . '">';
			$$listname->render();
			echo '</div>';
		}
		else
		{
			echo '<p>&nbsp;</p>';
			$toggler = '<div class="toggler_pointer toggler_pointer_off" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
			echo $toggler;
			
			echo '<div id="' . $listname . '">';
			$$listname->render();
			echo '</div>';
		}
	}
	else
	{
		echo '<p>&nbsp;</p>';
		$toggler = '<div class="toggler_pointer toggler_pointer_off" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
		echo $toggler;
		
		echo '<div id="' . $listname . '">';
		$$listname->render();
		echo '</div>';
	}
}


?>
<script language="javascript">
jQuery(document).ready(function($) {
	<?php
	foreach($list_names as $key=>$listname)
	{
	?>
		$('#l<?php echo $group_ids[$key];?>_on').click(function()  {
			$('#<?php echo $listname;?>').css('display', 'none');
			$('#l<?php echo $group_ids[$key];?>_off').css('display', 'block');

			$.ajax({
				type: "POST",
				data: "listname=<?php echo $listname;?>&visibility=0",
				url: "../shared/ajx_costsheet_liststaes.php",
				success: function(msg){
				}
			});
		});
		
		$('#l<?php echo $group_ids[$key];?>_off').click(function()  {
			$('#<?php echo $listname;?>').css('display', 'block');
			$('#l<?php echo $group_ids[$key];?>_off').css('display', 'none');

			$.ajax({
				type: "POST",
				data: "listname=<?php echo $listname;?>&visibility=1",
				url: "../shared/ajx_costsheet_liststaes.php",
				success: function(msg){
				}
			});
		});
	<?php
	}
	?>
});


function add_new_cost_position(pid, gid, bid)
{		
	url = '/user/project_costs_add_positions.php?pid=' + pid + '&gid=' + gid + '&mode=bid' + '&bid=' + bid;
	$.nyroModalManual({
	  url: url
	});

}

function select_all_cost_positions_for_budget(gid)
{
	var selector = "#selector" + gid;

	if($(selector).html() == ' Budget ')
	{
		<?php
			foreach($checkbox_ids as $key=>$ids)
			{
				?>
				if(<?php echo $key;?> == gid){
					<?php
						foreach($ids as $key2=>$id)
						{
							echo "$('#" . $id . "').attr('checked', false);";
						}
					?>
				}
				<?php
			}
		?>
		$(selector).html(' Budget');
	}
	else
	{
		<?php
			foreach($checkbox_ids as $key=>$ids)
			{
				?>
				if(<?php echo $key;?> == gid){
					<?php
						foreach($ids as $key2=>$id)
						{
							echo "$('#" . $id . "').attr('checked', true);";
						}
					?>
				}
				<?php
			}
		?>
		$(selector).html(' Budget ');
	}
}


function select_all_delete_positions(gid)
{
	var selector = "#selector2" + gid;

	if($(selector).html() == '  ')
	{
		<?php
			foreach($delete_ids as $key=>$ids)
			{
				?>
				if(<?php echo $key;?> == gid){
					<?php
						foreach($ids as $key2=>$id)
						{
							echo "$('#" . $id . "').attr('checked', false);";
						}
					?>
				}
				<?php
			}
		?>
		$(selector).html(' ');
	}
	else
	{
		<?php
			foreach($delete_ids as $key=>$ids)
			{
				?>
				if(<?php echo $key;?> == gid){
					<?php
						foreach($ids as $key2=>$id)
						{
							echo "$('#" . $id . "').attr('checked', true);";
						}
					?>
				}
				<?php
			}
		?>
		$(selector).html('  ');
	}
}



</script>

<?php

$page->footer();

?>