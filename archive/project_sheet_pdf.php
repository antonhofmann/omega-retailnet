<?php
/********************************************************************

    project_sheet_pdf.php

    Creates a PDF project sheet.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-10-07
    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date modified:  2003-05-12
    Version:        1.1.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

$SUPPRESS_HEADERS = true;
define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 

require "../include/frame.php";

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');

define("COLUMN1_XPOS", 15);
define("COLUMN1_DATA_OFFSET", 32);
define("COLUMN1_DATA_WIDTH", 33);
define("COLUMN1_DATA2_OFFSET", 37);
define("COLUMN1_DATA2_WIDTH", 28);

define("COLUMN2_XPOS", 98);
define("COLUMN2_DATA_OFFSET", 40);
define("COLUMN2_DATA_WIDTH", 44);
define("COLUMN2_DATA2_WIDTH", 60);

define("TITLE_YPOS", 15);
define("GROUP1_YPOS", 25);
define("GROUP2_YPOS", 68);
define("GROUP3_YPOS", 122);
define("JUSTIFICATION_YPOS", 184);
define("JUSTIFICATION_COST_XPOS", 176);
define("SUPPLIER_YPOS", 250);

// Check access and id parameter

if (!isset($id))
{
    error("Must be called width id of an existing order record");
}

// Select project data

$sql = "select project_number, project_cost_center, project_account_number, project_id, " .
       "    unix_timestamp(project_opening_date) as project_opening_date, " .
       "    unix_timestamp(project_closing_date) as project_closing_date, " .
       "    project_budget_total, project_budget_committed, project_budget_spent, " .
       "    project_budget_after_project, project_name, product_line_name, " .
       "    project_planned_amount_current_year, project_planned_amount_next_year, " .
       "    currency_symbol, currency_exchange_rate, currency_factor, " .
       "    project_share_company, project_share_other, " .
       "    project_approval_person1, project_approval_person2, project_approval_person3, " .
       "    project_approval_person4, project_approval_person5, project_approval_person6, " .
       "    project_approval_person7, business_unit_name, " .
       "    unix_timestamp(project_approval_date1) as project_approval_date1, " .
       "    unix_timestamp(project_approval_date2) as project_approval_date2, " .
       "    unix_timestamp(project_approval_date3) as project_approval_date3, " .
       "    unix_timestamp(project_approval_date4) as project_approval_date4, " .
       "    unix_timestamp(project_approval_date5) as project_approval_date5, " .
       "    unix_timestamp(project_approval_date6) as project_approval_date6, " .
       "    unix_timestamp(project_approval_date7) as project_approval_date7, " .
       "    project_approval_description, project_sheet_name, " .
       "    suppliers.address_company, suppliers.address_company2, " .
       "    suppliers.address_address, suppliers.address_address2, " .
       "    suppliers.address_zip, suppliers.address_place, " .
       "    country_name, user_firstname, user_name, user_phone, user_email, " .
	   "    project_costtype_text " . 
       "from orders left join projects on order_id = project_order " .
       "    left join product_lines on project_product_line = product_line_id " .
       "    left join addresses on order_client_address = addresses.address_id  " .
       "    left join currencies on addresses.address_currency = currency_id " .
       "    left join addresses as suppliers on order_supplier_address = suppliers.address_id " .
       "    left join countries on suppliers.address_country = country_id " .
       "    left join users on suppliers.address_contact = user_id " .
       "    left join business_units on business_unit_id = project_business_unit " .
	   "    left join project_costs on project_cost_order = order_id " . 
	   "    left join project_costtypes on project_costtype_id = project_cost_type " . 
       "where order_id = $id";

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

// Create and setup PDF document
$pdf = new TCPDF("P", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 23, 11);
$pdf->setPrintHeader(false);

$pdf->Open();
$pdf->SetTitle($row['project_sheet_name']);
$pdf->SetAuthor(BRAND . " Retail Net");
$pdf->SetDisplayMode(150);
$pdf->AddPage();

$pdf->AddFont('arialn');
$pdf->AddFont('arialn', 'B');

$pdf->SetLeftMargin(0);

$pdf->Image('../pictures/omega_logo.jpg',16,8,33);
$pdf->SetFont('arialn','B',12);
$pdf->SetY(10);
$pdf->Cell(0,12, "Project Sheet", 0, 0, 'R');

// Title

$pdf->SetFont("arialn", "B", 18);
$pdf->SetXY(COLUMN1_XPOS, TITLE_YPOS);
$pdf->Cell(0, 0, strtoupper($row['project_sheet_name']));

// Cost center

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS, GROUP1_YPOS);
$pdf->Cell(0, 8, "Cost center");

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA_OFFSET, GROUP1_YPOS);
$pdf->Cell(COLUMN1_DATA_WIDTH, 8, $row["project_cost_center"], 1, 0, "L");

// Account number

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS, GROUP1_YPOS + 9 + 6);
$pdf->Cell(0, 8, "Account number");

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA_OFFSET, GROUP1_YPOS + 9 + 6);
$pdf->Cell(COLUMN1_DATA_WIDTH, 8, $row["project_account_number"], 1, 0, "L");

// Opening date

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS, GROUP2_YPOS);
$pdf->Cell(0, 7, "Opening date");

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA_OFFSET, GROUP2_YPOS);
$pdf->Cell(COLUMN1_DATA_WIDTH, 7, date("d.m.Y", $row["project_opening_date"]), 1, 0, "L");

// Closing date

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS, GROUP2_YPOS + 7 + 6);
$pdf->Cell(0, 7, "Closing date");

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA_OFFSET, GROUP2_YPOS + 7 + 6);
$pdf->Cell(33, 7, date("d.m.Y", $row["project_closing_date"]), 1, 0, "L");


// Budget situation

$pdf->SetFont("arialn", "B", 10);
$pdf->SetXY(COLUMN1_XPOS, GROUP3_YPOS - 10);
$pdf->Cell(0, 4, "BUDGET SITUATION");

// Total budget in current year for responsible

$currency = get_system_currency_symbol();

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA2_OFFSET, GROUP3_YPOS - 3);
$pdf->Cell(0, 3, "In $currency");
$pdf->SetXY(COLUMN1_XPOS, GROUP3_YPOS);
$pdf->MultiCell(0, 3.5, "Total budget c. year\nfor responsible");

$number = number_format($row["project_budget_total"], 2, ".", "'");
$pdf->SetFont("arialn", "", 10);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA2_OFFSET, GROUP3_YPOS);
$pdf->Cell(COLUMN1_DATA2_WIDTH, 7, $number, 1, 0, "C");

// Committed in current year

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS, GROUP3_YPOS + 10);
$pdf->Cell(0, 7, "Committed c. year");

$number = number_format($row["project_budget_committed"], 2, ".", "'");
$pdf->SetFont("arialn", "", 10);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA2_OFFSET, GROUP3_YPOS + 10);
$pdf->Cell(COLUMN1_DATA2_WIDTH, 7, $number, 1, 0, "C");

// Already spent current year

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS, GROUP3_YPOS + 20);
$pdf->Cell(0, 7, "Already spent c. year");

$number = number_format($row["project_budget_spent"], 2, ".", "'");
$pdf->SetFont("arialn", "", 10);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA2_OFFSET, GROUP3_YPOS + 20);
$pdf->Cell(COLUMN1_DATA2_WIDTH, 7, $number, 1, 0, "C");

// Open to commit after this project

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS, GROUP3_YPOS + 30);
$pdf->MultiCell(0, 3.5, "Open to commit after\nthis project");

$number = number_format($row["project_budget_after_project"], 2, ".", "'");
$pdf->SetFont("arialn", "", 10);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA2_OFFSET, GROUP3_YPOS + 30);
$pdf->Cell(COLUMN1_DATA2_WIDTH, 7, $number, 1, 0, "C");

//column 2
// Project number

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS, GROUP1_YPOS);
$pdf->Cell(0, 6, "Project Number");

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, GROUP1_YPOS);
$pdf->Cell(COLUMN2_DATA_WIDTH, 6, $row["project_number"], 1, 0, "L");


// Project legal type

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS, GROUP1_YPOS + 6);
$pdf->Cell(0, 8, "Legal Type");

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, GROUP1_YPOS + 6);
$pdf->Cell(COLUMN2_DATA_WIDTH, 6, $row["project_costtype_text"], 1, 0, "L");

// Project name

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS, GROUP1_YPOS + 6 + 8);
$pdf->Cell(0, 6, "Project Name");

$pdf->SetFont("arialn", "", 7);
$text = limit($row["project_name"], COLUMN2_DATA2_WIDTH);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, GROUP1_YPOS + 6 + 8);
$pdf->Cell(COLUMN2_DATA2_WIDTH, 6, $text, 1, 0, "L");


// Product line

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS, GROUP1_YPOS + 9 + 6 + 9 + 3);
$pdf->Cell(0, 8, "Product line");

$text = $row["product_line_name"];
$pdf->SetFont("arialn", "", 10);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, GROUP1_YPOS + 9 + 6 + 9 + 3);
$pdf->Cell(COLUMN2_DATA2_WIDTH, 8, $text, 1, 0, "C");

// Planned amount in current year

$width1 = round(COLUMN2_DATA2_WIDTH / 6);
$width2 = COLUMN2_DATA2_WIDTH - $width1;

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS, GROUP2_YPOS);
$pdf->Cell(0, 7, "Planned amount cur. year");

$number = round($row["project_planned_amount_current_year"] * $row["currency_exchange_rate"] / $row["currency_factor"]);
$number = number_format($number, 2, ".", "'");
$pdf->SetFont("arialn", "", 10);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, GROUP2_YPOS);
$pdf->Cell($width1, 7, $currency, 1, 0, "C");
$pdf->Cell($width2, 7, $number, 1, 0, "C");

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS, GROUP2_YPOS + 7);
$pdf->Cell(0, 7, "loc. cur.");

$number = number_format($row["project_planned_amount_current_year"], 2, ".", "'");
$pdf->SetFont("arialn", "", 10);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, GROUP2_YPOS + 7);
$pdf->Cell($width1, 7, $row["currency_symbol"], 1, 0, "C");
$pdf->Cell($width2, 7, $number, 1, 0, "C");

// Planned amount next year

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS, GROUP2_YPOS + 7 + 7 + 3);
$pdf->Cell(0, 7, "Planned amount next year");

$number = round($row["project_planned_amount_next_year"] * $row["currency_exchange_rate"] / $row["currency_factor"]);
$number = number_format($number, 2, ".", "'");
$pdf->SetFont("arialn", "", 10);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, GROUP2_YPOS + 7 + 7 + 3);
$pdf->Cell($width1, 7, $currency, 1, 0, "C");
$pdf->Cell($width2, 7, $number, 1, 0, "C");

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS, GROUP2_YPOS + 7 + 7 + 3 + 7);
$pdf->Cell(0, 7, "loc. cur.");

$number = number_format($row["project_planned_amount_next_year"], 2, ".", "'");
$pdf->SetFont("arialn", "", 10);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, GROUP2_YPOS + 7 + 7 + 3 + 7);
$pdf->Cell($width1, 7, $row["currency_symbol"], 1, 0, "C");
$pdf->Cell($width2, 7, $number, 1, 0, "C");

// Share Brand Biel, share other

$width = COLUMN2_DATA2_WIDTH - COLUMN2_DATA_WIDTH;
$ypos = GROUP2_YPOS + 7 + 7 + 3 + 7 + 7 + 3;

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS, $ypos);
$pdf->Cell(0, 7, "Share " . BRAND . " Biel in %");

$pdf->SetFont("arialn", "", 10);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, $ypos);
$pdf->Cell($width, 7, $row["project_share_company"], 1, 0, "C");

$pdf->SetFont("arialn", "B", 7);
$text = "Share other in %";
$text_width = $pdf->GetStringWidth($text) + 4;
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET + COLUMN2_DATA2_WIDTH - $width - $text_width, $ypos);
$pdf->Cell(0, 7, $text);

$pdf->SetFont("arialn", "", 10);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET + COLUMN2_DATA2_WIDTH - $width, $ypos);
$pdf->Cell($width, 7, $row["project_share_other"], 1, 0, "C");

// Approval

$pdf->SetFont("arialn", "B", 10);
$pdf->SetXY(COLUMN2_XPOS, GROUP3_YPOS - 10);
$pdf->Cell(0, 4, "APPROVAL");

// Column headers for approval table

$width = COLUMN2_DATA_OFFSET + COLUMN2_DATA2_WIDTH;
$width1 = floor($width * 0.30);
$width2 = floor($width * 0.20);
$width3 = floor($width * 0.35);
$width4 = $width - $width1 - $width2 - $width3;

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS, GROUP3_YPOS);
$pdf->Cell($width1, 7, "Name", 1, 0, "C");
$pdf->Cell($width2, 7, "", 1, 0, "C");
$pdf->Cell($width3, 7, "Signature", 1, 0, "C");
$pdf->Cell($width4, 7, "Date", 1, 0, "C");
$pdf->SetXY(COLUMN2_XPOS + $width1, GROUP3_YPOS + 1);
$pdf->MultiCell($width2, 2.5, "Deadline for\napproval", 0, "C");

// Data columns for approval table

$ypos = GROUP3_YPOS + 7;
$pdf->SetFont("arialn", "", 7);
$lines = 0;


for ($i = 1; $i <= 7; $i++)
{
    $text = limit($row["project_approval_person" . $i], $width1);
    if ($text)
    {
        $date = $row["project_approval_date" . $i] ? date("d.m.Y", $row["project_approval_date" . $i]) : "";

        $pdf->SetXY(COLUMN2_XPOS, $ypos);
        $pdf->Cell($width1, 6, $text, 1);
        $pdf->Cell($width2, 6, $date, 1, 0, "C");
        $pdf->Cell($width3, 6, "", 1);
        $pdf->Cell($width4, 6, "", 1);
        $lines++;
        $ypos += 6;
    }
}

while($lines < 7)
{
    $pdf->SetXY(COLUMN2_XPOS, $ypos);
    $pdf->Cell($width1, 6, "", 1);
    $pdf->Cell($width2, 6, "", 1, 0, "C");
    $pdf->Cell($width3, 6, "", 1);
    $pdf->Cell($width4, 6, "", 1);
    $lines++;
    $ypos += 6;
}

// Table for description and financial justification

$text_width = COLUMN2_XPOS - COLUMN1_XPOS + COLUMN2_DATA_OFFSET;
$full_width = $text_width + COLUMN2_DATA2_WIDTH;

$pdf->SetFont("arialn", "B", 6);
$pdf->SetTextColor(255, 0, 0);
$pdf->SetXY(COLUMN1_XPOS, JUSTIFICATION_YPOS - 6);
$pdf->Cell(COLUMN1_DATA2_OFFSET, 3, "DESCRIPTION", 0, 2);
$pdf->Cell(COLUMN1_DATA2_OFFSET, 3, "FINANCIAL JUSTIFICATION");

$pdf->SetFont("arialn", "", 6);
$pdf->SetTextColor(0, 0, 0);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA2_OFFSET, JUSTIFICATION_YPOS - 6);
$pdf->Cell(80, 3, "(Goals, Actions, Timing, Quantities)", 0, 2);
$pdf->Cell(80, 3, "(Costs planning by items, return on investment, ...)");

// Business Unit

$pdf->SetFont("arialn", "", 10);
$pdf->SetXY(COLUMN1_XPOS, JUSTIFICATION_YPOS+2);
$pdf->Cell(80,3, "BU " . $row['business_unit_name']);

/*
$str = "Shop in shop with Coastline 2000 in Germany in Augsburg, Juw. Herbert Mayer\n" .
       "Will be reinvoiced at 100% to the Swatch Group Germany\n" .
       "Project Coordinator: Sabrina Hohl\n" .
       "PLEASE BACK TO SABRINA HOHL AFTER SIGNATURES - THANK YOU";

$str = $row["project_approval_description"];

$pdf->SetFont("arialn", "", 10);
$pdf->SetXY(COLUMN1_XPOS, JUSTIFICATION_YPOS + 10);
$pdf->MultiCell($text_width, 4.5, $str);
*/

// new by neo 2003-04-26

$pdf->SetFont("arialn", "", 8);
$ypos = JUSTIFICATION_YPOS+6+4.5;
$justification_cost_sum = 0;

$sql = "select project_financial_justification_description, " .
       "    project_financial_justification_cost " .
       "from project_financial_justifications " .
       "where project_financial_justification_project = " . $row['project_id'] . " " .
       "order by project_financial_justification_priority asc";

$res_2 = mysql_query($sql);
$number_of_description_rows = (mysql_num_rows($res_2) > 10) ? mysql_num_rows($res_2) : 10 ;
$longer = false;

while ($row_2 = mysql_fetch_assoc($res_2))
{
    if (LINE_BREAK)
    {
        $text = $row_2['project_financial_justification_description'];
        while ($text)
        {
            if ($longer)
            {
                $ypos +=4.5;
                $number_of_description_rows++;
            }
            $longer = true;
            $text_out = substr($text,0,60);
            $text = substr($text,60);

            $pdf->SetXY(COLUMN1_XPOS, $ypos);
            $pdf->Cell(JUSTIFICATION_COST_XPOS - COLUMN1_XPOS, 3, 
                   $text_out, 0, "", "L");
        }
    }
    else
    {
        $text = limit($row_2['project_financial_justification_description'], 
                      (JUSTIFICATION_COST_XPOS - COLUMN1_XPOS));
        $pdf->SetXY(COLUMN1_XPOS, $ypos);
        $pdf->Cell(JUSTIFICATION_COST_XPOS - COLUMN1_XPOS, 3, 
                   $text, 0, "", "L");
    }
    
    $pdf->SetXY(JUSTIFICATION_COST_XPOS, $ypos);
    $pdf->Cell((COLUMN1_XPOS + ($full_width - JUSTIFICATION_COST_XPOS)), 3, 
               number_format($row_2['project_financial_justification_cost'],2,".","'"), 0, "", "R");

    if (!LINE_BREAK)
    {
        $ypos +=4.5;
    }
    $justification_cost_sum += $row_2['project_financial_justification_cost'];
}

$pdf->SetLineWidth(0.5);
$pdf->Rect(COLUMN1_XPOS, JUSTIFICATION_YPOS +0.5, $full_width, ($number_of_description_rows + 3) * 4.5);
$pdf->SetLineWidth(0.2);

for ($i = 1; $i <= ($number_of_description_rows +1); $i++)
{
    $pdf->Line(COLUMN1_XPOS, JUSTIFICATION_YPOS + 5 + $i * 4.5, COLUMN1_XPOS + $full_width, JUSTIFICATION_YPOS + 5 + $i * 4.5);
}

$pdf->SetFont("arialn", "", 8);
$pdf->SetXY(JUSTIFICATION_COST_XPOS, JUSTIFICATION_YPOS+6);
$pdf->Cell(0,3, "Cost in CHF",0,0,"L");
$pdf->Line(JUSTIFICATION_COST_XPOS, JUSTIFICATION_YPOS + 6,JUSTIFICATION_COST_XPOS,
           JUSTIFICATION_YPOS + 5 + ($number_of_description_rows + 2) * 4.5);

$pdf->SetXY(COLUMN1_XPOS, JUSTIFICATION_YPOS+6 + ($number_of_description_rows + 1)* 4.5);
$pdf->Cell(JUSTIFICATION_COST_XPOS - COLUMN1_XPOS, 3, "Total",0,"","R");
$pdf->SetXY(JUSTIFICATION_COST_XPOS, JUSTIFICATION_YPOS+6 + ($number_of_description_rows + 1)* 4.5);
$pdf->Cell((COLUMN1_XPOS + ($full_width - JUSTIFICATION_COST_XPOS)), 3, 
            number_format($justification_cost_sum,2,".","'"),0,"","R");

// Table for supplier information

$supplier_ypos = SUPPLIER_YPOS + (($number_of_description_rows - 10) * 4.5);

$pdf->SetLineWidth(0.5);
$pdf->Rect(COLUMN1_XPOS, $supplier_ypos, $full_width, 6 * 4.5);
$pdf->SetLineWidth(0.2);

for ($i = 1; $i <= 5; $i++)
{
    $pdf->Line(COLUMN1_XPOS, $supplier_ypos + $i * 4.5, COLUMN1_XPOS + $full_width, $supplier_ypos + $i * 4.5);
}

$supplier = smart_join(", ", array($row["address_company"], $row["address_company2"]));
$supplier = limit("SUPPLIER: " . $supplier, $full_width);

$pdf->SetFont("arialn", "B", 10);
$pdf->SetXY(COLUMN1_XPOS, $supplier_ypos);
$pdf->Cell($full_width, 4.5, $supplier, 0,2);

$pdf->SetFont("arialn", "", 10);

$address = smart_join(" ", array($row["address_address"], $row["address_address2"]));
$zipplace = smart_join(", ", array($row["address_zip"], $row["address_place"]));
$address = smart_join(", ", array($address, $zipplace, $row["country_name"]));
$address = limit("Address: " . $address, $full_width);

$contact = smart_join(" ", array($row["user_name"], $row["user_firstname"]));
$contact = limit("Contact Person: " . $contact, $full_width);

$phone = limit("Phone: " . $row["user_phone"], $full_width);
$email = limit("Email: " . $row["user_email"], $full_width);

$pdf->Cell($full_width, 4.5, $address, 0, 2);
$pdf->Cell($full_width, 4.5, $contact, 0, 2);
$pdf->Cell($full_width, 4.5, $phone, 0, 2);
$pdf->Cell($full_width, 4.5, $email, 0, 2);

$pdf->Output();

function limit($str, $length)
{
    global $pdf;

    $length -= 2;

    while (true)
    {
        if ($pdf->GetStringWidth($str) < $length)
        {
            return $str;
        }
        else
        {
            $str = substr($str, 0, strlen($str) - 1);
        }
    }
}

?>