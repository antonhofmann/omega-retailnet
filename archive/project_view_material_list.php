<?php
/********************************************************************

    project_view_material_list.php

    Edit List of Materials

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-09
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-12-04
    Version:        1.2.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_view_list_of_materials_in_projects");

register_param("pid");
set_referer("project_view_material_list_view_item.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

// get System currency
$system_currency = get_system_currency_fields();

// create sql for oder items
$sql_order_items = "select order_item_id, order_item_text, order_item_quantity, ".
                   "    order_item_po_number, order_item_system_price, item_id, ".
                   "    if(order_item_not_in_budget=1 , 'no', 'yes') as order_item_in_budget, ".
                   "    if(item_code <>'', item_code, item_type_name) as item_shortcut, ".
                   "    category_priority, category_name, ".
                   "    addresses.address_shortcut as supplier_company, ".
                   "    addresses_1.address_shortcut as forwarder_company, ".
                   "    item_type_id, item_type_name, ".
                   "    item_type_priority, unit_name ".
                   "from order_items ".
                   "left join items on order_item_item = item_id ".
                   "left join categories on order_item_category = category_id ".
                   "left join addresses on order_item_supplier_address = addresses.address_id ".
                   "left join addresses as addresses_1 ".
                   "     on order_item_forwarder_address = addresses_1.address_id ".
                   "left join item_types on order_item_type = item_type_id " .
				   "left join units on unit_id = item_unit";


$where_clause = " where (order_item_type = " . ITEM_TYPE_STANDARD .
                "    or order_item_type = " . ITEM_TYPE_SERVICES .
                "    or order_item_type = " . ITEM_TYPE_SPECIAL . ") ".
                "    and order_item_order = " . $project["project_order"];

$values = array();
$res = mysql_query($sql_order_items . $where_clause) or dberror($sql_order_items . $where_clause);
while ($row = mysql_fetch_assoc($res))
{
    $values[$row["order_item_id"]] = $row["order_item_quantity"];
}


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project");

$form->add_section("Project");
$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid",$project["project_order"]);

require_once "include/project_head_small.php";

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Create List for Catalog Items
*********************************************************************/ 
$list1 = new ListView($sql_order_items);

$list1->set_title("Catalog Items");
$list1->set_entity("order_item");
$list1->set_filter("(order_item_type = " . ITEM_TYPE_STANDARD . " or order_item_type = " . ITEM_TYPE_SERVICES . ") and order_item_order = " . $project["project_order"]);
$list1->set_order("supplier_company, item_code");
$list1->set_group("category_priority", "category_name");

$link="project_view_material_list_view_item.php?pid=" . param("pid");

$list1->add_column("item_shortcut", "Item Code", $link);

$list1->add_column("order_item_in_budget", "in Budget", "", "", "", COLUMN_NO_WRAP | COLUMN_ALIGN_CENTER);
$list1->add_column("order_item_text", "Name");

$list1->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
$list1->add_column("unit_name", "Unit", "", "", "", COLUMN_NO_WRAP);

$list1->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list1->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);
$list1->add_column("supplier_company", "Supplier");
$list1->add_column("forwarder_company", "Forwarder");


/********************************************************************
    Create List for Special Items
*********************************************************************/ 
$list2 = new ListView($sql_order_items);

$list2->set_title("Special Items");
$list2->set_entity("order_item");
$list2->set_filter("order_item_type = " . ITEM_TYPE_SPECIAL . " and order_item_order = " . $project["project_order"]);
$list2->set_order("supplier_company, order_item_text");

$link="project_view_material_list_view_item.php?pid=" . param("pid");

$list2->add_column("item_shortcut", "Item Code", $link);

$list2->add_column("order_item_in_budget", "in Budget", "", "", "", COLUMN_NO_WRAP | COLUMN_ALIGN_CENTER);
$list2->add_column("order_item_text", "Name");

$list2->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);

$list2->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
$list2->add_column("order_item_po_number", "P.O. Number");
$list2->add_column("supplier_company", "Supplier");
$list2->add_column("forwarder_company", "Forwarder");


/********************************************************************
    Create List for Cost Estimation Positions
*********************************************************************/ 
$list3 = new ListView($sql_order_items);

$list3->set_title("Cost Estimation");
$list3->set_entity("order_item");
$list3->set_filter("order_item_type = " . ITEM_TYPE_COST_ESTIMATION . " and order_item_order = " . $project["project_order"]);
$list3->set_order("item_code");

$list3->add_column("item_shortcut", "Item Code");

$list3->add_column("order_item_text", "Name");
$list3->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list1->populate();
$list1->process();

$list2->populate();
$list2->process();

$list3->populate();
$list3->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("View List of Materials");
$form->render();
$list1->render();
$list2->render();
$list3->render();
$page->footer();

?>