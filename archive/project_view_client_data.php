<?php
/********************************************************************

    project_view.php

    See the projec details.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-03
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-02-18
    Version:        1.0.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";

check_access("can_view_client_data_in_projects");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

// get order's currency
$currency = get_order_currency($project["project_order"]);

// read design_item_ids from project items
$project_design_objective_item_ids = get_project_design_objective_item_ids(param("pid"));

// read information from order_addresses
$delivery_address = get_order_address(2, $project["project_order"]);

// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ".
                 "order by country_name";


$billing_address_province_name = "";
if($project["order_billing_address_place_id"])
{
	$billing_address_province_name = get_province_name($project["order_billing_address_place_id"]);
}

$delivery_address_province_name = "";
if($delivery_address["place_id"])
{
	$delivery_address_province_name = get_province_name($delivery_address["place_id"]);
}


/********************************************************************
    Get Cost Monitoring Sheet Budget Data
*********************************************************************/ 
$sql =  "select * from project_costs " .
        "where project_cost_order = " . $project["project_order"];

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

$project_cost_sqms = $row["project_cost_sqms"];

/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");


$form->add_lookup("project_number", "Project Number", "projects", "project_number", 0, param("pid"));
$form->add_lookup("product_line", "Product Line", "product_lines", "product_line_name", 0, $project["project_product_line"]);
$form->add_lookup("product_line_subclass", "Product Line Subclass", "productline_subclasses", "productline_subclass_name", 0, $project["project_product_line_subclass"]);
$form->add_lookup("project_postype", "POS Type", "postypes", "postype_name", 0, $project["project_postype"]);
$form->add_lookup("project_pos_subclass", "POS Type Subclass", "possubclasses", "possubclass_name", NOTNULL, $project["project_pos_subclass"]);

$form->add_label("type3", "Project Legal Type", 0, $project["project_costtype_text"]);
$form->add_label("project_projectkind", "Project Kind", 0, $project["projectkind_name"]);

$order_state_name = get_actual_order_state_name($project["order_actual_order_state_code"], 1);
$form->add_label("status", "Project State", 0, $project["order_actual_order_state_code"]  . " " . $order_state_name);

$project_state_name = get_project_state_name($project["project_state"]);
$form->add_label("status2", "Treatment State", 0, $project_state_name);


if ($project["project_retail_coordinator"])
{
    $form->add_lookup("retail_coordinator", "Project Manager", "users", "concat(user_name, ' ', user_firstname)", 0, $project["project_retail_coordinator"]);
}
else
{
    $form->add_label("retail_coordinator", "Project Manager");
}




$form->add_section("Client Address");
$form->add_label("client_address_company", "Company", 0, $client_address["company"]);


if ($client_address["company2"])
{
    $form->add_label("client_address_company2", "", 0, $client_address["company2"]);
}


$form->add_label("client_address_address", "Address", 0, $client_address["address"]);


if ($client_address["address2"])
{
    $form->add_label("client_address_address2", "", 0, $client_address["address2"]);
}


$form->add_label("client_address_place", "City", 0, $client_address["zip"] . " " . $client_address["place"]);
$form->add_lookup("client_address_country", "", "countries", "country_name", 0, $client_address["country"]);


$line = "concat(user_name, ' ', user_firstname)";
$form->add_lookup("client_address_contact", "Contact", "users", $line , 0, $project["order_user"]);
$form->add_lookup("client_address_phone", "Phone", "users", "user_phone", 0, $project["order_user"]);
$form->add_lookup("client_address_fax", "Fax", "users", "user_fax", 0, $project["order_user"]);
$form->add_lookup("client_address_email", "Email", "users", "user_email", 0, $project["order_user"]);


if($project["project_cost_type"] == 2) // Franchisee
{
	//Franchisee Address
	$form->add_section("Franchisee Address");
	$form->add_label("franchisee_address_company", "Company", 0, $project["order_franchisee_address_company"]);


	if ($project["order_franchisee_address_company2"])
	{
		$form->add_label("franchisee_address_company2", "", 0, $project["order_franchisee_address_company2"]);
	}


	$form->add_label("franchisee_address_address", "Address", 0, $project["order_franchisee_address_address"]);


	if ($project["order_franchisee_address_address2"])
	{
		$form->add_label("franchisee_address_address2", "", 0, $project["order_franchisee_address_address2"]);
	}

	$form->add_label("franchisee_address_place", "City", 0, $project["order_franchisee_address_zip"] . " " . $project["order_franchisee_address_place"]);
	$form->add_lookup("franchisee_address_country", "", "countries", "country_name", 0, $project["order_franchisee_address_country"]);


	$form->add_label("franchisee_address_phone", "Phone", 0, $project["order_franchisee_address_phone"]);
	$form->add_label("franchisee_address_fax", "Fax", 0, $project["order_franchisee_address_fax"]);
	$form->add_label("franchisee_address_email", "Email", 0, $project["order_franchisee_address_email"]);
}


if($project["project_projectkind"] != 4 and $project["project_projectkind"] != 5) // take over, lease renewal
{
	//Notify Address
	$form->add_section("Notify Address)");
	$form->add_label("billing_address_company", "Company", 0, $project["order_billing_address_company"]);


	if ($project["order_billing_address_company2"])
	{
		$form->add_label("billing_address_company2", "", 0, $project["order_billing_address_company2"]);
	}


	$form->add_label("billing_address_address", "Address", 0, $project["order_billing_address_address"]);


	if ($project["order_billing_address_address2"])
	{
		$form->add_label("billing_address_address2", "", 0, $project["order_billing_address_address2"]);
	}


	$form->add_label("billing_address_place", "City", 0, $project["order_billing_address_zip"] . " " . $project["order_billing_address_place"]);
	$form->add_label("billing_address_province_name", "Province", 0, $billing_address_province_name);
	$form->add_label("billing_address_country_name", "Country", 0, $project["order_billing_address_country_name"]);


	$form->add_label("billing_address_phone", "Phone", 0, $project["order_billing_address_phone"]);
	$form->add_label("billing_address_fax", "Fax", 0, $project["order_billing_address_fax"]);
	$form->add_label("billing_address_email", "Email", 0, $project["order_billing_address_email"]);


	$form->add_section("Delivery Address (consignee address)");
	$form->add_label("delivery_address_company", "Company", 0, $delivery_address["company"]);


	if ($delivery_address["company2"])
	{
		$form->add_label("delivery_address_company2", "", 0, $delivery_address["company2"]);
	}


	$form->add_label("delivery_address_address", "Address", 0, $delivery_address["address"]);


	if ($delivery_address["address2"])
	{
		$form->add_label("delivery_address_address2", "", 0, $delivery_address["address2"]);
	}


	$form->add_label("delivery_address_place", "City", 0, $delivery_address["zip"] . " " . $delivery_address["place"]);
	$form->add_label("delivery_address_province_name", "Province", 0, $delivery_address_province_name);

	$form->add_lookup("delivery_address_country", "Country", "countries", "country_name", 0, $delivery_address["country"]);


	$form->add_label("delivery_address_phone", "Phone", 0, $delivery_address["phone"]);
	$form->add_label("delivery_address_fax", "Fax", 0, $delivery_address["fax"]);
	$form->add_label("delivery_address_email", "Email", 0, $delivery_address["email"]);
}


$form->add_section("POS Location Address");
$form->add_label("shop_address_company", "Project Name", 0, $project["order_shop_address_company"]);


if ($project["order_shop_address_company2"])
{
    $form->add_label("shop_address_company2", "", 0, $project["order_shop_address_company2"]);
}


$form->add_label("shop_address_address", "Address", 0, $project["order_shop_address_address"]);


if ($project["order_shop_address_address2"])
{
    $form->add_label("shop_address_address2", "", 0, $project["order_shop_address_address2"]);
}

$form->add_label("shop_address_place", "City", 0, $project["order_shop_address_zip"] . " " . $project["order_shop_address_place"]);
$form->add_lookup("shop_address_country", "", "countries", "country_name", 0, $project["order_shop_address_country"]);
$form->add_label("shop_address_phone", "Phone", 0, $project["order_shop_address_phone"]);
$form->add_label("shop_address_fax", "Fax", 0, $project["order_shop_address_fax"]);
$form->add_label("shop_address_email", "Email", 0, $project["order_shop_address_email"]);

if($project["project_projectkind"] != 4 and $project["project_projectkind"] != 5) // take over, lease renewal
{
	$form->add_section("Sales Surface");
	$form->add_label("shop_sqms", "Sales Surface sqms", 0, $project_cost_sqms);


	$form->add_section("Location Info");
	if ($project["project_location_type"])
	{
		$form->add_lookup("location_type_name", "Location Type", "location_types", "location_type_name", 0, $project["project_location_type"]);
		$form->add_lookup("location_type", "", "projects", "project_location", HIDEEMPTY, param("pid"));
	}
	else
	{
		$form->add_lookup("location_type", "Location Type", "projects", "project_location", HIDEEMPTY, param("pid"));
	}

	$form->add_lookup("voltage_name", "Voltage", "voltages", "voltage_name", 0, $project["order_voltage"]);

	$form->add_section("Furniture Height");
	$form->add_label("furniture_height", "Furniture Height in mm", 0, $project["project_furniture_height_mm"] . ' mm');


	// read design_item_ids from project items
	$sql = "select project_item_item, design_objective_group_name ".
		   "from project_items ".
		   "left join design_objective_items on project_item_item = design_objective_item_id ".
		   "left join design_objective_groups on design_objective_item_group = design_objective_group_id ".
		   "where project_item_project  = " . param("pid") ." ".
		   "order by design_objective_group_priority, design_objective_item_priority";


	$res = mysql_query($sql) or dberror($sql);


	$i=1;
	$group="";
	while ($row = mysql_fetch_assoc($res))
	{
		if($i == 1)
		{
			$form->add_section("Design Objectives");
		}
		
		if ($group != $row["design_objective_group_name"])
		{
			$form->add_lookup("design_objective_item" . $i, $row["design_objective_group_name"], "design_objective_items", "design_objective_item_name", 0, $row["project_item_item"]);
			$group = $row["design_objective_group_name"];
		}
		else
		{
			$form->add_lookup("design_objective_item" . $i, "", "design_objective_items", "design_objective_item_name", 0, $row["project_item_item"]);
		}
		$i++;
	}




	$form->add_section("Capacity Request by Client");
	$form->add_label("watches_displayed", "Watches Displayed", 0, $project["project_watches_displayed"]);
	$form->add_label("watches_stored", "Watches Stored", 0, $project["project_watches_stored"]);
	$form->add_label("bijoux_displayed", "Bijoux Displayed", 0, $project["project_bijoux_displayed"]);
	$form->add_label("bijoux_stored", "Bijoux Stored", 0, $project["project_bijoux_stored"]);


	$form->add_section("Preferences and Traffic Checklist");
	$form->add_label("preferred_delivery_date", "Preferred Arrival Date", 0, to_system_date($project["order_preferred_delivery_date"]));
	
	
	$form->add_lookup("preferred_transportation_arranged", "Transportation arranged by", "transportation_types", "transportation_type_name", 0, $project["order_preferred_transportation_arranged"]);

	$form->add_lookup("preferred_transportation_mode", "Transportation mode", "transportation_types", "transportation_type_name", 0, $project["order_preferred_transportation_mode"]);
	

	/*
	$value="no";
	if ($project["order_packaging_retraction"])
	{
		$value="yes";
	}
	$form->add_label("order_packaging_retraction", "Packaging Retraction Desired", 0, $value);
	*/

	$value="no";
	if ($project["order_pedestrian_mall_approval"])
	{
		$value="yes";
	}
	$form->add_label("pedestrian_mall_approval", "Pedestrian Area Approval Needed", 0, $value);


	$value="no";
	if ($project["order_full_delivery"])
	{
		$value="yes";
	}
	$form->add_label("full_delivery", "Full Delivery Desired", 0, $value);


	$form->add_label("delivery_comments", "Delivery Comments", 0, $project["order_delivery_comments"]);


	$form->add_section("Insurance");
	if($project["order_insurance"] == 1)
	{
		$form->add_label("order_insurance", "Insurance by " . BRAND . "/Forwarder", 0, "covered");
	}
	else
	{
		$form->add_label("order_insurance", "Insurance by " . BRAND . "/Forwarder", 0, "not covered");
	}


	$form->add_section("Other Information");
	$form->add_label("approximate_budget", "Approximate Budget in " . $currency["symbol"], 0, $project["project_approximate_budget"]);
	$form->add_label("project_opening_date", "Preferred Opening Date", 0, to_system_date($project["project_planned_opening_date"]));
	$form->add_label("project_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]));
	$form->add_label("project_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]));

	$form->add_section("General Comments");
	$form->add_label("general_comments", "General Comments", 0, $project["project_comments"]);

}




/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


  
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");


require "include/project_page_actions.php";


$page->header();
$page->title("View Project's Client Data");
$form->render();
$page->footer();

?>