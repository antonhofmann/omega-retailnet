<?php
/********************************************************************

    project_page_actions.php

    Defines the possible page actions depending on access rights

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-25
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-12-03
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "order_state_constants.php";


if (has_access("can_edit_retail_data"))
{   
	$page->register_action('edit_retail_data', 'Edit Retail Data', "project_edit_retail_data.php?pid=" . param("pid") . "&y1=" . param("y1") . "&y2=" . param("y2"));
}
elseif(has_access("can_view_client_data_in_projects"))
{
	$page->register_action('view_retail_data', 'View Retail Data', "project_view_retail_data.php?pid=" . param("pid") . "&y1=" . param("y1") . "&y2=" . param("y2"));
}

/*
if (has_access("has_access_to_construction_data") and $project["project_state"] != 2)
{   
    $page->register_action('construction_data', 'Construction Data', "project_view_construction_data.php?pid=" . param("pid"));
}
*/

if (has_access("can_edit_pos_data"))
{   
	$page->register_action('edit_pos_data', 'Edit POS Data', "project_edit_pos_data.php?pid=" . param('pid'));
}
elseif (has_access("can_view_pos_data") and $project["project_state"] != 2)
{   

	$page->register_action('view_pos_data', 'View POS Data', 
		                       "project_view_pos_data.php?pid=" . param('pid'));

}


if (has_access("can_edit_client_data_in_projects"))
{   
    
	$page->register_action('view_client_data', 'View Client Data', 
                           "project_view_client_data.php?pid=" . param('pid'));

	$page->register_action('edit_client_data', 'Edit Request', 
                           "project_edit_client_data.php?pid=" . param('pid'));
}
elseif (has_access("can_view_client_data_in_projects"))
{   
    $page->register_action('view_client_data', 'View Client Data', 
                           "project_view_client_data.php?pid=" . param('pid'));

}

$page->register_action('dummy1', '');


if (has_access("can_use_taskcentre_in_projects"))
{   
    $page->register_action('flow_control', 'Task Centre', "project_task_center.php?pid=" . param("pid") . "&y1=" . param("y1") . "&y2=" . param("y2"));
}


if (has_access("can_edit_list_of_materials_in_projects") or has_access("can_view_budget_in_projects"))
{   
	$page->register_action('project_costs', 'Project Costs', "project_costs_overview.php?pid=" . param("pid"));
}


if (has_access("has_access_to_cer"))
{
	//check if project has a cer or af
	$sql = "select order_number, order_shop_address_place, country_name, order_shop_address_company, " .
		   "project_postype, project_projectkind, project_cost_type " . 
		   "from projects " .
		   "left join orders on order_id = project_order " .
		   "left join countries on country_id = order_shop_address_country " .
		   "left join project_costs on project_cost_order = order_id " .
		   "where project_id = " .  dbquote(param('pid'));

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$sql = "select posproject_type_needs_cer, posproject_type_needs_af, posproject_type_needs_inr03 " . 
			   "from posproject_types " . 
			   "where posproject_type_postype =  " . dbquote($row["project_postype"]) .
			   " and posproject_type_projectcosttype =  " . dbquote($row["project_cost_type"]) .
			   " and posproject_type_projectkind =  " . dbquote($row["project_projectkind"]);

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			if($row["posproject_type_needs_cer"] == 1 
				or $row["posproject_type_needs_af"] == 1
				or $row["posproject_type_needs_inr03"] == 1)
			{
				$url = "/cer/cer_project.php?pid=". param('pid') . "&amp;id=" .  param('pid');
				$page->register_action('cer', 'Access CER/AF', $url, "_blank");
			}
		}
	}
}

$page->register_action('dummy3', '');


if($project["project_projectkind"] != 4 and $project["project_projectkind"] != 5) // take over, lease renewal
{

	if (has_access("can_edit_project_sheet"))
	{   
		$project = get_project(param("pid"));

		
		$date_created = $project["date_created"];
		if($date_created < '2004-09-27' and $project["project_use_ps2004"] != 1)
		{
			$url = "http://" . $_SERVER["HTTP_HOST"] . "/archive/project_sheet_pdf.php?id=" . $project["project_order"];
		}
		else
		{
			$url = "http://" . $_SERVER["HTTP_HOST"] . "/archive/project_sheet_pdf_2004.php?id=" . $project["project_order"] . "&pid=" . param("pid");
		}
		
		$page->register_action('view_project_sheet', 'View Project Sheet', $url, "_blank");
		  
	}

	/*
	if (has_access("can_view_ordered_values_in_projects"))
	{   
		$page->register_action('edit_view_ordered_values', 'View Ordered Values', 
							   "project_view_ordered_values.php?pid=" . param("pid"));
	}
	

	if($project["order_archive_date"] >= '2013-09-01')
	{
		if (has_access("can_view_project_invoice_information"))
		{   
			$page->register_action('view_cost_information', 'View Cost Information', 
								   "project_view_cost_information.php?pid=" . param("pid"));
		}
	}


	if (has_access("can_view_delivery_schedule_in_projects"))
	{
		$page->register_action('view_traffic_data', 'View Delivery Schedule', 
							   "project_view_traffic_data.php?pid=" .param('pid') . "&y1=" . param("y1") . "&y2=" . param("y2"));
	}


	
	*/
}

$page->register_action('dummy4', '');

if (has_access("can_view_client_data_in_projects")
   or has_access("has_access_to_all_projects")
   or has_access("can_view_budget_in_projects")
   or has_access("can_view_delivery_schedule_in_projects")
   or has_access("can_edit_project_sheet")
   or (has_access("can_edit_cost_monitoring") and $project["project_state"] != 2))
{
	$page->register_action('print', 'Print', "projects_print.php?pid=" . param("pid"));
	$page->register_action('dummy5', '');
}


if (has_access("can_view_milestones"))
{   
    $page->register_action('view_milestones', 'Project Milestones', "project_view_milestones.php?pid=" . param("pid"));    
}

if (has_access("can_view_comments_in_projects"))
{   
    $page->register_action('view_comments', 'Comments', "project_view_comments.php?pid=" . param("pid") . "&y1=" . param("y1") . "&y2=" . param("y2"));
}

if (has_access("can_view_attachments_in_projects"))
{   
    $page->register_action('view_attachments', 'Attachments', "project_view_attachments.php?pid=" . param("pid") . "&y1=" . param("y1") . "&y2=" . param("y2"));
}



if (has_access("can_view_history_in_projects"))
{   
    $page->register_action('history', 'View History', "project_history.php?pid=" . param("pid") . "&y1=" . param("y1") . "&y2=" . param("y2"));
    $page->register_action('mail_history', 'View Mails', "project_mail_history.php?pid=" . param("pid") . "&y1=" . param("y1") . "&y2=" . param("y2"));

	
}

$page->register_action('dummy6', '');
//$link = "projects_archive_projects.php?y1=" . param("y1") . "&y2=" . param("y2");
//$page->register_action('back', 'Back To List', $link);
//$page->register_action('orders', 'Orders', "orders_archive.php");
$page->register_action('projects', 'Projects', "projects_archive.php");
$page->register_action('home', 'Home', "../user/welcome.php");
?>