<?php

$page->add_tab("oveview", "Overview", "project_costs_overview.php?pid=" . param("pid"), $target = "_self", $flags = 0);

if (has_access("can_edit_cost_monitoring"))
{
	$page->add_tab("real", "Real Costs", "project_costs_real_costs.php?pid=" . param("pid"), $target = "_self", $flags = 0);
}

if (has_access("can_edit_local_constrction_work"))
{
	$page->add_tab("bids_cw", "Bids", "project_costs_bids.php?pid=" . param("pid"), $target = "_self", $flags = 0);
}
$page->add_tab("attachments", "Attachments", "project_costs_attachments.php?pid=" . param("pid"), $target = "_self", $flags = 0);


$page->tabs();

?>