<?php
/********************************************************************

    save_functions.php

    Various utility functions to save information into tables

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-08
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-02-23
    Version:        1.2.4

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

/********************************************************************
   update cost monitoring information for items
*********************************************************************/
function  project_update_order_item_cost1($list, $nocom = true)
{
    foreach ($list->values("order_item_cost_group") as $key=>$value)
    {
        // update record
        if(!$value) {$value = 0;}
        $order_item_fields = array();

        $order_item_fields[] = "order_item_cost_group = " . $value;

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);
    }


    foreach ($list->values("order_item_quantity") as $key=>$value)
    {
        // update record
        if(!$value) {$value = 0;}
        $order_item_fields = array();

        $order_item_fields[] = "order_item_quantity = " . $value;

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);

    }


    foreach ($list->values("order_item_real_system_price") as $key=>$value)
    {
        // update record
        if(!$value) {$value = 0;}
    
        $order_item_fields = array();

        if($value > 0) {
			$order_item_fields[] = "order_item_system_price = " . $value;
		}

		$order_item_fields[] = "order_item_real_system_price = " . $value;

        $value1 = "current_timestamp";
        $project_fields[] = "date_modified = " . $value1;

        if (isset($_SESSION["user_login"]))
        {
            $value1 = dbquote($_SESSION["user_login"]);
            $project_fields[] = "user_modified = " . $value1;
        }
   
        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);
    }

    /*
    if($project["order_date"] > "2005-10-31")
    {
        //update budget approved
        if(isset($list["order_item_quantity_freezed"]))
        {
            foreach ($list->values("order_item_quantity_freezed") as $key=>$value)
            {
                // update record
                if(!$value) {$value = 0;}
                $order_item_fields = array();

                $order_item_fields[] = "order_item_quantity_freezed = " . $value;

                $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
                mysql_query($sql) or dberror($sql);
            }

            foreach ($list->values("order_item_system_price_freezed") as $key=>$value)
            {
                // update record
                if(!$value) {$value = 0;}
            
                $order_item_fields = array();

                $order_item_fields[] = "order_item_system_price_freezed = " . $value;

                $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
                mysql_query($sql) or dberror($sql);
            }
        }
    }
	*/
	
	if($nocom == true)
	{
		foreach ($list->values("order_items_costmonitoring_group") as $key=>$value)
		{
			// update record
			if(!$value) {$value = 0;}
		
			$order_item_fields = array();

			$order_item_fields[] = "order_items_costmonitoring_group = " . $value;

			$value1 = "current_timestamp";
			$project_fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = dbquote($_SESSION["user_login"]);
				$project_fields[] = "user_modified = " . $value1;
			}
	   
			$sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
			mysql_query($sql) or dberror($sql);
		}
	}
}


/********************************************************************
   update cost monitoring information for otehr cost
*********************************************************************/
function  project_update_order_item_cost2($list)
{
    
    foreach ($list->values("order_item_cost_group") as $key=>$value)
    {
        // update record
        if(!$value) {$value = 0;}
        $order_item_fields = array();

        $order_item_fields[] = "order_item_cost_group = " . $value;

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);
    }

    /*
	foreach ($list->values("order_item_supplier_freetext") as $key=>$value)
    {
        // update record

        $order_item_fields = array();

        $order_item_fields[] = "order_item_supplier_freetext = " . dbquote($value);

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);

    }
	*/

    foreach ($list->values("order_item_real_system_price") as $key=>$value)
    {
        // update record
        if(!$value) {$value = 0;}

        $order_item_fields = array();

        if($value > 0) {
			$order_item_fields[] = "order_item_system_price = " . $value;
		}

		$order_item_fields[] = "order_item_real_system_price = " . $value;

        $value1 = "current_timestamp";
        $project_fields[] = "date_modified = " . $value1;

        if (isset($_SESSION["user_login"]))
        {
            $value1 = dbquote($_SESSION["user_login"]);
            $project_fields[] = "user_modified = " . $value1;
        }
   
        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);

    }

    /*
	if($project["order_date"] > "2005-10-31")
    {
        //budget approved
        foreach ($list->values("order_item_system_price_freezed") as $key=>$value)
        {
            // update record
            if(!$value) {$value = 0;}

            $order_item_fields = array();

            $order_item_fields[] = "order_item_system_price_freezed = " . $value;

            $value1 = "current_timestamp";
            $project_fields[] = "date_modified = " . $value1;

            if (isset($_SESSION["user_login"]))
            {
                $value1 = dbquote($_SESSION["user_login"]);
                $project_fields[] = "user_modified = " . $value1;
            }
       
            $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
            mysql_query($sql) or dberror($sql);
        }
    }
	*/
}



/********************************************************************
   update cost monitoring information for items: reinvoice data
*********************************************************************/
function  project_update_order_item_reinvoice_data_0($list)
{
    
    $pos = array();
    $sups = array();

    $sql = $list->sql;

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $pos[] = $row["order_item_po_number"];
        $sups[] = $row["order_item_supplier_address"];

    }
    
    $i = 0;
    foreach ($list->values("order_item_reinvoiced") as $key=>$value)
    {
           
           if($i < count($pos))
           {
               // update record
                $order_item_fields = array();
                if($value)
                {
                    $order_item_fields[] = "order_item_reinvoiced = " . dbquote(from_system_date($value));

                    $sql = "update order_items set " . join(", ", $order_item_fields) . 
                " where order_item_po_number = " . dbquote($pos[$i]) . 
                " and order_item_supplier_address = " . dbquote($sups[$i]);
                mysql_query($sql) or dberror($sql);
                }

           }
            $i++;
    }

    $i = 0;
    foreach ($list->values("order_item_reinvoicenbr") as $key=>$value)
    {
           
           if($i < count($pos))
           {
               // update record
                $order_item_fields = array();

                $order_item_fields[] = "order_item_reinvoicenbr = " . dbquote(trim($value));

                $sql = "update order_items set " . join(", ", $order_item_fields) . 
                " where order_item_po_number = " . dbquote($pos[$i]) . 
                " and order_item_supplier_address = " . dbquote($sups[$i]);
                mysql_query($sql) or dberror($sql);
           }
            $i++;
    }


	$i = 0;
    foreach ($list->values("order_item_reinvoiced2") as $key=>$value)
    {
           
           if($i < count($pos))
           {
               // update record
                $order_item_fields = array();
                if($value)
                {
                    $order_item_fields[] = "order_item_reinvoiced2 = " . dbquote(from_system_date($value));

                    $sql = "update order_items set " . join(", ", $order_item_fields) . 
                " where order_item_po_number = " . dbquote($pos[$i]) . 
                " and order_item_supplier_address = " . dbquote($sups[$i]);
                mysql_query($sql) or dberror($sql);
                }

           }
            $i++;
    }

    $i = 0;
    foreach ($list->values("order_item_reinvoicenbr2") as $key=>$value)
    {
           
           if($i < count($pos))
           {
               // update record
                $order_item_fields = array();

                $order_item_fields[] = "order_item_reinvoicenbr2 = " . dbquote(trim($value));

                $sql = "update order_items set " . join(", ", $order_item_fields) . 
                " where order_item_po_number = " . dbquote($pos[$i]) . 
                " and order_item_supplier_address = " . dbquote($sups[$i]);
                mysql_query($sql) or dberror($sql);
           }
            $i++;
    }
}


/********************************************************************
   update cost monitoring information for items: reinvoice data
*********************************************************************/
function  project_update_order_item_reinvoice_data($list)
{
    
    foreach ($list->values("order_item_reinvoiced") as $key=>$value)
    {
        // update record
        
            $order_item_fields = array();

            if($value)
            {
                $order_item_fields[] = "order_item_reinvoiced = " . dbquote(from_system_date($value));

                $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
                mysql_query($sql) or dberror($sql);
            }

            

    }

    foreach ($list->values("order_item_reinvoicenbr") as $key=>$value)
    {
        // update record
        $order_item_fields = array();

        $order_item_fields[] = "order_item_reinvoicenbr = " . dbquote(trim($value));

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);
    }

	foreach ($list->values("order_item_reinvoiced2") as $key=>$value)
    {
        // update record
        
            $order_item_fields = array();

            if($value)
            {
                $order_item_fields[] = "order_item_reinvoiced2 = " . dbquote(from_system_date($value));

                $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
                mysql_query($sql) or dberror($sql);
            }

            

    }

    foreach ($list->values("order_item_reinvoicenbr2") as $key=>$value)
    {
        // update record
        $order_item_fields = array();

        $order_item_fields[] = "order_item_reinvoicenbr2 = " . dbquote(trim($value));

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);
    }

}

/********************************************************************
   update cost monitoring information for items: cost group data
*********************************************************************/
function  project_update_order_item_group_data($list, $order_id)
{

    foreach ($list->values("order_items_monitoring_group") as $key=>$value)
    {

        // update record
        $order_item_fields = array();

        if($value)
		{
			$order_item_fields[] = "order_items_monitoring_group = " . dbquote($value);
		}
		else
		{
			$order_item_fields[] = "order_items_monitoring_group = NULL";
		}

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);
    }

    // update all empty fields with th entry the user made for the sam supplier

    $sql = "select distinct order_item_supplier_address, " .
           "order_items_monitoring_group  " .
           "from order_items " .
           "where order_item_order = " . $order_id .
           "   and order_items_monitoring_group <>'' ";

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $monitoring_group = $row["order_items_monitoring_group"];
        $supplier = $row["order_item_supplier_address"];

        $order_item_fields = array();

        $order_item_fields[] = "order_items_monitoring_group = " . dbquote($monitoring_group);

        $sql = "update order_items set " . join(", ", $order_item_fields) . 
        "  where order_item_order = " . $order_id .
        "   and order_item_supplier_address = " . $supplier . 
        "   and (order_items_monitoring_group  = '' or order_items_monitoring_group is null) ";

        mysql_query($sql) or dberror($sql);
    }

}

/********************************************************************
    freeze budget
*********************************************************************/
function freeze_budget($order_id)
{
    
    $budget = 0;

    $sql =  "select order_item_id, order_item_quantity, " .
            "order_item_system_price, order_item_client_price " .
            "from order_items " . 
            " where order_item_not_in_budget <> 1 " .
            "  and order_item_order = " . $order_id;

    $res = mysql_query($sql) or dberror($sql);
    while($row = mysql_fetch_assoc($res))
    {
        if($row["order_item_quantity"] > 0)
        {
            $budget = $budget + $row["order_item_quantity"] * $row["order_item_system_price"];
        }
        else
        {
            $budget = $budget + $row["order_item_system_price"];
        }

        $quantity = $row["order_item_quantity"];
        $sprice = $row["order_item_system_price"];
        $cprice = $row["order_item_client_price"];

        if(!$quantity){$quantity = 0;}
        if(!$sprice){$sprice = 0;}
        if(!$cprice){$cprice = 0;}

        $sql_u = "Update order_items set " .
                 "order_item_in_freezed_budget = 1, " .
                 "order_item_quantity_freezed = " . $quantity  . ", " .
                 "order_item_system_price_freezed = " . $sprice . ", " .
                 "order_item_client_price_freezed = " . $cprice . " " .
                 "where order_item_id = " . $row["order_item_id"];
        
        $result = mysql_query($sql_u) or dberror($sql_u);
    }


    //check if the cost monitoring sheet exists already
    $sql = "select count(project_cost_id) as num_recs " .
           "from project_costs " .
           "where project_cost_order = "  . $order_id; 

    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);

    if($row["num_recs"] == 0)
    {
        
        $fields = array();
        $values = array();

        $fields[] = "project_cost_order";
        $values[] = $order_id;

        $fields[] = "date_created";
        $values[] = dbquote(date("Y-m-d"));

        $fields[] = "date_modified";
        $values[] = dbquote(date("Y-m-d"));

        $fields[] = "user_created";
        $values[] = dbquote(user_login());

        $fields[] = "user_modified";
        $values[] = dbquote(user_login());
        
        $sql = "insert into project_costs (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

        mysql_query($sql) or dberror($sql);
    }


    $sql = "update project_costs " .
           "set project_cost_budget =  " . $budget . 
           " where project_cost_order = " . $order_id;

    mysql_query($sql) or dberror($sql);


    $sql = "update orders " .
           "set order_budget_freezed_date =  " . dbquote(date("Y-m-d")) . 
           " where order_id = " . $order_id;

    mysql_query($sql) or dberror($sql);

}


/********************************************************************
   update project client data
*********************************************************************/
function  project_update_client_data($form,$design_objectives_listbox_names,$design_objectives_checklist_names)
{

    $order_fields = array();

    $delivery_address_fields = array();

    $project_fields = array();

    $project_item_fields = array();
    $project_item_values = array();


	//update table posorders or posorderspipeline
	$sql = "select order_number, project_projectkind from orders " . 
		   "left join projects on project_order = order_id " . 
		   "where order_id = " . $form->value("oid");

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$old_project_number = $row["order_number"];
		$old_projectkind = $row["project_projectkind"];

		$year_of_order = substr($form->value("project_number"),0,2);
		if($year_of_order > 30 and $year_of_order <= 99)
		{
			$year_of_order = "19" . $year_of_order;
		}
		else
		{
			$year_of_order = substr($year_of_order, 0, 1) . '00' . substr($year_of_order, 1, 1);
		}

		if(substr($form->value("project_number"),3,1) == '.')
		{
			$year_of_order = substr($form->value("project_number"),0,1) . '0' . substr($form->value("project_number"),1,2);
		}


		$sql = "update posorders set " .
			   "posorder_year = " . dbquote($year_of_order) . ", " . 
			   "posorder_product_line = " . dbquote($form->value("product_line")) . ", " . 
			   "posorder_postype = " . dbquote($form->value("project_postype")) . ", " . 
			   "posorder_subclass = " . dbquote($form->value("project_pos_subclass")) . ", " . 
			   "posorder_project_kind = " . dbquote($form->value("project_projectkind")) . ", " . 
			   "posorder_legal_type = " . dbquote($form->value("project_cost_type")) . ", " .
			   "posorder_furniture_height = " . dbquote($form->value("furniture_height")) . ", " .
			   "posorder_furniture_height_mm = " . dbquote($form->value("project_furniture_height_mm")) . ", " .
			   "posorder_ordernumber = " . dbquote($form->value("project_number")) .
			   " where posorder_ordernumber = " . dbquote($old_project_number);


		$result = mysql_query($sql) or dberror($sql);

		$sql = "update posorderspipeline set " .
			   "posorder_year = " . dbquote($year_of_order) . ", " . 
			   "posorder_product_line = " . dbquote($form->value("product_line")) . ", " . 
			   "posorder_postype = " . dbquote($form->value("project_postype")) . ", " . 
			   "posorder_subclass = " . dbquote($form->value("project_pos_subclass")) . ", " . 
			   "posorder_project_kind = " . dbquote($form->value("project_projectkind")) . ", " . 
			   "posorder_legal_type = " . dbquote($form->value("project_cost_type")) . ", " .
			   "posorder_furniture_height = " . dbquote($form->value("furniture_height")) . ", " .
			   "posorder_furniture_height_mm = " . dbquote($form->value("project_furniture_height_mm")) . ", " .
			   "posorder_ordernumber = " . dbquote($form->value("project_number")) . 
			   " where posorder_ordernumber = " . dbquote($old_project_number);

		$result = mysql_query($sql) or dberror($sql);
	}


    // update record in table orders
    $value = trim($form->value("project_number")) == "" ? "null" : dbquote($form->value("project_number"));
    $order_fields[] = "order_number = " . $value;

    $value = $form->value("status");
    $order_fields[] = "order_actual_order_state_code = " . $value;

    $value = $form->value("client_address_id");
    $order_fields[] = "order_client_address = " . $value;

    $value = $form->value("client_address_user_id");
    $order_fields[] = "order_user = " . $value;

    $value = trim($form->value("preferred_transportation_arranged")) == "" ? "null" : dbquote($form->value("preferred_transportation_arranged"));
    $order_fields[] = "order_preferred_transportation_arranged = " . $value;

	$value = trim($form->value("preferred_transportation_mode")) == "" ? "null" : dbquote($form->value("preferred_transportation_mode"));
    $order_fields[] = "order_preferred_transportation_mode = " . $value;

    $value = trim($form->value("voltage")) == "" ? "null" : dbquote($form->value("voltage"));
    $order_fields[] = "order_voltage = " . $value;

    $value = dbquote(from_system_date($form->value("preferred_delivery_date")), true);
    $order_fields[] = "order_preferred_delivery_date = " . $value;


    $value = trim($form->value("full_delivery")) == "" ? "null" : dbquote($form->value("full_delivery"));
    $order_fields[] = "order_full_delivery = " . $value;

    //$value = trim($form->value("packaging_retraction")) == "" ? "null" : dbquote($form->value("packaging_retraction"));
    //$order_fields[] = "order_packaging_retraction = " . $value;

    $value = trim($form->value("pedestrian_mall_approval")) == "" ? "null" : dbquote($form->value("pedestrian_mall_approval"));
    $order_fields[] = "order_pedestrian_mall_approval = " . $value;

    $value = trim($form->value("delivery_comments")) == "" ? "null" : dbquote($form->value("delivery_comments"));
    $order_fields[] = "order_delivery_comments = " . $value;

	$value = dbquote($form->value("order_insurance"));
    $order_fields[] = "order_insurance = " . $value;

	$value = "current_timestamp";
    $order_fields[] = "date_modified = " . $value;
    
    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $order_fields[] = "user_modified = " . $value;
    }

    $sql = "update orders set " . join(", ", $order_fields) . " where order_id = " . $form->value("oid");

    mysql_query($sql) or dberror($sql);

    
	    
    // update record in table projects

    $value = trim($form->value("project_postype")) == "" ? "0" : dbquote($form->value("project_postype"));
    $project_fields[] = "project_postype = " . $value;


	$value = trim($form->value("project_pos_subclass")) == "" ? "0" : dbquote($form->value("project_pos_subclass"));
    $project_fields[] = "project_pos_subclass = " . $value;


    $value = trim($form->value("project_state")) == "" ? "1" : dbquote($form->value("project_state"));
    $project_fields[] = "project_state = " . $value;

    $value = trim($form->value("project_projectkind")) == "" ? "null" :
    dbquote($form->value("project_projectkind"));
    $project_fields[] = "project_projectkind = " . $value;

    $value = trim($form->value("product_line")) == "" ? "null" : dbquote($form->value("product_line"));
    $project_fields[] = "project_product_line = " . $value;

    $value = dbquote($form->value("location_type"));
    $project_fields[] = "project_location_type = " . $value;

    $value = trim($form->value("location_type_other")) == "" ? "null" : dbquote($form->value("location_type_other"));
    $project_fields[] = "project_location = " . $value;

    $value = trim($form->value("approximate_budget")) == "" ? "null" : dbquote($form->value("approximate_budget"));
    $project_fields[] = "project_approximate_budget = " . $value;

    $value = dbquote(from_system_date($form->value("planned_opening_date")), true);
    $project_fields[] = "project_planned_opening_date = " . $value;

    $value = trim($form->value("watches_displayed")) == "" ? "null" : dbquote($form->value("watches_displayed"));
    $project_fields[] = "project_watches_displayed = " . $value;

    $value = trim($form->value("watches_stored")) == "" ? "null" : dbquote($form->value("watches_stored"));
    $project_fields[] = "project_watches_stored = " . $value;

	$value = trim($form->value("bijoux_displayed")) == "" ? "null" : dbquote($form->value("bijoux_displayed"));
    $project_fields[] = "project_bijoux_displayed = " . $value;

    $value = trim($form->value("bijoux_stored")) == "" ? "null" : dbquote($form->value("bijoux_stored"));
    $project_fields[] = "project_bijoux_stored = " . $value;

    $value = trim($form->value("comments")) == "" ? "null" : dbquote($form->value("comments"));
    $project_fields[] = "project_comments = " . $value;

	$value = trim($form->value("furniture_height")) == "" ? "null" : dbquote($form->value("furniture_height"));
    $project_fields[] = "project_furniture_height = " . $value;

	$value = trim($form->value("project_furniture_height_mm")) == "" ? "null" : dbquote($form->value("project_furniture_height_mm"));
    $project_fields[] = "project_furniture_height_mm = " . $value;


	$value = trim($form->value("project_is_relocation_project")) == "" ? "0" : dbquote($form->value("project_is_relocation_project"));
    $project_fields[] = "project_is_relocation_project = " . $value;

	$value = trim($form->value("project_relocated_posaddress_id")) == "" ? "0" : dbquote($form->value("project_relocated_posaddress_id"));
    $project_fields[] = "project_relocated_posaddress_id = " . $value;


    $value = "current_timestamp";
    $project_fields[] = "date_modified = " . $value;
    
    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $project_fields[] = "user_modified = " . $value;
    }

    $sql = "update projects set " . join(", ", $project_fields) . " where project_id = " . $form->value("pid");
    mysql_query($sql) or dberror($sql);

    $_REQUEST["id"] = $form->value("pid");

    // delete all records form project_items belonign to the project
    $sql = "delete from project_items where project_item_project = " . $form->value("pid");
    mysql_query($sql) or dberror($sql);

    // insert records into table project_items
    foreach ($design_objectives_listbox_names as $element)
    {
        $project_item_fields[0] = "project_item_project";
        $project_item_values[0] = $form->value("pid");

        $project_item_fields[1] = "project_item_item";
        $project_item_values[1] = $form->value($element);
        
        $project_item_fields[2] = "date_created";
        $project_item_values[2] = "current_timestamp";

        $project_item_fields[3] = "date_modified";
        $project_item_values[3] = "current_timestamp";
                
        if (isset($_SESSION["user_login"]))
        {
            $project_item_fields[4] = "user_created";
            $project_item_values[4] = dbquote($_SESSION["user_login"]);

            $project_item_fields[5] = "user_modified";
            $project_item_values[5] = dbquote($_SESSION["user_login"]);
        }
        $sql = "insert into project_items (" . join(", ", $project_item_fields) . ") values (" . join(", ", $project_item_values) . ")";
        mysql_query($sql) or dberror($sql);
    }
    

    foreach ($design_objectives_checklist_names as $name)
    {
        $tmp = $form->value($name);
        foreach ($tmp as $value)
        {
        $project_item_fields[0] = "project_item_project";
        $project_item_values[0] =$form->value("pid");

        $project_item_fields[1] = "project_item_item";
        $project_item_values[1] = $value;
        
        $project_item_fields[2] = "date_created";
        $project_item_values[2] = "current_timestamp";

        $project_item_fields[3] = "date_modified";
        $project_item_values[3] = "current_timestamp";
                
        if (isset($_SESSION["user_login"]))
        {
            $project_item_fields[4] = "user_created";
            $project_item_values[4] = dbquote($_SESSION["user_login"]);

            $project_item_fields[5] = "user_modified";
            $project_item_values[5] = dbquote($_SESSION["user_login"]);
        }
        $sql = "insert into project_items (" . join(", ", $project_item_fields) . ") values (" . join(", ", $project_item_values) . ")";
        mysql_query($sql) or dberror($sql);
        }
    }


    // update record in table project_costs
    $value = dbquote($form->value("shop_sqms"));
    $project_cost_fields[] = "project_cost_sqms = " . $value;
	
	$value = dbquote($form->value("project_cost_type"));
    $project_cost_fields[] = "project_cost_type = " . $value;

    $value = "current_timestamp";
    $project_cost_fields[] = "date_modified = " . $value;
    
    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $project_cost_fields[] = "user_modified = " . $value;
    }

    $sql = "update project_costs set " . join(", ", $project_cost_fields) . " where project_cost_order = " . $form->value("oid");
    mysql_query($sql) or dberror($sql);


	
	return true;

}

/********************************************************************
    append records to table order_mails
*********************************************************************/
function append_mail($order_id, $recepient_id, $user_id, $text, $order_state_code, $type, $order_revision=0)
{

    $text = str_replace("\n\n", "\n", $text);
	$text = str_replace("\n\n", "\n", $text);

	$sql = "select order_state_id " .
           "from order_states ".
           "left join order_state_groups on order_state_group = order_state_group_id ".
           "where order_state_code = " . dbquote($order_state_code) .
           "    and order_state_group_order_type = " .$type;

    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);

    $order_mail_fields = array();
    $order_mail_values = array();
    
    // insert record into table tasks

    $order_mail_fields[] = "order_mail_order";
    $order_mail_values[] = $order_id;

    if ($recepient_id != "")
    {
        $order_mail_fields[] = "order_mail_user";
        $order_mail_values[] = $recepient_id;
    }

    $order_mail_fields[] = "order_mail_from_user";
    $order_mail_values[] = $user_id;

    $order_mail_fields[] = "order_mail_text";
    $order_mail_values[] = dbquote($text);

    if ($order_state_code != "")
    {
        $order_mail_fields[] = "order_mail_order_state";
        $order_mail_values[] = $row["order_state_id"];
    }

	$order_mail_fields[] = "order_revisioned";
    $order_mail_values[] = $order_revision;

    $order_mail_fields[] = "date_created";
    $order_mail_values[] = "current_timestamp";

    $order_mail_fields[] = "date_modified";
    $order_mail_values[] = "current_timestamp";

    if (isset($_SESSION["user_login"]))
    {
        $order_mail_fields[] = "user_created";
        $order_mail_values[] = dbquote($_SESSION["user_login"]);

        $order_mail_fields[] = "user_modified";
        $order_mail_values[] = dbquote($_SESSION["user_login"]);
    }

    $sql = "insert into order_mails (" . join(", ", $order_mail_fields) . ") values (" . join(", ", $order_mail_values) . ")";
        mysql_query($sql) or dberror($sql);
}



/***********************************************************************
   save suppliers currency and pricing data and
   copy item information into all other item records of the same order
   belonging to the same supplier
************************************************************************/
function  project_edit_order_item_save($form)
{
		$order_item_fields = array();

        $value =  trim($form->value("order_item_text")) == "" ? "null" : dbquote($form->value("order_item_text"));
        $order_item_fields[] = "order_item_text = " . $value;

        if ($form->value("order_item_type") == ITEM_TYPE_STANDARD or $form->value("order_item_type") == ITEM_TYPE_SERVICES or $form->value("order_item_type") == ITEM_TYPE_SPECIAL)
        {

            $value =  trim($form->value("order_item_quantity")) == "" ? "null" : dbquote($form->value("order_item_quantity"));
            $order_item_fields[] = "order_item_quantity = " . $value;
    
            // get suppliers' currency data
            $supplier_currency_id = null;
            $supplier_currency_exchangerate = null;

            
            if ($form->value("order_item_currency"))
            {
                $currency = get_currency($form->value("order_item_currency"));
                $supplier_currency_id = $currency["id"];
                $supplier_currency_exchangerate = $currency["exchange_rate"];
            }
            else
            {
                $currency = get_address_currency($form->value("order_item_supplier_address"));
                $supplier_currency_id = $currency["id"];
                $supplier_currency_exchangerate = $currency["exchange_rate"];
            }

            
            $order_item_fields[] = "order_item_supplier_currency = " . $supplier_currency_id;

            $order_item_fields[] = "order_item_supplier_exchange_rate = " . $supplier_currency_exchangerate;

            if ($form->value("order_item_type") == ITEM_TYPE_STANDARD or $form->value("order_item_type") == ITEM_TYPE_SERVICES)
            {
                $value =  trim($form->value("order_item_client_price")) == "" ? "null" : dbquote($form->value("order_item_client_price"));
                $order_item_fields[] = "order_item_client_price = " . $value;

                // update supplier's price
                $sql = "select * ".
                       "from suppliers ".
                       "where supplier_address = " . $form->value("order_item_supplier_address") .
                       "    and supplier_item = " . $form->value("order_item_item");

                $res = mysql_query($sql) or dberror($sql);
                if ($row = mysql_fetch_assoc($res))
                {
                    $order_item_fields[] = "order_item_supplier_price = " . $row["supplier_item_price"];
                }
            }
            else if ($form->value("order_item_type") == ITEM_TYPE_SPECIAL)
            {
                
				if(isset($form->items["order_item_cost_group"]))
				{
					$value = $form->value("order_item_cost_group");
					$order_item_fields[] = "order_item_cost_group = " . $value;

					$value = $form->value("order_items_costmonitoring_group");
					$order_item_fields[] = "order_items_costmonitoring_group = " . $value;
				}

				$value =  trim($form->value("order_item_system_price")) == "" ? "null" : dbquote($form->value("order_item_system_price"));
                $order_item_fields[] = "order_item_system_price = " . $value;

                $order_currency = get_order_currency($form->value("oid"));
                $value = $form->value("order_item_system_price") / $order_currency["exchange_rate"] * $order_currency["factor"];
                $order_item_fields[] = "order_item_client_price = " . $value;

                $value =  trim($form->value("order_item_supplier_price")) == "" ? "null" : dbquote($form->value("order_item_supplier_price"));
                $order_item_fields[] = "order_item_supplier_price = " . $value;


                $value =  trim($form->value("order_item_supplier_item_code")) == "" ? "null" : dbquote($form->value("order_item_supplier_item_code"));
                $order_item_fields[] = "order_item_supplier_item_code = " . $value;

                $value =  trim($form->value("order_item_offer_number")) == "" ? "null" : dbquote($form->value("order_item_offer_number"));
                $order_item_fields[] = "order_item_offer_number = " . $value;

                $value =  trim($form->value("order_item_production_time")) == "" ? "null" : dbquote($form->value("order_item_production_time"));
                $order_item_fields[] = "order_item_production_time = " . $value;

            }

            $value =  trim($form->value("order_item_cost_unit_number")) == "" ? "null" : dbquote($form->value("order_item_cost_unit_number"));
            $order_item_fields[] = "order_item_cost_unit_number = " . $value;

            $value =  trim($form->value("order_item_po_number")) == "" ? "null" : dbquote($form->value("order_item_po_number"));
            $order_item_fields[] = "order_item_po_number = " . $value;

            $value =  trim($form->value("order_item_supplier_address")) == "" ? "null" : dbquote($form->value("order_item_supplier_address"));
            $order_item_fields[] = "order_item_supplier_address = " . $value;


            if ($form->value("order_item_type") == ITEM_TYPE_STANDARD or $form->value("order_item_type") == ITEM_TYPE_SPECIAL)
			{
				$value =  trim($form->value("order_item_transportation")) == "" ? "null" : dbquote($form->value("order_item_transportation"));
				$order_item_fields[] = "order_item_transportation = " . $value;

				$value =  trim($form->value("order_item_forwarder_address")) == "" ? "null" : dbquote($form->value("order_item_forwarder_address"));
				$order_item_fields[] = "order_item_forwarder_address = " . $value;

				$value =  trim($form->value("order_item_staff_for_discharge")) == "" ? "null" :     dbquote($form->value("order_item_staff_for_discharge"));
				$order_item_fields[] = "order_item_staff_for_discharge = " . $value;

				$value =  trim($form->value("order_item_no_offer_required")) == "" ? "null" : dbquote($form->value("order_item_no_offer_required"));
				$order_item_fields[] = "order_item_no_offer_required = " . $value;

				$value =  trim($form->value("order_item_not_in_budget")) == "" ? "null" : dbquote($form->value("order_item_not_in_budget"));
				$order_item_fields[] = "order_item_not_in_budget = " . $value;
			}

        }
        else if ($form->value("order_item_type") == ITEM_TYPE_COST_ESTIMATION OR $form->value("order_item_type") == ITEM_TYPE_LOCALCONSTRUCTIONCOST)
        {

			if(isset($form->items["order_item_cost_group"]))
			{
				$value = $form->value("order_item_cost_group");
				$order_item_fields[] = "order_item_cost_group = " . $value;

			}
			
			if(isset($form->items["order_item_supplier_freetext"]))
			{
				$value =  trim($form->value("order_item_supplier_freetext")) == "" ? "null" : dbquote($form->value("order_item_supplier_freetext"));
				$order_item_fields[] = "order_item_supplier_freetext = " . $value;
			}

            $value =  trim($form->value("order_item_system_price")) == "" ? "null" : dbquote($form->value("order_item_system_price"));
            $order_item_fields[] = "order_item_system_price = " . $value;

            $order_currency = get_order_currency($form->value("oid"));
            $value = $form->value("order_item_system_price") / $order_currency["exchange_rate"] * $order_currency["factor"];
            $order_item_fields[] = "order_item_client_price = " . $value;

        }
        
        $value = "current_timestamp";
        $order_item_fields[] = "date_modified = " . $value;
    
        if (isset($_SESSION["user_login"]))
        {
            $value = dbquote($_SESSION["user_login"]);
            $order_item_fields[] = "user_modified = " . $value;
        }

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $form->value("order_item_id");
        mysql_query($sql) or dberror($sql);


        if ($form->value("order_item_type") == ITEM_TYPE_STANDARD or $form->value("order_item_type") == ITEM_TYPE_SPECIAL or $form->value("order_item_type") == ITEM_TYPE_SERVICES)
        {
            // copy values into all aother order_items
            $sql_order_item = "select * ".
                              "from order_items ".
                              "where (order_item_not_in_budget is null or order_item_not_in_budget = 0) ".
                              "    and order_item_order = " . param("oid") . " " .
                              "    and order_item_supplier_address = ". $form->value("order_item_supplier_address").
                              "    and (order_item_type = " . ITEM_TYPE_STANDARD . " ".
				              "    or order_item_type = " . ITEM_TYPE_SERVICES . " ".
                              "    or order_item_type = " . ITEM_TYPE_SPECIAL . ")";
    
            $res = mysql_query($sql_order_item) or dberror($sql_order_item);
            while ($row = mysql_fetch_assoc($res))
            {
                // update all empty P.O. Numbers
                $value = trim($form->value("order_item_po_number")) == "" ? "null" : dbquote($form->value("order_item_po_number"));

                if (!$row["order_item_po_number"])
                {
                    $sql = "update order_items set order_item_po_number =" . $value . " ".
                       "where order_item_id = " . $row["order_item_id"];
                    mysql_query($sql) or dberror($sql);
                }


                // update all empty cost unit numbers
                $value = trim($form->value("order_item_cost_unit_number")) == "" ? "null" : dbquote($form->value("order_item_cost_unit_number"));

                if (!$row["order_item_cost_unit_number"])
                {
                    $sql = "update order_items set order_item_cost_unit_number =" . $value . " ".
                       "where order_item_id = " . $row["order_item_id"];
                    mysql_query($sql) or dberror($sql);
                }

                if ($form->value("order_item_type") == ITEM_TYPE_STANDARD or $form->value("order_item_type") == ITEM_TYPE_SPECIAL)
				{
					// update all empty forwarder addresses
					$value = trim($form->value("order_item_forwarder_address")) == "" ? "null" :     dbquote($form->value("order_item_forwarder_address"));

					if (!$row["order_item_forwarder_address"])
					{
						$sql = "update order_items set order_item_forwarder_address =" . $value . " ".
							   "where order_item_id = " . $row["order_item_id"];
						mysql_query($sql) or dberror($sql);
					}
		
					// update all transportation codes
					$value = trim($form->value("order_item_transportation")) == "" ? "null" : dbquote($form->value("order_item_transportation"));

					if (!$row["order_item_transportation"])
					{
						$sql = "update order_items set order_item_transportation =" . $value . " ".
						   "where order_item_id = " . $row["order_item_id"];
						mysql_query($sql) or dberror($sql);
					}
				}
            }
        }

}

/********************************************************************
    update budget state
*********************************************************************/
function update_budget_state($order_id, $state)
{
    $sql = "update orders set order_budget_is_locked = " . $state . " where order_id = " .  $order_id;
    mysql_query($sql) or dberror($sql);

}


/********************************************************************
    unfreeze project budget
*********************************************************************/
function unfreeze_project_budget($order_id)
{

    $sql = "update orders set order_budget_is_locked = 0 where order_id = " .  $order_id;
    mysql_query($sql) or dberror($sql);


    $sql = "update order_items set " .
           "order_item_in_freezed_budget = 0, " .
           "order_item_quantity_freezed = '0000-00-00', " .
           "order_item_system_price_freezed = NULL, " .
           "order_item_client_price_freezed = NULL " .
           " where order_item_order = " . $order_id;

    mysql_query($sql) or dberror($sql);


    $sql = "update orders " .
           "set order_budget_freezed_date =  Null " . 
           " where order_id = " . $order_id;

    mysql_query($sql) or dberror($sql);          

}

/***********************************************************************
   insert special item or cost estimation record
************************************************************************/
function  project_add_special_item_save($form)
{

        $order_item_fields = array();
        $order_item_values = array();

        $order_item_fields[] = "order_item_order";
        $order_item_values[] = trim($form->value("order_item_order")) == "" ? "null" : dbquote($form->value("order_item_order"));

        $order_item_fields[] = "order_item_type";
        $order_item_values[] = trim($form->value("order_item_type")) == "" ? "null" : dbquote($form->value("order_item_type"));

        $order_item_fields[] = "order_item_text";
        $order_item_values[] = trim($form->value("order_item_text")) == "" ? "null" : dbquote($form->value("order_item_text"));

        if ($form->value("order_item_type") == ITEM_TYPE_SPECIAL)
        {
            $order_item_fields[] = "order_item_quantity";
            $order_item_values[] = trim($form->value("order_item_quantity")) == "" ? "null" : dbquote($form->value("order_item_quantity"));

            $order_item_fields[] = "order_item_supplier_address";
            $order_item_values[] = trim($form->value("order_item_supplier_address")) == "" ? "null" : dbquote($form->value("order_item_supplier_address"));

            // get suppliers's currency information
            $supplier_currency = get_address_currency($form->value("order_item_supplier_address"));

            $order_item_fields[] = "order_item_supplier_currency";
            $order_item_values[] = $supplier_currency["id"];

            $order_item_fields[] = "order_item_supplier_exchange_rate";
            $order_item_values[] = $supplier_currency["exchange_rate"];

            $order_item_fields[] = "order_item_cost_group";
            $order_item_values[] =$form->value("order_item_cost_group");

			$order_item_fields[] = "order_items_costmonitoring_group";
            $order_item_values[] =$form->value("order_items_costmonitoring_group");

        }

        if ($form->value("order_item_type") == ITEM_TYPE_COST_ESTIMATION)
        {
            $order_item_fields[] = "order_item_supplier_freetext";
            $order_item_values[] = dbquote($form->value("order_item_supplier_freetext"));

            $order_item_fields[] = "order_item_cost_group";
            $order_item_values[] =$form->value("order_item_cost_group");

			$order_item_fields[] = "order_item_system_price";
            $order_item_values[] =$form->value("order_item_system_price");

            $order_item_fields[] = "order_item_quantity";
            $order_item_values[] = 1;
 
        }

        if ($form->value("order_item_type") == ITEM_TYPE_LOCALCONSTRUCTIONCOST)
        {
            $order_item_fields[] = "order_item_supplier_freetext";
            $order_item_values[] = dbquote($form->value("order_item_supplier_freetext"));

            $order_item_fields[] = "order_item_system_price";
            $order_item_values[] =$form->value("order_item_system_price");

            $order_item_fields[] = "order_item_cost_group";
            $order_item_values[] = $form->value("order_item_cost_group");

            $order_item_fields[] = "order_item_quantity";
            $order_item_values[] = 1;
 
        }


        // get orders's currency
        $currency = get_order_currency($form->value("order_item_order"));

        $client_price = $form->value("order_item_system_price") / $currency["exchange_rate"] * $currency["factor"];

        $order_item_fields[] = "order_item_client_price";
        $order_item_values[] = trim($client_price) == "" ? "null" : dbquote($client_price);

        $order_item_fields[] = "date_created";
        $order_item_values[] = "current_timestamp";

        $order_item_fields[] = "date_modified";
        $order_item_values[] = "current_timestamp";
                
        if (isset($_SESSION["user_login"]))
        {
            $order_item_fields[] = "user_created";
            $order_item_values[] = dbquote($_SESSION["user_login"]);

            $order_item_fields[] = "user_modified";
            $order_item_values[] = dbquote($_SESSION["user_login"]);
        }
        $sql = "insert into order_items (" . join(", ", $order_item_fields) . ") values (" . join(", ", $order_item_values) . ")";
        mysql_query($sql) or dberror($sql);

        $item_id = mysql_insert_id();

        if ($form->value("order_item_type") == ITEM_TYPE_SPECIAL)
        {
            // copy delivery address for new item in table order_addresses
            $sql = "select * from order_addresses ".
                   "where order_address_type = 2 ".
                   "    and order_address_order = ". param("oid") .
                   "    and (order_address_order_item = 0 ".
                   "    or order_address_order_item is null) ";

            $res2 = mysql_query($sql) or dberror($sql);
            $row2 = mysql_fetch_assoc($res2);

            $delivery_address_fields = array();
            $delivery_address_values = array();

            $delivery_address_fields[] = "order_address_order";
            $delivery_address_values[] = $form->value("order_item_order");

            $delivery_address_fields[] = "order_address_order_item";
            $delivery_address_values[] = $item_id;

            $delivery_address_fields[] = "order_address_type";
            $delivery_address_values[] = 2;

            $delivery_address_fields[] = "order_address_company";
            $delivery_address_values[] = dbquote($row2["order_address_company"]);

            $delivery_address_fields[] = "order_address_company2";
            $delivery_address_values[] = dbquote($row2["order_address_company2"]);

            $delivery_address_fields[] = "order_address_address";
            $delivery_address_values[] = dbquote($row2["order_address_address"]);

            $delivery_address_fields[] = "order_address_address2";
            $delivery_address_values[] = dbquote($row2["order_address_address2"]);

            $delivery_address_fields[] = "order_address_zip";
            $delivery_address_values[] = dbquote($row2["order_address_zip"]);

            $delivery_address_fields[] = "order_address_place";
            $delivery_address_values[] = dbquote($row2["order_address_place"]);

            $delivery_address_fields[] = "order_address_country";
            $delivery_address_values[] = dbquote($row2["order_address_country"]);

            $delivery_address_fields[] = "order_address_phone";
            $delivery_address_values[] = dbquote($row2["order_address_phone"]);

            $delivery_address_fields[] = "order_address_fax";
            $delivery_address_values[] = dbquote($row2["order_address_fax"]);

            $delivery_address_fields[] = "order_address_email";
            $delivery_address_values[] = dbquote($row2["order_address_email"]);

            $delivery_address_fields[] = "order_address_parent";
            $delivery_address_values[] = $row2["order_address_parent"];

            $delivery_address_fields[] = "order_address_contact";
            $delivery_address_values[] = dbquote($row2["order_address_contact"]);

            $delivery_address_fields[] = "date_created";
            $delivery_address_values[] = "current_timestamp";

            $delivery_address_fields[] = "date_modified";
            $delivery_address_values[] = "current_timestamp";

            if (isset($_SESSION["user_login"]))
            {
                $delivery_address_fields[] = "user_created";
                $delivery_address_values[] = dbquote($_SESSION["user_login"]);

                $delivery_address_fields[] = "user_modified";
                $delivery_address_values[] = dbquote($_SESSION["user_login"]);
            }

            $sql = "insert into order_addresses (" . join(", ", $delivery_address_fields) . ") values (" . join(", ", $delivery_address_values) . ")";
            mysql_query($sql) or dberror($sql);
        }


}

/********************************************************************
   insert, update or delete project order items (material list)
*********************************************************************/
function  project_save_order_items($list, $type)
{
	foreach($list as $key=>$value)
    {
        if($key == "hidden" and isset($value["pline"]))
        {
            $pline = $value["pline"];
        }
        if($key == "hidden" and isset($value["cid"]))
        {
            $cid_at_start = $value["cid"];
            
        }
		
    }

    foreach ($list->values("item_entry_field") as $key=>$value)
    {

        if(isset($cid_at_start) and isset($pline))
        {
            
            $sql = "select category_item_category " .
                   "from items " .
                   "left join category_items on category_item_item = " . $key . " " .
                   "left join categories on category_id = category_item_category " .
                   "where category_product_line = " . $pline . 
                   "   and item_id = " . $key . 
                   "   and category_item_category = " . $cid_at_start;

        }
        elseif(isset($pline))
        {
            $sql = "select category_item_category " .
                   "from items " .
                   "left join category_items on category_item_item = " . $key . " " .
                   "left join categories on category_id = category_item_category " .
                   "where category_product_line = " . $pline . 
                   "   and item_id = " . $key;
        }
        else //orders only
        {
            $sql = "select category_item_category " .
                   "from items " .
                   "left join category_items on category_item_item = " . $key . " " .
                   "where item_id = " . $key;
        }

        $res = mysql_query($sql) or dberror($sql);
        $row = mysql_fetch_assoc($res);

        $cid = $row["category_item_category"];
        
        if(!$cid)
        {
            $cid = 0;

            // chek if item is new or already in order_items
            $sql_order_item = "select order_item_item ".
                          "from order_items ".
                          "where (order_item_category = " . $cid .
                          "    or order_item_category is null) " .
                          "    and order_item_item = " . $key .
                          "    and order_item_order = " . param("oid");
        }
        else
        {
            // chek if item is new or already in order_items
            $sql_order_item = "select order_item_item ".
                          "from order_items ".
                          "where order_item_category = " . $cid .
                          "    and order_item_item = " . $key .
                          "    and order_item_order = " . param("oid");
        
        }
        
        $res = mysql_query($sql_order_item) or dberror($sql_order_item);
        if ($row = mysql_fetch_assoc($res))
        {
            // update record in table order_item
            if ($value >0)
            {
                $order_item_fields = array();

                if ($type == ITEM_TYPE_COST_ESTIMATION)
                {
                    $order_item_fields[] = "order_item_system_price = " . $value;
                    $order_item_fields[] = "order_item_quantity = 1";
                }
                elseif ($type == ITEM_TYPE_LOCALCONSTRUCTIONCOST)
                {
                    $order_item_fields[] = "order_item_system_price = " . $value;
                    $order_item_fields[] = "order_item_quantity = 1";
                }
                else
                {
                    $order_item_fields[] = "order_item_quantity = " . $value;
                }

                $value1 = "current_timestamp";
                $project_fields[] = "date_modified = " . $value1;
    
                if (isset($_SESSION["user_login"]))
                {
                    $value1 = dbquote($_SESSION["user_login"]);
                    $project_fields[] = "user_modified = " . $value1;
                }

                $order_item_fields[] = "order_item_category = " . $cid;

       
                $sql = "update order_items set " . join(", ", $order_item_fields) . " " .
                       "where order_item_item = " . $key .
                       "    and order_item_order = " . param("oid");

                mysql_query($sql) or dberror($sql);
            }

            // delete record in table order_item
            if ($value == 0)
            {
                $sql = "delete from order_items " .
                       "where order_item_category = " . $cid .
                       "   and order_item_item = " . $key .
                       "    and order_item_order = " . param("oid");
                mysql_query($sql) or dberror($sql);
            }
        }
        else
        {
            // insert new records into table order_items
            if ($value >0)
            {

                // get item_category (first of any)
                $category = 0;
                $sql = "select * ".
                       "from category_items ".
                       "where category_item_category = " . $cid . 
                       "   and category_item_item = " . $key;

                $res1 = mysql_query($sql) or dberror($sql);

                if ($row = mysql_fetch_assoc($res1))
                {
                    $category = $row["category_item_category"];
                }


				//get item_type
				$sql = "select item_type from items where item_id = " . $key;
				$res1 = mysql_query($sql) or dberror($sql);

                if ($row = mysql_fetch_assoc($res1))
                {
                    $type = $row["item_type"];
                }


                
                $order_item_fields = array();
                $order_item_values = array();
    
                $order_item_fields[] = "order_item_order";
                $order_item_values[] = param("oid");

                $order_item_fields[] = "order_item_item";
                $order_item_values[] = $key;

                $order_item_fields[] = "order_item_type";
                $order_item_values[] = $type;


                if ($type == ITEM_TYPE_COST_ESTIMATION)
                {
                    $order_item_fields[] = "order_item_quantity";
                    $order_item_values[] = 1;
                }

                if ($type == ITEM_TYPE_LOCALCONSTRUCTIONCOST)
                {
                    $order_item_fields[] = "order_item_quantity";
                    $order_item_values[] = 1;
                }

                if ($type == ITEM_TYPE_STANDARD or $type == ITEM_TYPE_SPECIAL or $type == ITEM_TYPE_SERVICES)
                {
					$order_item_fields[] = "order_item_quantity";
                    $order_item_values[] = $value;

                    $order_item_fields[] = "order_item_category";
                    $order_item_values[] = $category;
                

                    // get supplier's address id (first record in case of several)
                    $sql = "select address_id, address_company, supplier_item_price, supplier_item_currency ".
                           "from items ".
                           "left join suppliers on item_id = supplier_item ".
                           "left join addresses on supplier_address = address_id ".
                           "where item_id = " . $key;
                
                    $res1 = mysql_query($sql) or dberror($sql);

                    if ($row1 = mysql_fetch_assoc($res1))
                    {
                        if ($row1["address_id"])
                        {
                            
                            if ($row1["supplier_item_currency"])
                            {
                                $currency = get_currency($row1["supplier_item_currency"]);
                            }
                            else
                            {
                                $currency = get_address_currency($row1["address_id"]);
                            }
                            
                            $order_item_fields[] = "order_item_supplier_address";
                            $order_item_values[] = $row1["address_id"];

                            // get suppliers's currency information
                            $supplier_currency = get_address_currency($row1["address_id"]);
    
                            $order_item_fields[] = "order_item_supplier_currency";
                            $order_item_values[] = $currency["id"];
    
                            $order_item_fields[] = "order_item_supplier_exchange_rate";
                            $order_item_values[] = $currency["exchange_rate"];

                            $order_item_fields[] = "order_item_supplier_price";
                            $order_item_values[] = $row1["supplier_item_price"];
                        }
                    }
                }

                // get orders's currency
                $currency = get_order_currency(param("oid"));

                // get item data

                $sql = "select * ".
                       "from items ".
                       "left join category_items on item_id = category_item_item ".
                       "left join categories on category_item_category = category_id ".
                       "where item_id = " . $key;

                $res1 = mysql_query($sql) or dberror($sql);

                if ($row = mysql_fetch_assoc($res1))
                {
                    $order_item_fields[] = "order_item_text";
                    $order_item_values[] = trim($row["item_name"]) == "" ? "null" : dbquote($row["item_name"]);

                    $order_item_fields[] = "order_item_system_price";
                    if ($type == ITEM_TYPE_COST_ESTIMATION)
                    {
                        $order_item_values[] = $value;
                        $client_price = $value / $currency["exchange_rate"] * $currency["factor"];
                    }
                    elseif ($type == ITEM_TYPE_LOCALCONSTRUCTIONCOST)
                    {
                        $order_item_values[] = $value;
                        $client_price = $value / $currency["exchange_rate"] * $currency["factor"];
                    }
                    else
                    {
                        $order_item_values[] = trim($row["item_price"]) == "" ? "null" : dbquote($row["item_price"]);
                        $client_price = $row["item_price"] / $currency["exchange_rate"] * $currency["factor"];
                    }


                    $order_item_fields[] = "order_item_client_price";
                    $order_item_values[] = trim($client_price) == "" ? "null" : dbquote($client_price);

                    if (!param("pid"))
                    {
                        $order_item_fields[] = "order_item_cost_unit_number";
                        $order_item_values[] = trim($row["category_cost_unit_number"]) == "" ? "null" : dbquote($row["category_cost_unit_number"]);
                    }

                    $order_item_fields[] = "order_items_costmonitoring_group";
                    $order_item_values[] = dbquote($row["item_costmonitoring_group"]);

                }

                $order_item_fields[] = "order_item_no_offer_required";
                $order_item_values[] = 1;


                if ($type > 0)
                {
                    $order_item_fields[] = "order_item_cost_group";
					$order_item_values[] = $row["item_cost_group"];
                }

                $order_item_fields[] = "date_created";
                $order_item_values[] = "current_timestamp";

                $order_item_fields[] = "date_modified";
                $order_item_values[] = "current_timestamp";
                
                if (isset($_SESSION["user_login"]))
                {
                    $order_item_fields[] = "user_created";
                    $order_item_values[] = dbquote($_SESSION["user_login"]);

                    $order_item_fields[] = "user_modified";
                    $order_item_values[] = dbquote($_SESSION["user_login"]);
                }
                $sql = "insert into order_items (" . join(", ", $order_item_fields) . ") values (" . join(", ", $order_item_values) . ")";
                mysql_query($sql) or dberror($sql);

                $item_id = mysql_insert_id();

                if ($type == ITEM_TYPE_STANDARD or $type == ITEM_TYPE_SPECIAL)
                {
					// copy delivery address for new item in table order_addresses
                    $sql = "select * from order_addresses ".
                           "where order_address_type = 2 ".
                           "    and order_address_order = ". param("oid") .
                           "    and (order_address_order_item = 0 ".
                           "    or order_address_order_item is null) ";

                    $res2 = mysql_query($sql) or dberror($sql);
                    $row2 = mysql_fetch_assoc($res2);

                    $delivery_address_fields = array();
                    $delivery_address_values = array();

                    $delivery_address_fields[] = "order_address_order";
                    $delivery_address_values[] = param("oid");

                    $delivery_address_fields[] = "order_address_order_item";
                    $delivery_address_values[] = $item_id;

                    $delivery_address_fields[] = "order_address_type";
                    $delivery_address_values[] = 2;

                    $delivery_address_fields[] = "order_address_company";
                    $delivery_address_values[] = dbquote($row2["order_address_company"]);

                    $delivery_address_fields[] = "order_address_company2";
                    $delivery_address_values[] = dbquote($row2["order_address_company2"]);

                    $delivery_address_fields[] = "order_address_address";
                    $delivery_address_values[] = dbquote($row2["order_address_address"]);

                    $delivery_address_fields[] = "order_address_address2";
                    $delivery_address_values[] = dbquote($row2["order_address_address2"]);

                    $delivery_address_fields[] = "order_address_zip";
                    $delivery_address_values[] = dbquote($row2["order_address_zip"]);

                    $delivery_address_fields[] = "order_address_place";
                    $delivery_address_values[] = dbquote($row2["order_address_place"]);

                    $delivery_address_fields[] = "order_address_country";
                    $delivery_address_values[] = dbquote($row2["order_address_country"]);

                    $delivery_address_fields[] = "order_address_phone";
                    $delivery_address_values[] = dbquote($row2["order_address_phone"]);

                    $delivery_address_fields[] = "order_address_fax";
                    $delivery_address_values[] = dbquote($row2["order_address_fax"]);

                    $delivery_address_fields[] = "order_address_email";
                    $delivery_address_values[] = dbquote($row2["order_address_email"]);

                    $delivery_address_fields[] = "order_address_parent";
                    $delivery_address_values[] = $row2["order_address_parent"];

                    $delivery_address_fields[] = "order_address_contact";
                    $delivery_address_values[] = dbquote($row2["order_address_contact"]);

                    $delivery_address_fields[] = "date_created";
                    $delivery_address_values[] = "current_timestamp";

                    $delivery_address_fields[] = "date_modified";
                    $delivery_address_values[] = "current_timestamp";

                    if (isset($_SESSION["user_login"]))
                    {
                        $delivery_address_fields[] = "user_created";
                        $delivery_address_values[] = dbquote($_SESSION["user_login"]);

                        $delivery_address_fields[] = "user_modified";
                        $delivery_address_values[] = dbquote($_SESSION["user_login"]);
                    }

                    $sql = "insert into order_addresses (" . join(", ", $delivery_address_fields) . ") values (" . join(", ", $delivery_address_values) . ")";
                    mysql_query($sql) or dberror($sql);
                
                }

            }
        }
    }

}

/********************************************************************
    delete design objective items of a project
*********************************************************************/
function delete_design_objective_items($project_id)
{
    $sql = "delete from project_items where project_item_project = " . $project_id;
    mysql_query($sql) or dberror($sql);

}

/********************************************************************
    update project type
*********************************************************************/
function project_update_project_product_line($project_id, $project_line)
{
    $sql = "update projects set project_product_line = " . $project_line . " where project_id = " .  $project_id;
    mysql_query($sql) or dberror($sql);

}


/********************************************************************
    save attachment infos concerning visibility and roles
*********************************************************************/
function save_attachment_accessibility_info($form, $check_box_names)
{

    // get order_file_id from the inserted record
    $sql = "select order_file_id ".
           "from order_files ".
           "where order_file_order = " . $form->value("order_file_order") . " " .
           "order by date_created DESC ".
           "limit 1";

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $attachment_id = $row["order_file_id"];
    }

    // insert records into order_file_addresses
    if (has_access("can_set_attachment_accessibility_in_orders") or has_access("can_set_attachment_accessibility_in_projects"))
    {
    
        foreach ($check_box_names as $key=>$value)
        {
            if ($form->value($key))
            {
                $sql = "select user_address from users where user_id = " . $value;
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				$address_id = $row["user_address"];

				$attachment_address_fields = array();
                $attachment_address_values = array();

                $attachment_address_fields[] = "order_file_address_file";
                $attachment_address_values[] = $attachment_id;

                $attachment_address_fields[] = "order_file_address_address";
                $attachment_address_values[] = $address_id;

                $attachment_address_fields[] = "date_created";
                $attachment_address_values[] = "current_timestamp";

                $attachment_address_fields[] = "date_modified";
                $attachment_address_values[] = "current_timestamp";
                
                if (isset($_SESSION["user_login"]))
                {
                    $attachment_address_fields[] = "user_created";
                    $attachment_address_values[] = dbquote($_SESSION["user_login"]);

                    $attachment_address_fields[] = "user_modified";
                    $attachment_address_values[] = dbquote($_SESSION["user_login"]);
                }
    
                $sql = "insert into order_file_addresses (" . join(", ", $attachment_address_fields) . ") values (" . join(", ", $attachment_address_values) . ")";
                mysql_query($sql) or dberror($sql);
            }
        }
    }
}


/********************************************************************
    update file accessibility
*********************************************************************/
function update_attachment_accessibility_info($id, $form, $check_box_names)
{
	$sql = "delete from order_file_addresses where order_file_address_file = " . $id;
    mysql_query($sql) or dberror($sql);

	foreach ($check_box_names as $key=>$value)
    {
		if ($form->value($key) == 1)
        {
			$sql = "select user_address from users where user_id = " . $value;
			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);
			$address_id = $row["user_address"];

			
			$attachment_address_fields = array();
            $attachment_address_values = array();

            $attachment_address_fields[] = "order_file_address_file";
            $attachment_address_values[] = $id;

            $attachment_address_fields[] = "order_file_address_address";
            $attachment_address_values[] = $address_id;

            $attachment_address_fields[] = "date_created";
            $attachment_address_values[] = "current_timestamp";

            $attachment_address_fields[] = "date_modified";
            $attachment_address_values[] = "current_timestamp";
                
            if (isset($_SESSION["user_login"]))
            {
                $attachment_address_fields[] = "user_created";
                $attachment_address_values[] = dbquote($_SESSION["user_login"]);

                $attachment_address_fields[] = "user_modified";
                $attachment_address_values[] = dbquote($_SESSION["user_login"]);
            }
    
            $sql = "insert into order_file_addresses (" . join(", ", $attachment_address_fields) . ") values (" . join(", ", $attachment_address_values) . ")";
                mysql_query($sql) or dberror($sql);
        }

    }
}

/********************************************************************
    delete attachment
*********************************************************************/
function delete_attachment($id)
{
    // delete access rights
    $sql = "delete from order_file_addresses where order_file_address_file = " . $id;
    mysql_query($sql) or dberror($sql);

    // delete file
    $sql = "select order_file_path from order_files where order_file_id = " . $id;

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $file = ".." . $row["order_file_path"];
        if (file_exists($file))
        {
            unlink($file);
        }
    }

    // delete attachment data
    $sql = "delete from order_files where order_file_id = " . $id;
    mysql_query($sql) or dberror($sql);


}


/********************************************************************
   update project client data
*********************************************************************/
function  order_update_client_data($form)
{

    $order_fields = array();
    $delivery_address_fields = array();

    // update record in table orders
    $value = $form->value("client_address_id");
    $order_fields[] = "order_client_address = " . $value;

    
    $value = $form->value("status");
    $order_fields[] = "order_actual_order_state_code = " . $value;

    $value = $form->value("client_address_user_id");
    $order_fields[] = "order_user = " . $value;

    $value = trim($form->value("billing_address_company")) == "" ? "null" : dbquote($form->value("billing_address_company"));
    $order_fields[] = "order_billing_address_company = " . $value;

     $value = trim($form->value("billing_address_company2")) == "" ? "null" : dbquote($form->value("billing_address_company2"));
    $order_fields[] = "order_billing_address_company2 = " . $value;

    $value = trim($form->value("billing_address_address")) == "" ? "null" : dbquote($form->value("billing_address_address"));
    $order_fields[] = "order_billing_address_address = " . $value;

    $value = trim($form->value("billing_address_address2")) == "" ? "null" : dbquote($form->value("billing_address_address2"));
    $order_fields[] = "order_billing_address_address2 = " . $value;

    $value = trim($form->value("billing_address_zip")) == "" ? "null" : dbquote($form->value("billing_address_zip"));
    $order_fields[] = "order_billing_address_zip = " . $value;

    $value = dbquote($form->value("billing_address_place_id"));
    $order_fields[] = "order_billing_address_place_id = " . $value;

	$value = trim($form->value("billing_address_place")) == "" ? "null" : dbquote($form->value("billing_address_place"));
    $order_fields[] = "order_billing_address_place = " . $value;

    $value = trim($form->value("billing_address_country")) == "" ? "null" : dbquote($form->value("billing_address_country"));
    $order_fields[] = "order_billing_address_country = " . $value;

    $value = trim($form->value("billing_address_phone")) == "" ? "null" : dbquote($form->value("billing_address_phone"));
    $order_fields[] = "order_billing_address_phone = " . $value;

    $value = trim($form->value("billing_address_fax")) == "" ? "null" : dbquote($form->value("billing_address_fax"));
    $order_fields[] = "order_billing_address_fax = " . $value;

    $value = trim($form->value("billing_address_email")) == "" ? "null" : dbquote($form->value("billing_address_email"));
    $order_fields[] = "order_billing_address_email = " . $value;

    $value = trim($form->value("billing_address_contact")) == "" ? "null" : dbquote($form->value("billing_address_contact"));
    $order_fields[] = "order_billing_address_contact = " . $value;

    $value = trim($form->value("shop_address_company")) == "" ? "null" : dbquote($form->value("shop_address_company"));
    $order_fields[] = "order_shop_address_company = " . $value;

     $value = trim($form->value("shop_address_company2")) == "" ? "null" : dbquote($form->value("shop_address_company2"));
    $order_fields[] = "order_shop_address_company2 = " . $value;

    $value = trim($form->value("shop_address_address")) == "" ? "null" : dbquote($form->value("shop_address_address"));
    $order_fields[] = "order_shop_address_address = " . $value;

    $value = trim($form->value("shop_address_address2")) == "" ? "null" : dbquote($form->value("shop_address_address2"));
    $order_fields[] = "order_shop_address_address2 = " . $value;

    $value = trim($form->value("shop_address_zip")) == "" ? "null" : dbquote($form->value("shop_address_zip"));
    $order_fields[] = "order_shop_address_zip = " . $value;

    $value = trim($form->value("shop_address_place")) == "" ? "null" : dbquote($form->value("shop_address_place"));
    $order_fields[] = "order_shop_address_place = " . $value;

    $value = trim($form->value("shop_address_country")) == "" ? "null" : dbquote($form->value("shop_address_country"));
    $order_fields[] = "order_shop_address_country = " . $value;

	$value = trim($form->value("shop_address_phone")) == "" ? "null" : dbquote($form->value("shop_address_phone"));
    $order_fields[] = "order_shop_address_phone = " . $value;

	$value = trim($form->value("shop_address_fax")) == "" ? "null" : dbquote($form->value("shop_address_fax"));
    $order_fields[] = "order_shop_address_fax = " . $value;

	$value = trim($form->value("shop_address_email")) == "" ? "null" : dbquote($form->value("shop_address_email"));
    $order_fields[] = "order_shop_address_email = " . $value;

    $value = trim($form->value("preferred_transportation_arranged")) == "" ? "null" : dbquote($form->value("preferred_transportation_arranged"));
    $order_fields[] = "order_preferred_transportation_arranged = " . $value;

	$value = trim($form->value("preferred_transportation_mode")) == "" ? "null" : dbquote($form->value("preferred_transportation_mode"));
    $order_fields[] = "order_preferred_transportation_mode = " . $value;

    $value = trim($form->value("voltage")) == "" ? "null" : dbquote($form->value("voltage"));
    $order_fields[] = "order_voltage = " . $value;

    $value = trim($form->value("preferred_delivery_date")) == "" ? "null" : dbquote(from_system_date($form->value("preferred_delivery_date")));
    $order_fields[] = "order_preferred_delivery_date = " . $value;


    $value = trim($form->value("full_delivery")) == "" ? "null" : dbquote($form->value("full_delivery"));
    $order_fields[] = "order_full_delivery = " . $value;

    //$value = trim($form->value("packaging_retraction")) == "" ? "null" : dbquote($form->value("packaging_retraction"));
    //$order_fields[] = "order_packaging_retraction = " . $value;

    $value = trim($form->value("pedestrian_mall_approval")) == "" ? "null" : dbquote($form->value("pedestrian_mall_approval"));
    $order_fields[] = "order_pedestrian_mall_approval = " . $value;

    $value = trim($form->value("delivery_comments")) == "" ? "null" : dbquote($form->value("delivery_comments"));
    $order_fields[] = "order_delivery_comments = " . $value;

	$value = dbquote($form->value("order_insurance"));
    $order_fields[] = "order_insurance = " . $value;


    $value = "current_timestamp";
    $order_fields[] = "date_modified = " . $value;

    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $order_fields[] = "user_modified = " . $value;
    }

    $sql = "update orders set " . join(", ", $order_fields) . " where order_id = " . $form->value("oid");
    mysql_query($sql) or dberror($sql);


    // update delivery address in table oder_addresses
    $value = trim($form->value("delivery_address_company")) == "" ? "null" : dbquote($form->value("delivery_address_company"));
    $delivery_address_fields[] = "order_address_company = " . $value;

     $value = trim($form->value("delivery_address_company2")) == "" ? "null" : dbquote($form->value("delivery_address_company2"));
    $delivery_address_fields[] = "order_address_company2 = " . $value;

    $value = trim($form->value("delivery_address_address")) == "" ? "null" : dbquote($form->value("delivery_address_address"));
    $delivery_address_fields[] = "order_address_address = " . $value;

    $value = trim($form->value("delivery_address_address2")) == "" ? "null" : dbquote($form->value("delivery_address_address2"));
    $delivery_address_fields[] = "order_address_address2 = " . $value;

    $value = trim($form->value("delivery_address_zip")) == "" ? "null" : dbquote($form->value("delivery_address_zip"));
    $delivery_address_fields[] = "order_address_zip = " . $value;

    $value = dbquote($form->value("delivery_address_place_id"));
    $delivery_address_fields[] = "order_address_place_id = " . $value;

	$value = trim($form->value("delivery_address_place")) == "" ? "null" : dbquote($form->value("delivery_address_place"));
    $delivery_address_fields[] = "order_address_place = " . $value;

    $value = trim($form->value("delivery_address_country")) == "" ? "null" : dbquote($form->value("delivery_address_country"));
    $delivery_address_fields[] = "order_address_country = " . $value;

    $value = trim($form->value("delivery_address_phone")) == "" ? "null" : dbquote($form->value("delivery_address_phone"));
    $delivery_address_fields[] = "order_address_phone = " . $value;

    $value = trim($form->value("delivery_address_fax")) == "" ? "null" : dbquote($form->value("delivery_address_fax"));
    $delivery_address_fields[] = "order_address_fax = " . $value;

    $value = trim($form->value("delivery_address_email")) == "" ? "null" : dbquote($form->value("delivery_address_email"));
    $delivery_address_fields[] = "order_address_email = " . $value;

    $value = $form->value("client_address_id");
    $delivery_address_fields[] = "order_address_parent = " . $value;

    $value = trim($form->value("delivery_address_contact")) == "" ? "null" : dbquote($form->value("delivery_address_contact"));
    $delivery_address_fields[] = "order_address_contact = " . $value;

    $value = "current_timestamp";
    $delivery_address_fields[] = "date_modified = " . $value;

    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $delivery_address_fields[] = "user_modified = " . $value;
    }

    $sql = "update order_addresses set " . join(", ", $delivery_address_fields) . " where order_address_type = 2 and order_address_order = " . $form->value("oid");
    mysql_query($sql) or dberror($sql);


	// update POSaddresses (POS Index)
	if($form->value("posaddress_id") > 0)
	{
		$sql = "select posorder_id " . 
			   "from posorders " . 
			   "where posorder_order = " . $form->value("oid");
		
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
		
			$sql = "Update posorders set " . 
				   "posorder_posaddress = " . $form->value("posaddress_id") . 
				   " where posorder_id = " . $row["posorder_id"];

			mysql_query($sql) or dberror($sql);
		}
		else
		{
			$sql = "insert into posorders (" . 
				   "posorder_posaddress, " .
				   "posorder_order, " .
				   "posorder_ordernumber, " .
				   "posorder_type) Values ( " .
				   $form->value("posaddress_id") . ", " .
				   $form->value("oid") . ", " .
				   dbquote($form->value("order_number")) . ", " .
				   2  . ")";

			$result = mysql_query($sql) or dberror($sql);
		}
	
	}


} // order_update_client_data


/********************************************************************
   update project retail data
*********************************************************************/
function  project_update_retail_data($form)
{

    $project_fields = array();
    $order_fields = array();

    // update record in table projects

	$value = dbquote($form->value("product_line_subclass"));
    $project_fields[] = "project_product_line_subclass = " . $value;

    $value = dbquote($form->value("retail_coordinator"));
    $project_fields[] = "project_retail_coordinator = " . $value;

	//$value = dbquote($form->value("hq_project_manager"));
    //$project_fields[] = "project_hq_project_manager = " . $value;

	$value = dbquote($form->value("local_retail_coordinator"));
    $project_fields[] = "project_local_retail_coordinator = " . $value;
    
    
	if($form->value("project_no_planning") == 1) {
		$project_fields[] = "project_design_contractor = 0";
	}
	else
	{
		$value = dbquote($form->value("contractor_user_id"));
		$project_fields[] = "project_design_contractor = " . $value;
	}
    
    
    $value = dbquote($form->value("supervisor_user_id"));
    $project_fields[] = "project_design_supervisor = " . $value;

	$value = dbquote($form->value("cms_approver_user_id"));
    $project_fields[] = "project_cms_approver = " . $value;

    $value = dbquote(from_system_date($form->value("project_real_opening_date")));
    $project_fields[] = "project_real_opening_date = " . $value;

	$value = dbquote($form->value("project_state"));
    $project_fields[] = "project_state = " . $value;

	$value = dbquote($form->value("project_no_planning"));
    $project_fields[] = "project_no_planning = " . $value;

	$value = dbquote($form->value("project_is_local_production"));
    $project_fields[] = "project_is_local_production = " . $value;

	$value = dbquote($form->value("project_is_special_production"));
    $project_fields[] = "project_is_special_production = " . $value;

	$value = dbquote($form->value("project_is_relocation_project"));
    $project_fields[] = "project_is_relocation_project = " . $value;

	$value = dbquote($form->value("project_relocated_posaddress_id"));
    $project_fields[] = "project_relocated_posaddress_id = " . $value;


    $value = dbquote($form->value("project_use_ps2004"));
    $project_fields[] = "project_use_ps2004 = " . $value;

    $value = "current_timestamp";
    $project_fields[] = "date_modified = " . $value;
    
    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $project_fields[] = "user_modified = " . $value;
    }

    $sql = "update projects set " . join(", ", $project_fields) . " where project_id = " . $form->value("pid");
    mysql_query($sql) or dberror($sql);

    $_REQUEST["id"] = $form->value("pid");


    // update record in table orders
    
    $value = dbquote($form->value("retail_operator"));
    $order_fields[] = "order_retail_operator = " . $value;

    $value = dbquote($form->value("delivery_confirmation_by"));
    $order_fields[] = "order_delivery_confirmation_by = " . $value;

    $value = "current_timestamp";
    $order_fields[] = "date_modified = " . $value;
    
    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $order_fields[] = "user_modified = " . $value;
    }

    $sql = "update orders set " . join(", ", $order_fields) . " where order_id = " . $form->value("oid");
    mysql_query($sql) or dberror($sql);


	//update posorders
	$sql = "update posorders set " . 
		   "posorder_project_locally_produced = " . dbquote($form->value("project_is_local_production")) . ", " . 
		   "posorder_project_special_project = " . dbquote($form->value("project_is_special_production")) . 
		   " where posorder_order = " . dbquote($form->value("oid"));
	$result = mysql_query($sql) or dberror($sql);
	$sql = "update posorderspipeline set " . 
		   "posorder_project_locally_produced = " . dbquote($form->value("project_is_local_production")) . ", " . 
		   "posorder_project_special_project = " . dbquote($form->value("project_is_special_production")) . 
		   " where posorder_order = " . dbquote($form->value("oid"));
	$result = mysql_query($sql) or dberror($sql);

}


/********************************************************************
    save comment
*********************************************************************/
function save_comment($form, $check_box_names)
{
    $comment_fields = array();
    $comment_values = array();

    $comment_fields[] = "comment_order";
    $comment_values[] = $form->value("order_id");

    $comment_fields[] = "comment_user";
    $comment_values[] = user_id();

    $comment_fields[] = "comment_category";
    $comment_values[] = $form->value("comment_category");

    $comment_fields[] = "comment_text";
    $comment_values[] = dbquote($form->value("comment_text"));

    $comment_fields[] = "date_created";
    $comment_values[] = "current_timestamp";

    $comment_fields[] = "date_modified";
    $comment_values[] = "current_timestamp";
                
    if (isset($_SESSION["user_login"]))
    {
        $comment_fields[] = "user_created";
        $comment_values[] = dbquote($_SESSION["user_login"]);

        $comment_fields[] = "user_modified";
        $comment_values[] = dbquote($_SESSION["user_login"]);
    }
    
    $sql = "insert into comments (" . join(", ", $comment_fields) . ") values (" . join(", ", $comment_values) . ")";
    mysql_query($sql) or dberror($sql);

    $comment_id = mysql_insert_id();


    // insert records into comment_addresses

    if (has_access("can_set_comment_accessibility_in_projects") or has_access("can_set_comment_accessibility_in_orders"))
    {

        foreach ($check_box_names as $key=>$value)
        {
            if ($form->value($value))
            {
                $comment_address_fields = array();
                $comment_address_values = array();

                $comment_address_fields[] = "comment_address_comment";
                $comment_address_values[] = $comment_id;

                $comment_address_fields[] = "comment_address_address";
                $comment_address_values[] = $key;

                $comment_address_fields[] = "date_created";
                $comment_address_values[] = "current_timestamp";

                $comment_address_fields[] = "date_modified";
                $comment_address_values[] = "current_timestamp";
                
                if (isset($_SESSION["user_login"]))
                {
                    $comment_address_fields[] = "user_created";
                    $comment_address_values[] = dbquote($_SESSION["user_login"]);

                    $comment_address_fields[] = "user_modified";
                    $comment_address_values[] = dbquote($_SESSION["user_login"]);
                }
    
                $sql = "insert into comment_addresses (" . join(", ", $comment_address_fields) . ") values (" . join(",        ", $comment_address_values) . ")";
                mysql_query($sql) or dberror($sql);
            }
        }
    }
    return $comment_id;
}
?>