<?php
/********************************************************************

    order_page_actions.php

    Defines the possible page actions depending on access rights

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-25
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-12-03
    Version:        1.1.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "order_state_constants.php";


if (has_access("can_edit_client_data_in_orders"))
{   
    $page->register_action('view_client_data', 'Edit Request', 
                           "order_edit_request.php?oid=" . param('oid') . "&y1=" . param("y1") . "&y2=" . param("y2"));

	$page->register_action('view_client_data', 'View Client Data', 
                           "order_view_client_data.php?oid=" . param('oid') . "&y1=" . param("y1") . "&y2=" . param("y2"));
}
elseif (has_access("can_view_client_data_in_orders"))
{   
    $page->register_action('view_client_data', 'View Client Data', 
                           "order_view_client_data.php?oid=" . param('oid') . "&y1=" . param("y1") . "&y2=" . param("y2"));
}

if (has_access("can_edit_retail_data"))
{   
    $page->register_action('edit_retail_data', 'View Retail Data', 
                           "order_view_retail_data.php?oid=" . param('oid') . "&y1=" . param("y1") . "&y2=" . param("y2"));
}

if (has_access("can_edit_list_of_materials_in_orders"))
{   
    $page->register_action('edit_material_list', 'View List of Materials', 
                           "order_view_material_list.php?oid=" . param('oid') . "&y1=" . param("y1") . "&y2=" . param("y2"));
}


if (has_access("can_edit_his_list_of_materials_in_orders"))
{   
    $page->register_action('edit_material_list_supplier', 'View Offer Data', 
                           "order_view_material_list_supplier.php?oid=" . param('oid') . "&y1=" . param("y1") . "&y2=" . param("y2"));
}

if (has_access("can_edit_budget_in_orders"))
{   
    $page->register_action('edit_order_budget', 'View ' . BRAND . '\'s Budget',
                           "order_view_order_swatch_budget.php?oid=" . param("oid") . "&y1=" . param("y1") . "&y2=" . param("y2"));

}

if (has_access("has_access_to_all_orders"))
{   
   $page->register_action('view_order_budget', 'View Client\'s Budget', 
                          "order_view_order_client_budget.php?oid=" . param("oid") . "&y1=" . param("y1") . "&y2=" . param("y2"));

   $url = "http://" . $_SERVER["HTTP_HOST"] . "/archive/order_view_order_budget_pdf.php?oid=" . param("oid");
   $page->register_action('print_budget', 'Print Budget', $url, "_blank");

}
else
{
    if (has_access("can_view_budget_in_orders"))
    {   
        //if (is_present_order_state(param("oid"), REQUEST_FOR_BUDGET_APPROVAL_SUBMITTED))
        //{
            $page->register_action('view_order_budget', 'View Client\'s Budget', 
                          "order_view_order_client_budget.php?oid=" . param("oid") . "&y1=" . param("y1") . "&y2=" . param("y2"));

            $url = "http://" . $_SERVER["HTTP_HOST"] . "/archive/order_view_order_budget_pdf.php?oid=" . param("oid");
            $page->register_action('print_budget', 'Print Budget', $url, "_blank");
        //}
    }
}


if (has_access("can_view_ordered_values_in_orders"))
{   
    $page->register_action('edit_view_ordered_values', 'View Ordered Values', 
                           "order_view_ordered_values.php?oid=" . param('oid'));
}

if (has_access("can_view_order_invoice_information"))
{   
	$page->register_action('view_cost_information', 'View Cost Information', 
						   "order_view_cost_information.php?oid=" . param("oid"));
}

if (has_access("can_view_delivery_schedule_in_orders"))
{
    
    $url = "http://" . $_SERVER["HTTP_HOST"] . "/archive/order_view_traffic_data_pdf.php?oid=" . param("oid");
    $page->register_action('print_traffic_data', 'Print Delivery Schedule', $url, "_blank");

    $page->register_action('view_traffic_data', 'View Delivery Schedule', 
                           "order_view_traffic_data.php?oid=" .param('oid') . "&y1=" . param("y1") . "&y2=" . param("y2"));
}


if (has_access("can_view_comments_in_orders"))
{   
    $page->register_action('nothing1', '', "");
    $page->register_action('view_comments', 'Comments', "order_view_comments.php?oid=" . param("oid") . "&y1=" . param("y1") . "&y2=" . param("y2"));
}

if (has_access("can_view_attachments_in_orders"))
{   
    $page->register_action('view_attachments', 'Attachments', "order_view_attachments.php?oid=" . param("oid") . "&y1=" . param("y1") . "&y2=" . param("y2"));
}

if (has_access("can_use_taskcentre_in_orders"))
{   
    $page->register_action('nothing1', '', "");
    $page->register_action('flow_control', 'Task Centre', "order_task_center.php?oid=" . param("oid") . "&y1=" . param("y1") . "&y2=" . param("y2"));
}

if (has_access("can_view_history_in_orders"))
{   
    $page->register_action('history', 'View History', "order_history.php?oid=" . param("oid") . "&y1=" . param("y1") . "&y2=" . param("y2"));
    $page->register_action('mail_history', 'View Mails', "order_mail_history.php?oid=" . param("oid") . "&y1=" . param("y1") . "&y2=" . param("y2"));
}

$page->register_action('nothing2', '', "");
$link = "orders_archive_orders.php?y1=" . param("y1") . "&y2=" . param("y2");
$page->register_action('back', 'Back To List', $link);
//$page->register_action('orders', 'Orders', "orders_archive.php");
$page->register_action('projects', 'Projects', "projects_archive.php");
$page->register_action('home', 'Quit Archive', "../user/welcome.php");

?>