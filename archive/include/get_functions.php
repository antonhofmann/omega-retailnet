<?php
/********************************************************************

    get_functions.php

    Various utility functions to get information from tables.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-07
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-19

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

/********************************************************************
    get category name
*********************************************************************/
function get_category_name($id)
{

    $category_name = "";

    $sql = "select category_name, product_line_name " .
           "from categories " .
           "left join product_lines on product_lines.product_line_id = " .
           "categories.category_product_line " .
           "where category_id = " . $id;

    $res = mysql_query($sql);

    if (($res) && (mysql_num_rows($res) != 0))
    {
        $row = mysql_fetch_assoc($res);
        $category_name = $row['product_line_name']. ", " .$row['category_name'] . ": Itemlist";
    }

    return $category_name;
}


/********************************************************************
    get currency informations assigned to an address
*********************************************************************/
function get_address_currency($address_id)
{
    $currency = array();

    $sql = "select address_currency from addresses where address_id = " . dbquote($address_id);
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $currency_id = $row["address_currency"];
    }

    if ($currency_id > 0)
    {
        $sql = "select * from currencies where currency_id = " . $currency_id;
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $currency["id"] = $currency_id;
            $currency["symbol"] = $row["currency_symbol"];
            $currency["exchange_rate"] = $row["currency_exchange_rate"];
            $currency["factor"] = $row["currency_factor"];
        }
    }
    else
    {
        $currency["id"] = 0;
        $currency["symbol"] = "";
        $currency["exchange_rate"] = 0;
        $currency["factor"] = 0;
    }
    
    return $currency;
}

/********************************************************************
    get currency informations assigned to an order
*********************************************************************/
function get_order_currency($order_id)
{
    $currency = array();

    $sql = "select * from orders where order_id = " . $order_id;
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $currency_id = $row["order_client_currency"];
        $currency["exchange_rate"] = $row["order_client_exchange_rate"];
    }


    if ($currency_id > 0)
    {
        $sql = "select * from currencies where currency_id = " . $currency_id;
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $currency["id"] = $currency_id;
            $currency["symbol"] = $row["currency_symbol"];
            $currency["exchange_rate"] = $row["currency_exchange_rate"];
            $currency["factor"] = $row["currency_factor"];
        }
    }
    else
    {
        $currency["id"] = 0;
        $currency["symbol"] = "";
        $currency["exchange_rate"] = 0;
        $currency["factor"] = 0;
    }
    
    return $currency;
}


/********************************************************************
    get currency symbol
*********************************************************************/
function get_currency_symbol($currency_id)
{
    if ($currency_id)
    {        $sql = "select * from currencies where currency_id = " . $currency_id;
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $currency_symbol = $row["currency_symbol"];
        }
    }
    else
    {
        $currency_symbol = "n/a";
    }
    return $currency_symbol;
}


/********************************************************************
    get item currency symbol from supplier table
*********************************************************************/
function get_item_currency_symbol($supplier_id, $item_id)
{
    $currency_symbol = "";

    $sql = "select currency_symbol " .
           "from suppliers " .
           "left join currencies on supplier_item_currency = currency_id ".
           "where supplier_address = " . $supplier_id .
           "   and supplier_item = " . $item_id;
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $currency_symbol = $row["currency_symbol"];
    }
    
    
    return $currency_symbol;


}

/********************************************************************
    get system currency informations
*********************************************************************/
function get_system_currency_fields()
{
    $system_currency = array();

    $sql = "select * from currencies where currency_system=1";
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $system_currency["id"] = $row["currency_id"];
        $system_currency["symbol"] = $row["currency_symbol"];
        $system_currency["exchange_rate"] = $row["currency_exchange_rate"];
        $system_currency["factor"] = $row["currency_factor"];
    }
    
    return $system_currency;
}



/********************************************************************
    get currency informations
*********************************************************************/
function get_currency($currency_id)
{
    $currency = array();

    $sql = "select * from currencies where currency_id = " . $currency_id;
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $currency["id"] = $currency_id;
        $currency["symbol"] = $row["currency_symbol"];
        $currency["exchange_rate"] = $row["currency_exchange_rate"];
        $currency["factor"] = $row["currency_factor"];
    }
    else
    {
        $currency["id"] = 0;
        $currency["symbol"] = "";
        $currency["exchange_rate"] = 0;
        $currency["factor"] = 0;
    }
    
    return $currency;
}


/********************************************************************
    get user's currency
*********************************************************************/

function get_user_currency ($user_id)
{
    $currency = array();
    
    $sql_currency = "select currency_symbol " .
                    "from currencies " .
                    "left join users on users.user_id = " . user_id() . " " .
                    "left join addresses on addresses.address_id = users.user_address " .
                    "where currencies.currency_id = addresses.address_currency ";
    
    $res = mysql_query($sql_currency) or dberror($sql_currency);
    if ($res)
    {
     $currency = mysql_fetch_assoc($res);
    }
    return $currency;
}

/********************************************************************
    get the field values of an address
*********************************************************************/
function get_address($id)
{
    $address = array();

    if ($id == '')
    {
            $address["id"] = "";
			$address["shortcut"] = "";
            $address["company"] = "";
            $address["company2"] = "";
            $address["address"] = "";
            $address["address2"] = "";
            $address["zip"] = "";
            $address["place"] = "";
			$address["place_id"] = "";
			$address["place_province"] = "";
            $address["country"] = "";
            $address["country_name"] = "";
            $address["currency"] = "";
            $address["phone"] = "";
            $address["fax"] = "";
            $address["email"] = "";
            $address["contact"] = "";
            $address["client_type"] = "";
			$address["contact_name"] = "";
			$address["website"] = "";
			$address["country_region"] = "";
    }

    else
    {
        $sql = "select * from addresses left join places on place_id = address_place_id where address_id = " . dbquote($id);
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $address["id"] = $row["address_id"];
			$address["shortcut"] = $row["address_shortcut"];
            $address["company"] = $row["address_company"];
            $address["company2"] = $row["address_company2"];
            $address["address"] = $row["address_address"];
            $address["address2"] = $row["address_address2"];
            $address["zip"] = $row["address_zip"];
            $address["place"] = $row["address_place"];
			$address["place_id"] = $row["address_place_id"];
			$address["place_province"] = $row["place_province"];
            $address["country"] = $row["address_country"];
            $address["country_name"] = "";
            $address["currency"] = $row["address_currency"];
            $address["phone"] = $row["address_phone"];
            $address["fax"] = $row["address_fax"];
            $address["email"] = $row["address_email"];
            $address["contact"] = $row["address_contact"];
            $address["client_type"] = $row["address_client_type"];
			$address["contact_name"] = $row["address_contact_name"];
			$address["website"] = $row["address_website"];

            $sql = "select country_id, country_name, country_region ".
                   "from countries ".
                   "where country_id = " . dbquote($address["country"]);



            $res = mysql_query($sql);
            if ($res)
            {
				$row = mysql_fetch_assoc($res);
                $address["country_name"] = $row['country_name'];
				$address["country_region"] = $row['country_region'];
            }

        }
    }


    return $address;
}

/********************************************************************
    get the region code of an address
*********************************************************************/
function get_address_region($address_id)
{
    $region = 0;

    $sql = "select country_region ".
           "from addresses " .
           "left join countries on address_country = country_id ".
           "where address_id = " . dbquote($address_id);

     $res = mysql_query($sql) or dberror($sql);
     if ($row = mysql_fetch_assoc($res))
     {
        $region = $row["country_region"];
     }
     return $region;
}


/********************************************************************
    get the region code of a user's address
*********************************************************************/
function get_user_region($user_id)
{
    $region = 0;

    $sql = "select country_region ".
           "from users " .
           "left join addresses on user_address = address_id ".
           "left join countries on address_country = country_id ".
           "where user_id = " . $user_id;

     $res = mysql_query($sql) or dberror($sql);
     if ($row = mysql_fetch_assoc($res))
     {
        $region = $row["country_region"];
     }
     return $region;
}

/********************************************************************
    get the field values of a user
*********************************************************************/
function get_user($id)
{
    $user = array();

    if ($id == '' or $id == 0)
    {
        $user["firstname"] = "";
        $user["name"] = "";
        $user["contact"] = "";
        $user["phone"] = "";
        $user["fax"] = "";
        $user["email"] = "";
        $user["cc"] = "";
        $user["deputy"] = "";
        $user["address"] = "";
		$user["country"] = "";
		$user["has_access_to_travelling_projects"] = false;
    }
    else
    {
        $sql = "select * from users left join addresses on address_id = user_address where user_id = " . $id;
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $user["firstname"] = $row["user_firstname"];
            $user["name"] = $row["user_name"];
            $user["contact"] = $row["user_name"] . " " . $row["user_firstname"];
            $user["phone"] = $row["user_phone"];
            $user["fax"] = $row["user_fax"];
            $user["email"] = $row["user_email"];
            $user["cc"] = $row["user_email_cc"];
            $user["deputy"] = $row["user_email_deputy"];
            $user["address"] = $row["user_address"];
			$user["country"] = $row["address_country"];


			$sql = "select * from posareatypes " . 
				   "where posareatype_id IN (4, 5) and " . 
				   "posareatype_email1 = " . dbquote($user["email"]) . 
				   " or posareatype_email2 = " . dbquote($user["email"]) .
				   " or posareatype_email3 = " . dbquote($user["email"]) .
				   " or posareatype_email4 = " . dbquote($user["email"]) .
				   " or posareatype_email5 = " . dbquote($user["email"]) .
				   " or posareatype_email6 = " . dbquote($user["email"]);
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$user["has_access_to_travelling_projects"] = true;
			}
        }

		

    }
    return $user;
}


/*************************************************************************
   get all the roles of a user
**************************************************************************/
function get_user_roles($user_id)
{
    $user_roles = array();
    
    $sql = "select user_role_role ".
           "from user_roles ".
           "left join roles on user_role_role = role_id ".
           "where user_role_user = " . $user_id;

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $user_roles[] = $row["user_role_role"];
    }

    return $user_roles;
}

/********************************************************************
    get all franchisee addresses used by the client up to now
*********************************************************************/
function get_franchisee_addresses($id)
{
    $franchisee_addresses=array();

    $sql = "select order_id, ". 
           "concat(order_franchisee_address_company, ', ', order_franchisee_address_address, ', ', order_franchisee_address_contact) as full_name ".
           "from orders ".
           "where order_franchisee_address_company is not NULL and order_client_address = ". $id . " ".
           "order by order_franchisee_address_company ";

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        $franchisee_addresses[$row["order_id"]]=$row["full_name"];
    }
    
    $franchisee_addresses=array_unique($franchisee_addresses);

    return $franchisee_addresses;
}

/********************************************************************
    get all invoice addresses used by the client up to now
*********************************************************************/
function get_billing_addresses($id)
{
    $billing_addresses=array();

    $sql = "select order_id, ". 
           "concat(order_billing_address_company, ', ', order_billing_address_address, ', ', order_billing_address_contact) as full_name ".
           "from orders ".
           "where order_client_address = ". $id . " ".
           "order by order_billing_address_company ";

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        $billing_addresses[$row["order_id"]]=$row["full_name"];
    }
    
    $billing_addresses=array_unique($billing_addresses);

    return $billing_addresses;
}


/********************************************************************
    get all delivery addresses used by the client up to now
*********************************************************************/
function get_delivery_addresses($id)
{
    $delivery_addresses=array();

    $sql = "select order_address_id, concat(order_address_company, ', ', order_address_address, ', ', order_address_place) as address ".
           "from order_addresses ".
           "where order_address_type=2 and order_address_parent = ". $id . " ".
           "order by order_address_company ";

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        $delivery_addresses[$row["order_address_id"]]=$row["address"];
    }
    
    $delivery_addresses=array_unique($delivery_addresses);

    return $delivery_addresses;
}


/********************************************************************
    get all warehouse addresses used by the supplier up to now
*********************************************************************/
function get_warehouse_addresses($supplier_id)
{
    $warehouse_addresses=array();
    if ($supplier_id)
    {
        $sql = "select order_address_id, ".
               "concat(order_address_company, ', ', order_address_contact) as full_name ".
               "from order_addresses ".
               "where order_address_type=4 and order_address_parent = ". $supplier_id . " ".
               "order by order_address_company ";

        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {
            $warehouse_addresses[$row["order_address_id"]]=$row["full_name"];
        }
    
        $warehouse_addresses=array_unique($warehouse_addresses);
    }
    return $warehouse_addresses;
}

/********************************************************************
    get project and order data 
*********************************************************************/
function get_project($id)
{

	$project = array();

    $sql = "select *, projects.date_created as date_created, " .
		   "IF(TIMESTAMPDIFF(MONTH,project_fagrstart,project_fagrend) >0, TIMESTAMPDIFF(MONTH,project_fagrstart,project_fagrend), 'n.a.') as dmonths " . 
           "from projects ".
           "left join product_lines on project_product_line = product_line_id ".
			"left join productline_subclasses on productline_subclass_id = project_product_line_subclass ".
           "left join postypes on postype_id = project_postype ".
           "left join orders on project_order = order_id ".
           "left join transportation_types as transportation_types1 on order_preferred_transportation_arranged = transportation_types1. transportation_type_id ".
		   "left join transportation_types as transportation_types2 on order_preferred_transportation_mode = transportation_types2. transportation_type_id ".
		   "left join project_costs on project_cost_order = order_id " .
		   "left join project_costtypes on project_costtype_id = project_cost_type " .
		   "left join project_states on project_state_id = project_state ".
		   "left join projectkinds on projectkind_id = project_projectkind " .
		   "left join possubclasses on possubclass_id = project_pos_subclass " .
		   "left join agreement_types on agreement_type_id = project_fagagreement_type " . 
		    "left join users on user_id = order_user " .
           "where project_id  = " . $id;


    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $project = $row;
		$project["submitted_by"] = $row["user_name"] . " " . $row["user_firstname"];
    }


	$project["project_manager"] = "";
	$sql = "select concat(user_name, ' ', user_firstname) as username from users where user_id =" . dbquote($project["project_retail_coordinator"]);
	$res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $project["project_manager"] = $row["username"];
    }
	
	$project["operator"] = "";
	$sql = "select concat(user_name, ' ', user_firstname) as username from users where user_id =" . dbquote($project["order_retail_operator"]);
	$res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $project["operator"] = $row["username"];
    }

	
	$project["order_franchisee_address_country_name"] = "";
	$sql = "select country_name ".
           "from countries ".
           "where country_id  = " . dbquote($project["order_franchisee_address_country"]);

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $project["order_franchisee_address_country_name"] = $row["country_name"];
    }

    
	$project["order_shop_address_country_name"] = "";

	$sql = "select country_name ".
           "from countries ".
           "where country_id  = " . dbquote($project["order_shop_address_country"]);

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $project["order_shop_address_country_name"] = $row["country_name"];
    }
	else
	{
		$project["order_shop_address_country"] = "0";
	}

    $sql = "select country_name ".
           "from countries ".
           "where country_id  = " . dbquote($project["order_billing_address_country"]);


	$res = mysql_query($sql) or dberror($sql);

	$project["order_billing_address_country_name"] = "";
    if ($row = mysql_fetch_assoc($res))
    {
        $project["order_billing_address_country_name"] = $row["country_name"];
    }
	
	//get pos address from POS Index
	$sql = "select posorder_posaddress from posorders where posorder_order = " . $project["order_id"];
	$res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $project["posaddress_id"] = $row["posorder_posaddress"];
		$project["pipeline"] = 0;
    }
	else
	{
		$sql = "select posorder_posaddress from posorderspipeline where posorder_order = " . $project["order_id"];
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$project["posaddress_id"] = $row["posorder_posaddress"];
			$project["pipeline"] = 1;
		}
		else
		{
			$project["posaddress_id"] = "";
			$project["pipeline"] = 0;
		}
	}


	//postypes CER/AF
	$project["needs_cer"] = 0;
	$project["needs_af"] = 0;
	$project["needs_inr03"] = 0;

	$sql = "select posproject_type_needs_cer, posproject_type_needs_af, posproject_type_needs_inr03 " . 
		   " from posproject_types " . 
	       "where posproject_type_postype = " . dbquote($project["project_postype"]) .  
		   " and posproject_type_projectcosttype = " . dbquote($project["project_cost_type"]) . 
		   " and posproject_type_projectkind = " . dbquote($project["project_projectkind"]);

	$res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $project["needs_cer"] = $row["posproject_type_needs_cer"];
		$project["needs_af"] = $row["posproject_type_needs_af"];
		$project["needs_inr03"] = $row["posproject_type_needs_inr03"];
    }
    
    return $project;
}



/********************************************************************
    get order data 
*********************************************************************/
function get_order($id)
{
    $order = array();

    $sql = "select * ".
           "from orders ".
           "left join transportation_types as transportation_types1 on order_preferred_transportation_arranged = transportation_types1. transportation_type_id ".
		   "left join transportation_types as transportation_types2 on order_preferred_transportation_mode = transportation_types2. transportation_type_id ".
		   "left join addresses on address_id =order_client_address " . 
		   "left join users on user_id = order_user " .
           "where order_id  = " . $id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $order = $row;
		$order["submitted_by"] = $row["user_name"] . " " . $row["user_firstname"];
    }

	$order["order_shop_address_country_name"] = "";
    $sql = "select country_name ".
           "from countries ".
           "where country_id  = " . dbquote($order["order_shop_address_country"]);

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $order["order_shop_address_country_name"] = $row["country_name"];
    }

    
	$order["order_billing_address_country_name"] = "";
	$sql = "select country_name ".
           "from countries ".
           "where country_id  = " . dbquote($order["order_billing_address_country"]);

    $res = mysql_query($sql) or dberror($sql);

    
	if ($row = mysql_fetch_assoc($res))
    {
        $order["order_billing_address_country_name"] = $row["country_name"];
    }

	$sql = "select posorder_posaddress from posorders where posorder_order = " . $id;
	$res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $order["posaddress_id"] = $row["posorder_posaddress"];
    }
	else
	{
		$order["posaddress_id"] = "";
	}

    return $order;
}


/********************************************************************
    get order item data
*********************************************************************/
function get_order_item($id)
{
    $order_item = array();


    $sql = "select *, ".
           "    if(order_item_item <>'', item_code, item_type_name) as item_shortcut, ".
           "    if(order_item_item <>'', 0, 1) as item_price_changable ".
           "from order_items ".
           "left join items on order_item_item = item_id ".
           "left join item_types on order_item_type = item_type_id ".
           "where order_item_id = " . $id; 

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $order_item["item"] = $row["order_item_item"];
        $order_item["code"] = $row["item_shortcut"];
        $order_item["price_changable"] = $row["item_price_changable"];
        $order_item["text"] = $row["order_item_text"];
        $order_item["type"] = $row["order_item_type"];
        $order_item["quantity"] = $row["order_item_quantity"];
        $order_item["po_number"] = $row["order_item_po_number"];
        $order_item["client_price"] = $row["order_item_client_price"];
        $order_item["supplier"] = $row["order_item_supplier_address"];
        $order_item["supplier_price"] = $row["order_item_supplier_price"];
        $order_item["supplier_currency"] = $row["order_item_supplier_currency"];
        $order_item["supplier_exchange_rate"] = $row["order_item_supplier_exchange_rate"];
        $order_item["forwarder"] = $row["order_item_forwarder_address"];
        $order_item["system_price"] = $row["order_item_system_price"];
        $order_item["transportation"] = $row["order_item_transportation"];
        $order_item["no_offer_required"] = $row["order_item_no_offer_required"];
        $order_item["offer_number"] = $row["order_item_offer_number"];
        $order_item["production_time"] = $row["order_item_production_time"];
        $order_item["supplier_item_code"] = $row["order_item_supplier_item_code"];
        $order_item["shipment_code"] = $row["order_item_shipment_code"];

        // get currency information
        $order_item["supplier_system_price"] = Null;
        $order_item["supplier_system_price"] = NULL;
        
        if($order_item["supplier_currency"])
        {
            $currency = get_currency($order_item["supplier_currency"]);
            $order_item["supplier_system_price"] = number_format($order_item["supplier_price"] * $order_item["supplier_exchange_rate"] / $currency["factor"],2);

        }
        else
        {
            if($order_item["supplier"])
            {
                $currency = get_address_currency($order_item["supplier"]);
                $order_item["supplier_system_price"] = number_format($order_item["supplier_price"] * $order_item["supplier_exchange_rate"] / $currency["factor"],2);

            }
        }

    }

    return $order_item;
}

/********************************************************************
    get project design objective items ids data 
*********************************************************************/
function get_project_design_objective_item_ids($id)
{
    $project_design_objective_item_ids = array();
    
    $sql = "select project_item_item, design_objective_group_id, design_objective_group_name ".
           "from project_items ".
           "left join design_objective_items on project_item_item = design_objective_item_id ".
           "left join design_objective_groups on design_objective_item_group = design_objective_group_id ".
           "where project_item_project  = " . $id ." ".
           "order by design_objective_group_priority, design_objective_item_priority";

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
         $project_design_objective_item_ids[$row["project_item_item"]] = $row["design_objective_group_name"];
    }

    return  $project_design_objective_item_ids;
}

/********************************************************************
    get the address of an order specified by an address_type
*********************************************************************/
function get_order_address($address_type, $order_id)
{
    $order_address = array();

    $sql = "select * ".
           "from order_addresses ".
           "where order_address_order  = " . $order_id . " and order_address_type = " . $address_type;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $order_address["id"] = $row["order_address_id"];
        $order_address["company"] = $row["order_address_company"];
        $order_address["company2"] = $row["order_address_company2"];
        $order_address["address"] = $row["order_address_address"];
        $order_address["address2"] = $row["order_address_address2"];
        $order_address["zip"] = $row["order_address_zip"];
        $order_address["place"] = $row["order_address_place"];
		$order_address["place_id"] = $row["order_address_place_id"];
        $order_address["country"] = $row["order_address_country"];
        $order_address["country_name"] = "";
        $order_address["phone"] = $row["order_address_phone"];
        $order_address["fax"] = $row["order_address_fax"];
        $order_address["email"] = $row["order_address_email"];
        $order_address["contact"] = $row["order_address_contact"];


        $sql = "select country_id, country_name ".
               "from countries ".
               "where country_id = " . dbquote($order_address["country"]);

        $res = mysql_query($sql);
        if ($res)
        {
           $row = mysql_fetch_assoc($res);
           $order_address["country_name"] = $row['country_name'];
        }

    }   

    return $order_address;
}


/********************************************************************
    get the address of an order_item from table order_addresses
*********************************************************************/
function get_order_item_address($address_type, $order_id, $order_item_id)
{
    $order_address = array();

    $sql = "select * ".
           "from order_addresses ".
           "where order_address_order  = " . $order_id . 
           "    and order_address_order_item = " . $order_item_id .
           "    and order_address_type = " . $address_type;

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        $order_address["id"] = $row["order_address_id"];
        $order_address["company"] = $row["order_address_company"];
        $order_address["company2"] = $row["order_address_company2"];
        $order_address["address"] = $row["order_address_address"];
        $order_address["address2"] = $row["order_address_address2"];
        $order_address["zip"] = $row["order_address_zip"];
        $order_address["place"] = $row["order_address_place"];
        $order_address["country"] = $row["order_address_country"];
        $order_address["phone"] = $row["order_address_phone"];
        $order_address["fax"] = $row["order_address_fax"];
        $order_address["email"] = $row["order_address_email"];
        $order_address["contact"] = $row["order_address_contact"];
    }   

    return $order_address;
}


/*************************************************************************
    get the grand totals of a list of order_items of a certain item type
**************************************************************************/
function get_order_item_type_total($order_id, $item_type)
{
    $totals = array();
    $totals["in_system_currency"] = 0;
    $totals["in_order_currency"] = 0;

	if($item_type == ITEM_TYPE_STANDARD)
	{
		$filter = " (order_item_type=" . ITEM_TYPE_STANDARD . " or order_item_type = " . ITEM_TYPE_SERVICES . ")";
	}
	else
	{
		$filter = " order_item_type=" . $item_type;
	}


    $sql = "select order_item_order, order_item_type, order_item_not_in_budget, ".
           "    sum(order_item_system_price * if(order_item_quantity>0, order_item_quantity, 1)) as total_in_system_currency, ".
           "    sum(order_item_client_price * if(order_item_quantity>0, order_item_quantity, 1)) as total_in_order_currency ".
           "from order_items ".
           "where order_item_order=" . $order_id . 
           "   and order_item_not_in_budget = 0 or order_item_not_in_budget is null ".
           "group by order_item_order, order_item_type ".
           "having " . $filter;

	$res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $totals["in_system_currency"] = $totals["in_system_currency"] + $row["total_in_system_currency"];
        $totals["in_order_currency"] = $totals["in_order_currency"] + $row["total_in_order_currency"];
    }


    return $totals;
}


/****************************************************************************************
    get the grand totals of a list of order_items of a certain item type budget freezed
*****************************************************************************************/
function get_order_item_type_total_freezed($order_id, $item_type)
{
    $totals = array();
    $totals["in_system_currency"] = 0;
    $totals["in_order_currency"] = 0;


    $sql = "select order_item_order, order_item_type, order_item_not_in_budget, ".
           "    sum(order_item_system_price_freezed * if(order_item_quantity_freezed>0, order_item_quantity_freezed, 1)) as total_in_system_currency, ".
           "    sum(order_item_client_price_freezed * if(order_item_quantity_freezed>0, order_item_quantity_freezed, 1)) as total_in_order_currency ".
           "from order_items ".
           "where order_item_not_in_budget = 0 or order_item_not_in_budget is null ".
           "group by order_item_order, order_item_type ".
           "having order_item_order=" . $order_id .
           "    and order_item_type=" . $item_type;

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $totals["in_system_currency"] = $row["total_in_system_currency"];
        $totals["in_order_currency"] = $row["total_in_order_currency"];
    }


    return $totals;
}



/*************************************************************************
   build group totals for catalog items
**************************************************************************/
function get_group_total_of_standard_items($order_id, $field)
{
    $group_totals = array();
    
    $sql = "select category_priority, ".
           "sum(order_item_quantity * " . $field . ") AS group_total ".
           "from order_items LEFT JOIN categories ON order_item_category = category_id ".
           "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) " . 
		   "and order_item_order=" . $order_id . 
		   " and (order_item_type = " . ITEM_TYPE_STANDARD . " or order_item_type = " . ITEM_TYPE_SERVICES . ") ".
           "group by category_priority, category_name ".
           "order by category_priority";

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $group_totals[$row["category_priority"]] = $row["group_total"];
    }
    return $group_totals;
}


/*************************************************************************
   build group totals for catalog items, budget freezed
**************************************************************************/
function get_group_total_of_standard_items_freezed($order_id, $field)
{
    $group_totals = array();
    
    $sql = "select category_priority, ".
           "sum(order_item_quantity_freezed * " . $field . ") AS group_total ".
           "from order_items LEFT JOIN categories ON order_item_category = category_id ".
           "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) and order_item_order=" . $order_id . 
		   " AND (order_item_type= " . ITEM_TYPE_STANDARD . " or order_item_type= " . ITEM_TYPE_SERVICES . ") " .
           "group by category_priority, category_name ".
           "order by category_priority";

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $group_totals[$row["category_priority"]] = $row["group_total"];
    }
    return $group_totals;
}


/*************************************************************************
   build supplier's group totals
**************************************************************************/
function get_group_total_of_suppliers($order_id, $field)
{
    $group_totals = array();
    $sql = "select address_company, currency_symbol, ".
           "sum(order_item_quantity * " . $field . ") AS group_total ".
           "from order_items ".
           "left join addresses on order_item_supplier_address = address_id ".
           "left join currencies on currency_id = order_item_supplier_currency ".
           "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
           "      and order_item_order = " . $order_id . " ".
           "group by address_company, order_item_supplier_currency ".
           "order by address_company";

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $key = $row["address_company"] . ', ' . $row["currency_symbol"];
        $group_totals["currency"][$key] = $row["currency_symbol"];
        $group_totals["total"][$key] = number_format($row["group_total"], 2);
    }
    return $group_totals;
}



/*************************************************************************
   build line numbers for budget (position numbers)
**************************************************************************/
function get_budet_line_numbers($order_id)
{
    $line_numbers = array();

    // build budget position numbers for standard items
    $sql_1 = "select order_item_id, order_item_text, order_item_quantity, ".
             "    order_item_po_number, item_id, order_item_system_price, ".
             "    (order_item_quantity * order_item_system_price) as total_price, ".
             "    order_item_client_price, ".
             "    item_code, ".
             "    category_priority, category_name, ".
             "    address_shortcut, item_type_id, item_type_name, ".
             "    item_type_priority, order_item_type ".
             "from order_items ".
             "left join items on order_item_item = item_id ".
             "left join categories on order_item_category = category_id ".
             "left join addresses on order_item_supplier_address = address_id ".
             "left join item_types on order_item_type = item_type_id ";
    $sql_2 = "where (order_item_type = " . ITEM_TYPE_STANDARD . " or order_item_type = " . ITEM_TYPE_SERVICES . ") ";
    $sql_3 = "and order_item_order = " . $order_id . " ".
             "and (order_item_not_in_budget = 0 or order_item_not_in_budget is null)  " .
             "order by category_priority, item_code";

    $sql_order_items = $sql_1 . $sql_2 . $sql_3;

    $line_numbers_tmp = array();
    $i = 1;

    $res = mysql_query($sql_order_items) or dberror($sql_order_items);
    while ($row = mysql_fetch_assoc($res))
    {
        if ($i < 10)
        {
            $p = "00" . $i;
        }
        else if ($i < 100)
        {
            $p = "0" . $i;
        }
        else
        {
            $p = $i;
        }
        $line_numbers_tmp[$row["order_item_id"]] = $p;
        $i++;
    }

    $line_numbers[ITEM_TYPE_STANDARD] = $line_numbers_tmp;

    // build budget position numbers for special items
    $sql_2 = "where order_item_type = " . ITEM_TYPE_SPECIAL . " ";
    $sql_3 = "and order_item_order = " . $order_id . " ".
             "and (order_item_not_in_budget = 0 or order_item_not_in_budget is null)  " .
             "order by item_code";


    $sql_order_items = $sql_1 . $sql_2 . $sql_3;

    $line_numbers_tmp = array();

    $res = mysql_query($sql_order_items) or dberror($sql_order_items);
    while ($row = mysql_fetch_assoc($res))
    {
        if ($i < 10)
        {
            $p = "00" . $i;
        }
        else if ($i < 100)
        {
            $p = "0" . $i;
        }
        else
        {
            $p = $i;
        }
        $line_numbers_tmp[$row["order_item_id"]] = $p;
        $i++;
    }

    $line_numbers[ITEM_TYPE_SPECIAL] = $line_numbers_tmp;

    // build budget position numbers for local construction cost  positions
    $sql_2 = "where order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST . " ";
    $sql_3 = "and order_item_order = " . $order_id;
 
    $sql_order_items = $sql_1 . $sql_2 . $sql_3;

    $line_numbers_tmp = array();

    $res = mysql_query($sql_order_items) or dberror($sql_order_items);
    while ($row = mysql_fetch_assoc($res))
    {
        if ($i < 10)
        {
            $p = "00" . $i;
        }
        else if ($i < 100)
        {
            $p = "0" . $i;
        }
        else
        {
            $p = $i;
        }
        $line_numbers_tmp[$row["order_item_id"]] = $p;
        $i++;
    }

    
    $line_numbers[ITEM_TYPE_LOCALCONSTRUCTIONCOST] = $line_numbers_tmp;

    // build budget position numbers for exclusions
    $sql_2 = "where order_item_type = " . ITEM_TYPE_COST_ESTIMATION . " ";
    $sql_3 = "and order_item_order = " . $order_id;

    $sql_order_items = $sql_1 . $sql_2 . $sql_3;

    $line_numbers_tmp = array();

    $res = mysql_query($sql_order_items) or dberror($sql_order_items);
    while ($row = mysql_fetch_assoc($res))
    {
        if ($i < 10)
        {
            $p = "00" . $i;
        }
        else if ($i < 100)
        {
            $p = "0" . $i;
        }
        else
        {
            $p = $i;
        }
        $line_numbers_tmp[$row["order_item_id"]] = $p;
        $i++;
    }

    $line_numbers[ITEM_TYPE_COST_ESTIMATION] = $line_numbers_tmp;

    // build budget position numbers for exclusions
    $sql_2 = "where order_item_type = " . ITEM_TYPE_EXCLUSION . " ";
    $sql_3 = "and order_item_order = " . $order_id;

    $sql_order_items = $sql_1 . $sql_2 . $sql_3;

    $line_numbers_tmp = array();

    $res = mysql_query($sql_order_items) or dberror($sql_order_items);
    while ($row = mysql_fetch_assoc($res))
    {
        if ($i < 10)
        {
            $p = "00" . $i;
        }
        else if ($i < 100)
        {
            $p = "0" . $i;
        }
        else
        {
            $p = $i;
        }
        $line_numbers_tmp[$row["order_item_id"]] = $p;
        $i++;
    }

    $line_numbers[ITEM_TYPE_EXCLUSION] = $line_numbers_tmp;

    // build budget position numbers for notifications
    $sql_2 = "where order_item_type = " . ITEM_TYPE_NOTIFICATION . " ";
    $sql_3 = "and order_item_order = " . $order_id;

    $sql_order_items = $sql_1 . $sql_2 . $sql_3;

    $line_numbers_tmp = array();

    $res = mysql_query($sql_order_items) or dberror($sql_order_items);
    while ($row = mysql_fetch_assoc($res))
    {
        if ($i < 10)
        {
            $p = "00" . $i;
        }
        else if ($i < 100)
        {
            $p = "0" . $i;
        }
        else
        {
            $p = $i;
        }
        $line_numbers_tmp[$row["order_item_id"]] = $p;
        $i++;
    }

    $line_numbers[ITEM_TYPE_NOTIFICATION] = $line_numbers_tmp;

    return $line_numbers;
}


/*************************************************************************
   build array with dates from table dates
**************************************************************************/
function get_order_dates_from_dates($sql_order_items)
{

    $dates = array();
    $dates_expr = array();
    $dates_pick = array();
    $dates_exar = array();
    $dates_acar = array();
    $dates_ordr = array();


    $res = mysql_query($sql_order_items) or dberror($sql_order_items);
    while ($row = mysql_fetch_assoc($res))
    {
        // expected ready fro pickup date
        $number_tag = "";
        $number_of_records = 0;
        $dates_expr[$row["order_item_id"]] =  "";

        $sql = "select count(date_id) as number_of_records ".
               "from dates ".
               "left join date_types on date_type = date_type_id ".
               "where date_order_item = " . $row["order_item_id"] .
               "    and date_type_code = 'EXRP' ";

        $res1 = mysql_query($sql) or dberror($sql);

        $limit = "";        
        if ($row1 = mysql_fetch_assoc($res1))
        {
            $number_of_records = $row1["number_of_records"];
        }


        if ($number_of_records > 0)
        {
            $number_tag = "[" . $number_of_records . "]";
        }

        $limit = "";
        if ($number_of_records > 1)
        {
            $start = $number_of_records -1;
            $limit = "limit " . $start .", 1";
        }
        
        $sql = "select date_date ".
               "from dates ".
               "left join date_types on date_type = date_type_id ".
               "where date_order_item = " . $row["order_item_id"] .
               "    and date_type_code = 'EXRP' " .
               "    order by dates.date_modified ".
               $limit;


        $res1 = mysql_query($sql) or dberror($sql);
        if ($row1 = mysql_fetch_assoc($res1))
        {
            $dates_expr[$row["order_item_id"]] = $number_tag . to_system_date($row1["date_date"]);
        }
        else
        {
            $dates_expr[$row["order_item_id"]] = "n/a";
        }


        // pick up date
        $number_tag = "";
        $number_of_records = 0;
        $dates_pick[$row["order_item_id"]] =  "";

        $sql = "select count(date_id) as number_of_records ".
               "from dates ".
               "left join date_types on date_type = date_type_id ".
               "where date_order_item = " . $row["order_item_id"] .
               "    and date_type_code = 'PICK' ";

        $res1 = mysql_query($sql) or dberror($sql);

        if ($row1 = mysql_fetch_assoc($res1))
        {
            $number_of_records = $row1["number_of_records"];
        }

        if ($number_of_records > 0)
        {
            $number_tag = "[" . $number_of_records . "]";
        }

        
        $limit = "";
        if ($number_of_records > 1)
        {
            $start = $number_of_records -1;
            $limit = "limit " . $start .", 1";
        }

        $sql = "select date_date ".
               "from dates ".
               "left join date_types on date_type = date_type_id ".
               "where date_order_item = " . $row["order_item_id"] .
               "    and date_type_code = 'PICK' ".
               "    order by dates.date_modified ".
               $limit;
        
        $res1 = mysql_query($sql) or dberror($sql);
        if ($row1 = mysql_fetch_assoc($res1))
        {
            $dates_pick[$row["order_item_id"]] =  $number_tag . to_system_date($row1["date_date"]);
        }
        else
        {
            $dates_pick[$row["order_item_id"]] = "n/a";
        }

        // expected arrival date
        $number_tag = "";
        $number_of_records = 0;
        $dates_exar[$row["order_item_id"]] =  "";

        $sql = "select count(date_id) as number_of_records ".
               "from dates ".
               "left join date_types on date_type = date_type_id ".
               "where date_order_item = " . $row["order_item_id"] .
               "    and date_type_code = 'EXAR' ";

        $res1 = mysql_query($sql) or dberror($sql);

        if ($row1 = mysql_fetch_assoc($res1))
        {
            $number_of_records = $row1["number_of_records"];
        }

        if ($number_of_records > 0)
        {
            $number_tag = "[" . $number_of_records . "]";
        }

        
        $limit = "";
        if ($number_of_records > 1)
        {
            $start = $number_of_records -1;
            $limit = "limit " . $start .", 1";
        }

        $sql = "select date_date ".
               "from dates ".
               "left join date_types on date_type = date_type_id ".
               "where date_order_item = " . $row["order_item_id"] .
               "    and date_type_code = 'EXAR' ".
               "    order by dates.date_modified ".
               $limit;
        
        $res1 = mysql_query($sql) or dberror($sql);
        if ($row1 = mysql_fetch_assoc($res1))
        {
            $dates_exar[$row["order_item_id"]] =  $number_tag . to_system_date($row1["date_date"]);
        }
        else
        {
            $dates_exar[$row["order_item_id"]] = "n/a";
        }


        // actual arrival date
        $number_tag = "";
        $number_of_records = 0;
        $dates_acar[$row["order_item_id"]] =  "";

        $sql = "select count(date_id) as number_of_records ".
               "from dates ".
               "left join date_types on date_type = date_type_id ".
               "where date_order_item = " . $row["order_item_id"] .
               "    and date_type_code = 'ACAR' ";

        $res1 = mysql_query($sql) or dberror($sql);

        if ($row1 = mysql_fetch_assoc($res1))
        {
            $number_of_records = $row1["number_of_records"];
        }

        if ($number_of_records > 0)
        {
            $number_tag = "[" . $number_of_records . "]";
        }
        
        $limit = "";
        if ($number_of_records > 1)
        {
            $start = $number_of_records -1;
            $limit = "limit " . $start .", 1";
        }

        $sql = "select date_date ".
               "from dates ".
               "left join date_types on date_type = date_type_id ".
               "where date_order_item = " . $row["order_item_id"] .
               "    and date_type_code = 'ACAR' ".
               "    order by dates.date_modified ".
               $limit;

        $res1 = mysql_query($sql) or dberror($sql);
        if ($row1 = mysql_fetch_assoc($res1))
        {
            $dates_acar[$row["order_item_id"]] =  $number_tag . to_system_date($row1["date_date"]);
        }
        else
        {
            $dates_acar[$row["order_item_id"]] = "n/a";
        }

        // order date (order to supplier)
        $number_tag = "";
        $number_of_records = 0;
        $dates_ordr[$row["order_item_id"]] =  "";

        $sql = "select count(date_id) as number_of_records ".
               "from dates ".
               "left join date_types on date_type = date_type_id ".
               "where date_order_item = " . $row["order_item_id"] .
               "    and date_type_code = 'ORSU' ";

        $res1 = mysql_query($sql) or dberror($sql);

        if ($row1 = mysql_fetch_assoc($res1))
        {
            $number_of_records = $row1["number_of_records"];
        }

        if ($number_of_records > 0)
        {
            $number_tag = "[" . $number_of_records . "]";
        }
        
        $limit = "";
        if ($number_of_records > 1)
        {
            $start = $number_of_records -1;
            $limit = "limit " . $start .", 1";
        }

        $sql = "select date_date ".
               "from dates ".
               "left join date_types on date_type = date_type_id ".
               "where date_order_item = " . $row["order_item_id"] .
               "    and date_type_code = 'ORSU' ".
               "    order by dates.date_modified ".
               $limit;

        $res1 = mysql_query($sql) or dberror($sql);
        if ($row1 = mysql_fetch_assoc($res1))
        {
            $dates_ordr[$row["order_item_id"]] =  $number_tag . to_system_date($row1["date_date"]);
        }
        else
        {
            $dates_ordr[$row["order_item_id"]] = "n/a";
        }

    }

    $dates["EXRP"] = $dates_expr;
    $dates["PICK"] = $dates_pick;
    $dates["EXAR"] = $dates_exar;
    $dates["ACAR"] = $dates_acar;
    $dates["ORDR"] = $dates_ordr;

    return $dates;

}

/*************************************************************************
   get the last date entry of a series of a specific date type from dates
**************************************************************************/
function get_last_order_item_date($order_item_id, $type)
{
    $date_entry = array();    

    $number_of_records = 0;

    $sql = "select count(date_id) as number_of_records ".
           "from dates ".
           "left join date_types on date_type = date_type_id ".
           "where date_order_item = " . $order_item_id .
           "    and date_type_code = '" . $type ."' ";

    $res1 = mysql_query($sql) or dberror($sql);

    if ($row1 = mysql_fetch_assoc($res1))
    {
        $number_of_records = $row1["number_of_records"];
    }

    if ($number_of_records > 0)
    {
        $date_entry["changes"] = $number_of_records - 1;
    }
    else
    {
        $date_entry["changes"] = "0";
    }
        
    $limit = "";
    if ($number_of_records > 1)
    {
        $start = $number_of_records -1;
        $limit = "limit " . $start .", 1";
    }
    
    $sql_order_item_dates = "select date_date, date_type_name ".
                            "from dates ".
                            "left join date_types on date_type = date_type_id ".
                            "where date_order_item=" .$order_item_id . 
                            "    and date_type_code= '". $type . "' ".
                            "    order by dates.date_modified ".
                            $limit;


    $res = mysql_query($sql_order_item_dates) or dberror($sql_order_item_dates);
    if ($row = mysql_fetch_assoc($res))
    {
        $date_entry["last_date"] = to_system_date($row["date_date"]);
    }
    else
    {
        $date_entry["last_date"] = "";
    }

    return $date_entry;

}

/*************************************************************************
   get item stock data
**************************************************************************/
function get_item_stock_data($item_id)
{
 
    require_once "include/order_state_constants.php";

    $item_stock_data = array();

    $sql = "select item_code, item_name, supplier_address, address_company, ".
           "    store_global_order, ".
           "    store_last_global_order, store_last_global_order_date, ".
           "    store_last_global_order_confirmation_date, ".
           "    store_inventory, store_physical_stock, ".
           "    store_stock_control_starting_date, store_consumed_upto_021101, " .
           "    store_minimum_order_quantity, ".
           "    store_reproduction_time_in_weeks, store_consumption_coverage ".
           "from suppliers ".
           "left join addresses on supplier_address = address_id ".
           "left join items on supplier_item = item_id ".
           "left join stores on (item_id = store_item and supplier_address = store_address) ".
           "where item_id = ". $item_id;

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $item_stock_data["code"] = $row["item_code"];
        $item_stock_data["name"] = $row["item_name"];
        $item_stock_data["supplier_id"] = $row["supplier_address"];
        $item_stock_data["supplier_name"] = $row["address_company"];
        $item_stock_data["global_order"] = $row["store_global_order"];
        $item_stock_data["last_global_order"] = $row["store_last_global_order"];
        $item_stock_data["last_global_order_date"] = to_system_date($row["store_last_global_order_date"]);
        $item_stock_data["last_global_order_confirmation_date"] = to_system_date($row["store_last_global_order_confirmation_date"]);
        $item_stock_data["inventory"] = $row["store_inventory"];
        $item_stock_data["physical_stock"] = $row["store_physical_stock"];

        $item_stock_data["stock_control_starting_date"] =  $row["store_stock_control_starting_date"];
        $item_stock_data["store_consumed_upto_021101"] = $row["store_consumed_upto_021101"];
        $item_stock_data["minimum_order_quantity"] = $row["store_minimum_order_quantity"];
        $item_stock_data["reproduction_time_in_weeks"] = $row["store_reproduction_time_in_weeks"];
        $item_stock_data["consumption_coverage"] = $row["store_consumption_coverage"];

        
        // calculate quantities having a pick up date
        $sum_of_quantity_having_a_pick_up_date = 0;
        $sql = "Select sum(order_item_quantity) as tmp_sum ".
               "from order_items ".
               "left join orders on order_item_order = order_id ".
               "where order_item_item= " . $item_id .
               "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
               "    and order_item_pickup <> '0000-00-00' ".
               "    and order_item_pickup is not null ".
               "    and order_cancelled is null";
    
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $sum_of_quantity_having_a_pick_up_date = $row["tmp_sum"];

        }

        
        $item_stock_data["remaining_global_order"] = $item_stock_data["global_order"] - $sum_of_quantity_having_a_pick_up_date;
        $item_stock_data["remaining_inventory"] = $item_stock_data["inventory"]  - $sum_of_quantity_having_a_pick_up_date;
    
        // calculate quantities having an expected ready for pick up date
        $sum_of_quantity_having_a_ready_for_pick_up_date = 0;
        $sql = "Select sum(order_item_quantity) as tmp_sum ".
               "from order_items ".
               "left join orders on order_item_order = order_id ".
               "where order_item_item= " . $item_id .
               "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
               "    and order_item_ready_for_pickup <> '0000-00-00' ".
               "    and order_item_ready_for_pickup is not null ".
               "    and order_cancelled is null";

        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $sum_of_quantity_having_a_ready_for_pick_up_date = $row["tmp_sum"];

        }

        // calculate quantities having actual arrival date
        $sum_of_quantity_having_an_actual_arrival_date = 0;
        $sql = "Select sum(order_item_quantity) as tmp_sum ".
               "from order_items ".
               "left join orders on order_item_order = order_id ".
               "where order_item_item= " . $item_id .
               "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
               "    and order_item_arrival <> '0000-00-00' ".
               "    and order_item_arrival is not null ".
               "    and order_cancelled is null";
    
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $sum_of_quantity_having_an_actual_arrival_date = $row["tmp_sum"];

        }
    
    
        $item_stock_data["external_stock"] = $sum_of_quantity_having_a_ready_for_pick_up_date - $sum_of_quantity_having_an_actual_arrival_date;



        // calculate quantities having an order confirmation date (for orders)
        $sum_of_quantity_having_an_order_confirmation_date = 0;

        $sql = "Select sum(order_item_quantity) as tmp_sum ".
               "from order_items ".
               "left join orders on order_item_order = order_id ".
               "where order_item_item= " . $item_id .
               "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
               "    and order_cancelled is null " .
               "    and order_actual_order_state_code >= " . ORDER_CONFIRMED .
               "    and order_type = 2";

        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $sum_of_quantity_having_an_order_confirmation_date = $row["tmp_sum"];

        }

        // calculate quantities having an approved budget (for projects)
        $sum_of_quantity_having_an_approved_budget = 0;
        
        $sql = "Select sum(order_item_quantity) as tmp_sum ".
               "from order_items ".
               "left join orders on order_item_order = order_id ".
               "where order_item_item= " . $item_id .
               "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
               "    and order_cancelled is null " .
               "    and order_actual_order_state_code >= " . BUDGET_APPROVED .
               "    and order_type = 1";

        $res = mysql_query($sql) or dberror($sql);
        while ($row = mysql_fetch_assoc($res))
        {
            $sum_of_quantity_having_an_approved_budget = $row["tmp_sum"];
        }
        
        $item_stock_data["bookings"] = $sum_of_quantity_having_an_approved_budget + $sum_of_quantity_having_an_order_confirmation_date - $sum_of_quantity_having_a_ready_for_pick_up_date;

        $item_stock_data["available"] = $item_stock_data["remaining_inventory"] - $item_stock_data["bookings"];


        // calculate Average Consumption per Week
        if ($item_stock_data["stock_control_starting_date"])
        {
            $date=$item_stock_data["stock_control_starting_date"];
            $y = substr($date, 0, 4);
            $m = substr($date, 5, 2);
            $d = substr($date, 8, 2);
            $now = mktime(); 
            $then = mktime(0, 0, 0, $m, $d, $y);
            $num_of_seconds = ($now - $then);
            $num_of_days = $num_of_seconds / 86400; 
            $num_of_weeks = $num_of_days / 7;
        
          
            $item_stock_data["weeks_since_stock_control_started"] = number_format($num_of_weeks, 2);

            // calcualte delivered as from stock_control_starting_date
            $sum_of_quantity_delivered = 0;
            $sql = "Select sum(order_item_quantity) as tmp_sum ".
               "from order_items ".
               "left join orders on order_item_order = order_id ".
               "where order_item_item= " . $item_id .
               "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
               "    and order_item_arrival >= " . dbquote($item_stock_data["stock_control_starting_date"]).
               "    and order_cancelled is null";
            
            $res = mysql_query($sql) or dberror($sql);
            if ($row = mysql_fetch_assoc($res))
            {
                $sum_of_quantity_delivered = $row["tmp_sum"];

            }

            $item_stock_data["delivered_since_stock_control_started"] = $sum_of_quantity_delivered + $item_stock_data["store_consumed_upto_021101"];
            
            if ($num_of_weeks > 0)
            {
                $item_stock_data["average_per_week"] = number_format(($item_stock_data["store_consumed_upto_021101"] + $sum_of_quantity_delivered) / $num_of_weeks , 2);

                if ($item_stock_data["average_per_week"] > 0)
                {
                    $item_stock_data["medium_range"] = number_format($item_stock_data["available"] /  $item_stock_data["average_per_week"],2);
                }
                else
                {
                    $item_stock_data["medium_range"] = "n/a";  
                }
            }
            else
            {
                $item_stock_data["average_per_week"] = "n/a";
                $item_stock_data["weeks_since_stock_control_started"] = "n/a";
                $item_stock_data["delivered_since_stock_control_started"] = "n/a";
                $item_stock_data["medium_range"] = "n/a";
            }
        
        }
        else
        {
            $item_stock_data["average_per_week"] = "n/a";
            $item_stock_data["weeks_since_stock_control_started"] = "n/a";
            $item_stock_data["delivered_since_stock_control_started"] = "n/a";
            $item_stock_data["medium_range"] = "n/a";
        }
        $item_stock_data["stock_control_starting_date"] = to_system_date($item_stock_data["stock_control_starting_date"]);
    }

    return $item_stock_data;

}


/*************************************************************************
   get warehouse address from table order_addresses
**************************************************************************/
function get_order_warehouses($sql)
{
    $warehouses = array();

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $sql = "select order_address_company ".
               "from order_addresses ".
               "where order_address_order = " .  $row["order_item_order"] .
               "    and order_address_order_item = " . $row["order_item_id"] .
               "    and order_address_type = 4";

        $res1 = mysql_query($sql) or dberror($sql);
        if ($row1 = mysql_fetch_assoc($res1))
        {
            $warehouses[$row["order_item_id"]] = $row1["order_address_company"];
        }
        else
        {
            $warehouses[$row["order_item_id"]] = "";
        }
    }
    return $warehouses;
}


/*************************************************************************
   get addresses from table order_addresses belonging to all order_items
**************************************************************************/
function get_order_adresses($order_id, $type)
{
    $addresses = array();

    $sql = "select * from order_addresses ".
           "where order_address_order = " . $order_id .
           "   and (order_address_order_item <> 0 or order_address_order_item is not null) ".
           "   and order_address_type = " . $type;

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {

            $province_name = "";
			
			if($type == 2) //delivery address
			{
				$sql1 = "select province_canton ".
					   "from places " .
					   "left join provinces on province_id = place_province ".
					   "where place_id = " . dbquote($row['order_address_place_id']);

				$res1 = mysql_query($sql1);
				if ($res1)
				{
					$row1 = mysql_fetch_assoc($res1);
					$province_name = $row1['province_canton'];
				}
			}

			$sql1 = "select country_id, country_name ".
                   "from countries ".
                   "where country_id = " . dbquote($row['order_address_country']);

            $res1 = mysql_query($sql1);
            if ($res1)
            {
                $row1 = mysql_fetch_assoc($res1);
                $country_name = $row1['country_name'];
            }

			if($province_name)
			{
				$country_name = $province_name . ', ' . $country_name;
			}
            
            $tmp = $row["order_address_company"] . "\n" .
                   $row["order_address_address"] . "\n" .
                   $row["order_address_zip"] . " " . $row["order_address_place"]  . "\n" .
                   $country_name  . "\n" .
                   $row["order_address_contact"] . "\n" .
                   $row["order_address_phone"];

            $addresses[$row["order_address_order_item"]] = $tmp;
    }
    return $addresses;
}


/***************************************************************************
   get the last date entry of a series of order states for a specific order
****************************************************************************/
function get_order_states($order_id, $type)
{
    
    require_once "include/order_state_constants.php";

    $order_states = array();
    $order_states_dates = array();
    $order_states_done_dates = array();
    $order_states_users = array();
    $order_states_users_done = array();
    $order_states_performers = array();
    $order_states_recepients = array();


    // --------------------------------------
    // assign process steps to the different roles
    
    // client
    $steps_client = array();
    $steps_client[] = "100";
    if ($type == 1) // project
    {
        $steps_client[] = "340";
        $steps_client[] = "350";
    }
    $steps_client[] = "610";
    $steps_client[] = "620";
    $steps_client[] = "800";
    $steps_client[] = "810";

 
    if ($type == 1) // project
    {
        // retail coordinator
        $steps_rtco = array();
        $steps_rtco[] = "110";
        $steps_rtco[] = "120";
        $steps_rtco[] = "200";
        $steps_rtco[] = "210";
        $steps_rtco[] = "220";
        $steps_rtco[] = "270";
        $steps_rtco[] = "280";
        $steps_rtco[] = "300";
        $steps_rtco[] = "330";
        $steps_rtco[] = "400";
        $steps_rtco[] = "450";
        $steps_rtco[] = "460";
        $steps_rtco[] = "510";
        $steps_rtco[] = "550";
        $steps_rtco[] = "560";
        $steps_rtco[] = "600";

        // design contractor
        $steps_design_contractor = array();
        $steps_design_contractor[] = "230";
        $steps_design_contractor[] = "240";
        $steps_design_contractor[] = "250";
        $steps_design_contractor[] = "260";
        $steps_design_contractor[] = "410";
        $steps_design_contractor[] = "420";
        $steps_design_contractor[] = "430";
        $steps_design_contractor[] = "440";

        // design supervisor
        $steps_design_supervisor = array();
        $steps_design_supervisor[] = "310";
        $steps_design_supervisor[] = "320";
    }

    // retail operator
    $steps_rto = array();
    if ($type == 2) // order
    {
        $steps_rto[] = "110";
        $steps_rto[] = "200";
        $steps_rto[] = "210";
        $steps_rto[] = "510";
        $steps_rto[] = "550";
        $steps_rto[] = "560";
        $steps_rto[] = "600";
    }
    $steps_rto[] = "700";
    $steps_rto[] = "730";
    $steps_rto[] = "820";
    $steps_rto[] = "900";


    // suppliers to offer
    $steps_supp_offer = array();
    $steps_supp_offer[] = "520";
    $steps_supp_offer[] = "530";
    $steps_supp_offer[] = "540";

    // suppliers delivering
    $steps_supp_order = array();
    $steps_supp_order[] = "710";
    $steps_supp_order[] = "720";

    // forwarders        
    $steps_frwd = array();
    $steps_frwd[] = "740";
    $steps_frwd[] = "750";

    // --------------------------------------
    // get all relevant order- / project data 
    
    // $client
    $client = "";
    $sql = "select address_shortcut ".
           "from orders ".
           "left join addresses on address_id = order_client_address ".
           "where address_id is not null ".
           "   and order_id = " . $order_id;

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $client = $row["address_shortcut"];
    }

    if ($type == 1) 
    {
        // retail coordinator
        $retail_coordinator = "";
        $sql = "select address_shortcut ".
               "from projects ".
               "left join users on user_id = project_retail_coordinator ".
               "left join addresses on address_id = user_address ".
               "where address_id is not null ".
               "   and project_order = " . $order_id;
    
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $retail_coordinator = $row["address_shortcut"];
        }
    

        // design contractor
        $design_contractor = "";
        $sql = "select address_shortcut ".
               "from projects ".
               "left join users on user_id = project_design_contractor ".
               "left join addresses on address_id = user_address ".
               "where address_id is not null ".
               "   and project_order = " . $order_id;
    
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $design_contractor = $row["address_shortcut"];
        }


        // design supervisor
        $design_supervisor = "";
        $sql = "select address_shortcut ".
               "from projects ".
               "left join users on user_id = project_design_supervisor ".
               "left join addresses on address_id = user_address ".
               "where address_id is not null ".
               "   and project_order = " . $order_id;
    
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $design_supervisor = $row["address_shortcut"];
        }
    }


    // retail operator
    $retail_operator = "";
    $sql = "select address_shortcut ".
           "from orders ".
           "left join users on user_id = order_retail_operator ".
           "left join addresses on address_id = user_address ".
           "where address_id is not null ".
           "   and order_id = " . $order_id;
   
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $retail_operator = $row["address_shortcut"];
    }


    // supplieres to make an offer
    $suppliers_offering = "";
    $suppliers_offering_ids = array();

    $sql = "select distinct address_id, address_shortcut ".
           "from order_items ".
           "left join addresses on address_id = order_item_supplier_address ".
           "where order_item_order = " . $order_id .
           "   and (order_item_not_in_budget is null or order_item_not_in_budget = 0) ".
           "   and (order_item_no_offer_required is null or order_item_no_offer_required = 0) " .
           "   and address_id is not null ".
           "order by address_shortcut";

     $res = mysql_query($sql) or dberror($sql);
     while ($row = mysql_fetch_assoc($res))
     {
          $suppliers_offering = $suppliers_offering . $row["address_shortcut"] . "\n";
          $suppliers_offering_ids[$row["address_id"]] = $row["address_id"];
     }


    // supplieres to make an order to
    $suppliers = "";
    $suppliers_ids = array();

    $sql = "select distinct address_id, address_shortcut ".
           "from order_items ".
           "left join addresses on address_id = order_item_supplier_address ".
           "where order_item_order = " . $order_id .
           "   and (order_item_not_in_budget is null or order_item_not_in_budget = 0) ".
           "   and address_id is not null ".
           "order by address_shortcut";

     $res = mysql_query($sql) or dberror($sql);
     while ($row = mysql_fetch_assoc($res))
     {
          $suppliers = $suppliers . $row["address_shortcut"] . "\n";
          $suppliers_ids[$row["address_id"]] = $row["address_id"];
     }

    // forwarders
    $forwarders = "";
    $forwarders_ids = array();

    $sql = "select distinct address_id, address_shortcut ".
           "from order_items ".
           "left join addresses on address_id = order_item_forwarder_address ".
           "where order_item_order = " . $order_id .
           "   and (order_item_not_in_budget is null or order_item_not_in_budget = 0) ".
           "   and address_id is not null ".
           "order by address_shortcut";

     $res = mysql_query($sql) or dberror($sql);
     while ($row = mysql_fetch_assoc($res))
     {
          $forwarders = $forwarders . $row["address_shortcut"] . "\n";
          $forwarders_ids[$row["address_id"]] = $row["address_id"];
     }

    
    // --------------------------------------
    // assign recipients
    
    $recipients = array();
    $recipients["100"] = "Retail Net";

    if($type ==1)
    {
        $recipients["110"] = $retail_coordinator;
        $recipients["120"] = $retail_coordinator;
    }
    else
    {
        $recipients["110"] = $retail_operator;
    }
 
    $recipients["200"] = $client;
    $recipients["210"] = $client;

    if($type ==1)
    {
        $recipients["220"] = $design_contractor;    
        $recipients["230"] = $retail_coordinator;
        $recipients["240"] = $retail_coordinator;
        $recipients["250"] = $retail_coordinator;
        $recipients["260"] = $retail_coordinator;
        $recipients["270"] = $design_contractor;
        $recipients["280"] = $design_contractor;
        $recipients["300"] = $design_supervisor;    
        $recipients["310"] = $retail_coordinator;
        $recipients["320"] = $retail_coordinator;
        $recipients["330"] = $client;
        $recipients["340"] = $retail_coordinator;
        $recipients["350"] = $retail_coordinator;
        $recipients["400"] = $design_contractor;
        $recipients["410"] = $retail_coordinator;    
        $recipients["420"] = $retail_coordinator;
        $recipients["430"] = $retail_coordinator;
        $recipients["440"] = $retail_coordinator;
        $recipients["450"] = $design_contractor;
        $recipients["460"] = $design_contractor;
        $recipients["510"] = $suppliers_offering;
        $recipients["520"] = $retail_coordinator;    
        $recipients["530"] = $retail_coordinator;
        $recipients["540"] = $retail_coordinator;
        $recipients["550"] = $suppliers_offering;
        $recipients["560"] = $suppliers_offering;
        $recipients["600"] = $client;
        $recipients["610"] = $retail_coordinator;
        $recipients["620"] = $retail_coordinator;    
    }

    if ($type == 2)
    {
        $recipients["510"] = $suppliers_offering;
        $recipients["520"] = $retail_operator;    
        $recipients["530"] = $retail_operator;
        $recipients["540"] = $retail_operator;
        $recipients["550"] = $suppliers_offering;
        $recipients["560"] = $suppliers_offering;
        $recipients["600"] = $client;
        $recipients["610"] = $retail_operator;
        $recipients["620"] = $retail_operator;
    }

    $recipients["700"] = $suppliers;
    $recipients["710"] = $retail_operator;
    $recipients["720"] = $retail_operator;
    $recipients["730"] = $forwarders;
    $recipients["740"] = $retail_operator;
    $recipients["750"] = $retail_operator;
    $recipients["800"] = $retail_operator;    
    $recipients["810"] = $retail_operator;
    $recipients["820"] = "Retail Net";
    $recipients["900"] = "Retail Net";    
    $recipients["910"] = "";


    // build step relations 1:n
    $steps_one_to_n = array();
    $steps_one_to_n[] = "510";
    $steps_one_to_n[] = "550";
    $steps_one_to_n[] = "560";
    $steps_one_to_n[] = "700";
    $steps_one_to_n[] = "730";

    // build step relations n:1
    $steps_n_to_one = array();
    $steps_n_to_one[] = "520";
    $steps_n_to_one[] = "530";
    $steps_n_to_one[] = "540";
    $steps_n_to_one[] = "700";
    $steps_n_to_one[] = "710";
    $steps_n_to_one[] = "720";
    $steps_n_to_one[] = "740";
    $steps_n_to_one[] = "750";




    // --------------------------------------
    // build history data 

    $where_clause_addon = "";
    if ($type == 1)
    {
        if (!has_access("can_view_order_step_user_names_in_projects"))
        {
            $where_clause_addon = "    and actual_order_state_user = " . user_id();
        }
    }
    else
    {
        if (!has_access("can_view_order_step_user_names_in_orders"))
        {
            $where_clause_addon = "    and actual_order_state_user = " . user_id();
        }
    }

    
    $sql = "select  order_state_id, order_state_code ".
           "from order_states ".
           "left join order_state_groups on order_state_group = order_state_group_id ".
           "where order_state_group_order_type = " . $type . " ".
           "order by order_state_code";
    

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
       $number_of_records = 0;

       $sql = "select count(actual_order_state_id) as number_of_records ".
              "from actual_order_states ".
              "where actual_order_state_order = " . $order_id .
              "    and actual_order_state_state = ". $row["order_state_id"] .
              $where_clause_addon;

        $res1 = mysql_query($sql) or dberror($sql);

        if ($row1 = mysql_fetch_assoc($res1))
        {
            $number_of_records = $row1["number_of_records"];
        }

        if ($number_of_records > 0)
        {
            $entries = "[" . $number_of_records . "]";
        }
        else
        {
            $entries = "";
        }

        $limit = "";
        if ($number_of_records > 1)
        {
            $start = $number_of_records -1;
            $limit = "limit " . $start .", 1";
        }

        $sql = "select actual_order_states.date_created as state_date, ".
               "    concat(user_name, ' ', left(user_firstname,1), '.') as user_fullname ".
               "from actual_order_states ".
               "left join users on actual_order_state_user = user_id ".
               "where actual_order_state_order = " . $order_id . 
               "    and actual_order_state_state = ". $row["order_state_id"] . 
               $where_clause_addon . " " . $limit;

        $res2 = mysql_query($sql) or dberror($sql);
        if ($row2 = mysql_fetch_assoc($res2))
        {
            $order_states_dates[$row["order_state_code"]] = $entries . to_system_date($row2["state_date"]);
            $order_states_users[$row["order_state_code"]] = $row2["user_fullname"];
        }


        // get roles to perform an action
        if(in_array($row["order_state_code"], $steps_client))
        {
            $order_states_performers[$row["order_state_code"]] = $client;
        }
        else if(in_array($row["order_state_code"], $steps_rto))
        {
            $order_states_performers[$row["order_state_code"]] = $retail_operator;
        }
        else if(in_array($row["order_state_code"], $steps_supp_offer))
        {
            $order_states_performers[$row["order_state_code"]] = $suppliers_offering;
        }
        else if(in_array($row["order_state_code"], $steps_supp_order))
        {
           $order_states_performers[$row["order_state_code"]] = $suppliers;
        }
        else if(in_array($row["order_state_code"], $steps_frwd))
        {
            $order_states_performers[$row["order_state_code"]] = $forwarders;
        }

        if ($type == 1) // project
        {
            if(in_array($row["order_state_code"], $steps_rtco))
            {
                $order_states_performers[$row["order_state_code"]] = $retail_coordinator;
            }
            else if(in_array($row["order_state_code"], $steps_design_contractor))
            {
                $order_states_performers[$row["order_state_code"]] = $design_contractor;
            }
            else if(in_array($row["order_state_code"], $steps_design_supervisor))
            {
                $order_states_performers[$row["order_state_code"]] = $design_supervisor;
            }
        }

        $order_states_recepients[$row["order_state_code"]] = $recipients[$row["order_state_code"]];



        //check if a user of the performing address has made an email to the recepient address
        // 1:n Relations are: brand to suppliers, swatch to forwarders
        // n:1 Relations are: suppliers to brand, forwarders to swatch

        if(in_array($row["order_state_code"], $steps_one_to_n))
        {
                // process 1:n relations
                if($row["order_state_code"] == REQUEST_FOR_OFFER_SUBMITTED or $row["order_state_code"] == OFFER_REJECTED or $row["order_state_code"] == OFFER_ACCEPTED)
                {
                    $address_ids = array_values(array_unique($suppliers_offering_ids));  
                }
                elseif($row["order_state_code"] == ORDER_TO_SUPPLIER_SUBMITTED)
                {
                    $address_ids = array_values(array_unique($suppliers_ids));  
                }
                elseif($row["order_state_code"] == REQUEST_FOR_DELIVERY_SUBMITTED)
                {
                    $address_ids = array_values(array_unique($forwarders_ids));  
                }

                $tmp1 = "";
                $tmp2 = "";

                foreach($address_ids as $key => $value)
                {
                    //echo $row["order_state_code"] . " " . $value . "<br>";
                    // check if reciepient already has got a mail
                    $sql = "select order_mail_id, order_mails.date_created, ".
                           "    concat(users_2.user_name, ' ', left(users_2.user_firstname,1), '.') as from_user_fullname ".
                           "from order_mails ".
                           "left join users as users_1 on users_1.user_id = order_mail_user ".
                           "left join users as users_2 on users_2.user_id = order_mail_from_user ".
                           "where order_mail_order = ". $order_id .
                           "   and order_mail_order_state = " . $row["order_state_id"] .
                           "   and users_1.user_address = " . $value . " " .
                           "order by order_mails.date_created desc";

                    $res4 = mysql_query($sql) or dberror($sql);

                    if ($row4 = mysql_fetch_assoc($res4))
                    {
                        $tmp1 = $tmp1 . $row4["from_user_fullname"] . "\n";
                        $tmp2 = $tmp2 . to_system_date($row4["date_created"]) . "\n";
                    }
                    else
                    {
                        $tmp1 = $tmp1 .  " \n";
                        $tmp2 = $tmp2 .  " \n";
                    }
                }
                $order_states_users_done[$row["order_state_code"]] = $tmp1;
                $order_states_done_dates[$row["order_state_code"]] = $tmp2;
        }
        elseif(in_array($row["order_state_code"], $steps_n_to_one))
        {
                // process n:1 relations
                if($row["order_state_code"] == REQUEST_FOR_OFFER_REJECTED or $row["order_state_code"] == REQUEST_FOR_OFFER_ACCEPTED or $row["order_state_code"] == OFFER_SUBMITTED)
                {
                    $address_ids = array_values(array_unique($suppliers_offering_ids));
                }
                elseif($row["order_state_code"] == REJECT_ORDER_BY_SUPPLIER or $row["order_state_code"] == CONFIRM_ORDER_BY_SUPPLIER)
                {
                    $address_ids = array_values(array_unique($suppliers_ids));  
                }
                elseif($row["order_state_code"] == REQUEST_FOR_DELIVERY_ACCEPTED or $row["order_state_code"] == DELIVERY_CONFIRMED_FRW)
                {
                    $address_ids = array_values(array_unique($forwarders_ids));  
                }
                

                $tmp1 = "";
                $tmp2 = "";

                foreach($address_ids as $key => $value)
                {
                    // check if sender has already has  a mail
                    $sql = "select order_mail_id, order_mails.date_created, ".
                           "    concat(users_2.user_name, ' ', left(users_2.user_firstname,1), '.') as from_user_fullname ".
                           "from order_mails ".
                           "left join users as users_1 on users_1.user_id = order_mail_user ".
                           "left join users as users_2 on users_2.user_id = order_mail_from_user ".
                           "where order_mail_order = ". $order_id .
                           "   and order_mail_order_state = " . $row["order_state_id"] .
                           "   and users_2.user_address = " . $value . " " .
                           "order by order_mails.date_created desc";

                    $res4 = mysql_query($sql) or dberror($sql);

                    if ($row4 = mysql_fetch_assoc($res4))
                    {
                        $tmp1 = $tmp1 . $row4["from_user_fullname"] . "\n";
                        $tmp2 = $tmp2 . to_system_date($row4["date_created"]) . "\n";
                    }
                    else
                    {
                        $tmp1 = $tmp1 .  " \n";
                        $tmp2 = $tmp2 .  " \n";
                    }
                }
                $order_states_users_done[$row["order_state_code"]] = $tmp1;
                $order_states_done_dates[$row["order_state_code"]] = $tmp2;
        }
        else
        {
            // process 1:1 relations
            $sql = "select actual_order_states.date_created, ".
                   "   concat(user_name, ' ', left(user_firstname,1), '.') as from_user_fullname ".
                   "from actual_order_states ".
                   "left join users on user_id = actual_order_state_user ".
                   "left join addresses on address_id = user_address ".
                   "where actual_order_state_order = ". $order_id .
                   "   and actual_order_state_state = " . $row["order_state_id"] . " " .
                   "order by actual_order_states.date_created desc;";

             $res4 = mysql_query($sql) or dberror($sql);

             if ($row4 = mysql_fetch_assoc($res4))
             {
                $order_states_users_done[$row["order_state_code"]] = $row4["from_user_fullname"];
                $order_states_done_dates[$row["order_state_code"]] = to_system_date($row4["date_created"]);
             }
        }
    }

    $order_states["dates"] = $order_states_dates;
    $order_states["done_dates"] = $order_states_done_dates;
    $order_states["users"] = $order_states_users;
    $order_states["users_done"] = $order_states_users_done;
    $order_states["performers"] = $order_states_performers;
    $order_states["recepients"] = $order_states_recepients;
    return $order_states;

}

/*************************************************************************
   get actual order state of a specific order
**************************************************************************/
function get_actual_order_state($order_id)
{
    $actual_order_state = array();
    $images = array();

    //if (has_access("can_view_order_step_user_names_in_orders") or has_access("can_view_order_step_user_names_in_projects"))
    //{
        $sql = "select order_state_id, order_state_code, order_state_name ".
               "from actual_order_states ".
               "left join order_states on actual_order_state_state = order_state_id ".
               "where actual_order_state_order = " . $order_id . " " .
               "order by actual_order_states.date_modified desc";

        $res = mysql_query($sql) or dberror($sql);

        if ($row = mysql_fetch_assoc($res))
        {
            $actual_order_state["code"] = $row["order_state_code"];
            $actual_order_state["name"] = $row["order_state_name"];
            $images[$row["order_state_code"]] = "/pictures/actualorderstate.gif";
            $actual_order_state["images"] = $images;
        }
        else
        {
            $actual_order_state["code"] = "";
            $actual_order_state["name"] = "";
            $actual_order_state["images"] = "/pictures/actualorderstate_empty.gif";
        }
    //}
    //else
    //{
    //   $sql = "select order_state_id, order_state_code, order_state_name ".
    //           "from actual_order_states ".
    //           "left join order_states on actual_order_state_state = order_state_id ".
    //           "where actual_order_state_order = " . $order_id . " " .
    //           "order by actual_order_states.date_modified desc";

   //     $res = mysql_query($sql) or dberror($sql);

   //     if ($row = mysql_fetch_assoc($res))
   //     {
   //         $actual_order_state["code"] = $row["order_state_code"];
   //         $actual_order_state["name"] = $row["order_state_name"];
   //     }
   //     else
   //     {
   //         $actual_order_state["code"] = "";
   //         $actual_order_state["name"] = "";
   //     }
        
   //     $sql = "select order_state_id, order_state_code, order_state_name ".
   //            "from actual_order_states ".
   //            "left join order_states on actual_order_state_state = order_state_id ".
   //            "where actual_order_state_order = " . $order_id . 
   //            "    and actual_order_state_user = " . user_id() . " " .
   //            "order by actual_order_states.date_modified desc";

   //     $res = mysql_query($sql) or dberror($sql);

   //     if ($row = mysql_fetch_assoc($res))
   //     {
   //         $images[$row["order_state_code"]] = "/pictures/actualorderstate.gif";
   //         $actual_order_state["images"] = $images;
   //     }
   //     else
   //     {
   //         $images[$actual_order_state["code"]] = "/pictures/actualorderstate_empty.gif";
   //         $actual_order_state["images"] =  $images;
   //     }
   // }
    return $actual_order_state;

}

/*************************************************************************
   get project state name
**************************************************************************/
function get_project_state_name($id)
{
    $name = "";

    $sql = "select project_state_text ".
           "from project_states ".
           "where project_state_id = " . $id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $name = $row["project_state_text"];
    }

    return $name;
}

/*************************************************************************
   get actual order state name
**************************************************************************/
function get_actual_order_state_name($code, $type)
{
    $name = "";

    $sql = "select order_state_name ".
           "from order_states ".
           "left join order_state_groups on order_state_group_id = order_state_group ".
           "where order_state_group_order_type = " . $type .
           "   and order_state_code = '" . $code . "'";

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $name = $row["order_state_name"];
    }

    return $name;
}

/*************************************************************************
   check a specific order state (if present in actual_order_states)
**************************************************************************/
function is_present_order_state($order_id, $step)
{
    
    $sql = "select order_state_code, order_state_name ".
           "from actual_order_states ".
           "left join order_states on actual_order_state_state = order_state_id ".
           "where actual_order_state_order = " . $order_id . 
           "    and order_state_code = " . dbquote($step);

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        return true;
    }
    else
    {
        return false;
    }
}



/*************************************************************************
   check a specific order state (if present in actual_order_states)
**************************************************************************/
function get_last_order_state_performed($order_id)
{
    
    $sql = "select actual_order_state_state ".
           "from actual_order_states ".
           "where actual_order_state_order = " . $order_id .
           " order by actual_order_state_id DESC";

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        return $row["actual_order_state_state"];
    }
    else
    {
        return 0;
    }
}


/*************************************************************************
   get code of last order_state performed
**************************************************************************/
function get_code_of_last_order_state_performed($order_id)
{
    
    $sql = "select order_state_code ".
           "from actual_order_states ".
           "left join order_states on order_state_id = actual_order_state_state " . 
           "where actual_order_state_order = " . $order_id .
           " order by actual_order_state_id DESC";

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        return $row["order_state_code"];
    }
    else
    {
        return 0;
    }
}



/*************************************************************************
   get code of last order_state performed by a user
**************************************************************************/
function get_code_of_last_order_state_performed_by_user($order_id, $user_id)
{
    
    $sql = "select order_state_code ".
           "from actual_order_states ".
           "left join order_states on order_state_id = actual_order_state_state " . 
           "where actual_order_state_order = " . $order_id .
           "   and actual_order_state_user = " . $user_id .
           " order by actual_order_state_id DESC";

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        return $row["order_state_code"];
    }
    else
    {
        return 0;
    }
}

/**************************************************************************
   check if a specific predecessor action has been performed (order states)
***************************************************************************/
function predecessor_is_performed($order_id, $step, $type, $no_planning)
{
    if ($no_planning == 1)
    {
        $predecessor_field = "order_state_predecessor_no_planning";
    }
    else
    {
        $predecessor_field = "order_state_predecessor";
    }

        $sql = "select order_state_code, order_state_predecessor, order_state_predecessor_no_planning ".
               "from order_state_groups ".
               "left join order_states on order_state_group_id = order_state_group ".
               "where order_state_group_order_type = " . $type .
               "    and order_state_code = " . dbquote($step);

        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            if($row[$predecessor_field] == 0 or $row[$predecessor_field] == null)
            {
                return true;
            }
            else
            {
                $sql = "select actual_order_state_id ".
                       "from actual_order_states ".
                       "left join order_states on actual_order_state_state = order_state_id ".
                       "where actual_order_state_order = " . $order_id . " " .
                       "    and order_state_id = " . $row[$predecessor_field];

                $res = mysql_query($sql) or dberror($sql);

                if ($row = mysql_fetch_assoc($res))
                {
                    return true;
                }
                else
                {   
                    return false;
                }
            }
        }
        else
        {
            return false;
        }
}

/**************************************************************************
   check if a specific predecessor action has been performed (order states)
***************************************************************************/
function get_order_state_predecessor($step, $type, $no_planning)
{
    if ($no_planning == 1)
    {
        $predecessor_field = "order_state_predecessor_no_planning";
    }
    else
    {
        $predecessor_field = "order_state_predecessor";
    }

    $sql = "select order_state_code, order_state_predecessor, order_state_predecessor_no_planning ".
           "from order_state_groups ".
           "left join order_states on order_state_group_id = order_state_group ".
           "where order_state_group_order_type = " . $type .
           "    and order_state_code = " . dbquote($step);

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        return $row[$predecessor_field];
    }
}

/*************************************************************************
   get the parameters for the action selected in project_flow_control.php
**************************************************************************/
function get_action_parameter($code, $type)
{
    $action_parameters = array();

    $sql = "select  order_state_id, order_state_code, ".
           "    order_state_name, order_state_action_name, ".
           "    order_state_append_task, order_state_send_email, order_state_change_state, ".
           "    notification_recipient_name " .
           "from order_states ".
           "left join order_state_groups on order_state_group = order_state_group_id ".
           "left join notification_recipients on order_state_notification_recipient = notification_recipient_id ".
           "where order_state_group_order_type = " . $type . " ".
           "    and order_state_code = " . $code;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $action_parameters["name"] = $row["order_state_name"];
        $action_parameters["action_name"] = $row["order_state_action_name"];
        $action_parameters["recipient"] = $row["notification_recipient_name"];
        $action_parameters["append_task"] = $row["order_state_append_task"];
        $action_parameters["send_email"] = $row["order_state_send_email"];
        $action_parameters["change_state"] = $row["order_state_change_state"];

    }

    return $action_parameters;

}




/*************************************************************************
   get task data
**************************************************************************/
function get_task_data($task_id)
{
    $task_data = array();

    $sql =  "select * ".
            "from tasks ".
            "left join orders on order_id = task_order ".
            "left join projects on project_order = task_order ".
            "left join users on task_from_user = user_id ".
            "where task_id=" . $task_id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {

        $task_data["user"] = $row["task_user"];
        $task_data["text"] = $row["task_text"];
        $task_data["url"] = $row["task_url"];
        $task_data["due_date"] = to_system_date($row["task_due_date"]);
        $task_data["done_date"] = to_system_date($row["task_done_date"]);
        
        $task_data["assigned_by"] = $row["user_name"] . " " . $row["user_firstname"];

        $task_data["number"] = $row["order_number"];
        if ($row["project_id"])
        {
            $task_data["type_name"] = "Project";
        }
        else
        {
            $task_data["type_name"] = "Order";
        }
    
    }

    return $task_data;
}


/*************************************************************************
   get message data
**************************************************************************/
function get_message($id)
{
    $message_data = array();

    $sql =  "select message_title, message_text ".
            "from messages ".
            "where message_id=" . $id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $message_data["title"] = $row["message_title"];
        $message_data["text"] = $row["message_text"];
    }

    return $message_data;
}


/*************************************************************************
   get all the companies, involved in an order for attachments
**************************************************************************/
function get_involved_companies($order_id, $id)
{
    
    $companies = array();

    // get the retail coordinator
    $sql = "select distinct address_id, address_company, user_id, user_name, user_firstname ".
           "from orders ".
           "left join projects on project_order = order_id ".
           "left join users on user_id = project_retail_coordinator ".
           "left join addresses on address_id = user_address ".
           "where order_id = ". $order_id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        
		if ($row["address_id"])
        {
            $company = array();
            $company["id"] = $row["address_id"];
            $company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
			$company["user"] = $row["user_id"];
			$company["role"] = "Project Manager";
            
            $sql1 = "select order_file_address_id from order_file_addresses ". 
                    "where order_file_address_file = " . $id .
                    "    and order_file_address_address = " .dbquote($row["address_id"]);

            $res1 = mysql_query($sql1) or dberror($sql1);

            if ($row1 = mysql_fetch_assoc($res1))
            {
                $company["access"] = 1;            
            }
            else
            {
                $company["access"] = 0;
            }

            $companies[] = $company;
        }
    }


    // get the retail operator
    $sql = "select distinct address_id, address_company, user_id, user_name, user_firstname ".
           "from orders ".
           "left join projects on project_order = order_id ".
           "left join users on user_id = order_retail_operator ".
           "left join addresses on address_id = user_address ".
           "where order_id = ". $order_id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        if ($row["address_id"])
        {
            $company = array();
            $company["id"] = $row["address_id"];
            $company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
			$company["user"] = $row["user_id"];
			$company["role"] = "Retail Operator";

            $sql1 = "select order_file_address_id from order_file_addresses ". 
                    "where order_file_address_file = " . $id .
                    "    and order_file_address_address = " .dbquote($row["address_id"]);

            $res1 = mysql_query($sql1) or dberror($sql1);

            if ($row1 = mysql_fetch_assoc($res1))
            {
                $company["access"] = 1;            
            }
            else
            {
                $company["access"] = 0;
            }

            $companies[] = $company;
        }
    }
    
    // get the client
    $sql = "select distinct address_id, address_company, order_user, user_name, user_firstname ".
           "from orders ".
		   "left join users on user_id = order_user ".
           "left join addresses on address_id = order_client_address ".
           "where order_id = ". $order_id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $company = array();
        $company["id"] = $row["address_id"];
        $company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
		$company["user"] = $row["order_user"];
		$company["role"] = "Client";

            $sql1 = "select order_file_address_id from order_file_addresses ". 
                    "where order_file_address_file = " . $id .
                    "    and order_file_address_address = " .dbquote($row["address_id"]);

            $res1 = mysql_query($sql1) or dberror($sql1);

            if ($row1 = mysql_fetch_assoc($res1))
            {
                $company["access"] = 1;            
            }
            else
            {
                $company["access"] = 0;
            }

        $companies[] = $company;
    }


	// get the local project manager
    $sql = "select distinct address_id, address_company, project_local_retail_coordinator, user_name, user_firstname ".
           "from projects ".
		   "left join users on user_id = project_local_retail_coordinator ".
           "left join addresses on address_id = user_address ".
           "where project_order = ". $order_id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        if($row["address_id"] > 0)
		{
			$company = array();
			$company["id"] = $row["address_id"];
			$company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
			$company["user"] = $row["project_local_retail_coordinator"];
			$company["role"] = "Local Project Manager";

				$sql1 = "select order_file_address_id from order_file_addresses ". 
						"where order_file_address_file = " . $id .
						"    and order_file_address_address = " .dbquote($row["address_id"]);

				$res1 = mysql_query($sql1) or dberror($sql1);

				if ($row1 = mysql_fetch_assoc($res1))
				{
					$company["access"] = 1;            
				}
				else
				{
					$company["access"] = 0;
				}

			$companies[] = $company;
		}
    }


    // get the design contractor
    $sql = "select distinct address_id, address_company, user_id, user_name, user_firstname ".
           "from orders ".
           "left join projects on project_order = order_id ".
           "left join users on user_id = project_design_contractor ".
           "left join addresses on address_id = user_address ".
           "where order_id = ". $order_id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        if ($row["address_id"])
        {
            $company = array();
            $company["id"] = $row["address_id"];
            $company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
			$company["user"] = $row["user_id"];
			$company["role"] = "Design Contractor";
            
            $sql1 = "select order_file_address_id from order_file_addresses ". 
                    "where order_file_address_file = " . $id .
                    "    and order_file_address_address = " .dbquote($row["address_id"]);

            $res1 = mysql_query($sql1) or dberror($sql1);

            if ($row1 = mysql_fetch_assoc($res1))
            {
                $company["access"] = 1;            
            }
            else
            {
                $company["access"] = 0;
            }

            $companies[] = $company;
        }
    }
   
    
    // get the design supervisor
    $sql = "select distinct address_id, address_company, user_id, user_name, user_firstname ".
           "from orders ".
           "left join projects on project_order = order_id ".
           "left join users on user_id = project_design_supervisor ".
           "left join addresses on address_id = user_address ".
           "where order_id = ". $order_id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        if ($row["address_id"])
        {
            $company = array();
            $company["id"] = $row["address_id"];
            $company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
			$company["user"] = $row["user_id"];
			$company["role"] = "Design Supervisor";
            
            $sql1 = "select order_file_address_id from order_file_addresses ". 
                    "where order_file_address_file = " . $id .
                    "    and order_file_address_address = " .dbquote($row["address_id"]);

            $res1 = mysql_query($sql1) or dberror($sql1);

            if ($row1 = mysql_fetch_assoc($res1))
            {
                $company["access"] = 1;            
            }
            else
            {
                $company["access"] = 0;
            }

            $companies[] = $company;
        }
    }
    
    

    // get the suppliers
    $sql = "select distinct order_item_supplier_address, address_company,user_id, user_name, user_firstname ".
           "from orders ".
           "left join order_items on order_item_order = order_id ".
           "left join addresses on address_id = order_item_supplier_address " .
		   "left join users on user_address = order_item_supplier_address ".
           "where address_id != 2390 and user_active = 1 and order_id = ". $order_id;

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        if ($row["order_item_supplier_address"])
        {
            $company = array();
            $company["id"] = $row["order_item_supplier_address"];
            $company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
			$company["user"] = $row["user_id"];
			$company["role"] = "Supplier";

            $sql1 = "select order_file_address_id from order_file_addresses ". 
                    "where order_file_address_file = " . $id .
                    "    and order_file_address_address = " . $row["order_item_supplier_address"];

            $res1 = mysql_query($sql1) or dberror($sql1);

            if ($row1 = mysql_fetch_assoc($res1))
            {
                $company["access"] = 1;            
            }
            else
            {
                $company["access"] = 0;
            }

            $companies[] = $company;
        }
    }

	// get the forwarders
    $sql = "select distinct order_item_forwarder_address, address_company,user_id, user_name, user_firstname ".
           "from orders ".
           "left join order_items on order_item_order = order_id ".
           "left join addresses on address_id = order_item_forwarder_address ".
		   "left join users on user_address = order_item_forwarder_address ".
           "where address_id != 2390 and user_active = 1 and order_id = ". $order_id;

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        if ($row["order_item_forwarder_address"])
        {
            $company = array();
            $company["id"] = $row["order_item_forwarder_address"];
            $company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
			$company["user"] = $row["user_id"];
			$company["role"] = "Forwarder";

            $sql1 = "select order_file_address_id from order_file_addresses  ". 
                    "where order_file_address_file = " . $id .
                    "    and order_file_address_address = " . $row["order_item_forwarder_address"];

            $res1 = mysql_query($sql1) or dberror($sql1);

            if ($row1 = mysql_fetch_assoc($res1))
            {
                $company["access"] = 1;            
            }
            else
            {
                $company["access"] = 0;
            }

            $companies[] = $company;
        }
    }

    
	// get speical users as additional reciepeints
	$users = array();
	$sql = "select order_type, address_country ".
		   "from orders ".
		   "left join addresses on address_id = order_client_address " . 
		   "where order_id = ". $order_id;

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		
		
		//get users
		if($row["order_type"] == 1) // project
		{
			$sql = "select user_id, user_substitute " . 
				   "from users " . 
				   "left join addresses on address_id = user_address " .
				   "where address_country = " . $row["address_country"] . 
				   " and user_active = 1 and user_project_reciepient = 1";
		}
		else
		{
			$sql = "select user_id, user_substitute " . 
				   "from users " . 
				   "left join addresses on address_id = user_address " .
				   "where address_country = " . $row["address_country"] . 
				   " and user_active = 1 and user_order_reciepient = 1";
		}
				
		$res = mysql_query($sql) or dberror($sql);
		while($row = mysql_fetch_assoc($res))
		{
			$users[]  = $row["user_id"];
		}
		
		if(count($users) > 0)
		{
			$user_filter = implode(',', $users);
			$sql = "select distinct address_id, address_company, user_id, user_name, user_firstname ".
				   "from addresses ".
				   "left join users on user_address = address_id ".
				   "where user_id IN (" . $user_filter . ")";


			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{
				$company = array();
				$company["id"] = $row["address_id"];
				$company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
				$company["user"] = $row["user_id"];
				$company["role"] = "Additional Recipient";


				$sql1 = "select order_file_address_id from order_file_addresses ". 
						"where order_file_address_file = " . $id .
						"    and order_file_address_address = " .dbquote($row["address_id"]);

				$res1 = mysql_query($sql1) or dberror($sql1);

				if ($row1 = mysql_fetch_assoc($res1))
				{
					$company["access"] = 1;            
				}
				else
				{
					$company["access"] = 0;
				}

				$companies[] = $company;
			}


		}

	}
   

    return $companies;
}



/*************************************************************************
   get all the companies, involved in an order for comments
**************************************************************************/
function get_involved_companies_2($order_id, $id)
{
    
    $companies = array();

    // get the retail coordinator
    $sql = "select distinct address_id, address_company, user_id, user_name, user_firstname ".
           "from orders ".
           "left join projects on project_order = order_id ".
           "left join users on user_id = project_retail_coordinator ".
           "left join addresses on address_id = user_address ".
           "where order_id = ". $order_id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        if ($row["address_id"])
        {
            $company = array();
            $company["id"] = $row["address_id"];
            $company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
			$company["user"] = $row["user_id"];
			$company["role"] = "Project Manager";
            
            $sql1 = "select comment_address_id from comment_addresses ". 
                    "where comment_address_comment = " . $id .
                    "    and comment_address_address = " . $row["address_id"];

            $res1 = mysql_query($sql1) or dberror($sql1);

            if ($row1 = mysql_fetch_assoc($res1))
            {
                $company["access"] = 1;            
            }
            else
            {
                $company["access"] = 0;
            }

            $companies[] = $company;
        }
    }


    // get the retail operator
    $sql = "select distinct address_id, address_company, user_id, user_name, user_firstname ".
           "from orders ".
           "left join projects on project_order = order_id ".
            "left join users on user_id = order_retail_operator ".
           "left join addresses on address_id = user_address ".
           "where order_id = ". $order_id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        if ($row["address_id"])
        {
            $company = array();
            $company["id"] = $row["address_id"];
            $company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
			$company["user"] = $row["user_id"];
			$company["role"] = "Retail Operator";

            $sql1 = "select comment_address_id from comment_addresses ". 
                    "where comment_address_comment = " . $id .
                    "    and comment_address_address = " . $row["address_id"];
            
            $res1 = mysql_query($sql1) or dberror($sql1);

            if ($row1 = mysql_fetch_assoc($res1))
            {
                $company["access"] = 1;            
            }
            else
            {
                $company["access"] = 0;
            }

            $companies[] = $company;
        }
    }
    
    // get the client
    $sql = "select distinct address_id, address_company, order_user, user_name, user_firstname ".
           "from orders ".
		    "left join users on user_id = order_user ".
           "left join addresses on address_id = order_client_address ".
           "where order_id = ". $order_id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $company = array();
        $company["id"] = $row["address_id"];
        $company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
		$company["user"] = $row["order_user"];
		$company["role"] = "Client";

            $sql1 = "select comment_address_id from comment_addresses ". 
                    "where comment_address_comment = " . $id .
                    "    and comment_address_address = " . $row["address_id"];

            $res1 = mysql_query($sql1) or dberror($sql1);

            if ($row1 = mysql_fetch_assoc($res1))
            {
                $company["access"] = 1;            
            }
            else
            {
                $company["access"] = 0;
            }

        $companies[] = $company;
    }


    // get the design contractor
    $sql = "select distinct address_id, address_company, user_id, user_name, user_firstname ".
           "from orders ".
           "left join projects on project_order = order_id ".
           "left join users on user_id = project_design_contractor ".
           "left join addresses on address_id = user_address ".
           "where order_id = ". $order_id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        if ($row["address_id"])
        {
            $company = array();
            $company["id"] = $row["address_id"];
            $company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
			$company["user"] = $row["user_id"];
			$company["role"] = "Design Contractor";
            
            $sql1 = "select comment_address_id from comment_addresses ". 
                    "where comment_address_comment = " . $id .
                    "    and comment_address_address = " . $row["address_id"];

            $res1 = mysql_query($sql1) or dberror($sql1);

            if ($row1 = mysql_fetch_assoc($res1))
            {
                $company["access"] = 1;            
            }
            else
            {
                $company["access"] = 0;
            }

            $companies[] = $company;
        }
    }

    
    
    
    // get the design supervisor
    $sql = "select distinct address_id, address_company, user_id, user_name, user_firstname ".
           "from orders ".
           "left join projects on project_order = order_id ".
           "left join users on user_id = project_design_supervisor ".
           "left join addresses on address_id = user_address ".
           "where order_id = ". $order_id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        if ($row["address_id"])
        {
            $company = array();
            $company["id"] = $row["address_id"];
            $company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
			$company["user"] = $row["user_id"];
			$company["role"] = "Design Supervisor";
            
            $sql1 = "select comment_address_id from comment_addresses ". 
                    "where comment_address_comment = " . $id .
                    "    and comment_address_address = " . $row["address_id"];

            $res1 = mysql_query($sql1) or dberror($sql1);

            if ($row1 = mysql_fetch_assoc($res1))
            {
                $company["access"] = 1;            
            }
            else
            {
                $company["access"] = 0;
            }

            $companies[] = $company;
        }
    }
    
    

    // get the suppliers
    $sql = "select distinct order_item_supplier_address, address_company, user_id, user_name, user_firstname ".
           "from orders ".
           "left join order_items on order_item_order = order_id ".
           "left join addresses on address_id = order_item_supplier_address ".
           "left join users on user_address = order_item_supplier_address ".
           "where address_id != 2390 and user_active = 1 and order_id = ". $order_id;

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        if ($row["order_item_supplier_address"])
        {
            $company = array();
            $company["id"] = $row["order_item_supplier_address"];
            $company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
			$company["user"] = $row["user_id"];
			$company["role"] = "Supplier";

            $sql1 = "select comment_address_id from comment_addresses ". 
                    "where comment_address_comment = " . $id .
                    "    and comment_address_address = " . $row["order_item_supplier_address"];

            $res1 = mysql_query($sql1) or dberror($sql1);

            if ($row1 = mysql_fetch_assoc($res1))
            {
                $company["access"] = 1;            
            }
            else
            {
                $company["access"] = 0;
            }

            $companies[] = $company;
        }
    }

    // get the forwarders
    $sql = "select distinct order_item_forwarder_address, address_company, user_id, user_name, user_firstname ".
           "from orders ".
           "left join order_items on order_item_order = order_id ".
           "left join addresses on address_id = order_item_forwarder_address ".
           "left join users on user_address = order_item_forwarder_address ".
           "where address_id != 2390 and user_active = 1 and order_id = ". $order_id;

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        if ($row["order_item_forwarder_address"])
        {
            $company = array();
            $company["id"] = $row["order_item_forwarder_address"];
            $company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
			$company["user"] = $row["user_id"];
			$company["role"] = "Forwarder";

            $sql1 = "select comment_address_id from comment_addresses ". 
                    "where comment_address_comment = " . $id .
                    "    and comment_address_address = " . $row["order_item_forwarder_address"];

            $res1 = mysql_query($sql1) or dberror($sql1);

            if ($row1 = mysql_fetch_assoc($res1))
            {
                $company["access"] = 1;            
            }
            else
            {
                $company["access"] = 0;
            }

            $companies[] = $company;
        }
    }


    return $companies;
}

/*************************************************************************
   set a picture for every order the user has to do something
**************************************************************************/
function set_to_do_pictures($sql_images, $type)
{
    $images = array();

    $res = mysql_query($sql_images) or dberror($sql_images);

    while ($row = mysql_fetch_assoc($res))
    {

        $sql = "select task_id " .
               "from tasks " .
               "where task_order = " . $row["order_id"] .
               "   and task_user = " . user_id() .
               "   and task_done_date is null ";

        $res1 = mysql_query($sql) or dberror($sql);

        if ($row1 = mysql_fetch_assoc($res1))
        {            
            if ($type == 1)
            {
                $images[$row["project_id"]] = "/pictures/todo.gif";
            }
            else
            {
                $images[$row["order_id"]] = "/pictures/todo.gif";
            }
        }
        else
        {

            if ($type == 1 and has_access("can_edit_retail_data"))
            {
                if (!$row["concat(user_name,' ',user_firstname)"])
                {
                    $images[$row["project_id"]] = "/pictures/neworder.gif";
                }
            }
            else if ($type == 2 and has_access("can_edit_retail_data"))
            {
                if (!$row["concat(user_name,' ',user_firstname)"])
                {
                    $images[$row["order_id"]] = "/pictures/neworder.gif";
                }
            }

        }
    }

    return $images;

}


/*************************************************************************
   get file owener
**************************************************************************/
function get_file_owner($id)
{
    $sql = "select order_file_owner from order_files where order_file_id = " . $id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {            
        $owner = $row["order_file_owner"];
    }
    else
    {
        $owener = 0;
    }
    return $owner;
}


/*************************************************************************
   check if all supplier's offers have a price entered
**************************************************************************/
function check_if_all_items_have_prices($oid, $supplier)
{
    
    $num_recs = 0;

    // check if all special items have a price entered
    $sql = "select count(order_item_id) as num_recs " .
           "from order_items " .
           "where order_item_type = " . ITEM_TYPE_SPECIAL . 
           "    and order_item_item is null " .
           "    and order_item_order = " . $oid .
           "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) " . 
           "    and (order_item_no_offer_required = 0 or order_item_no_offer_required is null) " .
           "    and (order_item_supplier_price = 0 or order_item_supplier_price is null) " .
           "    and order_item_supplier_address = " . $supplier;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {            
        $num_recs = $row["num_recs"];
    }

    if ($num_recs > 0) 
    {
        return 0;
    }
    else
    {
        return 1;
    }

}


/*************************************************************************
   check if all supplier's offes have been accepted
**************************************************************************/
function check_if_all_items_are_offered($oid)
{
    
    $num_recs = 0;

    // check if all special items have been offered by suppliers
    $sql = "select count(order_item_id) as num_recs " .
           "from order_items " .
           "where order_item_type = " . ITEM_TYPE_SPECIAL . 
           "    and order_item_item is null " .
           "    and order_item_order = " . $oid .
           "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) " . 
           "    and (order_item_no_offer_required = 0 or order_item_no_offer_required is null) " .
           "    and order_item_client_price = 0 ";

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {            
        $num_recs = $row["num_recs"];
    }

    if ($num_recs > 0) 
    {
        return 0;
    }
    

    // check if all offers have been accepted by retail staff
    $offers_accepted = 1;

    $sql = "select distinct order_item_supplier_address ".
           "from order_items ".
           "where order_item_type = " . ITEM_TYPE_SPECIAL . 
           "    and order_item_item is null ".
           "    and order_item_order = " . $oid .
           "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
           "    and (order_item_no_offer_required = 0 or order_item_no_offer_required is null) ";

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {  
        $sql = "select count(order_mail_id) as num_recs " .
               "from order_mails ".
               "left join order_states on order_state_id = order_mail_order_state ".
               "left join users on order_mail_user = user_id ".
               "left join addresses on user_address = address_id ".
               "where order_mail_order = " . $oid .
               "    and order_state_code = " . OFFER_ACCEPTED .
               "    and user_address = " . $row["order_item_supplier_address"];
        $res1 = mysql_query($sql) or dberror($sql);

        if ($row1 = mysql_fetch_assoc($res1))
        {
            if($row1["num_recs"] == 0)
            {
                $offers_accepted = 0;
            }
        }
    }


    if ($offers_accepted == 0)
    {
        return 0;
    }
    else
    {
        return 1;
    }

}



/*************************************************************************
   check if all items do have a ready for pick up date
**************************************************************************/
function check_if_all_items_hav_pick_up_date($oid, $address_id)
{
    
    $sql = "select count(order_item_id) as num_recs " .
                   "from order_items " .
                   "where order_item_order = " . $oid .
                   "  and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                   "  and (order_item_ready_for_pickup is null or order_item_ready_for_pickup = '0000-00-00') " .
                   "  and (order_item_not_in_budget is null or order_item_not_in_budget = 0) " .
                   "  and order_item_supplier_address = " . $address_id;
        
    $res = mysql_query($sql) or dberror($sql);
    
    if ($row = mysql_fetch_assoc($res))
    {
        if($row["num_recs"] > 0)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
}


/*************************************************************************
   check if all items do have an arrival date
**************************************************************************/
function check_if_all_items_hav_arrival_date($oid, $address_id)
{
    
    if($address_id)
    {
        $sql = "select count(order_item_id) as num_recs " .
                       "from order_items " .
                       "where order_item_order = " . $oid .
                       "  and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                       "  and (order_item_arrival is null or order_item_arrival = '0000-00-00') " .
                       "  and (order_item_not_in_budget is null or order_item_not_in_budget = 0) " .
                       "  and order_item_forwarder_address = " . $address_id;
    }
    else
    {
        $sql = "select count(order_item_id) as num_recs " .
                       "from order_items " .
                       "where order_item_order = " . $oid .
                       "  and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                       "  and (order_item_arrival is null or order_item_arrival = '0000-00-00') " .
                       "  and (order_item_not_in_budget is null or order_item_not_in_budget = 0) ";
    }

    $res = mysql_query($sql) or dberror($sql);
    
    if ($row = mysql_fetch_assoc($res))
    {
        if($row["num_recs"] > 0)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }

}


/*************************************************************************
   check if all items do have an expected arrival date
**************************************************************************/
function check_if_all_items_hav_expected_arrival_date($oid, $address_id)
{
    
    
    //old version before 23.10.05
    //all items have to have a ready for pick up date
    /*
    $sql = "select count(order_item_id) as num_recs " .
                   "from order_items " .
                   "where order_item_order = " . $oid .
                   "  and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                   "  and (order_item_expected_arrival is null or order_item_expected_arrival = '0000-00-00') " .
                   "  and (order_item_not_in_budget is null or order_item_not_in_budget = 0) " .
                   "  and order_item_forwarder_address = " . $address_id;
        
    $res = mysql_query($sql) or dberror($sql);
    
    if ($row = mysql_fetch_assoc($res))
    {
        if($row["num_recs"] > 0)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
    */


    //new version after 22.10.05
    //if there is at least one item haveing a ready for pick up date it can be delivered
    $sql = "select count(order_item_id) as num_recs " .
                   "from order_items " .
                   "where order_item_order = " . $oid .
                   "  and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                   "  and (order_item_expected_arrival is not null and order_item_expected_arrival <> '0000-00-00') " .
                   "  and (order_item_not_in_budget is null or order_item_not_in_budget = 0) " .
                   "  and order_item_forwarder_address = " . $address_id;
        
    $res = mysql_query($sql) or dberror($sql);
    
    if ($row = mysql_fetch_assoc($res))
    {
        if($row["num_recs"] > 0)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
}

/*************************************************************************
   check if all items do have an order date
**************************************************************************/
function check_if_all_items_hav_order_date($oid, $address_id)
{
    
    if($address_id)
    {
        $sql = "select count(order_item_id) as num_recs " .
                       "from order_items " .
                       "where order_item_order = " . $oid .
                       "  and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                       "  and (order_item_ordered is null or order_item_ordered = '0000-00-00') " .
                       "  and (order_item_not_in_budget is null or order_item_not_in_budget = 0) " .
                       "  and order_item_forwarder_address = " . $address_id;
    }
    else
    {
        $sql = "select count(order_item_id) as num_recs " .
                       "from order_items " .
                       "where order_item_order = " . $oid .
                       "  and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                       "  and (order_item_ordered is null or order_item_ordered = '0000-00-00') " .
                       "  and (order_item_not_in_budget is null or order_item_not_in_budget = 0) ";
    }

    $res = mysql_query($sql) or dberror($sql);
    
    if ($row = mysql_fetch_assoc($res))
    {
        if($row["num_recs"] > 0)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
}


/*************************************************************************
   check if there was made a request for delivery for all items
**************************************************************************/
function check_requast_for_deleivery_for_all_items($oid, $order_state_code)
{
    
    // count forwarders in order
    $num_forwarders = 0;

    $sql = "select distinct order_item_forwarder_address as forwarder " .
                       "from order_items " .
                       "where order_item_order = " . $oid .
                       "  and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                       "  and (order_item_not_in_budget is null or order_item_not_in_budget = 0) ";


    $res = mysql_query($sql) or dberror($sql);
    
    while ($row = mysql_fetch_assoc($res))
    {
        $num_forwarders++;
    }


    // count distinct mails made
    $num_mails = 0;
    $sql = "select distinct order_mail_order, order_state_code, order_mail_user ".
           "from order_mails ".
           "left join order_states on order_state_id = order_mail_order_state " .
           "where order_mail_order = " . $oid . " and order_state_code = " . $order_state_code;

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        $num_mails++;
    }


    if ($num_mails >= $num_forwarders)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

/*************************************************************************
   get the role of a user in the context of an order or project
   contains the orders the user has access to because of his role
**************************************************************************/
function get_user_specific_order_list($user_id, $type, $user_roles = array(), $travelling_projects = false)
{
    
    $order_ids = array();
    $condition = "";
    $user_address = 0;
	$project_access_filter = "";
	$can_only_see_projects_of_his_address = false;
	$can_only_see_orders_of_his_address = false;

    // get user's address
    $sql = "select user_address, user_can_only_see_his_projects, user_can_only_see_his_orders, " .    
		   "user_can_only_see_orders_of_his_address, user_can_only_see_projects_of_his_address " . 
		   "from users ".
           "where user_id = " . user_id();

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {            
        $user_address = $row["user_address"];

		$project_access_filter = "";
		if($type == 1 and $row["user_can_only_see_his_projects"] == 1)
		{
			$project_access_filter = " and order_user = " . user_id() . " ";
		}
		elseif($type == 2 and $row["user_can_only_see_his_orders"] == 1)
		{
			$project_access_filter = " and order_user = " . user_id() . " ";
		}

		if($row["user_can_only_see_projects_of_his_address"] == 1)
		{
			$can_only_see_projects_of_his_address = true;
		}

		if($row["user_can_only_see_orders_of_his_address"] == 1)
		{
			$can_only_see_orders_of_his_address = true;
		}
    }


    //get users country
    $user_country = 0;
    $sql = "select address_country from addresses " .
           "where address_id = " . dbquote($user_address);

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $user_country = $row["address_country"];
    }


    // check client
    if($can_only_see_projects_of_his_address == true)
	{
		$sql = "select order_id " .
			   "from orders ".
			   "where (order_archive_date is not null and order_archive_date <> '0000-00-00') and order_client_address = " . $user_address;
	}
	elseif($can_only_see_orders_of_his_address == true)
	{
		$sql = "select order_id " .
			   "from orders ".
			   "where (order_archive_date is not null and order_archive_date <> '0000-00-00') and  order_client_address = " . $user_address;
	}
	elseif(has_access("has_access_to_all_projects_of_his_company"))
	{
		$sql = "select order_id " .
			   "from orders ".
			   "where (order_archive_date is not null and order_archive_date <> '0000-00-00') and  order_client_address = " . $user_address;
	}
	else
	{
		$sql = "select order_id " .
			   "from orders ".
			   "where (order_archive_date is not null and order_archive_date <> '0000-00-00') and  order_user = " . $user_id . 
			   "   and order_client_address = " . $user_address;
	}


	if($project_access_filter)
    {
		$sql .= " " . $project_access_filter;
	}


	$res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {            
        $order_ids[] = $row["order_id"];
    }


	

	// retail operator
    $sql = "select order_id " .
           "from orders ".
           "where (order_archive_date is not null and order_archive_date <> '0000-00-00') and  order_retail_operator = " . $user_id;

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {            
        $order_ids[] = $row["order_id"];
    }


    // local agent
	/*
	if ($can_only_see_projects_of_his_address == false 
		and has_access("has_access_to_all_projects_of_his_country") 
		AND $type == 1)
    {
	*/
		$country_filter = "";
		$tmp = array();
        $sql = "select * from country_access " .
               "where country_access_user = " . user_id();


        $res = mysql_query($sql) or dberror($sql);

		while ($row = mysql_fetch_assoc($res))
        {            
            $tmp[] = $row["country_access_country"];
        }

        if(count($tmp) > 0) {
			
			if(has_access("has_access_to_all_projects_of_his_country"))
			{
				$country_filter = " and address_country IN (" . implode(",", $tmp) . ", " . $user_country . ") ";
			}
			else
			{
				$country_filter = " and address_country IN (" . implode(",", $tmp)  . ") ";
			}
		}

		if($project_access_filter and $country_filter)
		{
			$filter = "((order_archive_date is not null and order_archive_date <> '0000-00-00') " . $project_access_filter . ") ";
			$filter .= " or ((order_archive_date is not null and order_archive_date <> '0000-00-00') " . $country_filter. ")";
		}
		elseif($project_access_filter)
		{
			$filter = "(order_archive_date is not null and order_archive_date <> '0000-00-00') " . $project_access_filter;
		}
		elseif($country_filter)
		{
			$filter = "(order_archive_date is not null and order_archive_date <> '0000-00-00') " . $country_filter;
		}
		else
		{
			$filter = "(order_archive_date is not null and order_archive_date <> '0000-00-00') and address_country  = " . $user_country;
		}

		
        
        $sql = "select project_order " .
               "from projects ".
               "left join orders on order_id = project_order " .
               "left join addresses on address_id = order_client_address " .
               "where " . $filter;

        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {            
            $order_ids[] = $row["project_order"];
        }
    /*
	}
	*/
	

    if ($can_only_see_orders_of_his_address == false and has_access("has_access_to_all_orders_of_his_country") AND $type == 2)
    {
		$country_filter = "";
		$tmp = array();
        $sql = "select * from country_access " .
               "where country_access_user = " . user_id();


        $res = mysql_query($sql) or dberror($sql);

		while ($row = mysql_fetch_assoc($res))
        {            
            $tmp[] = $row["country_access_country"];
        }

        if(count($tmp) > 0) {
			$country_filter = " and address_country IN (" . implode(",", $tmp) . ", " . $user_country . ") ";
		}

        
		if($project_access_filter and $country_filter)
		{
			$filter = "((order_archive_date is not null and order_archive_date <> '0000-00-00') " . $project_access_filter . ") ";
			$filter .= " or ((order_archive_date is not null and order_archive_date <> '0000-00-00') " . $country_filter. ")";
		}
		elseif($project_access_filter)
		{
			$filter = "(order_archive_date is not null and order_archive_date <> '0000-00-00') " . $project_access_filter;
		}
		elseif($country_filter)
		{
			$filter = "(order_archive_date is not null and order_archive_date <> '0000-00-00') " . $country_filter;
		}
		else
		{
			$filter = "(order_archive_date is not null and order_archive_date <> '0000-00-00') and address_country  = " . $user_country;
		}

		     
        
        $sql = "select order_id " .
               "from orders ".
               "left join addresses on address_id = order_client_address " .
               "where " . $filter;


        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {            
            $order_ids[] = $row["order_id"];
        }
    }

    //orders
    if($type == 2)
    {
        // check supplier or forwarder
        if (has_access("can_view_empty_orders"))
        {
            $sql = "select distinct order_item_order " .
                   "from order_items ".
				   "left join orders on order_id = order_item_order " . 
                   "where (order_archive_date is not null and order_archive_date <> '0000-00-00') and order_item_supplier_address = " .$user_address . 
                   "   or order_item_forwarder_address = " . $user_address;
        }
        else
        {
            $sql = "select distinct order_item_order " .
                   "from order_items ".
				   "left join orders on order_id = order_item_order " .
                   "where (order_archive_date is not null and order_archive_date <> '0000-00-00') and (order_item_supplier_address = " .$user_address . 
                   "   or order_item_forwarder_address = " . $user_address . ")" .
                   "   and (order_item_not_in_budget = 0 ".
                   "   or order_item_not_in_budget is null)";
        }


        $res = mysql_query($sql) or dberror($sql);
    
        while ($row = mysql_fetch_assoc($res))
        {            
            $order_ids[] = $row["order_item_order"];
        }
		

    }

    //projects
    if($type == 1)
    {
        // retail coordinator, design contractor, design supervisor
        if(in_array(7, $user_roles)) // design contractor
		{
			$sql = "select project_order " .
				   "from projects ".
				   "left join orders on order_id = project_order " .
				   "where (order_archive_date is not null and order_archive_date <> '0000-00-00') and order_actual_order_state_code >= 220 " . 
				   "   and (project_retail_coordinator = " . $user_id . 
				   "   or project_design_contractor = " . $user_id .
				   "   or project_design_supervisor = " . $user_id . 
				   ")";
		}
		else
		{

			$sql = "select project_order " .
				   "from projects ".
				   "left join orders on order_id = project_order " .
				   "where (order_archive_date is not null and order_archive_date <> '0000-00-00') and project_retail_coordinator = " . $user_id . 
				   "   or project_design_contractor = " . $user_id .
				   "   or project_design_supervisor = " . $user_id;
		}

        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {            
            $order_ids[] = $row["project_order"];
        }

       
        // check supplier or forwarder
        if (has_access("can_view_empty_orders") and has_access("can_view_order_before_booklet_approval"))
        {
            $sql = "select distinct order_item_order " .
                   "from order_items ".
				   "left join orders on order_id = order_item_order " .
                   "where (order_archive_date is not null and order_archive_date <> '0000-00-00') and order_item_supplier_address = " .$user_address . 
                   "   or order_item_forwarder_address = " . $user_address;
        }
        elseif (has_access("can_view_empty_orders") and !has_access("can_view_order_before_booklet_approval"))
        {
            $sql = "select distinct order_item_order " .
                   "from order_items ".
				   "left join orders on order_id = order_item_order " .
                   "where (order_archive_date is not null and order_archive_date <> '0000-00-00') and (order_item_supplier_address = " .$user_address .
                   "   or order_item_forwarder_address = " . $user_address . ")".
                   "   and order_item_show_to_suppliers = 1";
        }
        elseif (!has_access("can_view_empty_orders") and has_access("can_view_order_before_booklet_approval"))
        {
            $sql = "select distinct order_item_order " .
                   "from order_items ".
				   "left join orders on order_id = order_item_order " .
                   "where (order_archive_date is not null and order_archive_date <> '0000-00-00') and (order_item_supplier_address = " .$user_address . 
                   "   or order_item_forwarder_address = " . $user_address . ")" .
                   "   and (order_item_not_in_budget = 0 ".
                   "   or order_item_not_in_budget is null)";
        }
        elseif (!has_access("can_view_empty_orders") and !has_access("can_view_order_before_booklet_approval"))
        {
			$sql = "select distinct order_item_order " .
                   "from order_items " .
				   "left join orders on order_id = order_item_order " .
                   "where (order_archive_date is not null and order_archive_date <> '0000-00-00') and (order_item_supplier_address = " .$user_address . 
                   "   or order_item_forwarder_address = " . $user_address . ")" .
                   "   and (order_item_not_in_budget = 0 ".
                   "   or order_item_not_in_budget is null)" .
                   "   and order_item_show_to_suppliers = 1";
        }

        $res = mysql_query($sql) or dberror($sql);
    
        while ($row = mysql_fetch_assoc($res))
        {            
            $order_ids[] = $row["order_item_order"];
        }
    }


	//get projects from attachments only for suppliers
	if(in_array(5, $user_roles) or in_array(29, $user_roles)) {
		$sql_order_addresses = 'select DISTINCT order_file_order from order_files ' . 
						   'left join order_file_addresses on order_file_address_file = order_file_id ' .
						   'where order_file_address_address = ' . $user_address;


		$res = mysql_query($sql_order_addresses) or dberror($sql_order_addresses);
		while ($row = mysql_fetch_assoc($res))
		{
			 $order_ids[] = $row["order_file_order"];
		}
	}

	
    
	/*
	$condition = implode(" or order_id = ", $order_ids);
    
    if($condition != "")
    {
        $condition = "order_id = " . $condition;
    }
	*/


	//user has access to all travelling projects
	if($travelling_projects == true)
	{
		$sql = "select distinct posorder_order " .
			   "from posorderspipeline ".
			   "left join orders on order_id = posorder_order " .
			   "left join posareaspipeline on posarea_posaddress = posorder_posaddress " .
			   "where (order_archive_date is not null and order_archive_date <> '0000-00-00') " . 
			   " and (posarea_area IN (4,5) or posorder_subclass IN(17)) and posorder_order > 0 and posorder_type = 1";


		$res = mysql_query($sql) or dberror($sql);
    
        while ($row = mysql_fetch_assoc($res))
        {            
            $order_ids[] = $row["posorder_order"];
        }


		$sql = "select distinct posorder_order " .
			   "from posorders ".
			   "left join orders on order_id = posorder_order " .
			   "left join posareas on posarea_posaddress = posorder_posaddress " .
			   "where (order_archive_date is not null and order_archive_date <> '0000-00-00') " . 
			   " and (posarea_area IN (4,5) or posorder_subclass IN(17)) and posorder_order > 0 and posorder_type = 1";


		$res = mysql_query($sql) or dberror($sql);
    
        while ($row = mysql_fetch_assoc($res))
        {            
            $order_ids[] = $row["posorder_order"];
        }

	}

	if(count($order_ids) > 0)
	{
		$tmp = implode(",", $order_ids);
		$condition = " order_id IN (" .  $tmp . ") ";
	
	
	}

    return $condition;

}

/********************************************************************
    get cer currency
*********************************************************************/
function get_cer_currency($pid)
{
    $currency = array();
    
	$sql = "select * from cer_basicdata " . 
		   "left join currencies on currency_id = cer_basicdata_currency " . 
		   "where cer_basicdata_version = 0 and cer_basicdata_project = " . $pid;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$currency["id"] = $row["cer_basicdata_currency"];
		$currency["symbol"] = $row["currency_symbol"];
		$currency["exchange_rate"] = $row["cer_basicdata_exchangerate"];
		$currency["factor"] = $row["cer_basicdata_factor"];
	}

    else
    {
        $currency["id"] = 0;
        $currency["symbol"] = "";
        $currency["exchange_rate"] = 0;
        $currency["factor"] = 0;
    }
    
    return $currency;
}


/********************************************************************
    get intangibles
*********************************************************************/
function get_pos_intangibles($project_id, $investment_type)
{
	$posintangibles = array();
	
	$sql = "select * from cer_investments " .
		   "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
		   "where cer_investment_cer_version = 0 and cer_investment_project = " . $project_id .
		   " and cer_investment_type = $investment_type ";

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$posintangibles = $row;
	}
	return $posintangibles;
}


/********************************************************************
    check if cer_summary exists
*********************************************************************/
function build_missing_cer_records($pid)
{
	$sql = "select count(cer_summary_id) as num_recs " . 
       "from cer_summary " . 
	   "where cer_summary_cer_version = 0 and cer_summary_project = " . $pid;

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	if ($row["num_recs"] == 0)
	{
		//create cer_revenue records
	
		$fields = array();
		$values = array();

		$fields[] = "cer_summary_project";
		$values[] = param("pid");

	
		$fields[] = "date_created";
		$values[] = "now()";

		$fields[] = "date_modified";
		$values[] = "now()";

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$fields[] = "user_modified";
		$values[] = dbquote(user_login());

		$sql = "insert into cer_summary (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
		
		mysql_query($sql) or dberror($sql);

	}

	// cer basic data
	$currency_id = 0;
	$exchange_rate = 0;
	$factor = 0;
	$country_id = 0;
	$order_state = 0;

	$sql = "select currency_id, currency_exchange_rate, currency_factor, country_id, " .
		   "order_actual_order_state_code " . 
		   "from projects " . 
		   "left join orders on order_id = project_order " . 
		   "left join addresses on address_id = order_client_address " .
		   "left join countries on country_id = address_country " . 
		   "left join currencies on currency_id = country_currency " . 
		   "where project_id = " . $pid;

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$currency_id = $row["currency_id"];
		$exchange_rate = $row["currency_exchange_rate"];
		$factor = $row["currency_factor"];
		$country_id = $row["country_id"];
		$order_state = $row["order_actual_order_state_code"];
	}


	//get currency data
	$currency_id2 = 0;
	$exchange_rate2 = 0;
	$factor2 = 0;

	$sql = "select currency_id, currency_exchange_rate, currency_factor " . 
		   "from projects " . 
		   "left join orders on order_id = project_order " . 
		   "left join countries on country_id = order_shop_address_country " . 
		   "left join currencies on currency_id = country_currency " . 
		   "where project_id = " . $pid;

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$currency_id2 = $row["currency_id"];
		$exchange_rate2 = $row["currency_exchange_rate"];
		$factor2 = $row["currency_factor"];
	}

	
	//update cer_basic_data
	$sql = "select count(cer_basicdata_id) as num_recs " . 
			"from cer_basicdata " . 
			"where cer_basicdata_version = 0 and cer_basicdata_project = " . $pid;

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	if ($row["num_recs"] == 0)
	{
		
		$fields = array();
		$values = array();

		$fields[] = "cer_basicdata_version";
		$values[] = 0;

		$fields[] = "cer_basicdata_version_context";
		$values[] = "";

		$fields[] = "cer_basicdata_version_user_id";
		$values[] = user_id();

		$fields[] = "cer_basicdata_project";
		$values[] = param("pid");

		$fields[] = "cer_basicdata_currency";
		$values[] = dbquote($currency_id);

		$fields[] = "cer_basicdata_factor";
		$values[] = dbquote($factor);


		$fields[] = "cer_basicdata_currency2";
		$values[] = dbquote($currency_id2);

		$fields[] = "cer_basicdata_factor2";
		$values[] = dbquote($factor2);


		$fields[] = "date_created";
		$values[] = "now()";

		$fields[] = "date_modified";
		$values[] = "now()";

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$fields[] = "user_modified";
		$values[] = dbquote(user_login());

		$sql = "insert into cer_basicdata (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
		
		mysql_query($sql) or dberror($sql);

	}


	$sql = "select * from posinvestment_types where posinvestment_type_active = 1";
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$sql_i = "select cer_investment_id from cer_investments " .
				 "where cer_investment_cer_version = 0 and cer_investment_project = " .  $pid . 
				 " and cer_investment_type = " . $row["posinvestment_type_id"];

		$res_i = mysql_query($sql_i) or dberror($sql_i);
		if (!$row_i = mysql_fetch_assoc($res_i))
		{
			$sql = "insert into cer_investments (" .
				   "cer_investment_project, " .
				   "cer_investment_type, " .
				   "user_created, date_created) values (".
				   param("pid") . ", " .
				   $row["posinvestment_type_id"] . ", " . 
				   dbquote(user_login()) . ", " . 
				   "current_timestamp)";

			$result = mysql_query($sql) or dberror($sql);
		}
	}
	
	return true;

}


/********************************************************************
    get investments from the list of materials
*********************************************************************/
function update_investments_from_the_list_of_materials($project_id, $order_id, $update_mode)
{
	$amounts = array();
	$comments = array();

	//get the local currency of shop
	$currency = get_cer_currency($project_id);

	$sql = "select * from cer_investments " .
		   "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
		   "where cer_investment_cer_version = 0 and cer_investment_project = " . $project_id;

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		if($row["posinvestment_type_cms_sumcostgroups"])
		{
			$group_total = 0;
			$cost_groups = explode(",", $row["posinvestment_type_cms_sumcostgroups"]);

			foreach($cost_groups as $key=>$cost_group)
			{
				$sql_i = "select sum(order_item_system_price*order_item_quantity) as total_in_system_currency " . 
						 "from order_items ".
						 "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
						 "   and order_item_order = " . $order_id . 
						 "   and order_item_cost_group =  " . dbquote($cost_group);

				$res_i = mysql_query($sql_i) or dberror($sql_i);
				if ($row_i = mysql_fetch_assoc($res_i))
				{
					if($currency["exchange_rate"] > 0)
					{
						$group_total = $group_total + ($row_i["total_in_system_currency"] / $currency["exchange_rate"])*$currency["factor"];
					}
				}

				if($update_mode == 1)
				{
					$amounts[$row["cer_investment_id"]] = $group_total;
					$comments[$row["cer_investment_id"]] = "inserted from the list of materials";
				}
				else
				{
					if($row["cer_investment_amount_cer_loc"] == 0)
					{
						$amounts[$row["cer_investment_id"]] = $group_total;
						$comments[$row["cer_investment_id"]] = "inserted from the list of materials";
					}
				}
			}
		}
	}
	
	foreach($amounts as $id=>$value)
	{
		if($value)
		{
			$fields = array();
		
			$value = dbquote($value);
			$fields[] = "cer_investment_amount_cer_loc = " . $value;

			$value = dbquote($comments[$id]);
			$fields[] = "cer_investment_comment = " . $value;

			$value1 = "current_timestamp";
			$project_fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$project_fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_cer_version = 0 and cer_investment_id = " . $id;

			mysql_query($sql) or dberror($sql);
		}

	}

	/*
	//update kl approved investments
	$sql = "update cer_investments set cer_investment_amount_cer_loc_approved = cer_investment_amount_cer_loc " . 
			"where cer_investment_cer_version = 0 and cer_investment_amount_cer_loc_approved = 0 and cer_investment_type in(1, 3, 5, 7, 11) and cer_investment_project = " . $project_id;

	$result = mysql_query($sql) or dberror($sql);
	*/

}


/********************************************************************
    get prroved investments from the list of materials
*********************************************************************/
function update_approved_investments_from_the_list_of_materials($project_id, $order_id, $update_mode)
{
	$amounts = array();
	$comments = array();

	//get the local currency of shop
	$currency = get_cer_currency($project_id);

	$sql = "select * from cer_investments " .
		   "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
		   "where cer_investment_cer_version = 0 and cer_investment_project = " . $project_id;

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		
		if($row["posinvestment_type_cms_sumcostgroups"])
		{
			$group_total = 0;
			$cost_groups = explode(",", $row["posinvestment_type_cms_sumcostgroups"]);

			foreach($cost_groups as $key=>$cost_group)
			{
				$sql_i = "select sum(order_item_system_price_freezed*order_item_quantity_freezed) as total_in_system_currency " . 
						 "from order_items ".
						 "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
						 "   and order_item_order = " . $order_id . 
						 "   and order_item_cost_group =  " . dbquote($cost_group);

				$res_i = mysql_query($sql_i) or dberror($sql_i);
				if ($row_i = mysql_fetch_assoc($res_i))
				{
					if($currency["exchange_rate"] > 0)
					{
						$group_total = $group_total + ($row_i["total_in_system_currency"] / $currency["exchange_rate"])*$currency["factor"];
					}
				}

				if($update_mode == 1)
				{
					$amounts[$row["cer_investment_id"]] = $group_total;
					$comments[$row["cer_investment_id"]] = "inserted from the list of materials";
				}
				else
				{
					if($row["cer_investment_amount_cer_loc"] == 0)
					{
						$amounts[$row["cer_investment_id"]] = $group_total;
						$comments[$row["cer_investment_id"]] = "inserted from the list of materials";
					}
				}
			}
		}
	}
	
	foreach($amounts as $id=>$value)
	{
		$fields = array();
    
		$value = dbquote($value);
		$fields[] = "cer_investment_amount_cer_loc = " . $value;

		$value = dbquote($comments[$id]);
		$fields[] = "cer_investment_comment = " . $value;

		$value1 = "current_timestamp";
		$project_fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$project_fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_cer_version = 0 and cer_investment_id = " . $id;
		mysql_query($sql) or dberror($sql);

	}


	//update kl approved investments
	/*
	$sql = "update cer_investments set cer_investment_amount_cer_loc_approved = cer_investment_amount_cer_loc " . 
			"where cer_investment_cer_version = 0 and cer_investment_amount_cer_loc_approved = 0 and cer_investment_type in(1, 3, 5, 7, 11) and cer_investment_project = " . $project_id;

	$result = mysql_query($sql) or dberror($sql);
    */
}


/********************************************************************
    get_province_name
*********************************************************************/
function get_province_name($place_id)
{
	$province_name = "";

	$sql = "select province_canton " . 
	       "from places " .
		   "left join provinces on province_id = place_province " . 
		   "where place_id = " . dbquote($place_id);

	$res = mysql_query($sql) or dberror($sql);
	
	if($row = mysql_fetch_assoc($res))
	{
		$province_name = $row["province_canton"];
	}

	return $province_name;
}

/*************************************************************************
   set a picture for every new attachment (introduced 2006-05-18
**************************************************************************/
function set_new_attachment_pictures($sql, $oid)
{
    $images = array();

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        $users = explode(",", $row["order_file_visited"]);
        
        if($row["date_created"] > '2006-05-18' and !in_array(user_id(), $users))
        {
            $images[$row["order_file_id"]] = "/pictures/bullet_ball_glass_red.gif";
        
            $visited = user_id() . "," . implode(",", $users);
            
            $sql = "Update order_files SET " .
                   "order_file_visited = " . dbquote($visited) .
                   " where order_file_id = " . $row["order_file_id"];

            $result = mysql_query($sql) or dberror($sql);
        }
    }

    return $images;

}


/*************************************************************************
   set a picture for every new comment (introduced 2006-05-18
**************************************************************************/
function set_new_comment_pictures($sql, $oid)
{
    $images = array();

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        $users = explode(",", $row["comment_visited"]);
        
        if($row["date_created"] > '2006-05-18' and !in_array(user_id(), $users))
        {
            $images[$row["comment_id"]] = "/pictures/bullet_ball_glass_red.gif";

            $visited = user_id() . "," . implode(",", $users);
            
            $sql = "Update comments SET " .
                   "comment_visited = " . dbquote($visited) .
                   " where comment_id = " . $row["comment_id"];

            $result = mysql_query($sql) or dberror($sql);
        }
    }

    return $images;

}


/********************************************************************
    get restrictions for file categoreis
*********************************************************************/
function get_file_category_restirctions($user_roles)
{
	$restrictions = array();

	if(count($user_roles) > 0)
	{
		$sql = "select role_file_category_file_category_id ". 
			   "from role_file_categories " . 
			   " where role_file_category_role_id IN (" . implode(',', $user_roles) . ") ";
		
		$res = mysql_query($sql) or dberror($sql);

		while ($row = mysql_fetch_assoc($res))
		{
			$restrictions[] = $row["role_file_category_file_category_id"];
		}
				   
	}

	return $restrictions;
}



/********************************************************************
    get POS Info of relocated POS
*********************************************************************/
function get_renovated_pos_info($order_id)
{
	$pos_id = 0;

	$sql = "select posaddress_id " .
		   "from posorders " .
		   "left join posaddresses on posaddress_id = posorder_posaddress " . 
		   "where posorder_order = " . $order_id;

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$pos_id = $row["posaddress_id"];
	}

	return $pos_id;
}

/********************************************************************
    get POS Info of relocated POS
*********************************************************************/
function get_relocated_pos_info($pos_id)
{
	$pod_data = array();

	$sql = "select posaddress_name, posaddress_zip, place_name, country_name " .
		   "from posaddresses " . 
		   "left join places on place_id = posaddress_place_id " .
		   "left join countries on country_id = posaddress_country " .
		   "where posaddress_id = " . $pos_id;

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$pos_data = $row;
	}

	return $pos_data;
}
?>