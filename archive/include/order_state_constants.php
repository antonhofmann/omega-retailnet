<?php
/********************************************************************

    order_state_constants.php

    Defines Constants used in the context with order states

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-10-19
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-03-03
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

/********************************************************************
    Constants
    Acces rights to a step are connected with the name of the step
    refer to table permissions
    e.g. can_perform_action_in_orders_110

    also there is a string for each order state in table strings
    having the order_state_code in its name e.g. order_110

    so if you are going to change constants you also have to
    change the names of permissions in the table permissions
    and the names of strings in table strings

*********************************************************************/

define("RETAIL_OPERATOR_ASSIGNED", "110");
define("RETAIL_COORDINATOR_ASSIGNED", "110");
define("ORDER_CONFIRMED", "210");
define("PROJECT_BOOKLET_APPROVED", "460");
define("RETAIL_OPERATOR_ASSIGNED_P", "120");
define("SUBMIT_BRIEFING_FOR_LAYOUT_PREVIEW", "220");
define("SUBMIT_REQUEST_FOR_DESIGN_APPROVAL", "300");
define("SUBMIT_REQUEST_FOR_OFFER", "510");
define("REQUEST_FOR_OFFER_SUBMITTED", "510");
define("REQUEST_FOR_OFFER_REJECTED", "520");
define("REQUEST_FOR_OFFER_ACCEPTED", "530");
define("OFFER_SUBMITTED", "540");
define("OFFER_REJECTED", "550");
define("OFFER_ACCEPTED", "560");

define("REQUEST_FOR_BUDGET_APPROVAL_SUBMITTED", "600");
define("BUDGET_APPROVED", "620");
define("ORDER_TO_SUPPLIER_SUBMITTED", "700");
define("REJECT_ORDER_BY_SUPPLIER", "710");
define("CONFIRM_ORDER_BY_SUPPLIER", "720");
define("REQUEST_FOR_DELIVERY_SUBMITTED", "730");
define("REQUEST_FOR_DELIVERY_ACCEPTED", "740");
define("DELIVERY_CONFIRMED_FRW", "750");
define("DELIVERY_CONFIRMED", "800");
define("MOVED_TO_THE_RECORDS", "820");
define("ORDER_CANCELLED", "900");
define("EMAIL_SENT", "910");

?>