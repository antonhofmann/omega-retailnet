<?php
/********************************************************************

    project_edit_milestone_selector

    Enter Filter Criteria for Project Milestones

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2012-04-06
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2012-04-06
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../include/page_modal.php";

check_access("can_edit_milestones");


/********************************************************************
    prepare all data needed
*********************************************************************/
$project_id = param("pid");

//get  parameters


$sql = "select project_milestone_id, milestone_code, milestone_text, project_milestone_visible " . 
       "from project_milestones " .
       " left join milestones on milestone_id = project_milestone_milestone " .
	   " where project_milestone_project = " . $project_id . 
	   " order by milestone_code";


/********************************************************************
    save data
*********************************************************************/
if(param("save_form"))
{
	
	if(param("save_form") == "milestones") //Milestones
	{

		$sql_u = "update project_milestones " . 
				   "set project_milestone_visible = 0 " . 
				   " where project_milestone_project = " . $project_id;

		$result = mysql_query($sql_u) or dberror($sql_u);
		
		$res = mysql_query($sql) or dberror($sql);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["M_" . $row["project_milestone_id"]])
			{
				$sql_u = "update project_milestones " . 
					   "set project_milestone_visible = 1 " . 
					   " where project_milestone_project = " . $project_id . 
					   " and project_milestone_id = " . $row["project_milestone_id"];

				$result = mysql_query($sql_u) or dberror($sql_u);

				
			}
		}
	}
}


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new form("projects", "project");
$form->add_hidden("pid", $project_id);
$form->add_hidden("save_form", "milestones");

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($row["project_milestone_visible"] == 1)
	{
		$form->add_checkbox("M_" . $row["project_milestone_id"], $row["milestone_code"] . " " .  $row["milestone_text"], true);
	}
	else
	{
		$form->add_checkbox("M_" . $row["project_milestone_id"], $row["milestone_code"] . " " .  $row["milestone_text"], false);
	}
	
}

$form->add_input_submit("submit", "Save Values", 0);

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Process buttons
*********************************************************************/ 

/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page_Modal("projects");

$page->header();
$page->title("Milestone Selector");

$form->render();

if(param("save_form"))
{
?>

<script languege="javascript">
var back_link = "project_edit_milestones.php?pid=<?php echo $project_id;?>"; 
$.nyroModalRemove();
</script>

<?php
}
$page->footer();
