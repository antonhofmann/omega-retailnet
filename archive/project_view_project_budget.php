<?php
/********************************************************************

    project_view_project_budget.php

    View Project's Budget Sheet in Client's Currency

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-17
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-07
    Version:        1.1.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_view_budget_in_projects");


/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

// get orders's currency
$order_currency = get_order_currency($project["project_order"]);


// create sql for oder items
if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
{
    $sql_order_items = "select order_item_id, order_item_text, order_item_quantity, ".
                   "    order_item_po_number, item_id, order_item_client_price, ".
                   "    TRUNCATE(order_item_quantity * order_item_client_price, 2) as total_price, ".
                   "    order_item_client_price, ".
                   "    if(item_code <> '', item_code, item_type_name) as item_shortcut, ".
                   "    category_priority, category_name, ".
                   "    address_shortcut, item_type_id, ".
                   "    item_type_priority, order_item_type, ".
                   "    concat(item_category_sortorder, ' ', item_category_name, ', ', category_name) as cat_name  ".
                   "from order_items ".
                   "left join items on order_item_item = item_id ".
                   "left join categories on order_item_category = category_id ".
                   "left join item_categories on item_category_id = item_category " .
                   "left join addresses on order_item_supplier_address = address_id ".
                   "left join item_types on order_item_type = item_type_id";

	$list1_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) " . 
		            " and order_item_order = " . $project["project_order"] .  
		            " and order_item_cost_group IN (10,6, 2)";
    
    $list2_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) and order_item_order = " . $project["project_order"] .  " and order_item_cost_group = 9";

    $list3_filter = "order_item_order = " . $project["project_order"] .  " and order_item_cost_group = 11";

	$list7_filter = "order_item_order = " . $project["project_order"] .  " and order_item_cost_group = 8";

    $list6_filter = "order_item_order = " . $project["project_order"] .  " and order_item_cost_group = 7";


}
else
{
    $sql_order_items = "select order_item_id, order_item_text, order_item_quantity_freezed, ".
                   "    order_item_po_number, item_id, order_item_client_price_freezed, ".
                   "    TRUNCATE(order_item_quantity_freezed * order_item_client_price_freezed, 2) as total_price, ".
                   "    order_item_client_price_freezed, ".
                   "    if(item_code <> '', item_code, item_type_name) as item_shortcut, ".
                   "    category_priority, category_name, ".
                   "    address_shortcut, item_type_id, ".
                   "    item_type_priority, order_item_type, ".
                   "    concat(item_category_sortorder, ' ', item_category_name, ', ', category_name) as cat_name  ".
                   "from order_items ".
                   "left join items on order_item_item = item_id ".
                   "left join categories on order_item_category = category_id ".
                   "left join item_categories on item_category_id = item_category " .
                   "left join addresses on order_item_supplier_address = address_id ".
                   "left join item_types on order_item_type = item_type_id";


    
    $list1_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) " .
		            " and order_item_order = " . $project["project_order"] .  
		            " and order_item_cost_group IN (10,6, 2) " . 
		            " and order_item_in_freezed_budget = 1";

    $list2_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) and order_item_order = " . $project["project_order"] .  " and order_item_cost_group = 9 and order_item_in_freezed_budget = 1";

    $list3_filter = "order_item_order = " . $project["project_order"] .  " and order_item_cost_group = 11 and order_item_in_freezed_budget = 1";

	$list7_filter = "order_item_order = " . $project["project_order"] .  " and order_item_cost_group = 8 and order_item_in_freezed_budget = 1";

    $list6_filter = "order_item_order = " . $project["project_order"] .  " and order_item_cost_group = 7 and order_item_in_freezed_budget = 1";

}

//calculate budget group totals
$grouptotals_investment = array();
$grouptotals_investment[2] = 0;
$grouptotals_investment[6] = 0;
$grouptotals_investment[7] = 0;
$grouptotals_investment[8] = 0;
$grouptotals_investment[9] = 0;
$grouptotals_investment[10] = 0;
$grouptotals_investment[11] = 0;



$sql = "select order_item_cost_group, order_item_type, ".
       "order_item_system_price, order_item_quantity,  ".
       "order_item_system_price_freezed, order_item_quantity_freezed ".
       "from order_items ".
       "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
       "   and order_item_order=" . $project["project_order"] . 
       "   and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION .
	   "   or order_item_type = " . ITEM_TYPE_SERVICES .
       "   or order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST .
       " ) " .
	   " and order_item_cost_group > 0 " . 
	   " order by order_item_cost_group";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
	{
		if($row["order_item_quantity"] > 0)
		{
			$grouptotals_investment[$row["order_item_cost_group"]] = $grouptotals_investment[$row["order_item_cost_group"]] + $row["order_item_system_price"] * $row["order_item_quantity"];

		}
		else
		{
			$grouptotals_investment[$row["order_item_cost_group"]] = $grouptotals_investment[$row["order_item_cost_group"]] + $row["order_item_system_price"];
		}
	}
	else
	{
		if($row["order_item_quantity_freezed"] > 0)
		{
			$grouptotals_investment[$row["order_item_cost_group"]] = $grouptotals_investment[$row["order_item_cost_group"]] + $row["order_item_system_price_freezed"] * $row["order_item_quantity_freezed"];
		}
		else
		{
			$grouptotals_investment[$row["order_item_cost_group"]] = $grouptotals_investment[$row["order_item_cost_group"]] + $row["order_item_system_price_freezed"];
		}
	}
}

//budget
$project_cost_construction = $grouptotals_investment[7];
$project_cost_fixturing = $grouptotals_investment[10] + $grouptotals_investment[6] + $grouptotals_investment[2];
$project_cost_architectural = $grouptotals_investment[9];
$project_cost_equipment = $grouptotals_investment[11];
$project_cost_other = + $grouptotals_investment[8];
$budget_total = $project_cost_construction + $project_cost_fixturing + $project_cost_architectural + $project_cost_equipment + $project_cost_other;


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project");

$form->add_section("Project");
require_once "include/project_head_small.php";

$form->add_section("Cost Overview");

$form->add_label("t1", "Construction/Building in " . $order_currency["symbol"], 0, number_format($project_cost_construction,2));
$form->add_label("t2", "Store Fixturing/Furniture in " . $order_currency["symbol"], 0, number_format($project_cost_fixturing,2));
$form->add_label("t3", "Architectural Cost in " . $order_currency["symbol"], 0, number_format($project_cost_architectural,2));
$form->add_label("t4", "Equipment in " . $order_currency["symbol"], 0, number_format($project_cost_equipment,2));
$form->add_label("t5", "Other Cost in " . $order_currency["symbol"], 0, number_format($project_cost_other,2));
$form->add_label("t6", "Total in " . $order_currency["symbol"], 0, number_format($budget_total,2));



/********************************************************************
    Create Locla COnstruction Cost Positions
*********************************************************************/ 
$list6 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list6->set_title("Construction/Building");
$list6->set_entity("order_item");
$list6->set_filter($list6_filter);
$list6->set_order("item_code, order_item_id");

$list6->add_hidden("pid", param("pid"));
$list6->add_hidden("oid",$project["project_order"]);

$list6->add_column("item_shortcut", "Code");
$list6->add_column("order_item_text", "Text");

if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
{
    $list6->add_column("order_item_client_price", "Estimated Cost " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	
	$list6->set_footer("item_shortcut",  "Total " . $order_currency["symbol"]);
	$list6->set_footer("order_item_client_price", number_format($project_cost_construction,2));
}
else
{
    $list6->add_column("order_item_client_price_freezed", "Estimated Cost " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	
	$list6->set_footer("item_shortcut",  "Total " . $order_currency["symbol"]);
	$list6->set_footer("order_item_client_price_freezed", number_format($project_cost_construction,2));
}




/********************************************************************
    Create Standard Item List
*********************************************************************/ 
$list1 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list1->set_title("Store Fixturing/Furniture");
$list1->set_entity("order_item");
$list1->set_filter($list1_filter);
$list1->set_order("item_code");
$list1->set_group("cat_name");

$list1->add_hidden("pid", param("pid"));
$list1->add_hidden("oid",$project["project_order"]);

$list1->add_column("item_shortcut", "Item Code", "popup:catalog_item_view.php?id={item_id}");
$list1->add_column("order_item_text", "Name");

if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
{
    $list1->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
    $list1->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
}
else
{
    $list1->add_column("order_item_quantity_freezed", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
    $list1->add_column("order_item_client_price_freezed", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
}

$list1->add_column("total_price", "Total " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);




$list1->set_footer("item_shortcut", "Total " . $order_currency["symbol"]);
$list1->set_footer("total_price", number_format($project_cost_fixturing,2));



/********************************************************************
    Create Architectural Cost
*********************************************************************/ 
$list2 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list2->set_title("Architectural Cost");
$list2->set_entity("order_item");
$list2->set_filter($list2_filter);
//$list2->set_order("item_code_text");

$list2->add_hidden("pid", param("pid"));
$list2->add_hidden("oid",$project["project_order"]);

$list2->add_column("item_shortcut", "Item Code");
$list2->add_column("order_item_text", "Name");

if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
{
    $list2->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
    $list2->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
}
else
{
    $list2->add_column("order_item_quantity_freezed", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
    $list2->add_column("order_item_client_price_freezed", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
}

$list2->add_column("total_price", "Total " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

$list2->set_footer("item_shortcut", "Total " . $order_currency["symbol"]);
$list2->set_footer("total_price", number_format($project_cost_architectural,2));


/********************************************************************
    Create Equipment
*********************************************************************/ 
$list3 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list3->set_title("Equipment");
$list3->set_entity("order_item");
$list3->set_filter($list3_filter);

$list3->add_hidden("pid", param("pid"));
$list3->add_hidden("oid",$project["project_order"]);

$list3->add_column("item_shortcut", "Code");
$list3->add_column("order_item_text", "Text");

if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
{
    $list3->add_column("order_item_client_price", "Estimated Cost " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);

    $list3->set_footer("item_shortcut", "Total " . $order_currency["symbol"]);
    $list3->set_footer("order_item_client_price", number_format($project_cost_equipment,2));
}
else
{
    $list3->add_column("order_item_client_price_freezed", "Estimated Cost " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);

    $list3->set_footer("item_shortcut", "Total " . $order_currency["symbol"]);
    $list3->set_footer("order_item_client_price_freezed", number_format($project_cost_equipment,2));
}


/********************************************************************
    Other Cost
*********************************************************************/ 
$list7 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list7->set_title("Other Cost");
$list7->set_entity("order_item");
$list7->set_filter($list7_filter);

$list7->add_hidden("pid", param("pid"));
$list7->add_hidden("oid",$project["project_order"]);

$list7->add_column("item_shortcut", "Code");
$list7->add_column("order_item_text", "Text");

if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
{
    
	
	$list7->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
	$list7->add_column("total_price", "Estimated Cost " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);

    $list7->set_footer("item_shortcut", "Total " . $order_currency["symbol"]);
    $list7->set_footer("order_item_client_price", number_format($project_cost_other,2));
}
else
{
    $list7->add_column("order_item_quantity_freezed", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
	$list7->add_column("total_price", "Estimated Cost " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);

    $list7->set_footer("item_shortcut", "Total " . $order_currency["symbol"]);
    $list7->set_footer("total_price", number_format($project_cost_other,2));
}


/********************************************************************
    Create Exclusion Positions
*********************************************************************/ 
$list4 = new ListView($sql_order_items, LIST_HAS_HEADER);

$list4->set_title("Exclusions");
$list4->set_entity("order_item");
$list4->set_filter("order_item_order = " . $project["project_order"] .  " and order_item_type = " . ITEM_TYPE_EXCLUSION);

$list4->add_hidden("pid", param("pid"));
$list4->add_hidden("oid",$project["project_order"]);

$list4->add_column("item_shortcut", "Code");
$list4->add_column("order_item_text", "Exclusion");


/********************************************************************
    Create Notification Positions
*********************************************************************/ 
$list5 = new ListView($sql_order_items, LIST_HAS_HEADER);

$list5->set_title("Notifications");
$list5->set_entity("order_item");
$list5->set_filter("order_item_order = " . $project["project_order"] .  " and order_item_type = " . ITEM_TYPE_NOTIFICATION);

$list5->add_hidden("pid", param("pid"));
$list5->add_hidden("oid",$project["project_order"]);

$list5->add_column("item_shortcut", "Code");
$list5->add_column("order_item_text", "Notification");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list1->populate();
$list1->process();

$list2->populate();
$list2->process();

$list3->populate();
$list3->process();

$list4->populate();
$list4->process();

$list5->populate();
$list5->process();

$list6->populate();
$list6->process();


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Project Budget");

$form->render();

$list6->render();
echo "<br>";

echo "<br>";
$list1->render();
echo "<br>";
$list2->render();
echo "<br>";

$list3->render();
echo "<br>";

$list7->render();
echo "<br>";


$list4->render();
echo "<br>";
$list5->render();

$page->footer();

?>