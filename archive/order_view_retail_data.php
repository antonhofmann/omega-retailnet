<?php
/********************************************************************

    order_view_retail_data.php

    Edit Retail Assignements.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-08
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-12-03
    Version:        1.0.5

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/order_state_constants.php";

check_access("can_edit_retail_data");

register_param("oid");


/********************************************************************
    prepare all data needed
*********************************************************************/

// read project and order details
$order = get_order(param('oid'));

// get Action parameter
$action_parameter = get_action_parameter(RETAIL_OPERATOR_ASSIGNED, 2);

// get company's address
$client_address = get_address($order["order_client_address"]);



/********************************************************************
    build form
*********************************************************************/
$form = new Form("orders", "order");

$form->add_section("Order");
$form->add_hidden("oid", param('oid'));

require_once "include/order_head_small.php";

// Confirmation of delivery
$form->add_section("Confirmation of Delivery by");

$line = "concat(user_name, ' ', user_firstname)";

$form->add_lookup("order_delivery_confirmation_by", "Person", "users", $line, 0, $order["order_delivery_confirmation_by"]);


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("View Retail Data");
$form->render();
$page->footer();

?>