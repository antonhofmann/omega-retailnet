<?php
/********************************************************************

    orders_archive_orders.php

    Entry page for the orders section.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-11-07
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-12-03
    Version:        1.0.5

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("has_access_to_archive");

register_param("y1");
register_param("y2");
register_param("c");
register_param("on");


/********************************************************************
    prepare all data needed
*********************************************************************/
if (!param("y1"))
{
    $y1 = 0;
}
else
{
	$y1 = param("y1");
}
if (!param("y2"))
{
    $y2 = 0;
}
else
{
	$y2 = param("y2");
}

if (!param("c"))
{
    $c = 0;
}
else
{
	$c = param("c");
}

if (!param("on"))
{
    $on = 0;
}
else
{
	$on = param("on");
}

// get user_data
$user_data = get_user(user_id());

// create list filter
$condition = get_user_specific_order_list(user_id(), 2);

// create filter
$filter = "";

if($y1 > 0)
{
	$filter =  "   and left(order_date,4) >= " . $y1;
}
if($y2 > 0)
{
	$filter .=   "   and left(order_date,4) <= " . $y2;
}
if($c > 0)
{
	$filter .=   "   and country_id = " . $c;
}
if($on != '')
{
	$filter .=   "   and order_number = " . dbquote($on);
}


// create sql for the list of orders
if (param("showall"))
{
    $sql = "select distinct order_id, order_number, order_actual_order_state_code, left(orders.date_created, 10), " .
           "   concat(user_name,' ',user_firstname), " . 
           "   concat(order_shop_address_place,', ', order_shop_address_company), " .
           "   country_name ".
           "from orders " .
		   "left join addresses on address_id = order_client_address " . 
           "left join countries on address_country = countries.country_id ".
           "left join users on order_retail_operator = users.user_id ";
}
else
{
    $sql = "select distinct order_id, order_number, order_actual_order_state_code, left(orders.date_created, 10), " .
           "   concat(user_name,' ',user_firstname), " . 
           "   concat(order_shop_address_place,', ', order_shop_address_company), " .
           "   country_name ".
           "from orders " .
           "left join order_items on order_item_order = order_id ".
           "left join addresses on address_id = order_client_address " . 
           "left join countries on address_country = countries.country_id ".
           "left join users on order_retail_operator = users.user_id ";
}

// create sql for clients
if (has_access("has_access_to_all_orders"))
{
    $sql_clients = "select distinct concat(order_shop_address_place,', ', order_shop_address_company) ".
                   "from orders ".
                   "left join order_addresses on order_address_order = order_id ".
		           "left join countries on order_shop_address_country = country_id ".
                   "where order_archive_date is not null ".
                   "   and order_archive_date <> '0000-00-00' ".
                   $filter . 
                   "   and  order_type = 2 ".
                   " order by order_shop_address_place";
}
else
{
    if ($condition == "")
    {
		$sql_clients = "select distinct concat(order_shop_address_place,', ', order_shop_address_company) ".
					   "from orders ".
					   "left join order_items on order_item_order = order_id ".
			           "left join countries on order_shop_address_country = country_id ".
					   "where order_archive_date is not null ".
					   "   and order_archive_date <> '0000-00-00' ".
					   $filter .
					   "   and  order_type = 2 ".
					   "   and (order_item_supplier_address = " . $user_data["address"] . " " .
					   "   or order_item_forwarder_address = " . $user_data["address"] . " " .
					   "   or order_retail_operator = " . user_id() . " " .
					   "   or order_retail_operator is null " . 
					   "   or order_client_address = " . $user_data["address"] . ") " .   
					   " order by order_shop_address_place";
	}
	else
	{
		$sql_clients = "select distinct concat(order_shop_address_place,', ', order_shop_address_company) ".
					   "from orders ".
					   "left join order_items on order_item_order = order_id ".
			           "left join countries on order_shop_address_country = country_id ".
					   "where order_archive_date is not null ".
					   "   and order_archive_date <> '0000-00-00' ".
			           "   and (" . $condition . ")" .
					   $filter .
			           "   and  order_type = 2 ".
					   " order by order_shop_address_place";
	}
}


// create sql for retail_operators
if (has_access("has_access_to_all_orders"))
{
    $sql_retail_operators = "select distinct concat(user_name, ' ', user_firstname) ".
                            "from orders ".
                            "left join users on  order_retail_operator = user_id ".
		                    "left join countries on order_shop_address_country = country_id ".
                            "where user_name <>'' ".
                            "   and  order_type = 2 ".
                            "   and order_archive_date is not null ".
                            "   and order_archive_date <> '0000-00-00' ".
                            $filter .
                            " order by user_name";
}
else
{
    if ($condition == "")
    {
		$sql_retail_operators = "select distinct concat(user_name, ' ', user_firstname) ".
                        "from orders ".
                        "left join users on  order_retail_operator = user_id ".
                        "left join order_items on order_item_order = order_id ".
			            "left join countries on order_shop_address_country = country_id ".
                        "where order_archive_date is not null ".
                        "   and order_archive_date <> '0000-00-00' ".
                        $filter .
                        "   and  order_type = 2 ".
                        "   and (order_item_supplier_address = " . $user_data["address"] . " " .
                        "   or order_item_forwarder_address = " . $user_data["address"] . " " .
                        "   or order_retail_operator = " . user_id() . " " .
                        "   or order_retail_operator is null " . 
                        "   or order_client_address = " . $user_data["address"] . ") " .   
                        " order by user_name";
	}
	else
	{
		$sql_retail_operators = "select distinct concat(user_name, ' ', user_firstname) ".
                        "from orders ".
                        "left join users on  order_retail_operator = user_id ".
                        "left join order_items on order_item_order = order_id ".
			            "left join countries on order_shop_address_country = country_id ".
                        "where order_archive_date is not null ".
                        "   and order_archive_date <> '0000-00-00' ".
			            "   and (" . $condition . ")" .
                        $filter .
			            "   and  order_type = 2 ".
                        " order by user_name";
	}
}

// create sql for country list
if (has_access("has_access_to_all_orders"))
{
    $sql_countries = "select distinct country_name ".
                     "from orders ".
                     "left join countries on order_shop_address_country = country_id ".
                     "where order_archive_date is not null ".
                     "   and order_archive_date <> '0000-00-00' ".
                     $filter .
                     "   and  order_type = 2 ".
                     " order by country_name";
}
else
{
    if ($condition == "")
    {
		$sql_countries = "select distinct country_name ".
						 "from orders ".
						 "left join countries on order_shop_address_country = country_id ".
						 "left join order_items on order_item_order = order_id ".
						 "where order_archive_date is not null ".
						 "   and order_archive_date <> '0000-00-00' ".
						 $filter .
						 "   and  order_type = 2 ".
						 "   and (order_item_supplier_address = " . $user_data["address"] . " " .
						 "   or order_item_forwarder_address = " . $user_data["address"] . " " .
						 "   or order_retail_operator = " . user_id() . " " .
						 "   or order_retail_operator is null " . 
						 "   or order_client_address = " . $user_data["address"] . ") " .   
						 " order by country_name";
	}
	else
	{
		$sql_countries = "select distinct country_name ".
						 "from orders ".
						 "left join countries on order_shop_address_country = country_id ".
						 "left join order_items on order_item_order = order_id ".
						 "where order_archive_date is not null ".
						 "   and order_archive_date <> '0000-00-00' ".
						 "   and (" . $condition . ")" .
						 $filter .
			             "   and  order_type = 2 ".
						 " order by country_name";
	}

}

if (has_access("has_access_to_all_orders"))
{
    $list_filter = "order_archive_date is not null ".
                   "   and order_archive_date <> '0000-00-00' ".
                   $filter .
                   "   and order_type = 2";
}
elseif(has_access("has_access_to_all_orders_of_his_country"))
{
	if ($condition == "")
    {
		if (has_access("can_view_order_before_budget_approval_in_orders"))
		{   
			if (has_access("can_view_empty_orders"))
			{ 
				$list_filter = "order_archive_date is not null ".
							   "   and order_archive_date <> '0000-00-00' " .
							   " and country_id = " . $user_data["country"] . 
							   $filter .
							   "   and  order_type = 2";
			}
			else
			{
				$list_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
							   "   and order_archive_date is not null ".
							   "   and order_archive_date <> '0000-00-00' ".
							   " and country_id = " . $user_data["country"] . 
							   $filter .
							   "   and  order_type = 2";
			}
		}
		else
		{   
			 if (has_access("can_view_empty_orders"))
			 {
				 $list_filter = "  order_show_in_delivery = 1 ".
								"   and order_archive_date is not null ".
								"   and order_archive_date <> '0000-00-00' ".
								" and country_id = " . $user_data["country"] . 
								$filter .
								"   and  order_type = 2";
			 }
			 else
			 {
				 $list_filter = "  order_show_in_delivery = 1 ".
								"   and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
								"   and order_archive_date is not null ".
								"   and order_archive_date <> '0000-00-00' " .
								" and country_id = " . $user_data["country"] . 
								$filter .
								"   and  order_type = 2";
			 }
		}
	}
	else
	{
		if (has_access("can_view_order_before_budget_approval_in_orders"))
		{   
			if (has_access("can_view_empty_orders"))
			{ 
				$list_filter = " order_archive_date is not null ".
							   "   and order_archive_date <> '0000-00-00' " .
							   "   and (" . $condition . ")" .
							   $filter .
							   "   and  order_type = 2";
			}
			else
			{
				$list_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
							   "   and order_archive_date is not null ".
							   "   and order_archive_date <> '0000-00-00' ".
							   "   and (" . $condition . ")" .
							   $filter .
							   "   and  order_type = 2";
			}
		}
		else
		{   
			 if (has_access("can_view_empty_orders"))
			 {
				 $list_filter = "  order_show_in_delivery = 1 ".
								"   and order_archive_date is not null ".
								"   and order_archive_date <> '0000-00-00' ".
								"   and (" . $condition . ")" .
								$filter .
								"   and  order_type = 2";
			 }
			 else
			 {
				 $list_filter = "  order_show_in_delivery = 1 ".
								"   and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
								"   and order_archive_date is not null ".
								"   and order_archive_date <> '0000-00-00' " .
								"   and (" . $condition . ")" .
								$filter .
								"   and  order_type = 2";
			 }
		}
	}
}
else
{
    if ($condition == "")
    {
        if (has_access("can_view_order_before_budget_approval_in_orders"))
        {   
            if (has_access("can_view_empty_orders"))
            { 
                $list_filter = " order_archive_date is not null ".
                               "   and order_archive_date <> '0000-00-00' ".
                               $filter .
                               "   and  order_type = 2 ".
                               "   and (order_item_supplier_address = " . $user_data["address"] . " " .
                               "   or order_item_forwarder_address = " . $user_data["address"] . " " .
                               "   or order_retail_operator = " . user_id() . " " .
                               "   or order_client_address = " . $user_data["address"] . ")";   
            }
            else
            {
                $list_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
                               "   and order_archive_date is not null ".
                               "   and order_archive_date <> '0000-00-00' ".
                               $filter .
                               "   and  order_type = 2 ".
                               "   and (order_item_supplier_address = " . $user_data["address"] . " " .
                               "   or order_item_forwarder_address = " . $user_data["address"] . " " .
                               "   or order_retail_operator = " . user_id() . " " .
                               "   or order_client_address = " . $user_data["address"] . ")";   
            }
        }
        else
        {   
             if (has_access("can_view_empty_orders"))
             {
                 $list_filter = "  order_show_in_delivery = 1 ".
                                "   and order_archive_date is not null ".
                                "   and order_archive_date <> '0000-00-00' ".
                                $filter .
                                "   and  order_type = 2 ".
                                "   and (order_item_supplier_address = " . $user_data["address"] . " " .
                                "   or order_item_forwarder_address = " . $user_data["address"] . " " .
                                "   or order_retail_operator = " . user_id() . " " .
                                "   or order_client_address = " . $user_data["address"] . ")";
             }
             else
             {
                 $list_filter = "  order_show_in_delivery = 1 ".
                                "   and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
                                "   and order_archive_date is not null ".
                                "   and order_archive_date <> '0000-00-00' ".
                                $filter .
                                "   and  order_type = 2 ".
                                "   and (order_item_supplier_address = " . $user_data["address"] . " " .
                                "   or order_item_forwarder_address = " . $user_data["address"] . " " .
                                "   or order_retail_operator = " . user_id() . " " .
                                "   or order_client_address = " . $user_data["address"] . ")";
             
             }
        }
    }
    else
    {
        if (has_access("can_view_order_before_budget_approval_in_orders"))
        {   
            if (has_access("can_view_empty_orders"))
            { 
                $list_filter = " order_archive_date is not null ".
                               "   and order_archive_date <> '0000-00-00' ".
                               $filter .
                               "   and  order_type = 2 " .
                               "   and (" . $condition . ")";
            }
            else
            {
                $list_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
                               "   and order_archive_date is not null ".
                               "   and order_archive_date <> '0000-00-00' ".
                               $filter .
                               "   and  order_type = 2 " .
                               "   and (" . $condition . ")";
            }
        }
        else
        {   
             if (has_access("can_view_empty_orders"))
             {
                 $list_filter = "  order_show_in_delivery = 1 ".
                                "   and order_archive_date is not null ".
                                "   and order_archive_date <> '0000-00-00' ".
                                $filter .
                                "   and  order_type = 2 ".
                                "   and (" . $condition . ")";
             }
             else
             {
                 $list_filter = "  order_show_in_delivery = 1 ".
                                "   and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
                                "   and order_archive_date is not null ".
                                "   and order_archive_date <> '0000-00-00' ".
                                $filter .
                                "   and  order_type = 2 " .
                                "   and (" . $condition . ")";
             }
        }
    }
}



//count orders

$sql_count = "select count(distinct order_id) as num_recs ".
			   "from orders " .
			   "left join order_items on order_item_order = order_id " .
			   "left join countries on order_shop_address_country = countries.country_id ".
			   "left join users on order_retail_operator = users.user_id ";

$sql_count = $sql_count . " where " . $list_filter;
$res = mysql_query($sql_count);
$row = mysql_fetch_assoc($res);



if($row["num_recs"] == 1) {
	
	
	$sql_count = "select distinct order_id ".
           "from orders " .
           "left join countries on order_shop_address_country = countries.country_id ".
		    "left join order_items on order_item_order = order_id ".
           "left join users on order_retail_operator = users.user_id ";

	$sql_count = $sql_count . " where " . $list_filter;
	$res = mysql_query($sql_count);
	$row = mysql_fetch_assoc($res);


	$link = "order_task_center.php?oid=" . $row["order_id"]. "&y1=" .param("y1") . "&y2=" . param("y2") . "&c=" . param("c") . "&on=" . param("on");
	redirect($link);
}

/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);

$list->set_entity("orders in the records");
$list->set_order("orders.date_created desc");
$list->set_filter($list_filter);

$list->add_hidden("y1", param("y1"));
$list->add_hidden("y2", param("y2"));
$list->add_hidden("c", param("c"));
$list->add_hidden("on", param("on"));

$link = "order_task_center.php?oid={order_id}&y1=" .param("y1") . "&y2=" . param("y2") . "&c=" . param("c") . "&on=" . param("on");
$list->add_column("order_number", "Order Number", $link, "", "", COLUMN_NO_WRAP);
$list->add_column("order_actual_order_state_code", "Status");
$list->add_column("left(orders.date_created, 10)", "Submitted", "", "", "", COLUMN_NO_WRAP);
$list->add_column("country_name", "Country", "", LIST_FILTER_LIST, $sql_countries);
$list->add_column("concat(order_shop_address_place,', ', order_shop_address_company)", "Shop Address", "", LIST_FILTER_LIST, $sql_clients);
$list->add_column("concat(user_name,' ',user_firstname)", "Retail Operator", "", LIST_FILTER_LIST, $sql_retail_operators);

$list->add_button(LIST_BUTTON_FILTER, "Filter");
$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove Filter");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->process();


$page = new Page("orders");

$page->register_action('orders', 'Orders', "orders_archive.php");
$page->register_action('projects', 'Projects', "projects_archive.php");
$page->register_action('welcome', 'Quit Archive', "../user/welcome.php");

$page->header();
$page->title('Orders: Archive');
$list->render();
$page->footer();

?>