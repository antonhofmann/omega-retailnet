<?php
/********************************************************************

    project_cost_functions.php

    Various functions in the context of project_costs

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-01-09
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2012, OMEGA SA, All Rights Reserved.

*********************************************************************/


/********************************************************************
    get currency informations assigned to an order
*********************************************************************/
function get_client_currency($order_id)
{
    $currency = array();

    $sql = "select * from orders where order_id = " . $order_id;
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $currency_id = $row["order_client_currency"];
        $currency["exchange_rate"] = $row["order_client_exchange_rate"];
    }


    if ($currency_id > 0)
    {
        $sql = "select * from currencies where currency_id = " . $currency_id;
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $currency["id"] = $currency_id;
            $currency["symbol"] = $row["currency_symbol"];
            $currency["factor"] = $row["currency_factor"];
        }
    }
    else
    {
        $currency["id"] = 0;
        $currency["symbol"] = "";
        $currency["exchange_rate"] = 1;
        $currency["factor"] = 1;
    }
    
    return $currency;
}


/********************************************************************
    get currency informations assigned to an order
*********************************************************************/
function get_currency_from_cer($project_id)
{
    $currency = array();

    $sql = "select * from cer_basicdata where cer_basicdata_project = " . $project_id;
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $currency_id = $row["cer_basicdata_currency"];
        $currency["exchange_rate"] = $row["cer_basicdata_exchangerate"];
    }


    if ($currency_id > 0)
    {
        $sql = "select * from currencies where currency_id = " . $currency_id;
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $currency["id"] = $currency_id;
            $currency["symbol"] = $row["currency_symbol"];
            $currency["factor"] = $row["currency_factor"];
        }
    }
    else
    {
        $currency["id"] = 0;
        $currency["symbol"] = "";
        $currency["exchange_rate"] = 1;
        $currency["factor"] = 1;
    }
    
    return $currency;
}


/********************************************************************
    get_cost_groups
*********************************************************************/
function get_cost_groups()
{
	$costgroups = array();
	$sql = "select pcost_group_id, " . 
		   "concat(pcost_group_code, ' ', pcost_group_name) as costgroup " .
		   "from pcost_groups " .
		   "order by pcost_group_code ";

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$costgroups[$row["pcost_group_id"]] = $row["costgroup"];
	}

	return $costgroups;
}

/********************************************************************
    get_cost_groups
*********************************************************************/
function get_cost_sheet_cost_groups($project_id = 0)
{
	$costgroups = array();
	
	$sql = "select DISTINCT pcost_group_id, " .
		   "concat(pcost_group_code, ' ', pcost_group_name) as costgroup " .
			"from costsheets " .
			"left join pcost_groups on pcost_group_id = costsheet_pcost_group_id " .
			" where costsheet_project_id = " . $project_id. " and costsheet_is_in_cms = 1";
			"order by pcost_group_code";

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$costgroups[$row["pcost_group_id"]] = $row["costgroup"];
	}

	return $costgroups;
}



/********************************************************************
    create cost sheet for a project
*********************************************************************/
function create_cost_sheet($project_id = 0 , $template_id = 0 , $from_project_id = 0)
{
	$cost_sheet_created = false;
	if($template_id > 0) // create costsheet from templates
	{
		$sql = "select * from pcost_positions " .
			   "left join pcost_templates on pcost_template_id = pcost_position_pcost_template_id " . 
			   " where pcost_position_pcost_template_id = " . dbquote($template_id);


		$res = mysql_query($sql) or dberror($sql);
		while($row = mysql_fetch_assoc($res))
		{
			 $fields = array();
			 $values = array();

			 $fields[] = "costsheet_project_id";
			 $values[] = dbquote($project_id);

			 $fields[] = "costsheet_pcost_group_id";
			 $values[] = dbquote($row["pcost_position_pcostgroup_id"]);

			 $fields[] = "costsheet_pcost_subgroup_id";
			 $values[] = dbquote($row["pcost_position_pcost_subgroup_id"]);

			 $fields[] = "costsheet_code";
			 $values[] = dbquote($row["pcost_position_code"]);

			 $fields[] = "costsheet_text";
			 $values[] = dbquote($row["pcost_position_text"]);

			 $fields[] = "costsheet_is_in_budget";
			 $values[] = 1;

			 $fields[] = "date_created";
			 $values[] = dbquote(date("Y-m-d H:i:s"));

			 $fields[] = "user_created";
			 $values[] = dbquote(user_login());

			 $sql = "insert into costsheets (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

			 $result = mysql_query($sql) or dberror($sql);

			 $cost_sheet_created = true;
		}

	}
	elseif($from_project_id > 0) // create template from existing project
	{
		
		$sql = "select * from costsheets " .
			   " where costsheet_project_id = " . dbquote($from_project_id);

		$res = mysql_query($sql) or dberror($sql);
		while($row = mysql_fetch_assoc($res))
		{
			 $fields = array();
			 $values = array();

			 $fields[] = "costsheet_project_id";
			 $values[] = dbquote($project_id);

			 $fields[] = "costsheet_pcost_group_id";
			 $values[] = dbquote($row["costsheet_pcost_group_id"]);

			 $fields[] = "costsheet_pcost_subgroup_id";
			 $values[] = dbquote($row["costsheet_pcost_subgroup_id"]);

			 $fields[] = "costsheet_code";
			 $values[] = dbquote($row["costsheet_code"]);

			 $fields[] = "costsheet_text";
			 $values[] = dbquote($row["costsheet_text"]);

			 $fields[] = "costsheet_is_in_budget";
			 $values[] = 1;

			 $fields[] = "date_created";
			 $values[] = dbquote(date("Y-m-d H:i:s"));

			 $fields[] = "user_created";
			 $values[] = dbquote(user_login());

			 $sql = "insert into costsheets (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

			 $result = mysql_query($sql) or dberror($sql);

			 $cost_sheet_created = true;
		}
		

	}

	if($cost_sheet_created == false) //empty template;
	{
		$old_sub_group = '';
		$sql = "select * from pcost_groups " .
			   "left join pcost_subgroups on pcost_subgroup_pcostgroup_id = pcost_group_id " . 
			   "order by pcost_group_code, pcost_subgroup_code";

		$res = mysql_query($sql) or dberror($sql);
		while($row = mysql_fetch_assoc($res))
		{
			 $fields = array();
			 $values = array();

			 $fields[] = "costsheet_project_id";
			 $values[] = dbquote($project_id);

			 $fields[] = "costsheet_pcost_group_id";
			 $values[] = dbquote($row["pcost_group_id"]);

			 $fields[] = "costsheet_pcost_subgroup_id";
			 $values[] = dbquote($row["pcost_subgroup_id"]);

			 
			 if($old_sub_group != $row["pcost_subgroup_code"])
			 {
				 $i = 1;
				 $old_sub_group = $row["pcost_subgroup_code"];

			 }
			 if($i < 10){$tmp = '0' . $i;} else {$tmp = $i;}
			 $code = $row["pcost_subgroup_code"] . '.' . $tmp;
			 $i++;

			 
			 $fields[] = "costsheet_code";
			 $values[] = dbquote($code);
			 
			 $fields[] = "costsheet_is_in_budget";
			 $values[] = 1;

			 
			 $fields[] = "date_created";
			 $values[] = dbquote(date("Y-m-d H:i:s"));

			 $fields[] = "user_created";
			 $values[] = dbquote(user_login());

			 $sql = "insert into costsheets (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

			 $result = mysql_query($sql) or dberror($sql);

			 $cost_sheet_created = true;
		}
	}

	//check if there is a positions for every cost group
	$sql = "select * from pcost_groups " .
		   "order by pcost_group_code";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		 
		$sql_c = "select count(costsheet_pcost_group_id) as num_recs " . 
				 "from costsheets " . 
				 "where costsheet_project_id = " . dbquote($project_id) . 
				 " and costsheet_pcost_group_id = " . $row["pcost_group_id"];
		
		$res_c = mysql_query($sql_c) or dberror($sql_c);
		$row_c = mysql_fetch_assoc($res_c);

		if($row_c["num_recs"] == 0)
		{

		 $fields = array();
		 $values = array();

		 $fields[] = "costsheet_project_id";
		 $values[] = dbquote($project_id);

		 $fields[] = "costsheet_pcost_group_id";
		 $values[] = dbquote($row["pcost_group_id"]);

		 $fields[] = "costsheet_pcost_subgroup_id";
		 $values[] = dbquote($row["pcost_subgroup_id"]);

		 $fields[] = "costsheet_is_in_budget";
		 $values[] = 1;

		 
		 $fields[] = "date_created";
		 $values[] = dbquote(date("Y-m-d H:i:s"));

		 $fields[] = "user_created";
		 $values[] = dbquote(user_login());

		 $sql = "insert into costsheets (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

		 $result = mysql_query($sql) or dberror($sql);
		}

	}

	return true;
}



/********************************************************************
    create positions for a bid from costsheet
*********************************************************************/
function create_bid_positions($bid_id = 0, $project_id = 0, $selected_costgroups = array())
{
	
	$filter = " and costsheet_pcost_group_id IN (" . implode(',', $selected_costgroups) . ")";
	
	//add positions from costsheet to table costsheet_bid_positions
	$sql = "select * " .
		   "from costsheets " .
		   "where costsheet_project_id = " . param("pid") . " and costsheet_is_in_budget = 1 " . 
		   $filter;
	

	
	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		//check if record is there
		$sql_p = "select costsheet_bid_position_id " . 
				 "from costsheet_bid_positions " . 
				 "where costsheet_bid_position_costsheet_id = " . $row["costsheet_id"] . 
			     " and costsheet_bid_position_costsheet_bid_id = " . $bid_id;

		$res_p = mysql_query($sql_p) or dberror($sql_p);
		if($row_p = mysql_fetch_assoc($res_p))
		{
			
		}
		else
		{
			 $fields = array();
			 $values = array();

			 $fields[] = "costsheet_bid_position_costsheet_bid_id";
			 $values[] = dbquote($bid_id);

			 $fields[] = "costsheet_bid_position_project_id";
			 $values[] = dbquote($project_id);

			 $fields[] = "costsheet_bid_position_costsheet_id";
			 $values[] = dbquote($row["costsheet_id"]);

			 $fields[] = "costsheet_bid_position_pcost_group_id";
			 $values[] = dbquote($row["costsheet_pcost_group_id"]);

			 $fields[] = "costsheet_bid_position_pcost_subgroup_id";
			 $values[] = dbquote($row["costsheet_pcost_subgroup_id"]);

			 $fields[] = "costsheet_bid_position_code";
			 $values[] = dbquote($row["costsheet_code"]);

			 $fields[] = "costsheet_bid_position_text";
			 $values[] = dbquote($row["costsheet_text"]);

			 $fields[] = "date_created";
			 $values[] = dbquote(date("Y-m-d H:i:s"));

			 $fields[] = "user_created";
			 $values[] = dbquote(user_login());

			 $sql = "insert into costsheet_bid_positions (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

			 $result = mysql_query($sql) or dberror($sql);
		}

	}
	return true;
}

/********************************************************************
    get totals from budget
*********************************************************************/
function get_project_budget_totals($project_id, $currency = array())
{
	$budget = array();


	//get budget_total
	$budget_group_totals = array();
	$budget_group_totals_formated = array();
	$budget_sub_group_totals = array();
	$budget_sub_group_totals_by_id = array();
	$budget_total = 0;

	$budget_group_totals_chf = array();
	$budget_group_totals_chf_formated = array();
	$budget_sub_group_totals_chf = array();
	$budget_sub_group_totals_chf_by_id = array();
	$budget_total_chf = 0;

	$approved_budget_group_totals = array();
	$approved_budget_group_totals_formated = array();
	$approved_budget_sub_group_totals = array();
	$approved_budget_sub_group_totals_by_id = array();
	$approved_budget_total = 0;

	$sg_approved_budget_group_totals = array();
	$sg_approved_budget_group_totals_formated = array();
	$sg_approved_budget_sub_group_totals = array();
	$sg_approved_budget_sub_group_totals_by_id = array();
	$sg_approved_budget_total = 0;

	$real_group_totals = array();
	$real_group_totals_formated = array();
	$real_sub_group_totals = array();
	$real_sub_group_totals_by_id = array();
	$real_total = 0;

	$difference_group_totals = array();
	$difference_group_totals_formated = array();
	$difference_sub_group_totals = array();
	$difference_sub_group_totals_by_id = array();
	$difference_total = 0;

	$difference_percent_group_totals = array();
	$difference_percent_group_totals_formated = array();
	$difference_percent_sub_group_totals = array();
	$difference_percent_sub_group_totals_by_id = array();
	$difference_percent_total = 0;


	$sql_b = "select costsheet_pcost_group_id, costsheet_pcost_subgroup_id, " .
			 "IF(costsheet_pcost_subgroup_id > 0,  concat(pcost_subgroup_code, ' ', pcost_subgroup_name), pcost_group_name) as subgroup,  " .
			 "sum(costsheet_budget_amount) as budget_total, " .
		     "sum(costsheet_budget_approved_amount) as approved_budget_total, " .
		     "sum(costsheet_sg_approved_amount) as sg_approved_budget_total, " .
		     "sum(costsheet_real_amount) as real_total " .
			 "from costsheets " .
			 "left join pcost_groups on pcost_group_id = costsheet_pcost_group_id " . 
			 "left join pcost_subgroups on pcost_subgroup_id = costsheet_pcost_subgroup_id " .  
			 "where costsheet_project_id = " . $project_id . 
			 " group by costsheet_pcost_group_id, costsheet_pcost_subgroup_id";

	$res_b = mysql_query($sql_b) or dberror($sql_b);
	while($row_b = mysql_fetch_assoc($res_b))
	{
		//budget
		$budget_sub_group_totals[$row_b["subgroup"]] = $row_b["budget_total"];
		$budget_total = $budget_total + $row_b["budget_total"];
		$budget_sub_group_totals_by_id[$row_b["costsheet_pcost_group_id"]][$row_b["costsheet_pcost_subgroup_id"]] = $row_b["budget_total"];

		if(count($currency) > 0 and $currency["factor"] > 0)
		{
			
			$budget_sub_group_totals_chf[$row_b["subgroup"]] = ($currency["exchange_rate"] * $row_b["budget_total"])/$currency["factor"];
			$budget_total_chf = $budget_total_chf + ($currency["exchange_rate"] * $row_b["budget_total"])/$currency["factor"];
			$budget_sub_group_totals_chf_by_id[$row_b["costsheet_pcost_group_id"]][$row_b["costsheet_pcost_subgroup_id"]] = ($currency["exchange_rate"] * $row_b["budget_total"])/$currency["factor"];
		}
		else
		{
			$budget_sub_group_totals_chf[$row_b["subgroup"]] = 0;
			$budget_total_chf = 0;
			$budget_sub_group_totals_chf_by_id[$row_b["costsheet_pcost_group_id"]][$row_b["costsheet_pcost_subgroup_id"]] = 0;
		}

	
		if(array_key_exists($row_b["costsheet_pcost_group_id"], $budget_group_totals))
		{
			$budget_group_totals[$row_b["costsheet_pcost_group_id"]] = $budget_group_totals[$row_b["costsheet_pcost_group_id"]] + $row_b["budget_total"];

			if(count($currency) > 0 and $currency["factor"] > 0)
			{
				$budget_group_totals_chf[$row_b["costsheet_pcost_group_id"]] = $budget_group_totals_chf[$row_b["costsheet_pcost_group_id"]] + ($currency["exchange_rate"] * $row_b["budget_total"])/$currency["factor"];
			}
			else
			{
				$budget_group_totals_chf[$row_b["costsheet_pcost_group_id"]] = 0;
			}
		}
		else
		{
			$budget_group_totals[$row_b["costsheet_pcost_group_id"]] = $row_b["budget_total"];
			
			if(count($currency) > 0 and $currency["factor"] > 0)
			{
				$budget_group_totals_chf[$row_b["costsheet_pcost_group_id"]] = ($currency["exchange_rate"] * $row_b["budget_total"])/$currency["factor"];
			}
			else
			{
				$budget_group_totals_chf[$row_b["costsheet_pcost_group_id"]] = 0;
			}
		}

		$budget_group_totals_formated[$row_b["costsheet_pcost_group_id"]] = number_format($budget_group_totals[$row_b["costsheet_pcost_group_id"]], 2);

		$budget_group_totals_chf_formated[$row_b["costsheet_pcost_group_id"]] = number_format($budget_group_totals_chf[$row_b["costsheet_pcost_group_id"]], 2);


		
		//approved budget
		$approved_budget_sub_group_totals[$row_b["subgroup"]] = $row_b["approved_budget_total"];
		$approved_budget_total = $approved_budget_total + $row_b["approved_budget_total"];
		$approved_budget_sub_group_totals_by_id[$row_b["costsheet_pcost_group_id"]][$row_b["costsheet_pcost_subgroup_id"]] = $row_b["approved_budget_total"];

				
		if(array_key_exists($row_b["costsheet_pcost_group_id"], $approved_budget_group_totals))
		{
			$approved_budget_group_totals[$row_b["costsheet_pcost_group_id"]] = $approved_budget_group_totals[$row_b["costsheet_pcost_group_id"]] + $row_b["approved_budget_total"];
		}
		else
		{
			$approved_budget_group_totals[$row_b["costsheet_pcost_group_id"]] = $row_b["approved_budget_total"];
		}
		
		$approved_budget_group_totals_formated[$row_b["costsheet_pcost_group_id"]] = number_format($approved_budget_group_totals[$row_b["costsheet_pcost_group_id"]], 2);



		//sa approved budget
		$sg_approved_budget_sub_group_totals[$row_b["subgroup"]] = $row_b["sg_approved_budget_total"];
		$sg_approved_budget_total = $sg_approved_budget_total + $row_b["sg_approved_budget_total"];
		$sg_approved_budget_sub_group_totals_by_id[$row_b["costsheet_pcost_group_id"]][$row_b["costsheet_pcost_subgroup_id"]] = $row_b["sg_approved_budget_total"];

				
		if(array_key_exists($row_b["costsheet_pcost_group_id"], $sg_approved_budget_group_totals))
		{
			$sg_approved_budget_group_totals[$row_b["costsheet_pcost_group_id"]] = $sg_approved_budget_group_totals[$row_b["costsheet_pcost_group_id"]] + $row_b["sg_approved_budget_total"];
		}
		else
		{
			$sg_approved_budget_group_totals[$row_b["costsheet_pcost_group_id"]] = $row_b["sg_approved_budget_total"];
		}
		
		$sg_approved_budget_group_totals_formated[$row_b["costsheet_pcost_group_id"]] = number_format($sg_approved_budget_group_totals[$row_b["costsheet_pcost_group_id"]], 2);



		//approved budget
		$real_sub_group_totals[$row_b["subgroup"]] = $row_b["real_total"];
		$real_total = $real_total + $row_b["real_total"];
		$real_sub_group_totals_by_id[$row_b["costsheet_pcost_group_id"]][$row_b["costsheet_pcost_subgroup_id"]] = $row_b["real_total"];
		
		if(array_key_exists($row_b["costsheet_pcost_group_id"], $real_group_totals))
		{
			$real_group_totals[$row_b["costsheet_pcost_group_id"]] = $real_group_totals[$row_b["costsheet_pcost_group_id"]] + $row_b["real_total"];
		}
		else
		{
			$real_group_totals[$row_b["costsheet_pcost_group_id"]] = $row_b["real_total"];
		}

		$real_group_totals_formated[$row_b["costsheet_pcost_group_id"]] = number_format($real_group_totals[$row_b["costsheet_pcost_group_id"]], 2);
	}


	$budget["subgroup_totals_by_id"] = $budget_sub_group_totals_by_id;
	$budget["subgroup_totals"] = $budget_sub_group_totals;
	$budget["group_totals"] = $budget_group_totals;
	$budget["group_totals_formated"] = $budget_group_totals_formated;
	$budget["budget_total"] = $budget_total;

	$budget["subgroup_totals_chf_by_id"] = $budget_sub_group_totals_chf_by_id;
	$budget["subgroup_totals_chf"] = $budget_sub_group_totals_chf;
	$budget["group_totals_chf"] = $budget_group_totals_chf;
	$budget["group_totals_chf_formated"] = $budget_group_totals_chf_formated;
	$budget["budget_total_chf"] = $budget_total_chf;


	$budget["subgroup_approved_totals_by_id"] = $approved_budget_sub_group_totals_by_id;
	$budget["subgroup_approved_totals"] = $approved_budget_sub_group_totals;
	$budget["group_approved_totals"] = $approved_budget_group_totals;
	$budget["group_approved_totals_formated"] = $approved_budget_group_totals_formated;
	$budget["approved_budget_total"] = $approved_budget_total;

	$budget["subgroup_sg_approved_totals_by_id"] = $sg_approved_budget_sub_group_totals_by_id;
	$budget["subgroup_sg_approved_totals"] = $sg_approved_budget_sub_group_totals;
	$budget["group_sg_approved_totals"] = $sg_approved_budget_group_totals;
	$budget["group_sg_approved_totals_formated"] = $sg_approved_budget_group_totals_formated;
	$budget["sg_approved_budget_total"] = $sg_approved_budget_total;


	/*
	foreach($budget["subgroup_approved_totals_by_id"] as $group_id=>$subgrouptotals)
	{
		echo $group_id . "<br />";
		foreach($subgrouptotals as $subgroup_id=>$total)
		{
			echo "-> " . $group_id . " " . $total . "<br />";
		}
	}
	*/

	$budget["subgroup_real_totals_by_id"] = $real_sub_group_totals_by_id;
	$budget["subgroup_real_totals"] = $real_sub_group_totals;
	$budget["group_real_totals"] = $real_group_totals;
	$budget["group_real_totals_formated"] = $real_group_totals_formated;
	$budget["real_total"] = $real_total;

	foreach($sg_approved_budget_group_totals as $id=>$approved)
	{
		$difference_group_totals[$id] = $real_group_totals[$id] - $sg_approved_budget_group_totals[$id];
		$difference_group_totals_formated[$id] = number_format($real_group_totals[$id] - $sg_approved_budget_group_totals[$id], 2);



		if($sg_approved_budget_group_totals[$id] > 0)
		 {
			$difference_percent_group_totals[$id] = -1 * (1- ($real_group_totals[$id] / $sg_approved_budget_group_totals[$id]));
		 }
		 elseif($sg_approved_budget_group_totals[$id] == 0)
		 {
			$difference_percent_group_totals[$id] = 1;
		 }
		 else
		 {
			$difference_percent_group_totals[$id] = 0;
		 }
		 
		 if($difference_percent_group_totals[$id] > 0)
		 {
			$difference_percent_group_totals_formated[$id] = '<span class="red">' . number_format(100*$difference_percent_group_totals[$id], 2)  . "%</span>";
		 }
		 else
		 {
			$difference_percent_group_totals_formated[$id] = number_format(100*$difference_percent_group_totals[$id], 2)  . "%"; 
		 }
	}
	
	$difference_total = $real_total - $sg_approved_budget_total;
	
	
	if($sg_approved_budget_total > 0)
	{
		$difference_percent_total = -1 * (1- ($real_total / $sg_approved_budget_total));
	}
	elseif($sg_approved_budget_total == 0)
	{
		$difference_percent_total = 1;
	}
	else
	{
		$difference_percent_total = 0;
	}


	
	$budget["difference_group_totals"] = $difference_group_totals;
	$budget["difference_group_totals_formated"] = $difference_group_totals_formated;
	$budget["difference_total"] = $difference_total;
	$budget["difference_percent_group_totals"] = $difference_percent_group_totals;
	$budget["difference_percent_group_totals_formated"] = $difference_percent_group_totals_formated;
	$budget["difference_percent_total"] = $difference_percent_total;



	//get budget_group_totals by posinvestment_type
	$budget_group_totals_by_posinvestment_type = array();
	$budget_group_totals_by_posinvestment_type_chf = array();
	$sql_b = "select pcost_group_posinvestment_type_id, " .
			 "sum(costsheet_budget_amount) as budget_total, " .
		     "sum(costsheet_budget_approved_amount) as approved_budget_total, " .
		     "sum(costsheet_sg_approved_amount) as sg_approved_budget_total, " .
		     "sum(costsheet_real_amount) as real_total " .
			 "from costsheets " .
			 "left join pcost_groups on pcost_group_id = costsheet_pcost_group_id " . 
			 "left join pcost_subgroups on pcost_subgroup_id = costsheet_pcost_subgroup_id " .  
			 "where costsheet_project_id = " . $project_id . 
			 " group by pcost_group_posinvestment_type_id";

	$res_b = mysql_query($sql_b) or dberror($sql_b);
	while($row_b = mysql_fetch_assoc($res_b))
	{
		$budget_group_totals_by_posinvestment_type[$row_b["pcost_group_posinvestment_type_id"]] = $row_b["budget_total"];
		
		if(count($currency) > 0 and $currency["factor"] > 0)
		{
			$budget_group_totals_by_posinvestment_type_chf[$row_b["pcost_group_posinvestment_type_id"]] = ($currency["exchange_rate"] * $row_b["budget_total"])/$currency["factor"];
		}
		else
		{
			$budget_group_totals_by_posinvestment_type_chf[$row_b["pcost_group_posinvestment_type_id"]] = 0;
		}

	}
	$budget["group_totals_by_posinvestment_type"] = $budget_group_totals_by_posinvestment_type;
	$budget["group_totals_by_posinvestment_type_chf"] = $budget_group_totals_by_posinvestment_type_chf;


	//get real cost_group_totals by posinvestment_type
	$real_group_totals_by_posinvestment_type = array();
	$real_group_totals_by_posinvestment_type_chf = array();
	$sql_b = "select pcost_group_posinvestment_type_id, " .
			 "sum(costsheet_budget_amount) as budget_total, " .
		     "sum(costsheet_budget_approved_amount) as approved_budget_total, " .
		     "sum(costsheet_sg_approved_amount) as sg_approved_budget_total, " .
		     "sum(costsheet_real_amount) as real_total " .
			 "from costsheets " .
			 "left join pcost_groups on pcost_group_id = costsheet_pcost_group_id " . 
			 "left join pcost_subgroups on pcost_subgroup_id = costsheet_pcost_subgroup_id " .  
			 "where costsheet_project_id = " . $project_id . 
			 " group by pcost_group_posinvestment_type_id";

	$res_b = mysql_query($sql_b) or dberror($sql_b);
	while($row_b = mysql_fetch_assoc($res_b))
	{
		$real_group_totals_by_posinvestment_type[$row_b["pcost_group_posinvestment_type_id"]] = $row_b["real_total"];
		
		if(count($currency) > 0 and $currency["factor"] > 0)
		{
			$real_group_totals_by_posinvestment_type_chf[$row_b["pcost_group_posinvestment_type_id"]] = ($currency["exchange_rate"] * $row_b["real_total"])/$currency["factor"];
		}
		else
		{
			$real_group_totals_by_posinvestment_type_chf[$row_b["pcost_group_posinvestment_type_id"]] = 0;
		}

	}
	$budget["group_real_totals_by_posinvestment_type"] = $real_group_totals_by_posinvestment_type;
	$budget["group_real_totals_by_posinvestment_type_chf"] = $real_group_totals_by_posinvestment_type_chf;

	
	
	return $budget;
}


/********************************************************************
    get totals from a bid
*********************************************************************/
function copy_approved_ammounts($project_id = 0)
{
	$sql = "update costsheets " . 
		   "set costsheet_sg_approved_amount = costsheet_budget_approved_amount " . 
		   " where costsheet_project_id = " . $project_id . 
		   " and costsheet_sg_approved_amount = 0";


	$res = mysql_query($sql) or dberror($sql);
	return true;
}

/********************************************************************
    get totals from a bid
*********************************************************************/
function get_project_bid_totals($bid_id, $currency = array())
{
	$bid_totals = array();


	//get budget_total
	$bid_group_totals = array();
	$bid_group_totals_formated = array();
	$bid_sub_group_totals = array();
	$bid_sub_group_totals_by_id = array();
	$bid_total = 0;
	$bid_total_in_budget = 0;

	


	$sql_b = "select costsheet_bid_position_pcost_group_id, costsheet_bid_position_pcost_subgroup_id, " .
			 "IF(costsheet_bid_position_pcost_subgroup_id > 0,  concat(pcost_subgroup_code, ' ', pcost_subgroup_name), pcost_group_name) as subgroup,  " .
			 "sum(costsheet_bid_position_amount) as bid_total " .
			 "from costsheet_bid_positions " .
			 "left join pcost_groups on pcost_group_id = costsheet_bid_position_pcost_group_id " . 
			 "left join pcost_subgroups on pcost_subgroup_id = costsheet_bid_position_pcost_subgroup_id " .  
			 "where costsheet_bid_position_costsheet_bid_id = " . $bid_id . 
			 " group by costsheet_bid_position_pcost_group_id, costsheet_bid_position_pcost_subgroup_id";

	$res_b = mysql_query($sql_b) or dberror($sql_b);
	while($row_b = mysql_fetch_assoc($res_b))
	{
		//budget
		$bid_sub_group_totals[$row_b["subgroup"]] = $row_b["bid_total"];
		$bid_total = $bid_total + $row_b["bid_total"];
		$bid_sub_group_totals_by_id[$row_b["costsheet_bid_position_pcost_group_id"]][$row_b["costsheet_bid_position_pcost_subgroup_id"]] = $row_b["bid_total"];
		
		if(array_key_exists($row_b["costsheet_bid_position_pcost_group_id"], $bid_group_totals))
		{
			$bid_group_totals[$row_b["costsheet_bid_position_pcost_group_id"]] = $bid_group_totals[$row_b["costsheet_bid_position_pcost_group_id"]] + $row_b["bid_total"];
		}
		else
		{
			$bid_group_totals[$row_b["costsheet_bid_position_pcost_group_id"]] = $row_b["bid_total"];
		}

		$bid_group_totals_formated[$row_b["costsheet_bid_position_pcost_group_id"]] = number_format($bid_group_totals[$row_b["costsheet_bid_position_pcost_group_id"]], 2);
		
		
	}


	$bid_totals["subgroup_totals_by_id"] = $bid_sub_group_totals_by_id;
	$bid_totals["subgroup_totals"] = $bid_sub_group_totals;
	$bid_totals["group_totals"] = $bid_group_totals;
	$bid_totals["group_totals_formated"] = $bid_group_totals_formated;
	$bid_totals["bid_total"] = $bid_total;


	//get bidtotal in budget
	$sql_b = "select costsheet_bid_position_amount as bid_total_in_budget " .
			 "from costsheet_bid_positions " .
			 "left join pcost_groups on pcost_group_id = costsheet_bid_position_pcost_group_id " . 
			 "left join pcost_subgroups on pcost_subgroup_id = costsheet_bid_position_pcost_subgroup_id " .  
			 "where costsheet_bid_position_is_in_budget = 1 and costsheet_bid_position_costsheet_bid_id = " . $bid_id;

	$res_b = mysql_query($sql_b) or dberror($sql_b);
	while($row_b = mysql_fetch_assoc($res_b))
	{
		$bid_total_in_budget = $bid_total_in_budget + $row_b["bid_total_in_budget"];
	}
	$bid_totals["bid_total_in_budget"] = $bid_total_in_budget;


	
	//calculate values in system currency
	$bid_sub_group_totals_by_id_chf = array();
	foreach($bid_totals["subgroup_totals_by_id"] as $key2=>$sub_group_total)
	{
		foreach($sub_group_total as $key=>$total)
		{
			if(count($currency) > 0 and $currency["factor"] > 0)
			{
				$bid_sub_group_totals_by_id_chf[$key] = ($currency["exchange_rate"] * $total)/$currency["factor"];
			}
			else
			{
				$bid_sub_group_totals_by_id_chf[$key] = 0;
			}
		}
	}
	$bid_totals["subgroup_totals_by_id_chf"] = $bid_sub_group_totals_by_id_chf;

	
	$bid_sub_group_totals_chf = array();
	foreach($bid_totals["subgroup_totals"] as $key=>$total)
	{
		if(count($currency) > 0 and $currency["factor"] > 0)
		{
			$bid_sub_group_totals_chf[$key] = ($currency["exchange_rate"] * $total)/$currency["factor"];
		}
		else
		{
			$bid_sub_group_totals_chf[$key] = 0;
		}
	}
	$bid_totals["subgroup_totals_by_id_chf"] = $bid_sub_group_totals_chf;

	
	$bid_group_totals_chf = array();
	foreach($bid_totals["group_totals"] as $key=>$total)
	{
		if(count($currency) > 0 and $currency["factor"] > 0)
		{
			$bid_group_totals_chf[$key] = ($currency["exchange_rate"] * $total)/$currency["factor"];
		}
		else
		{
			$bid_group_totals_chf[$key] = 0;
		}
	}
	$bid_totals["group_totals_chf"] = $bid_group_totals_chf;

	
	
	
	if(count($currency) > 0 and $currency["factor"] > 0)
	{
		$bid_totals["bid_total_chf"] = ($currency["exchange_rate"] * $bid_totals["bid_total"])/$currency["factor"];
	}
	else
	{
		$bid_totals["bid_total_chf"] = 0;
	}



	//get bids having positions in the budget
	$bids_in_budget = array();
	$sql_b = "select distinct costsheet_bid_position_costsheet_bid_id " . 
			 "from costsheet_bid_positions " .
			 " where costsheet_bid_position_is_in_budget = 1";

	$res_b = mysql_query($sql_b) or dberror($sql_b);
	while($row_b = mysql_fetch_assoc($res_b))
	{
		$bids_in_budget[] = $row_b["costsheet_bid_position_costsheet_bid_id"];
	}

	$bid_totals["bids_in_budget"] = $bids_in_budget;
	
	return $bid_totals;
}

/********************************************************************
    create and update budget with bid posisions
*********************************************************************/
function update_costsheet_budget_from_bid($bid_id = 0)
{
	
	$sql = "select * from costsheet_bid_positions " .
		   "left join costsheet_bids on costsheet_bid_id = costsheet_bid_position_costsheet_bid_id " . 
	       "where costsheet_bid_position_is_in_budget = 1 " . 
		   " and costsheet_bid_position_costsheet_bid_id = " . $bid_id;

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
	
		$sql_c = "select costsheet_id " . 
			     "from costsheets " . 
			     " where costsheet_id = " . dbquote($row["costsheet_bid_position_costsheet_id"]);

		$res_c = mysql_query($sql_c) or dberror($sql_c);
		if($row_c = mysql_fetch_assoc($res_c)) // cost sheet positions exists
		{
			$fields = array();

			$fields[] = "costsheet_code = " . dbquote($row["costsheet_bid_position_code"]);
			$fields[] = "costsheet_text = " . dbquote($row["costsheet_bid_position_text"]);
			$fields[] = "costsheet_company = " . dbquote($row["costsheet_bid_company"]);
			$fields[] = "costsheet_budget_amount = " . dbquote($row["costsheet_bid_position_amount"]);
			$fields[] = "date_modified = " . dbquote(date("Y-m-d H:i:s"));
			$fields[] = "user_modified = " . dbquote(user_login());

			$sql = "update costsheets set " . join(", ", $fields) . " where costsheet_id = " . $row_c["costsheet_id"];
			mysql_query($sql) or dberror($sql);

		}
		else
		{
			$fields = array();
			$values = array();

			$fields[] = "costsheet_project_id";
			$values[] = dbquote($row["costsheet_bid_position_project_id"]);

			$fields[] = "costsheet_pcost_group_id";
			$values[] = dbquote($row["costsheet_bid_position_pcost_group_id"]);

			$fields[] = "costsheet_pcost_subgroup_id";
			$values[] = dbquote($row["costsheet_bid_position_pcost_subgroup_id"]);

			$fields[] = "costsheet_code";
			$values[] = dbquote($row["costsheet_bid_position_code"]);

			$fields[] = "costsheet_text";
			$values[] = dbquote($row["costsheet_bid_position_text"]);

			$fields[] = "costsheet_budget_amount";
			$values[] = dbquote($row["costsheet_bid_position_amount"]);

			$fields[] = "costsheet_company";
			$values[] = dbquote($row["costsheet_bid_company"]);

			$fields[] = "costsheet_is_in_budget";
			$values[] = 1;

			$fields[] = "date_created";
			$values[] = dbquote(date("Y-m-d H:i:s"));

			$fields[] = "user_created";
			$values[] = dbquote(user_login());

			$sql = "insert into costsheets (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

			$result = mysql_query($sql) or dberror($sql);
			$new_id = mysql_insert_id();

			//connect new bid position with budget record (update)
			$sql_u = "update costsheet_bid_positions " . 
				     "set costsheet_bid_position_costsheet_id = " . dbquote($new_id) .
				     " where  costsheet_bid_position_id = " . $row["costsheet_bid_position_id"];

			$result = mysql_query($sql_u) or dberror($sql_u);
		}
	}
	return true;
}


/********************************************************************
    create and update budget with bid posisions
*********************************************************************/
function update_bid_positions_from_budget($project_id = 0)
{
	$sql = "select * from costsheets " . 
		   "where costsheet_project_id = " . dbquote($project_id);

	
	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$fields = array();

		$fields[] = "costsheet_bid_position_code = " . dbquote($row["costsheet_code"]);
		$fields[] = "costsheet_bid_position_text = " . dbquote($row["costsheet_text"]);
		$fields[] = "date_modified = " . dbquote(date("Y-m-d H:i:s"));
		$fields[] = "user_modified = " . dbquote(user_login());



		$sql = "update costsheet_bid_positions set " . join(", ", $fields) . " where costsheet_bid_position_costsheet_id = " . $row["costsheet_id"];
		mysql_query($sql) or dberror($sql);

	}

	return true;
}


/********************************************************************
    get totals from cer and approved cer totals
*********************************************************************/
function get_project_cer_totals($project_id, $costgroups)
{
	$cer_totals = array();

	//build array structure

	$cer_totals["investment_total"] = 0;
	$cer_totals["investments_approved_total"] = 0;
	$cer_totals["investments_additional_approved_total"] = 0;

	foreach($costgroups as $group_id=>$group_name)
	{
		$cer_totals["investments"][$group_id] = 0;
		$cer_totals["investments_approved"][$group_id] = 0;
		$cer_totals["investments_additional_approved"][$group_id] =0;
	}

	$sql = "select * from cer_investments " . 
		   "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
		   "where cer_investment_cer_version = 0 and cer_investment_type in(1, 3, 5, 7, 11) and cer_investment_project = " . param("pid")  .
		   " order by cer_investment_type";


	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res)) {
		
		$cer_totals["investment_total"] = $cer_totals["investment_total"] + $row["cer_investment_amount_cer_loc"];
		$cer_totals["investments_approved_total"] = $cer_totals["investments_approved_total"] + $row["cer_investment_amount_cer_loc_approved"];
		$cer_totals["investments_additional_approved_total"] = $cer_totals["investments_additional_approved_total"] + $row["cer_investment_amount_additional_cer_loc_approved"];
			
		
		if($row["cer_investment_type"] == 1) //construction
		{
			$cer_totals["investments"][1] = $row["cer_investment_amount_cer_loc"];
			$cer_totals["investments_approved"][1] = $row["cer_investment_amount_cer_loc_approved"];
			$cer_totals["investments_additional_approved"][1] = $row["cer_investment_amount_additional_cer_loc_approved"];
		}
		elseif($row["cer_investment_type"] == 3) //fixturing
		{
			$cer_totals["investments"][2] = $row["cer_investment_amount_cer_loc"];
			$cer_totals["investments_approved"][2] = $row["cer_investment_amount_cer_loc_approved"];
			$cer_totals["investments_additional_approved"][2] = $row["cer_investment_amount_additional_cer_loc_approved"];
		}
		elseif($row["cer_investment_type"] == 7) //equipment
		{
			$cer_totals["investments"][3] = $row["cer_investment_amount_cer_loc"];
			$cer_totals["investments_approved"][3] = $row["cer_investment_amount_cer_loc_approved"];
			$cer_totals["investments_additional_approved"][3] = $row["cer_investment_amount_additional_cer_loc_approved"];
		}
		elseif($row["cer_investment_type"] == 5) //architectural
		{
			$cer_totals["investments"][4] = $row["cer_investment_amount_cer_loc"];
			$cer_totals["investments_approved"][4] = $row["cer_investment_amount_cer_loc_approved"];
			$cer_totals["investments_additional_approved"][4] = $row["cer_investment_amount_additional_cer_loc_approved"];
		}
		else // other cost
		{
			$cer_totals["investments"][5] = $row["cer_investment_amount_cer_loc"];
			$cer_totals["investments_approved"][5] = $row["cer_investment_amount_cer_loc_approved"];
			$cer_totals["investments_additional_approved"][5] = $row["cer_investment_amount_additional_cer_loc_approved"];
		}
	}

	return $cer_totals;
}


/********************************************************************
    get kl approved
*********************************************************************/
function get_kl_apporved_money($project_id, $version = 0)
{
	$approved_money = array();
	
	$sql = "select * from cer_investments " .
		   "left join pcost_groups on pcost_group_posinvestment_type_id = cer_investment_type " .
		   "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
		   "where pcost_group_id > 0 " . 
		   " and cer_investment_cer_version = " . $version . " and cer_investment_project = " . $project_id;

	$res = mysql_query($sql) or dberror($sql);

	while($row = mysql_fetch_assoc($res))
	{
		$approved_money[$row["pcost_group_id"]] = $row["cer_investment_amount_cer_loc_approved"] + $row["cer_investment_amount_additional_cer_loc_approved"];
	}
	return $approved_money;
}

/********************************************************************
    update posorderinvestments
*********************************************************************/
function update_posorder_investments($project_id = 0, $order_id = 0)
{
	
	//check if investment records are present
	$sql = "select * from posinvestment_types where posinvestment_type_active = 1";

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$sql_i = "select count(posorderinvestment_id) as num_recs " .
				 "from posorderinvestments " . 
				 "where posorderinvestment_investment_type = " . $row["posinvestment_type_id"] .
				 " and posorderinvestment_posorder = " . $order_id;

		$res_i = mysql_query($sql_i) or dberror($sql_i);
		$row_i = mysql_fetch_assoc($res_i);

		if ($row_i["num_recs"] == 0)
		{
			$sql = "insert into posorderinvestments (" .
				   "posorderinvestment_posorder, " .
				   "posorderinvestment_investment_type, " .
				   "user_created, date_created) values (".
				   $order_id . ", " .
				   $row["posinvestment_type_id"] . ", " . 
				   dbquote(user_login()) . ", " . 
				   "current_timestamp)";

			$result = mysql_query($sql) or dberror($sql);
			
		}
	}


	//update values from CER
	$currency = get_currency_from_cer($project_id);
	
	$sql = "select * from cer_investments " . 
		   " where cer_investment_cer_version = 0 " . 
		   " and cer_investment_project = " . $project_id;

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		
		$cer_investment_amount_cer_chf = ($currency["exchange_rate"] * $row["cer_investment_amount_cer_loc"])/$currency["factor"];
		$cer_investment_amount_cer_chf_approved = ($currency["exchange_rate"] * $row["cer_investment_amount_cer_loc_approved"])/$currency["factor"];
		$cer_investment_amount_additional_cer_chf_approved = ($currency["exchange_rate"] * $row["cer_investment_amount_additional_cer_loc_approved"])/$currency["factor"];

		
		$fields = array();
		$fields[] = "posorderinvestment_amount_cer = " .  dbquote($cer_investment_amount_cer_chf);
		$fields[] = "posorderinvestment_amount_cer_loc = " .  dbquote($row["cer_investment_amount_cer_loc"]);
		$fields[] = "posorderinvestment_amount_cer_approved = " .  dbquote($cer_investment_amount_cer_chf_approved);
		$fields[] = "posorderinvestment_amount_cer_loc_approved = " .  dbquote($row["cer_investment_amount_cer_loc_approved"]);
		$fields[] = "posorderinvestment_amount_cer_approved_additional = " .  dbquote($cer_investment_amount_additional_cer_chf_approved);
		$fields[] = "posorderinvestment_amount_cer_loc_approved_additional = " .  dbquote($row["cer_investment_amount_additional_cer_loc_approved"]);
		$fields[] = "user_modified = " .  dbquote(user_login());
		$fields[] = "date_modified = " .  dbquote(date("Y-m-d H:i:s"));


		$sql = "update posorderinvestments set " . join(", ", $fields) . 
			   " where posorderinvestment_investment_type = " . $row["cer_investment_type"] . 
			   " and posorderinvestment_posorder = " . $order_id;
		mysql_query($sql) or dberror($sql);
	}


	//update values from CMS
	$currency = get_client_currency($order_id);
	$budget_totals = get_project_budget_totals($project_id, $currency);

	foreach($budget_totals["group_real_totals_by_posinvestment_type"] as $posinvestment_type=>$amount)
	{
		$fields = array();
		$fields[] = "posorderinvestment_amount_cms = " .  dbquote($budget_totals["group_real_totals_by_posinvestment_type_chf"][$posinvestment_type]);
		$fields[] = "posorderinvestment_amount_cms_loc = " .  dbquote($amount);
		$fields[] = "user_modified = " .  dbquote(user_login());
		$fields[] = "date_modified = " .  dbquote(date("Y-m-d H:i:s"));


		$sql = "update posorderinvestments set " . join(", ", $fields) . 
			   " where posorderinvestment_investment_type = " . $posinvestment_type . 
			   " and posorderinvestment_posorder = " . $order_id;
		mysql_query($sql) or dberror($sql);
	}
}

?>