<?php
/********************************************************************

    projects_archive_projects.php

    Entry page for the projects section.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-11-07
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2002-12-03
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("has_access_to_archive");

register_param("y1");
register_param("y2");
register_param("c");
register_param("p");
register_param("pn");

/********************************************************************
    prepare all data needed
*********************************************************************/
if (!param("y1"))
{
    $y1 = 0;
}
else
{
	$y1 = param("y1");
}
if (!param("y2"))
{
    $y2 = 0;
}
else
{
	$y2 = param("y2");
}

if (!param("p"))
{
    $p = 0;
}
else
{
	$p = param("p");
}

if (!param("c"))
{
    $c = 0;
}
else
{
	$c = param("c");
}

if (!param("pn"))
{
    $pn = 0;
}
else
{
	$pn = param("pn");
}
// get user_data
$user_data = get_user(user_id());


// create sql
$sql = "select distinct project_id, project_number, " .
       "left(projects.date_created, 10), ".
       "    product_line_name, postype_name, " . "concat(order_shop_address_place,', ', order_shop_address_company), country_name, ".
        "    rtcs.user_name as rtc, rtos.user_name as rto,".
       "    order_id, order_actual_order_state_code, project_costtype_text,  ".
	   " project_cost_cms_completed, project_cost_cms_approved, " . 
	   " project_actual_opening_date, projectkind_code " . 
       "from projects ".
       "left join orders on project_order = order_id ".
	   "left join project_costs on project_cost_order = order_id " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
       "left join order_items on order_item_order = order_id ".
       "left join product_lines on project_product_line = product_line_id ".
       "left join postypes on postype_id = project_postype ".
       "left join countries on order_shop_address_country = countries.country_id ".
       "left join users as rtcs on project_retail_coordinator = rtcs.user_id " .
	   "left join projectkinds on projectkind_id = project_projectkind ".
	   "left join users as rtos on order_retail_operator = rtos.user_id ";



// create list filter
$condition = get_user_specific_order_list(user_id(), 1);


if (has_access("has_access_to_all_projects"))
{
    $list_filter = "(order_actual_order_state_code >= '820' or (order_archive_date is not null and order_archive_date <> '0000-00-00')) ";
    if($y1 > 0)
    {
        $list_filter .= "    and left(order_date,4) >= " .$y1;
    }
    if($y2 > 0)
    {
        $list_filter .= "    and left(order_date,4) <= " .$y2;
    }  

    if($p > 0)
    {
        $list_filter .= "    and product_line_id = " . $p;
    }

    if($c > 0)
    {
        $list_filter .= "    and country_id = " . $c;
    }

	if($pn != 0)
    {
        $list_filter .= "    and order_number like '%" . $pn . "%' ";
    }

}
elseif (has_access("has_access_to_all_projects_of_his_country"))
{

    if ($condition == "")
    {
		$list_filter = "(order_actual_order_state_code >= '820' or (order_archive_date is not null and order_archive_date <> '0000-00-00')) ";

		if($y1 > 0)
		{
			$list_filter .= "    and left(order_date,4) >= " .$y1;
		}
		if($y2 > 0)
		{
			$list_filter .= "    and left(order_date,4) <= " .$y2;
		}
		if($p > 0)
		{
			$list_filter .= "    and product_line_id = " . $p;
		}
		if($c > 0)
		{
			$list_filter .= "    and country_id = " . $c;
		}
		
		$list_filter .= "    and (order_item_supplier_address = " . $user_data["address"] . " " .
					   "    or order_item_forwarder_address = " . $user_data["address"] . " " .
					   "    or order_retail_operator = " . user_id() . " " .
					   "    or project_retail_coordinator = " . user_id()  . " " .
					   "    or project_design_contractor = " .  user_id()  . " " .
					   "    or project_design_supervisor = " .   user_id()   . " " .
					   "    or country_id = " . $user_data["country"] . 
					   "    or order_client_address = " . $user_data["address"] . ")";
		

		if($pn != 0)
		{
			$list_filter .= "    and order_number like '%" . $pn . "%' ";
		}
	}
	else
	{

		$list_filter = "(order_actual_order_state_code >= '820' or (order_archive_date is not null and order_archive_date <> '0000-00-00')) ";

		if($y1 > 0)
		{
			$list_filter .= "    and left(order_date,4) >= " .$y1;
		}
		if($y2 > 0)
		{
			$list_filter .= "    and left(order_date,4) <= " .$y2;
		}
		if($p > 0)
		{
			$list_filter .= "    and product_line_id = " . $p;
		}
		if($c > 0)
		{
			$list_filter .= "    and country_id = " . $c;
		}
		
		$list_filter .= "   and (" . $condition . ")";
		

		if($pn != 0)
		{
			$list_filter .= "    and order_number like '%" . $pn . "%' ";
		}
	
	}

	if(has_access("has_access_to_retail_only")) {
		$list_filter .= " and project_cost_type in (1) ";
	}

	if(has_access("has_access_to_wholesale")) {
		$list_filter .= " and project_cost_type in (2, 6) ";
	}

}
else
{
	if ($condition == "")
    {
        
        $list_filter = "(order_actual_order_state_code >= '820' or (order_archive_date is not null and order_archive_date <> '0000-00-00')) ";

        if($y1 > 0)
        {
            $list_filter .= "    and left(order_date,4) >= " .$y1;
        }
        if($y2 > 0)
        {
            $list_filter .= "    and left(order_date,4) <= " .$y2;
        }
        if($p > 0)
        {
            $list_filter .= "    and product_line_id = " . $p;
        }
        if($c > 0)
        {
            $list_filter .= "    and country_id = " . $c;
        }
        
        $list_filter .= "    and (order_item_supplier_address = " . $user_data["address"] . " " .
                       "    or order_item_forwarder_address = " . $user_data["address"] . " " .
                       "    or order_retail_operator = " . user_id() . " " .
                       "    or project_retail_coordinator = " . user_id()  . " " .
                       "    or project_design_contractor = " .  user_id()  . " " .
                       "    or project_design_supervisor = " .   user_id()   . " " .
                       "    or order_client_address = " . $user_data["address"] . ")";
    }
    else
    {
        
        $list_filter = "(order_actual_order_state_code >= '820' or (order_archive_date is not null and order_archive_date <> '0000-00-00')) ";

        if($y1 > 0)
        {
            $list_filter .= "    and left(order_date,4) >= " .$y1;
        }
        if($y2 > 0)
        {
            $list_filter .= "    and left(order_date,4) <= " .$y2;
        }
        if($p > 0)
        {
            $list_filter .= "    and product_line_id = " . $p;
        }
        if($c > 0)
        {
            $list_filter .= "    and country_id = " . $c;
        }

        $list_filter .= "   and (" . $condition . ")";
    }


	if($pn != 0)
    {
        $list_filter .= "    and order_number like '%" . $pn . "%' ";
    }


	if(has_access("has_access_to_retail_only")) {
		$list_filter .= " and project_cost_type in (1) ";
	}

	if(has_access("has_access_to_wholesale")) {
		$list_filter .= " and project_cost_type in (2, 6) ";
	}
}

$cms_states = array();
$aods = array();
$duty_free_projects = array();

if (has_access("has_access_to_all_projects"))
{
    $sql_tmp = $sql . " where " . $list_filter;
    $res = mysql_query($sql_tmp);

    while($row = mysql_fetch_assoc($res))
    {
		
		if($row["order_actual_order_state_code"] != '900')
		{
			if($row['project_actual_opening_date'] != NULL and $row['project_actual_opening_date'] != '0000-00-00')
			{
				$aods[$row["project_id"]] = "/pictures/ok.gif";
			}
			else
			{
				$aods[$row["project_id"]] = "/pictures/not_ok.gif";
			}

			if($row['project_cost_cms_completed'] == 1)
			{
				$cms_states[$row["project_id"]] = "/pictures/ok.gif";
			}
			else
			{
				$cms_states[$row["project_id"]] = "/pictures/not_ok.gif";
			}
		}

		
		//check if project is a duty free project
		$project_design_objective_item_ids = get_project_design_objective_item_ids($row["project_id"]);
		
		if(array_key_exists(161 ,$project_design_objective_item_ids))
		{
			$duty_free_projects[$row["project_id"]] = "/pictures/dutyfree.png";
		}
    }
}


//count projects
$sql_count = "select count(distinct project_id) as num_recs " . 
       "from projects ".
       "left join orders on project_order = order_id ".
	   "left join order_items on order_item_order = order_id ".
	   "left join project_costs on project_cost_order = order_id " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
       "left join product_lines on project_product_line = product_line_id ".
       "left join postypes on postype_id = project_postype ".
       "left join countries on order_shop_address_country = countries.country_id ".
       "left join users as rtcs on project_retail_coordinator = rtcs.user_id " . 
	   "left join users as rtos on order_retail_operator = rtos.user_id ";

$sql_count = $sql_count . " where " . $list_filter;
$res = mysql_query($sql_count);
$row = mysql_fetch_assoc($res);

if($row["num_recs"] == 1) {
	
	
	$sql_count = "select distinct project_id " . 
		   "from projects ".
		   "left join orders on project_order = order_id ".
		   "left join project_costs on project_cost_order = order_id " .
		   "left join order_items on order_item_order = order_id ".
		   "left join project_costtypes on project_costtype_id = project_cost_type " .
		   "left join product_lines on project_product_line = product_line_id ".
		   "left join postypes on postype_id = project_postype ".
		   "left join countries on order_shop_address_country = countries.country_id ".
		   "left join users as rtcs on project_retail_coordinator = rtcs.user_id " . 
		   "left join users as rtos on order_retail_operator = rtos.user_id ";

	$sql_count = $sql_count . " where " . $list_filter;
	$res = mysql_query($sql_count);
	$row = mysql_fetch_assoc($res);

	$link = "project_task_center.php?pid=" . $row["project_id"] . "&y1=" .param("y1") . "&y2=" . param("y2");
	redirect($link);
}


/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);

$list->set_entity("projects in the records");
$list->set_order("left(projects.date_created, 10) desc");
$list->set_filter($list_filter);

$list->add_hidden("y1", param("y1"));
$list->add_hidden("y2", param("y2"));
$list->add_hidden("p", param("p"));
$list->add_hidden("c", param("c"));
$list->add_hidden("pn", param("pn"));

$link = "project_task_center.php?pid={project_id}&y1=" .param("y1") . "&y2=" . param("y2");
$list->add_column("project_number", "Project No.", $link, "", "", COLUMN_NO_WRAP);



$list->add_column("project_costtype_text", "Legal Type");
$list->add_column("projectkind_code", "Kind", "", 0, "", COLUMN_NO_WRAP);

$list->add_column("product_line_name", "Product Line");


$list->add_column("postype_name", "POS Type");


$list->add_column("order_actual_order_state_code", "Status");
$list->add_column("left(projects.date_created, 10)", "Submitted", "", "", "", COLUMN_NO_WRAP);
$list->add_column("country_name", "Country");
$list->add_column("concat(order_shop_address_place,', ', order_shop_address_company)", "POS Location");

$list->add_column("rtc", "PM");
$list->add_column("rto", "RTO");

$list->add_image_column("dutyfree", "DF", 0, $duty_free_projects);

if (has_access("has_access_to_all_projects"))
{
	$list->add_image_column("cms", "CMS", 0, $cms_states);
	$list->add_image_column("aod", "AOD", 0, $aods);
}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->process();

$page = new Page("projects");

//$page->register_action('orders', 'Orders', "orders_archive.php");
$page->register_action('projects', 'Projects', "projects_archive.php");
$page->register_action('welcome', 'Quit Archive', "../user/welcome.php");

$page->header();
$page->title("Projects: Archive");
$list->render();
$page->footer();

?>