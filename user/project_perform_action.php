<?php
/********************************************************************

    project_perform_action.php

    Perform an action selected from the action list
    in project_task_center.php

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-10-03
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-07
    Version:        1.1.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

require_once "include/order_state_constants.php";

check_access("can_use_taskcentre_in_projects");



/********************************************************************
    prepare data needed
*********************************************************************/
$keep_project_state = 0;

// read project and order details
$project = get_project(param("pid"));


//get notification reciepients for attachments of the type layout
$notification_reciepients1 = array();
$notification_reciepients2 = array();
$notification_reciepients3 = array();
$notification_reciepients4 = array();

$sql = "select * from postype_notifications " .
       "where postype_notification_postype = " . $project["project_postype"] . 
	   " and postype_notification_prodcut_line = " . $project["project_product_line"];

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
    if($row["postype_notification_email5"])
    {
        $notification_reciepients1[] = $row["postype_notification_email5"];
    }
    if($row["postype_notification_email6"])
    {
        $notification_reciepients2[] = $row["postype_notification_email6"];
    }
    if($row["postype_notification_email7"])
    {
        $notification_reciepients3[] = $row["postype_notification_email7"];
    }
    if($row["postype_notification_email8"])
    {
        $notification_reciepients4[] = $row["postype_notification_email8"];
    }
}

// get company's address
$client_address = get_address($project["order_client_address"]);

// get user data of order's user
$client_user = get_user($project["order_user"]);
$logged_user = get_user(user_id());



if (id() == MOVED_TO_THE_RECORDS or id() == ORDER_CANCELLED)
{
    /********************************************************************
        build form
    *********************************************************************/
    $form = new Form("addresses", "adress");

    $form->add_section("Project");
    $form->add_hidden("pid", param("pid"));
    $form->add_hidden("order_id", $project["project_order"]);

    $form->add_label("project_number", "Project Number", 0, $project["order_number"]);
    $form->add_lookup("product_line", "Product Line", "product_lines", "product_line_name", 0, $project["project_product_line"]);
    $form->add_lookup("project_postype", "POS Type", "postypes", "postype_name", 0, $project["project_postype"]);

    $line = "concat(user_name, ' ', user_firstname)";
    if ($project["project_retail_coordinator"])
    {
        $form->add_lookup("retail_coordinator", "Project Manager", "users", $line, 0, $project["project_retail_coordinator"]);
    }
    else
    {
        $form->add_label("retail_coordinator", "Project Manager");
    }

    if ($project["order_retail_operator"])
    {
        $form->add_lookup("retail_operator", "Retail Operator", "users", $line, 0, $project["order_retail_operator"]);
    }
    else
    {
        $form->add_label("retail_operator", "Retail Operator");
    }

    $client = $client_address["company"] . ", " .
              $client_address["zip"] . " " . $client_address["place"] . ", " .
              $client_address["country"];

    $form->add_label("client_address", "Client", 0, $client);

    
    $cms_filled = 1;
    if (id() == MOVED_TO_THE_RECORDS)
    {
        //check if cost monitoring sheet is approved
                
		if($project['project_projectkind'] != 4 and $project['project_projectkind'] != 5) // no take over, noe lease renewal
		{
			$sql = "select project_cost_cms_approved " . 
				   "from project_costs " . 
				   "where project_cost_order = " . $project["project_order"];

			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);
			if($row["project_cost_cms_approved"] == 0)
			{
				$cms_filled = 1;        
				$form->error("The project can not be put to the records since the cost monitoring sheet has not been approved! Please submit the project for approval of the CMS to the Project Manager!");
			}
			
			if($project["project_actual_opening_date"] == NULL or $project["project_actual_opening_date"] == '0000-00-00')
			{
				$cms_filled = 1;        
				$form->error("The project can not be put to the records since the actual POS opening date is missing!");
			}
		}
		
		
		$form->add_comment("Please confirm that you want to put the project to the records!");
    }
    if (id() == ORDER_CANCELLED)
    {
        $creps = get_reciepients_on_cancellation(param("pid"));

		
		$form->add_comment("Please confirm the cancellation of the project!<br />The following persons will be informed by an email notification.");

		foreach($creps as $key=>$user)
		{
			if($user["role_name"] == "Standard Recipient")
			{
				$form->add_checkbox("CR" . $user["user_id"], $user["role_name"], true, DISABLED, $user["full_name"]);
			}
			else
			{
				$form->add_checkbox("CR" . $user["user_id"], $user["role_name"], true, 0, $user["full_name"]);
			}
		}
		
    }

    if($cms_filled == 1)
    {
        $form->add_button("confirm", "Put the Project to the Records");
    }

    $form->add_button("back", "Back");

    $form->populate();
    $form->process();

    if ($form->button("back"))
    {
        $link = "project_task_center.php?pid=" . param("pid") . "&id=" . param("pid");
        redirect($link);
    }
    if ($form->button("confirm"))
    {
        $uids = "";
		foreach($creps as $key=>$user)
		{
			
			if($user["role_name"] == "Standard Recipient")
			{
				$uids .= $user["user_id"] . "-";

			}
			elseif($form->value("CR" . $user["user_id"]) == 1)
			{
				$uids .= $user["user_id"] . "-";
			}
		}
		
		$link = "project_perform_action_confirm.php?pid=" . param("pid") . "&id=" . param("pid") . "&action=" . id() ."&uids=" . $uids;
        redirect($link);
    }

    /********************************************************************
        render page
    *********************************************************************/
    $page = new Page("projects");

    $page->header();
    
    if (id() == MOVED_TO_THE_RECORDS)
    {    
        $page->title("Move project to the records");
    }
    if (id() == ORDER_CANCELLED)
    {    
        $page->title("Cancel project");
    }

    $form->render();

    $page->footer();
}
else
{
    /********************************************************************
        prepare data needed
    *********************************************************************/
    // errors
    $error = array();
    $error[1] = "No mail was sent!<br />Please select a recipient at the bottom of the page.";
    $error[2] = "There are no recipients for this step! The cause might be one of the following:<br />- you have not entered the P.O. Numbers (edit list of matrials)<br />- you have already ordered all items<br />- the supplier has no email address specified in the address data (contact administrator)";
    $error[3] = "There are no recipients for this step! The cause might be one of the following:<br />- you have not assigned forwarders (edit list of materials)<br />- you have made no order to a supplier (step 700)<br />- the supplier has not entered an expected ready for pick up date<br />- all items already have a pickup date<br />- the forwarder has no email address specified in the address data (contact administrator)";
    $error[4] = "There are no recipients for this step! The cause might be one of the following:<br />- you have not assigned a design contractor<br />- the design contractor has no email address specified in the address data (contact administrator)";
    $error[5] = "There are no recipients for this step! The cause might be one of the following:<br />- you have not assigned a design supervisor<br />- the design supervisor has no email address specified in the address data (contact administrator)";
    $error[6] = "There are no recipients for this step! The cause might be one of the following:<br />- there is no item requiering an offer<br />- you have no entries in the list of materials<br />- the suppliers have no email address specified in the address data (contact administrator)";
    $error[7] = "You can not confirm delivery because there are items in your project not having an arrival date.";
    $error[8] = "You can not submit a request for budget approval since not all items have been offered by the suppliers or not all offers made have been accepted! <br />Another cause might be that the owner of the project is a user not beeing active anymore and was not replaced by another person.";
    $error[9] = "You can not submit an offer without having indicated in the prices (edit offer data).";
    $error[10] = "You can not confirm an order without having indicated the pick up data<br />or your might not be logged in as a valid supplier.";
    $error[11] = "You can not accept a request for delivery without having indicated <br />the expected date of arrival.";
	$error[12] = "Layout request can not be accepted. Please enter the realistic shop opening date first (edit pso data).";


    // get Action parameter
    $action_parameter = get_action_parameter(id(), 1);

    // get people involved in the project step
    $recepients = array();


    if ($action_parameter["recipient"] == 'Client')
    {
		
		$sql = "select user_id, address_id, ".
               "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
               "    user_email, user_email_cc, user_email_deputy, address_type ".
               "from users ".
               "left join addresses on user_address = address_id ".
               "where user_email<>'' ".
               "    and user_active = 1 " .
               "    and user_id = " . dbquote($project["order_user"]);

		$res = mysql_query($sql) or dberror($sql);

        if ($row = mysql_fetch_assoc($res))
        {
            $user_data = array();
            $user_data["id"] = $row["user_id"];
            $user_data["email"] = $row["user_email"];
            $user_data["cc"] = $row["user_email_cc"];
            $user_data["deputy"] = $row["user_email_deputy"];
            $user_data["full_name"] = $row["user_full_name"];
            $user_data["address"] = $row["address_id"];
            $recepients[] = $user_data;
        }
    }
    elseif ($action_parameter["recipient"] == 'Supplier')
    {
		 $user_data_key = 0;
		 $where_clause = "";
		 if (id() == SUBMIT_REQUEST_FOR_OFFER or id() == OFFER_REJECTED or id() == OFFER_ACCEPTED)
		 {
			$where_clause = "    and (order_item_no_offer_required is null or order_item_no_offer_required = 0) ";
		 }
        
        $sql = "select distinct user_id, address_id, address_type, order_item_order_revisions, ".
               "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
               "    user_email, user_email_cc, user_email_deputy ".
               "from order_items ".
               "left join addresses on order_item_supplier_address = address_id ".
               "left join users on user_address = address_id ".
               "where user_email<>'' " . $where_clause .
               "    and user_active = 1 " .
               "    and order_item_order = " . $project["project_order"] . " " .
               "    and (order_item_not_in_budget is null or order_item_not_in_budget =0)" .
               " order by address_company, user_name";

		
		$res = mysql_query($sql) or dberror($sql);
        while ($row = mysql_fetch_assoc($res))
        {
			if (id() == ORDER_TO_SUPPLIER_SUBMITTED)
            {

                // check if there are items in the order not having been ordered already
                $sql_ordered = "select order_item_id, order_item_order_revisions, " .
                               "   order_item_po_number, order_item_ordered ".
                               "from order_items ".
                               "where order_item_type <= " . ITEM_TYPE_SPECIAL .
                               "    and order_item_supplier_address = ". $row["address_id"] .
                               "    and (order_item_not_in_budget is null or order_item_not_in_budget =0)" .
                               "    and order_item_order = " . $project["project_order"];


                $res1= mysql_query($sql_ordered) or dberror($sql_ordered);
				$revisions = 0;

                while ($row1 = mysql_fetch_assoc($res1))
                {
                    $already_ordered = 1;
                    $has_po_number = 0;

                    if ($row1["order_item_ordered"] == "0000-00-00"   or $row1["order_item_ordered"] == Null)
                    {
                        $already_ordered = 0;
                    }
               
                    if ($row1["order_item_po_number"])
                    {
                        $has_po_number = 1;
                    }

					if($revisions == 0) // number of order revisions
					{
						$revisions = $row1["order_item_order_revisions"];
					}

                    if ($already_ordered == 0 and $has_po_number == 1)
                    {
					    
						$user_data = array();
						$user_data["id"] = $row["user_id"];
						$user_data["email"] = $row["user_email"];
						$user_data["cc"] = $row["user_email_cc"];
						$user_data["deputy"] = $row["user_email_deputy"];
						$user_data["full_name"] = $row["user_full_name"];
						$user_data["address"] = $row["address_id"];
						$user_data["order_revisions"] = $revisions;
						$user_data["role"] = "Supplier";
						$user_data["address_type"] = $row["address_type"];
						if($user_data_key != $row["user_id"])
						{
							$recepients[] = $user_data;
							$user_data_key = $row["user_id"];
						}
                    }
                }

            }
            else
            {
                $user_data = array();
                $user_data["id"] = $row["user_id"];
                $user_data["email"] = $row["user_email"];
                $user_data["cc"] = $row["user_email_cc"];
                $user_data["deputy"] = $row["user_email_deputy"];
                $user_data["full_name"] = $row["user_full_name"];
                $user_data["address"] = $row["address_id"];
				$user_data["order_revisions"] = $row["order_item_order_revisions"];
				$user_data["role"] = "Supplier";
                if($user_data_key != $row["user_id"])
                {
                    $recepients[] = $user_data;
                    $user_data_key = $row["user_id"];
                }
				
            }
        }
    }
    else if ($action_parameter["recipient"] == 'Forwarder')
    {
        $user_data_key = 0;
        $sql = "select distinct user_id, address_id, ".
               "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
               "    user_email, user_email_cc, user_email_deputy, address_type ".
               "from order_items ".
               "left join addresses on order_item_forwarder_address = address_id ".
               "left join users on user_address = address_id ".
               "where user_email<>'' ".
               "    and user_active = 1 " .
               "    and order_item_order = " . $project["project_order"] . " " .
               "    and (order_item_not_in_budget is null or order_item_not_in_budget =0)" .
               " order by address_company, user_name";

		$res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {
            if (id() == REQUEST_FOR_DELIVERY_SUBMITTED)
            {

                // check if there are items in the order not haveing been ordered already
                $sql_ordered = "select order_item_id, " .
                               "   order_item_ready_for_pickup, " .
                               "   order_item_pickup " .
                               "from order_items ".
                               "where order_item_type <= " . ITEM_TYPE_SPECIAL .
                               "    and order_item_forwarder_address = ". $row["address_id"] .
                               "    and (order_item_not_in_budget is null or order_item_not_in_budget =0)" .
                               "    and order_item_order = " .$project["project_order"];


				
				$res1= mysql_query($sql_ordered) or dberror($sql_ordered);


                while ($row1 = mysql_fetch_assoc($res1))
                {
                    $ready_for_pick_up = 0;
                    $pick_up_date_entered = 1;
      
                    if ($row1["order_item_ready_for_pickup"])
                    {
                        $ready_for_pick_up = 1;
                    }

                    if ($row1["order_item_pickup"] == "0000-00-00"   or $row1["order_item_pickup"] == Null)
                    {
                      $pick_up_date_entered = 0;
                    }

                    if ($ready_for_pick_up == 1 and $pick_up_date_entered == 0)
                    {
						$user_data = array();
						$user_data["id"] = $row["user_id"];
						$user_data["email"] = $row["user_email"];
						$user_data["cc"] = $row["user_email_cc"];
						$user_data["deputy"] = $row["user_email_deputy"];
						$user_data["full_name"] = $row["user_full_name"];
						$user_data["address"] = $row["address_id"];
						$user_data["role"] = "Forwarder";
						$user_data["address_type"] = $row["address_type"];
						if($user_data_key != $row["user_id"])
						{
							$recepients[] = $user_data;
							$user_data_key = $row["user_id"];
						}
                    }
                }
            }
            else
            {
                $user_data = array();
                $user_data["id"] = $row["user_id"];
                $user_data["email"] = $row["user_email"];
                $user_data["cc"] = $row["user_email_cc"];
                $user_data["deputy"] = $row["user_email_deputy"];
                $user_data["full_name"] = $row["user_full_name"];
                $user_data["address"] = $row["address_id"];
				$user_data["role"] = "Forwarder";
                if($user_data_key != $row["user_id"])
                {
                    $recepients[] = $user_data;
                    $user_data_key = $row["user_id"];
                }
            }
        } 
    }
    else if ($action_parameter["recipient"] == 'Project Manager')
    {
        $sql = "select user_id, address_id, ".
              "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
               "    user_email, user_email_cc, user_email_deputy ".
               "from users ".
               "left join addresses on user_address = address_id ".
               "where user_email<>'' ".
               "    and user_active = 1 " .
               "    and user_id = " . dbquote($project["project_retail_coordinator"]);

        $res = mysql_query($sql) or dberror($sql);

        if ($row = mysql_fetch_assoc($res))
        {
            $user_data = array();
            $user_data["id"] = $row["user_id"];
            $user_data["email"] = $row["user_email"];
            $user_data["cc"] = $row["user_email_cc"];
            $user_data["deputy"] = $row["user_email_deputy"];
            $user_data["full_name"] = $row["user_full_name"];
            $user_data["address"] = $row["address_id"];
            $recepients[] = $user_data;
        }
    }
    elseif ($action_parameter["recipient"] == 'Design Contractor')
    {

        $user_data_key = 0;
        $sql = "select user_id, address_id, ".
               "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
               "    user_email, user_email_cc, user_email_deputy ".
               "from users ".
               "left join addresses on user_address = address_id ".
               "where user_email<>'' ".
               "    and user_active = 1 " .
               "    and user_id = " . dbquote($project["project_design_contractor"]);

        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {
            $user_data = array();
            $user_data["id"] = $row["user_id"];
            $user_data["email"] = $row["user_email"];
            $user_data["cc"] = $row["user_email_cc"];
            $user_data["deputy"] = $row["user_email_deputy"];
            $user_data["full_name"] = $row["user_full_name"];
            $user_data["address"] = $row["address_id"];
            if($user_data_key != $row["user_id"])
            {
                $recepients[] = $user_data;
                $user_data_key = $row["user_id"];

            }
        }
    }
    else if ($action_parameter["recipient"] == 'Design Supervisor')
    {
        $user_data_key = 0;
        $sql = "select user_id, address_id, ".
               "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
               "    user_email, user_email_cc, user_email_deputy ".
               "from users ".
               "left join addresses on user_address = address_id ".
               "where user_email<>'' ".
               "    and user_active = 1 " .
               "    and user_id = " . dbquote($project["project_design_supervisor"]);

        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {
            $user_data = array();
            $user_data["id"] = $row["user_id"];
            $user_data["email"] = $row["user_email"];
            $user_data["cc"] = $row["user_email_cc"];
            $user_data["deputy"] = $row["user_email_deputy"];
            $user_data["full_name"] = $row["user_full_name"];
            $user_data["address"] = $row["address_id"];
            if($user_data_key != $row["user_id"])
            {
                $recepients[] = $user_data;
                $user_data_key = $row["user_id"];
            }
        }
    }
    else if ($action_parameter["recipient"] == 'Retail Operator')
    {
        $user_data_key = 0;
        $sql = "select user_id, address_id, ".
               "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
               "    user_email, user_email_cc, user_email_deputy ".
               "from users ".
               "left join addresses on user_address = address_id ".
               "where user_email<>'' ".
               "    and user_active = 1 " .
               "    and user_id = " . dbquote($project["order_retail_operator"]);

        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {
            if (id() == DELIVERY_CONFIRMED)
            {
                // check if there are items in the order not having an actual arrival date
                $sql_ordered = "select order_item_id, order_item_po_number ".
                               "from order_items ".
                               "where order_item_type <= " . ITEM_TYPE_SPECIAL .
                               "    and order_item_order = " . $project["project_order"] .
                               "    and (order_item_not_in_budget is null or order_item_not_in_budget =0)";

                $res1= mysql_query($sql_ordered) or dberror($sql_ordered);

                $already_arrived = 1;
                while ($row1 = mysql_fetch_assoc($res1))
                {

                    $sql_dates = "select date_id ".
                                 "from dates ".
                                 "left join date_types on date_type = date_type_id ".
                                 "where date_order_item = ". $row1["order_item_id"] .
                                 "    and date_type_code = 'ACAR'";

                    $res2= mysql_query($sql_dates) or dberror($sql_dates);
  
                    if ($row2 = mysql_fetch_assoc($res2))
                    {
                      // nothing
                    }
                    else
                    {
                      $already_arrived = 0;
                    }
                }

                if ($already_arrived == 1)
                {
                    $user_data = array();
                    $user_data["id"] = $row["user_id"];
                    $user_data["email"] = $row["user_email"];
                    $user_data["cc"] = $row["user_email_cc"];
                    $user_data["deputy"] = $row["user_email_deputy"];
                    $user_data["full_name"] = $row["user_full_name"];
                    $user_data["address"] = $row["address_id"];
                    if($user_data_key != $row["user_id"])
                    {
                        $recepients[] = $user_data;
                        $user_data_key = $row["user_id"];
                    }
                }

            }        
            else
            {
                $user_data = array();
                $user_data["id"] = $row["user_id"];
                $user_data["email"] = $row["user_email"];
                $user_data["cc"] = $row["user_email_cc"];
                $user_data["deputy"] = $row["user_email_deputy"];
                $user_data["full_name"] = $row["user_full_name"];
                $user_data["address"] = $row["address_id"];
                if($user_data_key != $row["user_id"])
                {
                    $recepients[] = $user_data;
                    $user_data_key = $row["user_id"];
                }
            }
            

        }
		
		//check if ther is a task for the performing user that tells us to keep the order state
		
		$sql_t = "select task_keep_order_state " . 
			     "from tasks " . 
			     "where task_order = " . $project["project_order"] .
			     " and task_user = " . user_id() . 
			     " order by task_id";

		$res_t = mysql_query($sql_t) or dberror($sql_t);
		if ($row_t = mysql_fetch_assoc($res_t))
		{
			$keep_project_state = $row_t["task_keep_order_state"];
		}
    }
    else // all addresses in the project
    {
        
		// client & & retail coordinator & retail operator
        $user_data_key = 0;
        $sql = "select user_id, address_id, ".
               "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
               "    user_email, user_email_cc, user_email_deputy ".
               "from users ".
               "left join addresses on user_address = address_id ".
               "where address_id != 2390 and user_email<>'' ".
               "    and user_active = 1 " .
               "    and (user_id = " . dbquote($project["order_user"]) .
               "    or user_id = " . dbquote($project["project_retail_coordinator"]) . 
               "    or user_id = " . dbquote($project["order_retail_operator"]) . ")";

        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {
            $user_data = array();
            $user_data["id"] = $row["user_id"];
            $user_data["email"] = $row["user_email"];
            $user_data["cc"] = $row["user_email_cc"];
            $user_data["deputy"] = $row["user_email_deputy"];
            $user_data["full_name"] = $row["user_full_name"];
            $user_data["address"] = $row["address_id"];
            if($user_data_key != $row["user_id"])
            {
                $recepients[] = $user_data;
                $user_data_key = $row["user_id"];
            }
        }

        // design contractor & design supervisor

        $user_data_key = 0;
        $sql = "select user_id, address_id, ".
               "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
               "    user_email, user_email_cc, user_email_deputy ".
               "from users ".
               "left join addresses on user_address = address_id ".
               "where user_email<>'' ".
               "    and user_active = 1 " .
               "    and (user_id = " . dbquote($project["project_design_supervisor"]) .
               "    or user_id = " . dbquote($project["project_design_contractor"]) . ")";


        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {
            $user_data = array();
            $user_data["id"] = $row["user_id"];
            $user_data["email"] = $row["user_email"];
            $user_data["cc"] = $row["user_email_cc"];
            $user_data["deputy"] = $row["user_email_deputy"];
            $user_data["full_name"] = $row["user_full_name"];
            $user_data["address"] = $row["address_id"];
            if($user_data_key != $row["user_id"])
            {
                $recepients[] = $user_data;
                $user_data_key = $row["user_id"];
            }
        }

        // supplier
        $user_data_key = 0;
        $sql = "select distinct user_id, address_id, ".
               "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
               "    user_email, user_email_cc, user_email_deputy ".
               "from order_items ".
               "left join addresses on order_item_supplier_address = address_id ".
               "left join users on user_address = address_id ".
               "where address_id != 2390 and user_email<>'' ".
               "    and user_active = 1 " .
               "    and order_item_order = " . $project["project_order"] . " " .
               "    and (order_item_not_in_budget is null or order_item_not_in_budget =0)" .
               " order by address_company";

        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {
            $user_data = array();
            $user_data["id"] = $row["user_id"];
            $user_data["email"] = $row["user_email"];
            $user_data["cc"] = $row["user_email_cc"];
            $user_data["deputy"] = $row["user_email_deputy"];
            $user_data["full_name"] = $row["user_full_name"];
            $user_data["address"] = $row["address_id"];
			$user_data["role"] = "Supplier";
            if($user_data_key != $row["user_id"])
            {
                $recepients[] = $user_data;
                $user_data_key = $row["user_id"];
            }
        }

        // forwarder
        $user_data_key = 0;
        $sql = "select distinct user_id, address_id, ".
               "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
               "    user_email, user_email_cc, user_email_deputy ".
               "from order_items ".
               "left join addresses on order_item_forwarder_address = address_id ".
               "left join users on user_address = address_id ".
               "where address_id != 2390 and user_email<>'' ".
               "    and user_active = 1 " .
               "    and order_item_order = " . $project["project_order"] . " " .
               "    and (order_item_not_in_budget is null or order_item_not_in_budget =0)" .
               " order by address_company";

        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {
            $user_data = array();
            $user_data["id"] = $row["user_id"];
            $user_data["email"] = $row["user_email"];
            $user_data["cc"] = $row["user_email_cc"];
            $user_data["deputy"] = $row["user_email_deputy"];
            $user_data["full_name"] = $row["user_full_name"];
            $user_data["address"] = $row["address_id"];
			$user_data["role"] = "Forwarder";
            if($user_data_key != $row["user_id"])
            {
                $recepients[] = $user_data;
                $user_data_key = $row["user_id"];
            }
        }


		// special users
        $user_data_key = 0;
        $sql = "select distinct user_id, address_id, ".
               "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
               "    user_email, user_email_cc, user_email_deputy ".
               "from orders ".
               "left join addresses on order_client_address = address_id ".
               "left join users on user_address = address_id ".
               "where user_email<>'' ".
               "    and user_active = 1 and user_project_reciepient = 1 " .
			   " and user_address = address_id " .
			   " and order_id = " . $project["project_order"] .
               " order by address_company";


        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {
            $user_data = array();
            $user_data["id"] = $row["user_id"];
            $user_data["email"] = $row["user_email"];
            $user_data["cc"] = $row["user_email_cc"];
            $user_data["deputy"] = $row["user_email_deputy"];
            $user_data["full_name"] = $row["user_full_name"];
            $user_data["address"] = $row["address_id"];
			$user_data["role"] = "Additional Recipient";
            if($user_data_key != $row["user_id"])
            {
                $recepients[] = $user_data;
                $user_data_key = $row["user_id"];
            }
        }
    }

    // lock budget
    if (id() == BUDGET_APPROVED or id() == DELIVERY_CONFIRMED or id() == MOVED_TO_THE_RECORDS or id() == ORDER_CANCELLED)
    {
        update_budget_state($project["project_order"], 1);


        if(id() == BUDGET_APPROVED)
        {
            freeze_budget($project["project_order"]);
        }
    }

    // check if all budget positions have a valid price
    
    if (id() == OFFER_SUBMITTED)
    {
        $user_data = get_user(user_id());
        $all_offered = check_if_all_items_have_prices($project["project_order"], $user_data["address"]);
        if ($all_offered == 0)
        {
            $recepients = array();
        }
    }

    // check if all budget positions have been accepted and have a valid price
    if (id() == REQUEST_FOR_BUDGET_APPROVAL_SUBMITTED)
    {
        $all_offered = check_if_all_items_are_offered($project["project_order"]);
        if ($all_offered == 0)
        {
			$recepients = array();
        }
    }


    // check if all budget positions have have a pick up date
    
    if (id() == CONFIRM_ORDER_BY_SUPPLIER)
    {
        $pickupdates_entered = check_if_all_items_hav_pick_up_date($project["project_order"], $logged_user["address"]);
        if ($pickupdates_entered == 0)
        {
            $recepients = array();
        }
    }

    // check if all budget positions have have an arrival date
    if (id() == DELIVERY_CONFIRMED_FRW)
    {
        $arrival_entered = check_if_all_items_hav_arrival_date($project["project_order"], $logged_user["address"]);
        if ($arrival_entered == 0)
        {
            $recepients = array();
        }
    }

    // check if all positions have an expected date of arrival
    if (id() == REQUEST_FOR_DELIVERY_ACCEPTED)
    {
		$expected_arrival_entered = check_if_all_items_hav_expected_arrival_date($project["project_order"], $logged_user["address"]);
        if ($expected_arrival_entered == 0)
        {
            $recepients = array();
        }
    }

    // get standard text
    $string_name = "project_" .id();
    $message_text = get_string($string_name);
    // create E-Mails Subject
    $sql = "select order_shop_address_company, " .
           "order_shop_address_place, country_name " .
           "from projects " .
           "left join orders on order_id = project_order " .
           "left join countries on country_id = order_shop_address_country " .
           "where project_id = " . $project["project_id"];

    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
    $subject = $action_parameter["name"] . " - Project " . $project["order_number"] . ", " . $row["country_name"]  . ", " . $row["order_shop_address_company"];

    
    /********************************************************************
        build form
    *********************************************************************/
	$show_standardtexts = 0;
	$sql_taskworks = "select count(taskwork_id) as num_recs " .
		             "from taskworks " . 
		             "left join order_states on order_state_id = taskwork_order_state " . 
		             "where taskwork_productline = " . $project["project_product_line"] . 
		             " and taskwork_postype = " . $project["project_postype"] . 
		             " and order_state_code = " . id();

	$res_t = mysql_query($sql_taskworks) or dberror($sql_taskworks);
    $row_t = mysql_fetch_assoc($res_t);
	
	if($row_t["num_recs"] > 0)
	{
		$show_standardtexts = 1;
	}


	$addresses = array();
	foreach($recepients as $key=>$reciepient)
	{
		$addresses[] = $reciepient["address"];
	}

	$filter_taskworks = "";
	$sql_taskworks = "select taskwork_id, taskwork_shortcut, taskwork_address " .
		             "from taskworks " . 
		             "left join order_states on order_state_id = taskwork_order_state " . 
		             "where taskwork_productline = " . $project["project_product_line"] . 
		             " and taskwork_postype = " . $project["project_postype"] . 
		             " and order_state_code = " . id();

	
	$res_t = mysql_query($sql_taskworks) or dberror($sql_taskworks);
    while($row_t = mysql_fetch_assoc($res_t))
	{
		if($row_t["taskwork_address"] > 0)
		{
			foreach($addresses as $key=>$address)
			{
				if($row_t["taskwork_address"] == $address)
				{
					$filter_taskworks = " and (taskwork_address = " . $reciepient["address"] . " or  taskwork_address is Null) ";
					$sql_taskworks .= $filter_taskworks;
				}
			}
		}
	}

	if($filter_taskworks == "")
	{
		$filter_taskworks = " and taskwork_address is Null ";
		$sql_taskworks .= $filter_taskworks;
	}

    $form = new Form("addresses", "adress");

    $form->add_section("Project");
    $form->add_hidden("pid", param("pid"));
    $form->add_hidden("order_id", $project["project_order"]);

    require_once "include/project_head_small.php";

    $form->add_section("Email and Task Entry");
    $form->add_label("action_name", "Action to be Performed", 0, $action_parameter["action_name"]);
    $form->add_label("sender_name", "Sender", 0, $logged_user["contact"]);
    $form->add_hidden("sender_email",  $logged_user["email"]);
    $form->add_label("dummy1", "", 0, "");

    $form->add_edit("subject", "Subject*", NOTNULL, $subject, TYPE_CHAR);

	if($show_standardtexts == 1)
	{
		$form->add_list("standard_text", "Choose Standard Option", $sql_taskworks, SUBMIT);
		$form->add_multiline("body_text", "Mail Message*", 6, NOTNULL);
	}
	else
	{
		$form->add_multiline("body_text", "Mail Message*", 6, NOTNULL, $message_text);
	}

    
    $form->add_edit("due_date", "Due Date*", 0, "", TYPE_DATE, 20);

    // create recipient section depending on the action selected
    $form->add_comment("Select the recipients of your message from the following list.");

    foreach ($recepients as $key=>$value_array)
    {
        
		//get user_role
		$user_role = "";
		if($value_array["id"] == $project["project_retail_coordinator"]) {$user_role = "Project Manager";}
		elseif($value_array["id"] == $project["project_design_contractor"]) {$user_role = "Design Contractor";}
		elseif($value_array["id"] == $project["project_design_supervisor"]) {$user_role = "Design Supervisor";}
		elseif($value_array["id"] == $project["order_retail_operator"]) {$user_role = "Retail Coordinator";}
		elseif($value_array["id"] == $project["order_user"]) {$user_role = "Client";}
		elseif(array_key_exists('role', $user_data)){$user_role = $value_array["role"];}

	
		
		$form->add_checkbox("R" . $value_array["id"], $value_array["full_name"], "", 0, $user_role);
    }


	

    if (count($recepients)>0)
    {
        
		if (id() == LAYOUT_REQUEST_ACCEPTED and ($project["project_real_opening_date"] === Null or $project["project_real_opening_date"] == '0000-00-00'))
		{
			$form->error($error[12]);
		}
		else
		{
			$form->add_button("send", "Send");
		}
    }
    else
    {
		if (id() == ORDER_TO_SUPPLIER_SUBMITTED)
        {
            $form->error($error[2]);
        }
        else if (id() == REQUEST_FOR_DELIVERY_SUBMITTED)
        {
            $form->error($error[3]);
        }
        else if (id() == SUBMIT_BRIEFING_FOR_LAYOUT_PREVIEW)
        {
            $form->error($error[4]);
        }
        else if (id() == SUBMIT_REQUEST_FOR_DESIGN_APPROVAL)
        {
            $form->error($error[5]);
        }
        else if (id() == SUBMIT_REQUEST_FOR_OFFER)
        {
            $form->error($error[6]);
        }
        else if (id() == DELIVERY_CONFIRMED_FRW)
        {
            $form->error($error[7]);
        }
        else if (id() == REQUEST_FOR_BUDGET_APPROVAL_SUBMITTED)
        {
            $form->error($error[8]);
        }
        else if (id() == OFFER_SUBMITTED)
        {
            $form->error($error[9]);
        }
        else if (id() == CONFIRM_ORDER_BY_SUPPLIER)
        {
            $form->error($error[10]);
        }
        else if (id() == REQUEST_FOR_DELIVERY_ACCEPTED)
        {
            $form->error($error[11]);
        }
    }

    if (count($recepients)>0)
    {
        $show_copy_to = 1;
        foreach ($recepients as $key=>$value_array)
        {
            if($value_array["email"] == $logged_user["email"])
            {
                $show_copy_to = 0;
            }
        }
        if($show_copy_to == 1)
        {
            $copy_to_name = $logged_user["firstname"] . " " . $logged_user["name"] . " " . $logged_user["email"];
        
			$form->add_comment("Send a Copy of the Email to the Following Recepient");
            $form->add_checkbox("copy_to", $copy_to_name);
        }

		if(isset($action_parameter["order_state_email_copy_to_email"]) and $action_parameter["order_state_email_copy_to_email"])
		{
			if($show_copy_to != 1)
			{
				$form->add_comment("Send a Copy of the Email to the Following Recepient");
			}
			$form->add_checkbox("order_state_email_copy_to", $action_parameter["order_state_email_copy_to_email"]);
		}


		if($action_parameter["recipient"] == 'Supplier' or $action_parameter["recipient"] == 'Forwarder')
        {
                    
			$form->add_section("Delivery Request out of Sequence");
			$form->add_comment("Only use this option in case of a special delivery request where the project's state should not be changed!");
            $form->add_checkbox("keep_project_state", "keep project's state", 0, 0, "Special Delivery Request");

        }
		else
		{
			$form->add_hidden("keep_project_state", $keep_project_state);
		}

    }

	if(id() == 910) {
		$form->add_section("CC Recipients");
		$form->add_modal_selector("ccmails", "Selected Recipients", 8);
	}
	else
	{
		$form->add_hidden("ccmails");
	}

    $form->add_button(FORM_BUTTON_BACK, "Back");


    /********************************************************************
        Populate form and process button clicks
    *********************************************************************/ 
    $form->populate();
    $form->process();


    if ($form->button("standard_text"))
    {
		
		if($form->value("standard_text"))
		{
			$sql = "select taskwork_text, taskwork_alert_text from taskworks where taskwork_id = " . $form->value("standard_text");
			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);

			$form->value("body_text", $row["taskwork_text"]);

			if($row["taskwork_alert_text"])
			{
				$form->error($row["taskwork_alert_text"]);
			}
		}
		else
		{
			$string_name = "project_" .id();
			$form->value("body_text", get_string($string_name));
		}
	}
	elseif ($form->button("send"))
    {
        $num_mails = 0;
        $num_tasks = 0;

        if ($form->validate())
        {

            // send email
            $num_mails = 0;
            $mail_to_logged_user = 0;
			$selected_ccreciepients = array();
			$selected_ccreciepient_ids = array();
			$selected_reciepients = array();


			//get all ccmails
			$ccmails =  explode(' ', preg_replace("'\r?\n'"," ",$form->value('ccmails'))); 

			foreach($ccmails as $ccmail) {
				if(is_email_address($ccmail)) {
					
					$selected_ccreciepients[strtolower($ccmail)] = strtolower($ccmail);
				}
			
			}


			foreach($selected_ccreciepients as $key=>$email)
			{
				
				$sql = "select user_id, user_email_cc, user_email_deputy " . 
					   "from users " . 
					   "where user_email = " . dbquote($email);

				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					$selected_ccreciepient_ids[] = $row["user_id"];
					if($row["user_email_cc"])
					{
						$selected_ccreciepients[strtolower($row["user_email_cc"])] = strtolower($row["user_email_cc"]);
					}
					if($row["user_email_deputy"])
					{
						$selected_ccreciepients[strtolower($row["user_email_deputy"])] = strtolower($row["user_email_deputy"]);
					}
				}
			}


            if (count($recepients) > 0)
            {
				
				foreach ($recepients as $key=>$value_array)
                {
                    if ($form->value("R" . $value_array["id"]) == 1)
                    {
                        
						//check if there are order_items not having been ordered
						$num_recs = 0;
						$sql = "select count(order_item_id) as num_recs " .
							   "from order_items " .
							   " where order_item_order = " . $project["project_order"] . 
							   " and (order_item_ordered is null or order_item_ordered = '0000-00-00') " . 
							   " and (order_item_not_in_budget is null or order_item_not_in_budget =0) " . 
							   " and order_item_ordered_changes > 0 and order_item_supplier_address = " . $value_array["address"];

						$res = mysql_query($sql) or dberror($sql);
						if ($row = mysql_fetch_assoc($res))
						{
							$num_recs = $row["num_recs"];
						}

						$revision_subject = "";
						$revision_text = "";
						$order_revisioned = 0;
						if ($num_recs > 0 and id() == ORDER_TO_SUPPLIER_SUBMITTED and $value_array["order_revisions"] > 0)
                        {
							$revision_subject = " (Revisioned Order)";
							$revision_text = "\n\nPLEASE NOTE: This is a revisioned order!\n\n";
							$order_revisioned = 1;
						}

						$mail = new Mail();
                        $mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $form->value("subject") . $revision_subject);
                        $mail->set_sender($form->value("sender_email"), $form->value("sender_name"));

                        
						$bodytext0 = str_replace("\r\n", "\n", $form->value("body_text"))  . $revision_text;
						$link ="project_task_center.php?pid=" . param("pid");
						$bodytext = $bodytext0 . "\nClick below to have direct access to the project:\n";
						$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";
						
						$mail->add_text($bodytext);

						echo $bodytext . " <br />";

						

                        $mail->add_recipient($value_array["email"]);
						$selected_reciepients[strtolower($value_array["email"])] = strtolower($value_array["email"]);

                        if($value_array["cc"])
                        {
                            $mail->add_cc($value_array["cc"]);
							$selected_reciepients[strtolower($value_array["cc"])] = strtolower($value_array["cc"]);
                        }

                        if($value_array["deputy"])
                        {
                            //special case for bahrain and only for this guy
							if($value_array["deputy"] === 'skutty@rivoligroup.ae')
							{
								if($project["order_actual_order_state_code"] >= '620')
								{
									$mail->add_cc($value_array["deputy"]);
									$selected_reciepients[strtolower($value_array["deputy"])] = strtolower($value_array["deputy"]);
								}
							}
							else
							{
								$mail->add_cc($value_array["deputy"]);
								$selected_reciepients[strtolower($value_array["deputy"])] = strtolower($value_array["deputy"]);
							}
                        }

						foreach($selected_ccreciepients as $key=>$value)
						{
							if(!in_array($value, $selected_reciepients)) {
								$mail->add_cc($value);
								
							}
						}

                        $m_result = 0;
                        $m_result = $mail->send();

						append_mail($project["project_order"], $value_array["id"], user_id(), $bodytext0, id(), 1, $order_revisioned);

						foreach($selected_ccreciepient_ids as $key=>$user_id)
						{
							append_mail($project["project_order"], $user_id, user_id(), $bodytext0, id(), 1, $order_revisioned);
						}
						
						//delete arrays
						$selected_ccreciepients = array();
						$selected_ccreciepient_ids = array();

                        /*
                        //update mailmonitoring
                        $msql = "insert into mailmonitor (" .
                                "mfrom, mto, cc, deputee, " .
                                "mwhen, result) values (" .
                                dbquote($form->value("sender_email")) . ", " .
                                dbquote($value_array["email"]) . ", " .
                                dbquote($value_array["cc"]) . ", " .
                                dbquote($value_array["deputy"]) . ", " .
                                dbquote(date("Y-m-d H:i:s")) . ", " .
                                $m_result . ")";

                        $hm = mysql_query($msql) or dberror($msql);
                        */

                        $num_mails++;

                        // update dates (date of order)
                        if (id() == ORDER_TO_SUPPLIER_SUBMITTED)
                        {
                           set_visibility_for_suppliers($project["project_order"], $value_array["address"]);              append_order_dates($project["project_order"], $value_array["address"]);
                        }
                        if (id() == REQUEST_FOR_DELIVERY_SUBMITTED)
                        {
                            append_request_for_delivery_dates($project["project_order"], $value_array["address"]);
                        }
                        if (id() == SUBMIT_REQUEST_FOR_OFFER)
                        {
                           set_visibility_for_suppliers($project["project_order"], $value_array["address"]);
                        }

                   
                    }
                }

                if($show_copy_to == 1)
                {
                    if($form->value("copy_to") and $show_copy_to == 1)
                    {
                        
                        $revision_subject = "";
						$revision_text = "";

						if (id() == ORDER_TO_SUPPLIER_SUBMITTED and $value_array["order_revisions"] > 0)
                        {
							$revision_subject = " (Revisioned Order)";
							$revision_text = "\n\nPLEASE NOTE: This is a revisioned order!\n\n";
						}

						$mail = new Mail();
                        $mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $form->value("subject") . $revision_subject);
                        $mail->set_sender($form->value("sender_email"), $form->value("sender_name"));

                        $bodytext0 = str_replace("\r\n", "\n", $form->value("body_text")) . $revision_text;
                        $link ="project_task_center.php?pid=" . param("pid");
                        $bodytext = $bodytext0 . "Click below to have direct access to the project:\n";
                        $bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
                        $mail->add_text($bodytext);

                        $mail->add_recipient($logged_user["email"]);

						//add cc and deputee
						$sql = "select user_email_cc, user_email_deputy ".
							   "from users ".
							   "where (user_id = " . user_id() . 
							   "   and user_active = 1)";
						
						$res1 = mysql_query($sql) or dberror($sql);
						if ($row1 = mysql_fetch_assoc($res1))
						{
							if($row1["user_email_cc"])
							{
								$mail->add_cc($row1["user_email_cc"]);
							}
							if($row1["user_email_deputy"])
							{
								$mail->add_cc($row1["user_email_deputy"]);
							}
						}


                        $mail->send();
						append_mail($project["project_order"], user_id(), user_id(), $bodytext0, id(), 1);
						
                    }
                }


                if ($num_mails > 0 and $action_parameter["send_email"] == 1)
                {

                   //send mail also to order_state_email_copy_to
				   if($action_parameter["order_state_email_copy_to_email"] and $form->value("order_state_email_copy_to") == 1)
					{
						$revision_text = "";

						if (id() == ORDER_TO_SUPPLIER_SUBMITTED and $value_array["order_revisions"] > 0)
                        {
							$revision_subject = " (Revisioned Order)";
							$revision_text = "\n\nPLEASE NOTE: This is a revisioned order!\n\n";
						}

						$mail = new Mail();
                        $mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $form->value("subject") . $revision_subject);
                        $mail->set_sender($form->value("sender_email"), $form->value("sender_name"));

                        $bodytext0 = str_replace("\r\n", "\n", $form->value("body_text")) . $revision_text;
                        $link ="project_task_center.php?pid=" . param("pid");
                        $bodytext = $bodytext0 . "Click below to have direct access to the project:\n";
                        $bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
                        $mail->add_text($bodytext);

						$mail->add_recipient($action_parameter["order_state_email_copy_to_email"]);

						

                        $mail->send();


						// retail operator
                        $sql = "select user_id from users ".
                               "where user_email = " . dbquote($action_parameter["order_state_email_copy_to_email"]);
                        $res = mysql_query($sql) or dberror($sql);

                        if ($row = mysql_fetch_assoc($res))
                        {
							append_mail($project["project_order"], $row["user_id"], user_id(), $bodytext0, id(), 1);
						}

					}
				   
				   //send a mail also to retail operator if budget is aproved
                    if (id() == BUDGET_APPROVED)
                    {
                        // retail operator
                        $sql = "select user_id, address_id, ".
                               "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
                               "    user_email, user_email_cc, user_email_deputy ".
                               "from users ".
                               "left join addresses on user_address = address_id ".
                               "where user_email<>'' ".
                               "    and user_active = 1 " .
                               "    and user_id = " . dbquote($project["order_retail_operator"]);

                        $res = mysql_query($sql) or dberror($sql);

                        if ($row = mysql_fetch_assoc($res))
                        {
                            if($row["user_email"])
                            {
                                
                                $mail = new Mail();
                                $mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $form->value("subject"));
                                $mail->set_sender($form->value("sender_email"), $form->value("sender_name"));

                                $bodytext0 = str_replace("\r\n", "\n", $form->value("body_text")) . "\n\n";
                                $link ="project_task_center.php?pid=" . param("pid");
                                $bodytext = $bodytext0 . "Click below to have direct access to the project:\n";
                                $bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
                                $mail->add_text($bodytext);
                                $mail->add_recipient($row["user_email"]);
                                
                                $mail->send();
                            }
                        }
                    }
                }
            }
            
            // append task entry
            $num_tasks = 0;
            if (count($recepients) > 0)
            {
                if ($form->value("body_text"))
                {
                    if ($form->value("due_date"))
                    {
                        $date = from_system_date($form->value("due_date"));
                    }
                    else
                    {
                        $date = date("Y-m-d");
                    }
                    $link ="project_task_center.php?pid=" . param("pid");
                    foreach ($recepients as $key=>$value_array)
                    {
                        if ($form->value("R" . $value_array["id"]) == 1)
                        {
                           delete_user_tasks($project["project_order"], $value_array["id"], $form->value("body_text"), $link, $date, user_id(), id(), 1);
                           
                           if ($action_parameter["append_task"] == 1)
                           {
                               
							   
								$revision_text = "";

								if (id() == ORDER_TO_SUPPLIER_SUBMITTED and $value_array["order_revisions"] > 0)
								{
									$revision_text = " PLEASE NOTE: This is a revisioned order!";
								}
							   
							   if($value_array["role"] != "Additional Recipient")
							   {
									append_task($project["project_order"], $value_array["id"], $form->value("body_text") . $revision_text, $link, $date, user_id(), id(), 1, $form->value("keep_project_state"));
							   }

                           }
                           $num_tasks++;
                           
						   if ($action_parameter["send_email"] == 1)
                           {
							   //append_mail($project["project_order"], $value_array["id"], user_id(), $bodytext0, id(), 1);
                           }
                        }
                    }
                }
            }


			// append auto item to the list of materials
            if (count($recepients) > 0 and $show_standardtexts == 1 and $form->value("standard_text") > 0)
            {
				$taskwork_id =  $form->value("standard_text");
				//append_auto_item($project["project_order"], $taskwork_id); //appneds the item to the list of materials			
			}

            if ($num_mails > 0 or $num_tasks > 0)
            {
                
                //set task for design contractor
                if (id() == REQUEST_FOR_LAYOUT_PREVIEW_ACCEPTED)
                {
                    append_task($project["project_order"], user_id(), "Please submit briefing for approval!", $link, $date, user_id(), id(), 1, $form->value("keep_project_state"));

                }
                elseif (id() == LAYOUT_PREVIEW_ACCEPTED)
                {
					//add notification_reciepients
                    if(count($notification_reciepients4) > 0)
                    {
                        $new_recipient = 0;
						$mail = new Mail();
                        $mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $form->value("subject"));
                        $mail->set_sender($form->value("sender_email"), "retail net");

                        $bodytext0 = "The Preview was accepted by the client \n\n";
                        $link ="project_view_client_data.php?pid=" . param("pid");
                        $bodytext = $bodytext0 . "Click below to have direct access to the project:\n";
                        $bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
                        $mail->add_text($bodytext);

                        foreach($notification_reciepients4 as $key=>$value)
                        {
                            if(!in_array($value, $mail->recipients)) {
								$mail->add_recipient($value);
								$new_recipient = 1;
							}
                        }

                        if($new_recipient == 1) {
							$mail->send();
						}
                    }
                }
                elseif (id() == REQUEST_FOR_BOOKLET_ACCEPTED)
                {
                    append_task($project["project_order"], user_id(), "Please submit booklet for approval!", $link, $date, user_id(), id(), 1, $form->value("keep_project_state"));
                }

                // set task for supplier
                if (id() == REQUEST_FOR_OFFER_ACCEPTED)
                {
                    append_task($project["project_order"], user_id(), "Please submit your offer!", $link, $date, user_id(), id(), 1, $form->value("keep_project_state"));
                }

                // set task for forwarder
                elseif (id() == REQUEST_FOR_DELIVERY_ACCEPTED)
                {
                    append_task($project["project_order"], user_id(), "Please confirm delivery on arrival!", $link, $date, user_id(), id(), 1, $form->value("keep_project_state"));
                }
                elseif (id() == LAYOUT_ACCEPTED)
                {
					//add notification_reciepients
                    if(count($notification_reciepients3) > 0)
                    {
                        $new_recipient = 0;
						$mail = new Mail();
                        $mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $form->value("subject"));
                        $mail->set_sender($form->value("sender_email"), "retail net");

                        $bodytext0 = "The Layout was accepted by the client \n\n";
                        $link ="project_view_client_data.php?pid=" . param("pid");
                        $bodytext = $bodytext0 . "Click below to have direct access to the project:\n";
                        $bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
                        $mail->add_text($bodytext);

                        foreach($notification_reciepients3 as $key=>$value)
                        {
                            if(!in_array($value, $mail->recipients)) {
								$mail->add_recipient($value);
								$new_recipient = 1;
							}
                        }

                        if($new_recipient == 1) {
							$mail->send();
						}
                    }
					
					// send mail also to the retail operator
					$sql = "select user_id, address_id, ".
						   "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
						   "    user_email, user_email_cc, user_email_deputy ".
						   "from users ".
						   "left join addresses on user_address = address_id ".
						   "where user_email<>'' ".
						   "    and user_active = 1 " .
						   "    and user_id = " . dbquote($project["order_retail_operator"]);

					$res = mysql_query($sql) or dberror($sql);

					if ($row = mysql_fetch_assoc($res))
					{
						if($row["user_email"])
						{
							
							$mail = new Mail();
							$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $form->value("subject"));
							$mail->set_sender($form->value("sender_email"), $form->value("sender_name"));

							
							$bodytext0 = "The Layout was accepted by the client \n\n";
							$link ="project_view_client_data.php?pid=" . param("pid");
							$bodytext = $bodytext0 . "Click below to have direct access to the project:\n";
							$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
							$mail->add_text($bodytext);
							
							$mail->add_recipient($row["user_email"]);
						
							$mail->send();
						}
					}

                }
                elseif (id() == REQUEST_FOR_BUDGET_APPROVAL_SUBMITTED)
                {
                    //add notification_reciepients
                    if(count($notification_reciepients1) > 0)
                    {
                        $new_recipient = 0;
						$mail = new Mail();
                        $mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $form->value("subject"));
                        $mail->set_sender($form->value("sender_email"), "retail net");

                        $bodytext0 = "The Budget was submittet to the client for approval \n\n";
                        $link ="project_view_project_budget.php?pid=" . param("pid");
                        $bodytext = $bodytext0 . "Click below to have direct access to the project:\n";
                        $bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
                        $mail->add_text($bodytext);

                        foreach($notification_reciepients1 as $key=>$value)
                        {
                            if(!in_array($value, $mail->recipients)) {
								$mail->add_recipient($value);
								$new_recipient = 1;
							}
                        }

                        if($new_recipient == 1) {
							$mail->send();
						}
                    }
                }
                elseif (id() == BUDGET_APPROVED)
                {
                   
                   set_visibility_for_suppliers($project["project_order"], "");
                   set_visibility_in_delivery_schedule($project["project_order"]);



                    //add notification_reciepients

					
                    if(count($notification_reciepients2) > 0)
                    {
                        $new_recipient = 0;
						$mail = new Mail();
                        $mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $form->value("subject"));
                        $mail->set_sender($form->value("sender_email"), "retail net");

                        $bodytext0 = "The project budget was approved \n\n";
                        $link ="project_view_project_budget.php?pid=" . param("pid");
                        $bodytext = $bodytext0 . "Click below to have direct access to the project:\n";
                        $bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
                        $mail->add_text($bodytext);

                        foreach($notification_reciepients2 as $key=>$value)
                        {
                            if(!in_array($value, $mail->recipients)) {
								$mail->add_recipient($value);
								$new_recipient = 1;
							}

                        }

                        if($new_recipient == 1) {
							$mail->send();
						}
                    }
                }
				elseif (id() == REQUEST_FOR_DELIVERY_SUBMITTED)
                {
					set_visibility_in_delivery_schedule($project["project_order"]);
				}
                elseif (id() == DELIVERY_CONFIRMED_FRW)
                {
                    $arrival_entered = check_if_all_items_hav_arrival_date($project["project_order"], "");
                    if ($arrival_entered == 1)
                    {
                        // get mailtext and email address of client
                        $text = "";

                        $sql = "select string_text ".
                               "from strings ".
                               "where string_name = 'general_750p'";

                        $res = mysql_query($sql) or dberror($sql);
    
                        if ($row = mysql_fetch_assoc($res))
                        {
                            $text = $row["string_text"];
                        }

                        //send email to client
                        $mail = new Mail();
                        $mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $form->value("subject"));
                        $mail->set_sender($form->value("sender_email"), "retail net");

                        $bodytext0 = $text . "\n\n";
                        $link ="project_task_center.php?pid=" . param("pid");
                        $bodytext = $bodytext0 . "Click below to have direct access to the project:\n";
                        $bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
                        $mail->add_text($bodytext);

                        $mail->add_recipient($client_user["email"]);
                        if($client_user["cc"])
                        {
                            $mail->add_cc($client_user["cc"]);
                        }
                        if($client_user["deputy"])
                        {
                            //special case for bahrain
							if($client_user["deputy"] == 'skutty@rivoligroup.ae')
							{
								if($project["order_actual_order_state_code"] >= '620')
								{
									$mail->add_cc($client_user["deputy"]);
								}
							}
							else
							{
								$mail->add_cc($client_user["deputy"]);
							}
							
                        }
                        
                        $mail->send();


                        append_mail($project["project_order"], $project["order_user"], user_id(), $bodytext0, id(), 1);

                        append_task2($project["project_order"], $project["order_user"], "Please confirm delivery on arrival!", $link, $date, user_id(), id(), 1);
                    }
                }
                elseif (id() == DELIVERY_CONFIRMED)
                {
                    // delete predecessor task
                    $sql = "delete from tasks ".
                           "where task_user = " . user_id() .
                           "   and task_order = " . $project["project_order"];

                    mysql_query($sql) or dberror($sql);
                }

                // append record to table actual_order_states
				append_order_state($project["project_order"], id(), 1, 1, $form->value("keep_project_state"));

                $params = "?pid=" . param("pid") . "&num_mails=" . $num_mails . "&num_tasks=" . $num_tasks;
                $link = "project_send_message_confirm.php" . $params;
                $link = "project_task_center.php?pid=" . param("pid");
                
				redirect ($link);
            }
            if ($num_mails == 0 and $num_tasks == 0)
            {
                $form->error($error[1]);
            }

        }
    }

      
    /********************************************************************
        render page
    *********************************************************************/
    $page = new Page("projects");

    require "include/project_page_actions.php";

    $page->header();
    $page->title($action_parameter["action_name"]);
    $form->render();

	?>

	<script type="text/javascript">
	  jQuery(document).ready(function($) {
	  $('#ccmails_selector').click(function(e) {
		e.preventDefault();
		$.nyroModalManual({
		  url: '/shared/select_mail_recipients.php'
		});
		return false;
	  });
	});
	</script>

	<?php

    $page->footer();
}

?>