<?php
/********************************************************************

    project_costs_costsheet_bids.php

    View or edit bids for a project

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-01-09
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2012, OMEGA SA, All Rights Reserved.

*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/project_cost_functions.php";

check_access("can_edit_local_constrction_work");

if(!param("pid"))
{
	$link = "welcome.php";
	redirect($link);
}



/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));
// get company's address
$client_address = get_address($project["order_client_address"]);

$currency_symbol = get_currency_symbol($project["order_client_currency"]);


/********************************************************************
    check if project cost positions are present and create if not
*********************************************************************/

$sql = "select count(costsheet_id) as num_recs from costsheets " . 
       " where costsheet_project_id = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

if($row["num_recs"] == 0) //make the user select a cost template
{
	$link = "project_costs_overview.php?pid=" . param("pid");
	redirect($link);
}


/********************************************************************
    check if bid positions are present 
*********************************************************************/
$has_positions = true;

$sql = "select count(costsheet_bid_position_id) as num_recs from costsheet_bid_positions " . 
       " where costsheet_bid_position_costsheet_bid_id = " . id();

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

if($row["num_recs"] == 0) 
{
	$has_positions = false;
}

/********************************************************************
	prepare data
*********************************************************************/ 
$sql_cost_groups = "select DISTINCT costsheet_pcost_group_id, " . 
		               "concat(pcost_group_code, ' ', pcost_group_name) as costgroup " .
		               "from costsheets " .
					   "left join pcost_groups on pcost_group_id = costsheet_pcost_group_id " .
					   "where costsheet_project_id = " . param("pid") . 
					   " order by costgroup";


$sql_bids = "select costsheet_bid_id, costsheet_bid_company " . 
            " from costsheet_bids " . 
			" where costsheet_bid_project_id = " . param("pid") . 
			" order by costsheet_bid_company";

$has_bids = false;

$res = mysql_query($sql_bids) or dberror($sql_bids);
if($row = mysql_fetch_assoc($res))
{
	$has_bids = true;
}

/********************************************************************
	Create Form
*********************************************************************/ 

$form = new Form("costsheet_bids", "costsheet_bids");


$form->add_section("Project");
$form->add_hidden("pid", param('pid'));


require_once "include/project_head_small.php";

$form->add_section("General Information");
$form->add_hidden("costsheet_bid_project_id", param("pid"));
$form->add_edit("costsheet_bid_company", "Company*", NOTNULL);
$form->add_edit("costsheet_bid_date", "Date*", NOTNULL, "", TYPE_DATE);
$form->add_multiline("costsheet_bid_remark", "Remarks", 4);




if($has_positions == true)
{
	$form->add_hidden("new_sheet", 0);
}
else
{
	$form->add_hidden("new_sheet", 1);
	
	if($has_bids == true)
	{
		$form->add_section("Copy from existing Bid");
		$form->add_comment("Copy sheet structure from an existing bid.");
		$form->add_list("bid_id", "Existing Bid", $sql_bids, SUBMIT);
	}
	else
	{
		$form->add_hidden("bid_id", 0);
	}
	
	
	$form->add_section("Costgroups");
	$form->add_comment("Please select the cost groups to be contained in the bid form.");
	
	$check_box_names = array();
	$res = mysql_query($sql_cost_groups) or dberror($sql_cost_groups);
	while($row = mysql_fetch_assoc($res))
	{
		$form->add_checkbox("CG" . $row["costsheet_pcost_group_id"], "", 0, 0, $row["costgroup"]);
		$check_box_names["CG" . $row["costsheet_pcost_group_id"]] = $row["costsheet_pcost_group_id"];
	}
}


//$form->add_list("costsheet_bid_pcost_group_id", "Cost Group*",$sql_cost_groups, NOTNULL);



$link = "javascript:popup('/user/project_costs_bid_pdf.php?pid=" . param("pid") . "&bid=" . id() . "', 800, 600);";
$form->add_button("print_bid", "Print Bid", $link);

//if(has_access("can_edit_local_constrction_work") and $project["order_budget_is_locked"] == 0)
if(has_access("can_edit_local_constrction_work"))
{
	
	if($project["order_budget_is_locked"] == 0)
	{
		if(id() > 0)
		{
			$form->add_button("delete", "Delete this Bid");
		}
		
		$form->add_button("save", "Save General Information");
		$form->add_button("update_budget", "Update Budget from this Bid");
	}
}

	

$form->add_button("back", "Back");

if($project["order_budget_is_locked"] == 1)
{
	$form->error("Budget is locked, no more changes can be made!");
}

$form->populate();
$form->process();


if($form->button("save") or $form->button("bid_id"))
{
	
	
	if($form->validate())
	{
		$form->save();

		if($form->value("new_sheet") == 1)
		{
			$new_bid = mysql_insert_id();
			
			if($form->button("bid_id") or $form->value("bid_id") > 0 and $new_bid > 0)
			{
				$sql = "select * from costsheet_bid_positions " . 
					   "where costsheet_bid_position_costsheet_bid_id = " . $form->value("bid_id");

				$res = mysql_query($sql) or dberror($sql);
				while($row = mysql_fetch_assoc($res))
				{
					$fields = array();
					$values = array();

					$fields[] = "costsheet_bid_position_costsheet_bid_id";
					$values[] = $new_bid;

					$fields[] = "costsheet_bid_position_project_id";
					$values[] = dbquote(param("pid"));
					
					$fields[] = "costsheet_bid_position_costsheet_id";
					$values[] = dbquote($row["costsheet_bid_position_costsheet_id"]);

					$fields[] = "costsheet_bid_position_pcost_group_id";
					$values[] = dbquote($row["costsheet_bid_position_pcost_group_id"]);

					$fields[] = "costsheet_bid_position_pcost_subgroup_id";
					$values[] = dbquote($row["costsheet_bid_position_pcost_subgroup_id"]);

					$fields[] = "costsheet_bid_position_code";
					$values[] = dbquote($row["costsheet_bid_position_code"]);

					$fields[] = "costsheet_bid_position_text";
					$values[] = dbquote($row["costsheet_bid_position_text"]);

					$fields[] = "date_created";
					$values[] = "now()";

					$fields[] = "date_modified";
					$values[] = "now()";

					$fields[] = "user_created";
					$values[] = dbquote(user_login());

					$fields[] = "user_modified";
					$values[] = dbquote(user_login());

					$sql = "insert into costsheet_bid_positions (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					
					mysql_query($sql) or dberror($sql);
				}
				$link = "project_costs_bid.php?pid=" . param("pid"). "&id=" . $new_bid;
				redirect($link);
			}
			else
			{
				$selected_costgroups = array();
				foreach ($check_box_names as $key=>$value)
				{
					if ($form->value($key))
					{
						$selected_costgroups[] = $value;
					}
				}
				if(count($selected_costgroups) > 0)
				{
					$result = create_bid_positions( id(), param("pid"), $selected_costgroups);
					$link = "project_costs_bid.php?id=". id() . "&pid=" . param("pid");
					redirect($link);

				}
				else
				{
					$form->error("Please select at least one cost group.");
				}
			}
		}
		else
		{
			//update budget with selected positions
			$form->message("Your data has been saved.");
		}
	}

}
elseif($form->button("update_budget"))
{
	//update budget with selected positions
	$result = update_costsheet_budget_from_bid(id());

	$form->message("Your data has been saved.");
}
elseif($form->button("back"))
{
	$link = "project_costs_bids.php?pid=" . param("pid");
	redirect($link);
}
elseif($form->button("delete"))
{
	
	//check if bid is used in budget
	$sql = "select count(costsheet_bid_position_id) as num_recs from costsheet_bid_positions " . 
		   " where costsheet_bid_position_is_in_budget = 1 and costsheet_bid_position_costsheet_bid_id = " . id();
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);

	if($row["num_recs"] == 0) 
	{
		$sql = "delete from costsheet_bid_positions " . 
				   "where costsheet_bid_position_costsheet_bid_id = " . id();
			mysql_query($sql) or dberror($sql);

			$sql = "delete from costsheet_bids " . 
						   "where costsheet_bid_id = " . id();
					mysql_query($sql) or dberror($sql);

			$link = "project_costs_bids.php?pid=" . param("pid");
			redirect($link);
	}
	else
	{
		$form->error("The bid can not be deleted since it is used in the project's budget.");
	}
	
	
}





/********************************************************************
	Create list of cost positions
*********************************************************************/ 
$list_names = array();
$checkbox_ids = array();
$delete_ids = array();
$list_has_positions = array();
if($has_positions == true)
{	
	//get data 
	$bid_totals = get_project_bid_totals(id());


	$code_data = array();
	$bid_data = array();
	$currency_data = array();
	$text_data = array();
	$comment_data = array();
	$in_budget = array();
	

	$sql = "select costsheet_bid_position_id, costsheet_bid_position_pcost_group_id, " . 
		   "costsheet_bid_position_code, costsheet_bid_position_text, " . 
		   "costsheet_bid_position_amount, costsheet_bid_position_comment, costsheet_bid_position_is_in_budget " . 
		   "from costsheet_bid_positions " .
		   "where costsheet_bid_position_costsheet_bid_id =" .id();

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$code_data[$row["costsheet_bid_position_id"]] = $row["costsheet_bid_position_code"];
		$text_data[$row["costsheet_bid_position_id"]] = $row["costsheet_bid_position_text"];
		$bid_data[$row["costsheet_bid_position_id"]] = $row["costsheet_bid_position_amount"];
		$currency_data[$row["costsheet_bid_position_id"]] = $currency_symbol;
		$comment_data[$row["costsheet_bid_position_id"]] = $row["costsheet_bid_position_comment"];
		$in_budget[$row["costsheet_bid_position_id"]] = $row["costsheet_bid_position_is_in_budget"];
		$checkbox_ids[$row["costsheet_bid_position_pcost_group_id"]][$row["costsheet_bid_position_id"]] = "__costsheet_bid_positions_costsheet_bid_position_is_in_budget_" . $row["costsheet_bid_position_id"];
		$delete_ids[$row["costsheet_bid_position_pcost_group_id"]][$row["costsheet_bid_position_id"]] = "__costsheet_bid_positions_select_to_delete_" . $row["costsheet_bid_position_id"];
		
		$list_has_positions[$row["costsheet_bid_position_pcost_group_id"]] = true;
	}

	


	//add all cost groups and cost sub groups
	$save_button_names = array();
	$add_button_names = array();
	$remove_button_names = array();
	$remove_button2_names = array();
	$select_button_names = array();
	$group_ids = array();
	$group_titles = array();
	

	$sql = "select DISTINCT costsheet_bid_position_pcost_group_id, pcost_group_code, pcost_group_name " .
		   "from costsheet_bid_positions " .
		   "left join pcost_groups on pcost_group_id = costsheet_bid_position_pcost_group_id " . 
		   "where costsheet_bid_position_costsheet_bid_id = " . id() . 
		   " order by pcost_group_code";


	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$listname = "list" . $row["pcost_group_code"];
		$save_buttonname = "save_cost" . $row["pcost_group_code"];
		$add_buttonname = "add_new" . $row["pcost_group_code"];
		$select_buttonname = "select_all" . $row["pcost_group_code"];
		$remove_buttonname = "remove_cost" . $row["pcost_group_code"];
		$remove_buttonname2 = "remove2_cost" . $row["pcost_group_code"];
		$list_names[] = $listname;
		$save_button_names[] = $save_buttonname;
		$add_button_names[] = $add_buttonname;
		$select_button_names[] = $select_buttonname;
		$remove_button_names[] = $remove_buttonname;
		$remove_button2_names[] = $remove_buttonname2;
		$group_ids[] = $row["costsheet_bid_position_pcost_group_id"];
		$group_titles[] = $row["pcost_group_name"];

		
		$sql = "select costsheet_bid_position_id, costsheet_bid_position_code, costsheet_bid_position_text, " . 
			   "costsheet_bid_position_amount, costsheet_bid_position_comment, costsheet_bid_position_is_in_budget, " . 
			   "concat(pcost_subgroup_code, ' ', pcost_subgroup_name) as subgroup " . 
			   "from costsheet_bid_positions " .
			   "left join pcost_groups on pcost_group_id = costsheet_bid_position_pcost_group_id " .
			   " left join pcost_subgroups on pcost_subgroup_id = costsheet_bid_position_pcost_subgroup_id"; 

		$list_filter = "costsheet_bid_position_costsheet_bid_id = " . id() . " and costsheet_bid_position_pcost_group_id = " . $row["costsheet_bid_position_pcost_group_id"];
		
		$toggler = '<div class="toggler_pointer" id="l' . $row["costsheet_bid_position_pcost_group_id"] . '_on"><span class="fa fa-minus-square toggler"></span>' .$row["pcost_group_name"] . '</div>';
		
		$$listname = new ListView($sql, LIST_HAS_HEADER | LIST_HAS_FOOTER);

		$$listname->set_entity("costsheet_bid_positions");
		$$listname->set_order("LENGTH(costsheet_bid_position_code), COALESCE(costsheet_bid_position_code,'Z')");
		$$listname->set_group("subgroup");
		$$listname->set_filter($list_filter);
		$$listname->set_title($toggler);

		
		$link = "javascript:select_all_cost_positions_for_budget(" . $row["costsheet_bid_position_pcost_group_id"]. ")";
		$c1 = '<a href"javascript:void(0);" onclick="' .$link . '"><span class="fa fa-check-square-o checker"></span></a><span id="selector' . $row["costsheet_bid_position_pcost_group_id"] . '"> Budget</span>';

		$$listname->add_checkbox_column("costsheet_bid_position_is_in_budget", $c1, COLUMN_UNDERSTAND_HTML, $in_budget);
		$$listname->add_edit_column("costsheet_bid_position_code", "Code", 4, 0, $code_data);
		$$listname->add_edit_column("costsheet_bid_position_text", "Text", 70, 0, $text_data, 'textarea', 3);
		$$listname->add_edit_column("costsheet_bid_position_amount", "Costs", 12, 0, $bid_data);
		$$listname->add_text_column("currency", "", 0, $currency_data);
		$$listname->add_edit_column("costsheet_bid_position_comment", "Comment", 30, 0, $comment_data, 'textarea', 3);
		
		
		
		
		$link = "javascript:select_all_delete_positions(" . $row["costsheet_bid_position_pcost_group_id"]. ")";
		$c2 = '<a href"javascript:void(0);" onclick="' .$link . '"><span class="fa fa-check-square-o checker"></span></a><span id="selector2' . $row["costsheet_bid_position_pcost_group_id"] . '"> </span>';

		$$listname->add_checkbox_column("select_to_delete", $c2, COLUMN_UNDERSTAND_HTML);


		
		if(array_key_exists($row["costsheet_bid_position_pcost_group_id"], $list_has_positions))
		{
			foreach($bid_totals["subgroup_totals"] as $subgroup=>$bid_sub_group_total)
			{
				$$listname->set_group_footer("costsheet_bid_position_text", $subgroup , "Subgroup Total");
				$$listname->set_group_footer("costsheet_bid_position_amount",  $subgroup , number_format($bid_sub_group_total, 2));
			}
					
			$$listname->set_footer("costsheet_bid_position_code", "Total");
			$$listname->set_footer("costsheet_bid_position_amount", number_format($bid_totals["group_totals"][$row["costsheet_bid_position_pcost_group_id"]], 2));
			
			
			//buttons
			
			if($project["order_budget_is_locked"] == 0)
			{
				$link = "javascript:add_new_cost_position(" . param("pid") .", " . $row["costsheet_bid_position_pcost_group_id"] . ", " . id() . ")";
				$$listname->add_button($add_buttonname, "Add More Cost Positions", $link);
				$$listname->add_button($remove_buttonname, "Remove Empty Positions");
				$$listname->add_button($remove_buttonname2, "Remove Selected Positions");

				$$listname->add_button($save_buttonname, "Save List");
			}

			
			

			
		}

		$$listname->populate();
		$$listname->process();
	}




	foreach($save_button_names as $key=>$buttonname)
	{
		if($form->button($buttonname) or ($form->button("save") and $form->value("new_sheet") == 0))
		{
			$list = "";
			foreach($list_names as $key=>$listname)
			{
				$list = $listname;
			}

			
			$sql = "update costsheet_bid_positions set costsheet_bid_position_is_in_budget = 0 " . 
				   "where costsheet_bid_position_costsheet_bid_id = " . id();
			mysql_query($sql) or dberror($sql);
			foreach ($$list->values("costsheet_bid_position_is_in_budget") as $key=>$value)
			{
				
				if(array_key_exists("__costsheet_bid_positions_costsheet_bid_position_is_in_budget_" . $key, $_POST))
				{
					$fields = array();

					$fields[] = "costsheet_bid_position_is_in_budget = 1";

					$sql = "update costsheet_bid_positions set " . join(", ", $fields) . " where costsheet_bid_position_id = " . $key;
					mysql_query($sql) or dberror($sql);
				}
			}
			
			
			foreach ($$list->values("costsheet_bid_position_code") as $key=>$value)
			{
				$fields = array();

				$fields[] = "costsheet_bid_position_code = " . dbquote($value);

				$sql = "update costsheet_bid_positions set " . join(", ", $fields) . " where costsheet_bid_position_id = " . $key;
				mysql_query($sql) or dberror($sql);
			}

			foreach ($$list->values("costsheet_bid_position_text") as $key=>$value)
			{
				$fields = array();

				$fields[] = "costsheet_bid_position_text = " . dbquote($value);

				$sql = "update costsheet_bid_positions set " . join(", ", $fields) . " where costsheet_bid_position_id = " . $key;
				mysql_query($sql) or dberror($sql);
			}

			foreach ($$list->values("costsheet_bid_position_amount") as $key=>$value)
			{
				$value = trim(str_replace("'", "", $value));
				if(is_numeric($value))
				{
					$fields = array();

					$fields[] = "costsheet_bid_position_amount = " . dbquote($value);

					$sql = "update costsheet_bid_positions set " . join(", ", $fields) . " where costsheet_bid_position_id = " . $key;
					mysql_query($sql) or dberror($sql);
				}
				
			}

			foreach ($$list->values("costsheet_bid_position_comment") as $key=>$value)
			{
				$fields = array();

				$fields[] = "costsheet_bid_position_comment = " . dbquote($value);
				$fields[] = "date_modified = " . dbquote(date("Y-m-d H:i:s"));
				$fields[] = "user_modified = " . dbquote(user_login());

				$sql = "update costsheet_bid_positions set " . join(", ", $fields) . " where costsheet_bid_position_id = " . $key;
				mysql_query($sql) or dberror($sql);
				
			}

			//update budget with selected positions
			//$result = update_costsheet_budget_from_bid(id());

			
			$link = "project_costs_bid.php?id=". id() . "&pid=" . param("pid");
			redirect($link);
		}
	}

	foreach($remove_button_names as $key=>$buttonname)
	{
		if($form->button($buttonname))
		{
			

			$sql = "delete from costsheet_bid_positions " . 
				   "where (costsheet_bid_position_text is null or costsheet_bid_position_text = '') and costsheet_bid_position_amount = 0 and costsheet_bid_position_costsheet_bid_id = " . id();

			mysql_query($sql) or dberror($sql);

			
			$link = "project_costs_bid.php?id=" . id() . "&pid=" . param("pid");
			redirect($link);
		}
	}


	foreach($remove_button2_names as $key=>$buttonname)
	{
		if($form->button($buttonname))
		{
			$list = "";
			foreach($list_names as $key=>$listname)
			{
				$list = $listname;
			}

			foreach ($$list->values("select_to_delete") as $key=>$value)
			{
				if($value)
				{
					$sql = "delete from costsheet_bid_positions where costsheet_bid_position_id = " . $key;
					mysql_query($sql) or dberror($sql);
				}
				
			}
			
			//if all positions were removed
			$sql = "select count(costsheet_bid_position_id) as num_recs from costsheet_bid_positions " . 
				   " where costsheet_bid_position_costsheet_bid_id = " . id();
			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);

			$link = "project_costs_bid.php?id=" . id() . "&pid=" . param("pid");
			redirect($link);
		}
	}

	
}

$page = new Page("projects");


require "include/project_page_actions.php";

$page->header();
$page->title(id() ? "Project Costs - Edit Bid" : "Project Costs - Add Bid");

require_once("include/costsheet_tabs.php");
$form->render();


foreach($list_names as $key=>$listname)
{
	
	if(array_key_exists("costsheet", $_SESSION) and array_key_exists($listname, $_SESSION["costsheet"]))
	{
		if($_SESSION["costsheet"][$listname] == 0)
		{
			echo '<p>&nbsp;</p>';
			$toggler = '<div class="toggler_pointer" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
			echo $toggler;
			
			echo '<div style="display:none;" id="' . $listname . '">';
			$$listname->render();
			echo '</div>';
		}
		else
		{
			echo '<p>&nbsp;</p>';
			$toggler = '<div class="toggler_pointer toggler_pointer_off" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
			echo $toggler;
			
			echo '<div id="' . $listname . '">';
			$$listname->render();
			echo '</div>';
		}
	}
	else
	{
		echo '<p>&nbsp;</p>';
		$toggler = '<div class="toggler_pointer toggler_pointer_off" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
		echo $toggler;
		
		echo '<div id="' . $listname . '">';
		$$listname->render();
		echo '</div>';
	}
}


?>
<script language="javascript">
jQuery(document).ready(function($) {
	<?php
	foreach($list_names as $key=>$listname)
	{
	?>
		$('#l<?php echo $group_ids[$key];?>_on').click(function()  {
			$('#<?php echo $listname;?>').css('display', 'none');
			$('#l<?php echo $group_ids[$key];?>_off').css('display', 'block');

			$.ajax({
				type: "POST",
				data: "listname=<?php echo $listname;?>&visibility=0",
				url: "../shared/ajx_costsheet_liststaes.php",
				success: function(msg){
				}
			});
		});
		
		$('#l<?php echo $group_ids[$key];?>_off').click(function()  {
			$('#<?php echo $listname;?>').css('display', 'block');
			$('#l<?php echo $group_ids[$key];?>_off').css('display', 'none');

			$.ajax({
				type: "POST",
				data: "listname=<?php echo $listname;?>&visibility=1",
				url: "../shared/ajx_costsheet_liststaes.php",
				success: function(msg){
				}
			});
		});
	<?php
	}
	?>
});


function add_new_cost_position(pid, gid, bid)
{		
	url = '/user/project_costs_add_positions.php?pid=' + pid + '&gid=' + gid + '&mode=bid' + '&bid=' + bid;
	$.nyroModalManual({
	  url: url
	});

}

function select_all_cost_positions_for_budget(gid)
{
	var selector = "#selector" + gid;

	if($(selector).html() == ' Budget ')
	{
		<?php
			foreach($checkbox_ids as $key=>$ids)
			{
				?>
				if(<?php echo $key;?> == gid){
					<?php
						foreach($ids as $key2=>$id)
						{
							echo "$('#" . $id . "').attr('checked', false);";
						}
					?>
				}
				<?php
			}
		?>
		$(selector).html(' Budget');
	}
	else
	{
		<?php
			foreach($checkbox_ids as $key=>$ids)
			{
				?>
				if(<?php echo $key;?> == gid){
					<?php
						foreach($ids as $key2=>$id)
						{
							echo "$('#" . $id . "').attr('checked', true);";
						}
					?>
				}
				<?php
			}
		?>
		$(selector).html(' Budget ');
	}
}


function select_all_delete_positions(gid)
{
	var selector = "#selector2" + gid;

	if($(selector).html() == '  ')
	{
		<?php
			foreach($delete_ids as $key=>$ids)
			{
				?>
				if(<?php echo $key;?> == gid){
					<?php
						foreach($ids as $key2=>$id)
						{
							echo "$('#" . $id . "').attr('checked', false);";
						}
					?>
				}
				<?php
			}
		?>
		$(selector).html(' ');
	}
	else
	{
		<?php
			foreach($delete_ids as $key=>$ids)
			{
				?>
				if(<?php echo $key;?> == gid){
					<?php
						foreach($ids as $key2=>$id)
						{
							echo "$('#" . $id . "').attr('checked', true);";
						}
					?>
				}
				<?php
			}
		?>
		$(selector).html('  ');
	}
}



</script>

<?php

$page->footer();

?>