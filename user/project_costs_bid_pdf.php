<?php
/********************************************************************

    project_costs_bid_pdf.php

    View bid in a PDF

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-01-09
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2014, OMEGA SA, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/project_cost_functions.php";

check_access("can_view_budget_in_projects");



/********************************************************************
    prepare pdf
*********************************************************************/

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');

//Instanciation of inherited class
$pdf = new TCPDF("L", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 23, 10);
$pdf->setPrintHeader(false);

$pdf->AddFont('arialn');
$pdf->AddFont('arialn', 'B');

$pdf->Open();

include("project_costs_bid_detail_pdf.php");
$pdf->Output();


?>