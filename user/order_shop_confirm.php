<?php
/********************************************************************

    order_shop_confirm.php

    Confirmation of a shop order from the shop_configurator

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-08-04
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-08-04
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/order_functions.php";
require_once "include/save_functions.php";
require_once "include/shop_configurator_functions.php";



check_access("can_create_new_orders");

/********************************************************************
    prepare all data needed
*********************************************************************/
$hl_start = "<span class='highlite_value'>";
$hl_end = "</span>";

// get user data
$user = get_user(user_id());

// get users' company address
$address = get_address($user["address"]);

// get client's currency
$currency = get_address_currency($user["address"]);
$order_data = get_shop_basket_addresses();
$shop_data = get_shop_basket();


// get the region of users' address
$user_region = get_user_region(user_id());
$currency = get_user_currency(user_id());

$cost = get_cost_of_shop($shop_data["category_id"], $user_region, $shop_data);

$p_cost_type = $order_data["project_cost_type"];
if(!$p_cost_type) {$p_cost_type = 0;}

//picture configuration code
$picture_code = array();
foreach($shop_data as $key=>$value)
{
    
    if(substr($key, 0, 2) == "o_" and $value == 1)
    {
        $parts = explode ( "_", $key);

        $sql = "select item_option_configcode " .
               "from item_options " .
               "where item_option_id = " . $parts[2];

        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            if($row["item_option_configcode"])
            {
                $picture_code[] = $row["item_option_configcode"];
            }
        }
    }
    elseif(substr($key, 0, 2) == "r_"  and $value == 1)
    {
        $parts = explode ( "_", $key);

        $sql = "select item_group_option_configcode1 " .
               "from item_group_options " .
               "where item_group_option_id = " . $parts[1];

        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            if($row["item_group_option_configcode1"])
            {
                $picture_code[] = $row["item_group_option_configcode1"];
            }
        }
    }
    elseif(substr($key, 0, 3) == "rb_"  and $value == 1)
    {
        $parts = explode ( "_", $key);

        $sql = "select item_group_option_configcode1 " .
               "from item_group_options " .
               "where item_group_option_id = " . $parts[1];

        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            if($row["item_group_option_configcode1"])
            {
                $picture_code[] = $row["item_group_option_configcode1"];
            }
        }
        
    }
    elseif(substr($key, 0, 3) == "rb_"  and $value == 2)
    {
        $parts = explode ( "_", $key);

        $sql = "select item_group_option_configcode2 " .
               "from item_group_options " .
               "where item_group_option_id = " . $parts[1];

        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            if($row["item_group_option_configcode2"])
            {
                $picture_code[] = $row["item_group_option_configcode2"];
            }
        }
    }
}

$picture_config_code = "";
sort($picture_code);

foreach($picture_code as $key=>$value)
{
    $picture_config_code = $picture_config_code . $value;

}

/********************************************************************
    build adress form
*********************************************************************/
$form = new Form("", "address_data");


$form->add_comment("Please check the order before submitting!");
$form->add_section("Client");
$form->add_lookup("address_id","Client", "addresses" , "address_company", 0, $user["address"]);

$form->add_section("Project Legal Type");
$form->add_lookup("project_costtype_id","Project Legal Type", "project_costtypes" , "project_costtype_text", 0, $p_cost_type);

if($p_cost_type == 2)
{
	// franchisee address
	$form->add_section("Franchisee Address");
	$form->add_hidden("franchisee_address_id", $order_data["address_id"]);
	$form->add_hidden("franchisee_address_country", $order_data["franchisee_address_country"]);

	$form->add_label("franchisee_address_company", "Company*", 0, $order_data["franchisee_address_company"]);
	$form->add_label("franchisee_address_company2", "", HIDEEMPTY,
					 $order_data["franchisee_address_company2"]);
	$form->add_label("franchisee_address_address", "Address*", HIDEEMPTY,
					 $order_data["franchisee_address_address"]);
	$form->add_label("franchisee_address_address2", "", HIDEEMPTY, $order_data["franchisee_address_address2"]);
	$form->add_label("franchisee_address_zip", "ZIP*", HIDEEMPTY, $order_data["franchisee_address_zip"]);
	$form->add_label("franchisee_address_place", "City*", HIDEEMPTY, $order_data["franchisee_address_place"]);

	$form->add_label("franchisee_address_country_name", "Country*", 0, 
				  get_country($order_data["franchisee_address_country"]));

	$form->add_label("franchisee_address_phone", "Phone*", HIDEEMPTY, $order_data["franchisee_address_phone"]);
	$form->add_label("franchisee_address_fax", "Fax", HIDEEMPTY, $order_data["franchisee_address_fax"]);
	$form->add_label("franchisee_address_email", "Email", HIDEEMPTY, $order_data["franchisee_address_email"]);
	$form->add_label("franchisee_address_contact", "Contact*", HIDEEMPTY,
					 $order_data["franchisee_address_contact"]);
}

// notify address
/*
$form->add_section("Notify Address (notify address)");
$form->add_hidden("billing_address_id", $order_data["address_id"]);
$form->add_hidden("billing_address_country", $order_data["billing_address_country"]);

$form->add_label("billing_address_company", "Company*", 0, $order_data["billing_address_company"]);
$form->add_label("billing_address_company2", "", HIDEEMPTY,
                 $order_data["billing_address_company2"]);
$form->add_label("billing_address_address", "Address*", HIDEEMPTY,
                 $order_data["billing_address_address"]);
$form->add_label("billing_address_address2", "", HIDEEMPTY, $order_data["billing_address_address2"]);
$form->add_label("billing_address_zip", "ZIP*", HIDEEMPTY, $order_data["billing_address_zip"]);
$form->add_label("billing_address_place", "City*", HIDEEMPTY, $order_data["billing_address_place"]);

$form->add_label("billing_address_country_name", "Country*", 0, 
              get_country($order_data["billing_address_country"]));

$form->add_label("billing_address_phone", "Phone*", HIDEEMPTY, $order_data["billing_address_phone"]);
$form->add_label("billing_address_fax", "Fax", HIDEEMPTY, $order_data["billing_address_fax"]);
$form->add_label("billing_address_email", "Email", HIDEEMPTY, $order_data["billing_address_email"]);
$form->add_label("billing_address_contact", "Contact*", HIDEEMPTY,
                 $order_data["billing_address_contact"]);
*/

$form->add_hidden("billing_address_company", $address["company"], TYPE_CHAR);
$form->add_hidden("billing_address_company2", $address["company2"], TYPE_CHAR);
$form->add_hidden("billing_address_address", $address["address"], TYPE_CHAR);
$form->add_hidden("billing_address_address2", $address["address2"], TYPE_CHAR);
$form->add_hidden("billing_address_zip", $address["zip"], TYPE_CHAR, 20);
$form->add_hidden("billing_address_place", $address["place"], TYPE_CHAR, 20);
$form->add_hidden("billing_address_country", $address["country"], NOTNULL);
$form->add_hidden("billing_address_phone", $user["phone"], TYPE_CHAR, 20);
$form->add_hidden("billing_address_fax", $user["fax"], TYPE_CHAR, 20);
$form->add_hidden("billing_address_email", $user["email"], TYPE_CHAR);
$form->add_hidden("billing_address_contact", $user["contact"], TYPE_CHAR);


// Delivery Address

$form->add_section("Delivery Address (consignee address)");
$form->add_hidden("delivery_address_country", $order_data["delivery_address_country"]);
/*
$form->add_hidden("delivery_is_billing_address", 
                  $order_data["delivery_is_billing_address"]);

if ($order_data["delivery_is_billing_address"])
{
    $delivery_address_company = $order_data["billing_address_company"];
    $delivery_address_company2 = $order_data["billing_address_company2"];
    $delivery_address_address = $order_data["billing_address_address"];
    $delivery_address_address2 = $order_data["billing_address_address2"];
    $delivery_address_zip = $order_data["billing_address_zip"];
    $delivery_address_place = $order_data["billing_address_place"];
    $delivery_address_country = $order_data["billing_address_country"];
    $delivery_address_phone = $order_data["billing_address_phone"];
    $delivery_address_fax = $order_data["billing_address_fax"];
    $delivery_address_email = $order_data["billing_address_email"];
    $delivery_address_contact =  $order_data["billing_address_contact"];
}
else
{
    $delivery_address_company = $order_data["delivery_address_company"];
    $delivery_address_company2 = $order_data["delivery_address_company2"];
    $delivery_address_address = $order_data["delivery_address_address"];
    $delivery_address_address2 = $order_data["delivery_address_address2"];
    $delivery_address_zip = $order_data["delivery_address_zip"];
    $delivery_address_place = $order_data["delivery_address_place"];
    $delivery_address_country = $order_data["delivery_address_country"];
    $delivery_address_phone = $order_data["delivery_address_phone"];
    $delivery_address_fax = $order_data["delivery_address_fax"];
    $delivery_address_email = $order_data["delivery_address_email"];
    $delivery_address_contact =  $order_data["delivery_address_contact"];
}
*/

$delivery_address_company = $order_data["delivery_address_company"];
$delivery_address_company2 = $order_data["delivery_address_company2"];
$delivery_address_address = $order_data["delivery_address_address"];
$delivery_address_address2 = $order_data["delivery_address_address2"];
$delivery_address_zip = $order_data["delivery_address_zip"];
$delivery_address_place = $order_data["delivery_address_place"];
$delivery_address_country = $order_data["delivery_address_country"];
$delivery_address_phone = $order_data["delivery_address_phone"];
$delivery_address_fax = $order_data["delivery_address_fax"];
$delivery_address_email = $order_data["delivery_address_email"];
$delivery_address_contact =  $order_data["delivery_address_contact"];

$form->add_label("delivery_address_company", "Company*", 0, 
                 $delivery_address_company);
$form->add_label("delivery_address_company2", "",HIDEEMPTY,
                 $delivery_address_company2);
$form->add_label("delivery_address_address", "Address*", HIDEEMPTY,
                 $delivery_address_address);
$form->add_label("delivery_address_address2", "", HIDEEMPTY, 
                 $delivery_address_address2);
$form->add_label("delivery_address_zip", "ZIP*", 0, 
                 $delivery_address_zip);
$form->add_label("delivery_address_place", "City*", HIDEEMPTY, 
                 $delivery_address_place);
$form->add_label("delivery_address_country_name", "Country*", HIDEEMPTY, get_country($delivery_address_country));

$form->add_label("delivery_address_phone", "Phone*", HIDEEMPTY, $delivery_address_phone);
$form->add_label("delivery_address_fax", "Fax",HIDEEMPTY, $delivery_address_fax);
$form->add_label("delivery_address_email", "Email",HIDEEMPTY, $delivery_address_email);
$form->add_label("delivery_address_contact", "Contact*" , HIDEEMPTY, $delivery_address_contact);

// POS Address

$form->add_section("POS Location Address");
$form->add_hidden("shop_is_delivery_address", $order_data["shop_is_delivery_address"]);

if ($order_data["shop_is_delivery_address"])
{
    $shop_address_company = $delivery_address_company;
    $shop_address_company2 = $delivery_address_company2;
    $shop_address_address = $delivery_address_address;
    $shop_address_address2 = $delivery_address_address2;
    $shop_address_zip = $delivery_address_zip;
    $shop_address_place = $delivery_address_place;
    $shop_address_phone = $delivery_address_phone;
    $shop_address_fax = $delivery_address_fax;
    $shop_address_email = $delivery_address_email;
    $shop_address_country = $delivery_address_country;
}
else
{
    $shop_address_company = $order_data["shop_address_company"];
    $shop_address_company2 = $order_data["shop_address_company2"];
    $shop_address_address = $order_data["shop_address_address"];
    $shop_address_address2 = $order_data["shop_address_address2"];
    $shop_address_zip = $order_data["shop_address_zip"];
    $shop_address_place = $order_data["shop_address_place"];
    $shop_address_phone = $order_data["shop_address_phone"];
    $shop_address_fax = $order_data["shop_address_fax"];
    $shop_address_email = $order_data["shop_address_email"];
    $shop_address_country = $order_data["shop_address_country"];
}

$form->add_hidden("shop_address_country", $shop_address_country);
$shop_address_country_name = get_country($shop_address_country);

$form->add_hidden("shop_address_country", $shop_address_country);
$form->add_label("shop_address_company", "Company*", 0, 
                      $shop_address_company);
$form->add_label("shop_address_company2", "", HIDEEMPTY, 
                      $shop_address_company2);
$form->add_label("shop_address_address", "Address*", 0, 
                      $shop_address_address);
$form->add_label("shop_address_address2", "", HIDEEMPTY, 
                      $shop_address_address2);
$form->add_label("shop_address_zip", "ZIP*", 0, 
                      $shop_address_zip);
$form->add_label("shop_address_place", "City*", 0, 
                      $shop_address_place);
$form->add_label("shop_address_country_name", "Country*",0 ,$shop_address_country_name);

$form->add_label("shop_address_phone", "Phone", 0, 
                      $shop_address_phone);
$form->add_label("shop_address_fax", "Fax", 0, 
                      $shop_address_fax);
$form->add_label("shop_address_email", "Email", 0, 
                      $shop_address_email);


// Transportation Preferences

$form->add_section("Location Info");

$form->add_label("voltage_name", "Voltage", 0, get_voltage($order_data["voltage"]));
$form->add_hidden("voltage", $order_data["voltage"],0);

$form->add_label("location_name", "Location Type", 0, get_location_type($order_data["location_type"]));
$form->add_hidden("location_type", $order_data["location_type"],0);


// Transportation Preferences

$form->add_section("Transportation Preferences");
$form->add_hidden("preferred_transportation_arranged", $order_data["preferred_transportation_arranged"]);
$form->add_hidden("preferred_transportation_mode", $order_data["preferred_transportation_mode"]);
//$form->add_hidden("packaging_retraction", $order_data["packaging_retraction"]);
$form->add_hidden("pedestrian_mall_approval", $order_data["pedestrian_mall_approval"]);
$form->add_hidden("full_delivery", $order_data["full_delivery"]);

$form->add_label("preferred_delivery_date", "Preferred Arrival Date", 0,
                      $order_data["preferred_delivery_date"]);


$form->add_label("preferred_transportation_name", "Transportation arranged by", 0, get_preferred_transportation($order_data["preferred_transportation_arranged"]));

$form->add_label("preferred_transportation_name", "Transportation mode", 0, get_preferred_transportation($order_data["preferred_transportation_mode"]));


//$form->add_label("packaging_retraction_name", "Packaging Retraction Desired", 0, ($order_data["packaging_retraction"]) ? "yes":"no" );

$form->add_label("pedestrian_mall_approval_name", "Pedestrian Area Approval Needed", 0, 
                    ($order_data["pedestrian_mall_approval"]) ? "yes":"no" );

$form->add_label("full_delivery_name", "Full Delivery Desired", 0, 
                    ($order_data["full_delivery"]) ? "yes":"no" );

$form->add_label("delivery_comments", "Delivery Comments", 0,
                      $order_data["delivery_comments"]);


$form->add_label("comments", "Comments", 0, $order_data["comments"]);

$form->add_section("Other Information");
$form->add_label("approximate_budget", "Approximate Budget in " . $currency['currency_symbol'], 0,
                      number_format($order_data["approximate_budget"],2));
$form->add_label("planned_opening_date", "Planned Opening Date", 0,
                      $order_data["planned_opening_date"]);


$sql = "select item_id, category_item_id, item_code, item_name, ".
       "    round((item_price/currency_exchange_rate * " .
       "    currency_factor),2) as price " .
       "from category_items " .
       "left join items on category_items.category_item_item=items.item_id " .
       "left join item_regions on item_region_item = item_id " .
       "left join users on users.user_id = " . user_id() . " " .
       "left join addresses on addresses.address_id = users.user_address " .
       "left join currencies on currencies.currency_id = addresses.address_currency "  .
       "where category_item_category = " . $shop_data["category_id"] . 
       "    and item_visible = 1 " .
       "    and item_active = 1 " .
       "    and item_region_region = " . $user_region;


$form->populate();
$form->process();

/********************************************************************
    build items form
*********************************************************************/


$form_items = new Form("", "shop_data");


$form_items->add_section(" ");
$form_items->add_section(" ");

$form_items->add_section("Order Details");

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
    
    // item

    $form_items->add_section($row["item_name"]);
    $item_id = $row["item_id"];

    // item price
    $price = $currency['currency_symbol'] . " " . number_format(get_shop_item_price($row["item_id"], $user_region), 2);
    $form_items->add_label("price_" . $row["item_id"], "Price", 0,  $price);


    // addons

    $sql_a =   "select count(addon_id) as num_recs " .
               "from addons " .
               "left join items on item_id = addon_child " .
               "left join item_regions on item_region_item = item_id " .
               "left join users on users.user_id = " . user_id() . " " .
               "left join addresses on addresses.address_id = users.user_address " .
               "left join currencies on currencies.currency_id = addresses.address_currency " .
               "where addon_parent = " . $row["item_id"];


    $res_a = mysql_query($sql_a) or dberror($sql_a);
    $row_a = mysql_fetch_assoc($res_a);

    $num_addons = $row_a["num_recs"];
    
    
    if($num_addons > 0)
    {
        $form_items->add_label("spacer01_" . $row["item_id"], "", 0,  "");
        $form_items->add_label("addons_" . $row["item_id"], "Addons", RENDER_HTML,  "<b>The following addons will be delivered: </b>");

        $sql_a =   "select addon_id, item_id, item_code, item_name, " .
                   "    round(((item_price / currency_exchange_rate) * currency_factor),2) as price, " .
                   "    concat_ws(' x ', addon_package_quantity, item_name) as addon_ext_name " .
                   "from addons " .
                   "left join items on item_id = addon_child " .
                   "left join item_regions on item_region_item = item_id " .
                   "left join users on users.user_id = " . user_id() . " " .
                   "left join addresses on addresses.address_id = users.user_address " .
                   "left join currencies on currencies.currency_id = addresses.address_currency " .
                   "where addon_parent = " . $row["item_id"];


        $res_a = mysql_query($sql_a) or dberror($sql_a);
        
        while ($row_a = mysql_fetch_assoc($res_a))
        {
            $p = $hl_start . "\nPrice: " . $currency['currency_symbol'] . " " . number_format($row_a["price"], 2) . $hl_end;
            $addon = $row_a["addon_ext_name"] . $p;
            $addon_name = "addon" . $row_a["addon_id"];
           
            $form_items->add_label($addon_name, " ", RENDER_HTML,  $addon);
        }
    }



    // options

    $num_of_options = 0;
    foreach($shop_data as $key=>$value)
    {
        if(substr($key, 0, 2) == "o_" and $value == 1)
        {
            $num_of_options++;
        }
    }
    foreach($shop_data as $key=>$value)
    {
        if(substr($key, 0, 2) == "r_" and $value == 1)
        {
            $num_of_options++;
        }
    }
    foreach($shop_data as $key=>$value)
    {
        if(substr($key, 0, 2) == "rb" and $value > 0)
        {
            $num_of_options++;
        }
    }

    if($num_of_options)
    {
        $form_items->add_label("spacer02_" . $row["item_id"], "", 0,  "");
        $form_items->add_label("options_" . $row["item_id"], "Options", RENDER_HTML,  "<b>The following options will be delivered: </b>");

        
        foreach($shop_data as $key=>$value)
        {
            if(substr($key, 0, 2) == "o_" and $value == 1)
            {
                $parts = explode ( "_", $key);
                
                if($parts[1] == $row["item_id"])
                {

                    $sql_a = "select item_option_id, item_code, item_name, " .
                             "item_option_quantity, " .
                             "item_option_name, " .
                             "round((item_option_quantity*item_price / currency_exchange_rate * currency_factor),2) as price " .
                             "from item_options " .
                             "left join items on item_id = item_option_child " .
                             "left join item_regions on item_region_item = item_id " .
                             "left join users on users.user_id = " . user_id() . " " .
                             "left join addresses on addresses.address_id = users.user_address " .
                             "left join currencies on currencies.currency_id = addresses.address_currency " . 
                             "where item_option_id  = " . $parts[2];

                    $res_a = mysql_query($sql_a) or dberror($sql_a);
        
                    if($row_a = mysql_fetch_assoc($res_a))
                    {
                        $p = $hl_start . "\nPrice: " . $currency['currency_symbol'] . " " . number_format($row_a["price"], 2) . $hl_end;
                        $option = $row_a["item_option_quantity"]. "x " . $row_a["item_name"] . $p;
                        $option_name = "o_" . $row["item_id"] . "_" . $row_a["item_option_id"];
               
                        $form_items->add_label($option_name, $row_a["item_option_name"], RENDER_HTML,  $option);
                    }

                }

            }
            
        }
    }

    //replacement options
    $num_of_options = 0;
    foreach($shop_data as $key=>$value)
    {
        if(substr($key, 0, 2) == "r_" and $value == 1)
        {
            $num_of_options++;
        }
        elseif(substr($key, 0, 2) == "rb" and $value > 1)
        {
            $num_of_options++;
        }

    }

    if($num_of_options > 0)
    {

        foreach($shop_data as $key=>$value)
        {

            if(substr($key, 0, 1) == "r" and $value == 1)
            {
                $parts = explode ( "_", $key);
                
                $sql_g = "select * " .
                         "from item_group_options " .
                         "where item_group_option_id = " . $parts[1];

                $res_g = mysql_query($sql_g) or dberror($sql_g);
                $row_g = mysql_fetch_assoc($res_g);

                $option_name = $row_g["item_group_option_name"];
                $rname = "r_" . $row_g["item_group_option_id"]; 
                $rvalue = $row_g["item_group_option_description1"];


                $replacement_cost = get_difference_in_cost_of_replacement($parts[1]);

                if($replacement_cost < 0)
                {
                   $dcost = $hl_start . "Difference in Price: " . $currency['currency_symbol'] ." " . number_format($replacement_cost, 2) .  $hl_end;
                }
                else
                {
                   $dcost = $hl_start . "Additional Price: " . $currency['currency_symbol'] ." " . number_format($replacement_cost, 2) .  $hl_end;
                }

                $rvalue = $rvalue . "\n" . $dcost;
                 
                $form_items->add_label($rname, $option_name, RENDER_HTML,  $rvalue);
            }
            elseif(substr($key, 0, 2) == "rb" and $value > 0)
            {
                $parts = explode ( "_", $key);
                
                $sql_g = "select * " .
                         "from item_group_options " .
                         "where item_group_option_id = " . $parts[1];

                $res_g = mysql_query($sql_g) or dberror($sql_g);
                $row_g = mysql_fetch_assoc($res_g);
                
                
                $option_name = $row_g["item_group_option_name"];
                $rname = "rb_" . $row_g["item_group_option_id"]; 

                if($value == 1)
                {
                   $rvalue = $row_g["item_group_option_description1"];
                }
                else
                {
                    $rvalue = $row_g["item_group_option_description2"];
                }


                $replacement_cost = get_difference_in_cost_of_replacement($parts[1], $value);

                if($replacement_cost < 0)
                {
                   $dcost = $hl_start . "Difference in Price: " . $currency['currency_symbol'] ." " . number_format($replacement_cost, 2) .  $hl_end;
                }
                else
                {
                   $dcost = $hl_start . "Additional Price: " . $currency['currency_symbol'] ." " . number_format($replacement_cost, 2) .  $hl_end;
                }

                $rvalue = $rvalue . "\n" . $dcost;
                 
                $form_items->add_label($rname, $option_name, RENDER_HTML,  $rvalue);
            }
        }
    }
    
}


$form_items->add_section("");
$form_items->add_section("");

$form_items->populate();
$form_items->process();


/********************************************************************
    build items form
*********************************************************************/

$form_cost = new Form("", "Cost");

$form_cost->add_section("Total Cost");


$form_cost->add_label("item_total", "Kiosk", 0, $currency['currency_symbol'] . " " .$cost["item_total"]);

if($cost["addon_total"])
{
    $form_cost->add_label("addon_total", "Add Ons", 0, $currency['currency_symbol'] . " " .$cost["addon_total"]);
}

$form_cost->add_label("option_total", "Options", 0, $currency['currency_symbol'] . " " .$cost["option_total"]);


$form_cost->add_label("shop_total", "Order Total", RENDER_HTML, "<b>" . $currency['currency_symbol'] . " " .$cost["shop_total"] . "</br>");


$pix = "<img src=\"../pictures/view.gif\" align=\"absmiddle\" border=\"0\" alt=\"\" />";
$link = "javascript:popup('order_shop_posconfig_view.php?id=" . $item_id . "&pcc=" . $picture_config_code . "', 500,400)";

$view = " <a href =\"" .  $link . "\">" . $pix . "&nbsp;show picture of the selected configuration</a>";

$form_cost->add_label("view_pos_info" . $row["item_id"], "", RENDER_HTML,  $view);

$form_cost->add_label("spacer", "", 0, "");


$form_cost->add_button("submit", "Submit Order");
$form_cost->add_button("back", "Back");
$form_cost->add_button("cancel", "Cancel Ordering");

$form->populate();
$form_cost->process();

/********************************************************************
    Process buttons
*********************************************************************/ 



if ($form_cost->button("back"))
{
    redirect("order_shop_checkout.php");
}
elseif($form_cost->button("cancel"))
{
    redirect("orders.php");
}
elseif($form_cost->button("submit"))
{
    save_shop_order($item_id, $picture_config_code);
    redirect("order_new_submitted.php");
}
    
/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

$page->register_action('home', 'Home', "welcome.php");

$page->header();
$page->title("Order Preview");

$form->render();

$form_items->render();
$form_cost->render();

$page->footer();


?>