<?php
/********************************************************************

    item_group_view.php


    Info Screen for item groups.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-08-04
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-08-04
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
check_access("can_view_catalog_detail");

$gid = $_GET["oid"];
$value = $_GET["value"];

/********************************************************************
    prepare all data needed
*********************************************************************/
//get all items of the group

$option_description = "";
$info = "";

$sql = "select item_group_option_name, item_group_option_description1, " .
       "item_group_option_description2, item_group_option_info1, " .
       "item_group_option_info2 " .
       "from item_group_options " .
       "where item_group_option_id = " . $oid;

$res = mysql_query($sql) or dberror($sql);

if ($row = mysql_fetch_assoc($res))
{
    if($value == 2)
    {
        $option_description = $row["item_group_option_name"] . "\n" . $row["item_group_option_description2"];
        $info = $row["item_group_option_info2"];
    }
    else
    {
        $option_description = $row["item_group_option_name"] . "\n" . $row["item_group_option_description1"];
        $info = $row["item_group_option_info1"];
    }
}




// attached files
$files_have_records = 0;
$sql_files = "select item_group_file_id, item_group_file_title, file_type_name, " .
             "    item_group_file_description, item_group_file_path " . 
             "from item_group_files " .
             "left join file_types on file_type_id = item_group_file_type ";

$sql = $sql_files . " where item_group_file_type <> 2 and item_group_file_group_option = " . $oid;
$res = mysql_query($sql) or dberror();
if ($row = mysql_fetch_assoc($res))
{
    $files_have_records = 1;
}

/********************************************************************
    Create Form
*********************************************************************/ 
$form = new Form("item_groups", "item_group");

if($option_description)
{
    $form->add_label("description", "Description", RENDER_HTML, $option_description);
    $form->add_section("");
}

if($info)
{
    $form->add_label("info", "Detail", RENDER_HTML, $info);
    $form->add_section("");
}

$form->process();

/********************************************************************
    Create File List
*********************************************************************/ 
if ($files_have_records == 1)
{
    $list = new ListView($sql_files);

    $list->set_entity("item_group files");
    $list->set_title("Attached Files");
    $list->set_filter("item_group_file_group_option = " . $oid);
    $list->set_order("item_group_file_title");

    $link = "http://" . $_SERVER["HTTP_HOST"] . "/";

    $list->add_column("item_group_file_title", "Title", $link . "{item_group_file_path}", "", "", COLUMN_NO_WRAP);

    $list->add_column("file_type_name", "Type", "", "", "", COLUMN_NO_WRAP);

    $list->add_column("item_group_file_description", "Description");
}

if ($files_have_records == 1)
{
    $list->process();
}


/********************************************************************
    render page
*********************************************************************/
$page = new Page(PAGE_POPUP);
$page->header();
$page->title("Option Info");



$form->render();

// Get item image
$result = mysql_query("select item_group_file_path " .
                      "from item_group_files " .
                      "where item_group_file_group_option =" . $oid . " and item_group_file_type = 2");

if (mysql_num_rows($result) == 1 and $value == 0)
{
	$row = mysql_fetch_assoc($result);
	$size = getimagesize(combine_paths($_SERVER["DOCUMENT_ROOT"], $row['item_group_file_path']));
	echo "<div style=\"text-align:center\"><img src=\"" . $row['item_group_file_path'] . "\" width=\"$size[0]\" height=\"$size[1]\" border=\"0\" alt=\"\" /></div><br>";

    echo "<a class=\"value\" href=\"javascript:print();\">print this info</a>";
}
elseif (mysql_num_rows($result) == 2  and $value == 1)
{
    $row = mysql_fetch_assoc($result);
	$size = getimagesize(combine_paths($_SERVER["DOCUMENT_ROOT"], $row['item_group_file_path']));
	echo "<div style=\"text-align:center\"><img src=\"" . $row['item_group_file_path'] . "\" width=\"$size[0]\" height=\"$size[1]\" border=\"0\" alt=\"\" /></div><br>";

    echo "<a class=\"value\" href=\"javascript:print();\">print this info</a>";
}
elseif (mysql_num_rows($result) == 2  and $value == 2)
{
	$row = mysql_fetch_assoc($result);
    $row = mysql_fetch_assoc($result);
	$size = getimagesize(combine_paths($_SERVER["DOCUMENT_ROOT"], $row['item_group_file_path']));
	echo "<div style=\"text-align:center\"><img src=\"" . $row['item_group_file_path'] . "\" width=\"$size[0]\" height=\"$size[1]\" border=\"0\" alt=\"\" /></div><br>";

    echo "<a class=\"value\" href=\"javascript:print();\">print this info</a>";
}
else
{
	echo "<p>No picture available</p>";
}

if ($files_have_records == 1)
{
    $list->render();
}

$page->footer();
?>