<?php
/********************************************************************

    delivery_by_supplier_pdf.php

    List delivery jobs by supplier in a PDF

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2003-12-07
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-12-07
    Version:        1.0.0

    Copyright (c) 2003, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/order_state_constants.php";

check_access("can_view_delivery");


/********************************************************************
    prepare all data needed
*********************************************************************/
// get user data
$user_data = get_user(user_id());
$user_roles = get_user_roles(user_id());

// get params
$ordernumber = param("o");
$country = param("c");
$ponumber = param("p");
$shipmentcode = param("s");
$supplier = param("su");
$forwarder = param("fw");
$rto = param("rto");
$rtc = param("rtc");
$item = param("item");
$ordertype = param("t");

// create filter
// get user data
$user_data = get_user(user_id());
$user_roles = get_user_roles(user_id());


// build sql for the list
$sql_order_items = "select order_item_id, ".
                     "    concat_ws(', ', country_name, order_shop_address_place, ".
                     "    order_shop_address_company, order_number, ".
                     "    concat('Retail Contact: ', user_name, ' ', user_firstname)) as group_head, ".
                     "    order_number, address_company, ".
                     "    country_name, order_id, project_id, ".
                     "    address_company, order_item_quantity, " .
                     "    if(order_item_item <>'', item_code, 'Special Item') as item_shortcut, ".
                     "    order_item_po_number, order_item_shipment_code, order_item_quantity, ".
                     "    order_item_text, " .
                     "    order_item_ordered, order_item_ready_for_pickup, order_item_pickup, ".
                     "    order_item_expected_arrival, order_item_arrival " .
                     "from orders ".
                     "left join countries on order_shop_address_country = country_id ".
                     "left join order_items on order_id = order_item_order " .
                     "left join addresses on order_item_supplier_address = addresses.address_id " .
                     "left join projects on order_id = project_order ".
                     "left join items on order_item_item = item_id ".
                     "left join users on order_retail_operator = user_id ";

$ordernumber = param("o");
$country = param("c");
$ponumber = param("p");
$shipmentcode = param("s");
$supplier = param("su");
$forwarder = param("fw");
$item = param("item");

$selection = "";
if($ordernumber)
{
    $selection = "order_id = " . $ordernumber . " and ";
}
if($country)
{
    $selection = $selection . "order_shop_address_country = " . $country . " and ";
}
if($ponumber)
{
    $selection = $selection . "order_item_po_number = '" . $ponumber . "' and ";
}
if($shipmentcode)
{
    $selection = $selection . "order_item_shipment_code = '" . $shipmentcode . "' and ";
}
if($supplier)
{
    $selection = $selection . "order_item_supplier_address = " . $supplier . " and ";
}
if($forwarder)
{
    $selection = $selection . "order_item_forwarder_address = " . $forwarder . " and ";
}
if($rto)
{
    $selection = $selection . "order_retail_operator = " . $rto . " and ";
}
if($rtc)
{
    $selection = $selection . "project_retail_coordinator = " . $rtc . " and ";
}
if($item)
{
    $selection = $selection . "order_item_item = '" . $item . "' and ";
}
if($ordertype)
{
	$selection = $selection . "order_type = '" . $ordertype . "' and ";
}

if (!has_access("has_access_to_all_delivery_data")) // make them only see their own data
{
    if(in_array(5, $user_roles) or in_array(29, $user_roles)) // supplier or warehouse
    {
		$selection = $selection . "order_item_supplier_address = " . $user_data["address"] . " and ";   
    }
    elseif(in_array(6, $user_roles)) // user is a forwarder
    {
        $selection = $selection . "order_item_forwarder_address = " . $user_data["address"] . " and ";    
    }
}

$list_filter = $selection . 
               "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) ". 
			   "    and (order_item_arrival is null or order_item_arrival = '0000-00-00') " . 
               "    and order_show_in_delivery = 1 ".
               "    and order_archive_date is null ".
               "    and order_item_type <= " . ITEM_TYPE_SPECIAL;


// create list filter
if (!has_access("has_access_to_all_delivery_data"))
{
    if($ordertype)
	{
		$filter = get_user_specific_order_list(user_id(), $ordertype);
	}
	else
	{

		$filter = " (" . get_user_specific_order_list(user_id(), 1);
		$filter .= " or " . get_user_specific_order_list(user_id(), 2) . ")";
	}

	
    
    $list_filter = $filter .
                   "    and " . $selection . 
                   "    (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
		           "    and (order_item_arrival is null or order_item_arrival = '0000-00-00') " .
                   "    and order_show_in_delivery = 1 ".
                   "    and order_archive_date is null ".
                   "    and order_item_type <= " . ITEM_TYPE_SPECIAL;
}


$sql = $sql_order_items . " where " . $list_filter . " order by group_head, address_company, item_shortcut";


global $page_title;
$page_title = "Delivery Overview";
//delivery data
$captions1 = array();

$captions1[] = "Supplier";

$captions1[] = "Item";
$captions1[] = "Quantity";
$captions1[] = "P.O. Number";
$captions1[] = "Shipment Code";
$captions1[] = "Ordered";
$captions1[] = "Ready to Pickup";
$captions1[] = "Pickup";
$captions1[] = "Expected Arrival";
$captions1[] = "Arrival";



/********************************************************************
    prepare pdf
*********************************************************************/

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');

class MYPDF extends TCPDF
{
    //Page header
    function Header()
    {
        global $page_title;
		//Logo
        $this->Image('../pictures/omega_logo.jpg',10,8,33);
        //arialn bold 15
        $this->SetFont('arialn','B',12);
        //Move to the right
        $this->Cell(80);
        //Title
        $this->Cell(0,33,$page_title,0,0,'R');
        //Line break
        $this->Ln(20);

    }

    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //arialn italic 8
        $this->SetFont('arialn','I',8);
        //Page number
        $this->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$this->PageNo().'/'.$this->getAliasNbPages(),0,0,'R');
    }
   
}

//Instanciation of inherited class
$pdf = new MYPDF("L", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 22, 12);

$pdf->Open();

$pdf->SetLineWidth(0.1);
$pdf->SetFillColor(220, 220, 220); 

$pdf->AddFont('arialn');
$pdf->AddFont('arialn', 'B');

$pdf->AddPage();


// Delivery Data
$group_head = "";

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
    // group heads
    $pdf->SetFont('arialn','B',8);

    $y = $pdf->getY();
    if($y >= 175)
    {
        $pdf->AddPage();
    }
    
    if($row["group_head"] != $group_head)
    {
        $pdf->SetFillColor(220, 220, 220); 
        $pdf->Cell(275,5, $row["group_head"],1, 0, 'L', 1);
        $pdf->Ln();
        
        $pdf->SetFillColor(240, 240, 240); 
        $pdf->Cell(45,5, $captions1[0],1, 0, 'L', 1);
        $pdf->Cell(40,5,$captions1[1],1, 0, 'L', 1);
        $pdf->Cell(16,5,$captions1[2],1, 0, 'L', 1);
        $pdf->Cell(35,5,$captions1[3],1, 0, 'L', 1);
        $pdf->Cell(40,5,$captions1[4],1, 0, 'L', 1);
        $pdf->Cell(16,5,$captions1[5],1, 0, 'L', 1);
        $pdf->Cell(24,5,$captions1[6],1, 0, 'L', 1);
        $pdf->Cell(18,5,$captions1[7],1, 0, 'L', 1);
        $pdf->Cell(24,5,$captions1[8],1, 0, 'L', 1);
        $pdf->Cell(17,5,$captions1[9],1, 0, 'L', 1);
        $pdf->Ln();
        
        $group_head = $row["group_head"];
    }


    $date1 = "n/a";
    $date2 = "n/a";
    $date3 = "n/a";
    $date4 = "n/a";
    $date5 = "n/a";

    

    if($row["order_item_ordered"] != "0000-00-00")
    {
        $date1 = to_system_date($row["order_item_ready_for_pickup"]);
    }
    if($row["order_item_ready_for_pickup"] != "0000-00-00")
    {
        $date2 = to_system_date($row["order_item_ready_for_pickup"]);
    }
    if($row["order_item_pickup"] != "0000-00-00")
    {
        $date3 = to_system_date($row["order_item_pickup"]);
    }
    if($row["order_item_expected_arrival"] != "0000-00-00")
    {
        $date4 = to_system_date($row["order_item_expected_arrival"]);
    }
    if($row["order_item_arrival"] != "0000-00-00")
    {
        $date5 = to_system_date($row["order_item_arrival"]);
    }

    $pdf->SetFont('arialn','',8);
    $pdf->Cell(45, 5, substr($row["address_company"],0,32), 1, 0, 'L', 0);
    $pdf->Cell(230, 5, $row["item_shortcut"] . ", " . substr($row["order_item_text"],0,140), 1, 0, 'L', 0);
    $pdf->Ln();

    $pdf->Cell(85, 5, " ", 0, 0, 'L', 0);    
    $pdf->Cell(16, 5, $row["order_item_quantity"], 1, 0, 'L', 0);
    $pdf->Cell(35, 5, $row["order_item_po_number"], 1, 0, 'L', 0);
    $pdf->Cell(40, 5, $row["order_item_shipment_code"], 1, 0, 'L', 0);
    $pdf->Cell(16, 5, $date1,1, 0, 'L', 0);
    $pdf->Cell(24, 5, $date2,1, 0, 'L', 0);
    $pdf->Cell(18, 5, $date3,1, 0, 'L', 0);
    $pdf->Cell(24, 5, $date4,1, 0, 'L', 0);
    $pdf->Cell(17, 5, $date5,1, 0, 'L', 0);
    $pdf->Ln();
}

// write pdf
$pdf->Output();


?>