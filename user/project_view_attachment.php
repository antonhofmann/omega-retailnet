<?php
/********************************************************************

    project_view_attachment.php

    view attachment of a project

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-05-18
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-18
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_view_attachments_in_projects");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read order and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

//get the file
$sql = "select * from order_files where order_file_id = " . id();
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

$file = $row["order_file_path"];
$filelink = APPLICATION_URL . $file;

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("order_files", "file");

$form->add_section("Project");
$form->add_hidden("pid", param('pid'));
$form->add_hidden("order_id", $project["project_order"]);


require_once "include/project_head_small.php";

$form->add_section("attachment");


$form->add_lookup("order_file_owner", "Added by", "users", "Concat(user_name, ' ', user_firstname)", 0, "order_file_owner");

$form->add_label("date_created", "Date", 0);

$form->add_lookup("order_file_category", "Category", "order_file_categories", "order_file_category_name", 0, id());

$form->add_label("order_file_title", "Title", 0);
$form->add_label("order_file_description", "Description", 0);

$form->add_lookup("order_file_type", "File Type", "file_types", "file_type_name", 0, id());


$form->add_button("download", "click here to download file: " . basename($filelink), $view = $filelink, $flags = 0);


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


$page = new Page("projects");


require "include/project_page_actions.php";


$page->header();
$page->title("View attachment");
$form->render();
$page->footer();


?>