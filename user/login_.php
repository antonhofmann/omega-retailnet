<?php
/********************************************************************

    login.php

    Performs a user authentification.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2003-06-22
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-06-22
    Version:        1.0.0

    Copyright (c) 2003, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../sec/login_helper.php";

$page = new Page("login");
$page->header();
?>

<form name="main" method="post" action="login.php">
    <input type="hidden" name="action" />
    <table border="0" cellspacing="0" cellpadding="0">
        <tr> 
            <td colspan="2">Enter your username and password to log in.</td>
        </tr>
        <?php if ($error): ?>
            <tr> 
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr> 
                <td colspan="2" class="error"><?php echo $error?></td>
            </tr>
        <?php endif; ?>
		<?php if ($message): ?>
            <tr> 
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr> 
                <td colspan="2" class="message"><?php echo $message?></td>
            </tr>
        <?php endif; ?>
        <tr> 
            <td colspan="2">&nbsp;<br /><br /></td>
        </tr>
        <tr> 
            <td bgcolor="#F6F6FF" class="text">Username:</td>
            <td bgcolor="#F6F6FF"><input type="text" name="username" id="username" style="width:200px;" /></td>
        </tr>
        <tr> 
            <td colspan="2">&nbsp;</td>
        </tr>
		<tr> 
            <td bgcolor="#F6F6FF"class="text">Password:&nbsp;&nbsp;</td>
            <td bgcolor="#F6F6FF"><input type="password" name="password" id="password" style="width:200px;"/></td>
        </tr>
        <tr> 
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td bgcolor="#F6F6FF"class="text">&nbsp;&nbsp;</td>
			<td><a href="javascript:button('login')">Login</a>&nbsp;|&nbsp;<a href="/sec/password_forgotten.php">Password forgotten</a></td>
        </tr>
    </table>
</form>

<script type="text/javascript">
    
	var selectedInput = null;
	$(document).ready(function(){
	  $("#username").focus();

	  $('input').focus(function() {
				selectedInput = this;
			});

	});

	document.onkeydown = process_key;
	
	function process_key(e)
	{
	  if( !e ) 
	  {
		if( window.event ) 
		{
		  e = window.event;
		} 
		else 
		{
		  return;
		}
	  }

	  if(selectedInput.name == "password" && e.keyCode==13)
	  {
		button('login');
	  }
	}
</script>

<?php
$page->footer();
?>