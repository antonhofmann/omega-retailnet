<?php
/********************************************************************

    project_sheet_pdf.php

    Creates a PDF project sheet.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-10-07
    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date modified:  2003-05-12
    Version:        1.1.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

$SUPPRESS_HEADERS = true;
define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 

require "../include/frame.php";
require "../admin/snagging_elements.php";

define("FPDF_FONTPATH", $_SERVER["DOCUMENT_ROOT"] . "/include/font/");
require("../include/fpdf.php");

define("COLUMN1_XPOS", 15);
define("COLUMN2_XPOS", 38);
define("COLUMN3_XPOS", 62);
define("COLUMN4_XPOS", 74);

define("TITLE_YPOS", 12);
define("GROUP1_YPOS", 25);

define("COLUMN_DATA_OFFSET",3);
define("PAGE_FULL_WIDTH", 200);

define("LINE_HEIGHT", 5.4);
define("PAGE_TOP_YPOS", GROUP1_YPOS);

// Check access and id parameter

if (!param('id') && !param('template'))
{
    error("Must be called width id of an existing snagging list or snagging-template");
}

// Create and setup PDF document

$pdf = new FPDF("P", "mm", "A4");

$pdf->Open();
$pdf->SetTitle("Snagging-List");
$pdf->SetAuthor(BRAND . " Retail Net");
$pdf->SetDisplayMode(150);
$pdf->SetAutoPageBreak(false);

$pdf->AddFont('arialn');
$pdf->AddFont('arialn', 'B');

$pdf->AddPage();
$pdf->SetLeftMargin(0);

$list = new SnaggingList(param("id"), param("template"));
$list->load_structure();
$list->render();

// footer

$pdf->SetFont("Arial", "", SNAGGING_CAPTION_FONT_SIZE);
$pdf->SetXY(195, 275);
$pdf->Cell(0, 8, $pdf->PageNo(), 0);
$pdf->Image("pictures/jelly_logo.png", 100, 260, 22.5,22.2);

$pdf->Output();

?>