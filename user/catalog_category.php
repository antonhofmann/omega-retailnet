<?php
/********************************************************************

    catalog_category.php

    Entry page for the catalog section group.

    Created by:     Oliver Hofer (oliver.hofer@pageagent.com)
    Date created:   2002-08-20
    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date modified:  2003-04-29
    Version:        1.2.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_view_catalog");


$cid = param('cid');
if (!$cid)
{
    $cid = id();
}

$searchterm = '';
if(array_key_exists('searchterm', $_GET) and $_GET['searchterm'] != '') 
{
	$searchterm = $_GET['searchterm'];
}
else
{
	$searchterm = param('searchterm');
}


/********************************************************************
    prepare all data needed
*********************************************************************/
// get the currency assigned to users' address
$currency = get_user_currency(user_id());

// get the region of users' address
$user_region = get_user_region(user_id());

if($searchterm) {
	
	$sql_category = "select distinct category_item_id, item_code, item_name, item_id, " .
					"    round(((item_price / currency_exchange_rate) * " .
					"    currency_factor),2) as item_price, category_item_category, " .
					"    category_item_item, product_line_name, category_name, ".
		            " concat(product_line_name, ' - ', category_name) as productline_category " . 
					"from category_items " .
					"left join items on category_items.category_item_item=items.item_id " .
					"left join categories on category_id = category_item_category " .				
					"left join product_lines on product_line_id = category_product_line " .
					"left join users on users.user_id = " . user_id() . " " .
					"left join addresses on addresses.address_id = users.user_address " .
					"left join currencies on currencies.currency_id = addresses.address_currency ".
					"left join item_regions on item_region_item = item_id " . 
		            "left join productline_regions on productline_region_productline =  product_line_id";

	/*
	$list_filter = "(LOWER(item_code) like '%" . strtolower($searchterm) . "%' " . 
		           " OR LOWER(item_name) like '%" .  strtolower($searchterm) . "%' " . 
		           "OR LOWER(category_name) like '%" .  strtolower($searchterm) . "%' " . 
		           "OR LOWER(product_line_name) like '%" .  strtolower($searchterm) . "%' )" . 
				   "    and item_visible = 1 " .
				   "    and item_active = 1 " .
				   "    and item_region_region = " . $user_region;

	*/

	$list_filter = "(LOWER(item_code) like '%" . strtolower($searchterm) . "%' " . 
		           " OR LOWER(item_name) like '%" .  strtolower($searchterm) . "%' " . 
		           "OR LOWER(category_name) like '%" .  strtolower($searchterm) . "%' " . 
		           "OR LOWER(product_line_name) like '%" .  strtolower($searchterm) . "%' )" . 
				   "    and item_visible = 1 " .
				   "    and item_active = 1 " .
				   "    and item_region_region = " . $user_region . 
		           "    and product_line_visible = 1 " . 
		           "    and (category_not_in_use is null or category_not_in_use = 0) " . 
		           "    and category_catalog = 1 " . 
		           "    and product_line_clients=1 " . 
		           "    and productline_region_region = " . $user_region;

}
else {

	// get category name
	$category_name = get_category_name($cid);

	$sql_category = "select distinct category_item_id, item_code, item_name, item_id, " .
					"    round(((item_price / currency_exchange_rate) * " .
					"    currency_factor),2) as item_price, category_item_category, " .
					"    category_item_item ".
					"from category_items " .
					"left join items on category_items.category_item_item=items.item_id " .
					"left join users on users.user_id = " . user_id() . " " .
					"left join addresses on addresses.address_id = users.user_address " .
					"left join currencies on currencies.currency_id = addresses.address_currency ".
					"left join item_regions on item_region_item = item_id ";

	$list_filter = "category_item_category = " . $cid . 
				   "    and item_visible = 1 " .
				   "    and item_active = 1 " .
				   "    and item_region_region = " . $user_region;
}

/********************************************************************
	Create List
*********************************************************************/ 
$list = new ListView($sql_category);


$list->set_entity("items");
$list->set_filter($list_filter);
$list->set_order("item_code");

if($searchterm) {
	$list->set_group("productline_category");
	$list->set_order("product_line_name, category_name, item_code");
}

// access to product-details is restricted
$popup_link = "";
if (has_access("can_view_catalog_detail"))
{
	$popup_link = "popup:catalog_item_view.php?id={item_id}";
}

$list->add_hidden("cid", $cid);
$list->add_hidden('searchterm' , $searchterm);

$list->add_column("item_code", "Item Code", $popup_link);
$list->add_column("item_name", "Name/Description", "");
$list->add_column("item_price", "Price " . $currency["currency_symbol"], "", "", "", 
				  COLUMN_ALIGN_RIGHT );

if (has_access("can_create_new_orders"))
{
	$shopping_list_link = "order_category.php?action=basket_add&cid={category_item_category}";
	$list->set_edit_key("category_item_id");
	$list->add_button_column("category_item_id", "Add to Shopping List", $shopping_list_link);
}

$list->add_button(LIST_BUTTON_BACK, "Back");


/********************************************************************
	Populate list and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();

$page = new Page("catalog");

$page->register_action('home', 'Home', "welcome.php");

$page->header();

if($searchterm) {
	$page->title('Search Results for "' . $searchterm . '"');
}
else {
	$page->title($category_name);
}
$list->render();
$page->footer();


?>