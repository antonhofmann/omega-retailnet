<?php
/********************************************************************

    project_view_material_list_client_view.php

    View Project's List of Materials in Client's Currency

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-02-18
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-02-18
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_view_budget_in_projects");


/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

// get orders's currency
$order_currency = get_order_currency($project["project_order"]);


// build line numbers for budget positions
$line_numbers = get_budet_line_numbers($project["project_order"]);


// create sql for oder items

$sql_order_items = "select order_item_id, order_item_text, order_item_quantity, order_item_quantity_freezed, ".
			   "    order_item_po_number, item_id, order_item_client_price, order_item_client_price_freezed, ".
			   "    TRUNCATE(order_item_quantity * order_item_client_price, 2) as total_price, ".
			   "    TRUNCATE(order_item_quantity_freezed * order_item_client_price_freezed, 2) as total_price_freezed, ".
			   "    order_item_client_price, ".
			   "    if(item_code <> '', item_code, item_type_name) as item_shortcut, ".
			   "    category_priority, category_name, ".
			   "    address_shortcut, item_type_id, ".
			   "    item_type_priority, order_item_type, ".
			   "    concat(item_category_sortorder, ' ', item_category_name, ', ', category_name) as cat_name, unit_name  ".
			   "from order_items ".
			   "left join items on order_item_item = item_id ".
			   "left join categories on order_item_category = category_id ".
			   "left join item_categories on item_category_id = item_category " .
			   "left join addresses on order_item_supplier_address = address_id ".
			   "left join item_types on order_item_type = item_type_id " .
				   "left join units on unit_id = item_unit";


$list1_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) and order_item_order = " . $project["project_order"] .  " and (order_item_type = " . ITEM_TYPE_STANDARD . " or order_item_type = " . ITEM_TYPE_SERVICES . ")";

$list2_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) and order_item_order = " . $project["project_order"] .  " and order_item_type = " . ITEM_TYPE_SPECIAL;

$list3_filter = "order_item_order = " . $project["project_order"] .  " and order_item_type = " . ITEM_TYPE_COST_ESTIMATION;

$list6_filter = "order_item_order = " . $project["project_order"] .  " and order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST;


// build totals
$standard_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_STANDARD);
$standard_item_total_freezed = get_order_item_type_total_freezed($project["project_order"],  ITEM_TYPE_STANDARD);

$special_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_SPECIAL);
$special_item_total_freezed = get_order_item_type_total_freezed($project["project_order"],  ITEM_TYPE_SPECIAL);

$cost_estimation_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_COST_ESTIMATION);
$cost_estimation_item_total_freezed = get_order_item_type_total_freezed($project["project_order"],  ITEM_TYPE_COST_ESTIMATION);

$local_construction_item_total =  get_order_item_type_total($project["project_order"],  ITEM_TYPE_LOCALCONSTRUCTIONCOST);
$local_construction_item_total_freezed =  get_order_item_type_total_freezed($project["project_order"],  ITEM_TYPE_LOCALCONSTRUCTIONCOST);

$grand_total_in_order_currency = number_format($standard_item_total["in_order_currency"] + $special_item_total["in_order_currency"],2);

$total_cost = $standard_item_total["in_order_currency"] + $special_item_total["in_order_currency"] + $cost_estimation_item_total["in_order_currency"] + $local_construction_item_total["in_order_currency"];

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project");

$form->add_section("Project");
require_once "include/project_head_small.php";

$form->add_section("Cost Overview");
$form->add_label("grand_total1", "Standard/Special Items in " . $order_currency["symbol"], 0, $grand_total_in_order_currency);

$form->add_label("grand_total6", "Estimation of Local Construction Cost in " . $order_currency["symbol"], 0, number_format($local_construction_item_total["in_order_currency"],2));

$form->add_label("grand_total2", "Estimation of Add. Cost in " . $order_currency["symbol"], 0, number_format($cost_estimation_item_total["in_order_currency"],2));
$form->add_label("grand_total3", "Total Cost " . $order_currency["symbol"], 0, number_format($total_cost,2));



/********************************************************************
    Create Standard Item List
*********************************************************************/ 
$list1 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list1->set_title("Catalog Items");
$list1->set_entity("order_item");
$list1->set_filter($list1_filter);
$list1->set_order("item_code");
$list1->set_group("cat_name");

$list1->add_hidden("pid", param("pid"));
$list1->add_hidden("oid",$project["project_order"]);

$list1->add_text_column("pos", "No.", 0, $line_numbers[ITEM_TYPE_STANDARD]);
$list1->add_column("item_shortcut", "Item Code", "popup:catalog_item_view.php?id={item_id}");
$list1->add_column("order_item_text", "Name");

$list1->add_column("order_item_quantity_freezed", "Budget", "", "", "", COLUMN_ALIGN_RIGHT);
$list1->add_column("order_item_client_price_freezed", "Approved " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list1->add_column("total_price_freezed", "Total Approved " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

$list1->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);

$list1->add_column("unit_name", "Unit", "", "", "", COLUMN_NO_WRAP);
$list1->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

$list1->add_column("total_price", "Total " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);


$list1->set_footer("item_code", "Total");
$list1->set_footer("total_price", number_format($standard_item_total["in_order_currency"],2));
$list1->set_footer("total_price_freezed", number_format($standard_item_total_freezed["in_order_currency"],2));



/********************************************************************
    Create Special Item List
*********************************************************************/ 
$list2 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list2->set_title("Special Items");
$list2->set_entity("order_item");
$list2->set_filter($list2_filter);
//$list2->set_order("item_code_text");

$list2->add_hidden("pid", param("pid"));
$list2->add_hidden("oid",$project["project_order"]);

$list2->add_text_column("pos", "No.", 0, $line_numbers[ITEM_TYPE_SPECIAL]);
$list2->add_column("item_shortcut", "Item Code");
$list2->add_column("order_item_text", "Name");

$list2->add_column("order_item_quantity_freezed", "Budget", "", "", "", COLUMN_ALIGN_RIGHT);
$list2->add_column("order_item_client_price_freezed", "Approved " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

$list2->add_column("total_price_freezed", "Total Approved" . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

$list2->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
$list2->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

$list2->add_column("total_price", "Total " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

$list2->set_footer("item_code", "Total");
$list2->set_footer("total_price_freezed", number_format($special_item_total_freezed["in_order_currency"],2));
$list2->set_footer("total_price", number_format($special_item_total["in_order_currency"],2));


/********************************************************************
    Create Locla COnstruction Cost Positions
*********************************************************************/ 
$list6 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list6->set_title("Local Construction Cost Estimation");
$list6->set_entity("order_item");
$list6->set_filter($list6_filter);
$list6->set_order("item_code, order_item_id");

$list6->add_hidden("pid", param("pid"));
$list6->add_hidden("oid",$project["project_order"]);

$list6->add_text_column("pos", "No.", 0, $line_numbers[ITEM_TYPE_LOCALCONSTRUCTIONCOST]);
$list6->add_column("item_shortcut", "Code");
$list6->add_column("order_item_text", "Text");

$list6->add_column("order_item_client_price_freezed", "Approved" . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
$list6->add_column("order_item_client_price", "Estimated " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);

$list6->set_footer("item_code", "Total");
$list6->set_footer("order_item_client_price_freezed", number_format($local_construction_item_total_freezed["in_order_currency"],2));
$list6->set_footer("order_item_client_price", number_format($local_construction_item_total["in_order_currency"],2));


/********************************************************************
    Create Cost Estimation Positions
*********************************************************************/ 
$list3 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list3->set_title("Cost Estimation");
$list3->set_entity("order_item");
$list3->set_filter($list3_filter);

$list3->add_hidden("pid", param("pid"));
$list3->add_hidden("oid",$project["project_order"]);

$list3->add_text_column("pos", "No.", 0, $line_numbers[ITEM_TYPE_COST_ESTIMATION]);
$list3->add_column("item_shortcut", "Code");
$list3->add_column("order_item_text", "Text");

$list3->add_column("order_item_client_price_freezed", "Approved Cost " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
$list3->add_column("order_item_client_price", "Estimated Cost " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);

$list3->set_footer("item_code", "Total");
$list3->set_footer("order_item_client_price_freezed", number_format($cost_estimation_item_total_freezed["in_order_currency"],2));
$list3->set_footer("order_item_client_price", number_format($cost_estimation_item_total["in_order_currency"],2));


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list1->populate();
$list1->process();

$list2->populate();
$list2->process();

$list3->populate();
$list3->process();

$list6->populate();
$list6->process();


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Project's List of Material");

$form->render();

echo "<br>";
$list1->render();
echo "<br>";
$list2->render();
echo "<br>";

$list6->render();
echo "<br>";

$list3->render();
echo "<br>";


$page->footer();

?>