<?php
/********************************************************************

    welcome_message.php

    View message detail.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-10-04
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-10-06
    Version:        1.1.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access();

$message_data = get_message(id());

$page = new Page(PAGE_POPUP, "Welcome");
$page->header();
$page->title($message_data["title"]);

/*
echo "<p class=\"message_text\">", smart(htmlspecialchars($message_data["text"])), "</p>";
*/

echo "<p class=\"message_text\">", $message_data["text"], "</p>";


echo "<a href=\"javascript:window.close()\" class=\"value\">Close</a>";

$page->footer();