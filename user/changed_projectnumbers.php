<?php
/********************************************************************

    changed_projectnumbers.php

    Lists chaned project numbers

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-04-19
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-04-19
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_view_projects");


$user_data = get_user(user_id());
$user_roles = get_user_roles(user_id());
$project_state_restrictions = get_project_state_restrictions(user_id());


$sql = "select country_name, order_shop_address_company, " . 
       "concat(user_name, ' ', user_firstname) as username, " .
	   "oldproject_number_old_number, oldproject_number_new_number, " . 
	   "oldproject_numbers.date_created as date " . 
       "from oldproject_numbers "  .
	   "left join projects on project_id = oldproject_number_project_id " . 
	   "left join orders on order_id = project_order " . 
	   "left join countries on country_id = order_shop_address_country " . 
	   "left join users on user_id = oldproject_number_user_id";


// create list filter
$condition = "";
if(in_array(1, $user_roles) or in_array(2, $user_roles) or in_array(3, $user_roles) or in_array(8, $user_roles))
{
	$show_cer_state = true;
	if (has_access("has_access_to_all_projects") and has_access("can_view_all_entries_at_start_in_projects"))
    {
       
		$list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820' ";
    }
    else if (has_access("has_access_to_all_projects") and param("showall"))
    {
        $list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820' ";
    }
    else if (has_access("has_access_to_all_projects") and !param("showall"))
    {
		$list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820'  " .
                       " and (project_retail_coordinator = " . user_id() .
                       " or project_design_supervisor = " . user_id() . 
                       " or project_design_contractor = " . user_id() . 
                       " or order_retail_operator = " . user_id() . 
                       " or order_retail_operator is null " . 
			           " or order_actual_order_state_code = '100')";
    }
    else
    {
		
		if (has_access("can_view_order_before_budget_approval_in_projects"))
		{
            $list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820'  " .
               " and (project_retail_coordinator = " . user_id() .
               " or project_design_supervisor = " . user_id() . 
               " or project_design_contractor = " . user_id() . 
               " or order_retail_operator = " . user_id() . 
               " or order_retail_operator is null)";
        }
        else
        {
            $list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820'  " .
               "   and order_show_in_delivery = 1 ".
               " and (project_retail_coordinator = " . user_id() .
               " or project_design_supervisor = " . user_id() . 
               " or project_design_contractor = " . user_id() . 
               " or order_retail_operator = " . user_id() . 
               " or order_retail_operator is null)";
        }
    }

    if(in_array(7, $user_roles)) // design contractor
    {
        $list_filter.= " and project_design_contractor = "  . user_id();
    }

}
else
{

	$condition = get_user_specific_order_list(user_id(), 1, $user_roles, $user_data["has_access_to_travelling_projects"]);

    if (has_access("has_access_to_all_projects") and has_access("can_view_all_entries_at_start_in_projects"))
    {
        $list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820' ";

		$show_cer_state = true;
		
		
    }
    else if (has_access("has_access_to_all_projects") and param("showall"))
    {
        $list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820' ";
		$show_cer_state = true;

    }
    else if (has_access("has_access_to_all_projects") and !param("showall"))
    {

        if ($condition == "")
        {
            $list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820'  " .
                           "   and order_retail_operator is null";
        }
        else
        {
            $list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820'  " .
                           "   and (" . $condition . " or order_retail_operator is null)";
        }
		$show_cer_state = true;
    }
    else
    {
		if ($condition == "")
        {
            // order_type = 0 is a work around to sho no orders
            $list_filter = "order_type = 0 ";
        }
        else
        {
           
			if (has_access("can_view_order_before_budget_approval_in_projects"))
            {   
				$list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820'  ".
                               "   and (" . $condition . ")";
				
            }
        else
            {
               
				$list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820'  ".
                               "   and order_show_in_delivery = 1 ".
                               "   and (" . $condition . ")";

            }
        }
    }
}


if($project_state_restrictions['from_state'])
{
	$list_filter .= " and order_actual_order_state_code >= '" . $project_state_restrictions['from_state'] . "' ";
}
if($project_state_restrictions['to_state'])
{
	$list_filter .= " and order_actual_order_state_code <= '" . $project_state_restrictions['to_state'] . "' ";
}


//get the latest year

/*
$years = array();
$first_year = "";
$sql_y = "select DISTINCT YEAR(oldproject_numbers.date_created) as year " . 
		   "from oldproject_numbers "  .
		   "left join projects on project_id = oldproject_number_project_id " . 
		   "left join orders on order_id = project_order " . 
		   "left join countries on country_id = order_shop_address_country " . 
		   "left join users on user_id = oldproject_number_user_id" . 
		   " where " . $list_filter .
		   " order by year DESC";

$res = mysql_query($sql_y) or dberror($sql_y);
while ($row = mysql_fetch_assoc($res))
{
	$years[$row["year"]] = $row["year"];

	if($first_year == "")
	{
		$first_year = $row["year"];
	}
}



$selected_year = "";
if(param("year") > 0)
{
	$list_filter .= " and YEAR(oldproject_numbers.date_created) = " . param("year");
	$selected_year = param("year");
}
elseif(count($years) > 0)
{
	$selected_year = $first_year;
	$list_filter .= " and YEAR(oldproject_numbers.date_created) = " . $selected_year;
	
}
*/
/********************************************************************
   Search Form
*********************************************************************/ 
/*
$form = new Form("addresses", "address");
$form->add_section("List Filter Selection");
$form->add_list("year", "Year", $years, SUBMIT, $selected_year);
*/

/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);

$list->set_entity("oldproject_numbers");
$list->set_order("oldproject_number_old_number, country_name, order_shop_address_company");
$list->set_filter($list_filter);


$list->add_column("oldproject_number_old_number", "Old Number");
$list->add_column("oldproject_number_new_number", "New Number");
$list->add_column("country_name", "Country");
$list->add_column("order_shop_address_company", "Project Name");

$list->add_column("username", "Changed by");
$list->add_column("date", "Date");

$list->process();

$page = new Page("projects");


$page->register_action('projects', 'Projects', "projects.php");
if (has_access("can_create_new_projects"))
{
    $page->register_action('new', 'New Project', "project_new_01.php");
}


$page->header();
$page->title("Changed Project Numbers");
//$form->render();
$list->render();
$page->footer();

?>
