<?php
/********************************************************************

    project_view_comment.php

    view comment of a project

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-05-18
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-18
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_view_comments_in_projects");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));


// get company's address
$client_address = get_address($project["order_client_address"]);


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("comments", "comment");


$form->add_section("Project");
$form->add_hidden("pid", param('pid'));
$form->add_hidden("order_id", $project["project_order"]);


require_once "include/project_head_small.php";


$form->add_section("Comment");


$form->add_lookup("comment_user", "Added by", "users", "Concat(user_name, ' ', user_firstname)", 0, "comment_user");

$form->add_label("date_created", "Date", 0);

$form->add_lookup("comment_category", "Category", "comment_categories", "comment_category_name", 0, id());

$form->add_label("comment_text", "Comment", 0);


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


$page = new Page("projects");


require "include/project_page_actions.php";


$page->header();
$page->title("View Comment");
$form->render();
$page->footer();


?>