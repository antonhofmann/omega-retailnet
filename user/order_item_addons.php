<?php
/********************************************************************

    order_category.php

    "New order" catalog browser.

    Created by:     Oliver Hofer (oliver.hofer@pageagent.com)
    Date created:   2002-08-20
    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date modified:  2002-09-23
    Version:        1.0.5

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/save_functions.php";
require_once "include/order_functions.php";

check_access("can_view_orders");

// set-up page

$page = new Page("orders");
$page->register_action('basket', 'Shopping List');

$addon_items = array();
$addon_items = explode ('_' , param('items'));

if (isset($addon_items))
{
    $sql = "select addon_id, items.item_id, items.item_code, items.item_name, addon_package_quantity, " .
           "    addon_min_packages, addon_max_packages, items.item_priority, " . 
           "    concat_ws(' ', addon_min_packages, items.item_code) as item_shortcut, " .
           "    concat_ws(': ', itm.item_code, itm.item_name) as group_head " .
           "from addons " .
           "left join items on items.item_id = addons.addon_child " .
           "left join items as itm on itm.item_id = addons.addon_parent ";

}

$list = new ListView($sql);

$list->set_entity("category_items");
$list->set_filter("addon_parent in ( " . join(',', $addon_items) . ")" .
                  " and items.item_visible = 1");
$list->set_order("item_priority");
$list->set_group("group_head", "group_head");

$list->add_column("item_shortcut", "Item Code", "popup:catalog_item_view.php?id={item_id}", LIST_FILTER_FREE, '', COLUMN_NO_WRAP);
$list->add_column("item_name", "Name", "", LIST_FILTER_FREE);

// intialize quantities: set quantity of all items in this category to 0

$values = array();
$sql_q = $sql . "where addon_parent in ( " . join(',', $addon_items) . ")" .
                " and items.item_visible = 1";
$res = mysql_query($sql_q ) or dberror($sql_q );
while ($row = mysql_fetch_assoc($res))
{
    $values[$row['addon_id']] = '0.00';
}

// set number of items in shoping list: Update quantity from items in shoping-list

if (!$list->button("basket_add"))
{
    $sql_basket = "select basket_item_item, basket_item_quantity, addon_id " .
                  "from basket_items " .
                  "left join addons on addon_child = basket_item_item " .
                  "where basket_item_basket= " . get_users_basket() . " and " .
                  "    addon_package_quantity = 1";

    $res = mysql_query($sql_basket);
    if ($res)
    {
        while ($row = mysql_fetch_assoc($res))
        {
            $values[$row['addon_id']] = $row['basket_item_quantity'];
        }
    }
}

$list->add_hidden("cid", param('cid'));
$list->add_hidden("items", param('items'));
$list->add_edit_column("item_quantity", "Quantity", "2", 0, $values);
$list->add_button(LIST_BUTTON_BACK, "Back");
$list->add_button("basket_add", "Add to Shopping List");
$list->add_button("cancel_order", "Cancel Ordering");

$list->populate();
$list->process();

if ($list->button("basket"))
{
    redirect("basket.php");
}

if ($list->button("basket_add"))
{
    $add_this_items = array();
    foreach($list->values("item_quantity") as $key => $value)
    {
        $add_this_items[$key] = $value;
    }

    update_shopping_list($add_this_items, get_users_basket(), param("cid"), true);
    redirect("basket.php");
}

if ($list->button("cancel_order"))
{
	redirect("orders.php");
}

// form to display comment

$form = new Form();
$form->add_comment("To make your order complete you have to choose one or more from " .
                   "the following addons belonging to the choosen product line: ");

$sql_name ="select concat_ws(': ', product_line_name, category_name), " .
           "from categories " .
           "left join category_items on category_items.category_item_item = items.item_id " .
           "left join categories on category_items.category_item_category = categories.category_id " .
           "left join product_lines on product_lines.product_line_id = categories.category_product_line " .
           "where" ;

$page->header();
$page->title("Select Addons");
$form->render();
$list->render();
$page->footer();

?>