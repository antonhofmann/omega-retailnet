<?php
/********************************************************************

    projects_cms_completed.php

    Entry page for the projects section.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2012-03-06
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2012-03-06
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/order_state_constants.php";

check_access("can_view_projects");

register_param("showall");

/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());
$user_roles = get_user_roles(user_id());
$project_state_restrictions = get_project_state_restrictions(user_id());

// create sql
$sql = "select distinct project_id, project_number, project_projectkind, " .
       "project_actual_opening_date, product_line_name, postype_name, " . 
       "concat(order_shop_address_place,', ', order_shop_address_company), country_name, ".
       "    rtcs.user_name as project_retail_coordinator, rtos.user_name as order_retail_operator,".
       "    order_id, order_actual_order_state_code, project_costtype_text,  ".
	   " project_cost_cms_completed, project_cost_cms_approved, project_state_text, " .
	   "if(project_state IN (2,4), " . 
	   "if(project_state = 2, concat('<span class=\"error\">', project_state_text, '</span>'), concat('<strong>', project_state_text, '</strong>')), project_real_opening_date) as real_opening_date ".
       "from projects ".
       "left join orders on project_order = order_id ".
	   "inner join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
       "left join product_lines on project_product_line = product_line_id ".
       "left join postypes on postype_id = project_postype ".
	   "left join project_states on project_state_id = project_state ".
       "left join countries on order_shop_address_country = countries.country_id ".
       "left join users as rtcs on project_retail_coordinator = rtcs.user_id " . 
	   "left join users as rtos on order_retail_operator = rtos.user_id ";

$sql_count = "select count(project_id) as num_recs " . 
       "from projects ".
       "left join orders on project_order = order_id ".
	   "inner join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join project_states on project_state_id = project_state ".
       "left join product_lines on project_product_line = product_line_id ".
       "left join postypes on postype_id = project_postype ".
       "left join countries on order_shop_address_country = countries.country_id ".
       "left join users as rtcs on project_retail_coordinator = rtcs.user_id " . 
	   "left join users as rtos on order_retail_operator = rtos.user_id ";


if (has_access("has_access_to_all_projects"))
{
	$list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820' ";
}
else
{

	$condition = get_user_specific_order_list(user_id(), 1, $user_roles);

	if ($condition == "")
	{
		$list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820'  " .
					   "   and order_retail_operator is null";
	}
	else
	{
		$list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820'  " .
					   "   and (" . $condition . " or order_retail_operator is null)";
	}
}


//check if there is only one project

if(param('search_term')) {

	$search_term = trim(param('search_term'));
	$list_filter .= ' and (project_number like "%' . $search_term . '%" 
	                  or postype_name like "%' . $search_term . '%" 
					  or project_costtype_text like "%' . $search_term . '%"
					  or project_state_text like "%' . $search_term . '%"
					  or product_line_name like "%' . $search_term . '%"
					  or postype_name like "%' . $search_term . '%"
					  or order_actual_order_state_code like "%' . $search_term . '%"
					  or order_shop_address_place like "%' . $search_term . '%"
					  or order_shop_address_company like "%' . $search_term . '%"
					  or country_name like "%' . $search_term . '%")';

	$sql_count = $sql_count . ' where ' . $list_filter;
	$res = mysql_query($sql_count) or dberror($sql_count);

	if ($row = mysql_fetch_assoc($res))
	{
		if ($row['num_recs'] == 1)
		{
			$sql = $sql . ' where ' . $list_filter;
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$link = "project_task_center.php?pid=" . $row['project_id'];
				redirect($link);
			}
		}
	}
}


$images = array();
$cms_states = array();
$aods = array();

if(param('search_term')) {

	$sql_images = "select project_id from tasks " .
		   "left join orders on order_id = task_order " .
		   "left join projects on project_order = order_id " .
		   "left join project_costs on project_cost_order = order_id " .
		   "left join project_costtypes on project_costtype_id = project_cost_type " .
		   "left join project_states on project_state_id = project_state ".
		   "left join product_lines on project_product_line = product_line_id ".
		   "left join postypes on postype_id = project_postype ".
		   "left join countries on order_shop_address_country = countries.country_id ".
		   "where task_user = " . user_id() .
		   "   and task_done_date is null " .
		   "   and " . $list_filter;
}
else
{
	$sql_images = "select project_id from tasks " .
		   "left join orders on order_id = task_order " .
		   "left join projects on project_order = order_id " .
		   "where task_user = " . user_id() .
		   "   and task_done_date is null " .
		   "   and " . $list_filter;
}

$res = mysql_query($sql_images) or dberror($sql_images);

while ($row = mysql_fetch_assoc($res))
{
    $images[$row["project_id"]] = "/pictures/todo.gif";
}

if (has_access("can_edit_retail_data"))
{
    
	if(param('search_term')) {
		
		$sql_images = "select project_id from projects " .
					  "left join orders on order_id = project_order " .
			          "left join project_costs on project_cost_order = order_id " .
					   "left join project_costtypes on project_costtype_id = project_cost_type " .
					   "left join product_lines on project_product_line = product_line_id ".
					   "left join postypes on postype_id = project_postype ".
					   "left join countries on order_shop_address_country = countries.country_id ".
						"left join project_states on project_state_id = project_state ".
					  "where (order_retail_operator is null " .
					  "or project_retail_coordinator is null " .
					  "or project_real_opening_date is null " . 
					  "or project_real_opening_date = '0000-00-00') " .
					  "and order_actual_order_state_code = '100' ".
					  "   and " . $list_filter;
	}
	else
	{
		$sql_images = "select project_id from projects " .
					  "left join orders on order_id = project_order " .
					  "where (order_retail_operator is null " .
					  "or project_retail_coordinator is null " .
					  "or project_real_opening_date is null " . 
					  "or project_real_opening_date = '0000-00-00') " .
					  "and order_actual_order_state_code = '100' ".
					  "   and " . $list_filter;
	}

    $res = mysql_query($sql_images) or dberror($sql_images);

    while ($row = mysql_fetch_assoc($res))
    {
        $images[$row["project_id"]] = "/pictures/bullet_ball_glass_red.gif";
    }
}


// check if all 1:n relations in the process are fully processed
$fully_processed = array();
$cms_approved_states = array();
if (has_access("has_access_to_all_projects"))
{
    $sql_tmp = $sql . " where " . $list_filter;
    $res = mysql_query($sql_tmp);

    while($row = mysql_fetch_assoc($res))
    {
        $att = "";

        if($row['order_actual_order_state_code'] >= ORDER_TO_SUPPLIER_SUBMITTED)
        {
            //check if all items were ordered
            $ordered = check_if_all_items_hav_order_date($row['order_id'], "");
            if ($ordered == 0)
            {
                $fully_processed[$row['project_id']] = "/pictures/wf_warning.gif";
            }
        }
        elseif($row['order_actual_order_state_code'] >= REQUEST_FOR_DELIVERY_SUBMITTED)
        {
            //check if a request for delivery was made for all items
            $delivered = check_requast_for_deleivery_for_all_items($row['order_id'], REQUEST_FOR_DELIVERY_SUBMITTED);
            if ($delivered == 0)
            {
                $fully_processed[$row['project_id']] = "/pictures/wf_warning.gif";
            }
        }

		if($row['project_actual_opening_date'] != NULL and $row['project_actual_opening_date'] != '0000-00-00')
		{
			$aods[$row["project_id"]] = "/pictures/ok.gif";
		}
		else
		{
			$aods[$row["project_id"]] = "/pictures/not_ok.gif";
		}

		if($row['project_cost_cms_completed'] == 1)
		{
			$cms_states[$row["project_id"]] = "/pictures/ok.gif";
		}
		else
		{
			$cms_states[$row["project_id"]] = "/pictures/not_ok.gif";
		}


		if($row['project_cost_cms_approved'] == 1)
		{
			$cms_approved_states[$row["project_id"]] = "/pictures/ok.gif";
		}
		else
		{
			$cms_approved_states[$row["project_id"]] = "/pictures/not_ok.gif";
		}
		
    }
}


$list_filter .= " and project_cost_cms_completed = 1 and order_actual_order_state_code >= 800 ";

/********************************************************************
   Search Form
*********************************************************************/ 
if (has_access("has_access_to_all_projects"))
{
	$form = new Form("addresses", "address");

	$form->add_section("List Filter Selection");

	$form->add_edit("search_term", "Search Term", 0, param('search_term'), TYPE_CHAR, 20);

}

/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);

$list->set_entity("projects");
$list->set_order("left(projects.date_created, 10) desc");
$list->set_filter($list_filter);   

$list->add_image_column("todo", "Job", 0, $images);


if (preg_match("/" . APPLICATION_URLPART . "/", $_SERVER["HTTP_HOST"]))
{
	$link = APPLICATION_URL . "/user";
}
else
{
	$link = "/user";
}

$list->add_column("project_number", "Project No.", $link . "/project_task_center.php?pid={project_id}", "", "", COLUMN_NO_WRAP);
$list->add_column("project_costtype_text", "Legal Type", "", 0, "", COLUMN_NO_WRAP);
$list->add_column("product_line_name", "Product Line", "", 0, "", COLUMN_NO_WRAP);
$list->add_column("postype_name", "POS Type", "", 0, "", COLUMN_NO_WRAP);



$list->add_image_column("order_state", "", 0, $fully_processed);

if(has_access("has_access_to_order_status_report_in_projects"))
{
    $popup_link = "popup1:project_status_report.php?pid=";
    $list->add_column("order_actual_order_state_code", "Status", $popup_link . "{project_id}");
}
else
{
    $list->add_column("order_actual_order_state_code", "Status");
}

$list->add_column("real_opening_date", "Agreed \nOpening Date", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK | COLUMN_UNDERSTAND_HTML);


$list->add_column("country_name", "Country", "", 0, "");
$list->add_column("concat(order_shop_address_place,', ', order_shop_address_company)", "POS Address");

$list->add_column("project_retail_coordinator", "PM" , "", 0, "");
$list->add_column("order_retail_operator", "RTO", "", 0, "");

if (has_access("has_access_to_all_projects"))
{
	$list->add_image_column("cms", "CMS", 0, $cms_states);
	$list->add_image_column("cms", "APP", 0, $cms_approved_states);

	
	$list->add_image_column("aod", "AOD", 0, $aods);
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$list->process();


if ($list->button("new"))
{
    redirect("project_new_01.php");
}
if ($list->button("show_all"))
{
    $link = "projects.php?showall=1";
    redirect($link);
}
else if ($list->button("show_my_list"))
{
    $link = "projects.php";
    redirect($link);
}
else if ($list->button("extend"))
{
    if (param("showall"))
    {
        $link = "projects_extended_view.php?showall=1";
    }
    else
    {
        $link = "projects_extended_view.php";
    }
    redirect($link);
}
else if ($list->button("archive"))
{
    $link = "../archive/projects_archive.php";
    redirect($link);
}

$page = new Page("projects");

if (has_access("has_access_to_projects_dashboard"))
{
	$page->register_action('dashboard', "Dashboard", "", "", "dashboard");
}

if(has_access("has_access_to_all_projects") or has_access("can_complete_cms") or has_access("can_approve_cms"))
{
	$page->register_action('home', 'Projects', "projects.php");
}

if (has_access("can_create_new_projects"))
{
    $page->register_action('new', 'New Project', "project_new_01.php");
}




$page->register_action('home', 'Home', "welcome.php");

$page->header();
$page->title("Projects with completed CMS");

if (has_access("has_access_to_all_projects"))
{
	$form->render();
}
$list->render();

?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#dashboard').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/user/dashboard_projects.php'
    });
    return false;
  }); 
});
</script>

<?php

$page->footer();
?>