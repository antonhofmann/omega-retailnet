<?php
/********************************************************************

    order_edit_material_list.php

    Edit List of Materials

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-09
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-11-13
    Version:        1.2.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/order_functions.php";
require_once "include/save_functions.php";

check_access("can_edit_list_of_materials_in_orders");

register_param("oid");
set_referer("order_add_catalog_item_categories.php");
set_referer("order_edit_material_list_edit_item.php");
set_referer("order_add_catalog_item_special_item_list.php");
set_referer("order_add_special_item_individual.php");
set_referer("order_add_catalog_item_cost_estimation_list.php");
set_referer("order_add_cost_estimation_individual.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$order = get_order(param('oid'));

// get company's address
$client_address = get_address($order["order_client_address"]);

// get System currency
$system_currency = get_system_currency_fields();


$sql_suppliers = "select DISTINCT address_id, address_shortcut " . 
                 "from order_items " . 
				 "left join suppliers on supplier_item = order_item_item " . 
				 "left join addresses on address_id = supplier_address " . 
				 "where address_shortcut <> '' and order_item_order = " . param('oid') . 
				 " order by address_shortcut";




// create sql for oder items
$sql_order_items = "select order_item_id, order_item_text, order_item_quantity, ".
                   "    order_item_po_number, order_item_system_price, item_id, ".
                   "    if(order_item_not_in_budget=1 , 'no', 'yes') as order_item_in_budget, order_item_supplier_address, ".
                   "    if(item_code <>'', item_code, item_type_name) as item_shortcut, ".
                   "    category_priority, category_name, ".
                   "    addresses.address_shortcut as supplier_company, ".
                   "    addresses_1.address_shortcut as forwarder_company, ".
                   "    item_type_id, item_type_name, ".
                   "    item_type_priority, item_stock_property_of_swatch, unit_name, ".
                   "    concat_ws(': ', product_line_name, category_name) as group_head " .
                   "from order_items ".
                   "left join items on order_item_item = item_id ".
                   "left join categories on order_item_category = category_id ".
                   "left join product_lines on categories.category_product_line = product_lines.product_line_id " .
                   "left join addresses on order_item_supplier_address = addresses.address_id ".
                   "left join addresses as addresses_1 ".
                   "     on order_item_forwarder_address = addresses_1.address_id ".
                   "left join item_types on order_item_type = item_type_id  " .
				   "left join units on unit_id = item_unit";

$where_clause = " where (order_item_type = " . ITEM_TYPE_STANDARD . 
                "    or order_item_type = " . ITEM_TYPE_SPECIAL . ") ".
                "    and order_item_order = " . param('oid');

$values = array();
$item_suppliers = array();
$property = array();

$res = mysql_query($sql_order_items . $where_clause) or dberror($sql_order_items . $where_clause);
while ($row = mysql_fetch_assoc($res))
{
    $values[$row["order_item_id"]] = $row["order_item_quantity"];
	$item_suppliers[$row["order_item_id"]] = $row["order_item_supplier_address"];

	if($row["item_stock_property_of_swatch"] == 1)
	{
		$property[$row["order_item_id"]] = "/pictures/omega.gif";
	}
}


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("orders", "order", 640);

$form->add_section("Order");
$form->add_hidden("oid", param('oid'));
$form->add_hidden("order_file_order", param('oid'));

require_once "include/order_head_small.php";

if ($order["order_budget_is_locked"] == 1)
{
    $form->error = "Budget is locked, quantities and prices can not be changed anymore.";
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Create List for Catalog Items
*********************************************************************/ 
$list1 = new ListView($sql_order_items, LIST_HAS_HEADER);

$list1->set_title("Catalog Items");
$list1->set_entity("order_item");
$list1->set_filter("order_item_type = " . ITEM_TYPE_STANDARD . " and order_item_order = " . param('oid'));
$list1->set_order("supplier_company, item_code");
$list1->set_group("group_head", "group_head");

$link="order_edit_material_list_edit_item.php?oid=" . param('oid');

$list1->add_column("item_shortcut", "Item Code", $link);
$list1->add_image_column("property", "", 0, $property);

$list1->add_column("order_item_in_budget", "in Budget", "", "", "", COLUMN_ALIGN_CENTER);
$list1->add_column("order_item_text", "Name");

if ($order["order_budget_is_locked"] == 1)
{
    $list1->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
}
else
{
    $list1->add_edit_column("item_entry_field", "Quantity", "4", 0, $values);
}

$list1->add_column("unit_name", "Unit", "", "", "", COLUMN_NO_WRAP);
$list1->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list1->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);
$list1->add_column("supplier_company", "Supplier");
//$list1->add_list_column("item_suppliers", "Supplier", $sql_suppliers, NOTNULL, $item_suppliers);
$list1->add_column("forwarder_company", "Forwarder");

if ($order["order_budget_is_locked"] == 1)
{
        $list1->add_button("nothing", "");
}
else
{
    $list1->add_button("save_items", "Save");
    $list1->add_button("add_items", "Add Catalog Items");
    $list1->add_button("delete_items_1", "Delete All Catalog Items");
}


/********************************************************************
    Create List for Special Items
*********************************************************************/ 
$list2 = new ListView($sql_order_items, LIST_HAS_HEADER);

$list2->set_title("Special Items");
$list2->set_entity("order_item");
$list2->set_filter("order_item_type = " . ITEM_TYPE_SPECIAL . " and order_item_order = " . param('oid'));
$list2->set_order("supplier_company, order_item_text");

$link="order_edit_material_list_edit_item.php?oid=" . param("oid");

$list2->add_column("item_shortcut", "Item Code", $link);

$list2->add_column("order_item_in_budget", "in Budget", "", "", "", COLUMN_ALIGN_CENTER);
$list2->add_column("order_item_text", "Name");

if ($order["order_budget_is_locked"] == 1)
{
    $list2->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
}
else
{
    $list2->add_edit_column("item_entry_field", "Quantity", "4", 0, $values);
}

$list2->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
$list2->add_column("order_item_po_number", "P.O. Number");
$list2->add_column("supplier_company", "Supplier");
$list2->add_column("forwarder_company", "Forwarder");

if ($order["order_budget_is_locked"] == 1)
{
    $list2->add_button("nothing", "");
}
else
{
    $list2->add_button("save_items_2", "Save");
    $list2->add_button("add_special_items_1", "Add Special Items from Catalog");
    $list2->add_button("add_special_items_2", "Add Individual Special Item");
    $list2->add_button("delete_items_2", "Delete All Special Items");
}


/********************************************************************
    Create List for Cost Estimation Positions
*********************************************************************/ 
$list3 = new ListView($sql_order_items, LIST_HAS_HEADER);

$list3->set_title("Cost Estimation");
$list3->set_entity("order_item");
$list3->set_filter("order_item_type = " . ITEM_TYPE_COST_ESTIMATION . " and order_item_order = " . param('oid'));
$list3->set_order("item_code");

$link="order_edit_material_list_edit_item.php?pid=" . param("pid");

if ($order["order_budget_is_locked"] == 1)
{
    $list3->add_column("item_shortcut", "Item Code");
}
else
{
    $list3->add_column("item_shortcut", "Item Code", $link);
}

$list3->add_column("order_item_text", "Name");
$list3->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);

if ($order["order_budget_is_locked"] == 1)
{
    $list3->add_button("nothing", "");
}
else
{
    $list3->add_button("add_cost_1", "Add Cost Estimation from Catalog");
    $list3->add_button("add_cost_2", "Add Individual Cost Estimation");
    $list3->add_button("delete_items_3", "Delete All Positions");
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list1->populate();
$list1->process();

if ($list1->button("add_items"))
{
    $link = "order_add_catalog_item_categories.php?oid=" . param('oid'); 
    redirect($link);
}
elseif ($list1->button("save_items"))
{
    project_update_order_items($list1);
}
elseif ($list1->button("delete_items_1"))
{
    order_delete_all_items(param('oid'), 1);
}

$list2->populate();
$list2->process();


if ($list2->button("add_special_items_1"))
{
    $link = "order_add_catalog_item_special_item_list.php?oid=" . param('oid');
    redirect($link);
}
elseif ($list2->button("add_special_items_2"))
{
    $link = "order_add_special_item_individual.php?oid=" . param('oid'); 
    redirect($link);
}
elseif ($list2->button("save_items_2"))
{
	project_update_order_items($list2);
}
elseif ($list2->button("delete_items_2"))
{
    order_delete_all_items(param('oid'), 2);
}


$list3->populate();
$list3->process();


if ($list3->button("add_cost_1"))
{
    $link = "order_add_catalog_item_cost_estimation_list.php?oid=" . param('oid');
    redirect($link);
}
elseif ($list3->button("add_cost_2"))
{
    $link = "order_add_cost_estimation_individual.php?oid=" . param('oid'); 
    redirect($link);
}
elseif ($list3->button("delete_items_3"))
{
    order_delete_all_items(param('oid'), 3);
}


/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("Edit List of Materials");
$form->render();
echo "<br>";
$list1->render();
echo "<br>";
$list2->render();
echo "<br>";
$list3->render();
echo "<br>";
$page->footer();

?>