<?php
/********************************************************************

    projects_extended_view.php

    Entry page for the projects section.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-06-18
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.
*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";


check_access("can_view_projects");


register_param("showall");
set_referer("project_new_01.php");


/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());
$user_roles = get_user_roles(user_id());
$project_state_restrictions = get_project_state_restrictions(user_id());


// create sql
$sql = "select  project_id, ".
       "   concat(project_number, ', ',  product_line_name, ', ', country_name, ', ', order_shop_address_place, ', ', order_shop_address_company) as group_head,  ".
       "   if(item_code > '', item_code, 'Special Item') as item_shortcut, " .
       "   order_item_po_number, order_item_cost_unit_number, order_item_quantity, order_id, ".
       "   addresses_1.address_company as supplier, addresses_2.address_company as forwarder ".
       "from orders ".
       "left join projects on project_order = order_id ".
       "left join order_items on order_item_order = order_id ".
       "left join items on order_item_item = item_id ".
       "left join product_lines on project_product_line = product_line_id ".
       "left join countries on order_shop_address_country = countries.country_id ".
       "left join addresses as addresses_1 on order_item_supplier_address = addresses_1.address_id ".
       "left join addresses as addresses_2 on order_item_forwarder_address = addresses_2.address_id ".       
       "left join users on project_retail_coordinator = users.user_id ";


// create list filter
$condition = get_user_specific_order_list(user_id(), 1, $user_roles);


if (has_access("has_access_to_all_projects") and has_access("can_view_all_entries_at_start_in_projects"))
{
    $list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') ".
                   "   and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
                   "   and order_item_type <= " . ITEM_TYPE_SPECIAL;
}
else if (has_access("has_access_to_all_projects") and param("showall"))
{
    $list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') ".
                   "   and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
                   "   and order_item_type <= " . ITEM_TYPE_SPECIAL;


}
else if (has_access("has_access_to_all_projects") and !param("showall"))
{
        $list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') ".
                       "   and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
                       "   and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                       "   and (order_item_supplier_address = " . $user_data["address"] . " " .
                       "   or order_item_forwarder_address = " . $user_data["address"] . " " .
                       "   or order_retail_operator = " . user_id() . " " .
                       "   or project_retail_coordinator = " . user_id()  . " " .
                       "   or project_design_contractor = " . user_id() . " " .
                       "   or project_design_supervisor = " . user_id() . " " .
                       "   or order_client_address = " . $user_data["address"] . ")";
}
else
{

    if ($condition == "")
    {
         if (has_access("can_view_order_before_budget_approval_in_projects") and has_access("can_view_order_before_booklet_approval"))
         {   
            
            $list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') ".
                           "   and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
                           "   and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                           "   and (order_item_supplier_address = " . $user_data["address"] . " " .
                           "   or order_item_forwarder_address = " . $user_data["address"] . " " .
                           "   or order_retail_operator = " . user_id() . " " .
                           "   or project_retail_coordinator = " . user_id()  . " " .
                           "   or project_design_contractor = " . user_id()  . " " .
                           "   or project_design_supervisor = " . user_id()  . " " .
                           "   or order_client_address = " . $user_data["address"] . ")";   
        }
        else if (has_access("can_view_order_before_budget_approval_in_projects") and !has_access("can_view_order_before_booklet_approval"))
        {   
            $list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') ".
                           "   and order_item_show_to_suppliers = 1 ".
                           "   and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
                           "   and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                           "   and (order_item_supplier_address = " . $user_data["address"] . " " .
                           "   or order_item_forwarder_address = " . $user_data["address"] . " " .
                           "   or order_retail_operator = " . user_id() . " " .
                           "   or project_retail_coordinator = " . user_id()  . " " .
                           "   or project_design_contractor = " . user_id()  . " " .
                           "   or project_design_supervisor = " . user_id()   . " " .
                           "   or order_client_address = " . $user_data["address"] . ")";   


        }
        else
        {
            $list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') ".
                           "   and order_item_show_to_suppliers = 1 ".
                           "   and order_show_in_delivery = 1 ".
                           "   and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
                           "   and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                           "   and (order_item_supplier_address = " . $user_data["address"] . " " .
                           "   or order_item_forwarder_address = " . $user_data["address"] . " " .
                           "   or order_retail_operator = " . user_id() . " " .
                           "   or project_retail_coordinator = " . user_id()  . " " .
                           "   or project_design_contractor = " . user_id()  . " " .
                           "   or project_design_supervisor = " . user_id()   . " " .
                           "   or order_client_address = " . $user_data["address"] . ")";  
        }
    }
    else
    {
        if (has_access("can_view_order_before_budget_approval_in_projects") and has_access("can_view_order_before_booklet_approval"))
         {   
            
            $list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') ".
                           "   and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
                           "   and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                           " and (" . $condition . ")";  

        }
        else if (has_access("can_view_order_before_budget_approval_in_projects") and !has_access("can_view_order_before_booklet_approval"))
        {   
            $list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') ".
                           "   and order_item_show_to_suppliers = 1 ".
                           "   and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
                           "   and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                           " and (" . $condition . ")"; 
        }
        else
        {
            $list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') ".
                           "   and order_item_show_to_suppliers = 1 ".
                           "   and order_show_in_delivery = 1 ".
                           "   and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
                           "   and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                           " and (" . $condition . ")"; 
        }    
    }
}

if($project_state_restrictions['from_state'])
{
	$list_filter .= " and order_actual_order_state_code >= '" . $project_state_restrictions['from_state'] . "' ";
}
if($project_state_restrictions['to_state'])
{
	$list_filter .= " and order_actual_order_state_code <= '" . $project_state_restrictions['to_state'] . "' ";
}



$sql_po_numbers = "select distinct order_item_po_number ".
                  "from projects ".
                  "left join orders on project_order = order_id ".
                  "left join order_items on order_item_order = order_id ".
                  "where " . $list_filter . " " .
                  "order by order_item_po_number";


$sql_cost_unit_numbers = "select distinct order_item_cost_unit_number ".
                         "from projects ".
                         "left join orders on project_order = order_id ".
                         "left join order_items on order_item_order = order_id ".
                         "where " . $list_filter . " " .
                         "order by order_item_cost_unit_number";




// find if user is a supplier or a forwarder role the user has
$link = "project_edit_material_list.php?pid={project_id}";


$sql_count = "select count(order_id) as number_of_records " .
             "from projects ".
             "left join orders on project_order = order_id ".
             "left join order_items on order_item_order = order_id ".
             "left join addresses as addresses_1 on order_item_supplier_address = addresses_1.address_id ".
             "left join addresses as addresses_2 on order_item_forwarder_address = addresses_2.address_id ".
             "where ".
             "   (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
             "   and order_show_in_delivery = 1 ".
             "   and (order_archive_date is null or order_archive_date = '0000-00-00') ".
             "   and order_type = 1 " .
             "   and order_item_type <= " . ITEM_TYPE_SPECIAL . 
             "   and order_item_supplier_address = " . $user_data["address"];


$res = mysql_query($sql_count) or dberror($sql_count);
if ($row = mysql_fetch_assoc($res))
{
    if($row["number_of_records"] > 0)
    {
        $link = "project_edit_supplier_data.php?pid={project_id}";
    }
}


$sql_count = "select count(order_id) as number_of_records " .
             "from projects ".
             "left join orders on project_order = order_id ".
             "left join order_items on order_item_order = order_id ".
             "left join addresses as addresses_1 on order_item_supplier_address = addresses_1.address_id ".
             "left join addresses as addresses_2 on order_item_forwarder_address = addresses_2.address_id ".
             "where ".
             "   (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
             "   and order_show_in_delivery = 1 ".
             "   and (order_archive_date is null or order_archive_date = '0000-00-00') ".
             "   and order_type = 1 " .
             "   and order_item_type <= " . ITEM_TYPE_SPECIAL . 
             "   and order_item_forwarder_address = " . $user_data["address"];


$res = mysql_query($sql_count) or dberror($sql_count);
if ($row = mysql_fetch_assoc($res))
{
    if($row["number_of_records"] > 0)
    {
        $link = "project_edit_traffic_data.php?pid={project_id}";
    }
}


/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);
$list->add_hidden("showall", param("showall"));


$list->set_entity("projects");
$list->set_group("group_head");
$list->set_order("order_item_po_number");
$list->set_filter($list_filter);    


if (has_access("can_edit_list_of_materials_in_projects") or has_access("can_edit_his_list_of_materials_in_projects"))
{
    $list->add_column("order_item_po_number", "P.O. Number", $link , LIST_FILTER_LIST, $sql_po_numbers, COLUMN_NO_WRAP);
}
else
{
    $list->add_column("order_item_po_number", "P.O. Number", "", LIST_FILTER_LIST, $sql_po_numbers, COLUMN_NO_WRAP);
}


$list->add_column("order_item_cost_unit_number", "Cost Unit", "", LIST_FILTER_LIST, $sql_cost_unit_numbers, COLUMN_NO_WRAP);
$list->add_column("item_shortcut", "Item Code", "", "", "", COLUMN_NO_WRAP);
$list->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_NO_WRAP | COLUMN_ALIGN_RIGHT);
$list->add_column("supplier", "Supplier", "", "", "", COLUMN_NO_WRAP);
$list->add_column("forwarder", "Forwarder", "", "", "", COLUMN_NO_WRAP);




if (has_access("has_access_to_all_projects") and has_access("can_view_all_entries_at_start_in_projects"))
{
}
else if (has_access("has_access_to_all_projects"))
{
    if (param("showall"))
    {
        $list->add_button("show_my_list", "Show My List");
    }
    else
    {
        $list->add_button("show_all", "Show Complete List");
    }
}


$list->add_button("shrink", "Shrink View");
$list->add_button(LIST_BUTTON_FILTER, "Filter");
$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove filter");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->process();


if ($list->button("new"))
{
    redirect("project_new_01.php");
}
if ($list->button("show_all"))
{
    $link = "projects_extended_view.php?showall=1";
    redirect($link);
}
else if ($list->button("show_my_list"))
{
    $link = "projects_extended_view.php";
    redirect($link);
}
else if ($list->button("shrink"))
{
    if (param("showall"))
    {
        $link = "projects.php?showall=1";
    }
    else
    {
        $link = "projects.php";
    }


    redirect($link);
}


$page = new Page("projects");


if (has_access("can_create_new_projects"))
{
    $page->register_action('new', 'New Project', "project_new_01.php");
}


$page->register_action('home', 'Home', "welcome.php");


$page->header();
$page->title("Projects");
$list->render();
$page->footer();




?>