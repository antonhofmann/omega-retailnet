<?php
/********************************************************************

    dashboard_projects_pdf.php

    Creates a PDF of the project's dashboard.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-12-02
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-12-02
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/


require "../include/frame.php";

check_access("can_view_projects");


require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');


$today = date("Y-m-d");

class MYPDF extends TCPDF
{
	//Page header
	function Header()
	{
		//Logo
		$this->Image('../pictures/omega_logo.jpg',10,8,33);
		//arialn bold 15
		$this->SetFont('arialn','B',12);
		//Move to the right
		$this->Cell(80);
		//Title
		$this->Cell(0,33,"Dashboard Projects " . date("d.m.Y h:i:m"),0,0,'R');
		//Line break
		$this->Ln(20);

	}

	//Page footer
	function Footer()
	{
		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		//arialn italic 8
		$this->SetFont('arialn','I',8);
		//Page number
		$this->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$this->PageNo(),0,0,'R');
	}

}




// Create and setup PDF document
$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 23, 10);

$pdf->Open();

$pdf->SetFillColor(220, 220, 220); 
$pdf->AddFont('arialn');
$pdf->AddFont('arialn', 'B');

$pdf->AddPage();


$pdf->SetY(25);
$pdf->SetX(10);

//ordered
$pdf->SetFont('arialn','B',10);
$pdf->Cell(190,5,"Ordered including today",1, 0, 'L', 1);
$pdf->Ln();
$pdf->SetFont('arialn','',10);


$sql = "select distinct order_number, order_id, project_id, " . 
       "concat(users.user_name, ' ', users.user_firstname) as rto, " .
	   "concat(users1.user_name, ' ', users1.user_firstname) as rtc " .
       "from order_items " . 
	   "left join orders on order_id = order_item_order " . 
	   "left join projects on project_order = order_id " . 
	   "left join users on user_id = order_retail_operator " . 
	   "left join users as users1 on users1.user_id = project_retail_coordinator ";

$list_filter = "order_type = 1 " .
                " and order_actual_order_state_code >= '700' " .
				" and (order_archive_date is NULL or order_archive_date = '0000-00-00') " .
				" and (project_actual_opening_date is NULL or project_actual_opening_date = '0000-00-00') " .
				' and (order_item_ready_for_pickup = "0000-00-00" or order_item_ready_for_pickup is NULL) ' .
				' and (order_item_arrival = "0000-00-00" or order_item_arrival is NULL) ' .
                " and order_item_ordered  <= " . dbquote($today) . 
				" and (order_item_type = " . ITEM_TYPE_STANDARD . 
                "    or order_item_type = " . ITEM_TYPE_SPECIAL .")";

$sql1 = $sql . " where " . $list_filter . " order by rto, order_number";

$ready_for_picked_up = array();
$not_ready_for_picked_up = array();
$infolinks = array();
$res = mysql_query($sql1) or dberror($sql1);
while ($row = mysql_fetch_assoc($res))
{
	$sql_count = 'select count(order_item_id) as num_recs from order_items ' . 
		         'where order_item_order = ' . $row["order_id"] . 
		         ' and order_item_ordered  <= ' . dbquote($today) .
		         ' and (order_item_ready_for_pickup = "0000-00-00" or order_item_ready_for_pickup is NULL) ' . 
				 ' and (order_item_type = ' . ITEM_TYPE_STANDARD . 
                 '    or order_item_type = ' . ITEM_TYPE_SPECIAL . ')';

	$res_c = mysql_query($sql_count) or dberror($sql_count);
	if ($row_c = mysql_fetch_assoc($res_c))
	{
		if($row_c['num_recs'] > 0) {
			$not_ready_for_picked_up[$row["order_number"]] = 'no yet ready for pick up: ' . $row_c['num_recs'] ;
		}
		else
		{
			$not_ready_for_picked_up[$row["order_number"]] = '';
		}
	}

	$sql_count = 'select count(order_item_id) as num_recs from order_items ' . 
		         'where order_item_order = ' . $row["order_id"] . 
		         ' and order_item_ordered  <= ' . dbquote($today) .
		         ' and (order_item_ready_for_pickup <> "0000-00-00" and order_item_ready_for_pickup is NOT NULL) ' . 
				 ' and (order_item_type = ' . ITEM_TYPE_STANDARD . 
                 '    or order_item_type = ' . ITEM_TYPE_SPECIAL . ')';

	$res_c = mysql_query($sql_count) or dberror($sql_count);
	if ($row_c = mysql_fetch_assoc($res_c))
	{
		
		if($row_c['num_recs'] > 0) {
			$ready_for_picked_up[$row["order_number"]] = 'ready for pick up: ' . $row_c['num_recs'];
		}
		else
		{
			$ready_for_picked_up[$row["order_number"]] = '';
		}
		
	}

	
	
	$pdf->Cell(20,5,$row["order_number"],1, 0, 'L', 0);
	$pdf->Cell(40,5,$ready_for_picked_up[$row["order_number"]],1, 0, 'L', 0);
	$pdf->Cell(40,5,$not_ready_for_picked_up[$row["order_number"]],1, 0, 'L', 0);
	$pdf->Cell(40,5,$row["rtc"],1, 0, 'L', 0);
	$pdf->Cell(40,5,$row["rto"],1, 0, 'L', 0);
	$pdf->Ln();
	
}



//ready for pickup
$pdf->Ln();
$pdf->SetFont('arialn','B',10);
$pdf->Cell(190,5,"Ready for pick up including today",1, 0, 'L', 1);
$pdf->Ln();
$pdf->SetFont('arialn','',10);



$list_filter = "order_type = 1 " .
                "and order_item_ready_for_pickup  <= " . dbquote($today) . 
				" and (order_archive_date is NULL or order_archive_date = '0000-00-00') " .
				" and (project_actual_opening_date is NULL or project_actual_opening_date = '0000-00-00') " .
				 ' and (order_item_pickup = "0000-00-00" or order_item_pickup is NULL) ' .
				 ' and (order_item_arrival = "0000-00-00" or order_item_arrival is NULL) ' .
				" and (order_item_type = " . ITEM_TYPE_STANDARD . 
                "    or order_item_type = " . ITEM_TYPE_SPECIAL .")";


$sql1 = $sql . " where " . $list_filter . " order by rto, order_number";


$picked_up = array();
$not_picked_up = array();
$infolinks = array();
$delays = array();
$res = mysql_query($sql1) or dberror($sql1);
while ($row = mysql_fetch_assoc($res))
{
	$sql_count = 'select count(order_item_id) as num_recs from order_items ' . 
		         'where order_item_order = ' . $row["order_id"] . 
		         ' and order_item_ready_for_pickup  <= ' . dbquote($today) .
		         ' and (order_item_pickup = "0000-00-00" or order_item_pickup is NULL) ' . 
				 ' and (order_item_type = ' . ITEM_TYPE_STANDARD . 
                 '    or order_item_type = ' . ITEM_TYPE_SPECIAL . ')';

	$res_c = mysql_query($sql_count) or dberror($sql_count);
	if ($row_c = mysql_fetch_assoc($res_c))
	{
		if($row_c['num_recs'] > 0) {
			$not_picked_up[$row["order_number"]] = 'not yet picked up: ' . $row_c['num_recs'];
		}
		else
		{
			$not_picked_up[$row["order_number"]] = '';
		}
	}

	$sql_count = 'select count(order_item_id) as num_recs from order_items ' . 
		         'where order_item_order = ' . $row["order_id"] . 
		         ' and order_item_pickup  <= ' . dbquote($today) .
		         ' and (order_item_pickup <> "0000-00-00" and order_item_pickup is NOT NULL) ' . 
				 ' and (order_item_type = ' . ITEM_TYPE_STANDARD . 
                 '    or order_item_type = ' . ITEM_TYPE_SPECIAL . ')';

	$res_c = mysql_query($sql_count) or dberror($sql_count);
	if ($row_c = mysql_fetch_assoc($res_c))
	{
		
		if($row_c['num_recs'] > 0) {
			$picked_up[$row["order_number"]] = 'picked up: ' . $row_c['num_recs'];
		}
		else
		{
			$picked_up[$row["order_number"]] = '';
		}
		
	}

	//calculate delays
	$delay = '';
	$sql_count = 'select count(order_item_id) as num_recs, ' .
		         ' min(order_item_ready_for_pickup) as first_date ' . 
	             'from order_items ' . 
		         'where order_item_order = ' . $row["order_id"] . 
				 ' and (order_item_type = ' . ITEM_TYPE_STANDARD . 
                 '    or order_item_type = ' . ITEM_TYPE_SPECIAL . ')';

	$res_c = mysql_query($sql_count) or dberror($sql_count);
	if ($row_c = mysql_fetch_assoc($res_c))
	{
		if($row_c['num_recs'] > 0) {
			$delay = floor((strtotime(date("Y-m-d")) - strtotime($row_c["first_date"])) / 86400);
		}
	}
	$delays[$row["order_number"]] = $delay;

	
	$pdf->Cell(20,5,$row["order_number"],1, 0, 'L', 0);
	$pdf->Cell(10,5,$delays[$row["order_number"]],1, 0, 'R', 0);
	$pdf->Cell(40,5,$picked_up[$row["order_number"]],1, 0, 'L', 0);
	$pdf->Cell(40,5,$not_picked_up[$row["order_number"]],1, 0, 'L', 0);
	$pdf->Cell(40,5,$row["rtc"],1, 0, 'L', 0);
	$pdf->Cell(40,5,$row["rto"],1, 0, 'L', 0);
	$pdf->Ln();
}

//expected arrival
$pdf->Ln();
$pdf->SetFont('arialn','B',10);
$pdf->Cell(190,5,"Expected to arrive including today",1, 0, 'L', 1);
$pdf->Ln();
$pdf->SetFont('arialn','',10);


$list_filter = "order_type = 1 " .
                "and order_item_expected_arrival  <= " . dbquote($today). 
				" and (order_archive_date is NULL or order_archive_date = '0000-00-00') " .
				" and (project_actual_opening_date is NULL or project_actual_opening_date = '0000-00-00') " .
				' and (order_item_arrival = "0000-00-00" or order_item_arrival is NULL) ' . 
				" and (order_item_type = " . ITEM_TYPE_STANDARD . 
                "    or order_item_type = " . ITEM_TYPE_SPECIAL .")";


$sql1 = $sql . " where " . $list_filter . " order by rto, order_number";

$arrived = array();
$not_arrived = array();
$infolinks = array();
$res = mysql_query($sql1) or dberror($sql1);
while ($row = mysql_fetch_assoc($res))
{
	$sql_count = 'select count(order_item_id) as num_recs from order_items ' . 
		         'where order_item_order = ' . $row["order_id"] . 
		         ' and order_item_expected_arrival  <= ' . dbquote($today) .
		         ' and (order_item_arrival = "0000-00-00" or order_item_arrival is NULL) ' . 
				 ' and (order_item_type = ' . ITEM_TYPE_STANDARD . 
                 '    or order_item_type = ' . ITEM_TYPE_SPECIAL . ')';

	$res_c = mysql_query($sql_count) or dberror($sql_count);
	if ($row_c = mysql_fetch_assoc($res_c))
	{
		if($row_c['num_recs'] > 0) {
			$not_arrived[$row["order_number"]] = 'not yet arrived: ' . $row_c['num_recs'];
		}
		else
		{
			$not_arrived[$row["order_number"]] = '';
		}
	}

	$sql_count = 'select count(order_item_id) as num_recs from order_items ' . 
		         'where order_item_order = ' . $row["order_id"] . 
		         ' and order_item_expected_arrival  <= ' . dbquote($today) .
		         ' and (order_item_arrival <> "0000-00-00" and order_item_arrival is NOT NULL) ' . 
				 ' and (order_item_type = ' . ITEM_TYPE_STANDARD . 
                 '    or order_item_type = ' . ITEM_TYPE_SPECIAL . ')';

	$res_c = mysql_query($sql_count) or dberror($sql_count);
	if ($row_c = mysql_fetch_assoc($res_c))
	{
		
		if($row_c['num_recs'] > 0) {
			$arrived[$row["order_number"]] = 'arrived: ' . $row_c['num_recs'];
		}
		else
		{
			$arrived[$row["order_number"]] = '';
		}
		
	}

	$pdf->Cell(20,5,$row["order_number"],1, 0, 'L', 0);
	$pdf->Cell(40,5,$arrived[$row["order_number"]],1, 0, 'L', 0);
	$pdf->Cell(40,5,$not_arrived[$row["order_number"]],1, 0, 'L', 0);
	$pdf->Cell(40,5,$row["rtc"],1, 0, 'L', 0);
	$pdf->Cell(40,5,$row["rto"],1, 0, 'L', 0);
	$pdf->Ln();
}


$pdf->Output();

?>