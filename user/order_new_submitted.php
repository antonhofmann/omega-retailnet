<?php
/********************************************************************

    order_new_submitted.php

    List of links grouped by categories.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-31
    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date modified:  2002-10-05
    Version:        1.1.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_create_new_orders");



$page = new Page("orders");

$page->register_action('home', 'Home', "welcome.php");

$page->header();
$page->title("New Catalog Order");

echo "<p>", "Your order has been submitted.", "</p>";
$page->footer();

?>