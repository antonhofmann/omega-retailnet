<?php
/********************************************************************

    project_new03.php

    Creation of a new project step 03.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-07-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-07-14
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_create_new_projects");

if(!isset($_SESSION["new_project_step_1"]))
{
	$link = "project_new_01.php";
	redirect($link);
}
if(!isset($_SESSION["new_project_step_3"]))
{
	$_SESSION["new_project_step_3"] = array();
}

set_referer("project_new_04.php");

if(count($_POST) == 0 and isset($_SESSION["new_project_step_3"]))
{
	$_SESSION["new_project_step_3"]["action"] = "";
	foreach($_SESSION["new_project_step_3"] as $key=>$value)
	{
		register_param($key);
		param($key, $value);
	}
}
/********************************************************************
    prepare all data needed
*********************************************************************/

// Vars
$design_objectives_listbox_names = array();
$design_objectives_checklist_names = array();

$roles = get_user_roles(user_id());

// get the region of users' address
$user_region = get_user_region(user_id());
// get user data
$user = get_user(user_id());

// get users' company address
$address = get_address($user["address"]);

// get client's currency
$currency = get_address_currency($user["address"]);


// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ".
                 "order by country_name";


// create sql for the location type listbox
$sql_location_types = "select location_type_id, location_type_name ".
                      "from location_types ".
                      "order by location_type_name";

// create sql for the voltage listbox
$sql_voltages = "select voltage_id, voltage_name ".
                "from voltages";

// create sql design_objective_groups
$sql_design_objective_groups = "select design_objective_group_id, design_objective_group_name, " .
                               "    design_objective_group_multiple " .
                               "from design_objective_groups " .
                               "where design_objective_group_active = 1 and " . 
							   "design_objective_group_postype = " . $_SESSION["new_project_step_1"]["project_postype"] . " " .
                               "order by design_objective_group_priority";


/********************************************************************
    build form
*********************************************************************/
$form = new Form("", "project");

$form->add_section("POS Location");
$posaddress = $_SESSION["new_project_step_1"]["shop_address_company"] . ", " . $_SESSION["new_project_step_1"]["shop_address_zip"]. " " . $_SESSION["new_project_step_1"]["shop_address_place"];
$form->add_comment($posaddress);

$form->add_section("Location Info");
$form->add_list("voltage", "Voltage Choice*", $sql_voltages, NOTNULL);
$form->add_list("location_type", "Location Type*", $sql_location_types);
$form->add_edit("location_type_other", "Or Other Location Type*", 0, "", TYPE_CHAR, 20);

/*
if($_SESSION["new_project_step_1"]["project_postype"] == 2
   or $_SESSION["new_project_step_1"]["project_postype"] == 3) // SIS or Kiosk
{
*/
	// count design_objective_groups
	$res = mysql_query($sql_design_objective_groups) or dberror($sql_design_objective_groups);
	$number_of_design_objective_groups = mysql_num_rows($res);

	if ($number_of_design_objective_groups > 0)
	{
		$form->add_section("Design Objectives");
		$form->add_comment("Please indicate your basic needs and requirements to assist out design process. ".
						   "A Project Manager will ensure that the design is consistent with the ".
						   BRAND . " Retail objectives."); 

		$i = 1;
		
		while ($row = mysql_fetch_assoc($res))
		{
			$sql_design_objective_item = "select design_objective_item_id, design_objective_item_name ".
										 "from design_objective_items ".
										 "where design_objective_item_active = 1 " . 
										 "   and design_objective_item_group=" . $row["design_objective_group_id"] . " ".
										 "order by design_objective_item_priority";

			if ($row["design_objective_group_multiple"] == 0) 
			{
				$form->add_list("design_objective_items" . $i, $row["design_objective_group_name"] . "*", $sql_design_objective_item, NOTNULL);
				$design_objectives_listbox_names[] = "design_objective_items" . $i;
			}
			else 
			{
				$form->add_checklist("design_objective_items" . $i, $row["design_objective_group_name"], "project_items", $sql_design_objective_item, NOTNULL);
				$design_objectives_checklist_names[] = "design_objective_items" . $i;
			}
			
			$i++;   
		}
	}
/*
}
*/

$form->add_section("Capacity Request by Client");
$form->add_edit("watches_displayed", "Watches Displayed", 0, "", TYPE_INT, 20);
$form->add_edit("watches_stored", "Watches Stored", 0, "", TYPE_INT, 20);
$form->add_edit("bijoux_displayed", "Bijoux Displayed", 0, "", TYPE_INT, 20);
$form->add_edit("bijoux_stored", "Bijoux Stored", 0, "", TYPE_INT, 20);

$form->add_section("Other Information");
$form->add_comment("Please use only figures to indicate the budget ".
                   "and enter the date in the form of dd.mm.yy");
$form->add_edit("approximate_budget", "Approximate Budget in " . $currency["symbol"] . "*", NOTNULL, "", TYPE_DECIMAL, 20);
$form->add_edit("planned_opening_date", "Planned Opening Date*", NOTNULL, "", TYPE_DATE, 20);

$form->add_section(" ");
$form->add_button("step4", "Proceed to next step");
$form->add_button("back", "Back");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$form->populate();
$form->process();

//clean fields from apostrophes
foreach($form->items as $key=>$value)
{
	if(array_key_exists('value', $value)) {
		$form->value($key, str_replace('"', '', $form->value($key)));
	}
}

if ($form->button("back"))
{
	$_SESSION["new_project_step_3"] = $_POST;
	redirect("project_new_02.php");
}
elseif ($form->button("step4"))
{
	//$form->add_validation("from_system_date({planned_opening_date}) >  " . dbquote(date("Y-m-d")), "The planned opening date must be a future date!");
	
	if (!$form->value("location_type") and !$form->value("location_type_other"))
    {
        $form->add_validation("{location_type} or {location_type_other}", "The location type must not be empty.");
    }

	if($form->validate())
	{
		
		$_SESSION["new_project_step_3"] = $_POST;
		$_SESSION["new_project_step_3"]["design_objectives_listbox_names"] = $design_objectives_listbox_names;
		$_SESSION["new_project_step_3"]["design_objectives_checklist_names"] = $design_objectives_checklist_names;
		
		$link = "project_new_04.php";
		redirect($link);
	}
}
  
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

$page->register_action('home', 'Home', "welcome.php");

$page->header();
$page->title("New Project");

echo "<div>";
echo "<img class='stepcounter' src='/pictures/numbers/01_off.gif' alt=\"\" />";
echo "<span class='step_inactive'>Basic Project Data</span>";
echo "<img class='stepcounter' src='/pictures/numbers/02_off.gif' alt=\"\" />";
echo "<span class='step_inactive'>Addresses</span>";
echo "<img class='stepcounter' src='/pictures/numbers/03_on.gif' alt=\"\" />";
echo "<span class='step_active'>POS Information</span>";
echo "<img class='stepcounter' src='/pictures/numbers/04_off.gif' alt=\"\" />";
echo "<span class='step_inactive'>Environment and Neighbourhood</span>";
echo "<img class='stepcounter' src='/pictures/numbers/05_off.gif' alt=\"\" />";
echo "<span class='step_inactive'>Delivery</span>";
echo "<br /><br /></div>";

$form->render();
$page->footer();


?>