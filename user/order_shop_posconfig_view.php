<?php
/********************************************************************


    order_shop_posconfig_view.php


    Shows the selected Configuration

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-08-04
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-08-04
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
check_access("can_view_catalog_detail");


/********************************************************************
    prepare all data needed
*********************************************************************/
$item = $_GET["id"];
$picture_code = $_GET["pcc"];


// Read name & code of the sown item
$sql = "select concat_ws(' - ', item_code, item_name) as item_label " .
       "from items where item_id = " . id();

$res = mysql_query($sql) or dberror();
if ($row = mysql_fetch_assoc($res))
{
    $item_label = $row['item_label'];
}

$pix_name = "";
$pix_description = "";
$pix_path = "";

$sql = "select item_file_title, item_file_description, item_file_path " .
       "from item_files where item_file_item = " . $item .
       "   and item_file_purpose = 6 " .
       "   and item_file_picture_config_code = " . dbquote($picture_code);

$res = mysql_query($sql) or dberror();
if ($row = mysql_fetch_assoc($res))
{
    $pix_name = $row['item_file_title'];
    $pix_description = $row['item_file_description'];
    $pix_path = $row['item_file_path'];
}


if(!$picture_code)
{
    $sql = "select item_file_title, item_file_description, item_file_path " .
           "from item_files where item_file_item = " . $item .
           "   and item_file_purpose = 2 ";

    $res = mysql_query($sql) or dberror();
    if ($row = mysql_fetch_assoc($res))
    {
        $pix_name = $row['item_file_title'];
        $pix_description = $row['item_file_description'];
        $pix_path = $row['item_file_path'];
    }
}


/********************************************************************
    Create Form
*********************************************************************/ 
$form = new Form("items", "item");

$form->add_label("pix_name", "Title", HIDEEMPTY , $pix_name);
$form->add_label("pix_description", "Description", HIDEEMPTY , $pix_description);

$form->process();

/********************************************************************
    render page
*********************************************************************/
$page = new Page(PAGE_POPUP);
$page->header();
$page->title( $item_label);


$form->render();

if ($pix_path)
{
	$size = getimagesize(combine_paths($_SERVER["DOCUMENT_ROOT"], $pix_path));

	echo "<div style=\"text-align:center\"><img src=\"" . $pix_path . "\" width=\"$size[0]\" height=\"$size[1]\" border=\"0\" alt=\"\" /></div><br>";

    echo "<a class=\"value\" href=\"javascript:print();\">print this info</a>";

} else {
	echo "<p>No picture available</p>";
}



$page->footer();
?>