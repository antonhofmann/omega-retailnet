<?php
/********************************************************************

    projects_print.php

    Select object to be printed

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-10-04
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-10-04
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

if (has_access("can_view_client_data_in_projects")
   or has_access("has_access_to_all_projects")
   or has_access("can_view_budget_in_projects")
   or has_access("can_view_delivery_schedule_in_projects")
   or has_access("can_edit_project_sheet")
   or (has_access("can_edit_cost_monitoring") and $project["project_state"] != 2))
{
}
else
{
	redirect("noaccess.php");
}



// read project and order details
$project = get_project(param("pid"));
// get company's address
$client_address = get_address($project["order_client_address"]);

$order_currency = get_order_currency($project["project_order"]);

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new form("projects", "projects");
$form->add_hidden("pid", param("pid"));

require_once "include/project_head_small.php";

$form->add_section("Print");

if (has_access("can_view_client_data_in_projects"))
{   
		
	$url = '<a href="http://' . $_SERVER["HTTP_HOST"] . '/user/project_print_project_data.php?pid=' . param("pid") . '" target="_blank">Print</a>';
	$form->add_label("print_project_data", "Project Data", RENDER_HTML, $url);

}

if($project["project_projectkind"] != 4 and $project["project_projectkind"] != 5) // take over, lease renewal
{
	
	if (has_access("has_access_to_all_projects") or has_access("can_view_budget_in_projects"))
	{   
		$url = '<a href="http://' . $_SERVER["HTTP_HOST"] . '/user/project_costs_budget_pdf.php?pid=' . param("pid") . '&sc=0" target="_blank">Print</a>';
		$form->add_label("print_budget", "Budget in " . $order_currency["symbol"], RENDER_HTML, $url);

		if($project["order_client_currency"] != 1)
		{
			$url = '<a href="http://' . $_SERVER["HTTP_HOST"] . '/user/project_costs_budget_pdf.php?pid=' . param("pid") . '&sc=1" target="_blank">Print</a>';
			$form->add_label("print_budget_chf", "Budget in CHF", RENDER_HTML, $url);
		}
	}

	if (has_access("can_edit_local_constrction_work"))
	{   
		if($project["project_cost_type"] == 1) // Corporate
		{
			$url = '<a href="http://' . $_SERVER["HTTP_HOST"] . '/user/project_costs_bid_comparison_pdf.php?pid=' . param("pid") . '" target="_blank">Print</a>';
			$form->add_label("print_bid_comparison", "Bid Comparison", RENDER_HTML, $url);

			$url = '<a href="http://' . $_SERVER["HTTP_HOST"] . '/user/project_costs_bids_pdf.php?pid=' . param("pid") . '" target="_blank">Print</a>';
			$form->add_label("print_bids", "Bids", RENDER_HTML, $url);
		}
	}

	if (has_access("can_edit_cost_monitoring"))
	{
		$url = '<a href="http://' . $_SERVER["HTTP_HOST"] . '/user/project_costs_cms_pdf.php?pid=' . param("pid") . '&lwoid=0" target="_blank">Print</a>';
		$form->add_label("print_lcwoffers", "Cost Monitoring Sheet", RENDER_HTML, $url);
	}

	
}


if (has_access("can_edit_project_sheet") and $project["project_state"] != 2)
{
	$date_created = $project["date_created"];
	if($date_created < '2004-09-27' and $project["project_use_ps2004"] != 1)
	{
		$url = '<a href="http://' . $_SERVER["HTTP_HOST"] . '/user/project_sheet_pdf.php?id=' . $project["project_order"] . '&pid=' . $project["project_id"] . '" target="_blank">Print</a>';
	}
	else
	{
		$url = '<a href="http://' . $_SERVER["HTTP_HOST"] . '/user/project_sheet_pdf_2004.php?id=' . $project["project_order"] . '&pid=' . $project["project_id"] . '" target="_blank">Print</a>';
	}
	
	$form->add_label("print_project_sheet", "Project Sheet", RENDER_HTML, $url);

	
}

if (has_access("can_view_delivery_schedule_in_projects") and $project["project_state"] != 2)
{
	$url = '<a href="http://' . $_SERVER["HTTP_HOST"] . '/user/project_view_traffic_data_pdf.php?pid=' . param("pid") . '" target="_blank">Print</a>';
	$form->add_label("print_delivery", "Delivery Schedule", RENDER_HTML, $url);
}


if (has_access("can_view_history_in_projects"))
{   
	$url = '<a href="http://' . $_SERVER["HTTP_HOST"] . '/user/project_tracking_pdf.php?pid=' . param("pid") . '" target="_blank">Print</a>';
	$form->add_label("print_trakcing", "Project Tracking", RENDER_HTML, $url);
}





/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();




/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Project: Select Object to be Printed");


$form->render();


$page->footer();
?>