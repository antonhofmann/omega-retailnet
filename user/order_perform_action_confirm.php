<?php
/********************************************************************

    order_perform_action_confirmation.php

    confirm upon cancellation

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-06-18
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-06-18
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/order_state_constants.php";
require_once "include/save_functions.php";

check_access("can_use_taskcentre_in_orders");


/********************************************************************
    prepare data needed
*********************************************************************/
// read order details
$order = get_order(param("oid"));

// get company's address
$client_address = get_address($order["order_client_address"]);


// append record to table actual_order_states
append_order_state($order["order_id"], param("action"), 2, 1);
set_archive_date($order["order_id"]);


if (param("action") == ORDER_CANCELLED)
{
    set_order_to_cancelled($order["order_id"]);
}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("orders", "order");
$form->add_hidden("oid", param("oid"));
$form->add_hidden("order_id", $order["order_id"]);

$form->add_label("order_number", "Order Number", 0, $order["order_number"]);
$line = "concat(user_name, ' ', user_firstname)";

if ($order["order_retail_operator"])
{
    $form->add_lookup("retail_operator_name", "Retail Operator", "users", $line, 0, $order["order_retail_operator"]);
}
else
{
    $form->add_label("retail_operator_name", "Retail Operator");
}

$client = $client_address["company"] . ", " .
$client_address["zip"] . " " . $client_address["place"] . ", " .
$client_address["country"];

$form->add_label("client_address", "Client", 0, $client);


/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

$page->header();

if (param("action") == ORDER_CANCELLED)
{
    $page->title("The following order was cancelled:");
}
else if (param("action") == MOVED_TO_THE_RECORDS)
{
    $page->title("The following order was moved to the records:");
}

$form->render();

$page->footer();

?>