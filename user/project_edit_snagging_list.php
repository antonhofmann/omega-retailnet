<?php
/********************************************************************

    snagging_template.php

    Creation and mutation of snagging_list templates.

    Created by:     Oliver Hofer (oliver.hofer@pageagent.com)
    Date created:   2003-05-22
    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date modified:  2003-05-29
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

//check_access("can_edit_catalog");

register_param("pid");

$choices = array();

$sql= "select snagging_list_id, snagging_list_name ".
      "from snagging_lists ";

$form = new Form("snagging_lists", "snagging");
$form->add_hidden("pid", param("pid"));
$form->add_hidden("snagging_list_project", param("pid"));
$form->add_edit("snagging_list_name", "Name", 0);

// get project product-line

$sql = "select project_product_line " .
       "from projects " .
       "where project_id = " . param("pid");

$res = mysql_query($sql) or dberror($sql);
if ($res && mysql_num_rows($res) > 0)
{
    $row = mysql_fetch_assoc($res);
    $sql_templates = "select snagging_template_id, snagging_template_name ".
                     "from snagging_templates " .
                     "where snagging_template_product_line = " . 
                        $row['project_product_line'];

    $res = mysql_query($sql_templates);
    while ($row = mysql_fetch_assoc($res))
    {
        $choices[$row['snagging_template_id']] = $row['snagging_template_name'];
    }
}
else
{
    error("Invalid project-id");
}

if ($choices)
{
    $form->add_list("snagging_list_template", "Template", $choices);
    $form->add_button(FORM_BUTTON_SAVE, "Save");
}
else
{
    $form->add_comment("No Snagging-List Templates found");
}

$form->add_button(FORM_BUTTON_BACK, "Back", 
                  "project_edit_snagging_lists.php?pid=" . param("pid"));
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("projects");
require "include/project_page_actions.php";
$page->header();

$page->title(id() ? "Edit" : " Add" ." Snagging List");
$form->render();
$page->footer();

?>