<?php
/********************************************************************

    project_costs_edit_position.php

    Edit a cost position

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-01-09
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2012, OMEGA SA, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../include/page_modal.php";
require_once "include/get_functions.php";

check_access("can_edit_list_of_materials_in_projects");

if(!param("pid") and !param("cid"))
{
	$link = "welcome.php";
	redirect($link);
}


/********************************************************************
    prepare data
*********************************************************************/

$project = get_project(param("pid"));
$client_address = get_address($project["order_client_address"]);




//get costposition
$sql = "select * from costsheets " . 
       "where costsheet_id = " . param("cid");

$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	$cost_group = $row["costsheet_pcost_group_id"];
	$cost_subgroup = $row["costsheet_pcost_subgroup_id"];
	if($cost_subgroup > 0)
	{
		$cost_group = $cost_group . '_' . $cost_subgroup;
	}
}

$costgroups = array();


$sql = "select pcost_group_id, " . 
		  "concat(pcost_group_code, ' ', pcost_group_name) as costgroup " . 
		  "from pcost_groups " .
		  " order by pcost_group_code";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$id = $row["pcost_group_id"];
	$costgroups[$id] =  $row["costgroup"];


	$sql = "select pcost_subgroup_id, pcost_subgroup_pcostgroup_id, " . 
	          "concat(pcost_subgroup_code, ' ', pcost_subgroup_name) as costgroup " . 
			  "from pcost_subgroups  " . 
		      " where pcost_subgroup_pcostgroup_id = " . $row["pcost_group_id"] . 
			  " order by pcost_subgroup_code ";

	$res_c = mysql_query($sql) or dberror($sql);
	while($row_c = mysql_fetch_assoc($res_c))
	{
		$id = $row["pcost_group_id"] . '_' . $row_c["pcost_subgroup_id"];
		$costgroups[$id] =  $row["costgroup"] . ' ' . $row_c["costgroup"];
	}
}

/********************************************************************
    save data
*********************************************************************/

if(param("save_form") and param('cid') > 0)
{
	if(strpos($_POST["group_id"], '_') > 0)
	{
		$tmp = explode('_', $_POST["group_id"]);
		$group_id = $tmp[0];
		$subgroup_id = $tmp[1];
	}
	else
	{
		$group_id = $_POST["group_id"];
		$subgroup_id = 0;
	}
	
	$fields = array();

	$fields[] = "costsheet_pcost_group_id = " . dbquote($group_id);
	$fields[] = "costsheet_pcost_subgroup_id = " . dbquote($subgroup_id);
	$fields[] = "date_modified = " . dbquote(date("Y-m-d H:i:s"));
	$fields[] = "user_modified = " . dbquote(user_login());

	$sql = "update costsheets set " . join(", ", $fields) . " where costsheet_id = " . $_POST['cid'];

	mysql_query($sql) or dberror($sql); 
	
	$fields = array();

	$fields[] = "costsheet_bid_position_pcost_group_id = " . dbquote($group_id);
	$fields[] = "costsheet_bid_position_pcost_subgroup_id = " . dbquote($subgroup_id);
	$fields[] = "date_modified = " . dbquote(date("Y-m-d H:i:s"));
	$fields[] = "user_modified = " . dbquote(user_login());

	$sql = "update costsheet_bid_positions set " . join(", ", $fields) . 
		   " where costsheet_bid_position_costsheet_id = " . $_POST['cid'] . 
		   " and costsheet_bid_position_project_id = " . $_POST['pid'];
	
	mysql_query($sql) or dberror($sql); 
}

/********************************************************************
    render form
*********************************************************************/
$form = new Form("costsheets", "costsheets");

require_once "include/project_head_small.php";


$form->add_section(" ");
$form->add_hidden("save_form", "1");
$form->add_hidden("costsheet_project_id", param("pid"));
$form->add_hidden("pid", param("pid"));
$form->add_hidden("cid", param("cid"));

$form->add_list("group_id", "Cost Group", $costgroups, NOTNULL, $cost_group);


$form->add_input_submit("submit", "Save", 0);

$form->populate();
$form->process();


$page = new Page_Modal("projects");
$page->header();

$page->title("Move Budget Position");
$form->render();

if(param("save_form"))
{
?>
	<script languege="javascript">
		var back_link = "project_costs_budget.php?pid=<?php echo param("pid")?>";
		$.nyroModalRemove();
	</script>
<?php
}

$page->footer();

?>