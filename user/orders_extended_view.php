<?php
/********************************************************************
    orders_extended_view.php
    
    Extended order overview containing item positions.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-10-23
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-06-18
    Version:        1.0.4

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/order_functions.php";
require_once "include/save_functions.php";


check_access("can_view_orders");


register_param("showall");
set_referer("order_new.php");
set_referer("order_items.php");
set_referer("basket.php");
set_referer("order_view_client_data.php");




/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());


// create sql for the list of orders
$sql = "select order_id, " .
       "   concat(country_name, ', ', order_shop_address_place, ', ', order_shop_address_company, ', ', order_number) as group_head,  ".
       "   if(item_code > '', item_code, 'Special Item') as item_shortcut, " .
       "   order_item_po_number, order_item_cost_unit_number, order_item_quantity, ".
       "   addresses_1.address_company as supplier, addresses_2.address_company as forwarder ".
       "from orders " .
       "left join order_items on order_item_order = order_id ".
       "left join items on order_item_item = item_id ".
       "left join addresses as addresses_1 on order_item_supplier_address = addresses_1.address_id ".
       "left join addresses as addresses_2 on order_item_forwarder_address = addresses_2.address_id ".       "left join countries on order_shop_address_country = countries.country_id ".
       "left join users on order_retail_operator = users.user_id ";

// create list filter
$condition = get_user_specific_order_list(user_id(), 2);




if (has_access("has_access_to_all_orders") and has_access("can_view_all_entries_at_start_in_orders"))
{
    $list_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
                   "   and order_archive_date is null ".
                   "   and order_type = 2 ".
                   "   and order_item_type <= " . ITEM_TYPE_SPECIAL;
}
else if (has_access("has_access_to_all_orders") and param("showall"))
{
    $list_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
                   "   and order_archive_date is null ".
                   "   and order_type = 2 ".
                   "   and order_item_type <= " . ITEM_TYPE_SPECIAL;
}
else if (has_access("has_access_to_all_orders") and !param("showall"))
{
    $list_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
                   "   and order_archive_date is null ".
                   "   and order_type = 2 ".
                   "   and order_item_type <= " . ITEM_TYPE_SPECIAL .  
                   "   and (order_item_supplier_address = " . $user_data["address"] . 
                   "   or order_item_forwarder_address = " . $user_data["address"] . 
                   "   or order_retail_operator = " . user_id() . 
                   "   or order_client_address = " . $user_data["address"] . ")";
}
else
{
    
    if ($condition == "")
    {
        if (has_access("can_view_order_before_budget_approval_in_orders"))
        {   
            $list_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
                           "   and order_archive_date is null ".
                           "   and order_type = 2 ".
                           "   and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                           "   and (order_item_supplier_address = " . $user_data["address"] . 
                           "   or order_item_forwarder_address = " . $user_data["address"] . 
                           "   or order_retail_operator = " . user_id() . 
                           "   or order_client_address = " . $user_data["address"] . ")";
        }
        else
        {   
            $list_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
                           "   and order_show_in_delivery = 1 ".
                           "   and order_archive_date is null ".
                           "   and order_type = 2 " .
                           "   and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                           "   and (order_item_supplier_address = " . $user_data["address"] . 
                           "   or order_item_forwarder_address = " . $user_data["address"] . 
                           "   or order_retail_operator = " . user_id() . 
                           "   or order_client_address = " . $user_data["address"] . ")";
        }
    }
    else
    {
        if (has_access("can_view_order_before_budget_approval_in_orders"))
        {   
            $list_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
                           "   and order_archive_date is null ".
                           "   and order_type = 2 ".
                           "   and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                           "   and (" . $condition . ")";
        }
        else
        {   
            $list_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
                           "   and order_show_in_delivery = 1 ".
                           "   and order_archive_date is null ".
                           "   and order_type = 2 " .
                           "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
                           "   and (" . $condition . ")";
        }    
    
    }
}


$sql_po_numbers = "select distinct order_item_po_number ".
                  "from orders ".
                  "left join order_addresses on order_address_order = order_id ".
                  "left join order_items on order_item_order = order_id ".
                  "where " . $list_filter . " " .
                  "order by order_item_po_number";


$sql_cost_unit_numbers = "select distinct order_item_cost_unit_number ".
                         "from orders ".
                         "left join order_addresses on order_address_order = order_id ".
                         "left join order_items on order_item_order = order_id ".
                         "where " . $list_filter . " " .
                         "order by order_item_cost_unit_number";


// find if user is a supplier or a forwarder role the user has
$link = "order_edit_material_list.php?oid={order_id}";


$sql_count = "select count(order_id) as number_of_records " .
             "from orders " .
             "left join order_items on order_item_order = order_id ".
             "left join addresses as addresses_1 on order_item_supplier_address = addresses_1.address_id ".
             "left join addresses as addresses_2 on order_item_forwarder_address = addresses_2.address_id ".
             "where ".
             "   (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
             "   and order_show_in_delivery = 1 ".
             "   and order_archive_date is null ".
             "   and order_type = 2 " .
             "   and order_item_type <= " . ITEM_TYPE_SPECIAL . 
             "   and order_item_supplier_address = " . $user_data["address"];


$res = mysql_query($sql_count) or dberror($sql_count);
if ($row = mysql_fetch_assoc($res))
{
    if($row["number_of_records"] > 0)
    {
        $link = "order_edit_supplier_data.php?oid={order_id}";
    }
}


$sql_count = "select count(order_id) as number_of_records " .
             "from orders " .
             "left join order_items on order_item_order = order_id ".
             "left join addresses as addresses_1 on order_item_supplier_address = addresses_1.address_id ".
             "left join addresses as addresses_2 on order_item_forwarder_address = addresses_2.address_id ".
             "where ".
             "   (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
             "   and order_show_in_delivery = 1 ".
             "   and order_archive_date is null ".
             "   and order_type = 2 " .
             "   and order_item_type <= " . ITEM_TYPE_SPECIAL . 
             "   and order_item_forwarder_address = " . $user_data["address"];


$res = mysql_query($sql_count) or dberror($sql_count);
if ($row = mysql_fetch_assoc($res))
{
    if($row["number_of_records"] > 0)
    {
        $link = "order_edit_traffic_data.php?oid={order_id}";
    }
}




/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);


$list->add_hidden("showall", param("showall"));
$list->set_entity("catalog orders");
$list->set_group("group_head");
$list->set_order("order_item_po_number");
$list->set_filter($list_filter);    


if (has_access("can_edit_list_of_materials_in_orders") or has_access("can_edit_his_list_of_materials_in_orders"))
{
    $list->add_column("order_item_po_number", "P.O. Number", $link, LIST_FILTER_LIST, $sql_po_numbers , COLUMN_NO_WRAP);
}
else
{
    $list->add_column("order_item_po_number", "P.O. Number", "", LIST_FILTER_LIST, $sql_po_numbers , COLUMN_NO_WRAP);
}
$list->add_column("order_item_cost_unit_number", "Cost Unit", "", LIST_FILTER_LIST,  $sql_cost_unit_numbers
, COLUMN_NO_WRAP);
$list->add_column("item_shortcut", "Item Code", "", "", "", COLUMN_NO_WRAP);
$list->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_NO_WRAP | COLUMN_ALIGN_RIGHT);
$list->add_column("supplier", "Supplier", "", "", "", COLUMN_NO_WRAP);
$list->add_column("forwarder", "Forwarder", "", "", "", COLUMN_NO_WRAP);


if (has_access("has_access_to_all_orders") and has_access("can_view_all_entries_at_start_in_orders"))
{
}
else if (has_access("has_access_to_all_orders"))
{
    if (param("showall"))
    {
        $list->add_button("show_my_list", "Show My List");
    }
    else
    {
        $list->add_button("show_all", "Show Complete List");
    }
}


$list->add_button("shrink", "Shrink View");
$list->add_button(LIST_BUTTON_FILTER, "Filter");
$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove Filter");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->process();




if ($list->button("new"))
{
   redirect("order_new.php");
}
else if ($list->button("basket"))
{
    redirect("basket.php");
}


if ($list->button("show_all"))
{
    $link = "orders_extended_view.php?showall=1";
    redirect($link);
}
else if ($list->button("show_my_list"))
{
    $link = "orders_extended_view.php";
    redirect($link);
}
else if ($list->button("shrink"))
{
    if (param("showall"))
    {
        $link = "orders.php?showall=1";
    }
    else
    {
        $link = "orders.php";
    }    
    redirect($link);
}


$page = new Page("orders");


if (has_access("can_create_new_orders"))
{
    $page->register_action('new', 'New Order');
    if (!basket_is_empty())
    {
        $page->register_action('basket', 'Shopping List');
    }
}


$page->register_action('home', 'Home', "welcome.php");


$page->header();
$page->title('Catalog Orders');
$list->render();
$page->footer();


?>