<?php
/********************************************************************

    projects.php

    Entry page for the projects section.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-06-20
    Version:        1.1.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/order_state_constants.php";

check_access("can_view_projects");

register_param("showall");

if(param('search_term')) {
	param("showall", 1);
}
set_referer("projects_archive.php");


/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());
$user_roles = get_user_roles(user_id());
$project_state_restrictions = get_project_state_restrictions(user_id());

$show_cer_state = false;

// create sql
$sql = "select distinct project_id, project_number, project_projectkind, " .
       "project_actual_opening_date, product_line_name, postype_name, " . 
       "concat(order_shop_address_place,', ', order_shop_address_company), country_name, ".
       "    rtcs.user_name as project_retail_coordinator, rtos.user_name as order_retail_operator,".
       "    order_id, order_actual_order_state_code, project_cost_type, project_costtype_text,  project_postype, ".
	   " project_cost_cms_completed, project_cost_cms_approved, project_state_text, projectkind_code, " .
	   "if(project_state IN (2,4), " . 
	   "if(project_state = 2, concat('<span class=\"error\">', project_state_text, '</span>'), concat('<strong>', project_state_text, '</strong>')), project_real_opening_date) as real_opening_date ".
       "from projects ".
       "left join orders on project_order = order_id ".
	   "inner join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
       "left join product_lines on project_product_line = product_line_id ".
       "left join postypes on postype_id = project_postype ".
	   "left join project_states on project_state_id = project_state ".
       "left join countries on order_shop_address_country = countries.country_id ".
       "left join users as rtcs on project_retail_coordinator = rtcs.user_id " .
	   "left join projectkinds on projectkind_id = project_projectkind ".
	   "left join users as rtos on order_retail_operator = rtos.user_id ";

$sql_count = "select count(project_id) as num_recs " . 
       "from projects ".
       "left join orders on project_order = order_id ".
	   "inner join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join project_states on project_state_id = project_state ".
       "left join product_lines on project_product_line = product_line_id ".
       "left join postypes on postype_id = project_postype ".
       "left join countries on order_shop_address_country = countries.country_id ".
       "left join users as rtcs on project_retail_coordinator = rtcs.user_id " . 
	   "left join users as rtos on order_retail_operator = rtos.user_id ";

// create list filter
$condition = "";
if(in_array(1, $user_roles) 
	or in_array(2, $user_roles) 
	or in_array(3, $user_roles) 
	or in_array(8, $user_roles))
{
	$show_cer_state = true;
	if (has_access("has_access_to_all_projects") and has_access("can_view_all_entries_at_start_in_projects"))
    {
       
		$list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820' ";
    }
    else if (has_access("has_access_to_all_projects") and param("showall"))
    {
        $list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820' ";
    }
    else if (has_access("has_access_to_all_projects") and !param("showall"))
    {
		$list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820'  " .
                       " and (project_retail_coordinator = " . user_id() .
                       " or project_design_supervisor = " . user_id() . 
                       " or project_design_contractor = " . user_id() . 
                       " or order_retail_operator = " . user_id() . 
                       " or order_retail_operator is null " . 
			           " or order_actual_order_state_code = '100')";
    }
    else
    {
		
		if (has_access("can_view_order_before_budget_approval_in_projects"))
		{
            $list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820'  " .
               " and (project_retail_coordinator = " . user_id() .
               " or project_design_supervisor = " . user_id() . 
               " or project_design_contractor = " . user_id() . 
               " or order_retail_operator = " . user_id() . 
               " or order_retail_operator is null)";
        }
        else
        {
            $list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820'  " .
               "   and order_show_in_delivery = 1 ".
               " and (project_retail_coordinator = " . user_id() .
               " or project_design_supervisor = " . user_id() . 
               " or project_design_contractor = " . user_id() . 
               " or order_retail_operator = " . user_id() . 
               " or order_retail_operator is null)";
        }
    }

    if(in_array(7, $user_roles)) // design contractor
    {
        $list_filter.= " and project_design_contractor = "  . user_id();
    }

}
else
{

	$condition = get_user_specific_order_list(user_id(), 1, $user_roles, $user_data["has_access_to_travelling_projects"]);

    if (has_access("has_access_to_all_projects") and has_access("can_view_all_entries_at_start_in_projects"))
    {
        $list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820' ";

		$show_cer_state = true;
		
		
    }
    else if (has_access("has_access_to_all_projects") and param("showall"))
    {
        $list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820' ";
		$show_cer_state = true;

    }
    else if (has_access("has_access_to_all_projects") and !param("showall"))
    {

        if ($condition == "")
        {
            $list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820'  " .
                           "   and order_retail_operator is null";
        }
        else
        {
            $list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820'  " .
                           "   and (" . $condition . " or order_retail_operator is null)";
        }
		$show_cer_state = true;
    }
    else
    {
		if ($condition == "")
        {
            // order_type = 0 is a work around to sho no orders
            $list_filter = "order_type = 0 ";
        }
        else
        {
           
			if (has_access("can_view_order_before_budget_approval_in_projects"))
            {   
				$list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820'  ".
                               "   and (" . $condition . ")";
				
            }
        else
            {
               
				$list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820'  ".
                               "   and order_show_in_delivery = 1 ".
                               "   and (" . $condition . ")";

            }
        }
    }
}


if($project_state_restrictions['from_state'])
{
	$list_filter .= " and order_actual_order_state_code >= '" . $project_state_restrictions['from_state'] . "' ";
}
if($project_state_restrictions['to_state'])
{
	$list_filter .= " and order_actual_order_state_code <= '" . $project_state_restrictions['to_state'] . "' ";
}

//check if there is only one project

if(param('search_term')) {

	$search_term = trim(param('search_term'));
	$list_filter .= ' and (project_number like "%' . $search_term . '%" 
	                  or postype_name like "%' . $search_term . '%" 
					  or project_costtype_text like "%' . $search_term . '%"
					  or project_state_text like "%' . $search_term . '%"
					  or product_line_name like "%' . $search_term . '%"
					  or postype_name like "%' . $search_term . '%"
					  or order_actual_order_state_code like "%' . $search_term . '%"
					  or order_shop_address_place like "%' . $search_term . '%"
					  or order_shop_address_company like "%' . $search_term . '%"
					  or country_name like "%' . $search_term . '%")';

	$sql_count = $sql_count . ' where ' . $list_filter;
	$res = mysql_query($sql_count) or dberror($sql_count);

	if ($row = mysql_fetch_assoc($res))
	{
		if ($row['num_recs'] == 1)
		{
			$sql = $sql . ' where ' . $list_filter;
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$link = "project_task_center.php?pid=" . $row['project_id'];
				redirect($link);
			}
		}
	}
}

if(has_access("has_access_to_retail_only")) {
	$list_filter .= " and project_cost_type in (1) ";
}

if(has_access("has_access_to_wholesale")) {
	$list_filter .= " and project_cost_type in (2, 6) ";
}

// create sql for product lines
if ((has_access("has_access_to_all_projects") and param("showall")) or has_access("can_view_all_entries_at_start_in_projects"))
{
    $sql_product_lines = "select distinct product_line_name ".
                         "from projects ".
                         "left join product_lines on  project_product_line = product_line_id ".
                         "left join orders on project_order = order_id ".
                         "where (order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820' and project_product_line > 0 ".
                         "order by product_line_name";
    
    
	
	$sql_pos_types = "select distinct postype_name ".
                         "from projects ".
                         "left join postypes on  postype_id = project_postype ".
                         "left join orders on project_order = order_id ".
                         "where (order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820'  ".
                         "order by postype_name";

	$sql_legal_types =   "select distinct project_costtype_text " . 
		                 "from projects " .
		                 "left join orders on project_order = order_id ".
		                 "left join project_costs on project_cost_order = project_order " .
		                 "left join project_costtypes on project_costtype_id = project_cost_type " . 
		                 "where (order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820'  ".
                         "order by project_costtype_text";


	$sql_rtcs = "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as user_name " . 
		        "from projects " .
		        "left join orders on project_order = order_id ".
		        "left join users on user_id = project_retail_coordinator " . 
		        "where user_name <> '' and (order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820' " . 
		        "order by user_name ";


	$sql_rtos = "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as user_name " . 
		        "from projects " .
		        "left join orders on project_order = order_id ".
		        "left join users on user_id = order_retail_operator " . 
		        "where user_name <> '' and (order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820' " . 
		        "order by user_name ";

	$sql_project_kinds = "select DISTINCT projectkind_code, projectkind_name " . 
		                 "from projects " . 
		                 "left join projectkinds on projectkind_id = project_projectkind ". 
		                 " order by projectkind_name";
}
else
{
    
	
	$sql_product_lines = "select distinct product_line_name ".
                         "from projects ".
                         "left join product_lines on  project_product_line = product_line_id ".
                         "left join orders on project_order = order_id ".
						 "left join order_items on order_item_order = order_id ".
                         "left join addresses on order_client_address = address_id ".
		                 "inner join project_costs on project_cost_order = order_id " .
                         "where (order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820'  and project_product_line > 0 ".
                         "    and (";
    
    
	

	$sql_pos_types = "select distinct postype_name ".
                         "from projects ".
                         "left join postypes on  postype_id = project_postype ".
                         "left join orders on project_order = order_id ".
		                 "left join order_items on order_item_order = order_id ".
                         "left join addresses on order_client_address = address_id ".
						 "inner join project_costs on project_cost_order = order_id " .
                         "where (order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820'  " .
                         "    and (";

    

	$sql_legal_types =   "select distinct project_costtype_text " . 
		                 "from projects " . 
		                 "left join project_costs on project_cost_order = project_order " .
		                 "left join project_costtypes on project_costtype_id = project_cost_type " .
		                 "left join orders on project_order = order_id ".
                         "left join order_items on order_item_order = order_id ".
                         "left join addresses on order_client_address = address_id ".
		                 "where (order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820'  ".
                         "    and (";


	

	$sql_rtcs = "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as user_name " . 
		        "from projects " .
		        "left join orders on project_order = order_id ".
		        "left join order_items on order_item_order = order_id ".
		        "left join users on user_id = project_retail_coordinator " . 
		        "inner join project_costs on project_cost_order = order_id " .
		        "where user_name <> '' and (order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820' " . 
		        "    and (";


	
	
	$sql_rtos = "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as user_name " . 
		        "from projects " .
		        "left join orders on project_order = order_id ".
		        "left join order_items on order_item_order = order_id ".
		        "left join users on user_id = order_retail_operator " . 
		        "inner join project_costs on project_cost_order = order_id " .
		        "where user_name <> '' and (order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820' " . 
		        "    and (";
	
	
	
	$filter_tmp = " (order_item_supplier_address = " . $user_data["address"] . " " .
                  "or order_item_forwarder_address = " . $user_data["address"] . " " .
                  "or order_retail_operator = " . user_id() . " " .
                  "or project_retail_coordinator = " . user_id()  . " " .
                  "or project_design_contractor = " . user_id()  . " " .
                  "or project_design_supervisor = " . user_id()   . " " .
                  "or order_client_address = " . $user_data["address"] . " ) ";

	


	if($condition)
	{
		$filter_tmp = $condition;
	}


	if(has_access("has_access_to_retail_only")) {
		$filter_tmp .= " and project_cost_type in (1) ";
	}

	if(has_access("has_access_to_wholesale")) {
		$filter_tmp .= " and project_cost_type in (2, 6) ";
	}

	$sql_product_lines = $sql_product_lines . $filter_tmp . ") order by product_line_name";

    $sql_pos_types = $sql_pos_types . $filter_tmp . ") order by postype_name";

	$sql_legal_types = $sql_legal_types . $filter_tmp  . ") order by project_costtype_text";

	$sql_rtcs = $sql_rtcs . $filter_tmp  . ") order by user_name";
	$sql_rtos = $sql_rtos . $filter_tmp  . ") order by user_name";


	$sql_project_kinds = "select DISTINCT projectkind_code, projectkind_name " . 
		                 "from projects " . 
		                 "left join projectkinds on projectkind_id = project_projectkind ". 
		                 " order by projectkind_name";
}

/*
// create sql for country list
if ((has_access("has_access_to_all_projects") and param("showall")) or has_access("can_view_all_entries_at_start_in_projects"))
{
    $sql_countries = "select distinct country_name ".
                     "from projects ".
                     "left join orders on project_order = order_id ".
                     "left join countries on order_shop_address_country = countries.country_id ".
                     "where (order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820'  ".
                     "order by country_name";
}
else
{
    $sql_countries = "select distinct country_name ".
                     "from projects ".
                     "left join orders on project_order = order_id ".
                     "left join countries on order_shop_address_country = countries.country_id ".
                     "left join order_items on order_item_order = order_id ".
                     "where (order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '820'  ".
                     "    and (";
    
    $filter_tmp = "order_item_supplier_address = " . $user_data["address"] . " " .
                  "or order_item_forwarder_address = " . $user_data["address"] . " " .
                  "or order_retail_operator = " . user_id() . " " .
                  "or project_retail_coordinator = " . user_id()  . " " .
                  "or project_design_contractor = " . user_id()  . " " .
                  "or project_design_supervisor = " . user_id()   . " " .
                  "or order_client_address = " . $user_data["address"];
   
    $sql_countries = $sql_countries . $filter_tmp . ") order by country_name";
}

*/



// create sql for country list
if ((has_access("has_access_to_all_projects") and param("showall")) or has_access("can_view_all_entries_at_start_in_projects"))
{
    $sql_countries = "select distinct country_name ".
                     "from projects ".
                     "left join orders on project_order = order_id ".
					   "inner join project_costs on project_cost_order = project_order " .
					   "left join project_costtypes on project_costtype_id = project_cost_type " .
					   "left join product_lines on project_product_line = product_line_id ".
					   "left join postypes on postype_id = project_postype ".
					   "left join project_states on project_state_id = project_state ".
					   "left join countries on order_shop_address_country = countries.country_id ".
					   "left join users as rtcs on project_retail_coordinator = rtcs.user_id " .
					   "left join projectkinds on projectkind_id = project_projectkind ".
					   "left join users as rtos on order_retail_operator = rtos.user_id " . 
                     "where " . $list_filter . 
                     " order by country_name";
}
else
{
    $sql_countries = "select distinct country_name ".
                     "from projects ".
                     "left join orders on project_order = order_id ".
					   "inner join project_costs on project_cost_order = project_order " .
					   "left join project_costtypes on project_costtype_id = project_cost_type " .
					   "left join product_lines on project_product_line = product_line_id ".
					   "left join postypes on postype_id = project_postype ".
					   "left join project_states on project_state_id = project_state ".
					   "left join countries on order_shop_address_country = countries.country_id ".
					   "left join users as rtcs on project_retail_coordinator = rtcs.user_id " .
					   "left join projectkinds on projectkind_id = project_projectkind ".
					   "left join users as rtos on order_retail_operator = rtos.user_id " . 
									 "left join order_items on order_item_order = order_id ".
                     "where " . $list_filter;

	$sql_countries = $sql_countries . " order by country_name";

}

// get 2do column (show if user has to do something)
//$sql_images = $sql . " where " . $list_filter;
//$images = set_to_do_pictures($sql_images, 1);


$images = array();
$cms_states = array();
$aods = array();

if(param('search_term')) {

	$sql_images = "select project_id from tasks " .
		   "left join orders on order_id = task_order " .
		   "left join projects on project_order = order_id " .
		   "left join project_costs on project_cost_order = order_id " .
		   "left join project_costtypes on project_costtype_id = project_cost_type " .
		   "left join project_states on project_state_id = project_state ".
		   "left join product_lines on project_product_line = product_line_id ".
		   "left join postypes on postype_id = project_postype ".
		   "left join countries on order_shop_address_country = countries.country_id ".
		   "where task_user = " . user_id() .
		   "   and task_done_date is null " .
		   "   and " . $list_filter;
}
else
{
	$sql_images = "select project_id from tasks " .
		   "left join orders on order_id = task_order " .
		   "left join projects on project_order = order_id " .
		   "left join project_costs on project_cost_order = order_id " .
		   "where task_user = " . user_id() .
		   "   and task_done_date is null " .
		   "   and " . $list_filter;
}

$res = mysql_query($sql_images) or dberror($sql_images);

while ($row = mysql_fetch_assoc($res))
{
    $images[$row["project_id"]] = "/pictures/todo.gif";
}

if (has_access("can_edit_retail_data"))
{
    
	if(param('search_term')) {
		
		$sql_images = "select project_id from projects " .
					  "left join orders on order_id = project_order " .
			          "left join project_costs on project_cost_order = order_id " .
					   "left join project_costtypes on project_costtype_id = project_cost_type " .
					   "left join product_lines on project_product_line = product_line_id ".
					   "left join postypes on postype_id = project_postype ".
					   "left join countries on order_shop_address_country = countries.country_id ".
						"left join project_states on project_state_id = project_state ".
					  "where (order_retail_operator is null " .
					  "or project_retail_coordinator is null " .
					  "or project_real_opening_date is null " . 
					  "or project_real_opening_date = '0000-00-00') " .
					  "and order_actual_order_state_code = '100' ".
					  "   and " . $list_filter;
	}
	else
	{
		$sql_images = "select project_id from projects " .
					  "left join orders on order_id = project_order " .
			          " left join project_costs on project_cost_order = order_id " .
					  "where (order_retail_operator is null " .
					  "or project_retail_coordinator is null " .
					  "or project_real_opening_date is null " . 
					  "or project_real_opening_date = '0000-00-00') " .
					  "and order_actual_order_state_code = '100' ".
					  "   and " . $list_filter;
	}

    $res = mysql_query($sql_images) or dberror($sql_images);

    while ($row = mysql_fetch_assoc($res))
    {
        $images[$row["project_id"]] = "/pictures/bullet_ball_glass_red.gif";
    }
}


// check if all 1:n relations in the process are fully processed
$fully_processed = array();
$duty_free_projects = array();
$cer_approved = array();

$sql_tmp = $sql . " where " . $list_filter;
$res = mysql_query($sql_tmp);

while($row = mysql_fetch_assoc($res))
{
	$att = "";

	if($row['order_actual_order_state_code'] >= ORDER_TO_SUPPLIER_SUBMITTED)
	{
		//check if all items were ordered
		$ordered = check_if_all_items_hav_order_date($row['order_id'], "");
		if ($ordered == 0)
		{
			$fully_processed[$row['project_id']] = "/pictures/wf_warning.gif";
		}
	}
	elseif($row['order_actual_order_state_code'] >= REQUEST_FOR_DELIVERY_SUBMITTED)
	{
		//check if a request for delivery was made for all items
		$delivered = check_requast_for_deleivery_for_all_items($row['order_id'], REQUEST_FOR_DELIVERY_SUBMITTED);
		if ($delivered == 0)
		{
			$fully_processed[$row['project_id']] = "/pictures/wf_warning.gif";
		}
	}

	if($row['project_actual_opening_date'] != NULL and $row['project_actual_opening_date'] != '0000-00-00')
	{
		$aods[$row["project_id"]] = "/pictures/ok.gif";
	}
	else
	{
		$aods[$row["project_id"]] = "/pictures/not_ok.gif";
	}

	if($row['project_cost_cms_completed'] == 1)
	{
		$cms_states[$row["project_id"]] = "/pictures/ok.gif";
	}
	else
	{
		$cms_states[$row["project_id"]] = "/pictures/not_ok.gif";
	}
	
	
	//cehck if project is a duty free project
	$project_design_objective_item_ids = get_project_design_objective_item_ids($row["project_id"]);
	
	if(array_key_exists(161 ,$project_design_objective_item_ids))
	{
		$duty_free_projects[$row["project_id"]] = "/pictures/dutyfree.png";
	}

	//check if cer was approved (miletones id = 21 or 12)
	if($show_cer_state == true)
	{
		/* not for luxury brands
		if(($row['project_cost_type'] == 6 or $row['project_cost_type'] == 2) and $row['project_postype'] == 2)
		{
			//do nothing
		}
		else
		*/

		if($row['order_actual_order_state_code'] == '620')
		{
			
			$mileston_date_set = false;
			$sql_m = "select project_milestone_date " . 
				     "from project_milestones " . 
				     "where project_milestone_project = " . $row["project_id"] . 
				     " and project_milestone_milestone in (12, 21)";

			$res_m = mysql_query($sql_m) or dberror($sql_m);
			while ($row_m = mysql_fetch_assoc($res_m))
			{
				if($row_m["project_milestone_date"] != NULL and $row_m["project_milestone_date"] != '0000-00-00')
				{
					$mileston_date_set = true;
				}
			}

			if($mileston_date_set == false)
			{
				$cer_approved[$row['project_id']] = "/pictures/money_not_ok.gif";
			}
			else
			{
				$cer_approved[$row['project_id']] = "/pictures/money_ok.gif";
			}
		}
	}	
	

}


if(in_array(2, $user_roles))
{
	$list_filter .= " and ((project_cost_cms_completed = 0) or (project_cost_cms_completed = 1 and order_actual_order_state_code < 800))";
}

/********************************************************************
   Search Form
*********************************************************************/ 
if (has_access("has_access_to_all_projects"))
{
	$form = new Form("addresses", "address");

	$form->add_section("List Filter Selection");

	$form->add_edit("search_term", "Search Term", 0, param('search_term'), TYPE_CHAR, 20);

}

/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);
$list->add_hidden("showall", param("showall"));

$list->set_entity("projects");
$list->set_order("left(projects.date_created, 10) desc");
$list->set_filter($list_filter);   

$list->add_image_column("todo", "Job", 0, $images);




if (preg_match("/" . APPLICATION_URLPART . "/", $_SERVER["HTTP_HOST"]))
{
	$link = APPLICATION_URL . "/user";
}
else
{
	$link = "/user";
}


$list->add_column("project_number", "Project No.", $link . "/project_task_center.php?pid={project_id}", "", "", COLUMN_NO_WRAP);
$list->add_column("project_costtype_text", "Legal Type", "", LIST_FILTER_LIST, $sql_legal_types, COLUMN_NO_WRAP);
$list->add_column("projectkind_code", "Kind", "", LIST_FILTER_LIST, $sql_project_kinds, COLUMN_NO_WRAP);
$list->add_column("product_line_name", "Product Line", "", LIST_FILTER_LIST, $sql_product_lines, COLUMN_NO_WRAP);
$list->add_column("postype_name", "POS Type", "", LIST_FILTER_LIST, $sql_pos_types, COLUMN_NO_WRAP);


if (has_access("has_access_to_all_projects"))
{
	$list->add_image_column("order_state", "", 0, $fully_processed);
}

if(has_access("has_access_to_order_status_report_in_projects"))
{
    $popup_link = "popup1:project_status_report.php?pid=";
    $list->add_column("order_actual_order_state_code", "Status", $popup_link . "{project_id}");
}
else
{
    $list->add_column("order_actual_order_state_code", "Status");
}


if($show_cer_state == true)
{
	$list->add_image_column("cer_approved", "", 0, $cer_approved);
}


$list->add_column("real_opening_date", "Agreed \nOpening Date", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK | COLUMN_UNDERSTAND_HTML);


$list->add_column("country_name", "Country", "", LIST_FILTER_LIST, $sql_countries);
$list->add_column("concat(order_shop_address_place,', ', order_shop_address_company)", "POS Address");

$list->add_column("project_retail_coordinator", "PM" , "", LIST_FILTER_LIST, $sql_rtcs);
$list->add_column("order_retail_operator", "RTO", "", LIST_FILTER_LIST, $sql_rtos);

$list->add_image_column("dutyfree", "DF", 0, $duty_free_projects);



if (has_access("has_access_to_all_projects"))
{
	$list->add_image_column("cms", "CMS", 0, $cms_states);
	$list->add_image_column("aod", "AOD", 0, $aods);
}

if (has_access("has_access_to_all_projects") and has_access("can_view_all_entries_at_start_in_projects"))
{
}
else if (has_access("has_access_to_all_projects"))
{
    
	if(param('search_term') and param("showall")) {
		$list->add_button("show_all", "Show Complete List");
	}
	elseif (param("showall"))
    {
        $list->add_button("show_my_list", "Show My List");
    }
    else
    {
        $list->add_button("show_all", "Show Complete List");
    }
}

if (has_access("can_view_extended_view_in_projects"))
{
   $list->add_button("extend", "Extended View");
}
if (has_access("has_access_to_archive"))
{
   $list->add_button("archive", "Access Archive");
}

$list->add_button(LIST_BUTTON_FILTER, "Filter");
$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove filter");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$list->process();


if ($list->button("new"))
{
    redirect("project_new_01.php");
}
if ($list->button("show_all"))
{
    $link = "projects.php?showall=1";
    redirect($link);
}
else if ($list->button("show_my_list"))
{
    $link = "projects.php";
    redirect($link);
}
else if ($list->button("extend"))
{
    if (param("showall"))
    {
        $link = "projects_extended_view.php?showall=1";
    }
    else
    {
        $link = "projects_extended_view.php";
    }
    redirect($link);
}
else if ($list->button("archive"))
{
    $link = "../archive/projects_archive.php";
    redirect($link);
}

$page = new Page("projects");

if (has_access("has_access_to_projects_dashboard"))
{
	$page->register_action('dashboard', "Dashboard", "", "", "dashboard");
}

if(has_access("has_access_to_all_projects") or has_access("can_complete_cms") or has_access("can_approve_cms"))
{
	$page->register_action('home', 'Projects with completed CMS', "projects_cms_completed.php");
}

if (has_access("can_create_new_projects"))
{
    $page->register_action('new', 'New Project', "project_new_01.php");

}


//check if there are changes in project numbers
$sql_y = "select DISTINCT YEAR(oldproject_numbers.date_created) as year " . 
	   "from oldproject_numbers "  .
	   "left join projects on project_id = oldproject_number_project_id " . 
	   "left join orders on project_order = order_id ".
	   "inner join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
       "left join product_lines on project_product_line = product_line_id ".
       "left join postypes on postype_id = project_postype ".
	   "left join project_states on project_state_id = project_state ".
       "left join countries on order_shop_address_country = countries.country_id ".
       "left join users as rtcs on project_retail_coordinator = rtcs.user_id " .
	   "left join projectkinds on projectkind_id = project_projectkind ".
	   "left join users as rtos on order_retail_operator = rtos.user_id ";

	   " where " . $list_filter .
	   " order by year DESC";

$res = mysql_query($sql_y) or dberror($sql_y);
if ($row = mysql_fetch_assoc($res))
{
	$page->register_action('changedprojectnumbers', 'Info about Changed Project Numbers', "changed_projectnumbers.php");
}


if(has_access("has_access_to_all_projects"))
{
	$page->register_action('_project_numbers', 'Latest Project Numbers', "latest_project_numbers.php");
}





$page->register_action('home', 'Home', "welcome.php");

$page->header();
$page->title("Projects");

if (has_access("has_access_to_all_projects"))
{
	$form->render();
}
$list->render();

?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#dashboard').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/user/dashboard_projects.php'
    });
    return false;
  }); 
});
</script>

<?php

$page->footer();
?>