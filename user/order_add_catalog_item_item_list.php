<?php
/********************************************************************

    order_add_catalog_item_item_list.php

    Add items to the list of materials from category item list.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-11
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-02-16
    Version:        1.0.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.


*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "include/order_functions.php";


check_access("can_edit_list_of_materials_in_orders");


/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$order = get_order(param("oid"));


// get company's address
$client_address = get_address($order["order_client_address"]);


// get the region of the client's
$client_region = get_address_region($order["order_client_address"]);


$searchterm = '';
if(array_key_exists('searchterm', $_GET) and $_GET['searchterm'] != '') 
{
	$searchterm = $_GET['searchterm'];
}
else
{
	$searchterm = param('searchterm');
}


// create sql for oder items belongin to the selected category
if($searchterm)
{

	/*
	$sql_catrgory_items = "select item_id, category_item_id, item_code, item_name, concat(product_line_name, ' - ', category_name) as productline_category ".
		   "from category_items " .
		   "left join items on category_items.category_item_item=items.item_id " .
		   "left join categories on category_id = category_item_category " .
		   "left join product_lines on product_line_id = category_product_line " .
		   "left join item_regions on item_region_item = item_id ";

	
	   
	  $list_filter = "(LOWER(item_code) like '%" . strtolower($searchterm) . "%' OR LOWER(item_name) like '%" .  strtolower($searchterm) . "%' OR LOWER(category_name) like '%" .  strtolower($searchterm) . "%' OR LOWER(product_line_name) like '%" .  strtolower($searchterm) . "%' )" .
					   "    and item_visible = 1 " .
					   "    and item_active = 1 " .
					   "    and category_catalog = 1 " .
					   "    and (category_not_in_use is null or category_not_in_use = 0) " . 
					   "    and product_line_visible = 1 " . 
					   "    and item_region_region = " . $client_region;


	   
	  $where_clause = " where (LOWER(item_code) like '%" . strtolower($searchterm) . "%' OR LOWER(item_name) like '%" .  strtolower($searchterm) . "%' OR LOWER(category_name) like '%" .  strtolower($searchterm) . "%' OR LOWER(product_line_name) like '%" .  strtolower($searchterm) . "%' )" .
					   "    and item_visible = 1 " .
					   "    and item_active = 1 " .
					   "    and category_catalog = 1 " .
					   "    and (category_not_in_use is null or category_not_in_use = 0) " . 
					   "    and product_line_visible = 1 " . 
					   "    and item_region_region = " . $client_region;


	  */
	  $sql_catrgory_items = "select DISTINCT item_id, item_code, item_name ".
		   "from category_items " .
		   "left join items on category_items.category_item_item=items.item_id " .
		   "left join categories on category_id = category_item_category " .
		   "left join product_lines on product_line_id = category_product_line " .
		   "left join item_regions on item_region_item = item_id ";

	
	   
	  $list_filter = "(LOWER(item_code) like '%" . strtolower($searchterm) . "%' OR LOWER(item_name) like '%" .  strtolower($searchterm) . "%' OR LOWER(category_name) like '%" .  strtolower($searchterm) . "%' OR LOWER(product_line_name) like '%" .  strtolower($searchterm) . "%' )" .
					   "    and item_visible = 1 " .
					   "    and item_active = 1 " .
					   "    and category_catalog = 1 " .
					   "    and (category_not_in_use is null or category_not_in_use = 0) " . 
					   "    and product_line_visible = 1 " . 
					   "    and item_region_region = " . $client_region;


	   
	  $where_clause = " where (LOWER(item_code) like '%" . strtolower($searchterm) . "%' OR LOWER(item_name) like '%" .  strtolower($searchterm) . "%' OR LOWER(category_name) like '%" .  strtolower($searchterm) . "%' OR LOWER(product_line_name) like '%" .  strtolower($searchterm) . "%' )" .
					   "    and item_visible = 1 " .
					   "    and item_active = 1 " .
					   "    and category_catalog = 1 " .
					   "    and (category_not_in_use is null or category_not_in_use = 0) " . 
					   "    and product_line_visible = 1 " . 
					   "    and item_region_region = " . $client_region;
	
						
			
}
else
{

	$sql_catrgory_items = "select item_id, item_code, item_name ".
					   "from category_items ".
					   "left join items on category_item_item = items.item_id " .
					   "left join item_regions on item_region_item = item_id ";




	$where_clause = " where category_item_category = " . id() . 
					"    and item_visible = 1 " .
					"    and item_active = 1 " .
					"    and item_region_region = " . $client_region;


	$list_filter = "category_item_category = " . id() . 
               "    and item_visible = 1 " .
               "    and item_active = 1 " .
               "    and item_region_region = " . $client_region;
}


// read all items belonging to the category and beeing conteined in the order
$values = array();
$res = mysql_query($sql_catrgory_items . $where_clause) or dberror($sql_catrgory_items . $where_clause);
while ($row = mysql_fetch_assoc($res))
{
    // Check if item is already in table order_items
    $sql_order_item = "select order_item_item, order_item_quantity ".
                      "from order_items ".
                      "where order_item_order = " . param('oid') .
                      "    and order_item_item = " . $row["item_id"];


    $res1 = mysql_query($sql_order_item) or dberror($sql_order_item);
    if ($row1 = mysql_fetch_assoc($res1))
    {
        $values[$row["item_id"]] = $row1["order_item_quantity"];
    }
    else
    {
        $values[$row["item_id"]] = "";
    }
}


/********************************************************************
    Create Form
*********************************************************************/ 


$form = new Form("orders", "order", 640);


$form->add_section("Order");


require_once "include/order_head_small.php";


/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql_catrgory_items);


$list->set_entity("itmes");
$list->set_filter($list_filter);
$list->set_order("item_code");

if($searchterm) 
{
	//$list->set_group("productline_category");
	//$list->set_order("product_line_name, category_name, item_code");
}


$list->add_hidden("oid", param("oid"));
$list->add_hidden("cid", id());


$list->add_column("item_code", "Item Code", "popup:catalog_item_view.php?id={item_id}");
$list->add_column("item_name", "Item Name");
$list->add_edit_column("item_entry_field", "Quantity", "4", 0, $values);


$list->add_button("add_items", "Add Items");
$list->add_button(LIST_BUTTON_BACK, "Back");




/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();


if ($list->button("add_items"))
{
    project_save_order_items($list, ITEM_TYPE_STANDARD);
    $link = "order_edit_material_list.php?oid=" . param("oid"); 
    redirect($link);
}


/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");


require "include/order_page_actions.php";


$page->header();
$page->title("Add Catalog Items");
$form->render();


echo "<p>Please indicate the quantities to add to the list.", "</p>";


$list->render();
$page->footer();


?>