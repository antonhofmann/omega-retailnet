<?php
/********************************************************************
    project_edit_material_list_delete_items.php

    Delete Items in the list of materials

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-30
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-30
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.


*********************************************************************/
require_once "../include/frame.php";
require_once "include/save_functions.php";

check_access();

$project = param("pid");
$order = param("oid");
$type = param("t");


if($type == 1)
{
    $text = "Are you sure to delete all standard catalogue items?";
}
elseif($type == 2)
{
    $text = "Are you sure to delete all special items?";
}
elseif($type == 3)
{
    $text = "Are you sure to delete all cost estimation positions?";
}
elseif($type == 6)
{
    $text = "Are you sure to delete all local construction cost positions?";
}

$form = new Form();
$form->add_hidden("pid", $project);
$form->add_hidden("oid", $order);
$form->add_hidden("t", $type);

if ($form->button("delete"))
{
	order_delete_all_items(param("oid"), param("t"));
    $link = "project_edit_material_list.php?pid=" . param("pid");
    redirect($link);
}
else if ($form->button("abort"))
{
    $link = "project_edit_material_list.php?pid=" . param("pid");
    redirect($link);
}
else
{
    $form->add_comment($text);
    $form->add_button("delete", "Yes");
    $form->add_button("abort", "No");
}


$page = new Page();

require "include/project_page_actions.php";

$page->header();
$page->title("Delete Items");
$form->render();
$page->footer();
?>