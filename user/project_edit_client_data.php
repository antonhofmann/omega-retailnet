<?php
/********************************************************************

    project_edit_client_data.php

    Edit project's  data as entered by the client.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-07
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-19
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "../shared/func_posindex.php";

check_access("can_edit_client_data_in_projects");


/********************************************************************
    prepare all data needed
*********************************************************************/


// Vars
$error = "unspecified";
$design_objectives_listbox_names = array();
$design_objectives_checklist_names = array();

// read project and order details
$project = get_project(param("pid"));

$old_project_number = $project["project_number"];
$old_pos_type = $project["project_postype"];

//check if the cost monitoring sheet exists already
$sql = "select count(project_cost_id) as num_recs " .
       "from project_costs " .
       "where project_cost_order = "  . $project["project_order"]; 
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

if($row["num_recs"] == 0)
{
    $fields = array();
    $values = array();

    $fields[] = "project_cost_order";
    $values[] = $project["project_order"];
    
    $fields[] = "date_created";
    $values[] = dbquote(date("Y-m-d"));

    $fields[] = "date_modified";
    $values[] = dbquote(date("Y-m-d"));

    $fields[] = "user_created";
    $values[] = dbquote(user_login());

    $fields[] = "user_modified";
    $values[] = dbquote(user_login());

    $sql = "insert into project_costs (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

    mysql_query($sql) or dberror($sql);
}

//update order items with predefined cost_groups
$sql = "select order_item_id, order_item_item, item_cost_group, item_costmonitoring_group " . 
       "from order_items " . 
	   "left join items on item_id = order_item_item " .
	   "where order_item_order = " . $project["project_order"] . 
	   "  and order_item_not_in_budget <> 1 " .
	   "  and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION . " or order_item_type >= " . ITEM_TYPE_LOCALCONSTRUCTIONCOST . ") ";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$sql_u = "update order_items Set order_item_cost_group =  " . dbquote($row["item_cost_group"]) . " " . 
		     " where (order_item_cost_group is NULL or order_item_cost_group = 0) " .
		     "  and order_item_id = " . $row["order_item_id"];

	mysql_query($sql_u) or dberror($sql_u);

	$sql_u = "update order_items Set order_items_costmonitoring_group =  " . dbquote($row["item_costmonitoring_group"]) . " " . 
		     " where (order_items_costmonitoring_group is NULL or order_items_costmonitoring_group = 0) " .
		     "  and order_item_id = " . $row["order_item_id"];

	mysql_query($sql_u) or dberror($sql_u);
}

//get project_projectkind
$sql = "select project_projectkind from projects where project_id = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$old_projectkind = $row["project_projectkind"];
}

// get users' company address
$address = get_address($project["order_client_address"]);

// get client user data
$user = get_user($project["order_user"]);

//project legal types
if($address["client_type"] == 1) // client is agent, only franchisee as option
{
    $sql_project_cost_types = "select * from project_costtypes where project_costtype_id IN (2,6)";
}
else
{
    $sql_project_cost_types = "select * from project_costtypes where project_costtype_id IN (1, 2, 6)";    
}


$order_state_name = get_actual_order_state_name($project["order_actual_order_state_code"], 1);

$project_state_name = get_project_state_name($project["project_state"]);

// get orders's currency
$currency = get_order_currency($project["project_order"]);

// read design_item_ids from project items
$project_design_objective_item_ids = get_project_design_objective_item_ids(param("pid"));

$delivery_address = get_order_address(2, $project["project_order"]);


// create sql for the client listbox
$sql_address = "select address_id, address_company ".
               "from addresses ".
               "where address_type = 1 or address_type = 4 ".
               "order by address_company";


// create array for the franchisee address listbox
//$franchisee_addresses = get_franchisee_addresses($project["order_client_address"]);

// create array for the billing address listbox
//$billing_addresses = get_billing_addresses($project["order_client_address"]);

// create array for the delivery address listbox
//$delivery_addresses = get_delivery_addresses($project["order_client_address"]);

// create sql for the client's contact listbox
if (!param("client_address_id"))
{
    $sql_address_user = "select user_id, concat(user_name, ' ', user_firstname) ".
                        "from users ".
                        "where user_id = " . $project["order_user"] . " or (user_active = 1 and user_address = ". $project["order_client_address"] . ") ".
                        "order by user_name";
}
else
{
    $sql_address_user = "select user_id, concat(user_name, ' ', user_firstname) ".
                        "from users ".
                        "where user_id = " . $project["order_user"] . " or (user_active = 1 and user_address = ". param("client_address_id") . ") ".
                        "order by user_name";
}

// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ".
                 "order by country_name";

// create sql for the location type listbox
$sql_location_types = "select location_type_id, location_type_name ".
                      "from location_types ".
                      "order by location_type_name";

// create sql for the voltage listbox
$sql_voltages = "select voltage_id, voltage_name ".
                "from voltages";

// create sql for the transportation type listbox
$sql_transportation_modes = "select transportation_type_id, transportation_type_name ".
                            "from transportation_types ".
                            "where transportation_type_visible = 1 " .
							" and transportation_type_code = 'mode' " . 
                            "order by transportation_type_name";


$sql_transportation_arranged = "select transportation_type_id, transportation_type_name ".
								"from transportation_types ".
								"where transportation_type_visible = 1 " .
								" and transportation_type_code = 'arranged' " . 
								"order by transportation_type_name";

// create sql for the project type listbox
$sql_pos_types = "select postype_id, postype_name ".
                     "from postypes ".
                     "order by postype_name";


//pos subclasses
if(param("project_postype"))
{
	$sql_pos_subclasses = "select possubclass_id, possubclass_name " .
		              "from postype_subclasses " .
		              "left join possubclasses on possubclass_id = postype_subclass_subclass " .
		              "left join product_line_pos_types on  product_line_pos_type_id = postype_subclass_pl_pos_type " . 
		              "where  product_line_pos_type_pos_type = " . param("project_postype") .
		              " and product_line_pos_type_product_line is null " .
	                  " order by possubclass_name";
}
elseif($project["project_postype"])
{
	$sql_pos_subclasses = "select possubclass_id, possubclass_name " .
		              "from postype_subclasses " .
		              "left join possubclasses on possubclass_id = postype_subclass_subclass " .
		              "left join product_line_pos_types on  product_line_pos_type_id = postype_subclass_pl_pos_type " . 
		              "where  product_line_pos_type_pos_type = " . $project["project_postype"] .
		              " and product_line_pos_type_product_line is null " .
	                  " order by possubclass_name";
}
else
{
	$sql_pos_subclasses = "select possubclass_id, possubclass_name " .
		              "from postype_subclasses " .
		              "left join possubclasses on possubclass_id = postype_subclass_subclass " .
		              "left join product_line_pos_types on  product_line_pos_type_id = postype_subclass_pl_pos_type " . 
		              "where  product_line_pos_type_pos_type = 0 " .
		              " and product_line_pos_type_product_line =  0" .
	                  " order by possubclass_name";
}

//get addresses from pos index
$sql_posaddresses = "select posaddress_id, concat(posaddress_place, ', ', posaddress_name, ', ', posaddress_address) as posaddress " .
                    "from posaddresses " . 
					"where posaddress_country = " . $project["order_shop_address_country"] . 
					" order by posaddress_place, posaddress_name";


//project legal types
if(param("project_cost_type"))
{
	if(param("project_cost_type") == 1) // Corporate
	{
		$sql_project_kinds = "select projectkind_id, projectkind_name " . 
			                 "from projectkinds " . 
			                 "where projectkind_id > 0 ";
	}
	else
	{
		$sql_project_kinds = "select projectkind_id, projectkind_name " . 
			                 "from projectkinds " . 
			                 "where projectkind_id in (1,2,5,6)";
	}
}
else
{
	if($project["project_cost_type"] == 1) // Corporate
	{
		$sql_project_kinds = "select projectkind_id, projectkind_name " . 
			                 "from projectkinds " . 
			                 "where projectkind_id > 0 ";
	}
	else
	{
		$sql_project_kinds = "select projectkind_id, projectkind_name " . 
			                 "from projectkinds " . 
			                 "where projectkind_id in (1,2,5, 6)";
	}
}

// create sql for the furniture height listbox
$sql_ceiling_heights = "select ceiling_height_id, ceiling_height_height ".
                "from ceiling_heights";

//get pos data
if($project["pipeline"] == 0)
{
	$pos_data = get_poslocation($project["posaddress_id"], "posaddresses");
}
elseif($project["pipeline"] == 1)
{
	$pos_data = get_poslocation_from_pipeline($project["posaddress_id"], $project["order_id"]);
}

$projectkind_has_changed = false;

if($old_projectkind == 1 and param("project_projectkind") > 1) // changed from NEW to another type
{
	$projectkind_has_changed = true;
}


//create sql for places
if(param("invoice_recipient")) {
	
	$invoice_address = get_address(param("invoice_recipient"));
	$sql_billing_places = "select place_id, place_name from places " . 
		          "where place_country = " .  dbquote($invoice_address["country"]) . 
		          " order by place_name";

}
elseif(param("billing_address_country")) {
	$sql_billing_places = "select place_id, place_name from places " . 
		          "where place_country = " .  dbquote(param("billing_address_country")) . 
		          " order by place_name";
}
elseif($project["order_billing_address_country"]) {
	$sql_billing_places = "select place_id, place_name from places " . 
		          "where place_country = " .  dbquote($project["order_billing_address_country"]) . 
		          " order by place_name";
}

if(param("delivery_address_country")) {
	$sql_delivery_places = "select place_id, place_name from places " . 
		          "where place_country = " .   dbquote(param("delivery_address_country")) . 
		          " order by place_name";

}
elseif($delivery_address['country']) {
	$sql_delivery_places = "select place_id, place_name from places " . 
		          "where place_country = " .   dbquote($delivery_address['country']) . 
		          " order by place_name";
}
else
{
	$sql_delivery_places = "select place_id, place_name from places " . 
		          "where place_country = " .  dbquote($address['country']) . 
		          " order by place_name";
}


$billing_address_province_name = "";
if(param("billing_address_place_id"))
{
	$billing_address_province_name = get_province_name(param("billing_address_place_id"));
}
elseif($project["order_billing_address_place_id"])
{
	$billing_address_province_name = get_province_name($project["order_billing_address_place_id"]);
}



$delivery_address_province_name = "";
if(param("delivery_address_place_id"))
{
	$delivery_address_province_name = get_province_name(param("delivery_address_place_id"));
}
elseif($delivery_address["place_id"])
{
	$delivery_address_province_name = get_province_name($delivery_address["place_id"]);
}

//get invoice_addresses
$invoice_addresses = array();

$sql_inv = "select invoice_address_id, concat(invoice_address_company, ', ' ,place_name, ', ', country_name) as company " .
    "from invoice_addresses " .
	"left join places on place_id = invoice_address_place_id " . 
	"left join countries on country_id = invoice_address_country_id " . 
	"where invoice_address_active = 1 and invoice_address_address_id = " . $address["id"] . 
    " order by invoice_address_company";

$res = mysql_query($sql_inv) or dberror($sql_inv);
while ($row = mysql_fetch_assoc($res))
{
	$invoice_addresses[$row["invoice_address_id"]] = $row["company"];
}

/********************************************************************
    Get Cost Monitoring Sheet Budget Data
*********************************************************************/ 
$sql =  "select * from project_costs " .
        "where project_cost_order = " . $project["project_order"];

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

$project_cost_sqms = $row["project_cost_sqms"];

/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

$form->add_section("Project");
$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid", $project["project_order"]);

if (has_access("can_edit_product_line"))
{
    
    if (has_access("can_edit_project_number"))
    {
        $form->add_edit("project_number", "Project Number*", NOTNULL, $project["order_number"], TYPE_CHAR);
		$form->add_label("treatment_state", "Treatment State", 0, $project["project_state_text"]);
    }
    else
    {
        $form->add_label("project_number", "Project Number*", 0, $project["order_number"]);
		$form->add_label("treatment_state", "Treatment State", 0, $project["project_state_text"]);
    }

     $form->add_lookup("product_line", "Product Line", "product_lines", "product_line_name", NOTNULL, $project["project_product_line"]);
	//$form->add_list("product_line", "Product Line*", $sql_product_line, SUBMIT | NOTNULL, $project["project_product_line"]);
	$form->add_lookup("product_line_subclass", "Product Line Subclass", "productline_subclasses", "productline_subclass_name", 0, $project["project_product_line_subclass"]);
    
    $form->add_list("project_postype", "POS Type*", $sql_pos_types, NOTNULL | SUBMIT, $project["project_postype"]);

	$form->add_list("project_pos_subclass", "POS Type Subclass",
    $sql_pos_subclasses, 0, $project["project_pos_subclass"]);

    //$form->add_list("project_cost_type", "Project Legal Type", $sql_project_cost_types, SUBMIT, $project["project_cost_type"]);
	$form->add_list("project_cost_type", "Project Legal Type", $sql_project_cost_types, 0, $project["project_cost_type"]);
	
	/*
	if($old_projectkind != 2)
	{
		$form->add_list("project_projectkind", "Project Kind*", $sql_project_kinds, SUBMIT | NOTNULL, $project["project_projectkind"]);
	}
	else
	{
		$form->add_label("project_projectkind_label", "Project Kind", 0, $project["projectkind_name"]);
		$form->add_hidden("project_projectkind", $project["project_projectkind"]);
	}
	*/
	$form->add_list("project_projectkind", "Project Kind*", $sql_project_kinds,SUBMIT | NOTNULL, $project["project_projectkind"]);
}
else
{
    $form->add_label("project_number", "Project Number*", 0, $project["order_number"]);

    $form->add_lookup("product_line", "Product Line", "product_lines", "product_line_name", NOTNULL, $project["project_product_line"]);
	$form->add_lookup("product_line_subclass", "Product Line Subclass", "productline_subclasses", "productline_subclass_name", 0, $project["project_product_line_subclass"]);
    
    $form->add_lookup("project_postype", "POS Type", "postypes", "postype_name", NOTNULL, $project["project_postype"]);

	$form->add_lookup("project_pos_subclass", "POS Type Subclass", "possubclasses", "possubclass_name", NOTNULL, $project["project_pos_subclass"]);

    //$form->add_list("project_cost_type", "Project Legal Type", $sql_project_cost_types, SUBMIT, $project["project_cost_type"]);
	$form->add_list("project_cost_type", "Project Legal Type", $sql_project_cost_types, 0, $project["project_cost_type"]);
	
	/*
	if($old_projectkind != 2)
	{
		$form->add_list("project_projectkind", "Project Kind*", $sql_project_kinds, SUBMIT | NOTNULL, $project["project_projectkind"]);
	}
	else
	{
		$form->add_label("project_projectkind_label", "Project Kind", 0, $project["projectkind_name"]);
		$form->add_hidden("project_projectkind", $project["project_projectkind"]);
	}
	*/

	$form->add_list("project_projectkind", "Project Kind*", $sql_project_kinds, SUBMIT | NOTNULL, $project["project_projectkind"]);
}

if (has_access("can_edit_status_in_projects"))
{
    $sql = "select distinct order_state_code " .
           "from order_states " .
           "order by order_state_code";
    $form->add_list("status", "Project State", $sql, NOTNULL, $project["order_actual_order_state_code"]);

	/*
	$sql = "select project_state_id, project_state_text from " .
           " project_states  where project_state_selectable = 1";
    $form->add_list("project_state", "Treatment State", $sql, NOTNULL, $project["project_state"]);
    */

	$form->add_hidden("project_state", $project["project_state"]);
	$form->add_label("status2", "Treatment State", 0, $project_state_name);
}
else
{
    $form->add_hidden("project_state", $project["project_state"]);

    $form->add_hidden("status", $project["order_actual_order_state_code"]);

    $form->add_label("status1", "Project State", 0, $project["order_actual_order_state_code"]  . " " . $order_state_name);

    $form->add_label("status2", "Treatment State", 0, $project_state_name);
}

$form->add_section("Client");
$form->add_list("client_address_id", "Client*", $sql_address, SUBMIT | NOTNULL, $project["order_client_address"]);
$form->add_list("client_address_user_id", "Contact*", $sql_address_user, NOTNULL, $project["order_user"]);

if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) // take over, lease renewal
{
	$form->add_hidden("order_direct_invoice_address_id", $project["order_direct_invoice_address_id"]);


	$form->add_hidden("billing_address_company", $project["order_billing_address_company"]);
	$form->add_hidden("billing_address_company2", $project["order_billing_address_company2"]);
	$form->add_hidden("billing_address_address", $project["order_billing_address_address"]);
	$form->add_hidden("billing_address_address2",$project["order_billing_address_address2"]);
	$form->add_hidden("billing_address_zip", $project["order_billing_address_zip"]);
	$form->add_hidden("billing_address_place", $project["order_billing_address_place"]);
	$form->add_hidden("billing_address_place_id", $project["order_billing_address_place_id"]);
	$form->add_hidden("billing_address_country", $project["order_billing_address_country"]);
	$form->add_hidden("billing_address_phone", $project["order_billing_address_phone"]);
	$form->add_hidden("billing_address_fax", $project["order_billing_address_fax"]);
	$form->add_hidden("billing_address_email", $project["order_billing_address_email"]);
	$form->add_hidden("billing_address_contact", $project["order_billing_address_contact"]);


	$form->add_hidden("delivery_address_company", $delivery_address["company"]);
	$form->add_hidden("delivery_address_company2", $delivery_address["company2"]);
	$form->add_hidden("delivery_address_address", $delivery_address["address"]);
	$form->add_hidden("delivery_address_address2", $delivery_address["address2"]);
	$form->add_hidden("delivery_address_zip", $delivery_address["zip"]);
	$form->add_hidden("delivery_address_place", $delivery_address["place"]);
	$form->add_hidden("delivery_address_place_id", $delivery_address['place_id']);
	$form->add_hidden("delivery_address_country", $delivery_address["country"]);
	$form->add_hidden("delivery_address_phone", $delivery_address["phone"]);
	$form->add_hidden("delivery_address_fax", $delivery_address["fax"]);
	$form->add_hidden("delivery_address_email", $delivery_address["email"]);
	$form->add_hidden("delivery_address_contact", $delivery_address["contact"]);
}
else
{
	$form->add_section("Notify Address");

	if($address["invoice_recipient"] > 0)
	{
		$sql_invoice_addresses = "select address_id, concat(country_name, ': ', address_company) as company from addresses " .
                 "left join countries on country_id = address_country " .
				 "where address_id = " . $address["id"] . " or address_id = " . $address["invoice_recipient"] . 
			     " order by country_name, address_company";
		$form->add_list("invoice_recipient", "Change Notify Address", $sql_invoice_addresses, SUBMIT);
	}

	$form->add_edit("billing_address_company", "Company*", NOTNULL, $project["order_billing_address_company"], TYPE_CHAR);
	$form->add_edit("billing_address_company2", "", 0, $project["order_billing_address_company2"], TYPE_CHAR);
	$form->add_edit("billing_address_address", "Address*", NOTNULL, $project["order_billing_address_address"], TYPE_CHAR);
	$form->add_edit("billing_address_address2", "", 0, $project["order_billing_address_address2"], TYPE_CHAR);
	$form->add_edit("billing_address_zip", "ZIP*", NOTNULL, $project["order_billing_address_zip"], TYPE_CHAR, 20);
	$form->add_list("billing_address_place_id", "City*", $sql_billing_places, NOTNULL |SUBMIT, $project["order_billing_address_place_id"]);
	$form->add_label("billing_address_place", "", 0,$project["order_billing_address_place"]);
	$form->add_label("billing_address_province_name", "Province", 0, $billing_address_province_name);
	$form->add_list("billing_address_country", "Country*", $sql_countries, SUBMIT | NOTNULL, $project["order_billing_address_country"]);
	$form->add_edit("billing_address_phone", "Phone*", NOTNULL, $project["order_billing_address_phone"], TYPE_CHAR, 20);
	$form->add_edit("billing_address_fax", "Fax", 0, $project["order_billing_address_fax"], TYPE_CHAR, 20);
	$form->add_edit("billing_address_email", "Email", 0, $project["order_billing_address_email"], TYPE_CHAR);
	$form->add_edit("billing_address_contact", "Contact*", NOTNULL, $project["order_billing_address_contact"], TYPE_CHAR);


	if(count($invoice_addresses) > 0)
	{
		$form->add_section("Notify Address (invoice address) for direct Invoicing");
		$form->add_comment("Please indicate the notify address (invoice address) in case suppliers do send invoices to a different than the above address.");
		$form->add_list("order_direct_invoice_address_id", "Notify address", $invoice_addresses, 0, $project["order_direct_invoice_address_id"]);
	}
	else
	{
		$form->add_hidden("order_direct_invoice_address_id", 0);
	}


	$form->add_section("Delivery Address (consignee address)");
	$form->add_edit("delivery_address_company", "Company*", NOTNULL, $delivery_address["company"], TYPE_CHAR);
	$form->add_edit("delivery_address_company2", "", 0, $delivery_address["company2"], TYPE_CHAR);
	$form->add_edit("delivery_address_address", "Address*", NOTNULL, $delivery_address["address"], TYPE_CHAR);
	$form->add_edit("delivery_address_address2", "", 0, $delivery_address["address2"], TYPE_CHAR);
	$form->add_edit("delivery_address_zip", "ZIP*", NOTNULL, $delivery_address["zip"], TYPE_CHAR, 20);

	$form->add_list("delivery_address_place_id", "City*", $sql_delivery_places, NOTNULL |SUBMIT, $delivery_address['place_id']);
	$form->add_label("delivery_address_place", "", 0, $delivery_address['place']);
	$form->add_label("delivery_address_province_name", "Province", 0, $delivery_address_province_name);

	$form->add_list("delivery_address_country", "Country*", $sql_countries, NOTNULL | SUBMIT, $delivery_address["country"]);
	$form->add_edit("delivery_address_phone", "Phone*", NOTNULL, $delivery_address["phone"], TYPE_CHAR, 20);
	$form->add_edit("delivery_address_fax", "Fax", 0, $delivery_address["fax"], TYPE_CHAR, 20);
	$form->add_edit("delivery_address_email", "Email", 0, $delivery_address["email"], TYPE_CHAR);
	$form->add_edit("delivery_address_contact", "Contact*", NOTNULL, $delivery_address["contact"], TYPE_CHAR);

}

if(count($pos_data) > 0 and $projectkind_has_changed == false)
{
	$form->add_section("POS Location Address");
	$form->add_hidden("posaddress_id", $pos_data["posaddress_id"]);
	$form->add_label("shop_address_company", "Project Name", 0, $pos_data["posaddress_name"]);
	$form->add_label("shop_address_company2", "", 0, $pos_data["posaddress_name2"]);
	$form->add_label("shop_address_address", "Address", 0, $pos_data["posaddress_address"]);
	$form->add_label("shop_address_address2", "", 0, $pos_data["posaddress_address2"]);
	$form->add_label("shop_address_zip", "ZIP", 0, $pos_data["posaddress_zip"]);
	$form->add_label("shop_address_place", "City", 0, $pos_data["posaddress_place"]);
	$form->add_label("shop_address_country", "Country", 0, $pos_data["country_name"]);
	$form->add_label("shop_address_phone", "Phone", 0, $pos_data["posaddress_phone"]);
	$form->add_label("shop_address_fax", "Fax", 0, $pos_data["posaddress_fax"]);
	$form->add_label("shop_address_email", "Email", 0, $pos_data["posaddress_email"]);
}
else
{
	$form->add_section("POS Location Address");
	$form->add_comment("Assignment to POS Index.");
	$form->add_list("posaddress_id", "POS Index POS Name", $sql_posaddresses, SUBMIT, $project["posaddress_id"]);
	$form->add_edit("shop_address_company", "Project Name*", NOTNULL, $project["order_shop_address_company"], TYPE_CHAR);
	$form->add_edit("shop_address_company2", "", 0, $project["order_shop_address_company2"], TYPE_CHAR);
	$form->add_edit("shop_address_address", "Address*", NOTNULL, $project["order_shop_address_address"], TYPE_CHAR);
	$form->add_edit("shop_address_address2", "", 0, $project["order_shop_address_address2"], TYPE_CHAR);
	$form->add_edit("shop_address_zip", "ZIP*", NOTNULL, $project["order_shop_address_zip"], TYPE_CHAR, 20);
	$form->add_edit("shop_address_place", "City*", NOTNULL, $project["order_shop_address_place"], TYPE_CHAR, 20);
	$form->add_list("shop_address_country", "Country*", $sql_countries, NOTNULL, $project["order_shop_address_country"]);
	$form->add_edit("shop_address_phone", "Phone", 0, $project["order_shop_address_phone"], TYPE_CHAR, 20);
	$form->add_edit("shop_address_fax", "Fax", 0, $project["order_shop_address_fax"], TYPE_CHAR, 20);
	$form->add_edit("shop_address_email", "Email", 0, $project["order_shop_address_email"], TYPE_CHAR, 50);
}



if($project["project_projectkind"] == 6
   or param("project_projectkind") == 6) // relocation
{
	$form->add_section("Relocation Info");
	$form->add_comment("Please indicate if this new POS is a relocation of an existing POS within the same mall or city.");
	$form->add_checkbox("project_is_relocation_project", "Yes the POS is relocated", $project["project_is_relocation_project"], "", "Relocation");

	$form->add_list("project_relocated_posaddress_id", "POS being relocated", $sql_posaddresses, 0, $project["project_relocated_posaddress_id"]);
}
else
{
	$form->add_hidden("project_is_relocation_project", 0);
	$form->add_hidden("project_relocated_posaddress_id", 0);
}

if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) // take over, lease renewal
{
	$form->add_hidden("shop_sqms", $project_cost_sqms);
	$form->add_hidden("location_type", $project["project_location_type"]);
	$form->add_hidden("location_type_other", $project["project_location"]);
	$form->add_hidden("voltage", $project["order_voltage"]);


	$form->add_hidden("furniture_height");
	$form->add_hidden("project_furniture_height_mm");

	
	$form->add_hidden("watches_displayed",$project["project_watches_displayed"]);
	$form->add_hidden("watches_stored", $project["project_watches_stored"]);
	$form->add_hidden("bijoux_displayed",$project["project_bijoux_displayed"]);
	$form->add_hidden("bijoux_stored", $project["project_bijoux_stored"]);

	$form->add_hidden("preferred_delivery_date", to_system_date($project["order_preferred_delivery_date"]));
	
	$form->add_list("preferred_transportation_arranged", "Transportation arranged by*", $sql_transportation_arranged, NOTNULL, $project["order_preferred_transportation_arranged"]);

	$form->add_list("preferred_transportation_mode", "Transportation mode*", $sql_transportation_modes, NOTNULL, $project["order_preferred_transportation_mode"]);


	$form->add_hidden( "pedestrian_mall_approval", $project["order_pedestrian_mall_approval"]);
	$form->add_hidden( "full_delivery", $project["order_full_delivery"]);
	$form->add_hidden("delivery_comments", $project["order_delivery_comments"]);

	$form->add_hidden("order_insurance", $project["order_insurance"]);

	$form->add_hidden("approximate_budget", $project["project_approximate_budget"]);
	$form->add_hidden("planned_opening_date", to_system_Date($project["project_planned_opening_date"]));


	$form->add_hidden("comments", $project["project_comments"]);
}
else
{
	$form->add_section("Sales Surface");
	$form->add_edit("shop_sqms", "Sales Surface sqms", 0, $project_cost_sqms, TYPE_DECIMAL, 10,2);


	$form->add_section("Location Info");
	$form->add_list("location_type", "Location Type*", $sql_location_types, 0, $project["project_location_type"]);
	$form->add_edit("location_type_other", "Other Location Type*", 0, $project["project_location"], TYPE_CHAR, 20);
	$form->add_list("voltage", "Voltage Choice*", $sql_voltages, NOTNULL, $project["order_voltage"]);


	$form->add_section("Furniture Height");
	$form->add_list("furniture_height", "Furniture Height in mm*", $sql_ceiling_heights, SUBMIT, $project["project_furniture_height"]);
	$form->add_edit("project_furniture_height_mm", "", 0, $project["project_furniture_height_mm"], TYPE_INT, 4);
	
	if($project["date_created"] < '2010-12-05')
	{
		// count design_objective_groups old version when design objectives were depending on the product line
		$sql_design_objective_group = "select design_objective_group_id, design_objective_group_name, " .
									  "    design_objective_group_multiple " .
									  "from design_objective_groups " .
									  "where design_objective_group_product_line=  " . $project["project_product_line"] . " " .
									  "order by design_objective_group_priority";

		$res = mysql_query($sql_design_objective_group) or dberror($sql_design_objective_group);
		$number_of_design_objective_groups = mysql_num_rows($res);
	}
	else
	{
		// count design_objective_groups new version, design objectives depending on the postype
		$sql_design_objective_group = "select design_objective_group_id, design_objective_group_name, " .
									  "    design_objective_group_multiple " .
									  "from design_objective_groups " .
									  "where design_objective_group_postype =  " . $project["project_postype"] . " " .
									  "order by design_objective_group_priority";

		$res = mysql_query($sql_design_objective_group) or dberror($sql_design_objective_group);
		$number_of_design_objective_groups = mysql_num_rows($res);
	}

	if ($number_of_design_objective_groups > 0)
	{
		$form->add_section("Design Objectives");
		$form->add_comment("Please indicate your basic needs and requirements to assist out design process. ".
		"\n A Project Manager will ensure that the design is consistent with the ".
		BRAND ," Retail objectives."); 

		$i = 1;
		
		while ($row = mysql_fetch_assoc($res))
		{
			$sql_design_objective_item = "select design_objective_item_id, design_objective_item_name ".
										 "from design_objective_items ".
										 "where design_objective_item_group=" .    $row["design_objective_group_id"] . " ".
										 "order by design_objective_item_priority";

			if ($row["design_objective_group_multiple"] == 0) 
			{
				$value = "";
				foreach ($project_design_objective_item_ids as $key=>$element)
				{
				  if ($element == $row["design_objective_group_name"])
				  {
					  $value = $key;
				  }
				}

				$form->add_list("design_objective_items" . $i, $row["design_objective_group_name"] . "*", $sql_design_objective_item, NOTNULL, $value);
				$design_objectives_listbox_names[] = "design_objective_items" . $i;
			}
			else 
			{
				$values = array();
				foreach ($project_design_objective_item_ids as $key=>$element)
				{
					if ($element == $row["design_objective_group_name"])
					{
						$values[] = $key;
					}
					next($project_design_objective_item_ids);
				}
				$form->add_checklist("design_objective_items" . $i, $row["design_objective_group_name"] . "*", "project_items", $sql_design_objective_item, NOTNULL, $values);
				$design_objectives_checklist_names[] = "design_objective_items" . $i;
			}
			
			$i++;   
		}
	}

	$form->add_section("Capacity Request by Client");
	$form->add_edit("watches_displayed", "Watches Displayed", 0, $project["project_watches_displayed"], TYPE_INT, 20);
	$form->add_edit("watches_stored", "Watches Stored", 0, $project["project_watches_stored"], TYPE_INT, 20);
	$form->add_edit("bijoux_displayed", "Bijoux Displayed", 0, $project["project_bijoux_displayed"], TYPE_INT, 20);
	$form->add_edit("bijoux_stored", "Bijoux Stored", 0, $project["project_bijoux_stored"], TYPE_INT, 20);

	$form->add_section("Preferences and Traffic Checklist");
	$form->add_comment("Please enter the date in the form of dd.mm.yyyy.");
	$form->add_edit("preferred_delivery_date", "Preferred Arrival Date*", NOTNULL, to_system_date($project["order_preferred_delivery_date"]), TYPE_DATE, 20);
	

	$form->add_list("preferred_transportation_arranged", "Transportation arranged by*", $sql_transportation_arranged, NOTNULL, $project["order_preferred_transportation_arranged"]);

	$form->add_list("preferred_transportation_mode", "Transportation mode*", $sql_transportation_modes, NOTNULL, $project["order_preferred_transportation_mode"]);

	//$form->add_radiolist( "packaging_retraction", "Packaging Retraction Desired", array(0 => "no", 1 => "yes"), 0, $project["order_packaging_retraction"]);

	$form->add_comment("Please indicate if there is a special approval needed for delivery into ".
					   "a pedestrian area."); 
	$form->add_radiolist( "pedestrian_mall_approval", "Pedestrian Area Approval Needed",
		array(0 => "no", 1 => "yes"), 0, $project["order_pedestrian_mall_approval"]);
	$form->add_comment("Please indicate if partial delivery is possible or full delivery is required."); 
	$form->add_radiolist( "full_delivery", "Full Delivery",
		array(0 => "no", 1 => "yes"), 0, $project["order_full_delivery"]);
	$form->add_comment("Please indicate any other circumstances/restrictions concerning delivery and traffic."); 
	$form->add_multiline("delivery_comments", "Delivery Comments", 4, 0, $project["order_delivery_comments"]);

	$form->add_section("Insurance");
	$form->add_radiolist("order_insurance", array(1=>"Insurance by " . BRAND . "/Forwarder", 0=>""), array(1=>"covered",0=>"not covered"), VERTICAL, $project["order_insurance"]);


	$form->add_section("Other Information");
	$form->add_comment("Please use only figures to indicate the budget ".
					   "and enter the date in the form of dd.mm.yyyy.");
	$form->add_edit("approximate_budget", "Approximate Budget in " . $currency["symbol"] . "*", NOTNULL, $project["project_approximate_budget"], TYPE_DECIMAL, 20, 2);
	$form->add_edit("planned_opening_date", "Planned Opening Date*", NOTNULL, to_system_Date($project["project_planned_opening_date"]), TYPE_DATE, 20);


	$form->add_section("General Comments");
	$form->add_multiline("comments", "Comments", 4, 0, $project["project_comments"]);
}

$form->add_button("save", "Save Data");



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("product_line"))
{
    delete_design_objective_items(param("pid"));
    project_update_project_product_line(param("pid"), $form->value("product_line"));
    $link = "project_edit_client_data.php?pid=" . param("pid") . "&change_of_product_line=1";
    redirect($link);
}
elseif ($form->button("client_address_id"))
{
	
}
elseif($form->button("billing_address_place_id"))
{
	$sql= "select place_name, province_canton ".
		  "from places " .
		  "left join provinces on province_id = place_province " .
		  "where place_id = " . dbquote($form->value("billing_address_place_id"));

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$form->value('billing_address_place', $row['place_name']);
		$form->value("billing_address_province_name", $row["province_canton"]);
	}
}
elseif($form->button("delivery_address_place_id"))
{
	$sql= "select place_name, province_canton ".
		  "from places " .
		  "left join provinces on province_id = place_province " .
		  "where place_id = " . dbquote($form->value("delivery_address_place_id"));

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$form->value('delivery_address_place', $row['place_name']);
		$form->value("delivery_address_province_name", $row["province_canton"]);
	}
}
elseif($form->button("invoice_recipient"))
{
	if ($form->value("invoice_recipient"))
    {
        $invoice_address = get_address($form->value("invoice_recipient"));

        
		$form->value("billing_address_company", $invoice_address["company"]);
		$form->value("billing_address_company2",  $invoice_address["company2"]);
		$form->value("billing_address_address",  $invoice_address["address"]);
		$form->value("billing_address_address2",  $invoice_address["address2"]);
		$form->value("billing_address_zip",  $invoice_address["zip"]);
		$form->value("billing_address_place",  $invoice_address["place"]);
		$form->value("billing_address_place_id",  $invoice_address["place_id"]);

		$form->value("billing_address_province_name",  $invoice_address["province_name"]);

		$form->value("billing_address_country",  $invoice_address["country"]);
		$form->value("billing_address_phone",  $invoice_address["phone"]);
		$form->value("billing_address_fax",  $invoice_address["fax"]);
		$form->value("billing_address_email",  $invoice_address["email"]);
		$form->value("billing_address_contact",  $invoice_address["contact_name"]);


    }
}
elseif($form->button("furniture_height")) {
	$sql= "select ceiling_height_id, ceiling_height_height ".
		  "from ceiling_heights " . 
		  "where ceiling_height_id = " . dbquote($form->value("furniture_height"));

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$form->value('project_furniture_height_mm', $row['ceiling_height_height']);
	}
	
}
else if ($form->button("posaddress_id"))
{
	$sql = "select * from posaddresses where posaddress_id = " . $form->value("posaddress_id");
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$form->value("shop_address_company", $row["posaddress_name"]);
		$form->value("shop_address_address",  $row["posaddress_address"]);
		$form->value("shop_address_address2",  $row["posaddress_address2"]);
		$form->value("shop_address_zip",  $row["posaddress_zip"]);
		$form->value("shop_address_place",  $row["posaddress_place"]);
		$form->value("shop_address_country",  $row["posaddress_country"]);
		$form->value("shop_address_phone",  $row["posaddress_phone"]);
		$form->value("shop_address_fax",  $row["posaddress_fax"]);
		$form->value("shop_address_email",  $row["posaddress_email"]);
	}
}
else if ($form->button("save"))
{
	// add validation ruels

    if($project["project_projectkind"] != 4 and $project["project_projectkind"] != 5) // take over, lease renewal
	{
		if (!$form->value("location_type") and !$form->value("location_type_other"))
		{
			$form->add_validation("{location_type}", "The location type must not be empty.");
		}
	}

		
	if($form->value("project_projectkind") > 1 and $form->value("posaddress_id") == 0 and $old_projectkind == 1)
	{
		if($form->value("project_projectkind") == 2)
		{
			$form->error("You have changed the project kind from NEW to RENOVATION. You must assign an existing POS Location to the project.");
		}
		elseif($form->value("project_projectkind") == 3)
		{
			$form->error("You have changed the project kind from NEW to TAKE OVER/RENOVATION. You must assign an existing POS Location to the project.");
		}
		elseif($form->value("project_projectkind") == 4)
		{
			$form->error("You have changed the project kind from NEW to TAKE OVER. You must assign an existing POS Location to the project.");
		}
	}
	elseif($form->value("project_projectkind") == 6 and $form->value("project_relocated_posaddress_id") == 0)
	{
		$form->error("You have changed the project kind to relocation. You must assign the POS being relocated.");
	}
    elseif ($form->validate())
    {

		if($project["pipeline"] == 1)
		{
			$sql_u = "update posaddressespipeline 
						set posaddress_client_id = " . dbquote($form->value("client_address_id")) . "
						where posaddress_id = " . $pos_data['posaddress_id'];
			$result = mysql_query($sql_u) or dberror($sql_u);
		}

		if(($old_projectkind == 2 and $form->value("project_projectkind") == 1)
			or ($old_projectkind == 2 and $form->value("project_projectkind") == 6)
			) // project type from renovation to new or from renovation to relocation or from relocation to new
		{
			$old_posaddress_id = $form->value("posaddress_id");

			
			//insert into posaddress pipeline
			$fields = array();
			$values = array();

			$fields[] = "posaddress_client_id";
			$values[] = dbquote($project["order_client_address"]);

			$fields[] = "posaddress_ownertype";
			$values[] = dbquote($project["project_cost_type"]);

			$fields[] = "posaddress_franchisor_id";
			$values[] = dbquote($pos_data["posaddress_franchisor_id"]);

			$fields[] = "posaddress_franchisee_id";
			$values[] = dbquote($project["order_franchisee_address_id"]);

			$fields[] = "posaddress_name";
			$values[] = dbquote($project["order_shop_address_company"]);

			$fields[] = "posaddress_name2";
			$values[] = dbquote($project["order_shop_address_company2"]);

			$fields[] = "posaddress_address";
			$values[] = dbquote($project["order_shop_address_address"]);

			$fields[] = "posaddress_address2";
			$values[] = dbquote($project["order_shop_address_address2"]);

			$fields[] = "posaddress_zip";
			$values[] = dbquote($project["order_shop_address_zip"]);

			$fields[] = "posaddress_place";
			$values[] = dbquote($project["order_shop_address_place"]);

			$fields[] = "posaddress_place_id";
			$values[] = dbquote($pos_data["posaddress_place_id"]);

			$fields[] = "posaddress_country";
			$values[] = dbquote($project["order_shop_address_country"]);
			
			$fields[] = "posaddress_phone";
			$values[] = dbquote($project["order_shop_address_phone"]);

			$fields[] = "posaddress_fax";
			$values[] = dbquote($project["order_shop_address_fax"]);

			$fields[] = "posaddress_email";
			$values[] = dbquote($project["order_shop_address_email"]);

			$fields[] = "posaddress_store_postype";
			$values[] = dbquote($project["project_postype"]);

			$fields[] = "posaddress_store_subclass";
			$values[] = dbquote($project["project_pos_subclass"]);

			$fields[] = "posaddress_store_furniture";
			$values[] = dbquote($project["project_product_line"]);

			$fields[] = "posaddress_store_grosssurface";
			$values[] = dbquote($project["project_cost_gross_sqms"]);

			$fields[] = "posaddress_store_totalsurface";
			$values[] = dbquote($project["project_cost_totalsqms"]);

			$fields[] = "posaddress_store_retailarea";
			$values[] = dbquote($project["project_cost_sqms"]);

			$fields[] = "posaddress_store_backoffice";
			$values[] = dbquote($project["project_cost_backofficesqms"]);

			$fields[] = "posaddress_store_numfloors";
			$values[] = dbquote($project["project_cost_numfloors"]);

			$fields[] = "posaddress_store_floorsurface1";
			$values[] = dbquote($project["project_cost_floorsurface1"]);

			$fields[] = "posaddress_store_floorsurface2";
			$values[] = dbquote($project["project_cost_floorsurface2"]);

			$fields[] = "posaddress_store_floorsurface3";
			$values[] = dbquote($project["project_cost_floorsurface3"]);


			$fields[] = "posaddress_fagagreement_type";
			$values[] = dbquote($project["project_fagagreement_type"]);

			$fields[] = "posaddress_fagrsent";
			$values[] = dbquote($project["project_fagrsent"]);

			$fields[] = "posaddress_fagrsigned";
			$values[] = dbquote($project["project_fagrsigned"]);

			$fields[] = "posaddress_fagrstart";
			$values[] = dbquote($project["project_fagrstart"]);

			$fields[] = "posaddress_fagrend";
			$values[] = dbquote($project["project_fagrend"]);

			$fields[] = "posaddress_fag_comment";
			$values[] = dbquote($project["project_fag_comment"]);


			$fields[] = "posaddress_google_lat";
			$values[] = dbquote($pos_data["posaddress_google_lat"]);
			
			$fields[] = "posaddress_google_long";
			$values[] = dbquote($pos_data["posaddress_google_long"]);

			$fields[] = "posaddress_google_precision";
			$values[] = 1;

			$fields[] = "posaddress_perc_class";
			$values[] = dbquote($pos_data["posaddress_perc_class"]);

			$fields[] = "posaddress_perc_tourist";
			$values[] = dbquote($pos_data["posaddress_perc_tourist"]);

			$fields[] = "posaddress_perc_transport";
			$values[] = dbquote($pos_data["posaddress_perc_transport"]);

			$fields[] = "posaddress_perc_people";
			$values[] = dbquote($pos_data["posaddress_perc_people"]);

			$fields[] = "posaddress_perc_parking";
			$values[] = dbquote($pos_data["posaddress_perc_parking"]);

			$fields[] = "posaddress_perc_visibility1";
			$values[] = dbquote($pos_data["posaddress_perc_visibility1"]);

			$fields[] = "posaddress_perc_visibility2";
			$values[] = dbquote($pos_data["posaddress_perc_visibility2"]);

			$fields[] = "posaddress_neighbour_left";
			$values[] = dbquote($pos_data["posaddress_neighbour_left"]);

			$fields[] = "posaddress_neighbour_right";
			$values[] = dbquote($pos_data["posaddress_neighbour_right"]);

			$fields[] = "posaddress_neighbour_acrleft";
			$values[] = dbquote($pos_data["posaddress_neighbour_acrleft"]);

			$fields[] = "posaddress_neighbour_acrright";
			$values[] = dbquote($pos_data["posaddress_neighbour_acrright"]);

			$fields[] = "posaddress_neighbour_brands";
			$values[] = dbquote($pos_data["posaddress_neighbour_brands"]);

			$fields[] = "posaddress_neighbour_comment";
			$values[] = dbquote($pos_data["posaddress_neighbour_comment"]);

			$fields[] = "user_created";
			$values[] = dbquote(user_login());

			$fields[] = "date_created";
			$values[] = dbquote(date("y-m-d H:i:s"));

			$sql = "insert into posaddressespipeline (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			mysql_query($sql) or dberror($sql);

			$new_posaddress_id = mysql_insert_id();
			
			
			//insert into posorderspipeline

			$sql = "select * from posorders where posorder_order = " . $project["project_order"];
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{

				$old_posorder_id = $row["posorder_id"];
				$fields = array();
				$values = array();

				$fields[] = "posorder_parent_table";
				$values[] = dbquote("posaddressespipeline");

				$fields[] = "posorder_posaddress";
				$values[] = dbquote($new_posaddress_id);

				$fields[] = "posorder_order";
				$values[] = dbquote($row["posorder_order"]);

				$fields[] = "posorder_type";
				$values[] = dbquote($row["posorder_type"]);

				$fields[] = "posorder_ordernumber";
				$values[] = dbquote($row["posorder_ordernumber"]);

				$fields[] = "posorder_year";
				$values[] = dbquote($row["posorder_year"]);

				$fields[] = "posorder_product_line";
				$values[] = dbquote($row["posorder_product_line"]);

				$fields[] = "posorder_product_line_subclass";
				$values[] = dbquote($row["posorder_product_line_subclass"]);

				$fields[] = "posorder_postype";
				$values[] = dbquote($row["posorder_postype"]);

				$fields[] = "posorder_subclass";
				$values[] = dbquote($row["posorder_subclass"]);

				$fields[] = "posorder_project_kind";
				$values[] = dbquote($row["posorder_project_kind"]);

				$fields[] = "posorder_legal_type";
				$values[] = dbquote($row["posorder_legal_type"]);

				$fields[] = "posorder_system_currency";
				$values[] = dbquote($row["posorder_system_currency"]);

				$fields[] = "posorder_client_currency";
				$values[] = dbquote($row["posorder_client_currency"]);

				$fields[] = "posorder_neighbour_left";
				$values[] = dbquote($row["posorder_neighbour_left"]);

				$fields[] = "posorder_neighbour_right";
				$values[] = dbquote($row["posorder_neighbour_right"]);

				$fields[] = "posorder_neighbour_acrleft";
				$values[] = dbquote($row["posorder_neighbour_acrleft"]);

				$fields[] = "posorder_neighbour_acrright";
				$values[] = dbquote($row["posorder_neighbour_acrright"]);

				$fields[] = "posorder_neighbour_brands";
				$values[] = dbquote($row["posorder_neighbour_brands"]);

				$fields[] = "posorder_neighbour_comment";
				$values[] = dbquote($row["posorder_neighbour_comment"]);


				$fields[] = "posorder_currency_symbol";
				$values[] = dbquote($row["posorder_currency_symbol"]);

				$fields[] = "posorder_exchangerate";
				$values[] = dbquote($row["posorder_exchangerate"]);

				$fields[] = "posorder_remark";
				$values[] = dbquote($row["posorder_remark"]);

				$fields[] = "posorder_project_locally_produced";
				$values[] = dbquote($row["posorder_project_locally_produced"]);

				$fields[] = "posorder_project_special_project";
				$values[] = dbquote($row["posorder_project_special_project"]);


				$fields[] = "posorder_furniture_height";
				$values[] = dbquote($row["posorder_furniture_height"]);

				$fields[] = "posorder_furniture_height_mm";
				$values[] = dbquote($row["posorder_furniture_height_mm"]);

				$fields[] = "date_created";
				$values[] = dbquote(date("Y-m-d H:i:s"));

				$fields[] = "date_modified";
				$values[] = dbquote(date("Y-m-d H:i:s"));

				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				$fields[] = "user_modified";
				$values[] = dbquote(user_login());
				
				$sql = "insert into posorderspipeline (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);

				$new_posorder_id = mysql_insert_id();
			
				//insert into posareaspipeline
				$sql_t = "select * from posareas where posarea_posaddress = " . $old_posaddress_id;
				$res_t = mysql_query($sql_t) or dberror($sql_t);
				while ($row_t = mysql_fetch_assoc($res_t))
				{
					$sql_i = "Insert into posareaspipeline (" .
							 "posarea_posaddress, posarea_area, user_created, date_created, user_modified, date_modified " . 
							 ") values (" .
							 $new_posaddress_id . ", " . 
							 $row_t["posarea_area"]  . ", " .
							 dbquote($row_t["user_created"])  . ", " .
							 dbquote($row_t["date_created"])  . ", " .
							 dbquote(user_login())  . ", " .
							 "current_timestamp)";

					$result = mysql_query($sql_i) or dberror($sql_i);
				
				}

				//insert to posleasepipeline
				$sql_t = "select * from posleases where poslease_posaddress = " . $old_posaddress_id . 
					     " and poslease_order = " . $project["project_order"];
				$res_t = mysql_query($sql_t) or dberror($sql_t);
				if ($row_t = mysql_fetch_assoc($res_t))
				{
					$sql_i = "Insert into posleasespipeline (" .
							 "poslease_posaddress, poslease_order, poslease_lease_type, poslease_landlord_name, " . 
							 "poslease_negotiator, poslease_negotiated_conditions, " . 
							 "poslease_estate_agent, poslease_estate_address, poslease_startdate, poslease_enddate, " . 
							 "poslease_extensionoption, poslease_exitoption, poslease_isindexed, " . 
						     "poslease_indexrate, poslease_average_increase, poslease_realestate_fee, " . 
						     "poslease_annual_charges, poslease_other_fees, " . 
					         "poslease_resignation_deadline, poslease_nexttermin_date, poslease_termin_penalty, " . 
							 "poslease_anual_rent, poslease_addcharges, poslease_currency, poslease_salespercent, " . 
						     " poslease_indexclause_in_contract, poslease_termination_time, poslease_mailalert, " .
						     "poslease_handoverdate,  poslease_firstrentpayed, poslease_freeweeks, " . 
							 "user_created, date_created " . 
							 ") values (" .
							 $new_posaddress_id . ", " .
						     $project["project_order"] . ", " . 
							 dbquote($row_t["poslease_lease_type"])  . ", " .
							 dbquote($row_t["poslease_landlord_name"])  . ", " .
							 dbquote($row_t["poslease_negotiator"])  . ", " .
							 dbquote($row_t["poslease_negotiated_conditions"])  . ", " .
							 dbquote($row_t["poslease_estate_agent"])  . ", " .
							 dbquote($row_t["poslease_estate_address"])  . ", " .
							 dbquote($row_t["poslease_startdate"])  . ", " .
							 dbquote($row_t["poslease_enddate"])  . ", " .
							 dbquote($row_t["poslease_extensionoption"])  . ", " .
							 dbquote($row_t["poslease_exitoption"])  . ", " .
						     dbquote($row_t["poslease_isindexed"])  . ", " .
						     dbquote($row_t["poslease_indexrate"])  . ", " .
						     dbquote($row_t["poslease_average_increase"])  . ", " .
						     dbquote($row_t["poslease_realestate_fee"])  . ", " .
						     dbquote($row_t["poslease_annual_charges"])  . ", " .
						     dbquote($row_t["poslease_other_fees"])  . ", " .
						     dbquote($row_t["poslease_resignation_deadline"])  . ", " .
							 dbquote($row_t["poslease_nexttermin_date"])  . ", " .
							 dbquote($row_t["poslease_termin_penalty"])  . ", " .
							 dbquote($row_t["poslease_anual_rent"])  . ", " .
						     dbquote($row_t["poslease_addcharges"])  . ", " .
							 dbquote($row_t["poslease_currency"])  . ", " .
							 dbquote($row_t["poslease_salespercent"])  . ", " .
							 dbquote($row_t["poslease_indexclause_in_contract"])  . ", " .
						     dbquote($row_t["poslease_termination_time"])  . ", " .
						     dbquote($row_t["poslease_mailalert"])  . ", " .
						     dbquote($row_t["poslease_handoverdate"])  . ", " .
						     dbquote($row_t["poslease_firstrentpayed"])  . ", " .
						     dbquote($row_t["poslease_freeweeks"])  . ", " .
							 dbquote(user_login())  . ", " .
							 "current_timestamp)";

					$result = mysql_query($sql_i) or dberror($sql_i);
				
				}

				//delete from posorders
				$sql_d = "delete from posorders where posorder_posaddress = " . $old_posaddress_id . 
					     " and posorder_order = " . $project["project_order"];
				$result = mysql_query($sql_d) or dberror($sql_d);

				
				//delete from posleases
				$sql_d = "delete from posleases where poslease_posaddress = " . $old_posaddress_id . 
					     " and poslease_order = " . $project["project_order"];
				$result = mysql_query($sql_d) or dberror($sql_d);

			}
		}
		elseif($form->value("project_projectkind") > 1 
			and $old_projectkind == 1 
			and $form->value("project_projectkind") !=6) // project type from new to renovation
		{
			//remove project from pipeline
			$sql = "select * from posorderspipeline where posorder_order = " . param("oid");
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$posorder_posaddress_id = $row["posorder_posaddress"];
				$posorder_id = $row["posorder_id"];

				//transfer posareas
				$sql_d = "delete from posareas where posarea_posaddress = " . $form->value("posaddress_id");
				$result = mysql_query($sql_d) or dberror($sql_d);

				$sql_t = "select * from posareaspipeline where posarea_posaddress = " . $posorder_posaddress_id;
				$res_t = mysql_query($sql_t) or dberror($sql_t);
				while ($row_t = mysql_fetch_assoc($res_t))
				{
					$sql_i = "Insert into posareas (" .
							 "posarea_posaddress, posarea_area, user_created, date_created, user_modified, date_modified " . 
							 ") values (" .
							 $form->value("posaddress_id") . ", " . 
							 $row_t["posarea_area"]  . ", " .
							 dbquote($row_t["user_created"])  . ", " .
							 dbquote($row_t["date_created"])  . ", " .
							 dbquote(user_login())  . ", " .
							 "current_timestamp)";

					$result = mysql_query($sql_i) or dberror($sql_i);
				
				}

				$sql_d = "delete from posareaspipeline where posarea_posaddress = " . $posorder_posaddress_id;
				$result = mysql_query($sql_d) or dberror($sql_d);


				//transfer lease data
				$sql_t = "select * from posleasespipeline where poslease_order = " . $project["project_order"];
				$res_t = mysql_query($sql_t) or dberror($sql_t);
				if ($row_t = mysql_fetch_assoc($res_t))
				{
					$sql_i = "Insert into posleases (" .
							 "poslease_posaddress, poslease_order, poslease_lease_type, poslease_landlord_name, " . 
							 "poslease_negotiator, poslease_negotiated_conditions, " . 
							 "poslease_estate_agent, poslease_estate_address, poslease_startdate, poslease_enddate, " . 
							 "poslease_extensionoption, poslease_exitoption, poslease_isindexed, " .
						     "poslease_indexrate, poslease_average_increase, poslease_realestate_fee, " . 
						     "poslease_annual_charges, poslease_other_fees, " . 
							 "poslease_resignation_deadline, poslease_nexttermin_date, poslease_termin_penalty, " . 
							 "poslease_anual_rent, poslease_addcharges, poslease_currency, poslease_salespercent, " . 
						     " poslease_indexclause_in_contract, poslease_termination_time, poslease_mailalert, " .
						     "poslease_handoverdate,  poslease_firstrentpayed, poslease_freeweeks, " . 
							 "user_created, date_created " . 
							 ") values (" .
							 $form->value("posaddress_id") . ", " . 
						     $project["project_order"] . ", " . 
							 dbquote($row_t["poslease_lease_type"])  . ", " .
							 dbquote($row_t["poslease_landlord_name"])  . ", " .
							 dbquote($row_t["poslease_negotiator"])  . ", " .
							 dbquote($row_t["poslease_negotiated_conditions"])  . ", " .
							 dbquote($row_t["poslease_estate_agent"])  . ", " .
							 dbquote($row_t["poslease_estate_address"])  . ", " .
							 dbquote($row_t["poslease_startdate"])  . ", " .
							 dbquote($row_t["poslease_enddate"])  . ", " .
							 dbquote($row_t["poslease_extensionoption"])  . ", " .
							 dbquote($row_t["poslease_exitoption"])  . ", " .
						     dbquote($row_t["poslease_isindexed"])  . ", " .
						     dbquote($row_t["poslease_indexrate"])  . ", " .
						     dbquote($row_t["poslease_average_increase"])  . ", " .
						     dbquote($row_t["poslease_realestate_fee"])  . ", " .
						     dbquote($row_t["poslease_annual_charges"])  . ", " .
						     dbquote($row_t["poslease_other_fees"])  . ", " .
						     dbquote($row_t["poslease_resignation_deadline"])  . ", " .
							 dbquote($row_t["poslease_nexttermin_date"])  . ", " .
							 dbquote($row_t["poslease_termin_penalty"])  . ", " .
							 dbquote($row_t["poslease_anual_rent"])  . ", " .
						     dbquote($row_t["poslease_addcharges"])  . ", " .
							 dbquote($row_t["poslease_currency"])  . ", " .
							 dbquote($row_t["poslease_salespercent"])  . ", " .
							 dbquote($row_t["poslease_indexclause_in_contract"])  . ", " .
						     dbquote($row_t["poslease_termination_time"])  . ", " .
						     dbquote($row_t["poslease_mailalert"])  . ", " .
						     dbquote($row_t["poslease_handoverdate"])  . ", " .
						     dbquote($row_t["poslease_firstrentpayed"])  . ", " .
						     dbquote($row_t["poslease_freeweeks"])  . ", " .
							 dbquote(user_login())  . ", " .
							 "current_timestamp)";
					
					$result = mysql_query($sql_i) or dberror($sql_i);
					
					
				
				}

							

				$sql_d = "delete from posleasespipeline where poslease_order = " . $project["project_order"];
				$result = mysql_query($sql_d) or dberror($sql_d);

				//create pos order
				$sql_p = "select * from posorderspipeline where posorder_id = " . $posorder_id;
				$res_p = mysql_query($sql_p) or dberror($sql_p);
				if ($row_p = mysql_fetch_assoc($res_p))
				{
				
					$posorder_fields = array();
					$posorder_values = array();
					
					$posorder_fields[] = "posorder_posaddress";
					$posorder_values[] = $form->value("posaddress_id");

					$posorder_fields[] = "posorder_order";
					$posorder_values[] = $form->value("oid");

					$posorder_fields[] = "posorder_type";
					$posorder_values[] = 1;

					$posorder_fields[] = "posorder_ordernumber";
					$posorder_values[] = dbquote($form->value("project_number"));

					$posorder_fields[] = "posorder_system_currency";
					$posorder_values[] = dbquote($row_p["posorder_system_currency"]);

					$posorder_fields[] = "posorder_budget_approved_sc";
					$posorder_values[] = dbquote($row_p["posorder_budget_approved_sc"]);

					$posorder_fields[] = "posorder_real_cost_sc";
					$posorder_values[] = dbquote($row_p["posorder_real_cost_sc"]);

					$posorder_fields[] = "posorder_client_currency";
					$posorder_values[] = dbquote($row_p["posorder_client_currency"]);

					$posorder_fields[] = "posorder_budget_approved_cc";
					$posorder_values[] = dbquote($row_p["posorder_budget_approved_cc"]);

					$posorder_fields[] = "posorder_real_cost_cc";
					$posorder_values[] = dbquote($row_p["posorder_real_cost_cc"]);

					$posorder_fields[] = "posorder_opening_date";
					$posorder_values[] = dbquote($row_p["posorder_opening_date"]);

					$posorder_fields[] = "posorder_closing_date";
					$posorder_values[] = dbquote($row_p["posorder_closing_date"]);

					$posorder_fields[] = "posorder_neighbour_left";
					$posorder_values[] = dbquote($row_p["posorder_neighbour_left"]);

					$posorder_fields[] = "posorder_neighbour_right";
					$posorder_values[] = dbquote($row_p["posorder_neighbour_right"]);

					$posorder_fields[] = "posorder_neighbour_acrleft";
					$posorder_values[] = dbquote($row_p["posorder_neighbour_acrleft"]);

					$posorder_fields[] = "posorder_neighbour_acrright";
					$posorder_values[] = dbquote($row_p["posorder_neighbour_acrright"]);

					$posorder_fields[] = "posorder_neighbour_brands";
					$posorder_values[] = dbquote($row_p["posorder_neighbour_brands"]);

					$posorder_fields[] = "posorder_neighbour_comment";
					$posorder_values[] = dbquote($row_p["posorder_neighbour_comment"]);

					$posorder_fields[] = "posorder_currency_symbol";
					$posorder_values[] = dbquote($row_p["posorder_currency_symbol"]);

					$posorder_fields[] = "posorder_exchangerate";
					$posorder_values[] = dbquote($row_p["posorder_exchangerate"]);

					$posorder_fields[] = "posorder_remark";
					$posorder_values[] = dbquote($row_p["posorder_remark"]);

					$posorder_fields[] = "posorder_project_locally_produced";
					$posorder_values[] = dbquote($row_p["posorder_project_locally_produced"]);

					$posorder_fields[] = "posorder_project_special_project";
					$posorder_values[] = dbquote($row_p["posorder_project_special_project"]);

					$posorder_fields[] = "user_created";
					$posorder_values[] = dbquote($row_p["user_created"]);

					$posorder_fields[] = "date_created";
					$posorder_values[] = dbquote($row_p["date_created"]);

					$posorder_fields[] = "date_modified";
					$posorder_values[] = "current_timestamp";

					$posorder_fields[] = "user_modified";
					$posorder_values[] = dbquote(user_login());

					$sql = "insert into posorders (" . join(", ", $posorder_fields) . ") values (" . join(", ", $posorder_values) . ")";
					$result = mysql_query($sql) or dberror($sql);

					$sql_d = "delete from posorderspipeline where posorder_posaddress = " . $posorder_posaddress_id;
					$result = mysql_query($sql_d) or dberror($sql_d);
				}

				
				//update posaddress
				$posaddress_fields = array();

				$sql_p = "select * from posaddressespipeline where posaddress_id = " . $posorder_posaddress_id;
				$res_p = mysql_query($sql_p) or dberror($sql_p);
				if ($row_p = mysql_fetch_assoc($res_p))
				{

					$value = dbquote($row_p["posaddress_perc_class"]);
					$posaddress_fields[] = "posaddress_perc_class = " . $value;

					$value = dbquote($row_p["posaddress_perc_tourist"]);
					$posaddress_fields[] = "posaddress_perc_tourist = " . $value;

					$value = dbquote($row_p["posaddress_perc_transport"]);
					$posaddress_fields[] = "posaddress_perc_transport = " . $value;

					$value = dbquote($row_p["posaddress_perc_people"]);
					$posaddress_fields[] = "posaddress_perc_people = " . $value;

					$value = dbquote($row_p["posaddress_perc_parking"]);
					$posaddress_fields[] = "posaddress_perc_parking = " . $value;

					$value = dbquote($row_p["posaddress_perc_visibility1"]);
					$posaddress_fields[] = "posaddress_perc_visibility1 = " . $value;

					$value = dbquote($row_p["posaddress_perc_visibility2"]);
					$posaddress_fields[] = "posaddress_perc_visibility2 = " . $value;

					$value = dbquote($row_p["posaddress_google_lat"]);
					$posaddress_fields[] = "posaddress_google_lat = " . $value;

					$value = dbquote($row_p["posaddress_google_long"]);
					$posaddress_fields[] = "posaddress_google_long = " . $value;

					$value = dbquote($row_p["posaddress_google_precision"]);
					$posaddress_fields[] = "posaddress_google_precision = " . $value;

					$value = "current_timestamp";
					$posaddress_fields[] = "date_modified = " . $value;

					if (isset($_SESSION["user_login"]))
					{
						$value = dbquote($_SESSION["user_login"]);
						$posaddress_fields[] = "user_modified = " . $value;
					}

					$sql = "update posaddresses set " . join(", ", $posaddress_fields) . " where posaddress_id = " . $form->value("posaddress_id");
					mysql_query($sql) or dberror($sql);

					$sql_d = "delete from posaddressespipeline where posaddress_id = " . $posorder_posaddress_id;
					$result = mysql_query($sql_d) or dberror($sql_d);
				}

				//update shop address in order
				$order_fields = array();

				$value = trim($form->value("shop_address_company")) == "" ? "null" : dbquote($form->value("shop_address_company"));
				$order_fields[] = "order_shop_address_company = " . $value;

				$value = trim($form->value("shop_address_address")) == "" ? "null" : dbquote($form->value("shop_address_address"));
				$order_fields[] = "order_shop_address_address = " . $value;

				$value = trim($form->value("shop_address_address2")) == "" ? "null" : dbquote($form->value("shop_address_address2"));
				$order_fields[] = "order_shop_address_address2 = " . $value;

				$value = trim($form->value("shop_address_zip")) == "" ? "null" : dbquote($form->value("shop_address_zip"));
				$order_fields[] = "order_shop_address_zip = " . $value;

				$value = trim($form->value("shop_address_place")) == "" ? "null" : dbquote($form->value("shop_address_place"));
				$order_fields[] = "order_shop_address_place = " . $value;

				$value = trim($form->value("shop_address_country")) == "" ? "null" : dbquote($form->value("shop_address_country"));
				$order_fields[] = "order_shop_address_country = " . $value;
				
				$value = trim($form->value("shop_address_phone")) == "" ? "null" : dbquote($form->value("shop_address_phone"));
				$order_fields[] = "order_shop_address_phone = " . $value;
				
				$value = trim($form->value("shop_address_fax")) == "" ? "null" : dbquote($form->value("shop_address_fax"));
				$order_fields[] = "order_shop_address_fax = " . $value;
				
				$value = trim($form->value("shop_address_email")) == "" ? "null" : dbquote($form->value("shop_address_email"));
				$order_fields[] = "order_shop_address_email = " . $value;

				$value = "current_timestamp";
				$order_fields[] = "date_modified = " . $value;

				if (isset($_SESSION["user_login"]))
				{
					$value = dbquote($_SESSION["user_login"]);
					$order_fields[] = "user_modified = " . $value;
				}

				$sql = "update orders set " . join(", ", $order_fields) . " where order_id = " . $form->value("oid");
				mysql_query($sql) or dberror($sql);


			}
		}
		elseif($form->value("project_projectkind") > 1 
			and $old_projectkind == 6 
			and $form->value("project_projectkind") ==2) // project type from relocation to renovation
		{
			//remove project from pipeline
			$pos_id = $project["project_relocated_posaddress_id"];
			$sql = "select * from posorderspipeline where posorder_order = " . param("oid");
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$posorder_posaddress_id = $row["posorder_posaddress"];
				$posorder_id = $row["posorder_id"];

				//transfer posareas
				$sql_d = "delete from posareas where posarea_posaddress = " . $form->value("posaddress_id");
				$result = mysql_query($sql_d) or dberror($sql_d);

				$sql_t = "select * from posareaspipeline where posarea_posaddress = " . $posorder_posaddress_id;
				$res_t = mysql_query($sql_t) or dberror($sql_t);
				while ($row_t = mysql_fetch_assoc($res_t))
				{
					$sql_i = "Insert into posareas (" .
							 "posarea_posaddress, posarea_area, user_created, date_created, user_modified, date_modified " . 
							 ") values (" .
							 $pos_id . ", " . 
							 $row_t["posarea_area"]  . ", " .
							 dbquote($row_t["user_created"])  . ", " .
							 dbquote($row_t["date_created"])  . ", " .
							 dbquote(user_login())  . ", " .
							 "current_timestamp)";

					$result = mysql_query($sql_i) or dberror($sql_i);
				
				}

				$sql_d = "delete from posareaspipeline where posarea_posaddress = " . $posorder_posaddress_id;
				$result = mysql_query($sql_d) or dberror($sql_d);


				//transfer lease data
				$sql_t = "select * from posleasespipeline where poslease_order = " . $project["project_order"];
				$res_t = mysql_query($sql_t) or dberror($sql_t);
				if ($row_t = mysql_fetch_assoc($res_t))
				{
					$sql_i = "Insert into posleases (" .
							 "poslease_posaddress, poslease_order, poslease_lease_type, poslease_landlord_name, " . 
							 "poslease_negotiator, poslease_negotiated_conditions, " . 
							 "poslease_estate_agent, poslease_estate_address, poslease_startdate, poslease_enddate, " . 
							 "poslease_extensionoption, poslease_exitoption, poslease_isindexed, " .
							 "poslease_indexrate, poslease_average_increase, poslease_realestate_fee, " . 
							 "poslease_annual_charges, poslease_other_fees, " . 
							 "poslease_resignation_deadline, poslease_nexttermin_date, poslease_termin_penalty, " . 
							 "poslease_anual_rent, poslease_addcharges, poslease_currency, poslease_salespercent, " . 
							 " poslease_indexclause_in_contract, poslease_termination_time, poslease_mailalert, " .
							 "poslease_handoverdate,  poslease_firstrentpayed, poslease_freeweeks, " . 
							 "user_created, date_created " . 
							 ") values (" .
							 $pos_id . ", " . 
							 $project["project_order"] . ", " . 
							 dbquote($row_t["poslease_lease_type"])  . ", " .
							 dbquote($row_t["poslease_landlord_name"])  . ", " .
							 dbquote($row_t["poslease_negotiator"])  . ", " .
							 dbquote($row_t["poslease_negotiated_conditions"])  . ", " .
							 dbquote($row_t["poslease_estate_agent"])  . ", " .
							 dbquote($row_t["poslease_estate_address"])  . ", " .
							 dbquote($row_t["poslease_startdate"])  . ", " .
							 dbquote($row_t["poslease_enddate"])  . ", " .
							 dbquote($row_t["poslease_extensionoption"])  . ", " .
							 dbquote($row_t["poslease_exitoption"])  . ", " .
							 dbquote($row_t["poslease_isindexed"])  . ", " .
							 dbquote($row_t["poslease_indexrate"])  . ", " .
							 dbquote($row_t["poslease_average_increase"])  . ", " .
							 dbquote($row_t["poslease_realestate_fee"])  . ", " .
							 dbquote($row_t["poslease_annual_charges"])  . ", " .
							 dbquote($row_t["poslease_other_fees"])  . ", " .
							 dbquote($row_t["poslease_resignation_deadline"])  . ", " .
							 dbquote($row_t["poslease_nexttermin_date"])  . ", " .
							 dbquote($row_t["poslease_termin_penalty"])  . ", " .
							 dbquote($row_t["poslease_anual_rent"])  . ", " .
							 dbquote($row_t["poslease_addcharges"])  . ", " .
							 dbquote($row_t["poslease_currency"])  . ", " .
							 dbquote($row_t["poslease_salespercent"])  . ", " .
							 dbquote($row_t["poslease_indexclause_in_contract"])  . ", " .
							 dbquote($row_t["poslease_termination_time"])  . ", " .
							 dbquote($row_t["poslease_mailalert"])  . ", " .
							 dbquote($row_t["poslease_handoverdate"])  . ", " .
							 dbquote($row_t["poslease_firstrentpayed"])  . ", " .
							 dbquote($row_t["poslease_freeweeks"])  . ", " .
							 dbquote(user_login())  . ", " .
							 "current_timestamp)";
					
					$result = mysql_query($sql_i) or dberror($sql_i);
					
					
				
				}

							

				$sql_d = "delete from posleasespipeline where poslease_order = " . $project["project_order"];
				$result = mysql_query($sql_d) or dberror($sql_d);

				//create pos order
				$sql_p = "select * from posorderspipeline where posorder_id = " . $posorder_id;
				$res_p = mysql_query($sql_p) or dberror($sql_p);
				if ($row_p = mysql_fetch_assoc($res_p))
				{
				
					$posorder_fields = array();
					$posorder_values = array();
					
					$posorder_fields[] = "posorder_posaddress";
					$posorder_values[] = $pos_id;

					$posorder_fields[] = "posorder_order";
					$posorder_values[] = $form->value("oid");

					$posorder_fields[] = "posorder_type";
					$posorder_values[] = 1;

					$posorder_fields[] = "posorder_ordernumber";
					$posorder_values[] = dbquote($form->value("project_number"));

					$posorder_fields[] = "posorder_system_currency";
					$posorder_values[] = dbquote($row_p["posorder_system_currency"]);

					$posorder_fields[] = "posorder_budget_approved_sc";
					$posorder_values[] = dbquote($row_p["posorder_budget_approved_sc"]);

					$posorder_fields[] = "posorder_real_cost_sc";
					$posorder_values[] = dbquote($row_p["posorder_real_cost_sc"]);

					$posorder_fields[] = "posorder_client_currency";
					$posorder_values[] = dbquote($row_p["posorder_client_currency"]);

					$posorder_fields[] = "posorder_budget_approved_cc";
					$posorder_values[] = dbquote($row_p["posorder_budget_approved_cc"]);

					$posorder_fields[] = "posorder_real_cost_cc";
					$posorder_values[] = dbquote($row_p["posorder_real_cost_cc"]);

					$posorder_fields[] = "posorder_opening_date";
					$posorder_values[] = dbquote($row_p["posorder_opening_date"]);

					$posorder_fields[] = "posorder_closing_date";
					$posorder_values[] = dbquote($row_p["posorder_closing_date"]);

					$posorder_fields[] = "posorder_neighbour_left";
					$posorder_values[] = dbquote($row_p["posorder_neighbour_left"]);

					$posorder_fields[] = "posorder_neighbour_right";
					$posorder_values[] = dbquote($row_p["posorder_neighbour_right"]);

					$posorder_fields[] = "posorder_neighbour_acrleft";
					$posorder_values[] = dbquote($row_p["posorder_neighbour_acrleft"]);

					$posorder_fields[] = "posorder_neighbour_acrright";
					$posorder_values[] = dbquote($row_p["posorder_neighbour_acrright"]);

					$posorder_fields[] = "posorder_neighbour_brands";
					$posorder_values[] = dbquote($row_p["posorder_neighbour_brands"]);

					$posorder_fields[] = "posorder_neighbour_comment";
					$posorder_values[] = dbquote($row_p["posorder_neighbour_comment"]);

					$posorder_fields[] = "posorder_currency_symbol";
					$posorder_values[] = dbquote($row_p["posorder_currency_symbol"]);

					$posorder_fields[] = "posorder_exchangerate";
					$posorder_values[] = dbquote($row_p["posorder_exchangerate"]);

					$posorder_fields[] = "posorder_remark";
					$posorder_values[] = dbquote($row_p["posorder_remark"]);

					$posorder_fields[] = "posorder_project_locally_produced";
					$posorder_values[] = dbquote($row_p["posorder_project_locally_produced"]);

					$posorder_fields[] = "user_created";
					$posorder_values[] = dbquote($row_p["user_created"]);

					$posorder_fields[] = "date_created";
					$posorder_values[] = dbquote($row_p["date_created"]);

					$posorder_fields[] = "date_modified";
					$posorder_values[] = "current_timestamp";

					$posorder_fields[] = "user_modified";
					$posorder_values[] = dbquote(user_login());

					$sql = "insert into posorders (" . join(", ", $posorder_fields) . ") values (" . join(", ", $posorder_values) . ")";
					$result = mysql_query($sql) or dberror($sql);

					$sql_d = "delete from posorderspipeline where posorder_posaddress = " . $posorder_posaddress_id;
					$result = mysql_query($sql_d) or dberror($sql_d);
				}

				
				//update posaddress
				$posaddress_fields = array();

				$sql_p = "select * from posaddressespipeline where posaddress_id = " . dbquote($posorder_posaddress_id);
				$res_p = mysql_query($sql_p) or dberror($sql_p);
				if ($row_p = mysql_fetch_assoc($res_p))
				{

					$value = dbquote($row_p["posaddress_perc_class"]);
					$posaddress_fields[] = "posaddress_perc_class = " . $value;

					$value = dbquote($row_p["posaddress_perc_tourist"]);
					$posaddress_fields[] = "posaddress_perc_tourist = " . $value;

					$value = dbquote($row_p["posaddress_perc_transport"]);
					$posaddress_fields[] = "posaddress_perc_transport = " . $value;

					$value = dbquote($row_p["posaddress_perc_people"]);
					$posaddress_fields[] = "posaddress_perc_people = " . $value;

					$value = dbquote($row_p["posaddress_perc_parking"]);
					$posaddress_fields[] = "posaddress_perc_parking = " . $value;

					$value = dbquote($row_p["posaddress_perc_visibility1"]);
					$posaddress_fields[] = "posaddress_perc_visibility1 = " . $value;

					$value = dbquote($row_p["posaddress_perc_visibility2"]);
					$posaddress_fields[] = "posaddress_perc_visibility2 = " . $value;

					$value = dbquote($row_p["posaddress_google_lat"]);
					$posaddress_fields[] = "posaddress_google_lat = " . $value;

					$value = dbquote($row_p["posaddress_google_long"]);
					$posaddress_fields[] = "posaddress_google_long = " . $value;

					$value = dbquote($row_p["posaddress_google_precision"]);
					$posaddress_fields[] = "posaddress_google_precision = " . $value;

					$value = "current_timestamp";
					$posaddress_fields[] = "date_modified = " . $value;

					if (isset($_SESSION["user_login"]))
					{
						$value = dbquote($_SESSION["user_login"]);
						$posaddress_fields[] = "user_modified = " . $value;
					}

					$sql = "update posaddresses set " . join(", ", $posaddress_fields) . " where posaddress_id = " . dbquote($pos_id);
					mysql_query($sql) or dberror($sql);

					$sql_d = "delete from posaddressespipeline where posaddress_id = " . dbquote($posorder_posaddress_id);
					$result = mysql_query($sql_d) or dberror($sql_d);
				}

				//update shop address in order
				$order_fields = array();

				$value = trim($form->value("shop_address_company")) == "" ? "null" : dbquote($form->value("shop_address_company"));
				$order_fields[] = "order_shop_address_company = " . $value;

				$value = trim($form->value("shop_address_address")) == "" ? "null" : dbquote($form->value("shop_address_address"));
				$order_fields[] = "order_shop_address_address = " . $value;

				$value = trim($form->value("shop_address_address2")) == "" ? "null" : dbquote($form->value("shop_address_address2"));
				$order_fields[] = "order_shop_address_address2 = " . $value;

				$value = trim($form->value("shop_address_zip")) == "" ? "null" : dbquote($form->value("shop_address_zip"));
				$order_fields[] = "order_shop_address_zip = " . $value;

				$value = trim($form->value("shop_address_place")) == "" ? "null" : dbquote($form->value("shop_address_place"));
				$order_fields[] = "order_shop_address_place = " . $value;

				$value = trim($form->value("shop_address_country")) == "" ? "null" : dbquote($form->value("shop_address_country"));
				$order_fields[] = "order_shop_address_country = " . $value;
				
				$value = trim($form->value("shop_address_phone")) == "" ? "null" : dbquote($form->value("shop_address_phone"));
				$order_fields[] = "order_shop_address_phone = " . $value;
				
				$value = trim($form->value("shop_address_fax")) == "" ? "null" : dbquote($form->value("shop_address_fax"));
				$order_fields[] = "order_shop_address_fax = " . $value;
				
				$value = trim($form->value("shop_address_email")) == "" ? "null" : dbquote($form->value("shop_address_email"));
				$order_fields[] = "order_shop_address_email = " . $value;

				$value = "current_timestamp";
				$order_fields[] = "date_modified = " . $value;

				if (isset($_SESSION["user_login"]))
				{
					$value = dbquote($_SESSION["user_login"]);
					$order_fields[] = "user_modified = " . $value;
				}

				$sql = "update orders set " . join(", ", $order_fields) . " where order_id = " . $form->value("oid");
				mysql_query($sql) or dberror($sql);


			}
		}
		
		
		project_update_client_data($form,$design_objectives_listbox_names,$design_objectives_checklist_names);

		
		
		//send email notifications on change of project number
		if($old_project_number != $form->value('project_number')) {

				
				//update table old project numbers
				$sql = "Insert into oldproject_numbers (".
					   "oldproject_number_project_id, oldproject_number_user_id, " . 
					   "oldproject_number_old_number, oldproject_number_new_number, " . 
					   "user_created, date_created) VALUES (" .
					   $project["project_id"] . ', ' .
					   user_id() . ', ' .
					   dbquote($old_project_number) . ', ' .
					   dbquote($form->value('project_number')) . ', ' .
					   dbquote(user_login()) . ', ' .
					   dbquote(date("Y-m-d h:i:s")) . ')';
				$res = mysql_query($sql) or dberror($sql);   
				
				
				$sql = "select user_id, user_email, concat(user_name, ' ', user_firstname) as username ".
					   "from users ".
					   "where (user_id = '" . user_id() . "' " . 
					   "   and user_active = 1)";
				
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				
				$sender_id = $row["user_id"];
				$sender_email = $row["user_email"];
				$sender_name = $row["username"];

				//recipients
				$subject = "Project number was changed - Project " . $old_project_number . '/' . $form->value('project_number') . ", " . $project["order_shop_address_country_name"] . ", " . $project["order_shop_address_company"];
				

				if($old_pos_type != $form->value("project_postype")) {
					
					$sql = "select postype_name from postypes where postype_id = " . $old_pos_type;
					$res = mysql_query($sql) or dberror($sql);
					$row = mysql_fetch_assoc($res);
					$old_pos_type_name = $row["postype_name"];


					$sql = "select postype_name from postypes where postype_id = " . $form->value("project_postype");
					$res = mysql_query($sql) or dberror($sql);
					$row = mysql_fetch_assoc($res);
					$new_pos_type_name = $row["postype_name"];
					
					$bodytext0 = $sender_name . " has changed the project number from " . $old_project_number . " to " . $form->value('project_number') . ". The POS Type has changed from " . $old_pos_type_name . " to " . $new_pos_type_name . ".";
				}
				else
				{
					$bodytext0 = $sender_name . " has changed the project number from " . $old_project_number . " to " . $form->value('project_number') . ".";
				}

				$link ="project_view_client_data.php?pid=" . param("pid");
				$bodytext = $bodytext0 . "\nClick below to have direct access to the project:\n";
				$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";   

				//project manager
				$reciepients = array();
				$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
					   "from users ".
					   "where (user_id = " . dbquote($project['project_retail_coordinator']) . 
					   "   and user_active = 1)";
				
				$res1 = mysql_query($sql) or dberror($sql);
				if ($row1 = mysql_fetch_assoc($res1))
				{
					$reciepients[$row1["user_id"]] = strtolower($row1["user_email"]);
					$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
					$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
				}


				//retail operator
				$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
					   "from users ".
					   "where (user_id = " . dbquote($project['order_retail_operator']) . 
					   "   and user_active = 1)";
				
				$res1 = mysql_query($sql) or dberror($sql);
				if ($row1 = mysql_fetch_assoc($res1))
				{
					$reciepients[$row1["user_id"]] = strtolower($row1["user_email"]);
					$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
					$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
				}


				//design_contractor
				if($project['project_design_contractor'] and $project['order_actual_order_state_code'] < '620') {
					$send_mail = true;
					$project_state_restrictions = get_project_state_restrictions($project['project_design_contractor']);

				
					if($project_state_restrictions['from_state'] or $project_state_restrictions['to_state'])
					{
						if($project_state_restrictions['from_state'] and $project['order_actual_order_state_code'] < $project_state_restrictions['from_state'])
						{
							$send_mail = false;
						}
						if($project_state_restrictions['to_state'] and $project['order_actual_order_state_code'] > $project_state_restrictions['to_state'])
						{
							$send_mail = false;
						}
					}
					

					if($send_mail == true)
					{
						$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
							   "from users ".
							   "where (user_id = " . dbquote($project['project_design_contractor']) . 
							   "   and user_active = 1)";
						
						$res1 = mysql_query($sql) or dberror($sql);
						if ($row1 = mysql_fetch_assoc($res1))
						{
							$reciepients[$row1["user_id"]] = strtolower($row1["user_email"]);
							$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
							$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
						}
					}
				}


				//design_supervisor
				if($project['project_design_supervisor']) {
					$send_mail = true;
					$project_state_restrictions = get_project_state_restrictions($project['project_design_supervisor']);

				
					if($project_state_restrictions['from_state'] or $project_state_restrictions['to_state'])
					{
						if($project_state_restrictions['from_state'] and $project['order_actual_order_state_code'] < $project_state_restrictions['from_state'])
						{
							$send_mail = false;
						}
						if($project_state_restrictions['to_state'] and $project['order_actual_order_state_code'] > $project_state_restrictions['to_state'])
						{
							$send_mail = false;
						}
					}
					

					if($send_mail == true)
					{
						$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
							   "from users ".
							   "where (user_id = " . dbquote($project['project_design_supervisor']) . 
							   "   and user_active = 1)";
						
						$res1 = mysql_query($sql) or dberror($sql);
						if ($row1 = mysql_fetch_assoc($res1))
						{
							$reciepients[$row1["user_id"]] = strtolower($row1["user_email"]);
							$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
							$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
						}
					}
				}
				

				//client
				$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
					   "from users ".
					   "where (user_id = " . dbquote($project['order_user']) . 
					   "   and user_active = 1)";
				
				$res1 = mysql_query($sql) or dberror($sql);
				if ($row1 = mysql_fetch_assoc($res1))
				{
					$reciepients[$row1["user_id"]] = strtolower($row1["user_email"]);
					$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
					$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
				}


				//retail supervising team
				$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
					   "from supervisingteam " . 
					   "left join users on user_id = supervisingteam_user ".
					   "where user_active = 1";
				
				$res1 = mysql_query($sql) or dberror($sql);
				while ($row1 = mysql_fetch_assoc($res1))
				{
					$reciepients[$row1["user_id"]] = strtolower($row1["user_email"]);
					$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
					$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
				}

				
				$result = 0;
				foreach($reciepients as $user_id=>$user_email) {
					
					$mail = new Mail();
					$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
					$mail->set_sender($sender_email, $sender_name);
					$mail->add_text($bodytext);
					$mail->add_recipient($user_email);

					$result = $mail->send();
				}

				if($result == 1)
				{
					foreach($reciepients as $user_id=>$user_email) {
						append_mail($project["project_order"], $user_id, $sender_id, $bodytext0, "", 1);
					}
				}
		}

        $form->message("Your changes have been saved.");
    }
}

/*

else if ($form->button("delivery_address_id"))
{
    // set delivery address

    $form->value("delivery_address_company", "");
    $form->value("delivery_address_company2",  "");
    $form->value("delivery_address_address",  "");
    $form->value("delivery_address_address2",  "");
    $form->value("delivery_address_zip",  "");
    $form->value("delivery_address_place",  "");
    $form->value("delivery_address_country",  0);
    $form->value("delivery_address_phone",  "");
    $form->value("delivery_address_fax",  "");
    $form->value("delivery_address_email",  "");
    $form->value("delivery_address_contact",  "");


    if ($form->value("delivery_address_id"))
    {
       $sql = "select * from order_addresses where order_address_id = " . $form->value("delivery_address_id");
        $res = mysql_query($sql) or dberror($sql);

        if ($row = mysql_fetch_assoc($res))
        {
            $form->value("delivery_address_company", $row["order_address_company"]);
            $form->value("delivery_address_company2",  $row["order_address_company2"]);
            $form->value("delivery_address_address",  $row["order_address_address"]);
            $form->value("delivery_address_address2",  $row["order_address_address2"]);
            $form->value("delivery_address_zip",  $row["order_address_zip"]);
            $form->value("delivery_address_place",  $row["order_address_place"]);
            $form->value("delivery_address_country",  $row["order_address_country"]);
            $form->value("delivery_address_phone",  $row["order_address_phone"]);
            $form->value("delivery_address_fax",  $row["order_address_fax"]);
            $form->value("delivery_address_email",  $row["order_address_email"]);
            $form->value("delivery_address_contact",  $row["order_address_contact"]);
        }
    }   
}

else if ($form->button("project_cost_type"))
{
	if($form->value("project_cost_type") == 2) // Franchisee Store
	{
		$form->value("franchisee_address_company", "");
		$form->value("franchisee_address_company2",  "");
		$form->value("franchisee_address_address",  "");
		$form->value("franchisee_address_address2",  "");
		$form->value("franchisee_address_zip",  "");
		$form->value("franchisee_address_place",  "");
		$form->value("franchisee_address_country",  0);
		$form->value("franchisee_address_phone",  "");
		$form->value("franchisee_address_fax",  "");
		$form->value("franchisee_address_email",  "");
		$form->value("franchisee_address_contact",  "");
	}
}
else if ($form->button("franchisee_address_id"))
{
    
	if($form->value("project_cost_type") == 2) // Franchisee Store
	{
		// set new franchisee address
		$form->value("franchisee_address_company", "");
		$form->value("franchisee_address_company2",  "");
		$form->value("franchisee_address_address",  "");
		$form->value("franchisee_address_address2",  "");
		$form->value("franchisee_address_zip",  "");
		$form->value("franchisee_address_place",  "");
		$form->value("franchisee_address_country",  0);
		$form->value("franchisee_address_phone",  "");
		$form->value("franchisee_address_fax",  "");
		$form->value("franchisee_address_email",  "");
		$form->value("franchisee_address_contact",  "");

		if ($form->value("franchisee_address_id"))
		{
			$sql = "select * from orders where order_id = " . $form->value("franchisee_address_id");
			$res = mysql_query($sql) or dberror($sql);

			if ($row = mysql_fetch_assoc($res))
			{
				$form->value("franchisee_address_company", $row["order_franchisee_address_company"]);
				$form->value("franchisee_address_company2",  $row["order_franchisee_address_company2"]);
				$form->value("franchisee_address_address",  $row["order_franchisee_address_address"]);
				$form->value("franchisee_address_address2",  $row["order_franchisee_address_address2"]);
				$form->value("franchisee_address_zip",  $row["order_franchisee_address_zip"]);
				$form->value("franchisee_address_place",  $row["order_franchisee_address_place"]);
				$form->value("franchisee_address_country",  $row["order_franchisee_address_country"]);
				$form->value("franchisee_address_phone",  $row["order_franchisee_address_phone"]);
				$form->value("franchisee_address_fax",  $row["order_franchisee_address_fax"]);
				$form->value("franchisee_address_email",  $row["order_franchisee_address_email"]);
				$form->value("franchisee_address_contact",  $row["order_franchisee_address_contact"]);
			}
		}
	}
}
elseif ($form->button("billing_address_id"))
{
    // set new billing address

    $form->value("billing_address_company", "");
    $form->value("billing_address_company2",  "");
    $form->value("billing_address_address",  "");
    $form->value("billing_address_address2",  "");
    $form->value("billing_address_zip",  "");
    $form->value("billing_address_place",  "");
    $form->value("billing_address_country",  0);
    $form->value("billing_address_phone",  "");
    $form->value("billing_address_fax",  "");
    $form->value("billing_address_email",  "");
    $form->value("billing_address_contact",  "");

    if ($form->value("billing_address_id"))
    {
        $sql = "select * from orders where order_id = " . $form->value("billing_address_id");
        $res = mysql_query($sql) or dberror($sql);

        if ($row = mysql_fetch_assoc($res))
        {
            $form->value("billing_address_company", $row["order_billing_address_company"]);
            $form->value("billing_address_company2",  $row["order_billing_address_company2"]);
            $form->value("billing_address_address",  $row["order_billing_address_address"]);
            $form->value("billing_address_address2",  $row["order_billing_address_address2"]);
            $form->value("billing_address_zip",  $row["order_billing_address_zip"]);
            $form->value("billing_address_place",  $row["order_billing_address_place"]);
            $form->value("billing_address_country",  $row["order_billing_address_country"]);
            $form->value("billing_address_phone",  $row["order_billing_address_phone"]);
            $form->value("billing_address_fax",  $row["order_billing_address_fax"]);
            $form->value("billing_address_email",  $row["order_billing_address_email"]);
            $form->value("billing_address_contact",  $row["order_billing_address_contact"]);
        }
    }
}
*/


    
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Edit Request");
$form->render();
$page->footer();

?>