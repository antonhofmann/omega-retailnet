<?php
/********************************************************************

    project_edit_construction_data.php

    Edit project's construction data details

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-12-06
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-12-06
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("has_access_to_construction_data");

register_param("pid");


/********************************************************************
    prepare all data needed
*********************************************************************/

// read project and order details
$project = get_project(param("pid"));
$order_state_name = get_actual_order_state_name($project["order_actual_order_state_code"], 1);

// get company's address
$client_address = get_address($project["order_client_address"]);

$old_construction_startdate = $project["project_construction_startdate"];



$tracking_info = array();
$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
	   "concat(user_name, ' ', user_firstname) as user_name " . 
	   "from projecttracking " . 
       "left join users on user_id = projecttracking_user_id " . 
       "where projecttracking_project_id = " . param("pid") . 
	   " and projecttracking_field = 'project_construction_startdate' " . 
	   " order by projecttracking_time";




$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$tracking_info[] = array("projecttracking_oldvalue"=>$row["projecttracking_oldvalue"],
		"projecttracking_newvalue"=>$row["projecttracking_newvalue"],
		"projecttracking_comment"=>$row["projecttracking_comment"],
		"projecttracking_time"=>$row["projecttracking_time"],
		"user_name"=>$row["user_name"]
		);
}


$tracking_info2 = array();
$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
	   "concat(user_name, ' ', user_firstname) as user_name " . 
	   "from projecttracking " . 
       "left join users on user_id = projecttracking_user_id " . 
       "where projecttracking_project_id = " . param("pid") . 
	   " and projecttracking_field = 'project_drawing_submission_date' " . 
	   " order by projecttracking_time";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$tracking_info2[] = array("projecttracking_oldvalue"=>$row["projecttracking_oldvalue"],
		"projecttracking_newvalue"=>$row["projecttracking_newvalue"],
		"projecttracking_comment"=>$row["projecttracking_comment"],
		"projecttracking_time"=>$row["projecttracking_time"],
		"user_name"=>$row["user_name"]
		);
}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

$form->add_section("Project");
$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid", $project["project_order"]);

include('include/project_head_small.php');


$form->add_section("Construction Data");

if(has_access("can_edit_construction_data")) 
{
	if(count($tracking_info2) > 0) {
		$form->add_edit("project_drawing_submission_date", "Date to sumit drawings to retailer", NOTNULL, to_system_date($project["project_drawing_submission_date"]), TYPE_DATE, 20, 0, 1, "changehistory2");
	}
	else
	{
		$form->add_edit("project_drawing_submission_date", "Date to sumit drawings to retailer", NOTNULL, to_system_date($project["project_drawing_submission_date"]), TYPE_DATE, 20);
	}

	if($project["project_drawing_submission_date"] != NULL and $project["project_drawing_submission_date"] != '0000-00-00')
	{
		$form->add_edit("change_comment2", "Reason for changing the submission date*", 0);
	}
	else
	{
		$form->add_hidden("change_comment2");
	}
	$form->add_hidden("old_drawing_submission_date", to_system_date($project["project_drawing_submission_date"]));
	
	if(count($tracking_info) > 0) {
		$form->add_edit("project_construction_startdate", "Construction Starting Date", NOTNULL, to_system_date($project["project_construction_startdate"]), TYPE_DATE, 20, 0, 1, "changehistory");
	}
	else
	{
		$form->add_edit("project_construction_startdate", "Construction Starting Date", NOTNULL, to_system_date($project["project_construction_startdate"]), TYPE_DATE, 20);
	}

	if($project["project_construction_startdate"] != NULL and $project["project_construction_startdate"] != '0000-00-00')
	{
		$form->add_edit("change_comment", "Reason for changing the construction starting date*", 0);
	}
	else
	{
		$form->add_hidden("change_comment");
	}
	$form->add_hidden("old_construction_startdate", to_system_date($project["project_construction_startdate"]));

	$form->add_button("save", "Save Data");
}
else
{
	$form->add_label("project_construction_startdate", "Construction Starting Date", 0, to_system_date($project["project_construction_startdate"]));
}




/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("save"))
{
	
	if($form->value("old_construction_startdate") and $form->value("old_construction_startdate") != $form->value("project_construction_startdate"))
	{
		$form->add_validation("{change_comment} != ''", "Please indicate the reason for changing the construction starting date!");
	}
	
	if($form->value("old_drawing_submission_date") and $form->value("old_drawing_submission_date") != $form->value("project_drawing_submission_date"))
	{
		$form->add_validation("{change_comment2} != ''", "Please indicate the reason for changing the drawing submission date!");
	}

	if ($form->validate())
    {
		$sql = "update projects " . 
			   "set project_construction_startdate = " . dbquote(from_system_date($form->value("project_construction_startdate"))) .
			   ", project_drawing_submission_date = " . dbquote(from_system_date($form->value("project_drawing_submission_date"))) .
			   " where project_id = " . param("pid");

		$result = mysql_query($sql) or dberror($sql);


		if($form->value("old_construction_startdate") != $form->value("project_construction_startdate"))
		{
			
			//project tracking
			$field = "project_construction_startdate";
			$sql = "Insert into projecttracking (" . 
				   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
				   user_id() . ", " . 
				   $project["project_id"] . ", " . 
				   dbquote($field) . ", " . 
				   dbquote(from_system_date($form->value("old_construction_startdate"))) . ", " . 
				   dbquote(from_system_date($form->value("project_construction_startdate"))) . ", " . 
				   dbquote($form->value("change_comment")) . ", " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
				   
			$result = mysql_query($sql) or dberror($sql);
		}


		if($form->value("old_drawing_submission_date") != $form->value("project_drawing_submission_date"))
		{
			
			//project tracking
			$field = "project_drawing_submission_date";
			$sql = "Insert into projecttracking (" . 
				   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
				   user_id() . ", " . 
				   $project["project_id"] . ", " . 
				   dbquote($field) . ", " . 
				   dbquote(from_system_date($form->value("old_drawing_submission_date"))) . ", " . 
				   dbquote(from_system_date($form->value("project_drawing_submission_date"))) . ", " . 
				   dbquote($form->value("change_comment2")) . ", " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
				   
			$result = mysql_query($sql) or dberror($sql);
		}
	
	}

}


    
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Edit Construction Data");
$form->render();


?>


<div id="changehistory" style="display:none;">
    <strong>Changes of the construction starting date</strong>
	<table class="table_tracking">
	<tr>
	<td class="label">User</td>
	<td class="label">Time</td>
	<td class="label">Old Value</td>
	<td class="label">New Value</td>
	<td class="label">Comment</td>
	</tr>

	<?php
		foreach($tracking_info as $key=>$values)
		{
			echo '<tr class="tr_tracking"><td class="td_tracking_nobr">' . $values['user_name'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_time'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_oldvalue'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_newvalue'] . '</td>';
			echo '<td class="td_tracking">' . $values['projecttracking_comment'] . '</td></tr>';
		}
	?>
	
	</table>
</div> 

<div id="changehistory2" style="display:none;">
    <strong>Changes of the drawing submission date</strong>
	<table class="table_tracking">
	<tr>
	<td class="label">User</td>
	<td class="label">Time</td>
	<td class="label">Old Value</td>
	<td class="label">New Value</td>
	<td class="label">Comment</td>
	</tr>

	<?php
		foreach($tracking_info2 as $key=>$values)
		{
			echo '<tr class="tr_tracking"><td class="td_tracking_nobr">' . $values['user_name'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_time'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_oldvalue'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_newvalue'] . '</td>';
			echo '<td class="td_tracking">' . $values['projecttracking_comment'] . '</td></tr>';
		}
	?>
	
	</table>
</div> 


<?php



$page->footer();

?>