<?php
/********************************************************************

    project_edit_retail_data.php

    Edit Retail Assignements.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-08
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-02-04
    Version:        1.0.5

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "include/order_functions.php";
require_once "include/order_state_constants.php";

check_access("can_edit_retail_data");

register_param("oid");
set_referer("order_confirm_to_client.php");
set_referer("order_new.php");

/********************************************************************
    prepare all data needed
*********************************************************************/

// read project and order details
$order = get_order(param('oid'));

// get Action parameter
$action_parameter = get_action_parameter(RETAIL_OPERATOR_ASSIGNED, 2);

// get company's address
$client_address = get_address($order["order_client_address"]);


// create sql for the retail_operator listbox
$sql_retail_operators = "select user_id, concat(user_name, ' ', user_firstname) as user_fullname ".
                        "from users ".
                        "left join user_roles on user_id = user_role_user ".
                        "where user_role_role = 2 and user_active = 1 " .
						 "order by user_name, user_firstname";

// create sql for the design contractor listbox
$sql_contractors = "select address_id, address_company ".
                   "from addresses ".
                   "where address_type = 5 and address_active = 1 ".
                   "order by address_company";

// create sql for the design supervisor listbox
$sql_supervisors = "select address_id, address_company ".
                   "from addresses ".
                   "where address_active = 1 and (address_type = 6 or  address_type = 4) ".
                   "order by address_company";

// create sql for the client's contact listbox
$sql_address_user = "select user_id, concat(address_company, ', ', user_name, ' ', user_firstname) ".
                    "from users ".
                    "left join addresses on address_id = " . $order["order_client_address"] . " " .
                    "where user_active = 1 and user_address = ". $order["order_client_address"] . " ".
                    "order by address_company, user_name, user_firstname";


/********************************************************************
    build form
*********************************************************************/
$form = new Form("orders", "order");

$form->add_section("Order");
$form->add_hidden("oid", param('oid'));

require_once "include/order_head_small.php";

$form->add_section("Retail Operations");
$form->add_list("retail_operator", "Retail Operator", $sql_retail_operators, NOTNULL, $order["order_retail_operator"]);

// Confirmation of delivery
$form->add_section("Confirmation of Delivery by");
$form->add_list("order_delivery_confirmation_by", "Person", $sql_address_user, 0, $order["order_delivery_confirmation_by"]);

$form->add_button("save", "Save Data");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("save"))
{
    if ($form->validate())
    {
        order_update_retail_data($form);
        $form->message("Your changes have been saved.");

        if ($order["order_actual_order_state_code"] > RETAIL_OPERATOR_ASSIGNED_P)
        {
            $do_update = 0;
        }
        else
        {
            $do_update = 1;
        }

        
        // append record to table actual_order_states and append task
        if ($form->value("retail_operator"))
        {
            append_order_state(param('oid'), RETAIL_OPERATOR_ASSIGNED, 2, $do_update);
            $messagetext = get_string("order_" . RETAIL_OPERATOR_ASSIGNED);
            $date = date("Y-m-d");
            $link = "order_task_center.php?oid=" . param('oid');
            if ($action_parameter["append_task"] == 1)
            {
                append_task(param('oid'), $form->value("retail_operator"), $messagetext, $link, $date, user_id());
            }
        }
    }   
}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("Edit Retail Data");
$form->render();
$page->footer();

?>