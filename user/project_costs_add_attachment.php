<?php
/********************************************************************

    project_costs_add_attachment.php

    add an attachment to a project

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-02-17
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-02-17
    Version:        1.0.1

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

register_param("pid");

check_access("can_add_attachments_in_projects");

/********************************************************************
    prepare all data needed
*********************************************************************/





// read project and order details
$project = get_project(param("pid"));

//get notification reciepients for attachments of the type layout
$notification_reciepients = array();

$sql = "select * from postype_notifications " .
       "where postype_notification_email4 is not null " .
       "   and postype_notification_postype = " . $project["project_postype"] . 
	   "   and postype_notification_prodcut_line = " . $project["project_product_line"];

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
    $notification_reciepients[] =$row["postype_notification_email4"];
}

// get company's address
$client_address = get_address($project["order_client_address"]);

// buld sql for attachment categories
$sql_attachment_categories = "select order_file_category_id, order_file_category_name ".
                             "from order_file_categories ".
                             "where order_file_category_type = 2 ".
                             "order by order_file_category_priority";

// buld sql for file types
$sql_file_types = "select file_type_id, file_type_name ".
                  "from file_types ".
                  "order by file_type_name";


// get addresses involved in the project
$companies = get_involved_companies($project["project_order"], 0);

// checkbox names
$check_box_names = array();


$former_attachment = array();
$supp01 = 0;
$supp02 = 0;
if(array_key_exists("aid", $_GET) and $_GET["aid"] > 0)
{
	
	$sql = "select * " . 
		   "from order_files ". 
		   " where order_file_id = " . $_GET["aid"];

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$former_attachment = $row;

		$allowed_addresses = array();
		$sql = "select * from order_file_addresses " .
			   "where order_file_address_file = " . $_GET["aid"];

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$allowed_addresses[] = $row["order_file_address_address"];
		}
		$former_attachment["allowed_addresses"] = $allowed_addresses;


	}
}


/********************************************************************
    Create Form
*********************************************************************/ 
$form = new Form("order_files", "file");

$form->add_section("Project");
$form->add_hidden("pid", param("pid"));
$form->add_hidden("order_file_order", $project["project_order"]);
$form->add_hidden("order_file_owner", user_id());

require_once "include/project_head_small.php";

$form->add_section("Attachment");
$form->add_hidden("order_file_category", 15);


$form->add_edit("order_file_title", "Title*", NOTNULL, "", TYPE_CHAR);
$form->add_multiline("order_file_description", "Description*", 4);


if (has_access("can_set_attachment_accessibility_in_projects"))
{
    $form->add_section("Accessibility");
    $form->add_comment("Please indicate who is allowed to view this attachment.");


    $num_checkboxes=1;
    foreach ($companies as $key=>$value_array)
    {
        if(!array_key_exists('role', $value_array)) {
			$value_array["role"] = "";
		}
		
		if(count($former_attachment) > 0 and in_array($value_array["id"], $former_attachment["allowed_addresses"]))
		{
			$form->add_checkbox("A" . $num_checkboxes, $value_array["name"], 1, 0, $value_array["role"]);
		}
		else
		{
			$form->add_checkbox("A" . $num_checkboxes, $value_array["name"], 0, 0, $value_array["role"]);
		}
        $check_box_names["A" . $num_checkboxes] = $value_array["user"];
        $num_checkboxes++;
    }
 
}


$order_number = $project["order_number"];


$form->add_section("File");
if(count($former_attachment) > 0)
{
	$form->add_list("order_file_type", "File Type*", $sql_file_types, NOTNULL, $former_attachment["order_file_type"]);
}
else
{
	$form->add_list("order_file_type", "File Type*", $sql_file_types, NOTNULL);
}
$form->add_upload("order_file_path", "File", "/files/orders/$order_number", NOTNULL);


$form->add_section("CC Recipients");
$form->add_modal_selector("ccmails", "Selected Recipients", 8);


$form->add_section("Upload Multiple Attachments");
$form->add_checkbox("save_and_addnew", "", 0, 0, "Add another attachment after saving");

$form->add_button(FORM_BUTTON_SAVE, "Save Attachment");
$form->add_button(FORM_BUTTON_BACK, "Back");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button(FORM_BUTTON_SAVE))
{

    $attachment_id = mysql_insert_id();

    // check if a recipient was selected
    if (has_access("can_set_attachment_accessibility_in_projects"))
    {
        $no_recipient = 1;
    }
    else
    {
        $no_recipient = 0;
    }


    foreach ($form->items as $item)
    {
        if ($item["type"] == "checkbox" and $item["value"] == "1")
        {
            $no_recipient = 0;
        }
    }
	
	$invalid_file_extension = false;
	
	if($form->value("order_file_category") == 11)
	{
		$ext = pathinfo($form->value("order_file_path"), PATHINFO_EXTENSION);
		
		if($ext != 'jpg' and $ext != 'JPG')
		{
			$invalid_file_extension = true;
		}
	}


    if ($form->validate() and $no_recipient == 0 and $invalid_file_extension == false)
    {

		save_attachment_accessibility_info($form,  $check_box_names);


        // send email notifocation to the retail staff
        $order_id = $project["project_order"];

        $sql = "select order_id, order_number, " .
               "order_shop_address_company, order_shop_address_place, " .
               "project_retail_coordinator, country_name, ".
               "users.user_email as recepient1, users.user_address as address_id1, " .
               "users2.user_email as recepient2, users2.user_address as address_id2, " .
               "users1.user_email as sender, concat(users1.user_name, ' ', users1.user_firstname) as user_fullname, ".
			   "users.user_id as user_id1,  " .
               "users2.user_id as user_id2  " .
               "from orders ".
               "left join projects on project_order = " . $order_id . " " .
               "left join countries on country_id = order_shop_address_country " .
               "left join users on order_retail_operator = users.user_id ".
               "left join users as users1 on " . user_id() . "= users1.user_id ".
               "left join users as users2 on project_retail_coordinator = users2.user_id ".
               "where order_id = " . $order_id;

        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
			$subject = MAIL_SUBJECT_PREFIX . ": New attachment was added - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " .  $row["order_shop_address_company"];

            $sender_email = $row["sender"];
            $sender_name =  $row["user_fullname"];

			$mail = new PHPMailer();
			$mail->Subject = $subject;

			$mail->SetFrom($sender_email, $sender_name);
			$mail->AddReplyTo($sender_email, $sender_name);


            $bodytext0 = "A new attachment has been added by " . $sender_name . ": ";

			$bodytext1 = "A new attachment has been added by " . $sender_name . " for: ";
		
			$reciepients = array();
			$reciepients_cc = array();
			$reciepients_dp = array();
			$reciepient_ids = array();

			if($row["recepient1"])
			{
				$reciepients[strtolower($row["recepient1"])] = strtolower($row["recepient1"]);
				$reciepient_ids[$row["user_id1"]] =$row["user_id1"];
			
			}
			if($row["recepient2"])
			{
				$reciepients[strtolower($row["recepient2"])] = strtolower($row["recepient2"]);
				$reciepient_ids[$row["user_id2"]] =$row["user_id2"];
			}
			


            foreach ($check_box_names as $key=>$value)
            {
                if ($form->value($key))
                {
					foreach($companies as $key1=>$company)
					{
						
						if($value == $company["user"])
						{
							$sql = "select user_email, user_email_cc, user_email_deputy ".
								   "from users ".
								   "where (user_id = " . $company["user"] . 
								   "   and user_active = 1)";
							
							$res1 = mysql_query($sql) or dberror($sql);
							if ($row1 = mysql_fetch_assoc($res1))
							{
								$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
								$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
								$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);

								$reciepient_ids[$company["user"]] = $company["user"];
							}
						}
					}
                }
            }


			//get all ccmails
			$ccmails =  explode(' ', preg_replace("'\r?\n'"," ",$form->value('ccmails'))); 
			foreach($ccmails as $ccmail) {
				if(is_email_address($ccmail)) {
					
					$reciepients_cc[strtolower($ccmail)] = strtolower($ccmail);
				}
			
			}


			
			foreach($reciepients as $key=>$email)
			{
				$bodytext1 = $bodytext1 . "\n" . $email;
				$mail->AddAddress($email);
			}


			foreach($reciepients_cc as $key=>$email)
			{
				$bodytext1 = $bodytext1 . "\n" . $email;
				$mail->AddCC($reciepients_cc[$email]);
			}

			foreach($reciepients_dp as $key=>$email)
			{
				$bodytext1 = $bodytext1 . "\n" . $email;
				$mail->AddCC($reciepients_dp[$email]);
			}

            //add notification_reciepients for ne attachment of type layout
            if($form->value("order_file_category") == 8)
            {
                foreach($notification_reciepients as $key=>$value)
                {
					$mail->AddAddress($value);
					$bodytext1 = $bodytext1 . "\n" . $value;
                }
            }
            
            $bodytext0.= "\n\n<--\n";
            $bodytext0.= $form->value("order_file_title") . "\n";
            
			/*
			if($form->value("order_file_description"))
            {
                $bodytext0.= $form->value("order_file_description") . "\n";
            }
			$bodytext0.= "download the file here:\n" . APPLICATION_URL . $form->value("order_file_path");
            $bodytext0.= "\n-->";
			*/

            
			
			$link ="project_costs_attachment.php?pid=" . param("pid") . "&id=" .  $attachment_id;
			$bodytext = $bodytext0 . "\n\nclick below to have direct access to the project:\n";
			$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";
			
			$mail->Body = $bodytext;
            $mail->Send();


			$bodytext1.= "\n\n<--\n";
            $bodytext1.= $form->value("order_file_title") . "\n";
            $bodytext1.= "-->";

			
			$bodytext = $bodytext1 . "\n\nclick below to have direct access to the project:\n";
			$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";  

			foreach($reciepient_ids as $key=>$id)
			{
	            append_mail($project["project_order"], $id , user_id(), $bodytext, "", 1);
			}
        }

		if($form->value("save_and_addnew") == 1)
		{
			$link = "project_costs_add_attachment.php?pid=" . param("pid") . "&aid=" . id();
			redirect($link);
		}
		else
		{
			$link = "project_costs_attachments.php?pid=" . param("pid");
			redirect($link);
		}
    }
    else
    {
        if($invalid_file_extension == true)
		{
			$form->error("Please upload only files of the type 'jpg'.");
		}
		elseif($no_recipient == 0) {
			$form->error("Please fill in all required fields.");
		}
		else
		{
			$form->error("Please fill in all required fields and select a least one person to have access to the attachment.");
		}
    }
}


$page = new Page("projects");


require "include/project_page_actions.php";


$page->header();
$page->title("Project Costs - Add an Attachment");

require_once("include/costsheet_tabs.php");

$form->render();

?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#ccmails_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/shared/select_mail_recipients.php'
    });
    return false;
  });
});
</script>

<?php

$page->footer();


?>