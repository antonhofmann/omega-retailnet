<?php
/********************************************************************
    tasks.php

    Entry page for the tasks section.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-31
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-02-06
    Version:        1.1.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.
*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_view_tasks");
set_referer("task.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
$sql_tasks = "select task_id, task_text, task_url, " .    
             "    concat(dayofmonth(task_due_date), '.', month(task_due_date), '.', right(year(task_due_date),2)), " .
             "    task_done_date, order_number, " . 
             "    concat(users1.user_name, ' ', users1.user_firstname) as user_fullname, ".
             "    concat(users1.user_name, ' ', users1.user_firstname, ', ', order_number, ', ', address_company, ', ', country_name) as group_head, ".
             "    concat(users2.user_name, ' ', users2.user_firstname) as retail_operator_name ".
             "from tasks ".
             "left join orders on order_id = task_order ".
             "left join users as users1 on task_user = users1.user_id " .
             "left join users as users2 on order_retail_operator = users2.user_id " .
             "left join addresses on order_client_address = address_id ".
             "left join countries on address_country = countries.country_id " . 
			 "inner join project_costs on project_cost_order = order_id " ;

/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql_tasks);

$list->set_entity("tasks");
$list->set_group("group_head");
$list->set_order("users1.user_name, order_number, task_due_date desc");


if (!param("all"))
{
    $list->set_filter("task_user=" . user_id());

	if(has_access("has_access_to_retail_only")) {
		$list->set_filter("task_user=" . user_id() . " and project_cost_type in (1) ");
	}

	if(has_access("has_access_to_wholesale")) {
		$list->set_filter("task_user=" . user_id() . " and project_cost_type in (2,6) ");
	}
}



$list->add_column("order_number", "Order Number", "{task_url}", "", "", COLUMN_NO_WRAP);
$list->add_column("retail_operator_name", "Retail Operator", "", "", "", COLUMN_NO_WRAP);
$list->add_column("task_text", "Task Description");
$list->add_column("concat(dayofmonth(task_due_date), '.', month(task_due_date), '.', right(year(task_due_date),2))", "Due Date", "", "", "", COLUMN_NO_WRAP);


if (has_access("has_access_to_all_tasks") and !param("all"))
{
    $list->add_button("show_all_tasks", "Show All Tasks", "", OPTIONAL);
}
if (has_access("has_access_to_all_tasks") and param("all"))
{
    $list->add_button("show_my_tasks", "Show My Tasks", "", OPTIONAL);
}




/********************************************************************
    Populate list and process button clicks
*********************************************************************/ 


$list->process();




if ($list->button("show_all_tasks"))
{
       $link = "tasks.php?all=1";
       redirect ($link);
}
if ($list->button("show_my_tasks"))
{
       $link = "tasks.php";
       redirect ($link);
}


/********************************************************************
    render page
*********************************************************************/


$page = new Page("tasks");


$page->register_action('home', 'Home', "welcome.php");


$page->header();
$page->title("Task List");
$list->render();
$page->footer();


?>