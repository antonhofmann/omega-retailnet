<?php
/********************************************************************

    project_costs_budget.php

    View or edit the costs of a project

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-01-09
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2014, OMEGA SA, All Rights Reserved.

*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/project_cost_functions.php";
require_once "include/save_functions.php";
require_once "../shared/func_eventmails.php";

check_access("can_edit_list_of_materials_in_projects");

if(!param("pid"))
{
	$link = "welcome.php";
	redirect($link);
}

//var_dump($_POST);
//ini_set('max_input_vars', 5000);
//echo "->" . ini_get('max_input_vars');

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));
// get company's address
$client_address = get_address($project["order_client_address"]);

$currency_symbol = get_currency_symbol($project["order_client_currency"]);


/********************************************************************
    check if project cost positions are present and create if not
*********************************************************************/

$sql = "select count(costsheet_id) as num_recs from costsheets " . 
       " where costsheet_project_id = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

if($row["num_recs"] == 0) //make the user select a cost template
{
	$link = "project_costs_overview.php?pid=" . param("pid");
	redirect($link);
}

/********************************************************************
	get budget data from cost sheet
*********************************************************************/
$budget = get_project_budget_totals(param("pid"));

$code_data = array();
$budget_data = array();
$currency_data = array();
$text_data = array();
$comment_data = array();
$company_data = array();
$delete_ids = array();
$edit_links = array();
$list_has_positions = array();
$costsheet_partner_contribution = array();
$partner_ids = array();

$sql = "select costsheet_id, costsheet_pcost_group_id, costsheet_code, costsheet_text, " . 
       "costsheet_budget_amount, costsheet_partner_contribution, costsheet_comment, costsheet_company " . 
	   "from costsheets " . 
	   "where costsheet_project_id = " . param("pid");


$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$code_data[$row["costsheet_id"]] = $row["costsheet_code"];
	$budget_data[$row["costsheet_id"]] = $row["costsheet_budget_amount"];
	$currency_data[$row["costsheet_id"]] = $currency_symbol;
	$text_data[$row["costsheet_id"]] = $row["costsheet_text"];
	$comment_data[$row["costsheet_id"]] = $row["costsheet_comment"];
	$company_data[$row["costsheet_id"]] = $row["costsheet_company"];
	$delete_ids[$row["costsheet_pcost_group_id"]][$row["costsheet_id"]] = "__costsheets_select_to_delete_" . $row["costsheet_id"];

	$partner_ids[$row["costsheet_pcost_group_id"]][$row["costsheet_id"]] = "__costsheets_costsheet_partner_contribution_" . $row["costsheet_id"];

	$tmp = '<a href="javascript:void(0);" onclick="edit_cost_position(' . param("pid") . ', ' . $row["costsheet_id"] . ')"><span class="fa fa-pencil-square-o edit_icon"></span></a>';
	$edit_links[$row["costsheet_id"]] = $tmp;

	$list_has_positions[$row["costsheet_pcost_group_id"]] = true;

	$costsheet_partner_contribution[$row["costsheet_id"]] = $row["costsheet_partner_contribution"];

}


/********************************************************************
	Create Form
*********************************************************************/ 

$form = new Form("projects", "projects");


$form->add_section("Project");
$form->add_hidden("pid", param('pid'));
$form->add_hidden("order_id", $project["project_order"]);


require_once "include/project_head_small.php";


$form->add_section("Currency and Exchange Rate");


//$form->add_label("currency", "Currency", 0, $currency_symbol);
//$form->add_label("order_client_exchange_rate", "Exchange Rate", 0, $project["order_client_exchange_rate"]);


$form->add_list("order_client_currency", "Currency",
"select currency_id, concat(currency_symbol, ': ', currency_name) as currency from currencies order by currency_symbol", SUBMIT | NOTNULL, $project["order_client_currency"]);
$form->add_edit("order_client_exchange_rate", "Exchange Rate", NOTNULL, $project["order_client_exchange_rate"], TYPE_DECIMAL, 10, 6);


$form->add_section("Business Partner Contribution");
$form->add_comment("Fill in only if the business partner's contribution is a global contribution over all cost positions. <br />Otherwise add the contribution in percent for each single cost position below.");
$form->add_edit("project_share_other", "Business Partner Contribution in Percent", 0, $project["project_share_other"], TYPE_DECIMAL, 6,2);


$form->add_section("Budget State");

if(has_access("can_unfreeze_budget"))
{
	$form->add_checkbox("order_budget_is_locked", "Budget is locked", $project["order_budget_is_locked"], 0, "State");
}
else
{
	$form->add_hidden("order_budget_is_locked");
}

if(has_access("can_unfreeze_budget"))
{
	if($project["order_budget_unfreezed_by"] > 0) {
		$form->add_label("unfreezed_date", "Date last unfreeze", 0, to_system_date($project["order_budget_unfreezed_date"]));
		$form->add_lookup("unfreezed_by", "Unfreezed by", "users", "concat(user_name, ' ', user_firstname)", 0, $project["order_budget_unfreezed_by"]);
	}
	else
	{
		$form->add_label("unfreezed_date", "Date last unfreeze", 0);
		$form->add_label("unfreezed_by", "Unfreezed by", 0);
	}

	if($project["order_budget_is_locked"] == 0)
	{
		$form->add_button("freeze", "Freeze Budget");
	}
	
	$form->add_button("unfreeze", "Unfreeze Budget");

	if($project["order_budget_is_locked"] == 1 and has_access("can_unfreeze_budget"))
	{
		$form->add_button("unlock", "Unlock Budget");
	}
}


$link = "javascript:popup('/user/project_costs_budget_pdf.php?pid=" . param("pid") . "', 800, 600);";
$form->add_button("print", "Print Budget", $link);

if($project["order_budget_is_locked"] == 0)
{
	$form->add_button("save", "Save");
}




if($project["order_budget_is_locked"] == 1)
{
	$form->error("Budget is locked, no more changes can be made!");
}

$form->populate();
$form->process();


if($form->button("order_client_currency"))
{
	$sql = "select currency_exchange_rate " . 
		   "from currencies " . 
		   "where currency_id = " . $form->value("order_client_currency");

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$form->value("order_client_exchange_rate", $row["currency_exchange_rate"]);
	}

}
elseif($form->button("save"))
{
	
	$sql = "update orders set " . 
		 "order_client_currency = " . dbquote($form->value("order_client_currency")) . ", " . 
		 "order_client_exchange_rate = " . dbquote($form->value("order_client_exchange_rate")) . ", " .
		 "date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", " . 
		 "user_modified = " . dbquote(user_login()) . 
		 " where order_id = " . dbquote($project["project_order"]);


	$result = mysql_query($sql) or dberror($sql);


	//update business_partner_shares
	$sql = "select sum(costsheet_budget_amount) as budget_total
			from costsheets
			where costsheet_project_id = " . dbquote(param("pid"));

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	
	$budget_total = $row["budget_total"];

	$sql = "select sum(costsheet_partner_contribution*costsheet_budget_amount/100) as partner_total
			from costsheets
			where costsheet_partner_contribution > 0 
			   and costsheet_project_id = " . dbquote(param("pid"));

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	
	$partner_total = $row["partner_total"];
	
	$share = 0;
	if($budget_total > 0) {
		$share = round(100*($partner_total/$budget_total), 2);
	}


	if($share > 0) {
		$sql = "update projects set " . 
			 "project_share_other = " . dbquote($share) . ", " .
			 "date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", " . 
			 "user_modified = " . dbquote(user_login()) . 
			 " where project_id = " . dbquote(param("pid"));

		$result = mysql_query($sql) or dberror($sql);

		$sql = "update cer_basicdata set " . 
			 "cer_basicdata_franchsiee_investment_share = " . dbquote($share) . ", " .
			 "date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", " . 
			 "user_modified = " . dbquote(user_login()) . 
			 " where cer_basicdata_version = 0 and cer_basicdata_project = " . dbquote(param("pid"));

		$result = mysql_query($sql) or dberror($sql);
	}
	else {
		$sql = "update projects set " . 
			 "project_share_other = " . dbquote($form->value("project_share_other")) . ", " .
			 "date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", " . 
			 "user_modified = " . dbquote(user_login()) . 
			 " where project_id = " . dbquote(param("pid"));

		$result = mysql_query($sql) or dberror($sql);

		$sql = "update cer_basicdata set " . 
			 "cer_basicdata_franchsiee_investment_share = " . dbquote($form->value("project_share_other")) . ", " .
			 "date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", " . 
			 "user_modified = " . dbquote(user_login()) . 
			 " where cer_basicdata_version = 0 and cer_basicdata_project = " . dbquote(param("pid"));

		$result = mysql_query($sql) or dberror($sql);
	}


	
	if($form->value("order_budget_is_locked")!= $project["order_budget_is_locked"])
	{
		update_budget_state($project["project_order"], $form->value("order_budget_is_locked"));
	}
	
	

	$result = update_bid_positions_from_budget(param("pid"));


	//$link = "project_costs_budget.php?pid=" . param("pid");
	//redirect($link);

}
elseif ($form->button("unfreeze"))
{
    unfreeze_project_budget($project["project_order"]);
	send_project_event_mail(user_id(), 'unfreeze_project_budget', $project);

    $link = "project_costs_budget.php?pid=" . param("pid");
	redirect($link);
}
elseif ($form->button("freze"))
{
    freeze_budget($project["project_order"]);

    $link = "project_costs_budget.php?pid=" . param("pid");
	redirect($link);
}
elseif ($form->button("unlock"))
{
	update_budget_state($project["project_order"], 0);
	$link = "project_costs_budget.php?pid=" . param("pid");
	redirect($link);
}


/********************************************************************
	Compose Cost Sheet
*********************************************************************/ 

//add all cost groups and cost sub groups

$list_names = array();
$add_button_names = array();
$save_button_names = array();
$remove_button_names = array();
$group_ids = array();
$group_titles = array();

$sql = "select DISTINCT costsheet_pcost_group_id, pcost_group_code, pcost_group_name " .
	   "from costsheets " .
	   "left join pcost_groups on pcost_group_id = costsheet_pcost_group_id " . 
	   "order by pcost_group_code";


$sql2 = "select DISTINCT costsheet_pcost_group_id, pcost_group_code, pcost_group_name " .
	   "from costsheets " .
	   "left join pcost_groups on pcost_group_id = costsheet_pcost_group_id " .
	   " where costsheet_project_id = " . param("pid") . " and costsheet_is_in_budget = 1" .
	   " order by pcost_group_code";

$res = mysql_query($sql2) or dberror($sql2);
while ($row = mysql_fetch_assoc($res))
{
	$listname = "list" . $row["pcost_group_code"];
	$add_buttonname = "add_new" . $row["pcost_group_code"];
	$save_buttonname = "save_cost" . $row["pcost_group_code"];
	$remove_buttonname = "remove_cost" . $row["pcost_group_code"];
	$remove_buttonname2 = "remove2_cost" . $row["pcost_group_code"];
	$list_names[] = $listname;
	$add_button_names[] = $add_buttonname;
	$save_button_names[] = $save_buttonname;
	$remove_button_names[] = $remove_buttonname;
	$remove_button_names2[] = $remove_buttonname2;
	$group_ids[] = $row["costsheet_pcost_group_id"];
	$group_titles[] = $row["pcost_group_name"];
	

	$toggler = '<div class="toggler_pointer" id="l' . $row["costsheet_pcost_group_id"] . '_on"><span class="fa fa-minus-square toggler"></span>' .$row["pcost_group_name"] . '</div>';

	$sql = "select costsheet_id, costsheet_pcost_group_id, costsheet_pcost_subgroup_id, " . 
		   "costsheet_code, costsheet_text, costsheet_comment, " .
		   "concat(pcost_subgroup_code, ' ', pcost_subgroup_name) as subgroup  " .
		   "from costsheets " .
		   " left join pcost_subgroups on pcost_subgroup_id = costsheet_pcost_subgroup_id"; 

	$list_filter = "costsheet_project_id = " . param("pid") . " and costsheet_pcost_group_id = " . $row["costsheet_pcost_group_id"] . " and costsheet_is_in_budget = 1";
	
	
	
	//compose list
	$$listname = new ListView($sql, LIST_HAS_HEADER | LIST_HAS_FOOTER);

	$$listname->set_entity("costsheets");
	$$listname->set_order("LENGTH(costsheet_code), COALESCE(costsheet_code,'Z')");
	$$listname->set_group("subgroup");
	$$listname->set_filter($list_filter);
	$$listname->set_title($toggler);

	//$$listname->add_column("costsheet_code", "Code");


	$$listname->add_text_column("edit_links", "", COLUMN_UNDERSTAND_HTML, $edit_links);
	$$listname->add_edit_column("costsheet_code", "Code", 4, 0, $code_data);
	$$listname->add_edit_column("costsheet_text", "Text", 60, 0, $text_data, 'textarea', 3);
	$$listname->add_edit_column("costsheet_company", "Supplier", 18, 0, $company_data, 'inputfield');
	$$listname->add_edit_column("costsheet_budget_amount", "Budget", 12, 0, $budget_data);

	$$listname->add_text_column("currency", "", 0, $currency_data);
	
	
	$$listname->add_edit_column("costsheet_partner_contribution", "% paid by<br />Business Partner", 6, COLUMN_UNDERSTAND_HTML, $costsheet_partner_contribution, 'textarea', 3);
	

	//add_checkbox_column($name, $caption, $flags = 0, $values = array(), $checkbox_caption = "")
	
	$$listname->add_edit_column("costsheet_comment", "Comment", 30, 0, $comment_data, 'textarea', 3);

	$link = "javascript:select_all_delete_positions(" . $row["costsheet_pcost_group_id"]. ")";
	$c2 = '<a href"javascript:void(0);" onclick="' .$link . '"><span class="fa fa-check-square-o checker"></span></a><span id="selector2' . $row["costsheet_pcost_group_id"] . '"></span>';

	$$listname->add_checkbox_column("select_to_delete", $c2, COLUMN_UNDERSTAND_HTML);
	
	if(array_key_exists($row["costsheet_pcost_group_id"], $list_has_positions))
	{
		foreach($budget["subgroup_totals"] as $subgroup=>$budget_sub_group_total)
		{
			$$listname->set_group_footer("costsheet_text", $subgroup , "Subgroup Total");
			$$listname->set_group_footer("costsheet_budget_amount",  $subgroup , number_format($budget_sub_group_total, 2));
		}
		

		$$listname->set_footer("costsheet_code", "Total");
		$$listname->set_footer("costsheet_budget_amount", number_format($budget["group_totals"][$row["costsheet_pcost_group_id"]], 2));
		
	
		if($project["order_budget_is_locked"] == 0)
		{
			$link = "javascript:add_new_cost_position(" . param("pid") .", " . $row["costsheet_pcost_group_id"] .")";
			$$listname->add_button($add_buttonname, "Add More Budget Positions", $link);
			$$listname->add_button($remove_buttonname, "Remove Selected Positions");
			$$listname->add_button($remove_buttonname2, "Remove Empty Positions");
			$$listname->add_button($save_buttonname, "Save List");
		}
	}

	$$listname->populate();
	$$listname->process();
}


foreach($save_button_names as $key=>$buttonname)
{

	if($form->button($buttonname) or $form->button("save"))
	{
		$list = "";
		foreach($list_names as $key=>$listname)
		{
			$list = $listname;
		}

		foreach ($$list->values("costsheet_code") as $key=>$value)
		{
			$fields = array();

			$fields[] = "costsheet_code = " . dbquote($value);

			$sql = "update costsheets set " . join(", ", $fields) . " where costsheet_id = " . $key;
			mysql_query($sql) or dberror($sql);
			
		}

		foreach ($$list->values("costsheet_text") as $key=>$value)
		{
			$fields = array();

			$fields[] = "costsheet_text = " . dbquote($value);

			$sql = "update costsheets set " . join(", ", $fields) . " where costsheet_id = " . $key;
			mysql_query($sql) or dberror($sql);
			
		}

		foreach ($$list->values("costsheet_budget_amount") as $key=>$value)
		{
			$value = trim(str_replace("'", "", $value));
			if(is_numeric($value))
			{
				$fields = array();

				$fields[] = "costsheet_budget_amount = " . dbquote($value);

				$sql = "update costsheets set " . join(", ", $fields) . " where costsheet_id = " . $key;
				mysql_query($sql) or dberror($sql);
			}
			
		}

		foreach ($$list->values("costsheet_comment") as $key=>$value)
		{
			$fields = array();

			$fields[] = "costsheet_comment = " . dbquote($value);
			$fields[] = "date_modified = " . dbquote(date("Y-m-d H:i:s"));
			$fields[] = "user_modified = " . dbquote(user_login());

			$sql = "update costsheets set " . join(", ", $fields) . " where costsheet_id = " . $key;
			mysql_query($sql) or dberror($sql);
			
		}


		foreach ($$list->values("costsheet_company") as $key=>$value)
		{
			$fields = array();

			$fields[] = "costsheet_company = " . dbquote($value);

			$sql = "update costsheets set " . join(", ", $fields) . " where costsheet_id = " . $key;
			mysql_query($sql) or dberror($sql);
			
		}


		
		foreach ($$list->values("costsheet_partner_contribution") as $key=>$value)
		{
			$fields = array();

			$fields[] = "costsheet_partner_contribution = " . dbquote($value);

			$sql = "update costsheets set " . join(", ", $fields) . " where costsheet_id = " . $key;
			mysql_query($sql) or dberror($sql);
			
		}


		$result = update_bid_positions_from_budget(param("pid"));


		//update business_partner_shares
		$sql = "select sum(costsheet_budget_amount) as budget_total
		        from costsheets
		        where costsheet_project_id = " . dbquote(param("pid"));

		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);
		
		$budget_total = $row["budget_total"];

		$sql = "select sum(costsheet_partner_contribution*costsheet_budget_amount/100) as partner_total
		        from costsheets
		        where costsheet_partner_contribution > 0 
				   and costsheet_project_id = " . dbquote(param("pid"));

		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);
		
		$partner_total = $row["partner_total"];
		
		$share = 0;
		if($budget_total > 0) {
			$share = round(100*($partner_total/$budget_total), 2);
		}


		if($share > 0) {
			$sql = "update projects set " . 
				 "project_share_other = " . dbquote($share) . ", " .
				 "date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", " . 
				 "user_modified = " . dbquote(user_login()) . 
				 " where project_id = " . dbquote(param("pid"));

			$result = mysql_query($sql) or dberror($sql);

			$sql = "update cer_basicdata set " . 
				 "cer_basicdata_franchsiee_investment_share = " . dbquote($share) . ", " .
				 "date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", " . 
				 "user_modified = " . dbquote(user_login()) . 
				 " where cer_basicdata_version = 0 and cer_basicdata_project = " . dbquote(param("pid"));

			$result = mysql_query($sql) or dberror($sql);
		}
		

		//update CER Investments
		$order_currency = get_order_currency($project["project_order"]);
		$order_currency['exchange_rate'] = $form->value("order_client_exchange_rate");
		$budget = get_project_budget_totals(param("pid"), $order_currency);
		$result = update_investments_from_project_cost_sheet(param("pid"), $budget, 0);

		$link = "project_costs_budget.php?pid=" . param("pid");
		redirect($link);
	}
}


foreach($remove_button_names as $key=>$buttonname)
{
	if($form->button($buttonname))
	{
		$list = "";
		foreach($list_names as $key=>$listname)
		{
			$list = $listname;
		}


		
		foreach ($$list->values("select_to_delete") as $key=>$value)
		{
			if($value)
			{
				
				$sql = "update costsheet_bid_positions " . 
					   "set costsheet_bid_position_costsheet_id = 0, " . 
					   "costsheet_bid_position_is_in_budget = 0 " . 
					   "where costsheet_bid_position_costsheet_id = " . $key;
				
				$result = mysql_query($sql) or dberror($sql);


				$sql = "delete from costsheets where costsheet_id = " . $key;
				$result = mysql_query($sql) or dberror($sql);
			}
			
		}
		
		//if all positions were removed
		$sql = "select count(costsheet_id) as num_recs from costsheets " . 
			   " where costsheet_project_id = " . param("pid");
		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);

		if($row["num_recs"] == 0) //make the user select a cost template
		{
			$link = "project_costs_overview.php?pid=" . param("pid");
			redirect($link);
		}

		$link = "project_costs_budget.php?pid=" . param("pid");
		redirect($link);
	}
}


foreach($remove_button_names2 as $key=>$buttonname)
{
	if($form->button($buttonname))
	{
		
		$sql = "delete from costsheets " . 
			   "where costsheet_project_id = " . param('pid') . 
			   " and (costsheet_text is null or costsheet_text = '') " . 
			   "and costsheet_budget_amount = 0";
		$result = mysql_query($sql) or dberror($sql);
				
		$link = "project_costs_budget.php?pid=" . param("pid");
		redirect($link);
	}
}



$page = new Page("projects");


require "include/project_page_actions.php";


$page->header();
$page->title("Project Costs - Budget");

require_once("include/costsheet_tabs.php");
$form->render();

foreach($list_names as $key=>$listname)
{
	
	if(array_key_exists("costsheet", $_SESSION) and array_key_exists($listname, $_SESSION["costsheet"]))
	{
		if($_SESSION["costsheet"][$listname] == 0)
		{
			echo '<p>&nbsp;</p>';
			$toggler = '<div class="toggler_pointer" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
			echo $toggler;
			
			echo '<div style="display:none;" id="' . $listname . '">';
			$$listname->render();
			echo '</div>';
		}
		else
		{
			echo '<p>&nbsp;</p>';
			$toggler = '<div class="toggler_pointer toggler_pointer_off" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
			echo $toggler;
			
			echo '<div id="' . $listname . '">';
			$$listname->render();
			echo '</div>';
		}
	}
	else
	{
		echo '<p>&nbsp;</p>';
		$toggler = '<div class="toggler_pointer toggler_pointer_off" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
		echo $toggler;
		
		echo '<div id="' . $listname . '">';
		$$listname->render();
		echo '</div>';
	}
}


?>
<script language="javascript">
jQuery(document).ready(function($) {
	<?php
	foreach($list_names as $key=>$listname)
	{
	?>
		$('#l<?php echo $group_ids[$key];?>_on').click(function()  {
			$('#<?php echo $listname;?>').css('display', 'none');
			$('#l<?php echo $group_ids[$key];?>_off').css('display', 'block');

			$.ajax({
				type: "POST",
				data: "listname=<?php echo $listname;?>&visibility=0",
				url: "../shared/ajx_costsheet_liststaes.php",
				success: function(msg){
				}
			});
		});
		
		$('#l<?php echo $group_ids[$key];?>_off').click(function()  {
			$('#<?php echo $listname;?>').css('display', 'block');
			$('#l<?php echo $group_ids[$key];?>_off').css('display', 'none');

			$.ajax({
				type: "POST",
				data: "listname=<?php echo $listname;?>&visibility=1",
				url: "../shared/ajx_costsheet_liststaes.php",
				success: function(msg){
				}
			});
		});
	<?php
	}
	?>
});


function add_new_cost_position(pid, gid)
{		
	url = '/user/project_costs_add_positions.php?pid=' + pid + '&gid=' + gid + '&mode=budget';;
	$.nyroModalManual({
	  url: url
	});

}


function edit_cost_position(pid, cid)
{		
	url = '/user/project_costs_edit_position.php?pid=' + pid + '&cid=' + cid;
	$.nyroModalManual({
	  url: url
	});

}

function select_all_delete_positions(gid)
{
	var selector = "#selector2" + gid;

	if($(selector).html() == '  ')
	{
		<?php
			foreach($delete_ids as $key=>$ids)
			{
				?>
				if(<?php echo $key;?> == gid){
					<?php
						foreach($ids as $key2=>$id)
						{
							echo "$('#" . $id . "').attr('checked', false);";
						}
					?>
				}
				<?php
			}
		?>
		$(selector).html(' ');
	}
	else
	{
		<?php
			foreach($delete_ids as $key=>$ids)
			{
				?>
				if(<?php echo $key;?> == gid){
					<?php
						foreach($ids as $key2=>$id)
						{
							echo "$('#" . $id . "').attr('checked', true);";
						}
					?>
				}
				<?php
			}
		?>
		$(selector).html('  ');
	}
}

function select_all_partner_positions(gid)
{
	var selector = "#selector3" + gid;

	if($(selector).html() == '  ')
	{
		<?php
			foreach($partner_ids as $key=>$ids)
			{
				?>
				if(<?php echo $key;?> == gid){
					<?php
						foreach($ids as $key2=>$id)
						{
							echo "$('#" . $id . "').attr('checked', false);";
						}
					?>
				}
				<?php
			}
		?>
		$(selector).html(' ');
	}
	else
	{
		<?php
			foreach($partner_ids as $key=>$ids)
			{
				?>
				if(<?php echo $key;?> == gid){
					<?php
						foreach($ids as $key2=>$id)
						{
							echo "$('#" . $id . "').attr('checked', true);";
						}
					?>
				}
				<?php
			}
		?>
		$(selector).html('  ');
	}
}

</script>

<?php

$page->footer();

?>