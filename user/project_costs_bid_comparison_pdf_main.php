<?php
/********************************************************************

    project_costs_bid_comparison_pdf_main.php

    Print empty Offer form for Local Construction Work

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-01-09
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2014, OMEGA SA, All Rights Reserved.

*********************************************************************/

/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());

// read project and order details
$project = get_project(param("pid"));


// get orders's currency
$order_currency = get_client_currency($project["project_order"]);

$page_title = "Contractor Cost Comparison - Project " . $project["project_number"] . " / " . date("d.m.Y");

global $page_footer;
$page_footer = "Contractor Cost Comparison - Project " . $project["project_number"];

$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];


$order_state_name = get_actual_order_state_name($project["order_actual_order_state_code"], 1);
$retail_coordinator = get_user($project["project_retail_coordinator"]);
$retail_operator = get_user($project["order_retail_operator"]);


$invoice_address = "";

$billing_address_province_name = "";
if($project["order_billing_address_place_id"])
{
	$billing_address_province_name = get_province_name($project["order_billing_address_place_id"]);
}

$invoice_address = $invoice_address . $project["order_billing_address_company"];
if ($project["order_billing_address_company2"])
{
    $invoice_address = $invoice_address . ", " . $project["order_billing_address_company2"];
}

$invoice_address = $invoice_address . ", " . $project["order_billing_address_address"];

if ($project["order_billing_address_address2"])
{
    $invoice_address = $invoice_address . ", " . $project["order_billing_address_address2"];
}

$invoice_address = $invoice_address . ", " . $project["order_billing_address_zip"] . " " . $project["order_billing_address_place"];

if($billing_address_province_name) {
	$invoice_address .= ', ' . $billing_address_province_name;
}
$invoice_address = $invoice_address . ", " . $project["order_billing_address_country_name"];
$invoice_address = $invoice_address . ", Phone: " . $project["order_billing_address_phone"];

// get company's address
$client_address = get_address($project["order_client_address"]);
$client = $client_address["company"] . ", " .
          $client_address["zip"] . " " . $client_address["place"] . ", " .
          $client_address["country_name"];


// prepare output fields
$captions1 = array();
$captions1[] = "Project Number:";
$captions1[] = "Product Line / Type:";
$captions1[] = "Project Starting / Agreed Opening Date:";
$captions1[] = "Status:";
$captions1[] = "Project Manager / Retail Operator:";
$captions1[] = "";
$captions1[] = "";

$data1 = array();
$data1[] = $project["project_number"];
$data1[] = $project["product_line_name"] . " / " . $project["postype_name"];
$data1[] = to_system_date($project["order_date"]) . " / " .  to_system_date($project["project_real_opening_date"]);
$data1[] = $project["order_actual_order_state_code"] . " " . $order_state_name;
$data1[] = $retail_coordinator["firstname"] . " " . $retail_coordinator["name"] . " / " . $retail_operator["firstname"] . " " . $retail_operator["name"];
$data1[] = "";
$data1[] = "";

$captions3 = array();
$data3 = array();

$captions3[] = "Client:";
$captions3[] = "Shop:";
//$captions3[] = "Notify Address:";

$data3[] = $client;
$data3[] = $shop;
//$data3[] = $invoice_address;


//build group totals for each offer
$offers = array();
$offer_group_totals = array();

$offer_group_totals = array();
$offer_group_totals_chf = array();

$grandtotals = array();
$grandtotals_chf = array();
$currencies = array();
$remarks = array();
$subgroup_totals = array();

$cost_groups = array();
$cost_subgroups = array();

//1. get all cost groups
$sql = "select distinct costsheet_bid_position_pcost_group_id, " . 
		   "concat(pcost_group_code, ' ' , pcost_group_name) as groupname " . 
		   "from costsheet_bid_positions " . 
		   "left join pcost_groups on pcost_group_id = costsheet_bid_position_pcost_group_id " . 
		   "where costsheet_bid_position_project_id = " . $project["project_id"] . 
		   " order by pcost_group_code";


$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$cost_groups[$row["costsheet_bid_position_pcost_group_id"]] = $row["groupname"];
}

//2. get all cost subgroups
$sql = "select distinct costsheet_bid_position_pcost_group_id, costsheet_bid_position_pcost_subgroup_id, " . 
	   "concat(pcost_subgroup_code, ' ' , pcost_subgroup_name) as subgroupname " . 
	   "from costsheet_bid_positions " . 
	   "left join pcost_groups on pcost_group_id = costsheet_bid_position_pcost_group_id " . 
	   "left join pcost_subgroups on pcost_subgroup_id = costsheet_bid_position_pcost_subgroup_id " . 
	   "where costsheet_bid_position_project_id = " . $project["project_id"] . 
	   " order by pcost_subgroup_code";


$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	if($row["costsheet_bid_position_pcost_subgroup_id"] > 0)
	{
		$cost_subgroups[$row["costsheet_bid_position_pcost_group_id"]][$row["costsheet_bid_position_pcost_subgroup_id"]] = $row["subgroupname"];
	}
}


// 3. get all bids
$sql = "select * from costsheet_bids " . 
       "where costsheet_bid_project_id = " . $project["project_id"] .
       " order by (select sum(costsheet_bid_position_amount) from costsheet_bid_positions where costsheet_bid_position_costsheet_bid_id = costsheet_bid_id)";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
    $offers[$row["costsheet_bid_id"]] = $row["costsheet_bid_company"];
	$remarks[$row["costsheet_bid_id"]] = $row["costsheet_bid_remark"];
}

//4. build group_Totals fo each offer
$offergroups = array();

$offer_group_totals = array();
$offer_group_totals_chf = array();

$offer_subgroup_totals = array();
$offer_grandtotals_chf = array();

$offers_in_budget = array();

foreach($offers as $key=>$value)
{
	$bid_totals = get_project_bid_totals($key, $order_currency);
	$currencies[$key] = $order_currency["symbol"];
	$offer_subgroup_totals[$key] = $bid_totals["subgroup_totals_by_id"];
	
	$offer_group_totals[$key] = $bid_totals["group_totals"];
	$offer_group_totals_chf[$key] = $bid_totals["group_totals_chf"];

	$offer_grandtotals[$key] = $bid_totals["bid_total"];
	$offer_grandtotals_chf[$key] = $bid_totals["bid_total_chf"];

	$offers_in_budget = $bid_totals["bids_in_budget"];
}


/********************************************************************
    prepare pdf
*********************************************************************/

$pdf->SetLineWidth(0.1);


//print the set of offers separately for each cost group
foreach($cost_groups as $group_id=>$cost_group_name)
{
	
	$tmp_offers = $offers;
	$tmp_offer_group_totals = $offer_group_totals;
	$tmp_offer_group_totals_chf = $offer_group_totals_chf;
	$tmp_offer_subgroup_totals = $offer_subgroup_totals;
	$tmp_offer_grandtotals = $offer_grandtotals;
	$tmp_offer_grandtotals_chf = $offer_grandtotals_chf;
	$tmp_currencies = $currencies;
	$tmp_remarks = $remarks;
	
	
	$pages = array();
	$p_offers = array();
	$p_offer_group_totals = array();
	$p_offer_group_totals_chf = array();
	$p_offer_subgroup_totals = array();
	$p_grandtotals = array();
	$p_grandtotals_chf = array();
	$p_currencies = array();
	$p_remarks = array();

	$i=1;
	$page = 1;
	foreach($tmp_offers as $bid_id=>$company_name)
	{
		
		if(array_key_exists($bid_id, $offer_group_totals) and array_key_exists($group_id, $offer_group_totals[$bid_id]))
		{
			if($i < 5 and $page == 1)
			{
				$p_offers[$bid_id] = $tmp_offers[$bid_id];
				$p_offer_group_totals[$bid_id] = $tmp_offer_group_totals[$bid_id];
				$p_offer_group_totals_chf[$bid_id] = $tmp_offer_group_totals_chf[$bid_id];
				$p_offer_subgroup_totals[$bid_id] = $tmp_offer_subgroup_totals[$bid_id];
				$p_grandtotals[$bid_id] = $tmp_offer_grandtotals[$bid_id];
				$p_grandtotals_chf[$bid_id] = $tmp_offer_grandtotals_chf[$bid_id];
				$p_currencies[$bid_id] = $tmp_currencies[$bid_id];
				$p_remarks[$bid_id] = $tmp_remarks[$bid_id];
				$i++;
			}
			elseif($i < 4 and $page > 1)
			{
				$p_offers[$bid_id] = $tmp_offers[$bid_id];
				$p_offer_group_totals[$bid_id] = $tmp_offer_group_totals[$bid_id];
				$p_offer_group_totals_chf[$bid_id] = $tmp_offer_group_totals_chf[$bid_id];
				$p_offer_subgroup_totals[$bid_id] = $tmp_offer_subgroup_totals[$bid_id];
				$p_grandtotals[$bid_id] = $tmp_offer_grandtotals[$bid_id];
				$p_grandtotals_chf[$bid_id] = $tmp_offer_grandtotals_chf[$bid_id];
				$p_currencies[$bid_id] = $tmp_currencies[$bid_id];
				$p_remarks[$bid_id] = $tmp_remarks[$bid_id];
				$i++;
			}
			else
			{
				$i = 1;
				$pages[$page]["offers"] = $p_offers;
				$pages[$page]["offer_group_totals"] = $p_offer_group_totals;
				$pages[$page]["offer_group_totals_chf"] = $p_offer_group_totals_chf;
				$pages[$page]["offer_subgroup_totals"] = $p_offer_subgroup_totals;
				$pages[$page]["grandtotals"] = $p_grandtotals;
				$pages[$page]["grandtotals_chf"] = $p_grandtotals_chf;
				$pages[$page]["currencies"] = $p_currencies;
				$pages[$page]["remarks"] = $p_remarks;


				$p_offers = array();
				$p_offer_group_totals = array();
				$p_offer_group_totals_chf = array();
				$p_offer_subgroup_totals = array();
				$p_grandtotals = array();
				$p_grandtotals_chf = array();
				$p_currencies = array();
				$p_remarks = array();
				$page++;

				$p_offers[$bid_id] = $tmp_offers[$bid_id];
				$p_offer_group_totals[$bid_id] = $tmp_offer_group_totals[$bid_id];
				$p_offer_group_totals_chf[$bid_id] = $tmp_offer_group_totals_chf[$bid_id];
				$p_offer_subgroup_totals[$bid_id] = $tmp_offer_subgroup_totals[$bid_id];
				$p_grandtotals[$bid_id] = $tmp_offer_grandtotals[$bid_id];
				$p_grandtotals_chf[$bid_id] = $tmp_offer_grandtotals_chf[$bid_id];
				$p_currencies[$bid_id] = $tmp_currencies[$bid_id];
				$p_remarks[$bid_id] = $tmp_remarks[$bid_id];
			
			}
		}
	
	}
	
	
	$pages[$page]["offers"] = $p_offers;
	$pages[$page]["offer_group_totals"] = $p_offer_group_totals;
	$pages[$page]["offer_group_totals_chf"] = $p_offer_group_totals_chf;
	$pages[$page]["offer_subgroup_totals"] = $p_offer_subgroup_totals;
	$pages[$page]["grandtotals"] = $p_grandtotals;
	$pages[$page]["grandtotals_chf"] = $p_grandtotals_chf;
	$pages[$page]["currencies"] = $p_currencies;
	$pages[$page]["remarks"] = $p_remarks;

	foreach($pages as $page=>$content)
	{
		
		$tmp_offers = $pages[$page]["offers"];
		$tmp_offer_group_totals = $pages[$page]["offer_group_totals"];
		$tmp_offer_group_totals_chf = $pages[$page]["offer_group_totals_chf"];
		$tmp_offer_subgroup_totals = $pages[$page]["offer_subgroup_totals"];
		$tmp_offer_grandtotals = $pages[$page]["grandtotals"];
		$tmp_offer_grandtotals_chf = $pages[$page]["grandtotals_chf"];
		$tmp_currencies = $pages[$page]["currencies"];
		$tmp_remarks = $pages[$page]["remarks"];
		

		include("project_costs_bid_comparison_pdf_page.php");
		include("project_costs_bid_comparison_pdf_detail.php");
	}
	
}


?>