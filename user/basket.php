<?php

/********************************************************************

    basket.php

    Shopping Basket management.

    Created by:     Oliver Hofer (oliver.hofer@pageagent.com)
    Date created:   2002-08-31
    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date modified:  2003-05-01
    Version:        1.3.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/order_functions.php";
require_once "include/save_functions.php";   

check_access("can_view_orders");

$product_line = param("product_line");
//$cid = param("cid");

//set_referer("order_category.php?product_line=" . $product_line );
set_referer("orders.php");
set_referer("order_new.php");
set_referer("order_category.php");
set_referer("order_checkout.php");

$page = new Page("orders");
$page->register_action('new_order', 'New Order');

// get user's basket

$basket = get_users_basket();

$sql = "select distinct basket_item_id, items.item_id, items.item_code, items.item_name, " .
       "    items.item_priority, round(((items.item_price / currency_exchange_rate) * " .
       "    currency_factor),2) as price, " .
       "    category_name, basket_item_category, basket_item_quantity, " .
       "    round((items.item_price * basket_item_quantity  / currency_exchange_rate * " .
       "    currency_factor),2) as sub_total, product_line_priority, category_priority, " .
       "    concat_ws(': ', product_line_name, category_name) as group_head " .
       "from basket_items " .
       "left join items on basket_item_item = items.item_id " .
       "left join categories on basket_item_category = categories.category_id " .
       "left join product_lines on categories.category_product_line = " .
       "    product_lines.product_line_id " .
       "left join users on users.user_id = " . user_id() . " " .
       "left join addresses on addresses.address_id = users.user_address " .
       "left join currencies on currencies.currency_id = addresses.address_currency ";

$currency = get_user_currency(user_id());

$list = new ListView($sql, LIST_HAS_FOOTER | LIST_HAS_HEADER);

$list->set_title("Standard Items");
$list->set_entity("basket_items");
$list->set_filter("basket_item_basket=" . $basket . " and basket_item_parent IS NULL");
$list->set_group("group_head", "group_head");
$list->set_order("product_line_priority, category_priority");

// access to product-details is restricted
$popup_link = "";
if (has_access("can_view_catalog_detail"))
{
    $popup_link = "popup:catalog_item_view.php?id={item_id}";
}


$list->add_column("item_code", "Item Code", $popup_link, LIST_FILTER_FREE ,'', COLUMN_NO_WRAP);
$list->add_column("item_name", "Name", "", LIST_FILTER_FREE);
$list->add_column("price", "Price " . $currency['currency_symbol'], "", '', '', COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

// read quantity of items in basket and add edit quantity-column
$sql = "select basket_item_id, basket_item_item, basket_item_quantity " .
       "from basket_items " .
       "where basket_item_basket = " . $basket;

$values = array();
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
    $values[$row['basket_item_id']] = $row['basket_item_quantity'];
}


$list->add_edit_column("item_quantity", "Quantity", "6", 0, $values);
$list->add_column("sub_total", "Sub Total " . $currency['currency_symbol'], "", '', '', COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

$list->add_button("shop", "Add More Items");
$list->add_button("update", "Recalculate");

// check number of items in shopping basket
$sql = "select basket_item_item " . 
       "from basket_items " .
       "where basket_item_basket=" . $basket;

$res = mysql_query($sql);
if (mysql_num_rows($res) >= 1)
{
    $list->add_button("checkout", "Proceed to Checkout");
}

$list->add_button("empty", "Empty List");
$list->add_button("cancel_order", "Cancel Ordering");

$list->populate();
$list->process();

if (($list->button("checkout")) 
    || ($list->button("update")))
{
    $add_this_items = array();
    foreach($list->values("item_quantity") as $key => $value)
    {
        $add_this_items[$key] = $value;
    }

    update_shopping_list($add_this_items, $basket,"" , false);

    if ($list->button("update"))
    {
        redirect("basket.php");
    }
}
$list->set_footer("item_code", "Total");
$list->set_footer("sub_total", number_format(get_basket_part_total($basket, false),2));

if ($list->button("checkout"))
{
    redirect("order_checkout.php");
}

if ($list->button('new_order'))
{
    redirect("order_new.php");
}

if ($list->button("shop"))
{
	redirect("order_new.php");
}

if ($list->button("cancel_order"))
{
	redirect("orders.php");
}

if ($list->button("empty"))
{
	$res = mysql_query("delete from basket_items where basket_item_basket = " . $basket);
	redirect("basket.php");
}


/*****************************************************************************/
// Addon-List

$sql = "select distinct basket_item_id, items.item_id, items.item_code, items.item_name, " .
       "    items.item_priority, round(((items.item_price / currency_exchange_rate) * " .
       "    currency_factor),2) as price, " .
       "    category_name, basket_item_category, basket_item_quantity, " .
       "    round((items.item_price * basket_item_quantity  / currency_exchange_rate * " .
       "    currency_factor * addons.addon_package_quantity),2) as sub_total, " .
       "    product_line_priority, category_priority, " .
       "    if(addon_child = basket_item_item, concat('Add-Ons for', ' ',  items1.item_code, ': ', " .
       "    items1.item_name), items1.item_name) as group_head, " .
       "    concat_ws(' x ', addons.addon_package_quantity, items.item_name) as addon_ext_name " . 
       "from basket_items " .
       "left join items on basket_item_item = items.item_id " .
       "left join addons on addons.addon_child = basket_item_item " .
       "left join items as items1 on addon_parent = items1.item_id " .
       "left join categories on basket_item_category = categories.category_id " .
       "left join product_lines on categories.category_product_line = " .
       "    product_lines.product_line_id " .
       "left join users on users.user_id = " . user_id() . " " .
       "left join addresses on addresses.address_id = users.user_address " .
       "left join currencies on currencies.currency_id = addresses.address_currency ";


$list2 = new ListView($sql, LIST_HAS_FOOTER | LIST_HAS_HEADER);


$list2->set_title("Add-Ons");
$list2->set_entity("basket_items_addons");
$list2->set_filter("basket_item_basket=" . $basket . " and basket_item_parent IS NOT NULL and addons.addon_parent = basket_item_parent");
$list2->set_group("group_head", "group_head");
$list2->set_order("product_line_priority, category_priority");


// access to product-details is restricted

$popup_link = "";
if (has_access("can_view_catalog_detail"))
{
    $popup_link = "popup:catalog_item_view.php?id={item_id}";
}

$list2->add_column("item_code", "Item Code", $popup_link, LIST_FILTER_FREE ,'', COLUMN_NO_WRAP);
$list2->add_column("addon_ext_name", "Name", "", LIST_FILTER_FREE);
$list2->add_column("price", "Price " . $currency['currency_symbol'], "", 
                  '', '', COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list2->add_column("basket_item_quantity", "Quantity", "", LIST_FILTER_FREE, '', COLUMN_ALIGN_RIGHT);
$list2->add_column("sub_total", "Sub Total " . $currency['currency_symbol'], "", LIST_FILTER_FREE, '', COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

$list2->set_footer("item_code", "Total");
$list2->set_footer("sub_total", number_format(get_basket_part_total($basket, true),2));


// Cost overview
$form = new Form();
$form->add_section("Cost Overview");
$form->add_label("grand_total1", "Standard Items/Add-On's " . $currency["currency_symbol"], 0, number_format(get_basket_part_total($basket, false) + get_basket_part_total($basket, true),2));

$page->register_action('home', 'Home', "welcome.php");
$page->header();
$page->title('Shopping List');

$form->render();
$list->render();
$list2->render();

$page->footer();
?>