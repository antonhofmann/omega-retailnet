<?php
/********************************************************************

    project_view_project_budget_pdf.php

    View project budget in a PDF

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2003-10-03
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-10-07
    Version:        1.0.0

    Copyright (c) 2003, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_view_budget_in_orders");



/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());

// read project and order details
$order = get_order(param("oid"));

// get orders's currency
$order_currency = get_order_currency(param("oid"));

global $page_title;
$page_title = "Order Budget - Order " . $order["order_number"];

$shop = $order["order_shop_address_company"] . ", " .
        $order["order_shop_address_zip"] . " " .
        $order["order_shop_address_place"] . ", " .
        $order["order_shop_address_country_name"];


$order_state_name = get_actual_order_state_name($order["order_actual_order_state_code"], 2);
$retail_operator = get_user($order["order_retail_operator"]);


$invoice_address = "";

$billing_address_province_name = "";
if($order["order_billing_address_place_id"])
{
	$billing_address_province_name = get_province_name($order["order_billing_address_place_id"]);
}

$invoice_address = $invoice_address . $order["order_billing_address_company"];
if ($order["order_billing_address_company2"])
{
    $invoice_address = $invoice_address . ", " . $order["order_billing_address_company2"];
}

$invoice_address = $invoice_address . ", " . $order["order_billing_address_address"];

if ($order["order_billing_address_address2"])
{
    $invoice_address = $invoice_address . ", " . $order["order_billing_address_address2"];
}

$invoice_address = $invoice_address . ", " . $order["order_billing_address_zip"] . " " . $order["order_billing_address_place"];

if($billing_address_province_name) {
	$invoice_address .= ', ' . $billing_address_province_name;
}
$invoice_address = $invoice_address . ", " . $order["order_billing_address_country_name"];
$invoice_address = $invoice_address . ", Phone: " . $order["order_billing_address_phone"];

// get company's address
$client_address = get_address($order["order_client_address"]);
$client = $client_address["company"] . ", " .
          $client_address["zip"] . " " . $client_address["place"] . ", " .
          $client_address["country_name"];


// prepare SQLs
// create sql for oder items
$sql_order_items = "select order_item_id, order_item_text, order_item_quantity, ".
                   "    order_item_po_number, item_id, order_item_client_price, ".
                   "    TRUNCATE(order_item_quantity * order_item_client_price, 2) as total_price, ".
                   "    order_item_client_price, ".
                   "    if(item_code <> '', item_code, item_type_name) as item_shortcut, ".
                   "    category_priority, category_name, ".
                   "    address_shortcut, item_type_id, ".
                   "    item_type_priority, order_item_type ".
                   "from order_items ".
                   "left join items on order_item_item = item_id ".
                   "left join categories on order_item_category = category_id ".
                   "left join addresses on order_item_supplier_address = address_id ".
                   "left join item_types on order_item_type = item_type_id";

$list_filter1 = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
                      "   and order_item_order = " . $order["order_id"] .
                      "   and order_item_type = " . ITEM_TYPE_STANDARD;

$list_filter2 = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
                      "   and order_item_order = " . $order["order_id"] .
                      "   and order_item_type = " . ITEM_TYPE_SPECIAL;

$list_filter3 = "order_item_order = " . $order["order_id"] .
                      "   and order_item_type = " . ITEM_TYPE_COST_ESTIMATION;

$list_filter4 = "order_item_order = " . $order["order_id"] .
                      "   and order_item_type = " . ITEM_TYPE_EXCLUSION;

$list_filter5 = "order_item_order = " . $order["order_id"] .
                      "   and order_item_type = " . ITEM_TYPE_NOTIFICATION;


// count number of items
$num_recs1 = 0;
$num_recs2 = 0;
$num_recs3 = 0;
$num_recs4 = 0;
$num_recs5 = 0;

// standard items
$sql = "select count(order_item_id) as num_recs ".
       "from order_items where " . $list_filter1;

$res = mysql_query($sql) or dberror($sql);

if ($row = mysql_fetch_assoc($res))
{
    $num_recs1 = $row["num_recs"];
}

// special items
$sql = "select count(order_item_id) as num_recs ".
       "from order_items where " . $list_filter2;

$res = mysql_query($sql) or dberror($sql);

if ($row = mysql_fetch_assoc($res))
{
    $num_recs2 = $row["num_recs"];
}

// cost estimation
$sql = "select count(order_item_id) as num_recs ".
       "from order_items where " . $list_filter3;

$res = mysql_query($sql) or dberror($sql);

if ($row = mysql_fetch_assoc($res))
{
    $num_recs3 = $row["num_recs"];
}

// exclusions
$sql = "select count(order_item_id) as num_recs ".
       "from order_items where " . $list_filter4;

$res = mysql_query($sql) or dberror($sql);

if ($row = mysql_fetch_assoc($res))
{
    $num_recs4 = $row["num_recs"];
}

// notifications
$sql = "select count(order_item_id) as num_recs ".
       "from order_items where " . $list_filter5;

$res = mysql_query($sql) or dberror($sql);

if ($row = mysql_fetch_assoc($res))
{
    $num_recs5 = $row["num_recs"];
}


// build line numbers for budget positions
$line_numbers = get_budet_line_numbers($order["order_id"]);

// build group_totals of standard items
$group_totals = get_group_total_of_standard_items($order["order_id"], "order_item_client_price");

// build totals
$standard_item_total = get_order_item_type_total($order["order_id"],  ITEM_TYPE_STANDARD);
$special_item_total = get_order_item_type_total($order["order_id"],  ITEM_TYPE_SPECIAL);
$cost_estimation_item_total = get_order_item_type_total($order["order_id"],  ITEM_TYPE_COST_ESTIMATION);

$total_cost = $standard_item_total["in_order_currency"] + $special_item_total["in_order_currency"] + $cost_estimation_item_total["in_order_currency"];


// prepare output fields
$captions1 = array();
$captions1[] = "Order Number:";
$captions1[] = "Order Date:";
$captions1[] = "Status:";
$captions1[] = "Retail Operator:";

$captions2 = array();
$captions2[] = "Standard Items:";
$captions2[] = "Special Items:";
$captions2[] = "Estimation of Additional Cost:";
$captions2[] = "Total Cost";


$captions3 = array();
$captions3[] = "Client:";
$captions3[] = "Shop:";
$captions3[] = "Notify Address:";

$data1 = array();
$data1[] = $order["order_number"];
$data1[] = to_system_date($order["order_date"]);
$data1[] = $order["order_actual_order_state_code"] . " " . $order_state_name;
$data1[] = $retail_operator["firstname"] . " " . $retail_operator["name"];

$data2 = array();
$data2[] = number_format($standard_item_total["in_order_currency"], 2) . " " . $order_currency["symbol"];
$data2[] = number_format($special_item_total["in_order_currency"], 2) . " " . $order_currency["symbol"];
$data2[] = number_format($cost_estimation_item_total["in_order_currency"], 2) . " " . $order_currency["symbol"];
$data2[] = number_format($total_cost,2) . " " . $order_currency["symbol"];

$data3 = array();
$data3[] = $client;
$data3[] = $shop;
$data3[] = $invoice_address;


// item data
$captions4 = array();
$captions4[] = "No.";
$captions4[] = "Item";
$captions4[] = "Description";
$captions4[] = "Quantity";
$captions4[] = "Cost in " . $order_currency["symbol"];
$captions4[] = "Total in " . $order_currency["symbol"];

/********************************************************************
    prepare pdf
*********************************************************************/

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');


class MYPDF extends TCPDF
{
    //Page header
    function Header()
    {
        global $page_title;
		//Logo
        $this->Image('../pictures/omega_logo.jpg',10,8,33);
        //arialn bold 15
        $this->SetFont('arialn','B',12);
        //Move to the right
        $this->Cell(80);
        //Title
        $this->Cell(0,34, $page_title, 0, 0, 'R');
        //Line break
        $this->Ln(20);

    }

    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //arialn italic 8
        $this->SetFont('arialn','I',8);
        //Page number
        $this->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$this->PageNo().'/'.$this->getAliasNbPages(),0,0,'R');
    }
   
}

//Instanciation of inherited class
$pdf = new MYPDF("L", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 23, 12);

$pdf->Open();

$pdf->SetLineWidth(0.1);

$pdf->AddFont('arialn');
$pdf->AddFont('arialn', 'B');
$pdf->AddPage();


// output project header informations
$pdf->SetFont('arialn','B',12);
$pdf->Cell(140,5,"Project Details",1);
$pdf->Cell(5,5," ",0);
$pdf->Cell(75,5,"Project Cost",1);
$pdf->Cell(5,5," ",0);
$pdf->Cell(50,5,"Budget Approval",1);

$pdf->Ln();


$pdf->SetFont('arialn','',9);

foreach($captions1 as $key=>$value)
{
    $pdf->Cell(70,5,$captions1[$key],1);
    $pdf->Cell(70,5,$data1[$key],1);

    $pdf->Cell(5,5," ",0);

    if($key <=3)
    {
        if($key == 3)
        {
            $pdf->SetFont('arialn','B',9);
        }
        $pdf->Cell(45,5,$captions2[$key],1);
        $pdf->Cell(30,5,$data2[$key], 1, 0, 'R');
    }
    

    if($key == 0)
    {
        $pdf->Cell(5,5," ",0);
        $pdf->Cell(50,5, 'Name, Date and Signature' ,1);
        $pdf->Ln();
    }
    elseif($key == 1)
    {
        $pdf->Cell(5,5," ",0);
        $pdf->Cell(50,5, '' ,1);
        $pdf->Ln();
    }
    elseif($key == 2)
    {
        $pdf->Cell(5,5," ",0);
        $pdf->Cell(50,5, '' ,1);
        $pdf->Ln();
    }
    elseif($key == 3)
    {
        $pdf->Cell(5,10," ",0);
        $pdf->Cell(50,10, " " ,1);
        $pdf->Ln();
    }

}

$pdf->SetFont('arialn','',9);
$pdf->Ln();


foreach($captions3 as $key=>$value)
{
    $pdf->SetFont('arialn','B',9);
    $pdf->Cell(40,5,$captions3[$key],1);
    $pdf->SetFont('arialn','',9);
    $pdf->MultiCell(235,5,$data3[$key],1);
}


$pdf->Ln();
$pdf->Ln();

// Budget Positions Standard Items
if($num_recs1 > 0)
{
    $pdf->SetFont('arialn','B',9);
    $pdf->SetFillColor(220, 220, 220); 
    $pdf->Cell(275,5, "Standard Items", 1, 0, 'L', 1);
    $pdf->Ln();

    $pdf->SetFillColor(240, 240, 240); 
    $pdf->Cell(10,5, $captions4[0], 1, 0, 'L', 1);
    $pdf->Cell(40,5, $captions4[1], 1, 0, 'L', 1);
    $pdf->Cell(160,5, $captions4[2], 1, 0, 'L', 1);
    $pdf->Cell(15,5, $captions4[3], 1, 0, 'R', 1);
    $pdf->Cell(20,5, $captions4[4], 1, 0, 'R', 1);
    $pdf->Cell(30,5, $captions4[5], 1, 0, 'R', 1);
    $pdf->Ln();

    $pdf->SetFont('arialn','',9);


    $sql = $sql_order_items . " where " . $list_filter1 .
           " order by order_item_type, category_priority, item_shortcut";

    $res = mysql_query($sql) or dberror($sql);

    $type_total = 0;

    while ($row = mysql_fetch_assoc($res))
    {
        $pdf->Cell(10,5, $line_numbers[ITEM_TYPE_STANDARD][$row["order_item_id"]], 1);
        $pdf->Cell(40,5, $row["item_shortcut"], 1);
        $pdf->Cell(160,5, substr($row["order_item_text"], 0, 100), 1);
        $pdf->Cell(15,5, $row["order_item_quantity"], 1, 0, 'R');
        $pdf->Cell(20,5, number_format($row["order_item_client_price"], 2), 1, 0, 'R');
        $pdf->Cell(30,5, number_format($row["total_price"], 2), 1, 0, 'R');
        $pdf->Ln();
        $type_total = $type_total + $row["total_price"];
    }

    $pdf->SetFont('arialn','B',9);
    $pdf->Cell(275,5, 'Total Cost of Standard Items in ' . $order_currency["symbol"] . ' ' . number_format($type_total, 2) , 1, 0, 'R', 1);

    $pdf->Ln();
    $pdf->Ln();
}


// Budget Positions Special Items
if($num_recs2 > 0)
{
    $y = $pdf->getY();
    if($y >= 160)
    {
        $pdf->AddPage();
    }
    $pdf->SetFont('arialn','B',9);
    $pdf->SetFillColor(220, 220, 220); 
    $pdf->Cell(275,5, "Special Items", 1, 0, 'L', 1);
    $pdf->Ln();

    $pdf->SetFillColor(240, 240, 240); 
    $pdf->Cell(10,5, $captions4[0], 1, 0, 'L', 1);
    $pdf->Cell(40,5, $captions4[1], 1, 0, 'L', 1);
    $pdf->Cell(160,5, $captions4[2], 1, 0, 'L', 1);
    $pdf->Cell(15,5, $captions4[3], 1, 0, 'R', 1);
    $pdf->Cell(20,5, $captions4[4], 1, 0, 'R', 1);
    $pdf->Cell(30,5, $captions4[5], 1, 0, 'R', 1);
    $pdf->Ln();

    $pdf->SetFont('arialn','',9);


    $sql = $sql_order_items . " where " . $list_filter2 .
           " order by order_item_type, category_priority, item_shortcut";

    $res = mysql_query($sql) or dberror($sql);

    $type_total = 0;

    while ($row = mysql_fetch_assoc($res))
    {
        $pdf->Cell(10,5, $line_numbers[ITEM_TYPE_SPECIAL][$row["order_item_id"]], 1);
        $pdf->Cell(40,5, $row["item_shortcut"], 1);
        $pdf->Cell(160,5, substr($row["order_item_text"], 0, 100), 1);
        $pdf->Cell(15,5, $row["order_item_quantity"], 1, 0, 'R');
        $pdf->Cell(20,5, number_format($row["order_item_client_price"], 2), 1, 0, 'R');
        $pdf->Cell(30,5, number_format($row["total_price"], 2), 1, 0, 'R');
        $pdf->Ln();
        $type_total = $type_total + $row["total_price"];
    }

    $pdf->SetFont('arialn','B',9);
    $pdf->Cell(275,5, 'Total of Cost Special Items in ' . $order_currency["symbol"] . ' ' . number_format($type_total, 2) , 1, 0, 'R', 1);

    $pdf->Ln();
    $pdf->Ln();
}


// Budget Positions Cost Estimation
if($num_recs3 > 0)
{
    $y = $pdf->getY();
    if($y >= 160)
    {
        $pdf->AddPage();
    }
    $pdf->SetFont('arialn','B',9);
    $pdf->SetFillColor(220, 220, 220); 
    $pdf->Cell(275,5, "Additional Cost Estimation Positions", 1, 0, 'L', 1);
    $pdf->Ln();

    $pdf->SetFillColor(240, 240, 240); 
    $pdf->Cell(10,5, $captions4[0], 1, 0, 'L', 1);
    $pdf->Cell(40,5, $captions4[1], 1, 0, 'L', 1);
    $pdf->Cell(195,5, $captions4[2], 1, 0, 'L', 1);
    $pdf->Cell(30,5, $captions4[5], 1, 0, 'R', 1);
    $pdf->Ln();

    $pdf->SetFont('arialn','',9);


    $sql = $sql_order_items . " where " . $list_filter3 .
           " order by order_item_type, category_priority, item_shortcut";

    $res = mysql_query($sql) or dberror($sql);

    $type_total = 0;

    while ($row = mysql_fetch_assoc($res))
    {
        $pdf->Cell(10,5, $line_numbers[ITEM_TYPE_COST_ESTIMATION][$row["order_item_id"]], 1);
        $pdf->Cell(40,5, $row["item_shortcut"], 1);
        $pdf->Cell(195,5, substr($row["order_item_text"], 0, 140), 1);
        $pdf->Cell(30,5, number_format($row["order_item_client_price"], 2), 1, 0, 'R');
        $pdf->Ln();
        $type_total = $type_total + $row["order_item_client_price"];
    }

    $pdf->SetFont('arialn','B',9);
    $pdf->Cell(275,5, 'Total of Estimated Additional Cost in ' . $order_currency["symbol"] . ' ' . number_format($type_total, 2) , 1, 0, 'R', 1);

    $pdf->Ln();
    $pdf->Ln();
}

// Budget Exclusions
if($num_recs4 > 0)
{
    $y = $pdf->getY();
    if($y >= 160)
    {
        $pdf->AddPage();
    }

    $pdf->SetFont('arialn','B',9);
    $pdf->SetFillColor(220, 220, 220); 
    $pdf->Cell(275,5, "Exclusions", 1, 0, 'L', 1);
    $pdf->Ln();


    $pdf->SetFont('arialn','',9);


    $sql = $sql_order_items . " where " . $list_filter4 .
           " order by order_item_type, category_priority, item_shortcut";

    $res = mysql_query($sql) or dberror($sql);

    $type_total = 0;

    while ($row = mysql_fetch_assoc($res))
    {
        $pdf->Cell(10,5, $line_numbers[ITEM_TYPE_EXCLUSION][$row["order_item_id"]], 1);
        $pdf->Cell(40,5, $row["item_shortcut"], 1);
        $pdf->MultiCell(225,5, $row["order_item_text"], 1);
    }

    $pdf->Ln();
    $pdf->Ln();
}

// Notifications
if($num_recs5 > 0)
{
    $y = $pdf->getY();
    if($y >= 160)
    {
        $pdf->AddPage();
    }

    $pdf->SetFont('arialn','B',9);
    $pdf->SetFillColor(220, 220, 220); 
    $pdf->Cell(275,5, "Notifocations", 1, 0, 'L', 1);
    $pdf->Ln();


    $pdf->SetFont('arialn','',9);


    $sql = $sql_order_items . " where " . $list_filter5 .
           " order by order_item_type, category_priority, item_shortcut";

    $res = mysql_query($sql) or dberror($sql);

    $type_total = 0;

    while ($row = mysql_fetch_assoc($res))
    {
        $pdf->Cell(10,5, $line_numbers[ITEM_TYPE_NOTIFICATION][$row["order_item_id"]], 1);
        $pdf->Cell(40,5, $row["item_shortcut"], 1);
        $pdf->MultiCell(225,5, $row["order_item_text"], 1);
    }

    $pdf->Ln();
    $pdf->Ln();
}

// write pdf
$pdf->Output();


?>