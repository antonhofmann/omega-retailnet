<?php
/********************************************************************


    order_edit_attachment.php


    Add comments to a project


    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-10-27
    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date modified:  2003-06-18
    Version:        1.0.2


    Copyright (c) 2002, Swatch AG, All Rights Reserved.


*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "include/order_functions.php";


check_access("can_edit_attachment_data_in_orders");




/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$order = get_order(param("oid"));


// get company's address
$client_address = get_address($order["order_client_address"]);


// buld sql for attachment categories
$sql_attachment_categories = "select order_file_category_id, order_file_category_name ".
                             "from order_file_categories ".
                             "where order_file_category_type = 1 ".
                             "order by order_file_category_priority";




// buld sql for file types
$sql_file_types = "select file_type_id, file_type_name ".
                  "from file_types ".
                  "order by file_type_name";




// get addresses involved in the project
$companies = get_involved_companies(param('oid'), id());




// determine users that have already got an email announcing the new attachment
$old_recipients = array();
foreach($companies as $key=>$value)
{
    if($value["access"] == 1)
    {
        $old_recipients[] = $value["user"];
    }
}


// checkbox names
$check_box_names = array();


// get file owner
$owner = get_file_owner(id());


/********************************************************************
    Create Form
*********************************************************************/ 


$form = new Form("order_files", "file", 640);


$form->add_section("Order");
$form->add_hidden("oid", param('oid'));
$form->add_hidden("order_file_order", param('oid'));
$form->add_hidden("order_file_owner", user_id());


require_once "include/order_head_small.php";


$form->add_section("Attachment");
$form->add_list("order_file_category", "Category*", $sql_attachment_categories, NOTNULL);
$form->add_edit("order_file_title", "Title*", NOTNULL, "", TYPE_CHAR);
$form->add_multiline("order_file_description", "Description*", 4);



$form->add_hidden("supplier1", 0);
$form->add_hidden("supplier2", 0);


if (has_access("can_set_attachment_accessibility_in_orders"))
{
    $form->add_section("Accessibility");
    $form->add_comment("Please indicate who is allowed to view this attachment.");


    $num_checkboxes=1;
    foreach ($companies as $key=>$value_array)
    {
        if(!array_key_exists('role', $value_array)) {
			$value_array["role"] = "";
		}
		$form->add_checkbox("A" . $num_checkboxes, $value_array["name"], $value_array["access"], 0, $value_array["role"]);
        $check_box_names["A" . $num_checkboxes] = $value_array["user"];
        $num_checkboxes++;
    }
}


$order_number = $order["order_number"];


$form->add_section("File");
$form->add_list("order_file_type", "File Type*", $sql_file_types, NOTNULL);
$form->add_upload("order_file_path", "File", "/files/orders/$order_number", NOTNULL);


$form->add_button(FORM_BUTTON_SAVE, "Save");


if(has_access("can_delete_attachment_in_orders") or $owner == user_id())
{
    $form->add_button("delete", "Delete");
}


$form->add_button(FORM_BUTTON_BACK, "Back");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


if ($form->button(FORM_BUTTON_SAVE))
{
    // check if a recipient was selected
    if (has_access("can_set_attachment_accessibility_in_orders"))
    {
        $no_recipient = 1;
    }
    else
    {
        $no_recipient = 0;
    }


    foreach ($form->items as $item)
    {
        if ($item["type"] == "checkbox" and $item["value"] == "1")
        {
            $no_recipient = 0;
        }
    }


    if ($form->validate()  and $no_recipient == 0)
    {
        update_attachment_accessibility_info(id(), $form,  $check_box_names);

        // send email notifocation to the retail staff
        $order_id = param("oid");


        $sql = "select order_id, order_number, ".
               "users.user_email as recepient, users.user_address as address_id, " .
               "users1.user_email as sender, concat(users1.user_name, ' ', users1.user_firstname) as user_fullname ".
               "from orders ".
               "left join users on order_retail_operator = users.user_id ".
               "left join users as users1 on " . user_id() . "= users1.user_id ".
               "where order_id = " . $order_id;


        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res) and $row["recepient"])
        {
            $subject = MAIL_SUBJECT_PREFIX . ":  New attachment was added - Order " . $row["order_number"];
            $sender_email = $row["sender"];
            $sender_name =  $row["user_fullname"];


            $mail = new Mail();
            $mail->set_subject($subject);
            $mail->set_sender($sender_email, $sender_name);


            $bodytext0 = "An attachment has been updated by " . $sender_name . ":";
			$bodytext1 = "An attachment has been updated by " . $sender_name . " for:";
            
            $recipeint_added = 0;
			$reciepients = array();
			$reciepients_cc = array();
			$reciepients_dp = array();

            foreach ($check_box_names as $key=>$value)
            {
                if ($form->value($key) and !in_array($value, $old_recipients))
                {
					foreach($companies as $key1=>$company)
					{
						
						if($value == $company["user"])
						{
							$sql = "select user_email, user_email_cc, user_email_deputy ".
								   "from users ".
								   "where (user_id = " . $company["user"] . 
								   "   and user_active = 1)";
							
							$res1 = mysql_query($sql) or dberror($sql);
							if ($row1 = mysql_fetch_assoc($res1))
							{
								$reciepients[$row1["user_email"]] = $row1["user_email"];
								$reciepients_cc[$row1["user_email"]] = $row1["user_email_cc"];
								$reciepients_dp[$row1["user_email"]] = $row1["user_email_deputy"];
								$recipeint_added = 1;
							}
						}
					}
                }
            }

			foreach($reciepients as $key=>$email)
			{
				$bodytext1 = $bodytext1 . "\n" . $email;
				$mail->add_recipient($email);
				if(isset($reciepients_cc[$email]))
				{
					$mail->add_cc($reciepients_cc[$email]);
				}
				if(isset($reciepients_dp[$email]))
				{
					$mail->add_cc($reciepients_dp[$email]);
				}
			}


			$bodytext0.= "\n\n<--\n";
            $bodytext0.= $form->value("order_file_title") . "\n";
            
			/*
			if($form->value("order_file_description"))
            {
                $bodytext0.= $form->value("order_file_description") . "\n\n";
            }
            $bodytext0.= "download the file here:\n" . APPLICATION_URL . $form->value("order_file_path");
            $bodytext0.= "\n-->";
			*/

            $link ="order_view_attachments.php?oid=" . $order["order_id"];
            $bodytext = $bodytext0 . "\n\nclick below to have direct access to the order:\n";
            $bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
            $mail->add_text($bodytext);
            $mail->send();


			
			$bodytext1.= "\n\n<--\n";
            $bodytext1.= $form->value("order_file_title") . "\n";
            
			/*
			if($form->value("order_file_description"))
            {
                $bodytext1.= $form->value("order_file_description") . "\n\n";
            }
            $bodytext1.= "download the file here:\n" . APPLICATION_URL . $form->value("order_file_path");
            $bodytext1.= "\n-->";
			*/

			$link ="order_view_attachments.php?oid=" . $order["order_id"];
            $bodytext = $bodytext1 . "\n\nclick below to have direct access to the order:\n";
            $bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";  

            if($recipeint_added == 1)
            {
                append_mail($order["order_id"], "" , user_id(), $bodytext1, "", 2);
            }
        }
        $link = "order_view_attachments.php?oid=" . param("oid");
        redirect($link);
    }
    else
    {
        $form->error("Please select a least one person to have access to the attachment.");
    }
}
elseif ($form->button("delete"))
{
    delete_attachment(id());
    $link = "order_view_attachments.php?oid=" . param("oid");
    redirect($link);
}




$page = new Page("orders");


require "include/order_page_actions.php";


$page->header();
$page->title("Edit Attachment Data");
$form->render();
$page->footer();
?>