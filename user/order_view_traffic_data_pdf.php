<?php
/********************************************************************

    order_view_traffic_data_pdf.php

    View Data concerning delivery and traffic in a PDF

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2003-10-07
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-10-07
    Version:        1.0.0

    Copyright (c) 2003, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_view_delivery_schedule_in_orders");



/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());
$user_roles = get_user_roles(user_id());

// read project and order details
$order = get_order(param("oid"));

global $page_title;
$page_title = "Delivery Schedule for Order " . $order["order_number"];

$shop = $order["order_shop_address_company"] . ", " .
        $order["order_shop_address_zip"] . " " .
        $order["order_shop_address_place"] . ", " .
        $order["order_shop_address_country_name"];


$order_state_name = get_actual_order_state_name($order["order_actual_order_state_code"], 2);
$retail_operator = get_user($order["order_retail_operator"]);


$invoice_address = "";
$billing_address_province_name = "";
if($order["order_billing_address_place_id"])
{
	$billing_address_province_name = get_province_name($order["order_billing_address_place_id"]);
}


$invoice_address = $invoice_address . $order["order_billing_address_company"];
if ($order["order_billing_address_company2"])
{
    $invoice_address = $invoice_address . ", " . $order["order_billing_address_company2"];
}

$invoice_address = $invoice_address . ", " . $order["order_billing_address_address"];

if ($order["order_billing_address_address2"])
{
    $invoice_address = $invoice_address . ", " . $order["order_billing_address_address2"];
}

$invoice_address = $invoice_address . ", " . $order["order_billing_address_zip"] . " " . $order["order_billing_address_place"];

if($billing_address_province_name) {
	$invoice_address .= ", " . $billing_address_province_name;
}

$invoice_address = $invoice_address . ", " . $order["order_billing_address_country_name"];
$invoice_address = $invoice_address . ", Phone: " . $order["order_billing_address_phone"];

// get company's address
$client_address = get_address($order["order_client_address"]);
$client = $client_address["company"] . ", " .
          $client_address["zip"] . " " . $client_address["place"] . ", " .
          $client_address["country_name"];

$captions1 = array();
$captions1[] = "Order Number:";
$captions1[] = "Order Date:";
$captions1[] = "Status:";
$captions1[] = "Retail Operator:";
$captions1[] = '';

$captions2 = array();
$captions2[] = "Preferred Arrival Date:";
$captions2[] = "Preferred Transportation:";
//$captions2[] = "Packaging Retraction Desired:";
$captions2[] = "Pedestrian Area Approval Needed:";
$captions2[] = "Full Delivery Desired:";
$captions2[] = "Insurance:";

$captions3 = array();
$captions3[] = "Delivery Comments:";
$captions3[] = "Client:";
$captions3[] = "Shop:";
if(!in_array(5, $user_roles) and !in_array(29, $user_roles)) // no supplier, no warehouse
{
	$captions3[] = "Notify Address:";
}

$data1 = array();
$data1[] = $order["order_number"];
$data1[] = to_system_date($order["order_date"]);
$data1[] = $order["order_actual_order_state_code"] . " " . $order_state_name;
$data1[] = $retail_operator["firstname"] . " " . $retail_operator["name"];
$data1[] = '';

$data2 = array();
$data2[] = to_system_date($order["order_preferred_delivery_date"]);
$data2[] = $order["transportation_type_name"];

/*
$value="no";
if ($order["order_packaging_retraction"])
{
    $value="yes";
}
$data2[] = $value;
*/

$value="no";
if ($order["order_pedestrian_mall_approval"])
{
    $value="yes";
}
$data2[] = $value;
$value="no";
if ($order["order_full_delivery"])
{
    $value="yes";
}
$data2[] = $value;


$data3[] = $order["order_delivery_comments"];

if ($order["order_insurance"] == 1)
{
    $data2[] = "Insurance covered by " . BRAND . "/Forwarder";;
}
else
{
	$data2[] = "Insurance not covered by " . BRAND . "/Forwarder";
}


$data3[] = $client;
$data3[] = $shop;
if(!in_array(5, $user_roles) and !in_array(29, $user_roles)) // no supplier, no warehouse
{
	$data3[] = $invoice_address;
}


//delivery data
$captions41 = array();
$captions41[] = "Item Code";
$captions41[] = "Quantity";
$captions41[] = "P.O. Number";
$captions41[] = "Shipment Code";
$captions41[] = "Ready for Pickup";
$captions41[] = "Pickup Date";
$captions41[] = "Expected Arrival";
$captions41[] = "Arrival Date";

$captions43 = array();
$captions43[] = "Description";
$captions43[] = "Warehouse Pick Up Address";
$captions43[] = "Delivery Address";


// prepare SQLs
// create sql for oder items

$sql_order_items = "select order_item_id, order_item_text, order_item_quantity, order_item_order,".
                   "    order_item_po_number, order_item_shipment_code, ".
                   "    order_item_ready_for_pickup, order_item_pickup, order_item_expected_arrival, ".
                   "    order_item_arrival, " .
                   "    if(order_item_item <>'', item_code, item_type_name) as item_shortcut, ".
                   "    concat(order_item_quantity, ' ', if(order_item_item <>'', item_code, item_type_name), '\n', if(order_item_po_number <>'', order_item_po_number, '')) as item_po, " .
                   "    addresses.address_company as forwarder_company, ".
                   "    addresses_1.address_company as supplier_company ".
                   "from order_items ".
                   "left join items on order_item_item = item_id ".
                   "left join orders on order_item_order = order_id ".
                   "left join item_types on order_item_type = item_type_id ".
                   "left join addresses on order_item_forwarder_address = addresses.address_id ".
                   "left join addresses AS addresses_1 on order_item_supplier_address = " .
                   "    addresses_1.address_id ";


// get order_item_dates
 $sql_order_item_dates = $sql_order_items . " " .
                         "where (order_item_type = " . ITEM_TYPE_STANDARD .
                         "   or order_item_type = " . ITEM_TYPE_SPECIAL . " ) ".
                         "   and order_item_order = " . $order["order_id"] . " " .
                         "order by forwarder_company, order_item_po_number, order_item_type, item_shortcut";


// get item warehouse addresses
$warehouse_addresses = get_order_adresses($order["order_id"], 4);

//get item delivery addresses
$delivery_addresses = get_order_adresses($order["order_id"], 2);


if (has_access("has_access_to_all_traffic_data_in_orders"))
{
    $list_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
                      "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
                      "   and order_item_order = " . $order["order_id"] .
                      "   and order_item_forwarder_address > 0 ";
}
else
{
     $list_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
                       "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
                       "   and order_item_order = " . $order["order_id"] .
                       "   and (order_item_supplier_address = " . $user_data["address"] . " " .
                       "   or order_item_forwarder_address = " . $user_data["address"] . " " .
                       "   or order_retail_operator = " . user_id() . " " .
                       "   or order_client_address = " . $user_data["address"] . ") ";

}

$sql = $sql_order_items . " where " . $list_filter;



/********************************************************************
    prepare pdf
*********************************************************************/

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');


class MYPDF extends TCPDF
{
    //Page header
    function Header()
    {
        global $page_title;
		//Logo
        $this->Image('../pictures/omega_logo.jpg',10,8,33);
        //arialn bold 15
        $this->SetFont('arialn','B',12);
        //Move to the right
        $this->Cell(80);
        //Title
        $this->Cell(0,34,$page_title,0,0,'R');
        //Line break
        $this->Ln(20);

    }

    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //arialn italic 8
        $this->SetFont('arialn','I',8);
        //Page number
        $this->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$this->PageNo().'/'.$this->getAliasNbPages(),0,0,'R');
    }
   
}

//Instanciation of inherited class
$pdf = new MYPDF("L", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 22, 12);

$pdf->Open();

$pdf->SetLineWidth(0.1);
$pdf->SetFillColor(220, 220, 220); 

$pdf->AddFont('arialn');
$pdf->AddFont('arialn', 'B');

$pdf->AddPage();


// output project headernformations
$pdf->SetFont('arialn','B',12);
$pdf->Cell(140,5,"Order Details",1);
$pdf->Cell(10,5," ",0);
$pdf->Cell(125,5,"Traffic Checklist",1);

$pdf->Ln();


$pdf->SetFont('arialn','',9);

foreach($captions2 as $key=>$value)
{
    if($key <=3)
    {
        $pdf->Cell(70,5,$captions1[$key],1);
        $pdf->Cell(70,5,$data1[$key],1);

        $pdf->Cell(10,5," ",0);
    }
    else
    {
        $pdf->Cell(150,5," ",0);
    }

    

    $pdf->Cell(60,5,$captions2[$key],1);
    $pdf->multiCell(65,5,$data2[$key],1);

}


$pdf->Ln();

foreach($captions3 as $key=>$value)
{
    $pdf->SetFont('arialn','B',9);
    $pdf->Cell(40,5,$captions3[$key],1);
    $pdf->SetFont('arialn','',9);
    $pdf->multiCell(235,5,$data3[$key],1);
}


$pdf->Ln();
$pdf->Ln();

// Delivery Schedule

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
    $id = $row["order_item_id"];
    $date1 = "n/a";
    $date2 = "n/a";
    $date3 = "n/a";
    $date4 = "n/a";
    $y = $pdf->getY();
    if($y >= 160)
    {
        $pdf->AddPage();
    }

    if($row["order_item_ready_for_pickup"] != "0000-00-00")
    {
        $date1 = to_system_date($row["order_item_ready_for_pickup"]);
    }
    if($row["order_item_pickup"] != "0000-00-00")
    {
        $date2 = to_system_date($row["order_item_pickup"]);
    }
    if($row["order_item_expected_arrival"] != "0000-00-00")
    {
        $date3 = to_system_date($row["order_item_expected_arrival"]);
    }
    if($row["order_item_arrival"] != "0000-00-00")
    {
        $date4 = to_system_date($row["order_item_arrival"]);
    }

    $from_adr = "";
	$from_adr2 = "";
    $to_adr= "";
	$to_adr2= "";

    if(array_key_exists($id, $warehouse_addresses))
    {
        
		$tmp = explode("\n",$warehouse_addresses[$id]);
		if(array_key_exists(0, $tmp) and $tmp[0])
		{
			$from_adr .= $tmp[0];
		}
		if(array_key_exists(1, $tmp) and $tmp[1])
		{
			$from_adr .= "\n" . $tmp[1];
		}
		if(array_key_exists(2, $tmp) and $tmp[2])
		{
			$from_adr .= "\n" . $tmp[2];
		}
		if(array_key_exists(3, $tmp) and $tmp[3])
		{
			$from_adr .= "\n" . $tmp[3];
		}


		if(array_key_exists(4, $tmp) and $tmp[4])
		{
			$from_adr2 .= "Contact: " . $tmp[4];
		}
		if(array_key_exists(5, $tmp) and $tmp[5])
		{
			$from_adr2 .= "\n" ."Phone: " . $tmp[5];
		}
		if(array_key_exists(6, $tmp) and $tmp[6])
		{
			$from_adr2 .= "\n" ."Email: " . $tmp[6];
		}
    }
    if(array_key_exists($id, $delivery_addresses))
    {
        
		
		
		$tmp = explode("\n",$delivery_addresses[$id]);
		if(array_key_exists(0, $tmp) and $tmp[0])
		{
			$to_adr .= $tmp[0];
		}
		if(array_key_exists(1, $tmp) and $tmp[1])
		{
			$to_adr .= "\n" . $tmp[1];
		}
		if(array_key_exists(2, $tmp) and $tmp[2])
		{
			$to_adr .= "\n" . $tmp[2];
		}
		if(array_key_exists(3, $tmp) and $tmp[3])
		{
			$to_adr .= ", " . $tmp[3];
		}


		if(array_key_exists(4, $tmp) and $tmp[4])
		{
			$to_adr2 .= "Contact: " . $tmp[4];
		}
		if(array_key_exists(5, $tmp) and $tmp[5])
		{
			$to_adr2 .= "\n" ."Phone: " . $tmp[5];
		}
		if(array_key_exists(6, $tmp) and $tmp[6])
		{
			$to_adr2 .= "\n" ."Email: " . $tmp[6];
		}


    }

    // titles
    $pdf->SetFont('arialn','B',8);
    $pdf->Cell(40,5, $captions41[0],1, 0, 'L', 1);
    $pdf->Cell(20,5,$captions41[1],1, 0, 'L', 1);
    $pdf->Cell(40,5,$captions41[2],1, 0, 'L', 1);
    $pdf->Cell(55,5,$captions41[3],1, 0, 'L', 1);
    $pdf->Cell(30,5,$captions41[4],1, 0, 'L', 1);
    $pdf->Cell(30,5,$captions41[5],1, 0, 'L', 1);
    $pdf->Cell(30,5,$captions41[6],1, 0, 'L', 1);
    $pdf->Cell(30,5,$captions41[7],1, 0, 'L', 1);
    $pdf->Ln();
    $pdf->SetFont('arialn','',8);

    //data    
    $pdf->Cell(40,5, $row["item_shortcut"], 1);
    $pdf->Cell(20,5, $row["order_item_quantity"], 1);
    $pdf->Cell(40,5, $row["order_item_po_number"], 1);
    $pdf->Cell(55,5, $row["order_item_shipment_code"], 1);
    $pdf->Cell(30,5, $date1, 1);
    $pdf->Cell(30,5, $date2, 1);
    $pdf->Cell(30,5, $date3, 1);
    $pdf->Cell(30,5, $date4, 1);
    $pdf->Ln();


	$pdf->Cell(40,5,$captions43[0],1);
    $pdf->multiCell(235,5, $row["order_item_text"], 1);
	$x = $pdf->getX() + 175;
	$y = $pdf->getY();
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(40,5,$captions43[1],1);
	$pdf->SetFont('arialn','',8);
	$pdf->MultiCell(235,5, $from_adr, 1);
	$y2 = $pdf->getY();
	$pdf->setXY($x,$y);
	$pdf->MultiCell(100,5, $from_adr2, 0);
	$pdf->setY($y2);



	$x = $pdf->getX() + 175;
	$y = $pdf->getY();
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(40,5,$captions43[2],1);
	$pdf->SetFont('arialn','',8);
	$pdf->MultiCell(235,5, $to_adr, 1);
	$y2 = $pdf->getY();
	$pdf->setXY($x,$y);
	$pdf->MultiCell(100,5, $to_adr2, 0);
	$pdf->setY($y2);

	$pdf->Ln();
    
	/*
	$pdf->Cell(40,5,$captions43[1],1);
    $pdf->multiCell(235,5, $from_adr, 1);
    $pdf->Cell(40,5,$captions43[2],1);
    $pdf->multiCell(235,5, $to_adr, 1);
	*/
}

// write pdf
$pdf->Output();


?>