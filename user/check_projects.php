<?php
/********************************************************************

    projects.php

    Entry page for the projects section.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-02-11
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/order_state_constants.php";

check_access("can_view_projects");

register_param("showall");
set_referer("projects_archive.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());

// create sql
$sql = "select distinct project_id, project_number, left(projects.date_created, 10), ".
       "    product_line_name, concat(order_shop_address_place,', ', order_shop_address_company), country_name, ".
       "    concat(user_name,' ',user_firstname), ".
       "    order_id, order_actual_order_state_code ".
       "from projects ".
       "left join orders on project_order = order_id ".
       "left join product_lines on project_product_line = product_line_id ".
       "left join countries on order_shop_address_country = countries.country_id ".
       "left join users on project_retail_coordinator = users.user_id ";

// create list filter
$condition = get_user_specific_order_list(user_id(), 1);


if (has_access("has_access_to_all_projects") and has_access("can_view_all_entries_at_start_in_projects"))
{
    $list_filter = "order_archive_date is null";
}
else if (has_access("has_access_to_all_projects") and param("showall"))
{
    $list_filter = "order_archive_date is null";
}
else if (has_access("has_access_to_all_projects") and !param("showall"))
{
    if ($condition == "")
    {
        $list_filter = "order_archive_date is null " .
                       "   and project_retail_coordinator is null";
    }
    else
    {
        $list_filter = "order_archive_date is null " .
                       "   and (" . $condition . " or project_retail_coordinator is null)";
    }
}
else
{
	if ($condition == "")
    {
        // order_type = 0 is a trick to sho no orders
		$list_filter = "order_type = 0 ";
	}
	else
	{
	    if (has_access("can_view_order_before_budget_approval_in_projects"))
		{   
			$list_filter = "order_archive_date is null ".
				           "   and (" . $condition . ")";
	    }
    else
		{
			$list_filter = "order_archive_date is null ".
				           "   and order_show_in_delivery = 1 ".
					       "   and (" . $condition . ")";
	    }
	}
}


// create sql for product lines
if ((has_access("has_access_to_all_projects") and param("showall")) or has_access("can_view_all_entries_at_start_in_projects"))
{
    $sql_product_lines = "select distinct product_line_name ".
                         "from projects ".
                         "left join product_lines on  project_product_line = product_line_id ".
                         "left join orders on project_order = order_id ".
                         "where order_archive_date is null ".
                         "order by product_line_name";
}
else
{
    $sql_product_lines = "select distinct product_line_name ".
                         "from projects ".
                         "left join product_lines on  project_product_line = product_line_id ".
                         "left join orders on project_order = order_id ".
                         "left join addresses on order_client_address = address_id ".
                         "left join order_items on order_item_order = order_id ".
                         "where order_archive_date is null ".
                         "    and (";
    
    $filter_tmp = "order_item_supplier_address = " . $user_data["address"] . " " .
                  "or order_item_forwarder_address = " . $user_data["address"] . " " .
                  "or order_retail_operator = " . user_id() . " " .
                  "or project_retail_coordinator = " . user_id()  . " " .
                  "or project_design_contractor = " . user_id()  . " " .
                  "or project_design_supervisor = " . user_id()   . " " .
                  "or order_client_address = " . $user_data["address"];
   
    $sql_product_lines = $sql_product_lines . $filter_tmp . ") order by product_line_name";
}

// create sql for country list
if ((has_access("has_access_to_all_projects") and param("showall")) or has_access("can_view_all_entries_at_start_in_projects"))
{
    $sql_countries = "select distinct country_name ".
                     "from projects ".
                     "left join orders on project_order = order_id ".
                     "left join countries on order_shop_address_country = countries.country_id ".
                     "where order_archive_date is null ".
                     "order by country_name";
}
else
{
    $sql_countries = "select distinct country_name ".
                     "from projects ".
                     "left join orders on project_order = order_id ".
                     "left join countries on order_shop_address_country = countries.country_id ".
                     "left join order_items on order_item_order = order_id ".
                     "where order_archive_date is null ".
                     "    and (";
    
    $filter_tmp = "order_item_supplier_address = " . $user_data["address"] . " " .
                  "or order_item_forwarder_address = " . $user_data["address"] . " " .
                  "or order_retail_operator = " . user_id() . " " .
                  "or project_retail_coordinator = " . user_id()  . " " .
                  "or project_design_contractor = " . user_id()  . " " .
                  "or project_design_supervisor = " . user_id()   . " " .
                  "or order_client_address = " . $user_data["address"];
   
    $sql_countries = $sql_countries . $filter_tmp . ") order by country_name";
}

// create sql for clients
if ((has_access("has_access_to_all_projects") and param("showall")) or has_access("can_view_all_entries_at_start_in_projects"))
{
    $sql_clients = "select distinct concat(order_shop_address_place,', ', order_shop_address_company) ".
                   "from projects ".
                   "left join orders on  project_order = order_id ".
                   "where order_archive_date is null  ".
                   "order by order_shop_address_place";
}
else
{
    $sql_clients = "select distinct concat(order_shop_address_place,', ', order_shop_address_company) ".
                   "from projects ".
                   "left join orders on  project_order = order_id ".
                   "left join order_items on order_item_order = order_id ".
                   "where order_archive_date is null ".
                   "    and (";
    
    $filter_tmp = "order_item_supplier_address = " . $user_data["address"] . " " .
                  "or order_item_forwarder_address = " . $user_data["address"] . " " .
                  "or order_retail_operator = " . user_id() . " " .
                  "or project_retail_coordinator = " . user_id()  . " " .
                  "or project_design_contractor = " . user_id()  . " " .
                  "or project_design_supervisor = " . user_id()   . " " .
                  "or order_client_address = " . $user_data["address"];
   
    $sql_clients = $sql_clients . $filter_tmp . ") order by order_shop_address_place";
}

// create sql for retail_coordinators
if ((has_access("has_access_to_all_projects") and param("showall")) or has_access("can_view_all_entries_at_start_in_projects"))
{
    $sql_retail_coordinators = "select distinct concat(user_name, ' ', user_firstname) ".
                               "from projects ".
                               "left join users on  project_retail_coordinator = user_id ".
                               "left join orders on  project_order = order_id ".
                               "where order_archive_date is null ".
                               "    and user_name<>'' " .
                               "order by user_name";
}
else
{
    $sql_retail_coordinators = "select distinct concat(user_name, ' ', user_firstname) ".
                               "from projects ".
                               "left join orders on project_order = order_id ".
                               "left join addresses on order_client_address = address_id ".
                               "left join users on  project_retail_coordinator = user_id ".
                               "left join order_items on order_item_order = order_id ".
                               "where order_archive_date is null ".
                               "    and user_name<>'' " .
                               "    and (";
    
    $filter_tmp = "order_item_supplier_address = " . $user_data["address"] . " " .
                  "or order_item_forwarder_address = " . $user_data["address"] . " " .
                  "or order_retail_operator = " . user_id() . " " .
                  "or project_retail_coordinator = " . user_id()  . " " .
                  "or project_design_contractor = " . user_id()  . " " .
                  "or project_design_supervisor = " . user_id()   . " " .
                  "or order_client_address = " . $user_data["address"];

    $sql_retail_coordinators = $sql_retail_coordinators . $filter_tmp . ") order by user_name";
}





// get 2do column (show if user has to do something)
$sql_images = $sql . " where " . $list_filter;
$images = set_to_do_pictures($sql_images, 1);



// check if all 1:n relations in the process are fully processed
$fully_processed = array();
$jobs = array();

if (has_access("has_access_to_all_projects"))
{
    $sql_tmp = $sql . " where " . $list_filter;
    $res = mysql_query($sql_tmp);

    while($row = mysql_fetch_assoc($res))
    {
        
        $sql_t = "select count(task_id) as num from tasks " .
                 "where task_order = " . $row["order_id"]; 
        
        $res_t = mysql_query($sql_t);
        $row_t = mysql_fetch_assoc($res_t);

        if($row_t["num"] > 0)
        {
            $jobs[$row['project_id']] = "/pictures/bullet_ball_glass_green.gif";
        
        }
        else
        {
            $jobs[$row['project_id']] = "/pictures/bullet_ball_glass_red.gif";
        }


        $att = "";

        if($row['order_actual_order_state_code'] == ORDER_TO_SUPPLIER_SUBMITTED)
        {
            //check if all items were ordered
            $ordered = check_if_all_items_hav_order_date($row['order_id'], "");
            if ($ordered == 0)
            {
                $fully_processed[$row['project_id']] = "/pictures/wf_warning.gif";
            }
        }
        elseif($row['order_actual_order_state_code'] == REQUEST_FOR_DELIVERY_SUBMITTED)
        {
            //check if a request for delivery was made for all items
            $delivered = check_requast_for_deleivery_for_all_items($row['order_id'], REQUEST_FOR_DELIVERY_SUBMITTED);
            if ($delivered == 0)
            {
                $fully_processed[$row['project_id']] = "/pictures/wf_warning.gif";
            }
        }
    }
}

/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);
$list->add_hidden("showall", param("showall"));

$list->set_entity("projects");
$list->set_order("left(projects.date_created, 10) desc");
$list->set_filter($list_filter);   

$list->add_image_column("check_todo", "Job", 0, $jobs);
$list->add_image_column("todo", "Job", 0, $images);


$list->add_column("project_number", "Project No.", "project_task_center.php?pid={project_id}", "", "", COLUMN_NO_WRAP);

$list->add_column("product_line_name", "Product Line", "", LIST_FILTER_LIST, $sql_product_lines, COLUMN_NO_WRAP);

$list->add_image_column("order_state", "", 0, $fully_processed);

if(has_access("has_access_to_order_status_report_in_projects"))
{
    $popup_link = "popup1:project_status_report.php?pid=";
    $list->add_column("order_actual_order_state_code", "Status", $popup_link . "{project_id}");
}
else
{
    $list->add_column("order_actual_order_state_code", "Status");
}

$list->add_column("left(projects.date_created, 10)", "Submitted", "", "", "", COLUMN_NO_WRAP);


$list->add_column("country_name", "Country", "", LIST_FILTER_LIST, $sql_countries);
$list->add_column("concat(order_shop_address_place,', ', order_shop_address_company)", "POS Address", "", LIST_FILTER_LIST, $sql_clients);
$list->add_column("concat(user_name,' ',user_firstname)", "Project Manager", "", LIST_FILTER_LIST, $sql_retail_coordinators);


if (has_access("has_access_to_all_projects") and has_access("can_view_all_entries_at_start_in_projects"))
{
}
else if (has_access("has_access_to_all_projects"))
{
    if (param("showall"))
    {
        $list->add_button("show_my_list", "Show My List");
    }
    else
    {
        $list->add_button("show_all", "Show Complete List");
    }
}

if (has_access("can_view_extended_view_in_projects"))
{
   $list->add_button("extend", "Extended View");
}
if (has_access("has_access_to_archive"))
{
   $list->add_button("archive", "Access Archive");
}

$list->add_button(LIST_BUTTON_FILTER, "Filter");
$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove filter");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->process();

if ($list->button("new"))
{
    redirect("project_new_01.php");
}
if ($list->button("show_all"))
{
    $link = "projects.php?showall=1";
    redirect($link);
}
else if ($list->button("show_my_list"))
{
    $link = "projects.php";
    redirect($link);
}
else if ($list->button("extend"))
{
    if (param("showall"))
    {
        $link = "projects_extended_view.php?showall=1";
    }
    else
    {
        $link = "projects_extended_view.php";
    }
    redirect($link);
}
else if ($list->button("archive"))
{
    $link = "../archive/projects_archive.php";
    redirect($link);
}

$page = new Page("projects");

if (has_access("can_create_new_projects"))
{
    $page->register_action('new', 'New Project', "project_new_01.php");
}

$page->register_action('home', 'Home', "welcome.php");

$page->header();
$page->title("Projects");
$list->render();
$page->footer();

?>