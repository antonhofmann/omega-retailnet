<?php
/********************************************************************

    project_tracking_pdf.php

    Print Hitrory of agreed opening dates

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-10-03
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-10-03
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";
check_access("can_view_history_in_projects");


/********************************************************************
    prepare all data needed
*********************************************************************/

$system_currency = get_system_currency_fields();

$project = get_project(param("pid"));

if($project["pipeline"] == 0)
{
	$pos_data = get_poslocation($project["posaddress_id"], "posaddresses");
}
elseif($project["pipeline"] == 1)
{
	$pos_data = get_poslocation_from_pipeline($project["posaddress_id"], $project["order_id"]);
}


if (param("pid"))
{
	global $page_title;
	$page_title = "Project Tracking: " . $project["project_number"];

	
	//POS Basic Data
	$posname = $project["order_shop_address_company"];

	if($project["order_shop_address_company2"])
    {
		$posname .= ", " . $project["order_shop_address_company2"];
	}

	$posaddress = $project["order_shop_address_address"];
	
	if($project["order_shop_address_address2"])
    {
		$posaddress .= ", " . $project["order_shop_address_address2"];
	}
	if($project["order_shop_address_zip"])
    {
		$posaddress .= ", " . $project["order_shop_address_zip"] . " " . $project["order_shop_address_place"];
	}
	else
	{
		$posaddress .= ", " . $project["order_shop_address_place"];
	}
	if($project["order_shop_address_country_name"])
    {
		$posaddress .= ", " . $project["order_shop_address_country_name"];
	}
	

	$poslegaltype = $project["project_costtype_text"];
	$projectkind = $project["projectkind_name"];
	$posphone = $project["order_shop_address_phone"];
	$posfax = $project["order_shop_address_fax"];
	$posemail = $project["order_shop_address_email"];
	$poswebsite = "";
	$plannedopeningdate = to_system_date($project["project_planned_opening_date"]);
	$realisticopeningdate = to_system_date($project["project_real_opening_date"]);

	$posopeningdate = to_system_date($project["project_actual_opening_date"]);
	$posclosingdate = to_system_date($project["project_shop_closingdate"]);

	$projectbudget = number_format($project["project_approximate_budget"], 2);

	
	$postype = $project["postype_name"];
	$possubclass = $project["possubclass_name"];
	
	if($project["productline_subclass_name"])
	{
		$posfurniture = $project["product_line_name"] . " / " . $project["productline_subclass_name"];
	}
	else
	{
		$posfurniture = $project["product_line_name"];
	}


	



	$posareas = "";
	if(count($pos_data) > 0)
	{
		if($project["pipeline"] == 0)
		{
			$sql_i = "select * from posareas " . 
					 "left join posareatypes on posareatype_id = posarea_area " .
					 "where posarea_posaddress = " . $project["posaddress_id"];
			
		}
		elseif($project["pipeline"] == 1)
		{
			$sql_i = "select * from posareaspipeline " . 
					 "left join posareatypes on posareatype_id = posarea_area " .
					 "where posarea_posaddress = " . $project["posaddress_id"];
		}
		$res_i = mysql_query($sql_i) or dberror($sql_i);
		while ($row_i = mysql_fetch_assoc($res_i))
		{
			$posareas .= $row_i["posareatype_name"] . ", ";
		}
		$posareas = substr($posareas,0,strlen($posareas)-2);

		
		
		
	}
	


	//history of agreed opening dates

	$tracking_info = array();
	$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
		   "concat(user_name, ' ', user_firstname) as user_name " . 
		   "from projecttracking " . 
		   "left join users on user_id = projecttracking_user_id " . 
		   "where projecttracking_project_id = " . param("pid") . 
		   " and projecttracking_field = 'project_real_opening_date' " . 
		   " order by projecttracking_time";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$tracking_info[] = array("projecttracking_oldvalue"=>$row["projecttracking_oldvalue"],
			"projecttracking_newvalue"=>$row["projecttracking_newvalue"],
			"projecttracking_comment"=>$row["projecttracking_comment"],
			"projecttracking_time"=>$row["projecttracking_time"],
			"user_name"=>$row["user_name"]
			);
	}


	$tracking_info2 = array();
	$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
		   "concat(user_name, ' ', user_firstname) as user_name " . 
		   "from projecttracking " . 
		   "left join users on user_id = projecttracking_user_id " . 
		   "where projecttracking_project_id = " . param("pid") . 
		   " and projecttracking_field = 'project_state' " . 
		   " order by projecttracking_time";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$tracking_info2[] = array("projecttracking_oldvalue"=>$row["projecttracking_oldvalue"],
			"projecttracking_newvalue"=>$row["projecttracking_newvalue"],
			"projecttracking_comment"=>$row["projecttracking_comment"],
			"projecttracking_time"=>$row["projecttracking_time"],
			"user_name"=>$row["user_name"]
			);
	}


	$tracking_info3 = array();
	$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
		   "concat(user_name, ' ', user_firstname) as user_name " . 
		   "from projecttracking " . 
		   "left join users on user_id = projecttracking_user_id " . 
		   "where projecttracking_project_id = " . param("pid") . 
		   " and projecttracking_field = 'project_construction_startdate' " . 
		   " order by projecttracking_time";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$tracking_info3[] = array("projecttracking_oldvalue"=>$row["projecttracking_oldvalue"],
			"projecttracking_newvalue"=>$row["projecttracking_newvalue"],
			"projecttracking_comment"=>$row["projecttracking_comment"],
			"projecttracking_time"=>$row["projecttracking_time"],
			"user_name"=>$row["user_name"]
			);
	}

	$tracking_info3_1 = array();
	$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
		   "concat(user_name, ' ', user_firstname) as user_name " . 
		   "from projecttracking " . 
		   "left join users on user_id = projecttracking_user_id " . 
		   "where projecttracking_project_id = " . param("pid") . 
		   " and projecttracking_field = 'project_drawing_submission_date' " . 
		   " order by projecttracking_time";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$tracking_info3_1[] = array("projecttracking_oldvalue"=>$row["projecttracking_oldvalue"],
			"projecttracking_newvalue"=>$row["projecttracking_newvalue"],
			"projecttracking_comment"=>$row["projecttracking_comment"],
			"projecttracking_time"=>$row["projecttracking_time"],
			"user_name"=>$row["user_name"]
			);
	}


	$tracking_info4 = array();
	$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
		   "concat(user_name, ' ', user_firstname) as user_name " . 
		   "from projecttracking " . 
		   "left join users on user_id = projecttracking_user_id " . 
		   "where projecttracking_project_id = " . param("pid") . 
		   " and projecttracking_field = 'project_actual_opening_date' " . 
		   " order by projecttracking_time";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$tracking_info4[] = array("projecttracking_oldvalue"=>$row["projecttracking_oldvalue"],
			"projecttracking_newvalue"=>$row["projecttracking_newvalue"],
			"projecttracking_comment"=>$row["projecttracking_comment"],
			"projecttracking_time"=>$row["projecttracking_time"],
			"user_name"=>$row["user_name"]
			);
	}

	


	

	/********************************************************************
		prepare pdf
	*********************************************************************/

	require_once('../include/tcpdf/config/lang/eng.php');
	require_once('../include/tcpdf/tcpdf.php');


	class MYPDF extends TCPDF
	{
		//Page header
		function Header()
		{
			global $page_title;
			//Logo
			$this->Image('../pictures/logo.jpg',10,8,33);
			//arialn bold 15
			$this->SetFont('arialn','B',12);
			//Move to the right
			$this->Cell(80);
			//Title
			$this->Cell(0,33,$page_title,0,0,'R');
			//Line break
			$this->Ln(20);

		}

		//Page footer
		function Footer()
		{
			//Position at 1.5 cm from bottom
			$this->SetY(-15);
			//arialn italic 8
			$this->SetFont('arialn','I',8);
			//Page number
			$this->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$this->PageNo().'/'.$this->getAliasNbPages(),0,0,'R');
		}

	}

	//Instanciation of inherited class
	$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);
	$pdf->SetMargins(10, 23, 10);

	$pdf->Open();


	$pdf->SetFillColor(220, 220, 220); 
	$pdf->AddFont('arialn');
	$pdf->AddFont('arialn', 'B');

	$pdf->AddPage();
	$new_page = 0;

	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190,7,$posname,1, 0, 'L', 1);
	$pdf->Ln();
	$pdf->SetFont('arialn','',10);
	$pdf->Cell(190,7,$posaddress,1, 0, 'L', 0);
	$pdf->Ln();
	$pdf->Ln();

	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Phone",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$posphone,1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Fax",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$posfax,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Email",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$posemail,1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Website",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$poswebsite,1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Environment",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(163, 5,$posareas,1, 0, 'L', 0);
	$pdf->Ln();
	$pdf->Ln();

	
	if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
	{
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(68, 5,"",1, 0, 'L', 0);
	}
	else
	{
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"Planned Opening",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(68, 5,$plannedopeningdate,1, 0, 'L', 0);
	}
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,$project["projectkind_milestone_shortname_01"],1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$realisticopeningdate,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,$project["projectkind_milestone_shortname_02"],1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$posopeningdate,1, 0, 'L', 0);
	
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Closing Date",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$posclosingdate,1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Legal Type",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$poslegaltype,1, 0, 'L', 0);
	
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Project Kind",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$projectkind,1, 0, 'L', 0);
	$pdf->Ln();
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"POS Type",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$postype,1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Furniture",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$posfurniture,1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"POS Type Subcl.",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$possubclass,1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Aprox. Budget",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$projectbudget,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->Ln();
	$pdf->Ln();


	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Agreed Opening Dates",1, 0, 'L', 0);

	$pdf->Ln();
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
	$pdf->Cell(27, 5,"Time",1, 0, 'L', 0);
	$pdf->Cell(15, 5,'Old Value',1, 0, 'L', 0);
	$pdf->Cell(15, 5,'New Value',1, 0, 'L', 0);
	$pdf->Cell(106, 5,'Comment',1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Ln();
	
	
	
	
	foreach($tracking_info as $key=>$values)
	{
		
		$pdf->Cell(27, 5,$values['user_name'],1, 0, 'L', 0);
		$pdf->Cell(27, 5,$values['projecttracking_time'],1, 0, 'L', 0);
		$pdf->Cell(15, 5,$values['projecttracking_oldvalue'],1, 0, 'L', 0);
		$pdf->Cell(15, 5,$values['projecttracking_newvalue'],1, 0, 'L', 0);
		//$pdf->Cell(106, 5,$values['projecttracking_comment'],1, 0, 'L', 0);
		$pdf->MultiCell(106,5, $values['projecttracking_comment'], 1, "T");
		

	}


	$pdf->Ln();
	$pdf->Ln();


	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Actual Opening Dates",1, 0, 'L', 0);

	$pdf->Ln();
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
	$pdf->Cell(27, 5,"Time",1, 0, 'L', 0);
	$pdf->Cell(15, 5,'Old Value',1, 0, 'L', 0);
	$pdf->Cell(15, 5,'New Value',1, 0, 'L', 0);
	$pdf->Cell(106, 5,'Comment',1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Ln();

	foreach($tracking_info4 as $key=>$values)
	{
		
		$pdf->Cell(27, 5,$values['user_name'],1, 0, 'L', 0);
		$pdf->Cell(27, 5,$values['projecttracking_time'],1, 0, 'L', 0);
		$pdf->Cell(15, 5,$values['projecttracking_oldvalue'],1, 0, 'L', 0);
		$pdf->Cell(15, 5,$values['projecttracking_newvalue'],1, 0, 'L', 0);
		//$pdf->Cell(106, 5,$values['projecttracking_comment'],1, 0, 'L', 0);
		$pdf->MultiCell(106,5, $values['projecttracking_comment'], 1, "T");
		

	}


	$pdf->Ln();
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Treatment State",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);

	$pdf->Ln();
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
	$pdf->Cell(27, 5,"Time",1, 0, 'L', 0);
	$pdf->Cell(20, 5,'Old Value',1, 0, 'L', 0);
	$pdf->Cell(20, 5,'New Value',1, 0, 'L', 0);
	$pdf->Cell(96, 5,'Comment',1, 0, 'L', 0);
	$pdf->Ln();
	
	
	
	
	foreach($tracking_info2 as $key=>$values)
	{
		
		$pdf->Cell(27, 5,$values['user_name'],1, 0, 'L', 0);
		$pdf->Cell(27, 5,$values['projecttracking_time'],1, 0, 'L', 0);
		$pdf->Cell(20, 5,$values['projecttracking_oldvalue'],1, 0, 'L', 0);
		$pdf->Cell(20, 5,$values['projecttracking_newvalue'],1, 0, 'L', 0);
		//$pdf->Cell(106, 5,$values['projecttracking_comment'],1, 0, 'L', 0);
		$pdf->MultiCell(96,5, $values['projecttracking_comment'], 1, "T");
		

	}


	$pdf->Ln();
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Drawing Submission",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Ln();
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
	$pdf->Cell(27, 5,"Time",1, 0, 'L', 0);
	$pdf->Cell(20, 5,'Old Value',1, 0, 'L', 0);
	$pdf->Cell(20, 5,'New Value',1, 0, 'L', 0);
	$pdf->Cell(96, 5,'Comment',1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Ln();
	
	
	
	
	foreach($tracking_info3_1 as $key=>$values)
	{
		
		$pdf->Cell(27, 5,$values['user_name'],1, 0, 'L', 0);
		$pdf->Cell(27, 5,$values['projecttracking_time'],1, 0, 'L', 0);
		$pdf->Cell(20, 5,$values['projecttracking_oldvalue'],1, 0, 'L', 0);
		$pdf->Cell(20, 5,$values['projecttracking_newvalue'],1, 0, 'L', 0);
		//$pdf->Cell(106, 5,$values['projecttracking_comment'],1, 0, 'L', 0);
		$pdf->MultiCell(96,5, $values['projecttracking_comment'], 1, "T");
		

	}

	$pdf->Ln();
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Construction Starting",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Ln();
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
	$pdf->Cell(27, 5,"Time",1, 0, 'L', 0);
	$pdf->Cell(20, 5,'Old Value',1, 0, 'L', 0);
	$pdf->Cell(20, 5,'New Value',1, 0, 'L', 0);
	$pdf->Cell(96, 5,'Comment',1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Ln();
	
	
	
	
	foreach($tracking_info3 as $key=>$values)
	{
		
		$pdf->Cell(27, 5,$values['user_name'],1, 0, 'L', 0);
		$pdf->Cell(27, 5,$values['projecttracking_time'],1, 0, 'L', 0);
		$pdf->Cell(20, 5,$values['projecttracking_oldvalue'],1, 0, 'L', 0);
		$pdf->Cell(20, 5,$values['projecttracking_newvalue'],1, 0, 'L', 0);
		//$pdf->Cell(106, 5,$values['projecttracking_comment'],1, 0, 'L', 0);
		$pdf->MultiCell(96,5, $values['projecttracking_comment'], 1, "T");
		

	}


	//get user_tracking

	$pdf->Ln();
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(54, 5,"Other Tracking Information",1, 0, 'L', 0);

	$pdf->Ln();
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
	$pdf->Cell(27, 5,"Time",1, 0, 'L', 0);
	$pdf->Cell(116, 5,'Tracking Info',1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Ln();

	$sql = "select *, concat(user_name, ' ', user_firstname) as user_name, " .
		   "user_tracking.date_created as projecttracking_time " . 
		   "from user_tracking ". 
		   "left join users on user_id = user_tracking_user " . 
		   "where user_tracking_track like '% " . $project["project_order"] . "%' ". 
	       "order by user_tracking.date_created ";

	$tmp = " of order ". $project["project_order"];
	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$pdf->Cell(27, 5,$row['user_name'],1, 0, 'L', 0);
		$pdf->Cell(27, 5,$row['projecttracking_time'],1, 0, 'L', 0);
		$pdf->Cell(116, 5, str_replace($tmp, "", $row['user_tracking_track']),1, 0, 'L', 0);
		$pdf->Ln();
	}

	

	// write pdf
	$pdf->Output();

}

?>