<?php
/********************************************************************

    stock_global_orders_edit_order.php

    Edit global order data of an item

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2004-01-23
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-06-25
    Version:        1.0.0

    Copyright (c) 2004, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_edit_stock");

/********************************************************************
    prepare all data needed
*********************************************************************/


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("store_global_orders", "Global Order");

$form->add_hidden("sid", param("sid"));
$form->add_hidden("store_global_order_store");

$form->add_lookup("item_code", "Item", "items", "item_code", "", param("item_id"));
$form->add_label("store_global_order_date", "Order Date");

$form->add_edit("store_global_order_quantity", "Quantity", 0, "", TYPE_DECIMAL, 20);
$form->add_multiline("store_global_order_comment", "Comments", 4);


$form->add_button("save", "Save");
$form->add_button("delete", "Delete", "", SUBMIT);
$form->add_button("back", "Back");



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$form->populate();
$form->process();

if($form->button("save"))
{
    // get old quantity
    $old_quantity = 0;
    $sql = "select store_global_order_quantity ".
           "from store_global_orders ".
           "where store_global_order_id = " .  id();

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $old_quantity = $row["store_global_order_quantity"];
    }

    // update global order in table stores
    $sql = "select store_id, store_global_order ".
           "from stores ".
           "where store_id = " .  $form->value("store_global_order_store");

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {

        $fields = array();
        
        $tmp = $row["store_global_order"] - $old_quantity + $form->value("store_global_order_quantity");
        $fields[] = "store_global_order = " . $tmp;

        $sql = "update stores set " . join(", ", $fields) . " where store_id = " . $form->value("store_global_order_store");
        mysql_query($sql) or dberror($sql);

    }

    // update record in table store_global_orders    
    $fields = array();
        
    $tmp = $form->value("store_global_order_quantity");
    $fields[] = "store_global_order_quantity = " . $tmp;
    $tmp = dbquote($form->value("store_global_order_comment"));
    $fields[] = "store_global_order_comment = " . $tmp;
    
    
    $sql = "update store_global_orders set " . join(", ", $fields) . " where store_global_order_id = " . id();
    mysql_query($sql) or dberror($sql);


    // update last global order in table stores
    $fields = array();
        
    $tmp = $form->value("store_global_order_quantity");
    $fields[] = "store_last_global_order = " . $tmp;
    $order_date = dbquote(from_system_date($form->value("store_global_order_date")));

    $sql = "update stores set " . 
           join(", ", $fields) . 
           " where store_id = " . $form->value("store_global_order_store") .
           "   and store_last_global_order_date = " . $order_date;

    mysql_query($sql) or dberror($sql);

    $link = "stock_global_orders_edit.php?id=" . $form->value("sid");
    redirect ($link);

}
elseif ($form->button("delete"))
{
    
    // update global order in table stores
    $sql = "select store_id, store_global_order ".
           "from stores ".
           "where store_id = " .  $form->value("store_global_order_store");

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $fields = array();
        
        $tmp = $row["store_global_order"] - $form->value("store_global_order_quantity");
        $fields[] = "store_global_order = " . $tmp;

        $sql = "update stores set " . join(", ", $fields) . " where store_id = " . $form->value("store_global_order_store");
        mysql_query($sql) or dberror($sql);

    }

    // delete record from table store_global_orders    
    $sql = "delete  from store_global_orders " .
           "where store_global_order_id = " . id();

    $res = mysql_query($sql) or dberror($sql);


    // update last global order in table stores
    $sql = "select * ".
           "from store_global_orders ".
           "where store_global_order_store = ". $form->value("store_global_order_store") . " " .
           "order by store_global_order_date DESC";

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $fields = array();
        
        $tmp = $row["store_global_order_quantity"];
        $fields[] = "store_last_global_order = " . $tmp;
        $tmp = dbquote($row["store_global_order_date"]);
        $fields[] = "store_last_global_order_date = " . $tmp;

        $sql = "update stores set " . join(", ", $fields) . " where store_id = " . $form->value("store_global_order_store");
        mysql_query($sql) or dberror($sql);

    }
    else
    {
        $fields = array();
        
        $fields[] = "store_last_global_order = 0";
        $fields[] = "store_last_global_order_date = NULL";

        $sql = "update stores set " . join(", ", $fields) . " where store_id = " . $form->value("store_global_order_store");
        mysql_query($sql) or dberror($sql);

    }
    
    $link = "stock_global_orders_edit.php?id=" . $form->value("sid");
    redirect ($link);

}
elseif ($form->button("back"))
{
    $link = "stock_global_orders_edit.php?id=" . $form->value("sid");
    redirect ($link);
}


/********************************************************************
   Render
*********************************************************************/ 
$page = new Page("stock");

$page->register_action('home', 'Home', "welcome.php");
$page->register_action('stock', 'Stock Control Data', "stock.php");

$page->header();
$page->title("Global Order Data");
$form->render();
$page->footer();
?>