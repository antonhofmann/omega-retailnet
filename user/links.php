<?php
/********************************************************************


    links.php


    List of links grouped by categories.


    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-07-31
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2002-02-26
    Version:        1.1.0


    Copyright (c) 2002, Swatch AG, All Rights Reserved.


*********************************************************************/


require_once "../include/frame.php";


check_access("can_view_links");


// get user_roles
$sql = "select * from user_roles " .
       "where user_role_user =" . user_id();

$res = mysql_query($sql);
$filter = "";

while($row = mysql_fetch_assoc($res))
{
    $filter = $filter . " download_roles_role = " . $row["user_role_role"] . " or ";

}

if($filter != "")
{
    $filter = substr($filter, 0, strlen($filter)-4);
	$filter = "(" . $filter . ") ";
}

//user country
$sql = "select address_country from users " .
       " left join addresses on address_id = user_address " .
       "where user_id =" . user_id();

$res = mysql_query($sql);
if($row = mysql_fetch_assoc($res))
{
    $filter.= " and download_countries_country = " . $row["address_country"];
}
else
{
	$filter.= " download_countries_country = " . $row["address_country"];
}

// downloads
$sql = "select distinct link_id, link_title, link_path, link_url, link_topic_name, link_topic_id, " .
        "link_category_priority, link_category_name, ".
		"concat('<a href=\"', link_url, '\" target=\"_blank\">',link_title, '</a>') as link " . 
	    "from links " .
        "left join link_categories on link_category_id = link_category " .
		"left join link_topics on link_topic_id = link_topic " .
        "left join download_roles on download_roles_download = link_id " .
        "left join download_countries on download_countries_download = link_id";

/********************************************************************
    Create Filter Form
*********************************************************************/ 
$filter_topics = "";
if($filter)
{
	$filter_topics = " where " . $filter;
}

$sql_topic_filter = "select distinct link_topic_id, link_topic_name " . 
					"from links " .
					"left join link_topics on link_topic_id = link_topic " .
					"left join download_roles on download_roles_download = link_id " .
					"left join download_countries on download_countries_download = link_id " . 
					$filter_topics . 
					" order by link_topic_name";

$form = new Form("links", "link");

$form->add_section("List Filter Selection");

$form->add_list("ltf", "Topic Filter", $sql_topic_filter, SUBMIT | NOTNULL, param('ltf'));


/********************************************************************
    Create Lists
*********************************************************************/ 

if(param("ltf") > 0)
{
	$filter .= " and link_topic = " . param("ltf");
}

$topic_lists = array();


$res = mysql_query($sql . " where " . $filter . " order by link_topic_name");
while($row = mysql_fetch_assoc($res))
{
	$topic_lists[$row["link_topic_id"]] = $row["link_topic_name"];

	$list_name = "list_" . $row["link_topic_id"];
	$$list_name = new ListView($sql, LIST_HAS_HEADER);
	$$list_name->set_title($row["link_topic_name"]);
	$$list_name->set_entity("links");
	$$list_name->set_order("link_category_name, link_title");
	$$list_name->set_filter($filter . " and link_topic=" . $row["link_topic_id"]);
	$$list_name->set_group("link_category_name");
	
	if($row["link_url"])
	{
		$$list_name->add_column("link", "Link Title", "", "", "", COLUMN_UNDERSTAND_HTML);
	}
	else
	{
		
		$$list_name->add_column("link_title", "Document Title", "{link_path}");
	}
	
	

}


/********************************************************************
    Create List
*********************************************************************/ 


$list = new ListView($sql, LIST_HAS_HEADER);
$list->set_entity("links");
$list->set_order("link_category_name, link_title");
$list->set_filter($filter);
$list->set_group("link_category_name");
$list->add_column("link_title", "Document Title", "{link_path}");



$page = new Page("links");


$page->register_action('home', 'Home', "welcome.php");


$page->header();
$page->title("Downloads & Links");

$form->render();

foreach($topic_lists as $key=>$name)
{

	$list_name = "list_" . $key;
	$$list_name->render();

	echo "<p>&nbsp;</p>";
}


$page->footer();


?>