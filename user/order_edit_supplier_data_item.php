<?php
/********************************************************************

    order_edit_supplier_data_item.php

    List Data concerning delivery and traffic

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-24
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-06-27
    Version:        1.0.3

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "include/order_functions.php";

check_access("can_edit_supplier_data_in_orders");
set_referer("order_new.php");



/********************************************************************
    prepare all data needed
*********************************************************************/
$user_roles = get_user_roles(user_id());
$user_data = get_user(user_id());

// read project and order details
$order = get_order(param("oid"));

// read order item details
$order_item = get_order_item(id());

// get supplier address details
$supplier_address = get_address($order_item["supplier"]);

// get forwarder address details
$forwarder_address = get_address($order_item["forwarder"]);

// read information from order_addresses
$warehouse_address = get_order_item_address(4, param('oid'), id());
$delivery_address = get_order_item_address(2, param("oid"), id());

// read date entries from dates
$expr = get_last_order_item_date(id(), "EXRP");
$ordr = get_last_order_item_date(id(), "ORSU");

// get order's currency_symbol
$currency_symbol = get_currency_symbol($order_item["supplier_currency"]);

// create array for the warehouse address listbox
$warehouse_addresses = get_warehouse_addresses($order_item["supplier"]);

//get all supplier addresses
$supplier_addresses = get_order_item_supplier_addresses(param('oid'));

if(in_array(5, $user_roles)  or in_array(29, $user_roles)) //user is a supplier or warehouse so he can only allpy changes to his own address
{
	//reduce addresses to the ones the user belongs to
	$tmp = array();
	foreach($supplier_addresses as $key=>$address)
	{
		if($address["id"] == $user_data["address"])
		{
			$tmp[] = $address;
		}
	}
	$supplier_addresses = $tmp;
}

// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ".
                 "order by country_name";


//get province
$province_id = 0;
$country_id = 0;
$place_id = 0;

$sql_p = "select order_address_country, place_province, order_address_place_id " . 
         "from order_addresses " . 
		 "left join places on place_id = order_address_place_id " . 
		 "where order_address_type = 4 and order_address_order = ". $order["order_id"] . " and order_address_order_item = " . param("id");


$res = mysql_query($sql_p) or dberror($sql_p);

if ($row = mysql_fetch_assoc($res))
{
	$province_id = $row["place_province"];
	$country_id = $row["order_address_country"];
	$place_id = $row["order_address_place_id"];
}



if(param("warehouse_address_country"))
{
	$sql_provinces = "select province_id, province_canton " . 
		             "from provinces " . 
		             "where province_country = " . param("warehouse_address_country");
}
elseif(param("warehouse_address_id"))
{
	$sql = "select * ".
           "from order_addresses ".
		   "left join places on place_id = order_address_place_id " . 
           "where order_address_id  = " . param("warehouse_address_id");

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
       
		$sql_provinces = "select province_id, province_canton " . 
		             "from provinces " . 
		             "where province_country = " . $row["order_address_country"];

		
		$sql = "select place_province, province_canton " .
			   "from places " .
			   "left join provinces on province_id = place_province " . 
			   "where place_id = " . dbquote($row["order_address_place_id"]);
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$province_id =  $row["place_province"];
			
		}
		 
    } 
	
}
else
{
	$sql_provinces = "select province_id, province_canton " . 
		             "from provinces " . 
		             "where province_country = " . $country_id;
}



if(param("province"))
{
	$sql_places = "select place_id, place_name " . 
		          "from places " . 
		          " where place_province = " . param("province");
}
else
{
	$sql_places = "select place_id, place_name " . 
		          "from places " . 
		          " where place_province = " . $province_id;
}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("orders", "order");

$form->add_hidden("oid", param("oid"));
$form->add_hidden("order_item_order", param('oid'));
$form->add_hidden("order_item_id", id());
$form->add_hidden("order_item_supplier", $order_item["supplier"]);

$form->add_label("order_number", "Order Number", 0, $order["order_number"]);


$order_state_name = get_actual_order_state_name($order["order_actual_order_state_code"], 2);
$form->add_label("status", "Status", 0, $order["order_actual_order_state_code"] . " " . $order_state_name);

$line = "concat(user_name, ' ', user_firstname)";

if ($order["order_retail_operator"])
{
    $form->add_lookup("retail_operator", "Retail Operator", "users", $line, 0, $order["order_retail_operator"]);
}
else
{
    $form->add_label("retail_operator", "Retail Operator");
}

// POS location

$form->add_section("POS Location Address");
$form->add_label("shop_address_company", "Company", 0, $order["order_shop_address_company"]);

if ($order["order_shop_address_company2"])
{
    $form->add_label("shop_address_company2", "", 0, $order["order_shop_address_company2"]);
}

$form->add_label("shop_address_address", "Address", 0, $order["order_shop_address_address"]);

if ($order["order_shop_address_address2"])
{
    $form->add_label("shop_address_address2", "", 0, $order["order_shop_address_address2"]);
}

$form->add_label("shop_address_place", "City", 0, $order["order_shop_address_zip"] . " " . $order["order_shop_address_place"]);

$form->add_lookup("shop_address_country", "", "countries", "country_name", 0, $order["order_shop_address_country"]);


$line = "concat(user_name, ' ', user_firstname)";

// supplier

$form->add_section("Supplier");
if ($order_item["supplier"])
{
    $form->add_label("supplier_company", "Company", 0, $supplier_address["company"]);
    $form->add_hidden("order_item_supplier_address", $order_item["supplier"]);
}
else
{
    $form->add_label("supplier_company", "Company");
}

$form->add_section("Forwarder");
if ($order_item["forwarder"])
{
    $form->add_label("forwarder_company", "Company", 0, $forwarder_address["company"]);
    $form->add_label("forwarder_address", "Address", 0, $forwarder_address["address"]);
    $form->add_label("forwarder_place", "City", 0, $forwarder_address["zip"] . " " . $forwarder_address["place"]);
    $form->add_lookup("forwarder_country", "Country", "countries", "country_name", 0, $forwarder_address["country"]);

    if ($forwarder_address["contact"])
    {
        $line = "concat(user_name, ' ', user_firstname)";
        $form->add_lookup("forwarder_contact_user", "Contact", "users", $line, 0, $forwarder_address["contact"]);
        $form->add_lookup("forwarder_contact_phone", "Phone", "users", "user_phone", 0, $forwarder_address["contact"]);
        
        $form->add_lookup("forwarder_contact_fax", "Fax", "users", "user_fax", 0, $forwarder_address["contact"]);

        $form->add_lookup("forwarder_contact_email", "Email", "users", "user_email", 0, $forwarder_address["contact"]);
    }
}
else
{
    $form->add_label("forwarder_company", "Company");
    $form->add_label("forwarder_address", "Address");
    $form->add_label("forwarder_place", "City");
    $form->add_label("forwarder_country", "Country");
    $form->add_label("forwarder_contact", "Contact");
    $form->add_label("forwarder_phone", "Phone");
    $form->add_label("forwarder_fax", "Fax");
    $form->add_label("forwarder_email", "Email");
}


$form->add_section("Delivery Address ");
if (count($delivery_address) > 0 )
{
    $form->add_label("delivery_address_company", "Company", 0, $delivery_address["company"]);

    if ($delivery_address["company2"])
    {
        $form->add_label("delivery_address_company2", "", 0, $delivery_address["company2"]);
    }

    $form->add_label("delivery_address_address", "Address", 0, $delivery_address["address"]);

    if ($delivery_address["address2"])
{
        $form->add_label("delivery_address_address2", "", 0, $delivery_address["address2"]);
    }

    $form->add_label("delivery_address_place", "City", 0, $delivery_address["zip"] . " " . $delivery_address["place"]);
    $form->add_lookup("delivery_address_country", "", "countries", "country_name", 0, $delivery_address["country"]);
    $form->add_label("delivery_address_phone", "Phone", 0, $delivery_address["phone"]);
    $form->add_label("delivery_address_fax", "Fax", 0, $delivery_address["fax"]);
    $form->add_label("delivery_address_email", "Email", 0, $delivery_address["email"]);
    $form->add_label("delivery_address_contact", "Contact", 0, $delivery_address["contact"]);
}
else
{
    $form->add_label("delivery_address_company", "Company");
    $form->add_label("delivery_address_address", "Address");
    $form->add_label("delivery_address_place", "City");
    $form->add_label("delivery_address_country", "Country");
    $form->add_label("delivery_address_phone", "Phone");
    $form->add_label("delivery_address_fax", "Fax");
    $form->add_label("delivery_address_email", "Email");
    $form->add_label("delivery_address_contact", "Contact");
}

$tmp_text1="Please indicate the warehous address. ";
$tmp_text2="You can either select an existing address or enter a new address.";

$form->add_section("Supplier's Warehouse Pick Up Address");
if (count($warehouse_addresses) > 0 )
{
    $form->add_comment($tmp_text1 . $tmp_text2);
    $form->add_list("warehouse_address_id", "Supplier's Warehouse Pick Up Address", $warehouse_addresses, SUBMIT);
}
else
{
    $form->add_comment($tmp_text1);
}
if ($ordr["last_date"])
{
	if (count($warehouse_address) > 0)
	{
		$form->add_edit("warehouse_address_company", "Company*", NOTNULL, $warehouse_address["company"], TYPE_CHAR);
	    $form->add_edit("warehouse_address_company2", "", 0, $warehouse_address["company2"], TYPE_CHAR);
	    $form->add_edit("warehouse_address_address", "Address*", NOTNULL, $warehouse_address["address"], TYPE_CHAR);
		$form->add_edit("warehouse_address_address2", "Address2", 0, $warehouse_address["address2"], TYPE_CHAR);
	    $form->add_list("warehouse_address_country", "Country*", $sql_countries, SUBMIT | NOTNULL, $warehouse_address["country"]);

		
		$places = array();
		$places[999999999] = "Other city not listed below";
		$res = mysql_query($sql_places);
		while ($row = mysql_fetch_assoc($res))
		{
			$places[$row["place_id"]] = $row["place_name"];
		}
		

		if(param("warehouse_address_place_id") == 999999999)
		{
			$form->add_list("warehouse_address_place_id", "City*", $places, SUBMIT | NOTNULL, $warehouse_address["place_id"]);
			$form->add_edit("warehouse_address_place", "City*", NOTNULL, "", TYPE_CHAR, 20, 0, 1, "city");
			$form->add_list("province", "Province*", $sql_provinces, NOTNULL, $warehouse_address["province_id"]);
		}
		else
		{
			$form->add_list("province", "Province*", $sql_provinces, SUBMIT | NOTNULL, $warehouse_address["province_id"]);
			$form->add_list("warehouse_address_place_id", "City*", $places, SUBMIT | NOTNULL, $warehouse_address["place_id"]);
			$form->add_edit("warehouse_address_place", "City*", DISABLED | NOTNULL, $warehouse_address["place"], TYPE_CHAR, 20);
		}

	    $form->add_edit("warehouse_address_zip", "ZIP*", NOTNULL, $warehouse_address["zip"], TYPE_CHAR, 20);
	    $form->add_edit("warehouse_address_phone", "Phone*", NOTNULL, $warehouse_address["phone"], TYPE_CHAR, 20);
	    $form->add_edit("warehouse_address_fax", "Fax", 0, $warehouse_address["fax"], TYPE_CHAR, 20);
	    $form->add_edit("warehouse_address_email", "Email", 0, $warehouse_address["email"], TYPE_CHAR);
	    $form->add_edit("warehouse_address_contact", "Contact*", NOTNULL, $warehouse_address["contact"], TYPE_CHAR);
	}
	else
	{
	    $form->add_edit("warehouse_address_company", "Company*", NOTNULL, "", TYPE_CHAR);
	    $form->add_edit("warehouse_address_company2", "", 0, "", TYPE_CHAR);
	    $form->add_edit("warehouse_address_address", "Address*", NOTNULL,"", TYPE_CHAR);
	    $form->add_edit("warehouse_address_address2", "", 0, "", TYPE_CHAR);
		$form->add_list("warehouse_address_country", "Country*", $sql_countries, SUBMIT | NOTNULL, "");

		$places = array();
		$places[999999999] = "Other city not listed below";
		$res = mysql_query($sql_places);
		while ($row = mysql_fetch_assoc($res))
		{
			$places[$row["place_id"]] = $row["place_name"];
		}

		if(param("warehouse_address_place_id") == 999999999)
		{
			$form->add_list("warehouse_address_place_id", "City*", $places, SUBMIT | NOTNULL, $place_id);
			$form->add_edit("warehouse_address_place", "City*", NOTNULL, "", TYPE_CHAR, 20, 0, 1, "city");
			$form->add_list("province", "Province*", $sql_provinces, NOTNULL, $province_id);
		}
		else
		{
			$form->add_list("province", "Province*", $sql_provinces, SUBMIT | NOTNULL, $province_id);
			$form->add_list("warehouse_address_place_id", "City*", $places, SUBMIT | NOTNULL, $place_id);
			$form->add_edit("warehouse_address_place", "City*", DISABLED | NOTNULL, "", TYPE_CHAR, 20);
		}

	    $form->add_edit("warehouse_address_zip", "ZIP*", NOTNULL, "", TYPE_CHAR, 20);
	    $form->add_edit("warehouse_address_phone", "Phone*", NOTNULL, "", TYPE_CHAR, 20);
	    $form->add_edit("warehouse_address_fax", "Fax", 0, "", TYPE_CHAR, 20);
	    $form->add_edit("warehouse_address_email", "Email", 0, "", TYPE_CHAR);
	    $form->add_edit("warehouse_address_contact", "Contact*", NOTNULL, "", TYPE_CHAR);
	}

    $form->add_checkbox("change_all_warehouse_addresses", "apply address entry to every item of this supplier", 1, 0);
}


$form->add_section("Item Details");
$form->add_label("order_item_code", "Item Code", 0, $order_item["code"]);
$form->add_label("order_item_text", "", 0, $order_item["text"]);
$form->add_label("order_item_quantity", "Quantity", 0, $order_item["quantity"]);


$form->add_hidden("expected_ready_for_pick_up_old_value", $expr["last_date"]);

// show edit field only if item has already bee ordered
if (!$ordr["last_date"])
{
    $form->add_hidden("expected_ready_for_pick_up", $expr["last_date"]);
}
else
{
    $form->add_edit("expected_ready_for_pick_up", "Expected Ready for Pickup", 0, $expr["last_date"], TYPE_DATE, 10);
    $form->add_label("ch1", "Number of changes", 0,  $expr["changes"]);
    foreach($supplier_addresses as $key=>$supplier_address)
	{
		$form->add_checkbox("change_all_dates_" . $supplier_address["id"], "apply date entry to every item of " . $supplier_address["company"] . ", " . $supplier_address["place"], 0, 0);
	}
}



$form->add_button("save", "Save Data");
$form->add_button(FORM_BUTTON_BACK, "Back");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


if ($form->button("save"))
{
    if ($form->validate())
    {
        
        project_update_supplier_data_item($form, $supplier_addresses);

        // send an email to forwarder, if expected ready for pickup date has changed

        if ($form->value("expected_ready_for_pick_up_old_value"))
        {
            if ($form->value("expected_ready_for_pick_up_old_value") != $form->value("expected_ready_for_pick_up"))
            {

                
                $mail = new Mail();
                $num_mails = 0;

                $sql = "select order_id, order_number, order_item_po_number, ".
                       "if(order_item_item <>'', item_code, item_type_name) as item_shortcut, ".
                       "users.user_email as recepient, users.user_email_cc as cc," .
                       "users.user_email_deputy as deputy, " .
                       "users.user_address as address_id, " .
                       "users1.user_email as sender, concat(users1.user_name, ' ', users1.user_firstname) as user_fullname, ".
                       "users2.user_email as rtc_mail, concat(users2.user_name, ' ', users2.user_firstname) as rtc_fullname ".
                       "from orders ".
                       "left join order_items on order_item_order = order_id " .
                       "left join items on order_item_item = item_id ".
                       "left join item_types on order_item_type = item_type_id ".
                       "left join addresses on address_id = order_item_forwarder_address ".
                       "left join users on users.user_address = address_id ".
                       "left join users as users1 on " . user_id() . "= users1.user_id ".
                       "left join users as users2 on order_retail_operator = users2.user_id ".
                       "where users.user_active = 1 and order_item_id = " . id();

                $res = mysql_query($sql) or dberror($sql);

                $mail_sent_to = "\nMail sent to: ";

                while ($row = mysql_fetch_assoc($res) and $row["recepient"])
                {
                    $sender_email = $row["sender"];
                    $sender_name =  $row["user_fullname"];
                    $subject = MAIL_SUBJECT_PREFIX . ": Expected ready for pickup date has changed - Order " . $row["order_number"];
                    $mail->set_subject($subject);

                    $mail->set_sender($sender_email, $sender_name);

                    $mail->add_recipient($row["recepient"]);
                    
                    
                    if($row["rtc_mail"])
                    {
                        $mail->add_cc($row["rtc_mail"]);
                    }

                    if($row["cc"])
                    {
                        $mail->add_cc($row["cc"]);
                    }
                    if($row["deputy"])
                    {
                        $mail->add_cc($row["deputy"]);
                    }


                    $mail_sent_to = $mail_sent_to . "\n" . $row["recepient"];
                
                    $bodytext0 = "The expexted ready for pickup date has changed for the following order\n" .
                                 "Order: " . $row["order_number"] . "    Item:  " . $row["item_shortcut"] . "\n".
                                 "from " . $form->value("expected_ready_for_pick_up_old_value") . " to " . $form->value("expected_ready_for_pick_up");

                    $num_mails++;
                                                
                }


                $link ="order_edit_traffic_data_item.php?id=". id() . "&oid=" . $order["order_id"];
                $bodytext = $bodytext0 . "\n\nclick below to have direct access to the order:\n";

                $bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
                
                $mail->add_text($bodytext);
                if($num_mails > 0)
                {
                    $mail->send();


                    append_mail($order["order_id"], "" , user_id(), $bodytext0 . " " . $mail_sent_to , "", 2);
                }
            }
        }

        $link = "order_edit_supplier_data.php?oid=" . param("oid");
        redirect ($link);
    }
}
else if ($form->button("warehouse_address_id"))
{
    // set new warehouse address
    $form->value("warehouse_address_company", "");
    $form->value("warehouse_address_company2",  "");
    $form->value("warehouse_address_address",  "");
    $form->value("warehouse_address_address2",  "");
    $form->value("warehouse_address_zip",  "");
    $form->value("warehouse_address_place",  "");
	$form->value("warehouse_address_place_id",  0);
    $form->value("warehouse_address_country",  0);
    $form->value("warehouse_address_phone",  "");
    $form->value("warehouse_address_fax",  "");
    $form->value("warehouse_address_email",  "");
    $form->value("warehouse_address_contact",  "");

    if ($form->value("warehouse_address_id"))
    {
        $sql = "select * from order_addresses where order_address_id = " . $form->value("warehouse_address_id");
        $res = mysql_query($sql) or dberror($sql);

        if ($row = mysql_fetch_assoc($res))
        {
            $form->value("warehouse_address_company", $row["order_address_company"]);
            $form->value("warehouse_address_company2",  $row["order_address_company2"]);
            $form->value("warehouse_address_address",  $row["order_address_address"]);
            $form->value("warehouse_address_address2",  $row["order_address_address2"]);
            $form->value("warehouse_address_zip",  $row["order_address_zip"]);
            $form->value("warehouse_address_place",  $row["order_address_place"]);
			$form->value("warehouse_address_place_id",  $row["order_address_place_id"]);
            $form->value("warehouse_address_country",  $row["order_address_country"]);
            $form->value("warehouse_address_phone",  $row["order_address_phone"]);
            $form->value("warehouse_address_fax",  $row["order_address_fax"]);
            $form->value("warehouse_address_email",  $row["order_address_email"]);
            $form->value("warehouse_address_contact",  $row["order_address_contact"]);

			$sql = "select place_province, province_canton " .
					"from places " .
					"left join provinces on province_id = place_province " . 
					 "where place_id = " . dbquote($row["order_address_place_id"]);
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$form->value("province",  $row["place_province"]);
			}
			
        }
    }
}
elseif($form->button("warehouse_address_place_id"))
{
	if($form->value("warehouse_address_place_id") == 999999999 or $form->value("warehouse_address_place_id") == 0)
	{
		$form->value("warehouse_address_place",  "");
		$form->value("warehouse_address_zip",  "");
		$form->value("province", "");
	}
	else
	{
		$sql = "select place_name from places where place_id = " . dbquote($form->value("warehouse_address_place_id"));
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$form->value("warehouse_address_place",  $row["place_name"]);
			$form->value("warehouse_address_zip",  "");
		}
		
		$sql = "select place_province, province_canton " .
			   "from places " .
			   "left join provinces on province_id = place_province " . 
			   "where place_id = " . dbquote(param("warehouse_address_place_id"));
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$form->value("province",  $row["place_province"]);
		}
	}
}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("Pickup Data: Edit Item");
$form->render();

$page->footer();

?>