<?php
/********************************************************************
    project_add_catalog_item_item_list.php

    Add items to the list of materials from category item list.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-11
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-02-16
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.
*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_edit_list_of_materials_in_projects");

register_param("pid");
register_param("oid");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));

param("oid", $project["order_id"]);

// get the region of the client's
$client_region = get_address_region($project["order_client_address"]);

// get company's address
$client_address = get_address($project["order_client_address"]);

// create sql for oder items belongin to the selected category

$sql_category_items = "select item_id, item_code, item_name, item_category_name " .
                      "from item_regions " .
                      "left join category_items on category_item_item = item_region_item " .
                      "left join items on item_region_item = item_id " .
                      "left join item_categories on item_category_id = item_category " . 
					  "left join item_pos_types on item_pos_type_item = item_id";


$values = array();

$sql_order_item = "select order_item_item, order_item_quantity ".
                  "from order_items " .
                  "where order_item_category = " . id() .
                  "   and order_item_order = " . $project["project_order"];


$sql_order_item = "select order_item_item, order_item_quantity ".
                  "from order_items " .
                  "where order_item_category = " . id() .
                  "   and order_item_order = " . $project["project_order"];

$res = mysql_query($sql_order_item) or dberror($sql_order_item);
while ($row = mysql_fetch_assoc($res))
{
        $values[$row["order_item_item"]] = $row["order_item_quantity"];
}

$list_filter = "category_item_category = " . id() . 
               "    and item_visible = 1 " .
               "    and item_active = 1 " .
               "    and item_region_region = " . $client_region . 
			   "    and ((item_type = " . ITEM_TYPE_SERVICES . 
			   " and item_pos_type_pos_type = " . $project["project_postype"] . 
			   " and item_pos_type_product_line = " . $project["project_product_line"] . ") " . 
			   " or (item_type <> " . ITEM_TYPE_SERVICES . " and item_pos_type_pos_type is null))";;


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project");

$form->add_section("Project");
require_once "include/project_head_small.php";

/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql_category_items);

$list->set_entity("itmes");
$list->set_filter($list_filter);
$list->set_order("item_category_sortorder, item_code");
$list->set_group("item_category_name");

$list->add_hidden("pid", param("pid"));
$list->add_hidden("oid", $project["order_id"]);
$list->add_hidden("cid", id());
$list->add_hidden("pline", $project["project_product_line"]);

//$list->add_column("item_code", "Item Code", "popup:catalog_item_view.php?id={item_id}");
$list->add_column("item_code", "Item Code");
$list->add_edit_column("item_entry_field", "Quantity", "4", 0, $values);
$list->add_column("item_name", "Item Name");


$list->add_button("add_items", "Add Items");
$list->add_button(LIST_BUTTON_BACK, "Back");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();

if ($list->button("add_items"))
{
    project_save_order_items($list, ITEM_TYPE_STANDARD);
    $link = "project_edit_material_list.php?pid=" . param("pid"); 
    redirect($link);
}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Add Catalog Items: Suppliers's Standard Items");
$form->render();

echo "<p>", "Please indicate the quantities to add to the list.", "</p>";

$list->render();
$page->footer();

?>