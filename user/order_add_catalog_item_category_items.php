<?php

/********************************************************************



    order_add_catalog_item_category_items.php



    Add items to the list of materials

    List all Categories



    Created by:     Anton Hofmann (aho@mediaparx.ch)

    Date created:   2002-09-11

    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)

    Date modified:  2002-09-25

    Version:        1.0.2



    Copyright (c) 2002, Swatch AG, All Rights Reserved.



*********************************************************************/



require_once "../include/frame.php";

require_once "include/get_functions.php";

require_once "include/save_functions.php";

require_once "include/order_functions.php";



check_access("can_edit_list_of_materials_in_orders");



register_param("oid");

set_referer("order_add_catalog_item_item_list.php");



/********************************************************************

    prepare all data needed

*********************************************************************/

    

// read project and order details

$order = get_order(param("oid"));



// get company's address

$client_address = get_address($order["order_client_address"]);





// create sql for product lines and categories

$sql_categories = "select category_id, category_name, product_lines.product_line_name ".

                  "from categories ".

                  "left join product_lines on category_product_line = product_lines.product_line_id ";



/********************************************************************

    Create Form

*********************************************************************/ 



$form = new Form("orders", "order");



/********************************************************************

    Create List

*********************************************************************/ 

$list = new ListView($sql_categories);



$list->set_entity("categories");

$list->set_group("product_line_name");

$list->set_filter("category_catalog=1 and (category_not_in_use is null or category_not_in_use = 0)");



$list->add_hidden("oid", param("oid"));



$link = "order_add_catalog_item_item_list.php?oid=" . param("oid");

$list->add_column("category_name", "Category", $link, LIST_FILTER_NONE);



$list->add_button(LIST_BUTTON_BACK, "Back");



$list->process();





/********************************************************************

    render page

*********************************************************************/

$page = new Page("orders");



require "include/order_page_actions.php";



$page->header();



$page->title("Add Catalog Items: Category Selection");

$form->render();



echo "<p>", "Please choose from the following item categories.", "</p>";



$list->render();

$page->footer();



?>