<?php
/********************************************************************

    order_items.php

    Shows items of selected order

    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date created:   2002-07-31
    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date modified:  2002-09-16
    Version:        1.1.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/order_functions.php";

check_access("can_view_orders");

$sql = "select item_id, item_code, item_name, concat_ws(' ', currency_symbol, " .
       "    round(((item_price / currency_exchange_rate) * currency_factor),2)) as price, " .
       "    category_name, order_item_category, order_item_quantity, concat_ws(' ', " .
       "    currency_symbol, round((item_price * order_item_quantity *currency_exchange_rate * " .
       "    currency_factor),2)) as sub_total " .
       "from order_items " .
       "left join items on order_item_item = item_id " .
       "left join categories on order_item_category = categories.category_id " .
       "left join users on users.user_id = " . user_id() . " " .
       "left join addresses on addresses.address_id = users.user_address " .
       "left join currencies on currencies.currency_id = addresses.address_currency ";

$list = new ListView($sql, LIST_HAS_FOOTER | LIST_HAS_HEADER);

$list->set_entity("catalog_order_items");
$list->set_filter("order_item_order=" . id());

$list->add_column("item_code", "Ref. No", "popup:catalog_item_view.php?id={item_id}", LIST_FILTER_FREE);
$list->add_column("item_name", "Name", "", LIST_FILTER_FREE);
$list->add_column("order_item_quantity", "Qty", "");
$list->add_column("price", "Price", "");
$list->add_column("sub_total", "Sub Total", "");
$list->set_footer("sub_total", get_basket_total("order", id()));

$list->add_button(LIST_BUTTON_BACK, "Back");
$list->add_button(LIST_BUTTON_FILTER, "Filter");
$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove Filter");

$list->process();

$page = new Page("orders");

$page->header();
$page->title("Catalog Order Items");
$list->render();
$page->footer();
?>