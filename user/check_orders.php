<?php
/********************************************************************

    orders.php

    Entry page for the orders section.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-07-31
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-02-11
    Version:        1.0.5

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/order_functions.php";
require_once "include/save_functions.php";

check_access("can_view_orders");

register_param("showall");
set_referer("order_new.php");
set_referer("order_items.php");
set_referer("basket.php");
set_referer("order_view_client_data.php");
set_referer("orders_archive.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());



// create sql for the list of orders
$sql = "select distinct order_id, order_actual_order_state_code, order_number, left(orders.date_created, 10), " .
       "   concat(user_name,' ',user_firstname), " . 
       "   concat(order_shop_address_place,', ', order_shop_address_company), " .
       "   country_name ".
       "from orders " .
       "left join countries on order_shop_address_country = countries.country_id ".
       "left join users on order_retail_operator = users.user_id ";


// create list filter
$condition = get_user_specific_order_list(user_id(), 2);


if (has_access("has_access_to_all_orders") and has_access("can_view_all_entries_at_start_in_orders"))
{
    $list_filter = "order_archive_date is null " .
                   "   and order_type = 2";
}
else if (has_access("has_access_to_all_orders") and param("showall"))
{
    $list_filter = "order_archive_date is null " .
                   "   and order_type = 2";
}
else if (has_access("has_access_to_all_orders") and !param("showall"))
{
    if ($condition == "")
    {
        $list_filter = "order_archive_date is null ".
                       "   and order_type = 2 " .
                       "   and order_retail_operator is null";
    }
    else
    {
        $list_filter = "order_archive_date is null ".
                       "   and order_type = 2 " .
                       "   and (" . $condition . " or order_retail_operator is null)";
    }
}
else
{
    
	if ($condition == "")
    {
        // order_type = 0 is a trick to sho no orders
		$list_filter = "order_type = 0 ";
	}
	else
	{
		if (has_access("can_view_order_before_budget_approval_in_orders"))
		{   
			$list_filter = "order_archive_date is null ".
				           "   and  order_type = 2 ".
					       "   and (" . $condition . ")";
	    }
		else
	    {   
		    $list_filter = "order_show_in_delivery = 1 ".
			               "   and order_archive_date is null ".
				           "   and  order_type = 2 ".
					       "   and (" . $condition . ")";
	    }
	}
}



// create sql for clients
if ((has_access("has_access_to_all_orders") and param("showall")) or has_access("can_view_all_entries_at_start_in_orders"))
{
    $sql_clients = "select distinct concat(order_shop_address_place,', ', order_shop_address_company) ".
                   "from orders ".
                   "where order_archive_date is null ".
                   "   and  order_type = 2 ".
                   "order by order_shop_address_place";
}
else
{
    $sql_clients = "select distinct concat(order_shop_address_place,', ', order_shop_address_company) ".
                   "from orders ".
                   "left join order_items on order_item_order = order_id ".
                   "where order_archive_date is null ".
                   "   and  order_type = 2 ".
                   "   and (" . $condition . ") " .
                   "order by order_shop_address_place";
}


// create sql for retail_operators
if ((has_access("has_access_to_all_orders") and param("showall")) or has_access("can_view_all_entries_at_start_in_orders"))
{
    $sql_retail_operators = "select distinct concat(user_name, ' ', user_firstname) ".
                            "from orders ".
                            "left join users on  order_retail_operator = user_id ".
                            "where user_name <>'' ".
                            "    and  order_type = 2 ".
                            "    and order_archive_date is null ".
                            "order by user_name";
}
else
{
    $sql_retail_operators = "select distinct concat(user_name, ' ', user_firstname) ".
                        "from orders ".
                        "left join users on  order_retail_operator = user_id ".
                        "where order_archive_date is null ".
                        "   and  order_type = 2 ".
                        "   and (" . $condition . ") " .
                        "order by user_name";
}

// create sql for country list
if ((has_access("has_access_to_all_orders") and param("showall")) or has_access("can_view_all_entries_at_start_in_orders"))
{
    $sql_countries = "select distinct country_name ".
                     "from orders ".
                     "left join countries on order_shop_address_country = countries.country_id ".
                     "where order_archive_date is null ".
                     "   and  order_type = 2 ".
                     "order by country_name";
}
else
{
    $sql_countries = "select distinct country_name ".
                     "from orders ".
                     "left join countries on order_shop_address_country = countries.country_id ".
                     "where order_archive_date is null ".
                     "   and  order_type = 2 ".
                     "   and (" . $condition . ") " .
                     "order by country_name";
}




// get 2do column (show if user has to do something)
$sql_images = $sql . " where " . $list_filter;
$images = set_to_do_pictures($sql_images, 2);
$jobs = array();

// check if all 1:n relations in the process are fully processed
$fully_processed = array();
if (has_access("has_access_to_all_orders"))
{
    $sql_tmp = $sql . " where " . $list_filter;
    $res = mysql_query($sql_tmp);

    while($row = mysql_fetch_assoc($res))
    {

        $sql_t = "select count(task_id) as num from tasks " .
                 "where task_order = " . $row["order_id"]; 
        
        $res_t = mysql_query($sql_t);
        $row_t = mysql_fetch_assoc($res_t);

        if($row_t["num"] > 0)
        {
            $jobs[$row['order_id']] = "/pictures/bullet_ball_glass_green.gif";
        }
        else
        {
            $jobs[$row['order_id']] = "/pictures/bullet_ball_glass_red.gif";
        }
        
        if($row['order_actual_order_state_code'] == ORDER_TO_SUPPLIER_SUBMITTED)
        {
            //check if all items were ordered
            $ordered = check_if_all_items_hav_order_date($row['order_id'], "");
            if ($ordered == 0)
            {
                $fully_processed[$row['order_id']] = "/pictures/wf_warning.gif";
            }
        }
        elseif($row['order_actual_order_state_code'] == REQUEST_FOR_DELIVERY_SUBMITTED)
        {
            //check if a request for delivery was made for all items
            $delivered = check_requast_for_deleivery_for_all_items($row['order_id'], REQUEST_FOR_DELIVERY_SUBMITTED);
            if ($delivered == 0)
            {
                $fully_processed[$row['order_id']] = "/pictures/wf_warning.gif";
            }
        }
    }
}

/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);

$list->add_hidden("showall", param("showall"));
$list->set_entity("catalog orders");
$list->set_order("orders.date_created desc");
$list->set_filter($list_filter);    


$list->add_image_column("check_todo", "Job", 0, $jobs);
$list->add_image_column("todo", "Job", 0, $images);
$list->add_column("order_number", "Order Number", "order_task_center.php?oid={order_id}", LIST_FILTER_NONE, "", COLUMN_NO_WRAP);

$list->add_image_column("order_state", "", 0, $fully_processed);

if(has_access("has_access_to_order_status_report_in_orders"))
{
    $popup_link = "popup1:order_status_report.php?oid=";
    $list->add_column("order_actual_order_state_code", "Status", $popup_link . "{order_id}");
}
else
{
    $list->add_column("order_actual_order_state_code", "Status");
}

$list->add_column("left(orders.date_created, 10)", "Submitted", "", LIST_FILTER_NONE, "", COLUMN_NO_WRAP);
$list->add_column("country_name", "Country", "", LIST_FILTER_LIST, $sql_countries);
$list->add_column("concat(order_shop_address_place,', ', order_shop_address_company)", "POS Address", "", LIST_FILTER_LIST, $sql_clients);
$list->add_column("concat(user_name,' ',user_firstname)", "Retail Operator", "", LIST_FILTER_LIST, $sql_retail_operators);


if (has_access("has_access_to_all_orders") and has_access("can_view_all_entries_at_start_in_orders"))
{
}
else if (has_access("has_access_to_all_orders"))
{
    if (param("showall"))
    {
        $list->add_button("show_my_list", "Show My List");
    }
    else
    {
        $list->add_button("show_all", "Show Complete List");
    }
}

if (has_access("can_view_extended_view_in_orders"))
{
    $list->add_button("extend", "Extend View");
}

if (has_access("has_access_to_archive"))
{
    $list->add_button("archive", "Access Archive");
}


$list->add_button(LIST_BUTTON_FILTER, "Filter");
$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove Filter");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->process();


if ($list->button("new"))
{
   redirect("order_new.php");
}
else if ($list->button("basket"))
{
    redirect("basket.php");
}

if ($list->button("show_all"))
{
    $link = "orders.php?showall=1";
    redirect($link);
}
else if ($list->button("show_my_list"))
{
    $link = "orders.php";
    redirect($link);
}
else if ($list->button("extend"))
{
    if (param("showall"))
    {
        $link = "orders_extended_view.php?showall=1";
    }
    else
    {
        $link = "orders_extended_view.php";
    }
    redirect($link);
}
else if ($list->button("archive"))
{
    $link = "../archive/orders_archive.php";
    redirect($link);
}

$page = new Page("orders");

if (has_access("can_create_new_orders"))
{
    $page->register_action('new', 'New Order');
    if (!basket_is_empty())
    {
        $page->register_action('basket', 'Shopping List');
    }
}

$page->register_action('home', 'Home', "welcome.php");

$page->header();
$page->title('Catalog Orders');
$list->render();
$page->footer();

?>