<?php
/********************************************************************

    catalog_select.php

    decide if the shop configurator utility has to be called

    Modified by:    Anton.Hofmann (aho@mediaparx.ch)
    Date created:   2005-08-04
    Modified by:    Anton.Hofmann (aho@mediaparx.ch)
    Date modified:  2005-08-04
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";

$link = "catalog_category.php?id=" . id();

$sql = "select category_useconfigurator " .
       "from categories " .
       "where category_id = " . id();

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
    if($row["category_useconfigurator"] == 1)
    {
        $link = "project_new_00.php?id=" . id();
    }
}

header("location: " . $link);

