<?php
/********************************************************************

    order_add_catalog_item_special_item_list.php

    Add special items to the list of materials.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-02-16
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "include/order_functions.php";

check_access("can_edit_list_of_materials_in_orders");


/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$order = get_order(param("oid"));

// get company's address
$client_address = get_address($order["order_client_address"]);


// get the region of the client's
$client_region = get_address_region($order["order_client_address"]);


// create sql for special oder items
$sql_special_items = "select item_id, item_code, item_name ".
                     "from items " .
                     "left join item_regions on item_region_item = item_id ";

$where_clause = " where item_type = " . ITEM_TYPE_SPECIAL . 
                "    and item_visible = 1 " .
                "    and item_active = 1 " .
                "    and item_region_region = " . $client_region;

// read all items belonging to the category
$values = array();
$res = mysql_query($sql_special_items . $where_clause) or dberror($sql_special_items . $where_clause);
while ($row = mysql_fetch_assoc($res))
{
    // Check if item is already in table order_items
    $sql_order_item = "select order_item_item, order_item_quantity ".
                      "from order_items ".
                      "where order_item_order = " . param('oid') .
                      "    and order_item_item = " . $row["item_id"];

    $res1 = mysql_query($sql_order_item) or dberror($sql_order_item);
    if ($row1 = mysql_fetch_assoc($res1))
    {
        $values[$row["item_id"]] = $row1["order_item_quantity"];
    }
    else
    {
        $values[$row["item_id"]] = "0.00";
    }
}

$list_filter = "item_type = " . ITEM_TYPE_SPECIAL . 
               "    and item_visible = 1 " .
               "    and item_active = 1 " .
               "    and item_region_region = " . $client_region;


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("orders", "order", 640);

$form->add_section("Order");

require_once "include/order_head_small.php";

/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql_special_items);

$list->set_entity("itmes");
$list->set_filter($list_filter);
$list->set_order("item_code");

$list->add_hidden("oid", param("oid"));
$list->add_hidden("cid", id());

// access to product-details is restricted

$popup_link = "";
if (has_access("can_view_catalog_detail"))
{
    $popup_link = "popup:catalog_item_view.php?id={item_id}";
}

$list->add_column("item_code", "Item Code", $popup_link);
$list->add_column("item_name", "Item Name");
$list->add_edit_column("item_entry_field", "Quantity", "4", 0, $values);

$list->add_button("add_items", "Add Items");

$list->add_button(LIST_BUTTON_BACK, "Back");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();

if ($list->button("add_items"))
{
    project_save_order_items($list, ITEM_TYPE_SPECIAL);
    $link = "order_edit_material_list.php?oid=" . param("oid"); 
    redirect($link);
}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("Add Catalog Items: Special Items");
$form->render();

echo "<p>", "Please indicate the quantities to add to the list.", "</p>";

$list->render();
$page->footer();

?>