<?php
/********************************************************************

    order_edit_traffic_data_item.php

    List Data concerning delivery and traffic

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-20
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-16
    Version:        1.1.3

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/order_functions.php";
require_once "include/save_functions.php";
require_once "include/order_state_constants.php";

check_access("can_edit_traffic_data_in_orders");

set_referer("order_new.php");

/********************************************************************
    prepare all data needed
*********************************************************************/

// read project and order details
$order = get_order(param("oid"));

// read order item details
$order_item = get_order_item(id());

// get supplier address details
$supplier_address = get_address($order_item["supplier"]);
$warehouse_address = get_order_item_address(4, param("oid"), id());
$delivery_address = get_order_item_address(2, param("oid"), id());

// get forwarder address details
$forwarder_address = get_address($order_item["forwarder"]);

//get all supplier addresses
$supplier_addresses = get_order_item_supplier_addresses_of_this_forwarder(param("oid"), $order_item["forwarder"]);

// read date entries from dates
$exrp = get_last_order_item_date(id(), "EXRP");
$pick = get_last_order_item_date(id(), "PICK");
$exar = get_last_order_item_date(id(), "EXAR");
$acar = get_last_order_item_date(id(), "ACAR");


/********************************************************************
    build form
*********************************************************************/
$form = new Form("orders", "order");

$form->add_hidden("oid", param("oid"));
$form->add_hidden("order_item_order", param('oid'));
$form->add_hidden("order_item_id", id());
$form->add_hidden("order_item_po_number", $order_item["po_number"]);
$form->add_hidden("order_item_forwarder", $order_item["forwarder"]);

$form->add_label("order_number", "Order Number", 0, $order["order_number"]);

$order_state_name = get_actual_order_state_name($order["order_actual_order_state_code"], 2);
$form->add_label("status", "Status", 0, $order["order_actual_order_state_code"] . " " . $order_state_name);

$line = "concat(user_name, ' ', user_firstname)";

if ($order["order_retail_operator"])
{
    $form->add_lookup("retail_operator", "Retail Operator", "users", $line, 0, $order["order_retail_operator"]);
}
else
{
    $form->add_label("retail_operator", "Retail Operator");
}

// POS address
$form->add_section("POS Location Address");
$form->add_label("shop_address_company", "Company", 0, $order["order_shop_address_company"]);

if ($order["order_shop_address_company2"])
{
    $form->add_label("shop_address_company2", "", 0, $order["order_shop_address_company2"]);
}

$form->add_label("shop_address_address", "Address", 0, $order["order_shop_address_address"]);

if ($order["order_shop_address_address2"])
{
    $form->add_label("shop_address_address2", "", 0, $order["order_shop_address_address2"]);
}

$form->add_label("shop_address_place", "City", 0, $order["order_shop_address_zip"] . " " . $order["order_shop_address_place"]);
$form->add_label("shop_address_country", "", 0, $order["order_shop_address_country_name"]);


$line = "concat(user_name, ' ', user_firstname)";

$form->add_section("Forwarder");
if ($order_item["forwarder"])
{
    $form->add_label("forwarder_company", "Company", 0, $forwarder_address["company"]);
}
else
{
    $form->add_label("forwarder_company", "Company");
}



$form->add_section("Supplier");
if ($order_item["supplier"])
{
    $form->add_label("supplier_company", "Company", 0, $supplier_address["company"]);
    $form->add_label("supplier_address", "Address", 0, $supplier_address["address"]);
    $form->add_label("supplier_place", "City", 0, $supplier_address["zip"] . " " . $supplier_address["place"]);
    $form->add_lookup("supplier_country", "Country", "countries", "country_name", 0, $supplier_address["country"]);

    if ($supplier_address["contact"])
    {
        $line = "concat(user_name, ' ', user_firstname)";
        $form->add_lookup("supplier_contact_user", "Contact", "users", $line, 0, $supplier_address["contact"]);
        $form->add_lookup("supplier_contact_phone", "Phone", "users", "user_phone", 0, $supplier_address["contact"]);
        $form->add_lookup("supplier_contact_email", "Email", "users", "user_email", 0, $supplier_address["contact"]);
    }
}
else
{
    $form->add_label("supplier_company", "Company");
    $form->add_label("supplier_address", "Address");
    $form->add_label("supplier_place", "City");
    $form->add_label("supplier_country", "Country");
    $form->add_label("supplier_contact", "Contact");
    $form->add_label("supplier_phone", "Phone");
    $form->add_label("supplier_email", "Email");
}


$form->add_section("Supplier's Warehouse Pick Up Address");
if (count($warehouse_address) > 0 )
{
    $form->add_label("warehouse_address_company", "Company", 0, $warehouse_address["company"]);

    if ($warehouse_address["company2"])
    {
        $form->add_label("warehouse_address_company2", "", 0, $warehouse_address["company2"]);
    }

    $form->add_label("warehouse_address_address", "Address", 0, $warehouse_address["address"]);

    if ($warehouse_address["address2"])
{
        $form->add_label("warehouse_address_address2", "", 0, $warehouse_address["address2"]);
    }

    $form->add_label("warehouse_address_place", "City", 0, $warehouse_address["zip"] . " " . $warehouse_address["place"]);
    $form->add_lookup("warehouse_address_country", "", "countries", "country_name", 0, $warehouse_address["country"]);
    $form->add_label("warehouse_address_phone", "Phone", 0, $warehouse_address["phone"]);
    $form->add_label("warehouse_address_fax", "Fax", 0, $warehouse_address["fax"]);
    $form->add_label("warehouse_address_email", "Email", 0, $warehouse_address["email"]);
    $form->add_label("warehouse_address_contact", "Contact", 0, $warehouse_address["contact"]);
}
else
{
    $form->add_label("warehouse_address_company", "Company");
    $form->add_label("warehouse_address_address", "Address");
    $form->add_label("warehouse_address_place", "City");
    $form->add_label("warehouse_address_country", "Country");
    $form->add_label("warehouse_address_phone", "Phone");
    $form->add_label("warehouse_address_fax", "Fax");
    $form->add_label("warehouse_address_email", "Email");
    $form->add_label("warehouse_address_contact", "Contact");
}


$form->add_section("Delivery Address ");
if (count($delivery_address) > 0 )
{
    $form->add_label("delivery_address_company", "Company", 0, $delivery_address["company"]);

    if ($delivery_address["company2"])
    {
        $form->add_label("delivery_address_company2", "", 0, $delivery_address["company2"]);
    }

    $form->add_label("delivery_address_address", "Address", 0, $delivery_address["address"]);

    if ($delivery_address["address2"])
{
        $form->add_label("delivery_address_address2", "", 0, $delivery_address["address2"]);
    }

    $form->add_label("delivery_address_place", "City", 0, $delivery_address["zip"] . " " . $delivery_address["place"]);
    $form->add_lookup("delivery_address_country", "", "countries", "country_name", 0, $delivery_address["country"]);
    $form->add_label("delivery_address_phone", "Phone", 0, $delivery_address["phone"]);
    $form->add_label("delivery_address_fax", "Fax", 0, $delivery_address["fax"]);
    $form->add_label("delivery_address_email", "Email", 0, $delivery_address["email"]);
    $form->add_label("delivery_address_contact", "Contact", 0, $delivery_address["contact"]);
}
else
{
    $form->add_label("delivery_address_company", "Company");
    $form->add_label("delivery_address_address", "Address");
    $form->add_label("delivery_address_place", "City");
    $form->add_label("delivery_address_country", "Country");
    $form->add_label("delivery_address_phone", "Phone");
    $form->add_label("delivery_address_fax", "Fax");
    $form->add_label("delivery_address_email", "Email");
    $form->add_label("delivery_address_contact", "Contact");
}



$form->add_section("Item Details");
$form->add_label("order_item_code", "Item Code", 0, $order_item["code"]);
$form->add_label("order_item_text", "", 0, $order_item["text"]);
$form->add_label("order_item_quantity", "Quantity", 0, $order_item["quantity"]);
$form->add_label("order_item_po_number", "P.O. Number", 0, $order_item["po_number"]);

$form->add_section("Traffic Details");
$form->add_edit("order_item_shipment_code", "Shipment Code",0,  $order_item["shipment_code"], TYPE_CHAR, 20);
$form->add_label("expected_ready_for_pick_up", "Expected Ready for Pickup", 0, $exrp["last_date"]);
$form->add_label("ch1", "Number of changes", 0,  $exrp["changes"]);

if ($exrp["last_date"] and number_format($order["order_actual_order_state_code"],0) >= REQUEST_FOR_DELIVERY_SUBMITTED)
{
    $form->add_hidden("pick_up_old_value",  $pick["last_date"]);
    $form->add_edit("pick_up", "Pick Up", 0, $pick["last_date"], TYPE_DATE, 10);
    $form->add_label("ch2", "Number of changes", 0,  $pick["changes"]);

    $form->add_hidden("expected_arrival_old_value",  $exar["last_date"]);
    $form->add_edit("expected_arrival", "Expected Arrival", 0, $exar["last_date"], TYPE_DATE, 10);
    $form->add_label("ch3", "Number of changes", 0,  $exar["changes"]);

    $form->add_hidden("arrival_old_value",  $acar["last_date"]);
    $form->add_edit("arrival", "Arrival", 0, $acar["last_date"], TYPE_DATE, 10);
    $form->add_label("ch4", "Number of changes", 0,  $acar["changes"]);

    foreach($supplier_addresses as $key=>$supplier_address)
	{
		$form->add_checkbox("change_all_dates_" . $supplier_address["id"], "apply date entry to every item of " . $supplier_address["company"] . ", " . $supplier_address["place"], 0, 0);
	}

    $form->add_button("save", "Save Data");
}
$form->add_button(FORM_BUTTON_BACK, "Back");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


if ($form->button("save"))
{
    // validate form
    
    if ($form->validate())
    {
        update_traffic_data_item($form, 2, $supplier_addresses);

        // pickupd date has changed
        if ($form->value("pick_up_old_value"))
        {

            if ($form->value("pick_up_old_value") != $form->value("pick_up"))
            {

                
                $mail = new Mail();
                $num_mails = 0;

                $sql = "select order_id, order_number, order_item_po_number, ".
                       "if(order_item_item <>'', item_code, item_type_name) as item_shortcut, ".
                       "users.user_email as recepient, users.user_email_cc as cc," .
                       "users.user_email_deputy as deputy, " .
                       "users.user_address as address_id, " .
                       "users1.user_email as sender, concat(users1.user_name, ' ', users1.user_firstname) as user_fullname ".
                       "from orders ".
                       "left join order_items on order_item_order = order_id " .
                       "left join items on order_item_item = item_id ".
                       "left join item_types on order_item_type = item_type_id ".
                       "left join addresses on address_id = order_item_supplier_address ".
                       "left join users on users.user_address = address_id ".
                       "left join users as users1 on " . user_id() . "= users1.user_id ".
                       "where users.user_active = 1 and order_item_id = " . id();

                $mail_sent_to = "\nMail sent to: ";

                $res = mysql_query($sql) or dberror($sql);

                while ($row = mysql_fetch_assoc($res) and $row["recepient"])
                {
                    $sender_email = $row["sender"];
                    $sender_name =  $row["user_fullname"];
                    $subject = MAIL_SUBJECT_PREFIX . ": Pickup date has changed - Order " . $row["order_number"];
                    $mail->set_subject($subject);

                    $mail->set_sender($sender_email, $sender_name);

                    $mail->add_recipient($row["recepient"]);

                    if($row["cc"])
                    {
                        $mail->add_cc($row["cc"]);
                    }
                    if($row["deputy"])
                    {
                        $mail->add_cc($row["deputy"]);
                    }
                    
                    $mail_sent_to = $mail_sent_to . "\n" . $row["recepient"];

                    $bodytext0 = "The pickup date has changed for the following order\n" .
                                 "Order: " . $row["order_number"] . "    Item:  " . $row["item_shortcut"] . "\n".
                                 "from " . $form->value("pick_up_old_value") . " to " . $form->value("pick_up");

                    $num_mails++;
                                                
                }


                $link ="order_view_traffic_data.php?oid=" . $order["order_id"];
                $bodytext = $bodytext0 . "\n\nclick below to have direct access to the order:\n";

                $bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
                
                $mail->add_text($bodytext);
                if($num_mails > 0)
                {
                    $mail->send();


                    append_mail($order["order_id"], "" , user_id(), $bodytext0 . " " . $mail_sent_to , "", 2);
                }
            }
        }


        // expected arrival date has changed
        if ($form->value("expected_arrival_old_value"))
        {
            if ($form->value("expected_arrival_old_value") != $form->value("expected_arrival"))
            {

                $mail = new Mail();
                $num_mails = 0;

                $sql = "select order_id, order_number, order_item_po_number, ".
                       "if(order_item_item <>'', item_code, item_type_name) as item_shortcut, ".
                       "users.user_email as recepient, users.user_email_cc as cc," .
                       "users.user_email_deputy as deputy, " .
                       "users.user_address as address_id, " .
                       "users2.user_email as rto, " .                       "users1.user_email as sender, concat(users1.user_name, ' ', users1.user_firstname) as user_fullname ".
                       "from orders ".
                       "left join order_items on order_item_order = order_id " .
                       "left join items on order_item_item = item_id ".
                       "left join item_types on order_item_type = item_type_id ".
                       "left join users on users.user_id = order_user ".
                       "left join users as users2 on users2.user_id = order_retail_operator ".
                       "left join users as users1 on " . user_id() . "= users1.user_id ".
                       "where order_item_id = " . id();

                $mail_sent_to = "\nMail sent to: ";
                
                $res = mysql_query($sql) or dberror($sql);
                
                while ($row = mysql_fetch_assoc($res) and $row["recepient"])
                {
                    $sender_email = $row["sender"];
                    $sender_name =  $row["user_fullname"];
                    $subject = MAIL_SUBJECT_PREFIX . ": Expected arrival has changed - Order " . $row["order_number"];
                    $mail->set_subject($subject);

                    $mail->set_sender($sender_email, $sender_name);

                    $mail->add_recipient($row["recepient"]);

                    $mail->add_recipient($row["rto"]);

                    if($row["cc"])
                    {
                        $mail->add_cc($row["cc"]);
                    }
                    if($row["deputy"])
                    {
                        $mail->add_cc($row["deputy"]);
                    }

                    $mail_sent_to = $mail_sent_to . "\n" . $row["recepient"];

                    $bodytext0 = "The expected arrival has changed for the following order\n" .
                                 "Order: " . $row["order_number"] . "    Item:  " . $row["item_shortcut"] . "\n".
                                 "from " . $form->value("expected_arrival_old_value") . " to " . $form->value("expected_arrival");

                    $num_mails++;
                                                
                }


                $link ="order_view_traffic_data.php?oid=" . $order["order_id"];
                $bodytext = $bodytext0 . "\n\nclick below to have direct access to the order:\n";

                $bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
                
                $mail->add_text($bodytext);
                if($num_mails > 0)
                {
                    $mail->send();


                    append_mail($order["order_id"], "" , user_id(), $bodytext0 . " " . $mail_sent_to , "", 2);
                }
            }
        }

        // arrival date has changed
        if ($form->value("arrival_old_value"))
        {
            if ($form->value("arrival_old_value") != $form->value("arrival"))
            {

                
                $mail = new Mail();
                $num_mails = 0;

                $sql = "select order_id, order_number, order_item_po_number, ".
                       "if(order_item_item <>'', item_code, item_type_name) as item_shortcut, ".
                       "users.user_email as recepient, users.user_email_cc as cc," .
                       "users.user_email_deputy as deputy, " .
                       "users.user_address as address_id, " .
                       "users1.user_email as sender, concat(users1.user_name, ' ', users1.user_firstname) as user_fullname ".
                       "from orders ".
                       "left join order_items on order_item_order = order_id " .
                       "left join items on order_item_item = item_id ".
                       "left join item_types on order_item_type = item_type_id ".
                       "left join users on users.user_id = order_user ".
                       "left join users as users1 on " . user_id() . "= users1.user_id ".
                       "where order_item_id = " . id();

                $mail_sent_to = "\nMail sent to: ";
                
                $res = mysql_query($sql) or dberror($sql);
                while ($row = mysql_fetch_assoc($res) and $row["recepient"])
                {
                    $sender_email = $row["sender"];
                    $sender_name =  $row["user_fullname"];
                    $subject = MAIL_SUBJECT_PREFIX . ": Arrival date has changed - Order " . $row["order_number"];
                    $mail->set_subject($subject);

                    $mail->set_sender($sender_email, $sender_name);

                    $mail->add_recipient($row["recepient"]);

                    if($row["cc"])
                    {
                        $mail->add_cc($row["cc"]);
                    }
                    if($row["deputy"])
                    {
                        $mail->add_cc($row["deputy"]);
                    }

                    $mail_sent_to = $mail_sent_to . "\n" . $row["recepient"];

                    $bodytext0 = "The arrival date has changed for the following order\n" .
                                 "Order: " . $row["order_number"] . "    Item:  " . $row["item_shortcut"] . "\n".
                                 "from " . $form->value("arrival_old_value") . " to " . $form->value("arrival");

                    $num_mails++;
                                                
                }


                $link ="order_view_traffic_data.php?oid=" . $order["order_id"];
                $bodytext = $bodytext0 . "\n\nclick below to have direct access to the order:\n";

                $bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
                
                $mail->add_text($bodytext);
                if($num_mails > 0)
                {
                    $mail->send();

                    append_mail($order["order_id"], "" , user_id(), $bodytext0 . " " . $mail_sent_to , "", 2);
                }
            }
        }

        $link = "order_edit_traffic_data.php?oid=" . param("oid");
        redirect ($link);
    }
}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("Traffic Data: Edit Item");
$form->render();
$page->footer();

?>
