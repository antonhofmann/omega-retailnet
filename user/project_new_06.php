<?php
/********************************************************************

    project_new06.php

    Creation of a new project step 04.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-07-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-07-14
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/



require_once "../include/frame.php";

check_access("can_create_new_projects");

require_once "include/get_functions.php";
require_once "include/save_functions.php";

$user = get_user(user_id());

//save project and send notifications
if((isset($_SESSION["new_project_step_1"])
   and isset($_SESSION["new_project_step_2"])
   and isset($_SESSION["new_project_step_3"])
   and isset($_SESSION["new_project_step_4"])
   and isset($_SESSION["new_project_step_5"]))
   or
   (isset($_SESSION["new_project_step_1"]) and $_SESSION["new_project_step_1"]["project_kind"] == 4 and isset($_SESSION["new_project_step_2"]) and isset($_SESSION["new_project_step_4"]))
   or
   (isset($_SESSION["new_project_step_1"]) and $_SESSION["new_project_step_1"]["project_kind"] == 5  and isset($_SESSION["new_project_step_4"]))
   )
{
	//insert new record into provinces for new city in the POS Location Address
	$province_id = "";
	$place_id = "";
	if(isset($_SESSION["new_project_step_1"]["select_shop_address_province"]))
	{
		if($_SESSION["new_project_step_1"]["select_shop_address_province"] == "999999999")
		{
			$fields = array();
			$values = array();

			$fields[] = "province_country";
			$values[] = dbquote($_SESSION["new_project_step_1"]["shop_address_country"]);

			$fields[] = "province_canton";
			$values[] = dbquote($_SESSION["new_project_step_1"]["shop_address_province"]);

			$fields[] = "date_created";
			$values[] = "current_timestamp";

			$fields[] = "user_created";
			$values[] = dbquote($_SESSION["user_login"]);

			$sql = "insert into provinces (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			mysql_query($sql) or dberror($sql);

			$province_id =  mysql_insert_id();
		}
	}

	//insert new record into places or update place
	if(isset($_SESSION["new_project_step_1"]["select_shop_address_place"]))
	{
		if($_SESSION["new_project_step_1"]["select_shop_address_place"] == "999999999")
		{

			$fields = array();
			$values = array();

			$fields[] = "place_country";
			$values[] = dbquote($_SESSION["new_project_step_1"]["shop_address_country"]);

			if($province_id)
			{
				$fields[] = "place_province";
				$values[] = dbquote($province_id);
			}
			else
			{
				$fields[] = "place_province";
				$values[] = dbquote($_SESSION["new_project_step_1"]["select_shop_address_province"]);
			}
			

			$fields[] = "place_name";
			$values[] = dbquote($_SESSION["new_project_step_1"]["shop_address_place"]);

			$fields[] = "date_created";
			$values[] = "current_timestamp";

			$fields[] = "user_created";
			$values[] = dbquote($_SESSION["user_login"]);

			$sql = "insert into places (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			mysql_query($sql) or dberror($sql);


			$place_id =  mysql_insert_id();
			
			if(!$_SESSION["new_project_step_5"]["delivery_address_place_id"]) {
				$_SESSION["new_project_step_5"]["delivery_address_place_id"] = $place_id;
			}
		}
		else // update places with a new or another province
		{
			$fields = array();
			$values = array();

			if($province_id)
			{
				$value = dbquote($province_id);
			}
			else
			{
				$value = dbquote($_SESSION["new_project_step_1"]["select_shop_address_province"]);
			}
			$fields[] = "place_province = " . $value;
			
			$value = dbquote(date("Y-m-d"));
			$fields[] = "date_modified = " . $value;

			$value = dbquote(user_login());
			$fields[] = "user_modified = " . $value;

			$sql = "update places set " . join(", ", $fields) . " where place_id = " . $_SESSION["new_project_step_1"]["select_shop_address_place"];
			mysql_query($sql) or dberror($sql);
		}
	}
	else // update places with a new or another province
	{
		if(isset($_SESSION["new_project_step_1"]["select_shop_address_place"]))
		{
			$place_id = "";
			$sql = "select place_id from places where place_id = " . dbquote($_SESSION["new_project_step_1"]["select_shop_address_place"]);
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$place_id = $row["place_id"];
			}

			$fields = array();
			$values = array();

			if($province_id)
			{
				$value = dbquote($province_id);
			}
			else
			{
				$value = dbquote($_SESSION["new_project_step_1"]["select_shop_address_province"]);
			}
			$fields[] = "place_province = " . $value;
			
			$value = dbquote(date("Y-m-d"));
			$fields[] = "date_modified = " . $value;

			$value = dbquote(user_login());
			$fields[] = "user_modified = " . $value;

			$sql = "update places set " . join(", ", $fields) . " where place_id = " . dbquote($place_id);
			mysql_query($sql) or dberror($sql);
		}
	}


	//insert new record into provinces for new city in the Franchisee Address
	$province_id2 = "";
	$place_id2 = "";
	$new_province_inserted = false;
	if(isset($_SESSION["new_project_step_2"]["franchisee_address_province_id"]))
	{
		if($_SESSION["new_project_step_2"]["franchisee_address_province_id"] == "999999999")
		{
			
			$sql = "select province_id from provinces " . 
				   "where province_country = " . dbquote($_SESSION["new_project_step_2"]["franchisee_address_country"]) . 
				   " and province_canton = " . dbquote($_SESSION["new_project_step_2"]["franchisee_address_province_id"]);
			
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$province_id2 = $row["province_id"];
			}
			elseif($_SESSION["new_project_step_1"]["shop_address_province"] == $_SESSION["new_project_step_2"]["franchisee_address_province"]) 
			{
				$province_id2 = $province_id;
			}
			else
			{
				$fields = array();
				$values = array();

				$fields[] = "province_country";
				$values[] = dbquote($_SESSION["new_project_step_2"]["franchisee_address_country"]);

				$fields[] = "province_canton";
				$values[] = dbquote($_SESSION["new_project_step_2"]["franchisee_address_province"]);

				$fields[] = "date_created";
				$values[] = "current_timestamp";

				$fields[] = "user_created";
				$values[] = dbquote($_SESSION["user_login"]);

				$sql = "insert into provinces (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);

				$province_id2 =  mysql_insert_id();
				$new_province_inserted = true;
			}
		}
	}

	//insert new record into places or update place
	if(isset($_SESSION["new_project_step_2"]["franchisee_address_place_id"]))
	{
		if($_SESSION["new_project_step_2"]["franchisee_address_place_id"] == "999999999")
		{
			
			
			if($_SESSION["new_project_step_2"]["franchisee_address_place"] == $_SESSION["new_project_step_1"]["shop_address_place"])
			{
				$place_id2 = $place_id;
			}
			else
			{
				$fields = array();
				$values = array();

				$fields[] = "place_country";
				$values[] = dbquote($_SESSION["new_project_step_2"]["franchisee_address_country"]);

				if($province_id2)
				{
					$fields[] = "place_province";
					$values[] = dbquote($province_id2);
				}
				else
				{
					$fields[] = "place_province";
					$values[] = dbquote($_SESSION["new_project_step_2"]["franchisee_address_province_id"]);
				}
				

				$fields[] = "place_name";
				$values[] = dbquote($_SESSION["new_project_step_2"]["franchisee_address_place"]);

				$fields[] = "date_created";
				$values[] = "current_timestamp";

				$fields[] = "user_created";
				$values[] = dbquote($_SESSION["user_login"]);

				$sql = "insert into places (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);


				$place_id2 =  mysql_insert_id();
			}

		}
		else // update places with a new or another province
		{
			$fields = array();
			$values = array();

			if($province_id2)
			{
				$value = dbquote($province_id2);
			}
			else
			{
				$value = dbquote($_SESSION["new_project_step_2"]["franchisee_address_province_id"]);
			}
			$fields[] = "place_province = " . $value;
			
			$value = dbquote(date("Y-m-d"));
			$fields[] = "date_modified = " . $value;

			$value = dbquote(user_login());
			$fields[] = "user_modified = " . $value;

			$sql = "update places set " . join(", ", $fields) . " where place_id = " . $_SESSION["new_project_step_2"]["franchisee_address_place_id"];
			mysql_query($sql) or dberror($sql);
		}
	}
	else // update places with a new or another province
	{
		if(isset($_SESSION["new_project_step_2"]["franchisee_address_province_id"]))
		{
			$place_id2 = "";
			$sql = "select place_id from places where place_id = " . dbquote($_SESSION["new_project_step_2"]["franchisee_address_place_id"]);
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$place_id2 = $row["place_id"];
			}

			$fields = array();
			$values = array();

			if($province_id2)
			{
				$value = dbquote($province_id2);
			}
			else
			{
				$value = dbquote($_SESSION["new_project_step_2"]["franchisee_address_province_id"]);
			}
			$fields[] = "place_province = " . $value;
			
			$value = dbquote(date("Y-m-d"));
			$fields[] = "date_modified = " . $value;

			$value = dbquote(user_login());
			$fields[] = "user_modified = " . $value;

			$sql = "update places set " . join(", ", $fields) . " where place_id = " . dbquote($place_id2);
			mysql_query($sql) or dberror($sql);
		}
	}


	//insert new record into places for new city in the Delivery Address
	$province_id3 = "";
	$place_id3 = "";
	$new_province_inserted = false;

	

	if(isset($_SESSION["new_project_step_5"]["delivery_address_place_id"]))
	{

		if($_SESSION["new_project_step_5"]["delivery_address_place_id"] == "999999999")
		{
			if(array_key_exists("franchisee_address_place", $_SESSION["new_project_step_2"])) 
			{
				if($_SESSION["new_project_step_2"]["franchisee_address_place"] != $_SESSION["new_project_step_5"]["delivery_address_place"]) {
				
					$country_id = $_SESSION["new_project_step_5"]["delivery_address_country"];
					$province_id = $_SESSION["new_project_step_5"]["delivery_address_province_id"];
					$place_name = $_SESSION["new_project_step_5"]["delivery_address_place"];

					$sql = "insert into places (place_country, place_province, place_name, date_created, user_created) Values (" . 
						   $country_id .  "," . dbquote($province_id) . "," . dbquote($place_name) . "," . dbquote(date('Y-m-d')) . "," . dbquote(user_login()) . ")";

					$result = mysql_query($sql) or dberror($sql);

					$_SESSION["new_project_step_5"]["delivery_address_place_id"] = mysql_insert_id();

				}
				else
				{
					$_SESSION["new_project_step_5"]["delivery_address_place_id"] = $place_id2;
				}
			}
			else
			{
				if($_SESSION["new_project_step_1"]["shop_address_place"] != $_SESSION["new_project_step_5"]["delivery_address_place"]) {
				
					$country_id = $_SESSION["new_project_step_5"]["delivery_address_country"];
					$province_id = $_SESSION["new_project_step_5"]["delivery_address_province_id"];
					$place_name = $_SESSION["new_project_step_5"]["delivery_address_place"];

					$sql = "insert into places (place_country, place_province, place_name, date_created, user_created) Values (" . 
						   $country_id .  "," . dbquote($province_id) . "," . dbquote($place_name) . "," . dbquote(date('Y-m-d')) . "," . dbquote(user_login()) . ")";

					$result = mysql_query($sql) or dberror($sql);

					$_SESSION["new_project_step_5"]["delivery_address_place_id"] = mysql_insert_id();

				}
			}
		}
	}

	
	//update client address
	if(isset($_SESSION["new_project_step_2"]["client_address_company"]) and $_SESSION["new_project_step_2"]["client_address_place_id"] > 0)
	{
		$fields = array();
		$values = array();

		$value = dbquote($_SESSION["new_project_step_2"]["client_address_company"]);
		$fields[] = "address_company = " . $value;

		$value = dbquote($_SESSION["new_project_step_2"]["client_address_company2"]);
		$fields[] = "address_company2 = " . $value;

		$value = dbquote($_SESSION["new_project_step_2"]["client_address_address"]);
		$fields[] = "address_address = " . $value;

		$value = dbquote($_SESSION["new_project_step_2"]["client_address_address2"]);
		$fields[] = "address_address2 = " . $value;

		$value = dbquote($_SESSION["new_project_step_2"]["client_address_zip"]);
		$fields[] = "address_zip = " . $value;
		

		$value = dbquote($_SESSION["new_project_step_2"]["client_address_place_id"]);
		$fields[] = "address_place_id = " . $value;

		$place_name = '';
		$sql = "select place_name from places where place_id = " . dbquote($_SESSION["new_project_step_2"]["client_address_place_id"]);
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$place_name = $row['place_name'];
		}

		$value = dbquote($place_name);
		$fields[] = "address_place = " . $value;


		$value = dbquote($_SESSION["new_project_step_2"]["client_address_country"]);
		$fields[] = "address_country = " . $value;

		$value = dbquote($_SESSION["new_project_step_2"]["client_address_phone"]);
		$fields[] = "address_phone = " . $value;

		$value = dbquote($_SESSION["new_project_step_2"]["client_address_fax"]);
		$fields[] = "address_fax = " . $value;

		$value = dbquote($_SESSION["new_project_step_2"]["client_address_email"]);
		$fields[] = "address_email = " . $value;

		$value = dbquote($_SESSION["new_project_step_2"]["client_address_contact_name"]);
		$fields[] = "address_contact_name = " . $value;

		$value = dbquote(date("Y-m-d"));
		$fields[] = "date_modified = " . $value;

		$value = dbquote(user_login());
		$fields[] = "user_modified = " . $value;

		$sql = "update addresses set " . join(", ", $fields) . " where address_id = " . $_SESSION["new_project_step_2"]["client_address_id"];
		mysql_query($sql) or dberror($sql);
	}


	//update standard delivery address
	if(isset($_SESSION["new_project_step_5"]["delivery_address_id"]) and $_SESSION["new_project_step_5"]["delivery_address_id"] > 0)
	{
		$fields = array();
		$values = array();

		$value = dbquote($_SESSION["new_project_step_5"]["delivery_address_company"]);
		$fields[] = "delivery_address_company = " . $value;

		$value = dbquote($_SESSION["new_project_step_5"]["delivery_address_company2"]);
		$fields[] = "delivery_address_company2 = " . $value;

		$value = dbquote($_SESSION["new_project_step_5"]["delivery_address_address"]);
		$fields[] = "delivery_address_address = " . $value;

		$value = dbquote($_SESSION["new_project_step_5"]["delivery_address_address2"]);
		$fields[] = "delivery_address_address2 = " . $value;

		$value = dbquote($_SESSION["new_project_step_5"]["delivery_address_zip"]);
		$fields[] = "delivery_address_zip = " . $value;

		$value = dbquote(trim($_SESSION["new_project_step_5"]["delivery_address_place"]));
		$fields[] = "delivery_address_place = " . $value;

		$value = dbquote(trim($_SESSION["new_project_step_5"]["delivery_address_place_id"]));
		$fields[] = "delivery_address_place_id = " . $value;

		$value = dbquote($_SESSION["new_project_step_5"]["delivery_address_country"]);
		$fields[] = "delivery_address_country = " . $value;

		$value = dbquote($_SESSION["new_project_step_5"]["delivery_address_phone"]);
		$fields[] = "delivery_address_phone = " . $value;

		$value = dbquote($_SESSION["new_project_step_5"]["delivery_address_fax"]);
		$fields[] = "delivery_address_fax = " . $value;

		$value = dbquote($_SESSION["new_project_step_5"]["delivery_address_email"]);
		$fields[] = "delivery_address_email = " . $value;

		$value = dbquote($_SESSION["new_project_step_5"]["delivery_address_contact"]);
		$fields[] = "delivery_address_contact = " . $value;

		$value = dbquote(date("Y-m-d"));
		$fields[] = "date_modified = " . $value;

		$value = dbquote(user_login());
		$fields[] = "user_modified = " . $value;

		$sql = "update standard_delivery_addresses set " . join(", ", $fields) . " where delivery_address_id = " . $_SESSION["new_project_step_5"]["delivery_address_id"];
		mysql_query($sql) or dberror($sql);

	}

	//franchisee
	//new franchisee address
	if(($_SESSION["new_project_step_1"]["project_cost_type"] == 2 or $_SESSION["new_project_step_1"]["project_cost_type"] == 5 or $_SESSION["new_project_step_1"]["project_cost_type"] == 6) and $_SESSION["new_project_step_2"]["franchisee_address_id"] == 999999999)
	{
		$fields = array();
		$values = array();

		$fields[] = "address_shortcut";
		$values[] = dbquote(substr(strtolower($_SESSION["new_project_step_2"]["franchisee_address_company"]), 0, 15));

		$fields[] = "address_company";
		$values[] = dbquote($_SESSION["new_project_step_2"]["franchisee_address_company"]);

		$fields[] = "address_company2";
		$values[] = dbquote($_SESSION["new_project_step_2"]["franchisee_address_company2"]);

		$fields[] = "address_address";
		$values[] = dbquote($_SESSION["new_project_step_2"]["franchisee_address_address"]);

		$fields[] = "address_address2";
		$values[] = dbquote($_SESSION["new_project_step_2"]["franchisee_address_address2"]);

		$fields[] = "address_zip";
		$values[] = dbquote($_SESSION["new_project_step_2"]["franchisee_address_zip"]);

		$fields[] = "address_place";
		$values[] = dbquote(trim($_SESSION["new_project_step_2"]["franchisee_address_place"]));

		if(isset($_SESSION["new_project_step_2"]["franchisee_address_id"]))
		{
			if($place_id2)
			{
				$fields[] = "address_place_id";
				$values[] = dbquote($place_id2);
			}
			else
			{
				$fields[] = "address_place_id";
				$values[] = dbquote($_SESSION["new_project_step_2"]["franchisee_address_place_id"]);
			}
		}

		$fields[] = "address_country";
		$values[] = dbquote($_SESSION["new_project_step_2"]["franchisee_address_country"]);

		$fields[] = "address_phone";
		$values[] = dbquote($_SESSION["new_project_step_2"]["franchisee_address_phone"]);

		$fields[] = "address_fax";
		$values[] = dbquote($_SESSION["new_project_step_2"]["franchisee_address_fax"]);

		$fields[] = "address_email";
		$values[] = dbquote($_SESSION["new_project_step_2"]["franchisee_address_email"]);

		$fields[] = "address_contact_name";
		$values[] = dbquote($_SESSION["new_project_step_2"]["franchisee_address_contact_name"]);

		$fields[] = "address_website";
		$values[] = dbquote($_SESSION["new_project_step_2"]["franchisee_address_website"]);


		
		$currency = 0;
		$sql = "select country_currency from countries where country_id = " . dbquote($_SESSION["new_project_step_2"]["franchisee_address_country"]);
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$currency = $row['country_currency'];
		}

		$fields[] = "address_currency";
		$values[] = dbquote($currency);

		$fields[] = "address_type";
		$values[] = 7;

		if($_SESSION["new_project_step_1"]["project_cost_type"] == 6)
		{
			$fields[] = "address_is_independent_retailer";
			$values[] = 1;
		}
		else
		{
			$fields[] = "address_canbefranchisee";
			$values[] = 1;
		}

		$fields[] = "address_showinposindex";
		$values[] = 1;

		$fields[] = "address_active";
		$values[] = 1;

		$fields[] = "address_parent";
		$values[] = $user["address"];

		$fields[] = "address_checked";
		$values[] = "0";

		$fields[] = "date_created";
		$values[] = "current_timestamp";

		$fields[] = "date_modified";
		$values[] = "current_timestamp";

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$fields[] = "user_modified";
		$values[] = dbquote(user_login());

		$sql = "insert into addresses (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
		mysql_query($sql) or dberror($sql);

		$franchisee_id = mysql_insert_id();
	}
	elseif(($_SESSION["new_project_step_1"]["project_cost_type"] == 2 or $_SESSION["new_project_step_1"]["project_cost_type"] == 5  or $_SESSION["new_project_step_1"]["project_cost_type"] == 6) and $_SESSION["new_project_step_2"]["franchisee_address_id"] > 0)
	{
		//update franchisee-address
		
		if(array_key_exists("franchisee_address_company", $_SESSION["new_project_step_2"]) and $_SESSION["new_project_step_2"]["franchisee_address_company"])
		{
			$fields = array();
			$values = array();

			$value = dbquote($_SESSION["new_project_step_2"]["franchisee_address_company"]);
			$fields[] = "address_company = " . $value;

			$value = dbquote($_SESSION["new_project_step_2"]["franchisee_address_company2"]);
			$fields[] = "address_company2 = " . $value;

			$value = dbquote($_SESSION["new_project_step_2"]["franchisee_address_address"]);
			$fields[] = "address_address = " . $value;

			$value = dbquote($_SESSION["new_project_step_2"]["franchisee_address_address2"]);
			$fields[] = "address_address2 = " . $value;

			$value = dbquote($_SESSION["new_project_step_2"]["franchisee_address_zip"]);
			$fields[] = "address_zip = " . $value;

			if(isset($_SESSION["new_project_step_2"]["franchisee_address_place_id"]))
			{
				if($place_id2)
				{
					$value = dbquote($place_id2);
					$fields[] = "address_place_id = " . $value;
				}
				else
				{
					$value = dbquote($_SESSION["new_project_step_2"]["franchisee_address_place_id"]);
					$fields[] = "address_place_id = " . $value;
				}
			}
			
			$value = dbquote(trim($_SESSION["new_project_step_2"]["franchisee_address_place"]));
			$fields[] = "address_place = " . $value;

			$value = dbquote($_SESSION["new_project_step_2"]["franchisee_address_country"]);
			$fields[] = "address_country = " . $value;

			$value = dbquote($_SESSION["new_project_step_2"]["franchisee_address_phone"]);
			$fields[] = "address_phone = " . $value;

			$value = dbquote($_SESSION["new_project_step_2"]["franchisee_address_fax"]);
			$fields[] = "address_fax = " . $value;

			$value = dbquote($_SESSION["new_project_step_2"]["franchisee_address_email"]);
			$fields[] = "address_email = " . $value;

			$value = dbquote($_SESSION["new_project_step_2"]["franchisee_address_contact_name"]);
			$fields[] = "address_contact_name = " . $value;

			$value = dbquote($_SESSION["new_project_step_2"]["franchisee_address_website"]);
			$fields[] = "address_website = " . $value;

			$value = dbquote(date("Y-m-d"));
			$fields[] = "date_modified = " . $value;

			$value = dbquote(user_login());
			$fields[] = "user_modified = " . $value;

			$sql = "update addresses set " . join(", ", $fields) . " where address_id = " . $_SESSION["new_project_step_2"]["franchisee_address_id"];
			mysql_query($sql) or dberror($sql);
		}

		$franchisee_id = $_SESSION["new_project_step_2"]["franchisee_address_id"];
		
		if(!$franchisee_id)
		{
			$franchisee_id = $user["address"];
		}
	}
	elseif(($_SESSION["new_project_step_1"]["project_cost_type"] == 1 or $_SESSION["new_project_step_1"]["project_cost_type"] == 3 or $_SESSION["new_project_step_1"]["project_cost_type"] == 4)) //corporate store, joint venture or cooperation 3rd party
	{
		if(isset($_SESSION["new_project_step_2"]["franchisee_address_id"]) and $_SESSION["new_project_step_2"]["franchisee_address_id"] > 0)
		{
			//update franchisee-address
			if(array_key_exists("franchisee_address_company", $_SESSION["new_project_step_2"]))
			{
				$fields = array();
				$values = array();

				$value = dbquote($_SESSION["new_project_step_2"]["franchisee_address_company"]);
				$fields[] = "address_company = " . $value;

				$value = dbquote($_SESSION["new_project_step_2"]["franchisee_address_company2"]);
				$fields[] = "address_company2 = " . $value;

				$value = dbquote($_SESSION["new_project_step_2"]["franchisee_address_address"]);
				$fields[] = "address_address = " . $value;

				$value = dbquote($_SESSION["new_project_step_2"]["franchisee_address_address2"]);
				$fields[] = "address_address2 = " . $value;

				$value = dbquote($_SESSION["new_project_step_2"]["franchisee_address_zip"]);
				$fields[] = "address_zip = " . $value;

				if(isset($_SESSION["new_project_step_2"]["franchisee_address_place_id"]))
				{
					if($place_id2)
					{
						$value = dbquote($place_id2);
						$fields[] = "address_place_id = " . $value;
					}
					else
					{
						$value = dbquote($_SESSION["new_project_step_2"]["franchisee_address_place_id"]);
						$fields[] = "address_place_id = " . $value;
					}
				}
				
				$value = dbquote(trim($_SESSION["new_project_step_2"]["franchisee_address_place"]));
				$fields[] = "address_place = " . $value;

				$value = dbquote($_SESSION["new_project_step_2"]["franchisee_address_country"]);
				$fields[] = "address_country = " . $value;

				$value = dbquote($_SESSION["new_project_step_2"]["franchisee_address_phone"]);
				$fields[] = "address_phone = " . $value;

				$value = dbquote($_SESSION["new_project_step_2"]["franchisee_address_fax"]);
				$fields[] = "address_fax = " . $value;

				$value = dbquote($_SESSION["new_project_step_2"]["franchisee_address_email"]);
				$fields[] = "address_email = " . $value;

				$value = dbquote($_SESSION["new_project_step_2"]["franchisee_address_contact_name"]);
				$fields[] = "address_contact_name = " . $value;

				$value = dbquote($_SESSION["new_project_step_2"]["franchisee_address_website"]);
				$fields[] = "address_website = " . $value;

				$value = dbquote(date("Y-m-d"));
				$fields[] = "date_modified = " . $value;

				$value = dbquote(user_login());
				$fields[] = "user_modified = " . $value;

				$sql = "update addresses set " . join(", ", $fields) . " where address_id = " . $_SESSION["new_project_step_2"]["franchisee_address_id"];
				mysql_query($sql) or dberror($sql);
			}

			$franchisee_id = $_SESSION["new_project_step_2"]["franchisee_address_id"];
			
			if(!$franchisee_id)
			{
				$franchisee_id = $user["address"];
			}
		}
		else
		{
			$franchisee_id = $user["address"];
		}
	}

	//order
	$order_fields = array();
    $order_values = array();
    
    $delivery_address_fields = array();
    $delivery_address_values = array();

    $project_fields = array();
    $project_values = array();

    $project_item_fields = array();
    $project_item_values = array();
	
    // create project number project_kind

    $project_number = project_create_project_number($user["address"], $_SESSION["new_project_step_1"]["project_postype"], $_SESSION["new_project_step_1"]["project_kind"]);

    // get currency details
    $currency = get_address_currency($user["address"]);
    $system_currency = get_system_currency_fields();

    //get standard roles
	$standardroles = get_project_standard_roles($user["address"], $_SESSION["new_project_step_1"]["project_postype"], $_SESSION["new_project_step_1"]["project_cost_type"]);
	
	if($_SESSION["new_project_step_1"]["project_kind"] == 4 or $_SESSION["new_project_step_1"]["project_kind"] == 5) 
	{
		$standard_design_supervisor = 0;
		$standard_retail_coordinator = 0;
		$standard_retail_operator = 0;
		$standard_cms_approver = 0;
		$standard_local_retail_coordinator = user_id();
	}
	else
	{
		$standard_design_supervisor = $standardroles['dsup'];
		$standard_retail_coordinator = $standardroles['rtco'];
		$standard_retail_operator = $standardroles['rtop'];
		$standard_cms_approver = $standardroles['cms_approver'];
		$standard_local_retail_coordinator = user_id();
	}




	// insert record into table orders

    $order_fields[] = "order_number";
    $order_values[] = dbquote($project_number);

    $order_fields[] = "order_date";
    $order_values[] = "current_timestamp";

    $order_fields[] = "order_type";
    $order_values[] = 1;

	if($standard_retail_operator)
    {
        $order_fields[] = "order_retail_operator";
        $order_values[] = $standard_retail_operator;
    }

    $order_fields[] = "order_client_address";
    $order_values[] = $user["address"];


    $order_fields[] = "order_client_currency";
    $order_values[] = $currency["id"];

    $order_fields[] = "order_system_currency";
    $order_values[] = $system_currency["id"];

    $order_fields[] = "order_client_exchange_rate";
    $order_values[] = $currency["exchange_rate"];

    $order_fields[] = "order_system_exchange_rate";
    $order_values[] =  $system_currency["exchange_rate"];

    $order_fields[] = "order_user";
    $order_values[] = user_id();

    if($_SESSION["new_project_step_5"] != "")
	{
		$order_fields[] = "order_preferred_transportation_arranged";
		$order_values[] = trim($_SESSION["new_project_step_5"]["preferred_transportation_arranged"]) == "" ? "null" : dbquote($_SESSION["new_project_step_5"]["preferred_transportation_arranged"]);

		$order_fields[] = "order_preferred_transportation_mode";
		$order_values[] = trim($_SESSION["new_project_step_5"]["preferred_transportation_mode"]) == "" ? "null" : dbquote($_SESSION["new_project_step_5"]["preferred_transportation_mode"]);
	}
	else
	{
		$order_fields[] = "order_preferred_transportation_arranged";
		$order_values[] = "0";

		$order_fields[] = "order_preferred_transportation_mode";
		$order_values[] = "0";
	}

    
	if(isset($_SESSION["new_project_step_3"]["voltage"]))
	{
		$order_fields[] = "order_voltage";
		$order_values[] = trim($_SESSION["new_project_step_3"]["voltage"]) == "" ? "null" : dbquote($_SESSION["new_project_step_3"]["voltage"]);
	}

	if($_SESSION["new_project_step_5"] != "")
	{
		$order_fields[] = "order_preferred_delivery_date";
		$order_values[] = dbquote(from_system_date($_SESSION["new_project_step_5"]["preferred_delivery_date"]), true);
	}
	else
	{
		$order_fields[] = "order_preferred_delivery_date";
		$order_values[] = dbquote(date("Y-m-d"));
	}
	
	if($_SESSION["new_project_step_5"] != "")
	{	
		$order_fields[] = "order_full_delivery";
		$order_values[] = trim($_SESSION["new_project_step_5"]["full_delivery"]) == "" ? "null" : dbquote($_SESSION["new_project_step_5"]["full_delivery"]);

		//$order_fields[] = "order_packaging_retraction";
		//$order_values[] = trim($_SESSION["new_project_step_5"]["packaging_retraction"]) == "" ? "null" : dbquote($_SESSION["new_project_step_5"]["packaging_retraction"]);

		$order_fields[] = "order_pedestrian_mall_approval";
		$order_values[] = trim($_SESSION["new_project_step_5"]["pedestrian_mall_approval"]) == "" ? "null" : dbquote($_SESSION["new_project_step_5"]["pedestrian_mall_approval"]);

		$order_fields[] = "order_delivery_comments";
		$order_values[] = trim($_SESSION["new_project_step_5"]["delivery_comments"]) == "" ? "null" : dbquote($_SESSION["new_project_step_5"]["delivery_comments"]);

		$order_fields[] = "order_insurance";
		$order_values[] = dbquote($_SESSION["new_project_step_5"]["order_insurance"]);
	}
    
	// franchisee address
	$order_fields[] = "order_franchisee_address_id";
	$order_values[] = dbquote($franchisee_id);

	if(array_key_exists("new_project_step_2", $_SESSION) and array_key_exists("franchisee_address_company", $_SESSION["new_project_step_2"])) {
		$order_fields[] = "order_franchisee_address_company";
		$order_values[] = trim($_SESSION["new_project_step_2"]["franchisee_address_company"]) == "" ? "null" : dbquote($_SESSION["new_project_step_2"]["franchisee_address_company"]);

		$order_fields[] = "order_franchisee_address_company2";
		$order_values[] = trim($_SESSION["new_project_step_2"]["franchisee_address_company2"]) == "" ? "null" : dbquote($_SESSION["new_project_step_2"]["franchisee_address_company2"]);

		$order_fields[] = "order_franchisee_address_address";
		$order_values[] = trim($_SESSION["new_project_step_2"]["franchisee_address_address"]) == "" ? "null" : dbquote($_SESSION["new_project_step_2"]["franchisee_address_address"]);

		$order_fields[] = "order_franchisee_address_address2";
		$order_values[] = trim($_SESSION["new_project_step_2"]["franchisee_address_address2"]) == "" ? "null" : dbquote($_SESSION["new_project_step_2"]["franchisee_address_address2"]);

		$order_fields[] = "order_franchisee_address_zip";
		$order_values[] = trim($_SESSION["new_project_step_2"]["franchisee_address_zip"]) == "" ? "null" : dbquote($_SESSION["new_project_step_2"]["franchisee_address_zip"]);

		$order_fields[] = "order_franchisee_address_place";
		$order_values[] = trim($_SESSION["new_project_step_2"]["franchisee_address_place"]) == "" ? "null" : dbquote($_SESSION["new_project_step_2"]["franchisee_address_place"]);

		$order_fields[] = "order_franchisee_address_country";
		$order_values[] = trim($_SESSION["new_project_step_2"]["franchisee_address_country"]) == "" ? "null" : dbquote($_SESSION["new_project_step_2"]["franchisee_address_country"]);

		$order_fields[] = "order_franchisee_address_phone";
		$order_values[] = trim($_SESSION["new_project_step_2"]["franchisee_address_phone"]) == "" ? "null" : dbquote($_SESSION["new_project_step_2"]["franchisee_address_phone"]);

		$order_fields[] = "order_franchisee_address_fax";
		$order_values[] = trim($_SESSION["new_project_step_2"]["franchisee_address_fax"]) == "" ? "null" : dbquote($_SESSION["new_project_step_2"]["franchisee_address_fax"]);

		$order_fields[] = "order_franchisee_address_email";
		$order_values[] = trim($_SESSION["new_project_step_2"]["franchisee_address_email"]) == "" ? "null" : dbquote($_SESSION["new_project_step_2"]["franchisee_address_email"]);

		$order_fields[] = "order_franchisee_address_contact";
		$order_values[] = trim($_SESSION["new_project_step_2"]["franchisee_address_contact_name"]) == "" ? "null" : dbquote($_SESSION["new_project_step_2"]["franchisee_address_contact_name"]);

		$order_fields[] = "order_franchisee_address_website";
		$order_values[] = trim($_SESSION["new_project_step_2"]["franchisee_address_website"]) == "" ? "null" : dbquote($_SESSION["new_project_step_2"]["franchisee_address_website"]);
	}
	elseif(array_key_exists("new_project_step_2", $_SESSION))
	{
		$sql_a = "select * from addresses where address_id = " . $_SESSION["new_project_step_2"]["client_address_id"];
		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
			$order_fields[] = "order_franchisee_address_company";
			$order_values[] = dbquote($row_a["address_company"]);

			$order_fields[] = "order_franchisee_address_company2";
			$order_values[] = dbquote($row_a["address_company2"]);

			$order_fields[] = "order_franchisee_address_address";
			$order_values[] = dbquote($row_a["address_address"]);

			$order_fields[] = "order_franchisee_address_address2";
			$order_values[] = dbquote($row_a["address_address2"]);

			$order_fields[] = "order_franchisee_address_zip";
			$order_values[] = dbquote($row_a["address_zip"]);

			$order_fields[] = "order_franchisee_address_place";
			$order_values[] = dbquote(trim($row_a["address_place"]));

			$order_fields[] = "order_franchisee_address_country";
			$order_values[] = dbquote($row_a["address_country"]);

			$order_fields[] = "order_franchisee_address_phone";
			$order_values[] = dbquote($row_a["address_phone"]);

			$order_fields[] = "order_franchisee_address_fax";
			$order_values[] = dbquote($row_a["address_fax"]);

			$order_fields[] = "order_franchisee_address_email";
			$order_values[] = dbquote($row_a["address_email"]);

			$order_fields[] = "order_franchisee_address_contact";
			$order_values[] = dbquote($row_a["address_contact_name"]);
		}
	}
	else
	{
		$sql_a = "select * from posaddresses " .
		         "left join addresses on address_id = posaddress_franchisee_id " . 
				 "where posaddress_id = " . $_SESSION["new_project_step_1"]["posaddress_id"];
		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
			$order_fields[] = "order_franchisee_address_company";
			$order_values[] = dbquote($row_a["address_company"]);

			$order_fields[] = "order_franchisee_address_company2";
			$order_values[] = dbquote($row_a["address_company2"]);

			$order_fields[] = "order_franchisee_address_address";
			$order_values[] = dbquote($row_a["address_address"]);

			$order_fields[] = "order_franchisee_address_address2";
			$order_values[] = dbquote($row_a["address_address2"]);

			$order_fields[] = "order_franchisee_address_zip";
			$order_values[] = dbquote($row_a["address_zip"]);

			$order_fields[] = "order_franchisee_address_place";
			$order_values[] = dbquote(trim($row_a["address_place"]));

			$order_fields[] = "order_franchisee_address_country";
			$order_values[] = dbquote($row_a["address_country"]);

			$order_fields[] = "order_franchisee_address_phone";
			$order_values[] = dbquote($row_a["address_phone"]);

			$order_fields[] = "order_franchisee_address_fax";
			$order_values[] = dbquote($row_a["address_fax"]);

			$order_fields[] = "order_franchisee_address_email";
			$order_values[] = dbquote($row_a["address_email"]);

			$order_fields[] = "order_franchisee_address_contact";
			$order_values[] = dbquote($row_a["address_contact_name"]);
		}
	}


	$order_fields[] = "order_direct_invoice_address_id";
	$order_values[] = dbquote($_SESSION["new_project_step_5"]["invoice_address_id"]);
	
		
	if(array_key_exists("new_project_step_2", $_SESSION))
	{
		$sql_a = "select * from addresses where address_id = " . $_SESSION["new_project_step_2"]["client_address_id"];
		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
			if($row_a["address_invoice_recipient"] > 0)
			{
				$sql_a = "select * from addresses where address_id = " . $row_a["address_invoice_recipient"];
			}
		}

	}
	else
	{
		$sql_a = "select * from posaddresses " .
		         "left join addresses on address_id = posaddress_client_id " . 
				 "where posaddress_id = " . $_SESSION["new_project_step_1"]["posaddress_id"];
	}
	$res_a = mysql_query($sql_a) or dberror($sql_a);
	if ($row_a = mysql_fetch_assoc($res_a))
	{
		$order_fields[] = "order_billing_address_company";
		$order_values[] = dbquote($row_a["address_company"]);

		$order_fields[] = "order_billing_address_company2";
		$order_values[] = dbquote($row_a["address_company2"]);

		$order_fields[] = "order_billing_address_address";
		$order_values[] = dbquote($row_a["address_address"]);

		$order_fields[] = "order_billing_address_address2";
		$order_values[] = dbquote($row_a["address_address2"]);

		$order_fields[] = "order_billing_address_zip";
		$order_values[] = dbquote($row_a["address_zip"]);

		
		$order_fields[] = "order_billing_address_place_id";
		$order_values[] = dbquote(trim($row_a["address_place_id"]));

		$order_fields[] = "order_billing_address_place";
		$order_values[] = dbquote(trim($row_a["address_place"]));

		$order_fields[] = "order_billing_address_country";
		$order_values[] = dbquote($row_a["address_country"]);

		$order_fields[] = "order_billing_address_phone";
		$order_values[] = dbquote($row_a["address_phone"]);

		$order_fields[] = "order_billing_address_fax";
		$order_values[] = dbquote($row_a["address_fax"]);

		$order_fields[] = "order_billing_address_email";
		$order_values[] = dbquote($row_a["address_email"]);

		
		$sql_a = "select concat(user_name, ' ', user_firstname) as username from users where user_id = " . user_id();
		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
			$order_fields[] = "order_billing_address_contact";
			$order_values[] = dbquote($row_a["username"]);
		}
	}


    // POS address
    $order_fields[] = "order_shop_address_company";
    $order_values[] = trim($_SESSION["new_project_step_1"]["shop_address_company"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_company"]);

    $order_fields[] = "order_shop_address_company2";
    $order_values[] = trim($_SESSION["new_project_step_1"]["shop_address_company2"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_company2"]);

    $order_fields[] = "order_shop_address_address";
    $order_values[] = trim($_SESSION["new_project_step_1"]["shop_address_address"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_address"]);

    $order_fields[] = "order_shop_address_address2";
    $order_values[] = trim($_SESSION["new_project_step_1"]["shop_address_address2"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_address2"]);

    $order_fields[] = "order_shop_address_zip";
    $order_values[] = trim($_SESSION["new_project_step_1"]["shop_address_zip"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_zip"]);

    $order_fields[] = "order_shop_address_place";
    $order_values[] = trim($_SESSION["new_project_step_1"]["shop_address_place"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_place"]);

    $order_fields[] = "order_shop_address_country";
    $order_values[] = trim($_SESSION["new_project_step_1"]["shop_address_country"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_country"]);

    $order_fields[] = "order_shop_address_phone";
    $order_values[] = trim($_SESSION["new_project_step_1"]["shop_address_phone"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_phone"]);

    $order_fields[] = "order_shop_address_fax";
    $order_values[] = trim($_SESSION["new_project_step_1"]["shop_address_fax"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_fax"]);

    $order_fields[] = "order_shop_address_email";
    $order_values[] = trim($_SESSION["new_project_step_1"]["shop_address_email"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_email"]);

	//$order_fields[] = "order_shop_address_contact";
    //$order_values[] = trim($_SESSION["new_project_step_1"]["shop_address_contact_name"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_contact_name"]);

    $order_fields[] = "date_created";
    $order_values[] = "current_timestamp";

    $order_fields[] = "date_modified";
    $order_values[] = "current_timestamp";

    $order_fields[] = "user_created";
    $order_values[] = dbquote(user_login());

    $order_fields[] = "user_modified";
    $order_values[] = dbquote(user_login());

    $sql = "insert into orders (" . join(", ", $order_fields) . ") values (" . join(", ", $order_values) . ")";
    mysql_query($sql) or dberror($sql);

	$order_id= mysql_insert_id();

    append_order_state($order_id, "100", 1, 1);
    add_standard_budget_positions(ITEM_TYPE_EXCLUSION, $order_id);
    add_standard_budget_positions(ITEM_TYPE_NOTIFICATION, $order_id);

    // insert delivery address into table oder_addresses
	$delivery_address = array();
    if($_SESSION["new_project_step_5"] != "")
	{
		$delivery_address_fields[] = "order_address_order";
		$delivery_address_values[] = $order_id;
		$delivery_address["order_id"] = $order_id;

		$delivery_address_fields[] = "order_address_type";
		$delivery_address_values[] = 2;
		$delivery_address["address_type"] = 2;

		$delivery_address_fields[] = "order_address_company";
		$delivery_address_values[] = trim($_SESSION["new_project_step_5"]["delivery_address_company"]) == "" ? "null" : dbquote($_SESSION["new_project_step_5"]["delivery_address_company"]);
		$delivery_address["address_company"] = $_SESSION["new_project_step_5"]["delivery_address_company"];
		
		$delivery_address_fields[] = "order_address_company2";
		$delivery_address_values[] = trim($_SESSION["new_project_step_5"]["delivery_address_company2"]) == "" ? "null" : dbquote($_SESSION["new_project_step_5"]["delivery_address_company2"]);
		$delivery_address["address_company2"] = $_SESSION["new_project_step_5"]["delivery_address_company2"];
		
		$delivery_address_fields[] = "order_address_address";
		$delivery_address_values[] = trim($_SESSION["new_project_step_5"]["delivery_address_address"]) == "" ? "null" : dbquote($_SESSION["new_project_step_5"]["delivery_address_address"]);
		$delivery_address["address_address"] =$_SESSION["new_project_step_5"]["delivery_address_address"];
		

		$delivery_address_fields[] = "order_address_address2";
		$delivery_address_values[] = trim($_SESSION["new_project_step_5"]["delivery_address_address2"]) == "" ? "null" : dbquote($_SESSION["new_project_step_5"]["delivery_address_address2"]);
		$delivery_address["address_address2"] = $_SESSION["new_project_step_5"]["delivery_address_address2"];
		
		$delivery_address_fields[] = "order_address_zip";
		$delivery_address_values[] = trim($_SESSION["new_project_step_5"]["delivery_address_zip"]) == "" ? "null" : dbquote($_SESSION["new_project_step_5"]["delivery_address_zip"]);
		$delivery_address["address_zip"] = $_SESSION["new_project_step_5"]["delivery_address_zip"];


		$delivery_address_fields[] = "order_address_place_id";
		$delivery_address_values[] = trim($_SESSION["new_project_step_5"]["delivery_address_place_id"]) == "" ? "null" : dbquote($_SESSION["new_project_step_5"]["delivery_address_place_id"]);
		$delivery_address["address_place_id"] = $_SESSION["new_project_step_5"]["delivery_address_place_id"];
		
	
		$delivery_address_fields[] = "order_address_place";
		$delivery_address_values[] = trim($_SESSION["new_project_step_5"]["delivery_address_place"]) == "" ? "null" : dbquote($_SESSION["new_project_step_5"]["delivery_address_place"]);
		$delivery_address["address_place"] = $_SESSION["new_project_step_5"]["delivery_address_place"];
		
		$delivery_address_fields[] = "order_address_country";
		$delivery_address_values[] = trim($_SESSION["new_project_step_5"]["delivery_address_country"]) == "" ? "null" : dbquote($_SESSION["new_project_step_5"]["delivery_address_country"]);
		$delivery_address["address_country"] = $_SESSION["new_project_step_5"]["delivery_address_country"];
		
		$delivery_address_fields[] = "order_address_phone";
		$delivery_address_values[] = trim($_SESSION["new_project_step_5"]["delivery_address_phone"]) == "" ? "null" : dbquote($_SESSION["new_project_step_5"]["delivery_address_phone"]);
		$delivery_address["address_phone"] = $_SESSION["new_project_step_5"]["delivery_address_phone"];
		
		$delivery_address_fields[] = "order_address_fax";
		$delivery_address_values[] = trim($_SESSION["new_project_step_5"]["delivery_address_fax"]) == "" ? "null" : dbquote($_SESSION["new_project_step_5"]["delivery_address_fax"]);
		$delivery_address["address_fax"] = $_SESSION["new_project_step_5"]["delivery_address_fax"];
		
		$delivery_address_fields[] = "order_address_email";
		$delivery_address_values[] = trim($_SESSION["new_project_step_5"]["delivery_address_email"]) == "" ? "null" : dbquote($_SESSION["new_project_step_5"]["delivery_address_email"]);
		$delivery_address["address_email"] = $_SESSION["new_project_step_5"]["delivery_address_email"];
		
		$delivery_address_fields[] = "order_address_parent";
		$delivery_address_values[] = $user["address"];
		$delivery_address["address_parent"] = $user["address"];

		$delivery_address_fields[] = "order_address_contact";
		$delivery_address_values[] = trim($_SESSION["new_project_step_5"]["delivery_address_contact"]) == "" ? "null" : dbquote($_SESSION["new_project_step_5"]["delivery_address_contact"]);
		$delivery_address["address_contact"] = $_SESSION["new_project_step_5"]["delivery_address_contact"];
		
		$delivery_address_fields[] = "date_created";
		$delivery_address_values[] = "current_timestamp";

		$delivery_address_fields[] = "date_modified";
		$delivery_address_values[] = "current_timestamp";

		if (isset($_SESSION["user_login"]))
		{
			$delivery_address_fields[] = "user_created";
			$delivery_address_values[] = dbquote($_SESSION["user_login"]);

			$delivery_address_fields[] = "user_modified";
			$delivery_address_values[] = dbquote($_SESSION["user_login"]);
		}
	}
	else
	{
		$delivery_address_fields[] = "order_address_order";
		$delivery_address_values[] = $order_id;
		$delivery_address["order_id"] = $order_id;

		$delivery_address_fields[] = "order_address_type";
		$delivery_address_values[] = 2;
		$delivery_address["address_type"] = 2;

		$delivery_address_fields[] = "order_address_company";
		$delivery_address_values[] = trim($_SESSION["new_project_step_1"]["shop_address_company"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_company"]);
		$delivery_address["address_company"] = $_SESSION["new_project_step_1"]["shop_address_company"];
		
		$delivery_address_fields[] = "order_address_company2";
		$delivery_address_values[] = trim($_SESSION["new_project_step_1"]["shop_address_company2"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_company2"]);
		$delivery_address["address_company2"] = $_SESSION["new_project_step_1"]["shop_address_company2"];
		
		$delivery_address_fields[] = "order_address_address";
		$delivery_address_values[] = trim($_SESSION["new_project_step_1"]["shop_address_address"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_address"]);
		$delivery_address["address_address"] = $_SESSION["new_project_step_1"]["shop_address_address"];
		

		$delivery_address_fields[] = "order_address_address2";
		$delivery_address_values[] = trim($_SESSION["new_project_step_1"]["shop_address_address2"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_address2"]);
		$delivery_address["address_address2"] = $_SESSION["new_project_step_1"]["shop_address_address2"];
		
		$delivery_address_fields[] = "order_address_zip";
		$delivery_address_values[] = trim($_SESSION["new_project_step_1"]["shop_address_zip"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_zip"]);
		$delivery_address["address_zip"] = $_SESSION["new_project_step_1"]["shop_address_zip"];
		
		$delivery_address_fields[] = "order_address_place_id";
		$delivery_address_values[] = trim($_SESSION["new_project_step_1"]["shop_address_place_id"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_place_id"]);
		$delivery_address["address_place_id"] = $_SESSION["new_project_step_1"]["shop_address_place_id"];

		$delivery_address_fields[] = "order_address_place";
		$delivery_address_values[] = trim($_SESSION["new_project_step_1"]["shop_address_place"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_place"]);
		$delivery_address["address_place"] = $_SESSION["new_project_step_1"]["shop_address_place"];
		
		$delivery_address_fields[] = "order_address_country";
		$delivery_address_values[] = trim($_SESSION["new_project_step_1"]["shop_address_country"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_country"]);
		$delivery_address["address_country"] = $_SESSION["new_project_step_1"]["shop_address_country"];
		
		$delivery_address_fields[] = "order_address_phone";
		$delivery_address_values[] = trim($_SESSION["new_project_step_1"]["shop_address_phone"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_phone"]);
		$delivery_address["address_phone"] = $_SESSION["new_project_step_1"]["shop_address_phone"];
		
		$delivery_address_fields[] = "order_address_fax";
		$delivery_address_values[] = trim($_SESSION["new_project_step_1"]["shop_address_fax"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_fax"]);
		$delivery_address["address_fax"] = $_SESSION["new_project_step_1"]["shop_address_fax"];
		
		$delivery_address_fields[] = "order_address_email";
		$delivery_address_values[] = trim($_SESSION["new_project_step_1"]["shop_address_email"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_email"]);
		$delivery_address["address_email"] = $_SESSION["new_project_step_1"]["shop_address_email"];
		
		$delivery_address_fields[] = "order_address_parent";
		$delivery_address_values[] = $user["address"];

		//$delivery_address_fields[] = "order_address_contact";
		//$delivery_address_values[] = "";
		//$delivery_address["address_contact"] = "";
		
		$delivery_address_fields[] = "date_created";
		$delivery_address_values[] = "current_timestamp";

		$delivery_address_fields[] = "date_modified";
		$delivery_address_values[] = "current_timestamp";

		$delivery_address_fields[] = "user_created";
		$delivery_address_values[] = dbquote(user_login());

		$delivery_address_fields[] = "user_modified";
		$delivery_address_values[] = dbquote(user_login());
	}
	
	$sql = "insert into order_addresses (" . join(", ", $delivery_address_fields) . ") values (" . join(", ", $delivery_address_values) . ")";
	mysql_query($sql) or dberror($sql);

	
	// insert record into table projects
    if(array_key_exists("new_project_step_3", $_SESSION) and $_SESSION["new_project_step_3"] != "")
	{
	
		$project_fields[] = "project_number";
		$project_values[] = dbquote($project_number);

		if($standard_design_supervisor)
		{
			$project_fields[] = "project_design_supervisor";
			$project_values[] = $standard_design_supervisor;
		}
		
		if($standard_retail_coordinator)
		{
			$project_fields[] = "project_retail_coordinator";
			$project_values[] = $standard_retail_coordinator;
		}
		
		if($standard_cms_approver)
		{
			$project_fields[] = "project_cms_approver";
			$project_values[] = $standard_cms_approver;
		}

		if($standard_local_retail_coordinator)
		{
			$project_fields[] = "project_local_retail_coordinator";
			$project_values[] = $standard_local_retail_coordinator;
		}

		$project_fields[] = "project_postype";
		$project_values[] = trim($_SESSION["new_project_step_1"]["project_postype"]) == "" ? "0" : dbquote($_SESSION["new_project_step_1"]["project_postype"]);

		$project_fields[] = "project_pos_subclass";
		$project_values[] = trim($_SESSION["new_project_step_1"]["project_pos_subclass"]) == "" ? "0" : dbquote($_SESSION["new_project_step_1"]["project_pos_subclass"]);

		
		if($_SESSION["new_project_step_1"]["project_is_relocation_project"] == 1) {
			$project_fields[] = "project_projectkind";
			$project_values[] = 6;
		}
		else {
			$project_fields[] = "project_projectkind";
			$project_values[] = trim($_SESSION["new_project_step_1"]["project_kind"]) == "" ? "0" : dbquote($_SESSION["new_project_step_1"]["project_kind"]);
		}

		$project_fields[] = "project_order";
		$project_values[] = $order_id;

		
		$project_fields[] = "project_product_line";
		$project_values[] = trim($_SESSION["new_project_step_1"]["product_line"]) == "" ? "0" : dbquote($_SESSION["new_project_step_1"]["product_line"]);
		

		//$project_fields[] = "project_product_line";
		//$project_values[] = 1;


		$project_fields[] = "project_location_type";
		$project_values[] = trim($_SESSION["new_project_step_3"]["location_type"]) == "" ? "null" : dbquote($_SESSION["new_project_step_3"]["location_type"]);

		$project_fields[] = "project_location";
		$project_values[] = trim($_SESSION["new_project_step_3"]["location_type_other"]) == "" ? "null" : dbquote($_SESSION["new_project_step_3"]["location_type_other"]);

		$project_fields[] = "project_approximate_budget";
		$project_values[] = trim($_SESSION["new_project_step_3"]["approximate_budget"]) == "" ? "null" : dbquote($_SESSION["new_project_step_3"]["approximate_budget"]);

		$project_fields[] = "project_planned_opening_date";
		$project_values[] = dbquote(from_system_date($_SESSION["new_project_step_3"]["planned_opening_date"]), true);

		$project_fields[] = "project_real_opening_date";
		$project_values[] = dbquote(from_system_date($_SESSION["new_project_step_3"]["planned_opening_date"]), true);

		$project_fields[] = "project_watches_displayed";
		$project_values[] = trim($_SESSION["new_project_step_3"]["watches_displayed"]) == "" ? "null" : dbquote($_SESSION["new_project_step_3"]["watches_displayed"]);

		$project_fields[] = "project_watches_stored";
		$project_values[] = trim($_SESSION["new_project_step_3"]["watches_stored"]) == "" ? "null" : dbquote($_SESSION["new_project_step_3"]["watches_stored"]);

		$project_fields[] = "project_bijoux_displayed";
		$project_values[] = trim($_SESSION["new_project_step_3"]["bijoux_displayed"]) == "" ? "null" : dbquote($_SESSION["new_project_step_3"]["bijoux_displayed"]);

		$project_fields[] = "project_bijoux_stored";
		$project_values[] = trim($_SESSION["new_project_step_3"]["bijoux_stored"]) == "" ? "null" : dbquote($_SESSION["new_project_step_3"]["bijoux_stored"]);

		if($_SESSION["new_project_step_5"] != "")
		{
			$project_fields[] = "project_comments";
			$project_values[] = trim($_SESSION["new_project_step_5"]["comments"]) == "" ? "null" : dbquote($_SESSION["new_project_step_5"]["comments"]);
		}

		$project_fields[] = "project_is_relocation_project";
		$project_values[] = trim($_SESSION["new_project_step_1"]["project_is_relocation_project"]) == "" ? "0" : dbquote($_SESSION["new_project_step_1"]["project_is_relocation_project"]);

		$project_fields[] = "project_relocated_posaddress_id";
		$project_values[] = trim($_SESSION["new_project_step_1"]["project_relocated_posaddress_id"]) == "" ? "0" : dbquote($_SESSION["new_project_step_1"]["project_relocated_posaddress_id"]);


		$project_fields[] = "project_floor";
		$project_values[] = trim($_SESSION["new_project_step_4"]["project_floor"]) == "" ? "0" : dbquote($_SESSION["new_project_step_4"]["project_floor"]);

		$project_fields[] = "date_created";
		$project_values[] = "current_timestamp";

		$project_fields[] = "date_modified";
		$project_values[] = "current_timestamp";
					
		if (isset($_SESSION["user_login"]))
		{
			$project_fields[] = "user_created";
			$project_values[] = dbquote($_SESSION["user_login"]);

			$project_fields[] = "user_modified";
			$project_values[] = dbquote($_SESSION["user_login"]);
		}

		$sql = "insert into projects (" . join(", ", $project_fields) . ") values (" . join(", ", $project_values) . ")";
		mysql_query($sql) or dberror($sql);
		
		$project_id= mysql_insert_id();

		$_REQUEST["id"] = mysql_insert_id();
		
		// insert records into table project_items
		if(array_key_exists("design_objectives_listbox_names", $_SESSION["new_project_step_3"]))
		{
			foreach ($_SESSION["new_project_step_3"]["design_objectives_listbox_names"] as $element)
			{
				$project_item_fields[0] = "project_item_project";
				$project_item_values[0] = $project_id;

				$project_item_fields[1] = "project_item_item";
				$project_item_values[1] = $_SESSION["new_project_step_3"][$element];
				
				$project_item_fields[2] = "date_created";
				$project_item_values[2] = "current_timestamp";

				$project_item_fields[3] = "date_modified";
				$project_item_values[3] = "current_timestamp";

						
				if (isset($_SESSION["user_login"]))
				{
					$project_item_fields[4] = "user_created";
					$project_item_values[4] = dbquote($_SESSION["user_login"]);

					$project_item_fields[5] = "user_modified";
					$project_item_values[5] = dbquote($_SESSION["user_login"]);
				}
				$sql = "insert into project_items (" . join(", ", $project_item_fields) . ") values (" . join(", ", $project_item_values) . ")";
				mysql_query($sql) or dberror($sql);
			}


			foreach ($_SESSION["new_project_step_3"]["design_objectives_checklist_names"] as $name)
			{
				if(isset($_SESSION["new_project_step_3"][$name]))
				{
					$tmp = $_SESSION["new_project_step_3"][$name];

					foreach ($tmp as $value)
					{
						$project_item_fields[0] = "project_item_project";
						$project_item_values[0] = $project_id;

						$project_item_fields[1] = "project_item_item";
						$project_item_values[1] = $value;
						
						$project_item_fields[2] = "date_created";
						$project_item_values[2] = "current_timestamp";

						$project_item_fields[3] = "date_modified";
						$project_item_values[3] = "current_timestamp";
								
						if (isset($_SESSION["user_login"]))
						{
							$project_item_fields[4] = "user_created";
							$project_item_values[4] = dbquote($_SESSION["user_login"]);

							$project_item_fields[5] = "user_modified";
							$project_item_values[5] = dbquote($_SESSION["user_login"]);
						}
						$sql = "insert into project_items (" . join(", ", $project_item_fields) . ") values (" . join(", ", $project_item_values) . ")";
						mysql_query($sql) or dberror($sql);
					}
				}
			}
		}

		// create standard design objectives in table project items
		$sql_design_objective_groups = "select design_objective_group_id,  " .
                               "    design_objective_item_id " .
                               "from design_objective_groups " .
			                   "left join design_objective_items on design_objective_item_group = design_objective_group_id " . 
                               "where design_objective_item_isstandard = 1 " .
			                   "and design_objective_item_active = 1 " . 
							   "and design_objective_group_postype = " . $_SESSION["new_project_step_1"]["project_postype"] . " " .
                               "order by design_objective_group_priority";

		$res = mysql_query($sql_design_objective_groups) or dberror($sql_design_objective_groups);
		$number_of_design_objective_groups = mysql_num_rows($res);

		if ($number_of_design_objective_groups > 0)
		{
			
			while ($row = mysql_fetch_assoc($res))
			{
				$sql = "delete from project_items " . 
					   "where project_item_project = " . $project_id . 
					   " and project_item_item = " . $row["design_objective_item_id"];

				mysql_query($sql) or dberror($sql);
				
				$project_item_fields[0] = "project_item_project";
				$project_item_values[0] = $project_id;

				$project_item_fields[1] = "project_item_item";
				$project_item_values[1] = $row["design_objective_item_id"];
				
				$project_item_fields[2] = "date_created";
				$project_item_values[2] = "current_timestamp";

				$project_item_fields[3] = "date_modified";
				$project_item_values[3] = "current_timestamp";
						
				if (isset($_SESSION["user_login"]))
				{
					$project_item_fields[4] = "user_created";
					$project_item_values[4] = dbquote($_SESSION["user_login"]);

					$project_item_fields[5] = "user_modified";
					$project_item_values[5] = dbquote($_SESSION["user_login"]);
				}
				$sql = "insert into project_items (" . join(", ", $project_item_fields) . ") values (" . join(", ", $project_item_values) . ")";
				mysql_query($sql) or dberror($sql);

			}
		}
		
	}
	else // take over or lease renewal without project details
	{
		$project_fields[] = "project_number";
		$project_values[] = dbquote($project_number);

		/*	
		if($standard_retail_coordinator)
		{
			$project_fields[] = "project_retail_coordinator";
			$project_values[] = $standard_retail_coordinator;
		}
		*/
		
		
		if($standard_local_retail_coordinator)
		{
			$project_fields[] = "project_local_retail_coordinator";
			$project_values[] = $standard_local_retail_coordinator;
		}

		$project_fields[] = "project_postype";
		$project_values[] = trim($_SESSION["new_project_step_1"]["project_postype"]) == "" ? "0" : dbquote($_SESSION["new_project_step_1"]["project_postype"]);

		$project_fields[] = "project_pos_subclass";
		$project_values[] = trim($_SESSION["new_project_step_1"]["project_pos_subclass"]) == "" ? "0" : dbquote($_SESSION["new_project_step_1"]["project_pos_subclass"]);

		$project_fields[] = "project_projectkind";
		$project_values[] = trim($_SESSION["new_project_step_1"]["project_kind"]) == "" ? "0" : dbquote($_SESSION["new_project_step_1"]["project_kind"]);

		$project_fields[] = "project_order";
		$project_values[] = $order_id;

		
		$project_fields[] = "project_product_line";
		$project_values[] = trim($_SESSION["new_project_step_1"]["product_line"]) == "" ? "0" : dbquote($_SESSION["new_project_step_1"]["product_line"]);

		//$project_fields[] = "project_product_line";
		//$project_values[] = 1;


		$project_fields[] = "project_is_relocation_project";
		$project_values[] = trim($_SESSION["new_project_step_1"]["project_is_relocation_project"]) == "" ? "0" : dbquote($_SESSION["new_project_step_1"]["project_is_relocation_project"]);

		$project_fields[] = "project_relocated_posaddress_id";
		$project_values[] = trim($_SESSION["new_project_step_1"]["project_relocated_posaddress_id"]) == "" ? "0" : dbquote($_SESSION["new_project_step_1"]["project_relocated_posaddress_id"]);

		$project_fields[] = "date_created";
		$project_values[] = "current_timestamp";

		$project_fields[] = "date_modified";
		$project_values[] = "current_timestamp";
					
		if (isset($_SESSION["user_login"]))
		{
			$project_fields[] = "user_created";
			$project_values[] = dbquote($_SESSION["user_login"]);

			$project_fields[] = "user_modified";
			$project_values[] = dbquote($_SESSION["user_login"]);
		}

		$sql = "insert into projects (" . join(", ", $project_fields) . ") values (" . join(", ", $project_values) . ")";
		mysql_query($sql) or dberror($sql);
		
		$project_id= mysql_insert_id();

		$_REQUEST["id"] = mysql_insert_id();


		//copy data from latest existing project for take over and lease renewal
		$sql_p = 'select project_id, project_location_type, project_location, project_watches_displayed, ' . 
				 ' project_watches_stored, project_bijoux_displayed, project_bijoux_stored, projects.date_created as pdate, ' . 
			     ' order_id, order_voltage, order_preferred_transportation_arranged, order_preferred_transportation_mode ' . 
			     ' from posorders ' . 
			     ' left join orders on order_id = posorder_order ' .
			     ' left join projects on project_order = posorder_order ' . 
			     ' where posorder_type = 1 and posorder_posaddress = ' . $_SESSION["new_project_step_1"]["posaddress_id"]  . 
			     ' and project_actual_opening_date is not NULL and project_actual_opening_date <> "0000-00-00" ' . 
			     ' order by posorder_id DESC';

		$res_p = mysql_query($sql_p) or dberror($sql_p);
		if ($row_p = mysql_fetch_assoc($res_p))
		{
			//add missing information from project

			$fields = array();

			$value = dbquote($row_p['project_location_type']);
			$fields[] = "project_location_type = " . $value;

			$value = dbquote($row_p['project_location']);
			$fields[] = "project_location = " . $value;

			$value = dbquote($row_p['project_watches_displayed']);
			$fields[] = "project_watches_displayed = " . $value;

			$value = dbquote($row_p['project_watches_stored']);
			$fields[] = "project_watches_stored = " . $value;

			$value = dbquote($row_p['project_bijoux_displayed']);
			$fields[] = "project_bijoux_displayed = " . $value;

			$value = dbquote($row_p['project_bijoux_stored']);
			$fields[] = "project_bijoux_stored = " . $value;

			$value = dbquote(date("Y-m-d"));
			$fields[] = "date_modified = " . $value;

			$value = dbquote(user_login());
			$fields[] = "user_modified = " . $value;

			$sql = "update projects set " . join(", ", $fields) . " where project_id = " . $project_id;
			mysql_query($sql) or dberror($sql);

			//update orders
			$fields = array();

			$value = dbquote($row_p['order_voltage']);
			$fields[] = "order_voltage = " . $value;

			$value = dbquote($row_p['order_preferred_transportation_arranged']);
			$fields[] = "order_preferred_transportation_arranged = " . $value;

			$value = dbquote($row_p['order_preferred_transportation_mode']);
			$fields[] = "order_preferred_transportation_mode = " . $value;

			$value = dbquote(date("Y-m-d"));
			$fields[] = "date_modified = " . $value;

			$value = dbquote(user_login());
			$fields[] = "user_modified = " . $value;

			$sql = "update orders set " . join(", ", $fields) . " where order_id = " . $order_id;
			mysql_query($sql) or dberror($sql);


			//create design objectives
			if($row_p["pdate"] >= '2010-12-05')
			{
				$sql_p = 'select * from project_items where project_item_project = ' . $row_p['project_id'];

				$res_p = mysql_query($sql_p) or dberror($sql_p);
				while ($row_p = mysql_fetch_assoc($res_p))
				{
					$project_item_fields = array();
					$project_item_values = array();
					
					$project_item_fields[] = "project_item_project";
					$project_item_values[] = $project_id;

					$project_item_fields[] = "project_item_item";
					$project_item_values[] = $row_p['project_item_item'];

					$project_item_fields[] = "user_created";
					$project_item_values[] = dbquote(user_login());

					$project_item_fields[] = "date_created";
					$project_item_values[] = dbquote(date("Y-m-d"));

					$sql = "insert into project_items (" . join(", ", $project_item_fields) . ") values (" . join(", ", $project_item_values) . ")";
					mysql_query($sql) or dberror($sql);
				}
			}
		}

	}

			
	$fields = array();

	$value = dbquote(date("Y-m-d"));
	$fields[] = "date_modified = " . $value;

	$value = dbquote(user_login());
	$fields[] = "user_modified = " . $value;


	$sql = "update projects set " . join(", ", $fields) . " where project_id = " . $project_id;
	mysql_query($sql) or dberror($sql);


	
    //create cost monitoring sheet
    $fields = array();
    $values = array();

    $fields[] = "project_cost_order";
    $values[] = $order_id;

	$fields[] = "project_cost_sqms";
    $values[] = dbquote($_SESSION["new_project_step_4"]["posaddress_store_retailarea"]);
	

	$fields[] = "project_cost_gross_sqms";
    $values[] = dbquote($_SESSION["new_project_step_4"]["posaddress_store_grosssurface"]);


	$fields[] = "project_cost_totalsqms";
    $values[] = dbquote($_SESSION["new_project_step_4"]["posaddress_store_totalsurface"]);

	$fields[] = "project_cost_backofficesqms";
    $values[] = dbquote($_SESSION["new_project_step_4"]["posaddress_store_backoffice"]);

	$fields[] = "project_cost_numfloors";
    $values[] = dbquote($_SESSION["new_project_step_4"]["posaddress_store_numfloors"]);

	$fields[] = "project_cost_floorsurface1";
    $values[] = dbquote($_SESSION["new_project_step_4"]["posaddress_store_floorsurface1"]);

	$fields[] = "project_cost_floorsurface2";
    $values[] = dbquote($_SESSION["new_project_step_4"]["posaddress_store_floorsurface2"]);

	$fields[] = "project_cost_floorsurface3";
    $values[] = dbquote($_SESSION["new_project_step_4"]["posaddress_store_floorsurface3"]);

    $fields[] = "project_cost_type";
    $values[] = trim($_SESSION["new_project_step_1"]["project_cost_type"]) == "''" ? "" : dbquote($_SESSION["new_project_step_1"]["project_cost_type"]);
    
    $fields[] = "date_created";
    $values[] = dbquote(date("Y-m-d"));

    $fields[] = "date_modified";
    $values[] = dbquote(date("Y-m-d"));

    $fields[] = "user_created";
    $values[] = dbquote(user_login());

    $fields[] = "user_modified";
    $values[] = dbquote(user_login());

    $sql = "insert into project_costs (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
    mysql_query($sql) or dberror($sql);


    //Create Project Milestones
		
	$sql = "select * from milestones " .
		   "where milestone_active = 1 " .
		   "order by milestone_code ";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$sql_m = "select count(project_milestone_id) as num_recs from project_milestones " .
				 "where project_milestone_project = " . $project_id . 
				 "   and project_milestone_milestone = " . $row["milestone_id"];
		
		
		$res_m = mysql_query($sql_m) or dberror($sql_m);
		$row_m = mysql_fetch_assoc($res_m);

		if($row_m["num_recs"] == 0)
		{
			$fields = array();
			$values = array();

			$fields[] = "project_milestone_project";
			$values[] = $project_id;

			$fields[] = "project_milestone_milestone";
			$values[] = $row["milestone_id"];

			if($row["milestone_in_new_project"] == 1)
			{
				$fields[] = "project_milestone_date";
				$values[] = dbquote(date("Y-m-d"));

				$fields[] = "project_milestone_date_comment";
				$values[] = dbquote("On project submission");
			}
			
			$fields[] = "date_created";
			$values[] = dbquote(date("Y-m-d"));

			$fields[] = "date_modified";
			$values[] = dbquote(date("Y-m-d"));

			$fields[] = "user_created";
			$values[] = dbquote(user_login());

			$fields[] = "user_modified";
			$values[] = dbquote(user_login());

			$sql = "insert into project_milestones (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

			mysql_query($sql) or dberror($sql);
		}
	}

	
	//insert record into pos orders or posorderpipeline
	if($_SESSION["new_project_step_1"]["project_kind"] > 1 and $_SESSION["new_project_step_1"]["posaddress_id"] > 0) // renovation project, takeover/renovation ore take over or lease renewal
	{
		$fields = array();
		$values = array();

		if($_SESSION["new_project_step_1"]["project_kind"] == 1) // new
		{
			$fields[] = "posorder_parent_table";
			$values[] = dbquote("posaddresses");
		}

		$fields[] = "posorder_posaddress";
		$values[] = dbquote($_SESSION["new_project_step_1"]["posaddress_id"]);

		$fields[] = "posorder_order";
		$values[] = dbquote($order_id);

		$fields[] = "posorder_type";
		$values[] = 1;

		$fields[] = "posorder_ordernumber";
		$values[] = dbquote($project_number);

		$year_of_order = substr($project_number,0,2);
		if($year_of_order > 30 and $year_of_order <= 99)
		{
			$year_of_order = "19" . $year_of_order;
		}
		else
		{
			$year_of_order = substr($year_of_order, 0, 1) . '00' . substr($year_of_order, 1, 1);
		}

		
		if(substr($project_number,3,1) == '.')
		{
			$year_of_order = substr($project_number,0,1) . '0' . substr($project_number,1,2);
		}

		$fields[] = "posorder_year";
		$values[] = dbquote($year_of_order);

		$fields[] = "posorder_product_line";
		$values[] = dbquote($_SESSION["new_project_step_1"]["product_line"]);

		//$fields[] = "posorder_postype";
		//$values[] = dbquote($_SESSION["new_project_step_1"]["project_postype"]);

		$fields[] = "posorder_postype";
		$values[] = 1;

		$fields[] = "posorder_subclass";
		$values[] = dbquote($_SESSION["new_project_step_1"]["project_pos_subclass"]);

		$fields[] = "posorder_project_kind";
		$values[] = dbquote($_SESSION["new_project_step_1"]["project_kind"]);

		$fields[] = "posorder_legal_type";
		$values[] = dbquote($_SESSION["new_project_step_1"]["project_cost_type"]);

		$fields[] = "posorder_floor";
		$values[] = dbquote($_SESSION["new_project_step_4"]["project_floor"]);

		$fields[] = "posorder_neighbour_left";
		$values[] = dbquote($_SESSION["new_project_step_4"]["posorder_neighbour_left"]);

		$fields[] = "posorder_neighbour_right";
		$values[] = dbquote($_SESSION["new_project_step_4"]["posorder_neighbour_right"]);

		$fields[] = "posorder_neighbour_acrleft";
		$values[] = dbquote($_SESSION["new_project_step_4"]["posorder_neighbour_acrleft"]);

		$fields[] = "posorder_neighbour_acrright";
		$values[] = dbquote($_SESSION["new_project_step_4"]["posorder_neighbour_acrright"]);

		$fields[] = "posorder_neighbour_brands";
		$values[] = dbquote($_SESSION["new_project_step_4"]["posorder_neighbour_brands"]);


		$fields[] = "date_created";
		$values[] = dbquote(date("Y-m-d"));

		$fields[] = "date_modified";
		$values[] = dbquote(date("Y-m-d"));

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$fields[] = "user_modified";
		$values[] = dbquote(user_login());
		
		
		if($_SESSION["new_project_step_1"]["project_kind"] == 1) // new projects
		{
			$sql = "insert into posorderspipeline (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			mysql_query($sql) or dberror($sql);
		}
		else
		{
			$sql = "insert into posorders (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			mysql_query($sql) or dberror($sql);
		}

		//update pos areas
		$sql = "delete from posareas where posarea_posaddress = " . $_SESSION["new_project_step_1"]["posaddress_id"];
		$res = mysql_query($sql);

		
		$sql = "select posareatype_id, posareatype_name " . 
			   "from posareatypes " . 
			   " order by  posareatype_name";

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{

			if($_SESSION["new_project_step_4"]["AR_" .$row["posareatype_id"]] == 1)
			{
				$fields = array();
				$values = array();

				$fields[] = "posarea_posaddress";
				$values[] = dbquote($_SESSION["new_project_step_1"]["posaddress_id"]);

				$fields[] = "posarea_area";
				$values[] = dbquote($row["posareatype_id"]);

				$fields[] = "date_created";
				$values[] = dbquote(date("Y-m-d"));

				$fields[] = "date_modified";
				$values[] = dbquote(date("Y-m-d"));

				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				$fields[] = "user_modified";
				$values[] = dbquote(user_login());

				$sql = "insert into posareas (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);
			}
		}


		//update posaddress
		$fields = array();

		$value = dbquote($_SESSION["new_project_step_1"]["shop_address_company"]);
		$fields[] = "posaddress_name = " . $value;

		$value = dbquote($_SESSION["new_project_step_1"]["shop_address_company2"]);
		$fields[] = "posaddress_name2 = " . $value;

		$value = dbquote($_SESSION["new_project_step_1"]["shop_address_address"]);
		$fields[] = "posaddress_address = " . $value;

		$value = dbquote($_SESSION["new_project_step_1"]["shop_address_address2"]);
		$fields[] = "posaddress_address2 = " . $value;

		$value = dbquote($_SESSION["new_project_step_1"]["shop_address_zip"]);
		$fields[] = "posaddress_zip = " . $value;

		$value = dbquote(trim($_SESSION["new_project_step_1"]["shop_address_place"]));
		$fields[] = "posaddress_place = " . $value;

		if(isset($_SESSION["new_project_step_1"]["select_shop_address_place"]))
		{
			if($place_id)
			{
				$value = dbquote($place_id);
				$fields[] = "posaddress_place_id = " . $value;
			}
			else
			{
				$value = dbquote($_SESSION["new_project_step_1"]["select_shop_address_place"]);
				$fields[] = "posaddress_place_id = " . $value;
			}
		}

		$value = dbquote($_SESSION["new_project_step_1"]["shop_address_country"]);
		$fields[] = "posaddress_country = " . $value;

		$value = dbquote($_SESSION["new_project_step_1"]["shop_address_phone"]);
		$fields[] = "posaddress_phone = " . $value;

		$value = dbquote($_SESSION["new_project_step_1"]["shop_address_fax"]);
		$fields[] = "posaddress_fax = " . $value;

		$value = dbquote($_SESSION["new_project_step_1"]["shop_address_email"]);
		$fields[] = "posaddress_email = " . $value;
		
		
		$value = dbquote($_SESSION["new_project_step_1"]["posaddress_google_lat"]);
		$fields[] = "posaddress_google_lat = " . $value;

		$value = dbquote($_SESSION["new_project_step_1"]["posaddress_google_long"]);
		$fields[] = "posaddress_google_long = " . $value;

		$value = dbquote($_SESSION["new_project_step_1"]["posaddress_google_precision"]);
		$fields[] = "posaddress_google_precision = 1";

		$value = dbquote($_SESSION["new_project_step_4"]["posaddress_perc_class"]);
		$fields[] = "posaddress_perc_class = " . $value;

		$value = dbquote($_SESSION["new_project_step_4"]["posaddress_perc_tourist"]);
		$fields[] = "posaddress_perc_tourist = " . $value;

		$value = dbquote($_SESSION["new_project_step_4"]["posaddress_perc_transport"]);
		$fields[] = "posaddress_perc_transport = " . $value;

		$value = dbquote($_SESSION["new_project_step_4"]["posaddress_perc_people"]);
		$fields[] = "posaddress_perc_people = " . $value;

		$value = dbquote($_SESSION["new_project_step_4"]["posaddress_perc_parking"]);
		$fields[] = "posaddress_perc_parking = " . $value;

		$value = dbquote($_SESSION["new_project_step_4"]["posaddress_perc_visibility1"]);
		$fields[] = "posaddress_perc_visibility1 = " . $value;

		$value = dbquote($_SESSION["new_project_step_4"]["posaddress_perc_visibility2"]);
		$fields[] = "posaddress_perc_visibility2 = " . $value;

		$value = dbquote($_SESSION["new_project_step_4"]["posaddress_export_to_web"]);
		$fields[] = "posaddress_export_to_web = " . $value;

		$value = dbquote($_SESSION["new_project_step_4"]["posaddress_email_on_web"]);
		$fields[] = "posaddress_email_on_web = " . $value;

		$value = dbquote(date("Y-m-d"));
		$fields[] = "date_modified = " . $value;

		$value = dbquote(user_login());
		$fields[] = "user_modified = " . $value;

		
		$sql = "update posaddresses set " . join(", ", $fields) . " where posaddress_id = " . $_SESSION["new_project_step_1"]["posaddress_id"];
		mysql_query($sql) or dberror($sql);

	}
	else // new project into pipeline
	{
		// posaddresspipeline
		$fields = array();
		$values = array();

		$fields[] = "posaddress_client_id";
		$values[] = dbquote($user["address"]);

		$fields[] = "posaddress_ownertype";
		$values[] = dbquote($_SESSION["new_project_step_1"]["project_kind"]);

		$fields[] = "posaddress_franchisor_id";
		$values[] = 2;

		$fields[] = "posaddress_franchisee_id";
		$values[] = dbquote($franchisee_id);

		$fields[] = "posaddress_name";
		$values[] = dbquote($_SESSION["new_project_step_1"]["shop_address_company"]);

		$fields[] = "posaddress_name2";
		$values[] = dbquote($_SESSION["new_project_step_1"]["shop_address_company2"]);

		$fields[] = "posaddress_address";
		$values[] = dbquote($_SESSION["new_project_step_1"]["shop_address_address"]);

		$fields[] = "posaddress_address2";
		$values[] = dbquote($_SESSION["new_project_step_1"]["shop_address_address2"]);

		$fields[] = "posaddress_zip";
		$values[] = dbquote($_SESSION["new_project_step_1"]["shop_address_zip"]);

		$fields[] = "posaddress_place";
		$values[] = dbquote(trim($_SESSION["new_project_step_1"]["shop_address_place"]));

		if(isset($_SESSION["new_project_step_1"]["select_shop_address_place"]))
		{
			if($place_id)
			{
				$fields[] = "posaddress_place_id";
				$values[] = dbquote($place_id);
			}
			else
			{
				$fields[] = "posaddress_place_id";
				$values[] = dbquote(trim($_SESSION["new_project_step_1"]["select_shop_address_place"]));
			}
		}

		$fields[] = "posaddress_country";
		$values[] = dbquote($_SESSION["new_project_step_1"]["shop_address_country"]);
		
		$fields[] = "posaddress_phone";
		$values[] = dbquote($_SESSION["new_project_step_1"]["shop_address_phone"]);

		$fields[] = "posaddress_fax";
		$values[] = dbquote($_SESSION["new_project_step_1"]["shop_address_fax"]);

		$fields[] = "posaddress_email";
		$values[] = dbquote($_SESSION["new_project_step_1"]["shop_address_email"]);

		$fields[] = "posaddress_store_postype";
		$values[] = dbquote($_SESSION["new_project_step_1"]["project_postype"]);

		$fields[] = "posaddress_store_subclass";
		$values[] = dbquote($_SESSION["new_project_step_1"]["project_pos_subclass"]);

		$fields[] = "posaddress_store_furniture";
		$values[] = dbquote($_SESSION["new_project_step_1"]["product_line"]);

		//$fields[] = "posaddress_store_furniture";
		//$values[] = 1;

		$fields[] = "posaddress_store_grosssurface";
		$values[] = dbquote($_SESSION["new_project_step_4"]["posaddress_store_grosssurface"]);

		$fields[] = "posaddress_store_totalsurface";
		$values[] = dbquote($_SESSION["new_project_step_4"]["posaddress_store_totalsurface"]);

		$fields[] = "posaddress_store_retailarea";
		$values[] = dbquote($_SESSION["new_project_step_4"]["posaddress_store_retailarea"]);

		$fields[] = "posaddress_store_backoffice";
		$values[] = dbquote($_SESSION["new_project_step_4"]["posaddress_store_backoffice"]);

		$fields[] = "posaddress_store_numfloors";
		$values[] = dbquote($_SESSION["new_project_step_4"]["posaddress_store_numfloors"]);

		$fields[] = "posaddress_store_floorsurface1";
		$values[] = dbquote($_SESSION["new_project_step_4"]["posaddress_store_floorsurface1"]);

		$fields[] = "posaddress_store_floorsurface2";
		$values[] = dbquote($_SESSION["new_project_step_4"]["posaddress_store_floorsurface2"]);

		$fields[] = "posaddress_store_floorsurface3";
		$values[] = dbquote($_SESSION["new_project_step_4"]["posaddress_store_floorsurface3"]);


		$fields[] = "posaddress_google_lat";
		$values[] = dbquote($_SESSION["new_project_step_1"]["posaddress_google_lat"]);
		
		$fields[] = "posaddress_google_long";
		$values[] = dbquote($_SESSION["new_project_step_1"]["posaddress_google_long"]);

		$fields[] = "posaddress_google_precision";
		$values[] = 1;


		$fields[] = "posaddress_perc_class";
		$values[] = dbquote($_SESSION["new_project_step_4"]["posaddress_perc_class"]);

		$fields[] = "posaddress_perc_tourist";
		$values[] = dbquote($_SESSION["new_project_step_4"]["posaddress_perc_tourist"]);

		$fields[] = "posaddress_perc_transport";
		$values[] = dbquote($_SESSION["new_project_step_4"]["posaddress_perc_transport"]);

		$fields[] = "posaddress_perc_people";
		$values[] = dbquote($_SESSION["new_project_step_4"]["posaddress_perc_people"]);

		$fields[] = "posaddress_perc_parking";
		$values[] = dbquote($_SESSION["new_project_step_4"]["posaddress_perc_parking"]);

		$fields[] = "posaddress_perc_visibility1";
		$values[] = dbquote($_SESSION["new_project_step_4"]["posaddress_perc_visibility1"]);

		$fields[] = "posaddress_perc_visibility2";
		$values[] = dbquote($_SESSION["new_project_step_4"]["posaddress_perc_visibility2"]);

		$fields[] = "posaddress_export_to_web";
		$values[] = dbquote($_SESSION["new_project_step_4"]["posaddress_export_to_web"]);

		$fields[] = "posaddress_email_on_web";
		$values[] = dbquote($_SESSION["new_project_step_4"]["posaddress_email_on_web"]);

		$sql = "insert into posaddressespipeline (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
	    mysql_query($sql) or dberror($sql);

		$posaddress_id = mysql_insert_id();
		

		//posorderspipeline
		$fields = array();
		$values = array();

		$fields[] = "posorder_parent_table";
		$values[] = dbquote("posaddressespipeline");

		$fields[] = "posorder_posaddress";
		$values[] = dbquote($posaddress_id);

		$fields[] = "posorder_order";
		$values[] = dbquote($order_id);

		$fields[] = "posorder_type";
		$values[] = 1;

		$fields[] = "posorder_ordernumber";
		$values[] = dbquote($project_number);

		$year_of_order = substr($project_number,0,2);
		if($year_of_order > 30 and $year_of_order <= 99)
		{
			$year_of_order = "19" . $year_of_order;
		}
		else
		{
			$year_of_order = substr($year_of_order, 0, 1) . '00' . substr($year_of_order, 1, 1);
		}

		if(substr($project_number,3,1) == '.')
		{
			$year_of_order = substr($project_number,0,1) . '0' . substr($project_number,1,2);
		}

		$fields[] = "posorder_year";
		$values[] = dbquote($year_of_order);

		$fields[] = "posorder_product_line";
		$values[] = dbquote($_SESSION["new_project_step_1"]["product_line"]);

		//$fields[] = "posorder_product_line";
		//$values[] = 1;

		$fields[] = "posorder_postype";
		$values[] = dbquote($_SESSION["new_project_step_1"]["project_postype"]);

		$fields[] = "posorder_subclass";
		$values[] = dbquote($_SESSION["new_project_step_1"]["project_pos_subclass"]);

		$fields[] = "posorder_project_kind";
		$values[] = dbquote($_SESSION["new_project_step_1"]["project_kind"]);

		$fields[] = "posorder_legal_type";
		$values[] = dbquote($_SESSION["new_project_step_1"]["project_cost_type"]);

		$fields[] = "posorder_floor";
		$values[] = dbquote($_SESSION["new_project_step_4"]["project_floor"]);

		$fields[] = "posorder_neighbour_left";
		$values[] = dbquote($_SESSION["new_project_step_4"]["posorder_neighbour_left"]);

		$fields[] = "posorder_neighbour_right";
		$values[] = dbquote($_SESSION["new_project_step_4"]["posorder_neighbour_right"]);

		$fields[] = "posorder_neighbour_acrleft";
		$values[] = dbquote($_SESSION["new_project_step_4"]["posorder_neighbour_acrleft"]);

		$fields[] = "posorder_neighbour_acrright";
		$values[] = dbquote($_SESSION["new_project_step_4"]["posorder_neighbour_acrright"]);

		$fields[] = "posorder_neighbour_brands";
		$values[] = dbquote($_SESSION["new_project_step_4"]["posorder_neighbour_brands"]);

		$fields[] = "date_created";
		$values[] = dbquote(date("Y-m-d"));

		$fields[] = "date_modified";
		$values[] = dbquote(date("Y-m-d"));

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$fields[] = "user_modified";
		$values[] = dbquote(user_login());
		
		$sql = "insert into posorderspipeline (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
	    mysql_query($sql) or dberror($sql);

		//insert
		$sql = "select posareatype_id, posareatype_name " . 
			   "from posareatypes " . 
			   " order by  posareatype_name";

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{

			if(array_key_exists("AR_" .$row["posareatype_id"], $_SESSION["new_project_step_4"]) and $_SESSION["new_project_step_4"]["AR_" .$row["posareatype_id"]] == 1)
			{
				$fields = array();
				$values = array();

				$fields[] = "posarea_posaddress";
				$values[] = dbquote($posaddress_id);

				$fields[] = "posarea_area";
				$values[] = dbquote($row["posareatype_id"]);

				$fields[] = "date_created";
				$values[] = dbquote(date("Y-m-d"));

				$fields[] = "date_modified";
				$values[] = dbquote(date("Y-m-d"));

				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				$fields[] = "user_modified";
				$values[] = dbquote(user_login());

				$sql = "insert into posareaspipeline (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);

				
			}
		}
	}

	
	//save list of materials if project is created with shop configuretor (project_new_00.php)
	if(isset($_SESSION["new_project_step_0"]["cid"]))
	{

		//get shop basket
		$shop_data = array();
		$sql = "select shopbasket_shopdump " .
			   "from shopbaskets " .
			   "where shopbasket_user = " . user_id();

		$res = mysql_query($sql) or dberror();
		if ($res)
		{
			$row = mysql_fetch_assoc($res);
			$shop_data = unserialize(stripslashes($row['shopbasket_shopdump']));
		}

		//save items
		$options_added = false;
		$replacements_done = false;


		$sql = "select item_id, category_item_id, item_code, item_name, ".
			   "item_type, category_item_category " .
			   "from category_items " .
			   "left join items on category_items.category_item_item=items.item_id " .
			   "left join item_regions on item_region_item = item_id " .
			   "where category_item_category = " . $shop_data["category_id"] . 
			   "    and item_visible = 1 " .
			   "    and item_active = 1 " .
			   "    and item_region_region = " . get_user_region(user_id());;

		$res = mysql_query($sql) or dberror($sql);

		while ($row = mysql_fetch_assoc($res))
		{
			$oitems = array();
			$item_id = $row["item_id"];
			
			//check if item has composition groups
			$sql_c = "select count(item_composition_id) as num_recs " .
					 "from item_compositions " .
					 "left join item_groups on item_group_id = item_composition_group " .
					 "left join item_group_items on item_group_item_group = item_composition_group " .
					 "left join items on item_id = item_group_item_item " .
					 "where item_composition_item = " .  $item_id;

			$res_c = mysql_query($sql_c) or dberror($sql_c);
			$row_c = mysql_fetch_assoc($res_c);

			$num_items = $row_c["num_recs"];
			
			if($num_items > 0)
			{

				$sql_r = "select item_composition_id, item_id, item_group_item_quantity " .
						 "from item_compositions " .
						 "left join item_groups on item_group_id = item_composition_group " .
						 "left join item_group_items on item_group_item_group = item_composition_group " .
						 "left join items on item_id = item_group_item_item " .
						 "where item_composition_item = " . $item_id;

				$res_r = mysql_query($sql_r) or dberror($sql_r);
				while ($row_r = mysql_fetch_assoc($res_r))
				{
					if(isset($oitems[$row_r["item_id"]]))
					{
						$oitems[$row_r["item_id"]] = $oitems[$row_r["item_id"]] +  $row_r["item_group_item_quantity"];
					}
					else
					{
						$oitems[$row_r["item_id"]] = $row_r["item_group_item_quantity"];
					}
				}
			}
			else // item has no composition groups
			{
				if(isset($oitems[$item_id]))
				{
					$oitems[$item_id] = $oitems[$item_id] +  1;
				}
				else
				{
					$oitems[$item_id] = 1;
				}
			}


			// addons into tabel items
			$sql_a = "select addon_id, addon_child, " .
					 "item_id, item_name, item_type " .
					 "from addons " .
					 "left join items on item_id = addon_child " .
					 "where addon_parent = " . $item_id;

			

			$res_a = mysql_query($sql_a) or dberror($sql_a);
			
			while ($row_a = mysql_fetch_assoc($res_a))
			{
				if(isset($oitems[$row_a["item_id"]]))
				{
					$oitems[$row_a["item_id"]] = $oitems[$row_a["item_id"]] +  1;
				}
				else
				{
					$oitems[$row_a["item_id"]] = 1;
				}
			}




			// options into table items
			$num_of_options = 0;
			$options_ids = array();
			foreach($shop_data as $key=>$value)
			{
				if(substr($key, 0, 2) == "o_" and $value == 1)
				{
					$parts = explode ( "_", $key);
					
					if($parts[1] == $item_id)
					{
						$options_ids[] = $parts[2];
						$num_of_options++;
					}
				}
			}

			if($num_of_options > 0 and $options_added == false) // add options only once
			{
				$options_added = true;
				foreach($options_ids as $key=>$value)
				{

					$sql_a = "select item_id, item_name, item_type, " .
							 "item_option_quantity " .
							 "from item_options " .
							 "left join items on item_id = item_option_child " .
							 "where item_option_id = " . $value;

					$res_a = mysql_query($sql_a) or dberror($sql_a);
					
					if ($row_a = mysql_fetch_assoc($res_a))
					{
						if(isset($oitems[$row_a["item_id"]]))
						{
							$oitems[$row_a["item_id"]] = $oitems[$row_a["item_id"]] +  1;
						}
						else
						{
							$oitems[$row_a["item_id"]] = 1;
						}
					}
				}
			}


			// replacement groups

			$num_of_replacements = 0;
			foreach($shop_data as $key=>$value)
			{
				if((substr($key, 0, 1) == "r" and $value == 1) or (substr($key, 0, 2) == "rb" and $value > 0))
				{            
					$num_of_replacements++;
				}
			}

			if($num_of_replacements > 0 and $replacements_done == false) // replace items only once
			{
				$replacements_done = true;
				foreach($shop_data as $key=>$value)
				{
					if(substr($key, 0, 1) == "r" and $value == 1) // 
					{
						$parts = explode ( "_", $key);

						// get itemlist of group
						$sql_g = "select * " .
								 "from item_group_options " .
								 "where item_group_option_id = " . $parts[1];

						$res_g = mysql_query($sql_g) or dberror($sql_g);
						$row_g = mysql_fetch_assoc($res_g);

						if($row_g["item_group_option_item1"])
						{
							if(isset($oitems[$row_g["item_group_option_item1"]]))
							{
								$oitems[$row_g["item_group_option_item1"]] = $oitems[$row_g["item_group_option_item1"]] +  $row_g["item_group_option_quantity1"];
								
							}
							else
							{
							   $oitems[$row_g["item_group_option_item1"]] = $row_g["item_group_option_quantity1"];
							}
						}
						if($row_g["item_group_option_item2"])
						{
							if(isset($oitems[$row_g["item_group_option_item2"]]))
							{
								$oitems[$row_g["item_group_option_item2"]] = $oitems[$row_g["item_group_option_item2"]] -  $row_g["item_group_option_quantity2"];

								if($oitems[$row_g["item_group_option_item2"]] < 0)
								{
									$oitems[$row_g["item_group_option_item2"]] = 0;
								}
							}
						}
						if($row_g["item_group_option_item3"])
						{
							if(isset($oitems[$row_g["item_group_option_item3"]]))
							{
								$oitems[$row_g["item_group_option_item3"]] = $oitems[$row_g["item_group_option_item3"]] +  $row_g["item_group_option_quantity3"];
							}
							else
							{
							   $oitems[$row_g["item_group_option_item3"]] = $row_g["item_group_option_quantity3"];
							}
						}
						if($row_g["item_group_option_item4"])
						{
							if(isset($oitems[$row_g["item_group_option_item4"]]))
							{
								$oitems[$row_g["item_group_option_item4"]] = $oitems[$row_g["item_group_option_item4"]] -  $row_g["item_group_option_quantity4"];

								if($oitems[$row_g["item_group_option_item4"]] < 0)
								{
									$oitems[$row_g["item_group_option_item4"]] = 0;
								}
							}
						}
					}
					if(substr($key, 0, 2) == "rb" and $value > 0)
					{
						$parts = explode ( "_", $key);
					
						$sql_g = "select * " .
								 "from item_group_options " .
								 "where item_group_option_id = " . $parts[1];

						$res_g = mysql_query($sql_g) or dberror($sql_g);
						$row_g = mysql_fetch_assoc($res_g);
					
					
					   if($value == 1)
					   {
							if($row_g["item_group_option_item1"])
							{
								if(isset($oitems[$row_g["item_group_option_item1"]]))
								{
									$oitems[$row_g["item_group_option_item1"]] = $oitems[$row_g["item_group_option_item1"]] +  $row_g["item_group_option_quantity1"];
								}
								else
								{
								   $oitems[$row_g["item_group_option_item1"]] = $row_g["item_group_option_quantity1"];
								}
							}
							if($row_g["item_group_option_item2"])
							{
								if(isset($oitems[$row_g["item_group_option_item2"]]))
								{
									$oitems[$row_g["item_group_option_item2"]] = $oitems[$row_g["item_group_option_item2"]] - 1 * $row_g["item_group_option_quantity2"];
									if($oitems[$row_g["item_group_option_item2"]] < 0)
									{
										$oitems[$row_g["item_group_option_item2"]] = 0;
									}
								}
							}
					   }
					   elseif($value == 2)
					   {
							if($row_g["item_group_option_item3"])
							{
								if(isset($oitems[$row_g["item_group_option_item3"]]))
								{
									$oitems[$row_g["item_group_option_item3"]] = $oitems[$row_g["item_group_option_item3"]] +  $row_g["item_group_option_quantity3"];
								}
								else
								{
								   $oitems[$row_g["item_group_option_item3"]] = $row_g["item_group_option_quantity3"];
								}
							}
							if($row_g["item_group_option_item4"])
							{
								if(isset($oitems[$row_g["item_group_option_item4"]]))
								{
									$oitems[$row_g["item_group_option_item4"]] = $oitems[$row_g["item_group_option_item4"]] - 1 * $row_g["item_group_option_quantity4"];


									if($oitems[$row_g["item_group_option_item4"]] < 0)
									{
										$oitems[$row_g["item_group_option_item4"]] = 0;
									}
								}
							}
					   }
					}
				}
			}

			// create itemlist
			foreach($oitems as $key=>$value)
			{
				if($value > 0)
				{
					$sql_a = "select item_id, item_code, item_name, item_type " .
							 "from items " . 
							 "where item_id = " . $key;


					$res_a = mysql_query($sql_a) or dberror($sql_a);

					if($row_a = mysql_fetch_assoc($res_a))
					{
					
						$order_item_fields = array();
						$order_item_values = array();

						$order_item_fields[] = "order_item_order";
						$order_item_values[] = dbquote($order_id);

						$order_item_fields[] = "order_item_item";
						$order_item_values[] = $row_a["item_id"];

						$order_item_fields[] = "order_item_category";
						$order_item_values[] = $shop_data["category_id"];

						$order_item_fields[] = "order_item_type";
						$order_item_values[] = dbquote($row_a["item_type"]);

						$order_item_fields[] = "order_item_text";
						$order_item_values[] = dbquote($row_a["item_name"]);


						// get supplier's address id (first record in case of several)
						$sql_s = "select address_id, address_company, supplier_item_price, supplier_item_currency ".
								 "from items ".
								 "left join suppliers on item_id = supplier_item ".
								 "left join addresses on supplier_address = address_id ".
								 "where item_id = " . $row_a["item_id"];

						$res_s = mysql_query($sql_s) or dberror($sql_s);
						if ($row_s = mysql_fetch_assoc($res_s))
						{
							if ($row_s["address_id"])
							{
							   // price and currency

								if ($row_s["supplier_item_currency"])
								{
									$currency = get_currency($row_s["supplier_item_currency"]);
								}
								else
								{
									$currency = get_address_currency($row_s["address_id"]);
								}

								$order_item_fields[] = "order_item_supplier_currency";
								$order_item_values[] = $currency["id"];

								$order_item_fields[] = "order_item_supplier_exchange_rate";
								$order_item_values[] = $currency["exchange_rate"];

								$order_item_fields[] = "order_item_supplier_price";
								$order_item_values[] = $row_s["supplier_item_price"];

								$order_item_fields[] = "order_item_supplier_address";
								$order_item_values[] = dbquote($row_s["address_id"]);
							}
						}

						// get orders's currency  & get item data
						
						$currency = get_order_currency($order_id);

						$sql_c = "select * ".
								 "from items ".
								 "left join category_items on item_id = category_item_item ".
								 "left join categories on category_item_category = category_id ".
								 "where item_id = " . $row_a["item_id"];

						$res_c = mysql_query($sql_c) or dberror($sql_c);
						$type = "";

						if ($row_c = mysql_fetch_assoc($res_c))
						{
							$order_item_fields[] = "order_item_system_price";
							$order_item_values[] = trim($row_c["item_price"]) == "" ? "null" : dbquote($row_c["item_price"]);
							$client_price = $row_c["item_price"] / $currency["exchange_rate"] * $currency["factor"];

							$order_item_fields[] = "order_item_client_price";
							$order_item_values[] = trim($client_price) == "" ? "null" : dbquote($client_price);

							$order_item_fields[] = "order_item_cost_unit_number";

							$order_item_values[] = trim($row_c["category_cost_unit_number"]) == "" ? "null" : dbquote($row_c["category_cost_unit_number"]);
						}

						$order_item_fields[] = "order_item_po_number";
						$order_item_values[] = dbquote("");

						$order_item_fields[] = "order_item_offer_number";
						$order_item_values[] = dbquote("");   

						$order_item_fields[] = "order_item_shipment_code";
						$order_item_values[] = dbquote("");

						$order_item_fields[] = "order_item_forwarder_address";
						$order_item_values[] = dbquote("");

						$order_item_fields[] = "order_item_staff_for_discharge";
						$order_item_values[] = dbquote("");
						
						$order_item_fields[] = "order_item_no_offer_required";
						$order_item_values[] = dbquote("1");

						$order_item_fields[] = "order_item_quantity";
						$order_item_values[] = $value;

						$order_item_fields[] = "date_created";
						$order_item_values[] = "now()";

						$order_item_fields[] = "user_created";
						$order_item_values[] = dbquote(user_login());

						$sql = "insert into order_items (" . join(", ", $order_item_fields) . ") values (" . join(", ", $order_item_values) . ")";
						
						mysql_query($sql) or dberror($sql);
					}
				}
			}
		}
		
		// insert delivery addresses into table order_addresses for all items
		// not having a delivery address and beloging to the same order

		$sql_order_items = "select order_item_id, order_item_order ".
						   "from order_items ".
						   "where order_item_order = " . $order_id .
						   "    and order_item_type <= ". ITEM_TYPE_SPECIAL;


		$res = mysql_query($sql_order_items) or dberror($sql_order_items);

		while ($row = mysql_fetch_assoc($res)) 
		{
			// check if record is already there
			$sql = "select order_address_id ".
				   "from order_addresses ".
				   "where order_address_order = " . $row["order_item_order"] .
				   "    and order_address_order_item = " . $row["order_item_id"].
				   "    and order_address_type = 2";

			$res1 = mysql_query($sql) or dberror($sql);
			if ($row1 = mysql_fetch_assoc($res1)) 
			{
				//nothing
			}
			else
			{
				// insert new record
		
				$delivery_address_fields = array();
				$delivery_address_values = array();

				$delivery_address_fields[] = "order_address_order";
				$delivery_address_values[] = $row["order_item_order"];

				$delivery_address_fields[] = "order_address_order_item";
				$delivery_address_values[] = $row["order_item_id"];

				$delivery_address_fields[] = "order_address_type";
				$delivery_address_values[] = 2;

				$delivery_address_fields[] = "order_address_company";
				$delivery_address_values[] = dbquote($delivery_address["address_company"]);

				$delivery_address_fields[] = "order_address_company2";
				$delivery_address_values[] = dbquote($delivery_address["address_company2"]);

				$delivery_address_fields[] = "order_address_address";
				$delivery_address_values[] = dbquote($delivery_address["address_address"]);

				$delivery_address_fields[] = "order_address_address2";
				$delivery_address_values[] = dbquote($delivery_address["address_address2"]);

				$delivery_address_fields[] = "order_address_zip";
				$delivery_address_values[] = dbquote($delivery_address["address_zip"]);

				$delivery_address_fields[] = "order_address_place_id";
				$delivery_address_values[] = dbquote(trim($delivery_address["address_place_id"]));

				$delivery_address_fields[] = "order_address_place";
				$delivery_address_values[] = dbquote(trim($delivery_address["address_place"]));

				$delivery_address_fields[] = "order_address_country";
				$delivery_address_values[] = dbquote($delivery_address["address_country"]);

				$delivery_address_fields[] = "order_address_phone";
				$delivery_address_values[] = dbquote($delivery_address["address_phone"]);

				$delivery_address_fields[] = "order_address_fax";
				$delivery_address_values[] = dbquote($delivery_address["address_fax"]);

				$delivery_address_fields[] = "order_address_email";
				$delivery_address_values[] = dbquote($delivery_address["address_email"]);

				$delivery_address_fields[] = "order_address_parent";
				$delivery_address_values[] = dbquote($delivery_address["address_parent"]);;

				$delivery_address_fields[] = "order_address_contact";
				$delivery_address_values[] = dbquote($delivery_address["address_contact"]);

				$delivery_address_fields[] = "date_created";
				$delivery_address_values[] = "current_timestamp";

				$delivery_address_fields[] = "user_created";
				$delivery_address_values[] = dbquote($_SESSION["user_login"]);

				$sql = "insert into order_addresses (" . join(", ", $delivery_address_fields) . ") values (" . join(", ", $delivery_address_values) . ")";
				mysql_query($sql) or dberror($sql);
			}
		}
	
	}

	//send mail notification
	project_send_mail_notifications($project_id);
}
else
{
	$link = "project_new_01.php";
	redirect($link);
}

unset($_SESSION["new_project_step_0"]);
unset($_SESSION["new_project_step_1"]);
unset($_SESSION["new_project_step_2"]);
unset($_SESSION["new_project_step_3"]);
unset($_SESSION["new_project_step_4"]);
unset($_SESSION["new_project_step_5"]);


$page = new Page("projects");

$page->register_action('home', 'Home', "welcome.php");

$page->header();
$page->title("New Project");

echo "<p>", "Your request has been submitted.", "</p>";
echo "<p>", "Go directly to the list of your <a href='projects.php'>projects</a>.", "</p>";
echo "<p>", "Print a PDF of your request: <a href='project_print_project_data.php?pid=" . $project_id . "' target='_blank'>Print</a>.", "</p>";

$page->footer();

?>