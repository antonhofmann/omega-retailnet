<?php
/********************************************************************

    project_sheet_pdf_2004.php

    Creates a PDF project sheet for projects from 2004-09-27 on.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-10-07
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-23
    Version:        1.1.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require_once "include/get_functions.php";



function limit($str, $length)
{
    global $pdf;

    $length -= 2;

    while (true)
    {
        if ($pdf->GetStringWidth($str) < $length)
        {
            return $str;
        }
        else
        {
            $str = substr($str, 0, strlen($str) - 1);
        }
    }
}

$SUPPRESS_HEADERS = true;
define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 



require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');

define("COLUMN1_XPOS", 15);
define("COLUMN1_DATA_OFFSET", 22);
define("COLUMN1_DATA_WIDTH", 63);
define("COLUMN1_DATA2_OFFSET", 37);
define("COLUMN1_DATA2_WIDTH", 28);

define("COLUMN2_XPOS", 112);
define("COLUMN2_DATA_OFFSET", 40);
define("COLUMN2_DATA_WIDTH", 34);
define("COLUMN2_DATA2_WIDTH", 50);

define("TITLE_YPOS", 15);
define("GROUP1_YPOS", 25);
define("GROUP2_YPOS", 49);
define("GROUP3_YPOS", 112);
define("JUSTIFICATION_YPOS", 184);
define("JUSTIFICATION_COST_XPOS", 176);
define("SUPPLIER_YPOS", 250);

// Check access and id parameter

if (!isset($id))
{
    error("Must be called width id of an existing order record");
}



//get directly invoiced suppliers and amounts
$project = get_project(param("pid"));

$client_address = get_address($project["order_client_address"]);

$directly_invoiced_amount = 0;
$directly_invoiced_amount_loc = 0;

$suppliers = array();
$excluded_suppliers = array();

$sql_suppliers = "select DISTINCT order_item_supplier_address, address_id, address_company, address_shortcut, " .
                 " sum(order_item_quantity*order_item_system_price) as amount,  " .
				 " sum(order_item_quantity*order_item_client_price) as amount_loc " . 
				 "from order_items " . 
				 "left join addresses on order_item_supplier_address = address_id ".
				 " where (order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
                      "   and (order_item_type <= " . ITEM_TYPE_SPECIAL .
		              "   or order_item_type = " . ITEM_TYPE_SERVICES . ") " .
                      "   and order_item_order = " . $project["project_order"]  . 
				 " group by order_item_supplier_address, address_id, address_company";


$res = mysql_query($sql_suppliers) or dberror($sql_suppliers);


while ($row = mysql_fetch_assoc($res))
{
	
	$sql = "select supplier_client_invoice_id ". 
		   "from supplier_client_invoices " . 
		   "where supplier_client_invoice_supplier = " . $row["order_item_supplier_address"] .
		   " and supplier_client_invoice_client = " . $client_address["id"]  . 
		   " and supplier_client_invoice_startdate <= " . dbquote(date("Y-m-d")) . 
			   " and supplier_client_invoice_enddate >= " . dbquote(date("Y-m-d"));
	
	$res_s = mysql_query($sql) or dberror($sql);
	if ($row_s = mysql_fetch_assoc($res_s))
	{
		$suppliers[$row["address_id"]] = $row["address_shortcut"]; 
		$directly_invoiced_amount = $directly_invoiced_amount + $row["amount"];
		$directly_invoiced_amount_loc = $directly_invoiced_amount + $row["amount_loc"];
	}
	else
	{
		$excluded_suppliers[] = $row["address_id"];
	}
	
}


//get budget data
//cost, budget situation from the list of materials
$line = 1;
$group_totals = array();
$list_total = 0;
$list_total_loc = 0;

$sql_c = "select project_cost_hq_supplied, project_cost_hq_supplied_lc, " .
		 "project_cost_freight_charges,  project_cost_freight_charges_lc " . 
         "from project_costs " . 
		 "where project_cost_order = " . $id;
$res_c = mysql_query($sql_c) or dberror($sql_c);
if ($row_c = mysql_fetch_assoc($res_c))
{
	$group_totals["HQ Supplied Items"] = $row_c["project_cost_hq_supplied"];
	$group_totals["Freight Charges"] = $row_c["project_cost_freight_charges"];
	$list_total = $row_c["project_cost_hq_supplied"] + $row_c["project_cost_freight_charges"];
	$list_total_loc = $row_c["project_cost_hq_supplied_lc"] + $row_c["project_cost_freight_charges_lc"];
}



$local_production = 0;
$sql_c = "select order_item_cost_group, sum(order_item_quantity*order_item_system_price) as total, " .
         "sum(order_item_quantity*order_item_client_price) as total_loc " .
         "from order_items " . 
	     "where order_item_exclude_from_ps = 1 " . 
	     " and order_item_order = " .  $id . 
		 " group by order_item_cost_group";

$res_c = mysql_query($sql_c) or dberror($sql_c);
while($row_c = mysql_fetch_assoc($res_c))
{
	$local_production = $row_c["total"];
	$local_production_loc = $row_c["total_loc"];

	if($row_c["order_item_cost_group"] == 2) // HQ Supplied
	{
		$group_totals["HQ Supplied Items"] = $group_totals["HQ Supplied Items"] - $local_production;
		$list_total = $list_total - $local_production;
		$list_total_loc = $list_total_loc - $local_production_loc;
	}
	elseif($row_c["order_item_cost_group"] == 6) // Freight Charges
	{
		$group_totals["Freight Charges"] = $group_totals["Freight Charges"] - $local_production;
		$list_total = $list_total - $local_production;
		$list_total_loc = $list_total_loc - $local_production_loc;
	}
	
}
if($directly_invoiced_amount > 0)
{
	$planned = ceil(($list_total - $directly_invoiced_amount)/1000) * 1000;
	$planned_loc = ceil(($list_total_loc - $directly_invoiced_amount_loc)/1000) * 1000;


}
else
{
	$planned = ceil($list_total/1000) * 1000;
	$planned_loc = ceil($list_total_loc/1000) * 1000;
}



// Select project data

$sql = "select project_number, project_cost_center, project_account_number, ".       
       "    project_id, project_closing_date, project_ps_product_line, " .
       "    project_ps_spent_for, " .
       "    project_opening_date, " .
       "    project_closing_date_string, " .
       "    project_budget_total, project_budget_committed, project_budget_spent, " .
       "    project_budget_after_project, project_name, product_line_name, " .
       "    project_planned_amount_current_year, " .
       "    project_planned_amount_next_year, " .
       "    currency_symbol, currency_exchange_rate, currency_factor, " .
       "    project_share_company, project_share_other, " .
       "    project_approval_person1, project_approval_person2, ".
       "    project_approval_person3, project_approval_person4, ".
       "    project_approval_person5, project_approval_person6, " .
       "    project_approval_person7, project_approval_person8, " .
       "    project_approval_person9, business_unit_name, " .
       "    unix_timestamp(project_approval_date1) as project_approval_date1, " .
       "    unix_timestamp(project_approval_date2) as project_approval_date2, " .
       "    unix_timestamp(project_approval_date3) as project_approval_date3, " .
       "    unix_timestamp(project_approval_date4) as project_approval_date4, " .
       "    unix_timestamp(project_approval_date5) as project_approval_date5, " .
       "    unix_timestamp(project_approval_date6) as project_approval_date6, " .
       "    unix_timestamp(project_approval_date7) as project_approval_date7, " .
       "    unix_timestamp(project_approval_date8) as project_approval_date8, " .
       "    unix_timestamp(project_approval_date9) as project_approval_date9, " .
       "    project_approval_description, project_sheet_name, " .
       "    suppliers.address_company, suppliers.address_company2, " .
       "    suppliers.address_address, suppliers.address_address2, " .
       "    suppliers.address_zip, suppliers.address_place, " .
       "    country_name, user_firstname, user_name, user_phone, user_email, " .
	   "    project_costtype_text, projectkind_name " .
       "from orders " .
	   "    left join projects on order_id = project_order " .
	   "    left join projectkinds on projectkind_id = project_projectkind " . 
       "    left join product_lines on project_product_line = product_line_id " .
       "    left join addresses on order_client_address = addresses.address_id  " .
       "    left join currencies on addresses.address_currency = currency_id " .
       "    left join addresses as suppliers on order_supplier_address = suppliers.address_id " .
       "    left join countries on suppliers.address_country = country_id " .
       "    left join users on suppliers.address_contact = user_id " .
       "    left join business_units on business_unit_id = project_business_unit " .
	   "    left join project_costs on project_cost_order = order_id " . 
	   "    left join project_costtypes on project_costtype_id = project_cost_type " .
       "where order_id = $id";

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);


// Create and setup PDF document
$pdf = new TCPDF("P", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 23, 8);
$pdf->setPrintHeader(false);

$pdf->Open();
$pdf->SetTitle($row['project_sheet_name']);
$pdf->SetAuthor(BRAND . "Retail Net");
$pdf->SetDisplayMode(150);

$pdf->AddFont('arialn');
$pdf->AddFont('arialn', 'B');

$pdf->AddPage();
$pdf->SetLeftMargin(0);

/*
$pdf->Image('../pictures/omega_logo.jpg',16,8,33);

$pdf->SetFont('arialn','B',12);
$pdf->SetY(10);
$pdf->Cell(0,12, "Project Sheet", 0, 0, 'R');
*/

// Title

$pdf->SetFont("arialn", "B", 18);
$pdf->SetXY(COLUMN1_XPOS, TITLE_YPOS);
$pdf->Cell(0, 0, strtoupper($row['project_sheet_name']));

// Cost center

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS, GROUP1_YPOS);
$pdf->Cell(0, 6, "Cost Center");

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA_OFFSET, GROUP1_YPOS);
$pdf->Cell(COLUMN1_DATA_WIDTH, 6, $row["project_cost_center"], 1, 0, "L");

// Account number

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS, GROUP1_YPOS + 6 + 6);
$pdf->Cell(0, 6, "Account Number");

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA_OFFSET, GROUP1_YPOS + 6 + 6);
$pdf->Cell(COLUMN1_DATA_WIDTH, 6, $row["project_account_number"], 1, 0, "L");

// Opening date

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS, GROUP2_YPOS);
$pdf->Cell(0, 6, "Opening Date");

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA_OFFSET, GROUP2_YPOS);
$pdf->Cell(COLUMN1_DATA_WIDTH, 6, to_system_date($row["project_opening_date"]), 1, 0, "L");

// Closing date String

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS, GROUP2_YPOS + 6 + 6);
$pdf->Cell(0, 6, "Closing Date");

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA_OFFSET, GROUP2_YPOS + 6 + 6);
$pdf->Cell(COLUMN1_DATA_WIDTH, 6, $row["project_closing_date_string"], 1, 0, "L");


// Product Line
$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS, GROUP2_YPOS + 6 + 18);
$pdf->Cell(0, 6, "Product Line");

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA_OFFSET, GROUP2_YPOS + 6 + 18);
$pdf->Cell(COLUMN1_DATA_WIDTH, 6, $row["project_ps_product_line"], 1, 0, "L");


// Spent For
$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS, GROUP2_YPOS + 6 + 30);
$pdf->Cell(0, 6, "Spent for");

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA_OFFSET, GROUP2_YPOS + 6 + 30);
$pdf->Cell(COLUMN1_DATA_WIDTH, 6, $row["project_ps_spent_for"], 1, 0, "L");

// Budget situation

$pdf->SetFont("arialn", "B", 10);
$pdf->SetXY(COLUMN1_XPOS, GROUP3_YPOS - 10);
$pdf->Cell(0, 4, "BUDGET SITUATION");

// Total budget in current year for responsible

$currency = get_system_currency_symbol();

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA2_OFFSET, GROUP3_YPOS - 3);
$pdf->Cell(0, 3, "In $currency");
$pdf->SetXY(COLUMN1_XPOS, GROUP3_YPOS);
$pdf->MultiCell(0, 3.5, "Total Budget Current \n Year for Responsible");

$number = number_format($row["project_budget_total"], 2, ".", "'");
$pdf->SetFont("arialn", "", 10);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA2_OFFSET, GROUP3_YPOS);
$pdf->Cell(COLUMN1_DATA2_WIDTH, 7, $number, 1, 0, "R");

// Committed in current year

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS, GROUP3_YPOS + 10);
$pdf->Cell(0, 7, "Committed Current \nYear");

$number = number_format($row["project_budget_committed"], 2, ".", "'");
$pdf->SetFont("arialn", "", 10);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA2_OFFSET, GROUP3_YPOS + 10);
$pdf->Cell(COLUMN1_DATA2_WIDTH, 7, $number, 1, 0, "R");

// Already spent current year

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS, GROUP3_YPOS + 20);
$pdf->Cell(0, 7, "Already Spent \nCurrent Year");

$number = number_format($row["project_budget_spent"], 2, ".", "'");
$pdf->SetFont("arialn", "", 10);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA2_OFFSET, GROUP3_YPOS + 20);
$pdf->Cell(COLUMN1_DATA2_WIDTH, 7, $number, 1, 0, "R");

// Open to commit after this project

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS, GROUP3_YPOS + 30);
$pdf->MultiCell(0, 3.5, "Open to Commit after\nthis Project");

$number = number_format($row["project_budget_after_project"], 2, ".", "'");
$pdf->SetFont("arialn", "", 10);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA2_OFFSET, GROUP3_YPOS + 30);
$pdf->Cell(COLUMN1_DATA2_WIDTH, 7, $number, 1, 0, "R");


//column 2
// Project number

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS, GROUP1_YPOS);
$pdf->Cell(0, 6, "Project Number");

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, GROUP1_YPOS);
$pdf->Cell(COLUMN2_DATA2_WIDTH, 6, $row["project_number"], 1, 0, "L");

// Project legal type and project kind
$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS, GROUP1_YPOS + 6);
$pdf->Cell(0, 8, "Legal Type - Project Kind");

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, GROUP1_YPOS + 6);
$pdf->Cell(COLUMN2_DATA2_WIDTH, 6, $row["project_costtype_text"]. " - ". $row["projectkind_name"], 1, 0, "L");

// Project name

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS, GROUP1_YPOS + 15);
$pdf->Cell(0, 6, "Project Name");

$pdf->SetFont("arialn", "", 7);
$text = limit($row["project_name"], COLUMN2_DATA2_WIDTH);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, GROUP1_YPOS + 15);
$pdf->Cell(COLUMN2_DATA2_WIDTH, 6, $text, 1, 0, "L");


// Planned amount in current year

$width1 = round(COLUMN2_DATA2_WIDTH / 6);
$width2 = COLUMN2_DATA2_WIDTH - $width1;

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS, GROUP2_YPOS);
$pdf->Cell(0, 7, "Planned Amount \nCurrent Year");
//$number = round($row["project_planned_amount_current_year"] * $row["currency_exchange_rate"] / $row["currency_factor"]);

$number = number_format($planned, 2, ".", "'");
$pdf->SetFont("arialn", "", 9);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, GROUP2_YPOS);
$pdf->Cell($width1, 7, $currency, 1, 0, "C");
$pdf->Cell($width2, 7, $number, 1, 0, "R");

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS, GROUP2_YPOS + 7);
$pdf->Cell(0, 7, "Local Currency");

//$number = number_format($row["project_planned_amount_current_year"], 2, ".", "'");
$number = number_format($planned_loc, 2, ".", "'");
$pdf->SetFont("arialn", "", 9);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, GROUP2_YPOS + 7);
$pdf->Cell($width1, 7, $row["currency_symbol"], 1, 0, "C");
//$pdf->Cell($width2, 7, $number, 1, 0, "R");
$pdf->Cell($width2, 7, "", 1, 0, "R");

// Planned amount next year

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS, GROUP2_YPOS + 7 + 7 + 3);
$pdf->Cell(0, 7, "Planned Amount Next Year");

if($row["currency_factor"]) {
	$number = round($row["project_planned_amount_next_year"] * $row["currency_exchange_rate"] / $row["currency_factor"]);
}
else
{
	$number = 0;
}
$number = number_format($number, 2, ".", "'");
$pdf->SetFont("arialn", "", 9);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, GROUP2_YPOS + 7 + 7 + 3);
$pdf->Cell($width1, 7, $currency, 1, 0, "C");
//$pdf->Cell($width2, 7, $number, 1, 0, "R");
$pdf->Cell($width2, 7, "", 1, 0, "R");

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS, GROUP2_YPOS + 7 + 7 + 3 + 7);
$pdf->Cell(0, 7, "Local Currency");

$number = number_format($row["project_planned_amount_next_year"], 2, ".", "'");
$pdf->SetFont("arialn", "", 9);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, GROUP2_YPOS + 7 + 7 + 3 + 7);
$pdf->Cell($width1, 7, $row["currency_symbol"], 1, 0, "C");
//$pdf->Cell($width2, 7, $number, 1, 0, "R");
$pdf->Cell($width2, 7, "", 1, 0, "R");

// Share Brand Biel, share other

$width = COLUMN2_DATA2_WIDTH - COLUMN2_DATA_WIDTH;
$width = 10;
$ypos = GROUP2_YPOS + 7 + 7 + 3 + 7 + 7 + 3 + 2;

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS, $ypos);
$pdf->Cell(0, 6, "Share " . BRAND . " Biel in %");

$pdf->SetFont("arialn", "", 7);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, $ypos);
$pdf->Cell($width, 6, number_format($row["project_share_company"], 0) . "%", 1, 0, "R");

$pdf->SetFont("arialn", "B", 7);
$text = "Share Others in %";
$text_width = $pdf->GetStringWidth($text) + 4;
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET + COLUMN2_DATA2_WIDTH - $width - $text_width, $ypos);
$pdf->Cell(0, 6, $text);

$pdf->SetFont("arialn", "", 7);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET + COLUMN2_DATA2_WIDTH - $width, $ypos);
$pdf->Cell($width, 6, number_format($row["project_share_other"], 0) . "%", 1, 0, "R");

// Approval

$pdf->SetFont("arialn", "B", 10);
$pdf->SetXY(COLUMN2_XPOS, GROUP3_YPOS - 10);
$pdf->Cell(0, 4, "APPROVAL");

// Column headers for approval table

$width = COLUMN2_DATA_OFFSET + COLUMN2_DATA2_WIDTH;
$width1 = floor($width * 0.23);
$width2 = floor($width * 0.20);
$width3 = floor($width * 0.40);
$width4 = $width - $width1 - $width2 - $width3;

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS, GROUP3_YPOS);
$pdf->Cell($width1, 7, "Name", 1, 0, "C");
$pdf->Cell($width2, 7, "", 1, 0, "C");
$pdf->Cell($width3, 7, "Signature", 1, 0, "C");
$pdf->Cell($width4, 7, "Date", 1, 0, "C");
$pdf->SetXY(COLUMN2_XPOS + $width1, GROUP3_YPOS + 1);
$pdf->MultiCell($width2, 2.5, "Deadline for\napproval", 0, "C");

// Data columns for approval table

$ypos = GROUP3_YPOS + 7;
$pdf->SetFont("arialn", "", 7);
$lines = 0;


for ($i = 1; $i <= 9; $i++)
{
    $text = limit($row["project_approval_person" . $i], $width1);
	 $text = $row["project_approval_person" . $i];
    if ($text)
    {
        $date = $row["project_approval_date" . $i] ? date("d.m.Y", $row["project_approval_date" . $i]) : "";

        $pdf->SetXY(COLUMN2_XPOS, $ypos);
        $pdf->Cell($width1, 6, $text, 1);
        $pdf->Cell($width2, 6, $date, 1, 0, "C");
        $pdf->Cell($width3, 6, "", 1);
        $pdf->Cell($width4, 6, "", 1);
        $lines++;
        $ypos += 6;
    }
}

while($lines < 9)
{
    $pdf->SetXY(COLUMN2_XPOS, $ypos);
    $pdf->Cell($width1, 6, "", 1);
    $pdf->Cell($width2, 6, "", 1, 0, "C");
    $pdf->Cell($width3, 6, "", 1);
    $pdf->Cell($width4, 6, "", 1);
    $lines++;
    $ypos += 6;
}

// Table for description and financial justification

$text_width = COLUMN2_XPOS - COLUMN1_XPOS + COLUMN2_DATA_OFFSET;
$full_width = $text_width + COLUMN2_DATA2_WIDTH;

$pdf->SetFont("arialn", "B", 6);
$pdf->SetTextColor(255, 0, 0);
$pdf->SetXY(COLUMN1_XPOS, JUSTIFICATION_YPOS - 6);
$pdf->Cell(COLUMN1_DATA2_OFFSET, 3, "DESCRIPTION", 0, 2);
$pdf->Cell(COLUMN1_DATA2_OFFSET, 3, "FINANCIAL JUSTIFICATION");

$pdf->SetFont("arialn", "", 6);
$pdf->SetTextColor(0, 0, 0);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA2_OFFSET, JUSTIFICATION_YPOS - 6);
$pdf->Cell(80, 3, "(Goals, Actions, Timing, Quantities)", 0, 2);
$pdf->Cell(80, 3, "(Costs planning by items, return on investment, ...)");

// Business Unit

$pdf->SetFont("arialn", "", 10);
$pdf->SetXY(COLUMN1_XPOS, JUSTIFICATION_YPOS+2);
$pdf->Cell(80,3, $row['business_unit_name']);

$pdf->SetFont("arialn", "", 8);
$ypos = JUSTIFICATION_YPOS+6+4.5;
$justification_cost_sum = 0;


//asort($suppliers);
//$directly_invoiced_amount

foreach($group_totals as $group_name=>$amount)
{
	$pdf->SetXY(COLUMN1_XPOS, $ypos);
	$pdf->Cell(20, 3, $group_name, 0, "", "L");
	$pdf->Cell(30, 3, number_format($amount, 2, ".", "'"), 0, "", "R");
	$ypos +=4.5;

	if($group_name == "HQ Supplied Items" and $directly_invoiced_amount > 0)
	{
		asort($suppliers);
		$group_name = "./. direct invoicing:";
		$pdf->SetXY(COLUMN1_XPOS, $ypos);
		$pdf->Cell(20, 3,$group_name , 0, "", "L");
		$pdf->Cell(30, 3, number_format($directly_invoiced_amount, 2, ".", "'"), 0, "", "R");
		$pdf->Cell(30, 3, " by " .  implode(', ', $suppliers), 0, "", "L");
		$ypos +=4.5;

		$pdf->SetXY(COLUMN1_XPOS, $ypos);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(20, 3, "Total 1", 0, "", "L");
		
		$pdf->Cell(30, 3, number_format($amount - $directly_invoiced_amount, 2, ".", "'"), 0, "", "R");
		$ypos +=4.5;

		$list_total = $list_total - $directly_invoiced_amount;
		$pdf->SetFont("arialn", "", 8);
	}


	
}
$pdf->SetFont("arialn", "B", 8);
$pdf->SetXY(COLUMN1_XPOS, $ypos);
if($directly_invoiced_amount > 0)
{
	$pdf->Cell(20, 3, "Total 2", 0, "", "L");

	$list_total = round(($list_total+500)/1000, 0) * 1000;
	$pdf->Cell(30, 3, number_format($list_total, 2, ".", "'"), 0, "", "R");
}
else
{
	$pdf->Cell(20, 3, "Total", 0, "", "L");
	$pdf->Cell(30, 3, number_format($list_total, 2, ".", "'"), 0, "", "R");
}

$ypos +=9;

$sql = "select project_financial_justification_description, " .
       "    project_financial_justification_cost " .
       "from project_financial_justifications " .
       "where project_financial_justification_project = " . $row['project_id'] . " " .
       "order by project_financial_justification_priority asc";

$res_2 = mysql_query($sql);
$number_of_description_rows = (mysql_num_rows($res_2) > 10) ? mysql_num_rows($res_2) : 10 ;
$longer = false;

while ($row_2 = mysql_fetch_assoc($res_2))
{
    if (LINE_BREAK)
    {
		$text = $row_2['project_financial_justification_description'];
        while ($text)
        {
            if ($longer)
            {
                $ypos +=4.5;
                $number_of_description_rows++;
            }
            $longer = true;
            $text_out = substr($text,0,60);
            $text = substr($text,60);

            $pdf->SetXY(COLUMN1_XPOS, $ypos);
            $pdf->Cell(JUSTIFICATION_COST_XPOS - COLUMN1_XPOS, 3, 
                   $text_out, 0, "", "L");
        }
    }
    else
    {
        $text = limit($row_2['project_financial_justification_description'], (JUSTIFICATION_COST_XPOS - COLUMN1_XPOS));
        $pdf->SetXY(COLUMN1_XPOS, $ypos);
        $pdf->Cell(JUSTIFICATION_COST_XPOS - COLUMN1_XPOS, 3, $text, 0, "", "L");
    }
    
    
    if (!LINE_BREAK)
    {
        $ypos +=4.5;
    }
    
}

$pdf->SetLineWidth(0.5);
$pdf->Rect(COLUMN1_XPOS, JUSTIFICATION_YPOS +0.5, $full_width, ($number_of_description_rows + 3) * 4.5);
$pdf->SetLineWidth(0.2);

for ($i = 1; $i <= ($number_of_description_rows +1); $i++)
{
    $pdf->Line(COLUMN1_XPOS, JUSTIFICATION_YPOS + 5 + $i * 4.5, COLUMN1_XPOS + $full_width, JUSTIFICATION_YPOS + 5 + $i * 4.5);
}


// Table for supplier information

$supplier_ypos = SUPPLIER_YPOS + (($number_of_description_rows - 10) * 4.5);

$pdf->SetLineWidth(0.5);
$pdf->Rect(COLUMN1_XPOS, $supplier_ypos, $full_width, 6 * 4.5);
$pdf->SetLineWidth(0.2);

for ($i = 1; $i <= 5; $i++)
{
    $pdf->Line(COLUMN1_XPOS, $supplier_ypos + $i * 4.5, COLUMN1_XPOS + $full_width, $supplier_ypos + $i * 4.5);
}

$supplier = smart_join(", ", array($row["address_company"], $row["address_company2"]));
$supplier = limit("SUPPLIER: " . $supplier, $full_width);

$pdf->SetFont("arialn", "B", 8);
$pdf->SetXY(COLUMN1_XPOS, $supplier_ypos);
$pdf->Cell($full_width, 4.5, $supplier, 0,2);

$pdf->SetFont("arialn", "", 8);

$address = smart_join(" ", array($row["address_address"], $row["address_address2"]));
$zipplace = smart_join(", ", array($row["address_zip"], $row["address_place"]));
$address = smart_join(", ", array($address, $zipplace, $row["country_name"]));
$address = limit("Address: " . $address, $full_width);

$contact = smart_join(" ", array($row["user_name"], $row["user_firstname"]));
$contact = limit("Contact Person: " . $contact, $full_width);

$phone = limit("Phone: " . $row["user_phone"], $full_width);
$email = limit("Email: " . $row["user_email"], $full_width);

$pdf->Cell($full_width, 4.5, $address, 0, 2);
$pdf->Cell($full_width, 4.5, $contact, 0, 2);
$pdf->Cell($full_width, 4.5, $phone, 0, 2);
$pdf->Cell($full_width, 4.5, $email, 0, 2);

$pdf->Output();

?>