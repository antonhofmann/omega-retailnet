<?php
/********************************************************************

    project_edit_pos_data.php

    Edit POS Data

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-02-18
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-02-18
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "../shared/func_posindex.php";

if (has_access("can_edit_pos_data") or has_access("can_edit_pos_calendar_data"))
{
}
else
{
	check_access("can_edit_pos_data");
	//redirect("noaccess.php");
}


/********************************************************************
    prepare all data needed
*********************************************************************/
$user_roles = get_user_roles(user_id());

// read project and order details
$project = get_project(param("pid"));

$standard_dsitribution_channel = get_standard_distribution_channel($project["project_cost_type"], $project["project_postype"], $project["project_pos_subclass"]);

$tracking_info = array();
$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
	   "concat(user_name, ' ', user_firstname) as user_name " . 
	   "from projecttracking " . 
       "left join users on user_id = projecttracking_user_id " . 
       "where projecttracking_project_id = " . param("pid") . 
	   " and projecttracking_field = 'project_real_opening_date' " . 
	   " order by projecttracking_time";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$tracking_info[] = array("projecttracking_oldvalue"=>$row["projecttracking_oldvalue"],
		"projecttracking_newvalue"=>$row["projecttracking_newvalue"],
		"projecttracking_comment"=>$row["projecttracking_comment"],
		"projecttracking_time"=>$row["projecttracking_time"],
		"user_name"=>$row["user_name"]
		);
}


// get company's address
$client_address = get_address($project["order_client_address"]);
$franchisee_address = get_address($project["order_franchisee_address_id"]);

if(count($franchisee_address) == 0)
{
	$franchisee_address["place_id"] = 0;
}

// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ".
                 "order by country_name";

//get addresses from pos index
if(($project["project_projectkind"] == 1 or $project["project_projectkind"] == 6) and $project["pipeline"] == 1) //POS is in pipeline and it is a new POS
{
	$sql_posaddresses = "select posaddress_id, concat(posaddress_place, ', ', posaddress_name) as posaddress " .
						"from posaddressespipeline " . 
						"where posaddress_country = " . $project["order_shop_address_country"] . 
						" order by posaddress_place, posaddress_name";

}
else
{
	$sql_posaddresses = "select posaddress_id, concat(posaddress_place, ', ', posaddress_name) as posaddress " .
						"from posaddresses " . 
						"where posaddress_country = " . $project["order_shop_address_country"] . 
						" order by posaddress_place, posaddress_name";
	
}


//get pos data
$table = "posaddresses";
$table2 = "posareas";

if($project["pipeline"] == 0)
{
	$pos_data = get_poslocation($project["posaddress_id"], "posaddresses");
	$pos_order_sql = "select * from posorders where posorder_order = " . $project["order_id"];
}
elseif($project["pipeline"] == 1)
{
	$pos_data = get_poslocation_from_pipeline($project["posaddress_id"], $project["order_id"]);
	$table = "posaddressespipeline";
	$table2 = "posareaspipeline";

	$pos_order_sql = "select * from posorderspipeline where posorder_order = " . $project["order_id"];
}

//neighbourhood
$neighbourhoods = array();
$res = mysql_query($pos_order_sql) or dberror($pos_order_sql);
if ($row = mysql_fetch_assoc($res))
{
	$neighbourhoods["Shop on Left Side"] = $row["posorder_neighbour_left"];
	$neighbourhoods["Shop on Right Side"] = $row["posorder_neighbour_right"];
	$neighbourhoods["Shop Across Left Side"] = $row["posorder_neighbour_acrleft"];
	$neighbourhoods["Shop Across Right Side"] = $row["posorder_neighbour_acrright"];
	$neighbourhoods["Other Brands in Area"] = $row["posorder_neighbour_brands"];
}	

//posareas
$posareas = array();
$sql = "select posareatype_id, posareatype_name " . 
	   "from posareatypes " . 
	   " order by posareatype_name";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$posareas[$row["posareatype_id"]]	= $row["posareatype_name"];
}

$pos_posareas = array();

if(count($pos_data) > 0)
{
	$sql = "select posarea_area " . 
		   "from $table2 " . 
		   "where posarea_posaddress = " . dbquote($pos_data["posaddress_id"]) . 
		   " order by posarea_area";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$pos_posareas[$row["posarea_area"]]	= $row["posarea_area"];
	}
}

$sql_franchisees = "select address_id, concat(address_company, ', ', address_place) as name " . 
                   "from addresses " . 
				   "where address_active = 1 " .
				   "and address_canbefranchisee = 1  " . 
				   "and (address_parent = " . $project["order_client_address"] . 
				   " or address_id = " . $project["order_client_address"] . 
				   "  or address_canbefranchisee_worldwide = 1) " .  
				   " order by address_company";



//get former franchisee address
$former_franchisee_address = array();
if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 3)
{
	
	$sql = "select order_franchisee_address_id " . 
		   "from posorders " . 
		   "left join orders on order_id = posorder_order " .
		   "where posorder_type = 1 and (posorder_project_kind = 1 or posorder_project_kind = 2) " . 
		   " and posorder_posaddress = " . $project["posaddress_id"] . 
		   " and order_actual_order_state_code = '820' " . 
		   " order by posorder_id DESC";	

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$former_franchisee_address = get_address($row["order_franchisee_address_id"]);

	}
}


$ratings = array();
$ratings[5] = "bad";
$ratings[4] = "poor";
$ratings[3] = "good";
$ratings[2] = "very good";
$ratings[1] = "excellent";


$sql_floors = "select floor_id, floor_name " . 
		     "from floors " . 
		      " order by  floor_id";

$floors = array();
$res = mysql_query($sql_floors) or dberror($sql_floors);
while ($row = mysql_fetch_assoc($res))
{
	$floors[$row["floor_id"]] = $row["floor_name"];
}


/********************************************************************
    Get Cost Monitoring Sheet Budget Data
*********************************************************************/ 
$sql =  "select * from project_costs " .
        "where project_cost_order = " . $project["project_order"];

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

$project_cost_sqms = $row["project_cost_sqms"];
$project_cost_gross_sqms = $row["project_cost_gross_sqms"];
$project_cost_totalsqms = $row["project_cost_totalsqms"];
$project_cost_bakoffocesqms = $row["project_cost_backofficesqms"];
$project_cost_numfloors = $row["project_cost_numfloors"];
$project_cost_floorsurface1 = $row["project_cost_floorsurface1"];
$project_cost_floorsurface2 = $row["project_cost_floorsurface2"];
$project_cost_floorsurface3 = $row["project_cost_floorsurface3"];

//get total surface from posorders in case it is zero
if(!$project_cost_gross_sqms and array_key_exists("posaddress_store_grosssurface", $pos_data))
{
	$project_cost_gross_sqms = $pos_data["posaddress_store_grosssurface"];
}
if(!$project_cost_totalsqms and array_key_exists("posaddress_store_totalsurface", $pos_data))
{
	$project_cost_totalsqms = $pos_data["posaddress_store_totalsurface"];
}
if(!$project_cost_bakoffocesqms and array_key_exists("posaddress_store_backoffice", $pos_data))
{
	$project_cost_bakoffocesqms = $pos_data["posaddress_store_backoffice"];
}
if(!$project_cost_numfloors and array_key_exists("posaddress_store_numfloors", $pos_data))
{
	$project_cost_numfloors = $pos_data["posaddress_store_numfloors"];
}
if(!$project_cost_floorsurface1 and array_key_exists("posaddress_store_floorsurface1", $pos_data))
{
	$project_cost_floorsurface1 = $pos_data["posaddress_store_floorsurface1"];
}
if(!$project_cost_floorsurface2 and array_key_exists("posaddress_store_floorsurface2", $pos_data))
{
	$project_cost_floorsurface2 = $pos_data["posaddress_store_floorsurface2"];
}
if(!$project_cost_floorsurface3 and array_key_exists("posaddress_store_floorsurface3", $pos_data))
{
	$project_cost_floorsurface3 = $pos_data["posaddress_store_floorsurface3"];
}


//sales
$sql_distribution_channels = "select mps_distchannel_id, concat(mps_distchannel_group, ' - ', mps_distchannel_name , ' - ', mps_distchannel_code) as channel from mps_distchannels order by mps_distchannel_group, mps_distchannel_name, mps_distchannel_code ";



/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

$form->add_section("Project");
$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid", $project["project_order"]);
$form->add_hidden("pipeline", $project["pipeline"]);


//Project Head
$order_state_name = get_actual_order_state_name($project["order_actual_order_state_code"], 1);

$form->add_label("project_number", "Project Number", 0, $project["project_number"]);
$form->add_label("treatment_state", "Treatment State", 0, $project["project_state_text"]);
$form->add_lookup("product_line", "Product Line", "product_lines", "product_line_name", 0, $project["project_product_line"]);
$form->add_lookup("product_line_subclass", "Product Line Subclass", "productline_subclasses", "productline_subclass_name", 0, $project["project_product_line_subclass"]);
$form->add_lookup("project_postype", "POS Type", "postypes", "postype_name", 0, $project["project_postype"]);
$form->add_lookup("project_pos_subclass", "POS Type Subclass", "possubclasses", "possubclass_name", NOTNULL, $project["project_pos_subclass"]);

$form->add_label("type3", "Project Legal Type", 0, $project["project_costtype_text"]);
$form->add_label("type2", "Project Kind", 0, $project["projectkind_name"]);

$form->add_label("order_date", "Project Starting Date", 0, to_system_date($project["order_date"]));

if($project["project_projectkind"] != 4 and $project["project_projectkind"] != 5) //Take Over and lease renewal
{
	$form->add_label("planned_opening_date", "Client's preferred Shop Opening Date", 0, to_system_date($project["project_planned_opening_date"]));
}

$form->add_label("real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]));
$form->add_label("actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]));

$form->add_label("status", "Status", 0, $project["order_actual_order_state_code"]  . " " . $order_state_name);


$line = "concat(user_name, ' ', user_firstname)";
if ($project["project_retail_coordinator"])
{
    $form->add_lookup("retail_coordinator", "Project Manager", "users", $line, 0, $project["project_retail_coordinator"]);
}
else
{
    $form->add_label("project_retail_coordinator", "Project Manager");
}


if ($project["order_retail_operator"])
{
    $form->add_lookup("retail_operator", "Retail Operator", "users", $line, 0, $project["order_retail_operator"]);
}
else
{
    $form->add_label("order_retail_operator", "Retail Operator");
}


$client = $client_address["company"] . ", " .
          $client_address["zip"] . " " . $client_address["place"] . ", " .
          $client_address["country_name"];


$form->add_label("client_address", "Client", 0, $client);


/*
if($project["project_cost_type"] == 1 
   and ($project["project_projectkind"] == 1 or $project["project_projectkind"] == 6
   or $project["project_projectkind"] == 3 or $project["project_projectkind"] == 4)) // new corporate, relocation, take ober, takover renovation
{
	$form->add_edit("eprepnr", "Enterprise Reporting Number:", 0, $pos_data["posaddress_eprepnr"], TYPE_CHAR);
}
else
{
	$form->add_hidden("eprepnr", $pos_data["posaddress_eprepnr"]);
}
*/

$form->add_edit("eprepnr", "Enterprise Reporting Number:", 0, $pos_data["posaddress_eprepnr"], TYPE_CHAR);


//End Project Head

$form->add_section("POS Location Address");

if(count($pos_data) > 0)
{
	$form->add_hidden("posaddress_id", $pos_data["posaddress_id"]);
	
	if($project["project_actual_opening_date"] == '0000-00-00' or !$project["project_actual_opening_date"]) 
	{
		$form->add_edit("shop_address_company", "Project Name*", NOTNULL, $project["order_shop_address_company"], TYPE_CHAR, 0, 0, 2, "shop_address_company");
		$form->add_edit("shop_address_company2", "", 0, $project["order_shop_address_company2"], TYPE_CHAR, 0, 0, 2, "shop_address_company2");
		$form->add_edit("shop_address_address", "Address*", NOTNULL, $project["order_shop_address_address"], TYPE_CHAR, 0, 0, 2, "shop_address_address");
		$form->add_edit("shop_address_address2", "", 0, $project["order_shop_address_address2"], TYPE_CHAR, 0, 0, 2, "shop_address_address2");
		$form->add_edit("shop_address_zip", "ZIP*", NOTNULL, $project["order_shop_address_zip"], TYPE_CHAR, 20);
		
		$form->add_list("posaddress_place_id", "City Selection*",
			"select place_id, place_name from places where place_country = " . dbquote($pos_data["posaddress_country"]) . " order by place_name", NOTNULL | SUBMIT, $pos_data["posaddress_place_id"]);

		$form->add_edit("shop_address_place", "City*", NOTNULL | DISABLED, $project["order_shop_address_place"], TYPE_CHAR, 20);

		$form->add_label("province", "Province", 0, $pos_data["province_canton"]);

		$form->add_list("shop_address_country", "Country*", $sql_countries, NOTNULL, $project["order_shop_address_country"]);
		$form->add_edit("shop_address_phone", "Phone", 0, $project["order_shop_address_phone"], TYPE_CHAR, 20);
		$form->add_edit("shop_address_fax", "Fax", 0, $project["order_shop_address_fax"], TYPE_CHAR, 20);
		$form->add_edit("shop_address_email", "Email", 0, $project["order_shop_address_email"], TYPE_CHAR, 50);
		$form->add_edit("sapnr", "SAP Number", 0, $pos_data["posaddress_sapnumber"], TYPE_CHAR);

		$form->add_section("Google Map Coordinates");
		$form->add_label("GM", "Google Map", RENDER_HTML);
		$form->add_edit("posaddress_google_lat", "Latitude", NOTNULL | DISABLED, $pos_data["posaddress_google_lat"]);
		$form->add_edit("posaddress_google_long", "Longitude", NOTNULL | DISABLED, $pos_data["posaddress_google_long"]);
		$form->add_hidden("posaddress_google_precision", 1);
		$form->add_label("gmcheck", "");
		$form->add_hidden("overwrite_posaddress_data", 1);
	}
	else
	{
		$form->add_label("shop_address_company", "Project Name", 0, $project["order_shop_address_company"]);
		$form->add_label("shop_address_company2", "", 0, $project["order_shop_address_company2"]);
		$form->add_label("shop_address_address", "Address", 0, $project["order_shop_address_address"]);
		$form->add_label("shop_address_address2", "", 0, $project["order_shop_address_address2"]);
		$form->add_label("shop_address_zip", "ZIP*", 0, $project["order_shop_address_zip"]);
		
		$form->add_hidden("posaddress_place_id", $pos_data["posaddress_place_id"]);
		$form->add_label("shop_address_place", "City", 0 , $project["order_shop_address_place"]);
		$form->add_label("province", "Province", 0, $pos_data["province_canton"]);
		
		$form->add_list("shop_address_country", "Country*", $sql_countries, NOTNULL, $project["order_shop_address_country"]);
		$form->add_hidden("shop_address_country", $project["order_shop_address_country"]);

		$form->add_lookup("shop_address_country_name", "Country", "countries", "country_name", 0, $project["order_shop_address_country"]);

		$form->add_label("shop_address_phone", "Phone", 0, $project["order_shop_address_phone"]);
		$form->add_label("shop_address_fax", "Fax", 0, $project["order_shop_address_fax"]);
		$form->add_label("shop_address_email", "Email", 0, $project["order_shop_address_email"]);
		$form->add_label("sapnr", "SAP Number", 0, $pos_data["posaddress_sapnumber"]);

		$form->add_section("Google Map Coordinates");
		$form->add_label("GM", "Google Map", RENDER_HTML);
		$form->add_edit("posaddress_google_lat", "Latitude", NOTNULL | DISABLED, $pos_data["posaddress_google_lat"]);
		$form->add_edit("posaddress_google_long", "Longitude", NOTNULL | DISABLED, $pos_data["posaddress_google_long"]);
		$form->add_hidden("posaddress_google_precision", 1);
		$form->add_label("gmcheck", "");
		$form->add_hidden("overwrite_posaddress_data", 0);
	}

	$form->add_section("Surfaces");
	$form->add_edit("shop_gross_sqms", "Gross Surface in sqms*", NOTNULL, $project_cost_gross_sqms, TYPE_DECIMAL, 10,2);
	$form->add_edit("shop_totalsqms", "Total Surface in sqms*", NOTNULL, $project_cost_totalsqms, TYPE_DECIMAL, 10,2);
	$form->add_edit("shop_sqms", "Sales Surface in sqms*", NOTNULL, $project_cost_sqms, TYPE_DECIMAL, 10,2);
	$form->add_edit("shop_bakoffocesqms", "Back Office Surface in sqms", 0, $project_cost_bakoffocesqms, TYPE_DECIMAL, 10,2);
	$form->add_edit("shop_numfloors", "Number of Floors*", NOTNULL, $project_cost_numfloors, TYPE_DECIMAL, 10,2);
	$form->add_edit("shop_floorsurface1", "Floor 1: Surface in sqms", 0, $project_cost_floorsurface1, TYPE_DECIMAL, 10,2);
	$form->add_edit("shop_floorsurface2", "Floor 2: Surface in sqms", 0, $project_cost_floorsurface2, TYPE_DECIMAL, 10,2);
	$form->add_edit("shop_floorsurface3", "Floor 3: Surface in sqms", 0, $project_cost_floorsurface3, TYPE_DECIMAL, 10,2);


	$form->add_section("Sales");
	if($project["project_distribution_channel"])
	{
		$form->add_list("posaddress_distribution_channel", "Distribution Channel", $sql_distribution_channels, 0, $project["project_distribution_channel"]);
	}
	else
	{
		$form->add_list("posaddress_distribution_channel", "Distribution Channel", $sql_distribution_channels, 0, $standard_dsitribution_channel);
	}


	$form->add_section("Environment");
	foreach($posareas as $key=>$name)
	{
		if(array_key_exists($key, $pos_posareas))
		{
			$form->add_checkbox("area". $key, $name, 1, 0, "");
		}
		else
		{
			$form->add_checkbox("area". $key, $name, "", 0, "");
		}

	}

	$form->add_hidden("project_floor", $project["project_floor"]);

	$form->add_section("Area Perception");
	$form->add_comment("Please rate by clicking a star: 5 stars are the best rating.");
	if(count($pos_data) > 0 and array_key_exists($pos_data["posaddress_perc_class"], $ratings))
	{
		$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, $pos_data["posaddress_perc_class"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, $pos_data["posaddress_perc_tourist"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings,  $pos_data["posaddress_perc_transport"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings,  $pos_data["posaddress_perc_people"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings,  $pos_data["posaddress_perc_parking"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings,  $pos_data["posaddress_perc_visibility1"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from accross the Street*", $ratings,  $pos_data["posaddress_perc_visibility2"], NOTNULL);
	}
	else
	{
		$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from accross the Street*", $ratings, 0, NOTNULL);
	}

	$form->add_section("Neighbourhood");
	$form->add_edit("posorder_neighbour_left", "Shop on Left Side", 0, $neighbourhoods["Shop on Left Side"]);
	$form->add_edit("posorder_neighbour_right", "Shop on Right Side", 0, $neighbourhoods["Shop on Right Side"]);
	$form->add_edit("posorder_neighbour_acrleft", "Shop Across Left Side", 0, $neighbourhoods["Shop Across Left Side"]);
	$form->add_edit("posorder_neighbour_acrright", "Shop Across Right Side", 0, $neighbourhoods["Shop Across Right Side"]);
	$form->add_multiline("posorder_neighbour_brands", "Other Brands in Area", 4, 0, $neighbourhoods["Other Brands in Area"]);

}
else
{
	$pos_data["posaddress_google_lat"] = 0;
	$pos_data["posaddress_google_long"] = 0;

	if (has_access("can_use_posindex"))
	{
		$form->add_comment("Assignment to POS Index.");
		$form->add_list("posaddress_id", "POS Index POS Name", $sql_posaddresses, 0, $project["posaddress_id"]);
	}
	else
	{
		$form->add_hidden("posaddress_id", $project["posaddress_id"]);
	}

	$form->add_edit("shop_address_company", "Project Name*", NOTNULL, $project["order_shop_address_company"], TYPE_CHAR);
	$form->add_edit("shop_address_company2", "", 0, $project["order_shop_address_company2"], TYPE_CHAR);
	$form->add_edit("shop_address_address", "Address*", NOTNULL, $project["order_shop_address_address"], TYPE_CHAR);
	$form->add_edit("shop_address_address2", "", 0, $project["order_shop_address_address2"], TYPE_CHAR);
	$form->add_edit("shop_address_zip", "ZIP*", NOTNULL, $project["order_shop_address_zip"], TYPE_CHAR, 20);
	
	$place_id = "";
	$province = "";
	$sql = "select place_id, province_canton " .
	       "from places " .
		   "left join provinces on province_id = place_province " . 
		   "where place_country = " . dbquote($project["order_shop_address_country"]) . 
		   " and place_name = " . dbquote($project["order_shop_address_place"]);
	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$place_id = $row["place_id"];
		$province = $row["province_canton"];
	}
	
	$form->add_list("posaddress_place_id", "City Selection*",
		"select place_id, place_name from places where place_country = " . dbquote($project["order_shop_address_country"]) . " order by place_name", NOTNULL | SUBMIT, $place_id);

	$form->add_edit("shop_address_place", "City*", NOTNULL, $project["order_shop_address_place"], TYPE_CHAR, 20);

	$form->add_label("province", "Province", 0, $province);

	$form->add_list("shop_address_country", "Country*", $sql_countries, NOTNULL, $project["order_shop_address_country"]);
	$form->add_edit("shop_address_phone", "Phone", 0, $project["order_shop_address_phone"], TYPE_CHAR, 20);
	$form->add_edit("shop_address_fax", "Fax", 0, $project["order_shop_address_fax"], TYPE_CHAR, 20);
	$form->add_edit("shop_address_email", "Email", 0, $project["order_shop_address_email"], TYPE_CHAR, 50);

	$form->add_section("Google Map Coordinates");
	$form->add_label("GM", "Google Map", RENDER_HTML);
	$form->add_edit("posaddress_google_lat", "Latitude", NOTNULL | DISABLED, $pos_data["posaddress_google_lat"]);
	$form->add_edit("posaddress_google_long", "Longitude", NOTNULL | DISABLED, $pos_data["posaddress_google_long"]);
	$form->add_hidden("posaddress_google_precision", 1);
	$form->add_label("gmcheck", "");
	$form->add_hidden("overwrite_posaddress_data", 1);

	$form->add_section("Surfaces");
	$form->add_edit("shop_gross_sqms", "Gross Surface in sqms*", NOTNULL, $project_cost_gross_sqms, TYPE_DECIMAL, 10,2);
	$form->add_edit("shop_totalsqms", "Total Surface in sqms*", NOTNULL, $project_cost_totalsqms, TYPE_DECIMAL, 10,2);
	$form->add_edit("shop_sqms", "Sales Surface in sqms*", NOTNULL, $project_cost_sqms, TYPE_DECIMAL, 10,2);
	$form->add_edit("shop_bakoffocesqms", "Back Office Surface in sqms", 0, $project_cost_bakoffocesqms, TYPE_DECIMAL, 10,2);
	$form->add_edit("shop_numfloors", "Number of Floors*", NOTNULL, $project_cost_numfloors, TYPE_DECIMAL, 10,2);
	$form->add_edit("shop_floorsurface1", "Floor 1: Surface in sqms", 0, $project_cost_floorsurface1, TYPE_DECIMAL, 10,2);
	$form->add_edit("shop_floorsurface2", "Floor 2: Surface in sqms", 0, $project_cost_floorsurface2, TYPE_DECIMAL, 10,2);
	$form->add_edit("shop_floorsurface3", "Floor 3: Surface in sqms", 0, $project_cost_floorsurface3, TYPE_DECIMAL, 10,2);


	$form->add_section("Sales");
	if($project["project_distribution_channel"])
	{
		$form->add_list("posaddress_distribution_channel", "Distribution Channel", $sql_distribution_channels, 0, $project["project_distribution_channel"]);
	}
	else
	{
		$form->add_list("posaddress_distribution_channel", "Distribution Channel", $sql_distribution_channels, 0, $standard_dsitribution_channel);
	}

	$form->add_section("Environment");
	foreach($posareas as $key=>$name)
	{
		if(array_key_exists($key, $pos_posareas))
		{
			$form->add_checkbox("area". $key, $name, 1, 0, "");
		}
		else
		{
			$form->add_checkbox("area". $key, $name, "", 0, "");
		}

	}

	
	$form->add_hidden("project_floor", $project["project_floor"]);

	$form->add_section("Area Perception");
	$form->add_comment("Please rate by clicking a star: 5 stars are the best rating.");
	$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from accross the Street*", $ratings, 0, NOTNULL);

	$form->add_section("Neighbourhood");
	$form->add_edit("posorder_neighbour_left", "Shop on Left Side", 0);
	$form->add_edit("posorder_neighbour_right", "Shop on Right Side", 0);
	$form->add_edit("posorder_neighbour_acrleft", "Shop Across Left Side", 0);
	$form->add_edit("posorder_neighbour_acrright", "Shop Across Right Side", 0);
	$form->add_multiline("posorder_neighbour_brands", "Other Brands in Area", 4, 0);
}




if (has_access("can_edit_franchisee_data"))
{

	if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 3)
	{
		if(count($former_franchisee_address) > 0)
		{
			$form->add_section("Former Franchisee");

			$form->add_label("former_franchisee_address_company", "Company", 0, $former_franchisee_address["company"]);
			if($former_franchisee_address["company2"]) {
				$form->add_label("former_franchisee_address_company2", "", 0, $former_franchisee_address["company2"]);
			}
			$form->add_label("former_franchisee_address_address", "Address", 0, $former_franchisee_address["address"]);
			
			if($former_franchisee_address["address2"]) {
				$form->add_label("former_franchisee_address_address2","", 0, $former_franchisee_address["address2"]);
			}
			$form->add_label("former_franchisee_address_zip", "ZIP", 0, $former_franchisee_address["zip"]);
			$form->add_label("former_franchisee_address_place", "Place", 0, $former_franchisee_address["place"]);
			$form->add_label("former_franchisee_address_country", "Country", 0, $former_franchisee_address["country_name"]);
		}
	}
	
	if($project["project_cost_type"] != 6)
	{
		$form->add_section("Franchisee Address");
		$tmp_text1="Please indicate the franchisee address.";
		$tmp_text2="\nYou can either select an existing address or enter a new address.";
		$form->add_comment($tmp_text1 . $tmp_text2);
		$form->add_list("order_franchisee_address_id", "Franchisee", $sql_franchisees, SUBMIT, $project["order_franchisee_address_id"]);
	}
	else
	{
		$form->add_section("Address of Owner Company");
		$tmp_text1="Please indicate the owner company address.";
		$tmp_text2="\nYou can either select an existing address or enter a new address.";
		$form->add_comment($tmp_text1 . $tmp_text2);
		$form->add_list("order_franchisee_address_id", "Owner Company", $sql_franchisees, SUBMIT, $project["order_franchisee_address_id"]);
	}

	
	
	
	if($project["project_cost_type"] == 1 and ($project["project_projectkind"] == 3 or  $project["project_projectkind"] == 4))
	{
		$form->add_hidden("franchisee_address_parent", $project["order_client_address"]);
		$form->add_edit("franchisee_address_company", "Company", 0, $franchisee_address["company"], TYPE_CHAR, 0, 0, 2, "franchisee_address_company");
		$form->add_edit("franchisee_address_company2", "", 0, $franchisee_address["company2"], TYPE_CHAR, 0, 0, 2, "franchisee_address_company2");
		$form->add_edit("franchisee_address_address", "Address", 0, $franchisee_address["address"], TYPE_CHAR, 0, 0, 2, "franchisee_address_address2");
		$form->add_edit("franchisee_address_address2", "", 0, $franchisee_address["address2"], TYPE_CHAR, 0, 0, 2, "franchisee_address_address2");

		$form->add_list("franchisee_address_country", "Country", $sql_countries, SUBMIT, $franchisee_address["country"]);

		if(param("franchisee_address_country"))
		{
			$form->add_list("franchisee_address_place_id", "City Selection*",
				"select place_id, place_name from places where place_country = " . dbquote(param("franchisee_address_country")) . " order by place_name", NOTNULL | SUBMIT, $franchisee_address["place_id"]);
		}
		else
		{
			$form->add_list("franchisee_address_place_id", "City Selection*",
				"select place_id, place_name from places where place_country = " . dbquote($franchisee_address["country"]) . " order by place_name", NOTNULL | SUBMIT, $franchisee_address["place_id"]);
		}

		$form->add_edit("franchisee_address_place", "City", DISABLED, $franchisee_address["place"], TYPE_CHAR, 20);

		$form->add_edit("franchisee_address_zip", "ZIP", 0, $franchisee_address["zip"], TYPE_CHAR, 20);
		
		
		
		$form->add_edit("franchisee_address_phone", "Phone", 0, $franchisee_address["phone"], TYPE_CHAR, 20);
		$form->add_edit("franchisee_address_fax", "Fax", 0, $franchisee_address["fax"], TYPE_CHAR, 20);
		$form->add_edit("franchisee_address_email", "Email", 0, $franchisee_address["email"], TYPE_CHAR);
		$form->add_edit("franchisee_address_contact", "Contact", 0, $franchisee_address["contact_name"], TYPE_CHAR);
		$form->add_edit("franchisee_address_website", "Website", 0, $franchisee_address["website"], TYPE_CHAR);
	}
	else
	{

		$form->add_hidden("franchisee_address_parent", $project["order_client_address"]);
		$form->add_edit("franchisee_address_company", "Company", 0, $franchisee_address["company"], TYPE_CHAR, 0, 0, 2, "franchisee_address_company");
		$form->add_edit("franchisee_address_company2", "", 0, $franchisee_address["company2"], TYPE_CHAR, 0, 0, 2, "franchisee_address_company2");
		$form->add_edit("franchisee_address_address", "Address", 0, $franchisee_address["address"], TYPE_CHAR, 0, 0, 2, "franchisee_address_address");
		$form->add_edit("franchisee_address_address2", "", 0, $franchisee_address["address2"], TYPE_CHAR, 0, 0, 2, "franchisee_address_address2");

		$form->add_list("franchisee_address_country", "Country", $sql_countries, SUBMIT, $franchisee_address["country"]);

		if(param("franchisee_address_country"))
		{
			$form->add_list("franchisee_address_place_id", "City Selection*",
				"select place_id, place_name from places where place_country = " . dbquote(param("franchisee_address_country")) . " order by place_name", NOTNULL | SUBMIT, $franchisee_address["place_id"]);
		}
		else
		{
			$form->add_list("franchisee_address_place_id", "City Selection*",
				"select place_id, place_name from places where place_country = " . dbquote($franchisee_address["country"]) . " order by place_name", NOTNULL | SUBMIT, $franchisee_address["place_id"]);
		}

		$form->add_edit("franchisee_address_place", "City", DISABLED, $franchisee_address["place"], TYPE_CHAR, 20);

		$form->add_edit("franchisee_address_zip", "ZIP", 0, $franchisee_address["zip"], TYPE_CHAR, 20);
		
		
		
		$form->add_edit("franchisee_address_phone", "Phone", 0, $franchisee_address["phone"], TYPE_CHAR, 20);
		$form->add_edit("franchisee_address_fax", "Fax", 0, $franchisee_address["fax"], TYPE_CHAR, 20);
		$form->add_edit("franchisee_address_email", "Email", 0, $franchisee_address["email"], TYPE_CHAR);
		$form->add_edit("franchisee_address_contact", "Contact", 0, $franchisee_address["contact_name"], TYPE_CHAR);
		$form->add_edit("franchisee_address_website", "Website", 0, $franchisee_address["website"], TYPE_CHAR);

		/*
		
		$form->add_hidden("franchisee_address_parent", $project["order_client_address"]);
		$form->add_edit("franchisee_address_company", "Company", 0, $project["order_franchisee_address_company"], TYPE_CHAR);
		$form->add_edit("franchisee_address_company2", "", 0, $project["order_franchisee_address_company2"], TYPE_CHAR);
		$form->add_edit("franchisee_address_address", "Address", 0, $project["order_franchisee_address_address"], TYPE_CHAR);
		$form->add_edit("franchisee_address_address2", "", 0, $project["order_franchisee_address_address2"], TYPE_CHAR);
		
		$form->add_list("franchisee_address_country", "Country", $sql_countries, SUBMIT, $project["order_franchisee_address_country"]);

		if(param("franchisee_address_country"))
		{
			$form->add_list("franchisee_address_place_id", "City Selection*",
				"select place_id, place_name from places where place_country = " . dbquote(param("franchisee_address_country")) . " order by place_name", NOTNULL | SUBMIT, $franchisee_address["place_id"]);
		}
		elseif($project["order_franchisee_address_country"])
		{
			$form->add_list("franchisee_address_place_id", "City Selection*",
				"select place_id, place_name from places where place_country = " . dbquote($project["order_franchisee_address_country"]) . " order by place_name", NOTNULL | SUBMIT, $franchisee_address["place_id"]);
		}
		else
		{
			$form->add_list("franchisee_address_place_id", "City Selection*",
				"select place_id, place_name from places where place_country = " . dbquote($project["order_shop_address_country"]) . " order by place_name", NOTNULL | SUBMIT, $franchisee_address["place_id"]);
		}

		$form->add_edit("franchisee_address_place", "City", DISABLED, $project["order_franchisee_address_place"], TYPE_CHAR, 20);

		$form->add_edit("franchisee_address_zip", "ZIP", 0, $project["order_franchisee_address_zip"], TYPE_CHAR, 20);
		
		
		
		$form->add_edit("franchisee_address_phone", "Phone", 0, $project["order_franchisee_address_phone"], TYPE_CHAR, 20);
		$form->add_edit("franchisee_address_fax", "Fax", 0, $project["order_franchisee_address_fax"], TYPE_CHAR, 20);
		$form->add_edit("franchisee_address_email", "Email", 0, $project["order_franchisee_address_email"], TYPE_CHAR);
		$form->add_edit("franchisee_address_contact", "Contact", 0, $project["order_franchisee_address_contact"], TYPE_CHAR);
		$form->add_edit("franchisee_address_website", "Website", 0, $project["order_franchisee_address_website"], TYPE_CHAR);
		*/
	}
	
	$form->add_comment("Click the checkbox below in case you want to create a new franchisee address in 3rdparty adresses. A new address will only be created if the drop down list 'Franchisee' is empty. Only do this if you are sure that the franchisee is not contained in the list of existing franchisees.");
	$form->add_checkbox("create_new_franchisee_address", "Create New Address", "", 0, "Franchisee Address");
}
else
{	
	if($project["project_cost_type"] == 1 and ($project["project_projectkind"] == 3 or  $project["project_projectkind"] == 4))
	{
		$form->add_hidden("order_franchisee_address_id", $project["order_client_address"]);
		$form->add_hidden("franchisee_address_parent", $project["order_client_address"]);
		$form->add_hidden("franchisee_address_company", $franchisee_address["company"]);
		$form->add_hidden("franchisee_address_company2", $franchisee_address["company2"]);
		$form->add_hidden("franchisee_address_address", $franchisee_address["address"]);
		$form->add_hidden("franchisee_address_address2",$franchisee_address["address2"]);
		$form->add_hidden("franchisee_address_zip", $franchisee_address["zip"]);
		$form->add_hidden("franchisee_address_place", $franchisee_address["place"]);
		$form->add_hidden("franchisee_address_place_id", $franchisee_address["place_id"]);
		$form->add_hidden("franchisee_address_country", $franchisee_address["country"]);
		$form->add_hidden("franchisee_address_phone", $franchisee_address["phone"]);
		$form->add_hidden("franchisee_address_fax", $franchisee_address["fax"]);
		$form->add_hidden("franchisee_address_email", $franchisee_address["email"]);
		$form->add_hidden("franchisee_address_contact", $franchisee_address["contact_name"]);
		$form->add_hidden("franchisee_address_website", $franchisee_address["website"]);
	}
	else
	{
		/*
		$form->add_hidden("order_franchisee_address_id", $project["order_franchisee_address_id"]);
		$form->add_hidden("franchisee_address_parent", $project["order_client_address"]);
		$form->add_hidden("franchisee_address_company", $project["order_franchisee_address_company"]);
		$form->add_hidden("franchisee_address_company2", $project["order_franchisee_address_company2"]);
		$form->add_hidden("franchisee_address_address", $project["order_franchisee_address_address"]);
		$form->add_hidden("franchisee_address_address2",$project["order_franchisee_address_address2"]);
		$form->add_hidden("franchisee_address_zip", $project["order_franchisee_address_zip"]);
		$form->add_hidden("franchisee_address_place", $project["order_franchisee_address_place"]);
		$form->add_hidden("franchisee_address_place_id", $franchisee_address["place_id"]);
		$form->add_hidden("franchisee_address_country", $project["order_franchisee_address_country"]);
		$form->add_hidden("franchisee_address_phone", $project["order_franchisee_address_phone"]);
		$form->add_hidden("franchisee_address_fax", $project["order_franchisee_address_fax"]);
		$form->add_hidden("franchisee_address_email", $project["order_franchisee_address_email"]);
		$form->add_hidden("franchisee_address_contact", $project["order_franchisee_address_contact"]);
		$form->add_hidden("franchisee_address_website", $project["order_franchisee_address_website"]);
		*/

		$form->add_hidden("order_franchisee_address_id", $project["order_franchisee_address_id"]);
		$form->add_hidden("franchisee_address_parent", $project["order_client_address"]);
		$form->add_hidden("franchisee_address_company", $franchisee_address["company"]);
		$form->add_hidden("franchisee_address_company2", $franchisee_address["company2"]);
		$form->add_hidden("franchisee_address_address", $franchisee_address["address"]);
		$form->add_hidden("franchisee_address_address2",$franchisee_address["address2"]);
		$form->add_hidden("franchisee_address_zip", $franchisee_address["zip"]);
		$form->add_hidden("franchisee_address_place", $franchisee_address["place"]);
		$form->add_hidden("franchisee_address_place_id", $franchisee_address["place_id"]);
		$form->add_hidden("franchisee_address_country", $franchisee_address["country"]);
		$form->add_hidden("franchisee_address_phone", $franchisee_address["phone"]);
		$form->add_hidden("franchisee_address_fax", $franchisee_address["fax"]);
		$form->add_hidden("franchisee_address_email", $franchisee_address["email"]);
		$form->add_hidden("franchisee_address_contact", $franchisee_address["contact_name"]);
		$form->add_hidden("franchisee_address_website", $franchisee_address["website"]);
	}

	$form->add_hidden("create_new_franchisee_address", 0);
}


if($project["project_actual_opening_date"] == NULL or $project["project_actual_opening_date"] == '0000-00-00')
{
	if (has_access("can_edit_franchisee_agreement_data"))
	{

		if(count($pos_data) > 0 and array_key_exists("posaddress_fagagreement_type", $pos_data) and  $pos_data["posaddress_fagagreement_type"] > 0 and $project["project_projectkind"] ==  2) // renovation projects
		{

			/*
			$type = $pos_data["posaddress_fagagreement_type"];
			$sent = $pos_data["posaddress_fagrsent"];
			$signed = $pos_data["posaddress_fagrsigned"];
			$start = $pos_data["posaddress_fagrstart"];
			$end = $pos_data["posaddress_fagrend"];
			$comment = $pos_data["posaddress_fag_comment"];
			

			
			$type = "";
			$sent = "";
			$signed = "";
			$start = "";
			$end = "";
			$comment = "";;
			*/

			$type = $project["project_fagagreement_type"];
			$sent = $project["project_fagrsent"];
			$signed = $project["project_fagrsigned"];
			$start = $project["project_fagrstart"];
			$end = $project["project_fagrend"];
			$comment = $project["project_fag_comment"];
		}
		else
		{
			$type = $project["project_fagagreement_type"];
			$sent = $project["project_fagrsent"];
			$signed = $project["project_fagrsigned"];
			$start = $project["project_fagrstart"];
			$end = $project["project_fagrend"];
			$comment = $project["project_fag_comment"];

		}
		
		$form->add_section("Franchisee Agreement");
		$form->add_list("project_fagagreement_type", "Agreement Type*", "select agreement_type_id, agreement_type_name from agreement_types", 0, $type);
		$form->add_checkbox("project_fagrsent", "Sent", $sent, 0, "Franchisee Agreement");
		$form->add_checkbox("project_fagrsigned", "Signed", $signed, 0, "Franchisee Agreement");
		$form->add_label("dummy", "");
		$form->add_edit("project_fagrstart", "Agreement Starting Date", 0, to_system_date($start), TYPE_DATE);
		$form->add_edit("project_fagrend", "Agreement Ending Date", 0, to_system_date($end), TYPE_DATE);
		$form->add_multiline("project_fag_comment", "Comment", 4, 0, $comment);
	}
	else
	{	
		if(count($pos_data) > 0 and array_key_exists("posaddress_fagagreement_type", $pos_data) and  $pos_data["posaddress_fagagreement_type"] > 0)
		{
			$type = $pos_data["posaddress_fagagreement_type"];
			$sent = $pos_data["posaddress_fagrsent"];
			$signed = $pos_data["posaddress_fagrsigned"];
			$start = $pos_data["posaddress_fagrstart"];
			$end = $pos_data["posaddress_fagrend"];
			$comment = $pos_data["posaddress_fag_comment"];
		}
		else
		{
			$type = $project["project_fagagreement_type"];
			$sent = $project["project_fagrsent"];
			$signed = $project["project_fagrsigned"];
			$start = $project["project_fagrstart"];
			$end = $project["project_fagrend"];
			$comment = $project["project_fag_comment"];
		}
		
		$form->add_hidden("project_fagagreement_type", $type);
		$form->add_hidden("project_fagrsent", $sent);
		$form->add_hidden("project_fagrsigned", $signed);
		$form->add_hidden("project_fagrstart", to_system_date($start));
		$form->add_hidden("project_fagrend", to_system_date($end));
		$form->add_hidden("project_fag_comment",$comment);


	}
}
elseif (has_access("can_edit_franchisee_agreement_data"))
{
	if(count($pos_data) >0  and array_key_exists("posaddress_fagagreement_type", $pos_data) and  $pos_data["posaddress_fagagreement_type"] > 0)
	{
		$type = $pos_data["posaddress_fagagreement_type"];
		$sent = $pos_data["posaddress_fagrsent"];
		$signed = $pos_data["posaddress_fagrsigned"];
		$start = $pos_data["posaddress_fagrstart"];
		$end = $pos_data["posaddress_fagrend"];
		$comment = $pos_data["posaddress_fag_comment"];
	}
	else
	{
		$type = $project["project_fagagreement_type"];
		$sent = $project["project_fagrsent"];
		$signed = $project["project_fagrsigned"];
		$start = $project["project_fagrstart"];
		$end = $project["project_fagrend"];
		$comment = $project["project_fag_comment"];
	}
	
	$form->add_section("Franchisee Agreement");
	$form->add_list("project_fagagreement_type", "Agreement Type*", "select agreement_type_id, agreement_type_name from agreement_types", 0, $type);
	$form->add_checkbox("project_fagrsent", "Sent", $sent, 0, "Franchisee Agreement");
	$form->add_checkbox("project_fagrsigned", "Signed", $signed, 0, "Franchisee Agreement");
	$form->add_label("dummy", "");
	$form->add_edit("project_fagrstart", "Agreement Starting Date", 0, to_system_date($start), TYPE_DATE);
	$form->add_edit("project_fagrend", "Agreement Ending Date", 0, to_system_date($end), TYPE_DATE);
	$form->add_multiline("project_fag_comment", "Comment", 4, 0, $comment);
}


$form->add_hidden("project_state", $project["project_state"]);


if(in_array(4, $user_roles) and has_access("can_edit_pos_calendar_data"))
{
	if(in_array(2, $user_roles) or in_array(3, $user_roles) or in_array(10, $user_roles))
	{
		if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
		{
			$form->add_section("Project Dates");
		}
		else
		{
			$form->add_section("POS Opening Dates");
		}
		
		
		if(count($tracking_info) > 0) {
			$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20, 0, 1, "changehistory");
			
		}
		else
		{
			$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20);
		}

		if($project["project_real_opening_date"] != NULL and $project["project_real_opening_date"] != '0000-00-00')
		{
			$form->add_edit("change_comment", "Reason for changing agreed opening date*", 0);
		}
		else
		{
			$form->add_hidden("change_comment");
		}
		

		$form->add_edit("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]), TYPE_DATE, 20);

		$form->add_section("POS Closing Dates");
		$form->add_edit("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]), TYPE_DATE, 20);
	}
	elseif($project["order_actual_order_state_code"] >= '800')
	{
		
		
		if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
		{
			$form->add_section("Project Dates");
		}
		else
		{
			$form->add_section("POS Opening Dates");

		}
		
		if(count($tracking_info) > 0) {
			$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20, 0, 1, "changehistory");
			
		}
		else
		{
			$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20);
		}

		if($project["project_real_opening_date"] != NULL and $project["project_real_opening_date"] != '0000-00-00')
		{
			$form->add_edit("change_comment", "Reason for changing agreed opening date*", 0);
		}
		else
		{
			$form->add_hidden("change_comment");
		}

		$form->add_edit("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]), TYPE_DATE, 20);
		$form->add_section("POS Closing Dates");
		$form->add_edit("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]), TYPE_DATE, 20);
	}
	else
	{
		

		if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
		{
			$form->add_section("Project Dates");
		}
		else
		{
			$form->add_section("POS Opening Dates");

		}
		$form->add_label("shop_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]));
		$form->add_label("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]));
		$form->add_label("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]));
	}
}
elseif (has_access("can_edit_pos_calendar_data"))
{
	
	
	if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
	{
		$form->add_section("Project Dates");
	}
	else
	{
		$form->add_section("POS Opening Dates");
	}

	
	if(count($tracking_info) > 0) {
	$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20, 0, 1, "changehistory");
		
	}
	else
	{
		$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20);
	}

	if($project["project_real_opening_date"] != NULL and $project["project_real_opening_date"] != '0000-00-00')
	{
		$form->add_edit("change_comment", "Reason for changing agreed opening date*", 0);
	}
	else
	{
		$form->add_hidden("change_comment");
	}

	$form->add_edit("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]), TYPE_DATE, 20);

	$form->add_section("POS Closing Dates");
	$form->add_edit("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]), TYPE_DATE, 20);
}
else
{
	
	
	if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
	{
		$form->add_section("Project Dates");
	}
	else
	{
		$form->add_section("POS Opening Dates");
	}

	$form->add_label("shop_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]));
	$form->add_label("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]));

	$form->add_label("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]));
}

$form->add_hidden("old_shop_actual_opening_date", to_system_date($project["project_actual_opening_date"]));
$form->add_hidden("old_shop_real_opening_date", to_system_date($project["project_real_opening_date"]));

$form->add_button("save", "Save Data");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("save"))
{
    if($form->value("shop_actual_opening_date"))
	{
		if($project["project_projectkind"] != 5)
		{
			//$form->add_validation("from_system_date({shop_actual_opening_date}) <=  " . dbquote(date("Y-m-d")), "The actual POS Opening Date must not be a future date!");
			

			$form->add_validation("from_system_date({shop_actual_opening_date}) <=  " . dbquote(date("Y-m-d", strtotime("+3 days"))), "The actual POS opening date must be a date in the past or can only be at maximum three days in the future!");
		}

		$form->add_validation("{franchisee_address_company} != ''", "Please indicate the franchisee address (Company, Address, Zip, City, Country and Contact Name).");
		$form->add_validation("{franchisee_address_address} != ''", "Please indicate the franchisee address (Company, Address, Zip, City, Country and Contact Name).");
		$form->add_validation("{franchisee_address_zip} != ''", "Please indicate the franchisee address (Company, Address, Zip, City, Country and Contact Name).");
		$form->add_validation("{franchisee_address_place} != ''", "Please indicate the franchisee address (Company, Address, Zip, City, Country and Contact Name).");
		$form->add_validation("{franchisee_address_country} != ''", "Please indicate the franchisee address (Company, Address, Zip, City, Country and Contact Name).");
		$form->add_validation("{franchisee_address_contact} != ''", "Please indicate the franchisee address (Company, Address, Zip, City, Country and Contact Name).");
	}

	if($form->value("shop_closing_date"))
	{
		$form->add_validation("from_system_date({shop_closing_date}) <=  " . dbquote(date("Y-m-d", strtotime("+3 days"))), "The POS closing date must be a date in the past or can only be at maximum three days in the future!");

	}

	if($form->value("old_shop_real_opening_date") and $form->value("old_shop_real_opening_date") != $form->value("shop_real_opening_date"))
	{
		$form->add_validation("{change_comment} != ''", "Please indicate the reason for changing the agreed opening date!");
	}

	$form->add_validation("{shop_gross_sqms} >= {shop_totalsqms}", "The gross surface must not be less than the total surface!");

	$form->add_validation("{shop_totalsqms} >= {shop_sqms}", "The total surface must not be less than the sales surface!");


	if(!$form->value("create_new_franchisee_address") and $form->value("order_franchisee_address_id") == 0) {
	
		$form->error("Please select a franchisee address from the drop down or indicate that a new address schuld be created!");
	}
	elseif ($form->validate())
    {
	
       //update aggreement data
	   /*
	   if($project['project_projectkind'] == 1 or $project['project_projectkind'] == 3)
	   {
		   if($form->value("old_shop_real_opening_date") != $form->value("shop_real_opening_date"))
		   {
				$realistic_opening_date = from_system_date($form->value("shop_real_opening_date"));

				//get the posaddress
				$sql = "select posaddress_id, posaddress_fagrstart " . 
					   "from posorderspipeline " . 
					   "left join posaddressespipeline on posaddress_id = posorder_posaddress " .
					   " where posorder_order = " . dbquote($project['project_order']);
				
				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					$pos_id = $row["posaddress_id"];

					
					if($realistic_opening_date)
					{
						$start = to_system_date($realistic_opening_date);
						$start = "01." . substr($start, 3, strlen($start)-1);


						$months = 13 - substr($realistic_opening_date, 5,2);
						$duration = "3 years and " . $months . " months";


						$end = to_system_date($realistic_opening_date);
						if(substr($end, 6, strlen($end)-1) < 10) {
							$end = "31.12.0" . (substr($end, 6, strlen($end)-1) + 3);
						}
						else
						{
							$end = "31.12." . (substr($end, 6, strlen($end)-1) + 3);
						}
					}
					else
					{
						$start = "";
						$end = "";

					}
					$form->value('project_fagrstart', $start);
					$form->value('project_fagrend', $end);
				}
		   }
	   }
	   else
	   {
		   $form->value('project_fagrstart', "");
		   $form->value('project_fagrend', "");
	   }
	   */

		project_update_pos_data($form, 0);
		mysql_select_db(RETAILNET_DB, $db);

		//update posareas
		$sql = "delete from $table2 where posarea_posaddress =" . dbquote($pos_data["posaddress_id"]);
		$result = $res = mysql_query($sql) or dberror($sql);
		foreach($posareas as $key=>$name)
		{
			if($form->value("area" . $key) == 1)
			{
				$fields = array();
				$values = array();

				$fields[] = "posarea_posaddress";
				$values[] = dbquote($pos_data["posaddress_id"]);

				$fields[] = "posarea_area";
				$values[] = dbquote($key);

				$fields[] = "date_created";
				$values[] = dbquote(date("Y-m-d"));

				$fields[] = "date_modified";
				$values[] = dbquote(date("Y-m-d"));

				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				$fields[] = "user_modified";
				$values[] = dbquote(user_login());

				$sql = "insert into $table2 (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);
			}
		}


		

		if(!$form->value("shop_actual_opening_date"))
		{
			move_pos_data_to_pipeline($form);
		}

				
        $form->message("Your changes have been saved.");

		$sql = "select project_number, project_postype, project_product_line, " .
			   "order_shop_address_company, order_shop_address_place, country_name " .
			   "from projects " . 
			   "left join orders on order_id = project_order " .
			   "left join countries on country_id = order_shop_address_country " .
			   "where project_id = " . param("pid");

		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);
		$country_name = $row["country_name"];

		//send mail to recipient of mail_alert_type 1 if aggreed POS Opening Date was changed

		if($form->value("old_shop_real_opening_date") != $form->value("shop_real_opening_date"))
		{

			//project tracking
			$field = "project_real_opening_date";
			$sql = "Insert into projecttracking (" . 
				   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
				   user_id() . ", " . 
				   $project["project_id"] . ", " . 
				   dbquote($field) . ", " . 
				   dbquote(to_system_date($form->value("old_shop_real_opening_date"))) . ", " . 
				   dbquote(to_system_date($form->value("shop_real_opening_date"))) . ", " . 
				   dbquote($form->value("change_comment")) . ", " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
				   
			$result = mysql_query($sql) or dberror($sql);

			
			/*
			$sql = "select * from mail_alerts " .
				   "where (mail_alert_type = 1 or mail_alert_type = 2) " .
				   "and mail_alert_order = " . $project["project_order"];
			

			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
		    */
				$sql = "select * from mail_alert_types " . 
						   "where mail_alert_type_id = 1";

				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				
				$recipient = $row["mail_alert_type_sender_email"];

				$sql = "select user_id ".
					   "from users ".
					   "where user_email = '" . $recipient . "' ";
				
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				$reciepient_user_id = $row["user_id"];


				//project manager
				$reciepient_user_id_rtc = $project["project_retail_coordinator"];
				$sql = "select user_email, concat(user_name, ' ', user_firstname) as username ".
					   "from users ".
					   "where user_id = '" . $reciepient_user_id_rtc . "' ";
				
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				$recipient_rtc = $row["user_email"];
				$recipient_rtc_name = $row["username"];
				


				//retail operator
				$reciepient_user_id_rto = $project["order_retail_operator"];
				$sql = "select user_email, concat(user_name, ' ', user_firstname) as username ".
					   "from users ".
					   "where user_id = '" . $reciepient_user_id_rto . "' ";
				
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				$recipient_rto = $row["user_email"];
				$recipient_rto_name = $row["username"];
				


				
				$sql = "select user_id, user_email, concat(user_name, ' ', user_firstname) as username ".
					   "from users ".
					   "where (user_id = '" . user_id() . "' " . 
					   "   and user_active = 1)";
				
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				
				$sender_id = $row["user_id"];
				$sender_email = $row["user_email"];
				$sender_name = $row["username"];

				
				if($form->value("old_shop_real_opening_date"))
				{
					$fromto = "from " . $form->value("old_shop_real_opening_date") . " to " .  $form->value("shop_real_opening_date");
				}
				else
				{
					$fromto = " to " .  $form->value("shop_real_opening_date");
				}

				if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
				{
					$subject = "Project's expected ending was changed - Project " . $project["order_number"] . ", " . $country_name . ", " . $project["order_shop_address_company"];
					$bodytext0 = $sender_name . " has changed the project's expected ending date " . $fromto;
				}
				else
				{
					$subject = "Agreed POS opening date was changed - Project " . $project["order_number"] . ", " . $country_name . ", " . $project["order_shop_address_company"];
					$bodytext0 = $sender_name . " has changed the agreed POS Opening Date of the project " . $fromto;
				}
				

				$mail = new Mail();
				$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
				$mail->set_sender($sender_email, $sender_name);
				$mail->add_recipient($recipient);

				if($recipient_rtc_name) 
				{
					$mail->add_cc($recipient_rtc, $recipient_rtc_name);
				}
				if($recipient_rto_name) 
				{
					$mail->add_cc($recipient_rto, $recipient_rto_name);
				}


				$link ="project_edit_pos_data.php?pid=" . param("pid");
				$bodytext = $bodytext0 . "\nClick below to have direct access to the project:\n";
				$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";

				         
				$mail->add_text($bodytext);
				
				$result = $mail->send();

				$sql = "delete from mail_alerts where mail_alert_type = 1 " .
					   "and mail_alert_order = " . $project["project_order"];
				mysql_query($sql) or dberror($sql);

				append_mail($project["project_order"], $reciepient_user_id, user_id(), $bodytext0, "910", 1);

				if($recipient_rtc_name) 
				{
					append_mail($project["project_order"], $reciepient_user_id_rtc, user_id(), $bodytext0, "910", 1);
				}

				if($recipient_rto_name) 
				{
					append_mail($project["project_order"], $reciepient_user_id_rto, user_id(), $bodytext0, "910", 1);
				}

				$form->value("old_shop_real_opening_date", $form->value("shop_real_opening_date"));

	
			//}
		}
		
		
		//send mail to recipient of mail_alert_type 2 if actual POS Opening Date was changed
		
		if($form->value("old_shop_actual_opening_date") != $form->value("shop_actual_opening_date"))
		{
			
			//project tracking
			$field = "project_actual_opening_date";
			$sql = "Insert into projecttracking (" . 
				   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_time) VALUES (" . 
				   user_id() . ", " . 
				   $project["project_id"] . ", " . 
				   dbquote($field) . ", " . 
				   dbquote(to_system_date($form->value("old_shop_actual_opening_date"))) . ", " . 
				   dbquote(to_system_date($form->value("shop_actual_opening_date"))) . ", " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
				   
			$result = mysql_query($sql) or dberror($sql);
			
			
			/*
			$sql = "select * from mail_alerts " .
				   "where (mail_alert_type = 1 or mail_alert_type = 2) " .
				   "and mail_alert_order = " . $project["project_order"];
			
			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
			*/
				/*
				if($row["mail_alert_type"] == 1)
				{
					$sql = "select * from mail_alert_types " .
						   "where mail_alert_type_id = 1";
				}
				else
				{
					$sql = "select * from mail_alert_types " .
						   "where mail_alert_type_id = 2";
				}
				*/


				$sql = "select * from mail_alert_types " .
						   "where mail_alert_type_id = 2";

				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				
				$recipient = $row["mail_alert_type_sender_email"];
				
				$sql = "select user_id ".
					   "from users ".
					   "where user_email = '" . $recipient . "' ";
				
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				$reciepient_user_id = $row["user_id"];


				$sql = "select user_id, user_email, concat(user_name, ' ', user_firstname) as username ".
					   "from users ".
					   "where (user_id = '" . user_id() . "' " . 
					   "   and user_active = 1)";
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				$sender_id = $row["user_id"];
				$sender_email = $row["user_email"];
				$sender_name = $row["username"];

				if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
				{
					$subject = "Project's actual ending date was entered - Project " . $project["order_number"] . ", " . $country_name . ", " . $project["order_shop_address_company"];
					$bodytext0 = $sender_name . " has entered the project's actual ending date.";
				}
				else
				{
					
					$subject = "Actual POS opening date was entered - Project " . $project["order_number"] . ", " . $country_name . ", " . $project["order_shop_address_company"];
					$bodytext0 = $sender_name . " has entered the actual POS Opening Date of the project.";
				}

				$mail = new Mail();
				$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
				$mail->set_sender($sender_email, $sender_name);
				$mail->add_recipient($recipient);


				
				$link ="project_edit_pos_data.php?pid=" . param("pid");
				$bodytext = $bodytext0 . "\nClick below to have direct access to the project:\n";
				$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           

				$mail->add_text($bodytext);
				
				
				$result = $mail->send();

				$sql = "delete from mail_alerts where mail_alert_type IN (1, 13, 15, 2, 17, 18) " .
					   "and mail_alert_order = " . $project["project_order"];
				mysql_query($sql) or dberror($sql);

				append_mail($project["project_order"], $reciepient_user_id, user_id(), $bodytext0, "910", 1);

				$form->value("old_shop_actual_opening_date", $form->value("shop_actual_opening_date"));
				
			//}
		}


		//send mail in case pos opening was entered but distribution channel is missing
		if($form->value("old_shop_actual_opening_date") and !$project["project_distribution_channel"] and !$form->value("posaddress_distribution_channel"))
		{
			$sql = "select * from mail_alert_types " .
					   "where mail_alert_type_id = 22";

			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);
			
			$recipient = $row["mail_alert_type_sender_email"];

			$mail_alert_text  = $row["mail_alert_mail_text"];
			
			$sql = "select user_id ".
				   "from users ".
				   "where user_email = '" . $recipient . "' ";
			
			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);
			$reciepient_user_id = $row["user_id"];


			$sql = "select user_id, user_email, concat(user_name, ' ', user_firstname) as username ".
				   "from users ".
				   "where (user_id = '" . user_id() . "' " . 
				   "   and user_active = 1)";
			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);
			$sender_id = $row["user_id"];
			$sender_email = $row["user_email"];
			$sender_name = $row["username"];


			
			$subject = "Distribution channel is missing - Project " . $project["order_number"] . ", " . $country_name . ", " . $project["order_shop_address_company"];
			$bodytext0 = $mail_alert_text;


			$mail = new Mail();
			$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
			$mail->set_sender($sender_email, $sender_name);
			$mail->add_recipient($recipient);

			
			$link ="project_edit_pos_data.php?pid=" . param("pid");
			$bodytext = $bodytext0 . "\nClick below to have direct access to the project:\n";
			$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
		

			$mail->add_text($bodytext);
			
			
			$result = $mail->send();

			append_mail($project["project_order"], $reciepient_user_id, user_id(), $bodytext, "910", 1);
			
		}

		//put project to archive in case of opening date and approval
		if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
		{
			if($form->value("shop_actual_opening_date"))
			{
				append_order_state($project["project_order"], '820', 1, 1);
				set_archive_date($project["project_order"]);

				// send email notification for project types
				$sql = "select project_number, project_postype, project_product_line,  " .
					   "order_shop_address_company, order_shop_address_place, country_name, " .
					   "user_firstname, user_name, user_email from projects " . 
					   "left join orders on order_id = project_order " .
					   "left join countries on country_id = order_shop_address_country " .
					   "left join users on user_id =  " . user_id() .
					   "   where project_id = " . $project["project_id"];

				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{        
					
					// send mail to cost manager
					$project_postype = $row["project_postype"];
					$project_product_line = $row["project_product_line"];
					
					$subject = MAIL_SUBJECT_PREFIX . ": Project was put to the records - Project " . $row["project_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];

					$sender_email = $row["user_email"];
					$sender_name =  $row["user_name"] . " " . $row["user_firstname"];

					$bodytext0 = "The project was put to the records by by " . $sender_name;

					$mailaddress = 0;

					$mail = new Mail();
					$mail->set_subject($subject);
					$mail->set_sender($sender_email, $sender_name);

					$sql = "select * from postype_notifications " .
						   "where postype_notification_postype = " . $project_postype . 
						   " and postype_notification_prodcut_line = " . $project_product_line;

					$res = mysql_query($sql) or dberror($sql);
					while ($row = mysql_fetch_assoc($res))
					{
						if($row["postype_notification_email3"])
						{
							$sql_u = "select user_id from users " .
									 "where user_email = '" . $row["postype_notification_email3"] .  "'";
							
							$res_u = mysql_query($sql_u) or dberror($sql_u);
							
							if($row_u = mysql_fetch_assoc($res_u))
							{
								$recepient_id = $row_u["user_id"];
							}
							else
							{
								$recepient_id = '0';
							}

							$mail->add_recipient($row["postype_notification_email3"]);
							$mailaddress = 1;
						}
					}
					
					$link ="project_view_client_data.php?pid=" . $project["project_id"];
					$bodytext = $bodytext0 . "\nclick below to have direct access to the project:\n";
					$bodytext = $bodytext .  APPLICATION_URL . "/archive/" . $link . "\n\n";           
					$mail->add_text($bodytext);
					
					if($mailaddress == 1)
					{
						$mail->send();
						append_mail($project["project_order"], $recepient_id, user_id(), $bodytext, MOVED_TO_THE_RECORDS, 1);
						
					}
				}
			}
		}
		else
		{
			if($form->value("shop_actual_opening_date") and $project["project_cost_cms_approved"] == 1)
			{
				append_order_state($project["project_order"], '820', 1, 1);
				set_archive_date($project["project_order"]);

				// send email notification for project types
				$sql = "select project_number, project_postype, project_product_line,  " .
					   "order_shop_address_company, order_shop_address_place, country_name, " .
					   "user_firstname, user_name, user_email from projects " . 
					   "left join orders on order_id = project_order " .
					   "left join countries on country_id = order_shop_address_country " .
					   "left join users on user_id =  " . user_id() .
					   "   where project_id = " . $project["project_id"];

				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{        
					
					// send mail to cost manager
					$project_postype = $row["project_postype"];
					$project_product_line = $row["project_product_line"];
					
					$subject = MAIL_SUBJECT_PREFIX . ": Project was put to the records - Project " . $row["project_number"] . ", " . $row["country_name"] . ", " .  $row["order_shop_address_company"];

					$sender_email = $row["user_email"];
					$sender_name =  $row["user_name"] . " " . $row["user_firstname"];

					$bodytext0 = "The project was put to the records by by " . $sender_name;

					$mailaddress = 0;

					$mail = new Mail();
					$mail->set_subject($subject);
					$mail->set_sender($sender_email, $sender_name);

					$sql = "select * from postype_notifications " .
						   "where postype_notification_postype = " . $project_postype . 
						   " and postype_notification_prodcut_line = " . $project_product_line;

					$res = mysql_query($sql) or dberror($sql);
					while ($row = mysql_fetch_assoc($res))
					{
						if($row["postype_notification_email3"])
						{
							$sql_u = "select user_id from users " .
									 "where user_email = '" . $row["postype_notification_email3"] .  "'";
							
							$res_u = mysql_query($sql_u) or dberror($sql_u);
							
							if($row_u = mysql_fetch_assoc($res_u))
							{
								$recepient_id = $row_u["user_id"];
							}
							else
							{
								$recepient_id = '0';
							}

							$mail->add_recipient($row["postype_notification_email3"]);
							$mailaddress = 1;
						}
					}
					
					$link ="project_view_client_data.php?pid=" . $project["project_id"];
					$bodytext = $bodytext0 . "\nclick below to have direct access to the project:\n";
					$bodytext = $bodytext .  APPLICATION_URL . "/archive/" . $link . "\n\n";           
					$mail->add_text($bodytext);
					
					if($mailaddress == 1)
					{
						$mail->send();
						append_mail($project["project_order"], $recepient_id, user_id(), $bodytext, MOVED_TO_THE_RECORDS, 1);
						
					}
				}
			}
		}

		$link = "project_edit_pos_data.php?pid=" . param("pid"); 
        redirect($link);

    }
}
elseif ($form->button("order_franchisee_address_id"))
{
    
	// set new franchisee address
	$form->value("franchisee_address_company", "");
	$form->value("franchisee_address_company2",  "");
	$form->value("franchisee_address_address",  "");
	$form->value("franchisee_address_address2",  "");
	$form->value("franchisee_address_zip",  "");
	$form->value("franchisee_address_place",  "");
	$form->value("franchisee_address_place_id", 0);
	$form->value("franchisee_address_country",  0);
	$form->value("franchisee_address_phone",  "");
	$form->value("franchisee_address_fax",  "");
	$form->value("franchisee_address_email",  "");
	$form->value("franchisee_address_contact",  "");
	$form->value("franchisee_address_website",  "");

	if ($form->value("order_franchisee_address_id"))
	{
		$sql = "select * from addresses " .
			   "left join places on place_id = address_place_id " . 
		       "where address_id = " . $form->value("order_franchisee_address_id");
		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$form->value("franchisee_address_company", $row["address_company"]);
			$form->value("franchisee_address_company2",  $row["address_company2"]);
			$form->value("franchisee_address_address",  $row["address_address"]);
			$form->value("franchisee_address_address2",  $row["address_address2"]);
			$form->value("franchisee_address_zip",  $row["address_zip"]);
			$form->value("franchisee_address_place",  $row["place_name"]);
			$form->value("franchisee_address_place_id", $row["address_place_id"]);
			$form->value("franchisee_address_country",  $row["address_country"]);
			$form->value("franchisee_address_phone",  $row["address_phone"]);
			$form->value("franchisee_address_fax",  $row["address_fax"]);
			$form->value("franchisee_address_email",  $row["address_email"]);
			$form->value("franchisee_address_contact",  $row["address_contact_name"]);
			$form->value("franchisee_address_website",  $row["address_website"]);
		}
	}
}
elseif($form->button("posaddress_place_id"))
{
	$sql = "select place_name, province_canton " .
		   "from places " . 
		   "left join provinces on province_id = place_province " . 
		   "where place_id = " . dbquote($form->value("posaddress_place_id"));
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$form->value("shop_address_place", $row["place_name"]);
		$form->value("province", $row["province_canton"]);
	}
}
elseif($form->button("franchisee_address_place_id"))
{
	$sql = "select place_name " .
		   "from places " . 
		   "where place_id = " . dbquote($form->value("franchisee_address_place_id"));
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$form->value("franchisee_address_place",  $row["place_name"]);
	}
}
elseif($form->button("franchisee_address_country"))
{
	$form->value("franchisee_address_place",  "");
	$form->value("franchisee_address_place_id", 0);
}






//gogle map link
if($form->value("shop_address_country") and $form->value("shop_address_place") and $form->value("shop_address_address"))
{
	if($form->value("posaddress_id") > 0)
	{
		$googlemaplink = "Click <a href='javascript:popup(\"/pos/project_new_map.php?id=" . $form->value("posaddress_id") . "&table=" . $table . "&c=" . $form->value("shop_address_country") . "&p=" . str_replace("'", " ", $form->value("shop_address_place")) . "&a=" . str_replace("'", " ", $form->value("shop_address_address")) . "\", 700,650)'>here</a> to verify the geografical position of the POS.";
	}
	else
	{
		$googlemaplink = "Click <a href='javascript:popup(\"/pos/project_new_map.php?table=" . $table . "&c=" . $form->value("shop_address_country") . "&p=" . str_replace("'", " ", $form->value("shop_address_place")) . "&a=" . str_replace("'", " ", $form->value("shop_address_address")) . "\", 700,650)'>here</a> to choose the geografical position of the POS.";
	}
}
elseif($form->value("shop_address_country") and $form->value("shop_address_place"))
{
	if($form->value("posaddress_id") > 0)
	{
		$googlemaplink = "Click <a href='javascript:popup(\"/pos/project_new_map.php?id=" . $form->value("posaddress_id") . "&table=" . $table . "&c=" . $form->value("shop_address_country") . "&p=" . str_replace("'", " ", $form->value("shop_address_place")) . "&a=\", 700,650)'>here</a> to verify the geografical position of the POS.";
	}
	else
	{
		$googlemaplink = "Click <a href='javascript:popup(\"/pos/project_new_map.php?table=" . $table . "&c=" . $form->value("shop_address_country") . "&p=" . str_replace("'", " ", $form->value("shop_address_place")) . "&a=\", 700,650)'>here</a> to choose the geografical position of the POS.";
	}
}
elseif($form->value("shop_address_country"))
{
	if($form->value("posaddress_id") > 0)
	{
		$googlemaplink = "Click <a href='javascript:popup(\"/pos/project_new_map.php?id=" . $form->value("posaddress_id") . "&table=" . $table . "&c=" . $form->value("shop_address_country") . "&p=&a=\", 700,650)'>here</a> to verify the geografical position of the POS.";
	}
	else
	{
		$googlemaplink = "Click <a href='javascript:popup(\"/pos/project_new_map.php?table=" . $table . "&c=" . $form->value("shop_address_country") . "&p=&a=\", 700,650)'>here</a> to choose the geografical position of the POS.";
	}
}
elseif(!isset($googlemaplink))
{
	$googlemaplink = "";
}


$form->value("GM", $googlemaplink);

    
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Edit POS-Data");
$form->render();


?>

<script language="javascript">
	$("#h_shop_address_company").click(function() {
	   $('#shop_address_company').val($('#shop_address_company').val().toLowerCase());
	   var txt = $('#shop_address_company').val();

	   $('#shop_address_company').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	$("#h_shop_address_company2").click(function() {
	   $('#shop_address_company2').val($('#shop_address_company2').val().toLowerCase());
	   var txt = $('#shop_address_company2').val();

	   $('#shop_address_company2').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	$("#h_shop_address_address").click(function() {
	   $('#shop_address_address').val($('#shop_address_address').val().toLowerCase());
	   var txt = $('#shop_address_address').val();

	   $('#shop_address_address').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});


	$("#h_shop_address_address2").click(function() {
	   $('#shop_address_address2').val($('#shop_address_address2').val().toLowerCase());
	   var txt = $('#shop_address_address2').val();

	   $('#shop_address_address2').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});



	$("#h_franchisee_address_company").click(function() {
	   $('#franchisee_address_company').val($('#franchisee_address_company').val().toLowerCase());
	   var txt = $('#franchisee_address_company').val();

	   $('#franchisee_address_company').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	$("#h_franchisee_address_company2").click(function() {
	   $('#franchisee_address_company2').val($('#franchisee_address_company2').val().toLowerCase());
	   var txt = $('#franchisee_address_company2').val();

	   $('#franchisee_address_company2').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	
	$("#h_franchisee_address_address2").click(function() {
	   $('#franchisee_address_address2').val($('#franchisee_address_address2').val().toLowerCase());
	   var txt = $('#franchisee_address_address2').val();

	   $('#franchisee_address_address2').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	
	$("#h_franchisee_address_address").click(function() {
	   $('#franchisee_address_address').val($('#franchisee_address_address').val().toLowerCase());
	   var txt = $('#franchisee_address_address').val();

	   $('#franchisee_address_address').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	

	
</script>

<div id="changehistory" style="display:none;">
    <strong>Changes of the agreed opening date</strong>
	<table class="table_tracking">
	<tr>
	<td class="label">User</td>
	<td class="label">Time</td>
	<td class="label">Old Value</td>
	<td class="label">New Value</td>
	<td class="label">Comment</td>
	</tr>

	<?php
		foreach($tracking_info as $key=>$values)
		{
			echo '<tr class="tr_tracking"><td class="td_tracking_nobr">' . $values['user_name'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_time'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_oldvalue'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_newvalue'] . '</td>';
			echo '<td class="td_tracking">' . $values['projecttracking_comment'] . '</td></tr>';
		}
	?>
	
	</table>
</div> 

<?php
echo "<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>";


if(param("project_floor") > 0 or param("area2") == 1 or $form->value("area2") == 1)
{
	
	echo '<div id="floorselector" style="position:absolute;height:200px;width:200px;">';
}
else
{
	echo '<div id="floorselector" style="position:absolute;display:none;">';
}
	
	foreach($floors as $id=>$name)
	{
		$checked = "";
		//if($project['project_floor'] == $id or param("project_floor") == $id or $form->value("area2") == $id )
		if($project['project_floor'] == $id or param("project_floor") == $id)
		{
			$checked = "checked";
		}
		echo "<input type=\"radio\" onclick=\"javascript:$('#project_floor').val($(this).val());\" value=\"" . $id . "\" name=\"floor\" " . $checked . ">" . $name . '<br />';
	}

?>
</div>


<script language="Javascript">

	$(document).ready(function(){
		  
		  
		  
		  var p = $("#area2");
		  var position = p.position();

		  var x = position.left + 170;
		  var y = position.top - 60;

		  id="floorselector"
		  $("#floorselector").css({left:x,top:y});
		  
		  $("#area2").click(function(){
			
			
			if( $('#floorselector').is(':hidden') ) 
			{
				$("#floorselector").show();
			}
			else
		    {
				$("#floorselector").hide();
				$("#project_floor").val('');
		    }
			
			
			
		  });
	});

</script>

<?php

$page->footer();



?>