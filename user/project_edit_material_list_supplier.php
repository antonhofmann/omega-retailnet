<?php
/********************************************************************

    project_edit_material_list_supplier.php

    Edit List of Materials for Supplier's Use Only

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2003-01-22
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-16
    Version:        1.1.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_edit_his_list_of_materials_in_projects");

register_param("pid");
set_referer("project_add_catalog_item_item_list_supplier.php");
set_referer("project_edit_material_list_edit_item_supplier.php");
set_referer("project_add_special_item_individual_supplier.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));

// get user data
$user_data = get_user(user_id());

// get company's address
$client_address = get_address($project["order_client_address"]);

// get Supplier currency
//$supplier_currency = get_address_currency($user_data["address"]);

// create sql for oder items
$sql_order_items = "select order_item_id, order_item_text, order_item_quantity, ".
                   "    order_item_po_number, order_item_supplier_price, item_id, ".
                   "    if(item_code <>'', item_code, item_type_name) as item_shortcut, ".
                   "    category_priority, category_name, ".
                   "    addresses_1.address_shortcut as forwarder_company, ".
                   "    item_type_id, item_type_name, ".
                   "    item_type_priority, unit_name ".
                   "from order_items ".
                   "left join items on order_item_item = item_id ".
                   "left join categories on order_item_category = category_id ".
                   "left join addresses as addresses_1 ".
                   "     on order_item_forwarder_address = addresses_1.address_id ".
                   "left join item_types on order_item_type = item_type_id " .
				   "left join units on unit_id = item_unit";


$where_clause = " where (order_item_type = " . ITEM_TYPE_STANDARD . 
                "    or order_item_type = " . ITEM_TYPE_SPECIAL . ") ".
                "    and order_item_order = " . $project["project_order"] .
				"    and order_item_supplier_address = " . $user_data["address"] ;

$values = array();
$res = mysql_query($sql_order_items . $where_clause) or dberror($sql_order_items . $where_clause);
while ($row = mysql_fetch_assoc($res))
{
    $values[$row["order_item_id"]] = $row["order_item_quantity"];
}

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project");

$form->add_section("Project");
$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid",$project["project_order"]);

require_once "include/project_head_small.php";

//if ($project["order_budget_is_locked"] == 1)
if($project["order_actual_order_state_code"] > '600')
{
    $form->error = "Budget is locked, no more changes are allowed.";
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Create List for Catalog Items
*********************************************************************/ 
$list1 = new ListView($sql_order_items, LIST_HAS_HEADER);

$list1->set_title("Catalog Items");
$list1->set_entity("order_item");
$list1->set_filter("order_item_type = " . ITEM_TYPE_STANDARD . " and order_item_order = " . $project["project_order"] . " and order_item_supplier_address = " . $user_data["address"]);
$list1->set_order("category_priority, item_code");
$list1->set_group("category_priority", "category_name");

$link="project_edit_material_list_edit_item_supplier.php?pid=" . param("pid");

//if ($project["order_budget_is_locked"] == 1)
if($project["order_actual_order_state_code"] > '600')
{
    $list1->add_column("item_shortcut", "Item Code");
}
else
{
    $list1->add_column("item_shortcut", "Item Code", $link);
}

$list1->add_column("order_item_text", "Name");

$list1->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
$list1->add_column("unit_name", "Unit", "", "", "", COLUMN_NO_WRAP);

//$list1->add_edit_column("item_entry_field", "Quantity", "4", 0, $values);


// attention: curreny of suppliers price is defined in table suppliers
$list1->add_column("order_item_supplier_price", "Price", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);


$list1->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);
$list1->add_column("forwarder_company", "Forwarder");

//if ($project["order_budget_is_locked"] == 1)
if($project["order_actual_order_state_code"] > '600')
{
    $list1->add_button("nothing", "");
}
else
{
    //$list1->add_button("save_items", "Save");
    $list1->add_button("add_items", "Add Catalog Items");
}


/********************************************************************
    Create List for Special Items
*********************************************************************/ 
$list2 = new ListView($sql_order_items, LIST_HAS_HEADER);

$list2->set_title("Special Items");
$list2->set_entity("order_item");
$list2->set_filter("order_item_type = " . ITEM_TYPE_SPECIAL . " and order_item_order = " . $project["project_order"] . " and order_item_supplier_address = " . $user_data["address"]);
$list2->set_order("order_item_text");

$link="project_edit_material_list_edit_item_supplier.php?pid=" . param("pid");

//if ($project["order_budget_is_locked"] == 1)
if($project["order_actual_order_state_code"] > '600')
{
    $list2->add_column("item_shortcut", "Item Code");
}
else
{
    $list2->add_column("item_shortcut", "Item Code", $link);
}


$list2->add_column("order_item_text", "Name");

$list2->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);

//$list2->add_edit_column("item_entry_field", "Quantity", "4", 0, $values);

// attention: curreny of suppliers price is defined in table suppliers
$list2->add_column("order_item_supplier_price", "Price", "", "", "", COLUMN_ALIGN_RIGHT);


$list2->add_column("order_item_po_number", "P.O. Number");
$list2->add_column("forwarder_company", "Forwarder");

//if ($project["order_budget_is_locked"] == 1)
if($project["order_actual_order_state_code"] > '600')
{
    $list2->add_button("nothing", "");
}
else
{
    //$list2->add_button("save_items", "Save");
    $list2->add_button("add_special_items_2", "Add Special Item");
}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list1->populate();
$list1->process();

if ($list1->button("add_items"))
{
    $link = "project_add_catalog_item_item_list_supplier.php?pid=" . param("pid") . "&oid=" . $project["project_order"]; 
    redirect($link);
}
elseif ($list1->button("save_items"))
{
    project_update_order_items($list1);
}


$list2->populate();
$list2->process();


if ($list2->button("add_special_items_2"))
{
    $link = "project_add_special_item_individual_supplier.php?pid=" . param("pid") . "&oid=" . $project["project_order"]; 
    redirect($link);
}
elseif ($list2->button("save_items"))
{
    project_update_order_items($list2);
}


/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Edit Supplier's List of Materials");
$form->render();
$list1->render();
echo "<p>&nbsp;</p>";
$list2->render();
$page->footer();

?>