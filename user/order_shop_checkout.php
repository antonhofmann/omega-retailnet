<?php
/********************************************************************

    order_shop_checkout.php

    Checkout for a shop order from the shop_configurator

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-08-04
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-08-04
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/shop_configurator_functions.php";

set_referer("order_shop_confirm.php");

check_access("can_create_new_orders");

/********************************************************************
    prepare all data needed
*********************************************************************/
$tmp_text1="Please indicate the notify address.";
$tmp_text2="\nYou can either select an existing address or enter a new address.";
$tmp_text3="Please indicate the delivery address.";


// get user data
$user = get_user(user_id());

// get users' company address
$address = get_address($user["address"]);

// get client's currency
$currency = get_address_currency($user["address"]);

// create array for the franchisee address listbox
$franchisee_addresses = get_franchisee_addresses($user["address"]);


// create array for the billing address listbox
$billing_addresses = get_billing_addresses($user["address"]);

// create array for the delivery address listbox
$delivery_addresses = get_delivery_addresses($user["address"]);

$order_data = get_shop_basket_addresses();

// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ".
                 "order by country_name";


// create sql for the voltage listbox
$sql_voltages = "select voltage_id, voltage_name ".
                "from voltages";


// create sql for the location type listbox
$sql_location_types = "select location_type_id, location_type_name ".
                      "from location_types ".
                      "order by location_type_name";

// create sql for the transportation type listbox
$sql_transportation_modes = "select transportation_type_id, transportation_type_name ".
                            "from transportation_types ".
                            "where transportation_type_visible = 1 " .
							" and transportation_type_code = 'mode' " . 
                            "order by transportation_type_name";


$sql_transportation_arranged = "select transportation_type_id, transportation_type_name ".
								"from transportation_types ".
								"where transportation_type_visible = 1 " .
								" and transportation_type_code = 'arranged' " . 
								"order by transportation_type_name";


//project legal types
$p_cost_types = array();

if($address["client_type"] == 1) // client is agent, only franchisee as option
{
    $sql = "select * from project_costtypes where project_costtype_id = 2";
}
else
{
    $sql = "select * from project_costtypes where project_costtype_id < 3";    
}

$res = mysql_query($sql);
while ($row = mysql_fetch_assoc($res))
{
    $p_cost_types[$row["project_costtype_id"]] = $row["project_costtype_text"];
}

//$p_cost_type = $order_data["project_cost_type"];
//if(!$p_cost_type) {$p_cost_type = 0;}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("", "order");


$form->add_section("Client");
$form->add_lookup("address_id","Client", "addresses" , "address_company", 0, $user["address"]);



if($address["client_type"] == 1) // client is agent, only franchisee as option
{
    $form->add_hidden("project_cost_type", 2);
}
else
{
    $form->add_section("Project Legal Type");
    $form->add_list("project_cost_type", "Project Legal Type", $p_cost_types, NOTNULL | SUBMIT);
}

//Franchisee Address
if(param("project_cost_type") == 2 or $address["client_type"] == 1)
{
	$form->add_section("Franchisee Address");
	
	if (count($franchisee_addresses) > 0)
	{
		$tmp_text1="Please indicate the franchisee address.";
		$tmp_text2="\nYou can either select an existing address or enter a new address.";
		$form->add_comment($tmp_text1 . $tmp_text2);
		$form->add_list("franchisee_address_id", "Franchisee", $franchisee_addresses, SUBMIT);
	}
}

else
{
	$form->add_section("Client Address");
	$tmp_text1="Please indicate the client address.";
	$form->add_hidden("franchisee_address_id", 0);
	$form->add_comment($tmp_text1);
}


$form->add_edit("franchisee_address_company", "Company*",NOTNULL , "", TYPE_CHAR);
$form->add_edit("franchisee_address_company2", "", 0, "", TYPE_CHAR);
$form->add_edit("franchisee_address_address", "Address*",NOTNULL , "", TYPE_CHAR);
$form->add_edit("franchisee_address_address2", "", 0, "", TYPE_CHAR);
$form->add_edit("franchisee_address_zip", "ZIP*",NOTNULL , "", TYPE_CHAR, 20);
$form->add_edit("franchisee_address_place", "City*",NOTNULL , "", TYPE_CHAR, 20);
$form->add_list("franchisee_address_country", "Country*", $sql_countries, 0, "", NOTNULL);
$form->add_edit("franchisee_address_phone", "Phone*",NOTNULL , "", TYPE_CHAR, 20);
$form->add_edit("franchisee_address_fax", "Fax",0 , "", TYPE_CHAR, 20);
$form->add_edit("franchisee_address_email", "Email",0 , "", TYPE_CHAR);
$form->add_edit("franchisee_address_contact", "Contact*",NOTNULL ,"", TYPE_CHAR);

// Billing Address
/*
$form->add_section("Notify Address (notify address)");

if (count($billing_addresses) > 0 )
{
    $form->add_comment($tmp_text1 . $tmp_text2);
    $form->add_list("billing_address_id", "Notify to", $billing_addresses, SUBMIT);
}
else
{
    $form->add_comment($tmp_text1);
}

$form->add_edit("billing_address_company", "Company*", 0, 
                isset($order_data['billing_address_company']) ? 
                    $order_data['billing_address_company']: $address["company"]);

$form->add_edit("billing_address_company2", "",0,
                isset($order_data['billing_address_company2']) ? 
                $order_data['billing_address_company2'] : $address["company2"]);

$form->add_edit("billing_address_address", "Address*", 0,
                isset($order_data['billing_address_address']) ? 
                $order_data['billing_address_address']: $address["address"]);

$form->add_edit("billing_address_address2", "",0,
                isset($order_data['billing_address_address2'])? 
                $order_data['billing_address_address2']: $address["address2"]);

$form->add_edit("billing_address_zip", "ZIP*", 0, 
                isset($order_data['billing_address_zip'])?
                $order_data['billing_address_zip'] : $address["zip"], TYPE_CHAR, 20);

$form->add_edit("billing_address_place", "City*", 0, 
                isset($order_data['billing_address_place'])? 
                $order_data['billing_address_place'] : $address["place"], TYPE_CHAR, 20);

$form->add_list("billing_address_country", "Country*", $sql_countries, 0,
            isset($order_data['billing_address_country']) ?
            $order_data['billing_address_country'] :  $address["country"], TYPE_CHAR, 20);

$form->add_edit("billing_address_phone", "Phone*", 0, 
                isset($order_data['billing_address_phone']) 
                ? $order_data['billing_address_phone'] : $user["phone"] );

$form->add_edit("billing_address_fax", "Fax",0, 
                isset($order_data['billing_address_fax']) 
                ? $order_data['billing_address_fax'] : $user["fax"]);

$form->add_edit("billing_address_email", "Email",0, 
                isset($order_data['billing_address_email']) ? 
                $order_data['billing_address_email']: $user["email"] );

$form->add_edit("billing_address_contact", "Contact*" , 0,
                isset($order_data['billing_address_contact']) ?
                $order_data['billing_address_contact'] : $user["contact"]);

*/

$form->add_hidden("billing_address_company", $address["company"], TYPE_CHAR);
$form->add_hidden("billing_address_company2", $address["company2"], TYPE_CHAR);
$form->add_hidden("billing_address_address", $address["address"], TYPE_CHAR);
$form->add_hidden("billing_address_address2", $address["address2"], TYPE_CHAR);
$form->add_hidden("billing_address_zip", $address["zip"], TYPE_CHAR, 20);
$form->add_hidden("billing_address_place", $address["place"], TYPE_CHAR, 20);
$form->add_hidden("billing_address_country", $address["country"], NOTNULL);
$form->add_hidden("billing_address_phone", $user["phone"], TYPE_CHAR, 20);
$form->add_hidden("billing_address_fax", $user["fax"], TYPE_CHAR, 20);
$form->add_hidden("billing_address_email", $user["email"], TYPE_CHAR);
$form->add_hidden("billing_address_contact", $user["contact"], TYPE_CHAR);


//Delivery Address
$form->add_section("Delivery Address");

/*
$form->add_checkbox("delivery_is_billing_address", 
                    "delivery address is identical to notify address",
                    isset($order_data['delivery_is_billing_address']) ?
                    $order_data['delivery_is_billing_address'] :  0);


if (count($delivery_addresses) > 0 )
{
    $form->add_comment($tmp_text3 . $tmp_text2);
    $form->add_list("delivery_address_id", "Delivery to", $delivery_addresses, SUBMIT);
}
else
{
    $form->add_comment($tmp_text1);
}

*/

$form->add_edit("delivery_address_company", "Company*", 0, 
                isset($order_data['delivery_address_company']) ? 
                    $order_data['delivery_address_company']: "");

$form->add_edit("delivery_address_company2", "",0,
                isset($order_data['delivery_address_company2']) ? 
                $order_data['delivery_address_company2'] : "");

$form->add_edit("delivery_address_address", "Address*", 0,
                isset($order_data['delivery_address_address']) ? 
                $order_data['delivery_address_address']: "");

$form->add_edit("delivery_address_address2", "",0,
                isset($order_data['delivery_address_address2'])? 
                $order_data['delivery_address_address2']: "");

$form->add_edit("delivery_address_zip", "ZIP*", 0, 
                isset($order_data['delivery_address_zip'])?
                $order_data['delivery_address_zip'] : "", TYPE_CHAR, 20);

$form->add_edit("delivery_address_place", "City*", 0, 
                isset($order_data['delivery_address_place'])? 
                $order_data['delivery_address_place'] : "", TYPE_CHAR, 20);

$form->add_list("delivery_address_country", "Country*", $sql_countries, 0,
            isset($order_data['delivery_address_country']) ?
            $order_data['delivery_address_country'] :  $order_data['billing_address_country']);

$form->add_edit("delivery_address_phone", "Phone*", 0, 
                isset($order_data['delivery_address_phone']) 
                ? $order_data['delivery_address_phone'] : "" );

$form->add_edit("delivery_address_fax", "Fax",0, 
                isset($order_data['delivery_address_fax']) 
                ? $order_data['delivery_address_fax'] : "");

$form->add_edit("delivery_address_email", "Email",0, 
                isset($order_data['delivery_address_email']) ? 
                $order_data['delivery_address_email']: "" );

$form->add_edit("delivery_address_contact", "Contact*" , 0,
                isset($order_data['delivery_address_contact']) ?
                $order_data['delivery_address_contact'] : "");


//POS Location
$form->add_section("POS Location Address Address");
$form->add_checkbox("shop_is_delivery_address", 
                    "POS address is identical to delivery address",
                    isset($order_data['shop_is_delivery_address']) ?
                    $order_data['shop_is_delivery_address'] :  0);

$form->add_comment("Please indicate the POS address in case it is not identical to the delivery address.");

$form->add_edit("shop_address_company", "Company*", 0, 
                isset($order_data['shop_address_company']) ?
                $order_data['shop_address_company'] :  "", TYPE_CHAR);

$form->add_edit("shop_address_company2", "", 0, 
                isset($order_data['shop_address_company2']) ?
                $order_data['shop_address_company2'] : "", TYPE_CHAR);

$form->add_edit("shop_address_address", "Address*", 0, 
                isset($order_data['shop_address_address']) ?
                $order_data['shop_address_address'] :  "", TYPE_CHAR);

$form->add_edit("shop_address_address2", "", 0, 
                isset($order_data['shop_address_address2']) ?
                $order_data['shop_address_address2'] :  "", TYPE_CHAR);

$form->add_edit("shop_address_zip", "ZIP*", 0, 
                isset($order_data['shop_address_zip']) ?
                $order_data['shop_address_zip'] :  "", TYPE_CHAR, 20);

$form->add_edit("shop_address_place", "City*", 0, 
                isset($order_data['shop_address_place']) ?
                $order_data['shop_address_place'] :  "", TYPE_CHAR, 20);

$form->add_list("shop_address_country", "Country*", $sql_countries, 0,
            isset($order_data['shop_address_country']) ?
            $order_data['shop_address_country'] :  $order_data['billing_address_country']);

$form->add_edit("shop_address_phone", "Phone", 0, 
                isset($order_data['shop_address_phone']) ?
                $order_data['shop_address_phone'] :  "", TYPE_CHAR, 20);

$form->add_edit("shop_address_fax", "Fax", 0, 
                isset($order_data['shop_address_fax']) ?
                $order_data['shop_address_fax'] :  "", TYPE_CHAR, 20);

$form->add_edit("shop_address_email", "Email", 0, 
                isset($order_data['shop_address_email']) ?
                $order_data['shop_address_email'] :  "", TYPE_CHAR, 50);

$form->add_section("Location Info");
$form->add_list("location_type", "Location Type*", $sql_location_types);

$form->add_list("voltage", "Voltage*",
                    "select voltage_id, voltage_name " .
                    "from voltages ", NOTNULL,
                    isset($order_data['voltage']) ? 
                    $order_data['voltage'] : 0);


$form->add_section("Preferences and Traffic Checklist");
$form->add_comment("Please enter the date in the form of dd.mm.yy");

$form->add_edit("preferred_delivery_date", "Preferred Arrival Date*", NOTNULL, 
                isset($order_data['preferred_delivery_date']) ?
                $order_data['preferred_delivery_date'] :  "", TYPE_DATE, 20);


$form->add_list("preferred_transportation_arranged", "Transportation arranged by*", $sql_transportation_arranged, NOTNULL,
                    isset($order_data['preferred_transportation_arranged']) ? 
                    $order_data['preferred_transportation_arranged'] : 0);

$form->add_list("preferred_transportation_mode", "Transportation mode*", $sql_transportation_modes, NOTNULL,
                    isset($order_data['preferred_transportation_mode']) ? 
                    $order_data['preferred_transportation_mode'] : 0);

/*
$form->add_radiolist("packaging_retraction", "Packaging Retraction Desired",
                         array(0 => "no", 1 => "yes"), 0,
                         isset($order_data['packaging_retraction']) ? 
                         $order_data['packaging_retraction'] : 0);
*/

$form->add_comment("Please indicate if there is a special approval needed for delivery into ".
                   "a pedestrian area."); 
$form->add_radiolist("pedestrian_mall_approval", "Pedestrian Area Approval Needed",
                         array(0 => "no", 1 => "yes"), 0, 
                         isset($order_data['pedestrian_mall_approval']) ? 
                         $order_data['pedestrian_mall_approval'] : 0);

$form->add_comment("Please indicate if partial delivery is possible or full delivery is required."); 
$form->add_radiolist( "full_delivery", "Full Delivery", array(0 => "no", 1 => "yes"), 0, 
                        isset($order_data['full_delivery']) ? 
                         $order_data['full_delivery'] : 1);

$form->add_comment("Please indicate any other circumstances/restrictions concerning delivery and traffic."); 
$form->add_multiline("delivery_comments", "Delivery Comments", 4, 0, 
                isset($order_data['delivery_comments']) ?
                $order_data['delivery_comments'] :  "");



$form->add_section("Other Information");
$form->add_comment("Please use only figures to indicate the budget ".
                   "and enter the date in the form of dd.mm.yy");
$form->add_edit("approximate_budget", "Approximate Budget in " . $currency["symbol"] . "*", NOTNULL, "", TYPE_DECIMAL, 20,2);
$form->add_edit("planned_opening_date", "Planned Opening Date*", NOTNULL, "", TYPE_DATE, 20);


$form->add_section("General Comments");
$form->add_multiline("comments", "Comments", 4, 0, 
                isset($order_data['comments']) ?
                $order_data['comments'] :  "");


$form->add_button("save", "Proceed to Ordering");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button("cancel", "Cancel Ordering");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$form->populate();
$form->process();

if ($form->button("cancel"))
{
    redirect("orders.php");
}
else if ($form->button("project_cost_type"))
{
	if($form->value("project_cost_type") == 1) // Corporate Store
	{
		$form->value("franchisee_address_id", 0);
		$form->value("franchisee_address_company", $address["company"]);
		$form->value("franchisee_address_company2",  $address["company2"]);
		$form->value("franchisee_address_address",  $address["address"]);
		$form->value("franchisee_address_address2",  $address["address2"]);
		$form->value("franchisee_address_zip",  $address["zip"]);
		$form->value("franchisee_address_place",  $address["place"]);
		$form->value("franchisee_address_country",  $address["country"]);
		$form->value("franchisee_address_phone",  $user["phone"]);
		$form->value("franchisee_address_fax",  $user["fax"]);
		$form->value("franchisee_address_email",  $user["email"]);
		$form->value("franchisee_address_contact",  $user["contact"]);
	}
	elseif($form->value("project_cost_type") == 2) // Franchisee Store
	{
		$form->value("franchisee_address_company", "");
		$form->value("franchisee_address_company2",  "");
		$form->value("franchisee_address_address",  "");
		$form->value("franchisee_address_address2",  "");
		$form->value("franchisee_address_zip",  "");
		$form->value("franchisee_address_place",  "");
		$form->value("franchisee_address_country",  0);
		$form->value("franchisee_address_phone",  "");
		$form->value("franchisee_address_fax",  "");
		$form->value("franchisee_address_email",  "");
		$form->value("franchisee_address_contact",  "");
	}
}
else if ($form->button("franchisee_address_id"))
{
    
	if($form->value("project_cost_type") == 2) // Franchisee Store
	{
		// set new franchisee address
		$form->value("franchisee_address_company", "");
		$form->value("franchisee_address_company2",  "");
		$form->value("franchisee_address_address",  "");
		$form->value("franchisee_address_address2",  "");
		$form->value("franchisee_address_zip",  "");
		$form->value("franchisee_address_place",  "");
		$form->value("franchisee_address_country",  0);
		$form->value("franchisee_address_phone",  "");
		$form->value("franchisee_address_fax",  "");
		$form->value("franchisee_address_email",  "");
		$form->value("franchisee_address_contact",  "");

		if ($form->value("franchisee_address_id"))
		{
			$sql = "select * from orders where order_id = " . $form->value("franchisee_address_id");
			$res = mysql_query($sql) or dberror($sql);

			if ($row = mysql_fetch_assoc($res))
			{
				$form->value("franchisee_address_company", $row["order_franchisee_address_company"]);
				$form->value("franchisee_address_company2",  $row["order_franchisee_address_company2"]);
				$form->value("franchisee_address_address",  $row["order_franchisee_address_address"]);
				$form->value("franchisee_address_address2",  $row["order_franchisee_address_address2"]);
				$form->value("franchisee_address_zip",  $row["order_franchisee_address_zip"]);
				$form->value("franchisee_address_place",  $row["order_franchisee_address_place"]);
				$form->value("franchisee_address_country",  $row["order_franchisee_address_country"]);
				$form->value("franchisee_address_phone",  $row["order_franchisee_address_phone"]);
				$form->value("franchisee_address_fax",  $row["order_franchisee_address_fax"]);
				$form->value("franchisee_address_email",  $row["order_franchisee_address_email"]);
				$form->value("franchisee_address_contact",  $row["order_franchisee_address_contact"]);
			}
		}
	}
}
/*
else if ($form->button("billing_address_id"))
{
    // set new billing address
    $form->value("billing_address_company", "");
    $form->value("billing_address_company2",  "");
    $form->value("billing_address_address",  "");
    $form->value("billing_address_address2",  "");
    $form->value("billing_address_zip",  "");
    $form->value("billing_address_place",  "");
    $form->value("billing_address_country",  0);
    $form->value("billing_address_phone",  "");
    $form->value("billing_address_fax",  "");
    $form->value("billing_address_email",  "");
    $form->value("billing_address_contact",  "");

    if ($form->value("billing_address_id"))
    {
        $sql = "select * from orders where order_id = " . $form->value("billing_address_id");
        $res = mysql_query($sql) or dberror($sql);

        if ($row = mysql_fetch_assoc($res))
        {
            $form->value("billing_address_company", $row["order_billing_address_company"]);
            $form->value("billing_address_company2",  $row["order_billing_address_company2"]);
            $form->value("billing_address_address",  $row["order_billing_address_address"]);
            $form->value("billing_address_address2",  $row["order_billing_address_address2"]);
            $form->value("billing_address_zip",  $row["order_billing_address_zip"]);
            $form->value("billing_address_place",  $row["order_billing_address_place"]);
            $form->value("billing_address_country",  $row["order_billing_address_country"]);
            $form->value("billing_address_phone",  $row["order_billing_address_phone"]);
            $form->value("billing_address_fax",  $row["order_billing_address_fax"]);
            $form->value("billing_address_email",  $row["order_billing_address_email"]);
            $form->value("billing_address_contact",  $row["order_billing_address_contact"]);
        }
    }
}
else if ($form->button("delivery_address_id"))
{
    // set delivery address
    $form->value("delivery_address_company", "");
    $form->value("delivery_address_company2",  "");
    $form->value("delivery_address_address",  "");
    $form->value("delivery_address_address2",  "");
    $form->value("delivery_address_zip",  "");
    $form->value("delivery_address_place",  "");
    $form->value("delivery_address_country",  0);
    $form->value("delivery_address_phone",  "");
    $form->value("delivery_address_fax",  "");
    $form->value("delivery_address_email",  "");
    $form->value("delivery_address_contact",  "");

    if ($form->value("delivery_address_id"))
    {
        $sql = "select * from order_addresses where order_address_id = " . $form->value("delivery_address_id");
        $res = mysql_query($sql) or dberror($sql);

        if ($row = mysql_fetch_assoc($res))
        {
            $form->value("delivery_address_company", $row["order_address_company"]);
            $form->value("delivery_address_company2",  $row["order_address_company2"]);
            $form->value("delivery_address_address",  $row["order_address_address"]);
            $form->value("delivery_address_address2",  $row["order_address_address2"]);
            $form->value("delivery_address_zip",  $row["order_address_zip"]);
            $form->value("delivery_address_place",  $row["order_address_place"]);
            $form->value("delivery_address_country",  $row["order_address_country"]);
            $form->value("delivery_address_phone",  $row["order_address_phone"]);
            $form->value("delivery_address_fax",  $row["order_address_fax"]);
            $form->value("delivery_address_email",  $row["order_address_email"]);
            $form->value("delivery_address_contact",  $row["order_address_contact"]);
        }
    }   
}
*/
else if ($form->button("save"))
{
    // add validation ruels
    

    $form->add_validation("from_system_date({preferred_delivery_date}) >  " . dbquote(date("Y-m-d")), "The preferred arrival date must be a future date!");
    
    /*
	if (!$form->value("delivery_is_billing_address"))
    {
        $form->add_validation("{delivery_address_company} and " .
                              "{delivery_address_address} and ".
                              "{delivery_address_zip} and " . 
                              "{delivery_address_place} and " .
                              "{delivery_address_country} and " .
                              "{delivery_address_phone} and " .
                              "{delivery_address_contact}", "The delivery address is not complete.");
    }
	*/


	$form->add_validation("{delivery_address_company} and " .
                              "{delivery_address_address} and ".
                              "{delivery_address_zip} and " . 
                              "{delivery_address_place} and " .
                              "{delivery_address_country} and " .
                              "{delivery_address_phone} and " .
                              "{delivery_address_contact}", "The delivery address is not complete.");

    if (!$form->value("shop_is_delivery_address"))
    {
        $form->add_validation("{shop_address_company} and ".
                              "{shop_address_address} and ".
                              "{shop_address_address} and ".
                              "{shop_address_zip} and ".
                              "{shop_address_place} and ".
                              "{shop_address_country}", "The POS address is not complete!");
    }


    // validate form
    
    if ($form->validate())
    {
		$result = save_shop_basket_addresses($_REQUEST);
        redirect("order_shop_confirm.php");
    }

}
    
/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

$page->register_action('home', 'Home', "welcome.php");

$page->header();
$page->title("Checkout");
$form->render();
$page->footer();


?>