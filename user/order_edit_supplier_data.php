<?php
/********************************************************************

    order_edit_supplier_data.php

    Edit Data concerning delivery and traffic

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-24
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-01-07
    Version:        1.0.3

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "include/order_functions.php";

check_access("can_edit_supplier_data_in_orders");

register_param("oid");
set_referer("order_edit_supplier_data_item.php");
set_referer("order_new.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());
$user_roles = get_user_roles(user_id());

// read order details
$order = get_order(param("oid"));

// get company's address
$client_address = get_address($order["order_client_address"]);

// create sql for oder items
$sql_order_items = "select order_item_id, order_item_text, order_item_quantity, ".
                   "    order_item_supplier_price, order_item_po_number, ".
                   "    concat(if(order_item_item <>'', item_code, item_type_name), '\n', if(order_item_po_number <>'', order_item_po_number, '')) as item_po, " .
                   "    concat(order_item_quantity, '\n', order_item_supplier_price) as quantity_price, ".
                   "    (order_item_supplier_price * order_item_quantity) as total_price, ".
                   "    if(order_item_item <>'', item_code, item_type_name) as item_shortcut, ".
                   "    addresses.address_company as supplier_company, ".
                   "    addresses_1.address_company as forwarder_company, ".
                   "    currency_symbol, ".
                   "    concat(addresses.address_company, ', ', currency_symbol) as group_head " .
                   "from order_items ".
                   "left join items on order_item_item = item_id ".
                   "left join item_types on order_item_type = item_type_id ".
                   "left join addresses on order_item_supplier_address = addresses.address_id ".
                   "left join addresses AS addresses_1 on order_item_forwarder_address = addresses_1.address_id ".
                   "left join currencies on order_item_supplier_currency = currency_id ";



// get order_item_dates
 $sql_order_item_dates = $sql_order_items . " " .
                         "where (order_item_type = " . ITEM_TYPE_STANDARD .
                         "   or order_item_type = " . ITEM_TYPE_SPECIAL . ") ".
                         "   and order_item_order = " . param('oid') . " " .
                         "order by supplier_company, order_item_type, item_code";

$dates = get_order_dates_from_dates($sql_order_item_dates);

$dates1 = array();
foreach($dates["ORDR"] as $key=>$value)
{
     $dates1[$key] = $value . "\n" . $dates["EXRP"][$key];
}

// build group_totals of suppliers
$group_totals = get_group_total_of_suppliers(param('oid'), "order_item_supplier_price");


/********************************************************************
    build form
*********************************************************************/
$form = new Form("orders", "order");

$form->add_hidden("oid", param("oid"));

require_once "include/order_head_small.php";

// traffic preferences

$form->add_section("Preferences and Traffic Checklist");
$form->add_label("preferred_delivery_date", "Preferred Arrival Date", 0, to_system_date($order["order_preferred_delivery_date"]));

if((in_array(5, $user_roles)  or in_array(29, $user_roles)) and $order["order_actual_order_state_code"] > 699)
{
    $form->add_label("preferred_transportation_text", "Preferred Transportation", 0, "shipping defined by operator");
}
else
{
    $form->add_lookup("preferred_transportation_arranged", "Transportation arranged by", "transportation_types", "transportation_type_name", 0, $order["order_preferred_transportation_arranged"]);

	$form->add_lookup("preferred_transportation_mode", "Transportation mode", "transportation_types", "transportation_type_name", 0, $order["order_preferred_transportation_mode"]);
}

/*
$value="no";
if ($order["order_packaging_retraction"])
{
    $value="yes";
}
$form->add_label("order_packaging_retraction", "Packaging Retraction Desired", 0, $value);
*/

$value="no";
if ($order["order_pedestrian_mall_approval"])
{
    $value="yes";
}
$form->add_label("pedestrian_mall_approval", "Pedestrian Area Approval Needed", 0, $value);

$value="no";
if ($order["order_full_delivery"])
{
    $value="yes";
}
$form->add_label("full_delivery", "Full Delivery Desired", 0, $value);

$form->add_label("delivery_comments", "Delivery Comments", 0, $order["order_delivery_comments"]);

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Create Item List
*********************************************************************/ 
$list = new ListView($sql_order_items, LIST_HAS_SEPARATOR | LIST_HAS_HEADER);

$list->set_title("Items up to Delivery");
$list->set_entity("order_item");
$list->set_group("group_head");

if (has_access("has_access_to_all_supplier_data_in_orders"))
{
    $list->set_filter("(order_item_not_in_budget = 0 or order_item_not_in_budget is null) and ".
                      "order_item_type <= " . ITEM_TYPE_SPECIAL .
                      "   and order_item_order = " . param('oid'));
}
else
{
    $list->set_filter("(order_item_not_in_budget = 0 or order_item_not_in_budget is null) and ".          
                      "order_item_type <= " . ITEM_TYPE_SPECIAL .
                      "   and order_item_order = " . param('oid') .
                      "   and order_item_supplier_address = " . $user_data["address"]);
}


$list->set_order("order_item_type, order_item_text");

$link="order_edit_supplier_data_item.php?oid=" . param('oid');

$list->add_column("item_po", "Item Code\nP.O. Number", $link, "", "", COLUMN_BREAK);
$list->add_column("order_item_text", "Name", "", "", "", COLUMN_BREAK);
$list->add_column("quantity_price", "Quantity\nPrice", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_BREAK);
$list->add_column("total_price", "Total", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_BREAK );
$list->add_column("forwarder_company", "Forwarder", "", "", "", COLUMN_NO_WRAP );

$list->add_text_column("dates_1", "Ordered\nReady 4 Pickup", COLUMN_NO_WRAP | COLUMN_BREAK, $dates1);

// set group totals
foreach ($group_totals["currency"] as $key=>$value)
{
    $list->set_group_footer("quantity_price", $key , $value);
}
foreach ($group_totals["total"] as $key=>$value)
{
    $list->set_group_footer("total_price", $key , $value);
}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("Edit Pickup Data");
$form->render();
$list->render();

$page->footer();

?>