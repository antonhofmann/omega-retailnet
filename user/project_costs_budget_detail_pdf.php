<?php
/********************************************************************

    project_costs_budget_detail pdf.php

    View project budget in a PDF

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-01-09
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2014, OMEGA SA, All Rights Reserved.

*********************************************************************/

if(!function_exists ( "print_page_header"))
{
	function print_page_header($pdf, $page_title)
	{
		$pdf->AddPage("l","A4", $page_title);
		$pdf->SetY(8);
		$pdf->Image('../pictures/logo.jpg',10,8,33);
		$pdf->SetFont('arialn','B',12);
		$pdf->Cell(80);
		$pdf->Cell(0,10, $page_title, 0, 0, 'R');
		$pdf->Ln(5);
		$pdf->SetFont('arialn','I',8);
		$pdf->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$pdf->PageNo() ." ",0,0,'R');
		$pdf->Ln(15);
	}
}

/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());

// read project and order details
$project = get_project(param("pid"));

// get orders's currency
$order_currency = get_order_currency($project["project_order"]);
$currency_symbol = $order_currency["symbol"];

if(param("sc") == 1 or isset($sc) and $sc == 1)
{
	$currency_symbol = "CHF";
}

$page_title = "Project Budget - Project " . $project["project_number"];

$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];


$order_state_name = get_actual_order_state_name($project["order_actual_order_state_code"], 1);
$retail_coordinator = get_user($project["project_retail_coordinator"]);
$retail_operator = get_user($project["order_retail_operator"]);

$invoice_address = "";

$billing_address_province_name = "";
if($project["order_billing_address_place_id"])
{
	$billing_address_province_name = get_province_name($project["order_billing_address_place_id"]);
}

$invoice_address = $invoice_address . $project["order_billing_address_company"];
if ($project["order_billing_address_company2"])
{
    $invoice_address = $invoice_address . ", " . $project["order_billing_address_company2"];
}

$invoice_address = $invoice_address . ", " . $project["order_billing_address_address"];

if ($project["order_billing_address_address2"])
{
    $invoice_address = $invoice_address . ", " . $project["order_billing_address_address2"];
}

$invoice_address = $invoice_address . ", " . $project["order_billing_address_zip"] . " " . $project["order_billing_address_place"];

if($billing_address_province_name) {
	$invoice_address .= ", " . $billing_address_province_name;
}
$invoice_address = $invoice_address . ", " . $project["order_billing_address_country_name"];
$invoice_address = $invoice_address . ", Phone: " . $project["order_billing_address_phone"];

// get company's address
$client_address = get_address($project["order_client_address"]);
$client = $client_address["company"] . ", " .
          $client_address["zip"] . " " . $client_address["place"] . ", " .
          $client_address["country_name"];


//get budget totals
$budget = get_project_budget_totals(param("pid"), $order_currency);

if(param("sc") == 1 or isset($sc) and $sc == 1)
{
	$budget["subgroup_totals_by_id"] = $budget["subgroup_totals_chf_by_id"];
	$budget["subgroup_totals"] = $budget["subgroup_totals_chf"];
	$budget["group_totals"] = $budget["group_totals_chf"];
	$budget["group_totals_formated"] = $budget["group_totals_chf_formated"];
	$budget["budget_total"] = $budget["budget_total_chf"];
}


//get cost groups
$costgroups = get_cost_groups();


//get cost sub groups
$cost_sub_groups = array();
$sql = "select costsheet_id, costsheet_pcost_group_id, costsheet_pcost_subgroup_id, " . 
	   "costsheet_budget_amount, costsheet_budget_approved_amount, costsheet_real_amount, " . 
	   "costsheet_code, costsheet_text, costsheet_comment, " .
	   "concat(pcost_subgroup_code, ' ', pcost_subgroup_name) as subgroup  " .
	   "from costsheets " .
	   " left join pcost_subgroups on pcost_subgroup_id = costsheet_pcost_subgroup_id " . 
	   " where costsheet_project_id = " . param("pid") . 
	   " and costsheet_is_in_budget = 1 " . 
	   " order by subgroup ";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$cost_sub_groups[$row["costsheet_pcost_group_id"]][$row["costsheet_pcost_subgroup_id"]] = $row["subgroup"];
}


//get all cost positions
$costsheet_positions = array();
$sql = "select costsheet_id, costsheet_pcost_group_id, costsheet_pcost_subgroup_id, " . 
	   "costsheet_budget_amount, costsheet_budget_approved_amount, costsheet_real_amount, " . 
	   "costsheet_code, costsheet_text, costsheet_comment, costsheet_company " .
	   "from costsheets " .
	   " where costsheet_project_id = " . param("pid") . 
	   " and costsheet_is_in_budget = 1 " . 
	   " order by LENGTH(costsheet_code), costsheet_code ";
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$tmp = array();
	$tmp['code'] = $row["costsheet_code"];
	$tmp['text'] = trim(str_replace("\r\n", ' ', $row["costsheet_text"]));
	$tmp['company'] = trim(str_replace("\r\n", ' ', $row["costsheet_company"]));
	$tmp['comment'] = trim(str_replace("\r\n", ' ', $row["costsheet_comment"]));
	$tmp['budget_amount'] = $row["costsheet_budget_amount"];

	$costsheet_positions[$row["costsheet_pcost_group_id"]][$row["costsheet_pcost_subgroup_id"]][$row["costsheet_id"]] = $tmp;
}

// prepare output fields
$captions1 = array();
$captions1[] = "Project Number:";
if($project["productline_subclass_name"])
{
	$captions1[] = "Product Line / Subclass / Type:";
}
else
{
	$captions1[] = "Product Line / Type:";
}
$captions1[] = "Project Starting Date / Agreed Opening Date:";
$captions1[] = "Status:";
$captions1[] = "Project Manager / Retail Operator:";
$captions1[] = "Gross / Total / Sales Surface in sqm (TS/SS):";

$captions2 = array();
foreach($costgroups as $group_id=>$group_name)
{
	$captions2[] = $group_name;
}
$captions2[] = "Total";

$captions3 = array();
$captions3[] = "Client:";
$captions3[] = "POS Location:";
$captions3[] = "Notify Address:";

$data1 = array();
$data1[] = $project["project_number"];

if($project["productline_subclass_name"])
{
	$data1[] = $project["product_line_name"] . " / " . $project["productline_subclass_name"] . " / " . $project["postype_name"];
}
else
{
	$data1[] = $project["product_line_name"] . " / " . $project["postype_name"];	
}
$data1[] = to_system_date($project["order_date"]) . " / " .  to_system_date($project["project_real_opening_date"]);
$data1[] = $project["order_actual_order_state_code"] . " " . $order_state_name;
$data1[] = $retail_coordinator["firstname"] . " " . $retail_coordinator["name"] . " / " . $retail_operator["firstname"] . " " . $retail_operator["name"];

$data1[] = $project["project_cost_gross_sqms"] . " / " .$project["project_cost_totalsqms"] . " / " . $project["project_cost_sqms"];


$data2 = array();
$data2n = array();
foreach($costgroups as $group_id=>$group_name)
{
	if(array_key_exists($group_id, $budget["group_totals_formated"]))
	{
		$data2[] = $budget["group_totals_formated"][$group_id];
		$data2n[] = $budget["group_totals"][$group_id];
	}
	else
	{
		$data2[] = "";
		$data2n[] = "";
	}
}
$data2[] = $budget["budget_total"];
$data2n[] = $budget["budget_total"];

$data3 = array();
$data3[] = $client;
$data3[] = $shop;
$data3[] = $invoice_address;


//Instanciation of inherited class


$pdf->SetAutoPageBreak(true, 5);

$pdf->SetLineWidth(0.1);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

print_page_header($pdf, $page_title);


// output project header informations
$pdf->SetFont('arialn','B',10);
$pdf->Cell(120,5,"Project Details",1);
$pdf->Cell(5,5," ",0);
$pdf->Cell(100,5,"Project Costs in " . $currency_symbol,1);


$pdf->SetFont('arialn','',9);
$pdf->Cell(14,5,"Share", 1, 0, 'R');
$pdf->Cell(18,5,"per sqm TS", 1, 0, 'R');
$pdf->Cell(18	,5,"per sqm SS", 1, 0, 'R');
$pdf->Ln();


foreach($captions1 as $key=>$value)
{
    $pdf->Cell(60,5,$captions1[$key],1);
    $pdf->Cell(60,5,$data1[$key],1);

    $pdf->Cell(5,5," ",0);

    if($key <=4)
    {
        
        $pdf->Cell(75,5,$captions2[$key],1);
        $pdf->Cell(25,5,$data2[$key], 1, 0, 'R');

		$share = "";
		if($data2n[$key] > 0 and $budget["budget_total"] > 0)
		{
			$share = number_format(round(100*($data2n[$key] / $budget["budget_total"]), 2), 2)  . '%';
		}
		$pdf->Cell(14,5,$share, 1, 0, 'R');

		if($project["project_cost_totalsqms"] > 0)
		{
			$pdf->Cell(18,5,number_format($data2n[$key]/$project["project_cost_totalsqms"], 2), 1, 0, 'R');
		}
		else
		{
			$pdf->Cell(18,5,"", 1, 0, 'R');
		}

		if($project["project_cost_sqms"] > 0)
		{
			$pdf->Cell(18,5,number_format($data2n[$key]/$project["project_cost_sqms"], 2), 1, 0, 'R');
		}
		else
		{
			$pdf->Cell(18,5,"", 1, 0, 'R');
		}


		$pdf->Ln();
    }
    

    

}
$pdf->setX(135);
$pdf->SetFont('arialn','B',9);
$pdf->Cell(75,5,$captions2[5],1);
$pdf->Cell(25,5,number_format($budget["budget_total"],2), 1, 0, 'R');

if($budget["budget_total"] > 0)
{
	$pdf->Cell(14,5,"100.00%", 1, 0, 'R');
}
else
{
	$pdf->Cell(14,5,"", 1, 0, 'R');
}


if($project["project_cost_totalsqms"] > 0)
{
	$pdf->Cell(18,5,number_format($budget["budget_total"]/$project["project_cost_totalsqms"], 2), 1, 0, 'R');
}
else
{
	$pdf->Cell(18,5,"", 1, 0, 'R');
}

if($project["project_cost_sqms"] > 0)
{
	$pdf->Cell(18,5,number_format($budget["budget_total"]/$project["project_cost_sqms"], 2), 1, 0, 'R');
}
else
{
	$pdf->Cell(18,5,"", 1, 0, 'R');
}


$pdf->SetFont('arialn','',9);

$pdf->Ln();
$pdf->Ln();

foreach($captions3 as $key=>$value)
{
    $pdf->SetFont('arialn','B',9);
    $pdf->Cell(40,5,$captions3[$key],1, 'L');
    $pdf->SetFont('arialn','',9);
    $pdf->MultiCell(235,5,$data3[$key],1, 'L');
}

$pdf->Ln();


//print cost group details
$pdf->SetFillColor(240, 240, 240);
//$pdf->setCellHeightRatio(0.8);
$group_page_break_controller = "";
$number_of_groups = count($costgroups);
$group_counter = 0;
foreach($costgroups as $group_id=>$group_name)
{
	
	$y = $pdf->getY();
	if($y >= 185)
	{
		print_page_header($pdf, $page_title);
	}
	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(110 ,5, $group_name, 1, 0, 'L', 1);
	$pdf->Cell(50 ,5, "Company", 1, 0, 'L', 1);
	$pdf->Cell(75 ,5, "Comments", 1, 0, 'L', 1);
	$pdf->Cell(30 ,5, "Costs " . $currency_symbol , 1, 0, 'R', 1);
	$pdf->Cell(10 ,5, '%' , 1, 0, 'R', 1);
	$pdf->Ln();

	if(array_key_exists($group_id, $cost_sub_groups))
	{
		foreach($cost_sub_groups[$group_id] as $sub_group_id=>$subgroup_name)
		{
			if(array_key_exists($group_id, $budget["subgroup_totals_by_id"]))
			{
				if(array_key_exists($group_id, $costsheet_positions))
				{
					$y = $pdf->getY();
					if($y >= 185)
					{
						print_page_header($pdf, $page_title);
					}
					
					$pdf->SetFont('arialn','B',8);
					$pdf->Cell(110 ,4, $subgroup_name, 1, 0, 'L', 1);
					$pdf->Cell(50 ,4, "", 1, 0, 'L', 1);
					$pdf->Cell(75 ,4, "", 1, 0, 'L', 1);
					$pdf->Cell(30 ,4, number_format($budget["subgroup_totals_by_id"][$group_id][$sub_group_id], 2) , 1, 0, 'R', 1);
					
					$share = "";
					if($budget["subgroup_totals_by_id"][$group_id][$sub_group_id] > 0 and $budget["budget_total"] > 0)
					{
						$share = number_format(round(100*($budget["subgroup_totals_by_id"][$group_id][$sub_group_id] / $budget["budget_total"]), 2), 2)  . '%';
					}
					$pdf->Cell(10 ,4, $share , 1, 0, 'R', 1);

					$pdf->Ln();

					$pdf->SetFont('arialn','',8);

					foreach($costsheet_positions[$group_id][$sub_group_id] as $key=>$data)
					{
						
						if(param("sc") == 1 or isset($sc) and $sc == 1)
						{
							if(count($order_currency) > 0 and $order_currency["factor"] > 0)
							{
								$data["budget_amount"] = ($order_currency["exchange_rate"] *$data["budget_amount"])/$order_currency["factor"];
							}
							else
							{
								$data["budget_amount"] = 0;
							}
						}
						
						$num_lines = $pdf->getStringHeight(100, $data['text'])/3.528;
						$num_lines2 = $pdf->getStringHeight(75, $data['comment'])/3.528;
						
						//add new page if necessary
						$y = $pdf->getY();
						if($y >= 185 or $y+$num_lines > 185 or $y+$num_lines2 > 185)
						{
							print_page_header($pdf, $page_title);
						}
						
						//output data
						$pdf->Cell(10 ,4, $data['code'], 0, 0, 'L', 0, 0);
						$pdf->MultiCell(100, '', $data['text'],0,'L', false, 0);
						$pdf->MultiCell(50, 0,$data['company'],0,'L', false, 0);
						$pdf->MultiCell(75, 0,$data['comment'],0,'L', false, 0);
						$pdf->Cell(30 ,4, number_format($data['budget_amount'], 2), 0, 0, 'R');

						$share = "";
						if($data['budget_amount']> 0 and $budget["budget_total"] > 0)
						{
							$share = number_format(round(100*($data['budget_amount'] / $budget["budget_total"]), 2), 2) . '%';
						}
						$pdf->Cell(10 ,4, $share , 0, 0, 'R', 0);
						
						
						
						if($num_lines2 > $num_lines){$num_lines = $num_lines2;}
						for($i=0;$i<$num_lines;$i++)
						{
							$pdf->Ln();
						}
						$pdf->Line(10,$pdf->GetY(), 285, $pdf->GetY());
						
					}
				}
			}
		}
		$group_counter++;
		if($number_of_groups > $group_counter and $group_page_break_controller != $group_id)
		{
			print_page_header($pdf, $page_title);
			$pdf->setY($pdf->getY()-15);
			$group_page_break_controller = $group_id;
		}
	}

	$pdf->Ln();
	$pdf->Ln();
}




?>