<?php
/********************************************************************

    catalog.php

    Entry page for the catalog section group.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-31
    Modified by:    Anton.Hofmann (aho@mediaparx.ch)
    Date modified:  2003-07-14
    Version:        1.1.4

    Copyright (c) 2002, Swatch AG, All Rights Reserved.


*********************************************************************/
require_once "../include/frame.php";
require_once "include/order_functions.php";
require_once "include/get_functions.php";

set_referer("catalog_category.php");

check_access("can_view_catalog");

/********************************************************************
    prepare all data needed
*********************************************************************/
$roles = get_user_roles(user_id());

// get the region of users' address
$user_region = get_user_region(user_id());

$sql =  "select category_id, category_name, product_line_priority, product_line_name " .
        "from categories ".
        "left join product_lines on product_line_id = category_product_line " .
        "left join productline_regions on productline_region_productline =  product_line_id";

if (count($roles) == 1 and $roles[0] == 4) // user is a client
{
    $list1_filter = "product_line_visible = 1 and (category_not_in_use is null or category_not_in_use = 0) and category_catalog = 1 and product_line_clients=1 and productline_region_region = " . $user_region;
}
else
{
   $list1_filter = "product_line_visible = 1 and (category_not_in_use is null or category_not_in_use = 0) and category_catalog = 1 and productline_region_region = " . $user_region;
}

// product lines attached files
$sql_files_product_lines = "select product_line_file_id, product_line_file_title, file_type_name, " .
             "    product_line_file_description, product_line_file_path, product_line_name " . 
             "from product_line_files " .
             "left join product_lines on product_line_id = product_line_file_product_line " .
             "left join file_types on file_type_id = product_line_file_type ";



/********************************************************************
    Search Form
*********************************************************************/ 
$form = new Form("orders", "order");
$form->add_comment("Enter a search term or choose from the list of item categories below.<br /><br />");
$form->add_edit("searchterm", "Search Term");


$form->add_button("search", "Search Catalog");


/********************************************************************
    Create List of product categories
*********************************************************************/ 
$list1 = new ListView($sql, LIST_HAS_HEADER);

$list1->set_entity("productlines");
$list1->set_order("category_priority");
$list1->set_group("product_line_priority", "product_line_name");
$list1->set_title("Catalog");
$list1->set_filter($list1_filter);

$list1->add_column("category_name", "Product Line", "catalog_select.php");

/********************************************************************
    Create File List
*********************************************************************/ 
$list2 = new ListView($sql_files_product_lines, LIST_HAS_HEADER);

$list2->set_entity("product line files");
$list2->set_title("Documentation");
$list2->set_order("product_line_file_title");
$list2->set_group("product_line_name","product_line_name");

$link = "http://" . $_SERVER["HTTP_HOST"] . "/";
$list2->add_column("product_line_file_title", "Title", $link . "{product_line_file_path}", "", "", COLUMN_NO_WRAP);
$list2->add_column("file_type_name", "Type", "", "", "", COLUMN_NO_WRAP);
$list2->add_column("product_line_file_description", "Description");

/********************************************************************
    Populate page
*********************************************************************/ 

$form->populate();
$form->process();


$list1->process();
$list2->process();


if ($form->button("search"))
{
	if(!$form->value("searchterm"))
	{
		$form->error("The search term must not be empty.");
	}
	else
	{
		$link = "catalog_category.php?searchterm=" . $form->value("searchterm");
		redirect($link);
	}
}

$page = new Page("catalog");
$page->register_action('home', 'Home', "welcome.php");



$page->header();
$page->title('Catalog and Documentation');
$form->render();
echo '<br /><br />';
$list1->render();
$list2->render();
$page->footer();
?>