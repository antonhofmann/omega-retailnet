<?php
/********************************************************************

    project_view_milestones.php

    View MileStones of the Projecs

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-12-12
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-12-12
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_view_milestones");

register_param("pid");

/********************************************************************
    prepare all data needed
*********************************************************************/
$user_roles = get_user_roles(user_id());

// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

// get System currency
$order_currency = get_order_currency($project["project_order"]);


/********************************************************************
    Create Cost Monitoring Sheet
*********************************************************************/ 
//check if the cost monitoring sheet exists already
$sql = "select count(project_cost_id) as num_recs " .
       "from project_costs " .
       "where project_cost_order = "  . $project["project_order"]; 

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

if($row["num_recs"] == 0)
{
    $fields = array();
    $values = array();

    $fields[] = "project_cost_order";
    $values[] = $project["project_order"];
    
    $fields[] = "date_created";
    $values[] = dbquote(date("Y-m-d"));

    $fields[] = "date_modified";
    $values[] = dbquote(date("Y-m-d"));

    $fields[] = "user_created";
    $values[] = dbquote(user_login());

    $fields[] = "user_modified";
    $values[] = dbquote(user_login());

    $sql = "insert into project_costs (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

    mysql_query($sql) or dberror($sql);
}


/********************************************************************
    Create Project Milestones
*********************************************************************/ 
    
$sql = "select * from milestones " .
	   "where milestone_active = 1 " .
	   "order by milestone_code ";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$sql_m = "select count(project_milestone_id) as num_recs from project_milestones " .
		     "where project_milestone_project = " . $project["project_id"] . 
	         "   and project_milestone_milestone = " . $row["milestone_id"];
	
	
	$res_m = mysql_query($sql_m) or dberror($sql_m);
	$row_m = mysql_fetch_assoc($res_m);

	if($row_m["num_recs"] == 0)
	{
		$fields = array();
		$values = array();

		$fields[] = "project_milestone_project";
		$values[] = $project["project_id"];

		$fields[] = "project_milestone_milestone";
		$values[] = $row["milestone_id"];
		
		$fields[] = "date_created";
		$values[] = dbquote(date("Y-m-d"));

		$fields[] = "date_modified";
		$values[] = dbquote(date("Y-m-d"));

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$fields[] = "user_modified";
		$values[] = dbquote(user_login());

		$sql = "insert into project_milestones (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

		mysql_query($sql) or dberror($sql);
	}
}

/********************************************************************
    Get Cost Monitoring Sheet Budget Data
*********************************************************************/ 
$sql =  "select * from project_costs " .
        "where project_cost_order = " . $project["project_order"];

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

$project_cost_sqms = $row["project_cost_sqms"];

$project_cost_CER = $row["project_cost_CER"];
$project_cost_milestone_remarks = $row["project_cost_milestone_remarks"];
$project_cost_milestone_remarks2 = $row["project_cost_milestone_remarks2"];
$project_cost_milestone_turnaround = $row["project_cost_milestone_turnaround"];


/********************************************************************
    Caclucalte dates and diffeneces in dates
*********************************************************************/
$dates = array();
$comments = array();
$datecomments = array();
$daysconsumed = array();
$daysaccumulated = array();
$lastdate = "";
$milestone_turnaround = "";


//get trunaround
$date_min = "0000-00-00";
$sql_m = "select min(project_milestone_date) as date_min " .
         "from project_milestones " .
         " where project_milestone_date <> '0000-00-00' " . 
		 " and project_milestone_date is not NULL " . 
		 " and project_milestone_project = " . $project["project_id"];
$res = mysql_query($sql_m) or dberror($sql_m);
if($row = mysql_fetch_assoc($res))
{
	$date_min = $row["date_min"];
}

$date_max = "0000-00-00";
$sql_m = "select max(project_milestone_date) as date_max " . 
         "from project_milestones " .
         " where project_milestone_date <> '0000-00-00' " . 
		 " and project_milestone_date is not NULL " . 
		 " and project_milestone_project = " . $project["project_id"];
$res = mysql_query($sql_m) or dberror($sql_m);
if($row = mysql_fetch_assoc($res))
{
	$date_max = $row["date_max"];
}

$milestone_turnaround = floor((strtotime($date_max) - strtotime($date_min)) / 86400);


$sql = "select project_milestone_id, project_milestone_date, project_milestone_date_comment, " .
       "project_milestone_comment, milestone_code, milestone_text " . 
       "from project_milestones " .
       " left join milestones on milestone_id = project_milestone_milestone ";

$list_filter = "project_milestone_visible = 1 and project_milestone_project = " . $project["project_id"];


$sql_m = $sql . " where " . $list_filter . " order by milestone_code ";

$res = mysql_query($sql_m) or dberror($sql_m);

while($row = mysql_fetch_assoc($res))
{
	$dates[$row["project_milestone_id"]] = to_system_date($row["project_milestone_date"]);
	$datecomments[$row["project_milestone_id"]] = $row["project_milestone_date_comment"];
    $comments[$row["project_milestone_id"]] = $row["project_milestone_comment"];

	
	if($lastdate != "0000-00-00" and $lastdate != NULL and $row["project_milestone_date"] != "0000-00-00" and $row["project_milestone_date"] != NULL)
	{
		$thisdate = $row["project_milestone_date"];
		$difference_in_days = floor((strtotime($thisdate) - strtotime($date_min)) / 86400);
		
		
		
		$daysconsumed[$row["project_milestone_id"]] = $difference_in_days;
		
		
		//$milestone_turnaround = $milestone_turnaround + $difference_in_days;
		//$daysaccumulated[$row["project_milestone_id"]] = $milestone_turnaround;
	}

	if($row["project_milestone_date"] != "0000-00-00" and $row["project_milestone_date"] != NULL)
	{
		$lastdate = $row["project_milestone_date"];
	}
}



/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project");

$form->add_section("Project");
$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid",$project["project_order"]);

require_once "include/project_head_small.php";

$form->add_label("project_cost_sqms", "Sales Surface sqms", 0, $project_cost_sqms);

if($project["project_cost_type"])
{
    $form->add_label("project_cost_type", "Project Legal Type", $project["project_cost_type"]);
}
else
{
    $form->add_label("dummy", "Project Legal Type");

}


//$form->add_edit("project_cost_CER", "CER Amount " . $order_currency["symbol"], 0 , $project_cost_CER, TYPE_DECIMAL, 12,2);

$form->add_label("project_cost_milestone_turnaround", "Turn Around ", $milestone_turnaround);

$form->add_label("project_cost_milestone_remarks", "Remarks", 0, $project_cost_milestone_remarks);
$form->add_label("project_cost_milestone_remarks2", "Remarks2", 0, $project_cost_milestone_remarks);


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Create List for Milestones
*********************************************************************/ 
$list = new ListView($sql);

$list->set_title("Milestones");
$list->set_entity("project_milestones");
$list->set_filter($list_filter);
$list->set_order("milestone_code");


$list->add_column("milestone_code", "Code");   
$list->add_column("milestone_text", "Description");

$list->add_text_column("project_milestone_date", "Date", COLUMN_ALIGN_RIGHT, $dates);
$list->add_text_column("daysconsumed", "Days", COLUMN_ALIGN_RIGHT, $daysconsumed);
//$list->add_text_column("daysaccumulated", "Sum", COLUMN_ALIGN_RIGHT, $daysaccumulated);
$list->add_text_column("project_milestone_date_comment", "Date Comment",  0, $datecomments);
$list->add_text_column("project_milestone_comment", "General Comment",  0, $comments);



$list->populate();
$list->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Project Milestones");
$form->render();

echo "<br>";
$list->render();

$page->footer();

?>