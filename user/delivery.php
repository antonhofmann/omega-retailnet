<?php
/********************************************************************

    delivery.php

    Filter Screen for Preselection

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2003-12-07
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-12-07
    Version:        1.0.0

    Copyright (c) 2003, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/order_state_constants.php";

check_access("can_view_delivery");

set_referer("delivery_by_supplier.php");
set_referer("delivery_by_forwarder.php");


/********************************************************************
    prepare all data needed
*********************************************************************/
// get user data
$user_data = get_user(user_id());
$user_roles = get_user_roles(user_id());


$countries = array();
$orders = array();
$ponumbers = array();
$shipmentcodes = array();
$suppliers = array();
$forwarders = array();
$rtos = array();
$rtcs = array();

$order_types = array();
$order_types[1] = "Projects";
$order_types[2] = "Catalogue Orders";

$filter = "";
if (has_access("has_access_to_all_delivery_data"))
{
    $sql = "select DISTINCT order_id, order_number, country_id, country_name, ".
           "order_retail_operator,  project_retail_coordinator, " .
           "concat(rtos.user_name, ' ', rtos.user_firstname) as rto, " .
           "concat(rtcs.user_name, ' ', rtcs.user_firstname) as rtc " .
           "from orders " .
		    "left join order_items on order_item_order = order_id " .
           "left join projects on project_order = order_id " .
           "left join countries on order_shop_address_country = country_id ".
           "left join users as rtos on rtos.user_id = order_retail_operator " .
           "left join users as rtcs on rtcs.user_id = project_retail_coordinator " .
           "where order_item_po_number is not null ".
                          "   and order_show_in_delivery = 1 " .
                          "   and order_item_po_number <> '' " .
                          "   and order_archive_date is null ".
                          "   and (order_item_not_in_budget is null or order_item_not_in_budget = 0) ".
		                  "    and (order_item_arrival is null or order_item_arrival = '0000-00-00') " .
                          "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
           " order by country_name";

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        if($row["country_name"])
		{
			$countries[$row["country_id"]] = $row["country_name"];
		}
        $orders[$row["order_id"]] = $row["order_number"];
        
		if($row["rto"])
		{
			$rtos[$row["order_retail_operator"]] = $row["rto"];
		}
        
		if($row["rtc"])
		{
			$rtcs[$row["project_retail_coordinator"]] = $row["rtc"];
		}
    } 
    
    
    $sql = "select distinct order_item_id, order_item_po_number, order_item_item, ".
           "order_item_shipment_code, " .
           "suppliers.address_id as sid, suppliers.address_company as scom, " .
           "forwarders.address_id as fid, forwarders.address_company as fcom, item_code " .
           "from order_items ".
           "left join orders on order_id = order_item_order ".
		   "left join items on item_id = order_item_item " .
           "left join addresses as suppliers on order_item_supplier_address = suppliers.address_id ".
           "left join addresses as forwarders on order_item_forwarder_address = forwarders.address_id ".
           "where order_item_po_number is not null ".
                          "   and order_show_in_delivery = 1 " .
                          "   and order_item_po_number <> '' " .
                          "   and order_archive_date is null ".
                          "   and (order_item_not_in_budget is null or order_item_not_in_budget = 0) ".
		                  "    and (order_item_arrival is null or order_item_arrival = '0000-00-00') " .
                          "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
           " order by order_item_po_number";

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        $ponumbers[$row["order_item_po_number"]] = $row["order_item_po_number"];
        
		if($row["order_item_shipment_code"])
		{
			$shipmentcodes[$row["order_item_shipment_code"]] = $row["order_item_shipment_code"];
		}
        $suppliers[$row["sid"]] = $row["scom"];
        
		if($row["fcom"])
		{
			$forwarders[$row["fid"]] = $row["fcom"];
		}
		$items[$row["order_item_item"]] = $row["item_code"];
    } 

     if(count($orders) > 0)
	{
		 asort($orders);    
		 asort($countries);
		 asort($shipmentcodes);
		 asort($ponumbers);
		 asort($suppliers);
		 asort($forwarders);
		 asort($rtos);
		 asort($rtcs);
		 asort($items);
	}
}
else
{
    // get all orders where the user is involved
    
	$condition = get_user_specific_order_list(user_id(), 1);
	$condition2 = get_user_specific_order_list(user_id(), 2);
	$orders = array();

	if($condition == "" and $condition2 == "")
	{
    
		$sql = "select distinct order_item_order, order_number, " .
			   "country_id, country_name, order_item_po_number, ".
			   "order_item_shipment_code, " .
			   "suppliers.address_id as sid, suppliers.address_company as scom, " .
			   "forwarders.address_id as fid, forwarders.address_company as fcom " .
			   "from order_items " .
			   "left join orders on order_item_order = order_id ".
			   "inner join project_costs on project_cost_order = order_id " .
			   "left join countries on order_shop_address_country = country_id ".
			   "left join projects on project_order = order_id ".
			   "left join addresses as suppliers on order_item_supplier_address = suppliers.address_id ".
			   "left join addresses as forwarders on order_item_forwarder_address = forwarders.address_id ".
			   "where order_item_po_number is not null ".
                          "   and order_show_in_delivery = 1 " .
                          "   and order_item_po_number <> '' " .
                          "   and order_archive_date is null ".
                          "   and (order_item_not_in_budget is null or order_item_not_in_budget = 0) ".
		                  "    and (order_item_arrival is null or order_item_arrival = '0000-00-00') " .
                          "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
			   "   and (order_item_supplier_address = " . $user_data["address"] . " " .
			   "   or order_item_forwarder_address = " . $user_data["address"] . " " .
			   "   or order_retail_operator = " . user_id() . " " .
			   "   or (order_client_address = " . $user_data["address"] . " and order_user = " . user_id() . ") " .
			   "   or project_retail_coordinator = " . user_id()  . " " .
			   "   or project_design_contractor = " . user_id()  . " " .
			   "   or project_design_supervisor = " . user_id()   . ") ";
	}
	elseif($condition != "" and $condition2 == "")
	{
		$sql = "select distinct order_item_order, order_number, " .
			   "country_id, country_name, order_item_po_number, ".
			   "order_item_shipment_code, " .
			   "suppliers.address_id as sid, suppliers.address_company as scom, " .
			   "forwarders.address_id as fid, forwarders.address_company as fcom " .
			   "from order_items " .
			   "left join orders on order_item_order = order_id ".
			   "inner join project_costs on project_cost_order = order_id " .
			   "left join countries on order_shop_address_country = country_id ".
			   "left join projects on project_order = order_id ".
			   "left join addresses as suppliers on order_item_supplier_address = suppliers.address_id ".
			   "left join addresses as forwarders on order_item_forwarder_address = forwarders.address_id ".
			   "where order_item_po_number is not null ".
                          "   and order_show_in_delivery = 1 " .
                          "   and order_item_po_number <> '' " .
                          "   and order_archive_date is null ".
                          "   and (order_item_not_in_budget is null or order_item_not_in_budget = 0) ".
		                  "    and (order_item_arrival is null or order_item_arrival = '0000-00-00') " .
                          "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
			  "   and (" . $condition . ")";
	}
	elseif($condition == "" and $condition2 != "")
	{
		$sql = "select distinct order_item_order, order_number, " .
			   "country_id, country_name, order_item_po_number, ".
			   "order_item_shipment_code, " .
			   "suppliers.address_id as sid, suppliers.address_company as scom, " .
			   "forwarders.address_id as fid, forwarders.address_company as fcom " .
			   "from order_items " .
			   "left join orders on order_item_order = order_id ".
			   "inner join project_costs on project_cost_order = order_id " .
			   "left join countries on order_shop_address_country = country_id ".
			   "left join projects on project_order = order_id ".
			   "left join addresses as suppliers on order_item_supplier_address = suppliers.address_id ".
			   "left join addresses as forwarders on order_item_forwarder_address = forwarders.address_id ".
			   "where order_item_po_number is not null ".
                          "   and order_show_in_delivery = 1 " .
                          "   and order_item_po_number <> '' " .
                          "   and order_archive_date is null ".
                          "   and (order_item_not_in_budget is null or order_item_not_in_budget = 0) ".
		                  "    and (order_item_arrival is null or order_item_arrival = '0000-00-00') " .
                          "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
			   "   and (" . $condition2 . ")";
	}
	else
	{
		$sql = "select distinct order_item_order, order_number, " .
			   "country_id, country_name, order_item_po_number, ".
			   "order_item_shipment_code, " .
			   "suppliers.address_id as sid, suppliers.address_company as scom, " .
			   "forwarders.address_id as fid, forwarders.address_company as fcom " .
			   "from order_items " .
			   "left join orders on order_item_order = order_id ".
			   "inner join project_costs on project_cost_order = order_id " .
			   "left join countries on order_shop_address_country = country_id ".
			   "left join projects on project_order = order_id ".
			   "left join addresses as suppliers on order_item_supplier_address = suppliers.address_id ".
			   "left join addresses as forwarders on order_item_forwarder_address = forwarders.address_id ".
			   "where order_item_po_number is not null ".
                          "   and order_show_in_delivery = 1 " .
                          "   and order_item_po_number <> '' " .
                          "   and order_archive_date is null ".
                          "   and (order_item_not_in_budget is null or order_item_not_in_budget = 0) ".
		                  "    and (order_item_arrival is null or order_item_arrival = '0000-00-00') " .
                          "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
			   "   and ((" . $condition . ") " .
			   "   or (" . $condition2 . "))";
	}


	if(has_access("has_access_to_retail_only")) {
		$sql .= " and project_cost_type in (1) ";
	}

	if(has_access("has_access_to_wholesale")) {
		$sql .= " and project_cost_type in (2, 6) ";
	}
    
    $res = mysql_query($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        $orders[$row["order_item_order"]] = $row["order_number"];
        $countries[$row["country_id"]] = $row["country_name"];
        $ponumbers[$row["order_item_po_number"]] = $row["order_item_po_number"];
        $shipmentcodes[$row["order_item_shipment_code"]] = $row["order_item_shipment_code"];

        $suppliers[$row["sid"]] = $row["scom"];
        $forwarders[$row["fid"]] = $row["fcom"];

    }


    asort($orders);    
    asort($countries);
    asort($shipmentcodes);
    asort($ponumbers);
    asort($suppliers);
    asort($forwarders);
    asort($rtos);
    asort($rtcs);

}
    


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("orders", "orders");

if(count($orders) > 0)
{
	$form->add_section("Enter nothing or specific criteria");

	$form->add_list("ordertype", "Type", $order_types);
	$form->add_list("ordernumber", "Order Number", $orders);
	$form->add_list("country", "Country", $countries);
	$form->add_list("ponumber", "P.O. Number", $ponumbers);
	$form->add_list("shipmentcode", "Shipment Code", $shipmentcodes);

	if (has_access("has_access_to_all_delivery_data"))
	{
		$form->add_list("supplier", "Supplier", $suppliers);
		$form->add_list("forwarder", "Forwarder", $forwarders);
		$form->add_list("rtc", "Project Manager", $rtcs);
		$form->add_list("rto", "Retail Operator", $rtos);
		$form->add_list("item", "Item Code", $items);
	}
	else
	{
		if(in_array(5, $user_roles) or in_array(29, $user_roles)) // supplier or warehouse
		{
			$form->add_list("forwarder", "Forwarder", $forwarders);
		}
		elseif(in_array(6, $user_roles)) // user is a forwarder
		{
			$form->add_list("supplier", "Supplier", $suppliers);
		}
		else
		{
			$form->add_list("supplier", "Supplier", $suppliers);
			$form->add_list("forwarder", "Forwarder", $forwarders);

		}
	}

	$form->add_button("show_data_s", "Show List by Supplier");
	$form->add_button("show_data_f", "Show List by Forwarder");
}
else
{
	$form->message("There are no Order or Project Items in Delivery right now.");
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if($form->button("show_data_s"))
{
    $link = "delivery_by_supplier.php";
    $link = $link . "?o=" . $form->value("ordernumber");
	$link = $link . "&t=" . $form->value("ordertype");
    $link = $link . "&c=" . $form->value("country");
    $link = $link . "&p=" . $form->value("ponumber");
    $link = $link . "&s=" . $form->value("shipmentcode");
    
    
    if (has_access("has_access_to_all_delivery_data"))
    {
        $link = $link . "&su=" . $form->value("supplier");
        $link = $link . "&fw=" . $form->value("forwarder");
        $link = $link . "&rto=" . $form->value("rto");
        $link = $link . "&rtc=" . $form->value("rtc");
		$link = $link . "&item=" . $form->value("item");
    }
    elseif(in_array(5, $user_roles) or in_array(29, $user_roles)) // supplier or warehouse
    {
        $link = $link . "&fw=" . $form->value("forwarder");
    }
    elseif(in_array(6, $user_roles)) // user is a forwarder
    {
        $link = $link . "&su=" . $form->value("supplier");

    }
    else
    {
        $link = $link . "&su=" . $form->value("supplier");
        $link = $link . "&fw=" . $form->value("forwarder");
        
    }
    
    redirect($link);
}

if($form->button("show_data_f"))
{
    $link = "delivery_by_forwarder.php";
    $link = $link . "?o=" . $form->value("ordernumber");
	$link = $link . "&t=" . $form->value("ordertype");
    $link = $link . "&c=" . $form->value("country");
    $link = $link . "&p=" . $form->value("ponumber");
    $link = $link . "&s=" . $form->value("shipmentcode");
    
    
    if(has_access("has_access_to_all_delivery_data"))
    {
        $link = $link . "&su=" . $form->value("supplier");
        $link = $link . "&fw=" . $form->value("forwarder");
        $link = $link . "&rto=" . $form->value("rto");
        $link = $link . "&rtc=" . $form->value("rtc");
		$link = $link . "&item=" . $form->value("item");
    }
    elseif(in_array(5, $user_roles) or in_array(29, $user_roles)) // supplier or warehouse
    {
        $link = $link . "&fw=" . $form->value("forwarder");
    }
    elseif(in_array(6, $user_roles)) // user is a forwarder
    {
        $link = $link . "&su=" . $form->value("supplier");
    }
    else
    {
        $link = $link . "&su=" . $form->value("supplier");
        $link = $link . "&fw=" . $form->value("forwarder");
    }

    redirect($link);
}

$page = new Page("delivery");

$page->header();
$page->title("Delivery Data Search Criteria");
$form->render();


?>

	<script type="text/javascript">
		
		document.onkeydown = process_key;
		
		function process_key(e)
		{
		  if( !e ) 
		  {
			if( window.event ) 
			{
			  e = window.event;
			} 
			else 
			{
			  return;
			}
		  }

		  if(e.keyCode==13)
		  {
			  button('show_data_s');
		  }
		}
	</script>

	<?php

$page->footer();

?>