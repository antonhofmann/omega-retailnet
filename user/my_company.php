<?php
/********************************************************************


    my_company.php

    Shows all relevant data of the user's company

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2013-04-04
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-04-04
    Version:        1.1.2


    Copyright (c) 2013, Swatch AG, All Rights Reserved.


*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access();

$user = get_user(user_id());
$address = get_address($user["address"]);

$page_title = $address["company"];

/********************************************************************
    build form
*********************************************************************/
$form = new Form("tasks", "task");

//$form->add_section("Company");
//$form->add_label("compnay", "Company", 0, $address["company"]);



$form->populate();

/*
			
			$address["id"] = $row["address_id"];
			$address["shortcut"] = $row["address_shortcut"];
            $address["company"] = $row["address_company"];
            $address["company2"] = $row["address_company2"];
            $address["address"] = $row["address_address"];
            $address["address2"] = $row["address_address2"];
            $address["zip"] = $row["address_zip"];
            $address["place"] = $row["address_place"];
			$address["place_id"] = $row["address_place_id"];
			$address["place_province"] = $row["place_province"];
            $address["country"] = $row["address_country"];
            $address["country_name"] = "";
            $address["currency"] = $row["address_currency"];
            $address["phone"] = $row["address_phone"];
            $address["fax"] = $row["address_fax"];
            $address["email"] = $row["address_email"];
            $address["contact"] = $row["address_contact"];
            $address["client_type"] = $row["address_client_type"];
			$address["contact_name"] = $row["address_contact_name"];
			$address["website"] = $row["address_website"];
			$address["invoice_recipient"] = $row["address_invoice_recipient"];

            $sql = "select country_id, country_name, country_region ".
                   "from countries ".
                   "where country_id = " . dbquote($address["country"]);



            $res = mysql_query($sql);
            if ($res)
            {
				$row = mysql_fetch_assoc($res);
                $address["country_name"] = $row['country_name'];
				$address["country_region"] = $row['country_region'];
            }


			$sql = "select province_id, province_canton ".
                   "from provinces ".
                   "where province_id = " . dbquote($address["place_province"]);



            $res = mysql_query($sql);
            if ($res)
            {
				$row = mysql_fetch_assoc($res);
                $address["province_name"] = $row['province_canton'];
            }
			
			
			$user["firstname"] = $row["user_firstname"];
            $user["name"] = $row["user_name"];
            $user["contact"] = $row["user_name"] . " " . $row["user_firstname"];
            $user["phone"] = $row["user_phone"];
            $user["fax"] = $row["user_fax"];
            $user["email"] = $row["user_email"];
            $user["cc"] = $row["user_email_cc"];
            $user["deputy"] = $row["user_email_deputy"];
            $user["address"] = $row["user_address"];
			$user["country"] = $row["address_country"];
*/


/********************************************************************
    build list of active users
*********************************************************************/
$sql = "select *, concat(user_name, ' ', user_firstname) as user_fullname from users";

$roles = array();


$res = mysql_query($sql . " where user_active = 1 and user_address = " . $user["address"]);
while ($row = mysql_fetch_assoc($res))
{
	$user_roles = array();
	$sql_r = "select role_name ".
			 "from user_roles ".
			 "left join roles on user_role_role = role_id ".
			 "where user_role_user = " . $row["user_id"] . 
			 " order by role_name";

	$res_r = mysql_query($sql_r) or dberror($sql_r);
	while ($row_r = mysql_fetch_assoc($res_r))
	{
		$user_roles[] = $row_r["role_name"];
	}

	$roles[$row["user_id"]] = implode(', ', $user_roles); 
}

$list = new ListView($sql);
$list->set_entity("messages");
$list->set_title("Active Users");
$list->set_filter("user_active = 1 and user_address = " . $user["address"]);
$list->set_order("user_name, user_firstname");



$list->add_column("user_fullname", "User");
$list->add_column("user_email", "Email");
$list->add_column("user_phone", "Phone");
//$list->add_column("user_email_deputy", "Deputy Email");
$list->add_text_column("roles", "Roles", 0, $roles);
$list->populate();




$page = new Page("welcome");
$page->header();
$page->title($page_title);



//$form->render();
$list->render();
$page->footer();


?>