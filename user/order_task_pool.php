<?php
/********************************************************************

    order_task_pool.php

    show all open tasks

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-05-31
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-31
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/order_functions.php";

check_access("has_accessto_taskpool_in_orders");


/********************************************************************
    delete task data, when delete was clicked
*********************************************************************/
if (param("tid"))
{
    $sql = "select * from tasks where task_id = " . param("tid");
	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
	{
	
		$text = "User has deleted task for project state " . $row["task_order_state"] . " of order " . $row["task_order"] . " for user " . $row["task_user"];
		$sql = 'insert into user_tracking (user_tracking_user, user_tracking_track, date_created, user_created) VALUES (' .
			   user_id() . ', "' . $text . '", "' . date("Y-m-d H:i:s") . '", "' . user_login() . '")';

		 mysql_query($sql) or dberror($sql);
		
		$sql = "delete from tasks where task_id = " .  param("tid");
		mysql_query($sql) or dberror($sql);
	}
}

/********************************************************************
    prepare all data needed
*********************************************************************/
// read order details
$order = get_order(param("oid"));

// get company's address
$client_address = get_address($order["order_client_address"]);


/********************************************************************
   get order's pending tasks
*********************************************************************/
$sql = "select task_id, order_state_code, order_state_name, task_text, " .
      "CONCAT(users.user_name, ' ', users.user_firstname) as userto, " .
      "DATE_FORMAT(tasks.date_created,'%d.%m.%y') as date_of_task, " .
      "'remove' as task_action " .
      "from tasks " .
      "left join users on users.user_id = task_user " .
      "left join order_states on order_state_id = task_order_state " .
      "left join user_roles on user_role_user = user_id ";
            
$filter =  "task_done_date is null " .
           "  and task_order = " . param("oid");

/*
           "  and (user_role_role = 2 or user_role_role = 3 or user_role_role = 4)";
*/

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("orders", "task_pool", 640);
$form->add_hidden("oid", param("oid"));

$form->add_section("Order");
if(!$order["order_retail_operator"])
{
    $form->error("No Retail Operator has been assigned!");
}

require_once "include/order_head_small.php";


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Create List of pending tasks for this order
*********************************************************************/ 
$tasklist = new ListView($sql, LIST_HAS_HEADER);

$tasklist->set_title("Pending Tasks for this Order");
$tasklist->set_entity("task");
$tasklist->set_filter($filter);
$tasklist->set_order("userto");
$tasklist->set_group("userto");

$link = "order_task_pool_edit.php?&oid=" . param('oid');
$tasklist->add_column("order_state_name", "From Action", $link,"", "", COLUMN_NO_WRAP);
$tasklist->add_column("order_state_code", "Step");
$tasklist->add_column("date_of_task", "Date");
$tasklist->add_column("task_text", "Task");    

$link = "order_task_pool.php?&oid=" . param('oid') . "&tid={task_id}"; 
$tasklist->add_column("task_action", "", $link, "","", COLUMN_NO_WRAP);


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$tasklist->populate();
$tasklist->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("Task Pool");
$form->render();

$tasklist->render();

$page->footer();

?>