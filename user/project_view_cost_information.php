<?php
/********************************************************************

    project_view_cost_information.php

    View projects's itemlist in client's prices and currency

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2012-06-22
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2012-06-22
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
//require_once "include/save_functions.php";


check_access("can_view_project_invoice_information");

register_param("pid");


/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());


// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

// create sql for oder items
$sql_order_items = "select order_item_id, order_item_text, order_item_quantity, ".
                   "    order_item_supplier_price, order_item_po_number, " .
                   "    concat(RIGHT(order_item_ordered, 2), '.', MONTH(order_item_ordered), '.',  YEAR(order_item_ordered)) as order_date, ".
                   "    if(order_item_item <>'', item_code, item_type_name) as item_code, " .
                   "    TRUNCATE(order_item_supplier_price * order_item_quantity, 2) as total_price, ".
                   "    addresses.address_company as supplier_company, ".
                   "    currency_symbol, order_item_supplier_address, ".
                   "    concat(addresses.address_company, ', ', currency_symbol) as group_head,  " .
				   "    item_stock_property_of_swatch ". 
                   "from order_items ".
				   "left join orders on order_item_order = order_id ".
                   "left join items on order_item_item = item_id ".
                   "left join item_types on order_item_type = item_type_id ".
                   "left join addresses on order_item_supplier_address = addresses.address_id ".
                   "left join currencies on order_item_supplier_currency = currency_id ";


// build group_totals of suppliers
$group_totals = get_group_total_of_suppliers($project["project_order"], "order_item_supplier_price");





if (has_access("has_access_to_all_supplier_data_in_projects"))
{
    $list_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
                      "   and (order_item_type <= " . ITEM_TYPE_SPECIAL .
		              "   or order_item_type = " . ITEM_TYPE_SERVICES . ") " .
                      "   and order_item_order = " . $project["project_order"];
}
else
{
    $list_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
                      "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
                      "   and order_item_order = " . $project["project_order"] .
                      "   and (order_item_supplier_address = " . $user_data["address"] . 
		              "   or order_client_address = " . $user_data["address"] . ")";
}


$property = array();

$res = mysql_query($sql_order_items . " where " . $list_filter) or dberror($sql_order_items . " where " . $list_filter);


while ($row = mysql_fetch_assoc($res))
{
    if($row["item_stock_property_of_swatch"] == 1)
	{
		$property[$row["order_item_id"]] = "/pictures/omega.gif";
	}
}



//get invoice addresses
$invoice_adresses = array();
$suppliers = array();
$excluded_suppliers = array();

$sql_suppliers = "select DISTINCT order_item_supplier_address, address_id, address_company " . 
				 "from order_items " . 
				 "left join addresses on order_item_supplier_address = address_id ".
				 "left join orders on order_item_order = order_id ".
				 " where " . $list_filter;

$res = mysql_query($sql_suppliers) or dberror($sql_suppliers);


while ($row = mysql_fetch_assoc($res))
{
	
	$sql = "select supplier_client_invoice_id ". 
		   "from supplier_client_invoices " . 
		   "where supplier_client_invoice_supplier = " . $row["order_item_supplier_address"] .
		   " and supplier_client_invoice_client = " . $client_address["id"]  . 
		   " and supplier_client_invoice_startdate <= " . dbquote(date("Y-m-d")) . 
			   " and supplier_client_invoice_enddate >= " . dbquote(date("Y-m-d"));
	
	$res_s = mysql_query($sql) or dberror($sql);
	if ($row_s = mysql_fetch_assoc($res_s))
	{
		$suppliers[$row["address_id"]] = $row["address_company"]; 
		//get invoice_address
		$sql_i = "select invoice_address_company, invoice_address_company2, order_direct_invoice_address_id, " . 
			     "invoice_address_address, invoice_address_address2, invoice_address_zip, " . 
			     "place_name, country_name, invoice_address_phone, invoice_address_fax, " .
			     "invoice_address_email, invoice_address_contact_name " . 
			     "from orders " .
			     "inner join invoice_addresses on invoice_address_id = order_direct_invoice_address_id " . 
			     "inner join countries on country_id = invoice_address_country_id ". 
			     "inner join places on place_id = invoice_address_place_id " . 
			     "where order_id = " . $project["project_order"];
		
		
		$res_i = mysql_query($sql_i) or dberror($sql_i);
		if ($row_i = mysql_fetch_assoc($res_i))
		{
			$iaddress = $row_i["invoice_address_company"];

			if($row_i["invoice_address_company2"])
			{
				$iaddress .= ", " . $row_i["invoice_address_company2"];
			}

			if($row_i["invoice_address_address"])
			{
				$iaddress .= ", " . $row_i["invoice_address_address"];
			}

			if($row_i["invoice_address_address2"])
			{
				$iaddress .= ", " . $row_i["invoice_address_address2"];
			}

			$iaddress .= ", " . $row_i["invoice_address_zip"] . " " . $row_i["place_name"];
			$iaddress .= ", " . $row_i["country_name"];
			
			
			$invoice_adresses [$row["address_id"]] = $iaddress;
		
		}
		else
		{
			$suppliers[$row["address_id"]] = $row["address_company"]; 
			$iaddress = $client_address["company"];

			if($client_address["company2"])
			{
				$iaddress .= ", " . $client_address["company2"];
			}

			if($client_address["address"])
			{
				$iaddress .= ", " . $client_address["address"];
			}

			if($client_address["address2"])
			{
				$iaddress .= ", " . $client_address["address2"];
			}

			$iaddress .= ", " . $client_address["zip"] . " " . $client_address["place"];
			$iaddress .= ", " . $client_address["country_name"];

			$invoice_adresses [$row["address_id"]] = $iaddress;
		}
	}
	else
	{
		$excluded_suppliers[] = $row["address_id"];
	}
	
}

asort($suppliers);
asort($invoice_adresses);

$tmp_filter = "";
if(count($excluded_suppliers) > 0)
{
	$tmp_filter = " and order_item_supplier_address NOT IN (" . implode (',' , $excluded_suppliers) . ") ";
}
$list_filter .= $tmp_filter;

/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

$form->add_hidden("pid", param("pid"));

require_once "include/project_head_small.php";


$form->add_section("Suppliers send Invoices directly to the following Address");
foreach($suppliers as $id=>$companyname)
{
	$form->add_label("S" . $id, $companyname, RENDER_HTML, $invoice_adresses[$id]);
}

$form->add_comment("The listed costs must be considered as a non-binding calculation aid;<br />
Not included in the listed costs are handling fees, costs in relation to export and import documents, duties, taxes (incl. VAT), costs related to special packaging requirements, costs for insurance coverage (transport, theft, damage), etc.;<br />
The payment terms will be specified on the invoice document;<br />
The order from you to the supplier must be issued as per the instructions you receive from the supplier. The contractual relationship will be between you and the supplier.<br />");


$link = "project_view_cost_information_pdf.php?pid=" . param("pid");
$link = "javascript:popup('". $link . "', 800, 600)";
$form->add_button("print", "Print PDF", $link);


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Create Item List
*********************************************************************/ 
$list = new ListView($sql_order_items, LIST_HAS_SEPARATOR | LIST_HAS_HEADER);

$list->set_title("Items Being Directly Invoiced");
$list->set_entity("order_item");
$list->set_group("group_head");
$list->set_filter($list_filter);


$list->set_order("order_item_type, item_code");

$list->add_column("item_code", "Item Code", "", "", "", COLUMN_BREAK);
$list->add_image_column("property", "", 0, $property);
$list->add_column("order_item_text", "Name", "", "", "", COLUMN_BREAK);
$list->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_BREAK);
$list->add_column("order_date", "Ordered", "", "", "", COLUMN_BREAK);
$list->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_BREAK);
$list->add_column("order_item_supplier_price", "Price", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_BREAK);
$list->add_column("total_price", "Total", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_BREAK );


// set group totals
if(count($group_totals) > 0)
{
	foreach ($group_totals["currency"] as $key=>$value)
	{
		$list->set_group_footer("order_item_supplier_price", $key , $value);
	}
	foreach ($group_totals["total"] as $key=>$value)
	{
		$list->set_group_footer("total_price", $key , $value);
	}
}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("View Cost Information");
$form->render();
$list->render();


$page->footer();

?>