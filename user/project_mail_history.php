<?php
/********************************************************************

    project_mail_history.php

    View the history of mails of a project

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-11-20
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-16
    Version:        1.1.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";


check_access("can_view_history_in_projects");


/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));


// get company's address
$client_address = get_address($project["order_client_address"]);

// get data for history list
$sql_order_mails = "select distinct concat(order_mail_text, order_mails.date_created) as mail_identifier, " .
                   "order_mail_text, " .
                   "concat(order_mails.date_created, ':',order_revisioned) as date_created, ".
                   "order_state_code,  order_revisioned " .
                   "from order_mails " .
                   "left join order_states on order_mail_order_state = order_state_id ";


$list_filter = "order_mail_order = " . $project["project_order"];

//get all recipients of the same mail
$sql = "select DISTINCT  concat(order_mail_text, order_mails.date_created) as mail_identifier, " .
       "order_mail_text, concat(order_mails.date_created,':',order_revisioned) as date_created, ".
       "    concat(users.user_name, ' ', users.user_firstname) as sender, " .
       "    concat(users1.user_name, ' ', users1.user_firstname) as reciepient " .
       "from order_mails ".
       "left join users on order_mail_from_user = users.user_id " .
       "left join users as users1 on order_mail_user = users1.user_id ";


//reduce mails to several reciepinets to one line
$users = array();

$sql = $sql . " where " . $list_filter;

$res = mysql_query($sql) or dberror($sql);


while($row = mysql_fetch_assoc($res))
{
    
	if(isset($users[$row["mail_identifier"]]))
	{
		$users[$row["mail_identifier"]] = $users[$row["mail_identifier"]] . "\n" .  $row["reciepient"];
	}
	else
	{
		$users[$row["mail_identifier"]] = "<span class='highlite_value'>" . $row["sender"] . "</span>\n" .  $row["reciepient"];
	}
}


/********************************************************************
    Create Form
*********************************************************************/ 


$form = new Form("projects", "history");


$form->add_section("Project");
$form->add_hidden("pid", param("pid"));


require_once "include/project_head_small.php";




/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Create History of steps
*********************************************************************/ 
$list = new ListView($sql_order_mails, LIST_HAS_SEPARATOR | LIST_HAS_HEADER);


$list->set_title("History: Mails Sent");
$list->set_entity("order_mail");
$list->set_filter($list_filter);
$list->set_order("order_mails.date_created DESC");


$list->add_column("date_created", "Date", "", "", "", COLUMN_NO_WRAP);
$list->add_column("order_state_code", "Step");
$list->add_text_column("users", "Sender\nRecepients", COLUMN_NO_WRAP | COLUMN_UNDERSTAND_HTML | COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $users);
$list->add_column("order_mail_text", "Mailtext", "", "", "", COLUMN_BREAK);





/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 


$list->populate();
$list->process();




/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");


require "include/project_page_actions.php";


$page->header();
$page->title("Mail History");
$form->render();


$list->render();


$page->footer();


?>