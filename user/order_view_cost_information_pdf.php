<?php
/********************************************************************

    order_view_cost_information_pdf.php

    Print Project Invoice Information

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2012-06-22
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2008-10-18
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";
check_access("can_view_order_invoice_information");


/********************************************************************
    prepare all data needed
*********************************************************************/

if (param("oid"))
{
	// get user_data
	$user_data = get_user(user_id());
	
	// read order details
	$order = get_order(param("oid"));

		
	//Client Data
	$posclient = "";
	$sql_i = "select * from addresses " . 
		     "left join countries on country_id = address_country " .
	         "where address_id = " . $order["order_client_address"];

	$res_i = mysql_query($sql_i) or dberror($sql_i);
	if ($row_i = mysql_fetch_assoc($res_i))
	{
		$posclient = $row_i["address_company"];
		if($row_i["address_address"])
		{
			$posclient .= ", " . $row_i["address_address"];
		}
		if($row_i["address_zip"])
		{
			$posclient .= ", " . $row_i["address_zip"] . " " . $row_i["address_place"];
		}
		else
		{
			$posclient .= ", " . $row_i["address_place"];
		}
		if($row_i["country_name"])
		{
			$posclient .= ", " . $row_i["country_name"];
		}
	}



	
	
	global $page_title;
	$page_title = "Cost Information for Order: " . $order["order_number"] . "*";

	// get company's address
	$client_address = get_address($order["order_client_address"]);

	// create sql for oder items
	$sql_order_items = "select order_item_id, order_item_text, order_item_quantity, ".
					   "    order_item_supplier_price, order_item_po_number, " .
					   "    concat(RIGHT(order_item_ordered, 2), '.', " . 
		               "    MONTH(order_item_ordered), '.',  YEAR(order_item_ordered)) as order_date, ".
					   "    if(order_item_item <>'', item_code, item_type_name) as item_code, " .
					   "    TRUNCATE(order_item_supplier_price * order_item_quantity, 2) as total_price, ".
					   "    addresses.address_company as supplier_company, ".
					   "    currency_symbol, order_item_supplier_address, ".
					   "    concat(addresses.address_company, ', ', currency_symbol) as group_head " .
					   "from order_items ".
					   "left join items on order_item_item = item_id ".
		               "left join orders on order_item_order = order_id ".
					   "left join item_types on order_item_type = item_type_id ".
					   "left join addresses on order_item_supplier_address = addresses.address_id ".
					   "left join currencies on order_item_supplier_currency = currency_id ";



	if (has_access("has_access_to_all_supplier_data_in_orders"))
	{
		$list_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) and ".
						  "order_item_type <= " . ITEM_TYPE_SPECIAL .
						  "   and order_item_order = " . param('oid');
	}
	else
	{
		$list_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) and ".          
                      "order_item_type <= " . ITEM_TYPE_SPECIAL .
                      "   and order_item_order = " . param('oid') .
                      "   and (order_item_supplier_address = " . $user_data["address"] . 
		              "   or order_client_address = " . $user_data["address"] . ")";
	}


	
	//get invoice addresses
	$invoice_adresses = array();
	$suppliers = array();
	$excluded_suppliers = array();

	$sql_suppliers = "select DISTINCT order_item_supplier_address, address_id, address_company " . 
					 "from order_items " . 
					 "left join addresses on order_item_supplier_address = address_id ".
		             "left join orders on order_item_order = order_id ".
					 " where " . $list_filter;

	$res = mysql_query($sql_suppliers) or dberror($sql_suppliers);


	while ($row = mysql_fetch_assoc($res))
	{
		$suppliers[$row["address_id"]] = $row["address_company"]; 
		$sql = "select supplier_client_invoice_id ". 
			   "from supplier_client_invoices " . 
			   "where supplier_client_invoice_supplier = " . $row["order_item_supplier_address"] .
			   " and supplier_client_invoice_client = " . $client_address["id"]  . 
		   " and supplier_client_invoice_startdate <= " . dbquote(date("Y-m-d")) . 
			   " and supplier_client_invoice_enddate >= " . dbquote(date("Y-m-d"));
		
		$res_s = mysql_query($sql) or dberror($sql);
		if ($row_s = mysql_fetch_assoc($res_s))
		{
			//get invoice_address
			$sql_i = "select invoice_address_company, invoice_address_company2, order_direct_invoice_address_id, " . 
					 "invoice_address_address, invoice_address_address2, invoice_address_zip, " . 
					 "place_name, country_name, invoice_address_phone, invoice_address_fax, " .
					 "invoice_address_email, invoice_address_contact_name " . 
					 "from orders " .
					 "inner join invoice_addresses on invoice_address_id = order_direct_invoice_address_id " . 
					 "inner join countries on country_id = invoice_address_country_id ". 
					 "inner join places on place_id = invoice_address_place_id " . 
					 "where order_id = " . param("oid");
			
			
			$res_i = mysql_query($sql_i) or dberror($sql_i);
			if ($row_i = mysql_fetch_assoc($res_i))
			{
				$iaddress = $row_i["invoice_address_company"];

				if($row_i["invoice_address_company2"])
				{
					$iaddress .= ", " . $row_i["invoice_address_company2"];
				}

				if($row_i["invoice_address_address"])
				{
					$iaddress .= ", " . $row_i["invoice_address_address"];
				}

				if($row_i["invoice_address_address2"])
				{
					$iaddress .= ", " . $row_i["invoice_address_address2"];
				}

				$iaddress .= ", " . $row_i["invoice_address_zip"] . " " . $row_i["place_name"];
				$iaddress .= ", " . $row_i["country_name"];
				
				
				$invoice_adresses [$row["address_id"]] = $iaddress;
			
			}
			else
			{
				$iaddress = $client_address["company"];

				if($client_address["company2"])
				{
					$iaddress .= ", " . $client_address["company2"];
				}

				if($client_address["address"])
				{
					$iaddress .= ", " . $client_address["address"];
				}

				if($client_address["address2"])
				{
					$iaddress .= ", " . $client_address["address2"];
				}

				$iaddress .= ", " . $client_address["zip"] . " " . $client_address["place"];
				$iaddress .= ", " . $client_address["country_name"];

				$invoice_adresses [$row["address_id"]] = $iaddress;
			}
		}
		else
		{
			$excluded_suppliers[] = $row["address_id"];
		}
	}

	asort($suppliers);
	asort($invoice_adresses);

	$tmp_filter = "";
	if(count($excluded_suppliers) > 0)
	{
		$tmp_filter = " and order_item_supplier_address NOT IN (" . implode (',' , $excluded_suppliers) . ") ";
	}
	$list_filter .= $tmp_filter;



	

	/********************************************************************
		prepare pdf
	*********************************************************************/

	require_once('../include/tcpdf/config/lang/eng.php');
	require_once('../include/tcpdf/tcpdf.php');


	class MYPDF extends TCPDF
	{
		//Page header
		function Header()
		{
			global $page_title;
			//Logo
			$this->Image('../pictures/omega_logo.jpg',10,8,33);
			//arialn bold 15
			$this->SetFont('arialn','B',12);
			//Move to the right
			$this->Cell(80);
			//Title
			$this->Cell(0,33,$page_title,0,0,'R');
			//Line break
			$this->Ln(20);

		}

		//Page footer
		function Footer()
		{
			//Position at 1.5 cm from bottom
			$this->SetY(-15);
			//arialn italic 8
			$this->SetFont('arialn','I',8);
			//Page number
			$this->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$this->PageNo().'/'.$this->getAliasNbPages(),0,0,'R');
		}

	}

	//Instanciation of inherited class
	$pdf = new MYPDF("L", "mm", "A4", true, 'UTF-8', false);
	$pdf->SetMargins(10, 23, 10);

	$pdf->Open();
	
	$pdf->SetFillColor(220, 220, 220); 
	$pdf->AddFont('arialn');
	$pdf->AddFont('arialn', 'B');

	$pdf->AddPage();
	$new_page = 0;

	/*
	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(277,7,$posname,1, 0, 'L', 1);
	$pdf->Ln();
	$pdf->SetFont('arialn','',10);
	$pdf->Cell(277,7,$posaddress,1, 0, 'L', 0);
	$pdf->Ln();
	$pdf->Ln();
	*/	
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Client",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(250, 5,$posclient,1, 0, 'L', 0);
	$pdf->Ln();


	$group_head = "";
	$group_total = 0;
	$first_group = true;
	$currency_symbol = "";
	$invoice_address = "";
	$sql = $sql_order_items . " where " . $list_filter . " order by group_head";
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		
		if($row["group_head"] != $group_head)
		{
			$pdf->Ln();

			if($group_head != "")
			{
				$pdf->SetFont('arialn','B',8);
				$pdf->Cell(247, 5,"Total to be directly invoiced to: " . $invoice_address ,1, 0, 'L', 0);
				$pdf->Cell(30, 5, $currency_symbol . " " .  number_format($group_total, 2,".", "'" ),1, 0, 'R', 0);
				$pdf->Ln();
				$pdf->Ln();
				$group_total = 0;

			}


			$group_head = $row["group_head"];

			$pdf->SetFont('arialn','B',10);
			$pdf->Cell(277,7,$row["group_head"],1, 0, 'L', 1);
			$pdf->Ln();

			$pdf->Cell(30, 5,"Item Code",1, 0, 'L', 0);
			$pdf->Cell(140, 5,"Name",1, 0, 'L', 0);
			$pdf->Cell(27, 5,"P.O. Number",1, 0, 'L', 0);
			$pdf->Cell(20, 5,"Order Date",1, 0, 'L', 0);
			$pdf->Cell(15, 5,"Quantity",1, 0, 'R', 0);
			$pdf->Cell(15, 5,"Price",1, 0, 'R', 0);
			$pdf->Cell(30, 5,"Total",1, 0, 'R', 0);

			
			
		}

		$pdf->Ln();
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(30, 5,$row["item_code"],1, 0, 'L', 0);
		$pdf->Cell(140, 5,$row["order_item_text"],1, 0, 'L', 0);
		$pdf->Cell(27, 5,$row["order_item_po_number"],1, 0, 'L', 0);
		$pdf->Cell(20, 5,$row["order_date"],1, 0, 'L', 0);
		$pdf->Cell(15, 5,$row["order_item_quantity"],1, 0, 'R', 0);
		$pdf->Cell(15, 5,$row["order_item_supplier_price"],1, 0, 'R', 0);
		$pdf->Cell(30, 5,$row["total_price"],1, 0, 'R', 0);

		$group_total = $group_total + $row["total_price"];
		$currency_symbol = $row["currency_symbol"];


		$invoice_address = $invoice_adresses [$row["order_item_supplier_address"]];
	}
	
	if($group_total)
	{
		$pdf->Ln();
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(247, 5,"Total to be directly invoiced to: " . $invoice_address ,1, 0, 'L', 0);
		$pdf->Cell(30, 5,$currency_symbol . " " . number_format($group_total, 2,".", "'" ),1, 0, 'R', 0);
	}
	

	$pdf->Ln();
	$pdf->Ln();

	$pdf->SetFont('arialn','',8);

	$conditions = "The above listed costs must be considered as a non-binding calculation aid;" . "\n" .
"Not included in the above listed costs are handling fees, costs in relation to export and import documents, duties, taxes (incl. VAT), costs related to special packaging requirements, costs for insurance coverage (transport, theft, damage), etc.;" .  "\n" .
"The payment terms will be specified on the invoice document;" .  "\n" .
"The order from you to the supplier must be issued as per the instructions you receive from the supplier. The contractual relationship will be between you and the supplier.";

	$pdf->MultiCell(277,5,$conditions,0, 'L');
	

	// write pdf
	$pdf->Output();

}

?>