<?php
/********************************************************************

    project_costs_bid_comparison_pdf_detail.php

    Print Offer details for Offer Comparisikon for Local Construction Work

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-01-09
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2014, OMEGA SA, All Rights Reserved.

*********************************************************************/


	//print column headers
	$pdf->SetFont('arialn','B',9);

	$y = $pdf->GetY();

	$pdf->SetFillColor(220, 220, 220);
	$pdf->Cell(64,5, "Cost Group", 1, 0, 'L', 1);
	$pdf->Ln();
	
	//print cost group- and subgroup names
	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(64,5, $cost_group_name, 1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','',8);

	if(array_key_exists($group_id, $cost_subgroups) and count($cost_subgroups[$group_id]) > 0)
	{
		foreach($cost_subgroups[$group_id] as $subgroup_id=>$subgroup_text)
		{
				
			$pdf->Cell(64,4, "   " . $subgroup_text, 1, 0, 'L', 0);
			$pdf->Ln();
		}
	}
	
	
	$pdf->Ln();
	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(64,5, "Group Total in Local Currency", 1, 0, 'L', 0);
	
	$pdf->Ln();
	$pdf->Cell(64,5, "Group Total in CHF", 1, 0, 'L', 0);

	$pdf->Ln();
	$pdf->Ln();
	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(64,5, "Remarks", 0, 0, 'L', 0);

	$pdf->SetY($y);
	$pdf->Cell(64,5,"");

	foreach($tmp_offers as $bid_id=>$company_name)
	{
		if(array_key_exists($bid_id, $tmp_offer_group_totals) and array_key_exists($group_id, $tmp_offer_group_totals[$bid_id]))
		{
			if(in_array($bid_id, $offers_in_budget))
			{
				$pdf->Cell(53,5, substr($company_name . " (selected)", 0,30), 1, 0, 'L', 1);
			}
			else
			{
				$pdf->Cell(53,5, substr($company_name, 0,30), 1, 0, 'L', 1);
			}
		}
	}

	$pdf->Ln();
	$pdf->Cell(64,5,"");

	
	$bid_number = 1;
	$base_amount = 0;
	foreach($tmp_offers as $bid_id=>$company_name)
	{
		if(array_key_exists($bid_id, $tmp_offer_group_totals) and array_key_exists($group_id, $tmp_offer_group_totals[$bid_id]))
		{
			$totals = array();
			if(isset($tmp_offer_group_totals[$bid_id]))
			{
				$totals = $tmp_offer_group_totals[$bid_id];
				if(array_key_exists($group_id, $totals))
				{
					
					
					if($bid_number == 1)
					{
						$pdf->Cell(29,5, number_format($totals[$group_id],2), 1, 0, 'R', 0);
						$pdf->Cell(12,5, "share", 1, 0, 'R', 0);
						$pdf->Cell(12,5, "base", 1, 0, 'R', 0);
						$base_amount = $totals[$group_id];
					}
					else
					{
						$pdf->Cell(29,5, number_format($totals[$group_id],2), 1, 0, 'R', 0);
						
						if($base_amount != 0)
						{
							$over = round(100*($totals[$group_id]/$base_amount), 2) . "%";
							$delta = 100*round(($totals[$group_id]/$base_amount) - 1, 2) . "%";
						}
						else
						{
							$delta = "";
							$over = "";
						}

						$share = "";

						
						
						$pdf->Cell(12,5, "share", 1, 0, 'R', 0);
						$pdf->Cell(12,5, "delta", 1, 0, 'R', 0);
					}
				}
				else
				{
					$pdf->Cell(29,5, number_format(0,2), 1, 0, 'R', 0);
					$pdf->Cell(12,5, "", 1, 0, 'R', 0);
					$pdf->Cell(12,5, "", 1, 0, 'R', 0);
				}
			}
			else
			{
				$pdf->Cell(29,5, "", 1, 0, 'R', 0);
				$pdf->Cell(12,5, "", 1, 0, 'R', 0);
				$pdf->Cell(12,5, "", 1, 0, 'R', 0);
			}

			$bid_number++;
		}
		
	}

	
	$pdf->SetFont('arialn','',8);
	$pdf->Ln();
	
	
	//print sub groups
	
	if(array_key_exists($group_id, $cost_subgroups) and count($cost_subgroups[$group_id]) > 0)
	{
		foreach($cost_subgroups[$group_id] as $subgroup_id=>$subgroup_text)
		{
			$pdf->Cell(64,5,"");
			$bid_number = 1;
			$base_amount = 0;
			foreach($tmp_offers as $bid_id=>$company_name)
			{
				$totals = $tmp_offer_group_totals[$bid_id];

				if(array_key_exists($bid_id, $tmp_offer_group_totals) and array_key_exists($group_id, $tmp_offer_group_totals[$bid_id]))
				{
					$sub_totals = array();
					if(isset($tmp_offer_subgroup_totals[$bid_id]))
					{
						$sub_totals = $tmp_offer_subgroup_totals[$bid_id][$group_id];
						if(array_key_exists($subgroup_id, $sub_totals))
						{
							$share = "";
							if(array_key_exists($group_id, $totals) and $totals[$group_id] > 0)
							{
								$share = number_format(100*(round($sub_totals[$subgroup_id]/$totals[$group_id], 2)), 2) . "%";
							}

							if($bid_number == 1)
							{
								$pdf->Cell(29,4, number_format($sub_totals[$subgroup_id],2), 1, 0, 'R', 0);
								$pdf->Cell(12,4, $share, 1, 0, 'R', 0);
								$pdf->Cell(12,4, "100%", 1, 0, 'R', 0);
								$base_amount = $sub_totals[$subgroup_id];
							}
							else
							{
								if($base_amount != 0)
								{
									$over = round(100*($sub_totals[$subgroup_id]/$base_amount), 2) . "%";
									$delta = number_format(100*round(($sub_totals[$subgroup_id]/$base_amount) - 1, 3), 2) . "%";
								}
								else
								{
									$delta = "";
									$over = "";
								}

								
									
								
								$pdf->Cell(29,4, number_format($sub_totals[$subgroup_id],2), 1, 0, 'R', 0);
								$pdf->Cell(12,4, $share, 1, 0, 'R', 0);
								$pdf->Cell(12,4, $delta, 1, 0, 'R', 0);
							}
							
							
						}
						else
						{
							$pdf->Cell(29,4, number_format(0,2), 1, 0, 'R', 0);
							$pdf->Cell(12,4, "", 1, 0, 'R', 0);
							$pdf->Cell(12,4, "", 1, 0, 'R', 0);
						}
					}
					else
					{
						$pdf->Cell(29,5, "", 1, 0, 'R', 0);
						$pdf->Cell(12,5, "", 1, 0, 'R', 0);
						$pdf->Cell(12,5, "", 1, 0, 'R', 0);
					}
				}
				$bid_number++;
			}
			
			$pdf->Ln();
		}
	}

	
	
	$pdf->Ln();
	$pdf->Cell(64,5,"");
	$pdf->SetFont('arialn','B',9);
	$bid_number = 1;
	$base_amount = 0;
	foreach($tmp_offers as $bid_id=>$company_name)
	{
		if(array_key_exists($bid_id, $tmp_offer_group_totals) and array_key_exists($group_id, $tmp_offer_group_totals[$bid_id]))
		{
			
			
			if($bid_number == 1)
			{
				$pdf->Cell(29,5, $currencies[$bid_id] . " " . number_format($tmp_offer_group_totals[$bid_id][$group_id],2), 1, 0, 'R', 0);
				$pdf->Cell(12,5, "", 1, 0, 'R', 0);
				$pdf->Cell(12,5, "100%", 1, 0, 'R', 0);
				$base_amount = $tmp_offer_group_totals[$bid_id][$group_id];

			}
			else
			{
				$pdf->Cell(29,5, $currencies[$bid_id] . " " . number_format($tmp_offer_group_totals[$bid_id][$group_id],2), 1, 0, 'R', 0);
				if($base_amount != 0)
				{
					$over = round(100*($tmp_offer_group_totals[$bid_id][$group_id]/$base_amount), 2) . "%";
					$delta = number_format(100*round(($tmp_offer_group_totals[$bid_id][$group_id]/$base_amount) - 1, 3), 2) . "%";
				}
				else
				{
					$delta = "";
					$over = "";
				}
				
				$pdf->Cell(12,5, "", 1, 0, 'R', 0);
				$pdf->Cell(12,5, $delta, 1, 0, 'R', 0);
			}

			$bid_number++;
		}
	}

	$pdf->Ln();
	$pdf->Cell(64,5,"");

	
	$bid_number = 1;
	$base_amount = 0;
	foreach($tmp_offers as $bid_id=>$company_name)
	{
		if(array_key_exists($bid_id, $tmp_offer_group_totals) and array_key_exists($group_id, $tmp_offer_group_totals[$bid_id]))
		{
			
			
			if($bid_number == 1)
			{
				$pdf->Cell(29,5, "CHF " . number_format($tmp_offer_group_totals_chf[$bid_id][$group_id],2), 1, 0, 'R', 0);
				$pdf->Cell(12,5, "", 1, 0, 'R', 0);
				$pdf->Cell(12,5, "100%", 1, 0, 'R', 0);
				$base_amount = $tmp_offer_group_totals_chf[$bid_id][$group_id];
			}
			else
			{
				$pdf->Cell(29,5, "CHF " . number_format($tmp_offer_group_totals_chf[$bid_id][$group_id],2), 1, 0, 'R', 0);
				
				if($base_amount != 0)
				{
					$over = round(100*($tmp_offer_group_totals_chf[$bid_id][$group_id]/$base_amount), 2) . "%";
					$delta = number_format(100*round(($tmp_offer_group_totals_chf[$bid_id][$group_id]/$base_amount) - 1, 3), 2) . "%";
				}
				else
				{
					$delta = "";
					$over = "";
				}

				$pdf->Cell(12,5, "", 1, 0, 'R', 0);
				$pdf->Cell(12,5, $delta, 1, 0, 'R', 0);
			}
			$bid_number++;
		}
	}

	
	
	$pdf->SetFont('arialn','',8);
	
	$pdf->Ln();
	$pdf->Ln();
	$pdf->Cell(64,5,"");
	$x = 74;
	$y = $pdf->GetY();
	foreach($tmp_offers as $bid_id=>$company_name)
	{
		if(array_key_exists($bid_id, $tmp_offer_group_totals) and array_key_exists($group_id, $tmp_offer_group_totals[$bid_id]))
		{
			$pdf->MultiCell(53,3.5, $remarks[$bid_id], 'R', "L");
			$x = $x + 53;
			$pdf->SetY($y);
			$pdf->SetX($x);
		}
	}

?>