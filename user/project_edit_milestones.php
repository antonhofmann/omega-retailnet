<?php
/********************************************************************

    project_edit_milestones.php

    Edit MileStones of the Projecs

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-10-24
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-16
    Version:        1.0.1

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_edit_milestones");

register_param("pid");

/********************************************************************
    prepare all data needed
*********************************************************************/
$user_roles = get_user_roles(user_id());

// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

// get System currency
$order_currency = get_order_currency($project["project_order"]);


/********************************************************************
    Create Cost Monitoring Sheet
*********************************************************************/ 
//check if the cost monitoring sheet exists already
$sql = "select count(project_cost_id) as num_recs " .
       "from project_costs " .
       "where project_cost_order = "  . $project["project_order"]; 

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

if($row["num_recs"] == 0)
{
    $fields = array();
    $values = array();

    $fields[] = "project_cost_order";
    $values[] = $project["project_order"];
    
    $fields[] = "date_created";
    $values[] = dbquote(date("Y-m-d"));

    $fields[] = "date_modified";
    $values[] = dbquote(date("Y-m-d"));

    $fields[] = "user_created";
    $values[] = dbquote(user_login());

    $fields[] = "user_modified";
    $values[] = dbquote(user_login());

    $sql = "insert into project_costs (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

    mysql_query($sql) or dberror($sql);
}


/********************************************************************
    Create Project Milestones
*********************************************************************/ 
    
$sql = "select * from milestones " .
	   "where milestone_active = 1 " .
	   "order by milestone_code ";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$sql_m = "select count(project_milestone_id) as num_recs from project_milestones " .
		     "where project_milestone_project = " . $project["project_id"] . 
	         "   and project_milestone_milestone = " . $row["milestone_id"];
	
	
	$res_m = mysql_query($sql_m) or dberror($sql_m);
	$row_m = mysql_fetch_assoc($res_m);

	if($row_m["num_recs"] == 0)
	{
		$fields = array();
		$values = array();

		$fields[] = "project_milestone_project";
		$values[] = $project["project_id"];

		$fields[] = "project_milestone_milestone";
		$values[] = $row["milestone_id"];
		
		$fields[] = "date_created";
		$values[] = dbquote(date("Y-m-d"));

		$fields[] = "date_modified";
		$values[] = dbquote(date("Y-m-d"));

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$fields[] = "user_modified";
		$values[] = dbquote(user_login());

		$sql = "insert into project_milestones (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

		mysql_query($sql) or dberror($sql);
	}
}

/********************************************************************
    Get Cost Monitoring Sheet Budget Data
*********************************************************************/ 
$sql =  "select * from project_costs " .
        "where project_cost_order = " . $project["project_order"];

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

$project_cost_sqms = $row["project_cost_sqms"];

$project_cost_CER = $row["project_cost_CER"];
$project_cost_milestone_remarks = $row["project_cost_milestone_remarks"];
$project_cost_milestone_remarks2 = $row["project_cost_milestone_remarks2"];
$project_cost_milestone_turnaround = $row["project_cost_milestone_turnaround"];

/********************************************************************
    Caclucalte dates and diffeneces in dates
*********************************************************************/
$dates = array();
$comments = array();
$datecomments = array();
$daysconsumed = array();
$daysaccumulated = array();
$lastdate = "";
$milestone_turnaround = "";
$milestones_ids = array();


//get trunaround
$date_min = "0000-00-00";
$sql_m = "select min(project_milestone_date) as date_min " .
         "from project_milestones " .
         " where project_milestone_date <> '0000-00-00' " . 
		 " and project_milestone_date is not NULL " . 
		 " and project_milestone_project = " . $project["project_id"];
$res = mysql_query($sql_m) or dberror($sql_m);
if($row = mysql_fetch_assoc($res))
{
	$date_min = $row["date_min"];
}

$date_max = "0000-00-00";
$sql_m = "select max(project_milestone_date) as date_max " . 
         "from project_milestones " .
         " where project_milestone_date <> '0000-00-00' " . 
		 " and project_milestone_date is not NULL " . 
		 " and project_milestone_project = " . $project["project_id"];
$res = mysql_query($sql_m) or dberror($sql_m);
if($row = mysql_fetch_assoc($res))
{
	$date_max = $row["date_max"];
}

$milestone_turnaround = floor((strtotime($date_max) - strtotime($date_min)) / 86400);


$sql = "select project_milestone_id, project_milestone_date, project_milestone_date_comment, " .
       "project_milestone_comment, milestone_id, milestone_code, milestone_text " . 
       "from project_milestones " .
       " left join milestones on milestone_id = project_milestone_milestone ";

$list_filter = "project_milestone_visible = 1 and project_milestone_project = " . $project["project_id"];

if($project["project_projectkind"] == 5) //Lease Renewal
{
	$list_filter .= " and milestone_code <= '0020' ";	
}

if($project["project_cost_type"] == 2) //Franchisee
{
	//$list_filter .= " and milestone_code < '1250' ";	
}



$sql_m = $sql . " where " . $list_filter . " order by milestone_code ";

$res = mysql_query($sql_m) or dberror($sql_m);
$num_of_visible_milestones = 0;
while($row = mysql_fetch_assoc($res))
{
	$dates[$row["project_milestone_id"]] = to_system_date($row["project_milestone_date"]);
	$datecomments[$row["project_milestone_id"]] = $row["project_milestone_date_comment"];
    $comments[$row["project_milestone_id"]] = $row["project_milestone_comment"];
	$milestones_ids[$row["project_milestone_id"]] = $row["milestone_id"];

	
	if($lastdate != "0000-00-00" and $lastdate != NULL and $row["project_milestone_date"] != "0000-00-00" and $row["project_milestone_date"] != NULL)
	{
		$thisdate = $row["project_milestone_date"];
		$difference_in_days = floor((strtotime($thisdate) - strtotime($date_min)) / 86400);
		
		
		
		$daysconsumed[$row["project_milestone_id"]] = $difference_in_days;
		
		
		//$milestone_turnaround = $milestone_turnaround + $difference_in_days;
		//$daysaccumulated[$row["project_milestone_id"]] = $milestone_turnaround;
	}

	if($row["project_milestone_date"] != "0000-00-00" and $row["project_milestone_date"] != NULL)
	{
		$lastdate = $row["project_milestone_date"];
	}

	$num_of_visible_milestones++;
}



/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project");

$form->add_section("Project");
$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid",$project["project_order"]);

require_once "include/project_head_small.php";

$form->add_label("project_cost_sqms", "Sales Surface sqms", 0, $project_cost_sqms);

if($project["project_cost_type"])
{
    $form->add_label("project_cost_type", "Project Legal Type", $project["project_cost_type"]);
}
else
{
    $form->add_label("dummy", "Project Legal Type");

}


$form->add_edit("project_cost_CER", "CER Amount " . $order_currency["symbol"], 0 , $project_cost_CER, TYPE_DECIMAL, 12,2);

$form->add_edit("project_cost_milestone_turnaround", "Turn Around " , 0 , $milestone_turnaround, TYPE_DECIMAL, 12,2);

$form->add_multiline("project_cost_milestone_remarks", "Remarks", 6, 0, $project_cost_milestone_remarks);
$form->add_multiline("project_cost_milestone_remarks2", "Remarks 2 (MIS)", 6, 0, $project_cost_milestone_remarks2);

$form->add_section("Project Milestone Selector");
$icon = "../pictures/add_item.gif";
$link = "javascript:open_selector('')";
$form->add_label_selector("milestones", "Milestone Selector", 0, "Please select the milestones for this project.", $icon, $link);


if(param("s") == 1)
{
    $form->message = "Your changes have been saved.";
}

if($num_of_visible_milestones > 0)
{
	$form->add_button("remove_milestones", "Remove all Milestones");
}
else
{
	$form->add_button("show_milestones", "Add all Milestones");
}

$form->add_button("save_form", "Save Data");



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Create List for Milestones
*********************************************************************/ 
$list = new ListView($sql);

$list->set_title("Milestones");
$list->set_entity("project_milestones");
$list->set_filter($list_filter);
$list->set_order("milestone_code");


$list->add_column("milestone_code", "Code");   
$list->add_column("milestone_text", "Description");

$list->add_date_edit_column("project_milestone_date", "Date", "8", 0, $dates);
$list->add_text_column("daysconsumed", "Days", COLUMN_ALIGN_RIGHT, $daysconsumed);
//$list->add_text_column("daysaccumulated", "Sum", COLUMN_ALIGN_RIGHT, $daysaccumulated);
$list->add_edit_column("project_milestone_date_comment", "Date Comment", "30", 0, $datecomments);
$list->add_edit_column("project_milestone_comment", "General Comment", "55", 0, $comments);


$list->populate();
$list->process();


/********************************************************************
    save data
*********************************************************************/

if ($form->button("save_form") or $form->button("project_cost_type"))
{
    
    if ($form->validate())
    {
        $sql = "update project_costs set " .
               "project_cost_CER = '" . $form->value("project_cost_CER") . "', " .
               "project_cost_milestone_turnaround = " . dbquote($form->value("project_cost_milestone_turnaround")) . ", " .
               "project_cost_milestone_remarks = " . dbquote($form->value("project_cost_milestone_remarks")) . ", " .
			   "project_cost_milestone_remarks2 = " . dbquote($form->value("project_cost_milestone_remarks2")) .
               " where project_cost_order = " . $project["project_order"];

        $result = mysql_query($sql) or dberror($sql);

        $dates = $list->values("project_milestone_date");

        $comments = $list->values("project_milestone_comment");

		$datecomments = $list->values("project_milestone_date_comment");

        //print_r($hasoffers);
        foreach ($dates as $key=>$value)
        {
            
			//get old date value of milestone
			$old_date_value_of_milestone = '';
			$sql = "select project_milestone_date from project_milestones where project_milestone_id = " . $key;
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res) and $value)
			{
				$old_date_value_of_milestone = $row["project_milestone_date"];
			}
			
			// update record
            $project_milestone_fields = array();

            if($value and is_date_value($value))
            {
                $project_milestone_fields[] = "project_milestone_date = " . dbquote(from_system_date($value));

				
				
				//update cer_basicdata
				if($milestones_ids[$key] == 13 
					and $value 
					and ($old_date_value_of_milestone == NULL or $old_date_value_of_milestone == '0000-00-00')) //LN approved
				{
	
					$fields = array();

					$fields[] = "cer_basicdata_cer_locked = 0";

					$value1 = "current_timestamp";
					$fields[] = "date_modified = " . $value1;

					if (isset($_SESSION["user_login"]))
					{
						$value1 = $_SESSION["user_login"];
						$fields[] = "user_modified = " . dbquote($value1);
					}

					$sql = "update cer_basicdata set " . join(", ", $fields) . " where cer_basicdata_version = 0 and cer_basicdata_project = " . param("pid");
					mysql_query($sql) or dberror($sql);
				}
            }
			else
			{
				$project_milestone_fields[] = "project_milestone_date = NULL";
			}
            if($comments[$key])
            {
                $project_milestone_fields[] = "project_milestone_comment = " . dbquote($comments[$key]);
            }
			else
			{
				$project_milestone_fields[] = "project_milestone_comment = ''";
			}
			if($datecomments[$key])
            {
                $project_milestone_fields[] = "project_milestone_date_comment = " . dbquote($datecomments[$key]);
            }
			else
			{
				$project_milestone_fields[] = "project_milestone_date_comment = ''";
			}
            

            $value1 = "current_timestamp";
            $project_milestone_fields[] = "date_modified = " . $value1;

            if (isset($_SESSION["user_login"]))
            {
                $value1 = dbquote($_SESSION["user_login"]);
                $project_milestone_fields[] = "user_modified = " . $value1;
            }
       
            $sql = "update project_milestones set " . join(", ", $project_milestone_fields) . " where project_milestone_id = " . $key;
            mysql_query($sql) or dberror($sql);

			//check if poslease_handoverdate or poslease_firstrentpayed is missing in case of LN approval
			// submit email to client

			// only send mail alert if mail_alert was not sent before and in case project is a corporate project
			if($project["project_cost_type"] == 1 
				and ($old_date_value_of_milestone == NULL or $old_date_value_of_milestone == '0000-00-00') 
				and strpos($project["project_number"], '.L') == 0) // corporate, and no lease renewal
			{

				$sql = "select * from project_milestones " . 
					   "left join milestones on milestone_id = project_milestone_milestone " . 
					   "where milestone_on_check_cer_lease = 1 and project_milestone_id = " . $key;

				
				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res) and $value)
				{
					$milestone_email_cc1 = $row["milestone_email"];
					$milestone_email_cc2 = $row["milestone_ccmail"];

					//check if mail has to be sent
					$posdata = get_pos_data($project["project_order"]);
					$posleases = get_pos_leasedata($posdata["posaddress_id"], $project["project_order"]);


					if($project["project_projectkind"] != 2 and 
						($posleases["poslease_handoverdate"] == NULL or $posleases["poslease_handoverdate"] == '0000-00-00' or 
						$posleases["poslease_firstrentpayed"] == NULL or $posleases["poslease_firstrentpayed"] == '0000-00-00')) {

						$sql = "select user_id, user_firstname, user_name, user_email " .
							   "from users " . 
							   "where user_id = " . $project["order_user"];

						$res = mysql_query($sql) or dberror($sql);

						$res = mysql_query($sql) or dberror($sql);
						if ($row = mysql_fetch_assoc($res))
						{
							$client_name = $row["user_firstname"] . ' '  . $row["user_name"];
							$client_firstname = $row["user_firstname"];
							$client_email = $row["user_email"];


							$sql = "select order_number, order_shop_address_place, country_name, order_shop_address_company, " .
								   "project_postype, project_projectkind, project_cost_type " . 
								   "from projects " .
								   "left join orders on order_id = project_order " .
								   "left join countries on country_id = order_shop_address_country " .
								   "left join project_costs on project_cost_order = order_id " .
								   "where project_id = " .  $project["project_id"];

							$res = mysql_query($sql) or dberror($sql);
							if ($row = mysql_fetch_assoc($res))
							{
								$user_data = get_user(user_id());
								
								$subject = "LN approval alert - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_place"] . ", " . $row["order_shop_address_company"];
													
							
								$sql = "select * from mail_alert_types " . 
									   "where mail_alert_type_id = 10";
										

								$res = mysql_query($sql) or dberror($sql);
								if ($row = mysql_fetch_assoc($res))
								{
									$sender_email = $user_data["email"];
									$sender_name = $user_data["name"] . " " . $user_data["firstname"];


									$link = APPLICATION_URL . "/cer/cer_application_rental.php?pid=" . $project['project_id'];

									$mail_text = $row["mail_alert_mail_text"];
									$mail_text = str_replace('{name}', $client_firstname, $mail_text);
									$mail_text = str_replace('{link}', $link, $mail_text);
									$mail_text = str_replace('{sender}', $sender_name, $mail_text);


									$mail = new Mail();
									$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
									$mail->set_sender($sender_email, $sender_name);
									
									$mail->add_recipient($client_email);


									$rctps = $client_email;
									
									if($milestone_email_cc1) {

										$mail->add_cc($milestone_email_cc1);
										$rctps .=  "\n" . "and CC-Mail to:" . "\n" . $milestone_email_cc1;
									}
									if($milestone_email_cc2) {

										$mail->add_cc($milestone_email_cc2);
										$rctps .= "\n" . $milestone_email_cc2;
									}

									$mail->add_text($mail_text);

									$result = $mail->send();


									if($result == 1)
									{
										$fields = array();
										$values = array();

										$fields[] = "cer_mail_project";
										$values[] = dbquote(param("pid"));

										$fields[] = "cer_mail_group";
										$values[] = dbquote($subject);

										$fields[] = "cer_mail_text";
										$values[] = dbquote($mail_text);

										$fields[] = "cer_mail_sender";
										$values[] = dbquote($sender_name);

										$fields[] = "cer_mail_sender_email";
										$values[] = dbquote($sender_email);

										$fields[] = "cer_mail_reciepient";
										$values[] = dbquote($rctps);

										$fields[] = "date_created";
										$values[] = "now()";

										$fields[] = "date_modified";
										$values[] = "now()";

										$fields[] = "user_created";
										$values[] = dbquote(user_login());

										$fields[] = "user_modified";
										$values[] = dbquote(user_login());

										$sql = "insert into cer_mails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
										
										mysql_query($sql) or dberror($sql);
									}
								}
							}
						}
						
					}
				}
			}
        }
        $link = "project_edit_milestones.php?pid=" . param("pid") . "&s=1"; 
        redirect($link);
    }
}
elseif ($form->button("remove_milestones"))
{
	$sql_u = "update project_milestones " . 
			   "set project_milestone_visible = 0 " . 
			   " where project_milestone_project = " . param("pid");

	$result = mysql_query($sql_u) or dberror($sql_u);

	$link = "project_edit_milestones.php?pid=" . param("pid") . "&s=1"; 
        redirect($link);
}
elseif ($form->button("show_milestones"))
{
	$sql_u = "update project_milestones " . 
			   "set project_milestone_visible = 1 " . 
			   " where project_milestone_project = " . param("pid");

	$result = mysql_query($sql_u) or dberror($sql_u);

	$link = "project_edit_milestones.php?pid=" . param("pid") . "&s=1"; 
        redirect($link);
}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Project Milestones");
$form->render();

echo "<br>";
$list->render();


?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#milestones_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/user/project_edit_milestone_selector.php?pid=<?php echo $project["project_id"]?>'
    });
    return false;
  });
});
</script>


<?php

$page->footer();

?>