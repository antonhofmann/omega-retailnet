<?php
/********************************************************************

    order_checkout.php

    Checkout for new catalog-order

    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date created:   2002-09-01
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-06-15
    Version:        1.0.8

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
define('BILLING_ADDRESS', 1);
define('DELIVERY_ADDRESS',2);
define('SHOP_ADDRESS',3);
define('ADDRESS_ADDRESS', 4);

require_once "../include/frame.php";
require_once "include/order_functions.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_view_orders");

//coices for delivery address
$user = get_user(user_id());
$address = get_address($user["address"]);
$choices = array();

if(param("posaddress_id"))
{
	$choices[1] = "POS location";
}

$choices[2] = $address['company'];
$choices[3] = "Other";

$page = new Page("orders");
$page->register_action('basket', 'Shopping List');


// set default checkout-status


$checkout_status = param('status');


// form


$form = new Form("orders", "order");
$form->add_section("Client");


// helper-form: display buttons at bottom of page


$form3 = new Form();


// get user's basket


$res = mysql_query("select basket_id from baskets where basket_user=" . user_id());
if (mysql_num_rows($res) >= 1)
{
    $row = mysql_fetch_assoc($res);
    $basket = $row['basket_id'];
} else {
    mysql_query("insert into baskets (basket_user_id) values (" . user_id() . ")");
    $basket = mysql_insert_id();
}


$user_properties = get_user(user_id());
$address_id = $user_properties['address'];


if ($checkout_status != "valid")
{
    get_checkout_form($form, $address_id, $choices);


    $form->populate();
}
else
{
    list($list,$list2) = get_order_confirmation($form, $address_id, $basket);
}


if  ($form->button(LIST_BUTTON_BACK))
{
    dump_order_data();
}


$form->process();


// Checkout-Actions


if ($form->button("basket"))
{
    redirect("basket.php");
}


if ($form->button("cancel_order"))
{
    redirect("orders.php");
}
else if ($form->button("billing_address_id"))
{
    if ($form->value("billing_address_id"))
    {
        populate_address($form->value("billing_address_id"), "billing_address");
    }
}
else if ($form->button("delivery_address_place_id"))
{
    if ($form->value("delivery_address_place_id"))
    {
        $sql = "select place_name from places where place_id = " . dbquote($form->value("delivery_address_place_id"));
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$form->value("delivery_is_address", "");
			$form->value("delivery_address_place", $row["place_name"]);
			$form->value("delivery_address_zip", "");
			$form->value("delivery_address_address", "");
			$form->value("delivery_address_address2", "");
			$form->value("delivery_address_phone", "");
			$form->value("delivery_address_fax", "");
			$form->value("delivery_address_contact", "");
		}
		else
		{
			$form->value("delivery_is_address", "");
			$form->value("delivery_address_place", "");
			$form->value("delivery_address_zip", "");
			$form->value("delivery_address_address", "");
			$form->value("delivery_address_address2", "");
			$form->value("delivery_address_phone", "");
			$form->value("delivery_address_fax", "");
			$form->value("delivery_address_contact", "");
		}
    }
}
else if ($form->button("delivery_address_country"))
{
	$form->value("delivery_is_address", "");
	$form->value("delivery_address_place", "");
	$form->value("delivery_address_place_id", 0);
	$form->value("delivery_address_zip", "");
	$form->value("delivery_address_address", "");
	$form->value("delivery_address_address2", "");
	$form->value("delivery_address_phone", "");
	$form->value("delivery_address_fax", "");
	$form->value("delivery_address_contact", "");
}
else if ($form->button("delivery_address_province_id"))
{
	$form->value("delivery_is_address", "");
	$form->value("delivery_address_place", "");
	$form->value("delivery_address_place_id", 0);
	$form->value("delivery_address_zip", "");
	$form->value("delivery_address_address", "");
	$form->value("delivery_address_address2", "");
	$form->value("delivery_address_phone", "");
	$form->value("delivery_address_fax", "");
	$form->value("delivery_address_contact", "");
}
else if ($form->button("delivery_address_id"))
{
    if ($form->value("delivery_address_id"))
    {
       populate_address($form->value("delivery_address_id"), "delivery_address");
    }
}
else if ($form->button("posaddress_id"))
{
   
	if ($form->value("posaddress_id"))
    {
	   populate_address($form->value("posaddress_id"), "pos_address");

	   if ($form->value("delivery_is_address") == 1)
	   {
		   populate_deliveryaddress($form->value("delivery_is_address"), $address, $form->value("posaddress_id"));
	   }
    }
	elseif ($form->value("delivery_is_address") == 1)
	{
		populate_address($form->value("posaddress_id"), "pos_address");
		$form->value("delivery_address_company", "");
		$form->value("delivery_address_company2", "");
		$form->value("delivery_address_address", "");
		$form->value("delivery_address_address2", "");
		//$form->value("delivery_address_place", "");
		//$form->value("delivery_address_place_id", 0);
		$form->value("delivery_address_zip", "");
		//$form->value("delivery_address_province_id", 0);
		$form->value("delivery_address_country", "");
		$form->value("delivery_address_phone", "");
		$form->value("delivery_address_fax", "");
		$form->value("delivery_address_email", "");
		$form->value("delivery_address_contact", "");
	}
	elseif(!$form->value("posaddress_id"))
	{
		populate_address($form->value("posaddress_id"), "pos_address");
	}
}
else if ($form->button("delivery_is_address"))
{
    if ($form->value("delivery_is_address"))
    {
       populate_deliveryaddress($form->value("delivery_is_address"), $address, $form->value("posaddress_id"));
    }
}
else if ($form->button("order"))
{
    // store address-info 


    dump_order_data();

    $form->add_validation("from_system_date({preferred_delivery_date}) >  " . dbquote(date("Y-m-d")), "The preferred arrival date must be a future date!");

    /*
	if (!$form->value("delivery_is_billing_address"))
    {
        $form->add_validation("{delivery_address_company} and " .
                              "{delivery_address_address} and ".
                              "{delivery_address_zip} and " . 
                              "{delivery_address_place} and " .
                              "{delivery_address_country} and " .
                              "{delivery_address_phone} and " .
                              "{delivery_address_contact}", "The delivery address is not complete.");
    }
	*/


	$form->add_validation("{delivery_address_company} and " .
                              "{delivery_address_address} and ".
                              "{delivery_address_zip} and " .
							  "{delivery_address_place_id} and " .
		                      "{delivery_address_province_id} and " .
                              "{delivery_address_place} and " .
                              "{delivery_address_country} and " .
                              "{delivery_address_phone} and " .
                              "{delivery_address_contact}", "The delivery address is not complete.");



	$error = 0;
	if(strlen(preg_replace('/[^0-9a-z_ %\[\]\.\(\)%&-]/s', '', $form->value("delivery_address_address"))) < strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("delivery_address_address")))
		and strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("delivery_address_address"))) > 10)
	{
		$error = 1;
		$form->error("Company names and addresses must contain lower caracters. Do not use capital letters only.");
	}
	elseif(strlen(preg_replace('/[^0-9a-z_ %\[\]\.\(\)%&-]/s', '', $form->value("delivery_address_address2"))) < strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("delivery_address_address2")))
		and strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("delivery_address_address2"))) > 10)
	{
		$error = 1;
		$form->error("Company names and addresses must contain lower caracters. Do not use capital letters only.");
	}


    if ($error == 0 and $form->validate())
    {
        unset($form);
        $form = new Form("orders", "order");
        $checkout_status = "valid";


        list($list,$list2) = get_order_confirmation($form, $address_id, $basket);
        
        $form3->add_button("submit_order", "Submit Order");
        $form3->add_button("checkout_edit", "Back");
    }
}




$form3->process();


if ($form3->button("submit_order"))
{
	save_order($form);
    redirect("order_new_submitted.php");
}
else if ($form3->button("checkout_edit"))
{
    unset($form);
    $checkout_status = "edit";


    $form = new Form("orders", "order");
    get_checkout_form($form, $address_id, $choices);
    $form->populate();
}



$form->add_hidden("status", $checkout_status);


if ($checkout_status == "valid")
{
    // Cost overview


    $currency = get_user_currency(user_id());


    $form2 = new Form();
    $form2->add_section("Cost Overview");
    $form2->add_label("grand_total1", "Standard Items/Add-On's " . $currency["currency_symbol"], 0,
            number_format(get_basket_part_total($basket, false) + get_basket_part_total($basket, true),2));
}


$page->register_action('home', 'Home', "welcome.php");


// render page


$page->header();
if ($checkout_status == "valid")
{
    $page->title('Order Preview');
    $form->render();
    $form2->render();
    $list->render();
    $list2->render();
    $form3->render();
}
else 
{
    $page->title('Checkout');
    $form->render();
}

?>

<div id="city" style="display:none;">
    Please indicate the new city's name in English!
</div>

<?php
$page->footer();




?>