<?php
/********************************************************************


    shopping_list.php


    Entry page for the orders section.


    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-31
    Modified by:    Claudio Felber (page.agent@gmx.net)
    Date modified:  2002-08-31
    Version:        1.0.0


    Copyright (c) 2002, Swatch AG, All Rights Reserved.


*********************************************************************/


require_once "../include/frame.php";


check_access("can_view_orders");


/*
$list = new ListView("select order_id, order_number, order_date, concat_ws(' ', users.user_name, users.user_firstname) as retail_operator_name from orders left join users on users.user_id = orders.order_retail_operator where orders.user_created='" . user_login() . "'");


$list->set_entity("catalog_orders");
$list->set_order("order_date");


$list->add_column("order_date", "Order Date", "", LIST_FILTER_FREE);
$list->add_column("order_number", "Order Number", "order_items.php", LIST_FILTER_FREE);
$list->add_column("retail_operator_name", "Retail Operator", "");


$list->add_button(LIST_BUTTON_FILTER, "Filter");
$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove Filter");


$list->process();
*/
$page = new Page("orders");
$page->header();
$page->title('Shopping-List');




// temporary, will be removed when register_action() is available


$page->footer();


?>