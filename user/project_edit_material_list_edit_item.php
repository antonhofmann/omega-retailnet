<?php
/********************************************************************

    project_edit_material_list_edit_item.php

    Edit item position in material list.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-12
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-12-19
    Version:        1.2.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_edit_list_of_materials_in_projects");


register_param("pid");
register_param("id");
set_referer("project_edit_material_list.php");

/********************************************************************
    prepare all data needed
*********************************************************************/

// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

// get order_item's information
$order_item = get_order_item(id());

// get system currency
$system_currency = get_system_currency_fields();

// get orders's currency
$order_currency = get_order_currency($project["project_order"]);

// get supplier's currency
$supplier_currency_symbol = "";
if($order_item["item"]  and $order_item["supplier"])
{
    $supplier_currency_symbol = get_item_currency_symbol($order_item["supplier"], $order_item["item"]);
}
if (!$supplier_currency_symbol)
{
    $supplier_currency_symbol = get_currency_symbol($order_item["supplier_currency"]);
}

// create sql for the supplier listbox
if ($order_item["type"] == ITEM_TYPE_STANDARD or $order_item["type"] == ITEM_TYPE_SERVICES)
{
    $sql_suppliers = "select address_id, address_company ".
                     "from items ".
                     "left join suppliers on item_id = supplier_item ".
                     "left join addresses on supplier_address = address_id ".
                     "where address_active = 1 and item_id = " . $order_item["item"] . " ".
                     "order by address_company";
}
else
{
    $sql_suppliers = "select address_id, address_company ".
                     "from addresses ".
                     "where address_type = 2 and address_active = 1 " . " ".
                     "order by address_company";;
}


$sql_forwarders = "select address_id, address_company ".
                  "from addresses ".
                  "where address_type IN (3,9) and address_active = 1 ".
                  "order by address_company";
    


// create sql for the transportation type listbox
$sql_transportation_types = "select transportation_type_id, transportation_type_name ".
                           "from transportation_types ".
                           "order by transportation_type_name";


// get pick up date
$item_pick_up_date = get_last_order_item_date(id(), "PICK");


/********************************************************************
    build form
*********************************************************************/
$form = new Form("order_items", "item from list of materials");

$form->add_section("Project");
$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid",$project["project_order"]);
$form->add_hidden("order_item_item", $order_item["item"]);
$form->add_hidden("order_item_id", id());
$form->add_hidden("order_item_type", $order_item["type"]);
$form->add_hidden("order_item_currency", $order_item["supplier_currency"]);
$form->add_hidden("order_item_old_quantity", $order_item["quantity"]);


require_once "include/project_head_small.php";

$form->add_section("Item Information");
$form->add_multiline("order_item_text", "Description*", 4, NOTNULL);

if ($order_item["type"] == ITEM_TYPE_STANDARD or $order_item["type"] == ITEM_TYPE_SPECIAL or $order_item["type"] == ITEM_TYPE_SERVICES)
{
    if ($project["order_budget_is_locked"] == 1)
    {
        $form->add_label("order_item_quantity", "Quantity");
    }
    else
    {
        $form->add_edit("order_item_quantity", "Quantity*", NOTNULL);
    }
}

if ($order_item["type"] == ITEM_TYPE_STANDARD or $order_item["type"] == ITEM_TYPE_SERVICES)
{
    $form->add_label("order_item_system_price", "Selling Price in " . $system_currency["symbol"] . "*");
    $form->add_label("order_item_client_price", "Client's Price in " . $order_currency["symbol"] . "*");

    $form->add_label("order_item_supplier_system_price", "Supplier's Price in " . $system_currency["symbol"], 0, $order_item["supplier_system_price"]);

    $form->add_label("order_item_supplier_price", "Supplier's Price in " . $supplier_currency_symbol);
}

else if ($order_item["type"] == ITEM_TYPE_SPECIAL)
{
    if ($project["order_budget_is_locked"] == 1)
    {
	   $form->add_label("order_item_system_price", "Selling Price in " . $system_currency["symbol"]); 
    }
    else
    {
		$form->add_edit("order_item_system_price", "Selling Price in " . $system_currency["symbol"] . "*");
    }

    $form->add_label("order_item_client_price", "Client's Price in " . $order_currency["symbol"] . "*");

    $form->add_label("order_item_supplier_system_price", "Supplier's Price in " . $system_currency["symbol"], 0, $order_item["supplier_system_price"]);

    if ($project["order_budget_is_locked"] == 1)
    {
        $form->add_label("order_item_supplier_price", "Supplier's Price in " . $supplier_currency_symbol);
    }
    else
    {
        $form->add_edit("order_item_supplier_price", "Supplier's Price in " . $supplier_currency_symbol);
    }

	$form->add_section("Cost Monitoring");
	
	$sql = "select project_cost_groupname_id, project_cost_groupname_name " . 
       "from project_cost_groupnames " . 
	   "where project_cost_groupname_active = 1 and project_cost_groupname_id in (2, 10) order by project_cost_groupname_id";

	$form->add_list("order_item_cost_group", "Cost Group*",$sql, NOTNULL);

	$sql = "select costmonitoringgroup_id, costmonitoringgroup_text from costmonitoringgroups order by costmonitoringgroup_text";
	$form->add_list("order_items_costmonitoring_group", "Type*",$sql, NOTNULL);

}

else if ($order_item["type"] == ITEM_TYPE_COST_ESTIMATION)
{
    $form->add_edit("order_item_supplier_freetext", "Supplier Info");
    $form->add_edit("order_item_system_price", "Selling Price in " . $system_currency["symbol"] . "*");

	$sql = "select project_cost_groupname_id, project_cost_groupname_name " . 
		   "from project_cost_groupnames " . 
		   "where project_cost_groupname_active = 1 order by project_cost_groupname_id";

	$form->add_list("order_item_cost_group", "Cost Group",$sql, NOTNULL);

    $form->add_label("order_item_client_price", "Client's Price in " . $order_currency["symbol"] . "*");

}

else if ($order_item["type"] == ITEM_TYPE_LOCALCONSTRUCTIONCOST)
{
    $form->add_edit("order_item_supplier_freetext", "Supplier Info");
    $form->add_edit("order_item_system_price", "Selling Price in " . $system_currency["symbol"] . "*");
    
	$sql = "select project_cost_groupname_id, project_cost_groupname_name " . 
		   "from project_cost_groupnames " . 
		   "where project_cost_groupname_active = 1 order by project_cost_groupname_id";

	$form->add_list("order_item_cost_group", "Cost Group",$sql, NOTNULL);

	$form->add_label("order_item_client_price", "Client's Price in " . $order_currency["symbol"] . "*");

}

if ($order_item["type"] == ITEM_TYPE_STANDARD or $order_item["type"] == ITEM_TYPE_SPECIAL or $order_item["type"] == ITEM_TYPE_SERVICES)
{
    $form->add_section("Accounting Information");
    $form->add_edit("order_item_cost_unit_number", "Cost Unit Number");
    $form->add_edit("order_item_po_number", "P.O. Number");
	$form->add_hidden("order_item_old_po_number", $order_item["po_number"]);

	$form->add_checkbox("change_all_ponumbers", "apply change of P.O. number to all items of the same supplier having identical P.O. numbers");

    $form->add_section("Supplier");

    if(!$item_pick_up_date["last_date"])
    {
        $form->add_list("order_item_supplier_address", "Supplier*", $sql_suppliers, NOTNULL, $order_item["supplier"]);
    }
    else
    {
        $form->add_lookup("order_item_supplier_address", "Supplier", "addresses", "address_company", $flags = 0, $order_item["supplier"]);
    }
}

if ($order_item["type"] == ITEM_TYPE_STANDARD or $order_item["type"] == ITEM_TYPE_SPECIAL)
{
	$form->add_label("order_item_production_time", "Production Time");
    $form->add_label("order_item_supplier_item_code", "Supplier's Item Code");
    $form->add_label("order_item_offer_number", "Supplier's Offer Number");

    
	$form->add_checkbox("order_item_no_offer_required", "does not require a supplier's offer", $order_item["no_offer_required"], 0, "Options");
	
	if ($order_item["type"] == ITEM_TYPE_SPECIAL)
	{
		$form->add_checkbox("order_item_only_quantity_proposal", "does only need a supplier's quantity proposal", $order_item["only_quantity_proposal"]);
	}
	else
	{
		$form->add_hidden("order_item_only_quantity_proposal",0);
	}


    $form->add_section("Traffic Information");

    $form->add_hidden("old_order_item_forwarder_address", $order_item["forwarder"]);
    
    
    $form->add_list("order_item_transportation", "Transportation Type", $sql_transportation_types, 0, $order_item["transportation"]);
	$form->add_list("order_item_forwarder_address", "Forwarder", $sql_forwarders, 0, $order_item["forwarder"]);
	//$form->add_checkbox("change_all_forwarders", "apply change of transportation type and forwarder to all items of the same forwarder");
	//$form->add_checkbox("change_all_suppliers", "apply change of transportation type and forwarder to all items of the same supplier");
	
	$choices = array();
	$choices[1] = "apply change of transportation type and forwarder to all items of the same forwarder";
	$choices[2] = "apply change of transportation type and forwarder to all items of the same supplier";
	$captions = array();
	$captions[1] = '';
	$captions[2] = '';

	$form->add_radiolist("change_all_forwarders", $captions, $choices, VERTICAL,0);

	
	$form->add_edit("order_item_staff_for_discharge", "Staff for Discharge", 0, "", TYPE_INT, 1);

    $form->add_hidden("order_item_forwarder_address_old_value",  $order_item["forwarder"]);

    /*
    if(!$item_pick_up_date["last_date"])
    {

        $form->add_list("order_item_transportation", "Transportation Type", $sql_transportation_types, 0, $order_item["transportation"]);
        $form->add_list("order_item_forwarder_address", "Forwarder", $sql_forwarders, 0, $order_item["forwarder"]);
        $form->add_edit("order_item_staff_for_discharge", "Staff for Discharge", 0, "", TYPE_INT, 1);
    }
    else
    {
        $form->add_lookup("order_item_transportation", "Transportation Type", "transportation_types", "transportation_type_name", 0, $order_item["transportation"]);
        $form->add_lookup("order_item_forwarder_address", "Forwarder", "addresses", "address_company", 0, $order_item["forwarder"]);
        $form->add_label("order_item_staff_for_discharge", "Staff for Discharge");
    }
    */

    $form->add_section("Budgeting Information");
    $form->add_checkbox("order_item_not_in_budget", "does not appear in budget", "", "", "Project Budget");

	$form->add_section("Project Sheet Budget Request");
    $form->add_checkbox("order_item_exclude_from_ps", "local production", "", "", "Project Sheet");

}

$form->add_button("save_data", "Save");

if ($project["order_budget_is_locked"] == 1)
{
    $form->add_button(FORM_BUTTON_BACK, "Back");
}
else
{
    $form->add_button(FORM_BUTTON_DELETE, "Delete");
    $form->add_button(FORM_BUTTON_BACK, "Back");
}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("save_data"))
{
	if ($form->validate())
    {

		 $link = "project_edit_material_list.php?pid=" . param("pid");
         project_edit_order_item_save($form);


		 if ($order_item["type"] == ITEM_TYPE_STANDARD or $order_item["type"] == ITEM_TYPE_SPECIAL or $order_item["type"] == ITEM_TYPE_SERVICES)
         {
             //check if P.O. number has changed
			 if($form->value("order_item_po_number") and $form->value("order_item_po_number") != $form->value("order_item_old_po_number"))
			 {
				 $supplier = $form->value("order_item_supplier_address");
				 if($supplier and $form->value("change_all_ponumbers") == 1) //apply change to all items of the same forwarder
				 {
					$sql = "update order_items set " .
						   " order_item_po_number = " . dbquote($form->value("order_item_po_number")) . 
						   " where order_item_order = " . dbquote($project["project_order"]) . 
						   " and order_item_supplier_address = " . dbquote($supplier) . 
					       " and order_item_po_number = " . dbquote($form->value("order_item_old_po_number"));
					$res = mysql_query($sql) or dberror($sql);
				 }
			 }
			 
			 // check if quntity has changed and set order_date to NULL
			 if($form->value("order_item_old_quantity") > 0 and $form->value("order_item_quantity") != $form->value("order_item_old_quantity"))
			 {
				 $revisions = $order_item["order_revisions"] + 1;
				 $sql = "update order_items set " .
                       "order_item_ordered = NULL, " .
					   "order_item_order_revisions = " . $revisions . 
                       " where order_item_id = " . id();

                 $res = mysql_query($sql) or dberror($sql);
			 }

			 if ($order_item["type"] == ITEM_TYPE_STANDARD or $order_item["type"] == ITEM_TYPE_SPECIAL)
			 {

				 //check if forwarder has changed
				 $order_items_concerned = array();
				 $f = $form->value("order_item_forwarder_address");
				 $fo = $form->value("order_item_forwarder_address_old_value");

				 if($fo and $f and $f != $fo)
				 {
					if($form->value("change_all_forwarders") > 0) //apply change to all items of the same forwarder or supplier
					{
						if($form->value("change_all_forwarders") == 1) //items with same forwarder
						{
							$sql = "select order_item_id, if(order_item_item <>'', item_code, item_type_name) as item_shortcut " .
								   "from order_items " . 
								   "left join items on item_id = order_item_item " .
								   "left join item_types on order_item_type = item_type_id ".
								   "where order_item_order = " .dbquote($project["project_order"]) . 
								   " and order_item_forwarder_address = " . dbquote($fo);
						}
						elseif($form->value("change_all_forwarders") == 2) // items with same supplier
						{

							$sql = "select order_item_id, if(order_item_item <>'', item_code, item_type_name) as item_shortcut " .
								   "from order_items " . 
								   "left join items on item_id = order_item_item " .
								   "left join item_types on order_item_type = item_type_id ".
								   "where order_item_order = " .dbquote($project["project_order"]) . 
								   " and order_item_supplier_address = " . dbquote($form->value('order_item_supplier_address'));
						}

						$res = mysql_query($sql) or dberror($sql);

						while ($row = mysql_fetch_assoc($res))
						{
							$order_items_concerned[$row["order_item_id"]] = $row["item_shortcut"];
							$sql = "delete from dates " .
								   "where date_order_item = " . $row["order_item_id"] . 
								   "  and (date_type = 2 or date_type = 3) ";

							$res_d = mysql_query($sql) or dberror($sql);

							$sql = "update order_items set " .
								   "order_item_transportation = " . dbquote($form->value("order_item_transportation")) . ", " .
								   "order_item_forwarder_address = " . dbquote($f) . ", " . 
								   "order_item_pickup = Null, " .
								   "order_item_pickup_changes = 0, " .
								   "order_item_expected_arrival = Null, " .
								   "order_item_expected_arrival_changes = 0 " .
								   "where order_item_id = " . $row["order_item_id"];

							$res_u = mysql_query($sql) or dberror($sql);

						}
					}
					else
				    {
						$sql = "delete from dates " .
							   "where date_order_item = " . id() . 
							   "  and (date_type = 2 or date_type = 3) ";

						$res = mysql_query($sql) or dberror($sql);

						$sql = "update order_items set " .
							   "order_item_pickup = Null, " .
							   "order_item_pickup_changes = 0, " .
							   "order_item_expected_arrival = Null, " .
							   "order_item_expected_arrival_changes = 0 " .
							   "where order_item_id = " . id();

						$res = mysql_query($sql) or dberror($sql);
					}


					//get forwarde's names
					$f_a = get_address($f);
					$fo_a = get_address($fo);
					
					// send an email to supplier, if forwarder has changed

					$mail = new Mail();
					$num_mails = 0;

					$sql = "select order_id, order_number, order_item_po_number, ".
						   "if(order_item_item <>'', item_code, item_type_name) as item_shortcut, ".
						   "order_shop_address_company, order_shop_address_place, country_name, " .
						   "users.user_email as recepient, users.user_email_cc as cc," .
						   "users.user_email_deputy as deputy, " .
						   "users.user_address as address_id, " .
						   "users1.user_email as sender, concat(users1.user_name, ' ', users1.user_firstname) as user_fullname ".
						   "from orders ".
						   "left join countries on country_id = order_shop_address_country " .
						   "left join order_items on order_item_order = order_id " .
						   "left join items on order_item_item = item_id ".
						   "left join item_types on order_item_type = item_type_id ".
						   "left join addresses on address_id = order_item_supplier_address ".
						   "left join users on users.user_address = address_id ".
						   "left join users as users1 on " . user_id() . "= users1.user_id ".
						   "where users.user_active = 1 and order_item_id = " . id();
					
					$mail_sent_to = "\nMail sent to: ";

					$res = mysql_query($sql) or dberror($sql);
					while ($row = mysql_fetch_assoc($res) and $row["recepient"])
					{
						$sender_email = $row["sender"];
						$sender_name =  $row["user_fullname"];
						
						$subject = MAIL_SUBJECT_PREFIX . ": Forwarder has changed - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " .  $row["order_shop_address_company"];

						
						$mail->set_subject($subject);

						$mail->set_sender($sender_email, $sender_name);

						$mail->add_recipient($row["recepient"]);
						if($row["cc"])
						{
							$mail->add_cc($row["cc"]);
						}
						if($row["deputy"])
						{
							$mail->add_cc($row["deputy"]);
						}

						$mail_sent_to = $mail_sent_to . "\n" . $row["recepient"];

						
						if(count($order_items_concerned) > 0)
						{
							$items = "";
							foreach($order_items_concerned as $item_id=>$item_code)
							{
								$items .= $item_code . ", ";
							}
							$items = substr($items, 0, strlen($items)-2);
							$bodytext0 = "The forwarder has changed for the following Project\n" .
									 "Project: " . $project["order_number"] . "    Items:  " . $items . "\n".
									 "from " . $fo_a["company"] . " to " . $f_a["company"];
						}
						else
						{
							$items = $row["item_shortcut"];
							$bodytext0 = "The forwarder has changed for the following Project\n" .
									 "Project: " . $project["order_number"] . "    Item:  " . $items . "\n".
									 "from " . $fo_a["company"] . " to " . $f_a["company"];
						}

						$num_mails++;
													
					}


					$link ="project_view_traffic_data.php?pid=" . param("pid");
					$bodytext = $bodytext0 . "\n\nclick below to have direct access to the project:\n";

					$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
					
					$mail->add_text($bodytext);
					if($num_mails > 0)
					{
						$mail->send();

						append_mail($project["project_order"], "" , user_id(), $bodytext0 . " " . $mail_sent_to , "", 1);
					}


					// send an email to old forwarder, if forwarder has changed

					$mail = new Mail();
					$num_mails = 0;

					$sql = "select order_id, order_number, order_item_po_number, ".
						   "if(order_item_item <>'', item_code, item_type_name) as item_shortcut, ".
						   "order_shop_address_company, order_shop_address_place, country_name, " .
						   "users.user_email as recepient, users.user_email_cc as cc," .
						   "users.user_email_deputy as deputy, " .
						   "users.user_address as address_id, " .
						   "users1.user_email as sender, concat(users1.user_name, ' ', users1.user_firstname) as user_fullname ".
						   "from orders ".
						   "left join countries on country_id = order_shop_address_country " .
						   "left join order_items on order_item_order = order_id " .
						   "left join items on order_item_item = item_id ".
						   "left join item_types on order_item_type = item_type_id ".
						   "left join addresses on address_id = " . $fo .
						   " left join users on users.user_address = address_id ".
						   "left join users as users1 on " . user_id() . "= users1.user_id ".
						   "where users.user_active = 1 and order_item_id = " . id();
					
					$mail_sent_to = "\nMail sent to: ";

					$res = mysql_query($sql) or dberror($sql);
					while ($row = mysql_fetch_assoc($res) and $row["recepient"])
					{
						$sender_email = $row["sender"];
						$sender_name =  $row["user_fullname"];
						
						$subject = MAIL_SUBJECT_PREFIX . ": Forwarder has changed - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " .  $row["order_shop_address_company"];

						
						$mail->set_subject($subject);

						$mail->set_sender($sender_email, $sender_name);

						$mail->add_recipient($row["recepient"]);
						if($row["cc"])
						{
							$mail->add_cc($row["cc"]);
						}
						if($row["deputy"])
						{
							$mail->add_cc($row["deputy"]);
						}

						$mail_sent_to = $mail_sent_to . "\n" . $row["recepient"];


						if(count($order_items_concerned) > 0)
						{
							$items = "";
							foreach($order_items_concerned as $item_id=>$item_code)
							{
								$items .= $item_code . ", ";
							}
							$items = substr($items, 0, strlen($items)-2);
							$bodytext0 = "Please note that the transportation situation has changed for the following Project\n" .
									 "Project: " . $project["order_number"] . "\n" .
							"The following items will be forwarded by another company:  " . $items;
						}
						else
						{
							$items = $row["item_shortcut"];
							$bodytext0 = "Please note that the transportation situation has changed for the following Project\n" .
									 "Project: " . $project["order_number"] . "\n" .
							"The following item will be forwarded by another company:  " . $items;
						}

						$num_mails++;
													
					}

					$link ="project_view_traffic_data.php?pid=" . param("pid");
					$bodytext = $bodytext0 . "\n\nclick below to have direct access to the project:\n";

					$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
					
					$mail->add_text($bodytext);
					if($num_mails > 0)
					{
						$mail->send();

						append_mail($project["project_order"], "" , user_id(), $bodytext0 . " " . $mail_sent_to , "", 1);
					}
				 }
			 }
         }
		 //$form->message("Your changes were saved.");
		 $link = "project_edit_material_list.php?pid=" . $project["project_id"];
         redirect($link);
    }
}


   
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("List of Materials: Edit Item Position");
$form->render();
$page->footer();

?>