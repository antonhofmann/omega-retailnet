<?php 
/******************************************************************** 
 
    project_add_special_item_individual.php 
 
    Add special item positions to an order 
 
    Created by:     Anton Hofmann (aho@mediaparx.ch) 
    Date created:   2002-09-13 
    Modified by:    Anton Hofmann (aho@mediaparx.ch) 
    Date modified:  2003-02-16 
    Version:        1.0.0 
 
    Copyright (c) 2002, Swatch AG, All Rights Reserved. 
 
*********************************************************************/ 
 
require_once "../include/frame.php"; 
require_once "include/get_functions.php"; 
require_once "include/save_functions.php"; 
 
check_access("can_edit_list_of_materials_in_projects"); 
 
/******************************************************************** 
    prepare all data needed 
*********************************************************************/ 
 
// read project and order details 
$project = get_project(param("pid")); 
 
// get company's address 
$client_address = get_address($project["order_client_address"]); 
 
// create sql for the supplier listbox 
$sql_suppliers = "select address_id, address_company ". 
                 "from addresses ". 
                 "where address_type = 2 and address_active = 1 " . 
                 "order by address_company"; 
 
/******************************************************************** 
    build form 
*********************************************************************/ 
$form = new Form("order_items", "order_item"); 
 
$form->add_section("Project"); 
$form->add_hidden("pid", param("pid")); 
$form->add_hidden("oid",$project["project_order"]); 
$form->add_hidden("order_item_id", id()); 
$form->add_hidden("order_item_order", $project["project_order"]); 
$form->add_hidden("order_item_type", ITEM_TYPE_SPECIAL); 
 
require_once "include/project_head_small.php"; 
 
$form->add_section("Item Information"); 
$form->add_list("order_item_supplier_address", "Supplier*", $sql_suppliers, NOTNULL); 
$form->add_multiline("order_item_text", "Description*", 4, NOTNULL); 
$form->add_edit("order_item_quantity", "Quantity*", NOTNULL); 

$sql = "select project_cost_groupname_id, project_cost_groupname_name " . 
       "from project_cost_groupnames " . 
	   "where project_cost_groupname_active = 1 and project_cost_groupname_id in (2, 10) order by project_cost_groupname_id";

$form->add_list("order_item_cost_group", "Cost Group*",$sql, NOTNULL);

$form->add_hidden("order_item_system_price", 0); 

$sql = "select costmonitoringgroup_id, costmonitoringgroup_text from costmonitoringgroups order by costmonitoringgroup_text";
$form->add_list("order_items_costmonitoring_group", "Type*",$sql, NOTNULL);


$form->add_section("Supplying Options");
$form->add_checkbox("order_item_no_offer_required", "does not require a supplier's offer", "", 0, "Options");
$form->add_checkbox("order_item_only_quantity_proposal", "does only need a supplier's quantity proposal", "");	

$form->add_section("Project Sheet Budget Request");
$form->add_checkbox("order_item_exclude_from_ps", "local production", "", "", "Project Sheet");


$form->add_hidden("order_item_system_price", 0); 

 
 
$form->add_button("save_data", "Save"); 
 
$form->add_button(FORM_BUTTON_BACK, "Back"); 
 
 
/******************************************************************** 
    Populate form and process button clicks 
*********************************************************************/  
$form->populate(); 
$form->process(); 
 
if ($form->button("save_data")) 
{ 
    if ($form->validate()) 
    { 
        project_add_special_item_save($form); 
        $link = "project_edit_material_list.php?pid=" . param("pid");  
        redirect($link); 
    } 
} 
 
    
/******************************************************************** 
    render page 
*********************************************************************/ 
$page = new Page("projects"); 
 
require "include/project_page_actions.php"; 
 
$page->header(); 
$page->title("Add Individual Special Item"); 
$form->render(); 
$page->footer(); 
 
?>
