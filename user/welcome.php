<?php
/********************************************************************


    welcome.php

    Main entry page after successful login.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-31
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2002-10-23
    Version:        1.1.2


    Copyright (c) 2002, Swatch AG, All Rights Reserved.


*********************************************************************/


require_once "../include/frame.php";


if(!user_id() OR !user_id() > 0) {
	redirect('/user/login.php');
}
check_access();

/********************************************************************
    Ask Retail Coordintator/Retail Operator 
	for entering/adapting the agreed POS opening date
*********************************************************************/
require_once "../shared/func_mailalerts.php";

$result = ma_enter_realistic_sopd();
$result = ma_enter_actual_sopd();
$result = ma_resignation_deadline();
$result = ma_lease_start();

/********************************************************************
    get user_roles
*********************************************************************/
$sql = "select * from user_roles where user_role_user =" . dbquote(user_id());
$res = mysql_query($sql);
$filter = "";
while($row = mysql_fetch_assoc($res))
{
    $filter = $filter . " message_roles_role = " . $row["user_role_role"] . " or ";

}
if($filter == "")
{
    $filter = "message_date <= now() and (message_expiry_date > now() or message_expiry_date = '000-00-00' or message_expiry_date is null) ";
}
else
{
    $filter = substr($filter, 0, strlen($filter)-4);
    $filter = "(" . $filter . ") and message_date <= now() and (message_expiry_date > now() or message_expiry_date = '000-00-00' or message_expiry_date is null) ";
}



$sql = "select distinct message_id, message_title, message_date, " .
       "    date_format(message_date, '%d.%m.%y') as f_date, message_important " .
       "from messages " .
       "left join message_roles on message_roles_message = message_id ";


$images = array();
$sql_images = $sql . " where " .$filter;

$res = mysql_query($sql_images) or dberror($sql_images);

while ($row = mysql_fetch_assoc($res))
{
    if($row["message_important"] == 1)
    {
        $images[$row["message_id"]] = "/pictures/neworder.gif";
    }
}


$list = new ListView($sql);


$list->set_entity("messages");
$list->set_filter($filter);
$list->set_order("message_date desc");

$list->add_image_column("important", "", 0, $images);

$popup_link = "popup:welcome_message.php?id={message_id}";
$list->add_column("f_date", "Date", "", LIST_FILTER_FREE);
$list->add_column("message_title", "Retail News", $popup_link, LIST_FILTER_FREE);


$list->process();


// Get User-Name for greeting


$res = mysql_query("select user_firstname, user_name " .
                   "from users " .
                   "where user_id=" . user_id());


if (mysql_num_rows($res) == 0)
{
    error("Invalid user_id \"" . id() . "\" (" . user_id() . ").");
}
$row = mysql_fetch_assoc($res);


$page = new Page("welcome");
$page->header();
$page->title("Welcome");


echo "<p>", "Hello, " . $row['user_firstname'] . " " . $row['user_name'].  ", welcome to ". APPLICATION_NAME .".</p>";


$list->render();
$page->footer();


?>