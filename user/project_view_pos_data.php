<?php
/********************************************************************

    project_view_pos_data.php

    Edit POS Data

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-03-12
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-03-12
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "../shared/func_posindex.php";

check_access("can_view_pos_data");

/********************************************************************
    prepare all data needed
*********************************************************************/

// read project and order details
$project = get_project(param("pid"));

$standard_dsitribution_channel = get_standard_distribution_channel($project["project_cost_type"], $project["project_postype"], $project["project_pos_subclass"]);




// get company's address
$client_address = get_address($project["order_client_address"]);
$franchisee_address = get_address($project["order_franchisee_address_id"]);

if(count($franchisee_address) == 0)
{
	$franchisee_address["place_id"] = 0;
}

// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ".
                 "order by country_name";



//get pos data
$table = "posaddresses";
$table2 = "posareas";

if($project["pipeline"] == 0)
{
	$pos_data = get_poslocation($project["posaddress_id"], "posaddresses");
	$pos_order_sql = "select * from posorders where posorder_order = " . $project["order_id"];
}
elseif($project["pipeline"] == 1)
{
	$pos_data = get_poslocation_from_pipeline($project["posaddress_id"], $project["order_id"]);
	$table = "posaddressespipeline";
	$table2 = "posareaspipeline";

	$pos_order_sql = "select * from posorderspipeline where posorder_order = " . $project["order_id"];
}

//neighbourhood
$neighbourhoods = array();
$res = mysql_query($pos_order_sql) or dberror($pos_order_sql);
if ($row = mysql_fetch_assoc($res))
{
	$neighbourhoods["Shop on Left Side"] = $row["posorder_neighbour_left"];
	$neighbourhoods["Shop on Right Side"] = $row["posorder_neighbour_right"];
	$neighbourhoods["Shop Across Left Side"] = $row["posorder_neighbour_acrleft"];
	$neighbourhoods["Shop Across Right Side"] = $row["posorder_neighbour_acrright"];
	$neighbourhoods["Other Brands in Area"] = $row["posorder_neighbour_brands"];
}	

//posareas
$posareas = array();
$sql = "select posareatype_id, posareatype_name " . 
	   "from posareatypes " . 
	   " order by posareatype_name";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$posareas[$row["posareatype_id"]]	= $row["posareatype_name"];
}

$pos_posareas = array();

if(count($pos_data) > 0)
{
	$sql = "select posarea_area " . 
		   "from $table2 " . 
		   "where posarea_posaddress = " . dbquote($pos_data["posaddress_id"]) . 
		   " order by posarea_area";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$pos_posareas[$row["posarea_area"]]	= $row["posarea_area"];
	}
}


//get former franchisee address
$former_franchisee_address = array();
if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 3)
{
	
	$sql = "select order_franchisee_address_id " . 
		   "from posorders " . 
		   "left join orders on order_id = posorder_order " .
		   "where posorder_type = 1 and (posorder_project_kind = 1 or posorder_project_kind = 2) " . 
		   " and posorder_posaddress = " . $project["posaddress_id"] . 
		   " and order_actual_order_state_code = '820' " . 
		   " order by posorder_id DESC";	

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$former_franchisee_address = get_address($row["order_franchisee_address_id"]);

	}
}


$ratings = array();
$ratings[5] = "bad";
$ratings[4] = "poor";
$ratings[3] = "good";
$ratings[2] = "very good";
$ratings[1] = "excellent";


/********************************************************************
    Get Cost Monitoring Sheet Budget Data
*********************************************************************/ 
$sql =  "select * from project_costs " .
        "where project_cost_order = " . $project["project_order"];

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

$project_cost_sqms = $row["project_cost_sqms"];
$project_cost_gross_sqms = $row["project_cost_gross_sqms"];
$project_cost_totalsqms = $row["project_cost_totalsqms"];
$project_cost_bakoffocesqms = $row["project_cost_backofficesqms"];
$project_cost_numfloors = $row["project_cost_numfloors"];
$project_cost_floorsurface1 = $row["project_cost_floorsurface1"];
$project_cost_floorsurface2 = $row["project_cost_floorsurface2"];
$project_cost_floorsurface3 = $row["project_cost_floorsurface3"];

//get total surface from posorders in case it is zero
if(!$project_cost_gross_sqms and array_key_exists("posaddress_store_grosssurface", $pos_data))
{
	$project_cost_gross_sqms = $pos_data["posaddress_store_grosssurface"];
}
if(!$project_cost_totalsqms and array_key_exists("posaddress_store_totalsurface", $pos_data))
{
	$project_cost_totalsqms = $pos_data["posaddress_store_totalsurface"];
}
if(!$project_cost_bakoffocesqms and array_key_exists("posaddress_store_backoffice", $pos_data))
{
	$project_cost_bakoffocesqms = $pos_data["posaddress_store_backoffice"];
}
if(!$project_cost_numfloors and array_key_exists("posaddress_store_numfloors", $pos_data))
{
	$project_cost_numfloors = $pos_data["posaddress_store_numfloors"];
}
if(!$project_cost_floorsurface1 and array_key_exists("posaddress_store_floorsurface1", $pos_data))
{
	$project_cost_floorsurface1 = $pos_data["posaddress_store_floorsurface1"];
}
if(!$project_cost_floorsurface2 and array_key_exists("posaddress_store_floorsurface2", $pos_data))
{
	$project_cost_floorsurface2 = $pos_data["posaddress_store_floorsurface2"];
}
if(!$project_cost_floorsurface3 and array_key_exists("posaddress_store_floorsurface3", $pos_data))
{
	$project_cost_floorsurface3 = $pos_data["posaddress_store_floorsurface3"];
}



/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

$form->add_section("Project");
$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid", $project["project_order"]);
$form->add_hidden("pipeline", $project["pipeline"]);


//Project Head
$order_state_name = get_actual_order_state_name($project["order_actual_order_state_code"], 1);

$form->add_label("project_number", "Project Number", 0, $project["project_number"]);
$form->add_label("treatment_state", "Treatment State", 0, $project["project_state_text"]);
$form->add_lookup("product_line", "Product Line", "product_lines", "product_line_name", 0, $project["project_product_line"]);
$form->add_lookup("product_line_subclass", "Product Line Subclass", "productline_subclasses", "productline_subclass_name", 0, $project["project_product_line_subclass"]);
$form->add_lookup("project_postype", "POS Type", "postypes", "postype_name", 0, $project["project_postype"]);
$form->add_lookup("project_pos_subclass", "POS Type Subclass", "possubclasses", "possubclass_name", NOTNULL, $project["project_pos_subclass"]);

$form->add_label("type3", "Project Legal Type", 0, $project["project_costtype_text"]);
$form->add_label("type2", "Project Kind", 0, $project["projectkind_name"]);

$form->add_label("order_date", "Project Starting Date", 0, to_system_date($project["order_date"]));

if($project["project_projectkind"] != 4 and $project["project_projectkind"] != 5) //Take Over and lease renewal
{
	$form->add_label("planned_opening_date", "Client's preferred Shop Opening Date", 0, to_system_date($project["project_planned_opening_date"]));
}

$form->add_label("real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]));
$form->add_label("actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]));

$form->add_label("status", "Status", 0, $project["order_actual_order_state_code"]  . " " . $order_state_name);


$line = "concat(user_name, ' ', user_firstname)";
if ($project["project_retail_coordinator"])
{
    $form->add_lookup("retail_coordinator", "Project Manager", "users", $line, 0, $project["project_retail_coordinator"]);
}
else
{
    $form->add_label("project_retail_coordinator", "Project Manager");
}


if ($project["order_retail_operator"])
{
    $form->add_lookup("retail_operator", "Retail Operator", "users", $line, 0, $project["order_retail_operator"]);
}
else
{
    $form->add_label("order_retail_operator", "Retail Operator");
}


$client = $client_address["company"] . ", " .
          $client_address["zip"] . " " . $client_address["place"] . ", " .
          $client_address["country_name"];


$form->add_label("client_address", "Client", 0, $client);


if($project["project_cost_type"] == 1 and ($project["project_projectkind"] == 1
or $project["project_projectkind"] == 3 or $project["project_projectkind"] == 4)) // new corporate, take ober, tekeover renovation
{
	$form->add_label("eprepnr", "Enterprise Reporting Number:", 0, $pos_data["posaddress_eprepnr"]);
}
else
{
	//$form->add_label("lala", $pos_data["posaddress_eprepnr"]);
	$form->add_hidden("eprepnr", $pos_data["posaddress_eprepnr"]);
}



//End Project Head

$form->add_section("POS Location Address");

if(count($pos_data) > 0)
{
	$form->add_hidden("posaddress_id", $pos_data["posaddress_id"]);
	
	
	$form->add_label("shop_address_company", "Project Name", 0, $project["order_shop_address_company"]);
	$form->add_label("shop_address_company2", "", 0, $project["order_shop_address_company2"]);
	$form->add_label("shop_address_address", "Address", 0, $project["order_shop_address_address"]);
	$form->add_label("shop_address_address2", "", 0, $project["order_shop_address_address2"]);
	$form->add_label("shop_address_zip", "ZIP*", 0, $project["order_shop_address_zip"]);
	
	$form->add_label("shop_address_place", "City", 0 , $project["order_shop_address_place"]);
	$form->add_label("province", "Province", 0, $pos_data["province_canton"]);
	
	$form->add_lookup("shop_address_country_name", "Country", "countries", "country_name", 0, $project["order_shop_address_country"]);

	$form->add_label("shop_address_phone", "Phone", 0, $project["order_shop_address_phone"]);
	$form->add_label("shop_address_fax", "Fax", 0, $project["order_shop_address_fax"]);
	$form->add_label("shop_address_email", "Email", 0, $project["order_shop_address_email"]);
	$form->add_label("sapnr", "SAP Number", 0, $pos_data["posaddress_sapnumber"]);



	$form->add_section("Surfaces");
	$form->add_label("shop_gross_sqms", "Gross Surface in sqms", 0, $project_cost_gross_sqms);
	$form->add_label("shop_totalsqms", "Total Surface in sqms", 0, $project_cost_totalsqms);
	$form->add_label("shop_sqms", "Sales Surface in sqms", 0, $project_cost_sqms);
	$form->add_label("shop_bakoffocesqms", "Back Office Surface in sqms", 0, $project_cost_bakoffocesqms);
	$form->add_label("shop_numfloors", "Number of Floors*", 0, $project_cost_numfloors);
	$form->add_label("shop_floorsurface1", "Floor 1: Surface in sqms", 0, $project_cost_floorsurface1);
	$form->add_label("shop_floorsurface2", "Floor 2: Surface in sqms", 0, $project_cost_floorsurface2);
	$form->add_label("shop_floorsurface3", "Floor 3: Surface in sqms", 0, $project_cost_floorsurface3);


	$form->add_section("Sales");
	if($project["project_distribution_channel"])
	{
		$form->add_lookup("distribution_channel", "Distribution Channel", "mps_distchannels", "concat(mps_distchannel_group, ' - ', mps_distchannel_name, ' - ',mps_distchannel_code)", 0, $project["project_distribution_channel"]);
	}
	else
	{
		$form->add_lookup("distribution_channel", "Distribution Channel", "mps_distchannels", "concat(mps_distchannel_group, ' - ', mps_distchannel_name, ' - ',mps_distchannel_code)", 0, $standard_dsitribution_channel);
	}


	$form->add_section("Environment");
	foreach($posareas as $key=>$name)
	{
		if(array_key_exists($key, $pos_posareas))
		{
			$form->add_checkbox("area". $key, $name, 1, 0, "");
		}
		else
		{
			$form->add_checkbox("area". $key, $name, "", 0, "");
		}

	}

	$form->add_section("Area Perception");

	if(count($pos_data) > 0 and array_key_exists($pos_data["posaddress_perc_class"], $ratings))
	{
		$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, $pos_data["posaddress_perc_class"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, $pos_data["posaddress_perc_tourist"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings,  $pos_data["posaddress_perc_transport"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings,  $pos_data["posaddress_perc_people"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings,  $pos_data["posaddress_perc_parking"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings,  $pos_data["posaddress_perc_visibility1"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from accross the Street*", $ratings,  $pos_data["posaddress_perc_visibility2"], NOTNULL);
	}
	else
	{
		$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from accross the Street*", $ratings, 0, NOTNULL);
	}

	$form->add_section("Neighbourhood");
	$form->add_label("posorder_neighbour_left", "Shop on Left Side", 0, $neighbourhoods["Shop on Left Side"]);
	$form->add_label("posorder_neighbour_right", "Shop on Right Side", 0, $neighbourhoods["Shop on Right Side"]);
	$form->add_label("posorder_neighbour_acrleft", "Shop Across Left Side", 0, $neighbourhoods["Shop Across Left Side"]);
	$form->add_label("posorder_neighbour_acrright", "Shop Across Right Side", 0, $neighbourhoods["Shop Across Right Side"]);
	$form->add_label("posorder_neighbour_brands", "Other Brands in Area", RENDER_HTML, str_replace("\n", "<br />", $neighbourhoods["Other Brands in Area"]));

}
else
{
	$form->add_hidden("posaddress_id", $project["posaddress_id"]);
	

	$form->add_label("shop_address_company", "Project Name*", 0, $project["order_shop_address_company"]);
	$form->add_label("shop_address_company2", "", 0, $project["order_shop_address_company2"]);
	$form->add_label("shop_address_address", "Address*", 0, $project["order_shop_address_address"]);
	$form->add_label("shop_address_address2", "", 0, $project["order_shop_address_address2"]);
	$form->add_label("shop_address_zip", "ZIP*", 0, $project["order_shop_address_zip"]);
	
	$province = "";
	$sql = "select place_id, province_canton " .
	       "from places " .
		   "left join provinces on province_id = place_province " . 
		   "where place_country = " . dbquote($project["order_shop_address_country"]) . 
		   " and place_name = " . dbquote($project["order_shop_address_place"]);
	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$province = $row["province_canton"];
	}
	
	
	$form->add_label("shop_address_place", "City*", 0, $project["order_shop_address_place"]);
	$form->add_label("province", "Province", 0, $province);

	$form->add_lookup("shop_address_country_name", "Country", "countries", "country_name", 0, $project["order_shop_address_country"]);

	$form->add_label("shop_address_phone", "Phone", 0, $project["order_shop_address_phone"]);
	$form->add_label("shop_address_fax", "Fax", 0, $project["order_shop_address_fax"]);
	$form->add_label("shop_address_email", "Email", 0, $project["order_shop_address_email"]);

	
	$form->add_section("Surfaces");
	$form->add_label("shop_gross_sqms", "Gross Surface in sqms", 0, $project_cost_gross_sqms);
	$form->add_label("shop_totalsqms", "Total Surface in sqms", 0, $project_cost_totalsqms);
	$form->add_label("shop_sqms", "Sales Surface in sqms", 0, $project_cost_sqms);
	$form->add_label("shop_bakoffocesqms", "Back Office Surface in sqms", 0, $project_cost_bakoffocesqms);
	$form->add_label("shop_numfloors", "Number of Floors*", 0, $project_cost_numfloors);
	$form->add_label("shop_floorsurface1", "Floor 1: Surface in sqms", 0, $project_cost_floorsurface1);
	$form->add_label("shop_floorsurface2", "Floor 2: Surface in sqms", 0, $project_cost_floorsurface2);
	$form->add_label("shop_floorsurface3", "Floor 3: Surface in sqms", 0, $project_cost_floorsurface3);


	
	$form->add_section("Sales");
	if($project["project_distribution_channel"])
	{
		$form->add_lookup("distribution_channel", "Distribution Channel", "mps_distchannels", "concat(mps_distchannel_group, ' - ', mps_distchannel_name, ' - ',mps_distchannel_code)", 0, $project["project_distribution_channel"]);
	}
	else
	{
		$form->add_lookup("distribution_channel", "Distribution Channel", "mps_distchannels", "concat(mps_distchannel_group, ' - ', mps_distchannel_name, ' - ',mps_distchannel_code)", 0, $standard_dsitribution_channel);
	}

	$form->add_section("Environment");
	foreach($posareas as $key=>$name)
	{
		if(array_key_exists($key, $pos_posareas))
		{
			$form->add_checkbox("area". $key, $name, 1, 0, "");
		}
		else
		{
			$form->add_checkbox("area". $key, $name, "", 0, "");
		}

	}

	$form->add_section("Area Perception");
	$form->add_comment("Please rate by clicking a star: 5 stars are the best rating.");
	$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from accross the Street*", $ratings, 0, NOTNULL);

	
	$form->add_section("Neighbourhood");
	$form->add_label("posorder_neighbour_left", "Shop on Left Side", 0, $neighbourhoods["Shop on Left Side"]);
	$form->add_label("posorder_neighbour_right", "Shop on Right Side", 0, $neighbourhoods["Shop on Right Side"]);
	$form->add_label("posorder_neighbour_acrleft", "Shop Across Left Side", 0, $neighbourhoods["Shop Across Left Side"]);
	$form->add_label("posorder_neighbour_acrright", "Shop Across Right Side", 0, $neighbourhoods["Shop Across Right Side"]);
	$form->add_label("posorder_neighbour_brands", "Other Brands in Area", RENDER_HTML, str_replace("\n", "<br />", $neighbourhoods["Other Brands in Area"]));
}






if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 3)
{
	if(count($former_franchisee_address) > 0)
	{
		$form->add_section("Former Franchisee");

		$form->add_label("former_franchisee_address_company", "Company", 0, $former_franchisee_address["company"]);
		if($former_franchisee_address["company2"]) {
			$form->add_label("former_franchisee_address_company2", "", 0, $former_franchisee_address["company2"]);
		}
		$form->add_label("former_franchisee_address_address", "Address", 0, $former_franchisee_address["address"]);
		
		if($former_franchisee_address["address2"]) {
			$form->add_label("former_franchisee_address_address2","", 0, $former_franchisee_address["address2"]);
		}
		$form->add_label("former_franchisee_address_zip", "ZIP", 0, $former_franchisee_address["zip"]);
		$form->add_label("former_franchisee_address_place", "Place", 0, $former_franchisee_address["place"]);
		$form->add_label("former_franchisee_address_country", "Country", 0, $former_franchisee_address["country_name"]);
	}
}

if($project["project_cost_type"] != 6)
{
	$form->add_section("Franchisee Address");
}
else
{
	$form->add_section("Address of Owner Company");
}

$form->add_label("franchisee_address_company", "Company", 0, $franchisee_address["company"]);
$form->add_label("franchisee_address_company2", "", 0, $franchisee_address["company2"]);
$form->add_label("franchisee_address_address", "Address", 0, $franchisee_address["address"]);
$form->add_label("franchisee_address_address2", "", 0, $franchisee_address["address2"]);

$form->add_lookup("franchisee_address_country", "Country", "countries", "country_name", 0,  $franchisee_address["country"]);


$form->add_label("franchisee_address_place", "City", DISABLED, $franchisee_address["place"]);

$form->add_label("franchisee_address_zip", "ZIP", 0, $franchisee_address["zip"]);



$form->add_label("franchisee_address_phone", "Phone", 0, $franchisee_address["phone"]);
$form->add_label("franchisee_address_fax", "Fax", 0, $franchisee_address["fax"]);
$form->add_label("franchisee_address_email", "Email", 0, $franchisee_address["email"]);
$form->add_label("franchisee_address_contact", "Contact", 0, $franchisee_address["contact_name"]);
$form->add_label("franchisee_address_website", "Website", 0, $franchisee_address["website"]);


if($project["project_actual_opening_date"] == NULL or $project["project_actual_opening_date"] == '0000-00-00')
{
	if(count($pos_data) > 0 and array_key_exists("posaddress_fagagreement_type", $pos_data) and  $pos_data["posaddress_fagagreement_type"] > 0 and $project["project_projectkind"] ==  2) // renovation projects
	{

		$type = "";
		$sent = "";
		$signed = "";
		$start = "";
		$end = "";
		$comment = "";
	}
	else
	{
		$type = $project["project_fagagreement_type"];
		$sent = $project["project_fagrsent"];
		$signed = $project["project_fagrsigned"];
		$start = $project["project_fagrstart"];
		$end = $project["project_fagrend"];
		$comment = $project["project_fag_comment"];
	}
	
	$form->add_section("Franchisee Agreement");
	$form->add_lookup("project_fagagreement_type", "Agreement Type", "agreement_types", "agreement_type_name", 0,  $type);
	$form->add_checkbox("project_fagrsent", "Sent", $sent, 0, "Franchisee Agreement");
	$form->add_checkbox("project_fagrsigned", "Signed", $signed, 0, "Franchisee Agreement");
	$form->add_label("dummy", "");
	$form->add_label("project_fagrstart", "Agreement Starting Date", 0, to_system_date($start), TYPE_DATE);
	$form->add_label("project_fagrend", "Agreement Ending Date", 0, to_system_date($end), TYPE_DATE);
	$form->add_label("project_fag_comment", "Comment", 4, 0, str_replace("\n", "<br />", $comment));
	
}
elseif (has_access("can_edit_franchisee_agreement_data"))
{
	if(count($pos_data) >0  and array_key_exists("posaddress_fagagreement_type", $pos_data) and  $pos_data["posaddress_fagagreement_type"] > 0)
	{
		$type = $pos_data["posaddress_fagagreement_type"];
		$sent = $pos_data["posaddress_fagrsent"];
		$signed = $pos_data["posaddress_fagrsigned"];
		$start = $pos_data["posaddress_fagrstart"];
		$end = $pos_data["posaddress_fagrend"];
		$comment = $pos_data["posaddress_fag_comment"];
	}
	else
	{
		$type = $project["project_fagagreement_type"];
		$sent = $project["project_fagrsent"];
		$signed = $project["project_fagrsigned"];
		$start = $project["project_fagrstart"];
		$end = $project["project_fagrend"];
		$comment = $project["project_fag_comment"];
	}
	
	$form->add_section("Franchisee Agreement");
	$form->add_lookup("project_fagagreement_type", "Agreement Type", "agreement_types", "agreement_type_name", 0,  $type);
	$form->add_checkbox("project_fagrsent", "Sent", $sent, 0, "Franchisee Agreement");
	$form->add_checkbox("project_fagrsigned", "Signed", $signed, 0, "Franchisee Agreement");
	$form->add_label("dummy", "");
	$form->add_label("project_fagrstart", "Agreement Starting Date", 0, to_system_date($start), TYPE_DATE);
	$form->add_label("project_fagrend", "Agreement Ending Date", 0, to_system_date($end), TYPE_DATE);
	$form->add_label("project_fag_comment", "Comment", 4, 0, str_replace("\n", "<br />", $comment));
}



if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
{
	$form->add_section("Project Dates");
}
else
{
	$form->add_section("POS Opening Dates");
}

$form->add_label("shop_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]));
$form->add_label("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]));

$form->add_label("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]));



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

    
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("View POS-Data");
$form->render();


$page->footer();


?>