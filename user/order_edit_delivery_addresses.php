<?php
/********************************************************************

    order_edit_delivery_addresses.php

    Edit item delivery addresses

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-11-19
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-02-16
    Version:        1.1.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/order_functions.php";

check_access("can_edit_delivery_addresses_in_orders");

register_param("oid");
set_referer("order_edit_delivery_address.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$order = get_order(param('oid'));

// get company's address
$client_address = get_address($order["order_client_address"]);

// create sql for oder items
$sql_order_items = "select order_item_id, order_item_text, order_item_quantity, ".
                   "    order_item_po_number, order_item_system_price, ".
                   "    if(item_code <>'', item_code, item_type_name) as item_shortcut, ".
                   "    concat_ws(': ', product_line_name, category_name) as group_head, " .
                   "    concat_ws(', ', order_address_company, order_address_place) as delivery_address " .
                   "from order_items ".
                   "left join items on order_item_item = item_id ".
                   "left join categories on order_item_category = category_id ".
                   "left join product_lines on categories.category_product_line = product_lines.product_line_id " .
                   "left join order_addresses on order_address_order_item = order_item_id ".
                   "left join item_types on order_item_type = item_type_id";


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("orders", "order", 640);

$form->add_section("Order");
$form->add_hidden("oid", param('oid'));
$form->add_hidden("order_file_order", param('oid'));

require_once "include/order_head_small.php";

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Create List for Catalog Items
*********************************************************************/ 
$list = new ListView($sql_order_items);

$list->set_title("Items up to Delivery");
$list->set_entity("order_item");
$list->set_filter("order_item_type <= " . ITEM_TYPE_SPECIAL . 
                  "   and order_item_order = " . param('oid') .
                  "   and order_address_type = 2 ");

$list->set_order("category_priority, item_code");
$list->set_group("group_head", "group_head");

$link="order_edit_delivery_address.php?oid=" . param('oid');

$list->add_column("item_shortcut", "Item Code", $link);
$list->add_column("order_item_text", "Name");
$list->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
$list->add_column("delivery_address", "Delivery Address");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("Edit Delivery Addresses");
$form->render();
$list->render();
$page->footer();

?>