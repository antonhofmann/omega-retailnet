<?php
/********************************************************************

    order_edit_delivery_address.php

    Edit item's delivery address

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-11-19
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-02-16
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "include/order_functions.php";

check_access("can_edit_delivery_addresses_in_orders");

/********************************************************************
    prepare all data needed
*********************************************************************/

// read project and order details
$order = get_order(param("oid"));

// read order item details
$order_item = get_order_item(id());


// get company's address
$client_address = get_address($order["order_client_address"]);

// read information from order_addresses
$delivery_address = get_order_item_address(2, param('oid'), id());

// create array for the delivery address listbox
$delivery_addresses = get_delivery_addresses($order["order_client_address"]);

// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ".
                 "order by country_name";


$delivery_address_province_name = "";
if(param("delivery_address_place_id"))
{
	$delivery_address_province_name = get_province_name(param("delivery_address_place_id"));
}
elseif($delivery_address["place_id"])
{
	$delivery_address_province_name = get_province_name($delivery_address["place_id"]);
}


if(param("deliveryg_address_country")) {
	$sql_delivery_places = "select place_id, place_name from places " . 
		          "where place_country = " .  param("deliveryg_address_country") . 
		          " order by place_name";
}
elseif($delivery_address['country']) {
	$sql_delivery_places = "select place_id, place_name from places " . 
		          "where place_country = " .  $delivery_address['country'] . 
		          " order by place_name";
}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("orders", "order", 640);

$form->add_section("Order");
$form->add_hidden("oid", param("oid"));
$form->add_hidden("order_item_order", param('oid'));
$form->add_hidden("order_item_id", id());
$form->add_hidden("order_client_address", $order["order_client_address"]);

$form->add_hidden("order_item_supplier_address", $order_item["order_item_supplier_address"]);
$form->add_hidden("order_item_forwarder_address", $order_item["order_item_forwarder_address"]);

require_once "include/order_head_small.php";



$form->add_section("Item Details");
$form->add_label("order_item_code", "Item Code", 0, $order_item["code"]);
$form->add_label("order_item_text", "", 0, $order_item["text"]);
$form->add_label("order_item_quantity", "Quantity", 0, $order_item["quantity"]);


$tmp_text1="Please indicate the delivery address.\n";
$tmp_text2="You can either select an existing address or enter a new address.";

$form->add_section("Delivery Address (consignee address)");
if (count($delivery_addresses) > 0 )
{
    $form->add_comment($tmp_text1 . $tmp_text2);
    $form->add_list("delivery_address_id", "Delivery Address", $delivery_addresses, SUBMIT);
}
else
{
    $form->add_comment($tmp_text1);
}
if (count($delivery_address) > 0)
{
    $form->add_edit("delivery_address_company", "Company*", NOTNULL, $delivery_address["company"], TYPE_CHAR);
    $form->add_edit("delivery_address_company2", "", 0, $delivery_address["company2"], TYPE_CHAR);
    $form->add_edit("delivery_address_address", "Address*", NOTNULL, $delivery_address["address"], TYPE_CHAR);
    $form->add_edit("delivery_address_address2", "", 0, $delivery_address["address2"], TYPE_CHAR);
    $form->add_edit("delivery_address_zip", "ZIP*", NOTNULL, $delivery_address["zip"], TYPE_CHAR, 20);
    
	$form->add_list("delivery_address_place_id", "City*", $sql_delivery_places, NOTNULL |SUBMIT, $delivery_address['place_id']);
	$form->add_label("delivery_address_place", "City", 0, $delivery_address['place']);
	$form->add_label("delivery_address_province_name", "Province", 0, $delivery_address_province_name);

    $form->add_list("delivery_address_country", "Country*", $sql_countries, NOTNULL, $delivery_address["country"]);
    $form->add_edit("delivery_address_phone", "Phone*", NOTNULL, $delivery_address["phone"], TYPE_CHAR, 20);
    $form->add_edit("delivery_address_fax", "Fax", 0, $delivery_address["fax"], TYPE_CHAR, 20);
    $form->add_edit("delivery_address_email", "Email", 0, $delivery_address["email"], TYPE_CHAR);
    $form->add_edit("delivery_address_contact", "Contact*", NOTNULL, $delivery_address["contact"], TYPE_CHAR);

	$form->add_section(" ");
	$form->add_checkbox("same_supplier", "all items of the same supplier", 0, 0, "Apply Change to");
	$form->add_checkbox("same_forwarder", "all items of the same forwarder", 0, 0);
}
else
{
    $form->add_edit("delivery_address_company", "Company*", NOTNULL, "", TYPE_CHAR);
    $form->add_edit("delivery_address_company2", "", 0, "", TYPE_CHAR);
    $form->add_edit("delivery_address_address", "Address*", NOTNULL,"", TYPE_CHAR);
    $form->add_edit("delivery_address_address2", "", 0, "", TYPE_CHAR);
    $form->add_edit("delivery_address_zip", "ZIP*", NOTNULL, "", TYPE_CHAR, 20);

	$form->add_list("delivery_address_place_id", "City*", $sql_delivery_places, NOTNULL |SUBMIT, $delivery_address['place_id']);
	$form->add_label("delivery_address_place", "City", 0, $delivery_address['place']);
	$form->add_label("delivery_address_province_name", "Province");

    $form->add_list("delivery_address_country", "Country*", $sql_countries, NOTNULL, "");
    $form->add_edit("delivery_address_phone", "Phone*", NOTNULL, "", TYPE_CHAR, 20);
    $form->add_edit("delivery_address_fax", "Fax", 0, "", TYPE_CHAR, 20);
    $form->add_edit("delivery_address_email", "Email", 0, "", TYPE_CHAR);
    $form->add_edit("delivery_address_contact", "Contact*", NOTNULL, "", TYPE_CHAR);
}




$form->add_button("save", "Save Data");
$form->add_button(FORM_BUTTON_BACK, "Back");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


if ($form->button("save"))
{
    if ($form->validate())
    {
        project_update_delivery_address($form);

        $link = "order_edit_delivery_addresses.php?oid=" . param("oid");
        redirect ($link);
    }
}
else if ($form->button("delivery_address_id"))
{
    // set new delivery address
    $form->value("delivery_address_company", "");
    $form->value("delivery_address_company2",  "");
    $form->value("delivery_address_address",  "");
    $form->value("delivery_address_address2",  "");
    $form->value("delivery_address_zip",  "");
    $form->value("delivery_address_place",  "");
	$form->value("delivery_address_place_id",  0);
    $form->value("delivery_address_country",  0);
    $form->value("delivery_address_phone",  "");
    $form->value("delivery_address_fax",  "");
    $form->value("delivery_address_email",  "");
    $form->value("delivery_address_contact",  "");

    if ($form->value("delivery_address_id"))
    {
        $sql = "select * from order_addresses where order_address_id = " . $form->value("delivery_address_id");
        $res = mysql_query($sql) or dberror($sql);

        if ($row = mysql_fetch_assoc($res))
        {
            
			
			$form->value("delivery_address_company", $row["order_address_company"]);
            $form->value("delivery_address_company2",  $row["order_address_company2"]);
            $form->value("delivery_address_address",  $row["order_address_address"]);
            $form->value("delivery_address_address2",  $row["order_address_address2"]);
            $form->value("delivery_address_zip",  $row["order_address_zip"]);
            $form->value("delivery_address_place",  $row["order_address_place"]);
			$form->value("delivery_address_place_id",  $row["order_address_place_id"]);
            $form->value("delivery_address_country",  $row["order_address_country"]);
            $form->value("delivery_address_phone",  $row["order_address_phone"]);
            $form->value("delivery_address_fax",  $row["order_address_fax"]);
            $form->value("delivery_address_email",  $row["order_address_email"]);
            $form->value("delivery_address_contact",  $row["order_address_contact"]);

			$sql = "select province_canton from places left join provinces on province_id = place_id " . 
				   "where place_id = " . dbquote($row["order_address_place_id"]);
			
			$res = mysql_query($sql) or dberror($sql);

			if ($row = mysql_fetch_assoc($res))
			{
				$form->value("delivery_address_province_name", $row["province_canton"]);
			}
        }
    }
}
elseif($form->button("delivery_address_place_id"))
{
	$sql= "select place_name, province_canton ".
		  "from places " .
		  "left join provinces on province_id = place_province " .
		  "where place_id = " . dbquote($form->value("delivery_address_place_id"));

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$form->value('delivery_address_place', $row['place_name']);
		$form->value("delivery_address_province_name", $row["province_canton"]);
	}
}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("Edit Item: Delivery Address");
$form->render();
$page->footer();

?>