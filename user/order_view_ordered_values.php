<?php
/********************************************************************

    order_view_ordered_values.php

    View projects's itemlist in supplier's prices and currency

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2004-06-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-06-27
    Version:        1.0.0

    Copyright (c) 2004, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "include/order_functions.php";

check_access("can_view_ordered_values_in_orders");

register_param("oid");
set_referer("order_edit_supplier_data_item.php");
set_referer("order_new.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());

// read order details
$order = get_order(param("oid"));

// get company's address
$client_address = get_address($order["order_client_address"]);

// create sql for oder items
$sql_order_items = "select order_item_id, order_item_text, order_item_quantity, item_stock_property_of_swatch, ".
                   "    order_item_supplier_price, order_item_po_number, " .
                   "    concat(RIGHT(order_item_ordered, 2), '.', MONTH(order_item_ordered), '.',  YEAR(order_item_ordered)) as order_date, ".
                   "    if(order_item_item <>'', item_code, item_type_name) as item_code, unit_name, " .
                   "    TRUNCATE(order_item_supplier_price * order_item_quantity, 2) as total_price, ".
                   "    addresses.address_company as supplier_company, ".
                   "    currency_symbol, ".
                   "    concat(addresses.address_company, ', ', currency_symbol) as group_head " .
                   "from order_items ".
                   "left join items on order_item_item = item_id ".
                   "left join item_types on order_item_type = item_type_id ".
                   "left join addresses on order_item_supplier_address = addresses.address_id ".
                   "left join currencies on order_item_supplier_currency = currency_id " .
				   "left join units on unit_id = item_unit";



if (has_access("has_access_to_all_supplier_data_in_orders"))
{
    $list_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) and ".
                      "order_item_type <= " . ITEM_TYPE_SPECIAL .
                      "   and order_item_order = " . param('oid');
}
else
{
    $list_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) and ".          
                      "order_item_type <= " . ITEM_TYPE_SPECIAL .
                      "   and order_item_order = " . param('oid') .
                      "   and order_item_supplier_address = " . $user_data["address"];
}

// build group_totals of suppliers
$group_totals = get_group_total_of_suppliers(param('oid'), "order_item_supplier_price");


//get invoice addresses
$invoice_adresses = array();
$suppliers = array();

$sql_suppliers = "select DISTINCT order_item_supplier_address, address_id, address_company " . 
				 "from order_items " . 
				 "left join addresses on order_item_supplier_address = address_id ".
				 " where " . $list_filter;

$res = mysql_query($sql_suppliers) or dberror($sql_suppliers);


while ($row = mysql_fetch_assoc($res))
{
	$suppliers[$row["address_id"]] = $row["address_company"]; 
	$sql = "select supplier_client_invoice_id ". 
		   "from supplier_client_invoices " . 
		   "where supplier_client_invoice_supplier = " . $row["order_item_supplier_address"] .
		   " and supplier_client_invoice_client = " . $client_address["id"]  . 
		   " and supplier_client_invoice_startdate <= " . dbquote(date("Y-m-d")) . 
			   " and supplier_client_invoice_enddate >= " . dbquote(date("Y-m-d"));
	
	$res_s = mysql_query($sql) or dberror($sql);
	if ($row_s = mysql_fetch_assoc($res_s))
	{
		//get invoice_address
		$sql_i = "select invoice_address_company, invoice_address_company2, order_direct_invoice_address_id, " . 
			     "invoice_address_address, invoice_address_address2, invoice_address_zip, " . 
			     "place_name, country_name, invoice_address_phone, invoice_address_fax, " .
			     "invoice_address_email, invoice_address_contact_name " . 
			     "from orders " .
			     "inner join invoice_addresses on invoice_address_id = order_direct_invoice_address_id " . 
			     "inner join countries on country_id = invoice_address_country_id ". 
			     "inner join places on place_id = invoice_address_place_id " . 
			     "where order_id = " . param("oid");
		
		
		$res_i = mysql_query($sql_i) or dberror($sql_i);
		if ($row_i = mysql_fetch_assoc($res_i))
		{
			$iaddress = $row_i["invoice_address_company"];

			if($row_i["invoice_address_company2"])
			{
				$iaddress .= ", " . $row_i["invoice_address_company2"];
			}

			if($row_i["invoice_address_address"])
			{
				$iaddress .= ", " . $row_i["invoice_address_address"];
			}

			if($row_i["invoice_address_address2"])
			{
				$iaddress .= ", " . $row_i["invoice_address_address2"];
			}

			$iaddress .= ", " . $row_i["invoice_address_zip"] . " " . $row_i["place_name"];
			$iaddress .= ", " . $row_i["country_name"];
			
			
			$invoice_adresses [$row["address_id"]] = $iaddress;
		
		}
		else
		{
			$iaddress = $client_address["company"];

			if($client_address["company2"])
			{
				$iaddress .= ", " . $client_address["company2"];
			}

			if($client_address["address"])
			{
				$iaddress .= ", " . $client_address["address"];
			}

			if($client_address["address2"])
			{
				$iaddress .= ", " . $client_address["address2"];
			}

			$iaddress .= ", " . $client_address["zip"] . " " . $client_address["place"];
			$iaddress .= ", " . $client_address["country_name"];

			$invoice_adresses [$row["address_id"]] = $iaddress;
		}
	}
	else
	{
		//get invoice_address
		$sql_i = "select address_company, address_company2,  " . 
			     "address_address, address_address2, address_zip, " . 
			     "place_name, country_name, address_phone, address_fax, " .
			     "address_email, address_contact_name " . 
			     "from addresses " .
			     "inner join countries on country_id = address_country ". 
			     "inner join places on place_id = address_place_id " . 
			     "where address_is_standard_invoice_address = 1";
		
		
		$res_i = mysql_query($sql_i) or dberror($sql_i);
		if ($row_i = mysql_fetch_assoc($res_i))
		{
			$iaddress = str_replace(" - General", "", $row_i["address_company"]);

			if($row_i["address_company2"])
			{
				$iaddress .= ", " . $row_i["address_company2"];
			}

			if($row_i["address_address"])
			{
				$iaddress .= ", " . $row_i["address_address"];
			}

			if($row_i["address_address2"])
			{
				$iaddress .= ", " . $row_i["address_address2"];
			}

			$iaddress .= ", " . $row_i["address_zip"] . " " . $row_i["place_name"];
			$iaddress .= ", " . $row_i["country_name"];
			
			$invoice_adresses [$row["address_id"]] = $iaddress;
		
		}
	}
}

asort($suppliers);
asort($invoice_adresses);


$property = array();

$res = mysql_query($sql_order_items . " where " . $list_filter) or dberror($sql_order_items . " where " . $list_filter);


while ($row = mysql_fetch_assoc($res))
{
    if($row["item_stock_property_of_swatch"] == 1)
	{
		$property[$row["order_item_id"]] = "/pictures/omega.gif";
	}
}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("orders", "order");

$form->add_hidden("oid", param("oid"));

require_once "include/order_head_small.php";

$form->add_section("Suppliers send Invoices to the following Address");
foreach($suppliers as $id=>$companyname)
{
	$form->add_label("S" . $id, $companyname, RENDER_HTML, $invoice_adresses[$id]);
}


$link = "order_view_ordered_values_pdf.php?oid=" . param("oid");
$link = "javascript:popup('". $link . "', 800, 600)";
$form->add_button("print", "Print PDF", $link);


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Create Item List
*********************************************************************/ 
$list = new ListView($sql_order_items, LIST_HAS_SEPARATOR | LIST_HAS_HEADER);

$list->set_title("Items Ordered");
$list->set_entity("order_item");
$list->set_group("group_head");

$list->set_filter($list_filter);
$list->set_order("order_item_type, item_code");

$list->add_column("item_code", "Item Code", "", "", "", COLUMN_BREAK);
$list->add_image_column("property", "", 0, $property);
$list->add_column("order_item_text", "Name", "", "", "", COLUMN_BREAK);
$list->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_BREAK);
$list->add_column("order_date", "Ordered", "", "", "", COLUMN_BREAK);
$list->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_BREAK);
$list->add_column("unit_name", "Unit", "", "", "", COLUMN_NO_WRAP);
$list->add_column("order_item_supplier_price", "Price", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_BREAK);
$list->add_column("total_price", "Total", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_BREAK );


// set group totals
foreach ($group_totals["currency"] as $key=>$value)
{
    $list->set_group_footer("order_item_supplier_price", $key , $value);
}
foreach ($group_totals["total"] as $key=>$value)
{
    $list->set_group_footer("total_price", $key , $value);
}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("View Ordered Values in Supplier's Prices");
$form->render();
$list->render();

$page->footer();

?>