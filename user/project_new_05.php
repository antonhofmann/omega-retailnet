<?php
/********************************************************************

    project_new05.php

    Creation of a new project step 05.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-07-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-07-14
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_create_new_projects");

if(!isset($_SESSION["new_project_step_3"]))
{
	$link = "project_new_01.php";
	redirect($link);
}

if(count($_POST) == 0 and isset($_SESSION["new_project_step_5"]))
{
	$_SESSION["new_project_step_5"]["action"] = "";
	foreach($_SESSION["new_project_step_5"] as $key=>$value)
	{
		register_param($key);
		param($key, $value);
	}
}
else
{
	$_SESSION["new_project_step_5"] = $_POST;	
}

/********************************************************************
    prepare all data needed
*********************************************************************/
$roles = get_user_roles(user_id());

// get the region of users' address
$user_region = get_user_region(user_id());
// get user data
$user = get_user(user_id());

// get users' company address
$address = get_address($user["address"]);


// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ".
                 "order by country_name";


// create sql for the transportation type listbox
$sql_transportation_modes = "select transportation_type_id, transportation_type_name ".
                            "from transportation_types ".
                            "where transportation_type_visible = 1 " .
							" and transportation_type_code = 'mode' " . 
                            "order by transportation_type_name";


$sql_transportation_arranged = "select transportation_type_id, transportation_type_name ".
								"from transportation_types ".
								"where transportation_type_visible = 1 " .
								" and transportation_type_code = 'arranged' " . 
								"order by transportation_type_name";

$choices = array();

//standard delivery_addresses
$sql_delivery_addresses = "select * " . 
                          "from standard_delivery_addresses " . 
						  "where delivery_address_address_id = " . $user["address"] . 
						  " order by delivery_address_company";

$res = mysql_query($sql_delivery_addresses);

while ($row = mysql_fetch_assoc($res))
{
	$choices["s" . $row["delivery_address_id"]] = $row["delivery_address_company"] . ", " . $row["delivery_address_place"];
}

$choices[1] = "POS location";
$choices[2] = $address['company'];


if($_SESSION["new_project_step_1"]["project_cost_type"] == 2 or $_SESSION["new_project_step_1"]["project_cost_type"] == 5) // franchisee or cooperation brand 
{
	if(array_key_exists('franchisee_address_company', $_SESSION["new_project_step_2"]))
	{
		$choices[3] = "Franchisee address";
	}

}

$choices[4] = "Other";


//get invoice_addresses
$invoice_addresses = array();

$sql_inv = "select invoice_address_id, concat(invoice_address_company, ', ' ,place_name, ', ', country_name) as company " .
    "from invoice_addresses " .
	"left join places on place_id = invoice_address_place_id " . 
	"left join countries on country_id = invoice_address_country_id " . 
	"where invoice_address_active = 1 " . 
	" and invoice_address_address_id = " . $_SESSION["new_project_step_2"]["client_address_id"] . 
    " order by invoice_address_company";

$res = mysql_query($sql_inv) or dberror($sql_inv);
while ($row = mysql_fetch_assoc($res))
{
	$invoice_addresses[$row["invoice_address_id"]] = $row["company"];
}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("", "project");

$form->add_section("POS Location");
$posaddress = $_SESSION["new_project_step_1"]["shop_address_company"] . ", " . $_SESSION["new_project_step_1"]["shop_address_zip"]. " " . $_SESSION["new_project_step_1"]["shop_address_place"];
$form->add_comment($posaddress);


if(count($invoice_addresses) > 0)
{
	$invoice_addresses[0] = $_SESSION["new_project_step_2"]["client_address_company"];
	
	$form->add_section("Notify Address (invoice address)");
	$form->add_comment("Please indicate the notify address (invoice address) in case suppliers do send invoices directly to your company.");
	$form->add_list("invoice_address_id", "Notify address", $invoice_addresses);
}
else
{
	$form->add_hidden("invoice_address_id", 0);
}


$form->add_section("Delivery Address (consignee address)");
$tmp_text1="Please indicate or enter the delivery address.";

$form->add_charlist("delivery_is_address", "Delivery address is identical to*", $choices, SUBMIT);
$form->add_hidden("delivery_address_id");

if(param("delivery_is_address") and param("delivery_is_address") == 4)
{
	$form->add_edit("delivery_address_company", "Company*", NOTNULL, "", TYPE_CHAR);
	$form->add_edit("delivery_address_company2", "", 0, "", TYPE_CHAR);
	
	$form->add_list("delivery_address_country", "Country*", $sql_countries, NOTNULL | SUBMIT);


	if(param("delivery_address_country")) {
		$sql_provinces = "select province_id, province_canton from provinces where province_country = " . param("delivery_address_country") . " order by province_canton";
	}
	else
	{
		$sql_provinces = "select province_id, province_canton from provinces where province_country = 0";
	}

	$form->add_list("delivery_address_province_id", "Province*", $sql_provinces, SUBMIT,
                isset($order_data['delivery_address_province_id']) ?
                $order_data['delivery_address_province_id'] :  "");


	if(param("delivery_address_province_id")) {
		$sql_places = "select place_id, place_name from places where place_province = " . param("delivery_address_province_id") . " order by place_name";
	}
	elseif(param("delivery_address_country")) {
		$sql_places = "select place_id, place_name from places where place_country = " . param("delivery_address_country") . " order by place_name";
	}
	else
	{
		$sql_places = "select place_id, place_name from places where place_country = 0";
	}
	
	
	$places = array();
	$places[999999999] = "Other city not listed below";
	$res = mysql_query($sql_places);
	while ($row = mysql_fetch_assoc($res))
	{
		$places[$row["place_id"]] = $row["place_name"];
	}

	$form->add_list("delivery_address_place_id", "City*", $places, SUBMIT,
                isset($order_data['delivery_address_place_id']) ?
                $order_data['delivery_address_place_id'] :  "");

	if(param("delivery_address_place_id") == 999999999)
	{
		$form->add_edit("delivery_address_place", "City*", NOTNULL, "", TYPE_CHAR, 20, 0, 1, "city");
	}
	else {
		$form->add_edit("delivery_address_place", "City", DISABLED);
	}

	$form->add_edit("delivery_address_zip", "ZIP*", NOTNULL, "", TYPE_CHAR, 20);


	$form->add_edit("delivery_address_address", "Address*", NOTNULL, "", TYPE_CHAR);
	$form->add_edit("delivery_address_address2", "", 0, "", TYPE_CHAR);
	
	$form->add_edit("delivery_address_phone", "Phone*",  NOTNULL, "", TYPE_CHAR, 20);
	$form->add_edit("delivery_address_fax", "Fax",  0, "", TYPE_CHAR, 20);
	$form->add_edit("delivery_address_email", "Email",  0, "", TYPE_CHAR);
	$form->add_edit("delivery_address_contact", "Contact*",  NOTNULL, "", TYPE_CHAR);

}
elseif(substr(param("delivery_is_address"), 0,1) == 's') // standard delivery address
{
	$form->add_edit("delivery_address_company", "Company*", NOTNULL, "", TYPE_CHAR);
	$form->add_edit("delivery_address_company2", "", 0, "", TYPE_CHAR);

	
	$form->add_list("delivery_address_country", "Country*", $sql_countries, NOTNULL | SUBMIT);


	if(param("delivery_address_country")) {
		$sql_provinces = "select province_id, province_canton from provinces where province_country = " . param("delivery_address_country") . " order by province_canton";
	}
	else
	{
		$did = substr(param("delivery_is_address"), 1, strlen(param("delivery_is_address")));
		$sql_delivery_addresses = "select * " . 
								  "from standard_delivery_addresses " .
			                      "left join places on place_id = delivery_address_place_id " . 
			                      "left join provinces on province_id = place_province " . 
						          "where delivery_address_id = " . dbquote($did);

		$res = mysql_query($sql_delivery_addresses);

		if ($row = mysql_fetch_assoc($res))
		{
			$sql_provinces = "select province_id, province_canton from provinces where province_country = " . $row["delivery_address_country"];
		}
		else
		{
			$sql_provinces = "select province_id, province_canton from provinces where province_country = 0";
		}
	}

	$form->add_list("delivery_address_province_id", "Province*", $sql_provinces, SUBMIT,
                isset($order_data['delivery_address_province_id']) ?
                $order_data['delivery_address_province_id'] :  "");


	if(param("delivery_address_province_id")) {
		$sql_places = "select place_id, place_name from places where place_province = " . param("delivery_address_province_id") . " order by place_name";
	}
	elseif(param("delivery_address_country")) {
		$sql_places = "select place_id, place_name from places where place_country = " . param("delivery_address_country") . " order by place_name";
	}
	else
	{
		$did = substr(param("delivery_is_address"), 1, strlen(param("delivery_is_address")));
		$sql_delivery_addresses = "select * " . 
								  "from standard_delivery_addresses " .
			                      "left join places on place_id = delivery_address_place_id " . 
			                      "left join provinces on province_id = place_province " . 
						          "where delivery_address_id = " . dbquote($did);

		$res = mysql_query($sql_delivery_addresses);

		if ($row = mysql_fetch_assoc($res))
		{
			$sql_places = "select place_id, place_name from places where place_province = " . $row["place_province"] . " order by place_name";
		}
		else
		{
			$sql_places = "select place_id, place_name from places where place_country = 0";
		}
	}
	
	$places = array();
	$places[999999999] = "Other city not listed below";
	$res = mysql_query($sql_places);
	while ($row = mysql_fetch_assoc($res))
	{
		$places[$row["place_id"]] = $row["place_name"];
	}
	
	$form->add_list("delivery_address_place_id", "City*", $places, SUBMIT,
                isset($order_data['delivery_address_place_id']) ?
                $order_data['delivery_address_place_id'] :  "");

	if(param("delivery_address_place_id") == 999999999)
	{
		$form->add_edit("delivery_address_place", "City*", NOTNULL, "", TYPE_CHAR, 20, 0, 1, "city");
	}
	else {
		$form->add_edit("delivery_address_place", "City", DISABLED);
	}

	$form->add_edit("delivery_address_address", "Address*", NOTNULL, "", TYPE_CHAR);
	$form->add_edit("delivery_address_address2", "", 0, "", TYPE_CHAR);
	$form->add_edit("delivery_address_zip", "ZIP*", NOTNULL, "", TYPE_CHAR, 20);

	$form->add_edit("delivery_address_phone", "Phone*",  NOTNULL, "", TYPE_CHAR, 20);
	$form->add_edit("delivery_address_fax", "Fax",  0, "", TYPE_CHAR, 20);
	$form->add_edit("delivery_address_email", "Email",  0, "", TYPE_CHAR);
	$form->add_edit("delivery_address_contact", "Contact*",  NOTNULL, "", TYPE_CHAR);
}
elseif(param("delivery_is_address") > 0)
{
	if(param("delivery_is_address") == 1)
	{
		$form->add_label("delivery_address_company", "POS Name");
	}
	elseif(param("delivery_is_address") == 3)
	{
		$form->add_label("delivery_address_company", "Franchisee");
	}
	else
	{
		$form->add_label("delivery_address_company", "Company");
	}
	$form->add_label("delivery_address_company2", "");
	$form->add_label("delivery_address_address", "Address");
	$form->add_label("delivery_address_address2", "");
	$form->add_label("delivery_address_zip", "ZIP");
	$form->add_label("delivery_address_place", "City");
	$form->add_hidden("delivery_address_place_id");
	$form->add_hidden("delivery_address_province_id");

	$form->add_hidden("delivery_address_country");
	
	if(param("delivery_is_address") == 1)
	{
		$country = $_SESSION["new_project_step_1"]["shop_address_country"];
	}
	elseif(param("delivery_is_address") == 2)
	{
		$country = $_SESSION["new_project_step_2"]["client_address_country"];
	}
	elseif(param("delivery_is_address") == 3)
	{
		$country = $_SESSION["new_project_step_2"]["franchisee_address_country"];
	}
	$form->add_lookup("delivery_address_country_name", "Country",  "countries", "country_name", 0, $country);
	
	

	if(param("delivery_is_address") != 4)
	{
		$form->add_edit("delivery_address_phone", "Phone*",  NOTNULL, "", TYPE_CHAR, 20);
		$form->add_edit("delivery_address_fax", "Fax",  0, "", TYPE_CHAR, 20);
		$form->add_edit("delivery_address_email", "Email",  0, "", TYPE_CHAR);
		$form->add_edit("delivery_address_contact", "Contact*",  NOTNULL, "", TYPE_CHAR);
	}
	else
	{
		$form->add_hidden("delivery_address_phone", "Phone");
		$form->add_hidden("delivery_address_fax", "Fax");
		$form->add_hidden("delivery_address_email", "Email");
		$form->add_hidden("delivery_address_contact");
	}
}
else
{

	$form->add_hidden("delivery_address_company", "");
	$form->add_hidden("delivery_address_company2", "");
	$form->add_hidden("delivery_address_address", "");
	$form->add_hidden("delivery_address_address2", "");
	$form->add_hidden("delivery_address_zip", "");
	$form->add_hidden("delivery_address_place", "");
	$form->add_hidden("delivery_address_place_id");
	$form->add_hidden("delivery_address_province_id");
	$form->add_hidden("delivery_address_country");
	$form->add_hidden("delivery_address_phone", "");
	$form->add_hidden("delivery_address_fax", "");
	$form->add_hidden("delivery_address_email", "");
	$form->add_hidden("delivery_address_contact");
}

$form->add_section("Preferences and Traffic Checklist");
$form->add_comment("Please enter the date in the form of dd.mm.yy");
$form->add_edit("preferred_delivery_date", "Preferred Arrival Date*", NOTNULL, "", TYPE_DATE, 20);
$form->add_list("preferred_transportation_arranged", "Transportation arranged by*", $sql_transportation_arranged, NOTNULL);
$form->add_list("preferred_transportation_mode", "Transportation mode*", $sql_transportation_modes, NOTNULL);



//$form->add_radiolist( "packaging_retraction", "Packaging Retraction Desired", array(0 => "no", 1 => "yes"), 0, 0);

$form->add_comment("Please indicate if there is a special approval needed for delivery into ".
                   "a pedestrian area."); 
$form->add_radiolist( "pedestrian_mall_approval", "Pedestrian Area Approval Needed",
    array(0 => "no", 1 => "yes"), 0, 0);
$form->add_comment("Please indicate if partial delivery is possible or full delivery is required."); 
$form->add_radiolist( "full_delivery", "Full Delivery",
    array(0 => "no", 1 => "yes"), 0, 1);
$form->add_comment("Please indicate any other circumstances/restrictions concerning delivery and traffic."); 
$form->add_multiline("delivery_comments", "Delivery Comments", 4);

$form->add_section("Insurance");
	$form->add_radiolist("order_insurance", array(1=>"Insurance by " . BRAND . "/Forwarder", 0=>""), array(1=>"covered",0=>"not covered"), VERTICAL,1);


$form->add_section("General Comments");
$form->add_multiline("comments", "Comments", 4);

$form->add_section(" ");
$form->add_button("save", "Submit Request");
$form->add_button("back", "Back");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$form->populate();
$form->process();

//clean fields from apostrophes
foreach($form->items as $key=>$value)
{
	if(array_key_exists('value', $value)) {
		$form->value($key, str_replace('"', '', $form->value($key)));
	}
}

if ($form->button("back"))
{
	$_SESSION["new_project_step_5"] = $_POST;
		
	$link = "project_new_04.php";
	redirect($link);
}
else if ($form->button("delivery_address_place_id"))
{
    if ($form->value("delivery_address_place_id"))
    {
       $sql = "select place_name, place_province from places where place_id = " . dbquote($form->value("delivery_address_place_id"));
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$form->value("delivery_address_place", $row["place_name"]);
			$form->value("delivery_address_province_id", $row["place_province"]);
			$form->value("delivery_address_zip", "");
			$form->value("delivery_address_address", "");
			$form->value("delivery_address_address2", "");
			$form->value("delivery_address_phone", "");
			$form->value("delivery_address_fax", "");
			$form->value("delivery_address_contact", "");
		}
    }
}
else if ($form->button("delivery_address_country"))
{
	$form->value("delivery_address_place", "");
	$form->value("delivery_address_place_id", 0);
	$form->value("delivery_address_province_id", 0);
	$form->value("delivery_address_zip", "");
	$form->value("delivery_address_address", "");
	$form->value("delivery_address_address2", "");
	$form->value("delivery_address_phone", "");
	$form->value("delivery_address_fax", "");
	$form->value("delivery_address_contact", "");
}
else if ($form->button("delivery_address_province_id"))
{
	$form->value("delivery_address_place", "");
	$form->value("delivery_address_place_id", 0);
	$form->value("delivery_address_zip", "");
	$form->value("delivery_address_address", "");
	$form->value("delivery_address_address2", "");
	$form->value("delivery_address_phone", "");
	$form->value("delivery_address_fax", "");
	$form->value("delivery_address_contact", "");
}
elseif ($form->button("delivery_is_address") and isset($_SESSION["new_project_step_2"]))
{
	// set delivery address
    if($form->value("delivery_is_address") == 1) //POS Address
	{
		$form->value("delivery_address_id", 0);
		$form->value("delivery_address_company", $_SESSION["new_project_step_1"]["shop_address_company"]);
		$form->value("delivery_address_company2",  $_SESSION["new_project_step_1"]["shop_address_company2"]);
		$form->value("delivery_address_address",  $_SESSION["new_project_step_1"]["shop_address_address"]);
		$form->value("delivery_address_address2",  $_SESSION["new_project_step_1"]["shop_address_address2"]);
		$form->value("delivery_address_zip",  $_SESSION["new_project_step_1"]["shop_address_zip"]);
		$form->value("delivery_address_place",  $_SESSION["new_project_step_1"]["shop_address_place"]);
		if(array_key_exists("select_shop_address_place", $_SESSION["new_project_step_1"])) {
			$form->value("delivery_address_place_id",  $_SESSION["new_project_step_1"]["select_shop_address_place"]);
		}
		else
		{
			$form->value("delivery_address_place_id",  $_SESSION["new_project_step_1"]["shop_address_place_id"]);
		}
		$form->value("delivery_address_province_id", $_SESSION["new_project_step_1"]["select_shop_address_province"]);
		$form->value("delivery_address_country",  $_SESSION["new_project_step_1"]["shop_address_country"]);
		$form->value("delivery_address_phone",  $_SESSION["new_project_step_1"]["shop_address_phone"]);
		$form->value("delivery_address_fax",  $_SESSION["new_project_step_1"]["shop_address_fax"]);
		$form->value("delivery_address_email",  $_SESSION["new_project_step_1"]["shop_address_email"]);
	}
	elseif($form->value("delivery_is_address") == 2) //client Address
	{
		$sql = "select place_province from places where place_id = " . dbquote($_SESSION["new_project_step_2"]["client_address_place_id"]);

		
		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);
		$povince_id = $row["place_province"];



		$form->value("delivery_address_id", 0);
		$form->value("delivery_address_company", $_SESSION["new_project_step_2"]["client_address_company"]);
		$form->value("delivery_address_company2",  $_SESSION["new_project_step_2"]["client_address_company2"]);
		$form->value("delivery_address_address",  $_SESSION["new_project_step_2"]["client_address_address"]);
		$form->value("delivery_address_address2",  $_SESSION["new_project_step_2"]["client_address_address2"]);
		$form->value("delivery_address_zip",  $_SESSION["new_project_step_2"]["client_address_zip"]);
		$form->value("delivery_address_place",  $_SESSION["new_project_step_2"]["client_address_place"]);
		$form->value("delivery_address_place_id",  $_SESSION["new_project_step_2"]["client_address_place_id"]);
		$form->value("delivery_address_province_id", $povince_id);
		$form->value("delivery_address_country",  $_SESSION["new_project_step_2"]["client_address_country"]);
		$form->value("delivery_address_phone",  $_SESSION["new_project_step_2"]["client_address_phone"]);
		$form->value("delivery_address_fax",  $_SESSION["new_project_step_2"]["client_address_fax"]);
		$form->value("delivery_address_email",  $_SESSION["new_project_step_2"]["client_address_email"]);
		$form->value("delivery_address_contact",  $_SESSION["new_project_step_2"]["client_address_contact_name"]);
	}
	elseif($form->value("delivery_is_address") == 3) // franchisee address
	{
		$sql = "select place_province from places where place_id = " . dbquote($_SESSION["new_project_step_2"]["franchisee_address_place_id"]);

		
		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);
		$povince_id = $row["place_province"];

		$form->value("delivery_address_id", 0);
		$form->value("delivery_address_company", $_SESSION["new_project_step_2"]["franchisee_address_company"]);
		$form->value("delivery_address_company2",  $_SESSION["new_project_step_2"]["franchisee_address_company2"]);
		$form->value("delivery_address_address",  $_SESSION["new_project_step_2"]["franchisee_address_address"]);
		$form->value("delivery_address_address2",  $_SESSION["new_project_step_2"]["franchisee_address_address2"]);
		$form->value("delivery_address_zip",  $_SESSION["new_project_step_2"]["franchisee_address_zip"]);
		$form->value("delivery_address_place",  $_SESSION["new_project_step_2"]["franchisee_address_place"]);
		$form->value("delivery_address_place_id",  $_SESSION["new_project_step_2"]["franchisee_address_place_id"]);
		$form->value("delivery_address_country",  $_SESSION["new_project_step_2"]["franchisee_address_country"]);
		$form->value("delivery_address_province_id",  $povince_id);
		$form->value("delivery_address_phone",  $_SESSION["new_project_step_2"]["franchisee_address_phone"]);
		$form->value("delivery_address_fax",  $_SESSION["new_project_step_2"]["franchisee_address_fax"]);
		$form->value("delivery_address_email",  $_SESSION["new_project_step_2"]["franchisee_address_email"]);
		$form->value("delivery_address_contact",  $_SESSION["new_project_step_2"]["franchisee_address_contact_name"]);
	}
	elseif(substr($form->value("delivery_is_address"), 0, 1) == 's') // standard delivery_address
	{
		$did = substr($form->value("delivery_is_address"), 1, strlen($form->value("delivery_is_address")));
		$sql_delivery_addresses = "select * " . 
								  "from standard_delivery_addresses " .
			                      "left join places on place_id = delivery_address_place_id " . 
			                      "left join provinces on province_id = place_province " . 
						          "where delivery_address_id = " . dbquote($did);


		$res = mysql_query($sql_delivery_addresses);

		if ($row = mysql_fetch_assoc($res))
		{
			$form->value("delivery_address_id", $row["delivery_address_id"]);
			$form->value("delivery_address_company", $row["delivery_address_company"]);
			$form->value("delivery_address_company2",  $row["delivery_address_company2"]);
			$form->value("delivery_address_address",  $row["delivery_address_address"]);
			$form->value("delivery_address_address2",  $row["delivery_address_address2"]);
			$form->value("delivery_address_zip",  $row["delivery_address_zip"]);
			$form->value("delivery_address_province_id",  $row["province_id"]);
			$form->value("delivery_address_place",  $row["delivery_address_place"]);
			$form->value("delivery_address_place_id",  $row["delivery_address_place_id"]);
			$form->value("delivery_address_country",  $row["delivery_address_country"]);
			$form->value("delivery_address_phone",  $row["delivery_address_phone"]);
			$form->value("delivery_address_fax",  $row["delivery_address_fax"]);
			$form->value("delivery_address_email",  $row["delivery_address_email"]);
			$form->value("delivery_address_contact",  $row["delivery_address_contact"]);
		}
		
	}
	else
	{
		$form->value("delivery_address_id", 0);
		$form->value("delivery_address_company", "");
		$form->value("delivery_address_company2",  "");
		$form->value("delivery_address_address",  "");
		$form->value("delivery_address_address2",  "");
		$form->value("delivery_address_zip",  "");
		$form->value("delivery_address_place",  "");
		$form->value("delivery_address_province_id",  "");
		$form->value("delivery_address_place_id",  "");
		$form->value("delivery_address_country",  0);
		$form->value("delivery_address_phone",  "");
		$form->value("delivery_address_fax",  "");
		$form->value("delivery_address_email",  "");
		$form->value("delivery_address_contact",  "");
	}
}
elseif ($form->button("save"))
{
	$form->add_validation("from_system_date({preferred_delivery_date}) >  " . dbquote(date("Y-m-d")), "The preferred arrival date must be a future date!");
	$form->add_validation("{delivery_is_address}", "You must choose an option from the drop down list \"Delivery address is identical to\".");

	$form->add_validation("{delivery_address_province_id}", "You must indicate a province.");


	$error = 0;
	if(strlen(preg_replace('/[^0-9a-z_ %\[\]\.\(\)%&-]/s', '', $form->value("delivery_address_address"))) < strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("delivery_address_address")))
		and strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("delivery_address_address"))) > 10)
	{
		$error = 1;
		$form->error("Company names and addresses must contain lower caracters. Do not use capital letters only.");
	}
	elseif(strlen(preg_replace('/[^0-9a-z_ %\[\]\.\(\)%&-]/s', '', $form->value("delivery_address_address2"))) < strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("delivery_address_address2")))
		and strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("delivery_address_address2"))) > 10)
	{
		$error = 1;
		$form->error("Company names and addresses must contain lower caracters. Do not use capital letters only.");
	}

	if ($error == 0 and $form->validate())
	{
		
		$_SESSION["new_project_step_5"] = $_POST;

		$link = "project_new_06.php";
		redirect($link);
	}
}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

$page->register_action('home', 'Home', "welcome.php");

$page->header();
$page->title("New Project");

echo "<div>";
echo "<img class='stepcounter' src='/pictures/numbers/01_off.gif' alt=\"\" />";
echo "<span class='step_inactive'>Basic Project Data</span>";
echo "<img class='stepcounter' src='/pictures/numbers/02_off.gif' alt=\"\" />";
echo "<span class='step_inactive'>Addresses</span>";

echo "<img class='stepcounter' src='/pictures/numbers/04_off.gif' alt=\"\" />";
echo "<span class='step_inactive'>POS Information</span>";
echo "<img class='stepcounter' src='/pictures/numbers/04_off.gif' alt=\"\" />";
echo "<span class='step_inactive'>Environment and Neighbourhood</span>";
echo "<img class='stepcounter' src='/pictures/numbers/05_on.gif' alt=\"\" />";
echo "<span class='step_active'>Delivery</span>";
echo "<br /><br /></div>";

$form->render();

?>

<div id="city" style="display:none;">
    Please indicate the new city's name in English!
</div>

<?php

$page->footer();


?>