<?php
/********************************************************************

    project_costs_add_positions.php

    Add new cost positions to costsheet

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-01-09
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2012, OMEGA SA, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../include/page_modal.php";
require_once "include/get_functions.php";

check_access("can_edit_list_of_materials_in_projects");

if(!param("pid") and !param("gid") and !param("mode"))
{
	$link = "welcome.php";
	redirect($link);
}


/********************************************************************
    prepare data
*********************************************************************/

$project = get_project(param("pid"));
$client_address = get_address($project["order_client_address"]);


//get group
$costgroup_name = "";
$sql = "select pcost_group_name from pcost_groups " .
       "where pcost_group_id = " . param("gid");

$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	$costgroup_name = $row["pcost_group_name"];
}


$sql_subgroups = "select concat(pcost_group_id, '-', pcost_subgroup_id) as sid, concat(pcost_group_code, ' ', pcost_group_name, ' ', pcost_subgroup_code, ' ', pcost_subgroup_name) as name " . 
                 "from pcost_groups " . 
				 "left join pcost_subgroups on pcost_subgroup_pcostgroup_id = pcost_group_id " . 
				 " order by pcost_group_code, pcost_subgroup_code ";


/********************************************************************
    save data
*********************************************************************/

if(param("save_form") and $_POST["mode"] == 'bid')
{
	$tmp = explode('-', $_POST["subgroup_id"]);
	
	$k = 1*$_POST["number_of_lines"];
	for($i=1;$i<=$k;$i++)
	{
		 $fields = array();
		 $values = array();

		 
		 $fields[] = "costsheet_bid_position_costsheet_bid_id";
		 $values[] = dbquote($_POST["bid"]);

		 $fields[] = "costsheet_bid_position_project_id";
		 $values[] = dbquote($_POST["pid"]);
		 
		 $fields[] = "costsheet_bid_position_pcost_group_id";
		 $values[] = dbquote($tmp[0]);

		 $fields[] = "costsheet_bid_position_pcost_subgroup_id";
		 $values[] = dbquote($tmp[1]);

		 
		 $fields[] = "date_created";
		 $values[] = dbquote(date("yyyy-mm-dd H:i:s"));

		 $fields[] = "user_created";
		 $values[] = dbquote(user_login());

		 $sql = "insert into costsheet_bid_positions (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

		 $result = mysql_query($sql) or dberror($sql);
		 
	}
}
elseif(param("save_form"))
{
	$tmp = explode('-', $_POST["subgroup_id"]);

	//get latest number
	$last_code = "";
	$subgroup_code = "";
	$sql = "select max(costsheet_code) as last_code " . 
		   "from costsheets " . 
		   " where costsheet_project_id = " . $_POST["pid"] . 
		   " and costsheet_pcost_group_id = " . $tmp[0] . 
		   " and costsheet_pcost_subgroup_id = " . $tmp[1];
	
	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$tmp1 = explode('.', $row["last_code"]);
		if(count($tmp1) == 2)
		{
			$last_code = 1*$tmp1[1];
			$subgroup_code = $tmp1[0];		}
	}

	$k = 1*$_POST["number_of_lines"];
	for($i=1;$i<=$k;$i++)
	{
		 $fields = array();
		 $values = array();

		 $fields[] = "costsheet_project_id";
		 $values[] = dbquote($_POST["pid"]);

		 $fields[] = "costsheet_pcost_group_id";
		 $values[] = dbquote($tmp[0]);

		 $fields[] = "costsheet_pcost_subgroup_id";
		 $values[] = dbquote($tmp[1]);


		 if($last_code > 0)
		 {
			 $next_code = $last_code+$i;
			 if($next_code < 10){$tmp2 = '0' . $next_code;} else {$tmp2 = $next_code;}
			 $code = $subgroup_code . '.' . $tmp2;
			 
			 $fields[] = "costsheet_code";
			 $values[] = dbquote($code);
		 }


		 if($_POST["mode"] == 'cms')
		 {
			$fields[] = "costsheet_is_in_cms";
			$values[] = 1;
		 }
		 else
		 {
			$fields[] = "costsheet_is_in_budget";
			$values[] = 1;
		 }
		 
		 	 
		 
		 $fields[] = "date_created";
		 $values[] = dbquote(date("yyyy-mm-dd H:i:s"));

		 $fields[] = "user_created";
		 $values[] = dbquote(user_login());

		 $sql = "insert into costsheets (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

		 $result = mysql_query($sql) or dberror($sql);

		 
	}
}


$has_subgroups = false;

$res_b = mysql_query($sql_subgroups) or dberror($sql_subgroups);
if($row_b = mysql_fetch_assoc($res_b))
{
	$has_subgroups = true;
}


/********************************************************************
    render form
*********************************************************************/
$form = new Form("costsheets", "costsheets");

require_once "include/project_head_small.php";


$form->add_section(" ");
$form->add_hidden("save_form", "1");
$form->add_hidden("costsheet_project_id", param("pid"));
$form->add_hidden("bid", param("bid"));
$form->add_hidden("pid", param("pid"));
$form->add_hidden("gid", param("gid"));
$form->add_hidden("mode", param("mode"));

if($has_subgroups == true)
{
	$form->add_list("subgroup_id", "Cost Subgroup",$sql_subgroups);
}
else
{
	$form->add_hidden("subgroup_id", 0);
}
$form->add_edit("number_of_lines", "Number of Empty Lines", NOTNULL, "", TYPE_INT, 4);

$form->add_input_submit("submit", "Create Cost Positions", 0);

$form->populate();
$form->process();


$page = new Page_Modal("projects");
$page->header();

$page->title("Add New Cost Position in Cost Group " . $costgroup_name);
$form->render();

if(param("save_form") and param("mode") == 'cms')
{
?>
	<script languege="javascript">
		var back_link = "project_costs_real_costs.php?pid=<?php echo param("pid")?>";
		$.nyroModalRemove();
	</script>
<?php
}
elseif(param("save_form") and param("mode") == 'bid')
{
?>
	<script languege="javascript">
		var back_link = "project_costs_bid.php?id=<?php echo param("bid");?>&pid=<?php echo param("pid")?>";
		$.nyroModalRemove();
	</script>
<?php
}
elseif(param("save_form"))
{
?>
	<script languege="javascript">
		var back_link = "project_costs_budget.php?pid=<?php echo param("pid")?>";
		$.nyroModalRemove();
	</script>
<?php
}

$page->footer();

?>