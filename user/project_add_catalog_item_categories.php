<?php
/********************************************************************

    project_add_catalog_item_categories.php

    Add items to the list of materials
    List all Categories

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-11
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-06-23
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_edit_list_of_materials_in_projects");

register_param("pid");
register_param("oid");
set_referer("project_add_catalog_item_item_list.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
  
// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

// create sql for product lines and categories

$sql_categories = "select category_id, category_name ".
				  "from categories ".
				  "left join product_lines on category_product_line = product_lines.product_line_id ";


	$list_filter = "product_line_id = " . $project["project_product_line"] . " and category_budget = 1 and (category_not_in_use is null or category_not_in_use = 0)";


/********************************************************************
    Create Form
*********************************************************************/ 
$form = new Form("projects", "project");

$form->add_section("Project");
require_once "include/project_head_small.php";

/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql_categories);

$list->set_entity("categories");
$list->set_filter($list_filter);

$list->set_order("category_priority, category_name");

$list->add_hidden("pid", param("pid"));
$list->add_hidden("oid", $project["project_order"]);

$link = "project_add_catalog_item_item_list.php?pid=" . param("pid") . "&oid=" . param("oid");
$list->add_column("category_name", "Category", $link, LIST_FILTER_NONE);

$list->add_button(LIST_BUTTON_BACK, "Back");

$list->process();

/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";


$page->header();


$page->title("Add Catalog Items: Category Selection");
$form->render();


echo "<p>", "Please choose from the following item categories.", "</p>";


$list->render();
$page->footer();


?>