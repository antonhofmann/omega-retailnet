<?php
/********************************************************************

    project_costs_bid_detail_pdf.php

    View bid in a PDF

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-01-09
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2014, OMEGA SA, All Rights Reserved.

*********************************************************************/


if(!function_exists ("print_page_header"))
{
	function print_page_header($pdf, $page_title)
	{
		$pdf->AddPage("l","A4", $page_title);
		$pdf->SetY(8);
		$pdf->Image('../pictures/omega_logo.jpg',10,8,33);
		$pdf->SetFont('arialn','B',12);
		$pdf->Cell(80);
		$pdf->Cell(0,10, $page_title, 0, 0, 'R');
		$pdf->Ln(5);
		$pdf->SetFont('arialn','I',8);
		$pdf->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$pdf->PageNo() ." ",0,0,'R');
		$pdf->Ln(15);
	}
}

/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());

// read project and order details
$project = get_project(param("pid"));

// get orders's currency
$order_currency = get_order_currency($project["project_order"]);



$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];


$order_state_name = get_actual_order_state_name($project["order_actual_order_state_code"], 1);
$retail_coordinator = get_user($project["project_retail_coordinator"]);
$retail_operator = get_user($project["order_retail_operator"]);


//get bid details
$sql = "select * from costsheet_bids where costsheet_bid_id = " . dbquote(param("bid"));
$res = mysql_query($sql) or dberror($sql);
$bid_data = mysql_fetch_assoc($res);
$page_title = "Bid " . $bid_data["costsheet_bid_company"] . " - Project " . $project["project_number"];



//get budget totals
$bid_totals =  get_project_bid_totals(param("bid"), $order_currency);


//get cost groups
$costgroups = get_cost_groups();


//get cost sub groups
$cost_sub_groups = array();
$sql = "select costsheet_bid_position_id, costsheet_bid_position_pcost_group_id, " .     
       "costsheet_bid_position_pcost_subgroup_id, " . 
	   "costsheet_bid_position_amount, " . 
	   "costsheet_bid_position_code, costsheet_bid_position_text, costsheet_bid_position_comment, " .
	   "concat(pcost_subgroup_code, ' ', pcost_subgroup_name) as subgroup  " .
	   "from costsheet_bid_positions " .
	   " left join pcost_subgroups on pcost_subgroup_id = costsheet_bid_position_pcost_subgroup_id " . 
	   " where costsheet_bid_position_project_id = " . param("pid") . 
	   " and costsheet_bid_position_costsheet_bid_id = " . param("bid") . 
	   " order by subgroup ";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$cost_sub_groups[$row["costsheet_bid_position_pcost_group_id"]][$row["costsheet_bid_position_pcost_subgroup_id"]] = $row["subgroup"];
}


//get all cost positions
$costsheet_positions = array();
$costsheet_positions_in_budget = array();
$sql = "select costsheet_bid_position_id, costsheet_bid_position_pcost_group_id, " .     
       "costsheet_bid_position_pcost_subgroup_id, " . 
	   "costsheet_bid_position_amount, costsheet_bid_position_is_in_budget, " . 
	   "costsheet_bid_position_code, costsheet_bid_position_text, costsheet_bid_position_comment " . 
	   "from costsheet_bid_positions " .
	   " where costsheet_bid_position_project_id = " . param("pid") . 
	   " and costsheet_bid_position_costsheet_bid_id = " . param("bid") . 
	   " order by LENGTH(costsheet_bid_position_code), costsheet_bid_position_code ";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$tmp = array();
	$tmp['code'] = $row["costsheet_bid_position_code"];
	$tmp['text'] = trim(str_replace("\r\n", ' ', $row["costsheet_bid_position_text"]));
	$tmp['comment'] = trim(str_replace("\r\n", ' ', $row["costsheet_bid_position_comment"]));
	$tmp['amount'] = $row["costsheet_bid_position_amount"];

	$costsheet_positions[$row["costsheet_bid_position_pcost_group_id"]][$row["costsheet_bid_position_pcost_subgroup_id"]][$row["costsheet_bid_position_id"]] = $tmp;

	$costsheet_positions_in_budget[$row["costsheet_bid_position_pcost_group_id"]][$row["costsheet_bid_position_pcost_subgroup_id"]][$row["costsheet_bid_position_id"]] = $row["costsheet_bid_position_is_in_budget"];
}

// prepare output fields
$captions1 = array();
$captions1[] = "Project Number:";
if($project["productline_subclass_name"])
{
	$captions1[] = "Product Line / Subclass / Type:";
}
else
{
	$captions1[] = "Product Line / Type:";
}
$captions1[] = "Project Starting Date / Agreed Opening Date:";
$captions1[] = "Status:";
$captions1[] = "Project Manager / Retail Operator:";

$captions2 = array();
foreach($costgroups as $group_id=>$group_name)
{
	$captions2[] = $group_name;
}
$captions2[] = "Total";

$captions3 = array();
$captions3[] = "Client:";
$captions3[] = "POS Location:";
$captions3[] = "Notify Address:";

$data1 = array();
$data1[] = $project["project_number"];

if($project["productline_subclass_name"])
{
	$data1[] = $project["product_line_name"] . " / " . $project["productline_subclass_name"] . " / " . $project["postype_name"];
}
else
{
	$data1[] = $project["product_line_name"] . " / " . $project["postype_name"];	
}
$data1[] = to_system_date($project["order_date"]) . " / " .  to_system_date($project["project_real_opening_date"]);
$data1[] = $project["order_actual_order_state_code"] . " " . $order_state_name;
$data1[] = $retail_coordinator["firstname"] . " " . $retail_coordinator["name"] . " / " . $retail_operator["firstname"] . " " . $retail_operator["name"];

$data2 = array();
$data2n = array();
foreach($costgroups as $group_id=>$group_name)
{
	if(array_key_exists($group_id, $bid_totals["group_totals_formated"]))
	{
		
		$data2[] = $bid_totals["group_totals_formated"][$group_id];
		$data2n[] = $bid_totals["group_totals"][$group_id];
	}
	else
	{
		$data2[] = "";
		$data2n[] = "";
	}
}
$data2[] = number_format($bid_totals["bid_total"], 2);
$data2n[] = $bid_totals["bid_total"];


//Instanciation of inherited class


$pdf->SetAutoPageBreak(true, 5);

$pdf->SetLineWidth(0.1);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

print_page_header($pdf, $page_title);


// output project header informations
$pdf->SetFont('arialn','B',12);
$pdf->Cell(120,5,"Project Details",1);
$pdf->Cell(5,5," ",0);
$pdf->Cell(95,5,"Bidded Amount in " . $order_currency["symbol"],1);
$pdf->Cell(5,5," ",0);
$pdf->Cell(50,5,"Remarks",1);

$pdf->Ln();


$pdf->SetFont('arialn','',9);

foreach($captions1 as $key=>$value)
{
    $pdf->Cell(60,5,$captions1[$key],1);
    $pdf->Cell(60,5,$data1[$key],1);

    $pdf->Cell(5,5," ",0);

    if($key <=4)
    {
        
        $pdf->Cell(65,5,$captions2[$key],1);
        $pdf->Cell(18,5,$data2[$key], 1, 0, 'R');

		$share = "";
		if($data2n[$key] > 0 and $bid_totals["bid_total"] > 0)
		{
			$share = number_format(round(100*($data2n[$key] / $bid_totals["bid_total"]), 2), 2)  . '%';
		}
		$pdf->Cell(12,5,$share, 1, 0, 'R');
    }
    

    if($key == 0)
    {
        $pdf->Cell(5,5," ",0);
        $pdf->MultiCell(50, 30, $bid_data["costsheet_bid_remark"],1,'L', false, 0);
		//$pdf->Cell(50,30, $bid_data["costsheet_bid_remark"] ,1, 0, false, '', 0, false, 'T', 'T');
		//Cell ($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M')
        $pdf->Ln();
		$pdf->SetY($pdf->GetY()-25); 
    }
    elseif($key == 1)
    {
        //$pdf->Cell(5,5," ",0);
        //$pdf->Cell(50,5, $bid_data["costsheet_bid_remark"] ,1);
        $pdf->Ln();
    }
    elseif($key == 2)
    {
        //$pdf->Cell(5,5," ",0);
        //$pdf->Cell(50,5, '' ,1);
        $pdf->Ln();
    }
    elseif($key == 3)
    {
        //$pdf->Cell(5,15," ",0);
        //$pdf->Cell(50,15, " " ,1);
        $pdf->Ln();
        //$pdf->SetY($pdf->GetY()-10); 
    }

}
$pdf->Ln();
$pdf->Cell(125,5,'',0);
$pdf->SetFont('arialn','B',9);
$pdf->Cell(65,5,$captions2[5],1);
$pdf->Cell(18,5,$data2[5], 1, 0, 'R');

if($bid_totals["bid_total"] > 0)
{
	$pdf->Cell(12,5,"100.00%", 1, 0, 'R');
}
else
{
	$pdf->Cell(12,5,"", 1, 0, 'R');
}

$pdf->SetFont('arialn','',9);

$pdf->Ln();
$pdf->Ln();


//print cost group details
$pdf->SetFillColor(240, 240, 240);
//$pdf->setCellHeightRatio(0.8);

foreach($costgroups as $group_id=>$group_name)
{
	
	$y = $pdf->getY();
	if($y >= 185)
	{
		print_page_header($pdf, $page_title);
	}
	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(160 ,5, $group_name, 1, 0, 'L', 1);
	$pdf->Cell(75 ,5, "Comments", 1, 0, 'L', 1);
	$pdf->Cell(30 ,5, "Amount " . $order_currency["symbol"] , 1, 0, 'R', 1);
	$pdf->Cell(10 ,5, '%' , 1, 0, 'R', 1);
	$pdf->Ln();

	if(array_key_exists($group_id, $cost_sub_groups))
	{
		foreach($cost_sub_groups[$group_id] as $sub_group_id=>$subgroup_name)
		{
			
			if(array_key_exists($group_id, $bid_totals["subgroup_totals_by_id"]))
			{
				if(array_key_exists($group_id, $costsheet_positions))
				{
					$y = $pdf->getY();
					if($y >= 185)
					{
						print_page_header($pdf, $page_title);
					}
					
					$pdf->SetFont('arialn','B',8);
					$pdf->Cell(160 ,4, $subgroup_name, 1, 0, 'L', 1);
					$pdf->Cell(75 ,4, "", 1, 0, 'L', 1);
					$pdf->Cell(30 ,4, number_format($bid_totals["subgroup_totals_by_id"][$group_id][$sub_group_id], 2) , 1, 0, 'R', 1);
					
					$share = "";
					if($bid_totals["subgroup_totals_by_id"][$group_id][$sub_group_id] > 0 and $bid_totals["bid_total"] > 0)
					{
						$share = number_format(round(100*($bid_totals["subgroup_totals_by_id"][$group_id][$sub_group_id] / $bid_totals["bid_total"]), 2), 2)  . '%';
					}
					$pdf->Cell(10 ,4, $share , 1, 0, 'R', 1);

					$pdf->Ln();

					$pdf->SetFont('arialn','',8);

					foreach($costsheet_positions[$group_id][$sub_group_id] as $key=>$data)
					{

						
						
						$num_lines = $pdf->getStringHeight(150, $data['text'])/3.528;
						$num_lines2 = $pdf->getStringHeight(75, $data['comment'])/3.528;
						
						//add new page if necessary
						$y = $pdf->getY();
						if($y >= 185 or $y+$num_lines > 185 or $y+$num_lines2 > 185)
						{
							print_page_header($pdf, $page_title);
						}
						
						//output data
						$pdf->Cell(10 ,4, $data['code'], 0, 0, 'L', 0, 0);
						$pdf->MultiCell(150, '', $data['text'],0,'L', false, 0);
						$pdf->MultiCell(75, 0,$data['comment'],0,'L', false, 0);
						
						
						$pdf->Cell(30 ,4, number_format($data['amount'], 2) , 0, 0, 'R');

						if($costsheet_positions_in_budget[$group_id][$sub_group_id][$key] == 1)
						{
							$tmp = "ok";
						}
						else
						{
							$tmp = " ";
						}
						//$pdf->Cell(3 ,4, $tmp , 0, 0, 'R');

						$share = "";
						if($data['amount']> 0 and $bid_totals["bid_total"] > 0)
						{
							$share = number_format(round(100*($data['amount'] / $bid_totals["bid_total"]), 2), 2) . '%';
						}
						$pdf->Cell(10 ,4, $share , 0, 0, 'R', 0);
						
						if($num_lines2 > $num_lines){$num_lines = $num_lines2;}
						for($i=0;$i<$num_lines;$i++)
						{
							$pdf->Ln();
						}
						$pdf->Line(10,$pdf->GetY(), 285, $pdf->GetY());
						
					}
				}
			}
		}
	}


	$pdf->Ln();
	$pdf->Ln();
}




?>