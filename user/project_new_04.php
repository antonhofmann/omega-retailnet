<?php
/********************************************************************

    project_new04.php

    Creation of a new project step 04.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-07-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-07-14
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_create_new_projects");


if(!isset($_SESSION["new_project_step_1"]))
{
	$link = "project_new_01.php";
	redirect($link);
}

set_referer("project_new_05.php");


if(count($_POST) == 0 and isset($_SESSION["new_project_step_4"]))
{
	$_SESSION["new_project_step_4"]["action"] = "";
	foreach($_SESSION["new_project_step_4"] as $key=>$value)
	{
		register_param($key);
		param($key, $value);
	}
}
/********************************************************************
    prepare all data needed
*********************************************************************/

// get user data
$user = get_user(user_id());


$ratings = array();
$ratings[5] = "bad";
$ratings[4] = "poor";
$ratings[3] = "good";
$ratings[2] = "very good";
$ratings[1] = "excellent";


$sql_areas = "select posareatype_id, posareatype_name " . 
		     "from posareatypes " . 
		      " order by  posareatype_name";


$sql_floors = "select floor_id, floor_name " . 
		     "from floors " . 
		      " order by  floor_id";

$floors = array();
$res = mysql_query($sql_floors) or dberror($sql_floors);
while ($row = mysql_fetch_assoc($res))
{
	$floors[$row["floor_id"]] = $row["floor_name"];
}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("", "project");

$form->add_section("POS Location");
$posaddress = $_SESSION["new_project_step_1"]["shop_address_company"] . ", " . $_SESSION["new_project_step_1"]["shop_address_zip"]. " " . $_SESSION["new_project_step_1"]["shop_address_place"];
$form->add_comment($posaddress);

$form->add_section("Environment*");
$show_floors = false;


if($_SESSION["new_project_step_1"]["project_kind"] > 1) // renovation project, takover/renovation or take over
{
	$form->add_comment("Please verify the environment information. You must choose at least one environment. Multiple choice is also possible.");

	$res = mysql_query($sql_areas) or dberror($sql_areas);
	while ($row = mysql_fetch_assoc($res))
	{
		$sql = "select posarea_id from posareas " . 
			   "where posarea_area = " . $row["posareatype_id"] . 
			   "   and posarea_posaddress = " . $_SESSION["new_project_step_1"]["posaddress_id"];

		$num_rows = mysql_num_rows(mysql_query($sql));

		$value = 0;
		if($num_rows){$value = 1;}
		$form->add_checkbox("AR_" .$row["posareatype_id"], "", $value, 0, $row["posareatype_name"]);

		if($value == 1 and $row["posareatype_id"] == 2) // shopping mall
		{
			$show_floors = true;
			
		}
	}

	
	
	$sql = "select * from posorders " . 
		   "where posorder_posaddress = " . $_SESSION["new_project_step_1"]["posaddress_id"] . 
		   " and posorder_opening_date <> '0000-00-00' and posorder_opening_date is not null " . 
		   " order by posorder_opening_date DESC";
	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		
		$form->add_hidden("project_floor", $row["posorder_floor"]);

		$form->add_section("Neighbourhood");
		$form->add_comment("Please verify the neighbourhood information.");

		$form->add_edit("posorder_neighbour_left", "Shop on Left Side", 0, $row["posorder_neighbour_left"]);
		$form->add_edit("posorder_neighbour_right", "Shop on Right Side", 0, $row["posorder_neighbour_right"]);
		$form->add_edit("posorder_neighbour_acrleft", "Shop Across Left Side", 0, $row["posorder_neighbour_acrleft"]);
		$form->add_edit("posorder_neighbour_acrright", "Shop Across Right Side", 0, $row["posorder_neighbour_acrright"]);
		$form->add_multiline("posorder_neighbour_brands", "Other Brands in Area", 4, 0, $row["posorder_neighbour_brands"]);
	}
	else
	{
		
		$form->add_hidden("project_floor", "");

		$form->add_section("Neighbourhood");
		$form->add_comment("Please verify the neighbourhood information.");
		
		$form->add_edit("posorder_neighbour_left", "Shop on Left Side");
		$form->add_edit("posorder_neighbour_right", "Shop on Right Side");
		$form->add_edit("posorder_neighbour_acrleft", "Shop Across Left Side");
		$form->add_edit("posorder_neighbour_acrright", "Shop Across Right Side");
		$form->add_multiline("posorder_neighbour_brands", "Other Brands in Area", 4);
	}

	$sql = "select * from posaddresses where posaddress_id = " . $_SESSION["new_project_step_1"]["posaddress_id"];
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		
		$form->add_section("Area Perception");
		$form->add_comment("Please rate by clicking a star: 5 stars are the best rating.");
		
		
		if(array_key_exists("new_project_step_4", $_SESSION))
		{
			$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_class"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_tourist"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_transport"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_people"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_parking"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_visibility1"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from accross the Street*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_visibility2"], $flags = 0);
		}
		else
		{
			$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, $row["posaddress_perc_class"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, $row["posaddress_perc_tourist"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings, $row["posaddress_perc_transport"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings, $row["posaddress_perc_people"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings, $row["posaddress_perc_parking"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings, $row["posaddress_perc_visibility1"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from accross the Street*", $ratings, $row["posaddress_perc_visibility2"], $flags = 0);
		}

		$form->add_section("Inhouse Surfaces");
		$form->add_comment("Please verify the inhouse surface information.");
		
		$form->add_edit("posaddress_store_grosssurface", "Gross Surface in sqms*", NOTNULL, $row["posaddress_store_grosssurface"], TYPE_DECIMAL, 8, 2);

		$form->add_edit("posaddress_store_totalsurface", "Total Surface in sqms*", NOTNULL, $row["posaddress_store_totalsurface"], TYPE_DECIMAL, 8, 2);
		$form->add_edit("posaddress_store_retailarea", "Sales Surface in sqms*", NOTNULL, $row["posaddress_store_retailarea"], TYPE_DECIMAL, 8, 2);
		$form->add_edit("posaddress_store_backoffice", "Back Office Surface in sqms", 0, $row["posaddress_store_backoffice"], TYPE_DECIMAL, 8, 2);
		$form->add_edit("posaddress_store_numfloors", "Number of Floors*", NOTNULL, $row["posaddress_store_numfloors"], TYPE_DECIMAL, 1, 0);
		$form->add_edit("posaddress_store_floorsurface1", "Floor 1: Surface  in sqms", 0, $row["posaddress_store_floorsurface1"], TYPE_DECIMAL, 8, 2);
		$form->add_edit("posaddress_store_floorsurface2", "Floor 2: Surface  in sqms", 0, $row["posaddress_store_floorsurface2"], TYPE_DECIMAL, 8, 2);
		$form->add_edit("posaddress_store_floorsurface3", "Floor 3: Surface  in sqms", 0, $row["posaddress_store_floorsurface3"], TYPE_DECIMAL, 8, 2);
	}
	else
	{
		
		$form->add_section("Area Perception");
		$form->add_comment("Please rate by clicking a star: 5 stars are the best rating.");
		
		if(array_key_exists("new_project_step_4", $_SESSION))
		{
			$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_class"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_tourist"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_transport"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_people"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_parking"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_visibility1"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from accross the Street*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_visibility2"], $flags = 0);
		}
		else
		{
			$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, 0, NOTNULL);
			$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, 0, NOTNULL);
			$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings, 0, NOTNULL);
			$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings, 0, NOTNULL);
			$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings, 0, NOTNULL);
			$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings, 0, NOTNULL);
			$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from accross the Street*", $ratings, 0, NOTNULL);
		}

		$form->add_section("Inhouse Surfaces");
		$form->add_comment("Please verify the inhouse surface information.");
		$form->add_edit("posaddress_store_grosssurface", "Gross Surface rented by " . BRAND . " in sqms*", NOTNULL, "", TYPE_DECIMAL, 8, 2);
		$form->add_edit("posaddress_store_totalsurface", "Total Surface rented by " . BRAND . " in sqms*", NOTNULL, "", TYPE_DECIMAL, 8, 2);
		$form->add_edit("posaddress_store_retailarea", "Sales Surface occupied by " . BRAND . " in sqms*", NOTNULL, "", TYPE_DECIMAL, 8, 2);
		$form->add_edit("posaddress_store_backoffice", "Back Office Surface occupied by " . BRAND . " in sqms", 0, "", TYPE_DECIMAL, 8, 2);
		$form->add_edit("posaddress_store_numfloors", "Number of Floors occupied by " . BRAND . "*", NOTNULL, "", TYPE_DECIMAL, 1, 0);
		$form->add_edit("posaddress_store_floorsurface1", "Floor 1: Surface  in sqms", 0, "", TYPE_DECIMAL, 8, 2);
		$form->add_edit("posaddress_store_floorsurface2", "Floor 2: Surface  in sqms", 0, "", TYPE_DECIMAL, 8, 2);
		$form->add_edit("posaddress_store_floorsurface3", "Floor 3: Surface  in sqms", 0, "", TYPE_DECIMAL, 8, 2);
	}

}
else
{
	$form->add_comment("Please indicate the environment information of the new POS. You must choose at least one environment. Multiple choice is also possible.");

	$sql = "select posareatype_id, posareatype_name " . 
		   "from posareatypes " . 
		   " order by  posareatype_name";

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$form->add_checkbox("AR_" .$row["posareatype_id"], "", 0, 0, $row["posareatype_name"]);
	}

	
	$form->add_hidden("project_floor", $row["posorder_floor"]);


	$form->add_section("Area Perception");
	$form->add_comment("Please rate by clicking a star: 5 stars are the best rating.");
	
	if(array_key_exists("new_project_step_4", $_SESSION))
	{
		$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_class"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_tourist"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_transport"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_people"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_parking"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_visibility1"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from accross the Street*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_visibility2"], $flags = 0);
	}
	else
	{
		$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from accross the Street*", $ratings, 0, NOTNULL);
	}

	$form->add_section("Neighbourhood");
	$form->add_comment("Please indicate the neighbourhood information of the new POS.");
	$form->add_edit("posorder_neighbour_left", "Shop on Left Side");
	$form->add_edit("posorder_neighbour_right", "Shop on Right Side");
	$form->add_edit("posorder_neighbour_acrleft", "Shop Across Left Side");
	$form->add_edit("posorder_neighbour_acrright", "Shop Across Right Side");
	$form->add_multiline("posorder_neighbour_brands", "Other Brands in Area", 4);


	$form->add_section("Inhouse Surfaces");
	$form->add_comment("Please indicate the inhouse surface information.");
	$form->add_edit("posaddress_store_grosssurface", "Gross Surface rented by " . BRAND . " in sqms*", NOTNULL, "", TYPE_DECIMAL, 8, 2, 1, "grosssqm");
	$form->add_edit("posaddress_store_totalsurface", "Total Surface rented by " . BRAND . " in sqms*", NOTNULL, "", TYPE_DECIMAL, 8, 2, 1, "totalsqm");
	$form->add_edit("posaddress_store_retailarea", "Sales Surface occupied by " . BRAND . " in sqms*", NOTNULL, "", TYPE_DECIMAL, 8, 2, 1, "retailsqm");
	$form->add_edit("posaddress_store_backoffice", "Back Office Surface occupied by " . BRAND . " in sqms", 0, "", TYPE_DECIMAL, 8, 2, 1, "backofficesqm");
	$form->add_edit("posaddress_store_numfloors", "Number of Floors occupied by " . BRAND . "*", NOTNULL, "", TYPE_DECIMAL, 1, 0);
	$form->add_edit("posaddress_store_floorsurface1", "Floor 1: Surface  in sqms", 0, "", TYPE_DECIMAL, 8, 2);
	$form->add_edit("posaddress_store_floorsurface2", "Floor 2: Surface  in sqms", 0, "", TYPE_DECIMAL, 8, 2);
	$form->add_edit("posaddress_store_floorsurface3", "Floor 3: Surface  in sqms", 0, "", TYPE_DECIMAL, 8, 2);

}


$form->add_section("Store Locator at " . BRAND_WEBSITE);
$form->add_checkbox("posaddress_export_to_web", "Publish the POS Location as soon as the POS is opened.", 1, 0, BRAND_WEBSITE);
$form->add_checkbox("posaddress_email_on_web", "Publish the email address of the POS in the Store Locator as soon as the POS is opened.", 1, 0, BRAND_WEBSITE);

$form->add_section(" ");

if($_SESSION["new_project_step_1"]["project_kind"] == 4) // Take Over
{
	$form->add_comment("Delivery (Step 4) is not necessary in case of take over only.");
	$form->add_button("save", "Submit Request");
	$form->add_button("back_to_step_02", "Back");
}
elseif($_SESSION["new_project_step_1"]["project_kind"] == 5) // Lease Reneval
{
	$form->add_comment("Delivery (Step 4) is not necessary in case of leas renewal.");
	$form->add_button("save", "Submit Request");
	$form->add_button("back_to_step_01", "Back");
}
else
{
	$form->add_button("step5", "Proceed to next step");
	$form->add_button("back_to_step_03", "Back");
}



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$form->populate();
$form->process();

//clean fields from apostrophes
foreach($form->items as $key=>$value)
{
	if(array_key_exists('value', $value)) {
		$form->value($key, str_replace('"', '', $form->value($key)));
	}
}

if ($form->button("back_to_step_03"))
{
	$_SESSION["new_project_step_4"] = $_POST;
		
	$link = "project_new_03.php";
	redirect($link);
}
elseif ($form->button("back_to_step_02"))
{
	$_SESSION["new_project_step_4"] = $_POST;
		
	$link = "project_new_02.php";
	redirect($link);
}
elseif ($form->button("back_to_step_01"))
{
	$_SESSION["new_project_step_4"] = $_POST;
		
	$link = "project_new_01.php";
	redirect($link);
}
elseif ($form->button("step5"))
{
	
	$validation_rule = "";
	$res = mysql_query($sql_areas) or dberror($sql_areas);
	while ($row = mysql_fetch_assoc($res))
	{
		$validation_rule .= "{AR_" .$row["posareatype_id"] . "} > 0 OR ";
	}
	$validation_rule = substr($validation_rule, 0, strlen($validation_rule)-4);

	$form->add_validation($validation_rule, "The area must be indicated!");

	$form->add_validation("{posaddress_store_grosssurface} >= {posaddress_store_totalsurface}", "The gross surface must not be less than the total surface!");

	$form->add_validation("{posaddress_store_totalsurface} >= {posaddress_store_retailarea}", "The total surface must not be less than the sales surface!");

	$error = 0;

	if(!$form->value("posaddress_perc_class")){$error = 1;}
	if(!$form->value("posaddress_perc_tourist")){$error = 1;}
	if(!$form->value("posaddress_perc_transport")){$error = 1;}
	if(!$form->value("posaddress_perc_people")){$error = 1;}
	if(!$form->value("posaddress_perc_parking")){$error = 1;}
	if(!$form->value("posaddress_perc_visibility1")){$error = 1;}
	if(!$form->value("posaddress_perc_visibility2")){$error = 1;}


	if($form->value("AR_2") == 1 and !$form->value("project_floor"))
	{
		$error = 2;
	}

	if($error == 1)
	{
		$form->error("Please rate all parameters in section 'Area Perception'!");
	}
	elseif($error == 2)
	{
		$form->error("Please indicate the POS location's floor!");
	}
	elseif($form->validate())
	{
		$_SESSION["new_project_step_4"] = $_POST;
		
		$link = "project_new_05.php";
		redirect($link);
	}
}
elseif ($form->button("save"))
{
	if ($form->validate())
	{
		$_SESSION["new_project_step_4"] = $_POST;
		$_SESSION["new_project_step_5"] = "";
		$link = "project_new_06.php";
		redirect($link);
	}
}

  
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

$page->register_action('home', 'Home', "welcome.php");

$page->header();
$page->title("New Project");

echo "<div>";
echo "<img class='stepcounter' src='/pictures/numbers/01_off.gif' alt=\"\" />";
echo "<span class='step_inactive'>Basic Project Data</span>";
echo "<img class='stepcounter' src='/pictures/numbers/02_off.gif' alt=\"\" />";
echo "<span class='step_inactive'>Addresses</span>";
echo "<img class='stepcounter' src='/pictures/numbers/03_off.gif' alt=\"\" />";
echo "<span class='step_inactive'>POS Information</span>";
echo "<img class='stepcounter' src='/pictures/numbers/04_on.gif' alt=\"\" />";
echo "<span class='step_active'>Environment and Neighbourhood</span>";
echo "<img class='stepcounter' src='/pictures/numbers/05_off.gif' alt=\"\" />";
echo "<span class='step_inactive'>Delivery</span>";
echo "<br /><br /></div>";


$form->render();
?>

<div id="grosssqm" style="display:none;">
    Please indicate the Gross Surface in units of measurement <strong>rented by <?php echo BRAND;?></strong><br />incl. corridors and other public areas, back office, toilets etc.
</div> 
<div id="totalsqm" style="display:none;">
    Please indicate the total surface in square meters <strong>rented by <?php echo BRAND;?></strong>, incl. back office, toilets etc.
</div> 
<div id="retailsqm" style="display:none;">
    Please indicate the surface in square meters occupied by <strong><?php echo BRAND;?> sales surface only</strong> (without back office, toilets etc.).
</div> 
<div id="backofficesqm" style="display:none;">
    Please indicate the total back office surface in square meters <strong>occupied by <?php echo BRAND;?></strong> (total area minus sales surface).
</div> 

<?php

$top = 350;
if( strpos($_SERVER['HTTP_USER_AGENT'], "MSIE") > 0)
{
	$top = 320;
	
}

if($show_floors == true or param("project_floor") > 0 or param("AR_2") == 1)
{
	echo '<div id="floorselector" style="position:absolute;top:' . $top . 'px;left:550px;height:200px;width:200px;">';
}
else
{
	echo '<div id="floorselector" style="position:absolute;top:' . $top . 'px;left:550px;height:200px;width:200px;display:none;">';
}
	
	foreach($floors as $id=>$name)
	{
		$checked = "";
		if(param("project_floor") == $id)
		{
			$checked = "checked";
		}
		echo "<input type=\"radio\" onclick=\"javascript:$('#project_floor').val($(this).val());\" value=\"" . $id . "\" name=\"floor\" " . $checked . ">" . $name . '<br />';
	}

?>
</div>


<script language="Javascript">

	$(document).ready(function(){
		  
		  
		  
		  var p = $("#AR_2");
		  var position = p.position();

		  var x = position.left + 70;
		  var y = position.top - 60;

		  id="floorselector"
		  $("#floorselector").css({left:x,top:y});
		  
		  $("#AR_2").click(function(){
			
			
			if( $('#floorselector').is(':hidden') ) 
			{
				$("#floorselector").show();
			}
			else
		    {
				$("#floorselector").hide();
				$("#project_floor").val('');
		    }
			
			
			
		  });
	});

</script>

<?php
$page->footer();


?>