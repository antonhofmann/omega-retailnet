<?php
/********************************************************************

    delivery_by_forwarder.php

    List delivery jobs by supplier

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2003-12-07
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-12-07
    Version:        1.0.0

    Copyright (c) 2003, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/order_state_constants.php";

check_access("can_view_delivery");



/********************************************************************
    prepare all data needed
*********************************************************************/
// get user data
$user_data = get_user(user_id());
$user_roles = get_user_roles(user_id());

// get params
$ordernumber = param("o");
$country = param("c");
$ponumber = param("p");
$shipmentcode = param("s");
$supplier = param("su");
$forwarder = param("fw");
$rto = param("rto");
$rtc = param("rtc");
$item = param("item");
$ordertype = param("t");


// build sql for the list
$sql_order_items = "select order_item_id, ".
                   "    concat_ws(', ', country_name, order_shop_address_place, ".
                   "    order_shop_address_company, order_number, ".
                   "    concat('Retail Contact: ', user_name, ' ', user_firstname)) as group_head, ".
                   "    order_number, address_company, ".
                   "    country_name, order_id, project_id, ".
                   "    concat(address_company, '\n', order_item_quantity, ' ', if(order_item_item <>'', ".
                   "    item_code, 'Special Item')) as sup_item, ".
                   "    order_item_po_number, order_item_shipment_code, order_item_quantity, ".
                   "    concat(if(order_item_item <>'', item_code, 'Special Item'), '\n', " .
                   "    order_item_quantity) as item_shortcut, ".
                   "    concat(order_item_po_number, '\n', if(order_item_shipment_code > '', ".
                   "    order_item_shipment_code ,'')) as po_shipment, ".
                   "    concat('Order Date:', '\n', 'Ready for Pickup:', '\n', 'Pickup Date:') as date_titles_1, ". 
                   "    concat('Expected Arrival:', '\n', 'Arrival:') as date_titles_2, ". 
                   "    concat(if(order_item_ordered <> '0000-00-00', concat('[', order_item_ordered_changes, '] ', right(order_item_ordered,2), '.', month(order_item_ordered), '.', right(year(order_item_ordered),2)), 'n/a'), ".
                   "    '\n', if(order_item_ready_for_pickup <> '0000-00-00', concat('[', order_item_ready_for_pickup_changes, '] ', right(order_item_ready_for_pickup,2), '.', month(order_item_ready_for_pickup), '.', right(year(order_item_ready_for_pickup),2)), 'n/a'), ".
                   "    '\n', if(order_item_pickup <> '0000-00-00', concat('[', order_item_pickup_changes, '] ', right(order_item_pickup,2), '.', month(order_item_pickup), '.', right(year(order_item_pickup),2)), 'n/a')) as dates_1,  ". 
                   "    concat(if(order_item_expected_arrival <> '0000-00-00', concat('[', order_item_expected_arrival_changes, '] ', right(order_item_expected_arrival,2), '.', month(order_item_expected_arrival), '.', right(year(order_item_expected_arrival),2)), 'n/a'), ".
                   "    '\n', if(order_item_arrival <> '0000-00-00', concat('[', order_item_arrival_changes, '] ', right(order_item_arrival,2), '.', month(order_item_arrival), '.', right(year(order_item_arrival),2)), 'n/a')) as dates_2 ". 
                   "from order_items ".
                   "left join orders on order_id = order_item_order " .
                   "left join projects on project_order = order_id " .
                   "left join addresses on order_item_forwarder_address = addresses.address_id " .
                   "left join items on order_item_item = item_id ".
                   "left join users on order_retail_operator = user_id " .
                   "left join countries on order_shop_address_country = country_id";

$ordernumber = param("o");
$country = param("c");
$ponumber = param("p");
$shipmentcode = param("s");
$supplier = param("su");
$forwarder = param("fw");
$item = param("item");

$selection = "";
if($ordernumber)
{
    $selection = "order_id = " . $ordernumber . " and ";
}
if($country)
{
    $selection = $selection . "order_shop_address_country = " . $country . " and ";
}
if($ponumber)
{
    $selection = $selection . "order_item_po_number = '" . $ponumber . "' and ";
}
if($shipmentcode)
{
    $selection = $selection . "order_item_shipment_code = '" . $shipmentcode . "' and ";
}
if($supplier)
{
    $selection = $selection . "order_item_supplier_address = " . $supplier . " and ";
}
if($forwarder)
{
    $selection = $selection . "order_item_forwarder_address = " . $forwarder . " and ";
}
if($rto)
{
    $selection = $selection . "order_retail_operator = " . $rto . " and ";
}
if($rtc)
{
    $selection = $selection . "project_retail_coordinator = " . $rtc . " and ";
}
if($item)
{
    $selection = $selection . "order_item_item = '" . $item . "' and ";
}
if($ordertype)
{
	$selection = $selection . "order_type = '" . $ordertype . "' and ";
}

if (!has_access("has_access_to_all_delivery_data")) // make them only see their own data
{
    if(in_array(5, $user_roles) or in_array(29, $user_roles)) // supplier or warehouse
    {
        $selection = $selection . "order_item_supplier_address = " . $user_data["address"] . " and ";   
    }
    elseif(in_array(6, $user_roles)) // user is a forwarder
    {
        $selection = $selection . "order_item_forwarder_address = " . $user_data["address"] . " and ";    
    }
}

$list_filter = $selection . 
               "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
			   "    and (order_item_arrival is null or order_item_arrival = '0000-00-00') " .
               "    and order_show_in_delivery = 1 ".
               "    and order_archive_date is null ".
               "    and order_item_type <= " . ITEM_TYPE_SPECIAL;

// create list filter
if (!has_access("has_access_to_all_delivery_data"))
{
    if($ordertype)
	{
		$filter = get_user_specific_order_list(user_id(), $ordertype);
	}
	else
	{

		$filter = " (" . get_user_specific_order_list(user_id(), 1);
		$filter .= " or " . get_user_specific_order_list(user_id(), 2) . ")";
	}

	
    
    $list_filter = $filter .
                   "    and " . $selection . 
                   "    (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
		           "    and (order_item_arrival is null or order_item_arrival = '0000-00-00') " .
                   "    and order_show_in_delivery = 1 ".
                   "    and order_archive_date is null ".
                   "    and order_item_type <= " . ITEM_TYPE_SPECIAL;
}

/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql_order_items, LIST_HAS_SEPARATOR);
$list->add_hidden("frwd", param("frwd"));
$list->add_hidden("showall", param("showall"));

$list->set_entity("Delivery Data");
$list->set_filter($list_filter);

$list->set_group("group_head", "group_head");
$list->set_order("order_number, address_company, item_shortcut, order_item_po_number");


if (has_access("can_edit_traffic_data_in_orders") or has_access("can_edit_traffic_data_in_projects"))
{
    $list->add_column("sup_item", "Forwarder\nQuantity/Item Code", "delivery_redirect_page.php?page_type=forwarder", LIST_FILTER_NONE, "", COLUMN_NO_WRAP | COLUMN_BREAK);
}
else
{
    $list->add_column("sup_item", "Forwarder", "", LIST_FILTER_NONE, "", COLUMN_NO_WRAP | COLUMN_BREAK);
}


$list->add_column("po_shipment", "P.O. Number\nShipment Code", "", LIST_FILTER_NONE, "", COLUMN_NO_WRAP | COLUMN_BREAK);

$list->add_column("date_titles_1", "", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK);
$list->add_column("dates_1", "", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK);

$list->add_column("date_titles_2", "", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK);
$list->add_column("dates_2", "", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK);

$list->add_button(FORM_BUTTON_BACK, "Back");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();

$page = new Page("delivery");

$link = "delivery_by_forwarder_pdf.php";
$link = $link . "?o=" . $ordernumber;
$link = $link . "&t=" . $ordertype;
$link = $link . "&c=" . $country;
$link = $link . "&p=" . $ponumber;
$link = $link . "&s=" . $shipmentcode;


if(has_access("has_access_to_all_delivery_data"))
{
    $link = $link . "&su=" . $supplier;
    $link = $link . "&fw=" . $forwarder;
    $link = $link . "&rto=" . $rto;
    $link = $link . "&rtc=" . $rtc;
	$link = $link . "&item=" . $item;
}
elseif(in_array(5, $user_roles) or in_array(29, $user_roles)) // supplier or warehouse
{
    $link = $link . "&fw=" . $forwarder;
}
elseif(in_array(6, $user_roles)) // user is a forwarder
{
    $link = $link . "&su=" . $supplier;
}
else
{
    $link = $link . "&su=" . $supplier;
    $link = $link . "&fw=" . $forwarder;
    $link = $link . "&rto=" . $rto;
    $link = $link . "&rtc=" . $rtc;

}

$page->register_action('print_pdf', 'Print PDF', $link, "_blank");  
$page->register_action('dummy', '', "");
$page->register_action('home', 'Home', "welcome.php");

$page->header();
$page->title("Delivery Data by Forwarder");

$list->render();

$page->footer();

?>