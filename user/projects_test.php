<?php
/********************************************************************

    projects.php

    Entry page for the projects section.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-06-20
    Version:        1.1.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/order_state_constants.php";

check_access("can_view_projects");


$page = new Page("projects");

if (has_access("can_create_new_projects"))
{
    $page->register_action('new', 'New Project', "project_new_01.php");
}

$page->register_action('home', 'Home', "welcome.php");

$page->header();
$page->title("Projects");

$page->footer();
?>