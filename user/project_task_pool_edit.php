<?php
/********************************************************************

    project_task_pool_edit.php

    Edit task details.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2007-04-05
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2007-04-05
    Version:        1.0.0

    Copyright (c) 2007, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("has_accessto_taskpool_in_projects");


// read order details
$project = get_project(param("pid"));
$order_id = $project["project_order"];

// get company's address
$client_address = get_address($project["order_client_address"]);


$form = new Form("tasks", "task");

$form->add_section("Project");
$form->add_hidden("pid",param("pid"));
$form->add_hidden("task_from_user");
$form->add_hidden("task_order_state");

require_once "include/project_head_small.php";

$form->add_section("Task");
$form->add_label("task_text", "Task");
$form->add_lookup("task_user", "Old User", "users" , "concat(user_name, ' ', user_firstname)", 0, "{task_user}");

$sql = "select user_id, concat(user_name, ' ', user_firstname) as uname " .
       "from users " .
       "where user_active = 1 " .
       "order by user_name, user_firstname";

$form->add_list("new_user", "New User", $sql);

$form->add_button("save", "Save");
$form->add_button("goback", "Back");

$form->populate();



$link = "project_task_pool.php?&pid=" . param('pid'); 

if ($form->button("goback"))
{
    redirect($link);
}
elseif ($form->button("save"))
{
    
    if($form->value("new_user"))
    {
        $sql = "update tasks set " .
               "task_user = " . $form->value("new_user") . 
               " where task_id = " . id();
        
        $result = mysql_query($sql);
        
        
        // create E-Mails Subject
        
        //get order state code
        $sql = "select order_state_code " .
               "from order_states " . 
               "where order_state_id = " . $form->value("task_order_state");

        $res = mysql_query($sql) or dberror($sql);
        $row = mysql_fetch_assoc($res);
        $order_state_code = $row["order_state_code"];

        $action_parameter = get_action_parameter($order_state_code, 1);

        $sql = "select order_shop_address_company, " .
               "order_shop_address_place, country_name " .
               "from projects " .
               "left join orders on order_id = project_order " .
               "left join countries on country_id = order_shop_address_country " .
               "where project_id = " . $project["project_id"];

        $res = mysql_query($sql) or dberror($sql);
        $row = mysql_fetch_assoc($res);
        $subject = $action_parameter["name"] . " - Project " . $project["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];

        //get sender email
        $sql = "select user_email, user_name, user_firstname  " .
               "from users " . 
               "where user_id = " . $form->value("task_from_user");

        $res = mysql_query($sql) or dberror($sql);
        $row = mysql_fetch_assoc($res);
        $from_email = $row["user_email"];
        $sender_name = $row["user_name"] . " " . $row["user_firstname"];

        //get reciepient email
        $sql = "select user_email " .
               "from users " . 
               "where user_id = " . $form->value("new_user");

        $res = mysql_query($sql) or dberror($sql);
        $row = mysql_fetch_assoc($res);
        $to_email = $row["user_email"];

        //send mail
        
        $mail = new Mail();
        $mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
        $mail->set_sender($from_email, $sender_name);

        $bodytext0 = $form->value("task_text") . "\n\n";
        $link ="project_task_center.php?pid=" . param("pid");
        $bodytext = $bodytext0 . "click below to have direct access to the project:\n";
        $bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
        $mail->add_text($bodytext);

        $mail->add_recipient($to_email);

        $mail->send();

        append_mail($project["project_order"], $form->value("new_user"), $form->value("task_from_user"), $bodytext0, $order_state_code, 1);

        $form->message("Your changes have been saved.");
    }
    else
    {
        $form->error("Please indicate an alternative user!");
    }
}


$page = new Page("tasks");

require "include/project_page_actions.php";

$page->header();
$page->title("Edit Task");
$form->render();
$page->footer();

?>