<?php
/********************************************************************

    order_new.php

    Category selection when creating new orders.

    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date created:   2002-07-31
    Modified by:    Anton.Hofmann (aho@mediaparx.ch)
    Date modified:  2003-07-14
    Version:        1.0.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/order_functions.php";
require_once "include/get_functions.php";

check_access("can_create_new_orders");

set_referer("orders.php");
set_referer("basket.php");
set_referer("order_category.php");
set_referer("project_new_00.php");

$page = new Page("orders");
if (!basket_is_empty())
{
    $page->register_action('basket', 'Shopping List');
}

/********************************************************************
    prepare all data needed
*********************************************************************/
$roles = get_user_roles(user_id());

// get the region of users' address
$user_region = get_user_region(user_id());

$sql =  "select category_id, category_name, product_line_priority, product_line_name " .
        "from categories ".
        "left join product_lines on product_line_id = category_product_line " .
        "left join productline_regions on productline_region_productline =  product_line_id";

if (count($roles) == 1 and $roles[0] == 4) // user is a client
{
    $list1_filter = "product_line_visible = 1 and (category_not_in_use is null or category_not_in_use = 0) and category_catalog = 1 and product_line_clients=1 and productline_region_region = " . $user_region;
}
else
{
   $list1_filter = "product_line_visible = 1 and (category_not_in_use is null or category_not_in_use = 0) and category_catalog = 1 and productline_region_region = " . $user_region;
}


/********************************************************************
    Search Form
*********************************************************************/ 
$form = new Form("orders", "order");
$form->add_comment("Enter a search term or choose from the list of item categories below.<br /><br />");
$form->add_edit("searchterm", "Search Term");


$form->add_button("search", "Search Catalog");
$form->add_button(LIST_BUTTON_BACK, "Back");
$form->add_button("cancel", "Cancel Ordering");


/********************************************************************
    Create List of product categories
*********************************************************************/ 
$list1 = new ListView($sql, LIST_HAS_HEADER);

$list1->set_entity("productlines");
$list1->set_order("category_priority");
$list1->set_group("product_line_priority", "product_line_name");
$list1->set_title("Item Categories");
$list1->set_filter($list1_filter);

//$list1->add_column("category_name", "Product Line", "order_category.php");
$list1->add_column("category_name", "Product Line", "order_new_select.php");




/********************************************************************
    Populate page
*********************************************************************/ 
$form->populate();
$form->process();

$list1->process();


if ($form->button("basket"))
{
    redirect("basket.php");
}
elseif ($form->button("cancel"))
{
    redirect("orders.php");
}
elseif ($form->button("search"))
{
	if(!$form->value("searchterm"))
	{
		$form->error("The search term must not be empty.");
	}
	else
	{
		$link = "order_new_select.php?searchterm=" . $form->value("searchterm");
		redirect($link);
	}
}

$page->register_action('home', 'Home', "welcome.php");

$page->header();
$page->title('New Catalog Order');
$form->render();

echo '<br /><br />';
$list1->render();
$page->footer();

?>