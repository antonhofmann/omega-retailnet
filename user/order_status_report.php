<?php
/********************************************************************

    order_status_report.php

    Shows a status report of the actual order

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-11-26
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-11-26
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("has_access_to_order_status_report_in_orders");

register_param("oid");
register_param("os");


/********************************************************************
    prepare all data needed
*********************************************************************/
// read order details
$order = get_order(param("oid"));


// get company's address
$client_address = get_address($order["order_client_address"]);


/********************************************************************
   get order's pending tasks
*********************************************************************/

$sql_open_tasks = "select task_id, order_state_code, order_state_name, " .
                  "CONCAT(users.user_name, ' ', users.user_firstname) as userto, " .
                  "CONCAT(users1.user_name, ' ', users1.user_firstname) as userfrom, " .
                  "DATE_FORMAT(tasks.date_created,'%d.%m.%y') as date_of_task " .
                  "from tasks " .
                  "left join users on users.user_id = task_user " .
                  "left join users as users1 on users1.user_id = task_from_user " .
                  "left join order_states on order_state_id = task_order_state ";
            
$filter_open_tasks =  "task_done_date is null " .
                      "  and task_order = " . param("oid");


/********************************************************************
   get items not having a P.O. Number
*********************************************************************/
$sql_items_no_po = "select address_shortcut, item_code, order_item_text, " .
                     "order_item_po_number, order_item_quantity  " .
                     "from order_items " .
                     "left join items on item_id = order_item_item " .
                     "left join addresses on address_id = order_item_supplier_address ";

$filter_items_no_po = "order_item_type <= " . ITEM_TYPE_SPECIAL .
                        "    and order_item_order = " . $order["order_id"] .
                        "    and (order_item_not_in_budget is null or order_item_not_in_budget =0) " .
                        "    and (order_item_po_number = '' or order_item_po_number is null)";


/********************************************************************
   get items not having been ordered
*********************************************************************/
$sql_items_not_ordered = "select address_shortcut, item_code, order_item_text, " .
                         "order_item_po_number, order_item_quantity " .
                         "from order_items " .
                         "left join items on item_id = order_item_item " .
                         "left join addresses on address_id = order_item_supplier_address ";

$filter_items_not_ordered = "order_item_type <= " . ITEM_TYPE_SPECIAL .
                            "    and order_item_order = " . $order["order_id"] .
                            "    and (order_item_not_in_budget is null or order_item_not_in_budget =0) " .
                            "    and (order_item_ordered is null or order_item_ordered='0000-00-00')";


/********************************************************************
   get items not having a ready for pick up date
*********************************************************************/
$sql_items_with_no_pickup = "select address_shortcut, item_code, order_item_text, " .
                            "order_item_po_number, order_item_quantity " .
                            "from order_items " .
                            "left join items on item_id = order_item_item " .
                            "left join addresses on address_id = order_item_supplier_address ";

$filter_items_with_no_pickup = "order_item_type <= " . ITEM_TYPE_SPECIAL .
                                "    and order_item_order = " . $order["order_id"] .
                                "    and (order_item_not_in_budget is null or order_item_not_in_budget =0) " .
                                "    and (order_item_ready_for_pickup is null or order_item_ready_for_pickup='0000-00-00')";



/********************************************************************
   get items not having a pick up date
*********************************************************************/
$sql_items_with_no_pickup2 = "select address_shortcut, item_code, order_item_text, " .
                             "order_item_po_number, order_item_quantity " .
                             "from order_items " .
                             "left join items on item_id = order_item_item " .
                             "left join addresses on address_id = order_item_forwarder_address ";

$filter_items_with_no_pickup2 = "order_item_type <= " . ITEM_TYPE_SPECIAL .
                                "    and order_item_order = " . $order["order_id"] .
                                "    and (order_item_not_in_budget is null or order_item_not_in_budget =0) " .
                                "    and (order_item_pickup is null or order_item_pickup='0000-00-00')";


/********************************************************************
   get items not having an expexted arrival date
*********************************************************************/
$sql_items_expected_arrival = "select address_shortcut, item_code, order_item_text, " .
                              "order_item_po_number, order_item_quantity " .
                              "from order_items " .
                              "left join items on item_id = order_item_item " .
                              "left join addresses on address_id = order_item_forwarder_address ";

$filter_items_expected_arrival = "order_item_type <= " . ITEM_TYPE_SPECIAL .
                                 "    and order_item_order = " . $order["order_id"] .
                                 "    and (order_item_not_in_budget is null or order_item_not_in_budget =0) " .
                                 "    and (order_item_expected_arrival is null or order_item_expected_arrival='0000-00-00')";


/********************************************************************
   get items not having an  arrival date
*********************************************************************/
$sql_items_arrival = "select address_shortcut, item_code, order_item_text, " .
                      "order_item_po_number, order_item_quantity " .
                      "from order_items " .
                      "left join items on item_id = order_item_item " .
                      "left join addresses on address_id = order_item_forwarder_address ";

$filter_items_arrival = "order_item_type <= " . ITEM_TYPE_SPECIAL .
                         "    and order_item_order = " . $order["order_id"] .
                         "    and (order_item_not_in_budget is null or order_item_not_in_budget =0) " .
                         "    and (order_item_arrival is null or order_item_arrival='0000-00-00')";



/********************************************************************
   supplier: history subprocesses
*********************************************************************/
$s_sql = array();
$s_filter = array();

$sql_suppl = "select DISTINCT address_id, address_shortcut, " .
             "concat(address_shortcut, ': ', user_name, ' ', user_firstname) as ogroup, user_id " .
             "from order_items " .
             "left join addresses on address_id = order_item_supplier_address " . 
             "left join users on user_address = address_id";

$filter_suppl = "order_item_type <= " . ITEM_TYPE_SPECIAL .
                "    and order_item_order = " . $order["order_id"] .
                "    and (order_item_not_in_budget is null or order_item_not_in_budget =0) " .
                "    and order_item_supplier_address > 0";


$sql = $sql_suppl . " where " . $filter_suppl;

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
    $sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name, " .
              "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
              "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
              "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y') as performed " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id " . "left join actual_order_states on actual_order_state_order = " . param("oid") . " and actual_order_state_state = order_state_id " .
              "left join users on users.user_id = actual_order_state_user " .
              "left join order_mails on order_mail_order = " . param("oid") . " " .
              "and order_mail_order_state = order_state_id " .
              "left join users as tousers on tousers.user_id = order_mail_user " .
              "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

    $filter_hi = "order_state_group_order_type = 2 " .
                 "and (users.user_id = " . $row["user_id"] . 
                 "  or tousers.user_id = " . $row["user_id"] . ")" ;

    $s_sql[$row["ogroup"]] = $sql_hi;
    $s_filter[$row["ogroup"]] = $filter_hi;
}

/********************************************************************
   forwarders: history subprocesses
*********************************************************************/
$f_sql = array();
$f_filter = array();

$sql_forw = "select DISTINCT address_id, address_shortcut, " .
             "concat(address_shortcut, ': ', user_name, ' ', user_firstname) as ogroup, user_id " .
             "from order_items " .
             "left join addresses on address_id = order_item_forwarder_address " .
             "left join users on user_address = address_id";

$filter_forw = "order_item_type <= " . ITEM_TYPE_SPECIAL .
                "    and order_item_order = " . $order["order_id"] .
                "    and (order_item_not_in_budget is null or order_item_not_in_budget =0) " .
                "    and order_item_forwarder_address > 0";


$sql = $sql_forw . " where " . $filter_forw;

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
    
    
    $sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name, " .
              "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
              "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
              "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y') as performed " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id " . "left join actual_order_states on actual_order_state_order = " . param("oid") . " and actual_order_state_state = order_state_id " .
              "left join users on users.user_id = actual_order_state_user " .
              "left join order_mails on order_mail_order = " . param("oid") . " " .
              "and order_mail_order_state = order_state_id " .
              "left join users as tousers on tousers.user_id = order_mail_user " .
              "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

    $filter_hi = "order_state_group_order_type = 2 " .
                  "and (users.user_id = " . $row["user_id"] . 
                 "  or tousers.user_id = " . $row["user_id"] . ")" ;

    $f_sql[$row["ogroup"]] = $sql_hi;
    $f_filter[$row["ogroup"]] = $filter_hi;
}


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("orders", "order_status_report", 640);
$form->add_hidden("oid", param("oid"));

$form->add_section("Order");

require_once "include/order_head_small.php";


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Create List of pending tasks for this order
*********************************************************************/ 
$order_open_tasks = new ListView($sql_open_tasks, LIST_HAS_HEADER);

$order_open_tasks->set_title("Pending Tasks for this Order");
$order_open_tasks->set_entity("task");
$order_open_tasks->set_filter($filter_open_tasks);
$order_open_tasks->set_order("userto");

$order_open_tasks->add_column("userto", "Task for User");
$order_open_tasks->add_column("userfrom", "Task from User");
$order_open_tasks->add_column("order_state_name", "From Action");
$order_open_tasks->add_column("order_state_code", "Status");
$order_open_tasks->add_column("date_of_task", "Date");



/********************************************************************
    Create List of items not having a P.O. Number
*********************************************************************/ 
$items_no_po = new ListView($sql_items_no_po, LIST_HAS_HEADER);

$items_no_po->set_title("Items not Having a P.O. Number");
$items_no_po->set_entity("item");
$items_no_po->set_filter($filter_items_no_po);
$items_no_po->set_order("address_shortcut");

$items_no_po->add_column("order_item_po_number", "P.O. Number");
$items_no_po->add_column("address_shortcut", "Supplier");
$items_no_po->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
$items_no_po->add_column("item_code", "Item");
$items_no_po->add_column("order_item_text", "Item");



/********************************************************************
    Create List of items not having been ordered 
*********************************************************************/ 
$items_not_ordered = new ListView($sql_items_not_ordered, LIST_HAS_HEADER);

$items_not_ordered->set_title("Items not Having an Order to Supplier");
$items_not_ordered->set_entity("item");
$items_not_ordered->set_filter($filter_items_not_ordered);
$items_not_ordered->set_order("address_shortcut");

$items_not_ordered->add_column("address_shortcut", "Supplier");
$items_not_ordered->add_column("order_item_po_number", "P.O. Number");
$items_not_ordered->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
$items_not_ordered->add_column("item_code", "Item");
$items_not_ordered->add_column("order_item_text", "Item");


/********************************************************************
    Create List of items not having a ready for pick up date
*********************************************************************/ 
$items_with_no_pickup = new ListView($sql_items_with_no_pickup, LIST_HAS_HEADER);

$items_with_no_pickup->set_title("Items not Having a Ready for Pick Up Date");
$items_with_no_pickup->set_entity("item");
$items_with_no_pickup->set_filter($filter_items_with_no_pickup);
$items_with_no_pickup->set_order("address_shortcut");

$items_with_no_pickup->add_column("address_shortcut", "Supplier");
$items_with_no_pickup->add_column("order_item_po_number", "P.O. Number");
$items_with_no_pickup->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
$items_with_no_pickup->add_column("item_code", "Item");
$items_with_no_pickup->add_column("order_item_text", "Item");


/********************************************************************
    Create List of items not having a pick up date
*********************************************************************/ 
$items_with_no_pickup2 = new ListView($sql_items_with_no_pickup2, LIST_HAS_HEADER);

$items_with_no_pickup2->set_title("Items not Having a Pick Up Date");
$items_with_no_pickup2->set_entity("item");
$items_with_no_pickup2->set_filter($filter_items_with_no_pickup2);
$items_with_no_pickup2->set_order("address_shortcut");

$items_with_no_pickup2->add_column("address_shortcut", "Forwarder");
$items_with_no_pickup2->add_column("order_item_po_number", "P.O. Number");
$items_with_no_pickup2->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
$items_with_no_pickup2->add_column("item_code", "Item");
$items_with_no_pickup2->add_column("order_item_text", "Item");


/********************************************************************
    Create List of items not having an expected arrival date
*********************************************************************/ 
$items_expected_arrival = new ListView($sql_items_expected_arrival, LIST_HAS_HEADER);

$items_expected_arrival->set_title("Items not Having an Expected Arrival Date");
$items_expected_arrival->set_entity("item");
$items_expected_arrival->set_filter($filter_items_expected_arrival);
$items_expected_arrival->set_order("address_shortcut");

$items_expected_arrival->add_column("address_shortcut", "Forwarder");
$items_expected_arrival->add_column("order_item_po_number", "P.O. Number");
$items_expected_arrival->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
$items_expected_arrival->add_column("item_code", "Item");
$items_expected_arrival->add_column("order_item_text", "Item");


/********************************************************************
    Create List of items not having an  arrival date
*********************************************************************/ 
$items_arrival = new ListView($sql_items_arrival, LIST_HAS_HEADER);

$items_arrival->set_title("Items not Having an Arrival Date");
$items_arrival->set_entity("item");
$items_arrival->set_filter($filter_items_arrival);
$items_arrival->set_order("address_shortcut");

$items_arrival->add_column("address_shortcut", "Forwarder");
$items_arrival->add_column("order_item_po_number", "P.O. Number");
$items_arrival->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
$items_arrival->add_column("item_code", "Item");
$items_arrival->add_column("order_item_text", "Item");


/********************************************************************
    Create History of supplier
*********************************************************************/ 
$s_lists = array();
foreach($s_sql as $key=>$value)
{
    $s_lists[$key] = new ListView($value, LIST_HAS_HEADER);

    $s_lists[$key]->set_title("History for Supplier " . $key);
    $s_lists[$key]->set_entity("order_state");
    $s_lists[$key]->set_filter($s_filter[$key]);
    $s_lists[$key]->set_order("order_state_code, actual_order_states.date_created ASC");
    $s_lists[$key]->set_group("order_state_group_code", "order_state_group_name");

    $s_lists[$key]->add_column("order_state_code", "Step");
    $s_lists[$key]->add_column("order_state_action_name", "Action");

    $s_lists[$key]->add_column("fullname", "Performed by");
    $s_lists[$key]->add_column("performed", "Date");
    $s_lists[$key]->add_column("tofullname", "Recipient");

}


/********************************************************************
    Create History of forwarder
*********************************************************************/ 
$f_lists = array();
foreach($f_sql as $key=>$value)
{
    $f_lists[$key] = new ListView($value, LIST_HAS_HEADER);

    $f_lists[$key]->set_title("History for Forwarder " . $key);
    $f_lists[$key]->set_entity("order_state");
    $f_lists[$key]->set_filter($f_filter[$key]);
    $f_lists[$key]->set_order("order_state_code, actual_order_states.date_created ASC");
    $f_lists[$key]->set_group("order_state_group_code", "order_state_group_name");

    $f_lists[$key]->add_column("order_state_code", "Step");
    $f_lists[$key]->add_column("order_state_action_name", "Action");

    $f_lists[$key]->add_column("fullname", "Performed by");
    $f_lists[$key]->add_column("performed", "Date");
    $f_lists[$key]->add_column("tofullname", "Recipient");

}



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$order_open_tasks->populate();
$order_open_tasks->process();


$items_no_po->populate();
$items_no_po->process();

$items_not_ordered->populate();
$items_not_ordered->process();

$items_with_no_pickup2->populate();
$items_with_no_pickup2->process();

$items_with_no_pickup2->populate();
$items_with_no_pickup2->process();

$items_expected_arrival->populate();
$items_expected_arrival->process();

$items_arrival->populate();
$items_arrival->process();

foreach($s_sql as $key=>$value)
{
    $s_lists[$key]->populate();
    $s_lists[$key]->process();
}

foreach($f_sql as $key=>$value)
{
    $f_lists[$key]->populate();
    $f_lists[$key]->process();
}

/********************************************************************
    render page
*********************************************************************/
$page = new Popup_Page("orders");

$page->header();
$page->title("Status Report of Order - " . $order["order_number"]);

   
$form->render();

$order_open_tasks->render();

$items_no_po->render();
$items_not_ordered->render();
$items_with_no_pickup->render();
$items_with_no_pickup2->render();
$items_expected_arrival->render();
$items_arrival->render();

foreach($s_sql as $key=>$value)
{
    $s_lists[$key]->render();
}

foreach($f_sql as $key=>$value)
{
    $f_lists[$key]->render();
}

$page->footer();

?>