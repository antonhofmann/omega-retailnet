<?php
/********************************************************************


    project_edit_attachment.php


    Add comments to a project


    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-10-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-06-18
    Version:        1.0.1


    Copyright (c) 2002, Swatch AG, All Rights Reserved.


*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_edit_attachment_data_in_projects");

/********************************************************************
    prepare all data needed
*********************************************************************/

$user_roles = get_user_roles(user_id());

// read project and order details
$project = get_project(param("pid"));


// get company's address
$client_address = get_address($project["order_client_address"]);


// buld sql for attachment categories
$sql_attachment_categories = "select order_file_category_id, order_file_category_name ".
                             "from order_file_categories ".
                             "where order_file_category_type = 2 ".
                             "order by order_file_category_priority";






// buld sql for file types
$sql_file_types = "select file_type_id, file_type_name ".
                  "from file_types ".
                  "order by file_type_name";


// get addresses involved in the project
$companies = get_involved_companies($project["project_order"], id());


// determine users that have already got an email announcing the new attachment
$old_recipients = array();
foreach($companies as $key=>$value)
{
    if($value["access"] == 1)
    {
        $old_recipients[] = $value["user"];
    }
}



// checkbox names
$check_box_names = array();


// get file owner
$owner = get_file_owner(id());


/********************************************************************
    Create Form
*********************************************************************/ 


$form = new Form("order_files", "file");


$form->add_section("Project");
$form->add_hidden("pid", param("pid"));
$form->add_hidden("order_file_order", $project["project_order"]);
$form->add_hidden("order_file_owner", user_id());


require_once "include/project_head_small.php";


$form->add_section("Attachment");
$form->add_list("order_file_category", "Category*", $sql_attachment_categories, NOTNULL);
$form->add_edit("order_file_title", "Title*", NOTNULL, "", TYPE_CHAR);
$form->add_multiline("order_file_description", "Description*", 4);


if (has_access("can_set_attachment_accessibility_in_projects"))
{
    $form->add_section("Accessibility");
    $form->add_comment("Please indicate who is allowed to view this attachment.");


    $num_checkboxes=1;
    foreach ($companies as $key=>$value_array)
    {
        if(!array_key_exists('role', $value_array)) {
			$value_array["role"] = "";
		}
		$form->add_checkbox("A" . $num_checkboxes, $value_array["name"], $value_array["access"], 0, $value_array["role"]);
        $check_box_names["A" . $num_checkboxes] = $value_array["user"];
        $num_checkboxes++;
    }
}

$order_number = $project["order_number"];


$form->add_section("File");
$form->add_list("order_file_type", "File Type*", $sql_file_types, NOTNULL);
$form->add_upload("order_file_path", "File", "/files/orders/$order_number", NOTNULL);


if(in_array(5, $user_roles) or in_array(6, $user_roles) or in_array(7, $user_roles))
{
	$form->add_hidden("order_file_attach_to_cer_booklet", 0);
}
else
{
	$form->add_section("CER/AF Booklet Options");
	$form->add_checkbox("order_file_attach_to_cer_booklet", "Attach this file to the AF/CER booklet", "", 0, "AF/CER Booklet");
}


$form->add_section("CC Recipients");
$form->add_modal_selector("ccmails", "Selected Recipients", 8);

if(has_access("can_edit_catalog")) {
	$form->add_section("");
	$form->add_checkbox("donotsendmail", "Do NOT send mail upon changement", 0, 0, "Send Mail Options");
	$form->add_checkbox("sendonlytocc", "Send notification only to CC Recipients", 0, 0, "");
}
else
{
	$form->add_hidden("donotsendmail", 0);
	$form->add_hidden("sendonlytocc", 0);
}


$form->add_button(FORM_BUTTON_SAVE, "Save");


if(has_access("can_delete_attachment_in_projects")  or $owner == user_id())
{
    $form->add_button("delete", "Delete");
}

$form->add_button(FORM_BUTTON_BACK, "Back");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


if ($form->button(FORM_BUTTON_SAVE))
{
    // check if a recipient was selected
    if (has_access("can_set_attachment_accessibility_in_projects"))
    {
        $no_recipient = 1;
    }
    else
    {
        $no_recipient = 0;
    }


    foreach ($form->items as $item)
    {
        if ($item["type"] == "checkbox" and $item["value"] == "1")
        {
            $no_recipient = 0;
        }
    }

	$invalid_file_extension = false;
	
	if($form->value("order_file_category") == 11)
	{
		$ext = pathinfo($form->value("order_file_path"), PATHINFO_EXTENSION);
		
		if($ext != 'jpg' and $ext != 'JPG')
		{
			$invalid_file_extension = true;
		}
	}


    if($form->value("sendonlytocc") == 1) //send attachment infor only to cc mails
	{

		// send email notifocation to the retail staff
        $order_id = $project["project_order"];


        $sql = "select order_id, order_number, " .
               "order_shop_address_company, order_shop_address_place, " .
               "project_retail_coordinator, country_name, ".
               "users.user_email as recepient1, users.user_address as address_id1, " .
               "users2.user_email as recepient2, users2.user_address as address_id2, " .
               "users1.user_email as sender, concat(users1.user_name, ' ', users1.user_firstname) as user_fullname ".
               "from orders ".
               "left join projects on project_order = " . $order_id . " " .
               "left join countries on country_id = order_shop_address_country " .
               "left join users on order_retail_operator = users.user_id ".
               "left join users as users1 on " . user_id() . "= users1.user_id ".
               "left join users as users2 on project_retail_coordinator = users2.user_id ".
               "where order_id = " . $order_id;


        $res = mysql_query($sql) or dberror($sql);
        if ($form->value("donotsendmail") == 0 and $row = mysql_fetch_assoc($res) and $row["recepient2"])
        {
			$subject = MAIL_SUBJECT_PREFIX . ": New attachment was added - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " .  $row["order_shop_address_company"];


            $sender_email = $row["sender"];
            $sender_name =  $row["user_fullname"];


            $mail = new Mail();
            $mail->set_subject($subject);
            $mail->set_sender($sender_email, $sender_name);


            $bodytext0 = "An attachment has been updated by " . $sender_name . ":";
			$bodytext1 = "An attachment has been updated by " . $sender_name . " for:";


            $recipeint_added = 0;
			$reciepients = array();
			
			//get all ccmails
			$ccmails =  explode(' ', preg_replace("'\r?\n'"," ",$form->value('ccmails'))); 
			foreach($ccmails as $ccmail) {
				if(is_email_address($ccmail)) {
					
					$reciepients[strtolower($ccmail)] = strtolower($ccmail);
					$recipeint_added = 1;
				}
			
			}


						
			if($recipeint_added == 1)
            {
				foreach($reciepients as $key=>$email)
				{
					$bodytext1 = $bodytext1 . "\n" . $email;
					$mail->add_recipient($email);
				}
			}
			

			$bodytext0.= "\n\n<--\n";
            $bodytext0.= $form->value("order_file_title") . "\n";
            
			/*
			if($form->value("order_file_description"))
            {
                $bodytext0.= $form->value("order_file_description") . "\n";
            }
            $bodytext0.= "download the file here:\n" . APPLICATION_URL . $form->value("order_file_path");
            $bodytext0.= "\n-->";
			*/

            $link ="project_view_attachment.php?pid=" . param("pid") . "&id=" .  id();
            $bodytext = $bodytext0 . "\n\nclick below to have direct access to the project:\n";
            $bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
            $mail->add_text($bodytext);
            

			$mail->send();


			$bodytext1.= "\n\n<--\n";
            $bodytext1.= $form->value("order_file_title") . "\n";
            $bodytext1.= "-->";

			$bodytext = $bodytext1 . "\n\nclick below to have direct access to the project:\n";
            $bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";   

            if($recipeint_added == 1)
            {
				append_mail($project["project_order"], "" , user_id(), $bodytext, "", 1);
            }
        }


        $link = "project_view_attachments.php?pid=" . param("pid");
        redirect($link);
	}
	
	elseif ($form->validate() and $no_recipient == 0 and $invalid_file_extension == false)
    {
		update_attachment_accessibility_info(id(), $form,  $check_box_names);

		// send email notifocation to the retail staff
        $order_id = $project["project_order"];


        $sql = "select order_id, order_number, " .
               "order_shop_address_company, order_shop_address_place, " .
               "project_retail_coordinator, country_name, ".
               "users.user_email as recepient1, users.user_address as address_id1, " .
               "users2.user_email as recepient2, users2.user_address as address_id2, " .
               "users1.user_email as sender, concat(users1.user_name, ' ', users1.user_firstname) as user_fullname ".
               "from orders ".
               "left join projects on project_order = " . $order_id . " " .
               "left join countries on country_id = order_shop_address_country " .
               "left join users on order_retail_operator = users.user_id ".
               "left join users as users1 on " . user_id() . "= users1.user_id ".
               "left join users as users2 on project_retail_coordinator = users2.user_id ".
               "where order_id = " . $order_id;


        $res = mysql_query($sql) or dberror($sql);
        if ($form->value("donotsendmail") == 0 and $row = mysql_fetch_assoc($res) and $row["recepient2"])
        {
			$subject = MAIL_SUBJECT_PREFIX . ": New attachment was added - Project " . $row["order_number"] . ", " . $row["country_name"]  . ", " . $row["order_shop_address_company"];


            $sender_email = $row["sender"];
            $sender_name =  $row["user_fullname"];


            $mail = new Mail();
            $mail->set_subject($subject);
            $mail->set_sender($sender_email, $sender_name);


            $bodytext0 = "An attachment has been updated by " . $sender_name . ":";
			$bodytext1 = "An attachment has been updated by " . $sender_name . " for:";


            $recipeint_added = 0;
			$reciepients = array();
			$reciepients_cc = array();
			$reciepients_dp = array();

			if($row["recepient1"])
			{
				$reciepients[strtolower($row["recepient1"])] = strtolower($row["recepient1"]);
			
			}
			if($row["recepient2"])
			{
				$reciepients[strtolower($row["recepient2"])] = strtolower($row["recepient2"]);
			}


			foreach ($check_box_names as $key=>$value)
            {
                if ($form->value($key) and !in_array($value, $old_recipients))
                {
					foreach($companies as $key1=>$company)
					{
						
						if($value == $company["user"])
						{
							$sql = "select user_email, user_email_cc, user_email_deputy ".
								   "from users ".
								   "where (user_id = " . $company["user"] . 
								   "   and user_active = 1)";
							
							$res1 = mysql_query($sql) or dberror($sql);
							if ($row1 = mysql_fetch_assoc($res1))
							{
								$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
								$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
								$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
								$recipeint_added = 1;
							}
						}
					}
                }
            }
			


			//get all ccmails
			$ccmails =  explode(' ', preg_replace("'\r?\n'"," ",$form->value('ccmails'))); 
			foreach($ccmails as $ccmail) {
				if(is_email_address($ccmail)) {
					
					$reciepients_cc[strtolower($ccmail)] = strtolower($ccmail);
				}
			}


						
			if($recipeint_added == 1)
            {
				foreach($reciepients as $key=>$email)
				{
					$bodytext1 = $bodytext1 . "\n" . $email;
					$mail->add_recipient($email);
				}


				foreach($reciepients_cc as $key=>$email)
				{
					$bodytext1 = $bodytext1 . "\n" . $email;
					$mail->add_cc($reciepients_cc[$email]);
				}

				foreach($reciepients_dp as $key=>$email)
				{
					$bodytext1 = $bodytext1 . "\n" . $email;
					$mail->add_cc($reciepients_dp[$email]);
				}
			}
			else
			{
				foreach($reciepients_cc as $key=>$email)
				{
					$bodytext1 = $bodytext1 . "\n" . $email;
					$mail->add_recipient($reciepients_cc[$email]);
					$recipeint_added = 1;
				}
			}

			$bodytext0.= "\n\n<--\n";
            $bodytext0.= $form->value("order_file_title") . "\n";
            
			/*
			if($form->value("order_file_description"))
            {
                $bodytext0.= $form->value("order_file_description") . "\n";
            }
            $bodytext0.= "download the file here:\n" . APPLICATION_URL . $form->value("order_file_path");
            $bodytext0.= "\n-->";
			*/

            
			
			$link ="project_view_attachment.php?pid=" . param("pid") . "&id=" .  id();
			$bodytext = $bodytext0 . "\n\nclick below to have direct access to the project:\n";
			$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";   
			
                   
            $mail->add_text($bodytext);
            

			$mail->send();


			$bodytext1.= "\n\n<--\n";
            $bodytext1.= $form->value("order_file_title") . "\n";
            $bodytext1.= "-->";

			$bodytext = $bodytext1 . "\n\nclick below to have direct access to the project:\n";
            $bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";   

            if($recipeint_added == 1)
            {
                append_mail($project["project_order"], "" , user_id(), $bodytext, "", 1);
            }
        }


        $link = "project_view_attachments.php?pid=" . param("pid");
        redirect($link);
    }
    else
    {
        if($invalid_file_extension == true)
		{
			$form->error("Please upload only files of the type 'jpg'.");
		}
		else
		{
			$form->error("Please select a least one person to have access to the attachment.");
		}
    }
}
elseif ($form->button("delete"))
{
    delete_attachment(id());
    $link = "project_view_attachments.php?pid=" . param("pid");
    redirect($link);
}




$page = new Page("projects");


require "include/project_page_actions.php";


$page->header();
$page->title("Edit Attachment Data");
$form->render();

?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#ccmails_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/shared/select_mail_recipients.php'
    });
    return false;
  });
});
</script>

<?php

$page->footer();


?>