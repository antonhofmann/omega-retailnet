<?php
/********************************************************************


    item_option_view.php


    Info Screen for item groups.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-08-04
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-08-04
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
check_access("can_view_catalog_detail");


/********************************************************************
    prepare all data needed
*********************************************************************/
$option = $_GET["option"];

// calculate price in addres-currency
$sql = "select item_id, concat_ws(' ', currency_symbol, " .
       "    round(((item_price / currency_exchange_rate) * currency_factor),2)) as prize " .
       "from items " .
       "left join users on users.user_id = " . user_id() . " " .
       "left join addresses on addresses.address_id = users.user_address " .
       "left join currencies on currencies.currency_id = addresses.address_currency " .
       "where item_id = " . id();


$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
    $item_prize = $row['prize'];
}

// Read name & code of the sown item
$sql_name = "select concat_ws(' - ', item_code, item_name) as item_label " .
            "from items where item_id = " . id();


$res = mysql_query($sql_name) or dberror();
if ($row = mysql_fetch_assoc($res))
{
    $item_label = $row['item_label'];
}

// attached files
$files_have_records = 0;
$sql_files = "select item_option_file_id, item_option_file_title, file_type_name, " .
             "    item_option_file_description, item_option_file_path " . 
             "from item_option_files " .
             "left join file_types on file_type_id = item_option_file_type ";


$sql = $sql_files . " where item_option_file_type <> 2 and item_option_file_option = " . $option;
$res = mysql_query($sql) or dberror();
if ($row = mysql_fetch_assoc($res))
{
    $files_have_records = 1;
}


/********************************************************************
    Create Form
*********************************************************************/ 
$form = new Form("items", "item");


$form->add_section("Product Properties");
$form->add_hidden("parent", param("parent"));
$form->add_lookup("item_code", "Product Code", "items", "item_code",HIDEEMPTY ,id());
$form->add_lookup("item_name", "Product Name", "items", "item_name",HIDEEMPTY ,id());
$form->add_label("item_prize", "Price", HIDEEMPTY , $item_prize);
$form->add_lookup("item_description", "Description", "items", "item_description",HIDEEMPTY ,id());
$form->add_lookup("item_watches_displayed", "Watches Displayed", "items",
                  "item_watches_displayed",HIDEEMPTY ,id());
$form->add_lookup("item_watches_stored", "Watches Stored", "items", 
                  "item_watches_stored",HIDEEMPTY ,id());
$form->add_lookup("item_packed_size", "Packed size", "items", 
                  "item_packed_size",HIDEEMPTY ,id());
$form->add_lookup("item_packed_weight", "Packed weight", "items", 
                  "item_packed_weight",HIDEEMPTY ,id());
$form->add_lookup("item_materials", "Materials", "items", "item_materials",HIDEEMPTY ,id());
$form->add_lookup("item_electrical_specifications", "Electrical specifications", "items", 
                  "item_electrical_specifications",HIDEEMPTY ,id());
$form->add_lookup("item_lights_used", "Lights usesd", "items", 
                  "item_lights_used",HIDEEMPTY ,id());
$form->add_lookup("item_regulatory_approvals", "Regulatory approvals", "items", 
                  "item_regulatory_approvals",HIDEEMPTY ,id());
$form->add_lookup("item_install_requirements", "Install requirements", "items",
                  "item_install_requirements",HIDEEMPTY ,id());


if (param("parts"))
{
    $form->add_button("get_back", "Back");
}





/********************************************************************
    Create File List
*********************************************************************/ 
if ($files_have_records == 1)
{
    $list2 = new ListView($sql_files);


    $list2->set_entity("item option_files");
    $list2->set_title("Attached Files");
    $list2->set_filter("item_option_file_option = " . $option);
    $list2->set_order("item_option_file_title");


    $link = "http://" . $_SERVER["HTTP_HOST"] . "/";
    $list2->add_column("item_option_file_title", "Title", $link . "{item_option_file_path}", "", "", COLUMN_NO_WRAP);
    $list2->add_column("file_type_name", "Type", "", "", "", COLUMN_NO_WRAP);
    $list2->add_column("item_option_file_description", "Description");
}



if ($files_have_records == 1)
{
    $list2->process();
}


/********************************************************************
    render page
*********************************************************************/




if ($form->button("get_back"))
{
    $link = "catalog_item_view.php?id=" . param("parent"); 
    redirect($link);
}




$page = new Page(PAGE_POPUP);
$page->header();
$page->title( $item_label);


// Get item image
$result = mysql_query("select item_option_file_path " .
                      "from item_option_files " .
                      "where item_option_file_option=" . $option . " and item_option_file_type=2");


if (mysql_num_rows($result) != 0)
{
	$row = mysql_fetch_assoc($result);
	$size = getimagesize(combine_paths($_SERVER["DOCUMENT_ROOT"], $row['item_option_file_path']));


	echo "<div style=\"text-align:center\"><img src=\"" . $row['item_option_file_path'] . "\" width=\"$size[0]\" height=\"$size[1]\" border=\"0\" alt=\"\" /></div><br>";

    echo "<a class=\"value\" href=\"javascript:print();\">print this info</a>";
} else {
	echo "<p>No picture available</p>";
}



if ($files_have_records == 1)
{
    $list2->render();
}


$page->footer();
?>