<?php
/********************************************************************

    snagging_template.php

    Creation and mutation of snagging_list templates.

    Created by:     Oliver Hofer (oliver.hofer@pageagent.com)
    Date created:   2003-05-22
    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date modified:  2003-05-29
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

//check_access("can_edit_catalog");
register_param("pid");

set_referer("project_edit_snagging_list.php");
set_referer("project_edit_snagging_list_elements.php");

$sql= "select snagging_list_id, snagging_list_name ".
      "from snagging_lists ";

$list = new ListView($sql);
$list->add_hidden("pid", param("pid"));
$list->set_filter("snagging_list_project =" . param("pid"));
$list->add_column("snagging_list_name", "Name", 
                  "project_edit_snagging_list.php?pid=" . param("pid"));

$link = "project_edit_snagging_list_elements.php"; // ?id={snagging_list_id}
$list->set_edit_key("snagging_list_id");
$list->add_button_column("snagging_list_id", "Edit", $link);
/*
$list->add_button_column("snagging_list_id", "Preview", "snagging_list_pdf.php?preview=1");
$list->add_button_column("snagging_list_id", "Download", "snagging_list_pdf.php");
*/
$list->add_button("add_list", "Add Snagging List");

$list->process();

if ($list->button("add_list"))
{
    redirect("project_edit_snagging_list.php?pid=" . param("pid"));
}

$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Snagging Lists");
$list->render();
$page->footer();

?>