<?php
/********************************************************************

    filter.php

    Allows filtering of a list view.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-04
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-10-18
    Version:        1.0.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access();

$filter = get_session_value("filter_object");

$form = new Form();

foreach ($filter->items as $item)
{
    if ($item["type"] == LIST_FILTER_FREE)
    {
        $form->add_edit($item["field"], $item["caption"]);
    }
    else
    {
        $form->add_list($item["field"], $item["caption"], $item["sql"]);
    }
}

$form->add_button("filter", "Filter");
$form->add_button("back", "Back");
$form->populate();

if ($form->button("back"))
{
    redirect($filter->view);
}
else if ($form->button("filter"))
{
    $items = array();

    foreach ($filter->items as $item)
    {
        $value = $form->value($item["field"]);

        if (trim($value) <> "")
        {
            if ($item["type"] == LIST_FILTER_FREE)
            {
                $items[] = $item["altfield"] . " like " . dbquote("%" . $value . "%");
            }
            else
            {
                $items[] = $item["altfield"] . "=" . dbquote($value);
            }
        }
    }

    if (count($items))
    {

		redirect(build_url($filter->view, array("filter" => join(" and ", $items))));
    }
    else
    {
        redirect(remove_param($filter->view, "filter"));
    }
}

$page = new Page();
$page->header();
$page->title("Filter " . strtolower($filter->entity));
$form->render();

?>

	<script type="text/javascript">
		
		document.onkeydown = process_key;
		
		function process_key(e)
		{
		  if( !e ) 
		  {
			if( window.event ) 
			{
			  e = window.event;
			} 
			else 
			{
			  return;
			}
		  }

		  if(e.keyCode==13)
		  {
			  button('filter');
		  }
		}
	</script>

	<?php
$page->footer();

?>