<?php

/********************************************************************

    order_shop_configurator.php

    Shop Configurator

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-08-04
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-08-04
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/shop_configurator_functions.php";
require_once "include/get_functions.php";

set_referer("order_shop_checkout.php");

check_access("can_view_orders");

register_param('cid');

$cid = param('cid');
if (!$cid)
{
    $cid = id();
}


/********************************************************************
    prepare all data needed
*********************************************************************/
// 
$hl_start = "<span class='highlite_value'>";
$hl_end = "</span>";


// get the region of users' address
$user_region = get_user_region(user_id());
$currency = get_user_currency(user_id());


//caculate total price
$shop_data = get_shop_basket();

if(isset($_REQUEST["form_items"]))
{
    $cost = get_cost_of_shop($cid, $user_region, $_REQUEST);
    $pdata = $_REQUEST;
}
else
{
    $cost = get_cost_of_shop($cid, $user_region, $shop_data);
    $pdata = $shop_data;
}



if($pdata)
{
    //picture configuration code
    $picture_code = array();
    foreach($pdata as $key=>$value)
    {
        
        if(substr($key, 0, 2) == "o_" and $value == 1)
        {
            $parts = explode ( "_", $key);

            $sql = "select item_option_configcode " .
                   "from item_options " .
                   "where item_option_id = " . $parts[2];

            $res = mysql_query($sql) or dberror($sql);
            if ($row = mysql_fetch_assoc($res))
            {
                if($row["item_option_configcode"])
                {
                    $picture_code[] = $row["item_option_configcode"];
                }
            }
        }
        elseif(substr($key, 0, 2) == "r_"  and $value == 1)
        {
            $parts = explode ( "_", $key);

            $sql = "select item_group_option_configcode1 " .
                   "from item_group_options " .
                   "where item_group_option_id = " . $parts[1];

            $res = mysql_query($sql) or dberror($sql);
            if ($row = mysql_fetch_assoc($res))
            {
                if($row["item_group_option_configcode1"])
                {
                    $picture_code[] = $row["item_group_option_configcode1"];
                }
            }
        }
        elseif(substr($key, 0, 3) == "rb_"  and $value == 1)
        {
            $parts = explode ( "_", $key);

            $sql = "select item_group_option_configcode1 " .
                   "from item_group_options " .
                   "where item_group_option_id = " . $parts[1];

            $res = mysql_query($sql) or dberror($sql);
            if ($row = mysql_fetch_assoc($res))
            {
                if($row["item_group_option_configcode1"])
                {
                    $picture_code[] = $row["item_group_option_configcode1"];
                }
            }
            
        }
        elseif(substr($key, 0, 3) == "rb_"  and $value == 2)
        {
            $parts = explode ( "_", $key);

            $sql = "select item_group_option_configcode2 " .
                   "from item_group_options " .
                   "where item_group_option_id = " . $parts[1];

            $res = mysql_query($sql) or dberror($sql);
            if ($row = mysql_fetch_assoc($res))
            {
                if($row["item_group_option_configcode2"])
                {
                    $picture_code[] = $row["item_group_option_configcode2"];
                }
            }
        }
    }


    $picture_config_code = "";
    sort($picture_code);

    foreach($picture_code as $key=>$value)
    {
        $picture_config_code = $picture_config_code . $value;

    }

}

/********************************************************************
    Create Form for Items of selected Category
*********************************************************************/
$sql = "select item_id, category_item_id, item_code, item_name, ".
       "    round((item_price/currency_exchange_rate * " .
       "    currency_factor),2) as price " .
       "from category_items " .
       "left join items on category_items.category_item_item=items.item_id " .
       "left join item_regions on item_region_item = item_id " .
       "left join users on users.user_id = " . user_id() . " " .
       "left join addresses on addresses.address_id = users.user_address " .
       "left join currencies on currencies.currency_id = addresses.address_currency "  .
       "where category_item_category = " . $cid . 
       "    and item_visible = 1 " .
       "    and item_active = 1 " .
       "    and item_region_region = " . $user_region;

$form_items = new Form("item_options", "item_option");

$form_items->add_hidden("form_items", 1);
$form_items->add_hidden("cid", $cid);
$form_items->add_hidden("category_id", $cid);

$form_items->add_section(" ");
$form_items->add_section(" ");



$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
    
    $item_id = $row["item_id"];
    
    $form_items->add_section($row["item_code"] . " " . $row["item_name"]);

    $form_items->add_checkbox("i_" . $row["item_id"], $row["item_name"], "1", RENDER_HTML  | DISABLED, $row["item_code"]);


    //composition
    $sql_c = "select count(item_composition_id) as num_recs " .
             "from item_compositions " .
             "left join item_groups on item_group_id = item_composition_group " .
             "left join item_group_items on item_group_item_group = item_composition_group " .
             "left join items on item_id = item_group_item_item " .
             "where item_composition_item = " . $row["item_id"];

    $res_c = mysql_query($sql_c) or dberror($sql_c);
    $row_c = mysql_fetch_assoc($res_c);

    $num_items = $row_c["num_recs"];
    
    if($num_items > 0)
    {
        $form_items->add_label("composition_" . $row["item_id"], "Composition", RENDER_HTML,  "<span class = \"highlite_value_bold\">The standard consists of the following parts:</span>");

        $sql_r = "select item_composition_id, item_composition_group, item_group_name, " .
                 "item_group_item_quantity, item_code, item_name, " .
                 "item_group_id, item_group_name, item_group_item_id, item_id " .
                 "from item_compositions " .
                 "left join item_groups on item_group_id = item_composition_group " .
                 "left join item_group_items on item_group_item_group = item_composition_group " .
                 "left join items on item_id = item_group_item_item " .
                 "where item_composition_item = " . $row["item_id"] .
                 " order by item_code";

        $res_r = mysql_query($sql_r) or dberror($sql_r);
        while ($row_r = mysql_fetch_assoc($res_r))
        {
            $link = "javascript:popup('order_shop_configurator_item_view.php?id=" . $row_r["item_id"] . "', 500,400)";

            $view = $row_r["item_group_item_quantity"]. " <a href =\"" .  $link . "\">" . $row_r["item_code"] . "</a> " . $row_r["item_name"];

            $form_items->add_label("citem_" . $row_r["item_group_item_id"], "", RENDER_HTML,  $view);
            
        }
    }

    // item price
    $price = $currency['currency_symbol'] . " " . number_format(get_shop_item_price($row["item_id"], $user_region), 2);
    $form_items->add_label("price_" . $row["item_id"], "Price", 0,  $price);


    $pix = "<img src=\"../pictures/view.gif\" align=\"absmiddle\" border=\"0\" alt=\"\" />";
    $link = "javascript:popup('order_shop_configurator_item_view.php?id=" . $row["item_id"] . "', 500,400)";

    $view = " <a href =\"" .  $link . "\">" . $pix . "&nbsp;show picture and info of the standard</a>";

    $form_items->add_label("view_info" . $row["item_id"], "", RENDER_HTML,  $view);

    
    // list of addons
    
    $sql_a =   "select count(addon_id) as num_recs " .
               "from addons " .
               "left join items on item_id = addon_child " .
               "left join item_regions on item_region_item = item_id " .
               "left join users on users.user_id = " . user_id() . " " .
               "left join addresses on addresses.address_id = users.user_address " .
               "left join currencies on currencies.currency_id = addresses.address_currency " .
               "where addon_parent = " . $row["item_id"];


    $res_a = mysql_query($sql_a) or dberror($sql_a);
    $row_a = mysql_fetch_assoc($res_a);

    $num_addons = $row_a["num_recs"];
    
    
    if($num_addons > 0)
    {
    
        $form_items->add_label("spacer01_" . $row["item_id"], "", 0,  "");
        $form_items->add_label("addons_" . $row["item_id"], "Addons", RENDER_HTML,  "<span class = \"highlite_value_bold\">The following addons are needed: </span>");

        $sql_a =   "select addon_id, item_id, item_code, item_name, " .
                   "    round(((item_price / currency_exchange_rate) * currency_factor),2) as price, " .
                   "    addon_package_quantity, item_name " .
                   "from addons " .
                   "left join items on item_id = addon_child " .
                   "left join item_regions on item_region_item = item_id " .
                   "left join users on users.user_id = " . user_id() . " " .
                   "left join addresses on addresses.address_id = users.user_address " .
                   "left join currencies on currencies.currency_id = addresses.address_currency " .
                   "where addon_parent = " . $row["item_id"];


        $res_a = mysql_query($sql_a) or dberror($sql_a);
        
        while ($row_a = mysql_fetch_assoc($res_a))
        {
            $p = $hl_start . "\nAdditional Price: " . $currency['currency_symbol'] . " " . number_format($row_a["price"], 2) . $hl_end;
            
           
            $addon_name = "addon" . $row_a["addon_id"];
            $view = $row_a["addon_package_quantity"]. "x " . $row_a["item_name"]  . $p;
            $form_items->add_label($addon_name, " ", RENDER_HTML,  $view);
        }
    }

$form_items->populate();
$form_items->process();




/********************************************************************
    Create Form for Options
*********************************************************************/
$form_options = new Form("item_options", "item_option");

$form_options->add_hidden("form_options", 1);
$form_options->add_hidden("cid", $cid);
$form_options->add_hidden("category_id", $cid);

    // options

    $form_options->add_section("Options");
    $form_options->add_label("options_" . $row["item_id"], " ", RENDER_HTML,  "<span class = \"highlite_value_bold\">The following options are available: </span>");

    $sql_o = "select count(item_option_id) as num_recs " . 
             "from item_options " .
             "left join items on item_id = item_option_child " .
             "left join users on users.user_id = " . user_id() . " " .
             "left join addresses on addresses.address_id = users.user_address " .
             "left join currencies on currencies.currency_id = addresses.address_currency " .
             "where item_option_parent = " . $row["item_id"] . " " .
             "order by item_code";

    $res_o = mysql_query($sql_o) or dberror($sql_o);
    $row_o = mysql_fetch_assoc($res_o);

    $num_options = $row_o["num_recs"];
    
    if($num_options > 0)
    {

        $sql_o = "select item_option_id, item_option_quantity, item_option_name, " .
                 "item_option_description, " .
                 "item_option_quantity, item_id, item_code, item_name, " .
                 "round(((item_option_quantity * item_price / currency_exchange_rate) * currency_factor),2) as price " .
                 "from item_options " .
                 "left join items on item_id = item_option_child " .
                 "left join users on users.user_id = " . user_id() . " " .
                 "left join addresses on addresses.address_id = users.user_address " .
                 "left join currencies on currencies.currency_id = addresses.address_currency " .
                 "where item_option_parent = " . $row["item_id"] . " " .
                 "order by item_code";

        $res_o = mysql_query($sql_o) or dberror($sql_o);
        
        while ($row_o = mysql_fetch_assoc($res_o))
        {
            $option_name = $row_o["item_option_name"];

            $p = $hl_start . "\nAdditional Price: " . $currency['currency_symbol'] ." " . number_format($row_o["price"], 2) . $hl_end;

            // info link
            $pix = "<img src=\"../pictures/view.gif\" align=\"absmiddle\" border=\"0\" alt=\"\" />";
            $link = "javascript:popup('item_option_view.php?id=" . $row_o["item_id"] . "&option=" . $row_o["item_option_id"] . "', 500,400)";

            $view = " <a href =\"" .  $link . "\">" . $pix . "&nbsp;show picture and info</a>";
            
            $view = $row_o["item_option_quantity"]. "x " . $row_o["item_option_description"]  . $p . "\n" . $view;
            
            $form_options->add_checkbox("o_" . $row["item_id"] . "_" . $row_o["item_option_id"], $view,
                        isset($shop_data["o_" . $row["item_id"] . "_" . $row_o["item_option_id"]]) ?
                        $shop_data["o_" . $row["item_id"] . "_" . $row_o["item_option_id"]] :  0, RENDER_HTML, $option_name);

            $form_options->add_label("spacer_" . $row["item_id"] . "_" . $row_o["item_option_id"], " ", RENDER_HTML,  " ");

        }
    }


    //replacement options
  
    $sql_c = "select count(item_group_option_id) as num_recs " .
             "from item_groups " .
             "left join item_group_options on item_group_option_group = item_group_id " .
             "where item_group_category = " . $cid;

    $res_c = mysql_query($sql_c) or dberror($sql_c);
    $row_c = mysql_fetch_assoc($res_c);
    
    if($row_c["num_recs"] > 0)
    {

        $item_groups = array();
        
        $sql_r = "select item_group_id, item_group_name " .
                 "from item_groups " .
                 "left join item_group_options on item_group_option_group = item_group_id " .
                 "where item_group_category = " . $cid;

        $res_r = mysql_query($sql_r) or dberror($sql_r);
        while ($row_r = mysql_fetch_assoc($res_r))
        {
            $item_groups[$row_r["item_group_id"]] = $row_r["item_group_name"];
            
        }

        foreach($item_groups as $key=>$value)
        {
            // count replacement groups of the group
            $sql_g = "select count(item_group_option_id) as num_recs " .
                     "from item_group_options " .
                     "where item_group_option_group = " . $key;

            $res_g = mysql_query($sql_g) or dberror($sql_g);
            $row_g = mysql_fetch_assoc($res_g);
             
            
            if($row_g["num_recs"] > 1)
            {

                $sql_g = "select * " .
                         "from item_group_options " .
                         "where item_group_option_group = " . $key;

                 $res_g = mysql_query($sql_g) or dberror($sql_g);
                 while($row_g = mysql_fetch_assoc($res_g))
                 {

                     if($row_g["item_group_option_logical"] == 0) // and
                     {
                         $option_name = $row_g["item_group_option_caption1"];
                         $rname = "r_" . $row_g["item_group_option_id"]; 

                         $rvalue = $row_g["item_group_option_description1"];

                         $replacement_cost = get_difference_in_cost_of_replacement($row_g["item_group_option_id"]);

                         if($replacement_cost < 0)
                         {
                            $dcost = $hl_start . "Difference in Price: " . $currency['currency_symbol'] ." " . number_format($replacement_cost, 2) .  $hl_end;
                         }
                         else
                         {
                            $dcost = $hl_start . "Additional Price: " . $currency['currency_symbol'] ." " . number_format($replacement_cost, 2) .  $hl_end;
                         }

                         $rvalue = $rvalue . "\n" . $dcost;


                         // info link
                         $pix = "<img src=\"../pictures/view.gif\" align=\"absmiddle\" border=\"0\" alt=\"\" />";
                         $link = "javascript:popup('item_group_view.php?oid=" . $row_g["item_group_option_id"] . "&value=0', 500,400)";

                         $view = " <a href =\"" .  $link . "\">" . $pix . "&nbsp;show picture and info</a>";
                         
                         $rvalue = $rvalue . "<br>" . $view . "<br>";


                         
                         $form_options->add_checkbox($rname, $rvalue, isset($shop_data[$rname]) ? $shop_data[$rname] :  0, RENDER_HTML, $option_name);
                         $form_options->add_label("spacer_" . $row_g["item_group_option_id"], "", 0,  "");
                     }
                     else // or
                     {
                         $option_group_name = $row_g["item_group_option_name"];
                         $option_name1 = $row_g["item_group_option_caption1"];
                         $option_name2 = $row_g["item_group_option_caption2"];

                         $rname = "rb_" . $row_g["item_group_option_id"];
                         
                         if(isset($shop_data[$rname]))
                         {
                             $selected_value = $shop_data[$rname];
                         }
                         else
                         {
                             $selected_value = 0;
                         }
                         
                         
                         $item_replacement_groups = array();
                         $item_replacement_group_captions = array();

                         
                         $item_replacement_groups[0] = "standard configuration";
                         $item_replacement_group_captions[0] = $option_group_name;

                         $item_replacement_group_captions[1] = $option_name1;
                         $item_replacement_group_captions[2] = $option_name2;

                         $replacement_cost = get_difference_in_cost_of_replacement($row_g["item_group_option_id"], 1);

                         if($replacement_cost < 0)
                         {
                            $dcost = $hl_start . "Difference in Price: " . $currency['currency_symbol'] ." " . number_format($replacement_cost, 2) .  $hl_end;
                         }
                         else
                         {
                            $dcost = $hl_start . "Additional Price: " . $currency['currency_symbol'] ." " . number_format($replacement_cost, 2) .  $hl_end;
                         }

                         $item_replacement_groups[1] = $row_g["item_group_option_description1"] . "\n" . $dcost;

                         // info link
                         $pix = "<img src=\"../pictures/view.gif\" align=\"absmiddle\" border=\"0\" alt=\"\" />";
                         $link = "javascript:popup('item_group_view.php?oid=" . $row_g["item_group_option_id"] . "&value=1', 500,400)";

                         $view = " <a href =\"" .  $link . "\">" . $pix . "&nbsp;show picture and info</a>";
                         
                         $item_replacement_groups[1] = $item_replacement_groups[1] . "<br>" . $view . "<br>";

                         
                         $replacement_cost = get_difference_in_cost_of_replacement($row_g["item_group_option_id"], 2);

                         if($replacement_cost < 0)
                         {
                            $dcost = $hl_start . "Difference in Price: " . $currency['currency_symbol'] ." " . number_format($replacement_cost, 2) .  $hl_end;
                         }
                         else
                         {
                            $dcost = $hl_start . "Additional Price: " . $currency['currency_symbol'] ." " . number_format($replacement_cost, 2) .  $hl_end;
                         }
                         
                         $item_replacement_groups[2] = $row_g["item_group_option_description2"] . "\n" . $dcost;

                         // info link
                         $pix = "<img src=\"../pictures/view.gif\" align=\"absmiddle\" border=\"0\" alt=\"\" />";
                         $link = "javascript:popup('item_group_view.php?oid=" . $row_g["item_group_option_id"] . "&value=2', 500,400)";

                         $view = " <a href =\"" .  $link . "\">" . $pix . "&nbsp;show picture and info</a>";
                         
                         $item_replacement_groups[2] = $item_replacement_groups[2] . "<br>" . $view . "<br>";

                         $form_options->add_radiolist($rname, $item_replacement_group_captions, $item_replacement_groups, RENDER_HTML | VERTICAL, $selected_value);
                         
                         $form_options->add_label("spacer_" . $row_g["item_group_option_id"], "", 0,  "");

                     }
                 }
            }
        }
    }

}

$form_options->add_button("recalc", "Recalculate Price", "", SUBMIT);
$form_options->add_button("showconfig", "Show Picture of the Selected Configuration", "", SUBMIT);

$form_options->populate();
$form_options->process();

/********************************************************************
    Create Form for Total Cost
*********************************************************************/
$form_cost = new Form("item_options", "item_option");

    
    $form_cost->add_section(" ");
    $form_cost->add_section(" ");


    $form_cost->add_hidden("cid", $cid);
    $form_cost->add_hidden("category_id", $cid);

    $form_cost->add_section("Total Cost");

    $form_cost->add_label("item_total", "Kiosk", 0, $currency['currency_symbol'] . " " .$cost["item_total"]);

    if($cost["addon_total"])
    {
        $form_cost->add_label("addon_total", "Add Ons", 0, $currency['currency_symbol'] . " " .$cost["addon_total"]);
    }

    $form_cost->add_label("option_total", "Options", 0, $currency['currency_symbol'] . " " .$cost["option_total"]);


    $form_cost->add_label("shop_total", "Order Total", RENDER_HTML, "<b>" . $currency['currency_symbol'] . " " .$cost["shop_total"] . "</br>");

    /*
    $pix = "<img src=\"../pictures/view.gif\" align=\"absmiddle\" border=\"0\" alt=\"\" />";
    $link = "javascript:popup('order_shop_posconfig_view.php?id=" . $item_id . "&pcc=" . $picture_config_code . "', 500,400)";

    $view = " <a href =\"" .  $link . "\">" . $pix . "&nbsp;show picture of the selected configuration</a>";

    $form_cost->add_label("view_pos_info" . $row["item_id"], "", RENDER_HTML,  $view);

    $form_cost->add_label("spacer", "", 0, "");

    */

    $form_cost->add_button("submit", "Proceed to Checkout", "", 0);
    $form_cost->add_button(FORM_BUTTON_BACK, "Back");


$form_cost->process();


// process button
if ($form_cost->button("submit"))
{
	$result = save_shop_basket($_REQUEST);
    redirect("order_shop_checkout.php");
}

// process button
if ($form_options->button("showconfig"))
{
    
    echo "<script language=\"javascript\" src=\"../include/main.js\"></script>";
    echo '<script language="javascript">';
    
    echo 'url ="order_shop_posconfig_view.php?id=' . $item_id . '&pcc=' . $picture_config_code . '";';

    echo "win = popup(url, 500, 400);";
    
    echo '</script>';
}



/********************************************************************
    Render Page
*********************************************************************/ 
$page = new Page("orders");

$page->register_action('home', 'Home', "welcome.php");

$page->header();
$page->title("POS Configuration");


$form_items->render();

$form_options->render();

$form_cost->render();

$page->footer();

?>