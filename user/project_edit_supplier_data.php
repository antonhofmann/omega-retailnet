<?php
/********************************************************************

    project_edit_supplier_data.php

    Edit Data concerning delivery and traffic

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-24
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-06-27
    Version:        1.2.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";


check_access("can_edit_supplier_data_in_projects");

register_param("pid");
set_referer("project_edit_supplier_data_item.php");


/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());
$user_roles = get_user_roles(user_id());

// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

// create sql for oder items
$sql_order_items = "select order_item_id, order_item_text, order_item_quantity, ".
                   "    order_item_supplier_price, order_item_po_number, ".
                   "    if(order_item_item <>'', item_code, item_type_name) as item_code, " .
                   "    concat('[', order_item_ordered_changes, ']', RIGHT(order_item_ordered, 2), '.', MONTH(order_item_ordered), '.',  YEAR(order_item_ordered)) as order_date, ".
                   "    concat('[', order_item_ready_for_pickup_changes, ']', RIGHT(order_item_ready_for_pickup, 2), '.', MONTH(order_item_ready_for_pickup), '.',  YEAR(order_item_ready_for_pickup)) as ready_4_pickup, ".
                   "    addresses.address_company as supplier_company, ".
                   "    addresses_1.address_company as forwarder_company, ".
                   "    currency_symbol, ".
                   "    concat(addresses.address_company, ', ', currency_symbol) as group_head " .
                   "from order_items ".
                   "left join items on order_item_item = item_id ".
                   "left join item_types on order_item_type = item_type_id ".
                   "left join addresses on order_item_supplier_address = addresses.address_id ".
                   "left join addresses AS addresses_1 on order_item_forwarder_address = addresses_1.address_id ".
                   "left join currencies on order_item_supplier_currency = currency_id ";


/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

$form->add_hidden("pid", param("pid"));

require_once "include/project_head_small.php";


$form->add_section("Preferences and Traffic Checklist");
$form->add_label("preferred_delivery_date", "Preferred Arrival Date", 0, to_system_date($project["order_preferred_delivery_date"]));

if((in_array(5, $user_roles) or in_array(29, $user_roles)) and $project["order_actual_order_state_code"] > 699)
{
    $form->add_label("preferred_transportation_text", "Preferred Transportation", 0, "shipping defined by operator");
}
else
{
	$form->add_lookup("preferred_transportation_arranged", "Transportation arranged by", "transportation_types", "transportation_type_name", 0, $project["order_preferred_transportation_arranged"]);

	$form->add_lookup("preferred_transportation_mode", "Transportation mode", "transportation_types", "transportation_type_name", 0, $project["order_preferred_transportation_mode"]);
}

/*
$value="no";
if ($project["order_packaging_retraction"])
{
    $value="yes";
}
$form->add_label("order_packaging_retraction", "Packaging Retraction Desired", 0, $value);
*/

$value="no";
if ($project["order_pedestrian_mall_approval"])
{
    $value="yes";
}
$form->add_label("pedestrian_mall_approval", "Pedestrian Area Approval Needed", 0, $value);

$value="no";
if ($project["order_full_delivery"])
{
    $value="yes";
}
$form->add_label("full_delivery", "Full Delivery Desired", 0, $value);

$form->add_label("delivery_comments", "Delivery Comments", 0, $project["order_delivery_comments"]);




/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Create Item List
*********************************************************************/ 
$list = new ListView($sql_order_items, LIST_HAS_SEPARATOR | LIST_HAS_HEADER);

$list->set_title("Items up to Delivery");
$list->set_entity("order_item");
$list->set_group("group_head");

if (has_access("has_access_to_all_supplier_data_in_projects"))
{
    $list->set_filter("(order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
                      "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
                      "   and order_item_order = " . $project["project_order"]);
}
else
{
    $list->set_filter("(order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
                      "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
                      "   and order_item_order = " . $project["project_order"] .
                      "   and order_item_supplier_address = " . $user_data["address"]);
}

$list->set_order("order_item_type, item_code");


$link="project_edit_supplier_data_item.php?pid=" . param("pid");

$list->add_column("item_code", "Item Code", $link, "", "", COLUMN_BREAK);
$list->add_column("order_item_text", "Name", "", "", "", COLUMN_BREAK);
$list->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_BREAK);
$list->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_BREAK);
$list->add_column("forwarder_company", "Forwarder", "", "", "", COLUMN_NO_WRAP );
$list->add_column("order_date", "Ordered", "", "", "", COLUMN_NO_WRAP );
$list->add_column("ready_4_pickup", "Ready for Pickup", "", "", "", COLUMN_NO_WRAP );


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Edit Pickup Data");
$form->render();
$list->render();

$page->footer();

?>