<?php
/********************************************************************


    catalog_item_view.php

    Entry page for the catalog section group.

    Created by:     Oliver Hofer (oliver.hofer@pageagent.com)
    Date created:   2002-08-20
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-08-19
    Version:        1.1.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.
*********************************************************************/


require_once "../include/frame.php";
require_once "include/save_functions.php";
require_once "include/order_functions.php";
check_access("can_view_catalog_detail");

/********************************************************************
    prepare all data needed
*********************************************************************/

// calculate price in addres-currency
$sql = "select item_id, concat_ws(' ', currency_symbol, " .
       "    round(((item_price / currency_exchange_rate) * currency_factor),2)) as prize " .
       "from items " .
       "left join users on users.user_id = " . user_id() . " " .
       "left join addresses on addresses.address_id = users.user_address " .
       "left join currencies on currencies.currency_id = addresses.address_currency " .
       "where item_id = " . id();

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
    $item_prize = $row['prize'];
}

// Read name & code of the sown item
$sql_name = "select concat_ws(' - ', item_code, item_name) as item_label " .
            "from items where item_id = " . id();

$res = mysql_query($sql_name) or dberror();
if ($row = mysql_fetch_assoc($res))
{
    $item_label = $row['item_label'];
}


// spare parts
$parts_have_records = 0;

if (!param("parts"))
{
    $sql_parts = "select item_id, item_code, item_name ".
                 "from parts ".
                 "left join items on part_child = item_id ".
                 "where part_parent = " . id();


    $res = mysql_query($sql_parts) or dberror();
    if ($row = mysql_fetch_assoc($res))
    {
        $parts_have_records = 1;
    }
}

// attached files
$files_have_records = 0;
$sql_files = "select item_file_id, item_file_title, file_type_name, " .
             "    item_file_description, item_file_path " . 
             "from item_files " .
             "left join file_types on file_types.file_type_id = item_files.item_file_type ";


$sql = $sql_files . " where item_file_purpose <> 6 and item_file_purpose <> 7 and item_file_item = " . id();
$res = mysql_query($sql) or dberror();
if ($row = mysql_fetch_assoc($res))
{
    $files_have_records = 1;
}

/********************************************************************
    Create Form
*********************************************************************/ 
$form = new Form("items", "item");

$form->add_section("Product Properties");
$form->add_hidden("parent", param("parent"));
$form->add_lookup("item_code", "Product Code", "items", "item_code",HIDEEMPTY ,id());
$form->add_lookup("item_name", "Product Name", "items", "item_name",HIDEEMPTY ,id());
$form->add_label("item_prize", "Price", HIDEEMPTY , $item_prize);
$form->add_lookup("item_description", "Description", "items", "item_description",HIDEEMPTY ,id());
$form->add_lookup("item_watches_displayed", "Watches Displayed", "items",
                  "item_watches_displayed",HIDEEMPTY ,id());
$form->add_lookup("item_watches_stored", "Watches Stored", "items", 
                  "item_watches_stored",HIDEEMPTY ,id());
$form->add_lookup("item_packed_size", "Packed size", "items", 
                  "item_packed_size",HIDEEMPTY ,id());
$form->add_lookup("item_packed_weight", "Packed weight", "items", 
                  "item_packed_weight",HIDEEMPTY ,id());
$form->add_lookup("item_materials", "Materials", "items", "item_materials",HIDEEMPTY ,id());
$form->add_lookup("item_electrical_specifications", "Electrical specifications", "items", 
                  "item_electrical_specifications",HIDEEMPTY ,id());
$form->add_lookup("item_lights_used", "Lights usesd", "items", 
                  "item_lights_used",HIDEEMPTY ,id());
$form->add_lookup("item_regulatory_approvals", "Regulatory approvals", "items", 
                  "item_regulatory_approvals",HIDEEMPTY ,id());
$form->add_lookup("item_install_requirements", "Install requirements", "items",
                  "item_install_requirements",HIDEEMPTY ,id());



if (has_access("can_create_new_orders"))
{
    $shopping_list_link = "order_category.php?action=basket_add&cid=" .id();
    $form->add_button("add_to_shopping_list", "Add to Shopping List", $shopping_list_link);
}

if (param("parts"))
{
    $form->add_button("get_back", "Back");
}


/********************************************************************
    Create List of spare parts
*********************************************************************/ 
if ($parts_have_records == 1)
{
    $list1 = new ListView($sql_parts);


    $list1->set_entity("parts");
    $list1->set_title("Spare Parts");
    $list1->set_order("item_code");


    $popup_link = "";
    if (has_access("can_view_catalog_detail"))
    {
        $popup_link = "popup:catalog_item_view.php?id={item_id}&parts=1&parent=" .id();
    }


    $list1->add_column("item_code", "Item Code", $popup_link, "", "", COLUMN_NO_WRAP);
    $list1->add_column("item_name", "Item Name");
}

/********************************************************************
    Create File List
*********************************************************************/ 
if ($files_have_records == 1)
{
    $list2 = new ListView($sql_files);


    $list2->set_entity("item files");
    $list2->set_title("Attached Files");
    $list2->set_filter("item_file_purpose <> 6 and item_file_purpose <> 7 and item_file_item = " . id());
    $list2->set_order("item_file_title");


    $link = "http://" . $_SERVER["HTTP_HOST"] . "/";
    $list2->add_column("item_file_title", "Title", $link . "{item_file_path}", "", "", COLUMN_NO_WRAP);
    $list2->add_column("file_type_name", "Type", "", "", "", COLUMN_NO_WRAP);
    $list2->add_column("item_file_description", "Description");
}


if ($parts_have_records == 1)
{
    $list1->process();
}


if ($files_have_records == 1)
{
    $list2->process();
}


/********************************************************************
    render page
*********************************************************************/
if ($form->button("get_back"))
{
    $link = "catalog_item_view.php?id=" . param("parent"); 
    redirect($link);
}

$page = new Page(PAGE_POPUP);
$page->header();
$page->title( $item_label);


// Get item image
$result = mysql_query("select item_file_path " .
                      "from item_files " .
                      "where item_file_item=" . id() . 
                      " and item_file_type=2 and item_file_purpose = 2");


if (mysql_num_rows($result) != 0)
{
	$row = mysql_fetch_assoc($result);
	$size = getimagesize(combine_paths($_SERVER["DOCUMENT_ROOT"], $row['item_file_path']));


	echo "<div style=\"text-align:center\"><img src=\"" . $row['item_file_path'] . "\" width=\"$size[0]\" height=\"$size[1]\" border=\"0\" alt=\"\" /></div><br>";
} else {
	echo "<p>No picture available</p>";
}


if ($form->button("add_to_shopping_list"))
{
	//add item to shopping-list
	$sql_i = "select item_category from items where item_id = " . id();
	$result_i = mysql_query($sql_i);
	$row_i = mysql_fetch_assoc($result_i);
	$categroy = $row_i["item_category"];
	
	update_shopping_list(array(id()=>1), get_users_basket(), $categroy);
	echo "<script language=\"Javascript\">";
	echo "window.opener.document.location.href=\"basket.php\";";
	echo "window.close();";
	echo "</script>";
}


$form->render();
if ($parts_have_records == 1)
{
    $list1->render();
}
if ($files_have_records == 1)
{
    $list2->render();
}


$page->footer();


?>