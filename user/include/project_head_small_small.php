<?php
/********************************************************************


    project_head_small_small.php

    Show order information (Page heading only)

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-10-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-17
    Version:        1.0.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.


*********************************************************************/

$order_state_name = get_actual_order_state_name($project["order_actual_order_state_code"], 1);


$form->add_label("order_date", "Project Starting Date", 0, to_system_date($project["order_date"]));


if($project["project_projectkind"] != 4 and $project["project_projectkind"] != 5) //Take Over and lease renewal
{
	$form->add_label("planned_opening_date", "Client's preferred Shop Opening Date", 0, to_system_date($project["project_planned_opening_date"]));
}
$form->add_label("real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]));
$form->add_label("actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]));


$form->add_label("status", "Status", 0, $project["order_actual_order_state_code"]  . " " . $order_state_name);


$line = "concat(user_name, ' ', user_firstname)";
if ($project["project_retail_coordinator"])
{
    $form->add_lookup("retail_coordinator", "Project Manager", "users", $line, 0, $project["project_retail_coordinator"]);
}
else
{
    $form->add_label("project_retail_coordinator", "Project Manager");
}


if ($project["order_retail_operator"])
{
    $form->add_lookup("retail_operator", "Retail Operator", "users", $line, 0, $project["order_retail_operator"]);
}
else
{
    $form->add_label("order_retail_operator", "Retail Operator");
}


$client = $client_address["company"] . ", " .
          $client_address["zip"] . " " . $client_address["place"] . ", " .
          $client_address["country_name"];




$form->add_label("client_address", "Client", 0, $client);




$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];
        
$form->add_label("shop_address", "POS Location Address", 0, $shop);


$form->add_label("shop_address", "POS Location Address", 0, $shop);




?>