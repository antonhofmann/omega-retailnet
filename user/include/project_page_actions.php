<?php
/********************************************************************

    project_page_actions.php

    Defines the possible page actions depending on access rights

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-25
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-09-27
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "order_state_constants.php";

if (has_access("can_edit_retail_data"))
{   
    $page->register_action('edit_retail_data', 'Edit Retail Data', "project_edit_retail_data.php?pid=" . param("pid"));
}

if (has_access("has_access_to_construction_data") and $project["project_state"] != 2)
{   
    $page->register_action('construction_data', 'Construction Data', "project_edit_construction_data.php?pid=" . param("pid"));
}

if (has_access("can_edit_pos_data") and $project["project_state"] != 2)
{   

	$page->register_action('edit_pos_data', 'Edit POS Data', 
		                       "project_edit_pos_data.php?pid=" . param('pid'));

}
elseif (has_access("can_view_pos_data") and $project["project_state"] != 2)
{   

	$page->register_action('view_pos_data', 'View POS Data', 
		                       "project_view_pos_data.php?pid=" . param('pid'));

}


if (has_access("can_edit_client_data_in_projects") and $project["project_state"] != 2)
{   
    $page->register_action('view_client_data', 'View Client Data', 
                           "project_view_client_data.php?pid=" . param('pid'));
	
	$page->register_action('edit_client_data', 'Edit Request', "project_edit_client_data.php?pid=" . param("pid"));
}
elseif (has_access("can_view_client_data_in_projects"))
{   

	$page->register_action('view_client_data', 'View Client Data', 
                           "project_view_client_data.php?pid=" . param('pid'));
}

$page->register_action('dummy1', '');


if (has_access("can_use_taskcentre_in_projects"))
{   
    
    $page->register_action('flow_control', 'Task Centre', "project_task_center.php?pid=" . param("pid"));
}

//if(user_login() == 'anton.hofmann' or user_login() == 'afr')
//{
	if (has_access("can_edit_list_of_materials_in_projects") or has_access("can_view_budget_in_projects"))
	{   
		$page->register_action('project_costs', 'Project Costs', "project_costs_overview.php?pid=" . param("pid"));
	}
//}


/*
if($project["project_projectkind"] != 4 and $project["project_projectkind"] != 5) // take over, lease renewal
{
	if (has_access("can_edit_list_of_materials_in_projects") and $project["project_state"] != 2)
	{   
		$page->register_action('edit_material_list', 'Edit List of Materials', "project_edit_material_list.php?pid=" . param("pid"));
	}


	if (has_access("can_edit_his_list_of_materials_in_projects") and $project["project_state"] != 2)
	{   
		$page->register_action('edit_material_list_supplier', 'Edit Offer Data', "project_edit_material_list_supplier.php?pid=" . param("pid"));
	}

	if (has_access("can_edit_local_constrction_work") and $project["project_state"] != 2)
	{   
		if($project["project_cost_type"] == 1) // Corporate
		{
			$page->register_action('edit_local_construction_work', 'Local Construction Work', "project_offers.php?pid=" . param("pid"));
		}
	}

	
	
	if (has_access("has_access_to_all_projects"))
	{   
		$page->register_action('view_project_budget', 'View Budget', "project_view_project_budget.php?pid=" . param("pid"));

	}
	elseif (has_access("can_view_budget_in_projects"))
	{
			$page->register_action('view_meterial_list', 'View List of Materials', "project_view_material_list_client_view.php?pid=" . param("pid"));

			$page->register_action('view_project_budget', 'View Budget', "project_view_project_budget.php?pid=" . param("pid"));
	}
}

$page->register_action('dummy2', '');
*/


if (has_access("has_access_to_cer"))
{
	//check if project has a cer or af
	$sql = "select order_number, order_shop_address_place, country_name, order_shop_address_company, " .
		   "project_postype, project_projectkind, project_cost_type " . 
		   "from projects " .
		   "left join orders on order_id = project_order " .
		   "left join countries on country_id = order_shop_address_country " .
		   "left join project_costs on project_cost_order = order_id " .
		   "where project_id = " .  dbquote(param('pid'));

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$sql = "select posproject_type_needs_cer, posproject_type_needs_af, posproject_type_needs_inr03 " . 
			   "from posproject_types " . 
			   "where posproject_type_postype =  " . dbquote($row["project_postype"]) .
			   " and posproject_type_projectcosttype =  " . dbquote($row["project_cost_type"]) .
			   " and posproject_type_projectkind =  " . dbquote($row["project_projectkind"]);

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			if($row["posproject_type_needs_cer"] == 1 
				or $row["posproject_type_needs_af"] == 1
				or $row["posproject_type_needs_inr03"] == 1)
			{
				$url = "/cer/cer_project.php?pid=". param('pid') . "&amp;id=" .  param('pid');
				$page->register_action('cer', 'Access CER/AF', $url, "_blank");
			}
		}
	}
}


/*
if(($project["project_cost_type"] == 1 or $project["project_cost_type"] == 6 )
	and has_access("has_full_access_to_cer") 
	and $project["project_state"] != 2) // corporate only
{
	$page->register_action('edit_cer', 'Edit CER Data', "project_edit_cer_data.php?pid=" . param("pid"));    
}
*/

if(has_access("has_full_access_to_cer") 
	and $project["project_state"] != 2) 
{
	$page->register_action('edit_cer', 'Edit CER Data', "project_edit_cer_data.php?pid=" . param("pid"));    
}

if($project["project_projectkind"] != 4 and $project["project_projectkind"] != 5) // take over, lease renewal
{
	
}

$page->register_action('dummy3', '');


if($project["project_projectkind"] != 4 and $project["project_projectkind"] != 5) // take over, lease renewal
{
	if (has_access("can_edit_project_sheet") and $project["project_state"] != 2)
	{   
		//comment: the project sheet has been changed from this date on
		$date_created = $project["date_created"];
		if($date_created < '2004-09-27' and $project["project_use_ps2004"] != 1)
		{
			$page->register_action('edit_project_sheet', 'Project Sheet', "project_edit_project_sheet.php?pid=" . param("pid"));  
		}
		else
		{
			$page->register_action('edit_project_sheet', 'Project Sheet', "project_edit_project_sheet_2004.php?pid=" . param("pid"));  
		}
	}

	/*
	if (has_access("can_view_ordered_values_in_projects"))
	{   
		$page->register_action('edit_view_ordered_values', 'View Ordered Values', 
							   "project_view_ordered_values.php?pid=" . param("pid"));
	}

	
	if (has_access("can_view_project_invoice_information"))
	{   
		$page->register_action('view_cost_information', 'View Cost Information', 
							   "project_view_cost_information.php?pid=" . param("pid"));
	}
	*

	

	if (has_access("can_edit_supplier_data_in_projects") and $project["project_state"] != 2)
	{   
		$page->register_action('edit_supplier_data', 'Edit Pickup Data', "project_edit_supplier_data.php?pid=" . param("pid"));    
	}

	if (has_access("can_edit_traffic_data_in_projects") and $project["project_state"] != 2)
	{   
		$page->register_action('edit_traffic_data', 'Edit Traffic Data', "project_edit_traffic_data.php?pid=" . param("pid"));    
	}

	if (has_access("can_edit_delivery_addresses_in_projects") and $project["project_state"] != 2)
	{   
		$page->register_action('edit_delivery_addresses', 'Edit Delivery Addresses', "project_edit_delivery_addresses.php?pid=" . param("pid"));    
	}


	if (has_access("can_view_delivery_schedule_in_projects"))
	{
		$page->register_action('view_traffic_data', 'View Delivery Schedule', 
							   "project_view_traffic_data.php?pid=" .param('pid'));

	}

	
	*/
}

$page->register_action('dummy4', '');

if (has_access("can_view_client_data_in_projects")
   or has_access("has_access_to_all_projects")
   or has_access("can_view_budget_in_projects")
   or has_access("can_view_delivery_schedule_in_projects")
   or has_access("can_edit_project_sheet")
   or (has_access("can_edit_cost_monitoring") and $project["project_state"] != 2))
{
	$page->register_action('print', 'Print', "projects_print.php?pid=" . param("pid"));
	$page->register_action('dummy5', '');
}





if (has_access("can_edit_milestones") and ($project["needs_cer"] == 1 
	or $project["needs_af"] == 1 or $project["needs_inr03"] == 1) and $project["project_state"] != 2)
{   
    $page->register_action('edit_milestones', 'Project Milestones', "project_edit_milestones.php?pid=" . param("pid"));   
}
elseif (has_access("can_view_milestones") and ($project["needs_cer"] == 1 
	or $project["needs_af"] == 1 or $project["needs_inr03"] == 1))
{   
    $page->register_action('view_milestones', 'Project Milestones', "project_view_milestones.php?pid=" . param("pid"));    
}

if (has_access("can_view_comments_in_projects"))
{   
    $page->register_action('view_comments', 'Comments', "project_view_comments.php?pid=" . param("pid"));
}

if (has_access("can_view_attachments_in_projects"))
{   
    $page->register_action('view_attachments', 'Attachments', "project_view_attachments.php?pid=" . param("pid"));
}



if (has_access("has_accessto_taskpool_in_projects"))
{   
    $page->register_action('task_pool', 'Task Pool', "project_task_pool.php?pid=" . param("pid"));
}

if (has_access("can_view_history_in_projects"))
{   
    $page->register_action('history', 'View History', "project_history.php?pid=" . param("pid"));
    $page->register_action('mail_history', 'View Mails', "project_mail_history.php?pid=" . param("pid"));

}

$page->register_action('dummy6', '');


$page->register_action('home', 'Home', "welcome.php");



?>