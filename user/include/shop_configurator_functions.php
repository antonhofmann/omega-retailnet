<?php
/********************************************************************

    shop_configurator_functions.php

    Various utility functions to get information from tables.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-07
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-02-10

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/


/********************************************************************
    get item price
*********************************************************************/
function get_shop_item_price($item_id, $user_region)
{

    $item_price = 0;


    $sql = "select  item_id, item_price, currency_exchange_rate, currency_factor, " .
           "round((item_price/currency_exchange_rate * currency_factor),2) as price " .
           "from items " .
           "left join item_regions on item_region_item = item_id " .
           "left join users on users.user_id = " . user_id() . " " .
           "left join addresses on addresses.address_id = users.user_address " .
           "left join currencies on currencies.currency_id = addresses.address_currency " . 
           "where item_id = " . $item_id . 
           "    and item_visible = 1 " .
           "    and item_active = 1 " .
           "    and item_region_region = " . $user_region;

    
	$res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $item_id = $row["item_id"];
        
        //check if item has composition groups
        $sql_c = "select count(item_composition_id) as num_recs " .
                 "from item_compositions " .
                 "left join item_groups on item_group_id = item_composition_group " .
                 "left join item_group_items on item_group_item_group = item_composition_group " .
                 "left join items on item_id = item_group_item_item " .
                 "where item_composition_item = " .  $item_id;

        $res_c = mysql_query($sql_c) or dberror($sql_c);
        $row_c = mysql_fetch_assoc($res_c);

        $num_items = $row_c["num_recs"];
        
        if($num_items > 0)
        {

            $sql_r = "select item_composition_id, item_id, item_group_item_quantity, item_price " .
                     "from item_compositions " .
                     "left join item_groups on item_group_id = item_composition_group " .
                     "left join item_group_items on item_group_item_group = item_composition_group " .
                     "left join items on item_id = item_group_item_item " .
                     "where item_composition_item = " . $item_id;

            $res_r = mysql_query($sql_r) or dberror($sql_r);
            while ($row_r = mysql_fetch_assoc($res_r))
            {
                $res = mysql_query($sql) or dberror($sql);
                if($row = mysql_fetch_assoc($res))
                {
                    $item_price = $item_price + $row_r["item_group_item_quantity"] * $row_r["item_price"]/$row["currency_exchange_rate"] * $row["currency_factor"];
                }
            }
        }
        else // item has no composition groups
        {
            $item_price = $item_price + $row["price"];
        }
    }

    
    return $item_price;

}

/********************************************************************
    get total cost of the shop
*********************************************************************/
function get_cost_of_shop($cid, $user_region, $config)
{

    $cost = array();


    $cost["item_total"] = 0;
    $cost["addon_total"] = 0;
    $cost["option_total"] = 0;
    $cost["shop_total"] = 0;

    // get cost of items

    $sql = "select  item_id, item_name, item_price, currency_exchange_rate, currency_factor, " .
           "round((item_price/currency_exchange_rate * currency_factor),2) as price " .
           "from category_items " .
           "left join items on category_items.category_item_item=items.item_id " .
           "left join item_regions on item_region_item = item_id " .
           "left join users on users.user_id = " . user_id() . " " .
           "left join addresses on addresses.address_id = users.user_address " .
           "left join currencies on currencies.currency_id = addresses.address_currency " .
		   "left join item_compositions on item_composition_item = item_id " . 
           "where category_item_category = " . $cid . 
           "    and item_visible = 1 " .
           "    and item_active = 1 " .
           "    and item_region_region = " . $user_region . 
		   "    and item_composition_group > 1 ";
	
    

	$res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        $item_id = $row["item_id"];
        
        //check if item has composition groups
        $sql_c = "select count(item_composition_id) as num_recs " .
                 "from item_compositions " .
                 "left join item_groups on item_group_id = item_composition_group " .
                 "left join item_group_items on item_group_item_group = item_composition_group " .
                 "left join items on item_id = item_group_item_item " .
                 "where item_composition_item = " .  $item_id;

		
		$res_c = mysql_query($sql_c) or dberror($sql_c);
        $row_c = mysql_fetch_assoc($res_c);

        
		$num_items = $row_c["num_recs"];
        
        

		if($num_items > 0)
        {

			$sql_r = "select item_composition_id, item_id, item_group_item_quantity, item_price " .
                     "from item_compositions " .
                     "left join item_groups on item_group_id = item_composition_group " .
                     "left join item_group_items on item_group_item_group = item_composition_group " .
                     "left join items on item_id = item_group_item_item " .
                     "where item_composition_item = " . $item_id;

            
			$res_r = mysql_query($sql_r) or dberror($sql_r);
            while ($row_r = mysql_fetch_assoc($res_r))
            {
                $res = mysql_query($sql) or dberror($sql);
                if($row = mysql_fetch_assoc($res))
                {
                    $cost["shop_total"] = $cost["shop_total"] + $row_r["item_group_item_quantity"] * $row_r["item_price"]/$row["currency_exchange_rate"] * $row["currency_factor"];
                    $cost["item_total"] = $cost["item_total"] + $row_r["item_group_item_quantity"] * $row_r["item_price"]/$row["currency_exchange_rate"] * $row["currency_factor"];
                }
            }
        }
        else // item has no composition groups
        {
            $cost["shop_total"] = $cost["shop_total"] + $row["price"];
            $cost["item_total"] = $cost["shop_total"] + $row["price"];
        }
    }


    //get cost of addons

    $sql = "select sum(round(((citems.item_price / currency_exchange_rate) * currency_factor),2)) as total " .
       "from addons " .
       "left join items as citems on citems.item_id = addon_child " .
       "left join items as aitems on aitems.item_id = addon_parent " .
       "left join category_items on category_item_item=aitems.item_id " .
       "left join item_regions on item_region_item = aitems.item_id " .
       "left join users on users.user_id = " . user_id() . " " .
       "left join addresses on addresses.address_id = users.user_address " .
       "left join currencies on currencies.currency_id = addresses.address_currency " .
       "where category_item_category = " . $cid . 
       "    and aitems.item_visible = 1 " .
       "    and aitems.item_active = 1 " .
       "    and item_region_region = " . $user_region;

    $res = mysql_query($sql) or dberror($sql);
    if($row = mysql_fetch_assoc($res))
    {
        $cost["addon_total"] = $row["total"];
    }


    //get cost of options
    if(isset($config["form_items"]))
    {
        if($config["form_items"] == 1) //form was submitted
        {
            foreach($config as $key=>$value)
            {
                if(substr($key, 0, 2) == "o_" and $value == 1)
                {
                    $parts = explode ( "_", $key);
                    
                    $sql = "select item_name, round((item_option_quantity*item_price/currency_exchange_rate * currency_factor),2) as price " .
                   "from item_options " .
                   "left join items on item_id = item_option_child " .
                   "left join item_regions on item_region_item = item_id " .
                   "left join users on users.user_id = " . user_id() . " " .
                   "left join addresses on addresses.address_id = users.user_address " .
                   "left join currencies on currencies.currency_id = addresses.address_currency " . 
                   "where item_option_id  = " . $parts[2];

                    $res = mysql_query($sql) or dberror($sql);
                    if($row = mysql_fetch_assoc($res))
                    {
                         $cost["option_total"] = $cost["option_total"] + $row["price"];
                    }
                }
            }
        }

    }


    //get cost of replacement options
    if(isset($config["form_items"]))
    {
        if($config["form_items"] == 1) //form was submitted
        {
            foreach($config as $key=>$value)
            {
                //echo $key . " " . $value . "<br>";
                if(substr($key, 0, 1) == "r" and $value == 1)
                {
                    $parts = explode ( "_", $key);

                    $cost["option_total"] = $cost["option_total"] + get_difference_in_cost_of_replacement($parts[1]);
                    
                }
                elseif(substr($key, 0, 2) == "rb" and $value > 0)
                {
                    $parts = explode ( "_", $key);

                    $cost["option_total"] = $cost["option_total"] + get_difference_in_cost_of_replacement($parts[1], $value);
                }
            }
        }
    }


    // build shop total

    if($cost["addon_total"] > 0)
    {
        $cost["shop_total"] = $cost["shop_total"] + $cost["addon_total"];
        $cost["addon_total"] = number_format($cost["addon_total"], 2);
    }

    if($cost["option_total"] <> 0)
    {
        $cost["shop_total"] = $cost["shop_total"] + $cost["option_total"];
        $cost["option_total"] = number_format($cost["option_total"], 2);
    }

    
    $cost["item_total"] = number_format($cost["item_total"], 2);
    $cost["shop_total"] = number_format($cost["shop_total"], 2);
    
    return $cost;

}



/********************************************************************
    get the difference in cost of a replacement option
*********************************************************************/
function get_difference_in_cost_of_replacement($option_id, $group = 0)
{

    $diffenence_in_cost = 0;

    $sql = "select item_group_option_item1, item_group_option_quantity1, " .
           "item_group_option_item2, item_group_option_quantity2, " .
           "item_group_option_logical, " .
           "item_group_option_item3, item_group_option_quantity3, " .
           "item_group_option_item4, item_group_option_quantity4 " .
           "from item_group_options " .
           "where item_group_option_id = " . $option_id;

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        
        if($group == 0)
        {
            $sql_i = "select item_id, " .
                     "   round(((item_price / currency_exchange_rate) * currency_factor),2) as price " .
                     "from items " .
                     "left join users on users.user_id = " . user_id() . " " .
                     "left join addresses on addresses.address_id = users.user_address " .
                     "left join currencies on currencies.currency_id = addresses.address_currency " .
                     "where item_id = " . $row["item_group_option_item1"];


            $res_i = mysql_query($sql_i) or dberror($sql_i);
            if ($row_i = mysql_fetch_assoc($res_i))
            {
                $diffenence_in_cost = $diffenence_in_cost + $row["item_group_option_quantity1"] * $row_i["price"];
            }

            $sql_i = "select item_id, " .
                     "   round(((item_price / currency_exchange_rate) * currency_factor),2) as price " .
                     "from items " .
                     "left join users on users.user_id = " . user_id() . " " .
                     "left join addresses on addresses.address_id = users.user_address " .
                     "left join currencies on currencies.currency_id = addresses.address_currency " .
                     "where item_id = " . $row["item_group_option_item2"];


            $res_i = mysql_query($sql_i) or dberror($sql_i);
            if ($row_i = mysql_fetch_assoc($res_i))
            {
                $diffenence_in_cost = $diffenence_in_cost - $row["item_group_option_quantity2"] * $row_i["price"];
            }

            if($row["item_group_option_item3"] and $row["item_group_option_item4"] and $row["item_group_option_logical"] == 0)
            {
                $sql_i = "select item_id, " .
                         "   round(((item_price / currency_exchange_rate) * currency_factor),2) as price " .
                         "from items " .
                         "left join users on users.user_id = " . user_id() . " " .
                         "left join addresses on addresses.address_id = users.user_address " .
                         "left join currencies on currencies.currency_id = addresses.address_currency " .
                         "where item_id = " . $row["item_group_option_item3"];


                $res_i = mysql_query($sql_i) or dberror($sql_i);
                if ($row_i = mysql_fetch_assoc($res_i))
                {
                    $diffenence_in_cost = $diffenence_in_cost + $row["item_group_option_quantity3"] * $row_i["price"];
                }

                $sql_i = "select item_id, " .
                         "   round(((item_price / currency_exchange_rate) * currency_factor),2) as price " .
                         "from items " .
                         "left join users on users.user_id = " . user_id() . " " .
                         "left join addresses on addresses.address_id = users.user_address " .
                         "left join currencies on currencies.currency_id = addresses.address_currency " .
                         "where item_id = " . $row["item_group_option_item4"];


                $res_i = mysql_query($sql_i) or dberror($sql_i);
                if ($row_i = mysql_fetch_assoc($res_i))
                {
                    $diffenence_in_cost = $diffenence_in_cost - $row["item_group_option_quantity4"] * $row_i["price"];
                }
            }
        }
        elseif($group == 1)
        {
            $sql_i = "select item_id, " .
                     "   round(((item_price / currency_exchange_rate) * currency_factor),2) as price " .
                     "from items " .
                     "left join users on users.user_id = " . user_id() . " " .
                     "left join addresses on addresses.address_id = users.user_address " .
                     "left join currencies on currencies.currency_id = addresses.address_currency " .
                     "where item_id = " . $row["item_group_option_item1"];


            $res_i = mysql_query($sql_i) or dberror($sql_i);
            if ($row_i = mysql_fetch_assoc($res_i))
            {
                $diffenence_in_cost = $diffenence_in_cost + $row["item_group_option_quantity1"] * $row_i["price"];
            }

            $sql_i = "select item_id, " .
                     "   round(((item_price / currency_exchange_rate) * currency_factor),2) as price " .
                     "from items " .
                     "left join users on users.user_id = " . user_id() . " " .
                     "left join addresses on addresses.address_id = users.user_address " .
                     "left join currencies on currencies.currency_id = addresses.address_currency " .
                     "where item_id = " . $row["item_group_option_item2"];


            $res_i = mysql_query($sql_i) or dberror($sql_i);
            if ($row_i = mysql_fetch_assoc($res_i))
            {
                $diffenence_in_cost = $diffenence_in_cost - $row["item_group_option_quantity2"] * $row_i["price"];

            }
        
        }
        elseif($group == 2)
        {
            $sql_i = "select item_id, " .
                     "   round(((item_price / currency_exchange_rate) * currency_factor),2) as price " .
                     "from items " .
                     "left join users on users.user_id = " . user_id() . " " .
                     "left join addresses on addresses.address_id = users.user_address " .
                     "left join currencies on currencies.currency_id = addresses.address_currency " .
                     "where item_id = " . $row["item_group_option_item3"];


            $res_i = mysql_query($sql_i) or dberror($sql_i);
            if ($row_i = mysql_fetch_assoc($res_i))
            {
                $diffenence_in_cost = $diffenence_in_cost + $row["item_group_option_quantity3"] * $row_i["price"];
            }

            $sql_i = "select item_id, " .
                     "   round(((item_price / currency_exchange_rate) * currency_factor),2) as price " .
                     "from items " .
                     "left join users on users.user_id = " . user_id() . " " .
                     "left join addresses on addresses.address_id = users.user_address " .
                     "left join currencies on currencies.currency_id = addresses.address_currency " .
                     "where item_id = " . $row["item_group_option_item4"];


            $res_i = mysql_query($sql_i) or dberror($sql_i);
            if ($row_i = mysql_fetch_assoc($res_i))
            {
                $diffenence_in_cost = $diffenence_in_cost - $row["item_group_option_quantity4"] * $row_i["price"];
            }
        }
    }

    return $diffenence_in_cost;
}


/********************************************************************
    save shop_basket
*********************************************************************/
function save_shop_basket($shop)
{
    $shopdump = addslashes(serialize($shop));

    $sql = "Select * from shopbaskets where shopbasket_user = " . user_id();
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $sql = "Update shopbaskets SET " .
               "shopbasket_shopdump = '" . $shopdump . "', " .
               "user_modified = '" . user_login() . "', " .
               "date_modified = '" . date("Y-m-d") . "' " .
               "where shopbasket_user = " . user_id();

        $result = mysql_query($sql) or dberror($sql);
    }
    else
    {

        // insert new shop into shop_baskets

        $sql =  "insert into shopbaskets " .
                "(shopbasket_user, shopbasket_shopdump, user_created, date_created) values (" .
                user_id() . ", " .
                "'" . $shopdump . "', " .
                "'" . user_login() . "', " .
                "'" . date("Y-m-d") . "')";

        $result = mysql_query($sql) or dberror($sql);
    }
    
    return true;
}


/********************************************************************
    save shop_basket adresses
*********************************************************************/
function save_shop_basket_addresses($addresses)
{
  
    $dump = addslashes(serialize($addresses));

    $sql = "Update shopbaskets SET shopbasket_orderdump = '" . $dump . "' where shopbasket_user = " . user_id();

    $result = mysql_query($sql) or dberror($sql);

    return true;
}


/********************************************************************
    get shop_basket 
*********************************************************************/
function get_shop_basket()
{
    $shop_data = array();
    $sql = "select shopbasket_shopdump " .
           "from shopbaskets " .
           "where shopbasket_user = " . user_id();

    $res = mysql_query($sql) or dberror();
    if ($res)
    {
        $row = mysql_fetch_assoc($res);
        $shop_data = unserialize(stripslashes($row['shopbasket_shopdump']));
    }

    return $shop_data;
}


/********************************************************************
    get shop_basket adresses last used
*********************************************************************/
function get_shop_basket_addresses()
{
    $order_data = array();
    $sql = "select shopbasket_orderdump " .
           "from shopbaskets " .
           "where shopbasket_user = " . user_id();

    $res = mysql_query($sql) or dberror();
    if ($res)
    {
        $row = mysql_fetch_assoc($res);
        $order_data = unserialize(stripslashes($row['shopbasket_orderdump']));
    }

    return $order_data;
}


/********************************************************************
    save shop_basket
*********************************************************************/
function save_shop_order($config_item_id, $config_picture_config_code)
{

    $order_data = get_shop_basket_addresses();

    $shop_data = get_shop_basket();


    // count replacements
    $num_of_replacements = 0;
    foreach($shop_data as $key=>$value)
    {
        if((substr($key, 0, 1) == "r" and $value == 1) or (substr($key, 0, 2) == "rb" and $value > 0))
        {            
            $num_of_replacements++;
        }
    }


    $user_region = get_user_region(user_id());


    if ($order_data["shop_is_delivery_address"])
    {
        
        $order_data["shop_address_company"] = $order_data["delivery_address_company"];
        $order_data["shop_address_company2"] = $order_data["delivery_address_company2"];
        $order_data["shop_address_address"] = $order_data["delivery_address_address"];
        $order_data["shop_address_address2"] = $order_data["delivery_address_address2"];
        $order_data["shop_address_zip"] = $order_data["delivery_address_zip"];
        $order_data["shop_address_place"] = $order_data["delivery_address_place"];
        $order_data["shop_address_country"] = $order_data["delivery_address_country"];
        $order_data["shop_address_phone"] = $order_data["delivery_address_phone"];
        $order_data["shop_address_fax"] = $order_data["delivery_address_fax"];
        $order_data["shop_address_email"] = $order_data["delivery_address_email"];
    }

    $order_fields = array();
    $order_values = array();

    $delivery_address_fields = array();
    $delivery_address_values = array();

    $order_fields[] = "order_number";
    
    $order_values[] = dbquote(order_generate_order_number());

    $order_fields[] = "order_date";
    $order_values[] = "now()";

    $order_fields[] = "order_type";
    $order_values[] = 2;    // catalog order

  	$order_fields[] = "order_client_address";
    $order_values[] = $order_data["address_id"];

    $order_fields[] = "order_user";
    $order_values[] = user_id();

    // currencies
    $currency = get_address_currency($order_data["address_id"]);
    $system_currency = get_system_currency_fields();

    $order_fields[] = "order_client_currency";
    $order_values[] = $currency["id"];

    $order_fields[] = "order_system_currency";
    $order_values[] = $system_currency["id"];

    $order_fields[] = "order_client_exchange_rate";
    $order_values[] = $currency["exchange_rate"];

    $order_fields[] = "order_system_exchange_rate";
    $order_values[] = $system_currency["exchange_rate"];

    $order_fields[] = "order_preferred_transportation_arranged";
    $order_values[] = dbquote($order_data["preferred_transportation_arranged"]);

	$order_fields[] = "order_preferred_transportation_mode";
    $order_values[] = dbquote($order_data["preferred_transportation_mode"]);

    $order_fields[] = "order_voltage";
    $order_values[] = dbquote($order_data["voltage"]);

    $order_fields[] = "order_preferred_delivery_date";
    $order_values[] = dbquote(from_system_date($order_data["preferred_delivery_date"]));

    $order_fields[] = "order_full_delivery";
    $order_values[] = dbquote($order_data["full_delivery"]);

    //$order_fields[] = "order_packaging_retraction";
    //$order_values[] = dbquote($order_data["packaging_retraction"]);

    $order_fields[] = "order_pedestrian_mall_approval";
    $order_values[] = dbquote($order_data["pedestrian_mall_approval"]);

    $order_fields[] = "order_delivery_comments";
    $order_values[] = dbquote($order_data["delivery_comments"]);

    $order_fields[] = "order_special_item_request";
    $order_values[] = dbquote($order_data["comments"]);

	// franchisee address
    $order_fields[] = "order_franchisee_address_company";
    $order_values[] = trim($order_data["franchisee_address_company"]) == "" ? "null" : dbquote($order_data["franchisee_address_company"]);

    $order_fields[] = "order_franchisee_address_company2";
    $order_values[] = trim($order_data["franchisee_address_company2"]) == "" ? "null" : dbquote($order_data["franchisee_address_company2"]);

    $order_fields[] = "order_franchisee_address_address";
    $order_values[] = trim($order_data["franchisee_address_address"]) == "" ? "null" : dbquote($order_data["franchisee_address_address"]);

    $order_fields[] = "order_franchisee_address_address2";
    $order_values[] = trim($order_data["franchisee_address_address2"]) == "" ? "null" : dbquote($order_data["franchisee_address_address2"]);

    $order_fields[] = "order_franchisee_address_zip";
    $order_values[] = trim($order_data["franchisee_address_zip"]) == "" ? "null" : dbquote($order_data["franchisee_address_zip"]);

    $order_fields[] = "order_franchisee_address_place";
    $order_values[] = trim($order_data["franchisee_address_place"]) == "" ? "null" : dbquote($order_data["franchisee_address_place"]);

    $order_fields[] = "order_franchisee_address_country";
    $order_values[] = trim($order_data["franchisee_address_country"]) == "" ? "null" : dbquote($order_data["franchisee_address_country"]);

    $order_fields[] = "order_franchisee_address_phone";
    $order_values[] = trim($order_data["franchisee_address_phone"]) == "" ? "null" : dbquote($order_data["franchisee_address_phone"]);

    $order_fields[] = "order_franchisee_address_fax";
    $order_values[] = trim($order_data["franchisee_address_fax"]) == "" ? "null" : dbquote($order_data["franchisee_address_fax"]);

    $order_fields[] = "order_franchisee_address_email";
    $order_values[] = trim($order_data["franchisee_address_email"]) == "" ? "null" : dbquote($order_data["franchisee_address_email"]);

    $order_fields[] = "order_franchisee_address_contact";
    $order_values[] = trim($order_data["franchisee_address_contact"]) == "" ? "null" : dbquote($order_data["franchisee_address_contact"]);

    // billing address
    $order_fields[] = "order_billing_address_company";
    $order_values[] = trim($order_data["billing_address_company"]) == "" ? "null" : dbquote($order_data["billing_address_company"]);

    $order_fields[] = "order_billing_address_company2";
    $order_values[] = trim($order_data["billing_address_company2"]) == "" ? "null" : dbquote($order_data["billing_address_company2"]);

    $order_fields[] = "order_billing_address_address";
    $order_values[] = trim($order_data["billing_address_address"]) == "" ? "null" : dbquote($order_data["billing_address_address"]);

    $order_fields[] = "order_billing_address_address2";
    $order_values[] = trim($order_data["billing_address_address2"]) == "" ? "null" : dbquote($order_data["billing_address_address2"]);

    $order_fields[] = "order_billing_address_zip";
    $order_values[] = trim($order_data["billing_address_zip"]) == "" ? "null" : dbquote($order_data["billing_address_zip"]);

    $order_fields[] = "order_billing_address_place";
    $order_values[] = trim($order_data["billing_address_place"]) == "" ? "null" : dbquote($order_data["billing_address_place"]);

    $order_fields[] = "order_billing_address_country";
    $order_values[] = trim($order_data["billing_address_country"]) == "" ? "null" : dbquote($order_data["billing_address_country"]);

    $order_fields[] = "order_billing_address_phone";
    $order_values[] = trim($order_data["billing_address_phone"]) == "" ? "null" : dbquote($order_data["billing_address_phone"]);

    $order_fields[] = "order_billing_address_fax";
    $order_values[] = trim($order_data["billing_address_fax"]) == "" ? "null" : dbquote($order_data["billing_address_fax"]);

    $order_fields[] = "order_billing_address_email";
    $order_values[] = trim($order_data["billing_address_email"]) == "" ? "null" : dbquote($order_data["billing_address_email"]);

    $order_fields[] = "order_billing_address_contact";
    $order_values[] = trim($order_data["billing_address_contact"]) == "" ? "null" : dbquote($order_data["billing_address_contact"]);

    //POS location
    $order_fields[] = "order_shop_address_company";
    $order_values[] = trim($order_data["shop_address_company"]) == "" ? "null" : dbquote($order_data["shop_address_company"]);

    $order_fields[] = "order_shop_address_company2";
    $order_values[] = trim($order_data["shop_address_company2"]) == "" ? "null" : dbquote($order_data["shop_address_company2"]);

    $order_fields[] = "order_shop_address_address";
    $order_values[] = trim($order_data["shop_address_address"]) == "" ? "null" : dbquote($order_data["shop_address_address"]);

    $order_fields[] = "order_shop_address_address2";
    $order_values[] = trim($order_data["shop_address_address2"]) == "" ? "null" : dbquote($order_data["shop_address_address2"]);

    $order_fields[] = "order_shop_address_zip";
    $order_values[] = trim($order_data["shop_address_zip"]) == "" ? "null" : dbquote($order_data["shop_address_zip"]);

    $order_fields[] = "order_shop_address_place";
    $order_values[] = trim($order_data["shop_address_place"]) == "" ? "null" : dbquote($order_data["shop_address_place"]);

    $order_fields[] = "order_shop_address_country";
    $order_values[] = trim($order_data["shop_address_country"]) == "" ? "null" : dbquote($order_data["shop_address_country"]);

    $order_fields[] = "order_shop_address_phone";
    $order_values[] = trim($order_data["shop_address_phone"]) == "" ? "null" : dbquote($order_data["shop_address_phone"]);

    $order_fields[] = "order_shop_address_fax";
    $order_values[] = trim($order_data["shop_address_fax"]) == "" ? "null" : dbquote($order_data["shop_address_fax"]);

    $order_fields[] = "order_shop_address_email";
    $order_values[] = trim($order_data["shop_address_email"]) == "" ? "null" : dbquote($order_data["shop_address_email"]);

    //FBI-Info

    $order_fields[] = "date_created";
    $order_values[] = "now()";

    $order_fields[] = "user_created";
    $order_values[] = dbquote(user_login());


    $sql = "insert into orders (" . join(", ", $order_fields) . ") " .
           "values (" . join(", ", $order_values) . ")";
    mysql_query($sql) or dberror($sql);

    $order_id = mysql_insert_id();
    
    append_order_state($order_id, "100", 2, 1);

    // insert delivery address into table oder_addresses
    $delivery_address_fields[] = "order_address_order";
    $delivery_address_values[] = $order_id;

    $delivery_address_fields[] = "order_address_type";
    $delivery_address_values[] = 2;

    $delivery_address_fields[] = "order_address_parent";
    $delivery_address_values[] = $order_data["address_id"];


    $delivery_address_fields[] = "order_address_company";
    $delivery_address_values[] = dbquote($order_data["delivery_address_company"]);

    $delivery_address_fields[] = "order_address_company2";
    $delivery_address_values[] = dbquote($order_data["delivery_address_company2"]);

    $delivery_address_fields[] = "order_address_address";
    $delivery_address_values[] = dbquote($order_data["delivery_address_address"]);

    $delivery_address_fields[] = "order_address_address2";
    $delivery_address_values[] = dbquote($order_data["delivery_address_address2"]);

    $delivery_address_fields[] = "order_address_zip";
    $delivery_address_values[] = dbquote($order_data["delivery_address_zip"]);

    $delivery_address_fields[] = "order_address_place";
    $delivery_address_values[] = dbquote($order_data["delivery_address_place"]);

    $delivery_address_fields[] = "order_address_country";
    $delivery_address_values[] = dbquote($order_data["delivery_address_country"]);

    $delivery_address_fields[] = "order_address_phone";
    $delivery_address_values[] = dbquote($order_data["delivery_address_phone"]);

    $delivery_address_fields[] = "order_address_fax";
    $delivery_address_values[] = dbquote($order_data["delivery_address_fax"]);

    $delivery_address_fields[] = "order_address_email";
    $delivery_address_values[] = dbquote($order_data["delivery_address_email"]);
   
    $delivery_address_fields[] = "order_address_contact";
    $delivery_address_values[] = dbquote($order_data["delivery_address_contact"]);

    $delivery_address_fields[] = "user_created";
    $delivery_address_values[] = dbquote(user_login());

    $delivery_address_fields[] = "date_created";
    $delivery_address_values[] = "current_timestamp";

    $sql = "insert into order_addresses (" . join(", ", $delivery_address_fields) . ") values (" . join(", ", $delivery_address_values) . ")";
	mysql_query($sql) or dberror($sql);
    // save items

	$options_added = false;
	$replacements_done = false;


    $sql = "select item_id, category_item_id, item_code, item_name, ".
           "item_type, category_item_category " .
           "from category_items " .
           "left join items on category_items.category_item_item=items.item_id " .
           "left join item_regions on item_region_item = item_id " .
           "where category_item_category = " . $shop_data["category_id"] . 
           "    and item_visible = 1 " .
           "    and item_active = 1 " .
           "    and item_region_region = " . $user_region;

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        $oitems = array();
		$item_id = $row["item_id"];
        
        //check if item has composition groups
        $sql_c = "select count(item_composition_id) as num_recs " .
                 "from item_compositions " .
                 "left join item_groups on item_group_id = item_composition_group " .
                 "left join item_group_items on item_group_item_group = item_composition_group " .
                 "left join items on item_id = item_group_item_item " .
                 "where item_composition_item = " .  $item_id;

        $res_c = mysql_query($sql_c) or dberror($sql_c);
        $row_c = mysql_fetch_assoc($res_c);

        $num_items = $row_c["num_recs"];
        
        if($num_items > 0)
        {

            $sql_r = "select item_composition_id, item_id, item_group_item_quantity " .
                     "from item_compositions " .
                     "left join item_groups on item_group_id = item_composition_group " .
                     "left join item_group_items on item_group_item_group = item_composition_group " .
                     "left join items on item_id = item_group_item_item " .
                     "where item_composition_item = " . $item_id;

            $res_r = mysql_query($sql_r) or dberror($sql_r);
            while ($row_r = mysql_fetch_assoc($res_r))
            {
                if(isset($oitems[$row_r["item_id"]]))
                {
                    $oitems[$row_r["item_id"]] = $oitems[$row_r["item_id"]] +  $row_r["item_group_item_quantity"];
                }
                else
                {
                    $oitems[$row_r["item_id"]] = $row_r["item_group_item_quantity"];
                }
            }
        }
        else // item has no composition groups
        {
            if(isset($oitems[$item_id]))
            {
                $oitems[$item_id] = $oitems[$item_id] +  1;
            }
            else
            {
                $oitems[$item_id] = 1;
            }
        }


        // addons into tabel items
        $sql_a = "select addon_id, addon_child, " .
                 "item_id, item_name, item_type " .
                 "from addons " .
                 "left join items on item_id = addon_child " .
                 "where addon_parent = " . $item_id;

        

        $res_a = mysql_query($sql_a) or dberror($sql_a);
        
        while ($row_a = mysql_fetch_assoc($res_a))
        {
            if(isset($oitems[$row_a["item_id"]]))
            {
                $oitems[$row_a["item_id"]] = $oitems[$row_a["item_id"]] +  1;
            }
            else
            {
                $oitems[$row_a["item_id"]] = 1;
            }
        }




        // options into table items
        $num_of_options = 0;
        $options_ids = array();
        foreach($shop_data as $key=>$value)
        {
            if(substr($key, 0, 2) == "o_" and $value == 1)
            {
                $parts = explode ( "_", $key);
                
                if($parts[1] == $item_id)
                {
                    $options_ids[] = $parts[2];
                    $num_of_options++;
                }
            }
        }

		if($num_of_options > 0 and $options_added == false) // add options only once
        {
            $options_added = true;
			foreach($options_ids as $key=>$value)
            {

                $sql_a = "select item_id, item_name, item_type, " .
                         "item_option_quantity " .
                         "from item_options " .
                         "left join items on item_id = item_option_child " .
                         "where item_option_id = " . $value;

                $res_a = mysql_query($sql_a) or dberror($sql_a);
                
                if ($row_a = mysql_fetch_assoc($res_a))
                {
                    if(isset($oitems[$row_a["item_id"]]))
                    {
                        $oitems[$row_a["item_id"]] = $oitems[$row_a["item_id"]] +  1;
                    }
                    else
                    {
                        $oitems[$row_a["item_id"]] = 1;
                    }
                }
            }
        }


        // replacement groups

        $num_of_replacements = 0;
        foreach($shop_data as $key=>$value)
        {
            if((substr($key, 0, 1) == "r" and $value == 1) or (substr($key, 0, 2) == "rb" and $value > 0))
            {            
                $num_of_replacements++;
            }
        }

        if($num_of_replacements > 0 and $replacements_done == false) // replace items only once
        {
            $replacements_done = true;
			foreach($shop_data as $key=>$value)
            {
                if(substr($key, 0, 1) == "r" and $value == 1) // 
                {
                    $parts = explode ( "_", $key);

                    // get itemlist of group
                    $sql_g = "select * " .
                             "from item_group_options " .
                             "where item_group_option_id = " . $parts[1];

                    $res_g = mysql_query($sql_g) or dberror($sql_g);
                    $row_g = mysql_fetch_assoc($res_g);

                    if($row_g["item_group_option_item1"])
                    {
                        if(isset($oitems[$row_g["item_group_option_item1"]]))
                        {
                            $oitems[$row_g["item_group_option_item1"]] = $oitems[$row_g["item_group_option_item1"]] +  $row_g["item_group_option_quantity1"];
                            
                        }
                        else
                        {
                           $oitems[$row_g["item_group_option_item1"]] = $row_g["item_group_option_quantity1"];
                        }
                    }
                    if($row_g["item_group_option_item2"])
                    {
                        if(isset($oitems[$row_g["item_group_option_item2"]]))
                        {
                            $oitems[$row_g["item_group_option_item2"]] = $oitems[$row_g["item_group_option_item2"]] -  $row_g["item_group_option_quantity2"];

                            if($oitems[$row_g["item_group_option_item2"]] < 0)
                            {
                                $oitems[$row_g["item_group_option_item2"]] = 0;
                            }
                        }
                    }
                    if($row_g["item_group_option_item3"])
                    {
                        if(isset($oitems[$row_g["item_group_option_item3"]]))
                        {
                            $oitems[$row_g["item_group_option_item3"]] = $oitems[$row_g["item_group_option_item3"]] +  $row_g["item_group_option_quantity3"];
                        }
                        else
                        {
                           $oitems[$row_g["item_group_option_item3"]] = $row_g["item_group_option_quantity3"];
                        }
                    }
                    if($row_g["item_group_option_item4"])
                    {
                        if(isset($oitems[$row_g["item_group_option_item4"]]))
                        {
                            $oitems[$row_g["item_group_option_item4"]] = $oitems[$row_g["item_group_option_item4"]] -  $row_g["item_group_option_quantity4"];

                            if($oitems[$row_g["item_group_option_item4"]] < 0)
                            {
                                $oitems[$row_g["item_group_option_item4"]] = 0;
                            }
                        }
                    }
                }
                if(substr($key, 0, 2) == "rb" and $value > 0)
                {
                    $parts = explode ( "_", $key);
                
                    $sql_g = "select * " .
                             "from item_group_options " .
                             "where item_group_option_id = " . $parts[1];

                    $res_g = mysql_query($sql_g) or dberror($sql_g);
                    $row_g = mysql_fetch_assoc($res_g);
                
                
                   if($value == 1)
                   {
                        if($row_g["item_group_option_item1"])
                        {
                            if(isset($oitems[$row_g["item_group_option_item1"]]))
                            {
                                $oitems[$row_g["item_group_option_item1"]] = $oitems[$row_g["item_group_option_item1"]] +  $row_g["item_group_option_quantity1"];
                            }
                            else
                            {
                               $oitems[$row_g["item_group_option_item1"]] = $row_g["item_group_option_quantity1"];
                            }
                        }
                        if($row_g["item_group_option_item2"])
                        {
                            if(isset($oitems[$row_g["item_group_option_item2"]]))
                            {
                                $oitems[$row_g["item_group_option_item2"]] = $oitems[$row_g["item_group_option_item2"]] - 1 * $row_g["item_group_option_quantity2"];
                                if($oitems[$row_g["item_group_option_item2"]] < 0)
                                {
                                    $oitems[$row_g["item_group_option_item2"]] = 0;
                                }
                            }
                        }
                   }
                   elseif($value == 2)
                   {
                        if($row_g["item_group_option_item3"])
                        {
                            if(isset($oitems[$row_g["item_group_option_item3"]]))
                            {
                                $oitems[$row_g["item_group_option_item3"]] = $oitems[$row_g["item_group_option_item3"]] +  $row_g["item_group_option_quantity3"];
                            }
                            else
                            {
                               $oitems[$row_g["item_group_option_item3"]] = $row_g["item_group_option_quantity3"];
                            }
                        }
                        if($row_g["item_group_option_item4"])
                        {
                            if(isset($oitems[$row_g["item_group_option_item4"]]))
                            {
                                $oitems[$row_g["item_group_option_item4"]] = $oitems[$row_g["item_group_option_item4"]] - 1 * $row_g["item_group_option_quantity4"];


                                if($oitems[$row_g["item_group_option_item4"]] < 0)
                                {
                                    $oitems[$row_g["item_group_option_item4"]] = 0;
                                }
                            }
                        }
                   }
                }
            }
        }

        // create itemlist
        foreach($oitems as $key=>$value)
        {
            if($value > 0)
            {
                $sql_a = "select item_id, item_code, item_name, item_type " .
                         "from items " . 
                         "where item_id = " . $key;


                $res_a = mysql_query($sql_a) or dberror($sql_a);

                if($row_a = mysql_fetch_assoc($res_a))
                {
                
                    $order_item_fields = array();
                    $order_item_values = array();

                    $order_item_fields[] = "order_item_order";
                    $order_item_values[] = dbquote($order_id);

                    $order_item_fields[] = "order_item_item";
                    $order_item_values[] = $row_a["item_id"];

                    $order_item_fields[] = "order_item_category";
                    $order_item_values[] = $shop_data["category_id"];

                    $order_item_fields[] = "order_item_type";
                    $order_item_values[] = dbquote($row_a["item_type"]);

                    $order_item_fields[] = "order_item_text";
                    $order_item_values[] = dbquote($row_a["item_name"]);


                    // get supplier's address id (first record in case of several)
                    $sql_s = "select address_id, address_company, supplier_item_price, supplier_item_currency ".
                             "from items ".
                             "left join suppliers on item_id = supplier_item ".
                             "left join addresses on supplier_address = address_id ".
                             "where item_id = " . $row_a["item_id"];

                    $res_s = mysql_query($sql_s) or dberror($sql_s);
                    if ($row_s = mysql_fetch_assoc($res_s))
                    {
                        if ($row_s["address_id"])
                        {
                           // price and currency

                            if ($row_s["supplier_item_currency"])
                            {
                                $currency = get_currency($row_s["supplier_item_currency"]);
                            }
                            else
                            {
                                $currency = get_address_currency($row_s["address_id"]);
                            }

                            $order_item_fields[] = "order_item_supplier_currency";
                            $order_item_values[] = $currency["id"];

                            $order_item_fields[] = "order_item_supplier_exchange_rate";
                            $order_item_values[] = $currency["exchange_rate"];

                            $order_item_fields[] = "order_item_supplier_price";
                            $order_item_values[] = $row_s["supplier_item_price"];

                            $order_item_fields[] = "order_item_supplier_address";
                            $order_item_values[] = dbquote($row_s["address_id"]);
                        }
                    }

                    // get orders's currency  & get item data
                    
                    $currency = get_order_currency($order_id);

                    $sql_c = "select * ".
                             "from items ".
                             "left join category_items on item_id = category_item_item ".
                             "left join categories on category_item_category = category_id ".
                             "where item_id = " . $row_a["item_id"];

                    $res_c = mysql_query($sql_c) or dberror($sql_c);
                    $type = "";

                    if ($row_c = mysql_fetch_assoc($res_c))
                    {
                        $order_item_fields[] = "order_item_system_price";
                        $order_item_values[] = trim($row_c["item_price"]) == "" ? "null" : dbquote($row_c["item_price"]);
                        $client_price = $row_c["item_price"] / $currency["exchange_rate"] * $currency["factor"];

                        $order_item_fields[] = "order_item_client_price";
                        $order_item_values[] = trim($client_price) == "" ? "null" : dbquote($client_price);

                        $order_item_fields[] = "order_item_cost_unit_number";

                        $order_item_values[] = trim($row_c["category_cost_unit_number"]) == "" ? "null" : dbquote($row_c["category_cost_unit_number"]);
                    }

                    $order_item_fields[] = "order_item_po_number";
                    $order_item_values[] = dbquote("");

                    $order_item_fields[] = "order_item_offer_number";
                    $order_item_values[] = dbquote("");   

                    $order_item_fields[] = "order_item_shipment_code";
                    $order_item_values[] = dbquote("");

                    $order_item_fields[] = "order_item_forwarder_address";
                    $order_item_values[] = dbquote("");

                    $order_item_fields[] = "order_item_staff_for_discharge";
                    $order_item_values[] = dbquote("");
                    
                    $order_item_fields[] = "order_item_no_offer_required";
                    $order_item_values[] = dbquote("1");

                    $order_item_fields[] = "order_item_quantity";
                    $order_item_values[] = $value;

                    $order_item_fields[] = "date_created";
                    $order_item_values[] = "now()";

                    $order_item_fields[] = "user_created";
                    $order_item_values[] = dbquote(user_login());

                    $sql = "insert into order_items (" . join(", ", $order_item_fields) . ") values (" . join(", ", $order_item_values) . ")";
                    
                    mysql_query($sql) or dberror($sql);
                }
            }
        }
    }


    // insert delivery addresses into table order_addresses for all items
    // not having a delivery address and beloging to the same order

    $sql_order_items = "select order_item_id, order_item_order ".
                       "from order_items ".
                       "where order_item_order = " . $order_id .
                       "    and order_item_type <= ". ITEM_TYPE_SPECIAL;


    $res = mysql_query($sql_order_items) or dberror($sql_order_items);

    while ($row = mysql_fetch_assoc($res)) 
    {
        // check if record is already there
        $sql = "select order_address_id ".
               "from order_addresses ".
               "where order_address_order = " . $row["order_item_order"] .
               "    and order_address_order_item = " . $row["order_item_id"].
               "    and order_address_type = 2";

        $res1 = mysql_query($sql) or dberror($sql);
        if ($row1 = mysql_fetch_assoc($res1)) 
        {
            //nothing
        }
        else
        {
            // insert new record
    
            $delivery_address_fields = array();
            $delivery_address_values = array();

            $delivery_address_fields[] = "order_address_order";
            $delivery_address_values[] = $row["order_item_order"];

            $delivery_address_fields[] = "order_address_order_item";
            $delivery_address_values[] = $row["order_item_id"];

            $delivery_address_fields[] = "order_address_type";
            $delivery_address_values[] = 2;

            $delivery_address_fields[] = "order_address_company";
            $delivery_address_values[] = trim($order_data["delivery_address_company"]) == "" ? "null" : dbquote($order_data["delivery_address_company"]);

            $delivery_address_fields[] = "order_address_company2";
            $delivery_address_values[] = trim($order_data["delivery_address_company2"]) == "" ? "null" : dbquote($order_data["delivery_address_company2"]);

            $delivery_address_fields[] = "order_address_address";
            $delivery_address_values[] = trim($order_data["delivery_address_address"]) == "" ? "null" : dbquote($order_data["delivery_address_address"]);

            $delivery_address_fields[] = "order_address_address2";
            $delivery_address_values[] = trim($order_data["delivery_address_address2"]) == "" ? "null" : dbquote($order_data["delivery_address_address2"]);

            $delivery_address_fields[] = "order_address_zip";
            $delivery_address_values[] = trim($order_data["delivery_address_zip"]) == "" ? "null" : dbquote($order_data["delivery_address_zip"]);

            $delivery_address_fields[] = "order_address_place";
            $delivery_address_values[] = trim($order_data["delivery_address_place"]) == "" ? "null" : dbquote($order_data["delivery_address_place"]);

            $delivery_address_fields[] = "order_address_country";
            $delivery_address_values[] = trim($order_data["delivery_address_country"]) == "" ? "null" : dbquote($order_data["delivery_address_country"]);

            $delivery_address_fields[] = "order_address_phone";
            $delivery_address_values[] = trim($order_data["delivery_address_phone"]) == "" ? "null" : dbquote($order_data["delivery_address_phone"]);

            $delivery_address_fields[] = "order_address_fax";
            $delivery_address_values[] = trim($order_data["delivery_address_fax"]) == "" ? "null" : dbquote($order_data["delivery_address_fax"]);

            $delivery_address_fields[] = "order_address_email";
            $delivery_address_values[] = trim($order_data["delivery_address_email"]) == "" ? "null" : dbquote($order_data["delivery_address_email"]);

            $delivery_address_fields[] = "order_address_parent";
            $delivery_address_values[] = $order_data["address_id"];

            $delivery_address_fields[] = "order_address_contact";
            $delivery_address_values[] = trim($order_data["delivery_address_contact"]) == "" ? "null" : dbquote($order_data["delivery_address_contact"]);

            $delivery_address_fields[] = "date_created";
            $delivery_address_values[] = "current_timestamp";

            $delivery_address_fields[] = "user_created";
            $delivery_address_values[] = dbquote($_SESSION["user_login"]);

            $sql = "insert into order_addresses (" . join(", ", $delivery_address_fields) . ") values (" . join(", ", $delivery_address_values) . ")";
            mysql_query($sql) or dberror($sql);
        }
    }

    
    
    //reset basket

    $order_data = get_shop_basket_addresses();
    $order_data["preferred_delivery_date"] = "";
    save_shop_basket_addresses($order_data);

    $sql ="Update shopbaskets set shopbasket_shopdump = '' where shopbasket_user = " . user_id();
    mysql_query($sql) or dberror($sql);


    $result = add_picture_of_shop($order_id, $config_item_id, $config_picture_config_code);

    return true;
}


/********************************************************************
    add picture as attachment
*********************************************************************/
function add_picture_of_shop($oid, $config_item_id, $config_picture_config_code)
{


    $companies = get_involved_companies($oid, 0);


    $pix_name = "";
    $pix_description = "";
    $pix_path = "";

    $sql = "select item_file_title, item_file_description, item_file_path " .
           "from item_files where item_file_item = " . $config_item_id .
           "   and item_file_purpose = 6 " .
           "   and item_file_picture_config_code = " . dbquote($config_picture_config_code);

    $res = mysql_query($sql) or dberror();
    if ($row = mysql_fetch_assoc($res))
    {
        $pix_name = $row['item_file_title'];
        $pix_description = $row['item_file_description'];
        $pix_path = $row['item_file_path'];
    }


    if(!$config_picture_config_code)
    {
        $sql = "select item_file_title, item_file_description, item_file_path " .
               "from item_files where item_file_item = " . $config_item_id .
               "   and item_file_purpose = 2 ";

        $res = mysql_query($sql) or dberror();
        if ($row = mysql_fetch_assoc($res))
        {
            $pix_name = $row['item_file_title'];
            $pix_description = $row['item_file_description'];
            $pix_path = $row['item_file_path'];
        }
    }

    //add attachment to order

    $order_file_fields = array();
    $order_file_values = array();

    $order_file_fields[] = "order_file_order";
    $order_file_values[] = $oid;

    $order_file_fields[] = "order_file_title";
    $order_file_values[] = dbquote($pix_name);

    $order_file_fields[] = "order_file_description";
    $order_file_values[] = dbquote($pix_description);

    $order_file_fields[] = "order_file_path";
    $order_file_values[] = dbquote($pix_path);

    $order_file_fields[] = "order_file_type";
    $order_file_values[] = 2;

    $order_file_fields[] = "order_file_category";
    $order_file_values[] = 2;

    $order_file_fields[] = "order_file_owner";
    $order_file_values[] = user_id();

    $order_file_fields[] = "order_file_visibility_check";
    $order_file_values[] = 2;

    $order_file_fields[] = "date_created";
    $order_file_values[] = "current_timestamp";

    $order_file_fields[] = "date_modified";
    $order_file_values[] = "current_timestamp";

    if (isset($_SESSION["user_login"]))
    {
        $order_file_fields[] = "user_created";
        $order_file_values[] = dbquote($_SESSION["user_login"]);

        $order_file_fields[] = "user_modified";
        $order_file_values[] = dbquote($_SESSION["user_login"]);
    }

    $sql = "insert into order_files (" . join(", ", $order_file_fields) . ") values (" . join(", ", $order_file_values) . ")";
    
    mysql_query($sql) or dberror($sql);

    $order_file_id = mysql_insert_id();


    // save accessibility info
    foreach ($companies as $key=>$value)
    {
        if ($value)
        {
            $attachment_address_fields = array();
            $attachment_address_values = array();

            $attachment_address_fields[] = "order_file_address_file";
            $attachment_address_values[] = $order_file_id;

            $attachment_address_fields[] = "order_file_address_address";
            $attachment_address_values[] = $value["id"];

            $attachment_address_fields[] = "date_created";
            $attachment_address_values[] = "current_timestamp";

            $attachment_address_fields[] = "date_modified";
            $attachment_address_values[] = "current_timestamp";
            
            if (isset($_SESSION["user_login"]))
            {
                $attachment_address_fields[] = "user_created";
                $attachment_address_values[] = dbquote($_SESSION["user_login"]);

                $attachment_address_fields[] = "user_modified";
                $attachment_address_values[] = dbquote($_SESSION["user_login"]);
            }

            $sql = "insert into order_file_addresses (" . join(", ", $attachment_address_fields) . ") values (" . join(", ", $attachment_address_values) . ")";
            mysql_query($sql) or dberror($sql);
        }
    }
    
    return true;

}
?>