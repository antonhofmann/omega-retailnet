<?php
/********************************************************************

    order_page_actions.php

    Defines the possible page actions depending on access rights

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-25
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-06-27
    Version:        1.1.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "order_state_constants.php";

if (has_access("can_create_new_orders"))
{
    $page->register_action('new', 'New Order', "order_new.php");

   if (!basket_is_empty())
    {
        $page->register_action('basket', 'Shopping List', "basket.php");
        $page->register_action('nothing0', "", "");
    }
}

if (has_access("can_view_client_data_in_orders"))
{   
    $page->register_action('view_client_data', 'View Client Data', 
                           "order_view_client_data.php?oid=" . param('oid'));
}

if (has_access("can_edit_retail_data"))
{   
    $page->register_action('edit_retail_data', 'Edit Retail Data', 
                           "order_edit_retail_data.php?oid=" . param('oid'));
}

if (has_access("can_edit_list_of_materials_in_orders"))
{   
    $page->register_action('edit_material_list', 'Edit List of Materials', 
                           "order_edit_material_list.php?oid=" . param('oid'));
}


if (has_access("can_edit_his_list_of_materials_in_orders"))
{   
    $page->register_action('edit_material_list_supplier', 'Edit Offer Data', 
                           "order_edit_material_list_supplier.php?oid=" . param('oid'));
}

if (has_access("can_view_ordered_values_in_orders"))
{   
    $page->register_action('edit_view_ordered_values', 'View Ordered Values', 
                           "order_view_ordered_values.php?oid=" . param('oid'));
}

if (has_access("can_view_order_invoice_information"))
{   
	$page->register_action('view_cost_information', 'View Cost Information', 
						   "order_view_cost_information.php?oid=" . param("oid"));
}

if (has_access("can_edit_budget_in_orders"))
{   
    $page->register_action('edit_order_budget', 'Edit Budget',
                           "order_edit_order_budget.php?oid=" . param("oid"));

}

if (has_access("has_access_to_all_orders"))
{   
   $page->register_action('view_order_budget', 'View Budget', 
                          "order_view_order_budget.php?oid=" . param("oid"));

   $url = "http://" . $_SERVER["HTTP_HOST"] . "/user/order_view_order_budget_pdf.php?oid=" . param("oid");
   $page->register_action('print_budget', 'Print Budget', $url, "_blank");

}
else
{
    if (has_access("can_view_budget_in_orders"))
    {   
        //if (is_present_order_state(param("oid"), REQUEST_FOR_BUDGET_APPROVAL_SUBMITTED))
        //{
            $page->register_action('view_order_budget', 'View Budget', 
                                   "order_view_order_budget.php?oid=" . param("oid"));
            $url = "http://" . $_SERVER["HTTP_HOST"] . "/user/order_view_order_budget_pdf.php?oid=" . param("oid");
            $page->register_action('print_budget', 'Print Budget', $url, "_blank");
        //}
    }
}


if (has_access("can_edit_supplier_data_in_orders"))
{   
    $page->register_action('edit_supplier_data', 'Edit Pickup Data', "order_edit_supplier_data.php?oid=" . param("oid"));    
}

if (has_access("can_edit_traffic_data_in_orders"))
{
    $page->register_action('edit_traffic_data', 'Edit Traffic Data', 
                           "order_edit_traffic_data.php?oid=" .param('oid'));
}

if (has_access("can_edit_delivery_addresses_in_orders"))
{
    $page->register_action('edit_delivery_addresses', 'Edit Delivery Addresses', 
                           "order_edit_delivery_addresses.php?oid=" .param('oid'));

}

if (has_access("can_view_delivery_schedule_in_orders"))
{
    $url = "http://" . $_SERVER["HTTP_HOST"] . "/user/order_view_traffic_data_pdf.php?oid=" . param("oid");
    $page->register_action('print_traffic_data', 'Print Delivery Schedule', $url, "_blank");
    
    $page->register_action('view_traffic_data', 'View Delivery Schedule', 
                           "order_view_traffic_data.php?oid=" .param('oid'));
}

if (has_access("can_edit_client_data_in_orders"))
{   
    $page->register_action('edit_request', 'Edit Request', 
                           "order_edit_request.php?oid=" . param('oid'));
}

if (has_access("can_view_comments_in_orders"))
{   
    $page->register_action('nothing1', '', "");
    $page->register_action('view_comments', 'Comments', "order_view_comments.php?oid=" . param("oid"));
}

if (has_access("can_view_attachments_in_orders"))
{   
    $page->register_action('view_attachments', 'Attachments', "order_view_attachments.php?oid=" . param("oid"));
}

if (has_access("can_use_taskcentre_in_orders"))
{   
    $page->register_action('nothing1', '', "");
    $page->register_action('task_center', 'Task Center', "order_task_center.php?oid=" . param("oid"));
}

if (has_access("has_accessto_taskpool_in_orders"))
{   
    $page->register_action('task_pool', 'Task Pool', "order_task_pool.php?oid=" . param("oid"));
}

if (has_access("can_view_history_in_orders"))
{   
    $page->register_action('history', 'View History', "order_history.php?oid=" . param("oid"));
    $page->register_action('mail_history', 'View Mails', "order_mail_history.php?oid=" . param("oid"));
}

$page->register_action('nothing2', '', "");
$page->register_action('home', 'Home', "welcome.php");

?>