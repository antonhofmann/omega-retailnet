<?php
/********************************************************************

    save_functions.php

    Various utility functions to save information into tables

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-08
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-02-23
    Version:        1.2.4

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "order_state_constants.php";


/********************************************************************
    add fbi_info to the list of fields
*********************************************************************/
function add_fbi_info(&$fields, &$values)
{
     $fields[] = "date_created";
     $values[] = "now()";

     $fields[] = "date_modified";
     $values[] = "now()";

     $fields[] = "user_created";
     $values[] = dbquote(user_login());

     $fields[] = "user_modified";
     $values[] = dbquote(user_login());
}

/********************************************************************
    save an address record
*********************************************************************/
function save_address($form, $address_type = BILLING_ADDRESS, $order_id = "NULL",
                            $order_item_id = "NULL", $address_parent = "NULL", 
                            $prefix = "billing_address")
{
    
	
	//insert new place into places

	
	if(array_key_exists('delivery_address_place_id', $form->items))
	{
		if($form->items['delivery_address_place_id']['value'] == 999999999) {
			$country_id = $form->items['delivery_address_country']['value'];
			$province_id = $form->items['delivery_address_province_id']['value'];
			$place_name = $form->items['delivery_address_place']['value'];

			$sql = "insert into places (place_country, place_province, place_name, date_created, user_created) Values (" . 
				   $country_id .  "," . $province_id . "," . dbquote($place_name) . "," . dbquote(date('Y-m-d')) . "," . dbquote(user_login()) . ")";

			$result = mysql_query($sql) or dberror($sql);

			$form->items['delivery_address_place_id']['value'] = mysql_insert_id();

		}
	};



	$sql = "show columns ".
           "from order_addresses " . 
           "like 'order_address_%'";

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        if (empty($row['Key']))
        {
            if($row['Field'] != "order_address_inactive" and $row['Field'] != "order_address_place_new" and $row['Field'] != "order_address_place_tmp")
			{
				$table_fields[] = $row['Field'];
			}
        }
    }

    $address_fields[] = "order_address_order";
    $address_values[] = $order_id;

    $address_fields[] = "order_address_order_item";
    $address_values[] = $order_item_id;
    
    $address_fields[] = "order_address_type";
    $address_values[] = $address_type;

    $address_fields[] = "order_address_parent";
    $address_values[] = $address_parent;

    foreach ($table_fields as $field)
    {
        if (($field != "order_address_id")
           && ($field != "order_address_order")
           && ($field != "order_address_order_item")
           && ($field != "order_address_type")
           && ($field != "order_address_parent"))
        {

            if (($prefix == "shop_address") 
                && (($field == "order_address_phone")
                    || ($field == "order_address_fax")
                    || ($field == "order_address_email")
                    || ($field == "order_address_contact")))
            {
                continue;
            }

            $address_fields[] = $field;

            /*
			if ((($prefix == "delivery_address") 
                || ($prefix == "shop_address"))
                 && ($form->value("delivery_is_billing_address")))
            {
                $address_values[] = trim($form->value(str_replace("order_address", "billing_address", $field))) == "" ? "null" : dbquote($form->value(str_replace("order_address", "billing_address", $field)));
            } else {
                $address_values[] = trim($form->value(str_replace("order_address", $prefix, $field))) == "" ? "null" : dbquote($form->value(str_replace("order_address", $prefix, $field)));
            }
			*/

			if ((($prefix == "delivery_address") 
                || ($prefix == "shop_address"))
                 )
            {
                $address_values[] = trim($form->value(str_replace("order_address", "delivery_address", $field))) == "" ? "null" : dbquote($form->value(str_replace("order_address", "delivery_address", $field)));
            } else {
                $address_values[] = trim($form->value(str_replace("order_address", $prefix, $field))) == "" ? "null" : dbquote($form->value(str_replace("order_address", $prefix, $field)));
            }
        }
    }

    add_fbi_info($address_fields, $address_values);

    $sql = "insert into order_addresses (" . join(", ", $address_fields) . ") values (" . join(", ", $address_values) . ")";

    mysql_query($sql) or dberror($sql);

    return mysql_insert_id();
}


/********************************************************************
    create a new project number
*********************************************************************/
function project_create_project_number_check_number($project_number)
{
	$sql = "select count(project_id) as num_recs " . 
		   "from projects " . 
		   "where project_number = " . dbquote($project_number);

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);

    if($row["num_recs"] > 0)
	{
		return true;
	}
	else
	{
		return false;
	}

}

function project_create_project_number($client_id, $pos_type, $project_kind)
{
    // build prefix (first and last figure of year)

    $res = getdate(time());
    $yyyy = $res["year"];
	$project_number = substr($yyyy, 0, 1) . substr($yyyy, 2, 2) . ".";
	
    // get country code
    $sql = "select countries.country_code ".
           "from addresses LEFT JOIN countries ON address_country = countries.country_id ".
           "where address_id=" . $client_id;

    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
    $project_number = $project_number . $row["country_code"] . ".";

    // get project type's starting number
	if($project_kind == 4 or $project_kind == 5) // Take Over and Lease Renewalprojects get separate number
	{
		// get latest project number
		if($project_kind == 4) {
			$project_number .= 'T';
		}
		else
		{
			$project_number .= 'L';
		}
		
		$sql = "select project_number ".
			   "from projects ".
			   "where project_number Like '" . $project_number . "%' ".
			   "order by project_number desc ".
			   "limit 0,1";

		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$last = substr($row["project_number"],8,3);
			$next = intval($last) + 1;

			if ($next < 10)
			{
				$next = "00" . $next;
			}
			elseif ($next < 100)
			{
				$next = "0" . $next;
			}

			$project_number=$project_number . $next;
		}
		else
		{
			$project_number= $project_number ."01";
		}

		//check if project number is occupied by another product line
		$occupied = project_create_project_number_check_number($project_number);

		while($occupied == true)
		{
			$last = substr($project_number,9,2);
			$next = intval($last) + 1;
			if ($next < 10)
			{
				$next = "0" . $next;
			}
			$project_number = substr($project_number,0,9) . $next;
			$occupied = project_create_project_number_check_number($project_number);
		}
		
	}
	else
	{
		$sql = "select * ".
			   "from product_line_pos_types ".
			   "where product_line_pos_type_pos_type = " . $pos_type . 
			   " and product_line_pos_type_product_line is NULL";

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$first = $row["product_line_pos_type_starting_number"];
		}



		if ($first < 10)
		{
			$first = "00" . $first;
		}
		elseif ($first < 100)
		{
			$first = "0" . $first;
		}

		// get latest project number of non take over project
		
		$sql = "select project_number ".
			   "from projects ".
			   "where project_number Like '" . $project_number . "%' ".
			   "    and project_postype = " . $pos_type . " " .
			   "    and project_projectkind <> 3 " .
		       "    and project_projectkind <> 4 " . 
			   "    and project_projectkind <> 5 " . 
			   "order by project_number desc ".
			   "limit 0,1";

		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$last = substr($row["project_number"],8,3);
			$next = intval($last) + 1;

			if ($next < 10)
			{
				$next = "00" . $next;
			}
			elseif ($next < 100)
			{
				$next = "0" . $next;
			}

			$project_number=$project_number . $next;
		}
		else
		{
			$project_number=$project_number . $first;
		}

		//check if project number is occupied by another product line
		$occupied = project_create_project_number_check_number($project_number);

		while($occupied == true)
		{
			$last = substr($project_number,8,3);
			$next = intval($last) + 1;
			if ($next < 10)
			{
				$next = "0" . $next;
			}
			elseif ($next < 100)
			{
				$next = "0" . $next;
			}
			$project_number = substr($project_number,0,8) . $next;
			$occupied = project_create_project_number_check_number($project_number);
		}
	}

    return $project_number;
}



/********************************************************************
   update project client data
*********************************************************************/
function  project_update_client_data($form,$design_objectives_listbox_names,$design_objectives_checklist_names)
{

    $order_fields = array();

    $delivery_address_fields = array();

    $project_fields = array();

    $project_item_fields = array();
    $project_item_values = array();


	//update table posorders or posorderspipeline
	$sql = "select order_number, project_projectkind from orders " . 
		   "left join projects on project_order = order_id " . 
		   "where order_id = " . $form->value("oid");

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$old_project_number = $row["order_number"];
		$old_projectkind = $row["project_projectkind"];

		$year_of_order = substr($form->value("project_number"),0,2);
		if($year_of_order > 30 and $year_of_order <= 99)
		{
			$year_of_order = "19" . $year_of_order;
		}
		else
		{
			$year_of_order = substr($year_of_order, 0, 1) . '00' . substr($year_of_order, 1, 1);
		}

		if(substr($form->value("project_number"),3,1) == '.')
		{
			$year_of_order = substr($form->value("project_number"),0,1) . '0' . substr($form->value("project_number"),1,2);
		}

		$sql = "update posorders set " .
			   "posorder_year = " . dbquote($year_of_order) . ", " . 
			   "posorder_product_line = " . dbquote($form->value("product_line")) . ", " . 
			   "posorder_postype = " . dbquote($form->value("project_postype")) . ", " . 
			   "posorder_subclass = " . dbquote($form->value("project_pos_subclass")) . ", " . 
			   "posorder_project_kind = " . dbquote($form->value("project_projectkind")) . ", " . 
			   "posorder_legal_type = " . dbquote($form->value("project_cost_type")) . ", " .
			   "posorder_furniture_height = " . dbquote($form->value("furniture_height")) . ", " .
			   "posorder_furniture_height_mm = " . dbquote($form->value("project_furniture_height_mm")) . ", " .
			   "posorder_ordernumber = " . dbquote($form->value("project_number")) .
			   " where posorder_ordernumber = " . dbquote($old_project_number);


		$result = mysql_query($sql) or dberror($sql);

		$sql = "update posorderspipeline set " .
			   "posorder_year = " . dbquote($year_of_order) . ", " . 
			   "posorder_product_line = " . dbquote($form->value("product_line")) . ", " . 
			   "posorder_postype = " . dbquote($form->value("project_postype")) . ", " . 
			   "posorder_subclass = " . dbquote($form->value("project_pos_subclass")) . ", " . 
			   "posorder_project_kind = " . dbquote($form->value("project_projectkind")) . ", " . 
			   "posorder_legal_type = " . dbquote($form->value("project_cost_type")) . ", " .
			   "posorder_furniture_height = " . dbquote($form->value("furniture_height")) . ", " .
			   "posorder_furniture_height_mm = " . dbquote($form->value("project_furniture_height_mm")) . ", " .
			   "posorder_ordernumber = " . dbquote($form->value("project_number")) . 
			   " where posorder_ordernumber = " . dbquote($old_project_number);

		$result = mysql_query($sql) or dberror($sql);
	}

    // update record in table orders
    $value = trim($form->value("project_number")) == "" ? "null" : dbquote($form->value("project_number"));
    $order_fields[] = "order_number = " . $value;

    $value = $form->value("status");
    $order_fields[] = "order_actual_order_state_code = " . $value;

    $value = $form->value("client_address_id");
    $order_fields[] = "order_client_address = " . $value;

    $value = $form->value("client_address_user_id");
    $order_fields[] = "order_user = " . $value;

    $value = trim($form->value("preferred_transportation_arranged")) == "" ? "null" : dbquote($form->value("preferred_transportation_arranged"));
    $order_fields[] = "order_preferred_transportation_arranged = " . $value;

	$value = trim($form->value("preferred_transportation_mode")) == "" ? "null" : dbquote($form->value("preferred_transportation_mode"));
    $order_fields[] = "order_preferred_transportation_mode = " . $value;

    $value = trim($form->value("voltage")) == "" ? "null" : dbquote($form->value("voltage"));
    $order_fields[] = "order_voltage = " . $value;

    $value = dbquote(from_system_date($form->value("preferred_delivery_date")), true);
    $order_fields[] = "order_preferred_delivery_date = " . $value;


    $value = trim($form->value("full_delivery")) == "" ? "null" : dbquote($form->value("full_delivery"));
    $order_fields[] = "order_full_delivery = " . $value;

    //$value = trim($form->value("packaging_retraction")) == "" ? "null" : dbquote($form->value("packaging_retraction"));
    //$order_fields[] = "order_packaging_retraction = " . $value;

    $value = trim($form->value("pedestrian_mall_approval")) == "" ? "null" : dbquote($form->value("pedestrian_mall_approval"));
    $order_fields[] = "order_pedestrian_mall_approval = " . $value;

    $value = trim($form->value("delivery_comments")) == "" ? "null" : dbquote($form->value("delivery_comments"));
    $order_fields[] = "order_delivery_comments = " . $value;

	$value = dbquote($form->value("order_insurance"));
    $order_fields[] = "order_insurance = " . $value;

    
	// franchisee address
    /*
	$value = trim($form->value("franchisee_address_company")) == "" ? "null" : dbquote($form->value("franchisee_address_company"));
    $order_fields[] = "order_franchisee_address_company = " . $value;

     $value = trim($form->value("franchisee_address_company2")) == "" ? "null" : dbquote($form->value("franchisee_address_company2"));
    $order_fields[] = "order_franchisee_address_company2 = " . $value;

    $value = trim($form->value("franchisee_address_address")) == "" ? "null" : dbquote($form->value("franchisee_address_address"));
    $order_fields[] = "order_franchisee_address_address = " . $value;

    $value = trim($form->value("franchisee_address_address2")) == "" ? "null" : dbquote($form->value("franchisee_address_address2"));
    $order_fields[] = "order_franchisee_address_address2 = " . $value;

    $value = trim($form->value("franchisee_address_zip")) == "" ? "null" : dbquote($form->value("franchisee_address_zip"));
    $order_fields[] = "order_franchisee_address_zip = " . $value;

     $value = trim($form->value("franchisee_address_place")) == "" ? "null" : dbquote($form->value("franchisee_address_place"));
    $order_fields[] = "order_franchisee_address_place = " . $value;

    $value = trim($form->value("franchisee_address_country")) == "" ? "null" : dbquote($form->value("franchisee_address_country"));
    $order_fields[] = "order_franchisee_address_country = " . $value;

    $value = trim($form->value("franchisee_address_phone")) == "" ? "null" : dbquote($form->value("franchisee_address_phone"));
    $order_fields[] = "order_franchisee_address_phone = " . $value;

    $value = trim($form->value("franchisee_address_fax")) == "" ? "null" : dbquote($form->value("franchisee_address_fax"));
    $order_fields[] = "order_franchisee_address_fax = " . $value;

    $value = trim($form->value("franchisee_address_email")) == "" ? "null" : dbquote($form->value("franchisee_address_email"));
    $order_fields[] = "order_franchisee_address_email = " . $value;

    $value = trim($form->value("franchisee_address_contact")) == "" ? "null" : dbquote($form->value("franchisee_address_contact"));
    $order_fields[] = "order_franchisee_address_contact = " . $value;

	*/
	
	// billing address

	
	$value = trim($form->value("order_direct_invoice_address_id")) == "" ? "null" : dbquote($form->value("order_direct_invoice_address_id"));
    $order_fields[] = "order_direct_invoice_address_id = " . $value;



    $value = trim($form->value("billing_address_company")) == "" ? "null" : dbquote($form->value("billing_address_company"));
    $order_fields[] = "order_billing_address_company = " . $value;

     $value = trim($form->value("billing_address_company2")) == "" ? "null" : dbquote($form->value("billing_address_company2"));
    $order_fields[] = "order_billing_address_company2 = " . $value;

    $value = trim($form->value("billing_address_address")) == "" ? "null" : dbquote($form->value("billing_address_address"));
    $order_fields[] = "order_billing_address_address = " . $value;

    $value = trim($form->value("billing_address_address2")) == "" ? "null" : dbquote($form->value("billing_address_address2"));
    $order_fields[] = "order_billing_address_address2 = " . $value;

    $value = trim($form->value("billing_address_zip")) == "" ? "null" : dbquote($form->value("billing_address_zip"));
    $order_fields[] = "order_billing_address_zip = " . $value;

    $value = dbquote($form->value("billing_address_place_id"));
    $order_fields[] = "order_billing_address_place_id = " . $value;

	$value = trim($form->value("billing_address_place")) == "" ? "null" : dbquote($form->value("billing_address_place"));
    $order_fields[] = "order_billing_address_place = " . $value;

    $value = trim($form->value("billing_address_country")) == "" ? "null" : dbquote($form->value("billing_address_country"));
    $order_fields[] = "order_billing_address_country = " . $value;

    $value = trim($form->value("billing_address_phone")) == "" ? "null" : dbquote($form->value("billing_address_phone"));
    $order_fields[] = "order_billing_address_phone = " . $value;

    $value = trim($form->value("billing_address_fax")) == "" ? "null" : dbquote($form->value("billing_address_fax"));
    $order_fields[] = "order_billing_address_fax = " . $value;

    $value = trim($form->value("billing_address_email")) == "" ? "null" : dbquote($form->value("billing_address_email"));
    $order_fields[] = "order_billing_address_email = " . $value;

    $value = trim($form->value("billing_address_contact")) == "" ? "null" : dbquote($form->value("billing_address_contact"));
    $order_fields[] = "order_billing_address_contact = " . $value;


    $value = "current_timestamp";
    $order_fields[] = "date_modified = " . $value;

    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $order_fields[] = "user_modified = " . $value;
    }

    $sql = "update orders set " . join(", ", $order_fields) . " where order_id = " . $form->value("oid");

    mysql_query($sql) or dberror($sql);

    
   
    // update delivery address in table oder_addresses
    $value = trim($form->value("delivery_address_company")) == "" ? "null" : dbquote($form->value("delivery_address_company"));
    $delivery_address_fields[] = "order_address_company = " . $value;

     $value = trim($form->value("delivery_address_company2")) == "" ? "null" : dbquote($form->value("delivery_address_company2"));
    $delivery_address_fields[] = "order_address_company2 = " . $value;

    $value = trim($form->value("delivery_address_address")) == "" ? "null" : dbquote($form->value("delivery_address_address"));
    $delivery_address_fields[] = "order_address_address = " . $value;

    $value = trim($form->value("delivery_address_address2")) == "" ? "null" : dbquote($form->value("delivery_address_address2"));
    $delivery_address_fields[] = "order_address_address2 = " . $value;

    $value = trim($form->value("delivery_address_zip")) == "" ? "null" : dbquote($form->value("delivery_address_zip"));
    $delivery_address_fields[] = "order_address_zip = " . $value;

    $value = trim($form->value("delivery_address_place")) == "" ? "null" : dbquote($form->value("delivery_address_place"));
    $delivery_address_fields[] = "order_address_place = " . $value;

	$value = dbquote($form->value("delivery_address_place_id"));
    $delivery_address_fields[] = "order_address_place_id = " . $value;

    $value = trim($form->value("delivery_address_country")) == "" ? "null" : dbquote($form->value("delivery_address_country"));
    $delivery_address_fields[] = "order_address_country = " . $value;

    $value = trim($form->value("delivery_address_phone")) == "" ? "null" : dbquote($form->value("delivery_address_phone"));
    $delivery_address_fields[] = "order_address_phone = " . $value;

    $value = trim($form->value("delivery_address_fax")) == "" ? "null" : dbquote($form->value("delivery_address_fax"));
    $delivery_address_fields[] = "order_address_fax = " . $value;

    $value = trim($form->value("delivery_address_email")) == "" ? "null" : dbquote($form->value("delivery_address_email"));
    $delivery_address_fields[] = "order_address_email = " . $value;

    $value = $form->value("client_address_id");
    $delivery_address_fields[] = "order_address_parent = " . $value;

    $value = trim($form->value("delivery_address_contact")) == "" ? "null" : dbquote($form->value("delivery_address_contact"));
    $delivery_address_fields[] = "order_address_contact = " . $value;

    $value = "current_timestamp";
    $delivery_address_fields[] = "date_modified = " . $value;

    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $delivery_address_fields[] = "user_modified = " . $value;
    }

    $sql = "update order_addresses set " . join(", ", $delivery_address_fields) . " where order_address_type = 2 and order_address_order = " . $form->value("oid");
    mysql_query($sql) or dberror($sql);


    
    // update record in table projects
    $value = dbquote($form->value("project_number"));
    $project_fields[] = "project_number = " . $value;

    $value = trim($form->value("project_postype")) == "" ? "0" : dbquote($form->value("project_postype"));
    $project_fields[] = "project_postype = " . $value;


	$value = trim($form->value("project_pos_subclass")) == "" ? "0" : dbquote($form->value("project_pos_subclass"));
    $project_fields[] = "project_pos_subclass = " . $value;


    $value = trim($form->value("project_state")) == "" ? "1" : dbquote($form->value("project_state"));
    $project_fields[] = "project_state = " . $value;

    $value = trim($form->value("project_projectkind")) == "" ? "null" :
    dbquote($form->value("project_projectkind"));
    $project_fields[] = "project_projectkind = " . $value;

    $value = trim($form->value("product_line")) == "" ? "null" : dbquote($form->value("product_line"));
    $project_fields[] = "project_product_line = " . $value;

    $value = dbquote($form->value("location_type"));
    $project_fields[] = "project_location_type = " . $value;

    $value = trim($form->value("location_type_other")) == "" ? "null" : dbquote($form->value("location_type_other"));
    $project_fields[] = "project_location = " . $value;

    $value = trim($form->value("approximate_budget")) == "" ? "null" : dbquote($form->value("approximate_budget"));
    $project_fields[] = "project_approximate_budget = " . $value;

    $value = dbquote(from_system_date($form->value("planned_opening_date")), true);
    $project_fields[] = "project_planned_opening_date = " . $value;

    $value = trim($form->value("watches_displayed")) == "" ? "null" : dbquote($form->value("watches_displayed"));
    $project_fields[] = "project_watches_displayed = " . $value;

    $value = trim($form->value("watches_stored")) == "" ? "null" : dbquote($form->value("watches_stored"));
    $project_fields[] = "project_watches_stored = " . $value;

	$value = trim($form->value("bijoux_displayed")) == "" ? "null" : dbquote($form->value("bijoux_displayed"));
    $project_fields[] = "project_bijoux_displayed = " . $value;

    $value = trim($form->value("bijoux_stored")) == "" ? "null" : dbquote($form->value("bijoux_stored"));
    $project_fields[] = "project_bijoux_stored = " . $value;

    $value = trim($form->value("comments")) == "" ? "null" : dbquote($form->value("comments"));
    $project_fields[] = "project_comments = " . $value;

	$value = trim($form->value("furniture_height")) == "" ? "null" : dbquote($form->value("furniture_height"));
    $project_fields[] = "project_furniture_height = " . $value;

	$value = trim($form->value("project_furniture_height_mm")) == "" ? "null" : dbquote($form->value("project_furniture_height_mm"));
    $project_fields[] = "project_furniture_height_mm = " . $value;


	$value = 0;
	foreach($design_objectives_listbox_names as $key=>$field)
	{
		if($form->value($field) == 151 or $form->value($field) == 157 or $form->value($field) == 159)
		{
			$value = 1;
		}
	
	}

	$value = trim($form->value("project_is_relocation_project")) == "" ? "0" : dbquote($form->value("project_is_relocation_project"));
    $project_fields[] = "project_is_relocation_project = " . $value;

	$value = trim($form->value("project_relocated_posaddress_id")) == "" ? "0" : dbquote($form->value("project_relocated_posaddress_id"));
    $project_fields[] = "project_relocated_posaddress_id = " . $value;



    $value = "current_timestamp";
    $project_fields[] = "date_modified = " . $value;
    
    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $project_fields[] = "user_modified = " . $value;
    }

    $sql = "update projects set " . join(", ", $project_fields) . " where project_id = " . $form->value("pid");
    mysql_query($sql) or dberror($sql);

    $_REQUEST["id"] = $form->value("pid");

    // delete all records form project_items belonign to the project
    $sql = "delete from project_items where project_item_project = " . $form->value("pid");
    mysql_query($sql) or dberror($sql);

    // insert records into table project_items
    foreach ($design_objectives_listbox_names as $element)
    {
        $project_item_fields[0] = "project_item_project";
        $project_item_values[0] = $form->value("pid");

        $project_item_fields[1] = "project_item_item";
        $project_item_values[1] = $form->value($element);
        
        $project_item_fields[2] = "date_created";
        $project_item_values[2] = "current_timestamp";

        $project_item_fields[3] = "date_modified";
        $project_item_values[3] = "current_timestamp";
                
        if (isset($_SESSION["user_login"]))
        {
            $project_item_fields[4] = "user_created";
            $project_item_values[4] = dbquote($_SESSION["user_login"]);

            $project_item_fields[5] = "user_modified";
            $project_item_values[5] = dbquote($_SESSION["user_login"]);
        }
        $sql = "insert into project_items (" . join(", ", $project_item_fields) . ") values (" . join(", ", $project_item_values) . ")";
        mysql_query($sql) or dberror($sql);
    }
    

    foreach ($design_objectives_checklist_names as $name)
    {
        $tmp = $form->value($name);
        foreach ($tmp as $value)
        {
        $project_item_fields[0] = "project_item_project";
        $project_item_values[0] =$form->value("pid");

        $project_item_fields[1] = "project_item_item";
        $project_item_values[1] = $value;
        
        $project_item_fields[2] = "date_created";
        $project_item_values[2] = "current_timestamp";

        $project_item_fields[3] = "date_modified";
        $project_item_values[3] = "current_timestamp";
                
        if (isset($_SESSION["user_login"]))
        {
            $project_item_fields[4] = "user_created";
            $project_item_values[4] = dbquote($_SESSION["user_login"]);

            $project_item_fields[5] = "user_modified";
            $project_item_values[5] = dbquote($_SESSION["user_login"]);
        }
        $sql = "insert into project_items (" . join(", ", $project_item_fields) . ") values (" . join(", ", $project_item_values) . ")";
        mysql_query($sql) or dberror($sql);
        }
    }


    // update record in table project_costs
    $value = dbquote($form->value("shop_sqms"));
    $project_cost_fields[] = "project_cost_sqms = " . $value;
	
	$value = dbquote($form->value("project_cost_type"));
    $project_cost_fields[] = "project_cost_type = " . $value;

    $value = "current_timestamp";
    $project_cost_fields[] = "date_modified = " . $value;
    
    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $project_cost_fields[] = "user_modified = " . $value;
    }

    $sql = "update project_costs set " . join(", ", $project_cost_fields) . " where project_cost_order = " . $form->value("oid");
    mysql_query($sql) or dberror($sql);


	
	return true;

}


/********************************************************************
   update project retail data
*********************************************************************/
function  project_update_retail_data($form)
{

    $project_fields = array();
    $order_fields = array();

    // update record in table projects

	$value = dbquote($form->value("product_line"));
    $project_fields[] = "project_product_line = " . $value;


	$value = dbquote($form->value("product_line_subclass"));
    $project_fields[] = "project_product_line_subclass = " . $value;


    $value = dbquote($form->value("retail_coordinator"));
    $project_fields[] = "project_retail_coordinator = " . $value;

	//$value = dbquote($form->value("hq_project_manager"));
    //$project_fields[] = "project_hq_project_manager = " . $value;

	$value = dbquote($form->value("local_retail_coordinator"));
    $project_fields[] = "project_local_retail_coordinator = " . $value;
    
    
	if($form->value("project_no_planning") == 1) {
		$project_fields[] = "project_design_contractor = 0";
	}
	else
	{
		$value = dbquote($form->value("contractor_user_id"));
		$project_fields[] = "project_design_contractor = " . $value;
	}
    
    
	$value = dbquote($form->value("supervisor_user_id"));
	$project_fields[] = "project_design_supervisor = " . $value;

	$value = dbquote($form->value("cms_approver_user_id"));
    $project_fields[] = "project_cms_approver = " . $value;

    
	if($form->value("project_state") == 2) // on hold
	{
		$project_fields[] = "project_real_opening_date = NULL";
	}
	else
	{
		$value = dbquote(from_system_date($form->value("project_real_opening_date")));
		$project_fields[] = "project_real_opening_date = " . $value;
	}

	$value = dbquote($form->value("project_state"));
    $project_fields[] = "project_state = " . $value;
	
	$value = dbquote($form->value("project_is_local_production"));
    $project_fields[] = "project_is_local_production = " . $value;

	$value = dbquote($form->value("project_is_special_production"));
    $project_fields[] = "project_is_special_production = " . $value;


	$value = dbquote($form->value("project_is_relocation_project"));
    $project_fields[] = "project_is_relocation_project = " . $value;

	$value = dbquote($form->value("project_relocated_posaddress_id"));
    $project_fields[] = "project_relocated_posaddress_id = " . $value;


    $value = dbquote($form->value("project_use_ps2004"));
    $project_fields[] = "project_use_ps2004 = " . $value;

    $value = "current_timestamp";
    $project_fields[] = "date_modified = " . $value;
    
    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $project_fields[] = "user_modified = " . $value;
    }

    $sql = "update projects set " . join(", ", $project_fields) . " where project_id = " . $form->value("pid");
    mysql_query($sql) or dberror($sql);

    $_REQUEST["id"] = $form->value("pid");


    // update record in table orders

	$value = dbquote($form->value("status"));
    $order_fields[] = "order_actual_order_state_code = " . $value;
    
    $value = dbquote($form->value("retail_operator"));
    $order_fields[] = "order_retail_operator = " . $value;

    $value = dbquote($form->value("delivery_confirmation_by"));
    $order_fields[] = "order_delivery_confirmation_by = " . $value;

    $value = "current_timestamp";
    $order_fields[] = "date_modified = " . $value;
    
    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $order_fields[] = "user_modified = " . $value;
    }

    $sql = "update orders set " . join(", ", $order_fields) . " where order_id = " . $form->value("oid");
    mysql_query($sql) or dberror($sql);


	//update posorders
	$sql = "update posorders set " . 
		   "posorder_product_line = " . dbquote($form->value("product_line")) . ", " .
		   "posorder_product_line_subclass = " . dbquote($form->value("product_line_subclass")) . ", " .
	       "posorder_project_locally_produced = " . dbquote($form->value("project_is_local_production")) . ", " . 
		   "posorder_project_special_project = " . dbquote($form->value("project_is_special_production")) . 
		   " where posorder_order = " . dbquote($form->value("oid"));
	$result = mysql_query($sql) or dberror($sql);
	$sql = "update posorderspipeline set " . 
		   "posorder_product_line = " . dbquote($form->value("product_line")) . ", " .
		   "posorder_product_line_subclass = " . dbquote($form->value("product_line_subclass")) . ", " .
		   "posorder_project_locally_produced = " . dbquote($form->value("project_is_local_production")) . ", " . 
		   "posorder_project_special_project = " . dbquote($form->value("project_is_special_production")) . 
		   " where posorder_order = " . dbquote($form->value("oid"));
	$result = mysql_query($sql) or dberror($sql);


	//update posaddress
	$sql = 'select project_real_opening_date from projects where project_id = ' . $form->value("pid");
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{

	
	   if($row["project_real_opening_date"] != NULL or $row["project_real_opening_date"] != '0000-00-00')
	   {
			$realistic_opening_date = $row["project_real_opening_date"];
		
			//get the posaddress
			$sql = "select posaddress_id, posaddress_fagrstart, posaddress_store_postype " . 
				   "from posorderspipeline " . 
				   "left join posaddressespipeline on posaddress_id = posorder_posaddress " .
				   " where posorder_order = " . dbquote($form->value("oid"));
			
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$pos_id = $row["posaddress_id"];
				if($row["posaddress_store_postype"] == 1 or $row["posaddress_store_postype"] == 3)
				{
					

					//if($row["posaddress_fagrstart"] == NULL or $row["posaddress_fagrstart"] == '0000-00-00')
					//{
						$start = to_system_date($realistic_opening_date);
						$start = "01." . substr($start, 3, strlen($start)-1);


						$months = 13 - substr($realistic_opening_date, 5,2);
						$duration = "3 years and " . $months . " months";


						$end = to_system_date($realistic_opening_date);
						if(substr($end, 6, strlen($end)-1) < 10) {
							$end = "31.12.0" . (substr($end, 6, strlen($end)-1) + 3);
						}
						else
						{
							$end = "31.12." . (substr($end, 6, strlen($end)-1) + 3);
						}

						
						$sql = "update posaddressespipeline set " . 
							   "posaddress_fagrstart = " . dbquote(from_system_date($start)) . ', ' .
							   "posaddress_fagrend = " . dbquote(from_system_date($end)) . ', ' .
							   "posaddress_fargrduration = " . dbquote($duration) . 
							   " where posaddress_id = " . $pos_id;

						$result = mysql_query($sql) or dberror($sql);

						
						$sql = "update projects set " . 
							   "project_fagrstart = " . dbquote(from_system_date($start)) . ', ' .
							   "project_fagrend = " . dbquote(from_system_date($end)) .  
							   " where project_id = " . param("pid");

						

						$result = mysql_query($sql) or dberror($sql);

					//}
				}
				else
				{
					$sql = "update posaddressespipeline set " . 
							   "posaddress_fagrstart = NULL, " . 
							   "posaddress_fagrend = NULL, " . 
							   "posaddress_fargrduration = 0 " . 
							   " where posaddress_id = " . $pos_id;

						$result = mysql_query($sql) or dberror($sql);

						
						$sql = "update projects set " . 
							   "project_fagrstart = NULL, " . 
							   "project_fagrend = NULL " . 
							   " where project_id = " . param("pid");

						

						$result = mysql_query($sql) or dberror($sql);
				}
			}
	   }
	}


}


/********************************************************************
   update project delivery confirmation
*********************************************************************/
function  update_delivery_confirmation($form, $type)
{

    $order_fields = array();

    // update record in table orders
    
    $value = dbquote($form->value("quality_ok"));
    $order_fields[] = "order_quality_ok = " . $value;

    $value = dbquote($form->value("quantity_ok"));
    $order_fields[] = "order_quantity_ok = " . $value;

    $value = dbquote($form->value("packing_list_ok"));
    $order_fields[] = "order_packing_list_ok = " . $value;

    $value = "current_timestamp";
    $order_fields[] = "order_confirmation_date = " . $value;

    $order_fields[] = "date_modified = " . $value;
    
    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $order_fields[] = "user_modified = " . $value;
    }

    $sql = "update orders set " . join(", ", $order_fields) . " where order_id = " . $form->value("oid");
    mysql_query($sql) or dberror($sql);

    append_order_state($form->value("oid"), "800", $type, 1);

}

/********************************************************************
   insert, update or delete project order items (material list)
*********************************************************************/
function  project_save_order_items($list, $type)
{
	foreach($list as $key=>$value)
    {
        if($key == "hidden" and isset($value["pline"]))
        {
            $pline = $value["pline"];
        }
        if($key == "hidden" and isset($value["cid"]))
        {
            $cid_at_start = $value["cid"];
            
        }
		
    }

    foreach ($list->values("item_entry_field") as $key=>$value)
    {

        
		//check if item is already there
		$sql = "select order_item_category " .
			   "from order_items " .
			   "where order_item_item = " . $key  . 
			   " and order_item_order = " . param("oid");

		$res = mysql_query($sql) or dberror($sql);
        if($row = mysql_fetch_assoc($res))
		{
			$cid = $row["order_item_category"];	
		}
		else
		{
			if(isset($cid_at_start) and isset($pline))
			{
				
				$sql = "select category_item_category " .
					   "from items " .
					   "left join category_items on category_item_item = " . $key . " " .
					   "left join categories on category_id = category_item_category " .
					   "where category_product_line = " . $pline . 
					   "   and item_id = " . $key . 
					   "   and category_item_category = " . $cid_at_start;

			}
			elseif(isset($pline))
			{
				$sql = "select category_item_category " .
					   "from items " .
					   "left join category_items on category_item_item = " . $key . " " .
					   "left join categories on category_id = category_item_category " .
					   "where category_product_line = " . $pline . 
					   "   and item_id = " . $key;
			}
			else //orders only
			{
				$sql = "select category_item_category " .
					   "from items " .
					   "left join category_items on category_item_item = " . $key . " " .
					   "where item_id = " . $key;

			}

			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);

			$cid = $row["category_item_category"];
		}
        
        
        if(!$cid)
        {
            $cid = 0;

            // chek if item is new or already in order_items
            $sql_order_item = "select order_item_item ".
                          "from order_items ".
                          "where (order_item_category = " . $cid .
                          "    or order_item_category is null) " .
                          "    and order_item_item = " . $key .
                          "    and order_item_order = " . param("oid");
        }
        else
        {
			// chek if item is new or already in order_items
            $sql_order_item = "select order_item_item ".
                          "from order_items ".
                          "where order_item_category = " . $cid .
                          "    and order_item_item = " . $key .
                          "    and order_item_order = " . param("oid");

        }


		        
        $res = mysql_query($sql_order_item) or dberror($sql_order_item);
        if ($row = mysql_fetch_assoc($res))
        {
			
			// update record in table order_item
            if ($value >0)
            {
                $order_item_fields = array();

                if ($type == ITEM_TYPE_COST_ESTIMATION)
                {
                    $order_item_fields[] = "order_item_system_price = " . $value;
                    $order_item_fields[] = "order_item_quantity = 1";
                }
                elseif ($type == ITEM_TYPE_LOCALCONSTRUCTIONCOST)
                {
                    $order_item_fields[] = "order_item_system_price = " . $value;
                    $order_item_fields[] = "order_item_quantity = 1";
                }
                else
                {
                    $order_item_fields[] = "order_item_quantity = " . $value;
                }

                $value1 = "current_timestamp";
                $project_fields[] = "date_modified = " . $value1;
    
                if (isset($_SESSION["user_login"]))
                {
                    $value1 = dbquote($_SESSION["user_login"]);
                    $project_fields[] = "user_modified = " . $value1;
                }

                $order_item_fields[] = "order_item_category = " . $cid;

       
                $sql = "update order_items set " . join(", ", $order_item_fields) . " " .
                       "where order_item_item = " . $key .
                       "    and order_item_order = " . param("oid");

                mysql_query($sql) or dberror($sql);
            }

            // delete record in table order_item
            if ($value == 0)
            {
                $sql = "delete from order_items " .
                       "where order_item_category = " . $cid .
                       "   and order_item_item = " . $key .
                       "    and order_item_order = " . param("oid");
                mysql_query($sql) or dberror($sql);
            }
        }
        else
        {
			// insert new records into table order_items
            if ($value >0)
            {
                // get item_category (first of any)
                $category = 0;
                $sql = "select * ".
                       "from category_items ".
                       "where category_item_category = " . $cid . 
                       "   and category_item_item = " . $key;

                $res1 = mysql_query($sql) or dberror($sql);

                if ($row = mysql_fetch_assoc($res1))
                {
                    $category = $row["category_item_category"];
                }


				//get item_type
				$sql = "select item_type from items where item_id = " . $key;
				$res1 = mysql_query($sql) or dberror($sql);

                if ($row = mysql_fetch_assoc($res1))
                {
                    $type = $row["item_type"];
                }


                
                $order_item_fields = array();
                $order_item_values = array();
    
                $order_item_fields[] = "order_item_order";
                $order_item_values[] = param("oid");

                $order_item_fields[] = "order_item_item";
                $order_item_values[] = $key;

                $order_item_fields[] = "order_item_type";
                $order_item_values[] = $type;


                if ($type == ITEM_TYPE_COST_ESTIMATION)
                {
                    $order_item_fields[] = "order_item_quantity";
                    $order_item_values[] = 1;
                }

                if ($type == ITEM_TYPE_LOCALCONSTRUCTIONCOST)
                {
                    $order_item_fields[] = "order_item_quantity";
                    $order_item_values[] = 1;
                }

                if ($type == ITEM_TYPE_STANDARD or $type == ITEM_TYPE_SPECIAL or $type == ITEM_TYPE_SERVICES)
                {
					$order_item_fields[] = "order_item_quantity";
                    $order_item_values[] = $value;

                    $order_item_fields[] = "order_item_category";
                    $order_item_values[] = $category;
                

                    // get supplier's address id (first record in case of several)
                    $sql = "select address_id, address_company, supplier_item_price, supplier_item_currency ".
                           "from items ".
                           "left join suppliers on item_id = supplier_item ".
                           "left join addresses on supplier_address = address_id ".
                           "where item_id = " . $key;
                
                    $res1 = mysql_query($sql) or dberror($sql);

                    if ($row1 = mysql_fetch_assoc($res1))
                    {
                        if ($row1["address_id"])
                        {
                            
                            if ($row1["supplier_item_currency"])
                            {
                                $currency = get_currency($row1["supplier_item_currency"]);
                            }
                            else
                            {
                                $currency = get_address_currency($row1["address_id"]);
                            }
                            
                            $order_item_fields[] = "order_item_supplier_address";
                            $order_item_values[] = $row1["address_id"];

                            // get suppliers's currency information
                            $supplier_currency = get_address_currency($row1["address_id"]);
    
                            $order_item_fields[] = "order_item_supplier_currency";
                            $order_item_values[] = $currency["id"];
    
                            $order_item_fields[] = "order_item_supplier_exchange_rate";
                            $order_item_values[] = $currency["exchange_rate"];

                            $order_item_fields[] = "order_item_supplier_price";
                            $order_item_values[] = $row1["supplier_item_price"];
                        }
                    }
                }

                // get orders's currency
                $currency = get_order_currency(param("oid"));

                // get item data

                $sql = "select * ".
                       "from items ".
                       "left join category_items on item_id = category_item_item ".
                       "left join categories on category_item_category = category_id ".
                       "where item_id = " . $key;

                $res1 = mysql_query($sql) or dberror($sql);

                if ($row = mysql_fetch_assoc($res1))
                {
                    $order_item_fields[] = "order_item_text";
                    $order_item_values[] = trim($row["item_name"]) == "" ? "null" : dbquote($row["item_name"]);

                    $order_item_fields[] = "order_item_system_price";
                    if ($type == ITEM_TYPE_COST_ESTIMATION)
                    {
                        $order_item_values[] = $value;
                        $client_price = $value / $currency["exchange_rate"] * $currency["factor"];
                    }
                    elseif ($type == ITEM_TYPE_LOCALCONSTRUCTIONCOST)
                    {
                        $order_item_values[] = $value;
                        $client_price = $value / $currency["exchange_rate"] * $currency["factor"];
                    }
                    else
                    {
                        $order_item_values[] = trim($row["item_price"]) == "" ? "null" : dbquote($row["item_price"]);
                        $client_price = $row["item_price"] / $currency["exchange_rate"] * $currency["factor"];
                    }


                    $order_item_fields[] = "order_item_client_price";
                    $order_item_values[] = trim($client_price) == "" ? "null" : dbquote($client_price);

                    if (!param("pid"))
                    {
                        $order_item_fields[] = "order_item_cost_unit_number";
                        $order_item_values[] = trim($row["category_cost_unit_number"]) == "" ? "null" : dbquote($row["category_cost_unit_number"]);
                    }

                    $order_item_fields[] = "order_items_costmonitoring_group";
                    $order_item_values[] = dbquote($row["item_costmonitoring_group"]);

                }

                $order_item_fields[] = "order_item_no_offer_required";
                $order_item_values[] = 1;


                if ($type > 0)
                {
                    $order_item_fields[] = "order_item_cost_group";
					$order_item_values[] = $row["item_cost_group"];
                }
                

                $order_item_fields[] = "date_created";
                $order_item_values[] = "current_timestamp";

                $order_item_fields[] = "date_modified";
                $order_item_values[] = "current_timestamp";
                
                if (isset($_SESSION["user_login"]))
                {
                    $order_item_fields[] = "user_created";
                    $order_item_values[] = dbquote($_SESSION["user_login"]);

                    $order_item_fields[] = "user_modified";
                    $order_item_values[] = dbquote($_SESSION["user_login"]);
                }
                $sql = "insert into order_items (" . join(", ", $order_item_fields) . ") values (" . join(", ", $order_item_values) . ")";
                mysql_query($sql) or dberror($sql);

                $item_id = mysql_insert_id();

                if ($type == ITEM_TYPE_STANDARD or $type == ITEM_TYPE_SPECIAL)
                {
					// copy delivery address for new item in table order_addresses
                    $sql = "select * from order_addresses ".
                           "where order_address_type = 2 ".
                           "    and order_address_order = ". param("oid") .
                           "    and (order_address_order_item = 0 ".
                           "    or order_address_order_item is null) ";

                    $res2 = mysql_query($sql) or dberror($sql);
                    $row2 = mysql_fetch_assoc($res2);

                    $delivery_address_fields = array();
                    $delivery_address_values = array();

                    $delivery_address_fields[] = "order_address_order";
                    $delivery_address_values[] = param("oid");

                    $delivery_address_fields[] = "order_address_order_item";
                    $delivery_address_values[] = $item_id;

                    $delivery_address_fields[] = "order_address_type";
                    $delivery_address_values[] = 2;

                    $delivery_address_fields[] = "order_address_company";
                    $delivery_address_values[] = dbquote($row2["order_address_company"]);

                    $delivery_address_fields[] = "order_address_company2";
                    $delivery_address_values[] = dbquote($row2["order_address_company2"]);

                    $delivery_address_fields[] = "order_address_address";
                    $delivery_address_values[] = dbquote($row2["order_address_address"]);

                    $delivery_address_fields[] = "order_address_address2";
                    $delivery_address_values[] = dbquote($row2["order_address_address2"]);

                    $delivery_address_fields[] = "order_address_zip";
                    $delivery_address_values[] = dbquote($row2["order_address_zip"]);

                    $delivery_address_fields[] = "order_address_place";
                    $delivery_address_values[] = dbquote($row2["order_address_place"]);

					$delivery_address_fields[] = "order_address_place_id";
                    $delivery_address_values[] = dbquote($row2["order_address_place_id"]);

                    $delivery_address_fields[] = "order_address_country";
                    $delivery_address_values[] = dbquote($row2["order_address_country"]);

                    $delivery_address_fields[] = "order_address_phone";
                    $delivery_address_values[] = dbquote($row2["order_address_phone"]);

                    $delivery_address_fields[] = "order_address_fax";
                    $delivery_address_values[] = dbquote($row2["order_address_fax"]);

                    $delivery_address_fields[] = "order_address_email";
                    $delivery_address_values[] = dbquote($row2["order_address_email"]);

                    $delivery_address_fields[] = "order_address_parent";
                    $delivery_address_values[] = $row2["order_address_parent"];

                    $delivery_address_fields[] = "order_address_contact";
                    $delivery_address_values[] = dbquote($row2["order_address_contact"]);

                    $delivery_address_fields[] = "date_created";
                    $delivery_address_values[] = "current_timestamp";

                    $delivery_address_fields[] = "date_modified";
                    $delivery_address_values[] = "current_timestamp";

                    if (isset($_SESSION["user_login"]))
                    {
                        $delivery_address_fields[] = "user_created";
                        $delivery_address_values[] = dbquote($_SESSION["user_login"]);

                        $delivery_address_fields[] = "user_modified";
                        $delivery_address_values[] = dbquote($_SESSION["user_login"]);
                    }

                    $sql = "insert into order_addresses (" . join(", ", $delivery_address_fields) . ") values (" . join(", ", $delivery_address_values) . ")";
                    mysql_query($sql) or dberror($sql);
                
                }

            }
        }
    }


}

/********************************************************************
   update or delete project order items (material list)
*********************************************************************/
function  project_update_order_items($list)
{
	$suppliers = array();
	foreach($list->columns as $key=>$column)
	{
		if($column["name"] == "item_suppliers")
		{
			$suppliers = $list->values("item_suppliers");
		}
	}

	foreach ($list->values("item_entry_field") as $key=>$value)
    {
		// update record
        if ($value >0)
        {
            $order_item_fields = array();
    
            $order_item_fields[] = "order_item_quantity = " . $value;

			if(count($suppliers) > 0)
			{
				$order_item_fields[] = "order_item_supplier_address = " . dbquote($suppliers[$key]);
			}

            $value1 = "current_timestamp";
            $project_fields[] = "date_modified = " . $value1;
    
            if (isset($_SESSION["user_login"]))
            {
                $value1 = dbquote($_SESSION["user_login"]);
                $project_fields[] = "user_modified = " . $value1;
            }
       
            $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
            mysql_query($sql) or dberror($sql);

        }

        // delete record in table order_item
        if ($value == 0)
        {
            delete_record("order_items", $key, true);
        }
    }

	return true;

}



/********************************************************************
   update cost monitoring information for items
*********************************************************************/
function  project_update_order_item_cost1($list, $nocom = true)
{
    foreach ($list->values("order_item_cost_group") as $key=>$value)
    {
        // update record
        if(!$value) {$value = 0;}
        $order_item_fields = array();

        $order_item_fields[] = "order_item_cost_group = " . $value;

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);
    }


    foreach ($list->values("order_item_quantity") as $key=>$value)
    {
        // update record
        if(!$value) {$value = 0;}
        $order_item_fields = array();

        $order_item_fields[] = "order_item_quantity = " . $value;

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);

    }


    foreach ($list->values("order_item_real_system_price") as $key=>$value)
    {
        // update record
        if(!$value) {$value = 0;}
    
        $order_item_fields = array();

        if($value > 0) {
			$order_item_fields[] = "order_item_system_price = " . $value;
		}

		$order_item_fields[] = "order_item_real_system_price = " . $value;

        $value1 = "current_timestamp";
        $project_fields[] = "date_modified = " . $value1;

        if (isset($_SESSION["user_login"]))
        {
            $value1 = dbquote($_SESSION["user_login"]);
            $project_fields[] = "user_modified = " . $value1;
        }
   
        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);
    }

    /*
    if($project["order_date"] > "2005-10-31")
    {
        //update budget approved
        if(isset($list["order_item_quantity_freezed"]))
        {
            foreach ($list->values("order_item_quantity_freezed") as $key=>$value)
            {
                // update record
                if(!$value) {$value = 0;}
                $order_item_fields = array();

                $order_item_fields[] = "order_item_quantity_freezed = " . $value;

                $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
                mysql_query($sql) or dberror($sql);
            }

            foreach ($list->values("order_item_system_price_freezed") as $key=>$value)
            {
                // update record
                if(!$value) {$value = 0;}
            
                $order_item_fields = array();

                $order_item_fields[] = "order_item_system_price_freezed = " . $value;

                $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
                mysql_query($sql) or dberror($sql);
            }
        }
    }
	*/
	
	if($nocom == true)
	{
		foreach ($list->values("order_items_costmonitoring_group") as $key=>$value)
		{
			// update record
			if(!$value) {$value = 0;}
		
			$order_item_fields = array();

			$order_item_fields[] = "order_items_costmonitoring_group = " . $value;

			$value1 = "current_timestamp";
			$project_fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = dbquote($_SESSION["user_login"]);
				$project_fields[] = "user_modified = " . $value1;
			}
	   
			$sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
			mysql_query($sql) or dberror($sql);
		}
	}
}


/********************************************************************
   update cost monitoring information for otehr cost
*********************************************************************/
function  project_update_order_item_cost2($list)
{
    
    foreach ($list->values("order_item_cost_group") as $key=>$value)
    {
        // update record
        if(!$value) {$value = 0;}
        $order_item_fields = array();

        $order_item_fields[] = "order_item_cost_group = " . $value;

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);
    }

    /*
	foreach ($list->values("order_item_supplier_freetext") as $key=>$value)
    {
        // update record

        $order_item_fields = array();

        $order_item_fields[] = "order_item_supplier_freetext = " . dbquote($value);

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);

    }
	*/

    foreach ($list->values("order_item_real_system_price") as $key=>$value)
    {
        // update record
        if(!$value) {$value = 0;}

        $order_item_fields = array();

        if($value > 0) {
			$order_item_fields[] = "order_item_system_price = " . $value;
		}

		$order_item_fields[] = "order_item_real_system_price = " . $value;

        $value1 = "current_timestamp";
        $project_fields[] = "date_modified = " . $value1;

        if (isset($_SESSION["user_login"]))
        {
            $value1 = dbquote($_SESSION["user_login"]);
            $project_fields[] = "user_modified = " . $value1;
        }
   
        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);

    }


	/*
	foreach ($list->values("order_item_system_price_freezed") as $key=>$value)
    {
        // update record
        if(!$value) {$value = 0;}

        $order_item_fields = array();

		$order_item_fields[] = "order_item_system_price_freezed = " . $value;
		$order_item_fields[] = "order_item_client_price_freezed = " . $value;

        $value1 = "current_timestamp";
        $project_fields[] = "date_modified = " . $value1;

        if (isset($_SESSION["user_login"]))
        {
            $value1 = dbquote($_SESSION["user_login"]);
            $project_fields[] = "user_modified = " . $value1;
        }
   
        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);

    }
	*/

    /*
	if($project["order_date"] > "2005-10-31")
    {
        //budget approved
        foreach ($list->values("order_item_system_price_freezed") as $key=>$value)
        {
            // update record
            if(!$value) {$value = 0;}

            $order_item_fields = array();

            $order_item_fields[] = "order_item_system_price_freezed = " . $value;

            $value1 = "current_timestamp";
            $project_fields[] = "date_modified = " . $value1;

            if (isset($_SESSION["user_login"]))
            {
                $value1 = dbquote($_SESSION["user_login"]);
                $project_fields[] = "user_modified = " . $value1;
            }
       
            $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
            mysql_query($sql) or dberror($sql);
        }
    }
	*/
}



/********************************************************************
   update cost monitoring information for items: reinvoice data
*********************************************************************/
function  project_update_order_item_reinvoice_data_0($list)
{
    
    $pos = array();
    $sups = array();

    $sql = $list->sql;

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $pos[] = $row["order_item_po_number"];
        $sups[] = $row["order_item_supplier_address"];

    }
    
    $i = 0;
    foreach ($list->values("order_item_reinvoiced") as $key=>$value)
    {
           
           if($i < count($pos))
           {
               // update record
                $order_item_fields = array();
                if($value)
                {
                    $order_item_fields[] = "order_item_reinvoiced = " . dbquote(from_system_date($value));

                    $sql = "update order_items set " . join(", ", $order_item_fields) . 
                " where order_item_po_number = " . dbquote($pos[$i]) . 
                " and order_item_supplier_address = " . dbquote($sups[$i]);
                mysql_query($sql) or dberror($sql);
                }

           }
            $i++;
    }

    $i = 0;
    foreach ($list->values("order_item_reinvoicenbr") as $key=>$value)
    {
           
           if($i < count($pos))
           {
               // update record
                $order_item_fields = array();

                $order_item_fields[] = "order_item_reinvoicenbr = " . dbquote(trim($value));

                $sql = "update order_items set " . join(", ", $order_item_fields) . 
                " where order_item_po_number = " . dbquote($pos[$i]) . 
                " and order_item_supplier_address = " . dbquote($sups[$i]);
                mysql_query($sql) or dberror($sql);
           }
            $i++;
    }


	$i = 0;
    foreach ($list->values("order_item_reinvoiced2") as $key=>$value)
    {
           
           if($i < count($pos))
           {
               // update record
                $order_item_fields = array();
                if($value)
                {
                    $order_item_fields[] = "order_item_reinvoiced2 = " . dbquote(from_system_date($value));

                    $sql = "update order_items set " . join(", ", $order_item_fields) . 
                " where order_item_po_number = " . dbquote($pos[$i]) . 
                " and order_item_supplier_address = " . dbquote($sups[$i]);
                mysql_query($sql) or dberror($sql);
                }

           }
            $i++;
    }

    $i = 0;
    foreach ($list->values("order_item_reinvoicenbr2") as $key=>$value)
    {
           
           if($i < count($pos))
           {
               // update record
                $order_item_fields = array();

                $order_item_fields[] = "order_item_reinvoicenbr2 = " . dbquote(trim($value));

                $sql = "update order_items set " . join(", ", $order_item_fields) . 
                " where order_item_po_number = " . dbquote($pos[$i]) . 
                " and order_item_supplier_address = " . dbquote($sups[$i]);
                mysql_query($sql) or dberror($sql);
           }
            $i++;
    }
}


/********************************************************************
   update cost monitoring information for items: reinvoice data
*********************************************************************/
function  project_update_order_item_reinvoice_data($list)
{
    
    foreach ($list->values("order_item_reinvoiced") as $key=>$value)
    {
        // update record
        
            $order_item_fields = array();

            if($value)
            {
                $order_item_fields[] = "order_item_reinvoiced = " . dbquote(from_system_date($value));

                $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
                mysql_query($sql) or dberror($sql);
            }

            

    }

    foreach ($list->values("order_item_reinvoicenbr") as $key=>$value)
    {
        // update record
        $order_item_fields = array();

        $order_item_fields[] = "order_item_reinvoicenbr = " . dbquote(trim($value));

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);
    }

	foreach ($list->values("order_item_reinvoiced2") as $key=>$value)
    {
        // update record
        
            $order_item_fields = array();

            if($value)
            {
                $order_item_fields[] = "order_item_reinvoiced2 = " . dbquote(from_system_date($value));

                $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
                mysql_query($sql) or dberror($sql);
            }

            

    }

    foreach ($list->values("order_item_reinvoicenbr2") as $key=>$value)
    {
        // update record
        $order_item_fields = array();

        $order_item_fields[] = "order_item_reinvoicenbr2 = " . dbquote(trim($value));

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);
    }

}

/********************************************************************
   update cost monitoring information for items: cost group data
*********************************************************************/
function  project_update_order_item_group_data($list, $order_id)
{

    foreach ($list->values("order_items_monitoring_group") as $key=>$value)
    {

        // update record
        $order_item_fields = array();

        if($value)
		{
			$order_item_fields[] = "order_items_monitoring_group = " . dbquote($value);
		}
		else
		{
			$order_item_fields[] = "order_items_monitoring_group = NULL";
		}

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);
    }

    // update all empty fields with th entry the user made for the sam supplier

    $sql = "select distinct order_item_supplier_address, " .
           "order_items_monitoring_group  " .
           "from order_items " .
           "where order_item_order = " . $order_id .
           "   and order_items_monitoring_group <>'' ";

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $monitoring_group = $row["order_items_monitoring_group"];
        $supplier = $row["order_item_supplier_address"];

        $order_item_fields = array();

        $order_item_fields[] = "order_items_monitoring_group = " . dbquote($monitoring_group);

        $sql = "update order_items set " . join(", ", $order_item_fields) . 
        "  where order_item_order = " . $order_id .
        "   and order_item_supplier_address = " . $supplier . 
        "   and (order_items_monitoring_group  = '' or order_items_monitoring_group is null)";


        mysql_query($sql) or dberror($sql);
    }

}

/***********************************************************************
   insert special item or cost estimation record
************************************************************************/
function  project_add_special_item_save($form)
{

        $order_item_fields = array();
        $order_item_values = array();

        $order_item_fields[] = "order_item_order";
        $order_item_values[] = trim($form->value("order_item_order")) == "" ? "null" : dbquote($form->value("order_item_order"));

        $order_item_fields[] = "order_item_type";
        $order_item_values[] = trim($form->value("order_item_type")) == "" ? "null" : dbquote($form->value("order_item_type"));

        $order_item_fields[] = "order_item_text";
        $order_item_values[] = trim($form->value("order_item_text")) == "" ? "null" : dbquote($form->value("order_item_text"));

        if ($form->value("order_item_type") == ITEM_TYPE_SPECIAL)
        {
            $order_item_fields[] = "order_item_quantity";
            $order_item_values[] = trim($form->value("order_item_quantity")) == "" ? "null" : dbquote($form->value("order_item_quantity"));

            $order_item_fields[] = "order_item_supplier_address";
            $order_item_values[] = trim($form->value("order_item_supplier_address")) == "" ? "null" : dbquote($form->value("order_item_supplier_address"));

			
			$order_item_fields[] = "order_item_no_offer_required";
            $order_item_values[] = trim($form->value("order_item_no_offer_required")) == "" ? "null" : dbquote($form->value("order_item_no_offer_required"));

            $order_item_fields[] = "order_item_only_quantity_proposal";
            $order_item_values[] = trim($form->value("order_item_only_quantity_proposal")) == "" ? "null" : dbquote($form->value("order_item_only_quantity_proposal"));
			
			
			// get suppliers's currency information
            $supplier_currency = get_address_currency($form->value("order_item_supplier_address"));

            $order_item_fields[] = "order_item_supplier_currency";
            $order_item_values[] = $supplier_currency["id"];

            $order_item_fields[] = "order_item_supplier_exchange_rate";
            $order_item_values[] = $supplier_currency["exchange_rate"];

			if(isset($form->items["order_item_cost_group"]))
			{
				$order_item_fields[] = "order_item_cost_group";
				$order_item_values[] =$form->value("order_item_cost_group");

				$order_item_fields[] = "order_items_costmonitoring_group";
				$order_item_values[] =$form->value("order_items_costmonitoring_group");

				if(isset($form->items["order_item_exclude_from_ps"]))
				{
					$order_item_fields[] = "order_item_exclude_from_ps";
					$order_item_values[] = $form->value("order_item_exclude_from_ps");
				}
			}

        }

        if ($form->value("order_item_type") == ITEM_TYPE_COST_ESTIMATION)
        {
            if(isset($form->items["order_item_supplier_freetext"]))
			{
				$order_item_fields[] = "order_item_supplier_freetext";
				$order_item_values[] = dbquote($form->value("order_item_supplier_freetext"));
			}

			if(isset($form->items["order_item_cost_group"]))
			{
				$order_item_fields[] = "order_item_cost_group";
				$order_item_values[] =$form->value("order_item_cost_group");

			}
			$order_item_fields[] = "order_item_system_price";
            $order_item_values[] =$form->value("order_item_system_price");

            $order_item_fields[] = "order_item_quantity";
            $order_item_values[] = 1;
 
        }

        if ($form->value("order_item_type") == ITEM_TYPE_LOCALCONSTRUCTIONCOST)
        {
            if(isset($form->items["order_item_supplier_freetext"]))
			{
				$order_item_fields[] = "order_item_supplier_freetext";
				$order_item_values[] = dbquote($form->value("order_item_supplier_freetext"));
			}

            $order_item_fields[] = "order_item_system_price";
            $order_item_values[] =$form->value("order_item_system_price");

            if(isset($form->items["order_item_cost_group"]))
			{
				$order_item_fields[] = "order_item_cost_group";
				$order_item_values[] = $form->value("order_item_cost_group");

				$order_item_fields[] = "order_item_quantity";
				$order_item_values[] = 1;
			}
 
        }


        // get orders's currency
        $currency = get_order_currency($form->value("order_item_order"));

        $client_price = $form->value("order_item_system_price") / $currency["exchange_rate"] * $currency["factor"];

        $order_item_fields[] = "order_item_client_price";
        $order_item_values[] = trim($client_price) == "" ? "null" : dbquote($client_price);

        $order_item_fields[] = "date_created";
        $order_item_values[] = "current_timestamp";

        $order_item_fields[] = "date_modified";
        $order_item_values[] = "current_timestamp";
                
        if (isset($_SESSION["user_login"]))
        {
            $order_item_fields[] = "user_created";
            $order_item_values[] = dbquote($_SESSION["user_login"]);

            $order_item_fields[] = "user_modified";
            $order_item_values[] = dbquote($_SESSION["user_login"]);
        }
        $sql = "insert into order_items (" . join(", ", $order_item_fields) . ") values (" . join(", ", $order_item_values) . ")";
        mysql_query($sql) or dberror($sql);

        $item_id = mysql_insert_id();

        if ($form->value("order_item_type") == ITEM_TYPE_SPECIAL)
        {
            // copy delivery address for new item in table order_addresses
            $sql = "select * from order_addresses ".
                   "where order_address_type = 2 ".
                   "    and order_address_order = ". param("oid") .
                   "    and (order_address_order_item = 0 ".
                   "    or order_address_order_item is null) ";

            $res2 = mysql_query($sql) or dberror($sql);
            $row2 = mysql_fetch_assoc($res2);

            $delivery_address_fields = array();
            $delivery_address_values = array();

            $delivery_address_fields[] = "order_address_order";
            $delivery_address_values[] = $form->value("order_item_order");

            $delivery_address_fields[] = "order_address_order_item";
            $delivery_address_values[] = $item_id;

            $delivery_address_fields[] = "order_address_type";
            $delivery_address_values[] = 2;

            $delivery_address_fields[] = "order_address_company";
            $delivery_address_values[] = dbquote($row2["order_address_company"]);

            $delivery_address_fields[] = "order_address_company2";
            $delivery_address_values[] = dbquote($row2["order_address_company2"]);

            $delivery_address_fields[] = "order_address_address";
            $delivery_address_values[] = dbquote($row2["order_address_address"]);

            $delivery_address_fields[] = "order_address_address2";
            $delivery_address_values[] = dbquote($row2["order_address_address2"]);

            $delivery_address_fields[] = "order_address_zip";
            $delivery_address_values[] = dbquote($row2["order_address_zip"]);

            $delivery_address_fields[] = "order_address_place";
            $delivery_address_values[] = dbquote($row2["order_address_place"]);

            $delivery_address_fields[] = "order_address_country";
            $delivery_address_values[] = dbquote($row2["order_address_country"]);

            $delivery_address_fields[] = "order_address_phone";
            $delivery_address_values[] = dbquote($row2["order_address_phone"]);

            $delivery_address_fields[] = "order_address_fax";
            $delivery_address_values[] = dbquote($row2["order_address_fax"]);

            $delivery_address_fields[] = "order_address_email";
            $delivery_address_values[] = dbquote($row2["order_address_email"]);

            $delivery_address_fields[] = "order_address_parent";
            $delivery_address_values[] = $row2["order_address_parent"];

            $delivery_address_fields[] = "order_address_contact";
            $delivery_address_values[] = dbquote($row2["order_address_contact"]);

            $delivery_address_fields[] = "date_created";
            $delivery_address_values[] = "current_timestamp";

            $delivery_address_fields[] = "date_modified";
            $delivery_address_values[] = "current_timestamp";

            if (isset($_SESSION["user_login"]))
            {
                $delivery_address_fields[] = "user_created";
                $delivery_address_values[] = dbquote($_SESSION["user_login"]);

                $delivery_address_fields[] = "user_modified";
                $delivery_address_values[] = dbquote($_SESSION["user_login"]);
            }

            $sql = "insert into order_addresses (" . join(", ", $delivery_address_fields) . ") values (" . join(", ", $delivery_address_values) . ")";
            mysql_query($sql) or dberror($sql);
        }


}



/***********************************************************************
   insert special item entered by a supplier
************************************************************************/
function  project_add_special_item_save_supplier_data($form)
{

        $order_item_fields = array();
        $order_item_values = array();

        $order_item_fields[] = "order_item_order";
        $order_item_values[] = trim($form->value("order_item_order")) == "" ? "null" : dbquote($form->value("order_item_order"));

        $order_item_fields[] = "order_item_type";
        $order_item_values[] = trim($form->value("order_item_type")) == "" ? "null" : dbquote($form->value("order_item_type"));

        $order_item_fields[] = "order_item_text";
        $order_item_values[] = trim($form->value("order_item_text")) == "" ? "null" : dbquote($form->value("order_item_text"));

        $order_item_fields[] = "order_item_quantity";
        $order_item_values[] = trim($form->value("order_item_quantity")) == "" ? "null" : dbquote($form->value("order_item_quantity"));

        $order_item_fields[] = "order_item_supplier_address";
        $order_item_values[] = trim($form->value("order_item_supplier_address")) == "" ? "null" : dbquote($form->value("order_item_supplier_address"));

        $order_item_fields[] = "order_item_supplier_price";
        $order_item_values[] =  trim($form->value("order_item_supplier_price")) == "" ? "null" : dbquote($form->value("order_item_supplier_price"));


        // get suppliers currency

        $supplier_currency_id = null;
        $supplier_currency_exchangerate = null;

        $supplier_currency = get_address_currency($form->value("order_item_supplier_address"));
        $supplier_currency_id = $supplier_currency["id"];
        $supplier_currency_exchangerate = $supplier_currency["exchange_rate"];
        $supplier_currency_factor = $supplier_currency["factor"];

        $order_item_fields[] = "order_item_supplier_currency";
        $order_item_values[] = $supplier_currency_id;
        $order_item_fields[] = "order_item_supplier_exchange_rate";
        $order_item_values[] = $supplier_currency_exchangerate;

        // get orders currencies (system and client)
        $oid = 0;
        if ($form->value("oid"))
        {
            $oid = $form->value("oid");
        }

        $sql = "select order_client_address, order_client_currency, order_system_currency, " .
               "    order_client_exchange_rate, order_system_exchange_rate " .
               "    from orders " .
               "    where order_id = " . $oid;

         $res = mysql_query($sql) or dberror($sql);
         if ($row = mysql_fetch_assoc($res))
         {
             $supplier_price = $form->value("order_item_supplier_price");

             // set sytsem price
             $system_price = $supplier_price * $supplier_currency_exchangerate / $supplier_currency_factor;
             $order_item_fields[] = "order_item_system_price";
             $order_item_values[] = $system_price;

             // set client's price
             $client_currency = get_address_currency($row["order_client_address"]);
             $value = $system_price / $row["order_client_exchange_rate"] * $client_currency["factor"];
             $order_item_fields[] = "order_item_client_price";
             $order_item_values[] = $value;
         }
 
        $order_item_fields[] = "order_item_supplier_item_code";
        $order_item_values[] =  trim($form->value("order_item_supplier_item_code")) == "" ? "null" : dbquote($form->value("order_item_supplier_item_code"));
        
        $order_item_fields[] = "order_item_offer_number";
        $order_item_values[] =  trim($form->value("order_item_offer_number")) == "" ? "null" : dbquote($form->value("order_item_offer_number"));

        $order_item_fields[] = "order_item_production_time";
        $order_item_values[] =  trim($form->value("order_item_production_time")) == "" ? "null" : dbquote($form->value("order_item_production_time"));

        $order_item_fields[] = "order_item_cost_group";
        $order_item_values[] = 2;
        
        $order_item_fields[] = "date_created";
        $order_item_values[] = "current_timestamp";

        $order_item_fields[] = "date_modified";
        $order_item_values[] = "current_timestamp";
                
        if (isset($_SESSION["user_login"]))
        {
            $order_item_fields[] = "user_created";
            $order_item_values[] = dbquote($_SESSION["user_login"]);

            $order_item_fields[] = "user_modified";
            $order_item_values[] = dbquote($_SESSION["user_login"]);
        }
        $sql = "insert into order_items (" . join(", ", $order_item_fields) . ") values (" . join(", ", $order_item_values) . ")";
        mysql_query($sql) or dberror($sql);

        $item_id = mysql_insert_id();

        // copy delivery address for new item in table order_addresses
        $sql = "select * from order_addresses ".
               "where order_address_type = 2 ".
               "    and order_address_order = ". param("oid") .
               "    and (order_address_order_item = 0 ".
               "    or order_address_order_item is null) ";

        $res2 = mysql_query($sql) or dberror($sql);
        $row2 = mysql_fetch_assoc($res2);

        $delivery_address_fields = array();
        $delivery_address_values = array();

        $delivery_address_fields[] = "order_address_order";
        $delivery_address_values[] = $form->value("order_item_order");

        $delivery_address_fields[] = "order_address_order_item";
        $delivery_address_values[] = $item_id;

        $delivery_address_fields[] = "order_address_type";
        $delivery_address_values[] = 2;

        $delivery_address_fields[] = "order_address_company";
        $delivery_address_values[] = dbquote($row2["order_address_company"]);

        $delivery_address_fields[] = "order_address_company2";
        $delivery_address_values[] = dbquote($row2["order_address_company2"]);

        $delivery_address_fields[] = "order_address_address";
        $delivery_address_values[] = dbquote($row2["order_address_address"]);

        $delivery_address_fields[] = "order_address_address2";
        $delivery_address_values[] = dbquote($row2["order_address_address2"]);

        $delivery_address_fields[] = "order_address_zip";
        $delivery_address_values[] = dbquote($row2["order_address_zip"]);

        $delivery_address_fields[] = "order_address_place";
        $delivery_address_values[] = dbquote($row2["order_address_place"]);

        $delivery_address_fields[] = "order_address_country";
        $delivery_address_values[] = dbquote($row2["order_address_country"]);

        $delivery_address_fields[] = "order_address_phone";
        $delivery_address_values[] = dbquote($row2["order_address_phone"]);

        $delivery_address_fields[] = "order_address_fax";
        $delivery_address_values[] = dbquote($row2["order_address_fax"]);

        $delivery_address_fields[] = "order_address_email";
        $delivery_address_values[] = dbquote($row2["order_address_email"]);

        $delivery_address_fields[] = "order_address_parent";
        $delivery_address_values[] = $row2["order_address_parent"];

        $delivery_address_fields[] = "order_address_contact";
        $delivery_address_values[] = dbquote($row2["order_address_contact"]);

        $delivery_address_fields[] = "date_created";
        $delivery_address_values[] = "current_timestamp";

        $delivery_address_fields[] = "date_modified";
        $delivery_address_values[] = "current_timestamp";

        if (isset($_SESSION["user_login"]))
        {
            $delivery_address_fields[] = "user_created";
            $delivery_address_values[] = dbquote($_SESSION["user_login"]);

            $delivery_address_fields[] = "user_modified";
            $delivery_address_values[] = dbquote($_SESSION["user_login"]);
        }

        $sql = "insert into order_addresses (" . join(", ", $delivery_address_fields) . ") values (" . join(", ", $delivery_address_values) . ")";
        mysql_query($sql) or dberror($sql);
}

/***********************************************************************
   save suppliers currency and pricing data and
   copy item information into all other item records of the same order
   belonging to the same supplier
************************************************************************/
function  project_edit_order_item_save($form)
{
		$order_item_fields = array();

        $value =  trim($form->value("order_item_text")) == "" ? "null" : dbquote($form->value("order_item_text"));
        $order_item_fields[] = "order_item_text = " . $value;

        if ($form->value("order_item_type") == ITEM_TYPE_STANDARD or $form->value("order_item_type") == ITEM_TYPE_SERVICES or $form->value("order_item_type") == ITEM_TYPE_SPECIAL)
        {

            $value =  trim($form->value("order_item_quantity")) == "" ? "null" : dbquote($form->value("order_item_quantity"));
            $order_item_fields[] = "order_item_quantity = " . $value;
    
            // get suppliers' currency data
            $supplier_currency_id = null;
            $supplier_currency_exchangerate = null;

            
            if ($form->value("order_item_currency"))
            {
                $currency = get_currency($form->value("order_item_currency"));
                $supplier_currency_id = $currency["id"];
                $supplier_currency_exchangerate = $currency["exchange_rate"];
            }
            else
            {
                $currency = get_address_currency($form->value("order_item_supplier_address"));
                $supplier_currency_id = $currency["id"];
                $supplier_currency_exchangerate = $currency["exchange_rate"];
            }

            
            $order_item_fields[] = "order_item_supplier_currency = " . $supplier_currency_id;

            $order_item_fields[] = "order_item_supplier_exchange_rate = " . $supplier_currency_exchangerate;

            if ($form->value("order_item_type") == ITEM_TYPE_STANDARD or $form->value("order_item_type") == ITEM_TYPE_SERVICES)
            {
                $value =  trim($form->value("order_item_client_price")) == "" ? "null" : dbquote($form->value("order_item_client_price"));
                $order_item_fields[] = "order_item_client_price = " . $value;

                // update supplier's price
                $sql = "select * ".
                       "from suppliers ".
                       "where supplier_address = " . $form->value("order_item_supplier_address") .
                       "    and supplier_item = " . $form->value("order_item_item");

                $res = mysql_query($sql) or dberror($sql);
                if ($row = mysql_fetch_assoc($res))
                {
                    $order_item_fields[] = "order_item_supplier_price = " . $row["supplier_item_price"];
                }
            }
            else if ($form->value("order_item_type") == ITEM_TYPE_SPECIAL)
            {
                
				if(isset($form->items["order_item_cost_group"]))
				{
					$value = $form->value("order_item_cost_group");
					$order_item_fields[] = "order_item_cost_group = " . $value;

					$value = $form->value("order_items_costmonitoring_group");
					$order_item_fields[] = "order_items_costmonitoring_group = " . $value;
				}

				$value =  trim($form->value("order_item_system_price")) == "" ? "null" : dbquote($form->value("order_item_system_price"));
                $order_item_fields[] = "order_item_system_price = " . $value;

                $order_currency = get_order_currency($form->value("oid"));
                $value = $form->value("order_item_system_price") / $order_currency["exchange_rate"] * $order_currency["factor"];
                $order_item_fields[] = "order_item_client_price = " . $value;

                $value =  trim($form->value("order_item_supplier_price")) == "" ? "null" : dbquote($form->value("order_item_supplier_price"));
                $order_item_fields[] = "order_item_supplier_price = " . $value;


                $value =  trim($form->value("order_item_supplier_item_code")) == "" ? "null" : dbquote($form->value("order_item_supplier_item_code"));
                $order_item_fields[] = "order_item_supplier_item_code = " . $value;

                $value =  trim($form->value("order_item_offer_number")) == "" ? "null" : dbquote($form->value("order_item_offer_number"));
                $order_item_fields[] = "order_item_offer_number = " . $value;

                $value =  trim($form->value("order_item_production_time")) == "" ? "null" : dbquote($form->value("order_item_production_time"));
                $order_item_fields[] = "order_item_production_time = " . $value;

            }

            $value =  trim($form->value("order_item_cost_unit_number")) == "" ? "null" : dbquote($form->value("order_item_cost_unit_number"));
            $order_item_fields[] = "order_item_cost_unit_number = " . $value;

            $value =  trim($form->value("order_item_po_number")) == "" ? "null" : dbquote($form->value("order_item_po_number"));
            $order_item_fields[] = "order_item_po_number = " . $value;

            $value =  trim($form->value("order_item_supplier_address")) == "" ? "null" : dbquote($form->value("order_item_supplier_address"));
            $order_item_fields[] = "order_item_supplier_address = " . $value;


            if ($form->value("order_item_type") == ITEM_TYPE_STANDARD or $form->value("order_item_type") == ITEM_TYPE_SPECIAL)
			{
				$value =  trim($form->value("order_item_transportation")) == "" ? "null" : dbquote($form->value("order_item_transportation"));
				$order_item_fields[] = "order_item_transportation = " . $value;

				$value =  trim($form->value("order_item_forwarder_address")) == "" ? "null" : dbquote($form->value("order_item_forwarder_address"));
				$order_item_fields[] = "order_item_forwarder_address = " . $value;

				$value =  trim($form->value("order_item_staff_for_discharge")) == "" ? "null" :     dbquote($form->value("order_item_staff_for_discharge"));
				$order_item_fields[] = "order_item_staff_for_discharge = " . $value;

				$value =  trim($form->value("order_item_no_offer_required")) == "" ? "null" : dbquote($form->value("order_item_no_offer_required"));
				$order_item_fields[] = "order_item_no_offer_required = " . $value;

				$value =  trim($form->value("order_item_only_quantity_proposal")) == "" ? "null" : dbquote($form->value("order_item_only_quantity_proposal"));
				$order_item_fields[] = "order_item_only_quantity_proposal = " . $value;

				$value =  trim($form->value("order_item_not_in_budget")) == "" ? "null" : dbquote($form->value("order_item_not_in_budget"));
				$order_item_fields[] = "order_item_not_in_budget = " . $value;

				if(isset($form->items["order_item_exclude_from_ps"]))
				{
					$value =  trim($form->value("order_item_exclude_from_ps")) == "" ? "null" : dbquote($form->value("order_item_exclude_from_ps"));
					$order_item_fields[] = "order_item_exclude_from_ps = " . $value;
				}
			}

        }
        else if ($form->value("order_item_type") == ITEM_TYPE_COST_ESTIMATION OR $form->value("order_item_type") == ITEM_TYPE_LOCALCONSTRUCTIONCOST)
        {

			if(isset($form->items["order_item_cost_group"]))
			{
				$value = $form->value("order_item_cost_group");
				$order_item_fields[] = "order_item_cost_group = " . $value;

			}
			
			if(isset($form->items["order_item_supplier_freetext"]))
			{
				$value =  trim($form->value("order_item_supplier_freetext")) == "" ? "null" : dbquote($form->value("order_item_supplier_freetext"));
				$order_item_fields[] = "order_item_supplier_freetext = " . $value;
			}

            $value =  trim($form->value("order_item_system_price")) == "" ? "null" : dbquote($form->value("order_item_system_price"));
            $order_item_fields[] = "order_item_system_price = " . $value;

            $order_currency = get_order_currency($form->value("oid"));
            $value = $form->value("order_item_system_price") / $order_currency["exchange_rate"] * $order_currency["factor"];
            $order_item_fields[] = "order_item_client_price = " . $value;

        }
        
        $value = "current_timestamp";
        $order_item_fields[] = "date_modified = " . $value;
    
        if (isset($_SESSION["user_login"]))
        {
            $value = dbquote($_SESSION["user_login"]);
            $order_item_fields[] = "user_modified = " . $value;
        }

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $form->value("order_item_id");
        mysql_query($sql) or dberror($sql);


        if ($form->value("order_item_type") == ITEM_TYPE_STANDARD or $form->value("order_item_type") == ITEM_TYPE_SPECIAL or $form->value("order_item_type") == ITEM_TYPE_SERVICES)
        {
            // copy values into all aother order_items
            $sql_order_item = "select * ".
                              "from order_items ".
                              "where (order_item_not_in_budget is null or order_item_not_in_budget = 0) ".
                              "    and order_item_order = " . param("oid") . " " .
                              "    and order_item_supplier_address = ". $form->value("order_item_supplier_address").
                              "    and (order_item_type = " . ITEM_TYPE_STANDARD . " ".
				              "    or order_item_type = " . ITEM_TYPE_SERVICES . " ".
                              "    or order_item_type = " . ITEM_TYPE_SPECIAL . ")";
    
            $res = mysql_query($sql_order_item) or dberror($sql_order_item);
            while ($row = mysql_fetch_assoc($res))
            {
                // update all empty P.O. Numbers
                $value = trim($form->value("order_item_po_number")) == "" ? "null" : dbquote($form->value("order_item_po_number"));

                if (!$row["order_item_po_number"])
                {
                    $sql = "update order_items set order_item_po_number =" . $value . " ".
                       "where order_item_id = " . $row["order_item_id"];
                    mysql_query($sql) or dberror($sql);
                }


                // update all empty cost unit numbers
                $value = trim($form->value("order_item_cost_unit_number")) == "" ? "null" : dbquote($form->value("order_item_cost_unit_number"));

                if (!$row["order_item_cost_unit_number"])
                {
                    $sql = "update order_items set order_item_cost_unit_number =" . $value . " ".
                       "where order_item_id = " . $row["order_item_id"];
                    mysql_query($sql) or dberror($sql);
                }

                if ($form->value("order_item_type") == ITEM_TYPE_STANDARD or $form->value("order_item_type") == ITEM_TYPE_SPECIAL)
				{
					// update all empty forwarder addresses
					$value = trim($form->value("order_item_forwarder_address")) == "" ? "null" :     dbquote($form->value("order_item_forwarder_address"));

					if (!$row["order_item_forwarder_address"])
					{
						$sql = "update order_items set order_item_forwarder_address =" . $value . " ".
							   "where order_item_id = " . $row["order_item_id"];
						mysql_query($sql) or dberror($sql);
					}
		
					// update all transportation codes
					$value = trim($form->value("order_item_transportation")) == "" ? "null" : dbquote($form->value("order_item_transportation"));

					if (!$row["order_item_transportation"])
					{
						$sql = "update order_items set order_item_transportation =" . $value . " ".
						   "where order_item_id = " . $row["order_item_id"];
						mysql_query($sql) or dberror($sql);
					}
				}
            }
        }

}


/***********************************************************************
   save suppliers entries for an item in the list of materials
************************************************************************/
function  project_edit_order_item_save_supplier_data($form)
{

        $order_item_fields = array();

        $value =  trim($form->value("order_item_text")) == "" ? "null" : dbquote($form->value("order_item_text"));
        $order_item_fields[] = "order_item_text = " . $value;

        $value =  trim($form->value("order_item_quantity")) == "" ? "null" : dbquote($form->value("order_item_quantity"));
        $order_item_fields[] = "order_item_quantity = " . $value;
    
        if ($form->value("order_item_type") == ITEM_TYPE_STANDARD or $form->value("order_item_type") == ITEM_TYPE_SERVICES)
        {
            $value =  trim($form->value("order_item_client_price")) == "" ? "null" : dbquote($form->value("order_item_client_price"));
            $order_item_fields[] = "order_item_client_price = " . $value;

            // update supplier's price
            $sql = "select * ".
                   "from suppliers ".
                   "where supplier_address = " . $form->value("order_item_supplier_address") .
                   "    and supplier_item = " . $form->value("order_item_item");

            $res = mysql_query($sql) or dberror($sql);
            if ($row = mysql_fetch_assoc($res))
            {
                $order_item_fields[] = "order_item_supplier_price = " . dbquote($row["supplier_item_price"]);
            }
        }
        else if ($form->value("order_item_type") == ITEM_TYPE_SPECIAL)
        {

            $value = $form->value("order_item_supplier_price");
            $order_item_fields[] = "order_item_supplier_price = " . dbquote($value);

            $supplier_currency_id = null;
            $supplier_currency_exchangerate = null;


            if ($form->value("order_item_currency"))
            {
                $currency = get_currency($form->value("order_item_currency"));
                $supplier_currency_id = $currency["id"];
                $supplier_currency_exchangerate = $currency["exchange_rate"];
                $supplier_currency_factor = $currency["factor"];
            }
            else
            {
                $currency = get_address_currency($form->value("order_item_supplier_address"));
                $supplier_currency_id = $currency["id"];
                $supplier_currency_exchangerate = $currency["exchange_rate"];
                $supplier_currency_factor = $currency["factor"];
            }

            $order_item_fields[] = "order_item_supplier_currency = " . $supplier_currency_id;
            $order_item_fields[] = "order_item_supplier_exchange_rate = " . $supplier_currency_exchangerate;

            // get orders currencies (system and client)
            $oid = 0;
            if ($form->value("oid"))
            {
                $oid = $form->value("oid");
            }

            $sql = "select order_client_address, order_client_currency, order_system_currency, " .
                  "    order_client_exchange_rate, order_system_exchange_rate " .
                  "    from orders " .
                  "    where order_id = " . $oid;

            $res = mysql_query($sql) or dberror($sql);
            if ($row = mysql_fetch_assoc($res))
            {
                $supplier_price = $form->value("order_item_supplier_price");

                // set sytsem price
                $system_price = $supplier_price * $supplier_currency_exchangerate / $supplier_currency_factor;
                $order_item_fields[] = "order_item_system_price = " . $system_price;
       
                $client_currency = get_address_currency($row["order_client_address"]);
                // set client's price
                $value = $system_price / $row["order_client_exchange_rate"] * $client_currency["factor"];
                $order_item_fields[] = "order_item_client_price = " . $value;

            }
            
            $value =  trim($form->value("order_item_supplier_item_code")) == "" ? "null" : dbquote($form->value("order_item_supplier_item_code"));
            $order_item_fields[] = "order_item_supplier_item_code = " . $value;

            $value =  trim($form->value("order_item_offer_number")) == "" ? "null" : dbquote($form->value("order_item_offer_number"));
            $order_item_fields[] = "order_item_offer_number = " . $value;

            $value =  trim($form->value("order_item_production_time")) == "" ? "null" : dbquote($form->value("order_item_production_time"));
            $order_item_fields[] = "order_item_production_time = " . $value;
        }


        $value = "current_timestamp";
        $order_item_fields[] = "date_modified = " . $value;
    
        if (isset($_SESSION["user_login"]))
        {
            $value = dbquote($_SESSION["user_login"]);
            $order_item_fields[] = "user_modified = " . $value;
        }

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $form->value("order_item_id");
        mysql_query($sql) or dberror($sql);

}


/********************************************************************
    add standard budget positions
*********************************************************************/
function add_standard_budget_positions($type, $order_id)
{

    $sql = "select item_id, item_description ".
           "from items ".
           "where item_active = 1 " .
           "   and item_type = " . $type . " " .
           "order by item_priority";

  
    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $order_item_fields = array();
        $order_item_values = array();

        $order_item_fields[] = "order_item_order";
        $order_item_values[] = $order_id;

        $order_item_fields[] = "order_item_item";
        $order_item_values[] = $row["item_id"];

        $order_item_fields[] = "order_item_text";
        $order_item_values[] = dbquote($row["item_description"]);

        $order_item_fields[] = "order_item_type";
        $order_item_values[] = $type;

        $order_item_fields[] = "date_created";
        $order_item_values[] = "current_timestamp";

        $order_item_fields[] = "date_modified";
        $order_item_values[] = "current_timestamp";
                
        if (isset($_SESSION["user_login"]))
        {
            $order_item_fields[] = "user_created";
            $order_item_values[] = dbquote($_SESSION["user_login"]);

            $order_item_fields[] = "user_modified";
            $order_item_values[] = dbquote($_SESSION["user_login"]);
        }
        $sql = "insert into order_items (" . join(", ", $order_item_fields) . ") values (" . join(", ", $order_item_values) . ")";
        mysql_query($sql) or dberror($sql);
    }
}


/********************************************************************
    delete all budget positions
*********************************************************************/
function delete_all_budget_positions($type, $order_id)
{

    $sql = "delete ".
           "from order_items ".
           "where order_item_type = " . $type . 
           "   and order_item_order = " . $order_id;
    mysql_query($sql) or dberror($sql);
}

/***********************************************************************
   insert dates from order to supplier
************************************************************************/
function  append_order_dates($order_id, $address_id)
{
    $sql = "select order_item_id ".
           "from order_items ".
           "where order_item_order = " . $order_id .
           "    and order_item_supplier_address = " . $address_id .
           "    and order_item_po_number <> ''";

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        insert_date_item($row["order_item_id"], date("Y-m-d"), 'ORSU');
    }
    
}


/***********************************************************************
   insert dates from request for delivery to forwarder
************************************************************************/
function  append_request_for_delivery_dates($order_id, $address_id)
{
    $sql = "select order_item_id ".
           "from order_items ".
           "where order_item_order = " . $order_id .
           "    and order_item_forwarder_address = " . $address_id .
           "    and order_item_po_number <>''";

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        // check if ready for pick up date was entered
        $ready4pickup = 0;

        $sql_dates = "select date_id ".
                     "from dates ".
                     "left join date_types on date_type = date_type_id ".
                     "where date_order_item = ". $row["order_item_id"] .
                     "    and date_type_code = 'EXRP'";

        $res2= mysql_query($sql_dates) or dberror($sql_dates);
  
        if ($row2 = mysql_fetch_assoc($res2))
        {
            $ready4pickup = 1;
        }

        if ($ready4pickup == 1)
        {
            insert_date_item($row["order_item_id"], date("Y-m-d"), 'ORFW');
        }
    }
    
}

/***********************************************************************
   update item's delivery address
************************************************************************/
function  project_update_delivery_address($form)
{
    // insert or update delivery addresses
    $sql = "select order_address_id ".
           "from order_addresses ".
           "where order_address_order = " . $form->value("order_item_order") .
           "    and order_address_order_item = " . $form->value("order_item_id").
           "    and order_address_type = 2";

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res)) 
    {
        // update delivery address in table oder_addresses
        $delivery_address_fields = array();

        $value = trim($form->value("delivery_address_company")) == "" ? "null" : dbquote($form->value("delivery_address_company"));
        $delivery_address_fields[] = "order_address_company = " . $value;

         $value = trim($form->value("delivery_address_company2")) == "" ? "null" : dbquote($form->value("delivery_address_company2"));
        $delivery_address_fields[] = "order_address_company2 = " . $value;

        $value = trim($form->value("delivery_address_address")) == "" ? "null" : dbquote($form->value("delivery_address_address"));
        $delivery_address_fields[] = "order_address_address = " . $value;

        $value = trim($form->value("delivery_address_address2")) == "" ? "null" : dbquote($form->value("delivery_address_address2"));
        $delivery_address_fields[] = "order_address_address2 = " . $value;

        $value = trim($form->value("delivery_address_zip")) == "" ? "null" : dbquote($form->value("delivery_address_zip"));
        $delivery_address_fields[] = "order_address_zip = " . $value;

        $value = dbquote($form->value("delivery_address_place_id"));
        $delivery_address_fields[] = "order_address_place_id = " . $value;

		$value = trim($form->value("delivery_address_place")) == "" ? "null" : dbquote($form->value("delivery_address_place"));
        $delivery_address_fields[] = "order_address_place = " . $value;

        $value = trim($form->value("delivery_address_country")) == "" ? "null" : dbquote($form->value("delivery_address_country"));
        $delivery_address_fields[] = "order_address_country = " . $value;

        $value = trim($form->value("delivery_address_phone")) == "" ? "null" : dbquote($form->value("delivery_address_phone"));
        $delivery_address_fields[] = "order_address_phone = " . $value;

        $value = trim($form->value("delivery_address_fax")) == "" ? "null" : dbquote($form->value("delivery_address_fax"));
        $delivery_address_fields[] = "order_address_fax = " . $value;

        $value = trim($form->value("delivery_address_email")) == "" ? "null" : dbquote($form->value("delivery_address_email"));
        $delivery_address_fields[] = "order_address_email = " . $value;

        $value = $form->value("order_client_address");
        $delivery_address_fields[] = "order_address_parent = " . $value;

        $value = trim($form->value("delivery_address_contact")) == "" ? "null" : dbquote($form->value("delivery_address_contact"));
        $delivery_address_fields[] = "order_address_contact = " . $value;

        $value = "current_timestamp";
        $delivery_address_fields[] = "date_modified = " . $value;

        if (isset($_SESSION["user_login"]))
        {
            $value = dbquote($_SESSION["user_login"]);
            $delivery_address_fields[] = "user_modified = " . $value;
        }

        $item_filter = "";
		if($form->value("same_supplier") == 1) {
			//get items of the same supplier
			$item_ids = array();
			$sql = "select DISTINCT order_item_id from order_items " . 
				   "where order_item_order = " .  $form->value("order_item_order") . 
				   " and order_item_supplier_address = " . dbquote($form->value("order_item_supplier_address"));

			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{
				$item_ids[$row["order_item_id"]] = $row["order_item_id"];
			}

			if(count($item_ids) > 0) {
				$item_filter = " or order_address_order_item IN (" . implode(',', $item_ids) . ") ";
			}

	
		}
		if($form->value("same_forwarder") == 1) {
			$item_ids = array();
			$sql = "select DISTINCT order_item_id from order_items " . 
				   "where order_item_order = " .  $form->value("order_item_order") . 
				   " and order_item_forwarder_address = " . dbquote($form->value("order_item_forwarder_address"));

			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{
				$item_ids[$row["order_item_id"]] = $row["order_item_id"];
			}

			if(count($item_ids) > 0) {
				$item_filter = " or order_address_order_item IN (" . implode(',', $item_ids) . ") ";
			}
		}
		
		$sql = "update order_addresses set " . join(", ", $delivery_address_fields) . 
			   " where order_address_type = 2 ".
			   "    and order_address_order = " . $form->value("order_item_order") .
			   "    and (order_address_order_item = " . $form->value("order_item_id") . $item_filter . ")";
		
        mysql_query($sql) or dberror($sql);
    }
    else // insert new
    {
        // insert delivery addresses into table order_addresses for all items
        // not having a delivery address and beloging to the same order

        $sql_order_items = "select order_item_id, order_item_order ".
                           "from order_items ".
                           "where order_item_order = " . $form->value("order_item_order") .
                           "    and order_item_type <= ". ITEM_TYPE_SPECIAL;


        $res = mysql_query($sql_order_items) or dberror($sql_order_items);
        while ($row = mysql_fetch_assoc($res)) 
        {
            // check if record is already there
            $sql = "select order_address_id ".
                   "from order_addresses ".
                   "where order_address_order = " . $row["order_item_order"] .
                   "    and order_address_order_item = " . $row["order_item_id"].
                   "    and order_address_type = 2";

            $res1 = mysql_query($sql) or dberror($sql);
            if ($row1 = mysql_fetch_assoc($res1)) 
            {
                //nothing
            }
            else
            {
                // insert new record
        
                $delivery_address_fields = array();
                $delivery_address_values = array();

        
                $delivery_address_fields[] = "order_address_order";
                $delivery_address_values[] = $row["order_item_order"];

                $delivery_address_fields[] = "order_address_order_item";
                $delivery_address_values[] = $row["order_item_id"];

                $delivery_address_fields[] = "order_address_type";
                $delivery_address_values[] = 2;

                $delivery_address_fields[] = "order_address_company";
                $delivery_address_values[] = trim($form->value("delivery_address_company")) == "" ? "null" : dbquote($form->value("delivery_address_company"));

                $delivery_address_fields[] = "order_address_company2";
                $delivery_address_values[] = trim($form->value("delivery_address_company2")) == "" ? "null" : dbquote($form->value("delivery_address_company2"));

                $delivery_address_fields[] = "order_address_address";
                $delivery_address_values[] = trim($form->value("delivery_address_address")) == "" ? "null" : dbquote($form->value("delivery_address_address"));

                $delivery_address_fields[] = "order_address_address2";
                $delivery_address_values[] = trim($form->value("delivery_address_address2")) == "" ? "null" : dbquote($form->value("delivery_address_address2"));

                $delivery_address_fields[] = "order_address_zip";
                $delivery_address_values[] = trim($form->value("delivery_address_zip")) == "" ? "null" : dbquote($form->value("delivery_address_zip"));

                $delivery_address_fields[] = "order_address_place";
                $delivery_address_values[] = trim($form->value("delivery_address_place")) == "" ? "null" : dbquote($form->value("delivery_address_place"));

                $delivery_address_fields[] = "order_address_country";
                $delivery_address_values[] = trim($form->value("delivery_address_country")) == "" ? "null" : dbquote($form->value("delivery_address_country"));

                $delivery_address_fields[] = "order_address_phone";
                $delivery_address_values[] = trim($form->value("delivery_address_phone")) == "" ? "null" : dbquote($form->value("delivery_address_phone"));

                $delivery_address_fields[] = "order_address_fax";
                $delivery_address_values[] = trim($form->value("delivery_address_fax")) == "" ? "null" : dbquote($form->value("delivery_address_fax"));

                $delivery_address_fields[] = "order_address_email";
                $delivery_address_values[] = trim($form->value("delivery_address_email")) == "" ? "null" : dbquote($form->value("delivery_address_email"));

                $delivery_address_fields[] = "order_address_parent";
                $delivery_address_values[] = $form->value("order_client_address");

                $delivery_address_fields[] = "order_address_contact";
                $delivery_address_values[] = trim($form->value("delivery_address_contact")) == "" ? "null" : dbquote($form->value("delivery_address_contact"));

                $delivery_address_fields[] = "date_created";
                $delivery_address_values[] = "current_timestamp";

                $delivery_address_fields[] = "date_modified";
                $delivery_address_values[] = "current_timestamp";

                if (isset($_SESSION["user_login"]))
                {
                    $delivery_address_fields[] = "user_created";
                    $delivery_address_values[] = dbquote($_SESSION["user_login"]);

                    $delivery_address_fields[] = "user_modified";
                    $delivery_address_values[] = dbquote($_SESSION["user_login"]);
                }

                $sql = "insert into order_addresses (" . join(", ", $delivery_address_fields) . ") values (" . join(", ", $delivery_address_values) . ")";
                mysql_query($sql) or dberror($sql);
            }
        }
    }
}


/***********************************************************************
   update supplier's data into order_items and dates
************************************************************************/
function  project_update_supplier_data_item($form, $supplier_addresses)
{

    $update_all_warehouse_addresses = $form->value("change_all_warehouse_addresses");
    $order_item_fields = array();

    $value = "current_timestamp";
    $order_item_fields[] = "date_modified = " . $value;
    
    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $order_item_fields[] = "user_modified = " . $value;
    }

    $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $form->value("order_item_id");
    mysql_query($sql) or dberror($sql);
    
    // expected ready for pick up: insert new entry if new or date has changed
    
    if ($form->value("expected_ready_for_pick_up") != $form->value("expected_ready_for_pick_up_old_value"))
    {
        if ($form->value("expected_ready_for_pick_up"))
        {
            $update_alldates = 0;
            $suppliers_ids = array();
            
            foreach($supplier_addresses as $key=>$supplier_address)
            {
            	if($form->value("change_all_dates_" . $supplier_address["id"]) == 1)
	        	{
	        		$suppliers_ids[] = $supplier_address["id"];
	        		$update_alldates = 1;
	        	}
	            
            }
            
            if($update_alldates == 0)
	        {
	            insert_date_item($form->value("order_item_id"), $form->value("expected_ready_for_pick_up"), 'EXRP');
	        }
	
	        // insert the same date for all order item records belonging to the
	        // same P.O. Number and the same supplier
	
	        foreach($suppliers_ids as $key=>$supplier_id)
	        {
		        append_date_items($form->value("order_item_order"), 
		                          "empty", 
		                          $supplier_id, 
		                          $form->value("expected_ready_for_pick_up"),
		                          "EXRP",
		                          "SUPP",
		                          $update_alldates, 
		                          $supplier_id);
	        }
        }
		elseif(has_access("can_delete_project_pickup_dates") or has_access("can_delete_order_pickup_dates"))
		{
			insert_date_item($form->value("order_item_id"), $form->value("expected_ready_for_pick_up"), 'EXRP');
		}
    }
    

    // insert or update warehose addresses
	$new_place_id = 0;
	if($form->value("warehouse_address_place_id") == 999999999) //insert new city
	{
		$sql = "insert into places (place_country, place_province, place_name, date_created, user_created) VALUES (" .
			   dbquote($form->value("warehouse_address_country")) . ", " . 
			   dbquote($form->value("province")) . ", " . 
			   dbquote($form->value("warehouse_address_place")) . ", " .
			   dbquote(date("Y-m-d")) . ", " . 
			    dbquote(user_login()) . ")";
		
		mysql_query($sql) or dberror($sql);
		$new_place_id =  mysql_insert_id();
	
	}


    $sql = "select order_address_id ".
           "from order_addresses ".
           "where order_address_order = " . $form->value("order_item_order") .
           "    and order_address_order_item = " . $form->value("order_item_id").
           "    and order_address_type = 4";

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res)) 
    {
        // update warehous address in table oder_addresses
        $warehouse_address_fields = array();

        $value = trim($form->value("warehouse_address_company")) == "" ? "null" : dbquote($form->value("warehouse_address_company"));
        $warehouse_address_fields[] = "order_address_company = " . $value;

         $value = trim($form->value("warehouse_address_company2")) == "" ? "null" : dbquote($form->value("warehouse_address_company2"));
        $warehouse_address_fields[] = "order_address_company2 = " . $value;

        $value = trim($form->value("warehouse_address_address")) == "" ? "null" : dbquote($form->value("warehouse_address_address"));
        $warehouse_address_fields[] = "order_address_address = " . $value;

        $value = trim($form->value("warehouse_address_address2")) == "" ? "null" : dbquote($form->value("warehouse_address_address2"));
        $warehouse_address_fields[] = "order_address_address2 = " . $value;

        $value = trim($form->value("warehouse_address_zip")) == "" ? "null" : dbquote($form->value("warehouse_address_zip"));
        $warehouse_address_fields[] = "order_address_zip = " . $value;

        $value = trim($form->value("warehouse_address_place")) == "" ? "null" : dbquote($form->value("warehouse_address_place"));
        $warehouse_address_fields[] = "order_address_place = " . $value;

		
		if($new_place_id > 0)
		{
			$value = $new_place_id;
		}
		else
		{
			$value = trim($form->value("warehouse_address_place_id")) == "" ? "null" : dbquote($form->value("warehouse_address_place_id"));
		}
        $warehouse_address_fields[] = "order_address_place_id = " . $value;

        $value = trim($form->value("warehouse_address_country")) == "" ? "null" : dbquote($form->value("warehouse_address_country"));
        $warehouse_address_fields[] = "order_address_country = " . $value;

        $value = trim($form->value("warehouse_address_phone")) == "" ? "null" : dbquote($form->value("warehouse_address_phone"));
        $warehouse_address_fields[] = "order_address_phone = " . $value;

        $value = trim($form->value("warehouse_address_fax")) == "" ? "null" : dbquote($form->value("warehouse_address_fax"));
        $warehouse_address_fields[] = "order_address_fax = " . $value;

        $value = trim($form->value("warehouse_address_email")) == "" ? "null" : dbquote($form->value("warehouse_address_email"));
        $warehouse_address_fields[] = "order_address_email = " . $value;

        $value = $form->value("order_item_supplier");
        $warehouse_address_fields[] = "order_address_parent = " . $value;

        $value = trim($form->value("warehouse_address_contact")) == "" ? "null" : dbquote($form->value("warehouse_address_contact"));
        $warehouse_address_fields[] = "order_address_contact = " . $value;

        $value = "current_timestamp";
        $warehouse_address_fields[] = "date_modified = " . $value;

        if (isset($_SESSION["user_login"]))
        {
            $value = dbquote($_SESSION["user_login"]);
            $warehouse_address_fields[] = "user_modified = " . $value;
        }
		
		if($update_all_warehouse_addresses == 1) // update all warehouse addresses
		{
	        $sql = "select order_address_id " . 
	               "from order_addresses " .
	               "left join order_items on order_item_id = order_address_order_item " .
	               " where order_address_type = 4 ".
	               "    and order_address_order = " . $form->value("order_item_order") .
	               "    and order_item_supplier_address = " . $form->value("order_item_supplier");
	        
	        $res = mysql_query($sql) or dberror($sql);
		    while ($row = mysql_fetch_assoc($res)) 
		    {
		        $sql_u = "update order_addresses set " . join(", ", $warehouse_address_fields) . 
		               " where order_address_id = " . $row["order_address_id"];
		    	
		    	mysql_query($sql_u) or dberror($sql_u);
		    }
		}
		else
		{
			$sql = "update order_addresses set " . join(", ", $warehouse_address_fields) . 
	               " where order_address_type = 4 ".
	               "    and order_address_order = " . $form->value("order_item_order").
	               "    and order_address_order_item = ". $form->value("order_item_id");
	        
	        mysql_query($sql) or dberror($sql);
		}
        
    }
    else // insert new
    {
        // insert warehouse addresses into table order_addresses for all items
        // not having a warehose address and beloging to the same order and the
        // same supplier

        if($update_all_warehouse_addresses == 1) // update all warehouse addresses
		{
	        $sql_order_items = "select order_item_id, order_item_order ".
	                           "from order_items ".
	                           "where order_item_order = " . $form->value("order_item_order") .
	                           "    and order_item_supplier_address =" . $form->value("order_item_supplier") .
	                           "    and order_item_type <= ". ITEM_TYPE_SPECIAL;
		}
		else
		{
			$sql_order_items = "select order_item_id, order_item_order ".
	                           "from order_items ".
	                           "where order_item_order = " . $form->value("order_item_order") .
	                           "    and order_item_supplier_address =" . $form->value("order_item_supplier") .
	                           "    and order_item_id = ". $form->value("order_item_id") .
							   "    and order_item_type <= ". ITEM_TYPE_SPECIAL;
		}


        $res = mysql_query($sql_order_items) or dberror($sql_order_items);
        while ($row = mysql_fetch_assoc($res)) 
        {
            // check if record is already there
            $sql = "select order_address_id ".
                   "from order_addresses ".
                   "where order_address_order = " . $row["order_item_order"] .
                   "    and order_address_order_item = " . $row["order_item_id"].
                   "    and order_address_type = 4";

            $res1 = mysql_query($sql) or dberror($sql);
            if ($row1 = mysql_fetch_assoc($res1)) 
            {
                //nothing
            }
            else
            {
                // insert new record
        
                $warehouse_address_fields = array();
                $warehouse_address_values = array();

        
                $warehouse_address_fields[] = "order_address_order";
                $warehouse_address_values[] = $row["order_item_order"];

                $warehouse_address_fields[] = "order_address_order_item";
                $warehouse_address_values[] = $row["order_item_id"];

                $warehouse_address_fields[] = "order_address_type";
                $warehouse_address_values[] = 4;

                $warehouse_address_fields[] = "order_address_company";
                $warehouse_address_values[] = trim($form->value("warehouse_address_company")) == "" ? "null" : dbquote($form->value("warehouse_address_company"));

                $warehouse_address_fields[] = "order_address_company2";
                $warehouse_address_values[] = trim($form->value("warehouse_address_company2")) == "" ? "null" : dbquote($form->value("warehouse_address_company2"));

                $warehouse_address_fields[] = "order_address_address";
                $warehouse_address_values[] = trim($form->value("warehouse_address_address")) == "" ? "null" : dbquote($form->value("warehouse_address_address"));

                $warehouse_address_fields[] = "order_address_address2";
                $warehouse_address_values[] = trim($form->value("warehouse_address_address2")) == "" ? "null" : dbquote($form->value("warehouse_address_address2"));

                $warehouse_address_fields[] = "order_address_zip";
                $warehouse_address_values[] = trim($form->value("warehouse_address_zip")) == "" ? "null" : dbquote($form->value("warehouse_address_zip"));

                if($new_place_id > 0)
				{
					$value = $new_place_id;
				}
				else
				{
					$value = $form->value("warehouse_address_place_id");
				}
				
				$warehouse_address_fields[] = "order_address_place_id";
                $warehouse_address_values[] = dbquote($value);

				$warehouse_address_fields[] = "order_address_place";
                $warehouse_address_values[] = trim($form->value("warehouse_address_place")) == "" ? "null" : dbquote($form->value("warehouse_address_place"));

                $warehouse_address_fields[] = "order_address_country";
                $warehouse_address_values[] = trim($form->value("warehouse_address_country")) == "" ? "null" : dbquote($form->value("warehouse_address_country"));

                $warehouse_address_fields[] = "order_address_phone";
                $warehouse_address_values[] = trim($form->value("warehouse_address_phone")) == "" ? "null" : dbquote($form->value("warehouse_address_phone"));

                $warehouse_address_fields[] = "order_address_fax";
                $warehouse_address_values[] = trim($form->value("warehouse_address_fax")) == "" ? "null" : dbquote($form->value("warehouse_address_fax"));

                $warehouse_address_fields[] = "order_address_email";
                $warehouse_address_values[] = trim($form->value("warehouse_address_email")) == "" ? "null" : dbquote($form->value("warehouse_address_email"));

                $warehouse_address_fields[] = "order_address_parent";
                $warehouse_address_values[] = $form->value("order_item_supplier");

                $warehouse_address_fields[] = "order_address_contact";
                $warehouse_address_values[] = trim($form->value("warehouse_address_contact")) == "" ? "null" : dbquote($form->value("warehouse_address_contact"));

                $warehouse_address_fields[] = "date_created";
                $warehouse_address_values[] = "current_timestamp";

                $warehouse_address_fields[] = "date_modified";
                $warehouse_address_values[] = "current_timestamp";

                if (isset($_SESSION["user_login"]))
                {
                    $warehouse_address_fields[] = "user_created";
                    $warehouse_address_values[] = dbquote($_SESSION["user_login"]);

                    $warehouse_address_fields[] = "user_modified";
                    $warehouse_address_values[] = dbquote($_SESSION["user_login"]);
                }

                $sql = "insert into order_addresses (" . join(", ", $warehouse_address_fields) . ") values (" . join(", ", $warehouse_address_values) . ")";
                mysql_query($sql) or dberror($sql);
            }
        }
    }



	//update all slave items depending on the item, mark as delivered
	if ($form->value("expected_ready_for_pick_up") 
		and ($form->value("expected_ready_for_pick_up_old_value") == "" or $form->value("expected_ready_for_pick_up_old_value") == "0000-00-00"))
    {

		
		if($form->value("change_all_dates_" . $supplier_address["id"]) == 1)
	    {
			$sql = "select item_slave_item " . 
				   "from order_items "  .
				   "left join items on item_id = order_item_item " .
				   " where order_item_order = " . $form->value("order_item_order") . 
				   " and order_item_supplier_address = " . $form->value("order_item_supplier") .
				   " and item_slave_item > 0";
		}
		else
		{
			$sql = "select item_slave_item " . 
				   "from order_items "  .
				   "left join items on item_id = order_item_item " .
				   " where order_item_order = " . $form->value("order_item_order") . 
				   " and order_item_id = " . $form->value("order_item_id") . 
				   " and item_slave_item > 0";
		}
		
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res)) 
		{
			$sql_u = "update order_items set " . 
					 "order_item_ready_for_pickup = " . dbquote(to_system_date($form->value("expected_ready_for_pick_up"))) . ", " . 
					 "order_item_ready_for_pickup_changes = 1,  " . 
					 "order_item_pickup = " . dbquote(to_system_date($form->value("expected_ready_for_pick_up"))) . ", " .
					 "order_item_pickup_changes = 1, " . 
					 "order_item_expected_arrival = " . dbquote(to_system_date($form->value("expected_ready_for_pick_up"))) . ", " .
					 "order_item_expected_arrival_changes = 1, " .
					 "order_item_arrival = " . dbquote(to_system_date($form->value("expected_ready_for_pick_up"))) . ", " .
					 "order_item_arrival_changes = 1 " .
					 "where order_item_item = " . $row["item_slave_item"] . 
				     " and order_item_supplier_address = " . $form->value("order_item_supplier") . 
				     " and order_item_order = " . $form->value("order_item_order");

			
			
			mysql_query($sql_u) or dberror($sql_u);

		    
			$sql_i = "select order_item_id " . 
				     "from order_items "  .
				     "where order_item_item = " . $row["item_slave_item"] . 
				     " and order_item_supplier_address = " . $form->value("order_item_supplier") . 
				     " and order_item_order = " . $form->value("order_item_order");

			
			$res_i = mysql_query($sql_i) or dberror($sql_i);
			while ($row_i = mysql_fetch_assoc($res_i)) 
			{
				insert_date_item($row_i["order_item_id"], $form->value("expected_ready_for_pick_up"), 'EXRP');
				insert_date_item($row_i["order_item_id"], $form->value("expected_ready_for_pick_up"), 'PICK');
				insert_date_item($row_i["order_item_id"], $form->value("expected_ready_for_pick_up"), 'EXAR');
				insert_date_item($row_i["order_item_id"], $form->value("expected_ready_for_pick_up"), 'ACAR');
			}
		}


	}

}

/***********************************************************************
   insert dates from traffic data into dates
************************************************************************/
function  update_traffic_data_item($form, $type, $supplier_addresses)
{

    $update_alldates = 0;
    $suppliers_ids = array();
    foreach($supplier_addresses as $key=>$supplier_address)
    {
    	if($form->value("change_all_dates_" . $supplier_address["id"]) == 1)
    	{
    		$suppliers_ids[] = $supplier_address["id"];
    		$update_alldates = 1;
    	}
        
    }
    
    $order_item_fields = array();

    $value = dbquote($form->value("order_item_shipment_code"));
    $order_item_fields[] = "order_item_shipment_code = " . $value;
    
    $value = "current_timestamp";
    $order_item_fields[] = "date_modified = " . $value;
    
    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $order_item_fields[] = "user_modified = " . $value;
    }

    $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $form->value("order_item_id");
    mysql_query($sql) or dberror($sql);


    // update shipment code for all empty items belonging to the same forwarder and p.o. number
    foreach($suppliers_ids as $key=>$supplier_id)
	{
		$sql = "update order_items ".
			   "set order_item_shipment_code=" . dbquote($form->value("order_item_shipment_code")) . " ".
			   "where order_item_order=" . $form->value("order_item_order").
			   "    and order_item_forwarder_address=" . $form->value("order_item_forwarder").
			   "    and order_item_po_number='" . $form->value("order_item_po_number"). "' " .
			   //"    and (order_item_shipment_code is Null or order_item_shipment_code = '') " . 
			   "    and order_item_supplier_address = " . $supplier_id;
		mysql_query($sql) or dberror($sql);
	}

    // pick up: insert new entry if new or date has changed
    if ($form->value("pick_up") != $form->value("pick_up_old_value"))
    {
        if ($form->value("pick_up"))
        {
            if($update_alldates == 0)
	        {
            	insert_date_item($form->value("order_item_id"), $form->value("pick_up"), 'PICK');
	        }
            // insert the same date for all order item records belonging to the
	        // same p.o. number and the same forwarder
            foreach($suppliers_ids as $key=>$supplier_id)
	        {
	        	append_date_items($form->value("order_item_order"),
	                              $form->value("order_item_po_number"),
	                              $form->value("order_item_forwarder"), 
	                              $form->value("pick_up"),
	                              'PICK',
	                              'FRWD',
	                              $update_alldates, 
	                              $supplier_id);
	        }
        }
		elseif(has_access("can_delete_project_traffic_dates") or has_access("can_delete_order_traffic_dates"))
		{
			insert_date_item($form->value("order_item_id"), $form->value("pick_up"), 'PICK');
		}
    }

    // expected arrival: insert new entry if new or date has changed
    if ($form->value("expected_arrival") != $form->value("expected_arrival_old_value"))
    {
        if ($form->value("expected_arrival"))
        {
            if($update_alldates == 0)
	        {
            	insert_date_item($form->value("order_item_id"), $form->value("expected_arrival"), 'EXAR');
	        }
            
            // insert the same date for all order item records belonging to the
	        // same p.o. number and the same forwarder
	        foreach($suppliers_ids as $key=>$supplier_id)
	        {
	        	append_date_items($form->value("order_item_order"),
	                              $form->value("order_item_po_number"),
	                              $form->value("order_item_forwarder"), 
	                              $form->value("expected_arrival"),
	                              'EXAR',
	                              'FRWD',
	                              $update_alldates, 
	                              $supplier_id);
	        }
        }
		elseif(has_access("can_delete_project_traffic_dates") or has_access("can_delete_order_traffic_dates"))
		{
			insert_date_item($form->value("order_item_id"), $form->value("expected_arrival"), 'EXAR');
		}
    }

    // actial arrival: insert new entry if new or date has changed
    if ($form->value("arrival") != $form->value("arrival_old_value"))
    {
        if ($form->value("arrival"))
        {
            if($update_alldates == 0)
	        {
	        	insert_date_item($form->value("order_item_id"), $form->value("arrival"), 'ACAR');
	        }
            
            // insert the same date for all order item records belonging to the
	        // same p.o. number and the same forwarder
            foreach($suppliers_ids as $key=>$supplier_id)
	        {
	        	append_date_items($form->value("order_item_order"),
	                              $form->value("order_item_po_number"),
	                              $form->value("order_item_forwarder"), 
	                              $form->value("arrival"),
	                              'ACAR',
	                              'FRWD',
	                              $update_alldates, 
	                              $supplier_id);
	        }
        }
		elseif(has_access("can_delete_project_traffic_dates") or has_access("can_delete_order_traffic_dates"))
		{
			insert_date_item($form->value("order_item_id"), $form->value("arrival"), 'ACAR');
		}
    }
}


/***********************************************************************
   insert a new record into dates on the level of an order item
************************************************************************/
function  insert_date_item($order_item_id, $date, $type)
{
    //get date_type_id
    $sql = "select date_type_id ".
           "from date_types ".
           "where date_type_code =" . dbquote($type);

    $res = mysql_query($sql) or dberror($sql);
    
    if ($row = mysql_fetch_assoc($res))
    {
        $date_fields = array();
        $date_values = array();

        $date_fields[] = "date_order_item";
        $date_values[] = $order_item_id;

        $date_fields[] = "date_type";
        $date_values[] = $row["date_type_id"];

        $date_fields[] = "date_date";
        if ($date == "current_timestamp")
        {
            $date_values[] = dbquote(from_system_date($date), true);
        }
        else
        {
            $date_values[] = dbquote(from_system_date($date), true);
        }

        $date_fields[] = "date_created";
        $date_values[] = "current_timestamp";

        $date_fields[] = "date_modified";
        $date_values[] = "current_timestamp";
                
        if (isset($_SESSION["user_login"]))
        {
            $date_fields[] = "user_created";
            $date_values[] = dbquote($_SESSION["user_login"]);

            $date_fields[] = "user_modified";
            $date_values[] = dbquote($_SESSION["user_login"]);
        }
    
        $sql = "insert into dates (" . join(", ", $date_fields) . ") values (" . join(", ", $date_values) . ")";
        mysql_query($sql) or dberror($sql);
   
    }
    update_order_item_dates($order_item_id, $date, $type);
}



/***********************************************************************
   append traffic data to dates

   append records for all other empty entries belonging
   to the same forwarder and the same P.O. Number
************************************************************************/
function  append_date_items($order, $order_po_number, $address, $date, $type, $address_type, $update_mode, $supplier_address)
{
    
    $date_type = $type;

    //get date_type_id
    $sql = "select date_type_id ".
           "from date_types ".
           "where date_type_code =" . dbquote($type);

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        if ($address_type == 'FRWD')
        {
            $sql = "select order_item_id ".
                   "from order_items ".
                   "where order_item_type <= " . ITEM_TYPE_SPECIAL .
                   "   and order_item_order = " . $order .
                   "   and order_item_po_number = " . dbquote($order_po_number) .
                   "   and order_item_forwarder_address = " . $address;
                   
                   
           $sql = "select order_item_id ".
                   "from order_items ".
                   "where order_item_type <= " . ITEM_TYPE_SPECIAL .
                   "   and order_item_order = " . $order .
                   "   and order_item_po_number <>'' " .
                   "   and order_item_supplier_address = " . $supplier_address . 
                   "   and order_item_forwarder_address = " . $address;
        }
        else
        {   
            $sql = "select order_item_id, order_item_supplier_address ".
                   "from order_items ".
                   "where order_item_type <= " . ITEM_TYPE_SPECIAL .
                   "   and order_item_order = " . $order .
                   "   and order_item_supplier_address = " . $address .
                   "   and order_item_po_number <>''";
        }


        $res1 = mysql_query($sql) or dberror($sql);
        while ($row1 = mysql_fetch_assoc($res1))
        {
            // check if a date record is present
            $sql = "select date_id " .
                   "from dates " .
                   "where date_order_item = " . $row1["order_item_id"] . 
                   " and date_type = " . $row["date_type_id"];

            $res2 = mysql_query($sql) or dberror($sql);
                
            if ($row2 = mysql_fetch_assoc($res2) && $update_mode == 0) // update dates
            {
				// check if ready fro pick up date is entered
                if ($address_type == 'FRWD')
                {

                    $sql_dates = "select date_id ".
                                 "from dates ".
                                 "left join date_types on date_type = date_type_id ".
                                 "where date_order_item = ". $row1["order_item_id"] .
                                 "    and date_type_code = 'EXRP'";

                    $res3= mysql_query($sql_dates) or dberror($sql_dates);
  
                    
                    if ($row3 = mysql_fetch_assoc($res3))
                    {
                        insert_date_item($row1["order_item_id"], $date, $date_type);
                    }
                }
                else
                {
                	insert_date_item($row1["order_item_id"], $date, $date_type);
                }
            }
            else // insert dates
            {
                // check if ready fro pick up date is entered
                if ($address_type == 'FRWD')
                {
                    $ready4pickup = 0;

                    $sql_dates = "select date_id ".
                                 "from dates ".
                                 "left join date_types on date_type = date_type_id ".
                                 "where date_order_item = ". $row1["order_item_id"] .
                                 "    and date_type_code = 'EXRP'";

                    $res3= mysql_query($sql_dates) or dberror($sql_dates);
  
                    if ($row3 = mysql_fetch_assoc($res3))
                    {
                        $ready4pickup = 1;
                    }

                    if ($ready4pickup == 1)
                    {
                        insert_date_item($row1["order_item_id"], $date, $date_type);
                    }                    
                }
                else
                {
                    insert_date_item($row1["order_item_id"], $date, $date_type);
                }
            }
        }

    }
}


/***********************************************************************
   update dates in order_items
************************************************************************/
function  update_order_item_dates($order_item_id, $date, $type)
{

    if ($type == 'EXRP')
    {
        
        $sql= "select order_item_ready_for_pickup, order_item_ready_for_pickup_changes ".
              "from order_items ".
              "where order_item_id = " . $order_item_id;
        
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $number_of_changes = $row["order_item_ready_for_pickup_changes"] + 1;
        }
        
        $sql = "Update order_items set order_item_ready_for_pickup = " . dbquote(from_system_date($date), true) . ", order_item_ready_for_pickup_changes = " .   $number_of_changes . " where order_item_id = " . $order_item_id;
        mysql_query($sql) or dberror($sql);
    }
    elseif ($type == 'PICK')
    {
        
        $sql= "select order_item_pickup, order_item_pickup_changes ".
              "from order_items ".
              "where order_item_id = " . $order_item_id;
        
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $number_of_changes = $row["order_item_pickup_changes"] + 1;
        }
        $sql = "Update order_items set order_item_pickup = " . dbquote(from_system_date($date), true) . ", order_item_pickup_changes = " .   $number_of_changes . " where order_item_id = " . $order_item_id;
        mysql_query($sql) or dberror($sql);
    }
    elseif ($type == 'EXAR')
    {
        $sql= "select order_item_expected_arrival, order_item_expected_arrival_changes ".
              "from order_items ".
              "where order_item_id = " . $order_item_id;
        
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $number_of_changes = $row["order_item_expected_arrival_changes"] + 1;
        }
        $sql = "Update order_items set order_item_expected_arrival = " . dbquote(from_system_date($date), true) . ", order_item_expected_arrival_changes = " .   $number_of_changes . " where order_item_id = " . $order_item_id;
        mysql_query($sql) or dberror($sql);
    }
    elseif ($type == 'ACAR')
    {
        
        $sql= "select order_item_arrival, order_item_arrival_changes ".
              "from order_items ".
              "where order_item_id = " . $order_item_id;
        
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $number_of_changes = $row["order_item_arrival_changes"] + 1;
        }
        $sql = "Update order_items set order_item_arrival = " . dbquote(from_system_date($date), true) . ", order_item_arrival_changes = " .   $number_of_changes . " where order_item_id = " . $order_item_id;
        mysql_query($sql) or dberror($sql);
    }
    elseif ($type == 'ORSU')
    {
        
        $sql= "select order_item_ordered, order_item_ordered_changes ".
              "from order_items ".
              "where order_item_id = " . $order_item_id;
        
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $number_of_changes = $row["order_item_ordered_changes"] + 1;
        }
        if($row["order_item_ordered"] == null or $row["order_item_ordered"] == "0000-00-00") 
        {
            $sql = "Update order_items set order_item_ordered = " . dbquote(from_system_date($date), true) . ", order_item_ordered_changes = " .   $number_of_changes . " where order_item_id = " . $order_item_id;
            mysql_query($sql) or dberror($sql);
        }
    }
}

/********************************************************************
    save comment
*********************************************************************/
function save_comment($form, $check_box_names)
{
    $comment_fields = array();
    $comment_values = array();

    $comment_fields[] = "comment_order";
    $comment_values[] = $form->value("order_id");

    $comment_fields[] = "comment_user";
    $comment_values[] = user_id();

    $comment_fields[] = "comment_category";
    $comment_values[] = $form->value("comment_category");

    $comment_fields[] = "comment_text";
    $comment_values[] = dbquote($form->value("comment_text"));

    $comment_fields[] = "date_created";
    $comment_values[] = "current_timestamp";

    $comment_fields[] = "date_modified";
    $comment_values[] = "current_timestamp";
                
    if (isset($_SESSION["user_login"]))
    {
        $comment_fields[] = "user_created";
        $comment_values[] = dbquote($_SESSION["user_login"]);

        $comment_fields[] = "user_modified";
        $comment_values[] = dbquote($_SESSION["user_login"]);
    }
    
    $sql = "insert into comments (" . join(", ", $comment_fields) . ") values (" . join(", ", $comment_values) . ")";
    mysql_query($sql) or dberror($sql);

    $comment_id = mysql_insert_id();


    // insert records into comment_addresses

    if (has_access("can_set_comment_accessibility_in_projects") or has_access("can_set_comment_accessibility_in_orders"))
    {

        foreach ($check_box_names as $key=>$value)
        {
            if ($form->value($value))
            {
                $comment_address_fields = array();
                $comment_address_values = array();

                $comment_address_fields[] = "comment_address_comment";
                $comment_address_values[] = $comment_id;

                $comment_address_fields[] = "comment_address_address";
                $comment_address_values[] = $key;

                $comment_address_fields[] = "date_created";
                $comment_address_values[] = "current_timestamp";

                $comment_address_fields[] = "date_modified";
                $comment_address_values[] = "current_timestamp";
                
                if (isset($_SESSION["user_login"]))
                {
                    $comment_address_fields[] = "user_created";
                    $comment_address_values[] = dbquote($_SESSION["user_login"]);

                    $comment_address_fields[] = "user_modified";
                    $comment_address_values[] = dbquote($_SESSION["user_login"]);
                }
    
                $sql = "insert into comment_addresses (" . join(", ", $comment_address_fields) . ") values (" . join(",        ", $comment_address_values) . ")";
                mysql_query($sql) or dberror($sql);
            }
        }
    }
    return $comment_id;
}


/********************************************************************
    save attachment infos concerning visibility and roles
*********************************************************************/
function save_attachment_accessibility_info($form, $check_box_names)
{

    // get order_file_id from the inserted record
    $sql = "select order_file_id ".
           "from order_files ".
           "where order_file_order = " . $form->value("order_file_order") . " " .
           "order by date_created DESC ".
           "limit 1";

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $attachment_id = $row["order_file_id"];
    }

    // insert records into order_file_addresses
    if (has_access("can_set_attachment_accessibility_in_orders") or has_access("can_set_attachment_accessibility_in_projects"))
    {
    
        foreach ($check_box_names as $key=>$value)
        {
            if ($form->value($key))
            {
                $sql = "select user_address from users where user_id = " . $value;
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				$address_id = $row["user_address"];

				$attachment_address_fields = array();
                $attachment_address_values = array();

                $attachment_address_fields[] = "order_file_address_file";
                $attachment_address_values[] = $attachment_id;

                $attachment_address_fields[] = "order_file_address_address";
                $attachment_address_values[] = $address_id;

                $attachment_address_fields[] = "date_created";
                $attachment_address_values[] = "current_timestamp";

                $attachment_address_fields[] = "date_modified";
                $attachment_address_values[] = "current_timestamp";
                
                if (isset($_SESSION["user_login"]))
                {
                    $attachment_address_fields[] = "user_created";
                    $attachment_address_values[] = dbquote($_SESSION["user_login"]);

                    $attachment_address_fields[] = "user_modified";
                    $attachment_address_values[] = dbquote($_SESSION["user_login"]);
                }
    
                $sql = "insert into order_file_addresses (" . join(", ", $attachment_address_fields) . ") values (" . join(", ", $attachment_address_values) . ")";
                mysql_query($sql) or dberror($sql);
            }
        }
    }
}


/********************************************************************
    update file accessibility
*********************************************************************/
function update_attachment_accessibility_info($id, $form, $check_box_names)
{
	$sql = "delete from order_file_addresses where order_file_address_file = " . $id;
    mysql_query($sql) or dberror($sql);

	foreach ($check_box_names as $key=>$value)
    {
		if ($form->value($key) == 1)
        {
			$sql = "select user_address from users where user_id = " . $value;
			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);
			$address_id = $row["user_address"];

			
			$attachment_address_fields = array();
            $attachment_address_values = array();

            $attachment_address_fields[] = "order_file_address_file";
            $attachment_address_values[] = $id;

            $attachment_address_fields[] = "order_file_address_address";
            $attachment_address_values[] = $address_id;

            $attachment_address_fields[] = "date_created";
            $attachment_address_values[] = "current_timestamp";

            $attachment_address_fields[] = "date_modified";
            $attachment_address_values[] = "current_timestamp";
                
            if (isset($_SESSION["user_login"]))
            {
                $attachment_address_fields[] = "user_created";
                $attachment_address_values[] = dbquote($_SESSION["user_login"]);

                $attachment_address_fields[] = "user_modified";
                $attachment_address_values[] = dbquote($_SESSION["user_login"]);
            }
    
            $sql = "insert into order_file_addresses (" . join(", ", $attachment_address_fields) . ") values (" . join(", ", $attachment_address_values) . ")";
            mysql_query($sql) or dberror($sql);

        }

    }
}


/********************************************************************
    update comment accessibility
*********************************************************************/
function update_comment_accessibility_info($id, $form, $check_box_names)
{
    $sql = "delete from comment_addresses where comment_address_comment = " . $id;
    mysql_query($sql) or dberror($sql);


    foreach ($check_box_names as $key=>$value)
    {
        if ($form->value($value))
        {
            $comment_addressfields = array();
            $comment_addressvalues = array();

            $comment_addressfields[] = "comment_address_comment";
            $comment_addressvalues[] = $id;

            $comment_addressfields[] = "comment_address_address";
            $comment_addressvalues[] = $key;

            $comment_addressfields[] = "date_created";
            $comment_addressvalues[] = "current_timestamp";

            $comment_addressfields[] = "date_modified";
            $comment_addressvalues[] = "current_timestamp";
                
            if (isset($_SESSION["user_login"]))
            {
                $comment_addressfields[] = "user_created";
                $comment_addressvalues[] = dbquote($_SESSION["user_login"]);

                $comment_addressfields[] = "user_modified";
                $comment_addressvalues[] = dbquote($_SESSION["user_login"]);
            }
    
            $sql = "insert into comment_addresses (" . join(", ", $comment_addressfields) . ") values (" . join(", ", $comment_addressvalues) . ")";
                mysql_query($sql) or dberror($sql);
        }
    }
}

/********************************************************************
    delete attachment
*********************************************************************/
function delete_attachment($id)
{
    // delete access rights
    $sql = "delete from order_file_addresses where order_file_address_file = " . $id;
    mysql_query($sql) or dberror($sql);

    // delete file
    $sql = "select order_file_path from order_files where order_file_id = " . $id;

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $file = ".." . $row["order_file_path"];
        if (file_exists($file))
        {
            unlink($file);
        }
    }

    // delete attachment data
    $sql = "delete from order_files where order_file_id = " . $id;
    mysql_query($sql) or dberror($sql);


}


/********************************************************************
    delete comment
*********************************************************************/
function delete_comment($id)
{
    // delete access rights
    $sql = "delete from comment_addresses where comment_address_comment = " . $id;
    mysql_query($sql) or dberror($sql);

    // delete comment data
    $sql = "delete from comments where comment_id = " . $id;
    mysql_query($sql) or dberror($sql);

}


/**************************************************************************************
    append record to the tabel actual_order_states
***************************************************************************************/
function append_order_state($order_id, $order_state_code, $type, $do_update, $keep_project_state = 0)
{
	// get order_state_id
    $sql = "select order_state_id, order_state_change_state, " .
           "order_state_order_state_type, " .
           "   order_state_delete_tasks " .
           "from order_states ".
           "left join order_state_groups on order_state_group = order_state_group_id ".
           "where order_state_code = " . dbquote($order_state_code) .
           "    and order_state_group_order_type = " .$type;

    $res = mysql_query($sql) or dberror($sql);
    
    if ($row = mysql_fetch_assoc($res))
    {
        $order_state_change_state = $row["order_state_change_state"];
        $order_state_delete_tasks = $row["order_state_delete_tasks"];
        $order_state_order_state_type = $row["order_state_order_state_type"];
        
        $order_state_fields = array();
        $order_state_values = array();
    
        // insert record into table orders

        $order_state_fields[] = "actual_order_state_order";
        $order_state_values[] = $order_id;

        $order_state_fields[] = "actual_order_state_state";
        $order_state_values[] = $row["order_state_id"];

        $order_state_fields[] = "actual_order_state_user";
        $order_state_values[] = user_id();

        $order_state_fields[] = "date_created";
        $order_state_values[] = "current_timestamp";

        $order_state_fields[] = "date_modified";
        $order_state_values[] = "current_timestamp";

        if (isset($_SESSION["user_login"]))
        {
            $order_state_fields[] = "user_created";
            $order_state_values[] = dbquote($_SESSION["user_login"]);

            $order_state_fields[] = "user_modified";
            $order_state_values[] = dbquote($_SESSION["user_login"]);
        }


        $sql = "insert into actual_order_states (" . join(", ", $order_state_fields) . ") values (" . join(", ", $order_state_values) . ")";
        mysql_query($sql) or dberror($sql);
    }

    if ($order_state_change_state == 1 and $do_update == 1)
    {
        $can_update = 1;
        
        // check if all partners have confirmed

        if($order_state_code == DELIVERY_CONFIRMED_FRW)
        {
            // delivery confirmation by forwarder
            $sql = "select count(order_item_id) as num_recs " .
                   "from order_items " .
                   "where order_item_order = " . $order_id .
                   "  and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                   "  and (order_item_arrival is null or order_item_arrival = '0000-00-00') " .
                   "  and (order_item_not_in_budget is null or order_item_not_in_budget = 0)";
        
        
            $res = mysql_query($sql) or dberror($sql);
    
            if ($row = mysql_fetch_assoc($res))
            {
                if($row["num_recs"] > 0)
                {
                    $can_update = 0;
                }
            }
        }
        elseif($order_state_code == REQUEST_FOR_DELIVERY_ACCEPTED)
        {
            // request for delivery accepted by forwarder
            $sql = "select count(order_item_id) as num_recs " .
                   "from order_items " .
                   "where order_item_order = " . $order_id .
                   "  and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                   "  and (order_item_expected_arrival is null or order_item_expected_arrival = '0000-00-00') " .
                   "  and (order_item_not_in_budget is null or order_item_not_in_budget = 0)";
        
        
            $res = mysql_query($sql) or dberror($sql);
    
            if ($row = mysql_fetch_assoc($res))
            {
                if($row["num_recs"] > 0)
                {
                    $can_update = 0;
                }
            }
        }
        elseif($order_state_code == CONFIRM_ORDER_BY_SUPPLIER)
        {
            // order confrimation by supplier
            $sql = "select count(order_item_id) as num_recs " .
                   "from order_items " .
                   "where order_item_order = " . $order_id .
                   "  and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                   "  and (order_item_ready_for_pickup is null or order_item_ready_for_pickup = '0000-00-00') " .
                   "  and (order_item_not_in_budget is null or order_item_not_in_budget = 0)";
        
        
            $res = mysql_query($sql) or dberror($sql);
    
            if ($row = mysql_fetch_assoc($res))
            {
                if($row["num_recs"] > 0)
                {
                    $can_update = 0;
                }
            }
        }

        // update table orders
        if($can_update == 1)
        {
			// never fall back if budget has been approved
            $sql = "select order_actual_order_state_code " .
                   "from orders where order_id = " . $order_id;

            $res = mysql_query($sql) or dberror($sql);
            $row = mysql_fetch_assoc($res);

            $actual_order_state_code = $row["order_actual_order_state_code"];
            
            if($actual_order_state_code < 620 and $keep_project_state == 0)
            {
				$sql = "update orders set order_actual_order_state_code = " . $order_state_code . " where order_id = " . $order_id;
                mysql_query($sql) or dberror($sql);
            }
            elseif($actual_order_state_code >= 620 and $order_state_code > 620 and $keep_project_state == 0)
            {
				 $sql = "update orders set order_actual_order_state_code = " . $order_state_code . " where order_id = " . $order_id;
                 mysql_query($sql) or dberror($sql);
            }
        }
    }

}


/********************************************************************
    append records to table order_mails
*********************************************************************/
function append_mail($order_id, $recepient_id, $user_id, $text, $order_state_code, $type, $order_revision=0)
{

    $text = str_replace("\n\n", "\n", $text);
	$text = str_replace("\n\n", "\n", $text);

	$sql = "select order_state_id " .
           "from order_states ".
           "left join order_state_groups on order_state_group = order_state_group_id ".
           "where order_state_code = " . dbquote($order_state_code) .
           "    and order_state_group_order_type = " .$type;

    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);

    $order_mail_fields = array();
    $order_mail_values = array();
    
    // insert record into table tasks

    $order_mail_fields[] = "order_mail_order";
    $order_mail_values[] = $order_id;

    if ($recepient_id != "")
    {
        $order_mail_fields[] = "order_mail_user";
        $order_mail_values[] = $recepient_id;
    }

    $order_mail_fields[] = "order_mail_from_user";
    $order_mail_values[] = $user_id;

    $order_mail_fields[] = "order_mail_text";
    $order_mail_values[] = dbquote($text);

    if ($order_state_code != "")
    {
        $order_mail_fields[] = "order_mail_order_state";
        $order_mail_values[] = $row["order_state_id"];
    }

	$order_mail_fields[] = "order_revisioned";
    $order_mail_values[] = $order_revision;

    $order_mail_fields[] = "date_created";
    $order_mail_values[] = "current_timestamp";

    $order_mail_fields[] = "date_modified";
    $order_mail_values[] = "current_timestamp";

    if (isset($_SESSION["user_login"]))
    {
        $order_mail_fields[] = "user_created";
        $order_mail_values[] = dbquote($_SESSION["user_login"]);

        $order_mail_fields[] = "user_modified";
        $order_mail_values[] = dbquote($_SESSION["user_login"]);
    }

    $sql = "insert into order_mails (" . join(", ", $order_mail_fields) . ") values (" . join(", ", $order_mail_values) . ")";
        mysql_query($sql) or dberror($sql);

}



/********************************************************************
    append record to task list
*********************************************************************/
function append_task($order_id, $user_id, $text, $url, $due_date, $from_user, $order_state_code, $type, $keep_order_state = 0)
{

        $task_fields = array();
        $task_values = array();

        $order_state_id = null;
        $order_state_predecessor = "";
        $order_state_delete_tasks = 0;
        $order_state_manually_deleted = 0;

        // get order_state_id
        $sql = "select order_state_id, order_state_predecessor, ".
               "    order_state_delete_tasks, order_state_manually_deleted " .
               "from order_states ".
               "left join order_state_groups on order_state_group = order_state_group_id ".
               "where order_state_code = " . dbquote($order_state_code) .
               "    and order_state_group_order_type = " .$type;
        
        $res = mysql_query($sql) or dberror($sql);
    
        if ($row = mysql_fetch_assoc($res))
        {
            $order_state_id = $row["order_state_id"];
            $order_state_predecessor = $row["order_state_predecessor"];
            $order_state_delete_tasks = $row["order_state_delete_tasks"];
            $order_state_manually_deleted = $row["order_state_manually_deleted"];
        }

        // insert record into table tasks

        $task_fields[] = "task_order";
        $task_values[] = $order_id;

        $task_fields[] = "task_order_state";
        $task_values[] = $order_state_id;

        $task_fields[] = "task_user";
        $task_values[] = $user_id;

        $task_fields[] = "task_from_user";
        $task_values[] = $from_user;

        $task_fields[] = "task_text";
        $task_values[] = dbquote($text);

        $task_fields[] = "task_url";
        $task_values[] = dbquote($url);

        $task_fields[] = "task_due_date";
        $task_values[] = dbquote($due_date);

        $task_fields[] = "task_delete_task";
        $task_values[] = $order_state_delete_tasks;

        $task_fields[] = "task_manually_deleted";
        $task_values[] = $order_state_manually_deleted;

		$task_fields[] = "task_keep_order_state";
        $task_values[] = $keep_order_state;
		

        $task_fields[] = "date_created";
        $task_values[] = "current_timestamp";

        $task_fields[] = "date_modified";
        $task_values[] = "current_timestamp";

        if (isset($_SESSION["user_login"]))
        {
            $task_fields[] = "user_created";
            $task_values[] = dbquote($_SESSION["user_login"]);

            $task_fields[] = "user_modified";
            $task_values[] = dbquote($_SESSION["user_login"]);
        }

        $sql = "insert into tasks (" . join(", ", $task_fields) . ") values (" . join(", ", $task_values) . ")";
        mysql_query($sql) or dberror($sql);

}

/********************************************************************
    append record to task list set task_manually_deleted to 0
*********************************************************************/
function append_task2($order_id, $user_id, $text, $url, $due_date, $from_user, $order_state_code, $type)
{

        $task_fields = array();
        $task_values = array();

        $order_state_id = null;
        $order_state_predecessor = "";
        $order_state_delete_tasks = 0;
        $order_state_manually_deleted = 0;

        // get order_state_id
        $sql = "select order_state_id, order_state_predecessor, ".
               "    order_state_delete_tasks, order_state_manually_deleted " .
               "from order_states ".
               "left join order_state_groups on order_state_group = order_state_group_id ".
               "where order_state_code = " . dbquote($order_state_code) .
               "    and order_state_group_order_type = " .$type;
        
        $res = mysql_query($sql) or dberror($sql);
    
        if ($row = mysql_fetch_assoc($res))
        {
            $order_state_id = $row["order_state_id"];
            $order_state_predecessor = $row["order_state_predecessor"];
            $order_state_delete_tasks = $row["order_state_delete_tasks"];
            $order_state_manually_deleted = $row["order_state_manually_deleted"];
        }

        // insert record into table tasks

        $task_fields[] = "task_order";
        $task_values[] = $order_id;

        $task_fields[] = "task_order_state";
        $task_values[] = $order_state_id;

        $task_fields[] = "task_user";
        $task_values[] = $user_id;

        $task_fields[] = "task_from_user";
        $task_values[] = $from_user;

        $task_fields[] = "task_text";
        $task_values[] = dbquote($text);

        $task_fields[] = "task_url";
        $task_values[] = dbquote($url);

        $task_fields[] = "task_due_date";
        $task_values[] = dbquote($due_date);

        $task_fields[] = "task_delete_task";
        $task_values[] = $order_state_delete_tasks;

        $task_fields[] = "date_created";
        $task_values[] = "current_timestamp";

        $task_fields[] = "date_modified";
        $task_values[] = "current_timestamp";

        if (isset($_SESSION["user_login"]))
        {
            $task_fields[] = "user_created";
            $task_values[] = dbquote($_SESSION["user_login"]);

            $task_fields[] = "user_modified";
            $task_values[] = dbquote($_SESSION["user_login"]);
        }

        $sql = "insert into tasks (" . join(", ", $task_fields) . ") values (" . join(", ", $task_values) . ")";
        mysql_query($sql) or dberror($sql);

}


/********************************************************************
    delete all tasks of an order and a user
*********************************************************************/
function delete_user_tasks($order_id, $user_id, $text, $url, $due_date, $from_user, $order_state_code, $type)
{

        $order_state_id = null;
        $order_state_predecessor = "";

        // get order_state_id
        $sql = "select order_state_id, order_state_predecessor, ".
               "    order_state_delete_tasks, order_state_manually_deleted " .
               "from order_states ".
               "left join order_state_groups on order_state_group = order_state_group_id ".
               "where order_state_code = " . dbquote($order_state_code) .
               "    and order_state_group_order_type = " . $type;
        
        $res = mysql_query($sql) or dberror($sql);
    
        if ($row = mysql_fetch_assoc($res))
        {
            $order_state_id = $row["order_state_id"];
            $order_state_predecessor = $row["order_state_predecessor"];
            $order_state_delete_tasks = $row["order_state_delete_tasks"];
            $order_state_manually_deleted = $row["order_state_manually_deleted"];
        }

    
        if ($order_state_delete_tasks == 1)
        {
            // delete predecessor task
            $sql = "delete from tasks ".
                   "where task_user = " . $from_user .
                   "   and task_order = " . $order_id .
                   "   and task_manually_deleted <> 1";
            mysql_query($sql) or dberror($sql);
        }
}


/********************************************************************
    save task data
*********************************************************************/
function save_task_data($form)
{
   $task_fields = array();

   $value = dbquote(from_system_date($form->value("done_date")));
   $task_fields[] = "task_done_date = " . $value;

   $value = "current_timestamp";
   $task_fields[] = "date_modified = " . $value;

    if (isset($_SESSION["user_login"]))
    {
       $value = dbquote($_SESSION["user_login"]);
       $task_fields[] = "user_modified = " . $value;
    }

    $sql = "update tasks set " . join(", ",$task_fields) . " where task_id = " .  $form->value("tid");
    mysql_query($sql) or dberror($sql);
}


/********************************************************************
    update task done date
*********************************************************************/
function  update_task_done_date($id)
{
   $task_fields = array();

   $value = "current_timestamp";
   $task_fields[] = "task_done_date = " . $value;

   $value = "current_timestamp";
   $task_fields[] = "date_modified = " . $value;

    if (isset($_SESSION["user_login"]))
    {
       $value = dbquote($_SESSION["user_login"]);
       $task_fields[] = "user_modified = " . $value;
    }

    $sql = "update tasks set " . join(", ",$task_fields) . " where task_id = " .  $id;
    mysql_query($sql) or dberror($sql);
}


/********************************************************************
    delete task
*********************************************************************/
function  delete_task($id)
{

    $sql = "delete from tasks where task_id = " .  $id;
    mysql_query($sql) or dberror($sql);
}

/********************************************************************
    set archive date
*********************************************************************/
function  set_archive_date($order_id)
{
   $order_fields = array();

   $value = "current_timestamp";
   $order_fields[] = "order_archive_date = " . $value;

   $value = "current_timestamp";
   $order_fields[] = "date_modified = " . $value;

    if (isset($_SESSION["user_login"]))
    {
       $value = dbquote($_SESSION["user_login"]);
       $order_fields[] = "user_modified = " . $value;
    }

    $sql = "update orders set " . join(", ",$order_fields) . " where order_id = " .  $order_id;
    mysql_query($sql) or dberror($sql);

    // delete all pending tasks
    $sql = "delete from tasks where task_order = ". $order_id;
    mysql_query($sql) or dberror($sql);
}

/********************************************************************
    update visibility for supplöiers
    when project booklet is approved by client
*********************************************************************/
function set_visibility_for_suppliers($order_id, $address_id)
{
    $sql = "update orders set order_show_to_suppliers = 1 where order_id = " .  $order_id;
    mysql_query($sql) or dberror($sql);

    if ($address_id)
    {
        $sql = "update order_items ".
               "set order_item_show_to_suppliers = 1 ".
               "where order_item_order = " .  $order_id .
               "    and order_item_supplier_address = " . $address_id;

        mysql_query($sql) or dberror($sql);
    }
    else
    {
        $sql = "update order_items ".
               "set order_item_show_to_suppliers = 1 ".
               "where order_item_order = " .  $order_id;

        mysql_query($sql) or dberror($sql);
    }

}

/********************************************************************
    update visibility in delivery schedule
    when budget is approved by client
*********************************************************************/
function set_visibility_in_delivery_schedule($order_id)
{
    $sql = "update orders set order_show_in_delivery = 1 where order_id = " .  $order_id;
    mysql_query($sql) or dberror($sql);

}

/********************************************************************
    set order to cancelled
*********************************************************************/
function set_order_to_cancelled($order_id)
{
    $sql = "update orders set order_cancelled = 1 where order_id = " .  $order_id;
    mysql_query($sql) or dberror($sql);

	$sql = "update projects set project_state = 6 where project_order = " .  $order_id;
    mysql_query($sql) or dberror($sql);

}

/********************************************************************
    update order items with exchange rates
*********************************************************************/
function  update_exchange_rates($order_id)
{
    $sql = "select order_client_currency ".
           "from orders ".
           "where order_id = " . $order_id;

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $currency_id = $row["order_client_currency"];
    }

    $sql = "select currency_exchange_rate, currency_factor ".
           "from currencies ".
           "where currency_id = " . $currency_id ;

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $currency_exchange_rate = $row["currency_exchange_rate"];
        $currency_factor = $row["currency_factor"];
    }


    $sql = "update orders ".
           "set order_client_exchange_rate = " . $currency_exchange_rate .
           " where order_id = " .  $order_id;

    mysql_query($sql) or dberror($sql);

    $sql = "select order_item_id, order_item_system_price ".
           "from order_items ".
           "where order_item_order = " . $order_id;

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $client_price = $row["order_item_system_price"] / $currency_exchange_rate * $currency_factor;
        
        $sql2 = "update order_items ".
                "set order_item_client_price = " . $client_price .
                " where order_item_id = " .  $row["order_item_id"];

        mysql_query($sql2) or dberror($sql2);

    }
}


/********************************************************************
    update budget state
*********************************************************************/
function update_budget_state($order_id, $state)
{
    $sql = "update orders set order_budget_is_locked = '" . $state . "' where order_id = " .  $order_id;
    mysql_query($sql) or dberror($sql);

	//update user_tracking
	$text = "User has changed budget state of order " . $order_id . " to " . $state;
	$sql = 'insert into user_tracking (user_tracking_user, user_tracking_track, date_created, user_created) VALUES (' .
		   user_id() . ', "' . $text . '", "' . date("Y-m-d H:i:s") . '", "' . user_login() . '")';

	 mysql_query($sql) or dberror($sql);


}


/********************************************************************
    freeze budget
*********************************************************************/
function freeze_budget($order_id)
{
    
	//update order items with predefined cost_groups
	$sql = "select order_item_id, order_item_item, item_cost_group, item_costmonitoring_group " . 
		   "from order_items " . 
		   "left join items on item_id = order_item_item " .
		   "where order_item_order = " . $order_id . 
		   "  and order_item_cost_group = 0 and order_item_not_in_budget <> 1 " .
		   "  and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION . " or order_item_type >= " . ITEM_TYPE_LOCALCONSTRUCTIONCOST . ") ";


	/*
	$sql = "select item_code, order_item_id, order_item_item, item_cost_group, item_costmonitoring_group " . 
		   "from order_items " . 
		   "left join items on item_id = order_item_item " .
		   "where order_item_order = " . $order_id . 
		   "  and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION . " or order_item_type >= " . ITEM_TYPE_LOCALCONSTRUCTIONCOST . ") ";

	*/

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$sql_u = "update order_items Set order_item_cost_group =  " . dbquote($row["item_cost_group"]) . " " . 
				 " where (order_item_cost_group is NULL or order_item_cost_group = 0) " .
				 "  and order_item_id = " . $row["order_item_id"];

		mysql_query($sql_u) or dberror($sql_u);

		$sql_u = "update order_items Set order_items_costmonitoring_group =  " . dbquote($row["item_costmonitoring_group"]) . " " . 
				 " where (order_items_costmonitoring_group is NULL or order_items_costmonitoring_group = 0) " .
				 "  and order_item_id = " . $row["order_item_id"];

		mysql_query($sql_u) or dberror($sql_u);
	}
	
	$budget = 0;
	$cost_groups = array();
	$cost_groups_lc = array();
	$cost_groups[2] = 0; // HQ Supplied items
	$cost_groups[6] = 0; // Freight Charges
	$cost_groups_lc[2] = 0; // HQ Supplied items in LOC
	$cost_groups_lc[6] = 0; // Freight Charges in LOC


    $sql =  "select order_item_id, order_item_text, order_item_quantity, order_item_cost_group, " .
            "order_item_system_price, order_item_client_price " .
            "from order_items " . 
            " where order_item_not_in_budget <> 1 " .
            "  and order_item_order = " . $order_id;

    $res = mysql_query($sql) or dberror($sql);
    while($row = mysql_fetch_assoc($res))
    {
        if($row["order_item_quantity"] > 0)
        {
			$budget = $budget + $row["order_item_quantity"] * $row["order_item_system_price"];
			
			if(array_key_exists($row["order_item_cost_group"], $cost_groups))
			{
				$cost_groups[$row["order_item_cost_group"]] = $cost_groups[$row["order_item_cost_group"]]  + ($row["order_item_quantity"] * $row["order_item_system_price"]);
			}
			else
			{
				$cost_groups[$row["order_item_cost_group"]] = $row["order_item_quantity"] * $row["order_item_system_price"];
			}

			if(array_key_exists($row["order_item_cost_group"], $cost_groups_lc))
			{
				$cost_groups_lc[$row["order_item_cost_group"]] = $cost_groups_lc[$row["order_item_cost_group"]]  + ($row["order_item_quantity"] * $row["order_item_client_price"]);
			}
			else
			{
				$cost_groups_lc[$row["order_item_cost_group"]] = $row["order_item_quantity"] * $row["order_item_client_price"];
			}
        }
        else
        {
            $budget = $budget + $row["order_item_system_price"];

			if(array_key_exists($row["order_item_cost_group"], $cost_groups))
			{
				$cost_groups[$row["order_item_cost_group"]] = $cost_groups[$row["order_item_cost_group"]]  +  $row["order_item_system_price"];
			}
			else
			{
				$cost_groups[$row["order_item_cost_group"]] = $row["order_item_system_price"];
			}

			if(array_key_exists($row["order_item_cost_group"], $cost_groups_lc))
			{
				$cost_groups_lc[$row["order_item_cost_group"]] = $cost_groups_lc[$row["order_item_cost_group"]]  +  $row["order_item_client_price"];
			}
			else
			{
				$cost_groups_lc[$row["order_item_cost_group"]] = $row["order_item_client_price"];
			}
        }

        $quantity = $row["order_item_quantity"];
        $sprice = $row["order_item_system_price"];
        $cprice = $row["order_item_client_price"];

        if(!$quantity){$quantity = 0;}
        if(!$sprice){$sprice = 0;}
        if(!$cprice){$cprice = 0;}

        $sql_u = "Update order_items set " .
                 "order_item_in_freezed_budget = 1, " .
                 "order_item_quantity_freezed = " . $quantity  . ", " .
                 "order_item_system_price_freezed = " . $sprice . ", " .
                 "order_item_client_price_freezed = " . $cprice . " " .
                 "where order_item_id = " . $row["order_item_id"];
        
        $result = mysql_query($sql_u) or dberror($sql_u);
    }


    //check if the cost monitoring sheet exists already
    $sql = "select count(project_cost_id) as num_recs " .
           "from project_costs " .
           "where project_cost_order = "  . $order_id; 

    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);

    if($row["num_recs"] == 0)
    {
        
        $fields = array();
        $values = array();

        $fields[] = "project_cost_order";
        $values[] = $order_id;

        $fields[] = "date_created";
        $values[] = dbquote(date("Y-m-d"));

        $fields[] = "date_modified";
        $values[] = dbquote(date("Y-m-d"));

        $fields[] = "user_created";
        $values[] = dbquote(user_login());

        $fields[] = "user_modified";
        $values[] = dbquote(user_login());
        
        $sql = "insert into project_costs (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

        mysql_query($sql) or dberror($sql);
    }

	//update costgroups
	$sql = "update project_costs " .
           "set project_cost_budget =  " . $budget . ", " .
		   "project_cost_hq_supplied = " . $cost_groups[2] . ", " .
		   "project_cost_hq_supplied_lc = " . $cost_groups_lc[2] . ", " .
		   "project_cost_freight_charges = " . $cost_groups[6] . ", " .
		   "project_cost_freight_charges_lc = " . $cost_groups_lc[6] . 
           " where project_cost_order = " . $order_id;


	mysql_query($sql) or dberror($sql);


    $sql = "update orders " .
           "set order_budget_freezed_date =  " . dbquote(date("Y-m-d")) . 
           " where order_id = " . $order_id;

    mysql_query($sql) or dberror($sql);



	//new version of costsheets
	$sql = "select project_id from projects " . 
		   " where project_order = " . dbquote($order_id);
	$res = mysql_query($sql) or dberror($sql);
    if($row = mysql_fetch_assoc($res))
    {
		$sql = "update costsheets set costsheet_budget_approved_amount = costsheet_budget_amount " . 
			   " where costsheet_is_in_budget = 1 and costsheet_project_id = " . $row["project_id"];

		 mysql_query($sql) or dberror($sql);
	}


	//update user_tracking
	$text = "User has freezed budget of order " . $order_id;
	$sql = 'insert into user_tracking (user_tracking_user, user_tracking_track, date_created, user_created) VALUES (' .
		   user_id() . ', "' . $text . '", "' . date("Y-m-d H:i:s") . '", "' . user_login() . '")';

	mysql_query($sql) or dberror($sql);


}


/********************************************************************
    freeze budget
*********************************************************************/
function project_sheet_update_cost_groups($order_id)
{
	//update order items with predefined cost_groups
	$sql = "select order_item_id, order_item_item, item_cost_group, item_costmonitoring_group " . 
		   "from order_items " . 
		   "left join items on item_id = order_item_item " .
		   "where order_item_order = " . $order_id . 
		   "  and order_item_not_in_budget <> 1 " .
		   "  and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION . " or order_item_type >= " . ITEM_TYPE_LOCALCONSTRUCTIONCOST . ") ";


	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$sql_u = "update order_items Set order_item_cost_group =  " . dbquote($row["item_cost_group"]) . " " . 
				 " where (order_item_cost_group is NULL or order_item_cost_group = 0) " .
				 "  and order_item_id = " . $row["order_item_id"];

		mysql_query($sql_u) or dberror($sql_u);

		$sql_u = "update order_items Set order_items_costmonitoring_group =  " . dbquote($row["item_costmonitoring_group"]) . " " . 
				 " where (order_items_costmonitoring_group is NULL or order_items_costmonitoring_group = 0) " .
				 "  and order_item_id = " . $row["order_item_id"];

		mysql_query($sql_u) or dberror($sql_u);
	}
	
	$budget = 0;
	$cost_groups = array();
	$cost_groups_lc = array();
	$cost_groups[2] = 0; // HQ Supplied items
	$cost_groups[6] = 0; // Freight Charges
	$cost_groups_lc[2] = 0; // HQ Supplied items in LOC
	$cost_groups_lc[6] = 0; // Freight Charges in LOC


    $sql =  "select order_item_id, order_item_quantity, order_item_cost_group, " .
            "order_item_system_price, order_item_client_price " .
            "from order_items " . 
            " where order_item_not_in_budget <> 1 " .
            "  and order_item_order = " . $order_id;

    $res = mysql_query($sql) or dberror($sql);
    while($row = mysql_fetch_assoc($res))
    {
        if($row["order_item_quantity"] > 0)
        {
            $budget = $budget + $row["order_item_quantity"] * $row["order_item_system_price"];
			
			if(array_key_exists($row["order_item_cost_group"], $cost_groups))
			{
				$cost_groups[$row["order_item_cost_group"]] = $cost_groups[$row["order_item_cost_group"]]  + ($row["order_item_quantity"] * $row["order_item_system_price"]);
			}
			else
			{
				$cost_groups[$row["order_item_cost_group"]] = $row["order_item_quantity"] * $row["order_item_system_price"];
			}

			if(array_key_exists($row["order_item_cost_group"], $cost_groups_lc))
			{
				$cost_groups_lc[$row["order_item_cost_group"]] = $cost_groups_lc[$row["order_item_cost_group"]]  + ($row["order_item_quantity"] * $row["order_item_client_price"]);
			}
			else
			{
				$cost_groups_lc[$row["order_item_cost_group"]] = $row["order_item_quantity"] * $row["order_item_client_price"];
			}
        }
        else
        {
            $budget = $budget + $row["order_item_system_price"];

			if(array_key_exists($row["order_item_cost_group"], $cost_groups))
			{
				$cost_groups[$row["order_item_cost_group"]] = $cost_groups[$row["order_item_cost_group"]]  +  $row["order_item_system_price"];
			}
			else
			{
				$cost_groups[$row["order_item_cost_group"]] = $row["order_item_system_price"];
			}

			if(array_key_exists($row["order_item_cost_group"], $cost_groups_lc))
			{
				$cost_groups_lc[$row["order_item_cost_group"]] = $cost_groups_lc[$row["order_item_cost_group"]]  +  $row["order_item_client_price"];
			}
			else
			{
				$cost_groups_lc[$row["order_item_cost_group"]] = $row["order_item_client_price"];
			}
        }

    }


	//update costgroups
	$sql = "update project_costs " .
           "set project_cost_budget =  " . $budget . ", " .
		   "project_cost_hq_supplied = " . $cost_groups[2] . ", " .
		   "project_cost_hq_supplied_lc = " . $cost_groups_lc[2] . ", " .
		   "project_cost_freight_charges = " . $cost_groups[6] . ", " .
		   "project_cost_freight_charges_lc = " . $cost_groups_lc[6] . 
           " where project_cost_order = " . $order_id;


	mysql_query($sql) or dberror($sql);


    $sql = "update orders " .
           "set order_budget_freezed_date =  " . dbquote(date("Y-m-d")) . 
           " where order_id = " . $order_id;

    mysql_query($sql) or dberror($sql);

}

/********************************************************************
    delete design objective items of a project
*********************************************************************/
function delete_design_objective_items($project_id)
{
    $sql = "delete from project_items where project_item_project = " . $project_id;
    mysql_query($sql) or dberror($sql);

}

/********************************************************************
    update project type
*********************************************************************/
function project_update_project_product_line($project_id, $project_line)
{
    $sql = "update projects set project_product_line = " . $project_line . " where project_id = " .  $project_id;
    mysql_query($sql) or dberror($sql);

}


/********************************************************************
    delete all items of an order
*********************************************************************/
function order_delete_all_items($id, $type)
{
    if($type == ITEM_TYPE_STANDARD)
	{
		$filter = " and (order_item_type = " . ITEM_TYPE_STANDARD . " or order_item_type = " . ITEM_TYPE_SERVICES . ") ";
	}
	else
	{
		$filter = " and order_item_type = " . $type;
	}
	
	$sql = "select order_item_id " .
           "from order_items " .
           "where order_item_order = " . $id . " " . $filter;
        
    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $item_id = $row["order_item_id"];
        delete_record("order_items", $item_id); // cascaded deletion
    }
}

/********************************************************************
    unfreeze project budget
*********************************************************************/
function unfreeze_project_budget($order_id)
{

    $sql = "update orders set order_budget_is_locked = 0 where order_id = " .  $order_id;
    mysql_query($sql) or dberror($sql);

	//update user_tracking
	$text = "User has changed budget state of order " . $order_id . " to 0 by unfreezing the budget";
	$sql = 'insert into user_tracking (user_tracking_user, user_tracking_track, date_created, user_created) VALUES (' .
		   user_id() . ', "' . $text . '", "' . date("Y-m-d H:i:s") . '", "' . user_login() . '")';

	 mysql_query($sql) or dberror($sql);


    $sql = "update order_items set " .
           "order_item_in_freezed_budget = 0, " .
           "order_item_quantity_freezed = '0000-00-00', " .
           "order_item_system_price_freezed = NULL, " .
           "order_item_client_price_freezed = NULL " .
           " where order_item_order = " . $order_id;

    mysql_query($sql) or dberror($sql);


    $sql = "update orders " .
           "set order_budget_freezed_date =  Null, order_budget_unfreezed_date = '" . date('Y-m-d') . "', order_budget_unfreezed_by = " . user_id() . 
           " where order_id = " . $order_id;

    mysql_query($sql) or dberror($sql);
	


	//new version of costsheets
	$sql = "select project_id from projects " . 
		   " where project_order = " . dbquote($order_id);
	$res = mysql_query($sql) or dberror($sql);
    if($row = mysql_fetch_assoc($res))
    {
		$sql = "update costsheets set costsheet_budget_approved_amount = 0 " . 
			   " where costsheet_is_in_budget = 1 and costsheet_project_id = " . $row["project_id"];

		 mysql_query($sql) or dberror($sql);
	}

}


/********************************************************************
    Send mail notifications for projects
*********************************************************************/
function project_send_mail_notifications($project_id)
{

	$recepients = array();
	//check if projekt is a lease renewal
	$sql = "select project_projectkind, project_number, project_product_line, project_postype, project_order, order_user, " .
		   "order_shop_address_company, order_shop_address_place, order_shop_address_country, country_name, " . 
		   "user_firstname, user_name, user_email, user_id " . 
		   "from projects " . 
		   "left join orders on order_id = project_order " .
		   "left join countries on country_id = order_shop_address_country " .
		   "left join users on user_id = order_user " .
		   "where project_id = " . $project_id;

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		if($row["project_projectkind"] == 5 or $row["project_projectkind"] == 4) // lease renewal or take over
		{

			$project_kind = $row["project_projectkind"];
			$pos_country = $row["order_shop_address_country"];
			$project_postype = $row["project_postype"];
			$project_product_line = $row["project_product_line"];
			$project_number = $row["project_number"];
			
			$sender_email = $row["user_email"];
			$sender_name =  $row["user_name"] . " " . $row["user_firstname"];
			$order_id = $row["project_order"];
			$order_user = $row["order_user"];
			
			$recipient_user_ids = array();
			$mailaddress = 0;
			
			if($project_kind == 4) {
				$subject = MAIL_SUBJECT_PREFIX . ": Take Over - Project " . $project_number . ", " . $row["country_name"] . ", " .  $row["order_shop_address_company"];

				$bodytext0 = "A new take over project has been added by " . $sender_name;
			}
			else
			{
				$subject = MAIL_SUBJECT_PREFIX . ": Lease Renewal - Project " . $project_number . ", " . $row["country_name"] . ", " .  $row["order_shop_address_company"];

				$bodytext0 = "A new lease renewal project has been added by " . $sender_name;
			}

			//send mal to project kind notification recipients
			$sql = 'select * from projectkinds ' . 
				   'where projectkind_id = ' . $project_kind . 
				   ' and (projectkind_email1 is not null or projectkind_email2 is not null)';
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$recipient1 = $row["projectkind_email1"];
				$recipient2 = $row["projectkind_email2"];
				$recipient3 = $row["projectkind_email3"];
				
				if($recipient1)
				{
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($recipient1);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[$recipient1] = $row_u["user_id"];
					}
					else
					{
						$recipient_user_ids[$recipient1] = 0;
					}
					$mailaddress = 1;
					
				}

				if($recipient2)
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($recipient2);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[$recipient2] = $row_u["user_id"];
					}
					else
					{
						$recipient_user_ids[$recipient2] = 0;
					}
					$mailaddress = 1;
					
				}

				if($recipient3)
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($recipient3);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[$recipient3] = $row_u["user_id"];
					}
					else
					{
						$recipient_user_ids[$recipient3] = 0;
					}
					$mailaddress = 1;
					
				}
			}

			
			// send email notification for new projects
			$sql = 'select * from projecttype_newproject_notifications ' . 
				   'where projecttype_newproject_notification_on_new_project = 1 ' . 
				   ' and projecttype_newproject_notification_country = ' . $pos_country . 
				   ' and projecttype_newproject_notification_postype = ' . $project_postype;
			

			$res = mysql_query($sql) or dberror($sql);
			while ($row = mysql_fetch_assoc($res))
			{
				if($row["projecttype_newproject_notification_email"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_email"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_email"]] = $row_u["user_id"];
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_email"]] = 0;
					}
					$mailaddress = 1;
					
				}

				if($row["projecttype_newproject_notification_emailcc1"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc1"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc1"]] = $row_u["user_id"];
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc1"]] = 0;
					}
				}
				if($row["projecttype_newproject_notification_emailcc2"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc2"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc2"]] = $row_u["user_id"];
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc2"]] = 0;
					}
					
				}
				if($row["projecttype_newproject_notification_emailcc3"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc3"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc3"]] = $row_u["user_id"];
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc3"]] = 0;
					}
					
				}
				if($row["projecttype_newproject_notification_emailcc4"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc4"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc4"]] = $row_u["user_id"];
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc4"]] = 0;
					}
				
				}
				if($row["projecttype_newproject_notification_emailcc5"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc5"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc5"]] = $row_u["user_id"];
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc5"]] = 0;
					}
				
				}
				if($row["projecttype_newproject_notification_emailcc6"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc6"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc6"]] = $row_u["user_id"];
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc6"]] = 0;
					}
				
				}
				if($row["projecttype_newproject_notification_emailcc7"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc7"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc7"]] = $row_u["user_id"];
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc7"]] = 0;
					}
				}
			}


			$mail = new Mail();
			$mail->set_subject($subject);
			$mail->set_sender($sender_email, $sender_name);

			

			foreach($recipient_user_ids as $key=>$user_id)
			{
				$mail->add_recipient($key);
				$recipients_already_served[$key] = $user_id;
			}
			
			$link ="project_view_client_data.php?pid=" . $project_id;
			$bodytext = $bodytext0 . "\nclick below to have direct access to the project:\n";
			$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
			$mail->add_text($bodytext);
			
			if($mailaddress == 1)
			{
				$result = $mail->send();
				foreach($recipient_user_ids as $key=>$user_id)
				{
					if($user_id > 0)
					{
						append_mail($order_id, $user_id,  user_id(), $bodytext, "100", 1,0);
					}
				}
			}
		}
		else
		{
	
			// send email notification to design supervisor
			$sql = "select project_number, project_postype, project_order, order_user, " .
				   "order_shop_address_company, order_shop_address_place, country_name, " .
				   "user_firstname, user_name, user_email, user_id from projects " . 
				   "left join orders on order_id = project_order " .
				   "left join countries on country_id = order_shop_address_country " .
				   "left join users on user_id = order_user " .
				   "where project_id = " . $project_id;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$project_postype = $row["project_postype"];
				$order_id = $row["project_order"];
				$order_user = $row["order_user"];
				$recipient_user_ids = array();

				$subject = MAIL_SUBJECT_PREFIX . ": New layout request - Project " . $row["project_number"] . ", " . $row["country_name"] . ", " .  $row["order_shop_address_company"];

				$sender_email = $row["user_email"];
				$sender_name =  $row["user_name"] . " " . $row["user_firstname"];

				$bodytext0 = "You were assigned as design supervisor by " . $sender_name;

				$mailaddress = 0;

				$mail = new Mail();
				$mail->set_subject($subject);
				$mail->set_sender($sender_email, $sender_name);

				$sql = "select user_id, user_email from projects " . 
					   "left join users on user_id = project_design_supervisor " .
					   "where project_id = " . $project_id;

				$res = mysql_query($sql) or dberror($sql);
				while ($row = mysql_fetch_assoc($res))
				{
					if($row["user_email"])
					{
						$mailaddress = 1;
						$recipient_user_ids[$row["user_email"]] = $row["user_id"];
					}
				}

				foreach($recipient_user_ids as $key=>$user_id)
				{
					$mail->add_recipient($key);
				}
				
				$link ="project_view_client_data.php?pid=" . $project_id;
				$bodytext = $bodytext0 . "\nclick below to have direct access to the project:\n";
				$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
				$mail->add_text($bodytext);
				
				if($mailaddress == 1)
				{
					$mail->send();
					
					foreach($recipient_user_ids as $key=>$user_id)
					{
						if($user_id > 0)
						{
							append_mail($order_id, $user_id,  user_id(), $bodytext, "100", 1,0);
						}
					}
				}
				

			}
			
			// send email notification for product lines and legal types
			$recipients_already_served = array();
			
			$sql = "select project_number, project_product_line, project_order, order_user, " .
				   "order_shop_address_company, order_shop_address_place, country_name, " .
				   "user_firstname, user_name, user_email, user_id, project_cost_type " . 
				   "from projects " . 
				   "left join orders on order_id = project_order " .
				   "left join countries on country_id = order_shop_address_country " .
				   "left join users on user_id = order_user " .
				   "left join project_costs on project_cost_order = project_order " . 
				   "where project_id = " . $project_id;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$project_legal_type = $row["project_cost_type"];
				$product_line = $row["project_product_line"];
				$order_id = $row["project_order"];
				$order_user = $row["order_user"];
				$recipient_user_ids = array();
				
				$subject = MAIL_SUBJECT_PREFIX . ": New layout request - Project " . $row["project_number"] . ", " . $row["country_name"] . ", " .  $row["order_shop_address_company"];

				$sender_email = $row["user_email"];
				$sender_name =  $row["user_name"] . " " . $row["user_firstname"];

				$bodytext0 = "A new layout request has been added by " . $sender_name;

				$mail = new Mail();
				$mail->set_subject($subject);
				$mail->set_sender($sender_email, $sender_name);

				//legal types
				$sql = "select * from project_costtypes " .
					   "left join users on user_email = project_costtype_email1 " . 
					   "where project_costtype_id = " . dbquote($project_legal_type);

				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					if($row["project_costtype_email1"])
					{
						$mail->add_recipient($row["project_costtype_email1"]);
						$recipient_user_ids[$row["project_costtype_email1"]] = $row["user_id"];
						$recipients_already_served[$row["project_costtype_email1"]] = $row["user_id"];
					}

				}
				$sql = "select * from project_costtypes " .
					   "left join users on user_email = project_costtype_email2 " . 
					   "where project_costtype_id = " . dbquote($project_legal_type);

				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					if($row["project_costtype_email2"])
					{
						$mail->add_recipient($row["project_costtype_email2"]);
						$recipient_user_ids[$row["project_costtype_email2"]] = $row["user_id"];
						$recipients_already_served[$row["project_costtype_email1"]] = $row["user_id"];
					}
				}
				$sql = "select * from project_costtypes " .
					   "left join users on user_email = project_costtype_email3 " . 
					   "where project_costtype_id = " . dbquote($project_legal_type);

				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					if($row["project_costtype_email3"])
					{
						$mail->add_recipient($row["project_costtype_email3"]);
						$recipient_user_ids[$row["project_costtype_email3"]] = $row["user_id"];
						$recipients_already_served[$row["project_costtype_email1"]] = $row["user_id"];
					}

				}
				$sql = "select * from project_costtypes " .
					   "left join users on user_email = project_costtype_email4 " . 
					   "where project_costtype_id = " . dbquote($project_legal_type);

				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					if($row["project_costtype_email4"])
					{
						$mail->add_recipient($row["project_costtype_email4"]);
						$recipient_user_ids[$row["project_costtype_email4"]] = $row["user_id"];
						$recipients_already_served[$row["project_costtype_email1"]] = $row["user_id"];
					}

				}

				
				$link ="project_view_client_data.php?pid=" . $project_id;
				$bodytext = $bodytext0 . "\nclick below to have direct access to the project:\n";
				$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
				$mail->add_text($bodytext);
				$mail->send();
				foreach($recipient_user_ids as $key=>$user_id)
				{
					if($user_id > 0)
					{
						append_mail($order_id, $user_id,  user_id(), $bodytext, "100", 1,0);
					}
				}
			}

			// send email notification for new projects
			$sql = "select project_number, project_postype, project_product_line, project_order, order_user, " .
				   "order_shop_address_company, order_shop_address_place, order_shop_address_country, country_name, " .
				   "user_firstname, user_name, user_email, user_id from projects " . 
				   "left join orders on order_id = project_order " .
				   "left join countries on country_id = order_shop_address_country " .
				   "left join users on user_id = order_user " .
				   "where project_id = " . $project_id;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$pos_country = $row["order_shop_address_country"];
				$project_postype = $row["project_postype"];
				$project_product_line = $row["project_product_line"];
				$order_id = $row["project_order"];
				$order_user = $row["order_user"];
				$recipient_user_ids = array();
				
				$subject = MAIL_SUBJECT_PREFIX . ": New layout request - Project " . $row["project_number"] . ", " . $row["country_name"] . ", " .  $row["order_shop_address_company"];

				$sender_email = $row["user_email"];
				$sender_name =  $row["user_name"] . " " . $row["user_firstname"];

				$bodytext0 = "A new layout request has been added by " . $sender_name;

				$mailaddress = 0;

				$mail = new Mail();
				$mail->set_subject($subject);
				$mail->set_sender($sender_email, $sender_name);

				
				$sql = 'select * from projecttype_newproject_notifications ' . 
					   'where projecttype_newproject_notification_on_new_project = 1 ' . 
					   ' and projecttype_newproject_notification_country = ' . $pos_country . 
					   ' and projecttype_newproject_notification_postype = ' . $project_postype;
				

				$res = mysql_query($sql) or dberror($sql);
				while ($row = mysql_fetch_assoc($res))
				{
					if($row["projecttype_newproject_notification_email"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["projecttype_newproject_notification_email"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_email"]] = $row_u["user_id"];
						}
						else
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_email"]] = 0;
						}
						$mailaddress = 1;
						
					}

					if($row["projecttype_newproject_notification_emailcc1"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc1"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc1"]] = $row_u["user_id"];
						}
						else
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc1"]] = 0;
						}
					}
					if($row["projecttype_newproject_notification_emailcc2"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc2"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc2"]] = $row_u["user_id"];
						}
						else
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc2"]] = 0;
						}
						
					}
					if($row["projecttype_newproject_notification_emailcc3"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc3"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc3"]] = $row_u["user_id"];
						}
						else
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc3"]] = 0;
						}
						
					}
					if($row["projecttype_newproject_notification_emailcc4"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc4"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc4"]] = $row_u["user_id"];
						}
						else
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc4"]] = 0;
						}
					
					}
					if($row["projecttype_newproject_notification_emailcc5"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc5"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc5"]] = $row_u["user_id"];
						}
						else
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc5"]] = 0;
						}
					
					}
					if($row["projecttype_newproject_notification_emailcc6"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc6"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc6"]] = $row_u["user_id"];
						}
						else
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc6"]] = 0;
						}
					
					}
					if($row["projecttype_newproject_notification_emailcc7"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc7"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc7"]] = $row_u["user_id"];
						}
						else
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc7"]] = 0;
						}
					}
				}

				foreach($recipient_user_ids as $key=>$user_id)
				{
					if(!array_key_exists($key, $recipients_already_served))
					{
						$mail->add_recipient($key);
						$recipients_already_served[$key] = $user_id;
					}
				}
				
				$link ="project_view_client_data.php?pid=" . $project_id;
				$bodytext = $bodytext0 . "\nclick below to have direct access to the project:\n";
				$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
				$mail->add_text($bodytext);
				
				if($mailaddress == 1)
				{
					$result = $mail->send();
					foreach($recipient_user_ids as $key=>$user_id)
					{
						if($user_id > 0)
						{
							append_mail($order_id, $user_id,  user_id(), $bodytext, "100", 1,0);
						}
					}
				}
			}

			//send email notifications for possubclass recipients
			$sql = "select project_number, project_postype, project_pos_subclass, project_product_line, project_order, order_user, " .
				   "order_shop_address_company, order_shop_address_place, order_shop_address_country, country_name, " .
				   "user_firstname, user_name, user_email, user_id from projects " . 
				   "left join orders on order_id = project_order " .
				   "left join countries on country_id = order_shop_address_country " .
				   "left join users on user_id = order_user " .
				   "where project_id = " . $project_id;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$pos_country = $row["order_shop_address_country"];
				$project_postype = $row["project_postype"];
				$project_product_line = $row["project_product_line"];
				$order_id = $row["project_order"];
				$order_user = $row["order_user"];
				$project_pos_subclass = $row["project_pos_subclass"];
				$recipient_user_ids = array();
				
				$subject = MAIL_SUBJECT_PREFIX . ": New layout request - Project " . $row["project_number"] . ", " . $row["country_name"] . ", " .  $row["order_shop_address_company"];

				$sender_email = $row["user_email"];
				$sender_name =  $row["user_name"] . " " . $row["user_firstname"];

				$bodytext0 = "A new layout request has been added by " . $sender_name;

				$mailaddress = 0;

				$mail = new Mail();
				$mail->set_subject($subject);
				$mail->set_sender($sender_email, $sender_name);

				
				$sql = 'select * from possubclasses ' . 
					   'where possubclass_id = ' . dbquote($project_pos_subclass);
				

				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					if($row["possubclass_email1"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["possubclass_email1"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[$row["possubclass_email1"]] = $row_u["user_id"];
						}
						else
						{
							$recipient_user_ids[$row["possubclass_email1"]] = 0;
						}
						$mailaddress = 1;
						
					}

					if($row["possubclass_email2"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["possubclass_email2"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[$row["possubclass_email2"]] = $row_u["user_id"];
						}
						else
						{
							$recipient_user_ids[$row["possubclass_email2"]] = 0;
						}
						$mailaddress = 1;
						
					}

					if($row["possubclass_email3"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["possubclass_email3"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[$row["possubclass_email3"]] = $row_u["user_id"];
						}
						else
						{
							$recipient_user_ids[$row["possubclass_email3"]] = 0;
						}
						$mailaddress = 1;
						
					}

					if($row["possubclass_email4"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["possubclass_email4"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[$row["possubclass_email4"]] = $row_u["user_id"];
						}
						else
						{
							$recipient_user_ids[$row["possubclass_email4"]] = 0;
						}
						$mailaddress = 1;
						
					}

					if($row["possubclass_email5"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["possubclass_email5"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[$row["possubclass_email5"]] = $row_u["user_id"];
						}
						else
						{
							$recipient_user_ids[$row["possubclass_email5"]] = 0;
						}
						$mailaddress = 1;
						
					}

					if($row["possubclass_email6"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["possubclass_email6"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[$row["possubclass_email6"]] = $row_u["user_id"];
						}
						else
						{
							$recipient_user_ids[$row["possubclass_email6"]] = 0;
						}
						$mailaddress = 1;
						
					}

					
				}

				foreach($recipient_user_ids as $key=>$user_id)
				{
					if(!array_key_exists($key, $recipients_already_served))
					{
						$mail->add_recipient($key);
						$recipients_already_served[$key] = $user_id;
					}
				}
				
				$link ="project_view_client_data.php?pid=" . $project_id;
				$bodytext = $bodytext0 . "\nclick below to have direct access to the project:\n";
				$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
				$mail->add_text($bodytext);
				
				if($mailaddress == 1)
				{
					$result = $mail->send();
					foreach($recipient_user_ids as $key=>$user_id)
					{
						if($user_id > 0)
						{
							append_mail($order_id, $user_id,  user_id(), $bodytext, "100", 1,0);
						}
					}
				}
			}


			//send email notifications for area types recipients
			$sql = "select project_number, project_postype, project_product_line, project_order, project_projectkind, order_user, " .
				   "order_shop_address_company, order_shop_address_place, order_shop_address_country, country_name, " .
				   "user_firstname, user_name, user_email, user_id from projects " . 
				   "left join orders on order_id = project_order " .
				   "left join countries on country_id = order_shop_address_country " .
				   "left join users on user_id = order_user " .
				   "where project_id = " . $project_id;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$pos_country = $row["order_shop_address_country"];
				$project_postype = $row["project_postype"];
				$project_kind = $row["project_projectkind"];
				$project_product_line = $row["project_product_line"];
				$order_id = $row["project_order"];
				$order_user = $row["order_user"];
				$recipient_user_ids = array();

				//get areas
				$posareas = array();
				
				if($project_kind == 1) //new project
				{
					$sql_p = "select posarea_area from posorderspipeline " .
						   "left join posareaspipeline on posarea_posaddress = posorder_posaddress " . 
						   "where posorder_order = " . dbquote($order_id);
				}
				elseif($project_kind > 1) //renovation, takeover/renovation and takeover project
				{
					$sql_p = "select posarea_area from posorders " .
						   "left join posareas on posarea_posaddress = posorder_posaddress " . 
						   "where posorder_order = " . dbquote($order_id);
				}

				$res_p = mysql_query($sql_p) or dberror($sql_p);
				while ($row_p = mysql_fetch_assoc($res_p))
				{
					$posareas[$row_p["posarea_area"]] = $row_p["posarea_area"];
				}
				
				
				if(count($posareas) > 0)
				{
					$subject = MAIL_SUBJECT_PREFIX . ": New layout request - Project " . $row["project_number"] . ", " . $row["country_name"] . ", " .  $row["order_shop_address_company"];

					$sender_email = $row["user_email"];
					$sender_name =  $row["user_name"] . " " . $row["user_firstname"];

					$bodytext0 = "A new layout request has been added by " . $sender_name;

					$mailaddress = 0;

					$mail = new Mail();
					$mail->set_subject($subject);
					$mail->set_sender($sender_email, $sender_name);

					
					$filter = " IN (" . implode(',', $posareas ) . ')';
					
					$sql = 'select * from posareatypes ' . 
						   'where posareatype_id ' . $filter;
					

					$res = mysql_query($sql) or dberror($sql);
					while ($row = mysql_fetch_assoc($res))
					{
						if($row["posareatype_email1"])
						{
							
							$sql = 'select user_id from users ' . 
								   'where user_email = ' . dbquote($row["posareatype_email1"]);

							$res_u = mysql_query($sql) or dberror($sql);
							if ($row_u = mysql_fetch_assoc($res_u))
							{
								$recipient_user_ids[$row["posareatype_email1"]] = $row_u["user_id"];
							}
							else
							{
								$recipient_user_ids[$row["posareatype_email1"]] = 0;
							}
							$mailaddress = 1;
							
						}

						if($row["posareatype_email2"])
						{
							
							$sql = 'select user_id from users ' . 
								   'where user_email = ' . dbquote($row["posareatype_email2"]);

							$res_u = mysql_query($sql) or dberror($sql);
							if ($row_u = mysql_fetch_assoc($res_u))
							{
								$recipient_user_ids[$row["posareatype_email2"]] = $row_u["user_id"];
							}
							else
							{
								$recipient_user_ids[$row["posareatype_email2"]] = 0;
							}
							$mailaddress = 1;
							
						}

						if($row["posareatype_email3"])
						{
							
							$sql = 'select user_id from users ' . 
								   'where user_email = ' . dbquote($row["posareatype_email3"]);

							$res_u = mysql_query($sql) or dberror($sql);
							if ($row_u = mysql_fetch_assoc($res_u))
							{
								$recipient_user_ids[$row["posareatype_email3"]] = $row_u["user_id"];
							}
							else
							{
								$recipient_user_ids[$row["posareatype_email3"]] = 0;
							}
							$mailaddress = 1;
							
						}

						if($row["posareatype_email4"])
						{
							
							$sql = 'select user_id from users ' . 
								   'where user_email = ' . dbquote($row["posareatype_email4"]);

							$res_u = mysql_query($sql) or dberror($sql);
							if ($row_u = mysql_fetch_assoc($res_u))
							{
								$recipient_user_ids[$row["posareatype_email4"]] = $row_u["user_id"];
							}
							else
							{
								$recipient_user_ids[$row["posareatype_email4"]] = 0;
							}
							$mailaddress = 1;
							
						}

						if($row["posareatype_email5"])
						{
							
							$sql = 'select user_id from users ' . 
								   'where user_email = ' . dbquote($row["posareatype_email5"]);

							$res_u = mysql_query($sql) or dberror($sql);
							if ($row_u = mysql_fetch_assoc($res_u))
							{
								$recipient_user_ids[$row["posareatype_email5"]] = $row_u["user_id"];
							}
							else
							{
								$recipient_user_ids[$row["posareatype_email5"]] = 0;
							}
							$mailaddress = 1;
							
						}

						if($row["posareatype_email6"])
						{
							
							$sql = 'select user_id from users ' . 
								   'where user_email = ' . dbquote($row["posareatype_email6"]);

							$res_u = mysql_query($sql) or dberror($sql);
							if ($row_u = mysql_fetch_assoc($res_u))
							{
								$recipient_user_ids[$row["posareatype_email6"]] = $row_u["user_id"];
							}
							else
							{
								$recipient_user_ids[$row["posareatype_email6"]] = 0;
							}
							$mailaddress = 1;
							
						}

						
					}

					foreach($recipient_user_ids as $key=>$user_id)
					{
						if(!array_key_exists($key, $recipients_already_served))
						{
							$mail->add_recipient($key);
							$recipients_already_served[$key] = $user_id;
						}
					}
					
					$link ="project_view_client_data.php?pid=" . $project_id;
					$bodytext = $bodytext0 . "\nclick below to have direct access to the project:\n";
					$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
					$mail->add_text($bodytext);
					
					if($mailaddress == 1)
					{
						$result = $mail->send();
						foreach($recipient_user_ids as $key=>$user_id)
						{
							if($user_id > 0)
							{
								append_mail($order_id, $user_id,  user_id(), $bodytext, "100", 1,0);
							}
						}
					}
				}
			}
		}
	}


	//send mail notifications for CER/AF
	//check first if a mail has to be sent

	$sql = "select order_number, order_shop_address_place, country_name, order_shop_address_company, project_order, order_user, " .
		   "project_postype, project_projectkind, project_cost_type, project_projectkind, project_cost_type " . 
		   "from projects " .
		   "left join orders on order_id = project_order " .
		   "left join countries on country_id = order_shop_address_country " .
		   "left join project_costs on project_cost_order = order_id " .
		   "where project_id = " .  dbquote($project_id);

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		
		$recipient_user_ids = array();
		$order_id = $row["project_order"];
		$order_user = $row["order_user"];
		
		$project_kind = $row["project_projectkind"];
		$legal_type = $row["project_cost_type"];
		
		if($project_kind == 5) // lease renewal
		{
			$subject = "LN submission alert - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_place"] . ", " . $row["order_shop_address_company"];
			

		}
		else
		{
			if($legal_type == 1) //Corporate
			{
				$subject = "LN & CER submission - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_place"] . ", " . $row["order_shop_address_company"];
			}
			else
			{
				$subject = "AF submission - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_place"] . ", " . $row["order_shop_address_company"];
			}

		}
		
		
		$sql = "select posproject_type_needs_cer, posproject_type_needs_af " . 
			   "from posproject_types " . 
			   "where posproject_type_postype =  " . $row["project_postype"] .
			   " and posproject_type_projectcosttype =  " . $row["project_cost_type"] .
			   " and posproject_type_projectkind =  " . $row["project_projectkind"];

		
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			if($row["posproject_type_needs_cer"] == 1 or $row["posproject_type_needs_af"] == 1)
			{
				
				if($row["posproject_type_needs_cer"] == 1)
				{
					if($project_kind == 5) // lease renewal
					{
						$sql = "select * from mail_alert_types " . 
						   "where mail_alert_type_id = 11";
						$subject = "LN request for " . $subject;
					}
					else
					{
						$sql = "select * from mail_alert_types " . 
						       "where mail_alert_type_id = 3";

						$subject = "Request for " . $subject;
					}
					
					
				}
				else
				{
					$sql = "select * from mail_alert_types " . 
						   "where mail_alert_type_id = 7";

					$subject = "Application request for " . $subject;
				}

				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					$sender_email = $row["mail_alert_type_sender_email"];
					$sender_name = $row["mail_alert_type_sender_name"];
					$mail_text = $row["mail_alert_mail_text"];

					//get brand manager

					$sql = "select address_id " . 
						   "from projects " . 
						   "left join orders on order_id = project_order " . 
						   "left join addresses on address_id = order_client_address " . 
						   "where project_id = " . dbquote($project_id);
					
					$res = mysql_query($sql) or dberror($sql);
					if ($row = mysql_fetch_assoc($res))
					{
						$brand_manager_email = "";
						$brandmanager_name = "";
						$reciepient_emails = array();

						$address_id = $row["address_id"];
						
						$sql = "select * " .
							   "from user_roles " . 
							   "left join users on user_id = user_role_user " .
							   "left join addresses on address_id = user_address " . 
							   " where user_role_role = 15 " . 
							   " and address_type = 1 and address_id = " . dbquote($address_id) . 
							   " and user_active = 1";

						
						$res = mysql_query($sql) or dberror($sql);
						while ($row = mysql_fetch_assoc($res))
						{
							$brand_manager_email = $row["user_email"];
							$brandmanager_name  = $row["user_firstname"];
							$recipient_user_ids[$row["user_email"]] = $row["user_id"];
						}

						//client
						$sql = "select * " .
							   "from users " . 
							   "where user_id = " . dbquote($order_user);

						$res = mysql_query($sql) or dberror($sql);
						if ($row = mysql_fetch_assoc($res))
						{
							$reciepient_emails[$row["user_email"]] = $row["user_email"];
							$recipient_user_ids[$row["user_email"]] = $row["user_id"];
						}
						
						//local store coordinator, retail manager
						$sql = "select * " .
							   "from user_roles " . 
							   "left join users on user_id = user_role_user " .
							   "left join addresses on address_id = user_address " . 
							   " where user_role_role = 16 " . 
							   " and address_type = 1 and address_id = " . dbquote($address_id) . 
							   " and user_active = 1";

						
						$res = mysql_query($sql) or dberror($sql);
						while ($row = mysql_fetch_assoc($res))
						{
							if($row["user_can_only_see_his_projects"] and $order_user != $row["user_id"])
							{
							}
							else
							{
								$reciepient_emails[$row["user_email"]] = $row["user_email"];
								$recipient_user_ids[$row["user_email"]] = $row["user_id"];
							}
						}

						$reciepient_emails[$sender_email] = $sender_email;

					}

					$bodytext0 = "Dear " . $brandmanager_name . "\n\n" . $mail_text;
					$link ="cer_project.php?pid=" . $project_id . "&id=" . $project_id;
					$bodytext = $bodytext0 . "\nClick below to have direct access to the project:\n";
					$bodytext = $bodytext .  APPLICATION_URL . "/cer/" . $link . "\n\n";           
					
					$bodytext .= "Thank you very much.\nKind regards, " . $sender_name;

					
					$rctps = $brand_manager_email;

					$mail = new Mail();
					$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
					$mail->set_sender($sender_email, $sender_name);
					
					$mail->add_recipient($brand_manager_email);
					foreach($reciepient_emails as $key=>$email)
					{
						$mail->add_cc($email);
						$rctps = $rctps . $email . "\n";
					}

					$mail->add_text($bodytext);

					$result = $mail->send();
					
					foreach($recipient_user_ids as $key=>$user_id)
					{
						if($user_id > 0)
						{
							append_mail($order_id, $user_id,  user_id(), $bodytext, "100", 1,0);
						}
					}

					if($result == 1)
					{
						$fields = array();
						$values = array();

						$fields[] = "cer_mail_project";
						$values[] = $project_id;

						$fields[] = "cer_mail_group";
						$values[] = dbquote($subject);

						$fields[] = "cer_mail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "cer_mail_sender";
						$values[] = dbquote($sender_name);

						$fields[] = "cer_mail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "cer_mail_reciepient";
						$values[] = dbquote($rctps);

						$fields[] = "date_created";
						$values[] = "now()";

						$fields[] = "date_modified";
						$values[] = "now()";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into cer_mails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						
						mysql_query($sql) or dberror($sql);
					}
						   
				}
			}
		}
	}


	return true;
}

/********************************************************************
    appends an item to the list of materials on performing an action
*********************************************************************/
function append_auto_item($order_id, $taskwork_id)
{
	
	//get taskwork item
	$sql = "select taskwork_item from taskworks where taskwork_id = " . $taskwork_id;
	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		if($row["taskwork_item"] > 0)
		{
			$item_id = $row["taskwork_item"];

			//get item_type
			$sql = "select item_type from items where item_id = " . $item_id;
			$res1 = mysql_query($sql) or dberror($sql);

			if ($row = mysql_fetch_assoc($res1))
			{
				$type = $row["item_type"];
			}
		
			$order_item_fields = array();
			$order_item_values = array();

			$order_item_fields[] = "order_item_order";
			$order_item_values[] = $order_id;

			$order_item_fields[] = "order_item_item";
			$order_item_values[] = $item_id;

			$order_item_fields[] = "order_item_type";
			$order_item_values[] = $type;

			$order_item_fields[] = "order_item_quantity";
			$order_item_values[] = 1;

			// get supplier's address id (first record in case of several)
			$sql = "select address_id, address_company, supplier_item_price, supplier_item_currency ".
				   "from items ".
				   "left join suppliers on item_id = supplier_item ".
				   "left join addresses on supplier_address = address_id ".
				   "where item_id = " . $item_id;
		
			$res1 = mysql_query($sql) or dberror($sql);

			if ($row1 = mysql_fetch_assoc($res1))
			{
				if ($row1["address_id"])
				{
					
					if ($row1["supplier_item_currency"])
					{
						$currency = get_currency($row1["supplier_item_currency"]);
					}
					else
					{
						$currency = get_address_currency($row1["address_id"]);
					}
					
					$order_item_fields[] = "order_item_supplier_address";
					$order_item_values[] = $row1["address_id"];

					// get suppliers's currency information
					$supplier_currency = get_address_currency($row1["address_id"]);

					$order_item_fields[] = "order_item_supplier_currency";
					$order_item_values[] = $currency["id"];

					$order_item_fields[] = "order_item_supplier_exchange_rate";
					$order_item_values[] = $currency["exchange_rate"];

					$order_item_fields[] = "order_item_supplier_price";
					$order_item_values[] = $row1["supplier_item_price"];
				}
			}

			// get orders's currency
			$currency = get_order_currency($order_id);

			// get item data

			$sql = "select * ".
				   "from items ".
				   "left join category_items on item_id = category_item_item ".
				   "left join categories on category_item_category = category_id ".
				   "where item_id = " . $item_id;

			$res1 = mysql_query($sql) or dberror($sql);

			if ($row = mysql_fetch_assoc($res1))
			{
				$order_item_fields[] = "order_item_category";
				$order_item_values[] = $row["item_category"];

				$order_item_fields[] = "order_item_text";
				$order_item_values[] = trim($row["item_name"]) == "" ? "null" : dbquote($row["item_name"]);

				$order_item_fields[] = "order_item_system_price";
				$order_item_values[] = trim($row["item_price"]) == "" ? "null" : dbquote($row["item_price"]);

				$client_price = $row["item_price"] / $currency["exchange_rate"] * $currency["factor"];
				$order_item_fields[] = "order_item_client_price";
				$order_item_values[] = trim($client_price) == "" ? "null" : dbquote($client_price);

				$order_item_fields[] = "order_item_cost_unit_number";
				$order_item_values[] = trim($row["category_cost_unit_number"]) == "" ? "null" : dbquote($row["category_cost_unit_number"]);

				$order_item_fields[] = "order_items_costmonitoring_group";
				$order_item_values[] = dbquote($row["item_costmonitoring_group"]);

			}

			$order_item_fields[] = "order_item_no_offer_required";
			$order_item_values[] = 1;


			$order_item_fields[] = "order_item_cost_group";
			$order_item_values[] = dbquote($row["item_cost_group"]);


			$order_item_fields[] = "order_item_ordered";
			$order_item_values[] = "current_timestamp";


			$order_item_fields[] = "date_created";
			$order_item_values[] = "current_timestamp";

			$order_item_fields[] = "date_modified";
			$order_item_values[] = "current_timestamp";
			
			if (isset($_SESSION["user_login"]))
			{
				$order_item_fields[] = "user_created";
				$order_item_values[] = dbquote($_SESSION["user_login"]);

				$order_item_fields[] = "user_modified";
				$order_item_values[] = dbquote($_SESSION["user_login"]);
			}
			$sql = "insert into order_items (" . join(", ", $order_item_fields) . ") values (" . join(", ", $order_item_values) . ")";
			mysql_query($sql) or dberror($sql);
		}
	}
	

	return true;
}


?>