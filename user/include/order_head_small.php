<?php
/********************************************************************

    order_head_small.php

    Show order information (Page heading only)

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-10-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-07-17
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.
*********************************************************************/
$order_state_name = get_actual_order_state_name($order["order_actual_order_state_code"], 2);

$form->add_label("order_number", "Order Number", 0, $order["order_number"]);
$form->add_label("submitted_by", "Order Date", 0, to_system_date($order["order_date"]) . " by " . $order["submitted_by"]);

$form->add_label("status", "Status", 0, $order["order_actual_order_state_code"] . " " . $order_state_name);

$line = "concat(user_name, ' ', user_firstname)";

if ($order["order_retail_operator"])
{
    $form->add_lookup("retail_operator_name", "Retail Operator", "users", $line, 0, $order["order_retail_operator"]);
}
else
{
    $form->add_label("retail_operator_name", "Retail Operator");
}


$client = $client_address["company"] . ", " .
          $client_address["zip"] . " " . $client_address["place"] . ", " .
          $client_address["country_name"];


$form->add_label("client_address", "Client", 0, $client);

$shop = $order["order_shop_address_company"] . ", " .
        $order["order_shop_address_zip"] . " " .
        $order["order_shop_address_place"] . ", " .
        $order["order_shop_address_country_name"];
        
$form->add_label("shop_address", "POS Location Address", 0, $shop);


?>