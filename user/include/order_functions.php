<?php
/********************************************************************

    order_functions.php

    Helper-functions for order-management.

    Created by:     Oliver Hofer (oliver.hofer@pageagent.com)
    Date created:   2002-08-31
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-02-04
    Version:        1.2.3

    Copyright (c) 2002-2003, Swatch AG, All Rights Reserved.

*********************************************************************/

function order_generate_order_number()
{
    // find latest order in database

    $nr_today = "C-" . date("Y-m-d", time());

    $sql = "select order_number " .
           "from orders " .
           "where order_type = 2 and left(order_number, 12) = '" .$nr_today . "' " .
           "order by order_number desc";

    $res = mysql_query($sql) or dberror($sql);

    if ($res)
    {
        $bla = 2;
        $row = mysql_fetch_assoc($res);
        $tokens = explode("-", $row['order_number']);

        if (isset($tokens[4]))
        {
            $running_number = $tokens[4] + 1;
        }
        else
        {
            $running_number = 1;
        }
    }
    else
    {
        $running_number = 1;
    }

    $number = "C-" . date("Y-m-d", time()) . "-";
    $number .= sprintf("%04d", $running_number);
    return $number;
}

function get_user_country_region($user_id)
{
    $country_region = 0;

    $sql = "select country_region from users " .
           "left join addresses on addresses.address_id = users.user_address " .
           "left join countries on countries.country_id = addresses.address_country " .
           "where users.user_id = " . $user_id;

    $res = mysql_query($sql) or dberror($sql);
    if (($res) && (mysql_num_rows($res) != 0))
    {
        $row = mysql_fetch_assoc($res);
        $country_region = $row['country_region'];
    }

    return $country_region;
}
/** function order_basket_item
*
* returns false, if basket is not empty, 
* true is returned, if basket is empty
*/
function basket_is_empty($basket = "")
{
    if (!$basket)
    {
        $basket = get_users_basket();
    }

    // check number of items in shopping basket

    $sql = "select basket_item_item " . 
           "from basket_items " .
           "where basket_item_basket=" . $basket;

    $res = mysql_query($sql);
    if (mysql_num_rows($res) >= 1)
    {
        return false;
    } else {
        return true;
    }
}

/**************************************************************************
* function order_basket_item
*
* copies the contents of the basket_items table to the 
* order_items table. Adjusts quantity, if changed
*
* @param int $form   Form, where submitted values are available
* @param int $basket_item   which item from the basket should be copied
* @param int $order_id  Order, where the ordered items belong to
* @return
* @access    private
*
*/
function order_basket_item(&$form, $basket_item, $order_id, $quantity = 0)
{
    $order_item_fields[] = "order_item_order";
    $order_item_values[] = dbquote($order_id);

    $order_item_fields[] = "order_item_item";
    $order_item_values[] = dbquote($basket_item['basket_item_item']);

    $order_item_fields[] = "order_item_category";
    $order_item_values[] = dbquote($basket_item['basket_item_category']);

    $order_item_fields[] = "order_item_type";
    $order_item_values[] = dbquote(ITEM_TYPE_STANDARD);

    $order_item_fields[] = "order_item_text";
    $order_item_values[] = dbquote($basket_item['item_name']);


    // get supplier's address id (first record in case of several)
    $sql = "select address_id, address_company, supplier_item_price, supplier_item_currency ".
           "from items ".
           "left join suppliers on item_id = supplier_item ".
           "left join addresses on supplier_address = address_id ".
           "where item_id = " . $basket_item['basket_item_item'];

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        if ($row["address_id"])
        {
           // price and currency

            if ($row["supplier_item_currency"])
            {
                $currency = get_currency($row["supplier_item_currency"]);
            }
            else
            {
                $currency = get_address_currency($row["address_id"]);
            }

            $order_item_fields[] = "order_item_supplier_currency";
            $order_item_values[] = $currency["id"];

            $order_item_fields[] = "order_item_supplier_exchange_rate";
            $order_item_values[] = $currency["exchange_rate"];

            $order_item_fields[] = "order_item_supplier_price";
            $order_item_values[] = $row["supplier_item_price"];

            $order_item_fields[] = "order_item_supplier_address";
            $order_item_values[] = dbquote($row["address_id"]);
        }
    }

    // get orders's currency  & get item data
    
    $currency = get_order_currency($order_id);
    $sql = "select * ".
           "from items ".
           "left join category_items on item_id = category_item_item ".
           "left join categories on category_item_category = category_id ".
           "where item_id = " . $basket_item['basket_item_item'];

    $res1 = mysql_query($sql) or dberror($sql);
    $type = "";

    if ($row = mysql_fetch_assoc($res1))
    {
        $order_item_fields[] = "order_item_system_price";
        if ($type == ITEM_TYPE_COST_ESTIMATION)
        {
            $order_item_values[] = $value;
            $client_price = $value / $currency["exchange_rate"] * $currency["factor"];
        }
        else
        {
            $order_item_values[] = trim($row["item_price"]) == "" ? "null" : dbquote($row["item_price"]);
            $client_price = $row["item_price"] / $currency["exchange_rate"] * $currency["factor"];
        }


        $order_item_fields[] = "order_item_client_price";
        $order_item_values[] = trim($client_price) == "" ? "null" : dbquote($client_price);

        $order_item_fields[] = "order_item_cost_unit_number";
        $order_item_values[] = trim($row["category_cost_unit_number"]) == "" ? "null" : dbquote($row["category_cost_unit_number"]);
    }

    $order_item_fields[] = "order_item_po_number";
    $order_item_values[] = dbquote("");

    $order_item_fields[] = "order_item_offer_number";
    $order_item_values[] = dbquote("");   

    $order_item_fields[] = "order_item_shipment_code";
    $order_item_values[] = dbquote("");

    $order_item_fields[] = "order_item_forwarder_address";
    $order_item_values[] = dbquote("");

    $order_item_fields[] = "order_item_staff_for_discharge";
    $order_item_values[] = dbquote("");
    
    $order_item_fields[] = "order_item_no_offer_required";
    $order_item_values[] = dbquote("1");

    if ($basket_item['quantity'] > 0)
    {
        $order_item_fields[] = "order_item_quantity";
        $order_item_values[] = dbquote($basket_item['quantity']);

        add_fbi_info($order_item_fields, $order_item_values);

        $sql = "insert into order_items (" . join(", ", $order_item_fields) . ") values (" . join(", ", $order_item_values) . ")";

        mysql_query($sql) or dberror($sql);
    }

    // empty basket

    $sql = "delete from basket_items where basket_item_id = " . $basket_item['basket_item_id'];
    mysql_query($sql) or dberror($sql);

}  // END function order_basket_item

/**
* update_shopping_list
*
*/

function update_shopping_list($list_items, $basket, $category = "", $addon = false)
{
    $basket_item_id = 0;
    $addon_items = array();
    $update_addons = array();
/*    $add_this_items = array();


    if ($single_item)
    {
        $add_this_items[$single_item] = 1.00;
    }

    foreach($list->values("item_quantity") as $key => $value)
    {
        if (isset($add_this_items[$key]))
        {
            $add_this_items[$key] += $value;
        }
        else
        {
            $add_this_items[$key] = $value;
        }
    }
*/
    reset($list_items);
    foreach($list_items as $key => $value)
    {

        if ($addon)
        {
            $sql = "select addon_id, addon_child, addon_package_quantity, category_item_category " .
                   "from addons ". 
                   "left join category_items on category_items.category_item_item = addon_parent " .
                   "where addon_id = " .$key;
            
            $res = mysql_query($sql);
            if (($res) && ($row = mysql_fetch_assoc($res)))
            {
                if (!isset($addon_items[$row['addon_child']]))
                {
                    $addon_items[$row['addon_child']] = array(
                                    'category' => $row['category_item_category'],
                                    'value' => $value);
//                                    'value' => ($value * $row['addon_package_quantity']));
                }
                else 
                {
                    $addon_items[$row['addon_child']]['value'] += $value;
//                                ($value * $row['addon_package_quantity']);
                }
            }
            continue;
        }
        else 
        {
            update_basket_item($basket, $key, $value, $category);

            // check, if this item has add on's that need to be updated

            $sql = "select basket_item_item " .
                   "from basket_items " .
                   "where basket_item_id = " . $key;

            $res = mysql_query($sql);
            if ($res)
            {
                $row = mysql_fetch_assoc($res);
                if ($row['basket_item_item'])
                {
                    $sql = "select basket_item_id, basket_item_item, addon_package_quantity, " .
                           "basket_item_category, basket_item_quantity, basket_item_parent " .
                           "from basket_items " .
                           "left join addons on addon_child = basket_item_item " .
                           "where basket_item_parent = " . $row['basket_item_item'] . " " .
                           "    and basket_item_basket = " . $basket;

                    $res = mysql_query($sql) or dberror($sql);

                    while ($row = mysql_fetch_assoc($res))
                    {

                        $addon_items[$row['basket_item_id']] = array(
                                        'item' => $row['basket_item_item'],
                                        'category' => $row['basket_item_category'],
                                        'value' => $value,
                                        'parent'=> $row['basket_item_parent']);
//                                        'value' => ($value * $row['addon_package_quantity']));

                    }
                }
            }
        }
    }

    if ($addon_items)
    {
        foreach ($addon_items as $key => $value)
        {
            update_basket_item($basket, $value['item'], $value['value'], 
                               $value['category'], $value['parent']);
        }
        
    }
}

function update_basket_item($basket, $key, $value, $category = "", $item_parent = "")
{
    // item already in basket?

	
    if ($category != "" and $category != 'search_was_performed')
    {
		
		$sql = "select basket_item_id, basket_item_parent " .
               "from basket_items " .
               "where basket_item_basket = " . $basket . " and ".
               "    basket_item_category = " . $category . " and " .
               "    basket_item_item = " . $key;

        if (!empty($item_parent))
        {
            $sql .= " and basket_item_parent = " .$item_parent;
        }

        $res = mysql_query($sql) or dberror($sql);

        if (mysql_num_rows($res) == 0)
        {
            if ($value > 0)
            {
                $fields = array("basket_item_basket", "basket_item_category",
                                "basket_item_item", "basket_item_quantity");
                $values = array(dbquote($basket), dbquote($category), dbquote($key));
                $values[] = dbquote($value);

                // item_parent is set when adding addons. identifies parent item, to which
                // added item belongs to

                if (!empty($item_parent))
                {
                    $fields[] = "basket_item_parent";
                    $values[] = $item_parent;
                }

                add_fbi_info($fields, $values);

                $sql = "insert into basket_items ( " . join(',', $fields) . ")" .
                       "values(" . join(',', $values) . ")";

                mysql_query($sql) or dberror($sql); 
            } 
            else 
            {
                // do nothing
            }
            $row = mysql_fetch_assoc($res);
            $basket_item_id = $row['basket_item_id'];
        } 
        else
        {
            // set basket_item_id

            $row = mysql_fetch_assoc($res);
            $basket_item_id = $row['basket_item_id'];
        }
    } 
    elseif ($category != "" and $category == 'search_was_performed')
    {
		$sql = "select basket_item_id, basket_item_parent " .
               "from basket_items " .
               "where basket_item_basket = " . $basket . " and ".
               "    basket_item_item = " . $key;

        if (!empty($item_parent))
        {
            $sql .= " and basket_item_parent = " .$item_parent;
        }

        $res = mysql_query($sql) or dberror($sql);

        if (mysql_num_rows($res) == 0)
        {
			if ($value > 0)
            {
				$sql_i = 'select item_category from items where item_id = ' . $key;
				$res_i = mysql_query($sql_i) or dberror($sql_i);
				$row_i = mysql_fetch_assoc($res_i);
				$item_category = $row_i["item_category"];

				$fields = array("basket_item_basket", "basket_item_category",
                                "basket_item_item", "basket_item_quantity");
                $values = array(dbquote($basket), dbquote($item_category), dbquote($key));
                $values[] = dbquote($value);

                // item_parent is set when adding addons. identifies parent item, to which
                // added item belongs to

                if (!empty($item_parent))
                {
                    $fields[] = "basket_item_parent";
                    $values[] = $item_parent;
                }

                add_fbi_info($fields, $values);

                $sql = "insert into basket_items ( " . join(',', $fields) . ")" .
                       "values(" . join(',', $values) . ")";
                mysql_query($sql) or dberror($sql); 
            } 
            else 
            {
                // do nothing
            }
            $row = mysql_fetch_assoc($res);
            $basket_item_id = $row['basket_item_id'];
        } 
        else
        {
            // set basket_item_id

            $row = mysql_fetch_assoc($res);
            $basket_item_id = $row['basket_item_id'];
        }
	}
	else 
    {
		$basket_item_id = $key;
    }

    if ($basket_item_id)
    {
		if ($value > 0)
        {
            $sql = "update basket_items ";

            if ($item_parent)
            {
                $sql .= "set basket_item_quantity = ". $value . " ";
            }
            else 
            {
                $sql .= "set basket_item_quantity = ". $value . " ";
            }
            $sql .= "where basket_item_id = " . $basket_item_id;
        } else {

            $basket_delete_items[] = $basket_item_id;

            // check, if it was an addon. delete all other addons and the parent item

            $sql = "select basket_item_parent, basket_item_item " .
                   "from basket_items " .
                   "where basket_item_id = " . $basket_item_id . " " .
                   "    and basket_item_basket = " . $basket;

            $res = mysql_query($sql) or dberror($sql);
            if ($res)
            {
                $row = mysql_fetch_assoc($res);

                if ($row['basket_item_parent'])
                {
                    // basket item is an addon. select all other addons and parent
                    
                    $sql = "select basket_item_id " .
                           "from basket_items " .
                           "where ( basket_item_parent = " . $row['basket_item_parent'] .
                           "    or basket_item_item = " . $row['basket_item_parent'] . ")" .
                           "    and basket_item_basket = " . $basket;
                }
                else if ($row['basket_item_item'])
                {
                    // seek for addons to this item in the basket

                    $sql = "select basket_item_id " .
                           "from basket_items " .
                           "where basket_item_parent = " . $row['basket_item_item'] .
                           "    and basket_item_basket = " . $basket;
                
                }
            }

            $res = mysql_query($sql) or dberror($sql);
            while ($row = mysql_fetch_assoc($res))
            {
                if (!in_array($row['basket_item_id'], $basket_delete_items))
                {
                    $basket_delete_items[] = $row['basket_item_id'];
                }
            } 

            $sql = "delete from basket_items " .
                   "where basket_item_id in ( " . join (',', $basket_delete_items) . ")";
        }
        mysql_query($sql) or dberror($sql); 
    }
}


/********************************************************************
    get the id of the shopping-basket of a certain user
*********************************************************************/
function get_users_basket()
{
    // get user's basket

    $res = mysql_query("select basket_id from baskets where basket_user = " . user_id());
    if (mysql_num_rows($res) >= 1)
    {
        $row = mysql_fetch_assoc($res);
        $basket = $row['basket_id'];
    } else {
        $fields[] = "basket_user";
        $values[] = user_id();
        add_fbi_info($fields, $values);

        $sql = "insert into baskets (" . join(',', $fields) .") " .
               "values (" . join(',', $values) . ")";
        mysql_query($sql) or dberror($sql);
        $basket = mysql_insert_id();
    }
    return $basket;
}

/********************************************************************
    get the grand total of the contents of the shopping-list
*********************************************************************/
function get_basket_total($table, $id)
{
    $sql = "select currency_symbol, currency_exchange_rate, currency_factor ".
           "from currencies, users, addresses " .
           "where users.user_id = " . user_id() . " and ".
           "    addresses.address_id = users.user_address and ".
           "    currencies.currency_id = addresses.address_currency";

    $res = mysql_query($sql) or dberror($sql);
    if ($res)
    {
        $currency_info = mysql_fetch_assoc($res);
    }

    $sql = "select sum(". $table ."_item_quantity * item_price) as total " .
           "from " . $table . "_items " .
           "left join items on item_id = " . $table. "_items." . $table. "_item_item " .
           "where " . $table. "_item_" .$table ." = " . $id;

    $res = mysql_query($sql) or dberror($sql);
    if ($res)
    {
        $sum = mysql_fetch_assoc($res);
    }

    // $currency_info['currency_symbol'] . " " . 
    return number_format(($sum['total'] / $currency_info['currency_exchange_rate']) 
           * $currency_info['currency_factor'],2,".", "" );
}

function get_basket_part_total($basket_id, $addons = false)
{
    $sql = "select currency_symbol, currency_exchange_rate, currency_factor ".
           "from currencies, users, addresses " .
           "where users.user_id = " . user_id() . " and ".
           "    addresses.address_id = users.user_address and ".
           "    currencies.currency_id = addresses.address_currency";

    $res = mysql_query($sql) or dberror($sql);
    if ($res)
    {
        $currency_info = mysql_fetch_assoc($res);
    }

    if ($addons)
    {
        $sql = "select sum(basket_item_quantity * item_price * " .
               "    addons.addon_package_quantity) as total " .
               "from basket_items " .
               "left join items on item_id = basket_items.basket_item_item " .
               "left join addons on addons.addon_child = basket_item_item " .
               "where basket_item_basket = " . $basket_id. " and basket_item_parent IS NOT NULL " .
               "    and addons.addon_parent = basket_item_parent";
    }
    else 
    {
        $sql = "select sum(basket_item_quantity * item_price) as total " .
               "from basket_items " .
               "left join items on item_id = basket_items.basket_item_item " .
               "where basket_item_basket = " . $basket_id. " and basket_item_parent IS NULL";
    }

    $res = mysql_query($sql) or dberror($sql);
    if ($res)
    {
        $sum = mysql_fetch_assoc($res);
    }

    // $currency_info['currency_symbol'] . " " . 
    return number_format(($sum['total'] / $currency_info['currency_exchange_rate']) 
           * $currency_info['currency_factor'],2,".", "" );

}

function get_country($country_id)
{
    $country_name = "";

    $sql = "select country_id, country_name ".
           "from countries ".
           "where country_id = " . $country_id;

    $res = mysql_query($sql);
    if ($res)
    {
        $row = mysql_fetch_assoc($res);
        $country_name = $row['country_name'];
    }
    return $country_name;
}

function get_preferred_transportation($transportation)
{
    $transportation_type_name = "";

    $sql = "select transportation_type_name " .
           "from transportation_types " .
           "where transportation_type_id = " . $transportation;

    $res = mysql_query($sql);
    if ($res)
    {
        $row = mysql_fetch_assoc($res);
        $transportation_type_name = $row['transportation_type_name'];
    }
    return $transportation_type_name;
}

function get_voltage($voltage)
{
    $voltage_name = "";

    $sql = "select voltage_name " .
           "from voltages " .
           "where voltage_id = " . $voltage;

    $res = mysql_query($sql);
    if ($res)
    {
        $row = mysql_fetch_assoc($res);
        $voltage_name = $row['voltage_name'];
    }
    return $voltage_name;
}


function get_location_type($type)
{
    $type_name = "";

    $sql = "select location_type_name " .
           "from location_types " .
           "where location_type_id = " . $type;

    $res = mysql_query($sql);
    if ($res)
    {
        $row = mysql_fetch_assoc($res);
        $type_name = $row['location_type_name'];
    }
    return $type_name;
}

/********************************************************************
    save an order
*********************************************************************/

function save_order($form)
{
    $order_fields = array();
    $order_values = array();

    $delivery_address_fields = array();
    $delivery_address_values = array();

	$order_number = order_generate_order_number();

    $order_fields[] = "order_number";
    $order_values[] = dbquote($order_number);

    $order_fields[] = "order_date";
    $order_values[] = "now()";

    $order_fields[] = "order_type";
    $order_values[] = dbquote(2);    // catalog order

    $order_fields[] = "order_client_address";
    $order_values[] = dbquote($form->value("address_id"));

    $order_fields[] = "order_user";
    $order_values[] = dbquote(user_id());

    // currencies
    $currency = get_address_currency($form->value("address_id"));
    $system_currency = get_system_currency_fields();

    $order_fields[] = "order_client_currency";
    $order_values[] = dbquote($currency["id"]);

    $order_fields[] = "order_system_currency";
    $order_values[] = dbquote($system_currency["id"]);

    $order_fields[] = "order_client_exchange_rate";
    $order_values[] = dbquote($currency["exchange_rate"]);

    $order_fields[] = "order_system_exchange_rate";
    $order_values[] = dbquote($system_currency["exchange_rate"]);

    $order_fields[] = "order_preferred_transportation_arranged";
    $order_values[] = dbquote($form->value("preferred_transportation_arranged"));

	$order_fields[] = "order_preferred_transportation_mode";
    $order_values[] = dbquote($form->value("preferred_transportation_mode"));

    $order_fields[] = "order_voltage";
    $order_values[] = dbquote($form->value("voltage"));

    $order_fields[] = "order_preferred_delivery_date";
    $order_values[] = dbquote(from_system_date($form->value("preferred_delivery_date")));

    $order_fields[] = "order_full_delivery";
    $order_values[] = dbquote($form->value("full_delivery"));

    //$order_fields[] = "order_packaging_retraction";
    //$order_values[] = dbquote($form->value("packaging_retraction"));

    $order_fields[] = "order_pedestrian_mall_approval";
    $order_values[] = dbquote($form->value("pedestrian_mall_approval"));

    $order_fields[] = "order_delivery_comments";
    $order_values[] = dbquote($form->value("delivery_comments"));

	$order_fields[] = "order_insurance";
    $order_values[] = dbquote($form->value("order_insurance"));

    $order_fields[] = "order_special_item_request";
    $order_values[] = dbquote($form->value("comments"));


    // billing address

	$order_fields[] = "order_direct_invoice_address_id";
	$order_values[] = dbquote($form->value("invoice_address_id"));


    $order_fields[] = "order_billing_address_company";
    $order_values[] = trim($form->value("billing_address_company")) == "" ? "null" : dbquote($form->value("billing_address_company"));

    $order_fields[] = "order_billing_address_company2";
    $order_values[] = trim($form->value("billing_address_company2")) == "" ? "null" : dbquote($form->value("billing_address_company2"));

    $order_fields[] = "order_billing_address_address";
    $order_values[] = trim($form->value("billing_address_address")) == "" ? "null" : dbquote($form->value("billing_address_address"));

    $order_fields[] = "order_billing_address_address2";
    $order_values[] = trim($form->value("billing_address_address2")) == "" ? "null" : dbquote($form->value("billing_address_address2"));

    $order_fields[] = "order_billing_address_zip";
    $order_values[] = trim($form->value("billing_address_zip")) == "" ? "null" : dbquote($form->value("billing_address_zip"));

    $order_fields[] = "order_billing_address_place";
    $order_values[] = trim($form->value("billing_address_place")) == "" ? "null" : dbquote($form->value("billing_address_place"));

	$order_fields[] = "order_billing_address_place_id";
    $order_values[] = dbquote($form->value("billing_address_place_id"));

    $order_fields[] = "order_billing_address_country";
    $order_values[] = trim($form->value("billing_address_country")) == "" ? "null" : dbquote($form->value("billing_address_country"));

    $order_fields[] = "order_billing_address_phone";
    $order_values[] = trim($form->value("billing_address_phone")) == "" ? "null" : dbquote($form->value("billing_address_phone"));

    $order_fields[] = "order_billing_address_fax";
    $order_values[] = trim($form->value("billing_address_fax")) == "" ? "null" : dbquote($form->value("billing_address_fax"));

    $order_fields[] = "order_billing_address_email";
    $order_values[] = trim($form->value("billing_address_email")) == "" ? "null" : dbquote($form->value("billing_address_email"));

    $order_fields[] = "order_billing_address_contact";
    $order_values[] = trim($form->value("billing_address_contact")) == "" ? "null" : dbquote($form->value("billing_address_contact"));

    //POS location
    if(array_key_exists("shop_address_company", $form->items)) {
		$order_fields[] = "order_shop_address_company";
		$order_values[] = trim($form->value("shop_address_company")) == "" ? "null" : dbquote($form->value("shop_address_company"));

		$order_fields[] = "order_shop_address_company2";
		$order_values[] = trim($form->value("shop_address_company2")) == "" ? "null" : dbquote($form->value("shop_address_company2"));

		$order_fields[] = "order_shop_address_address";
		$order_values[] = trim($form->value("shop_address_address")) == "" ? "null" : dbquote($form->value("shop_address_address"));

		$order_fields[] = "order_shop_address_address2";
		$order_values[] = trim($form->value("shop_address_address2")) == "" ? "null" : dbquote($form->value("shop_address_address2"));

		$order_fields[] = "order_shop_address_zip";
		$order_values[] = trim($form->value("shop_address_zip")) == "" ? "null" : dbquote($form->value("shop_address_zip"));

		$order_fields[] = "order_shop_address_place";
		$order_values[] = trim($form->value("shop_address_place")) == "" ? "null" : dbquote($form->value("shop_address_place"));

		$order_fields[] = "order_shop_address_country";
		if($form->value("shop_address_country") > 0)
		{
			$order_values[] = trim($form->value("shop_address_country")) == "" ? "null" : dbquote($form->value("shop_address_country"));
		}
		else
		{
			$order_values[] = trim($form->value("delivery_address_country")) == "" ? "null" : dbquote($form->value("delivery_address_country"));
		}

		$order_fields[] = "order_shop_address_phone";
		$order_values[] = trim($form->value("shop_address_phone")) == "" ? "null" : dbquote($form->value("shop_address_phone"));

		$order_fields[] = "order_shop_address_fax";
		$order_values[] = trim($form->value("shop_address_fax")) == "" ? "null" : dbquote($form->value("shop_address_fax"));

		$order_fields[] = "order_shop_address_email";
		$order_values[] = trim($form->value("shop_address_email")) == "" ? "null" : dbquote($form->value("shop_address_email"));
	}
	

    add_fbi_info($order_fields, $order_values);
	

    $sql = "insert into orders (" . join(", ", $order_fields) . ") " .
           "values (" . join(", ", $order_values) . ")";
    mysql_query($sql) or dberror($sql);

    $order_id = mysql_insert_id();
    
    append_order_state($order_id, "100", 2, 1);

	

    // save ordered items from shopping-list
    $sql = "select basket_item_item, basket_item_category, item_price, item_name, " .
           "    basket_item_id, basket_item_parent, " .
           "    if(addon_parent = basket_item_parent, (basket_item_quantity * " . 
           "    addon_package_quantity), basket_item_quantity) as quantity " .
           "from basket_items " . 
           "left join items on basket_item_item = items.item_id " .
           "left join addons on addons.addon_parent = basket_item_parent " .
           "where basket_item_basket = " . $form->value("order_basket") . " " .
           "    and (addons.addon_child = basket_item_item or " .
           "    basket_item_parent IS NULL)";

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        order_basket_item($form, $row, $order_id);
    }

	

    // delivery address

    save_address($form, DELIVERY_ADDRESS, $order_id, 0,
                 $form->value("address_id"), "delivery_address");

    // insert delivery addresses into table order_addresses for all items
    // not having a delivery address and beloging to the same order

    $sql_order_items = "select order_item_id, order_item_order ".
                       "from order_items ".
                       "where order_item_order = " . $order_id .
                       "    and order_item_type <= ". ITEM_TYPE_SPECIAL;


    $res = mysql_query($sql_order_items) or dberror($sql_order_items);

    while ($row = mysql_fetch_assoc($res)) 
    {
        // check if record is already there
        $sql = "select order_address_id ".
               "from order_addresses ".
               "where order_address_order = " . $row["order_item_order"] .
               "    and order_address_order_item = " . $row["order_item_id"].
               "    and order_address_type = 2";

        $res1 = mysql_query($sql) or dberror($sql);
        if ($row1 = mysql_fetch_assoc($res1)) 
        {
            //nothing
        }
        else
        {
            // insert new record
    
			$delivery_address_fields = array();
            $delivery_address_values = array();

            $delivery_address_fields[] = "order_address_order";
            $delivery_address_values[] = $row["order_item_order"];

            $delivery_address_fields[] = "order_address_order_item";
            $delivery_address_values[] = $row["order_item_id"];

            $delivery_address_fields[] = "order_address_type";
            $delivery_address_values[] = 2;

            $delivery_address_fields[] = "order_address_company";
            $delivery_address_values[] = trim($form->value("delivery_address_company")) == "" ? "null" : dbquote($form->value("delivery_address_company"));

            $delivery_address_fields[] = "order_address_company2";
            $delivery_address_values[] = trim($form->value("delivery_address_company2")) == "" ? "null" : dbquote($form->value("delivery_address_company2"));

            $delivery_address_fields[] = "order_address_address";
            $delivery_address_values[] = trim($form->value("delivery_address_address")) == "" ? "null" : dbquote($form->value("delivery_address_address"));

            $delivery_address_fields[] = "order_address_address2";
            $delivery_address_values[] = trim($form->value("delivery_address_address2")) == "" ? "null" : dbquote($form->value("delivery_address_address2"));

            $delivery_address_fields[] = "order_address_zip";
            $delivery_address_values[] = trim($form->value("delivery_address_zip")) == "" ? "null" : dbquote($form->value("delivery_address_zip"));

            $delivery_address_fields[] = "order_address_place_id";
            $delivery_address_values[] = dbquote($form->value("delivery_address_place_id"));

			$delivery_address_fields[] = "order_address_place";
            $delivery_address_values[] = trim($form->value("delivery_address_place")) == "" ? "null" : dbquote($form->value("delivery_address_place"));

            $delivery_address_fields[] = "order_address_country";
            $delivery_address_values[] = trim($form->value("delivery_address_country")) == "" ? "null" : dbquote($form->value("delivery_address_country"));

            $delivery_address_fields[] = "order_address_phone";
            $delivery_address_values[] = trim($form->value("delivery_address_phone")) == "" ? "null" : dbquote($form->value("delivery_address_phone"));

            $delivery_address_fields[] = "order_address_fax";
            $delivery_address_values[] = trim($form->value("delivery_address_fax")) == "" ? "null" : dbquote($form->value("delivery_address_fax"));

            $delivery_address_fields[] = "order_address_email";
            $delivery_address_values[] = trim($form->value("delivery_address_email")) == "" ? "null" : dbquote($form->value("delivery_address_email"));

            $delivery_address_fields[] = "order_address_parent";
            $delivery_address_values[] = $form->value("address_id");

            $delivery_address_fields[] = "order_address_contact";
            $delivery_address_values[] = trim($form->value("delivery_address_contact")) == "" ? "null" : dbquote($form->value("delivery_address_contact"));

            $delivery_address_fields[] = "date_created";
            $delivery_address_values[] = "current_timestamp";

            $delivery_address_fields[] = "date_modified";
            $delivery_address_values[] = "current_timestamp";

            if (isset($_SESSION["user_login"]))
            {
                $delivery_address_fields[] = "user_created";
                $delivery_address_values[] = dbquote($_SESSION["user_login"]);

                $delivery_address_fields[] = "user_modified";
                $delivery_address_values[] = dbquote($_SESSION["user_login"]);
            }

			

            $sql = "insert into order_addresses (" . join(", ", $delivery_address_fields) . ") values (" . join(", ", $delivery_address_values) . ")";
		
			mysql_query($sql) or dberror($sql);

        }

    }
	

	//update POS-Index
	if($form->value("posaddress_id") > 0)
	{
		$fields = array();
		$values = array();

		$fields[] = "posorder_posaddress";
		$values[] = dbquote($form->value("posaddress_id"));

		$fields[] = "posorder_order";
		$values[] = dbquote($order_id);

		$fields[] = "posorder_type";
		$values[] = 2;

		$fields[] = "posorder_ordernumber";
		$values[] = dbquote($order_number);;

		$fields[] = "date_created";
		$values[] = "current_timestamp";

		$fields[] = "date_modified";
		$values[] = "current_timestamp";

		if (isset($_SESSION["user_login"]))
		{
			$fields[] = "user_created";
			$values[] = dbquote($_SESSION["user_login"]);

			$fields[] = "user_modified";
			$values[] = dbquote($_SESSION["user_login"]);
		}

		$sql = "insert into posorders (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
		mysql_query($sql) or dberror($sql);
	}

    return $order_id;
}


/********************************************************************
   update order retail data
*********************************************************************/
function order_update_retail_data($form)
{

//    $project_fields = array();
    $order_fields = array();

    // update record in table orders
    
    $value = dbquote($form->value("retail_operator"));
    $order_fields[] = "order_retail_operator = " . $value;

    $value = dbquote($form->value("order_delivery_confirmation_by"));
    $order_fields[] = "order_delivery_confirmation_by = " . $value;

    $order_fields[] = "order_show_in_delivery = 1 ";
    $order_fields[] = "order_show_to_suppliers = 1 ";

    $value = "current_timestamp";
    $order_fields[] = "date_modified = " . $value;
    
    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $order_fields[] = "user_modified = " . $value;
    }

    $sql = "update orders set " . join(", ", $order_fields) . " where order_id = " . $form->value("oid");
    mysql_query($sql) or dberror($sql);

    $sql = "update order_items ".
               "set order_item_show_to_suppliers = 1 ".
               "where order_item_order = " .  $form->value("oid");

        mysql_query($sql) or dberror($sql);
}

// order_update_order_items

/********************************************************************
   update project client data
*********************************************************************/
function  order_update_client_data($form)
{

    $order_fields = array();
    $delivery_address_fields = array();

    // update record in table orders
    $value = $form->value("client_address_id");
    $order_fields[] = "order_client_address = " . $value;

    
    $value = $form->value("status");
    $order_fields[] = "order_actual_order_state_code = " . $value;

    $value = $form->value("client_address_user_id");
    $order_fields[] = "order_user = " . $value;

    $value = trim($form->value("order_direct_invoice_address_id")) == "" ? "null" : dbquote($form->value("order_direct_invoice_address_id"));
    $order_fields[] = "order_direct_invoice_address_id = " . $value;
	
	$value = trim($form->value("billing_address_company")) == "" ? "null" : dbquote($form->value("billing_address_company"));
    $order_fields[] = "order_billing_address_company = " . $value;

     $value = trim($form->value("billing_address_company2")) == "" ? "null" : dbquote($form->value("billing_address_company2"));
    $order_fields[] = "order_billing_address_company2 = " . $value;

    $value = trim($form->value("billing_address_address")) == "" ? "null" : dbquote($form->value("billing_address_address"));
    $order_fields[] = "order_billing_address_address = " . $value;

    $value = trim($form->value("billing_address_address2")) == "" ? "null" : dbquote($form->value("billing_address_address2"));
    $order_fields[] = "order_billing_address_address2 = " . $value;

    $value = trim($form->value("billing_address_zip")) == "" ? "null" : dbquote($form->value("billing_address_zip"));
    $order_fields[] = "order_billing_address_zip = " . $value;

    $value = dbquote($form->value("billing_address_place_id"));
    $order_fields[] = "order_billing_address_place_id = " . $value;

	$value = trim($form->value("billing_address_place")) == "" ? "null" : dbquote($form->value("billing_address_place"));
    $order_fields[] = "order_billing_address_place = " . $value;

    $value = trim($form->value("billing_address_country")) == "" ? "null" : dbquote($form->value("billing_address_country"));
    $order_fields[] = "order_billing_address_country = " . $value;

    $value = trim($form->value("billing_address_phone")) == "" ? "null" : dbquote($form->value("billing_address_phone"));
    $order_fields[] = "order_billing_address_phone = " . $value;

    $value = trim($form->value("billing_address_fax")) == "" ? "null" : dbquote($form->value("billing_address_fax"));
    $order_fields[] = "order_billing_address_fax = " . $value;

    $value = trim($form->value("billing_address_email")) == "" ? "null" : dbquote($form->value("billing_address_email"));
    $order_fields[] = "order_billing_address_email = " . $value;

    $value = trim($form->value("billing_address_contact")) == "" ? "null" : dbquote($form->value("billing_address_contact"));
    $order_fields[] = "order_billing_address_contact = " . $value;

    $value = trim($form->value("shop_address_company")) == "" ? "null" : dbquote($form->value("shop_address_company"));
    $order_fields[] = "order_shop_address_company = " . $value;

     $value = trim($form->value("shop_address_company2")) == "" ? "null" : dbquote($form->value("shop_address_company2"));
    $order_fields[] = "order_shop_address_company2 = " . $value;

    $value = trim($form->value("shop_address_address")) == "" ? "null" : dbquote($form->value("shop_address_address"));
    $order_fields[] = "order_shop_address_address = " . $value;

    $value = trim($form->value("shop_address_address2")) == "" ? "null" : dbquote($form->value("shop_address_address2"));
    $order_fields[] = "order_shop_address_address2 = " . $value;

    $value = trim($form->value("shop_address_zip")) == "" ? "null" : dbquote($form->value("shop_address_zip"));
    $order_fields[] = "order_shop_address_zip = " . $value;

    $value = trim($form->value("shop_address_place")) == "" ? "null" : dbquote($form->value("shop_address_place"));
    $order_fields[] = "order_shop_address_place = " . $value;

    $value = trim($form->value("shop_address_country")) == "" ? "null" : dbquote($form->value("shop_address_country"));
    $order_fields[] = "order_shop_address_country = " . $value;

	$value = trim($form->value("shop_address_phone")) == "" ? "null" : dbquote($form->value("shop_address_phone"));
    $order_fields[] = "order_shop_address_phone = " . $value;

	$value = trim($form->value("shop_address_fax")) == "" ? "null" : dbquote($form->value("shop_address_fax"));
    $order_fields[] = "order_shop_address_fax = " . $value;

	$value = trim($form->value("shop_address_email")) == "" ? "null" : dbquote($form->value("shop_address_email"));
    $order_fields[] = "order_shop_address_email = " . $value;

    $value = trim($form->value("preferred_transportation_arranged")) == "" ? "null" : dbquote($form->value("preferred_transportation_arranged"));
    $order_fields[] = "order_preferred_transportation_arranged = " . $value;

	$value = trim($form->value("preferred_transportation_mode")) == "" ? "null" : dbquote($form->value("preferred_transportation_mode"));
    $order_fields[] = "order_preferred_transportation_mode = " . $value;

    $value = trim($form->value("voltage")) == "" ? "null" : dbquote($form->value("voltage"));
    $order_fields[] = "order_voltage = " . $value;

    $value = trim($form->value("preferred_delivery_date")) == "" ? "null" : dbquote(from_system_date($form->value("preferred_delivery_date")));
    $order_fields[] = "order_preferred_delivery_date = " . $value;


    $value = trim($form->value("full_delivery")) == "" ? "null" : dbquote($form->value("full_delivery"));
    $order_fields[] = "order_full_delivery = " . $value;

    //$value = trim($form->value("packaging_retraction")) == "" ? "null" : dbquote($form->value("packaging_retraction"));
    //$order_fields[] = "order_packaging_retraction = " . $value;

    $value = trim($form->value("pedestrian_mall_approval")) == "" ? "null" : dbquote($form->value("pedestrian_mall_approval"));
    $order_fields[] = "order_pedestrian_mall_approval = " . $value;

    $value = trim($form->value("delivery_comments")) == "" ? "null" : dbquote($form->value("delivery_comments"));
    $order_fields[] = "order_delivery_comments = " . $value;

	$value = dbquote($form->value("order_insurance"));
    $order_fields[] = "order_insurance = " . $value;


	$value = $form->value("order_insurance");
    $order_fields[] = "order_insurance = " . $value;

    $value = "current_timestamp";
    $order_fields[] = "date_modified = " . $value;

    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $order_fields[] = "user_modified = " . $value;
    }

    $sql = "update orders set " . join(", ", $order_fields) . " where order_id = " . $form->value("oid");
    mysql_query($sql) or dberror($sql);


    // update delivery address in table oder_addresses
    $value = trim($form->value("delivery_address_company")) == "" ? "null" : dbquote($form->value("delivery_address_company"));
    $delivery_address_fields[] = "order_address_company = " . $value;

     $value = trim($form->value("delivery_address_company2")) == "" ? "null" : dbquote($form->value("delivery_address_company2"));
    $delivery_address_fields[] = "order_address_company2 = " . $value;

    $value = trim($form->value("delivery_address_address")) == "" ? "null" : dbquote($form->value("delivery_address_address"));
    $delivery_address_fields[] = "order_address_address = " . $value;

    $value = trim($form->value("delivery_address_address2")) == "" ? "null" : dbquote($form->value("delivery_address_address2"));
    $delivery_address_fields[] = "order_address_address2 = " . $value;

    $value = trim($form->value("delivery_address_zip")) == "" ? "null" : dbquote($form->value("delivery_address_zip"));
    $delivery_address_fields[] = "order_address_zip = " . $value;

    $value = dbquote($form->value("delivery_address_place_id"));
    $delivery_address_fields[] = "order_address_place_id = " . $value;

	$value = trim($form->value("delivery_address_place")) == "" ? "null" : dbquote($form->value("delivery_address_place"));
    $delivery_address_fields[] = "order_address_place = " . $value;

	$value = trim($form->value("delivery_address_place_id")) == "" ? "null" : dbquote($form->value("delivery_address_place_id"));
    $delivery_address_fields[] = "order_address_place_id = " . $value;

    $value = trim($form->value("delivery_address_country")) == "" ? "null" : dbquote($form->value("delivery_address_country"));
    $delivery_address_fields[] = "order_address_country = " . $value;

    $value = trim($form->value("delivery_address_phone")) == "" ? "null" : dbquote($form->value("delivery_address_phone"));
    $delivery_address_fields[] = "order_address_phone = " . $value;

    $value = trim($form->value("delivery_address_fax")) == "" ? "null" : dbquote($form->value("delivery_address_fax"));
    $delivery_address_fields[] = "order_address_fax = " . $value;

    $value = trim($form->value("delivery_address_email")) == "" ? "null" : dbquote($form->value("delivery_address_email"));
    $delivery_address_fields[] = "order_address_email = " . $value;

    $value = $form->value("client_address_id");
    $delivery_address_fields[] = "order_address_parent = " . $value;

    $value = trim($form->value("delivery_address_contact")) == "" ? "null" : dbquote($form->value("delivery_address_contact"));
    $delivery_address_fields[] = "order_address_contact = " . $value;

    $value = "current_timestamp";
    $delivery_address_fields[] = "date_modified = " . $value;

    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $delivery_address_fields[] = "user_modified = " . $value;
    }

    $sql = "update order_addresses set " . join(", ", $delivery_address_fields) . " where order_address_type = 2 and order_address_order = " . $form->value("oid");
    mysql_query($sql) or dberror($sql);


	// update POSaddresses (POS Index)
	if($form->value("posaddress_id") > 0)
	{
		$sql = "select posorder_id " . 
			   "from posorders " . 
			   "where posorder_order = " . $form->value("oid");
		
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
		
			$sql = "Update posorders set " . 
				   "posorder_posaddress = " . $form->value("posaddress_id") . 
				   " where posorder_id = " . $row["posorder_id"];

			mysql_query($sql) or dberror($sql);
		}
		else
		{
			$sql = "insert into posorders (" . 
				   "posorder_posaddress, " .
				   "posorder_order, " .
				   "posorder_ordernumber, " .
				   "posorder_type) Values ( " .
				   $form->value("posaddress_id") . ", " .
				   $form->value("oid") . ", " .
				   dbquote($form->value("order_number")) . ", " .
				   2  . ")";

			$result = mysql_query($sql) or dberror($sql);
		}
	
	}


} // order_update_client_data


/** 
* get_order_confirmation
*
* composes form displaying order confirmation
*
*/
function get_order_confirmation(&$form, $address_id, $basket)
{
    $form->add_hidden("address_id", $address_id);

    $client_address = get_address($address_id);

    $form->add_comment("Please check the order before submitting!");
    $form->add_section("");
    $form->add_label("address_company","Client", 0 ,$client_address['company']);

    
	// notify address
	/*
    $form->add_section("Notify Address (notify address)");
    $form->add_hidden("billing_address_id", param("billing_address_id"));
    $form->add_hidden("billing_address_country", param("billing_address_country"));

    $form->add_label("billing_address_company", "Company", 0, param("billing_address_company"));
    $form->add_label("billing_address_company2", "", HIDEEMPTY,
                     param("billing_address_company2"));
    $form->add_label("billing_address_address", "Address", HIDEEMPTY,
                     param("billing_address_address"));
    $form->add_label("billing_address_address2", "", HIDEEMPTY, param("billing_address_address2"));
    $form->add_label("billing_address_zip", "ZIP", HIDEEMPTY, param("billing_address_zip"));
    $form->add_label("billing_address_place", "City", HIDEEMPTY, param("billing_address_place"));

    $form->add_label("billing_address_country_name", "Country", 0, 
                  get_country(param("billing_address_country")));

    $form->add_label("billing_address_phone", "Phone", HIDEEMPTY, param("billing_address_phone"));
    $form->add_label("billing_address_fax", "Fax", HIDEEMPTY, param("billing_address_fax"));
    $form->add_label("billing_address_email", "Email", HIDEEMPTY, param("billing_address_email"));
    $form->add_label("billing_address_contact", "Contact", HIDEEMPTY,
                     param("billing_address_contact"));

    */
	
	$form->add_hidden("invoice_address_id", param("invoice_address_id"));

	$form->add_hidden("billing_address_company", param("billing_address_company"));
    $form->add_hidden("billing_address_company2",param("billing_address_company2"));
    $form->add_hidden("billing_address_address", param("billing_address_address"));
    $form->add_hidden("billing_address_address2", param("billing_address_address2"));
    $form->add_hidden("billing_address_zip",param("billing_address_zip"));
    $form->add_hidden("billing_address_place", param("billing_address_place"));
	$form->add_hidden("billing_address_place_id", param("billing_address_place_id"));
    $form->add_hidden("billing_address_country", param("billing_address_country"));
    $form->add_hidden("billing_address_phone",param("billing_address_phone"));
    $form->add_hidden("billing_address_fax", param("billing_address_fax"));
    $form->add_hidden("billing_address_email", param("billing_address_email"));
    $form->add_hidden("billing_address_contact", param("billing_address_contact"));

	// Delivery Address

    $form->add_section("Delivery Address (consignee address)");
    $form->add_hidden("delivery_address_country", param("delivery_address_country"));
    
	/*
	$form->add_hidden("delivery_is_billing_address", 
                      param("delivery_is_billing_address"));

    
	if (param("delivery_is_billing_address"))
    {
        $delivery_address_company = param("billing_address_company");
        $delivery_address_company2 = param("billing_address_company2");
        $delivery_address_address = param("billing_address_address");
        $delivery_address_address2 = param("billing_address_address2");
        $delivery_address_zip = param("billing_address_zip");
        $delivery_address_place = param("billing_address_place");
        $delivery_address_country = param("billing_address_country");
        $delivery_address_phone = param("billing_address_phone");
        $delivery_address_fax = param("billing_address_fax");
        $delivery_address_email = param("billing_address_email");
        $delivery_address_contact =  param("billing_address_contact");
    }
    else
    {
        $delivery_address_company = param("delivery_address_company");
        $delivery_address_company2 = param("delivery_address_company2");
        $delivery_address_address = param("delivery_address_address");
        $delivery_address_address2 = param("delivery_address_address2");
        $delivery_address_zip = param("delivery_address_zip");
        $delivery_address_place = param("delivery_address_place");
        $delivery_address_country = param("delivery_address_country");
        $delivery_address_phone = param("delivery_address_phone");
        $delivery_address_fax = param("delivery_address_fax");
        $delivery_address_email = param("delivery_address_email");
        $delivery_address_contact =  param("delivery_address_contact");
    }
	*/

	$delivery_address_company = param("delivery_address_company");
	$delivery_address_company2 = param("delivery_address_company2");
	$delivery_address_address = param("delivery_address_address");
	$delivery_address_address2 = param("delivery_address_address2");
	$delivery_address_zip = param("delivery_address_zip");
	$delivery_address_province_id = param("delivery_address_province_id");
	$delivery_address_place_id = param("delivery_address_place_id");
	$delivery_address_place = param("delivery_address_place");
	$delivery_address_country = param("delivery_address_country");
	$delivery_address_phone = param("delivery_address_phone");
	$delivery_address_fax = param("delivery_address_fax");
	$delivery_address_email = param("delivery_address_email");
	$delivery_address_contact =  param("delivery_address_contact");

    $form->add_label("delivery_address_company", "Company", 0, 
                     $delivery_address_company);
    $form->add_label("delivery_address_company2", "",HIDEEMPTY,
                     $delivery_address_company2);
    $form->add_label("delivery_address_address", "Address", HIDEEMPTY,
                     $delivery_address_address);
    $form->add_label("delivery_address_address2", "", HIDEEMPTY, 
                     $delivery_address_address2);
    $form->add_label("delivery_address_zip", "ZIP", 0, 
                     $delivery_address_zip);

	 $form->add_hidden("delivery_address_place_id", $delivery_address_place_id);
	 $form->add_hidden("delivery_address_province_id", $delivery_address_province_id);

    $form->add_label("delivery_address_place", "City", HIDEEMPTY, 
                     $delivery_address_place);
    $form->add_label("delivery_address_country_name", "Country", HIDEEMPTY, 
                     get_country($delivery_address_country));

    $form->add_label("delivery_address_phone", "Phone", HIDEEMPTY, 
                     $delivery_address_phone);
    $form->add_label("delivery_address_fax", "Fax",HIDEEMPTY, 
                     $delivery_address_fax);
    $form->add_label("delivery_address_email", "Email",HIDEEMPTY, 
                     $delivery_address_email);
    $form->add_label("delivery_address_contact", "Contact" , HIDEEMPTY, 
                     $delivery_address_contact);

    // POS Address

    
    /*
	$form->add_hidden("shop_is_delivery_address", param("shop_is_delivery_address"));
   
	if (param("shop_is_delivery_address"))
    {
        $shop_address_company = $delivery_address_company;
        $shop_address_company2 = $delivery_address_company2;
        $shop_address_address = $delivery_address_address;
        $shop_address_address2 = $delivery_address_address2;
        $shop_address_zip = $delivery_address_zip;
        $shop_address_place = $delivery_address_place;
        $shop_address_phone = $delivery_address_phone;
        $shop_address_fax = $delivery_address_fax;
        $shop_address_email = $delivery_address_email;
        $shop_address_country = $delivery_address_country;
    }
    else
    {
        $shop_address_company = param("shop_address_company");
        $shop_address_company2 = param("shop_address_company2");
        $shop_address_address = param("shop_address_address");
        $shop_address_address2 = param("shop_address_address2");
        $shop_address_zip = param("shop_address_zip");
        $shop_address_place = param("shop_address_place");
        $shop_address_phone = param("shop_address_phone");
        $shop_address_fax = param("shop_address_fax");
        $shop_address_email = param("shop_address_email");
        $shop_address_country = param("shop_address_country");
		
    }
	*/
	
	$form->add_hidden("posaddress_id", param("posaddress_id"));
	if(param("shop_address_company")) {
		$form->add_section("POS Location Address");
		$shop_address_company = param("shop_address_company");
		$shop_address_company2 = param("shop_address_company2");
		$shop_address_address = param("shop_address_address");
		$shop_address_address2 = param("shop_address_address2");
		$shop_address_zip = param("shop_address_zip");
		$shop_address_place = param("shop_address_place");
		$shop_address_phone = param("shop_address_phone");
		$shop_address_fax = param("shop_address_fax");
		$shop_address_email = param("shop_address_email");
		$shop_address_country = param("shop_address_country");

		
		$form->add_hidden("shop_address_country", $shop_address_country);
		$shop_address_country_name = get_country($shop_address_country);

		$form->add_hidden("shop_address_country", $shop_address_country);
		$form->add_label("shop_address_company", "Company", 0, 
							  $shop_address_company);
		$form->add_label("shop_address_company2", "", HIDEEMPTY, 
							  $shop_address_company2);
		$form->add_label("shop_address_address", "Address", 0, 
							  $shop_address_address);
		$form->add_label("shop_address_address2", "", HIDEEMPTY, 
							  $shop_address_address2);
		$form->add_label("shop_address_zip", "ZIP", 0, 
							  $shop_address_zip);
		$form->add_label("shop_address_place", "City", 0, 
							  $shop_address_place);
		$form->add_label("shop_address_country_name", "Country",0 ,$shop_address_country_name);
		$form->add_label("shop_address_phone", "Phone", 0, 
							  $shop_address_phone);
		$form->add_label("shop_address_fax", "Fax", 0, 
							  $shop_address_fax);
		$form->add_label("shop_address_email", "Email", 0, 
							  $shop_address_email);
	}

    // Transportation Preferences

    $form->add_section("Transportation Preferences");
    $form->add_hidden("preferred_transportation_arranged", param("preferred_transportation_arranged"));
	$form->add_hidden("preferred_transportation_mode", param("preferred_transportation_mode"));
    //$form->add_hidden("packaging_retraction", param("packaging_retraction"));
    $form->add_hidden("pedestrian_mall_approval", param("pedestrian_mall_approval"));
    $form->add_hidden("full_delivery", param("full_delivery"));

    $form->add_label("preferred_delivery_date", "Preferred Arrival Date", 0,
                          param("preferred_delivery_date"));

    $form->add_label("preferred_transportation_arranged_name", "Transportation arranged by", 0, 
                          get_preferred_transportation(param("preferred_transportation_arranged")));

	 $form->add_label("preferred_transportation_mode_name", "Transportation mode", 0, 
                          get_preferred_transportation(param("preferred_transportation_mode")));

    /*
	$form->add_label("packaging_retraction_name", "Packaging Retraction Desired", 0, 
                          (param("packaging_retraction")) ? "yes":"no" );

    */

	$form->add_label("pedestrian_mall_approval_name", "Pedestrian Area Approval Needed", 0, 
                        (param("pedestrian_mall_approval")) ? "yes":"no" );

    $form->add_label("full_delivery_name", "Full Delivery Desired", 0, 
                        (param("full_delivery")) ? "yes":"no" );

    $form->add_label("delivery_comments", "Delivery Comments", 0,
                          param("delivery_comments"));
	
	$form->add_hidden("order_insurance", param("order_insurance"));
	if(param("order_insurance") == 1)
	{
		$form->add_label("insurance", "Insurance by " . BRAND . "/Forwarder", 0,"covered");
	}
	else
	{
		$form->add_label("insurance", "Insurance by " . BRAND . "/Forwarder", 0,"not covered");
	}

	$form->add_label("voltage_name", "Voltage", 0, get_voltage(param('voltage')));
    $form->add_hidden("voltage", param('voltage'),0);

    $form->add_label("comments", "Comments", 0, param("comments"));

    $form->add_hidden("order_basket", $basket, 0);
 //   $form->add_button("submit_order", "Submit Order");
 //   $form->add_button("checkout_edit", "Back");

    $sql = "select distinct basket_item_id, items.item_id, items.item_code, items.item_name, " .
           "    items.item_priority, round(((items.item_price / currency_exchange_rate) * " .
           "    currency_factor),2) as price, " .
           "    category_name, basket_item_category, basket_item_quantity, " .
           "    round((items.item_price * basket_item_quantity  / currency_exchange_rate * " .
           "    currency_factor),2) as sub_total, product_line_priority, category_priority, " .
           "    concat_ws(': ', product_line_name, category_name) as group_head " .
           "from basket_items " .
           "left join items on basket_item_item = items.item_id " .
           "left join categories on basket_item_category = categories.category_id " .
           "left join product_lines on categories.category_product_line = " .
           "    product_lines.product_line_id " .
           "left join users on users.user_id = " . user_id() . " " .
           "left join addresses on addresses.address_id = users.user_address " .
           "left join currencies on currencies.currency_id = addresses.address_currency ";

    $currency = get_user_currency(user_id());

    $list = new ListView($sql, LIST_HAS_FOOTER | LIST_HAS_HEADER);

    $list->set_entity("basket_items");
    $list->set_filter("basket_item_basket=" . $basket . " and basket_item_parent IS NULL");
    $list->set_group("group_head", "group_head");
    $list->set_order("product_line_priority, category_priority");

    $list->add_column("item_code", "Item Code", "", LIST_FILTER_FREE ,'', COLUMN_NO_WRAP);
    $list->add_column("item_name", "Name", "", LIST_FILTER_FREE);
    $list->add_column("price", "Price " . $currency['currency_symbol'], "", '', '',
                      COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

    $list->add_column("basket_item_quantity", "Quantity", "", 0);
            $list->add_column("sub_total", "Sub Total " . $currency['currency_symbol'], "", '', '', COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

    $list->set_footer("sub_total", number_format(get_basket_total("basket", $basket),2));

    /*****************************************************************************/
    // Addon-List

    $sql = "select distinct basket_item_id, items.item_id, items.item_code, items.item_name, " .
           "    items.item_priority, round(((items.item_price / currency_exchange_rate) * " .
           "    currency_factor),2) as price, " .
           "    category_name, basket_item_category, basket_item_quantity, " .
           "    round((items.item_price * basket_item_quantity  / currency_exchange_rate * " .
           "    currency_factor * addons.addon_package_quantity),2) as sub_total, " .
           "    product_line_priority, category_priority, " .
           "    if(addon_child = basket_item_item, concat('Add-Ons for', ' ',  items1.item_code, ': ', " .
           "    items1.item_name), items1.item_name) as group_head, " .
           "    concat_ws(' x ', addons.addon_package_quantity, items.item_name) as addon_ext_name " . 
           "from basket_items " .
           "left join items on basket_item_item = items.item_id " .
           "left join addons on addons.addon_child = basket_item_item " .
           "left join items as items1 on addon_parent = items1.item_id " .
           "left join categories on basket_item_category = categories.category_id " .
           "left join product_lines on categories.category_product_line = " .
           "    product_lines.product_line_id " .
           "left join users on users.user_id = " . user_id() . " " .
           "left join addresses on addresses.address_id = users.user_address " .
           "left join currencies on currencies.currency_id = addresses.address_currency ";

    $list2 = new ListView($sql, LIST_HAS_FOOTER | LIST_HAS_HEADER);

    $list2->set_title("Add-Ons");
    $list2->set_entity("basket_items_addons");
    $list2->set_filter("basket_item_basket=" . $basket . 
                       "     and basket_item_parent IS NOT NULL " .
                       "     and addons.addon_parent = basket_item_parent");

    $list2->set_group("group_head", "group_head");
    $list2->set_order("product_line_priority, category_priority");

    // access to product-details is restricted

    $popup_link = "";
    if (has_access("can_view_catalog_detail"))
    {
        $popup_link = "popup:item_view.php?id={item_id}";
    }

    $list2->add_column("item_code", "Item Code", $popup_link, LIST_FILTER_FREE ,'', COLUMN_NO_WRAP);
    $list2->add_column("addon_ext_name", "Name", "", LIST_FILTER_FREE);
    $list2->add_column("price", "Price " . $currency['currency_symbol'], "", 
                      '', '', COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
    $list2->add_column("basket_item_quantity", "Quantity", "", LIST_FILTER_FREE, '', COLUMN_ALIGN_RIGHT);
    $list2->add_column("sub_total", "Sub Total " . $currency['currency_symbol'], "", 
                       LIST_FILTER_FREE, '', COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

    $list2->set_footer("item_code", "Total");
    $list2->set_footer("sub_total", number_format(get_basket_part_total($basket, true),2));

    return array($list, $list2);
}

function dump_order_data($order_data = "")
{
    
	$order_data = array("address_id" => param("address_id"),
                        "address_company" => param("address_company"),
                        "billing_address_id" => param("billing_address_id"),
                        "billing_address_country" => param("billing_address_country"),
                        "billing_address_company" => param("billing_address_company"),
                        "billing_address_company2" => param("billing_address_company2"),
                        "billing_address_address" => param("billing_address_address"),
                        "billing_address_address2" => param("billing_address_address2"),
                        "billing_address_zip" => param("billing_address_zip"),
                        "billing_address_place" => param("billing_address_place"),
		                "billing_address_place_id" => param("billing_address_place_id"),
                        "billing_address_phone" =>  param("billing_address_phone"),
                        "billing_address_fax" => param("billing_address_fax"),
                        "billing_address_email" => param("billing_address_email"),
                        "billing_address_contact" => param("billing_address_contact"),
                        "delivery_address_company" => param("delivery_address_company"),
                        "delivery_address_address" => param("delivery_address_address"),
                        "delivery_address_zip" => param("delivery_address_zip"),
						"delivery_address_province_id" => param("delivery_address_province_id"),
					    "delivery_address_place_id" => param("delivery_address_place_id"),
                        "delivery_address_place" => param("delivery_address_place"),
                        "delivery_address_country" => param("delivery_address_country"),
                        "delivery_address_phone" => param("delivery_address_phone"),
                        "delivery_address_contact" => param("delivery_address_contact"),
                        "delivery_address_fax" => param("delivery_address_fax"),
                        "delivery_address_email" => param("delivery_address_email"),
                        "shop_address_company" => param("shop_address_company"),
                        "shop_address_address" => param("shop_address_address"),
                        "shop_address_address" => param("shop_address_address"),
                        "shop_address_zip" => param("shop_address_zip"),
                        "shop_address_place" =>param("shop_address_place"),
                        "shop_address_country" => param("shop_address_country"),
                        "preferred_transportation_arranged" => param("preferred_transportation_arranged"),
		                "preferred_transportation_mode" => param("preferred_transportation_mode"),
                        "pedestrian_mall_approval" => param("pedestrian_mall_approval"),
                        "full_delivery" => param("full_delivery"),
                        "preferred_delivery_date" => param("preferred_delivery_date"),
                        "delivery_comments" => param("delivery_comments"),
                        "voltage" => param("voltage"),
                        "comments" => param("comments"));

    $sql = "update baskets " .
           "set basket_dump ='" . addslashes(serialize($order_data)) . "' " .
           "where basket_id = " . get_users_basket();

    $res = mysql_query($sql) or dberror($sql);
}


function get_order_data_dump()
{
    $order_data = array();
    $sql = "select basket_dump " .
           "from baskets " .
           "where basket_id = " . get_users_basket();

	$res = mysql_query($sql) or dberror();
    if ($res)
    {
        $row = mysql_fetch_assoc($res);
        $order_data = unserialize(stripslashes($row['basket_dump']));
    }

    return $order_data;
}

/*****************************************************************************
* get_checkout_form
*
*/
function get_checkout_form(&$form, $address_id, $choices)
{
    $user = get_user(user_id());
	$order_data = get_order_data_dump();
    if (empty($order_data['address_id']))
    {
        $order_data['address_id'] = $address_id;
    }

    $sql_countries = "select country_id, country_name ".
                     "from countries ".
                     "order by country_name";

    $form->add_hidden("status", "edit");
    $form->add_lookup("address_id","Client", "addresses" , "address_company", 0, $address_id);

    /*
	$tmp_text1="Please indicate the notify address.";
    $tmp_text3="Please indicate the delivery address.";
    $tmp_text2="\nYou can either select an existing address or enter a new address.";

    // Notify Address

    $billing_addresses = get_address_array($address_id, BILLING_ADDRESS);

    $form->add_section("Notify Address");

    if (count($billing_addresses) > 0 )
    {
        $form->add_comment($tmp_text1 . $tmp_text2);
        $form->add_list("billing_address_id", "Notify to", $billing_addresses, SUBMIT);
    }
    else
    {
        $form->add_comment($tmp_text1);
    }

    $address =  get_address($address_id);
    $user_properties = get_user(user_id());

  
    $sql_countries = "select country_id, country_name ".
                     "from countries ".
                     "order by country_name";

    $form->add_edit("billing_address_company", "Company*", NOTNULL, $address["company"], TYPE_CHAR);
    $form->add_edit("billing_address_company2", "", 0, $address["company2"], TYPE_CHAR);
    $form->add_edit("billing_address_address", "Address*", NOTNULL, $address["address"], TYPE_CHAR);
    $form->add_edit("billing_address_address2", "", 0, $address["address2"], TYPE_CHAR);
    $form->add_edit("billing_address_zip", "ZIP*", NOTNULL, $address["zip"], TYPE_CHAR, 20);
    $form->add_edit("billing_address_place", "City*", NOTNULL, $address["place"], TYPE_CHAR, 20);
    $form->add_list("billing_address_country", "Country*", $sql_countries, NOTNULL, $address["country"]);
    $form->add_edit("billing_address_phone", "Phone*", NOTNULL, $user_properties["phone"], TYPE_CHAR, 20);
    $form->add_edit("billing_address_fax", "Fax", 0, $user_properties["fax"], TYPE_CHAR, 20);
    $form->add_edit("billing_address_email", "Email", 0, $user_properties["email"], TYPE_CHAR, 50);
    $form->add_edit("billing_address_contact", "Contact*", NOTNULL, $user_properties["contact"], TYPE_CHAR);
	*/


	//BILLING
	

	$address =  get_address($address_id);

	//invoice to directly to the client

	//get invoice_addresses
	$invoice_addresses = array();

	$sql_inv = "select invoice_address_id, concat(invoice_address_company, ', ' ,place_name, ', ', country_name) as company " .
			"from invoice_addresses " .
			"left join places on place_id = invoice_address_place_id " . 
			"left join countries on country_id = invoice_address_country_id " . 
			"where invoice_address_active = 1 and invoice_address_address_id = " . $address_id . 
			" order by invoice_address_company";

	$res = mysql_query($sql_inv) or dberror($sql_inv);
	while ($row = mysql_fetch_assoc($res))
	{
		$invoice_addresses[$row["invoice_address_id"]] = $row["company"];
	}

	if(count($invoice_addresses) > 0)
	{
		$invoice_addresses[0] = $address["company"];
		
		$form->add_section("Notify Address (invoice address)");
		$form->add_comment("Please indicate the notify address (invoice address) in case suppliers do send invoices directly to your company.");
		$form->add_list("invoice_address_id", "Notify address", $invoice_addresses);
	}
	else
	{
		$form->add_hidden("invoice_address_id", 0);
	}

	
    $user_properties = get_user(user_id());

	$invoice_address =  get_client_address($user_properties["address"]);
	if(count($invoice_address) > 0 and $invoice_address["invoice_recipient"] > 0)
	{
		$invoice_address =  get_client_address($invoice_address["invoice_recipient"]);
		$form->add_hidden("billing_address_company", $invoice_address["company"]);
		$form->add_hidden("billing_address_company2",$invoice_address["company2"]);
		$form->add_hidden("billing_address_address", $invoice_address["address"]);
		$form->add_hidden("billing_address_address2", $invoice_address["address2"]);
		$form->add_hidden("billing_address_zip",$invoice_address["zip"]);
		$form->add_hidden("billing_address_place_id", $invoice_address["place_id"]);
		$form->add_hidden("billing_address_place", $invoice_address["place"]);
		$form->add_hidden("billing_address_country", $invoice_address["country"]);
		$form->add_hidden("billing_address_phone",$invoice_address["phone"]);
		$form->add_hidden("billing_address_fax", $invoice_address["fax"]);
		$form->add_hidden("billing_address_email", $invoice_address["email"]);
		$form->add_hidden("billing_address_contact", $invoice_address["contact_name"]);
	}
	else
	{
		$form->add_hidden("billing_address_company", $address["company"]);
		$form->add_hidden("billing_address_company2",$address["company2"]);
		$form->add_hidden("billing_address_address", $address["address"]);
		$form->add_hidden("billing_address_address2", $address["address2"]);
		$form->add_hidden("billing_address_zip",$address["zip"]);
		$form->add_hidden("billing_address_place_id", $address["place_id"]);
		$form->add_hidden("billing_address_place", $address["place"]);
		$form->add_hidden("billing_address_country", $address["country"]);
		$form->add_hidden("billing_address_phone",$user_properties["phone"]);
		$form->add_hidden("billing_address_fax", $user_properties["fax"]);
		$form->add_hidden("billing_address_email", $user_properties["email"]);
		$form->add_hidden("billing_address_contact", $user_properties["contact"]);
	}



	 // POS-Address

    $form->add_section("POS Location");
	
	$sql = "select posaddress_id as num_recs " . 
		            "from posaddresses " . 
		            "where posaddress_client_id = " . $user["address"];
	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$form->add_comment("Please select or indicate the POS address in case the order is used for an existing POS.");
		$posaddresses = "select posaddress_id, concat(posaddress_place, ', ', posaddress_name) as company " . 
						"from posaddresses " . 
						" where (posaddress_store_closingdate = '0000-00-00' or posaddress_store_closingdate is null) " . 
			            " and posaddress_client_id = " . $user["address"] . 
			            " order by posaddress_place";

		$form->add_list("posaddress_id", "POS Location", $posaddresses, SUBMIT);
	}
	else
	{
		$form->add_comment("Please indicate the POS address in case the order is used for an existing POS.");
		$form->add_hidden("posaddress_id", 0);
	}


    $form->add_hidden("shop_address_company");

    $form->add_hidden("shop_address_company2");

    $form->add_hidden("shop_address_address");

    $form->add_hidden("shop_address_address2");

    $form->add_hidden("shop_address_zip", "ZIP");

    $form->add_hidden("shop_address_place");
	$form->add_hidden("shop_address_place_id");
	$form->add_hidden("shop_address_province_id");

    $form->add_hidden("shop_address_country");

    $form->add_hidden("shop_address_phone");

    $form->add_hidden("shop_address_fax");

    $form->add_hidden("shop_address_email");

	$form->add_hidden("shop_address_contact");

    // Delivery Address
	$form->add_section("Delivery Address");

	/*
    $form->add_checkbox("delivery_is_billing_address", 
                        "delivery address is identical to notify address",
                        isset($order_data['delivery_is_billing_address']) ?
                        $order_data['delivery_is_billing_address'] :  0);

    $delivery_addresses = get_address_array($address_id, DELIVERY_ADDRESS);

    if (count($delivery_addresses) > 0 )
    {
        $form->add_comment($tmp_text3 . $tmp_text2);
        $form->add_list("delivery_address_id", "Delivery to", $delivery_addresses, SUBMIT);
    }
    else
    {
        $form->add_comment($tmp_text1);
    }

    $order_data['billing_address_country'] = (empty($order_data['billing_address_country'])) ? 
        $address['country'] : $order_data['billing_address_country'];

    */

	$standard_delivery_address = array();
	$sql_delivery_addresses = "select * " . 
							  "from standard_delivery_addresses " . 
							  "where delivery_address_address_id = " . $user["address"] . 
							  " order by delivery_address_company";

	$res = mysql_query($sql_delivery_addresses);

	if ($row = mysql_fetch_assoc($res))
	{
		$standard_delivery_address = $row;

		$sql = "select place_province from places where place_id = " . $standard_delivery_address["delivery_address_place_id"];
		$res = mysql_query($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$standard_delivery_address["delivery_address_province_id"] = $row["place_province"];

		}
	}

	if(count($standard_delivery_address) == 0) {
	   $standard_delivery_address["delivery_address_country"] = $address["country"];	
	}


	$form->add_charlist("delivery_is_address", "Delivery address is identical to*", $choices, SUBMIT);

	$form->add_edit("delivery_address_company", "Company*", 0, 
                    isset($standard_delivery_address['delivery_address_company']) ? 
                        $standard_delivery_address['delivery_address_company']: "");

    $form->add_edit("delivery_address_company2", "",0,
                    isset($standard_delivery_address['delivery_address_company2']) ? 
                    $standard_delivery_address['delivery_address_company2'] : "");


	$form->add_list("delivery_address_country", "Country*", $sql_countries, SUBMIT,
                isset($standard_delivery_address['delivery_address_country']) ?
                $standard_delivery_address['delivery_address_country'] :  "");

	
	if (param("delivery_is_address") == 1)
	{
		
		$sql = "select posaddress_name, posaddress_name2,posaddress_address, posaddress_address2, " . 
			   "posaddress_zip, posaddress_place_id, posaddress_place, posaddress_country, posaddress_phone, ". 
			   "posaddress_fax, posaddress_email, posaddress_contact_name, place_province " . 
			   "from posaddresses " . 
			   "left join places on place_id = posaddress_place_id " . 
			   "where posaddress_id = " . dbquote(param("posaddress_id"));

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
        {
			$sql_provinces = "select province_id, province_canton from provinces where province_country = " . $row["posaddress_country"] . " order by province_canton";

			$form->add_list("delivery_address_province_id", "Province*", $sql_provinces, SUBMIT, $row["place_province"]);


			$sql_places = "select place_id, place_name from places where place_province = " . $row["place_province"] . " order by place_name";

			$places = array();
			$places[999999999] = "Other city not listed below";
			$res = mysql_query($sql_places);
			while ($row = mysql_fetch_assoc($res))
			{
				$places[$row["place_id"]] = $row["place_name"];
			}

			$form->add_list("delivery_address_place_id", "City*", $places, SUBMIT, $row["posaddress_place_id"]);

			$form->add_edit("delivery_address_place", "City*", DISABLED, $row["posaddress_place"]);
		}

	}
	elseif (param("delivery_is_address") == 2)
	{
		$address = get_address($user["address"]);

		$sql_provinces = "select province_id, province_canton from provinces where province_country = " . $address["country"] . " order by province_canton";

		$form->add_list("delivery_address_province_id", "Province*", $sql_provinces, SUBMIT, $address["place_province"]);

		$sql_places = "select place_id, place_name from places where place_province = " . $address["place_province"] . " order by place_name";

		$places = array();
		$places[999999999] = "Other city not listed below";
		$res = mysql_query($sql_places);
		while ($address = mysql_fetch_assoc($res))
		{
			$places[$address["place_id"]] = $address["place_name"];
		}

		$form->add_list("delivery_address_place_id", "City*", $places, SUBMIT, $address["place_id"]);

		$form->add_edit("delivery_address_place", "City*", DISABLED, $address["place"]);
	}
	else
	{

		if(param("delivery_address_country")) {
			$sql_provinces = "select province_id, province_canton from provinces where province_country = " . param("delivery_address_country") . " order by province_canton";
		}
		elseif(isset($standard_delivery_address['delivery_address_country']) and $standard_delivery_address['delivery_address_country'] > 0) {
			$sql_provinces = "select province_id, province_canton from provinces where province_country = " . $standard_delivery_address['delivery_address_country'] . " order by province_canton";
		}
		else
		{
			$sql_provinces = "select province_id, province_canton from provinces where province_country = 0";
		}

		$form->add_list("delivery_address_province_id", "Province*", $sql_provinces, SUBMIT,
					isset($standard_delivery_address['delivery_address_province_id']) ?
					$standard_delivery_address['delivery_address_province_id'] :  "");


		if(param("delivery_address_province_id")) {
			$sql_places = "select place_id, place_name from places where place_province = " . param("delivery_address_province_id") . " order by place_name";
		}
		elseif(isset($standard_delivery_address['delivery_address_province_id']) and $standard_delivery_address['delivery_address_province_id'] > 0) {
			$sql_places = "select place_id, place_name from places where place_province = " . $standard_delivery_address['delivery_address_province_id'] . " order by place_name";
		}
		elseif(param("delivery_address_country")) {
			$sql_places = "select place_id, place_name from places where place_country = " . param("delivery_address_country") . " order by place_name";
		}
		elseif(isset($standard_delivery_address['delivery_address_country']) and $standard_delivery_address['delivery_address_country'] > 0) {
			$sql_places = "select place_id, place_name from places where place_country = " . $standard_delivery_address['delivery_address_country'] . " order by place_name";
		}
		else
		{
			$sql_places = "select place_id, place_name from places where place_country = 0";
		}

		$places = array();
		$places[999999999] = "Other city not listed below";
		$res = mysql_query($sql_places);
		while ($row = mysql_fetch_assoc($res))
		{
			$places[$row["place_id"]] = $row["place_name"];
		}


		$form->add_list("delivery_address_place_id", "City*", $places, SUBMIT,
					isset($standard_delivery_address['delivery_address_place_id']) ?
					$standard_delivery_address['delivery_address_place_id'] :  "");

		
		if(param("delivery_address_place_id") == 999999999)
		{
			$form->add_edit("delivery_address_place", "City*", NOTNULL, "", TYPE_CHAR, 20, 0, 1, "city");
		}
		else {
			//$form->add_edit("delivery_address_place", "City", DISABLED);

			$form->add_edit("delivery_address_place", "City*", DISABLED, 
						isset($standard_delivery_address['delivery_address_place'])?
						$standard_delivery_address['delivery_address_place'] : "");
		}
	}

	$form->add_edit("delivery_address_zip", "ZIP*", 0, 
                    isset($standard_delivery_address['delivery_address_zip'])?
                    $standard_delivery_address['delivery_address_zip'] : "", TYPE_CHAR, 20);

    

	
	/*
	$form->add_edit("delivery_address_place", "City*", 0, 
                    isset($standard_delivery_address['delivery_address_place'])? 
                    $standard_delivery_address['delivery_address_place'] : "", TYPE_CHAR, 20);
    */


    $form->add_edit("delivery_address_address", "Address*", 0,
                    isset($standard_delivery_address['delivery_address_address']) ? 
                    $standard_delivery_address['delivery_address_address']: "");

    $form->add_edit("delivery_address_address2", "",0,
                    isset($standard_delivery_address['delivery_address_address2'])? 
                    $standard_delivery_address['delivery_address_address2']: "");

    

   

    $form->add_edit("delivery_address_phone", "Phone*", 0, 
                    isset($standard_delivery_address['delivery_address_phone']) 
                    ? $standard_delivery_address['delivery_address_phone'] : "" );

    $form->add_edit("delivery_address_fax", "Fax",0, 
                    isset($standard_delivery_address['delivery_address_fax']) 
                    ? $standard_delivery_address['delivery_address_fax'] : "");

    $form->add_edit("delivery_address_email", "Email",0, 
                    isset($standard_delivery_address['delivery_address_email']) ? 
                    $standard_delivery_address['delivery_address_email']: "" );

    $form->add_edit("delivery_address_contact", "Contact*" , 0,
                    isset($standard_delivery_address['delivery_address_contact']) ?
                    $standard_delivery_address['delivery_address_contact'] : "");

   

	

    // Transportation Preferences

    $form->add_section("Transportation Preferences");
    $form->add_comment("Please enter the date in the form of dd.mm.yy");
    $form->add_edit("preferred_delivery_date", "Preferred Arrival Date*", NOTNULL,
                    "", TYPE_DATE, 20);

    $form->add_list("preferred_transportation_arranged", "Transportation arranged by*",
                    "select transportation_type_id, transportation_type_name " .
                    "from transportation_types where transportation_type_visible = 1 " .
		            " and transportation_type_code = 'arranged' " .
                    "order by transportation_type_name", NOTNULL,
                    "");

	$form->add_list("preferred_transportation_mode", "Transportation mode*",
                    "select transportation_type_id, transportation_type_name " .
                    "from transportation_types where transportation_type_visible = 1 " .
		            " and transportation_type_code = 'mode' " .
                    "order by transportation_type_name", NOTNULL,
                    "");

    /*
	$form->add_radiolist("packaging_retraction", "Packaging Retraction Desired",
                         array(0 => "no", 1 => "yes"), 0,
                         isset($order_data['packaging_retraction']) ? 
                         $order_data['packaging_retraction'] : 0);

    */

	$form->add_comment("Please indicate if there is a special approval needed for delivery into ".
                       "a pedestrian area."); 
    $form->add_radiolist("pedestrian_mall_approval", "Pedestrian Area Approval Needed",
                         array(0 => "no", 1 => "yes"), 0, 
                         isset($order_data['pedestrian_mall_approval']) ? 
                         $order_data['pedestrian_mall_approval'] : 0);

    $form->add_comment("Please indicate if partial delivery is possible or " .
                       "full delivery is required."); 
    $form->add_radiolist( "full_delivery", "Full Delivery", array(0 => "no", 1 => "yes"), 0, 
                        isset($order_data['full_delivery']) ? 
                         $order_data['full_delivery'] : 1);

    $form->add_comment("Please indicate any other circumstances/restrictions " .
                       "concerning delivery and traffic."); 
    $form->add_multiline("delivery_comments", "Delivery Comments", 4,0);

	$form->add_section("Insurance");
	$form->add_radiolist("order_insurance", array(1=>"Insurance by " . BRAND . "/Forwarder", 0=>""), array(1=>"covered",0=>"not covered"), VERTICAL,1);


    // Voltage selection(s)

    $form->add_section("Location Info");
    $form->add_list("voltage", "Voltage*",
                    "select voltage_id, voltage_name " .
                    "from voltages ", NOTNULL,
                    isset($order_data['voltage']) ? 
                    $order_data['voltage'] : 0);

    $form->add_section("General Comments");
    $form->add_multiline("comments", "Comments", 4,0);
    
    $form->add_button("order", "Proceed to Ordering");
    $form->add_button(LIST_BUTTON_BACK, "Back");
    $form->add_button("cancel_order", "Cancel Ordering");
}

/**
* fetches address-information from the database and fills it into the 
* form-fields
* 
* @param    int $order_address_id   id of address to fetch
* @param    string  $addres_type    identifies the type of address to fetch. 
*                                   possible values are 'billing_address', delivery_address'
*                                   and 'shop_address'
* @return
*
*/

function populate_address($order_address_id, $address_type)
{
    global $form;

    if($address_type == "billing_address")
    {
        $sql = "select order_billing_address_company, order_billing_address_company2, ".
               "   order_billing_address_address, order_billing_address_address2, order_billing_address_zip, ". 
               "   order_billing_address_place, order_billing_address_place_id, order_billing_address_country, order_billing_address_phone, ".
               "   order_billing_address_fax, order_billing_address_email, order_billing_address_contact " .
               "from orders " .
               "where order_id = " . $order_address_id;

        $res = mysql_query($sql) or dberror($sql);

        if ($row = mysql_fetch_assoc($res))
        {
            $keys = array_keys($row);
            foreach ($keys as $key)
            {
               $form->value(str_replace("order_billing_address" , $address_type, $key), "");
               $form->value(str_replace("order_billing_address" , $address_type, $key), $row[$key]);
            }
        }
    }
    elseif($address_type == "delivery_address")
    {
        
		$not_address_field = array("order_address_id", "order_address_order", 
                                   "order_address_order_item", "order_address_type",
                                   "user_created", "user_modified", "date_created",
                                   "date_modified" , "order_address_parent", "order_address_inactive");

        $sql = "select * " .
               "from order_addresses " .
               "where order_address_id = " . $order_address_id;
        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {
            if (!is_array($row))
            {
                continue;
            }

            $keys = array_keys($row);
            foreach ($keys as $key)
            {
                if (!in_array($key, $not_address_field))
                {
					$form->value(str_replace("order_address" , $address_type, $key), "");                    
                    $form->value(str_replace("order_address" , $address_type, $key), $row[$key]);
                }
            }
        }
    }
	elseif($address_type == "pos_address")
    {
		$sql = "select posaddress_name, posaddress_name2,posaddress_address, posaddress_address2, " . 
			   "posaddress_zip, posaddress_place_id, posaddress_place, posaddress_country, posaddress_phone, ". 
			   "posaddress_fax, posaddress_email, posaddress_contact_name, place_province " . 
			   "from posaddresses " . 
			   "left join places on place_id = posaddress_place_id " . 
			   "where posaddress_id = " . dbquote($form->value("posaddress_id"));

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
        {
			$form->value("shop_address_company", "" . $row["posaddress_name"]);
			$form->value("shop_address_company2", "" . $row["posaddress_name2"]);
			$form->value("shop_address_address", "" . $row["posaddress_address"]);
			$form->value("shop_address_address2", "" . $row["posaddress_address2"]);
			$form->value("shop_address_place", "" . $row["posaddress_place"]);
			$form->value("shop_address_place_id", $row["posaddress_place_id"]);
			$form->value("shop_address_country", $row["posaddress_country"]);
			$form->value("shop_address_province_id", $row["place_province"]);
			$form->value("shop_address_zip", "" . $row["posaddress_zip"]);
			$form->value("shop_address_phone", "" . $row["posaddress_phone"]);
			$form->value("shop_address_fax", "" . $row["posaddress_fax"]);
			$form->value("shop_address_email", "" . $row["posaddress_email"]);
			$form->value("shop_address_contact", "" . $row["posaddress_contact_name"]);
		}
		else
		{
			$form->value("shop_address_company", "" );
			$form->value("shop_address_company2", "" );
			$form->value("shop_address_address", "" );
			$form->value("shop_address_address2", "" );
			$form->value("shop_address_place", "" );
			$form->value("shop_address_place_id",0);
			$form->value("shop_address_country", 0);
			$form->value("shop_address_province_id", 0);
			$form->value("shop_address_zip", "");
			$form->value("shop_address_phone", "");
			$form->value("shop_address_fax", "");
			$form->value("shop_address_email", "");
			$form->value("shop_address_contact", "");
		}
	}
}


/**
* fetches address-information from the database and fills it into the 
* form-fields
* 
* @param    int $order_address_id   id of address to fetch
* @param    string  $addres_type    identifies the type of address to fetch. 
*                                   possible values are 'billing_address', delivery_address'
*                                   and 'shop_address'
* @return
*
*/

function populate_deliveryaddress($address_type, $address, $pos_address_id)
{
    global $form;

    if($address_type == 1) // POS address
    {
		$sql = "select posaddress_name, posaddress_name2,posaddress_address, posaddress_address2, " . 
			   "posaddress_zip, posaddress_place_id, posaddress_place, posaddress_country, posaddress_phone, ". 
			   "posaddress_fax, posaddress_email, posaddress_contact_name, place_province " . 
			   "from posaddresses " . 
			   "left join places on place_id = posaddress_place_id " . 
			   "where posaddress_id = " . dbquote($pos_address_id);

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
        {
			$form->value("delivery_address_company", "" . $row["posaddress_name"]);
			$form->value("delivery_address_company2", "" . $row["posaddress_name2"]);
			$form->value("delivery_address_address", "" . $row["posaddress_address"]);
			$form->value("delivery_address_address2", "" . $row["posaddress_address2"]);
			$form->value("delivery_address_place", "" . $row["posaddress_place"]);
			$form->value("delivery_address_place_id", $row["posaddress_place_id"]);
			$form->value("delivery_address_country", $row["posaddress_country"]);
			$form->value("delivery_address_province_id", $row["place_province"]);
			$form->value("delivery_address_zip", "" . $row["posaddress_zip"]);
			$form->value("delivery_address_phone", "" . $row["posaddress_phone"]);
			$form->value("delivery_address_fax", "" . $row["posaddress_fax"]);
			$form->value("delivery_address_email", "" . $row["posaddress_email"]);
			$form->value("delivery_address_contact", "" . $row["posaddress_contact_name"]);
		}
		else
		{
			$form->value("delivery_address_company", "");
			$form->value("delivery_address_company2", "");
			$form->value("delivery_address_address", "");
			$form->value("delivery_address_address2", "");
			$form->value("delivery_address_place", "");
			$form->value("delivery_address_place_id", 0);
			$form->value("delivery_address_zip", "");
			$form->value("delivery_address_province_id", 0);
			$form->value("delivery_address_country", "");
			$form->value("delivery_address_phone", "");
			$form->value("delivery_address_fax", "");
			$form->value("delivery_address_email", "");
			$form->value("delivery_address_contact", "");
		}

	}
    elseif($address_type == 2) // client address
    {
		$form->value("delivery_address_company", "" . $address["company"]);
		$form->value("delivery_address_company2", "" . $address["company2"]);
		$form->value("delivery_address_address", "" . $address["address"]);
		$form->value("delivery_address_address2", "" . $address["address2"]);
		$form->value("delivery_address_place", "" . $address["place"]);
		$form->value("delivery_address_country", $address["country"]);
		$form->value("delivery_address_province_id", $address["place_province"]);
		$form->value("delivery_address_place_id", $address["place_id"]);
		$form->value("delivery_address_zip", "" . $address["zip"]);
		$form->value("delivery_address_phone", "" . $address["phone"]);
		$form->value("delivery_address_fax", "" . $address["fax"]);
		$form->value("delivery_address_email", "" . $address["email"]);
		$form->value("delivery_address_contact", "" . $address["contact_name"]);
    }
	else // other
    {
		$form->value("delivery_address_company", "");
		$form->value("delivery_address_company2", "");
		$form->value("delivery_address_address", "");
		$form->value("delivery_address_address2", "");
		$form->value("delivery_address_place", "");
		$form->value("delivery_address_place_id", 0);
		$form->value("delivery_address_zip", "");
		$form->value("delivery_address_province_id", 0);
		$form->value("delivery_address_country", "");
		$form->value("delivery_address_phone", "");
		$form->value("delivery_address_fax", "");
		$form->value("delivery_address_email", "");
		$form->value("delivery_address_contact", "");
	}
	
}


/**
* function get_address_array
*
* assemble query for list-box to display all existing, already used
" addresses from the orders and address table
*
*@param int     $address_id of client-address to fetch
*@param string  $address_type indicates, which addresses should be
*                             fechted . Possible values are: billing_address, 
*                             delivery_address, shop_address, forwarder_address,
*                             warehouse_address
*@return    string  sql-query for add_list() - function
*/

function get_address_array($address_id, $address_type = BILLING_ADDRESS)
{
    $addresses = array();

    if($address_type == BILLING_ADDRESS)
    {
        $sql =  "select order_id, concat_ws(', ', order_billing_address_company, order_billing_address_address," .
                "    order_billing_address_contact) as full_name ".
                "from orders ".
                "where order_client_address = ". $address_id . " and order_billing_address_active = 1 ".
                "order by order_billing_address_company ";

        $res = mysql_query($sql) or dberror($sql);
        while ($row = mysql_fetch_assoc($res))
        {
            $addresses[$row["order_id"]]=$row["full_name"];
        }

    }
    else
    {
        $sql =  "select order_address_id, concat_ws(', ', order_address_company, order_address_address, " .
                "    order_address_contact) as full_name ".
                "from order_addresses ".
                "where order_address_inactive <> 1 " . 
			    " and order_address_type=" . $address_type . " and order_address_parent = ". $address_id . " ".
                "order by order_address_company ";
    
        $res = mysql_query($sql) or dberror($sql);
        while ($row = mysql_fetch_assoc($res))
        {
            $addresses[$row["order_address_id"]]=$row["full_name"];
        }
    }
    $addresses = array_unique($addresses);
    return $addresses;
}


/********************************************************************
    get the field values of an address
*********************************************************************/
function get_client_address($id)
{
    $address = array();

    
	$sql = "select * from addresses left join places on place_id = address_place_id where address_id = " . $id;
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$address["id"] = $row["address_id"];
		$address["shortcut"] = $row["address_shortcut"];
		$address["company"] = $row["address_company"];
		$address["company2"] = $row["address_company2"];
		$address["address"] = $row["address_address"];
		$address["address2"] = $row["address_address2"];
		$address["zip"] = $row["address_zip"];
		$address["place"] = $row["address_place"];
		$address["place_id"] = $row["address_place_id"];
		$address["place_province"] = $row["place_province"];
		$address["country"] = $row["address_country"];
		$address["country_name"] = "";
		$address["currency"] = $row["address_currency"];
		$address["phone"] = $row["address_phone"];
		$address["fax"] = $row["address_fax"];
		$address["email"] = $row["address_email"];
		$address["contact"] = $row["address_contact"];
		$address["client_type"] = $row["address_client_type"];
		$address["contact_name"] = $row["address_contact_name"];
		$address["website"] = $row["address_website"];
		$address["invoice_recipient"] = $row["address_invoice_recipient"];

		$sql = "select country_id, country_name, country_region ".
			   "from countries ".
			   "where country_id = " . dbquote($address["country"]);



		$res = mysql_query($sql);
		if ($res)
		{
			$row = mysql_fetch_assoc($res);
			$address["country_name"] = $row['country_name'];
			$address["country_region"] = $row['country_region'];
		}


		$sql = "select province_id, province_canton ".
			   "from provinces ".
			   "where province_id = " . dbquote($address["place_province"]);



		$res = mysql_query($sql);
		if ($res)
		{
			$row = mysql_fetch_assoc($res);
			$address["province_name"] = $row['province_canton'];
		}

	}
    return $address;
}

?>