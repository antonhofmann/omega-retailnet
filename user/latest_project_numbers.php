<?php
/********************************************************************

    latest_project_numbers.php

    Lists used project numbers.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2013-07-22
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2013-07-22
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_access_to_all_projects");



//get all countries
$countries = array();
$sql_countries = "select distinct address_id, country_name, address_company " . 
				 " from orders " .
				 " left join addresses on address_id = order_client_address " . 
				 " left join countries on country_id = address_country " .
				 " where order_type = 1 and country_name is not null " . 
				 " order by country_name";

/*
$orders = array();
$sql = "select distinct country_id, project_postype, " .
	   " max(order_id) as latest_id " . 
	   " from orders " .
	   " left join projects on project_order = order_id " . 
	   " left join addresses on address_id = order_client_address " . 
	   " left join countries on country_id = address_country " .
	   " where order_type = 1 and country_name is not null  and project_postype in (1, 2, 3) " .
	   " and order_number not like '%T%' and order_number  not like '%L%' and order_number  not like '%P%' " . 
	   " group by country_id, project_postype";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$orders[$row["latest_id"]] = $row["latest_id"];
}


$sql = "select distinct country_id, project_postype, " .
	   " max(order_id) as latest_id " . 
	   " from orders " .
	   " left join projects on project_order = order_id " . 
	   " left join addresses on address_id = order_client_address " . 
	   " left join countries on country_id = address_country " .
	   " where order_type = 1 and country_name is not null  and project_postype in (1, 2, 3) " .
	   " and order_number like '%T%' " . 
	   " group by country_id, project_postype";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$orders[$row["latest_id"]] = $row["latest_id"];
}

$sql = "select distinct country_id, project_postype, " .
	   " max(order_id) as latest_id " . 
	   " from orders " .
	   " left join projects on project_order = order_id " . 
	   " left join addresses on address_id = order_client_address " . 
	   " left join countries on country_id = address_country " .
	   " where order_type = 1 and country_name is not null  and project_postype in (1, 2, 3) " .
	   " and order_number like '%L%' " . 
	   " group by country_id, project_postype";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$orders[$row["latest_id"]] = $row["latest_id"];
}


$sql = "select distinct country_id, project_postype, " .
	   " max(order_id) as latest_id " . 
	   " from orders " .
	   " left join projects on project_order = order_id " . 
	   " left join addresses on address_id = order_client_address " . 
	   " left join countries on country_id = address_country " .
	   " where order_type = 1 and country_name is not null  and project_postype in (1, 2, 3) " .
	   " and order_number like '%P%' " . 
	   " group by country_id, project_postype";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$orders[$row["latest_id"]] = $row["latest_id"];
}


$order_filter = $filter = " and order_id IN (" . implode(',', $orders). ")";
*/

//get store_projects
$stores = array();
$sql = "select distinct order_client_address, order_number, " .
       " REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (order_number,'29.','209.'),'28.','208.'),'27.','207.'),'26.','206.'),'25.','205.'),'24.','204.'),'23.','203.'),'22.','202.') AS order_number2 " . 
       " from orders " .
	   " left join projects on project_order = order_id " . 
       " where order_number not like '%T%' and order_number  not like '%L%' and order_number  not like '%P%' " . 
	   " and project_postype = 1 " . 
	   " order by order_number2";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$stores[$row["order_client_address"]] = $row["order_number"];
}


//get sis projects
$sis = array();
$sql = "select distinct order_client_address, order_number, " .
       " REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (order_number,'29.','209.'),'28.','208.'),'27.','207.'),'26.','206.'),'25.','205.'),'24.','204.'),'23.','203.'),'22.','202.') AS order_number2 " . 
       " from orders " .
	   " left join projects on project_order = order_id " . 
       " where order_number not like '%T%' and order_number  not like '%L%' and order_number  not like '%P%' " . 
	   " and project_postype = 2" . 
	   " order by order_number2";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$sis[$row["order_client_address"]] = $row["order_number"];
}


//get kiosk projects
$kiosk = array();
$sql = "select distinct order_client_address, order_number, " .
        " REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (order_number,'29.','209.'),'28.','208.'),'27.','207.'),'26.','206.'),'25.','205.'),'24.','204.'),'23.','203.'),'22.','202.') AS order_number2 " . 
       " from orders " .
	   " left join projects on project_order = order_id " . 
       " where order_number not like '%T%' and order_number  not like '%L%' and order_number  not like '%P%' " . 
	   " and project_postype = 3" . 
	   " order by order_number2";
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$kiosk[$row["order_client_address"]] = $row["order_number"];
}


//get takeover projects
$takeover = array();
$sql = "select distinct order_client_address, order_number, " .
        " REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (order_number,'29.','209.'),'28.','208.'),'27.','207.'),'26.','206.'),'25.','205.'),'24.','204.'),'23.','203.'),'22.','202.') AS order_number2 " . 
       " from orders " .
	   " left join projects on project_order = order_id " . 
       " where order_number like '%T%' " . 
	   " order by order_number2";
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$takeover[$row["order_client_address"]] = $row["order_number"];
}

//get lease renewal projects
$lease_renewals = array();
$sql = "select distinct order_client_address, order_number, " .
        " REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (order_number,'29.','209.'),'28.','208.'),'27.','207.'),'26.','206.'),'25.','205.'),'24.','204.'),'23.','203.'),'22.','202.') AS order_number2 " . 
       " from orders " .
	   " left join projects on project_order = order_id " . 
       " where order_number like '%L%' " . 
	   " order by order_number2";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$lease_renewals[$row["order_client_address"]] = $row["order_number"];
}



//get popups projects
$popups = array();
$sql = "select distinct order_client_address, order_number, " .
        " REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (order_number,'29.','209.'),'28.','208.'),'27.','207.'),'26.','206.'),'25.','205.'),'24.','204.'),'23.','203.'),'22.','202.') AS order_number2 " . 
       " from orders " .
	   " left join projects on project_order = order_id " . 
       " where order_number like '%P%' " . 
	   " order by order_number2";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$popups[$row["order_client_address"]] = $row["order_number"];
}



$list = new ListView($sql_countries);
$list->set_entity("projects");
$list->add_column("country_name", "Country");
$list->add_column("address_company", "Company");
$list->add_text_column("stores", "Stores", 0, $stores);
$list->add_text_column("sis", "SIS", 0, $sis);
$list->add_text_column("kiosk", "Kiosk", 0, $kiosk);
$list->add_text_column("takeover", "Take Over", 0, $takeover);
$list->add_text_column("leaserenewal", "Lease Renewal", 0, $lease_renewals);
$list->add_text_column("popups", "PopUps", 0, $popups);






$page = new Page("projects");

if (has_access("has_access_to_projects_dashboard"))
{
	$page->register_action('dashboard', "Dashboard", "", "", "dashboard");
}

if(has_access("has_access_to_all_projects") or has_access("can_complete_cms") or has_access("can_approve_cms"))
{
	$page->register_action('home', 'Projects with completed CMS', "projects_cms_completed.php");
}

if (has_access("can_create_new_projects"))
{
    $page->register_action('new', 'New Project', "project_new_01.php");

}


//check if there are changes in project numbers
$sql_y = "select DISTINCT YEAR(oldproject_numbers.date_created) as year " . 
	   "from oldproject_numbers "  .
	   "left join projects on project_id = oldproject_number_project_id " . 
	   "left join orders on project_order = order_id ".
	   "inner join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
       "left join product_lines on project_product_line = product_line_id ".
       "left join postypes on postype_id = project_postype ".
	   "left join project_states on project_state_id = project_state ".
       "left join countries on order_shop_address_country = countries.country_id ".
       "left join users as rtcs on project_retail_coordinator = rtcs.user_id " .
	   "left join projectkinds on projectkind_id = project_projectkind ".
	   "left join users as rtos on order_retail_operator = rtos.user_id ";

	   " where " . $list_filter .
	   " order by year DESC";

$res = mysql_query($sql_y) or dberror($sql_y);
if ($row = mysql_fetch_assoc($res))
{
	$page->register_action('changedprojectnumbers', 'Info about Changed Project Numbers', "changed_projectnumbers.php");
}


if(has_access("has_access_to_all_projects"))
{
	$page->register_action('_project_numbers', 'Latest Project Numbers', "latest_project_numbers.php");
}

$page->header();
$page->title("Latest Project Numbers");

$list->render();

$page->footer();

?>
