<?php
/********************************************************************
    project_perform_action_confirmation.php

    confirm upon cancellation

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-06-18
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-06-18
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/order_state_constants.php";
require_once "include/save_functions.php";

check_access("can_use_taskcentre_in_projects");

/********************************************************************
    prepare data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

// append record to table actual_order_states
append_order_state($project["project_order"], param("action"), 1, 1);
set_archive_date($project["project_order"]);

if (param("action") == ORDER_CANCELLED)
{
    
    set_order_to_cancelled($project["project_order"]);

    
    // send email notification for project types
    $sql = "select project_number, project_postype, project_product_line, " .
           "order_shop_address_company, order_shop_address_place, country_name, " .
           "user_firstname, user_name, user_email " . 
		   "from projects " . 
           "left join orders on order_id = project_order " .
           "left join countries on country_id = order_shop_address_country " .
           "left join users on user_id =  " . user_id() .
           "   where project_id = " . $project["project_id"];

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $recipeints = array();
		$project_postype = $row["project_postype"];
		$project_product_line = $row["project_product_line"];
        
        $subject = MAIL_SUBJECT_PREFIX . ": Project was cancelled - Project " . $row["project_number"] . ", " . $row["country_name"] . ", " .  $row["order_shop_address_company"];

        $sender_email = $row["user_email"];
        $sender_name =  $row["user_name"] . " " . $row["user_firstname"];

		$sender_name2 =  $row["user_firstname"] . " " . $row["user_name"];

        $bodytext0 = "Dear all " . "\n\n";
		$bodytext0 .= "The project " .$row["project_number"] . " was cancelled.";

        $mailaddress = 0;

        $mail = new Mail();
        $mail->set_subject($subject);
        $mail->set_sender($sender_email, $sender_name);

        
		$uids = explode('-',$_GET['uids']);

		foreach($uids as $key=>$user_id)
		{
			$sql = "select user_id, user_name, user_firstname, user_email " . 
				   "from users " . 
				   "where user_id = " . dbquote($user_id);

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$mail->add_recipient($row["user_email"], $row["user_firstname"] . " " . $row["user_name"]);
				$recipeints[] = $user_id;
				$mailaddress = 1;
			}


		}


				
		/*
		if($project["project_product_line"] > 0) 
		{
			$sql = "select postype_notification_email2 from postype_notifications " .
				   "where postype_notification_postype = " . $project_postype . 
				   "  and postype_notification_prodcut_line = " . $project_product_line;
		}
		else
		{
			$sql = "select DISTINCT postype_notification_email2 from postype_notifications " .
				   "where postype_notification_postype = " . $project_postype;
		}

		$res = mysql_query($sql) or dberror($sql);
        while ($row = mysql_fetch_assoc($res))
        {
            if($row["postype_notification_email2"])
            {
                
                $sql_u = "select user_id from users " .
                         "where user_email = '" . $row["postype_notification_email2"] .  "'";
                $res_u = mysql_query($sql_u) or dberror($sql_u);
                if($row_u = mysql_fetch_assoc($res_u))
                {
                    $recepient_id = $row_u["user_id"];
                }
                else
                {
                    $recepient_id = '0';
                }

                $mail->add_recipient($row["postype_notification_email2"]);
				$recipeints[] = $row_u["user_id"];

                $mailaddress = 1;
            }
        }
		*/

       
        $link ="project_view_client_data.php?pid=" . $project["project_id"];
        $bodytext = $bodytext0 . "\nclick below to have direct access to the project:\n";
        $bodytext = $bodytext .  APPLICATION_URL . "/archive/" . $link . "\n\n";
		
		$bodytext .= "Kind regards \n\n" . $sender_name2;
        $mail->add_text($bodytext);
        
        if($mailaddress == 1)
        {
            $mail->send();
            
			foreach($recipeints as $key=>$value)
			{
				append_mail($project["project_order"], $value, user_id(), $bodytext, ORDER_CANCELLED, 1);
			}
        }
    }
}
elseif (param("action") == MOVED_TO_THE_RECORDS)
{
	// send email notification for project types
    $sql = "select project_number, project_postype, project_product_line, " .
           "order_shop_address_company, order_shop_address_place, country_name, " .
           "user_firstname, user_name, user_email from projects " . 
           "left join orders on order_id = project_order " .
           "left join countries on country_id = order_shop_address_country " .
           "left join users on user_id =  " . user_id() .
           "   where project_id = " . $project["project_id"];

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {        
        $recipeints = array();
        // send mail to cost manager
        $project_postype = $row["project_postype"];
		$project_product_line = $row["project_product_line"];
        
        $subject = MAIL_SUBJECT_PREFIX . ": Project was put to the records - Project " . $row["project_number"] . ", " . $row["country_name"] . ", " .  $row["order_shop_address_company"];

        $sender_email = $row["user_email"];
        $sender_name =  $row["user_name"] . " " . $row["user_firstname"];

        $bodytext0 = "The project was put to the records by by " . $sender_name;

        $mailaddress = 0;

        $mail = new Mail();
        $mail->set_subject($subject);
        $mail->set_sender($sender_email, $sender_name);

        $sql = "select * from postype_notifications " .
               "where postype_notification_postype = " . $project_postype . 
			   "  and postype_notification_prodcut_line = " . $project_product_line;

        $res = mysql_query($sql) or dberror($sql);
        while ($row = mysql_fetch_assoc($res))
        {
            if($row["postype_notification_email3"])
            {
                $sql_u = "select user_id from users " .
                         "where user_email = '" . $row["postype_notification_email3"] .  "'";
                
                $res_u = mysql_query($sql_u) or dberror($sql_u);
                
                if($row_u = mysql_fetch_assoc($res_u))
                {
                    $recepient_id = $row_u["user_id"];
                }
                else
                {
                    $recepient_id = '0';
                }

                $mail->add_recipient($row["postype_notification_email3"]);
				$recipeints[] = $row_u["user_id"];
                $mailaddress = 1;
            }
        }
        
        $link ="project_view_client_data.php?pid=" . $project["project_id"];
        $bodytext = $bodytext0 . "\nclick below to have direct access to the project:\n";
        $bodytext = $bodytext .  APPLICATION_URL . "/archive/" . $link . "\n\n";           
        $mail->add_text($bodytext);
        
        if($mailaddress == 1)
        {
            $mail->send();
            foreach($recipeints as $key=>$value)
			{
				append_mail($project["project_order"], $recepient_id, user_id(), $bodytext, MOVED_TO_THE_RECORDS, 1);
			}
            
        }
    }
}


/********************************************************************
    build form
*********************************************************************/

$form = new Form("projects", "project");
$form->add_hidden("pid", param("pid"));
$form->add_hidden("order_id", $project["project_order"]);


$form->add_label("project_number", "Project Number", 0, $project["order_number"]);
$form->add_lookup("product_line", "Product Line", "product_lines", "product_line_name", 0, $project["project_product_line"]);
$form->add_lookup("project_postype", "POS Type", "postypes", "postype_name", 0, $project["project_postype"]);


$line = "concat(user_name, ' ', user_firstname)";
if ($project["project_retail_coordinator"])
{
    $form->add_lookup("retail_coordinator", "Project Manager", "users", $line, 0, $project["project_retail_coordinator"]);
}
else
{
    $form->add_label("retail_coordinator", "Project Manager");
}
if ($project["order_retail_operator"])
{
    $form->add_lookup("retail_operator", "Retail Operator", "users", $line, 0, $project["order_retail_operator"]);
}
else
{
    $form->add_label("retail_operator", "Retail Operator");
}


$client = $client_address["company"] . ", " .
          $client_address["zip"] . " " . $client_address["place"] . ", " .
          $client_address["country"];


$form->add_label("client_address", "Client", 0, $client);


/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

$page->header();

if (param("action") == ORDER_CANCELLED)
{
    $page->title("The following project was cancelled:");
}
else if (param("action") == MOVED_TO_THE_RECORDS)
{
    $page->title("The following project was moved to the records:");
}

$form->render();
$page->footer();

?>