<?php
/********************************************************************

    project_task_center.php

    List Flow Hitory and Actions

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-11-23
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2007-07-17
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
//require_once "include/order_functions.php";

check_access("can_use_taskcentre_in_projects");

register_param("pid");
set_referer("project_perform_action.php");


/********************************************************************
    delete task data, when done was clicked
*********************************************************************/
if (param("done"))
{
    $sql = "delete from tasks where task_id = " .  $id;
    mysql_query($sql) or dberror($sql);
}



/********************************************************************
    prepare all data needed
*********************************************************************/
// read order details
$project = get_project(param("pid"));
$order_id = $project["project_order"];
$order = get_order($order_id);

$order_state_code = $project["order_actual_order_state_code"];
$last_order_state_code = get_code_of_last_order_state_performed($order_id);
$last_order_state = get_last_order_state_performed($order_id);

// get company's address
$client_address = get_address($project["order_client_address"]);



//get user roles
$user_roles = get_user_roles(user_id());
$user = get_user(user_id());
$user_name = $user["firstname"] . " " . $user["name"];

//get permissions for task center
$stepfilter = "";
$sql = "select permission_name ".
       "from user_roles ".
       "left join role_permissions on user_role_role = role_permission_role ".
       "left join permissions on role_permission_permission = permission_id ".
       "where user_role_user = " . user_id() .
       "    and left(permission_name, 31) ='can_perform_action_in_projects_' ".
       "order by permission_name";


$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
    
    $step = substr($row["permission_name"], 31,3);

    if(predecessor_is_performed($order_id, $step, 1, $project["project_no_planning"]))
    {    
        if($stepfilter)
        {
            $stepfilter.= " or order_state_code = " . $step;
        }
        else
        {
            $stepfilter.= "order_state_code = " . $step;
        }
    }
}


// count the number of tasks waiting
$sql_tasks = "select task_id, task_text, task_url, " .
             "task_order_state, order_state_code, " . 
             "DATE_FORMAT(task_due_date,'%d.%m.%y') as due_date, " .
             "    if(task_manually_deleted = 1, 'job done', '') as task_action, ".
             "  concat(user_name, ' ', user_firstname) as from_user " .
             "from tasks " .
             "left join order_states on order_state_id = task_order_state ".
             "left join users on user_id = task_from_user";

$num_of_tasks = 0;
$tasks_filter = "task_done_date is null and task_user=" . user_id() . " and task_order = " . $order_id;

$sql_tmp = "select count(task_id) as num_of_tasks ".
           "from tasks ".
           "where " . $tasks_filter;

$res = mysql_query($sql_tmp) or dberror($sql_tmp);
if ($row = mysql_fetch_assoc($res))
{
    $num_of_tasks = $row["num_of_tasks"];
}


if(in_array(1, $user_roles) or has_access("can_perform_all_steps")) // Administrator
{
	$sql_as = "select order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id ";

   
    $filter_as = "order_state_group_order_type = 1 " .
                 "and order_state_group_code > 1 ";    

    //history
    $sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name, " .
              "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
              "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
              "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y %H:%i') as performed " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id " . "left join actual_order_states on actual_order_state_order = " . $order_id . " and actual_order_state_state = order_state_id " .
              "left join users on users.user_id = actual_order_state_user " .
              "left join order_mails on order_mail_order = " . $order_id . " " .
              "and order_mail_order_state = order_state_id " .
              "left join users as tousers on tousers.user_id = order_mail_user " .
              "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

    $filter_hi = "order_state_group_order_type = 1 " .
                 "and users.user_name <> '' ";


}
elseif(in_array(3, $user_roles)) // retrail coordinatro
{
    $sql_as = "select order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id ";
    
    
	if($stepfilter)
	{
		$filter_as = "order_state_group_order_type = 1 " .
					 "and order_state_group_code > 1 " .
					 " and (" . $stepfilter . ")";
	}
	else
	{
		$filter_as = "order_state_group_order_type = 1 " .
					 "and order_state_group_code > 1 ";
	}
    
	//history
    $sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name, " .
              "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
              "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
              "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y %H:%i') as performed " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id " . "left join actual_order_states on actual_order_state_order = " . $order_id . " and actual_order_state_state = order_state_id " .
              "left join users on users.user_id = actual_order_state_user " .
              "left join order_mails on order_mail_order = " . $order_id . " " .
              "and order_mail_order_state = order_state_id " .
              "left join users as tousers on tousers.user_id = order_mail_user " .
              "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

    $filter_hi = "order_state_group_order_type = 1 " .
                 "and users.user_name <> '' ";

}
elseif(in_array(2, $user_roles) or in_array(10, $user_roles)) // retail operator
{
    $sql_as = "select order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id ";
    
    if($stepfilter)
	{
		$filter_as = "order_state_group_order_type = 1 " .
					 "and order_state_group_code > 1 " .
					 " and (" . $stepfilter . ")";
	}
	else
	{
		$filter_as = "order_state_group_order_type = 1 " .
					 "and order_state_group_code > 1 ";
	}
   
    //history
    $sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name, " .
              "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
              "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
              "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y %H:%i') as performed " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id " . "left join actual_order_states on actual_order_state_order = " . $order_id . " and actual_order_state_state = order_state_id " .
              "left join users on users.user_id = actual_order_state_user " .
              "left join order_mails on order_mail_order = " . $order_id . " " .
              "and order_mail_order_state = order_state_id " .
              "left join users as tousers on tousers.user_id = order_mail_user " .
              "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

    $filter_hi = "order_state_group_order_type = 1 " .
                 "and users.user_name <> '' ";

}

elseif(in_array(4, $user_roles)) // client
{
    
    $task_order_state = "001";

    $sql_tmp = $sql_tasks . " where " . $tasks_filter;

    $res = mysql_query($sql_tmp) or dberror($sql_tmp);
    if ($row = mysql_fetch_assoc($res))
    {
        $task_order_state = $row["task_order_state"];
    }
    
    $sql_as = "select order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name, " .
              "order_state_performer, order_state_predecessor " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id ";

    
    //$filter_as = "order_state_group_order_type = 1 " .
    //             "and order_state_performer = 4 " .
    //             "and order_state_group_code > 1 " .
    //             " and order_state_predecessor = " . $task_order_state;


    $filter_as = "order_state_group_order_type = 1 " .
		         "and order_state_performer = 4 " .
                 "and order_state_group_code > 1 " .
		         " and order_state_predecessor = " . $task_order_state;
                 " and order_state_code <= 800 ";    
    
    
	   


    $action_links = array();
    $sql= $sql_as . " where " . $filter_as;

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {

        if($project["project_state"] == 2)
		{
			$link = $row["order_state_action_name"];
		}
		else
		{
			if($row["order_state_performer"] == 4 and $row["order_state_predecessor"] == $task_order_state)
			{
				$link = "<a href='project_perform_action.php?id=" . $row["order_state_code"] . "&pid=" . param("pid") . "'>" . $row["order_state_action_name"] . "</a>";
			}
			else
			{
				$link = $row["order_state_action_name"];
			}
		}

        $action_links[$row["order_state_code"]] = $link;
    }

    //history
    $sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name, " .
              "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
              "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
              "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y %H:%i') as performed " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id " . "left join actual_order_states on actual_order_state_order = " . $order_id . " and actual_order_state_state = order_state_id " .
              "left join users on users.user_id = actual_order_state_user " .
              "left join order_mails on order_mail_order = " . $order_id . " " .
              "and order_mail_order_state = order_state_id " .
              "left join users as tousers on tousers.user_id = order_mail_user " .
              "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

    $filter_hi = "order_state_group_order_type = 1 " .
                 "and users.user_name <> '' " .
                 "and (order_state_performer = 4 " .
                 "or notification_recipient_role = 4) ";
}
elseif(in_array(5, $user_roles) or in_array(29, $user_roles)) // supplier or warehouse
{
    
    $task_order_state = "001";
    $task_order_state_code = "001";

    $sql_tmp = $sql_tasks . " where " . $tasks_filter;

    $res = mysql_query($sql_tmp) or dberror($sql_tmp);
    if ($row = mysql_fetch_assoc($res))
    {
        $task_order_state = $row["task_order_state"];
        $task_order_state_code = $row["order_state_code"];
    }
    
    $sql_as = "select order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id ";


    if($task_order_state_code == 550)
    {
        $filter_as = "order_state_group_order_type = 1 " .
                     "and order_state_performer = 5 " .
                     "and order_state_group_code > 1 " .
                     " and order_state_code = 540";
    }
    else
    {
        $filter_as = "order_state_group_order_type = 1 " .
                     "and order_state_performer = 5 " .
                     "and order_state_group_code > 1 " .
                     " and order_state_predecessor = " . $task_order_state;
    }
    

    //history
    $sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name, " .
              "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
              "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
              "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y %H:%i') as performed " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id " . "left join actual_order_states on actual_order_state_order = " . $order_id . " and actual_order_state_state = order_state_id " .
              "left join users on users.user_id = actual_order_state_user " .
              "left join order_mails on order_mail_order = " . $order_id . " " .
              "and order_mail_order_state = order_state_id " .
              "left join users as tousers on tousers.user_id = order_mail_user " .
              "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

    $filter_hi = "order_state_group_order_type = 1 " .
                 "and users.user_name <> '' " .
                 "and (tousers.user_id = " . user_id() .
                 "  or users.user_id = " . user_id() . ")";
}
elseif(in_array(6, $user_roles)) // forwarder
{
    $task_order_state = "001";

    $sql_tmp = $sql_tasks . " where " . $tasks_filter;

    $res = mysql_query($sql_tmp) or dberror($sql_tmp);
    if ($row = mysql_fetch_assoc($res))
    {
        $task_order_state = $row["task_order_state"];
    }
    
   
    $sql_as = "select order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id ";

    $filter_as = "order_state_group_order_type = 1 " .
                 "and order_state_performer = 6 " .
                 "and order_state_group_code > 1 " .
                 " and order_state_predecessor = " . $task_order_state;    


    //history
    $sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name, " .
              "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
              "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
              "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y %H:%i') as performed " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id " . "left join actual_order_states on actual_order_state_order = " . $order_id . " and actual_order_state_state = order_state_id " .
              "left join users on users.user_id = actual_order_state_user " .
              "left join order_mails on order_mail_order = " . $order_id . " " .
              "and order_mail_order_state = order_state_id " .
              "left join users as tousers on tousers.user_id = order_mail_user " .
              "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

    $filter_hi = "order_state_group_order_type = 1 " .
                 "and users.user_name <> '' " .
                 "and (tousers.user_id = " . user_id() .
                 "  or users.user_id = " . user_id() . ")";
}
elseif(in_array(7, $user_roles)) // design contractor
{
    
    $task_order_state = "001";
    $task_order_state_code = "001";


    $sql_tmp = $sql_tasks . " where " . $tasks_filter;

    $res = mysql_query($sql_tmp) or dberror($sql_tmp);
    if ($row = mysql_fetch_assoc($res))
    {
        $task_order_state = $row["task_order_state"];
        $task_order_state_code = $row["order_state_code"];
    }
    
    
    $sql_as = "select order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id ";

    if($order_state_code == 250 or $order_state_code == 270)
    {
        $filter_as = "order_state_group_order_type = 1 " .
                     "and order_state_performer = 7 " .
                     "and order_state_group_code > 1 " .
                     " and (order_state_code = 250 or order_state_code = 260)";
    }
    elseif($order_state_code == 420 or $order_state_code == 450)
    {
        $filter_as = "order_state_group_order_type = 1 " .
                     "and order_state_performer = 7 " .
                     "and order_state_group_code > 1 " .
                     " and (order_state_code = 430 or order_state_code = 440)";
    }
    else
    {
        $filter_as = "order_state_group_order_type = 1 " .
                     "and order_state_performer = 7 " .
                     "and order_state_group_code > 1 " .
                     " and order_state_predecessor = " . $task_order_state;
     
    }

    //history
    $sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name, " .
              "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
              "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
              "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y %H:%i') as performed " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id " . "left join actual_order_states on actual_order_state_order = " . $order_id . " and actual_order_state_state = order_state_id " .
              "left join users on users.user_id = actual_order_state_user " .
              "left join order_mails on order_mail_order = " . $order_id . " " .
              "and order_mail_order_state = order_state_id " .
              "left join users as tousers on tousers.user_id = order_mail_user " .
              "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

    $filter_hi = "order_state_group_order_type = 1 " .
                 "and users.user_name <> '' " .
                 "and (order_state_performer = 7 " .
                 "or notification_recipient_role = 7) ";
}
elseif(in_array(8, $user_roles)) // Design Supervisor
{
    
    $task_order_state = "001";

    $sql_tmp = $sql_tasks . " where " . $tasks_filter;

    $res = mysql_query($sql_tmp) or dberror($sql_tmp);
    if ($row = mysql_fetch_assoc($res))
    {
        $task_order_state = $row["task_order_state"];
    }
    
    $sql_as = "select order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id ";

    $filter_as = "order_state_group_order_type = 1 " .
                 "and order_state_performer = 8 " .
                 "and order_state_group_code > 1 " .
                 " and order_state_predecessor = " . $task_order_state;

    //history
    $sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name, " .
              "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
              "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
              "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y %H:%i') as performed " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id " . "left join actual_order_states on actual_order_state_order = " . $order_id . " and actual_order_state_state = order_state_id " .
              "left join users on users.user_id = actual_order_state_user " .
              "left join order_mails on order_mail_order = " . $order_id . " " .
              "and order_mail_order_state = order_state_id " .
              "left join users as tousers on tousers.user_id = order_mail_user " .
              "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

    $filter_hi = "order_state_group_order_type = 1 " .
                 "and users.user_name <> '' " .
                 "and (order_state_performer = 8 " .
                 "or notification_recipient_role = 8) ";
}
else // all others
{
    $sql_as = "select order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id ";
    
    if(!$stepfilter)
    {
        $filter_as = "order_state_group_order_type = 1 " .
                     "and order_state_group_code > 1 " . 
                     " and order_state_code = '000'";
    }
    else
    {
        $filter_as = "order_state_group_order_type = 1 " .
                     "and order_state_group_code > 1 " . 
                     " and (" . $stepfilter . ")";
    }

    //history
    $sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name, " .
              "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
              "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
              "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y %H:%i') as performed " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id " . "left join actual_order_states on actual_order_state_order = " . $order_id . " and actual_order_state_state = order_state_id " .
              "left join users on users.user_id = actual_order_state_user " .
              "left join order_mails on order_mail_order = " . $order_id . " " .
              "and order_mail_order_state = order_state_id " .
              "left join users as tousers on tousers.user_id = order_mail_user " .
              "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

    $filter_hi = "order_state_group_order_type = 1 " .
                 "and users.user_name <> '' ";

}


/********************************************************************
   get order's pending tasks
*********************************************************************/
if(in_array(1, $user_roles) or in_array(2, $user_roles)  or in_array(3, $user_roles) or in_array(10, $user_roles))
{
    $sql_open_tasks = "select task_id, order_state_code, order_state_name, " .
                      "CONCAT(users.user_name, ' ', users.user_firstname) as userto, " .
                      "CONCAT(users1.user_name, ' ', users1.user_firstname) as userfrom, " .
                      "DATE_FORMAT(tasks.date_created,'%d.%m.%y') as date_of_task " .
                      "from tasks " .
                      "left join users on users.user_id = task_user " .
                      "left join users as users1 on users1.user_id = task_from_user " .
                      "left join order_states on order_state_id = task_order_state ";
                
    $filter_open_tasks =  "task_done_date is null " .
                          "  and task_order = " . $order_id;
}


/********************************************************************
   get dates of performance ot each step
*********************************************************************/
$os = "";
$dc = 1;
$dates_performed = array();
$performed_by = array();


$sql = "select order_state_code," .
       "actual_order_states.date_created as state_date, " .
       "    concat(user_name, ' ', left(user_firstname,1), '.') as user_fullname " .
       "from actual_order_states " .
       "left join order_states on order_state_id = actual_order_state_state " .
       "left join users on actual_order_state_user = user_id ".
       "where actual_order_state_order = " . $order_id . 
       " order by order_state_code, actual_order_states.date_created ASC";


$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
    if($os != $row["order_state_code"])
    {
        $os = $row["order_state_code"];
        $dc = 1;
    }

    $dates_performed[$row["order_state_code"]] = "[" . $dc . "]" . to_system_date($row["state_date"]);
    $dc++;
    $performed_by[$row["order_state_code"]] = $row["user_fullname"];



	
}


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "task_center", 640);
$form->add_hidden("pid", param("pid"));

$form->add_section("Project");
if(!$project["project_retail_coordinator"])
{
    $form->error("No Project Managerhas been assigned!");
}
elseif(!$order["order_retail_operator"])
{
    if($project['project_projectkind'] != 4 and $project['project_projectkind'] != 5) // no take over, no lease renewal
	{
		$form->error("No Retail Operator has been assigned!");
	}
}


require_once "include/project_head_small.php";

if ($num_of_tasks == 0)
{
    $form->add_section("Pending Tasks for " . $user_name);
    $form->add_comment("Right now there are no pending tasks for this order in your task list.");
}

if(has_access("has_access_to_order_status_report_in_projects"))
{
    $popup_link = "javascript:popup('project_status_report.php?pid=" . param("pid"). "', 800,600)";
    $form->add_button("report", "Status Report", $popup_link);

    $popup_link = "javascript:popup('project_task_history.php?pid=" . param("pid"). "', 800,600)";
    $form->add_button("history", "Task History", $popup_link);
}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Create userspecific task list
*********************************************************************/ 
$tasks = new ListView($sql_tasks, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$tasks->set_title("Pending Tasks for " . $user_name);
$tasks->set_entity("tasks");
$tasks->set_order("due_date desc");

$tasks->set_filter($tasks_filter);

$link = "project_task_center.php?done=1&pid=" . param('pid'); 


$tasks->add_column("task_text", "Description");
$tasks->add_column("due_date", "Due Date", "", "","", COLUMN_NO_WRAP);
$tasks->add_column("from_user", "Submitted by", "", "","", COLUMN_NO_WRAP);


$link = "project_task_center.php?done=1&pid=" . param('pid'); 
$tasks->add_column("task_action", "", $link, "","", COLUMN_NO_WRAP);


/********************************************************************
    Create List of pending tasks for this order
*********************************************************************/ 
if(in_array(1, $user_roles) or in_array(3, $user_roles) or in_array(10, $user_roles))
{
    $order_open_tasks = new ListView($sql_open_tasks, LIST_HAS_HEADER);

    $order_open_tasks->set_title("Pending Tasks for this Project");
    $order_open_tasks->set_entity("task");
    $order_open_tasks->set_filter($filter_open_tasks);
    $order_open_tasks->set_order("userto");

    $order_open_tasks->add_column("userfrom", "Task from User");
    $order_open_tasks->add_column("userto", "Task for User");
    $order_open_tasks->add_column("order_state_name", "From Action");
    $order_open_tasks->add_column("order_state_code", "Status");
    $order_open_tasks->add_column("date_of_task", "Date");
    
    
}

//actual_step
$icons = array();

if($last_order_state_code < $project["order_actual_order_state_code"])
{
    $icons[$last_order_state_code] = "/pictures/wf_right_red.gif";
}
else
{
    $icons[$project["order_actual_order_state_code"]] = "/pictures/wf_right_green.gif";
}


//lease renewal and take over prjects
if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
{
	$filter_as .= ' and order_state_code >= "820" ';

}


//get links
$restricted_action_links = array();

if($project["project_state"] == 2)
{
	
	$sql = $sql_as . " where " . $filter_as;


	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		
		if($row["order_state_code"] == '900' and has_access("can_perform_action_in_projects_900"))
		{
			$restricted_action_links[$row["order_state_code"]] = '<a href="project_perform_action.php?id=' . $row["order_state_code"] . '&pid=' . param("pid") . '" >' . $row["order_state_action_name"] . '</a>';
		}
		elseif($row["order_state_code"] == '910' and has_access("can_perform_action_in_projects_910"))
		{
			$restricted_action_links[$row["order_state_code"]] = '<a href="project_perform_action.php?id=' . $row["order_state_code"] . '&pid=' . param("pid") . '" >' . $row["order_state_action_name"] . '</a>';
		}
		else
		{
			$restricted_action_links[$row["order_state_code"]] = $row["order_state_action_name"];
		}
	}
}

/********************************************************************
    Create List of actions
*********************************************************************/ 
$actions = new ListView($sql_as, LIST_HAS_HEADER);

$actions->set_title("Actions");
$actions->set_entity("order_state");
$actions->set_filter($filter_as);
$actions->set_order("order_state_code");
$actions->set_group("order_state_group_code", "order_state_group_name");

$actions->add_image_column("actual_step", "Latest", 0, $icons);
$actions->add_column("order_state_code", "Step");

if(in_array(4, $user_roles) and count($user_roles) == 1) //client
{
	if($project["project_state"] == 2) // on hold
	{
		$actions->add_text_column("order_state_action_name", "Action", COLUMN_UNDERSTAND_HTML, $restricted_action_links);
	}
	else
	{
		$actions->add_text_column("action", "Action", COLUMN_UNDERSTAND_HTML, $action_links);
	}
}
else
{
    if($project["project_state"] == 2) // on hold
	{
		$actions->add_text_column("order_state_action_name", "Action", COLUMN_UNDERSTAND_HTML, $restricted_action_links);
	}
	else
	{
		$actions->add_column("order_state_action_name", "Action", "project_perform_action.php?pid=" . param("pid"));
	}
}


if(!in_array(5, $user_roles) and !in_array(29, $user_roles) and !in_array(6, $user_roles))
{
    $actions->add_text_column("date", "Performed", COLUMN_NO_WRAP, $dates_performed);
    $actions->add_text_column("date", "Performed by", COLUMN_NO_WRAP, $performed_by);
    
}


/********************************************************************
    Create History
*********************************************************************/ 
$history = new ListView($sql_hi, LIST_HAS_HEADER);

$history->set_title("History");
$history->set_entity("order_state");
$history->set_filter($filter_hi);
$history->set_order("order_state_code, actual_order_states.date_created ASC");
$history->set_group("order_state_group_code", "order_state_group_name");

$history->add_column("order_state_code", "Step");
$history->add_column("order_state_action_name", "Action");

$history->add_column("fullname", "Performed by");
$history->add_column("performed", "Date");
$history->add_column("tofullname", "Recipient");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$tasks->populate();
$tasks->process();

if(in_array(1, $user_roles) or in_array(3, $user_roles) or in_array(10, $user_roles))
{
    $order_open_tasks->populate();
    $order_open_tasks->process();
}

$actions->populate();
$actions->process();

$history->populate();
$history->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Task Centre");
$form->render();

if($order["order_retail_operator"] or($project['project_projectkind'] == 4 or $project['project_projectkind'] == 5))
{
    $tasks->render();

    if(in_array(1, $user_roles) or in_array(3, $user_roles) or in_array(10, $user_roles))
    {
        $order_open_tasks->render();
		echo '<br />';
    }

    $actions->render();

    if(in_array(1, $user_roles) or in_array(2, $user_roles)  or in_array(3, $user_roles) or in_array(4, $user_roles) or in_array(10, $user_roles))
    {
        
    }
    else
    {
        $history->render();
    }
}

$page->footer();

?>