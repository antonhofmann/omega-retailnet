<?php
/********************************************************************

    dashboard_projects.php

    Shows the dashboard for projects.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-11-25
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-11-25
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../include/page_modal.php";
require_once "include/get_functions.php";
//require_once "include/order_functions.php";

check_access("can_view_projects");

/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());
$user_roles = get_user_roles(user_id());

$today = date("Y-m-d");


//ordered
$sql = "select distinct order_number, order_id, project_id, " . 
       "concat(users.user_name, ' ', users.user_firstname) as rto, " .
	   "concat(users1.user_name, ' ', users1.user_firstname) as rtc, " .
	   " '&nbsp;&nbsp;&nbsp;&nbsp;' as spacer " .
       "from order_items " . 
	   "left join orders on order_id = order_item_order " . 
	   "left join projects on project_order = order_id " . 
	   "left join users on user_id = order_retail_operator " . 
	   "left join users as users1 on users1.user_id = project_retail_coordinator ";

$list0_filter = "order_type = 1 " .
                " and order_actual_order_state_code >= '700' " .
				" and (order_archive_date is NULL or order_archive_date = '0000-00-00') " .
				" and (project_actual_opening_date is NULL or project_actual_opening_date = '0000-00-00') " .
				' and (order_item_ready_for_pickup = "0000-00-00" or order_item_ready_for_pickup is NULL) ' .
				' and (order_item_arrival = "0000-00-00" or order_item_arrival is NULL) ' .
                " and order_item_ordered  <= " . dbquote($today) . 
				" and (order_item_type = " . ITEM_TYPE_STANDARD . 
                "    or order_item_type = " . ITEM_TYPE_SPECIAL .")";

$ready_for_picked_up = array();
$not_ready_for_picked_up = array();
$infolinks = array();
$delays = array();
$res = mysql_query($sql . " where " . $list0_filter) or dberror($sql . " where " . $list0_filter);
while ($row = mysql_fetch_assoc($res))
{
	
	$sql_count = 'select count(order_item_id) as num_recs ' .
	             'from order_items ' . 
		         'where order_item_order = ' . $row["order_id"] . 
		         ' and order_item_ordered  <= ' . dbquote($today) .
		         ' and (order_item_ready_for_pickup = "0000-00-00" or order_item_ready_for_pickup is NULL) ' . 
				 ' and (order_item_type = ' . ITEM_TYPE_STANDARD . 
                 '    or order_item_type = ' . ITEM_TYPE_SPECIAL . ')';

	$res_c = mysql_query($sql_count) or dberror($sql_count);
	if ($row_c = mysql_fetch_assoc($res_c))
	{
		if($row_c['num_recs'] > 0) {
			$not_ready_for_picked_up[$row["order_number"]] = '<span class="red">no yet ready for pick up: ' . $row_c['num_recs'] . '</span>';
		}
		else
		{
			$not_ready_for_picked_up[$row["order_number"]] = '';
		}
	}

	$sql_count = 'select count(order_item_id) as num_recs from order_items ' . 
		         'where order_item_order = ' . $row["order_id"] . 
		         ' and order_item_ordered  <= ' . dbquote($today) .
		         ' and (order_item_ready_for_pickup <> "0000-00-00" and order_item_ready_for_pickup is NOT NULL) ' .
				 ' and (order_item_type = ' . ITEM_TYPE_STANDARD . 
                 '    or order_item_type = ' . ITEM_TYPE_SPECIAL . ')';

	$res_c = mysql_query($sql_count) or dberror($sql_count);
	if ($row_c = mysql_fetch_assoc($res_c))
	{
		
		if($row_c['num_recs'] > 0) {
			$ready_for_picked_up[$row["order_number"]] = '<span class="green">ready for pick up: ' . $row_c['num_recs'] . '</span>';
		}
		else
		{
			$ready_for_picked_up[$row["order_number"]] = '';
		}
		
	}

	
	$link = "<a href=\"/user/project_edit_supplier_data.php?pid=" .  $row["project_id"] . "\" target=\"_blank\"><img src=\"/pictures/info.gif\" border=0/></a>";
	$infolinks[$row["order_number"]] = $link;


	//calculate delays
	/*
	$first_order_date = '';
	$sql_count = 'select count(order_item_id) as num_recs, ' .
		         ' min(order_item_ready_for_pickup) as first_date ' . 
	             'from order_items ' . 
		         'where order_item_order = ' . $row["order_id"] . 
		         ' and order_item_ordered  <= ' . dbquote($today) .
		         ' and (order_item_ordered <> "0000-00-00" or order_item_ordered is not NULL) ' . 
				 ' and (order_item_type = ' . ITEM_TYPE_STANDARD . 
                 '    or order_item_type = ' . ITEM_TYPE_SPECIAL . ')';

	$res_c = mysql_query($sql_count) or dberror($sql_count);
	if ($row_c = mysql_fetch_assoc($res_c))
	{
		if($row_c['num_recs'] > 0) {
			$first_order_date = $row_c["first_date"];
			
			
		}
	}
	$delays[$row["order_number"]] = $first_order_date;
	*/

	
}

//$list0 = new ListView($sql, LIST_HAS_HEADER | LIST_IN_MODAL_WINDOW);
$list0 = new ListView($sql, LIST_IN_MODAL_WINDOW);
$list0->set_entity("catalog orders");
$list0->set_order("rto, order_number");
$list0->set_filter($list0_filter);
$list0->set_title("Ordered including today");


$list0->add_text_column("info", "", COLUMN_UNDERSTAND_HTML | COLUMN_NO_WRAP, $infolinks);
$list0->add_column("order_number", "Order Number", "", LIST_FILTER_NONE, "", COLUMN_NO_WRAP);
//$list0->add_text_column("delay", "delay", COLUMN_UNDERSTAND_HTML | COLUMN_NO_WRAP, $delays);
$list0->add_text_column("pickedup", "picked up", COLUMN_UNDERSTAND_HTML | COLUMN_NO_WRAP, $ready_for_picked_up);
$list0->add_text_column("notpickedup", "not picked up", COLUMN_UNDERSTAND_HTML | COLUMN_NO_WRAP, $not_ready_for_picked_up);
$list0->add_column("rtc", "Retail Coordinatro", "", LIST_FILTER_NONE, "", COLUMN_NO_WRAP);
$list0->add_column("rto", "Retail Operator", "", LIST_FILTER_NONE, "", COLUMN_NO_WRAP);
$list0->add_column("spacer", "", "", LIST_FILTER_NONE, "", COLUMN_UNDERSTAND_HTML | COLUMN_NO_WRAP);
$list0->add_button("print", "Print Dashboard", "javascript:popup('/user/dashboard_projects_pdf.php', 1024, 768);");

$list0->process();


//ready for pickup today
$list1_filter = "order_type = 1 " .
                "and order_item_ready_for_pickup  <= " . dbquote($today) . 
				" and (order_archive_date is NULL or order_archive_date = '0000-00-00') " .
				" and (project_actual_opening_date is NULL or project_actual_opening_date = '0000-00-00') " .
				 ' and (order_item_pickup = "0000-00-00" or order_item_pickup is NULL) ' .
				 ' and (order_item_arrival = "0000-00-00" or order_item_arrival is NULL) ' .
				" and (order_item_type = " . ITEM_TYPE_STANDARD . 
                "    or order_item_type = " . ITEM_TYPE_SPECIAL .")";


$picked_up = array();
$not_picked_up = array();
$infolinks = array();
$delays = array();
$res = mysql_query($sql . " where " . $list1_filter) or dberror($sql . " where " . $list1_filter);
while ($row = mysql_fetch_assoc($res))
{
	$sql_count = 'select count(order_item_id) as num_recs from order_items ' . 
		         'where order_item_order = ' . $row["order_id"] . 
		         ' and order_item_ready_for_pickup  <= ' . dbquote($today) .
		         ' and (order_item_pickup = "0000-00-00" or order_item_pickup is NULL) ' . 
				 ' and (order_item_type = ' . ITEM_TYPE_STANDARD . 
                 '    or order_item_type = ' . ITEM_TYPE_SPECIAL . ')';

	$res_c = mysql_query($sql_count) or dberror($sql_count);
	if ($row_c = mysql_fetch_assoc($res_c))
	{
		if($row_c['num_recs'] > 0) {
			$not_picked_up[$row["order_number"]] = '<span class="red">not yet picked up: ' . $row_c['num_recs'] . '</span>';
		}
		else
		{
			$not_picked_up[$row["order_number"]] = '';
		}
	}

	$sql_count = 'select count(order_item_id) as num_recs from order_items ' . 
		         'where order_item_order = ' . $row["order_id"] . 
		         ' and order_item_pickup  <= ' . dbquote($today) .
		         ' and (order_item_pickup <> "0000-00-00" and order_item_pickup is NOT NULL) ' . 
				 ' and (order_item_type = ' . ITEM_TYPE_STANDARD . 
                 '    or order_item_type = ' . ITEM_TYPE_SPECIAL . ')';

	$res_c = mysql_query($sql_count) or dberror($sql_count);
	if ($row_c = mysql_fetch_assoc($res_c))
	{
		
		if($row_c['num_recs'] > 0) {
			$picked_up[$row["order_number"]] = '<span class="green">picked up: ' . $row_c['num_recs'] . '</span>';
		}
		else
		{
			$picked_up[$row["order_number"]] = '';
		}
		
	}

	
	$link = "<a href=\"/user/project_view_traffic_data.php?pid=" .  $row["project_id"] . "\" target=\"_blank\"><img src=\"/pictures/info.gif\" border=0/></a>";
	$infolinks[$row["order_number"]] = $link;

	//calculate delays
	$delay = '';
	$sql_count = 'select count(order_item_id) as num_recs, ' .
		         ' min(order_item_ready_for_pickup) as first_date ' . 
	             'from order_items ' . 
		         'where order_item_order = ' . $row["order_id"] . 
				 ' and (order_item_type = ' . ITEM_TYPE_STANDARD . 
                 '    or order_item_type = ' . ITEM_TYPE_SPECIAL . ')';

	$res_c = mysql_query($sql_count) or dberror($sql_count);
	if ($row_c = mysql_fetch_assoc($res_c))
	{
		if($row_c['num_recs'] > 0) {
			$delay = floor((strtotime(date("Y-m-d")) - strtotime($row_c["first_date"])) / 86400);

			if($delay > 11)
			{
				$delay = '<span class="red">' . $delay . '</span>';
			}
		}
	}
	$delays[$row["order_number"]] = $delay;
}

//$list1 = new ListView($sql, LIST_HAS_HEADER | LIST_IN_MODAL_WINDOW);
$list1 = new ListView($sql, LIST_IN_MODAL_WINDOW);
$list1->set_entity("catalog orders");
$list1->set_order("rto, order_number");
$list1->set_filter($list1_filter);
$list1->set_title("Ready for pick up including today");


$list1->add_text_column("info", "", COLUMN_UNDERSTAND_HTML | COLUMN_NO_WRAP, $infolinks);
$list1->add_column("order_number", "Order Number", "", LIST_FILTER_NONE, "", COLUMN_NO_WRAP);
$list1->add_text_column("delay", "delay", COLUMN_UNDERSTAND_HTML | COLUMN_NO_WRAP, $delays);
$list1->add_text_column("pickedup", "picked up", COLUMN_UNDERSTAND_HTML | COLUMN_NO_WRAP, $picked_up);
$list1->add_text_column("notpickedup", "not picked up", COLUMN_UNDERSTAND_HTML | COLUMN_NO_WRAP, $not_picked_up);
$list1->add_column("rtc", "Retail Coordinatro", "", LIST_FILTER_NONE, "", COLUMN_NO_WRAP);
$list1->add_column("rto", "Retail Operator", "", LIST_FILTER_NONE, "", COLUMN_NO_WRAP);
$list1->add_column("spacer", "", "", LIST_FILTER_NONE, "", COLUMN_UNDERSTAND_HTML | COLUMN_NO_WRAP);

$list1->add_button("print", "Print Dashboard", "javascript:popup('/user/dashboard_projects_pdf.php', 1024, 768);");
$list1->process();


//expected arrival today
$list2_filter = "order_type = 1 " .
                "and order_item_expected_arrival  <= " . dbquote($today). 
				" and (order_archive_date is NULL or order_archive_date = '0000-00-00') " .
				" and (project_actual_opening_date is NULL or project_actual_opening_date = '0000-00-00') " .
				' and (order_item_arrival = "0000-00-00" or order_item_arrival is NULL) ' . 
				" and (order_item_type = " . ITEM_TYPE_STANDARD . 
                "    or order_item_type = " . ITEM_TYPE_SPECIAL .")";


$arrived = array();
$not_arrived = array();
$infolinks = array();
$res = mysql_query($sql . " where " . $list2_filter) or dberror($sql . " where " . $list2_filter);
while ($row = mysql_fetch_assoc($res))
{
	$sql_count = 'select count(order_item_id) as num_recs from order_items ' . 
		         'where order_item_order = ' . $row["order_id"] . 
		         ' and order_item_expected_arrival  <= ' . dbquote($today) .
		         ' and (order_item_arrival = "0000-00-00" or order_item_arrival is NULL) ' . 
				 ' and (order_item_type = ' . ITEM_TYPE_STANDARD . 
                 '    or order_item_type = ' . ITEM_TYPE_SPECIAL . ')';

	$res_c = mysql_query($sql_count) or dberror($sql_count);
	if ($row_c = mysql_fetch_assoc($res_c))
	{
		if($row_c['num_recs'] > 0) {
			$not_arrived[$row["order_number"]] = '<span class="red">not yet arrived: ' . $row_c['num_recs'] . '</span>';
		}
		else
		{
			$not_arrived[$row["order_number"]] = '';
		}
	}

	$sql_count = 'select count(order_item_id) as num_recs from order_items ' . 
		         'where order_item_order = ' . $row["order_id"] . 
		         ' and order_item_expected_arrival  <= ' . dbquote($today) .
		         ' and (order_item_arrival <> "0000-00-00" and order_item_arrival is NOT NULL) ' . 
				 ' and (order_item_type = ' . ITEM_TYPE_STANDARD . 
                 '    or order_item_type = ' . ITEM_TYPE_SPECIAL . ')';

	$res_c = mysql_query($sql_count) or dberror($sql_count);
	if ($row_c = mysql_fetch_assoc($res_c))
	{
		
		if($row_c['num_recs'] > 0) {
			$arrived[$row["order_number"]] = '<span class="green">arrived: ' . $row_c['num_recs'] . '</span>';
		}
		else
		{
			$arrived[$row["order_number"]] = '';
		}
		
	}

	$link = "<a href=\"/user/project_view_traffic_data.php?pid=" .  $row["project_id"] . "\" target=\"_blank\"><img src=\"/pictures/info.gif\" border=0/></a>";
	$infolinks[$row["order_number"]] = $link;
}

//$list2 = new ListView($sql, LIST_HAS_HEADER | LIST_IN_MODAL_WINDOW);
$list2 = new ListView($sql, LIST_IN_MODAL_WINDOW);
$list2->set_entity("catalog orders");
$list2->set_order("rto, order_number");
$list2->set_filter($list2_filter);
$list2->set_title("Expected to arrive including today");

$list2->add_text_column("info", "", COLUMN_UNDERSTAND_HTML | COLUMN_NO_WRAP, $infolinks);
$list2->add_column("order_number", "Order Number", "", LIST_FILTER_NONE, "", COLUMN_NO_WRAP);
$list2->add_text_column("arrived", "picked up", COLUMN_UNDERSTAND_HTML | COLUMN_NO_WRAP, $arrived);
$list2->add_text_column("notarrived", "not picked up", COLUMN_UNDERSTAND_HTML | COLUMN_NO_WRAP, $not_arrived);
$list2->add_column("rtc", "Retail Coordinator", "", LIST_FILTER_NONE, "", COLUMN_NO_WRAP);
$list2->add_column("rto", "Retail Operator", "", LIST_FILTER_NONE, "", COLUMN_NO_WRAP);
$list2->add_column("spacer", "", "", LIST_FILTER_NONE, "", COLUMN_UNDERSTAND_HTML | COLUMN_NO_WRAP);

$list2->add_button("print", "Print Dashboard", "javascript:popup('/user/dashboard_projects_pdf.php', 1024, 768);");

$list2->process();


$page = new Page_Modal("orders");
$page->header();
$page->title('Dashboard Projects');


$list0->render();
echo '<p>&nbsp;</p>';

$list1->render();
echo '<p>&nbsp;</p>';

$list2->render();
echo '<p>&nbsp;</p>';

$page->footer();

?>