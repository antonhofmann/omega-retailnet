<?php
/********************************************************************

    order_perform_action.php

    Perform an action selected from the action list
    in order_task_center.php

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-10-09
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-07-14
    Version:        1.0.3

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "include/order_functions.php";
require_once "include/order_state_constants.php";

check_access("can_use_taskcentre_in_orders");


/********************************************************************
    prepare data needed
*********************************************************************/


// read order details
$order = get_order(param("oid"));

// get company's address
$client_address = get_address($order["order_client_address"]);

// get user data of order's user
$client_user = get_user($order["order_user"]);
$logged_user = get_user(user_id());



if (id() == MOVED_TO_THE_RECORDS or id() == ORDER_CANCELLED)
{

    /********************************************************************
        build form
    *********************************************************************/
    $form = new Form("addresses", "adress");
    $form->add_hidden("oid", param("oid"));
    $form->add_hidden("order_id", $order["order_id"]);

    $form->add_label("order_number", "Order Number", 0, $order["order_number"]);
    $line = "concat(user_name, ' ', user_firstname)";

    if ($order["order_retail_operator"])
    {
        $form->add_lookup("retail_operator_name", "Retail Operator", "users", $line, 0, $order["order_retail_operator"]);
    }
    else
    {
        $form->add_label("retail_operator_name", "Retail Operator");
    }

    $client = $client_address["company"] . ", " .
              $client_address["zip"] . " " . $client_address["place"] . ", " .
              $client_address["country"];

    $form->add_label("client_address", "Client", 0, $client);


    if (id() == MOVED_TO_THE_RECORDS)
    {
        $form->add_comment("Please confirm that you want to put the order to the records!");
    }
    if (id() == ORDER_CANCELLED)
    {
        $form->add_comment("Please confirm the cancellation of the order!");
    }

    $form->add_button("confirm", "I confirm the action");
    $form->add_button("back", "Back");

    $form->populate();
    $form->process();

    if ($form->button("back"))
    {
        $link = "order_task_center.php?oid=" . param("oid") . "&id=" . param("oid");
        redirect($link);
    }
    if ($form->button("confirm"))
    {
        $link = "order_perform_action_confirm.php?oid=" . param("oid") . "&id=" . param("oid") . "&action=" . id();
        redirect($link);
    }

    /********************************************************************
        render page
    *********************************************************************/
    $page = new Page("orders");

    $page->header();
    
    if (id() == MOVED_TO_THE_RECORDS)
    {    
        $page->title("Move order to the records");
    }
    if (id() == ORDER_CANCELLED)
    {    
        $page->title("Cancel order");
    }

    $form->render();

    $page->footer();

}
else
{
    /********************************************************************
        prepare data needed
    *********************************************************************/
    // errors
    $error = array();
    $error[1] = "No mail was sent!<br />Please select a recipient at the bottom of the page.";
    $error[2] = "There are no recipients for this step! The cause might be one of the following:<br /> - you have not entered the P.O. Numbers (edit list of matrials)<br /> - you have already ordered all items<br /> - the supplier has no email address specified in the address data (contact administrator)";
    $error[3] = "There are no recipients for this step! The cause might be one of the following:<br /> - you have not assigned forwarders (edit list of materials)<br /> - you have made no order to a supplier (step 700)<br /> - the supplier has not entered an expected ready for pick up date<br /> - all items already have a pickup date<br /> - the forwarder has no email address specified in the address data (contact administrator)";
    $error[4] = "There are no recipients for this step! The cause might be one of the following:<br /> - there is no item requiering an offer<br /> - you have no entries in the list of materials<br /> - the suppliers have no email address specified in the address data (contact administrator)";
    $error[5] = "You can not confirm delivery because there are items in your order not having an arrival date.";
    $error[6] = "You can not submit a request for budget approval since not all items have been offered by the suppliers or not all offers made have been accepted!";
    $error[7] = "You can not submit an offer without having indicated in the prices.";
    $error[8] = "You can not confirm an order without having indicated the pick up data<br />or your might not be logged in as a valid supplier.";
    $error[9] = "You can not accept a request for delivery without having indicated <br />the expected date of arrival.";

    // get Action parameter
    $action_parameter = get_action_parameter(id(), 2);


    // get standard text
    $sting_name = "order_" .id();
    $message_text = get_string($sting_name);

    // create E-Mails Subject
    $subject = "Order " . $order["order_number"] . ": " . $action_parameter["name"];

    
    // get people involved in the project step
    $recepients = array();

    if ($action_parameter["recipient"] == 'Client')
    {
        $sql = "select user_id, address_id, ".
               "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
               "    user_email, user_email_cc, user_email_deputy  ".
               "from users ".
               "left join addresses on user_address = address_id ".
               "where user_email<>'' ".
               "    and user_active = 1 " .
               "    and user_id = " . $order["order_user"];

        $res = mysql_query($sql) or dberror($sql);

        if ($row = mysql_fetch_assoc($res))
        {
            $user_data = array();
            $user_data["id"] = $row["user_id"];
            $user_data["email"] = $row["user_email"];
            $user_data["cc"] = $row["user_email_cc"];
            $user_data["deputy"] = $row["user_email_deputy"];
            $user_data["full_name"] = $row["user_full_name"];
            $user_data["address"] = $row["address_id"];
            $recepients[] = $user_data;
        }
    }
    else if ($action_parameter["recipient"] == 'Supplier')
    {
         $user_data_key = 0;

         $where_clause = "";
         if (id() == SUBMIT_REQUEST_FOR_OFFER or id() == OFFER_REJECTED or id() == OFFER_ACCEPTED)
         {
            $where_clause = "    and (order_item_no_offer_required is null or order_item_no_offer_required = 0)";
         }

        $sql = "select distinct user_id, address_id, ".
               "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
               "    user_email, user_email_cc, user_email_deputy ".
               "from order_items ".
               "left join addresses on order_item_supplier_address = address_id ".
               "left join users on user_address = address_id ".
               "where user_email<>'' " . $where_clause .
               "    and (order_item_not_in_budget is null or order_item_not_in_budget =0)" .
               "    and user_active = 1 " .
               "    and order_item_order = " . $order["order_id"] . " " .
               " order by address_company";


        $res = mysql_query($sql) or dberror($sql);
        while ($row = mysql_fetch_assoc($res))
        {
            if (id() == ORDER_TO_SUPPLIER_SUBMITTED)
            {
                // check if there are items in the order not haveing been ordered already
                $sql_ordered = "select order_item_id, " .
                               "   order_item_po_number, order_item_ordered ".
                               "from order_items ".
                               "where order_item_type <= " . ITEM_TYPE_SPECIAL .
                               "    and order_item_supplier_address = ". $row["address_id"] .
                               "    and order_item_order = " . $order["order_id"] .
                               "    and (order_item_not_in_budget is null or order_item_not_in_budget =0)";

                $res1= mysql_query($sql_ordered) or dberror($sql_ordered);

                while ($row1 = mysql_fetch_assoc($res1))
                {
                    $already_ordered = 1;
                    $has_po_number = 0;

                    if ($row1["order_item_ordered"] == "0000-00-00"  or $row1["order_item_ordered"] == Null)
                    {
                      $already_ordered = 0;
                    }
           
                    if ($row1["order_item_po_number"])
                    {
                        $has_po_number = 1;
                    }
                    if ($already_ordered == 0 and $has_po_number == 1)
                    {
                        $user_data = array();
                        $user_data["id"] = $row["user_id"];
                        $user_data["email"] = $row["user_email"];
                        $user_data["cc"] = $row["user_email_cc"];
                        $user_data["deputy"] = $row["user_email_deputy"];
                        $user_data["full_name"] = $row["user_full_name"];
                        $user_data["address"] = $row["address_id"];
						$user_data["role"] = "Supplier";
                        if($user_data_key != $row["user_id"])
                        {
							$recepients[] = $user_data;
                            $user_data_key = $row["user_id"];
                        }
                    }
                }

            }
            else
            {
                $user_data = array();
                $user_data["id"] = $row["user_id"];
                $user_data["email"] = $row["user_email"];
                $user_data["cc"] = $row["user_email_cc"];
                $user_data["deputy"] = $row["user_email_deputy"];
                $user_data["full_name"] = $row["user_full_name"];
                $user_data["address"] = $row["address_id"];
				$user_data["role"] = "Supplier";
                if($user_data_key != $row["user_id"])
                {
                   
					$recepients[] = $user_data;
                    $user_data_key = $row["user_id"];
                }

            }

        }
    }
    else if ($action_parameter["recipient"] == 'Forwarder')
    {
        $user_data_key = 0;

        $sql = "select distinct user_id, address_id, ".
               "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
               "    user_email, user_email_cc, user_email_deputy ".
               "from order_items ".
               "left join addresses on order_item_forwarder_address = address_id ".
               "left join users on user_address = address_id ".
               "where user_email<>'' ".
               "    and user_active = 1 " .
               "    and order_item_order = " . $order["order_id"] . " " .
               "    and (order_item_not_in_budget is null or order_item_not_in_budget =0)" .
               " order by address_company";


        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {
            $ready_for_pick_up = 0;
            $pick_up_date_entered = 1;
            if (id() == REQUEST_FOR_DELIVERY_SUBMITTED)
            {
                // check if there are items in the order not haveing been ordered already
                $sql_ordered = "select order_item_id, " .
                               "   order_item_ready_for_pickup, " .
                               "   order_item_pickup " .
                               "from order_items ".
                               "where order_item_type <= " . ITEM_TYPE_SPECIAL .
                               "    and order_item_forwarder_address = ". $row["address_id"] .
                               "    and (order_item_not_in_budget is null or order_item_not_in_budget =0)" .
                               "    and order_item_order = " . $order["order_id"];


                $res1= mysql_query($sql_ordered) or dberror($sql_ordered);

                while ($row1 = mysql_fetch_assoc($res1))
                {
					$ready_for_pick_up = 0;
                    $pick_up_date_entered = 1;
      
                    if ($row1["order_item_ready_for_pickup"])
                    {
                        $ready_for_pick_up = 1;
                    }

                    
					if ($row1["order_item_pickup"] == "0000-00-00"   or $row1["order_item_pickup"] == Null)
                    {
                      $pick_up_date_entered = 0;
                    }

					//work around in case of item replcement in the list of materials
					if($order["order_actual_order_state_code"] == "720")
					{
						$pick_up_date_entered = 0;
					}
					//end of workaround

                    if ($ready_for_pick_up == 1 and $pick_up_date_entered == 0)
                    {

						$user_data = array();
                        $user_data["id"] = $row["user_id"];
                        $user_data["email"] = $row["user_email"];
                        $user_data["cc"] = $row["user_email_cc"];
                        $user_data["deputy"] = $row["user_email_deputy"];
                        $user_data["full_name"] = $row["user_full_name"];
                        $user_data["address"] = $row["address_id"];
						$user_data["role"] = "Forwarder";
                        if($user_data_key != $row["user_id"])
                        {
                            $recepients[] = $user_data;
                            $user_data_key = $row["user_id"];
                        }
                    }
                }

            }
            else
            {
                $user_data = array();
                $user_data["id"] = $row["user_id"];
                $user_data["email"] = $row["user_email"];
                $user_data["cc"] = $row["user_email_cc"];
                $user_data["deputy"] = $row["user_email_deputy"];
                $user_data["full_name"] = $row["user_full_name"];
                $user_data["address"] = $row["address_id"];
				$user_data["role"] = "Forwarder";
                if($user_data_key != $row["user_id"])
                {
                    $recepients[] = $user_data;
                    $user_data_key = $row["user_id"];
                }
            }
        } 
    }
    else if ($action_parameter["recipient"] == 'Retail Operator')
    {
        $user_data_key = 0;

        $sql = "select user_id, address_id, ".
               "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
               "    user_email, user_email_cc, user_email_deputy ".
               "from users ".
               "left join addresses on user_address = address_id ".
               "where user_email<>'' ".
               "    and user_active = 1 " .
               "    and user_id = " . $order["order_retail_operator"];

        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {
            if (id() == DELIVERY_CONFIRMED)
            {
                // check if there are items in the order not having an actual arrival date
                $sql_ordered = "select order_item_id, order_item_po_number ".
                               "from order_items ".
                               "where order_item_type <= " . ITEM_TYPE_SPECIAL .
                               "    and order_item_order = " . $order["order_id"] .
                               "    and (order_item_not_in_budget is null or order_item_not_in_budget =0)";

                $res1= mysql_query($sql_ordered) or dberror($sql_ordered);

                $already_arrived = 1;
                while ($row1 = mysql_fetch_assoc($res1))
                {

                    $sql_dates = "select date_id ".
                                 "from dates ".
                                 "left join date_types on date_type = date_type_id ".
                                 "where date_order_item = ". $row1["order_item_id"] .
                                 "    and date_type_code = 'ACAR'";

                    $res2= mysql_query($sql_dates) or dberror($sql_dates);
  
                    if ($row2 = mysql_fetch_assoc($res2))
                    {
                      // nothing
                    }
                    else
                    {
                      $already_arrived = 0;
                    }
                }

                if ($already_arrived == 1)
                {
                    $user_data = array();
                    $user_data["id"] = $row["user_id"];
                    $user_data["email"] = $row["user_email"];
                    $user_data["cc"] = $row["user_email_cc"];
                    $user_data["deputy"] = $row["user_email_deputy"];
                    $user_data["full_name"] = $row["user_full_name"];
                    $user_data["address"] = $row["address_id"];
                    if($user_data_key != $row["user_id"])
                    {
                        $recepients[] = $user_data;
                        $user_data_key = $row["user_id"];
                    }
                }
            }        
            else
            {
                $user_data = array();
                $user_data["id"] = $row["user_id"];
                $user_data["email"] = $row["user_email"];
                $user_data["cc"] = $row["user_email_cc"];
                $user_data["deputy"] = $row["user_email_deputy"];
                $user_data["full_name"] = $row["user_full_name"];
                $user_data["address"] = $row["address_id"];
                if($user_data_key != $row["user_id"])
                {
                    $recepients[] = $user_data;
                    $user_data_key = $row["user_id"];
                }
            }
        }
    }
    else // all addresses in the project
    {
        // client & retail operator
        $user_data_key = 0;
        $sql = "select user_id, address_id, ".
               "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
               "    user_email, user_email_cc, user_email_deputy ".
               "from users ".
               "left join addresses on user_address = address_id ".
               "where user_email<>'' ".
               "    and user_active = 1 " .
               "    and (user_id = " . $order["order_user"] .
               "    or user_id = " . $order["order_retail_operator"] . ")";

        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {
            $user_data = array();
            $user_data["id"] = $row["user_id"];
            $user_data["email"] = $row["user_email"];
            $user_data["cc"] = $row["user_email_cc"];
            $user_data["deputy"] = $row["user_email_deputy"];
            $user_data["full_name"] = $row["user_full_name"];
            $user_data["address"] = $row["address_id"];
            if($user_data_key != $row["user_id"])
            {
                $recepients[] = $user_data;
                $user_data_key = $row["user_id"];
            }
        }

        // supplier
        $user_data_key = 0;
        $sql = "select distinct user_id, address_id, ".
               "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
               "    user_email, user_email_cc, user_email_deputy ".
               "from order_items ".
               "left join addresses on order_item_supplier_address = address_id ".
               "left join users on user_address = address_id ".
               "where user_email<>'' ".
               "    and (order_item_not_in_budget is null or order_item_not_in_budget =0)" .
               "    and user_active = 1 " .
               "    and order_item_order = " . $order["order_id"] . " " .
               " order by address_company";

        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {
            $user_data = array();
            $user_data["id"] = $row["user_id"];
            $user_data["email"] = $row["user_email"];
            $user_data["cc"] = $row["user_email_cc"];
            $user_data["deputy"] = $row["user_email_deputy"];
            $user_data["full_name"] = $row["user_full_name"];
            $user_data["address"] = $row["address_id"];
			$user_data["role"] = "Supplier";
            if($user_data_key != $row["user_id"])
            {
                $recepients[] = $user_data;
                $user_data_key = $row["user_id"];
            }
        }

        // forwarder
        $user_data_key = 0;
        $sql = "select distinct user_id, address_id, ".
               "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
               "    user_email, user_email_cc, user_email_deputy ".
               "from order_items ".
               "left join addresses on order_item_forwarder_address = address_id ".
               "left join users on user_address = address_id ".
               "where user_email<>'' ".
               "    and (order_item_not_in_budget is null or order_item_not_in_budget =0)" .
               "    and user_active = 1 " .
               "    and order_item_order = " . $order["order_id"] . " " .
               " order by address_company";

        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {
            $user_data = array();
            $user_data["id"] = $row["user_id"];
            $user_data["email"] = $row["user_email"];
            $user_data["cc"] = $row["user_email_cc"];
            $user_data["deputy"] = $row["user_email_deputy"];
            $user_data["full_name"] = $row["user_full_name"];
            $user_data["address"] = $row["address_id"];
			$user_data["role"] = "Forwarder";
            if($user_data_key != $row["user_id"])
            {
                $recepients[] = $user_data;
                $user_data_key = $row["user_id"];
            }
        }


		// special users
        $user_data_key = 0;
        $sql = "select distinct user_id, address_id, ".
               "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
               "    user_email, user_email_cc, user_email_deputy ".
               "from orders ".
               "left join addresses on order_client_address = address_id ".
               "left join users on user_address = address_id ".
               "where user_email<>'' ".
               "    and user_active = 1 and user_order_reciepient = 1 " .
			   " and user_address = address_id " . 
			    " and order_id = " . param("oid") .
               " order by address_company";

        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {
            $user_data = array();
            $user_data["id"] = $row["user_id"];
            $user_data["email"] = $row["user_email"];
            $user_data["cc"] = $row["user_email_cc"];
            $user_data["deputy"] = $row["user_email_deputy"];
            $user_data["full_name"] = $row["user_full_name"];
            $user_data["address"] = $row["address_id"];
			$user_data["role"] = "Additional Recipient";
            if($user_data_key != $row["user_id"])
            {
                $recepients[] = $user_data;
                $user_data_key = $row["user_id"];
            }
        }
    }
	

    // lock budget
    if (id() == ORDER_CONFIRMED or id() == BUDGET_APPROVED or id() == DELIVERY_CONFIRMED or id() == MOVED_TO_THE_RECORDS or id() == ORDER_CANCELLED)

    {
        update_budget_state(param("oid"), 1);
    }

    // check if all budget positions have a valid price
    
    if (id() == OFFER_SUBMITTED)
    {
        $user_data = get_user(user_id());
        $all_offered = check_if_all_items_have_prices(param("oid"), $user_data["address"]);
        if ($all_offered == 0)
        {
            $recepients = array();
        }
    }

    // check if all budget positions have been accepted and have a valid price
    
    if (id() == REQUEST_FOR_BUDGET_APPROVAL_SUBMITTED)
    {
        $all_offered = check_if_all_items_are_offered(param("oid"));
        if ($all_offered == 0)
        {
            $recepients = array();
        }
    }


    // check if all budget positions have have a pick up date
    
    if (id() == CONFIRM_ORDER_BY_SUPPLIER)
    {
        $pickupdates_entered = check_if_all_items_hav_pick_up_date(param("oid"), $logged_user["address"]);
        if ($pickupdates_entered == 0)
        {
            $recepients = array();
        }
    }

    // check if all budget positions have have an arrival date
    if (id() == DELIVERY_CONFIRMED_FRW)
    {
        $arrival_entered = check_if_all_items_hav_arrival_date(param("oid"), $logged_user["address"]);
        if ($arrival_entered == 0)
        {
            $recepients = array();
        }
    }

    // check if all positions have an expected date of arrival
    if (id() == REQUEST_FOR_DELIVERY_ACCEPTED)
    {
        $expected_arrival_entered = check_if_all_items_hav_expected_arrival_date(param("oid"), $logged_user["address"]);
        if ($expected_arrival_entered == 0)
        {
            $recepients = array();
        }
    }

    /********************************************************************
        build form
    *********************************************************************/
    $form = new Form("addresses", "adress");
    $form->add_hidden("oid", param("oid"));
    $form->add_hidden("order_id", $order["order_id"]);

    require_once "include/order_head_small.php";


    $form->add_section("Email and Task Entry");
    $form->add_label("action_name", "Action to be Performed", 0, $action_parameter["action_name"]);
    $form->add_label("sender_name", "Sender", 0, $logged_user["contact"]);
    $form->add_hidden("sender_email",  $logged_user["email"]);

    $form->add_label("dummy1", "", 0, "");

    $form->add_edit("subject", "Subject*", NOTNULL, $subject, TYPE_CHAR);
    $form->add_multiline("body_text", "Mail Message*", 6, NOTNULL, $message_text);
    $form->add_edit("due_date", "Due Date*", 0, "", TYPE_DATE, 20);


    // create recipient section depending on the action selected
    $form->add_comment("Select the recipients of your message from the following list.");

    foreach ($recepients as $key=>$value_array)
    {
        
		//get user_role
		$user_role = "";
		if($value_array["id"] == $order["order_retail_operator"]) {$user_role = "Retail Coordinator";}
		elseif($value_array["id"] == $order["order_user"]) {$user_role = "Client";}
		elseif(array_key_exists("role", $user_data)){$user_role = $value_array["role"];}
		
		$form->add_checkbox("R" . $value_array["id"], $value_array["full_name"], "", 0, $user_role);
    }


    if (count($recepients)>0)
    {
        $form->add_button("send", "Send");
    }
    else
    {
        if (id() == ORDER_TO_SUPPLIER_SUBMITTED)
        {
            $form->error($error[2]);
        }
        else if (id() == REQUEST_FOR_DELIVERY_SUBMITTED)
        {
            $form->error($error[3]);
        }
        else if (id() == SUBMIT_REQUEST_FOR_OFFER)
        {
            $form->error($error[4]);
        }
        else if (id() == DELIVERY_CONFIRMED_FRW)
        {
            $form->error($error[5]);
        }
        else if (id() == REQUEST_FOR_BUDGET_APPROVAL_SUBMITTED)
        {
            $form->error($error[6]);
        }
        else if (id() == OFFER_SUBMITTED)
        {
            $form->error($error[7]);
        }
        else if (id() == CONFIRM_ORDER_BY_SUPPLIER)
        {
            $form->error($error[8]);
        }
        else if (id() == REQUEST_FOR_DELIVERY_ACCEPTED)
        {
            $form->error($error[9]);
        }
    }

    if (count($recepients)>0)
    {
        $show_copy_to = 1;
        foreach ($recepients as $key=>$value_array)
        {
            if($value_array["email"] == $logged_user["email"])
            {
                $show_copy_to = 0;
            }
        }
        if($show_copy_to == 1)
        {
            $copy_to_name = $logged_user["firstname"] . " " . $logged_user["name"] . " " . $logged_user["email"];
        
            $form->add_comment("Send a Copy of the Email to the Following Recepient");
            $form->add_checkbox("copy_to", $copy_to_name);
        }

    }

    $form->add_button(FORM_BUTTON_BACK, "Back");


    /********************************************************************
        Populate form and process button clicks
    *********************************************************************/ 
    $form->populate();
    $form->process();


    if ($form->button("send"))
    {
        $num_mails = 0;
        $num_tasks = 0;

        if ($form->validate())
        {
			// send email
            $num_mails = 0;
            if (count($recepients) > 0)
            {

				foreach ($recepients as $key=>$value_array)
                {
                    if ($form->value("R" . $value_array["id"]) == 1)
                    {
                        
                        $mail = new Mail();
                        $mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $form->value("subject"));
                        $mail->set_sender($form->value("sender_email"), $form->value("sender_name"));

                        $bodytext0 = str_replace("\r\n", "\n", $form->value("body_text")) . "\n\n";
                        $link ="order_task_center.php?oid=" . $order["order_id"];
                        $bodytext = $bodytext0 . "click below to have direct access to the order:\n";
                        $bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
                        $mail->add_text($bodytext);

                        $mail->add_recipient($value_array["email"]);

                        if($value_array["cc"])
                        {
                            $mail->add_cc($value_array["cc"]);
                        }
                        if($value_array["deputy"])
                        {
                            $mail->add_cc($value_array["deputy"]);
                        }
                        

                        $num_mails++;

                        // update dates (date of order)
                        if (id() == ORDER_TO_SUPPLIER_SUBMITTED)
                        {
                            append_order_dates($order["order_id"], $value_array["address"]);
                        }
                        else if (id() == REQUEST_FOR_DELIVERY_SUBMITTED)
                        {
                            append_request_for_delivery_dates($order["order_id"], $value_array["address"]);
                        }
                        $mail->send();

                    }
                }
            }


            if($show_copy_to == 1)
            {
                if($form->value("copy_to") and $show_copy_to == 1)
                {
					$mail = new Mail();
                    $mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $form->value("subject"));
                    $mail->set_sender($form->value("sender_email"), $form->value("sender_name"));

                    $bodytext0 = str_replace("\r\n", "\n", $form->value("body_text")) . "\n\n";
                    $link ="order_task_center.php?oid=" . $order["order_id"];
                    $bodytext = $bodytext0 . "click below to have direct access to the order:\n";
                    $bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
                    $mail->add_text($bodytext);

                    $mail->add_recipient($logged_user["email"]);
                    $mail->send();
                }
            }
        
            // append task entry
            $num_tasks = 0;
            if (count($recepients) > 0)
            {
				if ($form->value("body_text"))
                {
                    if ($form->value("due_date"))
                    {
                        $date = from_system_date($form->value("due_date"));
                    }
                    else
                    {
                        $date = date("Y-m-d");
                    }
                    $link ="order_task_center.php?oid=" . $order["order_id"];
                    foreach ($recepients as $key=>$value_array)
                    {
                        if ($form->value("R" . $value_array["id"]) == 1)
                        {
                           delete_user_tasks($order["order_id"], $value_array["id"], $form->value("body_text"), $link, $date, user_id(), id(), 2);
                           if ($action_parameter["append_task"] == 1)
                           {
                               if($value_array["role"] != "Additional Recipient")
							   {
									append_task($order["order_id"], $value_array["id"], $form->value("body_text"), $link, $date, user_id(), id(), 2);
							   }
                           }
                           $num_tasks++;
                           if ($action_parameter["send_email"] == 1)
                           {
                                append_mail($order["order_id"], $value_array["id"], user_id(), $bodytext0, id(), 2);
                           }
                        }
                    }
                }
            }

            if ($num_mails > 0 or $num_tasks > 0)
            {
                
                // set task for supplier
                if (id() == REQUEST_FOR_OFFER_ACCEPTED)
                {
                    append_task($order["order_id"], user_id(), "Please submit your offer!", $link, $date, user_id(), id(), 2);
                }
                
                // set task for forwarder
                if (id() == REQUEST_FOR_DELIVERY_ACCEPTED)
                {
                    append_task($order["order_id"], user_id(), "Please confirm delivery on arrival!", $link, $date, user_id(), id(), 2);
                }

                // append record to table actual_order_states
                append_order_state($order["order_id"], id(), 2, 1);

                if (id() == DELIVERY_CONFIRMED_FRW)
                {
                    $arrival_entered = check_if_all_items_hav_arrival_date(param("oid"), "");
                    if ($arrival_entered == 1)
                    {
                        // get mailtext and email address of client
                        $text = "";

                        $sql = "select string_text ".
                               "from strings ".
                               "where string_name = 'general_750'";

                        $res = mysql_query($sql) or dberror($sql);
    
                        if ($row = mysql_fetch_assoc($res))
                        {
                            $text = $row["string_text"];
                        }

                        //send email to client
                        $mail = new Mail();
                        $mail->set_subject(MAIL_SUBJECT_PREFIX . ": Delivery confirmation");
                        $mail->set_sender($form->value("sender_email"), "retail net");

                        $bodytext0 = $text . "\n\n";
                        $link ="order_task_center.php?oid=" . $order["order_id"];
                        $bodytext = $bodytext0 . "click below to have direct access to the order:\n";
                        $bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
                        $mail->add_text($bodytext);

                        $mail->add_recipient($client_user["email"]);
                        if($client_user["cc"])
                        {
                            $mail->add_cc($client_user["cc"]);
                        }
                        if($client_user["deputy"])
                        {
                            $mail->add_cc($client_user["deputy"]);
                        }
                        
                        $mail->send();


                        append_mail($order["order_id"], $order["order_user"], user_id(), $bodytext0, id(), 2);
                        append_task($order["order_id"], $order["order_user"], $text, $link, $date, user_id(), id(), 2);

                    }
                }
                elseif (id() == DELIVERY_CONFIRMED)
                {
                    set_archive_date($order["order_id"]);
                }

                $params = "?oid=" . param("oid") . "&num_mails=" . $num_mails . "&num_tasks=" . $num_tasks;
                $link = "order_send_message_confirm.php" . $params;
                $link = "order_task_center.php?oid=" . param("oid");
                redirect ($link);
            }

            if ($num_mails == 0 and $num_tasks == 0)
            {
                $form->error($error[1]);
            }
        }
    }

  
    /********************************************************************
        render page
    *********************************************************************/
    $page = new Page("orders");

    require "include/order_page_actions.php";

    $page->header();
    $page->title($action_parameter["action_name"]);
    $form->render();

    $page->footer();
}

?>