<?php
/********************************************************************

    project_view_comments.php

    List of comments made

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-25
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-02-16
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_view_comments_in_projects");

register_param("pid");
set_referer("project_edit_comment.php");
set_referer("project_add_comment.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

// get user data
$user_data = get_user(user_id());

//get retail operator
// build sql for comment entries
$sql_comments = "select distinct ".
                "    comment_id, comment_text, comment_visited, " .
                "    comments.date_created, ".
                "    comment_category_name, comment_category_priority, ".
                "    concat(user_name, ' ', user_firstname) as user_fullname ".
                "from comments ".
                "left join users on user_id = comment_user ".                
                "left join comment_categories on comment_category_id = comment_category ".
                "left join comment_addresses on comment_address_comment = comment_id";


// build filter for the list of comments
$list1_filter = "comment_category_order_type = 2 and comment_order = " . $project["project_order"];


if (has_access("has_access_to_all_comments_in_projects"))
{
    
}
else
{
	$list1_filter = $list1_filter . " and (comment_address_address = " . $user_data["address"] . " or comment_user = " . user_id() . ")";
}




// get new comment info pix
$sql_pix = $sql_comments . " where " . $list1_filter;
$images = set_new_comment_pictures($sql_pix, $project["project_order"]);


// determine users that have already got an email announcing the new attachment
$recipients = array();


$res = mysql_query($sql_pix) or dberror($sql_pix);
while ($row = mysql_fetch_assoc($res))
{
	$names = "";
	$companies = get_involved_companies_2($project["project_order"], $row["comment_id"]);


	foreach($companies as $key=>$value)
	{
		if($value["access"] == 1)
		{
			$names .= $value["name"] . "<br />";
		}
	}
	if($names)
	{
		$recipients[$row["comment_id"]] = $names;
	}
}



/********************************************************************
    Create Form
*********************************************************************/ 


$form = new Form("comments", "comment");


$form->add_section("Project");
$form->add_hidden("pid", param("pid"));


require_once "include/project_head_small.php";


/********************************************************************
    Create List
*********************************************************************/ 


$list1 = new ListView($sql_comments);


$list1->set_entity("comments");
$list1->set_filter($list1_filter);
$list1->set_order("comments.date_created DESC");
$list1->set_group("comment_category_priority", "comment_category_name");





$link = "project_edit_comment.php?pid=" . param("pid");


if (has_access("can_edit_comment_data_in_projects"))
{
    $list1->add_column("date_created", "Date/Time", $link, "", "", COLUMN_NO_WRAP);
}
else
{
    $list1->add_column("date_created", "Date/Time", "", "", "", COLUMN_NO_WRAP);
}

if(count($images)> 0)
{
    $list1->add_image_column("comment_id", "New", 0, $images);
}

$list1->add_column("user_fullname", "Made by", "", "", "", COLUMN_NO_WRAP);
$list1->add_column("comment_text", "Comment", "", "", "", COLUMN_BREAK);

$list1->add_text_column("info", "Recipients", COLUMN_UNDERSTAND_HTML | COLUMN_NO_WRAP, $recipients);



if (has_access("can_add_comments_in_projects"))
{
    $list1->add_button("add_comment", "Add Comment");
}




/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list1->populate();
$list1->process();


if ($list1->button("add_comment"))
{
    $link = "project_add_comment.php?pid=" . param("pid");
    redirect ($link);
}

$page = new Page("projects");


require "include/project_page_actions.php";


$page->header();
$page->title("Comments");
$form->render();
$list1->render();
$page->footer();


?>