<?php
/********************************************************************

    project_task_history.php

    Shows a history of steps of the actual project

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-11-26
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-11-26
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("has_access_to_order_status_report_in_projects");

register_param("pid");


/********************************************************************
    prepare all data needed
*********************************************************************/
// read order details
$project = get_project(param("pid"));
$order_id = $project["project_order"];


// get company's address
$client_address = get_address($project["order_client_address"]);

/********************************************************************
    history data
*********************************************************************/

$sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
          "order_state_group_code, order_state_group_name, " .
          "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
          "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
          "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y %H:%i') as performed " .
          "from order_state_groups " .
          "left join order_states on order_state_group = order_state_group_id " . "left join actual_order_states on actual_order_state_order = " . $order_id . " and actual_order_state_state = order_state_id " .
          "left join users on users.user_id = actual_order_state_user " .
          "left join order_mails on order_mail_order = " . $order_id . " " .
          "and order_mail_order_state = order_state_id " .
          "left join users as tousers on tousers.user_id = order_mail_user " .
          "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

$filter_hi = "order_state_group_order_type = 1 " .
             "and users.user_name <> '' ";


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project_status_report", 640);
$form->add_hidden("pid", param("pid"));

$form->add_section("Project");

require_once "include/project_head_small.php";


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Create History
*********************************************************************/ 
$history = new ListView($sql_hi, LIST_HAS_HEADER);

$history->set_title("History");
$history->set_entity("order_state");
$history->set_filter($filter_hi);
$history->set_order("order_state_code, actual_order_states.date_created ASC");
$history->set_group("order_state_group_code", "order_state_group_name");

$history->add_column("order_state_code", "Step");
$history->add_column("order_state_action_name", "Action");

$history->add_column("fullname", "Performed by");
$history->add_column("performed", "Date");
$history->add_column("tofullname", "Recipient");

$history->populate();
$history->process();

/********************************************************************
    render page
*********************************************************************/
$page = new Popup_Page("orders");

$page->header();
$page->title("Task History of Project - " . $project["order_number"]);

   
$form->render();

$history->render();

$page->footer();

?>