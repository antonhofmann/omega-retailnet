<?php
/********************************************************************

    project_financial_justification.php

    Creation and mutation of project financial jusstifications

    Created by:     Oliver Hofer (oliver.hofer@pageagent.com)
    Date created:   2003-04-24
    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date modified:  2003-04-24
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_project_sheet");

register_param("pid");
$form = new Form("project_financial_justifications", "justification");

if (id())
{
    $sql = "select project_financial_justification_project " .
           "from project_financial_justifications " .
           "where project_financial_justification_id = " . id();
    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_row($res);
    param("project", $row[0]);
}

$form->add_hidden("project_financial_justification_project", param("project"));
$form->add_hidden("project_financial_justification_priority");

$form->add_section();
$form->add_edit("project_financial_justification_description", "Description", NOTNULL,"","",80);
$form->add_edit("project_financial_justification_cost", "Cost", 0);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();

if (!$form->value("project_financial_justification_priority"))
{
/*
if (param('project'))
{
*/
    $sql = "select max(project_financial_justification_priority) as max_prio " .
           "from project_financial_justifications " .
           "where project_financial_justification_project = " . param('project');
    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_row($res);
    $form->value("project_financial_justification_priority", $row[0] + 1);
}

$form->process();

$page = new Page("projects");

$page->header();
$page->title(id() ? "Edit Description" : "Add Description");
$form->render();
$page->footer();

?>
