<?php
/********************************************************************

    project_edit_project_sheet.php

    Edit budget sheet.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-09
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-02-06
    Version:        1.2.3

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_edit_project_sheet");
set_referer("project_financial_justification.php");

if (!isset($_REQUEST["id"]))
{
    $_REQUEST["id"] = param("pid");
}
else
{
    param("pid", $_REQUEST["id"]);
}

/********************************************************************
    prepare all data needed
*********************************************************************/

// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

// get clients's currency
$currency = get_address_currency($project["order_client_address"]);

// get system's currency
$system_currency = get_system_currency_fields();

// get suppliers's address
if ($project["order_supplier_address"])
{
    $supplier_address = get_address($project["order_supplier_address"]);
}

// sql for supplier's list box
$sql_suppliers = "select address_id, address_company ".
                 "from addresses ".
                 "where address_type IN(2,8)";

// sql for project requesters
// oho ---------------------------> MUST BE CHANGED TO MATCH USER-LIST IN EXCEL-SHEET!!!
$sql_requesters = "select user_id, concat(user_firstname, ' ', user_name) " .
                  "from user_roles ".
                  "left join users on users.user_id = user_roles.user_role_user ".
                  "where user_role_role in (2,3, 4)";

// sql for business-units select
$sql_bu = "select business_unit_id, business_unit_name " .
          "from business_units " . 
		  "where business_unit_application IN ('all', 'projects') " . 
		  "order by business_unit_name";



/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");
$form->add_edit("project_sheet_name", "Project Sheet Title", NOTNULL, 
                "Project Sheet " . date("Y"));

$form->add_section("Project");
$form->add_hidden("pid", param("pid"));

require_once "include/project_head_small.php";

$form->add_edit("project_name", "Project Name*", NOTNULL);
$form->add_list("project_requester", "Project Requester*", $sql_requesters, SUBMIT | NOTNULL, $project["project_requester"]);

$form->add_section("Accounting Information");
$form->add_edit("project_cost_center", "Cost Center*", NOTNULL);
$form->add_edit("project_account_number", "Account Number*", NOTNULL);

$form->add_section("Budget Information");

$form->add_date("project_opening_date", "Opening Date");
$form->add_date("project_closing_date", "Closing Date");

$form->add_edit("project_planned_amount_current_year", "Planned Amount current Year in ". $currency["symbol"], SUBMIT);
$form->add_edit("project_planned_amount_next_year", "Planned Amount next Year in ". $currency["symbol"]);

$share_options = array(0 => 0, 25 => 25, 50 => 50, 75 => 75, 100 => 100);
$form->add_list("project_share_company", "Share " . BRAND . " in %", $share_options); 
$form->add_list("project_share_other", "Share Other in %", $share_options); 

$form->add_section("Budget Situation");
$form->add_edit("project_budget_total", "Budget Total in ". $system_currency["symbol"]);
$form->add_edit("project_budget_committed", "Budget Committed in ". $system_currency["symbol"]);
$form->add_edit("project_budget_spent", "Already Spent in ". $system_currency["symbol"]);
$form->add_edit("project_budget_after_project", "Open to Commit after this Project in ". $system_currency["symbol"]);

$approval_names = array();

if (param("project_planned_amount_current_year"))
{
    for($i = 1; $i < 8; $i++)
    {
        $approval_names["project_approval_person" . $i] = "";
    }

    $sql = "select budget_approval_user " .
           "from budget_approvals " .
           "where budget_approval_budget_low < ". 
               intval(param("project_planned_amount_current_year")) . " " .
           "order by budget_approval_priority asc";
    $res = mysql_query($sql);

    $approval_line = 1;
    while($row = mysql_fetch_assoc($res))
    {
        if (!in_array($row['budget_approval_user'], $approval_names))
        {
            while ($approval_line < 8)
            {
                if (empty($approval_names["project_approval_person" . $approval_line]))
                {
                    $approval_names["project_approval_person" . $approval_line] = 
                        $row['budget_approval_user'];
                    break;
                }
                else
                {
                    $approval_line++;
                }
            }
        }
    }
}

if (param("project_business_unit"))
{
    $sql = "select concat(user_name, ' ', left(user_firstname,1), '.') as username " .
           "from business_units " .
           "left join users on users.user_id = business_units.business_unit_responsible " .
           "where business_unit_id = " . param("project_business_unit");

    $res = mysql_query($sql) or dberror($sql);
    if (($res) && (mysql_num_rows($res) > 0))
    {
        $row = mysql_fetch_assoc($res);

        if (!in_array($row['username'], $approval_names))
        {
            $approval_names["project_approval_person6"] = $row['username'];
        }
        else
        {
            $approval_names["project_approval_person6"] = "";
        }
    }
}

if (param("project_requester"))
{
    $sql = "select concat(user_name, ' ', left(user_firstname,1), '.') as username " .
           "from users " .
           "where user_id = " . param("project_requester");

    $res = mysql_query($sql) or dberror($sql);
    if (($res) && (mysql_num_rows($res) > 0))
    {
        $row = mysql_fetch_assoc($res);
        if (!in_array($row['username'], $approval_names))
        {
            $approval_names["project_approval_person7"] = $row['username'];
        }
        else
        {
            $approval_names["project_approval_person7"] = "";
        }
    }
}

// assign approval-names

foreach($approval_names as $key => $name)
{
        param($key, $name);
}

$form->add_section("Approvals");
$form->add_edit("project_approval_person1", "Person 1");
$form->add_date("project_approval_date1", "Dead Line for Approval");
$form->add_label("spacer1", "");
$form->add_edit("project_approval_person2", "Person 2");
$form->add_date("project_approval_date2", "Dead Line for Approval");
$form->add_label("spacer2", "");
$form->add_edit("project_approval_person3", "Person 3");
$form->add_date("project_approval_date3", "Dead Line for Approval");
$form->add_label("spacer3", "");
$form->add_edit("project_approval_person4", "Person 4");
$form->add_date("project_approval_date4", "Dead Line for Approval");
$form->add_label("spacer4", "");
$form->add_edit("project_approval_person5", "Person 5",0);
$form->add_date("project_approval_date5", "Dead Line for Approval");
$form->add_label("spacer5", "");
$form->add_edit("project_approval_person6", "Person 6");
$form->add_date("project_approval_date6", "Dead Line for Approval");
$form->add_label("spacer6", "");
$form->add_edit("project_approval_person7", "Person 7",0);
$form->add_date("project_approval_date7", "Dead Line for Approval");

// $form->add_multiline("project_approval_description", "Financial Justification", 6);

$form->add_section("Main Supplier");
$form->add_list("order_supplier_address", "Supplier*", $sql_suppliers, SUBMIT, $project["order_supplier_address"]);
if ($project["order_supplier_address"])
{
    $form->add_label("supplier_company", "Company", 0, $supplier_address["company"]);
    $form->add_label("supplier_address", "Address", 0, $supplier_address["address"]);
    $form->add_label("supplier_place", "City", 0, $supplier_address["zip"] . " " . $supplier_address["place"]);
    $form->add_lookup("supplier_country", "Country", "countries", "country_name", 0, $supplier_address["country"]);

    if ($supplier_address["contact"])
    {
        $line = "concat(user_name, ' ', user_firstname)";
        $form->add_lookup("supplier_contact_user", "Contact", "users", $line, 0, $supplier_address["contact"]);
        $form->add_lookup("supplier_contact_phone", "Phone", "users", "user_phone", 0, $supplier_address["contact"]);
        $form->add_lookup("supplier_contact_email", "Email", "users", "user_email", 0, $supplier_address["contact"]);
    }
}
else
{
    $form->add_label("supplier_company", "Company");
    $form->add_label("supplier_address", "Address");
    $form->add_label("supplier_place", "City");
    $form->add_label("supplier_country", "Country");
    $form->add_label("supplier_contact", "Contact");
    $form->add_label("supplier_phone", "Phone");
    $form->add_label("supplier_email", "Email");
}

// new by neo
$form->add_section("Description, Financial Justification");
$form->add_list("project_business_unit", "Business Unit*", $sql_bu, SUBMIT | NOTNULL, $project["project_business_unit"]);

$form->add_button("add_justification", "Add Financial Jusitification", 
                  "project_financial_justification.php?project=" . id(), OPTIONAL);

$form->add_subtable("project_financial_justifications", "Financial Justification",
                    "project_financial_justification.php", "Description",
    "select project_financial_justification_id, ". 
    "    project_financial_justification_description as Description, " .
    "    project_financial_justification_cost as Cost " .
    "from project_financial_justifications " .
    "where project_financial_justification_project = " . id() . " " .
    "order by project_financial_justification_priority ", "project_financial_justification_priority");

$form->add_button(FORM_BUTTON_SAVE, "Save Data");
$link = "http://" . $_SERVER["HTTP_HOST"] . "/user/project_sheet_pdf.php?id=" . $project["project_order"];
$form->add_button("print_pdf", "Print Project Sheet", $link);

/********************************************************************
    Populate form and process button clicks
*********************************************************************/
$form->populate();
$form->process();

if ($form->button("order_supplier_address"))
{
    $form->save();
    $sql = "update orders ".
           "set order_supplier_address = " . $form->value("order_supplier_address") . " ".
           "where order_id = ". $project["project_order"];

    $res = mysql_query($sql) or dberror($sql);
    
    redirect ("project_edit_project_sheet.php?pid=" . param("pid"));
}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Edit Project Sheet");
$form->render();
$page->footer();

?>