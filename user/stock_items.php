<?php
/********************************************************************

    stock_items.php

    List stock data of items for a specific suppliers

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-10-07
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-01-26
    Version:        2.1.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/order_state_constants.php";

check_access("can_view_stock");


register_param("id");
set_referer("stock_edit_stock.php");
set_referer("stock_global_orders.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
$images = array();

$sql_items = "select distinct store_item, item_code, store_minimum_stock " .
              "from stores " .
              "left join items on item_id = store_item ";

$list_filter = "store_address = " . id() . 
               " and store_show_in_stock_control = 1 " . 
               " and item_type = " . ITEM_TYPE_STANDARD;


$sql = "select item_id, item_code " .
       "from items " .
       "where item_type = " . ITEM_TYPE_STANDARD;

//prepare arrays
$sum_of_quantity_having_a_ready_for_pick_up_date = array();
$sum_of_quantity_having_a_pick_up_date = array();
$sum_of_quantity_having_an_actual_arrival_date = array();
$sum_of_quantity_having_an_order_confirmation_date = array();
$sum_of_quantity_having_an_approved_budget = array();
$sum_of_quantity_in_delivery = array();
$sum_of_quantity_booked =array();
$item_remaining_global_order = array();
$item_available = array();
$item_average_per_week = array();
$item_medium_range = array();
$item_reproduction_time = array();

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
    $sum_of_quantity_having_a_ready_for_pick_up_date[$row["item_id"]] = 0;
    $sum_of_quantity_having_a_pick_up_date[$row["item_id"]] = 0;
    $sum_of_quantity_having_an_actual_arrival_date[$row["item_id"]] = 0;
    $sum_of_quantity_having_an_order_confirmation_date[$row["item_id"]] = 0;
    $sum_of_quantity_having_an_approved_budget[$row["item_id"]] = 0;
    $sum_of_quantity_booked[$row["item_id"]] = 0;
    $item_remaining_global_order[$row["item_id"]] = 0;
    $item_available[$row["item_id"]] = 0;
    $item_average_per_week[$row["item_id"]] = 0;
    $item_medium_range[$row["item_id"]] = 0;
    $item_reproduction_time[$row["item_id"]] = 0;
}


// calculate quantities having an expected ready for pick up date
$sql = "Select item_id, item_code, sum(order_item_quantity) as tmp_sum ". 
       "from order_items " .
       "left join orders on order_item_order = order_id ".
       "left join items on item_id = order_item_item ".
       "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
       "   and order_item_ready_for_pickup <> '0000-00-00' ".
       "   and order_item_ready_for_pickup is not null and order_cancelled is null ".
       "   and order_item_type = " . ITEM_TYPE_STANDARD . " ".
       "   and order_item_supplier_address = " . id() . " " .
       "   and order_cancelled  is null " .
       "group by item_id, item_code ".
       "order by item_code ";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
    $sum_of_quantity_having_a_ready_for_pick_up_date[$row["item_id"]] = $row["tmp_sum"];

}


// calculate quantities having a pick up date
$sql = "Select item_id, item_code, sum(order_item_quantity) as tmp_sum ". 
       "from order_items " .
       "left join orders on order_item_order = order_id ".
       "left join items on item_id = order_item_item ".
       "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
       "   and order_item_pickup <> '0000-00-00' ".
       "   and order_item_pickup is not null and order_cancelled is null ".
       "   and order_item_type = " . ITEM_TYPE_STANDARD . " ".
       "   and order_item_supplier_address = " . id() . " " .
       "   and order_cancelled  is null " .
       "group by item_id, item_code ".
       "order by item_code ";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
    $sum_of_quantity_having_a_pick_up_date[$row["item_id"]] = $row["tmp_sum"];

}

// calculate quantities having actual arrival date
$sql = "Select item_id, item_code, sum(order_item_quantity) as tmp_sum ". 
       "from order_items " .
       "left join orders on order_item_order = order_id ".
       "left join items on item_id = order_item_item ".
       "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
       "   and order_item_arrival <> '0000-00-00' ".
       "   and order_item_arrival is not null and order_cancelled is null ".
       "   and order_item_type = " . ITEM_TYPE_STANDARD . " ".
       "   and order_item_supplier_address = " . id() . " " .
       "   and order_cancelled  is null " .
       "group by item_id, item_code ".
       "order by item_code ";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
    $sum_of_quantity_having_an_actual_arrival_date[$row["item_id"]] = $row["tmp_sum"];

}

// calculate quantities having an order confirmation date (for orders)
$sql = "Select item_id, item_code, sum(order_item_quantity) as tmp_sum ". 
       "from order_items " .
       "left join orders on order_item_order = order_id ".
       "left join items on item_id = order_item_item ".
       "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
       "   and order_item_type = " . ITEM_TYPE_STANDARD . " ".
       "   and order_actual_order_state_code >= " . ORDER_CONFIRMED .
       "   and order_type = 2 " .
       "   and order_item_supplier_address = " . id() . " " .
       "   and order_cancelled  is null " .
       "group by item_id, item_code ".
       "order by item_code ";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
    $sum_of_quantity_having_an_order_confirmation_date[$row["item_id"]] = $row["tmp_sum"];

}


// calculate quantities having an approved budget (for projects)
$sql = "Select item_id, item_code, sum(order_item_quantity) as tmp_sum ". 
       "from order_items " .
       "left join orders on order_item_order = order_id ".
       "left join items on item_id = order_item_item ".
       "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
       "   and order_item_type = " . ITEM_TYPE_STANDARD . " ".
       "   and order_actual_order_state_code >= " . BUDGET_APPROVED .
       "   and order_type = 1 " .
       "   and order_item_supplier_address = " . id() . " " .
       "   and order_cancelled  is null " .
       "group by item_id, item_code ".
       "order by item_code ";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
    $sum_of_quantity_having_an_approved_budget[$row["item_id"]] = $row["tmp_sum"];
}

//calculate bookings
foreach($sum_of_quantity_booked as $key=>$value)
{
    $sum_of_quantity_booked[$key] = $sum_of_quantity_having_an_order_confirmation_date[$key] + $sum_of_quantity_having_an_approved_budget[$key] - $sum_of_quantity_having_a_ready_for_pick_up_date[$key];
}


//calculat remaining global order , available quantities, avergae consumption and medium range and reproduction time in weeks
$sql = "select distinct order_item_item, store_global_order, store_inventory, ".
       "   store_stock_control_starting_date, store_consumed_upto_021101, " .
       "   store_reproduction_time_in_weeks, store_minimum_stock  " .
       "from order_items ".
       "left join stores on (order_item_item = store_item and order_item_supplier_address = store_address) ".
       "left join orders on order_item_order = order_id ".
       "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
       "   and order_item_type = " . ITEM_TYPE_STANDARD . " ".
       "   and order_item_supplier_address = " . id() . " " .
       "   and order_cancelled  is null " .
       "order by order_item_item ";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
    // remaining global order
    $item_remaining_global_order[$row["order_item_item"]] = $row["store_global_order"] - $sum_of_quantity_having_a_pick_up_date[$row["order_item_item"]];

    // available quntities
    $item_available[$row["order_item_item"]] = $row["store_global_order"] - $sum_of_quantity_having_a_pick_up_date[$row["order_item_item"]] - $sum_of_quantity_booked[$row["order_item_item"]];

    // quantity in delivery
    $sum_of_quantity_in_delivery[$row["order_item_item"]] =  $sum_of_quantity_having_a_pick_up_date[$row["order_item_item"]] - $sum_of_quantity_having_an_actual_arrival_date[$row["order_item_item"]];

    // average consumption per week
    if ($row["store_stock_control_starting_date"])
    {
        $date = $row["store_stock_control_starting_date"];
        $y = substr($date, 0, 4);
        $m = substr($date, 5, 2);
        $d = substr($date, 8, 2);
        $now = mktime(); 
        $then = mktime(0, 0, 0, $m, $d, $y);
        $num_of_seconds = ($now - $then);
        $num_of_days = $num_of_seconds / 86400; 
        $num_of_weeks = $num_of_days / 7; 
        
        number_format($num_of_weeks, 2);

        if ($num_of_weeks > 0)
        {
            $item_average_per_week[$row["order_item_item"]] = number_format(($row["store_consumed_upto_021101"] + $sum_of_quantity_having_an_actual_arrival_date[$row["order_item_item"]]) / $num_of_weeks , 2);

            if ($item_average_per_week[$row["order_item_item"]] > 0)
            {
                $item_medium_range[$row["order_item_item"]] = number_format($item_available[$row["order_item_item"]] /  $item_average_per_week[$row["order_item_item"]], 2);
            }
            else
            {
                $item_medium_range[$row["order_item_item"]] = "n/a";  
            }
        }
        else
        {
            $item_average_per_week[$row["order_item_item"]] = "n/a";
        }
    }

    // reproduction time in weeks
    $item_reproduction_time[$row["order_item_item"]] = $row["store_reproduction_time_in_weeks"];

    if($row["store_minimum_stock"] > $item_available[$row["order_item_item"]])
    {
        $images[$row["order_item_item"]] = "/pictures/problem.gif";
    }

}




/********************************************************************
    Create List
*********************************************************************/ 

$list = new ListView($sql_items);
$list->add_hidden("id", id());

$list->set_entity("stores");
$list->set_order("item_code");
$list->set_filter($list_filter);


$list->add_image_column("problem", "Att", 0, $images);

$list->add_column("item_code", "Item Code", "", 0, "", COLUMN_NO_WRAP);
$list->add_text_column("remaining_global_order", "Stock", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $item_remaining_global_order);

$list->add_column("store_minimum_stock", "Minimum\nStock", "", 0, "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP | COLUMN_BREAK);


$list->add_text_column("bookings", "Booked", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $sum_of_quantity_booked);
$list->add_text_column("available", "Available", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $item_available);



$list->add_text_column("ready4pickup", "in Delivery", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $sum_of_quantity_in_delivery);

$list->add_text_column("delivered", "Delivered", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $sum_of_quantity_having_an_actual_arrival_date);

$list->add_text_column("average", "Average\nper Week", COLUMN_ALIGN_RIGHT | COLUMN_BREAK, $item_average_per_week);
$list->add_text_column("range", "Range\nin Weeks", COLUMN_ALIGN_RIGHT | COLUMN_BREAK, $item_medium_range);
$list->add_text_column("reproduction", "Reproduction\nTime in Weeks", COLUMN_ALIGN_CENTER | COLUMN_BREAK, $item_reproduction_time);

//$list->add_text_column("ready", "ready", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $sum_of_quantity_having_a_ready_for_pick_up_date);


//$list->add_text_column("pickup", "pickup", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $sum_of_quantity_having_a_pick_up_date);

//$list->add_text_column("arrived", "arrived", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $sum_of_quantity_having_an_actual_arrival_date);

//$list->add_text_column("confirmed", "confirmed o", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $sum_of_quantity_having_an_order_confirmation_date);

//$list->add_text_column("approved", "approved p", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $sum_of_quantity_having_an_approved_budget);


if (has_access("can_enter_global_order"))
{
    $list->add_button("global_orders", "Global Orders");
}

$list->add_button(FORM_BUTTON_BACK, "Back");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$list->populate();
$list->process();

if($list->button("global_orders"))
{
    $link = "stock_global_orders.php?sid=" . id();
    redirect($link);
}


$page = new Page("stock");

$page->register_action('home', 'Home', "welcome.php");

$page->header();
$page->title("Stock Data");

$list->render();

$page->footer();

?>