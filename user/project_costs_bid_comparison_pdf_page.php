<?php
/********************************************************************

    project_costs_bid_comparison_pdf_page.php

    Print pge header for Offer Comparision for Local Construction Work

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-01-09
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2014, OMEGA SA, All Rights Reserved.

*********************************************************************/

$pdf->AddPage("l", "A4", $page_title);

$pdf->Image('../pictures/omega_logo.jpg',10,8,33);
$pdf->SetFont('arialn','B',12);
$pdf->SetY(10);
$pdf->Cell(0,12, $page_title, 0, 0, 'R');
$pdf->Ln(14);


// output project header informations
$pdf->SetFont('arialn','B',9);
$pdf->Cell(120,5,"Project Details",1);
$pdf->Cell(5,5," ",0);
$pdf->SetFont('arialn','B',9);
$pdf->Cell(67,5,$captions1[2],1);
$pdf->SetFont('arialn','',9);
$pdf->Cell(83,5,$data1[2], 1, 0, 'L');
$pdf->Ln();


$pdf->SetFont('arialn','B',9);
$pdf->Cell(70,5,$captions1[0],1);
$pdf->SetFont('arialn','',9);
$pdf->Cell(50,5,$data1[0],1);
$pdf->Cell(5,5," ",0);
$pdf->SetFont('arialn','B',9);
$pdf->Cell(67,5,$captions1[3],1);
$pdf->SetFont('arialn','',9);
$pdf->Cell(83,5,$data1[3], 1, 0, 'L');
$pdf->Ln();


$pdf->SetFont('arialn','B',9);
$pdf->Cell(70,5,$captions1[1],1);
$pdf->SetFont('arialn','',9);
$pdf->Cell(50,5,$data1[1],1);
$pdf->Cell(5,5," ",0);
$pdf->SetFont('arialn','B',9);
$pdf->Cell(67,5,$captions1[4],1);
$pdf->SetFont('arialn','',9);
$pdf->Cell(83,5,$data1[4], 1, 0, 'L');
$pdf->Ln();


$pdf->Ln();


$pdf->SetFont('arialn','',9);
foreach($captions3 as $key=>$value)
{
    $pdf->SetFont('arialn','B',9);
    $pdf->Cell(40,5,$captions3[$key],1, 0, 'L');
    $pdf->SetFont('arialn','',9);
    $pdf->MultiCell(235,5,$data3[$key],1, 'L');
}


$pdf->Ln();

?>