<?php
/********************************************************************

    order_add_catalog_item_categories.php

    Add items to the list of materials
    List all Categories

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-11
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-07-05
    Version:        1.0.3

    Copyright (c) 2002, Swatch AG, All Rights Reserved.
*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "include/order_functions.php";

check_access("can_edit_list_of_materials_in_orders");

register_param("oid");
set_referer("order_add_catalog_item_item_list.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$order = get_order(param("oid"));
$region = get_user_country_region($order['order_user']);

// get company's address
$client_address = get_address($order["order_client_address"]);

/*
$visible_category = '';
$sql =  "select category_item_category " .
        "from category_items ".
        "left join items on category_items.category_item_item = items.item_id " .
        "left join item_regions on items.item_id = item_regions.item_region_item ".
        "where items.item_visible=1 and item_region_region = " . 1 . " ".
        "group by category_item_category";

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
    $visible_category[] = $row['category_item_category'];
}

$sql_categories = "select category_id, category_name, product_lines.product_line_name, " . 
                  "    product_line_priority " . 
                  "from categories " . 
                  "left join product_lines on product_lines.product_line_id = category_product_line ";

// compose filter-clause

$filter = "category_catalog = 1 and (category_not_in_use is null or category_not_in_use = 0)";
if (is_array($visible_category))
    $filter .= " and category_id in (" . implode(",", $visible_category) . ") ";

*/

$sql_categories = "select distinct category_id, category_name, product_lines.product_line_name, " . 
                  "    product_line_priority " . 
                  "from categories " . 
                  "left join product_lines on product_lines.product_line_id = category_product_line " .
                  "left join category_items on category_item_category = category_id " .
                  "left join items on category_item_item = item_id " .
                  "left join item_regions on item_id = item_region_item ";

// compose filter-clause

$filter = "category_catalog = 1 " . 
          "and (category_not_in_use is null or category_not_in_use = 0) " .
          "and item_region_region = 1";

/********************************************************************
    Create Form
*********************************************************************/ 


$form = new Form("orders", "order", 640);
$form->add_section("Order");
require_once "include/order_head_small.php";

$form->add_section(" ");

$form->add_comment("Enter a search term or choose from the list of item categories below.<br /><br />");
$form->add_edit("searchterm", "Search Term");


$form->add_button("search", "Search Catalog");
$form->add_button(LIST_BUTTON_BACK, "Back");


/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql_categories);


$list->set_entity("categories");
$list->set_order("product_line_priority", "category_priority");
$list->set_group("product_line_priority","product_line_name");
$list->set_filter($filter);


$list->add_hidden("oid", param("oid"));


$link = "order_add_catalog_item_item_list.php?oid=" . param("oid");
$list->add_column("category_name", "Category", $link, LIST_FILTER_NONE);


/********************************************************************
    Populate page
*********************************************************************/ 
$form->populate();
$form->process();

$list->process();



if ($form->button("search"))
{
	if(!$form->value("searchterm"))
	{
		$form->error("The search term must not be empty.");
	}
	else
	{
		$link = "order_add_catalog_item_item_list.php?oid=" . param('oid') . "&searchterm=" . $form->value("searchterm");
		redirect($link);
	}
}




/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");


require "include/order_page_actions.php";


$page->header();


$page->title("Add Catalog Items: Standard Items");
$form->render();


echo "<p>", "Please choose from the following item categories.", "</p>";


$list->render();
$page->footer();


?>