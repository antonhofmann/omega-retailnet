<?php
/********************************************************************

    project_flow_control_history.php

    List Flow History and Actions

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-04-22
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-04-22
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_view_history_in_projects");

register_param("pid");
register_param("os");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));


// get order states for history
$sql_order_states = "select order_state_code, order_state_group_code, ".
                    "    order_state_group_name, order_state_action_name, ".
                    "    order_state_name, role_id, role_name, order_state_notification_recipient, order_state_id ".
                    "from order_state_groups ".
                    "left join order_states on order_state_group_id = order_state_group " .
                    "left join roles on order_state_performer = role_id ";


//$list3_filter = "order_state_group_order_type = 1 and order_state_code = '" . param("os") . "'";
$list3_filter = "order_state_group_order_type = 1";


// prepare icon
$icons = array();
$icons[$project["order_actual_order_state_code"]] = "/pictures/actualorderstate_now.gif";


$history_sender = array();
$history_recipient = array();
$history_performers = array();
$history_date_of_performance = array();


$sql =  "select order_state_code, order_state_group_code, ".
        "    order_state_group_name, order_state_action_name, ".
        "    order_state_name, role_id, role_name, order_state_notification_recipient, " .
        "    order_state_id ".
        "from order_state_groups ".
        "left join order_states on order_state_group_id = order_state_group " .
        "left join roles on order_state_performer = role_id " .
        "where order_state_group_order_type = 1 " .
        "order by order_state_code";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{

    //get sender
    if($row["role_id"] == 2) // retail operator
    {
        $user = get_user($project["order_retail_operator"]);
        $address = get_address($user["address"]);

        $history_sender[$row["order_state_code"]] = $address["shortcut"];

    }
    elseif($row["role_id"] == 3) // retail coordinator
    {
        $user = get_user($project["project_retail_coordinator"]);
        $address = get_address($user["address"]);

        $history_sender[$row["order_state_code"]] = $address["shortcut"];
    }
    elseif($row["role_id"] == 4) // client
    {
        $address = get_address($project["order_client_address"]);
        $history_sender[$row["order_state_code"]] = $address["shortcut"];
    }
    elseif($row["role_id"] == 7) // design contractor
    {
        if($project["project_design_contractor"])
        {
            $user = get_user($project["project_design_contractor"]);
            $address = get_address($user["address"]);

        }
        $history_sender[$row["order_state_code"]] = $address["shortcut"];
    }
    elseif($row["role_id"] == 8) // design supervisor
    {
        if($project["project_design_contractor"])
        {
            $user = get_user($project["project_design_supervisor"]);
            $address = get_address($user["address"]);
        }

        $history_sender[$row["order_state_code"]] = $address["shortcut"];
    }
    elseif($row["role_id"] == 5) // supplier
    {
        $s1 = "";
        $s2 = "";
        $s3 = "";

        $sql_p = "select distinct address_shortcut " .
                 "from order_items " .
                 "left join addresses on address_id = order_item_supplier_address " .
                 "where order_item_order = " . $project["project_order"] . " " .
                 "   and order_item_type < 3 " .
                 "order by address_shortcut";
               

        $res_p = mysql_query($sql_p) or dberror($sql_p);
        while ($row_p = mysql_fetch_assoc($res_p))
        {
            $s1 .= $row_p["address_shortcut"] . "\n";

            $sql_m = "select order_mails.date_modified as date, " .
                     "concat(user_name, ' ', left(user_firstname,1), '.') as from_user_fullname " .
                     "from order_mails " .
                     "left join users on user_id = order_mail_from_user " . 
                     "left join addresses on address_id = user_address " .
                     "where order_mail_order = " . $project["project_order"] . " " .
                     "   and order_mail_order_state = " . $row["order_state_id"] . " " .
                     "   and address_shortcut = '" . $row_p["address_shortcut"] . "' " .
                     "order by order_mails.date_modified DESC";


             $res_m = mysql_query($sql_m) or dberror($sql_m);
             if ($row_m = mysql_fetch_assoc($res_m))
             {
                $s2 .= $row_m["from_user_fullname"] . "\n";
                $s3 .= to_system_date($row_m["date"]) . "\n";
             }
             else
             {
                 $s2 .= "\n";
                 $s3 .= "\n";
             }

             $history_performers[$row["order_state_code"]] = $s2;
             $history_date_of_performance[$row["order_state_code"]] = $s3;
        }

        $history_sender[$row["order_state_code"]] = $s1;
    }
    elseif($row["role_id"] == 6) // forwarder
    {
        $s1 = "";
        $s2 = "";
        $s3 = "";

        $sql_p = "select distinct address_shortcut " .
                 "from order_items " .
                 "left join addresses on address_id = order_item_forwarder_address " .
                 "where order_item_order = " . $project["project_order"] . " " .
                 "   and order_item_type < 3 " .
                 "order by address_shortcut";
               

        $res_p = mysql_query($sql_p) or dberror($sql_p);
        while ($row_p = mysql_fetch_assoc($res_p))
        {
            $s1 .= $row_p["address_shortcut"] . "\n";

            $sql_m = "select order_mails.date_modified as date, " .
                     "concat(user_name, ' ', left(user_firstname,1), '.') as from_user_fullname " .
                     "from order_mails " .
                     "left join users on user_id = order_mail_from_user " . 
                     "left join addresses on address_id = user_address " .
                     "where order_mail_order = " . $project["project_order"] . " " .
                     "   and order_mail_order_state = " . $row["order_state_id"] . " " .
                     "   and address_shortcut = '" . $row_p["address_shortcut"] . "' " .
                     "order by order_mails.date_modified DESC";


             $res_m = mysql_query($sql_m) or dberror($sql_m);
             if ($row_m = mysql_fetch_assoc($res_m))
             {
                $s2 .= $row_m["from_user_fullname"] . "\n";
                $s3 .= to_system_date($row_m["date"]) . "\n";
             }
             else
             {
                 $s2 .= "\n";
                 $s3 .= "\n";
             }

             $history_performers[$row["order_state_code"]] = $s2;
             $history_date_of_performance[$row["order_state_code"]] = $s3;
        }

        $history_sender[$row["order_state_code"]] = $s1;
    }

    // get done data
    if($row["role_id"] == 2 or $row["role_id"] == 3 or $row["role_id"] == 4 or $row["role_id"] == 7 or $row["role_id"] == 8)
    {
        $sql_p = "select actual_order_states.date_modified as done_date, " .
                 "concat(user_name, ' ', left(user_firstname,1), '.') as from_user_fullname " .
                 "from actual_order_states " .
                 "left join users on user_id = actual_order_state_user " .
                 "where actual_order_state_order = " . $project["project_order"] . " " .
                 "   and actual_order_state_state = " . $row["order_state_id"] . " " .
                 "order by actual_order_states.date_modified DESC";

        $res_p = mysql_query($sql_p) or dberror($sql_p);
        if($row_p = mysql_fetch_assoc($res_p))
        {
            $history_performers[$row["order_state_code"]] = $row_p["from_user_fullname"];
            $history_date_of_performance[$row["order_state_code"]] = to_system_date($row_p["done_date"]);
        }
    }


    //get recipients
    if($row["order_state_notification_recipient"] == 4) // retail operator
    {
        $user = get_user($project["order_retail_operator"]);
        $address = get_address($user["address"]);

        $history_recipient[$row["order_state_code"]] = $address["shortcut"];
    }
    elseif($row["order_state_notification_recipient"] == 5) // retail coordinator
    {
        $user = get_user($project["project_retail_coordinator"]);
        $address = get_address($user["address"]);

        $history_recipient[$row["order_state_code"]] = $address["shortcut"];
    }
    elseif($row["order_state_notification_recipient"] == 1) // client
    {
        $address = get_address($project["order_client_address"]);
        $history_recipient[$row["order_state_code"]] = $address["shortcut"];
    }
    elseif($row["order_state_notification_recipient"] == 6) // design contractor
    {
        $user = get_user($project["project_design_contractor"]);
        $address = get_address($project["order_client_address"]);

        $history_recipient[$row["order_state_code"]] = $address["shortcut"];
    }
    elseif($row["order_state_notification_recipient"] == 8) // design supervisor
    {
        $user = get_user($project["project_design_supervisor"]);
        $address = get_address($project["order_client_address"]);

        $history_recipient[$row["order_state_code"]] = $address["shortcut"];
    }
    elseif($row["order_state_notification_recipient"] == 2) // supplier
    {
        $s1 = "";
        $sql_p = "select distinct address_shortcut " .
                 "from order_items " .
                 "left join addresses on address_id = order_item_supplier_address " .
                 "where order_item_order = " . $project["project_order"] . " " .
                 "   and order_item_type < 3 " .
                 "order by address_shortcut";
               

        $res_p = mysql_query($sql_p) or dberror($sql_p);
        while ($row_p = mysql_fetch_assoc($res_p))
        {
            $s1 .= $row_p["address_shortcut"] . "\n";
        }

        $history_recipient[$row["order_state_code"]] = $s1;
    }
    elseif($row["order_state_notification_recipient"] == 3) // forwarder
    {
        $s1 = "";
        $sql_p = "select distinct address_shortcut " .
                 "from order_items " .
                 "left join addresses on address_id = order_item_forwarder_address " .
                 "where order_item_order = " . $project["project_order"] . " " .
                 "   and order_item_type < 3 " .
                 "order by address_shortcut";
               

        $res_p = mysql_query($sql_p) or dberror($sql_p);
        while ($row_p = mysql_fetch_assoc($res_p))
        {
            $s1 .= $row_p["address_shortcut"] . "\n";
        }

        $history_recipient[$row["order_state_code"]] = $s1;
    }

}



/********************************************************************
    Create History List
*********************************************************************/ 

$list3 = new ListView($sql_order_states, LIST_HAS_SEPARATOR | LIST_HAS_HEADER);

$list3->set_title("History: Project Steps (Indicating the Latest Actions)");
$list3->set_entity("order_state");
$list3->set_filter($list3_filter);
$list3->set_order("order_state_code");
$list3->set_group("order_state_group_code", "order_state_group_name");

$list3->add_image_column("actual_step", "Now", 0, $icons);
$list3->add_column("order_state_code", "Step");
$list3->add_column("order_state_action_name", "Action", "", "", "", COLUMN_NO_WRAP);

$list3->add_text_column("performer", "Sender", COLUMN_NO_WRAP | COLUMN_BREAK, $history_sender);
$list3->add_text_column("user", "Sent by", COLUMN_NO_WRAP | COLUMN_BREAK, $history_performers);
$list3->add_text_column("date", "Date", COLUMN_NO_WRAP | COLUMN_BREAK, $history_date_of_performance);
$list3->add_text_column("recipient", "Recipient", COLUMN_NO_WRAP | COLUMN_BREAK, $history_recipient);


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$list3->populate();
$list3->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Popup_Page("projects");

$page->header();
$page->title("Task Centre - History of " . $project["order_number"]);
  
$list3->render();

$page->footer();

?>