<?php
/********************************************************************

    order_edit_budget_position.php

    Add cost estimation positions to an order

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-02-16
    Version:        1.0.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "include/order_functions.php";

check_access("can_edit_budget_in_orders");

register_param("oid");
register_param("id");
set_referer("order_edit_order_budget.php");

/********************************************************************
    prepare all data needed
*********************************************************************/

// read project and order details
$order = get_order(param("oid"));

// get company's address
$client_address = get_address($order["order_client_address"]);

// read order_item details
$order_item = get_order_item(id());
$item_type = $order_item["type"];

// get system currency
$system_currency = get_system_currency_fields();

// get orders's currency
$order_currency = get_order_currency(param('oid'));


// get item type name
$sql_item_types = "select item_type_name ".
                  "from item_types ".
                  "where item_type_id = " .  $item_type ;
               
$res = mysql_query($sql_item_types) or dberror($sql_item_types);
if ($row = mysql_fetch_assoc($res))
{
    $section_title = $row["item_type_name"];
}


// get item information
$price_adjustable=0;
$sql_items = "select item_price_adjustable ".
                  "from order_items ".
                  "left join items on order_item_item = item_id ".
                  "where order_item_id = " .  id() ;
               
$res = mysql_query($sql_items) or dberror($sql_items);
if ($row = mysql_fetch_assoc($res))
{
    $price_adjustable = $row["item_price_adjustable"];
}


/********************************************************************
    build form
*********************************************************************/
$form = new Form("order_items", "budget position");

$form->add_section("Order");
$form->add_hidden("oid",param('oid'));
$form->add_hidden("order_item_id", id());
$form->add_hidden("order_item_order",param('oid'));
$form->add_hidden("order_item_type",  $item_type);

require_once "include/order_head_small.php";

$form->add_section($section_title);
$form->add_multiline("order_item_text", "Description*", 4, NOTNULL);
if ( $item_type  == ITEM_TYPE_STANDARD OR $item_type  == ITEM_TYPE_SPECIAL or $item_type  == ITEM_TYPE_COST_ESTIMATION)
{
    $form->add_label("order_item_system_price", "Price in " . $system_currency["symbol"] . "*");
    $form->add_hidden("order_item_client_price_old", $order_item["client_price"]);

    if($price_adjustable == 1 or $item_type != ITEM_TYPE_STANDARD)
    {

        $form->add_edit("order_item_client_price", "Client's Price in " . $order_currency["symbol"] . "*", NOTNULL);
        $form->add_multiline("order_item_price_comment", "Comment for Price Changes", 4, 0);
    }
    else
    {
        $form->add_label("order_item_client_price", "Client's Price in  " . $system_currency["symbol"]);
    }
}

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_DELETE, "Delete");
$form->add_button(FORM_BUTTON_BACK, "Back");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button(FORM_BUTTON_SAVE))
{
    if ($form->validate())
    {
        if ($form->value('order_item_client_price_old') != $form->value('order_item_client_price') and !$form->value('order_item_price_comment'))
        {
            $form->error = "Since you have changed the price you have to enter a comment!";
        }
        else
        {
            $link = "order_edit_order_budget.php?oid=" . param("oid"); 
            redirect($link);
        }
    }
}

   
/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("Project Budget: Edit Position");
$form->render();
$page->footer();
?>