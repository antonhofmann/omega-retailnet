<?php
/********************************************************************

    project_new_01.php

    Creation of a new project step 01.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-07-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-07-14
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_create_new_projects");

set_referer("project_new_02.php");


if(count($_POST) == 0 and isset($_SESSION["new_project_step_1"]))
{
	$_SESSION["new_project_step_1"]["action"] = "";
	foreach($_SESSION["new_project_step_1"] as $key=>$value)
	{
		register_param($key);
		param($key, $value);
	}
}

/********************************************************************
    prepare all data needed
*********************************************************************/
// get the region of users' address
$user_region = get_user_region(user_id());

// get user data
$user = get_user(user_id());

// get users' company address
$address = get_address($user["address"]);


//get country access
$allowed_countries_filter = '';
$allowed_countries = get_country_access(user_id());
if(count($allowed_countries) > 0) {
	$allowed_countries_filter = " or posaddress_country in (" . implode(', ' , $allowed_countries) . ")";
}

if(!param("shop_address_country"))
{
	register_param("shop_address_country");
	param("shop_address_country", $address["country"]);

	$param_country_is_submitted = false;
}
else
{
	$param_country_is_submitted = true;
}

// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ".
                 "order by country_name";


//project legal types
$p_cost_types = array();

if($address["client_type"] == 1) // client is agent, only franchisee as option
{
    $sql = "select * from project_costtypes where project_costtype_id IN (2,6)";
}
else
{
    $sql = "select * from project_costtypes where project_costtype_id  IN (1, 2, 6)";
}
$res = mysql_query($sql);
while ($row = mysql_fetch_assoc($res))
{
    $p_cost_types[$row["project_costtype_id"]] = $row["project_costtype_text"];
}


if(param("project_cost_type"))
{
	
	if(param("project_cost_type") == 1)
	{
		$sql_pos_types = "select postype_id, postype_name ".
						 "from postypes ".
						 "where postype_id IN (1, 2, 3) " . 
						 " order by postype_name ";
	}
	elseif(param("project_cost_type") == 2)
	{
		$sql_pos_types = "select postype_id, postype_name ".
						 "from postypes ".
						 "where postype_id IN (1, 2, 3) " . 
						 " order by postype_name ";
	}
	elseif(param("project_cost_type") == 6)
	{
		$sql_pos_types = "select postype_id, postype_name ".
						 "from postypes ".
						 "where postype_id IN (2, 4) " . 
						 " order by postype_name ";
	}
}
else
{
	$sql_pos_types = "select postype_id, postype_name ".
					 "from postypes ".
					 "where postype_id = 0 " . 
					 " order by postype_name ";
}


if(param("project_postype"))
{
	$sql_subclasses = "select possubclass_id, possubclass_name " .
		              "from postype_subclasses " .
		              "left join possubclasses on possubclass_id = postype_subclass_subclass " .
		              "left join product_line_pos_types on  product_line_pos_type_id = postype_subclass_pl_pos_type " . 
		              "where  product_line_pos_type_pos_type = " . param("project_postype") .
		              " and product_line_pos_type_product_line is null " .
	                  " order by possubclass_name";
}
else
{
	$sql_subclasses = "select possubclass_id, possubclass_name " .
		              "from postype_subclasses " .
		              "left join possubclasses on possubclass_id = postype_subclass_subclass " .
		              "left join product_line_pos_types on  product_line_pos_type_id = postype_subclass_pl_pos_type " . 
		              "where  product_line_pos_type_pos_type = 0 " .
		              " and product_line_pos_type_product_line =  0" .
	                  " order by possubclass_name";
}


if(param("project_cost_type"))
{
	if(param("project_cost_type") == 1 or param("project_cost_type") == 3 or param("project_cost_type") == 4) // Corporate, joint venture or cooperation 3rd party
	{
		$sql_project_kinds = "select projectkind_id, projectkind_name " . 
			                 "from projectkinds " . 
			                 "where projectkind_id in (1,2,3,4,5,6) ";
	}
	else
	{
		$sql_project_kinds = "select projectkind_id, projectkind_name " . 
			                 "from projectkinds " . 
			                 "where projectkind_id in (1,2, 6)";
	}
}
else
{
	$sql_project_kinds = "select projectkind_id, projectkind_name " . 
			                 "from projectkinds " . 
			                 "where projectkind_id > 0 and projectkind_id < 3";
}

$sql_product_lines = 'select product_line_id, product_line_name 
							from product_lines 
							where product_line_clients = 1 
							and product_line_budget = 1 
							order by product_line_name';



if(param("project_kind") == 1
   or param("project_kind") == 6) // new project or relocation
{
	$sql_posaddresses = "select posaddress_id, concat(country_name, ' ', posaddress_place,  ', ', posaddress_name) as pos " .
						"from posaddresses " .
						"left join countries on country_id = posaddress_country " . 
						" where (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00')   " . 
						" and (posaddress_client_id = " .  $user["address"] . $allowed_countries_filter . ") ". 
						" order by country_name, posaddress_place";
}
elseif(param("project_kind") == 2) // renovation
{
	$sql_posaddresses = "select posaddress_id, concat(country_name, ' ', posaddress_place,  ', ', posaddress_name) as pos " .
						"from posaddresses " .
						"left join countries on country_id = posaddress_country " . 
						" where (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') " . 
						" and posaddress_ownertype = " . param("project_cost_type") . " and (posaddress_client_id = " .  $user["address"] . $allowed_countries_filter . ") ".
		                " and posaddress_store_postype = " . dbquote(param('project_postype')) . 
						" order by country_name, posaddress_place";
}
elseif(param("project_kind") == 3 ) // takover/renovation 
{
	$sql_posaddresses = "select posaddress_id, concat(country_name, ' ', posaddress_place,  ', ', posaddress_name) as pos " .
						"from posaddresses " .
						"left join countries on country_id = posaddress_country " . 
						" where (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00')   " . 
						" and posaddress_ownertype in (2, 6) and (posaddress_client_id = " .  $user["address"] . $allowed_countries_filter . ") " . 
		                " and posaddress_store_postype = " . dbquote(param('project_postype')) . 
						" order by country_name, posaddress_place";
}
elseif(param("project_kind") == 4) // take over
{
	$sql_posaddresses = "select posaddress_id, concat(country_name, ' ', posaddress_place,  ', ', posaddress_name) as pos " .
						"from posaddresses " .
						"left join countries on country_id = posaddress_country " . 
						" where (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00')   " . 
						" and posaddress_ownertype in (2,6) and (posaddress_client_id = " .  $user["address"] . $allowed_countries_filter . ") " . 
						" order by country_name, posaddress_place";
}
elseif(param("project_kind") == 5) // lease renewal
{
	$sql_posaddresses = "select posaddress_id, concat(country_name, ' ', posaddress_place,  ', ', posaddress_name) as pos " .
						"from posaddresses " .
						"left join countries on country_id = posaddress_country " . 
						" where (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00')   " . 
						" and posaddress_ownertype = 1 and (posaddress_client_id = " .  $user["address"] . $allowed_countries_filter . ") ".
						" order by country_name, posaddress_place";
}

$poslocation = array();

$poslocation_product_line_name = '';
$poslocation_postype_name = '';

if(param("posaddress_id"))
{
	
	$sql = "select * ".
			   "from posaddresses ".
			   "left join countries on country_id = posaddress_country " .
			   "left join addresses on address_id = posaddress_client_id " .
		       "left join product_lines on product_line_id = posaddress_store_furniture " . 
		       "left join postypes on postype_id = posaddress_store_postype " . 
			   "where posaddress_id  = " . dbquote(param("posaddress_id"));


	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$poslocation = $row;

		$poslocation_product_line_name = $row['product_line_name'];
		$poslocation_postype_name = $row['postype_name'];

	}
}


if(isset($_SESSION["new_project_step_0"]["cid"]))
{
	$shop_configurator_product_line = 0;
    $shop_configurator_product_name = "";
	$shop_configurator_pos_type = 3;
    $shop_configurator_pos_type_name = "Kiosk";
	//get product line
	$sql = "select category_product_line, product_line_name " . 
		   "from categories " . 
		   "left join product_lines on product_line_id = category_product_line " . 
		   "where category_id = " . $_SESSION["new_project_step_0"]["cid"];
	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$shop_configurator_product_line = $row["category_product_line"];
		$shop_configurator_product_name = $row["product_line_name"];
	}
}

$can_add_new_province = false;
if(param("shop_address_country"))
{
	$sql_c = "select country_provinces_complete " . 
		     "from countries " . 
		     "where country_id = " . param("shop_address_country");
	
	$res = mysql_query($sql_c) or dberror($sql_c);
	$row = mysql_fetch_assoc($res);

	if ($row["country_provinces_complete"] == 0)
	{
		$can_add_new_province = true;
	}
}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("", "project");

/*
if($address["client_type"] == 1) // client is agent, only franchisee as option
{
	 $form->add_hidden("project_cost_type", 2);
	 $form->add_label("pct", "Project Legal Type", 0, "Franchisee");
}
else
{
	$form->add_list("project_cost_type", "Project Legal Type*", $p_cost_types, NOTNULL  | SUBMIT, 0, 1, "ltype");
}
*/

$form->add_list("project_cost_type", "Project Legal Type*", $p_cost_types, NOTNULL  | SUBMIT, 0, 1, "ltype");

$form->add_list("project_kind", "Project Kind*", $sql_project_kinds, NOTNULL  | SUBMIT, 0, 1, "pkind");
$form->add_hidden("old_project_kind");

if(isset($_SESSION["new_project_step_0"]["cid"]))
{
	$form->add_hidden("product_line", $shop_configurator_product_line);
	$form->add_hidden("project_postype", $shop_configurator_pos_type);
	$form->add_hidden("project_pos_subclass");
	$form->add_label("product_line_name", "Product Line", 0, $shop_configurator_product_name);
	$form->add_label("pos_type_name","POS Type", 0, $shop_configurator_pos_type_name);
}
else
{
	if(param("project_kind") == 2 or param("project_kind") == 3) // renovation or tekaover/renovation
	{
		$form->add_hidden("product_line", 0);
		$form->add_list("product_line", "New Product Line*", $sql_product_lines, NOTNULL  | SUBMIT);
		$form->add_list("project_postype", "POS Type*", $sql_pos_types, NOTNULL  | SUBMIT, 0, 1, "ptype");
		$form->add_list("project_pos_subclass", "New POS Type Subclass", $sql_subclasses, 0, 0, 1, "sclass");
	}
	elseif(param("project_kind") == 4) // take over
	{
		if(array_key_exists('posaddress_store_furniture', $poslocation)) {
			$form->add_hidden("product_line", $poslocation['posaddress_store_furniture']);
			$form->add_hidden("project_postype", $poslocation['posaddress_store_postype']);
			$form->add_hidden("project_pos_subclass", $poslocation['posaddress_store_subclass']);	
		}
		else
		{
			$form->add_hidden("product_line");
			$form->add_hidden("project_postype");
			$form->add_hidden("project_pos_subclass");	
		}

		$form->add_label("pln", "Product Line", 0, $poslocation_product_line_name);
		$form->add_label("ptn","POS Type", 0, $poslocation_postype_name);
	}
	elseif(param("project_kind") == 5) // lease renewal
	{
		
		if(array_key_exists('posaddress_store_furniture', $poslocation)) {
			$form->add_hidden("product_line", $poslocation['posaddress_store_furniture']);
			$form->add_hidden("project_postype", $poslocation['posaddress_store_postype']);
			$form->add_hidden("project_pos_subclass", $poslocation['posaddress_store_subclass']);	
		}
		else
		{
			$form->add_hidden("product_line");
			$form->add_hidden("project_postype");
			$form->add_hidden("project_pos_subclass");	
		}

		$form->add_label("pln", "Product Line", 0, $poslocation_product_line_name);
		$form->add_label("ptn","POS Type", 0, $poslocation_postype_name);

	}
	else
	{
		$form->add_hidden("product_line", 0);
		$form->add_list("product_line", "Product Line*", $sql_product_lines, NOTNULL  | SUBMIT);
		$form->add_list("project_postype", "POS Type*", $sql_pos_types, NOTNULL  | SUBMIT, 0, 1, "ptype");
		$form->add_list("project_pos_subclass", "POS Type Subclass", $sql_subclasses, 0, 0, 1, "sclass");
	}
}

if(param("project_kind"))
{
	if(param("project_kind") == 1)
	{
		$form->add_section(" ");
		$form->add_section("Relocation Info");
        $form->add_comment("Please indicate if this new project is a relocation of an existing POS within the same mall or city.");
		$form->add_checkbox("project_is_relocation_project", "Yes the POS is relocated", "", "", "Relocation");

		$form->add_list("project_relocated_posaddress_id", "POS being relocated", $sql_posaddresses, SUBMIT);
	}
	elseif(param("project_kind") == 6)
	{
		$form->add_section(" ");
		$form->add_section("Relocation Info");
		$form->add_hidden("project_is_relocation_project", 1);
		$form->add_comment("Please indicate the POS being relocated.");
		$form->add_list("project_relocated_posaddress_id", "POS being relocated", $sql_posaddresses, SUBMIT | NOTNULL);
	}
	else
	{
		$form->add_hidden("project_is_relocation_project", 0);
		$form->add_hidden("project_relocated_posaddress_id", 0);
	}

	$form->add_section(" ");
	$form->add_section("POS Location");

	if(param("project_kind") == 2 and (param("project_cost_type") == 1 or param("project_cost_type") == 3 or param("project_cost_type") == 4)) // Renovation of a corporate store, joint venture or cooperation 3rd party
	{
		$form->add_list("posaddress_id", "POS Address*", $sql_posaddresses, NOTNULL | SUBMIT);
	}
	elseif((param("project_kind") == 3 or param("project_kind") == 4) and (param("project_cost_type") == 1 or param("project_cost_type") == 3 or param("project_cost_type") == 4)) // Take Over/Renovation of a corporate store, joint venuture or cooperation 3rd party
	{
		$form->error("In case the POS you want to take over does not exist in our records <br />(POS address selection below) you have to choose 'New POS' <br />as project kind instead of 'Take over'.");
		$form->add_list("posaddress_id", "POS Address*", $sql_posaddresses, NOTNULL | SUBMIT);
	}
	elseif(param("project_kind") == 5 and param("project_cost_type") == 1) //Lease Renewal
	{
		$form->error("In case the POS you want to take over does not exist in our records <br />(POS address selection below) you have to choose 'New POS' <br />as project kind instead of 'Lease Renewal'.");
		$form->add_list("posaddress_id", "POS Address*", $sql_posaddresses, NOTNULL | SUBMIT);
	}
	elseif(param("project_kind") == 2 and (param("project_cost_type") == 2 or param("project_cost_type") == 5 or param("project_cost_type") == 6)) // Renovation of a franchisee store or a cooperation brand
	{
		$form->error("In case the POS you want to renovate does not exist in our records <br />(POS address selection below) you have to choose 'New POS' <br />as project kind instead of 'Renovation'.");
		$form->add_list("posaddress_id", "POS Address*", $sql_posaddresses, NOTNULL | SUBMIT);
	}
	else
	{
		$form->add_hidden("posaddress_id");
	}

}
else
{
	$form->add_hidden("posaddress_id");
}

if(param("project_kind") == 1 
	or param("project_kind") == 6 
	or (param("posaddress_id") > 0 
	and param("old_project_kind") == param("project_kind")))
{
	if(param("posaddress_id"))
	{
		$form->add_comment("Please check the correctness of the POS address. <br /><strong><span style=\"color: #FF0000;\">IMPORTANT:</span></strong> Please read the helping tips for Project Names,  <br />Phone and Fax Numbers.");
	}
	else
	{
		$form->add_comment("Please indicate the POS address. <br/><strong><span style=\"color: #FF0000;\">IMPORTANT:</span></strong> Please read the helping tips for Project Names, <br />Phone and Fax Numbers.");
	}

	$form->add_edit("shop_address_company", "Project Name*", NOTNULL, "", TYPE_CHAR, 0, 0, 1, "poslocation");
	$form->add_edit("shop_address_company2", "", 0, "", TYPE_CHAR);
	$form->add_edit("shop_address_address", "Address*", NOTNULL, "", TYPE_CHAR);
	$form->add_edit("shop_address_address2", "", 0, "", TYPE_CHAR);
	
	
	
	if(param("project_kind") == 1 or param("project_kind") == 6)
	{
		$form->add_list("shop_address_country", "Country*", $sql_countries, NOTNULL  | SUBMIT);
	}

	
	if(param("project_kind") == 1 or param("project_kind") == 6)
	{
		if(param("shop_address_country") > 0)
		{	
			
			$places = array();
			$places[999999999] = "Other city not listed below";
			
			$sql_places = "select place_id, place_name from places where place_country = " . param("shop_address_country") . " order by place_name";
			$res = mysql_query($sql_places);

			while ($row = mysql_fetch_assoc($res))
			{
				$places[$row["place_id"]] = $row["place_name"];
			}
			
			
			$form->add_comment("Please select an existing city!");
			$form->add_list("select_shop_address_place", "City Selection*", $places, NOTNULL | SUBMIT);
		}

		if(param("select_shop_address_place") == 999999999)
		{
			$form->add_edit("shop_address_place", "City*", NOTNULL, "", TYPE_CHAR, 20, 0, 1, "city");
		}
		else
		{
			
			$form->add_edit("shop_address_place", "City*", DISABLED | NOTNULL, "", TYPE_CHAR, 20);
		}
		
	}
	else
	{
		$form->add_hidden("shop_address_place", "City");
		$form->add_hidden("shop_address_place_id");
		$form->add_hidden("shop_address_country", "Country");

		$form->add_label("label_shop_address_place", "City");
		$form->add_label("label_shop_address_country", "Country");
	}

	$form->add_edit("shop_address_zip", "ZIP*", NOTNULL, "", TYPE_CHAR, 20);

    
	$provinces = array();
	if($can_add_new_province == true)
	{
		$provinces[999999999] = "Other province not listed below";
	}
	
	if(param("project_kind") == 1 or param("project_kind") == 6) //new project, relocation
	{
		$sql_provinces = "select province_id, province_canton from provinces where province_country = " . dbquote(param("shop_address_country")) . " order by province_canton";
	}
	else
	{
		$sql_provinces = "select province_id, province_canton from provinces where province_country = " . dbquote($poslocation["posaddress_country"]) . " order by province_canton";
	}
	$res = mysql_query($sql_provinces);
	while ($row = mysql_fetch_assoc($res))
	{
		$provinces[$row["province_id"]] = $row["province_canton"];
	}
	
	
	$form->add_comment("Please select an existing province!");
	$form->add_list("select_shop_address_province", "Province Selection*", $provinces, NOTNULL | SUBMIT);

	if(param("select_shop_address_province") == 999999999)
	{
		$form->add_edit("shop_address_province", "Province*", NOTNULL, "", TYPE_CHAR, 50, 0, 1, "province");
	}
	else
	{
		
		$form->add_edit("shop_address_province", "Province*", DISABLED | NOTNULL, "", TYPE_CHAR, 50);
	}

	$form->add_edit("shop_address_phone", "Phone", 0, "", TYPE_CHAR, 20, 0, 1, "phone");
	$form->add_edit("shop_address_fax", "Fax", 0, "", TYPE_CHAR, 20, 0, 1, "fax");
	$form->add_edit("shop_address_email", "Email", 0, "", TYPE_CHAR, 100);
	//$form->add_edit("shop_address_contact_name", "Contact", 0, "", TYPE_CHAR, 100);
	
	
	
	if(param("shop_address_country") or param("posaddress_id") > 0)
	{
		$form->add_section(" ");
		$form->add_section("Google Map Coordinates");
		$form->add_comment("Please verify that the map information is correct!");
		$form->add_label("GM", "Google Map", RENDER_HTML);
		$form->add_edit("posaddress_google_lat", "Latitude", 0 | DISABLED);
		$form->add_edit("posaddress_google_long", "Longitude", 0 | DISABLED);
		$form->add_hidden("posaddress_google_precision", 1);
		//$form->add_checkbox("gmcheck", "",0, NOTNULL, "Confirmation");
		$form->add_label("gmcheck", "", 0);
	}
	else
	{
		$form->add_hidden("GM", "Google Map", RENDER_HTML);
		$form->add_hidden("posaddress_google_lat", "", 0);
		$form->add_hidden("posaddress_google_long", "", 0);
		$form->add_hidden("posaddress_google_precision", 1);
		$form->add_hidden("gmcheck", "", 0);
	}
}
else
{
	$form->add_hidden("shop_address_company");
	$form->add_hidden("shop_address_company2");
	$form->add_hidden("shop_address_address");
	$form->add_hidden("shop_address_address2");
	$form->add_hidden("shop_address_zip");
	$form->add_hidden("shop_address_place");
	$form->add_hidden("shop_address_place_id");
	$form->add_hidden("shop_address_country");
	$form->add_hidden("shop_address_phone");
	$form->add_hidden("shop_address_fax");
	$form->add_hidden("shop_address_email");
	//$form->add_edit("shop_address_contact_name");
	$form->add_hidden("posaddress_google_lat");
	$form->add_hidden("posaddress_google_long");
	$form->add_hidden("posaddress_google_precision");
	$form->add_hidden("GM");
}

$form->add_section(" ");


if(param("project_kind") == 5) //Lease Negotiaion
{
	$form->add_button("step4", "Proceed to next step");
}
else {
	$form->add_button("step2", "Proceed to next step");
}

if(isset($_SESSION["new_project_step_0"]["cid"]))
{
	$form->add_button("backto_pos_config", "Back to POS Configuration");
}

$form->add_button("reset", "Clear all Data (reset form)");


//$form->add_validation("{gmcheck}", "You have not checked the correctness of the map information!");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$form->populate();
$form->process();

//clean fields from apostrophes
foreach($form->items as $key=>$value)
{
	if(array_key_exists('value', $value)) {
		$form->value($key, str_replace('"', '', $form->value($key)));
	}
}


if ($form->button("backto_pos_config"))
{
	redirect("project_new_00.php?id=" . $_SESSION["new_project_step_0"]["cid"]);
}
elseif($form->button("reset"))
{
	$_SESSION["new_project_step_1"] = array();
	$_SESSION["new_project_step_2"] = array();
	$_SESSION["new_project_step_3"] = array();
	$_SESSION["new_project_step_4"] = array();

	$link = "project_new_01.php";
	redirect($link);
}
elseif ($form->button("step2"))
{
	
	//$form->add_validation("{posaddress_google_lat}", "Google Map Coordinates must be indicated!");
	//$form->add_validation("{posaddress_google_long}", "Google Map Coordinates must be indicated!");

	$error = 0;
	if($form->value("project_is_relocation_project") == 1 
		and $form->value("project_relocated_posaddress_id") == ""
		and $form->value("project_is_relocation_project") == 1)
	{
		$error = 1;
		$form->error("Please indicate the POS to be relocated.");
	}

	if(strlen(preg_replace('/[^0-9a-z_ %\[\]\.\(\)%&-]/s', '', $form->value("shop_address_address"))) < strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("shop_address_address")))
		and strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("shop_address_address"))) > 10)
	{
		$error = 1;
		$form->error("Project names and addresses must contain lower caracters. Do not use capital letters only.");
	}
	elseif(strlen(preg_replace('/[^0-9a-z_ %\[\]\.\(\)%&-]/s', '', $form->value("shop_address_address2"))) < strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("shop_address_address2")))
		and strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("shop_address_address2"))) > 10)
	{
		$error = 1;
		$form->error("Project names and addresses must contain lower caracters. Do not use capital letters only.");
	}

	
	if($error == 0 and $form->validate())
	{
		$_SESSION["new_project_step_1"] = $_POST;

		if($_SESSION["new_project_step_1"]["project_cost_type"] == 2 and $_SESSION["new_project_step_1"]["project_postype"] == 2)
		{
			$_SESSION["new_project_step_1"]["project_cost_type"] = "6";
		}

		
		$link = "project_new_02.php";
		redirect($link);
	}
}
elseif ($form->button("step4"))
{
	
	//$form->add_validation("{posaddress_google_lat}", "Google Map Coordinates must be indicated!");
	//$form->add_validation("{posaddress_google_long}", "Google Map Coordinates must be indicated!");
	
	if($form->validate())
	{
		$_SESSION["new_project_step_1"] = $_POST;
		
		$link = "project_new_04.php";
		redirect($link);
	}
}
elseif($form->button("project_kind"))
{
	$form->value("posaddress_id", 0);
	$form->value("shop_address_company", "");
	$form->value("shop_address_company2", "");
	$form->value("shop_address_address", "");
	$form->value("shop_address_address2", "");
	$form->value("shop_address_zip", "");
	$form->value("shop_address_place", "");
	//$form->value("shop_address_place_id", "");

	if(param("project_kind") == 1 or param("project_kind") == 6) // new POS, relocation
	{
		$form->value("shop_address_country", $address["country"]);
	}
	else {
		$form->value("shop_address_country", 0);
	}
	
	if(isset($form->items["shop_address_province"]))
	{
		$form->value("shop_address_province", "");
		$form->value("select_shop_address_province", "");
	}
	
	if(isset($form->items["label_shop_address_place"]))
	{
		$form->value("label_shop_address_place", "");
		$form->value("label_shop_address_country", "");
	}
	$form->value("shop_address_phone", "");
	$form->value("shop_address_fax", "");
	$form->value("shop_address_email", "");
	//$form->value("shop_address_contact_name", "");
	$form->value("posaddress_google_lat", "");
	$form->value("posaddress_google_long", "");
	$form->value("posaddress_google_precision", 0);
	$form->value("GM", "");
	$form->value("old_project_kind", $form->value("project_kind"));

	if(isset($form->items["pln"]))
	{
		$form->value("pln", "");
		$form->value("ptn", "");
	}
	
}
elseif($form->button("project_cost_type"))
{
	$_SESSION["new_project_step_1"] = array();
	$link = 'project_new_01.php?project_cost_type=' . $form->value("project_cost_type");
	redirect($link);
}
elseif($form->button("posaddress_id"))
{
	if(array_key_exists('posaddress_name', $poslocation))
	{
		$form->value("shop_address_company", $poslocation["posaddress_name"]);
		$form->value("shop_address_company2", "", 0, "");
		$form->value("shop_address_address", $poslocation["posaddress_address"]);
		$form->value("shop_address_address2", $poslocation["posaddress_address2"]);
		$form->value("shop_address_zip", $poslocation["posaddress_zip"]);
		$form->value("shop_address_place", $poslocation["posaddress_place"]);
		$form->value("shop_address_place_id", $poslocation["posaddress_place_id"]);
		$form->value("shop_address_country", $poslocation["posaddress_country"]);

		$form->value("label_shop_address_place", $poslocation["posaddress_place"]);
		$form->value("label_shop_address_country", $poslocation["country_name"]);

		$form->value("shop_address_phone", $poslocation["posaddress_phone"]);
		$form->value("shop_address_fax", $poslocation["posaddress_fax"]);
		$form->value("shop_address_email", $poslocation["posaddress_email"]);
		//$form->value("shop_address_contact_name", $poslocation["posaddress_contact_name"]);
		$form->value("posaddress_google_lat",  $poslocation["posaddress_google_lat"]);
		$form->value("posaddress_google_long",  $poslocation["posaddress_google_long"]);
		$form->value("posaddress_google_precision",  $poslocation["posaddress_google_precision"]);

		if($poslocation["posaddress_country"] and $poslocation["posaddress_place"] and $poslocation["posaddress_address"])
		{
			$googlemaplink = "Click <a href='javascript:popup(\"/pos/project_new_map.php?id=" . $form->value("posaddress_id") . "&c=" . $poslocation["posaddress_country"] . "&p=" . $poslocation["posaddress_place"] . "&a=" . str_replace("'", "\'",$poslocation["posaddress_address"]) . "\", 700,650)'>here</a> to verify the geografical position of the POS.";
		}
		elseif($poslocation["posaddress_country"] and $poslocation["posaddress_place"])
		{
			$googlemaplink = "Click <a href='javascript:popup(\"/pos/project_new_map.php?id=" . $form->value("posaddress_id") . "&c=" . $poslocation["posaddress_country"] . "&p=" . $poslocation["posaddress_place"] . "&a=\", 700,650)'>here</a> to verify the geografical position of the POS.";
		}
		elseif($poslocation["posaddress_country"])
		{
			$googlemaplink = "Click <a href='javascript:popup(\"/pos/project_new_map.php?id=" . $form->value("posaddress_id") . "&c=" . $poslocation["posaddress_country"] . "&p=&a=\", 700,650)'>here</a> to verify the geografical position of the POS.";
		}

		$sql = "select place_province, province_canton " .
			   "from places " .
			   "left join provinces on province_id = place_province " . 
			   "where place_id = " . dbquote($poslocation["posaddress_place_id"]);
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$form->value("select_shop_address_province",  $row["place_province"]);
			$form->value("shop_address_province", $row["province_canton"]);
		}

		if($form->value("project_kind") == 4 or $form->value("project_kind") == 5) // take over, lease negotioation
		{
			$form->value("product_line", $poslocation["posaddress_store_furniture"]);
			$form->value("project_postype", $poslocation["posaddress_store_postype"]);
			$form->value("project_pos_subclass", $poslocation["posaddress_store_subclass"]);

			$form->value("pln", $poslocation_product_line_name);
			$form->value("ptn", $poslocation_postype_name);

		}
	}

	
}
elseif($form->button("project_relocated_posaddress_id"))
{
	$sql = "select * ".
			   "from posaddresses ".
			   "left join countries on country_id = posaddress_country " .
			   "left join addresses on address_id = posaddress_client_id " .
		       "left join product_lines on product_line_id = posaddress_store_furniture " . 
		       "left join postypes on postype_id = posaddress_store_postype " . 
			   "where posaddress_id  = " . dbquote(param("project_relocated_posaddress_id"));


	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		
		$form->value("shop_address_country", $row["posaddress_country"]);
		$form->value("shop_address_email", $row["posaddress_email"]);
		$form->value("posaddress_google_lat",  $row["posaddress_google_lat"]);
		$form->value("posaddress_google_long",  $row["posaddress_google_long"]);
		$form->value("posaddress_google_precision",  $row["posaddress_google_precision"]);
	}
	
}
elseif($form->button("shop_address_country"))
{
	$form->value("shop_address_place",  "");
	$form->value("shop_address_zip",  "");
	$form->value("shop_address_province", "");
	$form->value("select_shop_address_province", "");
}
elseif($form->button("select_shop_address_place"))
{
	if($form->value("select_shop_address_place") == 999999999 or $form->value("select_shop_address_place") == 0)
	{
		$form->value("shop_address_place",  "");
		$form->value("shop_address_zip",  "");
		$form->value("shop_address_province", "");
		$form->value("select_shop_address_province", "");
	}
	else
	{
		$sql = "select place_name from places where place_id = " . dbquote($form->value("select_shop_address_place"));
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$form->value("shop_address_place",  $row["place_name"]);
			$form->value("shop_address_zip",  "");
		}
		$sql = "select place_province, province_canton " .
			   "from places " .
			   "left join provinces on province_id = place_province " . 
			   "where place_id = " . dbquote(param("select_shop_address_place"));
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$form->value("select_shop_address_province",  $row["place_province"]);
			$form->value("shop_address_province", $row["province_canton"]);
		}
	}
}
elseif($form->button("select_shop_address_province"))
{
	if($form->value("select_shop_address_province") == 999999999 or $form->value("select_shop_address_province") == 0)
	{
		$form->value("shop_address_province", "");
	}
	else
	{
		$sql = "select province_canton " .
			   "from provinces " .
			   "where province_id = " . dbquote(param("select_shop_address_province"));
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$form->value("shop_address_province", $row["province_canton"]);
		}
	}
}
if($form->button("project_kind") and isset($_SESSION["new_project_step_1"]["project_kind"]) and $_SESSION["new_project_step_1"]["project_kind"] > 0)
{
	unset($_SESSION["new_project_step_2"]);
	unset($_SESSION["new_project_step_3"]);
	unset($_SESSION["new_project_step_4"]);
	unset($_SESSION["new_project_step_5"]);
}


if($form->value("shop_address_country") and $form->value("shop_address_place") and $form->value("shop_address_address"))
{
	if($form->value("posaddress_id") > 0)
	{
		$googlemaplink = "Click <a href=\"javascript:popup('/pos/project_new_map.php?id=" . $form->value("posaddress_id") . "&c=" . $form->value("shop_address_country") . "&p=" . str_replace("'", "\'", $form->value("shop_address_place")) . "&a=" . str_replace("'", "\'", $form->value("shop_address_address")) . "', 700,650);\">here</a> to verify the geografical position of the POS.";
	}
	else
	{
		$googlemaplink = "Click <a href=\"javascript:popup('/pos/project_new_map.php?c=" . $form->value("shop_address_country") . "&p=" . str_replace("'", "\'", $form->value("shop_address_place")) . "&a=" . str_replace("'", "\'",$form->value("shop_address_address")) . "', 700,650);\">here</a> to choose the geografical position of the POS.";
	}
}
elseif($form->value("shop_address_country") and $form->value("shop_address_place"))
{
	if($form->value("posaddress_id") > 0)
	{
		$googlemaplink = "Click <a href=\"javascript:popup('/pos/project_new_map.php?id=" . $form->value("posaddress_id") . "&c=" . $form->value("shop_address_country") . "&p=" . str_replace("'", "\'", $form->value("shop_address_place")) . "&a=', 700,650);\">here</a> to verify the geografical position of the POS.";
	}
	else
	{
		$googlemaplink = "Click <a href=\"javascript:popup('/pos/project_new_map.php?c=" . $form->value("shop_address_country") . "&p=" . str_replace("'", "\'", $form->value("shop_address_place")) . "&a=', 700,650);\">here</a> to choose the geografical position of the POS.";
	}
}
elseif($form->value("shop_address_country") and $param_country_is_submitted == true)
{
	if($form->value("posaddress_id") > 0)
	{
		$googlemaplink = "Click <a href=\"javascript:popup('/pos/project_new_map.php?id=" . $form->value("posaddress_id") . "&c=" . $form->value("shop_address_country") . "&p=&a=', 700,650);\">here</a> to verify the geografical position of the POS.";
	}
	elseif(param("project_kind") == 2 and !param("posaddress_id"))
	{
		$googlemaplink = "";
	}
	else
	{
		$googlemaplink = "Click <a href=\"javascript:popup('/pos/project_new_map.php?c=" . $form->value("shop_address_country") . "&p=&a=', 700,650);\">here</a> to choose the geografical position of the POS.";
	}

}
elseif(!isset($googlemaplink))
{
	$googlemaplink = "";
}

$form->value("GM", $googlemaplink);
   
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

$page->register_action('home', 'Home', "welcome.php");

$page->header();
$page->title("New Project");

echo "<div>";
echo "<img class='stepcounter' src='/pictures/numbers/01_on.gif' alt=\"\" />";
echo "<span class='step_active'>Basic Project Data</span>";
echo "<img class='stepcounter' src='/pictures/numbers/02_off.gif' alt=\"\" />";
echo "<span class='step_inactive'>Addresses</span>";
echo "<img class='stepcounter' src='/pictures/numbers/03_off.gif' alt=\"\" />";
echo "<span class='step_inactive'>POS Information</span>";
echo "<img class='stepcounter' src='/pictures/numbers/04_off.gif' alt=\"\" />";
echo "<span class='step_inactive'>Environment and Neighbourhood</span>";
echo "<img class='stepcounter' src='/pictures/numbers/05_off.gif' alt=\"\" />";
echo "<span class='step_inactive'>Delivery</span>";
echo "<br /><br /><br /></div>";


$form->render();

?>

<div id="ltype" style="display:none;">
    <ul class="tiplist">
	<li><strong>Corporate</strong><br />POS is fully owned and operated by SG country</li>
	<li><strong>Franchisee</strong><br />POS is fully owned and operated by a third party</li>
	<li><strong>Other</strong><br />For non corporate SIS, operated by third party</li>
	</ul>
</div> 

<div id="pkind" style="display:none;">
    <ul class="tiplist">
	<li><strong>New POS</strong><br />If you are building a new POS</li>
	<li><strong>Renovation/Rebuilding</strong><br />If you are renovating an existing POS</li>
	<?php
	if(param("project_cost_type") == 1 or param("project_cost_type") == 3 or param("project_cost_type") == 4) // Corporate Store, joint venture or cooperation 3rd party
	{
	?>
		<li><strong>Take over/Renovation</strong><br />If you are taking over and renovating an existing POS</li>
		<li><strong>Take over</strong><br />If you are taking over an existing POS without renovating it</li>
		<li><strong>Lease Renewal</strong><br />If you only are going to renew the lease contract</li>
	<?php
	}
	?>
	</ul>
</div>


<div id="ptype" style="display:none;">
    <ul class="tiplist">
	<li><strong>Store</strong><br />Closed area with own entrance</li>
	<li><strong>SIS</strong><br />Shop in Shop: open space in Department store</li>
	<li><strong>Kiosk</strong><br />Freestanding, closed space in Airport, Train station, Shopping Mall</li>
	</ul>
</div> 

<div id="sclass" style="display:none;">
    Choose only if applicable, otherwise leave empty
</div> 


<div id="poslocation" style="display:none;">
    The POS location will be published on <?php echo BRAND_WEBSITE ;?>. Therefore it is important to follow a naming convention. The reason is that customers can easily find the POS Location. So we need the name of the shopping mall or the street's name or the area's name or the airpots'name AND the city. Please enter the projects's name as follows:
	<ul class="tiplist">
	<li><strong>Shopping Mall Example</strong>:<br />Shopping Mall Carabou, Madrid</li>
	<li><strong>Airport Example</strong><br />Int. Airport, Pantown</li>
	<li><strong>Area Example</strong><br />Westside, Pantown</li>
	<li><strong>Street Example</strong><br />Nassaustreet, Pantown</li>
	</ul>
</div> 

<div id="phone" style="display:none;">
    Please indicate the phone number according to international conventions like:<br />
	<strong>+41 61 926 67 00</strong><br />use only the 'plus sign', 'blank' and 'numbers'.
</div> 

<div id="fax" style="display:none;">
    Please indicate the fax umber according to international conventions like:<br />
	<strong>+41 61 926 67 00</strong><br />use only the 'plus sign', 'blank' and 'numbers'.
</div> 

<div id="city" style="display:none;">
    Please indicate the new city's name in English!
</div> 

<div id="province" style="display:none;">
    Please indicate the new province's name in English!
</div> 

<?php

$page->footer();


?>