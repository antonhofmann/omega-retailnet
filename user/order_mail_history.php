<?php
/********************************************************************

    order_mail_history.php

    View the history of mails of an order

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-11-20
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-16
    Version:        1.0.1


    Copyright (c) 2002, Swatch AG, All Rights Reserved.


*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/order_functions.php";


check_access("can_view_history_in_orders");


/********************************************************************
    prepare all data needed
*********************************************************************/
// read order details
$order = get_order(param("oid"));


// get company's address
$client_address = get_address($order["order_client_address"]);




// get data for history list
$sql_order_mails = "select distinct " .
                   "order_mails.date_created as date_created, ".
                   "order_state_code, order_mail_text " .
                   "from order_mails " .
                   "left join order_states on order_mail_order_state = order_state_id ";

$list_filter = "order_mail_order = " . param("oid");


$sql = "select order_mails.date_created as date_created, ".
       "    concat(users.user_name, ' ', users.user_firstname) as sender, " .
       "    concat(users1.user_name, ' ', users1.user_firstname) as reciepient " .
       "from order_mails ".
       "left join users on order_mail_from_user = users.user_id " .
       "left join users as users1 on order_mail_user = users1.user_id ";


//reduce mails to several reciepinets to one line
$users = array();

$sql = $sql . " where " . $list_filter;

$res = mysql_query($sql) or dberror($sql);

while($row = mysql_fetch_assoc($res))
{
    if(isset($users[$row["date_created"]]))
    {
        $users[$row["date_created"]] = $users[$row["date_created"]] . "\n" .  $row["reciepient"];
    }
    else
    {
        $users[$row["date_created"]] = "<span class='highlite_value'>" . $row["sender"] . "</span>\n" .  $row["reciepient"];
    }

}



/********************************************************************
    Create Form
*********************************************************************/ 


$form = new Form("order_mails", "mail history", 640);
$form->add_hidden("oid", param("oid"));


$form->add_section("Order");


require_once "include/order_head_small.php";




/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();




/********************************************************************
    Create History of steps
*********************************************************************/ 
$list = new ListView($sql_order_mails, LIST_HAS_SEPARATOR | LIST_HAS_HEADER);


$list->set_title("History: Mails Sent");
$list->set_entity("order_mail");
$list->set_filter($list_filter);
$list->set_order("order_mails.date_created DESC");


$list->add_column("date_created", "Date", "", "", "", COLUMN_NO_WRAP);
$list->add_column("order_state_code", "Step");
$list->add_text_column("users", "Sender\nRecepients", COLUMN_NO_WRAP | COLUMN_UNDERSTAND_HTML | COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $users);
$list->add_column("order_mail_text", "Mailtext", "", "", "", COLUMN_BREAK);




/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 


$list->populate();
$list->process();




/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");


require "include/order_page_actions.php";


$page->header();
$page->title("Mail History");
$form->render();
$list->render();
$page->footer();


?>