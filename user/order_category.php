<?php

/********************************************************************

    order_category.php

    "New order" catalog browser.

    Created by:     Oliver Hofer (oliver.hofer@pageagent.com)
    Date created:   2002-08-20
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-08-04
    Version:        1.0.8

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/order_functions.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_view_orders");

register_param('cid');

$cid = param('cid');
if (!$cid)
{
    $cid = id();
}


$searchterm = '';
if(array_key_exists('searchterm', $_GET) and $_GET['searchterm'] != '') 
{
	$searchterm = $_GET['searchterm'];
}
else
{
	$searchterm = param('searchterm');
}





/********************************************************************
    prepare all data needed
*********************************************************************/

// get the region of users' address
$user_region = get_user_region(user_id());
$roles = get_user_roles(user_id());

// get category name
$category_name = get_category_name($cid);


$sql_a_items = "select addon_id, addon_package_quantity, category_items.category_item_category, " .
               "    addon_min_packages, addon_max_packages, addon_parent, addon_child " . 
               "from addons " .
               "left join category_items on category_items.category_item_item = addons.addon_parent ";


$sql = "select item_id, category_item_id, item_code, item_name ".
       "from category_items " .
       "left join items on category_items.category_item_item=items.item_id " .
       "left join item_regions on item_region_item = item_id ";


$list_filter = "category_item_category = " . $cid . 
               "    and item_visible = 1 " .
               "    and item_active = 1 " .
               "    and item_region_region = " . $user_region;


if($searchterm)
{
	

	$sql = "select DISTINCT item_id, category_item_id, item_code, item_name, concat(product_line_name, ' - ', category_name) as productline_category ".
		   "from category_items " .
		   "left join items on category_items.category_item_item=items.item_id " .
		   "left join categories on category_id = category_item_category " .
		   "left join product_lines on product_line_id = category_product_line " .
		   "left join productline_regions on productline_region_productline =  product_line_id " . 
		   "left join item_regions on item_region_item = item_id ";


	if (count($roles) == 1 and $roles[0] == 4) // user is a client
	{
		$list_filter = "(LOWER(item_code) like '%" . strtolower($searchterm) . "%' OR LOWER(item_name) like '%" .  strtolower($searchterm) . "%' OR LOWER(category_name) like '%" .  strtolower($searchterm) . "%' OR LOWER(product_line_name) like '%" .  strtolower($searchterm) . "%' )" .
					   "    and item_visible = 1 " .
					   "    and item_active = 1 " .
					   "    and category_catalog = 1 " .
					   "    and (category_not_in_use is null or category_not_in_use = 0) " . 
					   "    and product_line_visible = 1 " . 
					   "    and product_line_clients = 1 " .  
					   "    and item_region_region = " . $user_region . 
					   "    and productline_region_region = " . $user_region;
	}
	else
	{
	   
	  $list_filter = "(LOWER(item_code) like '%" . strtolower($searchterm) . "%' OR LOWER(item_name) like '%" .  strtolower($searchterm) . "%' OR LOWER(category_name) like '%" .  strtolower($searchterm) . "%' OR LOWER(product_line_name) like '%" .  strtolower($searchterm) . "%' )" .
					   "    and item_visible = 1 " .
					   "    and item_active = 1 " .
					   "    and category_catalog = 1 " .
					   "    and (category_not_in_use is null or category_not_in_use = 0) " . 
					   "    and product_line_visible = 1 " . 
					   "    and item_region_region = " . $user_region . 
					   "    and productline_region_region = " . $user_region;
	}

	 $sql1 = $sql . ' where ' . $list_filter;

	 $res = mysql_query($sql1);
	 $listfilter = "";
	 $item_list = array();

	 while($row = mysql_fetch_assoc($res))
	 {
		$item_list[$row['item_id']] = $row["item_id"];

		
	 }

	 if(count($item_list) > 0)
	 {
		$list_filter = "item_id IN (" . implode(',', $item_list). ")";
		
		$sql = "select DISTINCT item_id, item_code, item_name ".
		   "from items ";
	 }
}




/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);

$list->set_entity("category_items");
$list->set_filter($list_filter);
$list->set_order("item_code");

if($searchterm) 
{
	//$list->set_group("productline_category");
	//$list->set_order("product_line_name, category_name, item_code");
}
$list->add_hidden('cid' , $cid);
$list->add_hidden('searchterm' , $searchterm);


// access to product-details is restricted


$popup_link = "";
if (has_access("can_view_catalog_detail"))
{
    $popup_link = "popup:catalog_item_view.php?id={item_id}";
}


$list->add_column("item_code", "Code", $popup_link, LIST_FILTER_FREE);
$list->add_column("item_name", "Item", "", LIST_FILTER_FREE);


// intialize quantities: set quantity of all items in this category to 0


$values = array();

if($searchterm) 
{
	$sql_q = $sql . " where item_category=" . $cid . " and items.item_visible=1";
}
else
{
	$sql_q = $sql . " where category_item_category=" . $cid . " and items.item_visible=1";
}


$res = mysql_query($sql_q ) or dberror($sql_q );
while ($row = mysql_fetch_assoc($res))
{
    $values[$row['item_id']] = '';
}


// set number of items in shoping list: Update quantity from items in shoping-list


if (!$list->button("basket_add"))
{
    $sql_basket = "select basket_item_item, basket_item_quantity " .
                  "from basket_items " .
                  "where basket_item_basket= " . get_users_basket();


    $res = mysql_query($sql_basket);
    if ($res)
    {
        while ($row = mysql_fetch_assoc($res))
        {
            $values[$row['basket_item_item']] = $row['basket_item_quantity'];
        }
    }
}


$list->add_edit_column("item_quantity", "Quantity", "6", 0, $values);
$list->add_button(LIST_BUTTON_BACK, "Back");
$list->add_button("basket_add", "Add to Shopping List");
$list->add_button("cancel_order", "Cancel Ordering");


$list->populate();
$list->process();


if ($list->button("basket"))
{
    redirect("basket.php");
}


if ($list->button("basket_add"))
{

	$add_this_items = array();

	/*
    if(param('searchterm') and param('searchterm') != '')
	{
		
		$sql = $sql . ' where ' . $list_filter;

        $res = mysql_query($sql) or dberror($sql);
        $row = mysql_fetch_assoc($res);
        $item_id = $row['item_id'];


        $sql = "select basket_item_quantity " .
               "from basket_items " .
               "where basket_item_basket = " . get_users_basket() .
               "    and basket_item_item = " . $item_id;


        $res = mysql_query($sql) or dberror($sql);
        $row = mysql_fetch_assoc($res);
        if ($row['basket_item_quantity'])
        {
            $value = $row['basket_item_quantity'] + 1.00;
        }
        else
        {
            $value = 1.00;
        }


        $add_this_items[$item_id] = $value;

		
	}
	elseif (param('id'))
    {
        $sql = "select category_item_item " .
               "from category_items " .
               "where category_item_id = " . id();
        
        $res = mysql_query($sql) or dberror($sql);
        $row = mysql_fetch_assoc($res);
        $item_id = $row['category_item_item'];


        $sql = "select basket_item_quantity " .
               "from basket_items " .
               "where basket_item_basket = " . get_users_basket() .
               "    and basket_item_item = " . $item_id;


        $res = mysql_query($sql) or dberror($sql);
        $row = mysql_fetch_assoc($res);
        if ($row['basket_item_quantity'])
        {
            $value = $row['basket_item_quantity'] + 1.00;
			cde();
        }
        else
        {
            $value = 1.00;
        }


        $add_this_items[$item_id] = $value;
    }
	*/

	if (param('id'))
    {
		$sql = "select category_item_item " .
               "from category_items " .
               "where category_item_id = " . id();
        
        $res = mysql_query($sql) or dberror($sql);
        $row = mysql_fetch_assoc($res);
        $item_id = $row['category_item_item'];


        $sql = "select basket_item_quantity " .
               "from basket_items " .
               "where basket_item_basket = " . get_users_basket() .
               "    and basket_item_item = " . $item_id;


        $res = mysql_query($sql) or dberror($sql);
        $row = mysql_fetch_assoc($res);
        if ($row['basket_item_quantity'])
        {
            $value = $row['basket_item_quantity'] + 1.00;
        }
        else
        {
            $value = 1.00;
        }


        $add_this_items[$item_id] = $value;
	}


    foreach($list->values("item_quantity") as $key => $value)
    {
        if (isset($add_this_items[$key]))
        {
            $add_this_items[$key] += $value;
        }
        else
        {
            if ($value)
            {
                $add_this_items[$key] = $value;
            }
        }
    }

	if(param('searchterm') and param('searchterm') != '')
	{
		update_shopping_list($add_this_items, get_users_basket(), 'search_was_performed');
	}
	else
	{
		update_shopping_list($add_this_items, get_users_basket(), $cid);
	}


    // check, if item has addons


    $addon_items = array();
    $parent_items = $add_this_items;


    foreach($parent_items as $key => $value)
    {
        if ($value > 0)
        {
            $addon_items[] = $key;
        }
    }


    if (!empty($addon_items))
    {
        $sql_addons = "select * " .
                      "from addons " .
                      "where addon_parent in (" . join (',' , $addon_items) . ") " .
                      "group by addon_parent"; 


        unset($addon_items);
        $res = mysql_query($sql_addons);
        while ($row = mysql_fetch_assoc($res))
        {
            $addon_items[] = $row['addon_parent'];
        }
    }


    if (!empty($addon_items))
    {


        // add addons to basket


        foreach ($addon_items as $addon_item_key => $addon_item_value)
        {
            $sql = $sql_a_items . "where addon_parent = " . $addon_item_value . " " .
                   "    and category_item_category = " . $cid;


            $res = mysql_query($sql) or dberror($sql);
            while ($row = mysql_fetch_assoc($res))
            {
                $addon_quantity = (isset($parent_items[$row['addon_parent']])) ? 
                                  $parent_items[$row['addon_parent']]: 0 ;


                update_basket_item(get_users_basket(), $row['addon_child'], 
                                   $addon_quantity, 
                                   $row['category_item_category'],
                                   $row['addon_parent']);
            }
         }
    }


    redirect("basket.php");
}


if ($list->button("cancel_order"))
{
	redirect("orders.php");
}




$page = new Page("orders");
if (!basket_is_empty())
{
    $page->register_action('basket', 'Shopping List');
}


$page->register_action('home', 'Home', "welcome.php");


$page->header();
$page->title($category_name);
$list->render();
$page->footer();


?>