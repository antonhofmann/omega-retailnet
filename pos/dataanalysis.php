<?php
/********************************************************************

    dataanalysis.php

    Analyse posorders and show possible error

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_administrate_posindex");

/********************************************************************
    multimple assigned projects
*********************************************************************/

$identical = array();

$sql = "select * from posorders";
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$sql2 = "select count(posorder_id) as num_recs from posorders " . 
		    "where posorder_posaddress = " . dbquote($row["posorder_posaddress"]) . 
		    " and posorder_order = " . dbquote($row["posorder_order"]);
	
	$res2 = mysql_query($sql2) or dberror($sql2);
	$row2 = mysql_fetch_assoc($res2);
	if($row2["num_recs"] > 1)
	{
		$identical[$row["posorder_posaddress"]] = $row2["num_recs"];
	}
}

$sql = "select posaddress_id, if(posaddress_name <> '', posaddress_name, 'n.a.') as posname, " .
       "posaddress_address, posaddress_address2, posaddress_zip, " .
       "    posaddress_place, country_name " .
       "from posaddresses " . 
	   "left join countries on posaddress_country = country_id ";

$filter = "";
foreach($identical as $posaddress_id=>$posorders)
{
	$filter .= "posaddress_id = " . $posaddress_id . " or ";
}

if($filter)
{
	$filter = substr($filter, 0, strlen($filter)-4);
}
else
{
	$filter = "posaddress_id = 0";
}

if(param("remove") == 1) //remove double entries
{
	$sql_d = $sql . " where " . $filter;
	$res_d = mysql_query($sql_d) or dberror($sql_d);
	if ($row_d = mysql_fetch_assoc($res_d))
	{
		$sql_r = "select posorder_id, posorder_order from posorders " . 
			     "where posorder_posaddress = " . dbquote($row_d["posaddress_id"]);

		$order_no = 0;
		$res_r = mysql_query($sql_r) or dberror($sql_r);
		while ($row_r = mysql_fetch_assoc($res_r))
		{
			if($order_no != $row_r["posorder_order"])
			{
				$sql_remove = "delete from posorders where posorder_id <> " . dbquote($row_r["posorder_id"]) .
							  " and posorder_posaddress = " . dbquote($row_d["posaddress_id"]) . 
							  " and posorder_order = " . dbquote($row_r["posorder_order"]);

				$result = mysql_query($sql_remove) or dberror($sql_remove);
				$order_no = $row_r["posorder_order"];
			}
		}
	}
	$identical = array();
	
}

/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);
$list->set_entity("posorders");
$list->set_filter($filter);
$list->set_title("Multiple Identical Projects");

$list->add_column("posname", "POS Name", "posindex_pos.php" , LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("posaddress_address", "Address", "", LIST_FILTER_FREE);
$list->add_column("posaddress_zip", "Zip", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("posaddress_place", "City", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);

$render_list = 0;
if(count($identical) > 0)
{
	$render_list = 1;
	$list->add_button("remove_multiple", "Remove Multiple");
}

$list->populate();
$list->process();

if($list->button("remove_multiple"))
{
	$link = "dataanalysis.php?remove=1";
	redirect($link);
}

/********************************************************************
    same project assigned to different posaddresses
*********************************************************************/

$identical = array();
$render_list2 = 0;

$sql = "select * from posorders";
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$sql2 = "select count(posorder_id) as num_recs from posorders " . 
		    "where posorder_posaddress <> " . dbquote($row["posorder_posaddress"]) . 
		    " and posorder_order = " . dbquote($row["posorder_order"]);
	
	$res2 = mysql_query($sql2) or dberror($sql2);
	$row2 = mysql_fetch_assoc($res2);
	if($row2["num_recs"] > 1)
	{
		$identical[$row["posorder_order"]] = $row2["num_recs"];
	}
	$render_list2 = 1;
}


$sql = "select posaddress_id, if(posaddress_name <> '', posaddress_name, 'n.a.') as posname, " .
       "posaddress_address, posaddress_address2, posaddress_zip, " .
       "    posaddress_place, country_name, order_number, order_type, posorder_id " .
       "from posorders " .
	   "left join posaddresses on posaddress_id = posorder_posaddress  " . 
	   "left join orders on order_id = posorder_order " . 
	   "left join countries on posaddress_country = country_id ";

$filter = "";
foreach($identical as $order_id=>$posorders)
{
	$filter .= "posorder_order = " . dbquote($order_id) . " or ";
}

if($filter)
{
	$filter = substr($filter, 0, strlen($filter)-4);
}
else
{
	$filter = "order_number = '0'";
}


/********************************************************************
    Create List
*********************************************************************/ 
$list2 = new ListView($sql);
$list2->set_entity("posorders");
$list2->set_filter($filter);
$list2->set_title("Identical Project or Catalogue Order Assigned to different POS Addresses");
$list2->set_order("order_number, posname");

$list2->add_column("order_number", "Project", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list2->add_column("posorder_id", "ID", "" , LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list2->add_column("posname", "POS Name", "" , LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list2->add_column("posaddress_address", "Address", "", LIST_FILTER_FREE);
$list2->add_column("posaddress_zip", "Zip", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list2->add_column("posaddress_place", "City", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);


$list2->process();


/********************************************************************
    posorders without posaddress
*********************************************************************/

$posorders = array();

$sql = "select * from posorders " . 
       "left join posaddresses on posaddress_id = posorder_posaddress";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	if($row["posaddress_name"] == "")
	{
		$posorders[] = $row["posorder_id"];
	}
}


$sql = "select posorder_id, posorder_order, posorder_ordernumber " .
       "from posorders ";

$filter = "";
foreach($posorders as $key=>$posorder_id)
{
	$filter .= "posorder_id = " . $posorder_id . " or ";
}

if($filter)
{
	$filter = substr($filter, 0, strlen($filter)-4);
}
else
{
	$filter = "posorder_id = 0";
}


if($filter and param("removeorphans") == 1) //remove orphans
{
	foreach($posorders as $key=>$posorder_id)
	{
		$sql_remove = "delete from posorders where posorder_id = " . $posorder_id;

		$result = mysql_query($sql_remove) or dberror($sql_remove);
	}
	$posorders = array();
}


/********************************************************************
    Create List
*********************************************************************/ 
$list3 = new ListView($sql);
$list3->set_entity("posorders");
$list3->set_filter($filter);
$list3->set_title("POS Orders without a POS Address");
$list3->set_order("posorder_id, posorder_ordernumber");

$list3->add_column("posorder_ordernumber", "Project", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list3->add_column("posorder_id", "ID", "" , LIST_FILTER_FREE,'', COLUMN_NO_WRAP);


if(count($posorders) > 0)
{
	$list3->add_button("remove_orphans", "Remove Orphans");
}

$list3->populate();
$list3->process();

if($list->button("remove_orphans"))
{
	$link = "dataanalysis.php?removeorphans=1";
	redirect($link);
}


$page = new Page("dataanalysis");
$page->header();

$page->title("Results of Data Analysis");
if($render_list == 1)
{
	$list->render();
}
if($render_list2 == 1)
{
	$list2->render();
}

if(count($posorders) > 0)
{
	$list3->render();
}

$page->footer();
?>