<?php
/********************************************************************

    loc_possubclasses.php

    Lists pos subclasses for translations.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");
set_referer("loc_possubclass.php");

//get all translations
$translations = array();
$sql = "select possubclass_id,language_name " . 
       "from possubclasses " . 
       "left join loc_possubclasses on loc_possubclass_possubclass = possubclass_id " . 
	   "left join languages on language_id = loc_possubclass_language " . 
	   "order by language_name";


$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	if(array_key_exists($row["possubclass_id"], $translations))
	{
		$translations[$row["possubclass_id"]] = $translations[$row["possubclass_id"]] . "/" . trim($row["language_name"]);
	}
	else
	{
		$translations[$row["possubclass_id"]] = trim($row["language_name"]);
	}
}	   

$sql = "select possubclass_id, possubclass_name " .
       "from possubclasses ";

$list = new ListView($sql);

$list->set_entity("possubclasses");
$list->set_order("possubclass_name");


$list->add_column("possubclass_name", "POS Type Subclass", "loc_possubclass.php", LIST_FILTER_FREE);
$list->add_text_column("translations", "Translations", COLUMN_UNDERSTAND_HTML, $translations);

$list->process();

$page = new Page("locales");

$page->header();
$page->title("POS Type Subclasses");
$list->render();
$page->footer();
?>
