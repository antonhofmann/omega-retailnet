<?php
/********************************************************************

    dataanalysis_orders_preselect.php

    Preselection of Order List

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_administrate_posindex");
set_referer("posindex.php");

//compose list

$sql2 = "select DISTINCT country_id, country_name " .
       "from posaddresses " . 
	   "left join countries on posaddress_country = country_id " . 
	   "where country_name is not null " . 
	   "order by country_name";

$form = new Form("posaddresses", "posaddress");

$form->add_list("country", "Country",$sql2);

$form->add_button("show_list", "Show List");

$form->populate();
$form->process();


if($form->button("show_list"))
{
	redirect("dataanalysis_orders.php?country=" . $form->value("country"));
}

$page = new Page("posindex");
require "include/pos_page_actions.php";
$page->header();

$page->title("Order Orphans");
$form->render();

?>

	<script type="text/javascript">
		
		document.onkeydown = process_key;
		
		function process_key(e)
		{
		  if( !e ) 
		  {
			if( window.event ) 
			{
			  e = window.event;
			} 
			else 
			{
			  return;
			}
		  }

		  if(e.keyCode==13)
		  {
			  button('show_list');
		  }
		}
	</script>

	<?php


$page->footer();

?>
