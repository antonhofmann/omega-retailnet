<?php
/********************************************************************

    loc_posaddress_posaddress.php

    Creation and mutation of pos locations translations.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");


$posaddress_id = 0;

if(param("posaddress_id"))
{
	$posaddress_id = param("posaddress_id");
}

//get posaddress name
$posaddress_name = "";
$sql = "select * " . 
       "from posaddresses " .
	   "left join countries on country_id = posaddress_country " . 
	   "where posaddress_id = " . $posaddress_id;
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	
	$country_name= $row["country_name"];
	$posaddress_name = $row["posaddress_name"];
	$posaddress_name2 = $row["posaddress_name2"];
	$posaddress_place = $row["posaddress_place"];
	$posaddress_address = $row["posaddress_address"];
	$posaddress_address2 = $row["posaddress_address2"];
	$posaddress_zip = $row["posaddress_zip"];
	$posaddress_phone = $row["posaddress_phone"];
	$posaddress_fax = $row["posaddress_fax"];
	

	$posaddress_country = $row["posaddress_country"];
	$posaddress_place_id = $row["posaddress_place_id"];
}

$form = new Form("loc_posaddresses", "posaddress");


$form->add_section("Basic Data");
$form->add_hidden("posaddress_id", $posaddress_id);
$form->add_hidden("loc_posaddress_posaddress", $posaddress_id);
$form->add_hidden("loc_posaddress_country", $posaddress_country);
$form->add_hidden("loc_posaddress_place_id", $posaddress_place_id);

$form->add_label("posaddress_country", "Country", 0, $country_name);
$form->add_label("posaddress_place", "Place", 0, $posaddress_place);
$form->add_label("posaddress_name", "POS Name", 0, $posaddress_name);
$form->add_label("posaddress_name2", "", 0, $posaddress_name2);
$form->add_label("posaddress_address", "Address", 0, $posaddress_address);
$form->add_label("posaddress_address2", "", 0, $posaddress_address2);
$form->add_label("posaddress_zipplace", "Place", 0, $posaddress_zip . " " . $posaddress_place);
$form->add_label("posaddress_phone", "Phone", 0, $posaddress_phone);
$form->add_label("posaddress_fax", "Fax", 0, $posaddress_fax);

$form->add_section("Translation");
$form->add_comment("If you do not provide a translation then the value from the basic data will be shown.");
$form->add_list("loc_posaddress_language", "Language*", 
    "select language_id, language_name from languages order by language_name", NOTNULL);

$form->add_edit("loc_posaddress_name", "POS Name*", NOTNULL);
$form->add_edit("loc_posaddress_name2", "");
$form->add_edit("loc_posaddress_address", "Address");
$form->add_edit("loc_posaddress_address2", "");
$form->add_edit("loc_posaddress_zip", "");
$form->add_edit("loc_posaddress_phone", "Phone");
$form->add_edit("loc_posaddress_fax", "Fax");

$form->add_button("save", "Save");
$form->add_button("back", "Back");
$form->add_button("delete", "Delete");

$form->populate();
$form->process();

if($form->button("back"))
{
	$link = "loc_posaddress.php?id= " . param("posaddress_id");
	redirect($link);
}
elseif($form->button("save"))
{
	if($form->validate())
	{
		$form->save();
		$link = "loc_posaddress.php?id= " . param("posaddress_id");
		redirect($link);
	}
}
elseif($form->button("delete"))
{
		$sql = "delete from loc_posaddresses where loc_posaddress_id =" . id();
		$result = mysql_query($sql) or dberror($sql);
		$link = "loc_posaddress.php?id= " . param("posaddress_id");
		redirect($link);

}

$page = new Page("locales");
$page->header();
$page->title(id() ? "Edit Translation of POS Location" : "Add Translation for POS Location");
$form->render();
$page->footer();

?>