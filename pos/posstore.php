<?php
/********************************************************************

    posstore.php

    Creation and mutation of posaddress records.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

require_once "include/check_access.php";

if(param("pos_id"))
{
	register_param("pos_id");
	param("id", param("pos_id"));
}
else
{
	register_param("pos_id");
	param("pos_id", id());
}

$pos = array();
if(param("pos_id") > 0)
{
	$pos = get_poslocation(param("pos_id"), "posaddresses");
}

$has_project = update_posdata_from_posorders(param("pos_id"));


//check if user can edit this company
$can_edit = false;
if(has_access("can_edit_posindex"))
{
	$can_edit = true;
}
elseif(has_access("can_edit_his_posindex"))
{
	$can_edit = get_user_edit_permission(user_id(), param("pos_id"));
}


// Build form

$form = new Form("posaddresses", "posaddress");

$form->add_section("Classification");

if($can_edit == true)
{
	if($has_project == false)
	{
		$form->add_list("posaddress_ownertype", "Legal Type*",
			"select project_costtype_id, project_costtype_text from project_costtypes where project_costtype_id IN (1, 2, 6)", NOTNULL);

		$form->add_list("posaddress_store_furniture", "Product Line*",
			"select product_line_id, product_line_name from product_lines where product_line_posindex = 1 order by product_line_name", NOTNULL);

		$form->add_list("posaddress_store_furniture_subclass", "Product Line Subclass",
			"select productline_subclass_id, productline_subclass_name from productline_subclasses where productline_subclass_productline = {posaddress_store_furniture} order by productline_subclass_name", 0);

		$form->add_list("posaddress_store_postype", "POS Type*",
			"select postype_id, postype_name from postypes order by postype_name", NOTNULL);

		$form->add_list("posaddress_store_subclass", "POS Type Subclass",
			"select possubclass_id, possubclass_name from possubclasses order by possubclass_name");
	}
	else
	{
		
		//$form->add_lookup("posaddress_ownertype", "Legal Type", "project_costtypes", "project_costtype_text");
		
		$form->add_list("posaddress_ownertype", "Legal Type*",
			"select project_costtype_id, project_costtype_text from project_costtypes where project_costtype_id IN (1, 2, 6)", NOTNULL);

		$form->add_lookup("posaddress_store_furniture", "Product Line", "product_lines", "product_line_name");
		$form->add_lookup("posaddress_store_furniture_subclass", "Product Line Subclass", "productline_subclasses", "productline_subclass_name");
		
		
		//$form->add_lookup("posaddress_store_postype", "POS Type", "postypes", "postype_name");
		
		$form->add_list("posaddress_store_postype", "POS Type*",
			"select postype_id, postype_name from postypes order by postype_name", NOTNULL);
		
		$form->add_lookup("posaddress_store_subclass", "POS Type Subclass", "possubclasses", "possubclass_name");
	}

	$form->add_checkbox("posaddress_local_production", "project is locally realized (local production)", 0, DISABLED, "Local Production");
	$form->add_checkbox("posaddress_special_project", "project is a special individual project", 0, DISABLED, "Special Project");
	
	$form->add_section("Distribution Channel");

	$form->add_list("posaddress_distribution_channel", "Distribution Channel",
			"select mps_distchannel_id, concat(mps_distchannel_group, ' - ', mps_distchannel_name, ' - ', mps_distchannel_code) as distributionchannel from mps_distchannels order by mps_distchannel_group, mps_distchannel_name, mps_distchannel_code");

	$form->add_section("Dates");
	$form->add_edit("posaddress_store_openingdate", "Opening Date", 0, "", TYPE_DATE);
	$form->add_edit("posaddress_store_planned_closingdate", "Planned Closing Date", 0, "", TYPE_DATE);
	$form->add_edit("posaddress_store_closingdate", "Closing Date", 0, "", TYPE_DATE);

	
	if($pos["posaddress_store_postype"] == 4)
	{
		$form->add_section("Surfaces");
		$form->add_edit("posaddress_overall_sqms", "Retailers overall sales surface", 0, "", TYPE_DECIMAL, 8);
		$form->add_edit("posaddress_dedicated_sqms_wall", "Sales wall surface dedicated to " . BRAND, 0, "", TYPE_DECIMAL, 8);
		$form->add_edit("posaddress_dedicated_sqms_free", "Sales freestanding surface dedicated to " . BRAND, 0, "", TYPE_DECIMAL, 8);
	}
	else
	{
		$form->add_section("Surfaces");
		$form->add_edit("posaddress_store_grosssurface", "Gross Surface in sqms", 0, "", TYPE_DECIMAL, 8);
		$form->add_edit("posaddress_store_totalsurface", "Total Surface in sqms", 0, "", TYPE_DECIMAL, 8);
		$form->add_edit("posaddress_store_retailarea", "Sales Surface in sqms", 0, "", TYPE_DECIMAL, 8);
		$form->add_edit("posaddress_store_backoffice", "Back Office Surface in sqms", 0, "", TYPE_DECIMAL, 8);
		$form->add_edit("posaddress_store_numfloors", "Number of Floors", 0, "", TYPE_INT);
		$form->add_edit("posaddress_store_floorsurface1", "Floor 1: Surface  in sqms", 0, "", TYPE_DECIMAL, 8);
		$form->add_edit("posaddress_store_floorsurface2", "Floor 2: Surface  in sqms", 0, "", TYPE_DECIMAL, 8);
		$form->add_edit("posaddress_store_floorsurface3", "Floor 3: Surface  in sqms", 0, "", TYPE_DECIMAL, 8);
	}

	$form->add_section("Staff");
	$form->add_edit("posaddress_store_headcounts", "Headcounts", 0, "", TYPE_INT);
	$form->add_edit("posaddress_store_fulltimeeqs", "Full Time Equivalents", 0, "", TYPE_DECIMAL, 8);

	
	$form->add_hidden("country", param("country"));
	$form->add_button('save', "Save");
}
elseif(has_access("can_view_posindex") or has_access("can_view_his_posindex"))
{
	$form->add_lookup("posaddress_ownertype", "Legal Type", "project_costtypes", "project_costtype_text");
	$form->add_lookup("posaddress_store_furniture", "Product Line", "product_lines", "product_line_name");
	$form->add_lookup("posaddress_store_furniture_subclass", "Product Line Subclass", "productline_subclasses", "productline_subclass_name");
	$form->add_lookup("posaddress_store_postype", "POS Type", "postypes", "postype_name");
	$form->add_lookup("posaddress_store_subclass", "POS Type Subclass", "possubclasses", "possubclass_name");

	$form->add_checkbox("posaddress_local_production", "project is locally realized (local production)", "", DISABLED, "Local Production");
	$form->add_checkbox("posaddress_special_project", "project is a special individual project", "", DISABLED, "Special Project");


	$form->add_section("Distribution Channel");

	$form->add_lookup("posaddress_distribution_channel", "Distribution Channel", "mps_distchannels", "concat(mps_distchannel_group, ' - ', mps_distchannel_name, ' - ', mps_distchannel_code) as distributionchannel");

	$form->add_section("Dates");
	$form->add_label("posaddress_store_openingdate", "Opening Date");
	$form->add_label("posaddress_store_planned_closingdate", "Planned Closing Date");
	$form->add_label("posaddress_store_closingdate", "Closing Date");


	if($pos["posaddress_store_postype"] != 4)
	{
		$form->add_section("Surfaces");
		$form->add_label("posaddress_overall_sqms", "Retailers overall sales surface");
		$form->add_label("posaddress_dedicated_sqms_wall", "Sales wall surface dedicated to " . BRAND);
		$form->add_label("posaddress_dedicated_sqms_free", "Sales freestanding surface dedicated to " . BRAND);
	}
	else
	{

		$form->add_section("Surfaces");
		$form->add_label("posaddress_store_grosssurface", "Gross Surface in sqms");
		$form->add_label("posaddress_store_totalsurface", "Total Surface in sqms");
		$form->add_label("posaddress_store_retailarea", "Sales Surface in sqms");
		$form->add_label("posaddress_store_backoffice", "Back Office Surface in sqms");
		$form->add_label("posaddress_store_numfloors", "Number of Floors");
		$form->add_label("posaddress_store_floorsurface1", "Floor 1: Surface  in sqms");
		$form->add_label("posaddress_store_floorsurface2", "Floor 2: Surface  in sqms");
		$form->add_label("posaddress_store_floorsurface3", "Floor 3: Surface  in sqms");

		$form->add_section("Staff");
		$form->add_label("posaddress_store_headcounts", "Headcounts");
		$form->add_label("posaddress_store_fulltimeeqs", "Full Time Equivalents");
	}

	$form->add_hidden("country", param("country"));
}

$form->add_hidden("ltf", param("ltf"));
$form->add_hidden("let", param("let"));
$form->add_hidden("ostate", param("ostate"));
$form->add_button("back", "Back to POS List");


// Populate form and process button clicks

$form->populate();
$form->process();

if($form->button("back"))
{
	redirect("posindex.php?country=" . param("country"). '&let=' . param('let') . '&ltf=' . param("ltf") . '&ostate=' . param("ostate"));
}
elseif($form->button('save'))
{
	if($form->validate())
	{
		$form->save();


		//update latest project with closing date and sqms
		//if($form->value("posaddress_store_closingdate"))
		//{
			$sql =	"select posorder_order, posorder_id " .  
					"from posorders " .
					"left join orders on order_id = posorder_order " . 
					"where (order_actual_order_state_code <> '900' or order_actual_order_state_code is null) " . 
					" and posorder_type = 1 and posorder_posaddress = " . $pos["posaddress_id"] .
					" and posorder_opening_date is not null and posorder_opening_date <> '0000-00-00' " . 
				    " and posorder_project_kind <> 8 " . 
					" order by posorder_year DESC, posorder_opening_date DESC";

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{

				if($row["posorder_order"] > 0)
				{
					
					if($form->value("posaddress_store_closingdate"))
					{
						$sql = "update projects " . 
							   "set project_state = 5, " . 
							   " project_shop_closingdate = " . dbquote(from_system_date($form->value("posaddress_store_closingdate"))) . 
							   " where project_order = " . $row["posorder_order"];
					}
					else
					{
						
						$sql = "update projects " . 
							   "set project_state = 4, project_shop_closingdate = " . dbquote(from_system_date($form->value("posaddress_store_closingdate"))) . 
							   " where project_order = " . $row["posorder_order"];
					}					
					
					$result = mysql_query($sql) or dberror($sql);


					$sql = "update project_costs set " .
						   "project_cost_gross_sqms = " . dbquote($form->value("posaddress_store_grosssurface")) . ", " .
						   "project_cost_totalsqms = " . dbquote($form->value("posaddress_store_totalsurface")) . ", " .
						   "project_cost_sqms = " . dbquote($form->value("posaddress_store_retailarea")) . ", " .
						   "project_cost_backofficesqms = " . dbquote($form->value("posaddress_store_backoffice")) . ", " .
						   "project_cost_floorsurface1 = " . dbquote($form->value("posaddress_store_floorsurface1")) . ", " .
						   "project_cost_floorsurface2 = " . dbquote($form->value("posaddress_store_floorsurface2")) . ", " .
						   "project_cost_floorsurface3 = " . dbquote($form->value("posaddress_store_floorsurface3")) . ", " .
						   "project_cost_numfloors = " . dbquote($form->value("posaddress_store_numfloors")) . " " .
						   " where project_cost_order = " . $row["posorder_order"];

					$result = mysql_query($sql) or dberror($sql);


				}
				else // fake project
				{
					$sql = "update posorders  " . 
						   "set posorder_closing_date  = " . dbquote(from_system_date($form->value("posaddress_store_closingdate"))) . 
						   " where posorder_id = " . $row["posorder_id"];
					$result = mysql_query($sql) or dberror($sql);
				}
			}
			
		//}

		if(param("pos_id"))
		{
			//update_store_locator(param("pos_id"));
			//mysql_select_db(RETAILNET_DB, $db);
		}

		redirect("posindex.php?country=" . param("country"). '&let=' . param('let') . '&ltf=' . param("ltf") . '&ostate=' . param("ostate"));
	}
}


// Render page
$poslocation = get_poslocation(id(), "posaddresses");

$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title(id() ? "POS Details: " . $poslocation["posaddress_name"] : "Add POS Location");

require_once("include/tabs.php");

$form->render();
$page->footer();

?>