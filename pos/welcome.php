<?php
/********************************************************************

    welcome.php

    Main entry page after successful login.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-03-23
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-23
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/


require_once "../include/frame.php";
check_access("can_use_posindex");

redirect("posindex_preselect.php");

if(has_access("can_administrate_posindex"))
{
	/********************************************************************
    prepare all data needed
	*********************************************************************/
	// new entries in the POS Index

	$posaddresses = "select posaddress_id, if(posaddress_name <> '', posaddress_name, 'n.a.') as posname, " .
					"posaddress_address, posaddress_address2, posaddress_zip, posaddress_country, " .
					"    posaddress_place, country_name, project_costtype_text, postype_name, posaddresses.date_created as pdate " .
					"from posaddresses " .
					"left join countries on posaddress_country = country_id " . 
					"left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
					"left join postypes on postype_id = posaddress_store_postype ";

	$list3_filter = "posaddress_checked = 0 and posaddress_store_postype <> 4";

	$posaddress_links = array();
	$sql_images = $posaddresses . " where " . $list3_filter;
	$res = mysql_query($sql_images) or dberror($sql_images);
	while ($row = mysql_fetch_assoc($res))
	{
		$link = "<a href=\"javascript:popup('../pos/posindex_pos.php?id=" .  $row["posaddress_id"] . "&country=" . $row["posaddress_country"]. "',1024,768);\"><img src=\"/pictures/wf_warning.gif\" border='0'/></a>";
		$posaddress_links[$row["posaddress_id"]] = $link;
	}

	//projects
	$projects = array();
	$sql_projects = "select address_id, project_id, order_number  " .
					 "from addresses " .
					 "left join orders on order_franchisee_address_id = address_id " . 
					 "left join projects on project_order = order_id " . 
					 "left join countries on address_country = country_id " . 
					 "left join address_types on address_type_id = address_type " . 
					 "where address_checked = 0 " . 
					 "order by address_id";


	$res = mysql_query($sql_projects) or dberror($sql_projects);
	while ($row = mysql_fetch_assoc($res))
	{
		$link = "<a href=\"javascript:popup('../user/project_edit_pos_data.php?pid=" .  $row["project_id"]. "',1024,768);\">" . $row["order_number"] . "</a>";
		if(array_key_exists($row["address_id"], $projects))
		{
			$projects[$row["address_id"]] = $projects[$row["address_id"]] . ", " . $link;
		}
		else
		{
			$projects[$row["address_id"]] = $link;
		}
	}
	
	//addresses
	$addresses = "select address_id, address_shortcut, address_company, address_zip,  " .
				 "    address_place, country_name, address_type_name " .
				 "from addresses " .
				 "left join countries on address_country = country_id " . 
				 "left join address_types on address_type_id = address_type";

	$list4_filter = "address_type in (1, 7) and address_checked = 0";

	$images = array();
	$sql_images = $addresses . " where address_checked = 0 and address_type = 7";

	$res = mysql_query($sql_images) or dberror($sql_images);

	while ($row = mysql_fetch_assoc($res))
	{
		$images[$row["address_id"]] = "/pictures/wf_warning.gif";
	}


	$address_links = array();
	$sql_images = $addresses . " where " . $list4_filter;
	$res = mysql_query($sql_images) or dberror($sql_images);
	while ($row = mysql_fetch_assoc($res))
	{
		$link = "<a href=\"javascript:popup('../admin/address3rd.php?id=" .  $row["address_id"] . "',1024,768);\"><img src=\"/pictures/wf_warning.gif\" border='0'/></a>";
		$address_links[$row["address_id"]] = $link;

	}


	/********************************************************************
		Create pos list
	*********************************************************************/ 

	$list3 = new ListView($posaddresses);
	$list3->set_title("POS Index: New POS not yet checked");
	$list3->set_entity("posaddresses");
	$list3->set_filter($list3_filter);
	$list3->set_order("country_name, posaddress_place, posaddress_name");

	$list3->add_text_column("posaddresses", "", COLUMN_UNDERSTAND_HTML, $posaddress_links);
	$list3->add_column("country_name", "Country", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);
	$list3->add_column("posaddress_place", "City", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
	$list3->add_column("posname", "POS Name", "" , LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
	$list3->add_column("posaddress_address", "Address", "", LIST_FILTER_FREE);
	$list3->add_column("postype_name", "POS Type", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);
	$list3->add_column("project_costtype_text", "Legal Type", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);
	//$list3->add_text_column("pdfs", "", COLUMN_UNDERSTAND_HTML, $pdfs);
	//$list3->add_text_column("map", "", COLUMN_UNDERSTAND_HTML, $googlemap);
	//$list3->add_image_column("closed", "", 0, $closed);

	
	/********************************************************************
		Create address list
	*********************************************************************/ 

	$list4 = new ListView($addresses);
	$list4->set_title("New Addresses not yet checked");
	$list4->set_entity("projects");
	$list4->set_filter($list4_filter);
	$list4->set_order("country_name, address_place, address_company");

	$list4->add_text_column("addresses", "", COLUMN_UNDERSTAND_HTML, $address_links);
	$list4->add_column("address_shortcut", "Shortcut", "", LIST_FILTER_FREE);
	$list4->add_column("address_type_name", "Type", "", LIST_FILTER_LIST);
	$list4->add_column("address_company", "Company", "", LIST_FILTER_FREE);
	$list4->add_column("address_zip", "Zip", "", LIST_FILTER_FREE);
	$list4->add_column("address_place", "City", "", LIST_FILTER_FREE);
	$list4->add_column("country_name", "Country", "", LIST_FILTER_LIST,
		"select country_name from countries order by country_name");
	
	$list4->add_text_column("projects", "Projects", COLUMN_UNDERSTAND_HTML, $projects);


	/********************************************************************
		Populate liste and process button clicks
	*********************************************************************/ 

	$list3->populate();
	$list4->populate();

	/********************************************************************
		 Render Page
	 *********************************************************************/ 
	$page = new Page("posindex");

	require "include/pos_page_actions.php";

	$page->header();
	$page->title("POSIndex: What's new?");

	$list3->render();
	echo "<br /><br />";
	$list4->render();

	$page->footer();
}
else
{
	$page = new Page("posindex");

	require "include/pos_page_actions.php";


	$page->header();
	echo "<p>Welcome to the POS-Index of " . APPLICATION_NAME . ".</p>";


	$page->footer();
}

?>