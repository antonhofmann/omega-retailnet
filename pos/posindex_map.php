<?php
/********************************************************************

    posindex_map.php

    Show google Map of the POS.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

require_once "include/check_access.php";

if(param("pos_id"))
{
	register_param("pos_id");
	param("id", param("pos_id"));
}
else
{
	register_param("pos_id");
	param("pos_id", id());
}

if(param("pos_id") > 0)
{
	$pos = get_poslocation(param("pos_id"), "posaddresses");
}

//check if user can edit this company
$can_edit = false;
if(has_access("can_edit_posindex"))
{
	$can_edit = true;
}
elseif(has_access("can_edit_his_posindex"))
{
	$can_edit = get_user_edit_permission(user_id(), param("pos_id"));
}


// Build form
$form = new Form("posaddresses", "posaddress");

$form->add_hidden("country", param("country"));
$form->add_hidden("ltf", param("ltf"));
$form->add_hidden("let", param("let"));
$form->add_hidden("ostate", param("ostate"));

$form->add_lookup("posaddress_client_id", "Client", "addresses", "concat(address_company, ', ', address_place, ' [', address_shortcut, ']') as company", 0, $pos["posaddress_client_id"]);

$form->add_section("Address Data");
$form->add_label("posaddress_name", "POS Name");

$form->add_label("posaddress_address", "Address*", NOTNULL);
$form->add_label("posaddress_address2", "Address 2");
$form->add_label("posaddress_zip", "Zip*", NOTNULL);
$form->add_label("posaddress_place", "City*", NOTNULL);
$form->add_lookup("posaddress_country", "Country", "countries", "country_name", 0, $pos["posaddress_country"]);

$form->add_section("Google Data");

/*
if($pos["posaddress_store_postype"] == 4) //POS Type Other
{
	$form->add_label("posaddress_google_lat", "Latitude");
	$form->add_label("posaddress_google_long", "Longitude");
}

else
*/
if(has_access("can_edit_posindex"))
{
	$form->add_edit("posaddress_google_lat", "Latitude*", NOTNULL, "", TYPE_DECIMAL, 20, 15);
	$form->add_edit("posaddress_google_long", "Longitude*", NOTNULL, "", TYPE_DECIMAL, 20, 15);
}
elseif($can_edit == true and has_access("can_edit_his_posindex"))
{
	$form->add_edit("posaddress_google_lat", "Latitude*", NOTNULL, "", TYPE_DECIMAL, 20, 15);
	$form->add_edit("posaddress_google_long", "Longitude*", NOTNULL, "", TYPE_DECIMAL, 20, 15);
}
elseif(has_access("can_view_posindex") or has_access("can_view_his_posindex"))
{
	$form->add_label("posaddress_google_lat", "Latitude");
	$form->add_label("posaddress_google_long", "Longitude");
}

if($can_edit == true)
{
	$form->add_button(FORM_BUTTON_SAVE, "Save");
}

$form->add_button("back", "Back to POS List");

// Populate form and process button clicks

$form->populate();
$form->process();



if($form->button(FORM_BUTTON_SAVE))
{
	$sql = "update posaddresses set " . 
		   "posaddress_google_lat = " . $form->value("posaddress_google_lat")  . ", ".
		   "posaddress_google_long = " . $form->value("posaddress_google_long") . ", ".
	       "posaddress_google_precision = 1 " . 
		   "where posaddress_id = " . id();
	$result = mysql_query($sql) or dberror($sql);

	if(id())
	{
		update_store_locator(id());
		mysql_select_db(RETAILNET_DB, $db);
	}

	redirect("posindex_map.php?pos_id=" . id() . "&country=" . param("country"). '&let=' . param('let') . "&ltf=" . param("ltf"). "&ostate=" . param("ostate"));
}
elseif($form->button("back"))
{
	redirect("posindex.php?country=" . param("country") . '&let=' . param('let') . "&ltf=" . param("ltf"). "&ostate=" . param("ostate"));
}


// Render page
$poslocation = get_poslocation(id(), "posaddresses");

$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title("Google Map: " . $poslocation["posaddress_name"]);

if(id())
{
	require_once("include/tabs.php");
}

$form->render();

echo "<iframe src =\"posindex_map2.php?id=" . param("pos_id") . "\" width=\"800\" height=\"800\" frameborder=\"0\" marginwidth=\"0\"></iframe>";

$page->footer();

?>