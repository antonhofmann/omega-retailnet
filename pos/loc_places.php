<?php
/********************************************************************

    loc_places.php

    Lists places for translations.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");
set_referer("loc_place.php");



if(param("place_country") > 1)
{
	$place_country =	param("place_country");
}
else
{
	$place_country = 1;
}


/********************************************************************
    Create Form
*********************************************************************/ 
$form = new Form("places", "place");

$form->add_section("List Filter Selection");

$form->add_list("place_country", "Country",
    "select country_id, country_name from countries order by country_name", SUBMIT | NOTNULL, $place_country);


/********************************************************************
    Populate and process button clicks
*********************************************************************/ 
$form->populate();

/********************************************************************
    Create List
*********************************************************************/ 

//get all translations
$translations = array();
$sql = "select place_id,language_name " . 
       "from places " . 
       "left join loc_places on loc_place_place = place_id " . 
	   "left join languages on language_id = loc_place_language " . 
	   "order by language_name";


$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	if(array_key_exists($row["place_id"], $translations))
	{
		$translations[$row["place_id"]] = $translations[$row["place_id"]] . "/" . trim($row["language_name"]);
	}
	else
	{
		$translations[$row["place_id"]] = trim($row["language_name"]);
	}
}

$sql = "select place_id, place_name " .
       "from places ";

$list_filter = "";

$list_filter =	"place_country = " . $place_country;

$list = new ListView($sql);

$list->set_entity("places");
$list->set_order("place_name");
$list->set_filter($list_filter);

$list->add_column("place_name", "Name", "loc_place.php", LIST_FILTER_FREE);
$list->add_text_column("translations", "Translations", COLUMN_UNDERSTAND_HTML, $translations);

$list->process();

$page = new Page("locales");

$page->header();
$page->title("Places");
$form->render();
$list->render();
$page->footer();
?>
