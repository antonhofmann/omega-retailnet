<?php
/********************************************************************

    pos_closing_assessment.php

    Data Entry for POS Closing Assessment.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2012-02-03
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2012-02-03
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../shared/func_posindex.php";
require_once "include/get_functions.php";

require_once "../include/phpmailer/class.phpmailer.php";


check_access("has_access_to_posclosing_assessments");


//check if user can edit this company
$user = get_user(user_id());

$country_filter = "";
$tmp = array();
$sqlc = "select * from country_access " .
		"where country_access_user = " . user_id();


$resc = mysql_query($sqlc) or dberror($sqlc);

while ($rowc = mysql_fetch_assoc($resc))
{            
	$tmp[] = $rowc["country_access_country"];
}

if(count($tmp) > 0) {
	$country_filter = " posaddress_country IN (" . implode(",", $tmp) . ") ";
}


$submission_date = '';
$resubmission_date = '';
$final_file = '';
$current_year = date("Y");
$approval_date = '';

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("posclosingassessments", "posclosingassessments", 750);

if(id() == 0)
{
	$broker_fees = 0;
	$hr_costs = 0;
	$last_year = '';
	if(has_access("can_edit_posclosing_assessments"))
	{
		$sql = "select DISTINCT country_id, country_name " .
			   "from posaddresses " . 
			   "left join countries on posaddress_country = country_id " . 
			   "where country_name is not null " .
			   "and (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') " .
			   "and posaddress_ownertype = 1 " . 
			   " order by country_name";
	}
	else
	{
		

		if($country_filter) 
		{
			$list_filter = " (posaddress_client_id = " . $user["address"] . " or " . $country_filter . ") ";
		}
		else
		{
			$list_filter = "posaddress_client_id = " . $user["address"];
		}
		
		
		$sql = "select DISTINCT country_id, country_name " .
			   "from posaddresses " . 
			   "left join countries on posaddress_country = country_id " . 
			   "where country_name is not null " .
			   "and (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') " .
			   "and posaddress_ownertype = 1 " .
			   " and ".  $list_filter . 
			   " order by country_name";

	}

	if(param("country"))
	{
		if(has_access("can_edit_posclosing_assessments"))
		{
			$sql_p = "select posaddress_id, concat(place_name, ', ', posaddress_name) as posname " .
					   "from posaddresses " . 
					   "left join places on place_id = posaddress_place_id " .
				       "left join posclosingassessments on posclosingassessment_posaddress_id = posaddress_id " . 
					   "where (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') " .
					   "and posaddress_ownertype = 1 " . 
					   "and posaddress_country = " . param("country") .
				       " and posclosingassessment_id is null " .
					   " order by place_name, posaddress_name";
		}
		else
		{
			
			
			if($country_filter) 
			{
				$list_filter = "";
			}
			else
			{
				$list_filter = " and posaddress_client_id = " . $user["address"];
			}

			$sql_p = "select posaddress_id, concat(place_name, ', ', posaddress_name) as posname " .
					   "from posaddresses " . 
					   "left join places on place_id = posaddress_place_id " . 
				       "left join posclosingassessments on posclosingassessment_posaddress_id = posaddress_id " . 
					   "where (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') " .
					   "and posaddress_ownertype = 1 " . 
					   "and posaddress_country = " . param("country") .
					   $list_filter . 
				        " and posclosingassessment_id is null " .
					   " order by place_name, posaddress_name";
		}

	}

	
	$form->add_section("POS Location");
	$form->add_hidden("posclosingassessment_year", $current_year);
	$form->add_comment("Please select the POS location for which you like to add closing assessment information.");

	$form->add_list("country", "Country", $sql, SUBMIT | NOTNULL, param("country"));

	if(param("country"))
	{
		$form->add_list("posclosingassessment_posaddress_id", "POS Location", $sql_p, SUBMIT | NOTNULL, param("posclosingassessment_posaddress_id"));
	}

	if(param("posclosingassessment_posaddress_id"))
	{
		if(has_access("can_edit_posclosing_assessments") or has_access("can_edit_his_posclosing_assessments"))
		{
			$form->add_button(FORM_BUTTON_SAVE, "Save");
		}
	}

	$form->add_button(FORM_BUTTON_BACK, "Back");
}
else
{
	$sql = "select posclosingassessment_posaddress_id, posclosingassessment_submissiondate, " .
		   "posclosingassessment_planned_closingdate , " . 
		   "posclosingassessment_resubmissiondate, posclosingassessment_approvaldate, " . 
		   "posclosingassessment_filesigned, posclosingassessment_year, posclosingassessment_rejected, " . 
		   "concat(users.user_name, ' ', users.user_firstname) as user_name, " .
		   "concat(users1.user_name, ' ', users1.user_firstname) as user_name1, " .
		   "concat(users2.user_name, ' ', users2.user_firstname) as user_name2 " .
		   "from posclosingassessments " .
		   "left join users on users.user_id = posclosingassessment_submissionuser " .
		   "left join users as users1 on users1.user_id = posclosingassessment_resubmissionuser " . 
		   "left join users as users2 on users2.user_id = posclosingassessment_rejected_by " . 
		   "where posclosingassessment_id = " . id();
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);

	$submission_date = $row["posclosingassessment_submissiondate"];
	$submission_user = $row["user_name"];
	$resubmission_date = $row["posclosingassessment_resubmissiondate"];
	$resubmission_user = $row["user_name1"];
	$current_year = $row["posclosingassessment_year"];
	$approvale_date = $row["posclosingassessment_approvaldate"];

	$rejected_date = $row["posclosingassessment_rejected"];

	$last_year = substr($row["posclosingassessment_planned_closingdate"], 0, 4);;

	$rejected_user = $row["user_name2"];

	$final_file =  $row["posclosingassessment_filesigned"];
	
	$pos_data = get_poslocation($row["posclosingassessment_posaddress_id"]);

	
	$posname = $pos_data["country_name"] . ", " . $pos_data["posaddress_name"];


	$sql_currency = "select currency_id, currency_symbol " . 
				    "from countries " .
				    "left join currencies on currency_id = country_currency " . 
				    "where country_id = " . $pos_data["country_id"];
	
	$broker_fees = $row['posclosingassessment_terminationcost03'];
	$hr_costs = $row['posclosingassessment_terminationcost06'];


	$form->add_label("posname", "POS Location", 0, $posname);

	if($submission_date != NULL and $submission_date != '0000-00-00' )
	{
		$form->add_label("sdate", "Submission Date", 0, to_system_date($submission_date));
		$form->add_label("suser", "Submitted by", 0, $submission_user);
	}
	if($resubmission_date != NULL and $resubmission_date != '0000-00-00' )
	{
		$form->add_label("rsdate", "Resubmission Date", 0, to_system_date($resubmission_date));
		$form->add_label("rsuser", "Resubmitted by", 0, $resubmission_user);
	}
	if($rejected_date != NULL and $rejected_date != '0000-00-00' )
	{
		$form->add_label("rjdate", "Rejection Date", 0, to_system_date($rejected_date));
		$form->add_label("rjuser", "Rejected by", 0, $rejected_user);
	}

	$form->add_section("Basic Information");
	$form->add_hidden("posclosingassessment_posaddress_id", param("posclosingassessment_posaddress_id"));


	$form->add_edit("posclosingassessment_contactperson", "Name of Contact Person*", NOTNULL);
	$form->add_edit("posclosingassessment_landlord", "Land Lord*", NOTNULL);
	$form->add_edit("posclosingassessment_lease_enddate", "Eding Date of Lease Contract*", NOTNULL, "", TYPE_DATE, 10,0,1, "lease_startdate");
	$form->add_edit("posclosingassessment_planned_closingdate", "Requested Closing Date*", NOTNULL, "", TYPE_DATE, 10, 0, 1, "lease_enddate");
	$form->add_multiline("posclosingassessment_reason", "Reason for Termination*", 8, NOTNULL);
	



	$form->add_section("Consequences of closing");
	$form->add_multiline("posclosingassessment_image_consequence", "Image*", 6, NOTNULL);
	$form->add_multiline("posclosingassessment_sales_consequence", "Sales/Distribution*", 6, NOTNULL);

	$form->add_multiline("posclosingassessment_legal_consequence", "Legal Consequences and Termination Agreement*", 6, NOTNULL);

	$form->add_multiline("posclosingassessment_furniture_consequence", "Furniture*", 6, NOTNULL);
	

	$form->add_section("Financial Data");
	$form->add_list("posclosingassessment_currency_id", "Currency*", $sql_currency, NOTNULL);
	$form->add_edit("posclosingassessment_initial_investment", "Initial Investment*", NOTNULL, "", TYPE_INT, 12);
	$form->add_edit("posclosingassessment_book_value", "Projected Book Value Closing Date*", NOTNULL, "", TYPE_INT, 12);
	$form->add_edit("posclosingassessment_termination_costs", "Projected Termination Costs incl. FA*", NOTNULL, "", TYPE_INT, 12);
	$form->add_edit("posclosingassessment_rental_costs", "Projected Rents Costs Remaining Lease Period*", NOTNULL, "", TYPE_INT, 12);


	$form->add_edit("posclosingassessment_operating_loss", "Projected Operating Loss Remaining Lease Period*", NOTNULL, "", TYPE_INT, 12);
	$form->add_edit("posclosingassessment_operating_loss_ws", "Projected Operating Loss Remaining Lease Period (incl. WS Margin*", NOTNULL, "", TYPE_INT, 12);

	$form->add_edit("posclosingassessment_depr_years", "Years of Depreciation left*", NOTNULL, "", TYPE_INT, 4);
	

}



$form->populate();
$form->process();



if($form->button(FORM_BUTTON_SAVE))
{
	//create records in posclosingassessmentfindates
	
	//Gross Sales
	$sql = "insert into posclosingassessmentfindates (" . 
		   "posclosingassessmentfindate_assessment_id, posclosingassessmentfindate_fintype, user_created, date_created) VALUES (".
		   id() . ", " .
		   "1, " .
		   "'" . user_login() . "', " .
		   "'" . date("Y-m-d H:i:s") . "')";

	$result = mysql_query($sql) or dberror($sql);

	//Net Sales
	$sql = "insert into posclosingassessmentfindates (" . 
		   "posclosingassessmentfindate_assessment_id, posclosingassessmentfindate_fintype, user_created, date_created) VALUES (".
		   id() . ", " .
		   "2, " .
		   "'" . user_login() . "', " .
		   "'" . date("Y-m-d H:i:s") . "')";

	$result = mysql_query($sql) or dberror($sql);

	//COGS
	$sql = "insert into posclosingassessmentfindates (" . 
		   "posclosingassessmentfindate_assessment_id, posclosingassessmentfindate_fintype, user_created, date_created) VALUES (".
		   id() . ", " .
		   "3, " .
		   "'" . user_login() . "', " .
		   "'" . date("Y-m-d H:i:s") . "')";

	$result = mysql_query($sql) or dberror($sql);

	//Depreciation
	$sql = "insert into posclosingassessmentfindates (" . 
		   "posclosingassessmentfindate_assessment_id, posclosingassessmentfindate_fintype, user_created, date_created) VALUES (".
		   id() . ", " .
		   "4, " .
		   "'" . user_login() . "', " .
		   "'" . date("Y-m-d H:i:s") . "')";

	$result = mysql_query($sql) or dberror($sql);

	//Salaries
	$sql = "insert into posclosingassessmentfindates (" . 
		   "posclosingassessmentfindate_assessment_id, posclosingassessmentfindate_fintype, user_created, date_created) VALUES (".
		   id() . ", " .
		   "5, " .
		   "'" . user_login() . "', " .
		   "'" . date("Y-m-d H:i:s") . "')";

	$result = mysql_query($sql) or dberror($sql);

	//Rents
	$sql = "insert into posclosingassessmentfindates (" . 
		   "posclosingassessmentfindate_assessment_id, posclosingassessmentfindate_fintype, user_created, date_created) VALUES (".
		   id() . ", " .
		   "6, " .
		   "'" . user_login() . "', " .
		   "'" . date("Y-m-d H:i:s") . "')";

	$result = mysql_query($sql) or dberror($sql);


	//Other expenses
	$sql = "insert into posclosingassessmentfindates (" . 
		   "posclosingassessmentfindate_assessment_id, posclosingassessmentfindate_fintype, user_created, date_created) VALUES (".
		   id() . ", " .
		   "7, " .
		   "'" . user_login() . "', " .
		   "'" . date("Y-m-d H:i:s") . "')";

	$result = mysql_query($sql) or dberror($sql);


	//Operating Profit
	$sql = "insert into posclosingassessmentfindates (" . 
		   "posclosingassessmentfindate_assessment_id, posclosingassessmentfindate_fintype, user_created, date_created) VALUES (".
		   id() . ", " .
		   "8, " .
		   "'" . user_login() . "', " .
		   "'" . date("Y-m-d H:i:s") . "')";

	$result = mysql_query($sql) or dberror($sql);

	//Operating Profit incl WHSM
	$sql = "insert into posclosingassessmentfindates (" . 
		   "posclosingassessmentfindate_assessment_id, posclosingassessmentfindate_fintype, user_created, date_created) VALUES (".
		   id() . ", " .
		   "9, " .
		   "'" . user_login() . "', " .
		   "'" . date("Y-m-d H:i:s") . "')";

	$result = mysql_query($sql) or dberror($sql);


	
	
	$link = "pos_closing_assessment.php?id=" . id();
	redirect($link);
}
/********************************************************************
    Create List
*********************************************************************/ 

if(id() > 0)
{
	$captions = array();
	$amounts01 = array();
	$amounts02 = array();
	$amounts03 = array();
	$amounts04 = array();
	$amounts05 = array();

			
	$sql = "select * from posclosingassessmentfindates " . 
		   " where posclosingassessmentfindate_assessment_id = " . id()  . 
		   " order by posclosingassessmentfindate_fintype";


	$res = mysql_query($sql) or dberror($sql);
    while($row = mysql_fetch_assoc($res))
	{
		if($row["posclosingassessmentfindate_fintype"] == 1)
		{
			$captions[$row["posclosingassessmentfindate_id"]] = "Gross Sales*";
			
		}
		elseif($row["posclosingassessmentfindate_fintype"] == 2)
		{
			$captions[$row["posclosingassessmentfindate_id"]] = "Net sales";
		}
		elseif($row["posclosingassessmentfindate_fintype"] == 3)
		{
			$captions[$row["posclosingassessmentfindate_id"]] = "Costs of Products Sold*";
		}
		elseif($row["posclosingassessmentfindate_fintype"] == 4)
		{
			$captions[$row["posclosingassessmentfindate_id"]] = "Depreciation of Fixed Assets*";
		}
		elseif($row["posclosingassessmentfindate_fintype"] == 5)
		{
			$captions[$row["posclosingassessmentfindate_id"]] = "Indirect Salaries & Social Benefits*";
		}
		elseif($row["posclosingassessmentfindate_fintype"] == 6)
		{
			$captions[$row["posclosingassessmentfindate_id"]] = "Rents and Depreciation Key Money*";
		}
		elseif($row["posclosingassessmentfindate_fintype"] == 7)
		{
			$captions[$row["posclosingassessmentfindate_id"]] = "Other operating expenses*";
		}
		elseif($row["posclosingassessmentfindate_fintype"] == 8)
		{
			$captions[$row["posclosingassessmentfindate_id"]] = "Operating Profit (excluding WS Margin)*";
		}
		elseif($row["posclosingassessmentfindate_fintype"] == 9)
		{
			$captions[$row["posclosingassessmentfindate_id"]] = "Operating Profit (including WS Margin)*";
		}
		
		$amounts01[$row["posclosingassessmentfindate_id"]] = $row["posclosingassessmentfindate_amount01"];
		$amounts02[$row["posclosingassessmentfindate_id"]] = $row["posclosingassessmentfindate_amount02"];
		$amounts03[$row["posclosingassessmentfindate_id"]] = $row["posclosingassessmentfindate_amount03"];
		$amounts04[$row["posclosingassessmentfindate_id"]] = $row["posclosingassessmentfindate_amount04"];
		$amounts05[$row["posclosingassessmentfindate_id"]] = $row["posclosingassessmentfindate_amount05"];
		
	}


	$sql = "select * from posclosingassessmentfindates";
	$list_filter = "posclosingassessmentfindate_assessment_id = " . id();
	$list_oder = "posclosingassessmentfindate_fintype";
	$list = new ListView($sql);
	$list->set_entity("posclosingassessmentfindates");
	$list->set_filter($list_filter);
	$list->set_order($list_oder );

	$list->set_title("Sales - Historic + Forecast");


	$list->add_text_column("captions", "", COLUMN_NO_WRAP, $captions);
	$list->add_number_edit_column("amount01", $current_year-3, "17",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $amounts01);
	$list->add_number_edit_column("amount02", $current_year-2, "17",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $amounts02);
	$list->add_number_edit_column("amount03", $current_year-1, "17",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $amounts03);
	$list->add_number_edit_column("amount04", "Forecast on\nContinuing " . $current_year, "17",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $amounts04);

	$list->add_number_edit_column("amount05", "Forecast on\nTermination " . $last_year, "17",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $amounts05);

	$list->populate();
	$list->process();



	$form1 = new Form("posclosingassessments", "posclosingassessments", 750);

	
	

	$form1->add_section("Termination Costs");
	$form1->add_edit("posclosingassessment_terminationcost01", "Unamortized Fixed Assets in Local Currency", 0, "", TYPE_INT);
	$form1->add_edit("posclosingassessment_terminationcost01_c", "&nbsp;&nbsp;&nbsp;> Comment Unamortized Fixed Assets", 0, "", TYPE_CHAR);


	$form1->add_edit("posclosingassessment_terminationcost07", "Redundancy Costs in Local Currency", 0, "", TYPE_INT);
	$form1->add_edit("posclosingassessment_terminationcost07_c", "&nbsp;&nbsp;&nbsp;> Comment Redundancy Costs", 0, "", TYPE_CHAR);

	$form1->add_edit("posclosingassessment_terminationcost02", "Penalties in Local Currency", 0, "", TYPE_INT);
	$form1->add_edit("posclosingassessment_terminationcost02_c", "&nbsp;&nbsp;&nbsp;> Comment Penalties", 0, "", TYPE_CHAR);

	$form1->add_edit("posclosingassessment_terminationcost08", "Rent Contribution New Tenant Local Currency", 0, "", TYPE_INT);
	$form1->add_edit("posclosingassessment_terminationcost08_c", "&nbsp;&nbsp;&nbsp;> Comment Rent Contribution New Tenant", 0, "", TYPE_CHAR);

	if ($broker_fees > 0) {
		$form1->add_edit("posclosingassessment_terminationcost03", "Broker Fees in Local Currency", 0, "", TYPE_INT);
		$form1->add_edit("posclosingassessment_terminationcost03_c", "&nbsp;&nbsp;&nbsp;> Comment Broker Fees", 0, "", TYPE_CHAR);
	} else {
		$form1->add_hidden("posclosingassessment_terminationcost03");
		$form1->add_hidden("posclosingassessment_terminationcost03_c");
	}
	
	
	$form1->add_edit("posclosingassessment_terminationcost04", "Dismantling Costs in Local Currency", 0, "", TYPE_INT);
	$form1->add_edit("posclosingassessment_terminationcost04_c", "&nbsp;&nbsp;&nbsp;> Comments Dismantling Costs", 0, "", TYPE_CHAR);

	if ($hr_costs > 0) {
		$form1->add_edit("posclosingassessment_terminationcost06", "Costs HR in Local Currency", 0, "", TYPE_INT);
		$form1->add_edit("posclosingassessment_terminationcost06_c", "&nbsp;&nbsp;&nbsp;> Comment Costs HR", 0, "", TYPE_CHAR);
	} else {
		$form1->add_hidden("posclosingassessment_terminationcost06");
		$form1->add_hidden("posclosingassessment_terminationcost06_c");
	}

	$form1->add_edit("posclosingassessment_terminationcost09", "Deposit (in case deposit is lost) in Local Currency", 0, "", TYPE_INT);
	$form1->add_edit("posclosingassessment_terminationcost09_c", "&nbsp;&nbsp;&nbsp;> Comment Deposit", 0, "", TYPE_CHAR);


	$form1->add_edit("posclosingassessment_terminationcost05", "Other Costs in Local Currency", 0, "", TYPE_INT);
	$form1->add_edit("posclosingassessment_terminationcost05_c", "&nbsp;&nbsp;&nbsp;> Comment Other Costs", 0, "", TYPE_CHAR);


	$form1->add_section("Attachments");
	
	$deletable = "";
	if(has_access("can_delete_uploaded_files"))
	{
		$deletable = DELETABLE;
	}
	$form1->add_comment("You must upload a summary (PDF) in case you have indicated termination costs.");
	$form1->add_upload("posclosingassessment_file01", "Unamortized Fixed Assets (PDF)", "/files/posclosings/pos_". $pos_data["posaddress_id"], $deletable);

	$form1->add_upload("posclosingassessment_file07", "Redundancy Cost (PDF)", "/files/posclosings/pos_". $pos_data["posaddress_id"], $deletable);


	$form1->add_upload("posclosingassessment_file02", "Penalties (PDF)", "/files/posclosings/pos_". $pos_data["posaddress_id"], $deletable);
	
	$form1->add_upload("posclosingassessment_file08", "Rent Contribution New Tenant (PDF)", "/files/posclosings/pos_". $pos_data["posaddress_id"], $deletable);

	if ($broker_fees > 0) {
		$form1->add_upload("posclosingassessment_file03", "Broker Fees (PDF)", "/files/posclosings/pos_". $pos_data["posaddress_id"], $deletable);
	} else {
		$form1->add_hidden("posclosingassessment_file03");
	}

	$form1->add_upload("posclosingassessment_file04", "Dismantling Costs (PDF)", "/files/posclosings/pos_". $pos_data["posaddress_id"], $deletable);
	
	if ($hr_costs > 0) {
		$form1->add_upload("posclosingassessment_file06", "HR Costs (PDF)", "/files/posclosings/pos_". $pos_data["posaddress_id"], $deletable);
	} else {
		$form1->add_hidden("posclosingassessment_file06");
	}

	$form1->add_upload("posclosingassessment_file09", "Deposit (PDF)", "/files/posclosings/pos_". $pos_data["posaddress_id"], $deletable);

	
	$form1->add_upload("posclosingassessment_file05", "Other Costs (PDF)", "/files/posclosings/pos_". $pos_data["posaddress_id"], $deletable);

	
	//signed file
	if(has_access("can_approve_posclosing_assessments") and $submission_date != NULL and $submission_date != '0000-00-00' )
	{
		$form1->add_section("Final Closing Assessment");
		$form1->add_edit("posclosingassessment_approvaldate", "Approval Date*", NOTNULL, "", TYPE_DATE);
		$form1->add_upload("posclosingassessment_filesigned", "PDF with signature", "/files/posclosings/pos_". $pos_data["posaddress_id"], 0);
	}
	else
	{
		$form1->add_hidden("posclosingassessment_filesigned");
		$form1->add_hidden("posclosingassessment_approvaldate");
		
	}
	

	
	$form1->add_button(FORM_BUTTON_BACK, "Back");
	
	if(has_access("can_edit_posclosing_assessments") or has_access("can_edit_his_posclosing_assessments"))
	{
		if($submission_date == NULL or $submission_date == '0000-00-00' )
		{
			$form1->add_button("delete", "Delete Closing Assessment");
		}


		if($submission_date == NULL or $submission_date == '0000-00-00' )
		{
			$form1->add_button("submit", "Submit Closing Assessment");
		}
	 
		if($submission_date != NULL and $submission_date != '0000-00-00'
		   and ($approvale_date == NULL or $approvale_date == '0000-00-00'))
		{
			$form1->add_button("resubmit", "Resubmit Closing Assessment");
		}

		if($submission_date == NULL or $submission_date == '0000-00-00' )
		{
			$form1->add_button("save", "Save Closing Assessment");
		}
		elseif(has_access("can_approve_posclosing_assessments") and !$final_file)
		{
			$form1->add_button("savef", "Approve Closing Assessment");
		}
		
		if(has_access("can_reject_submissions") and ($approvale_date == NULL or $approvale_date == '0000-00-00'))
		{
			if(($submission_date != NULL and $submission_date != '0000-00-00') or ($resubmission_date != NULL and $resubmission_date != '0000-00-00' ))
			{
					$link = "javascript:createWindowWithRemotingUrl2('Reject Closing Assessment', 'reject_submission.php?id=" . param("id") . "');";
					$form1->add_button("reject", "Reject Closing Assessment", $link);
			}
		}
		
		
		if(id() > 0)
		{
			if($approvale_date == NULL or $approvale_date == '0000-00-00')
			{
				$link = "javascript:popup('pos_closing_assessment_pdf.php?id=" .  id() . "&b=3',800,600);";
				$form1->add_button("print", "Print Booklet", $link);
			}
			else
			{
				$link = "javascript:popup('pos_closing_assessment_pdf.php?id=" .  id() . "&b=2',800,600);";
				$form1->add_button("print", "Print Booklet", $link);
			}
		}
	}
	

	$form1->populate();
	$form1->process();

	//process action of form 1
	if($form1->button("save") or $form1->button("submit") or $form1->button("resubmit"))
	{
		if(!$form->validate())
		{
			//$form->error("Please enter the contact person, the lease conract ending date, the scheduled closing date and the reason! The dates must take the following form: dd.mm.yy");
		}
		else
		{

			//check files
			$error = 0;
			if($form1->value("posclosingassessment_file01"))
			{
				$path_info = pathinfo($form1->value("posclosingassessment_file01"));
				if(strtolower($path_info['extension']) != 'pdf')
				{
					$error = 1;
				}
			}
			if($form1->value("posclosingassessment_file02"))
			{
				$path_info = pathinfo($form1->value("posclosingassessment_file02"));
				if(strtolower($path_info['extension']) != 'pdf')
				{
					$error = 1;
				}
			}
			if ($broker_fees > 0) {
				if($form1->value("posclosingassessment_file03"))
				{
					$path_info = pathinfo($form1->value("posclosingassessment_file03"));
					if(strtolower($path_info['extension']) != 'pdf')
					{
						$error = 1;
					}
				}
			}
			if($form1->value("posclosingassessment_file04"))
			{
				$path_info = pathinfo($form1->value("posclosingassessment_file04"));
				if(strtolower($path_info['extension']) != 'pdf')
				{
					$error = 1;
				}
			}
			if($form1->value("posclosingassessment_file05"))
			{
				$path_info = pathinfo($form1->value("posclosingassessment_file05"));
				if(strtolower($path_info['extension']) != 'pdf')
				{
					$error = 1;
				}
			}
			if ($hr_costs > 0) {
				if($form1->value("posclosingassessment_file06"))
				{
					$path_info = pathinfo($form1->value("posclosingassessment_file06"));
					if(strtolower($path_info['extension']) != 'pdf')
					{
						$error = 1;
					}
				}
			}
			if($form1->value("posclosingassessment_file07"))
			{
				$path_info = pathinfo($form1->value("posclosingassessment_file07"));
				if(strtolower($path_info['extension']) != 'pdf')
				{
					$error = 1;
				}
			}
			if($form1->value("posclosingassessment_file08"))
			{
				$path_info = pathinfo($form1->value("posclosingassessment_file08"));
				if(strtolower($path_info['extension']) != 'pdf')
				{
					$error = 1;
				}
			}
			if($form1->value("posclosingassessment_file09"))
			{
				$path_info = pathinfo($form1->value("posclosingassessment_file09"));
				if(strtolower($path_info['extension']) != 'pdf')
				{
					$error = 1;
				}
			}
			if($form1->value("posclosingassessment_filesigned"))
			{
				$path_info = pathinfo($form1->value("posclosingassessment_filesigned"));
				if(strtolower($path_info['extension']) != 'pdf')
				{
					$error = 1;
				}
			}

			if($error == 1) 
			{
				$form->error("Please upload only PDF-files!");
			}
			else
			{
				$exchangerate = 0;
				$factor = 1;
				$sql = "select * from currencies where currency_id = " . dbquote($form->value("posclosingassessment_currency_id"));
				$res = mysql_query($sql) or dberror($sql);
				if($row = mysql_fetch_assoc($res))
				{
					$exchangerate = $row["currency_exchange_rate"];
					$factor = $row["currency_factor"];
				}


				$sql_u = "UPDATE posclosingassessments " . 
						 " SET " .
					     "posclosingassessment_contactperson = " . dbquote($form->value("posclosingassessment_contactperson")) . ", " .
					     "posclosingassessment_landlord = " . dbquote($form->value("posclosingassessment_landlord")) . ", " .
					     "posclosingassessment_lease_enddate = " . dbquote(from_system_date($form->value("posclosingassessment_lease_enddate"))) . ", " .
						 "posclosingassessment_planned_closingdate = " . dbquote(from_system_date($form->value("posclosingassessment_planned_closingdate"))) . ", " . 
						 "posclosingassessment_reason = " . dbquote($form->value("posclosingassessment_reason")) . ", " .
					 "posclosingassessment_image_consequence = " . dbquote($form->value("posclosingassessment_image_consequence")) . ", " .
					 "posclosingassessment_sales_consequence = " . dbquote($form->value("posclosingassessment_sales_consequence")) . ", " .
						 "posclosingassessment_legal_consequence = " . dbquote($form->value("posclosingassessment_legal_consequence")) . ", " .
					"posclosingassessment_furniture_consequence = " . dbquote($form->value("posclosingassessment_furniture_consequence")) . ", " .
					     "posclosingassessment_initial_investment = " . dbquote($form->value("posclosingassessment_initial_investment")) . ", " .
					      "posclosingassessment_book_value = " . dbquote($form->value("posclosingassessment_book_value")) . ", " .
					      "posclosingassessment_termination_costs = " . dbquote($form->value("posclosingassessment_termination_costs")) . ", " .
					"posclosingassessment_rental_costs = " . dbquote($form->value("posclosingassessment_rental_costs")) . ", " .
					      "posclosingassessment_operating_loss = " . dbquote($form->value("posclosingassessment_operating_loss")) . ", " .
					      "posclosingassessment_operating_loss_ws = " . dbquote($form->value("posclosingassessment_operating_loss_ws")) . ", " .
					     "posclosingassessment_depr_years = " . dbquote($form->value("posclosingassessment_depr_years")) . ", " .
						 "posclosingassessment_currency_id = " . dbquote($form->value("posclosingassessment_currency_id")) . ", " .
					     "posclosingassessment_exchangerate = " . $exchangerate . ", " .
					     "posclosingassessment_factor = " . $factor . ", " .
						 "posclosingassessment_terminationcost01 = " . dbquote($form1->value("posclosingassessment_terminationcost01")) . ", " .
						 "posclosingassessment_terminationcost02 = " . dbquote($form1->value("posclosingassessment_terminationcost02")) . ", " .
						 "posclosingassessment_terminationcost03 = " . dbquote($form1->value("posclosingassessment_terminationcost03")) . ", " .
						 "posclosingassessment_terminationcost04 = " . dbquote($form1->value("posclosingassessment_terminationcost04")) . ", " .
						 "posclosingassessment_terminationcost05 = " . dbquote($form1->value("posclosingassessment_terminationcost05")) . ", " .
					     "posclosingassessment_terminationcost06 = " . dbquote($form1->value("posclosingassessment_terminationcost06")) . ", " .
					"posclosingassessment_terminationcost07 = " . dbquote($form1->value("posclosingassessment_terminationcost07")) . ", " .
					"posclosingassessment_terminationcost08 = " . dbquote($form1->value("posclosingassessment_terminationcost08")) . ", " .
					"posclosingassessment_terminationcost09 = " . dbquote($form1->value("posclosingassessment_terminationcost09")) . ", " .
						 "posclosingassessment_terminationcost01_c = " . dbquote($form1->value("posclosingassessment_terminationcost01_c")) . ", " .
						 "posclosingassessment_terminationcost02_c = " . dbquote($form1->value("posclosingassessment_terminationcost02_c")) . ", " .
						 "posclosingassessment_terminationcost03_c = " . dbquote($form1->value("posclosingassessment_terminationcost03_c")) . ", " .
						 "posclosingassessment_terminationcost04_c = " . dbquote($form1->value("posclosingassessment_terminationcost04_c")) . ", " .
						 "posclosingassessment_terminationcost05_c = " . dbquote($form1->value("posclosingassessment_terminationcost05_c")) . ", " .
					     "posclosingassessment_terminationcost06_c = " . dbquote($form1->value("posclosingassessment_terminationcost06_c")) . ", " .
					"posclosingassessment_terminationcost07_c = " . dbquote($form1->value("posclosingassessment_terminationcost07_c")) . ", " .
					"posclosingassessment_terminationcost08_c = " . dbquote($form1->value("posclosingassessment_terminationcost08_c")) . ", " .
					"posclosingassessment_terminationcost09_c = " . dbquote($form1->value("posclosingassessment_terminationcost09_c")) . ", " .
						 "posclosingassessment_file01 = " . dbquote($form1->value("posclosingassessment_file01")) . ", " .
						 "posclosingassessment_file02 = " . dbquote($form1->value("posclosingassessment_file02")) . ", " .
						 "posclosingassessment_file03 = " . dbquote($form1->value("posclosingassessment_file03")) . ", " .
						 "posclosingassessment_file04 = " . dbquote($form1->value("posclosingassessment_file04")) . ", " .
						 "posclosingassessment_file05 = " . dbquote($form1->value("posclosingassessment_file05")) . ", " .
					     "posclosingassessment_file06 = " . dbquote($form1->value("posclosingassessment_file06")) . ", " .
					"posclosingassessment_file07 = " . dbquote($form1->value("posclosingassessment_file07")) . ", " .
					"posclosingassessment_file08 = " . dbquote($form1->value("posclosingassessment_file08")) . ", " .
					"posclosingassessment_file09 = " . dbquote($form1->value("posclosingassessment_file09")) . ", " .
						 "posclosingassessment_filesigned = " . dbquote($form1->value("posclosingassessment_filesigned")) . ", "  .
					     "posclosingassessment_approvaldate = " . dbquote(from_system_date($form1->value("posclosingassessment_approvaldate"))) . ", " .
						 "user_modified = " . dbquote(user_login()) . ", " .
						 "date_modified = " . dbquote(date("Y-m-d H:i:s")) . 
						 " where posclosingassessment_id = " . id();

				$result = mysql_query($sql_u) or dberror($sql_u);

				$v1 = $list->values("amount01");
				$v2 = $list->values("amount02");
				$v3 = $list->values("amount03");
				$v4 = $list->values("amount04");
				$v5 = $list->values("amount05");

				foreach ($v1 as $key=>$value)
				{
					
						$fields = array();
				
						$value = dbquote($v1[$key]);
						$fields[] = "posclosingassessmentfindate_amount01 = " . dbquote($v1[$key]);

						$value = dbquote($v2[$key]);
						$fields[] = "posclosingassessmentfindate_amount02 = " . dbquote($v2[$key]);

						$value = dbquote($v3[$key]);
						$fields[] = "posclosingassessmentfindate_amount03 = " . dbquote($v3[$key]);

						$value = dbquote($v4[$key]);
						$fields[] = "posclosingassessmentfindate_amount04 = " . dbquote($v4[$key]);

						$value = dbquote($v5[$key]);
						$fields[] = "posclosingassessmentfindate_amount05 = " . dbquote($v5[$key]);

						$value1 = "current_timestamp";
						$project_fields[] = "date_modified = " . $value1;
				
						if (isset($_SESSION["user_login"]))
						{
							$value1 = $_SESSION["user_login"];
							$project_fields[] = "user_modified = " . dbquote($value1);
						}
				   
						$sql = "update posclosingassessmentfindates set " . join(", ", $fields) . " where posclosingassessmentfindate_id =  " . $key;
						mysql_query($sql) or dberror($sql);
				}


				if($form1->button("submit") or $form1->button("resubmit"))
				{
					//check conditions for submission
					$error = 0;
					if(!$form->value("posclosingassessment_planned_closingdate")
						or !$form->value("posclosingassessment_reason")
						or !$form->value("posclosingassessment_legal_consequence")
						or !$form->value("posclosingassessment_currency_id")
					)
					{
						$form->error("You can not submit the closing assessment without filling all mandatory fields (*)!");
						$error = 1;
					}
					else
					{
						
						$v1 = $list->values("amount01");
						$v2 = $list->values("amount02");
						$v3 = $list->values("amount03");
						$v4 = $list->values("amount04");
						$v5 = $list->values("amount05");

						foreach ($v1 as $key=>$value)
						{
							if(!$v1[$key]) {$error = 1;}
							if(!$v2[$key]) {$error = 1;}
							if(!$v3[$key]) {$error = 1;}
							if(!$v4[$key]) {$error = 1;}
							if(!$v5[$key]) {$error = 1;}
						}
						if($error == 1) {
							$form->error("You can not submit the closing assessment without indicating the sales and historic information!");
						}
					}

					if($form1->value("posclosingassessment_terminationcost01") and !$form1->value("posclosingassessment_file01"))
					{
						$error = 1;
						$form->error("Please add an attachment explaining the unamortized fixed assets.");
					}
					if($form1->value("posclosingassessment_terminationcost02") and !$form1->value("posclosingassessment_file02"))
					{
						$error = 1;
						$form->error("Please add an attachment explaining the penalties.");
					}
					if ($broker_fees > 0) {
						if($form1->value("posclosingassessment_terminationcost03") and !$form1->value("posclosingassessment_file03"))
						{
							$error = 1;
							$form->error("Please add an attachment explaining the value of the broker fees.");
						}
					}

					if($form1->value("posclosingassessment_terminationcost04") and !$form1->value("posclosingassessment_file04"))
					{
						$error = 1;
						$form->error("Please add an attachment explaining the dismantling costs.");
					}
					if($form1->value("posclosingassessment_terminationcost05") and !$form1->value("posclosingassessment_file05"))
					{
						$error = 1;
						$form->error("Please add an attachment explaining the other costs.");
					}
					
					if ($hr_costs > 0) {
						if($form1->value("posclosingassessment_terminationcost06") and !$form1->value("posclosingassessment_file06"))
						{
							$error = 1;
							$form->error("Please add an attachment explaining the HR costs.");
						}
					}

					if($form1->value("posclosingassessment_terminationcost07") and !$form1->value("posclosingassessment_file07"))
					{
						$error = 1;
						$form->error("Please add an attachment explaining the Redundancy Cost.");
					}
					if($form1->value("posclosingassessment_terminationcost08") and !$form1->value("posclosingassessment_file08"))
					{
						$error = 1;
						$form->error("Please add an attachment explaining the Rent Contribution New Tenant.");
					}
					if($form1->value("posclosingassessment_terminationcost09") and !$form1->value("posclosingassessment_file09"))
					{
						$error = 1;
						$form->error("Please add an attachment explaining the loss of deposit.");
					}
										
					
					if($error == 0)
					{
						if($form1->button("submit"))
						{
							$sql_u = "update posclosingassessments SET " .
									 "posclosingassessment_submissiondate = " . dbquote(date("Y-m-d")) . ", " . 
									 "posclosingassessment_submissionuser = " . dbquote(user_id()) . 
									 " where posclosingassessment_id = " . id();
						}
						else
						{
							$sql_u = "update posclosingassessments SET " .
									 "posclosingassessment_resubmissiondate = " . dbquote(date("Y-m-d")) . ", " . 
									 "posclosingassessment_resubmissionuser = " . dbquote(user_id()) . 
									 " where posclosingassessment_id = " . id();
						}


						mysql_query($sql_u) or dberror($sql_u);


						//send mail alert
						$sql = "select user_email, concat(user_name, ' ', user_firstname) as user_name " . 
							 "from users " . 
							 " where user_id = " . user_id();

						$res = mysql_query($sql) or dberror($sql);
						$row = mysql_fetch_assoc($res);

						$sender_email = $row["user_email"];
						$sender_name = $row["user_name"];


						
						$brandmanager_email = "";
						$brandmanager_name = "";

						$sql = "select user_email, concat(user_name, ' ', user_firstname) as user_name " . 
							 "from posaddresses " .
							 "left join users on user_address = posaddress_client_id " . 
							 "left join user_roles on user_role_user = user_id " . 
							 " where user_role_role = 15 and user_active = 1 " . 
							 " and posaddress_id = " . $pos_data["posaddress_id"];


						$res = mysql_query($sql) or dberror($sql);
						if($row = mysql_fetch_assoc($res))
						{
							$brandmanager_email = $row["user_email"];
							$brandmanager_name = $row["user_name"];
						}



						
						$sql = "select * from mail_alert_types where mail_alert_type_id = 19";
						$res = mysql_query($sql) or dberror($sql);
						$row = mysql_fetch_assoc($res);
						
						$mail_subject = $row["mail_alert_type_shortcut"];
						$pos_mail_group = $row["mail_alert_type_shortcut"];
						$mail_text = $row["mail_alert_mail_text"];
						$mail_cc1 = $row["mail_alert_type_cc1"];
						$mail_cc2 = $row["mail_alert_type_cc2"];
						$mail_cc3 = $row["mail_alert_type_cc3"];
						$mail_cc4 = $row["mail_alert_type_cc4"];

						$subject = $mail_subject . " - " . $pos_data["posaddress_name"] . ", " . $pos_data["country_name"];

						$link = APPLICATION_URL . "/pos/pos_closing_assessment_pdf.php?id=" . id() . "&b=1";
						
						$mail_text = str_replace("{name}", $sender_name, $mail_text);
						$mail_text = str_replace("{link}", $link, $mail_text);


						$mail = new PHPMailer();
						$mail->Subject = $subject;
						$mail->SetFrom($sender_email, $sender_name);
						$mail->AddReplyTo($sender_email, $sender_name);

						       
						$mail->Body = $mail_text;

												
						$rcpts = "";
						if($brandmanager_email == $sender_email)
						{
							$mail->AddAddress($brandmanager_email);
							$rcpts .= $brandmanager_email . "\n";
						}
						elseif($brandmanager_email)
						{
							$mail->AddAddress($brandmanager_email);
							$rcpts .= $brandmanager_email . "\n";

							$mail->AddCC($sender_email);
							$rcpts .= $sender_email . "\n";
						}
						else
						{
							$mail->AddAddress($sender_email);
							$rcpts .= $sender_email . "\n";
						}


						//regional sales manager
						$regional_sales_manager_email = "";
						$regional_sales_manager_name = "";

						$direct_regional_sales_manager_email = "";

						$sql = "select user_email, concat(user_name, ' ', user_firstname) as user_name " . 
							 "from posaddresses " .
							 "left join users on user_address = posaddress_client_id " . 
							 "left join user_roles on user_role_user = user_id " . 
							 " where user_role_role = 33 and user_active = 1 " . 
							 " and posaddress_id = " . $pos_data["posaddress_id"];


						$res = mysql_query($sql) or dberror($sql);
						if($row = mysql_fetch_assoc($res))
						{
							$regional_sales_manager_email = $row["user_email"];
							$regional_sales_manager_name = $row["user_name"];

							$direct_regional_sales_manager_email = $row["user_email"];

							if($regional_sales_manager_email 
								and $regional_sales_manager_email != $sender_email 
								and $regional_sales_manager_email != $brandmanager_email)
							{
								$mail->AddCC($regional_sales_manager_email);
								$rcpts .= $regional_sales_manager_email . "\n";
							}
						}

						if(!$regional_sales_manager_email)
						{
						
							$sql = "select user_email, concat(user_name, ' ', user_firstname) as user_name " . 
								 "from posaddresses " .
								 "left join country_access on country_access_country = posaddress_country " . 
								 "left join user_roles on user_role_user = country_access_user " .
								 "left join users on user_id = country_access_user " .
								 " where user_role_role = 33 and user_active = 1 " .
								 " and posaddress_id = " . $pos_data["posaddress_id"];

							$res = mysql_query($sql) or dberror($sql);
							while($row = mysql_fetch_assoc($res))
							{
								$regional_sales_manager_email = $row["user_email"];
								$regional_sales_manager_name = $row["user_name"];

								if($regional_sales_manager_email 
								and $regional_sales_manager_email != $sender_email 
								and $regional_sales_manager_email != $brandmanager_email
								and $regional_sales_manager_email != $direct_regional_sales_manager_email)
								{
									$mail->AddCC($regional_sales_manager_email);
									$rcpts .= $regional_sales_manager_email . "\n";
								}
							}
						}
					
						

						if($mail_cc1)
						{
							$mail->AddCC($mail_cc1);
							$rcpts .= $mail_cc1 . "\n";
						}

						if($mail_cc2)
						{
							$mail->AddCC($mail_cc2);
							$rcpts .= $mail_cc2 . "\n";
						}

						if($mail_cc3)
						{
							$mail->AddCC($mail_cc3);
							$rcpts .= $mail_cc3 . "\n";
						}

						if($mail_cc4)
						{
							$mail->AddCC($mail_cc4);
							$rcpts .= $mail_cc4 . "\n";
						}
						
						$mail->Send();

						//update mail history
						$pos_mail_fields = array();
						$pos_mail_values = array();

						$pos_mail_fields[] = "posmail_posaddress_id";
						$pos_mail_values[] = dbquote($pos_data["posaddress_id"]);;

						$pos_mail_fields[] = "posmail_sender_email";
						$pos_mail_values[] = dbquote($sender_email);;


						$pos_mail_fields[] = "posmail_recipeint_email";
						$pos_mail_values[] = dbquote($rcpts);

						$pos_mail_fields[] = "posmail_subject";
						$pos_mail_values[] = dbquote($subject);

						$pos_mail_fields[] = "posmail_text";
						$text = str_replace("\n\r", "<br>" , $mail_text);
						$text = str_replace("\n", "<br>" , $text);
						$text = str_replace("\r", "<br>" , $text);
						
						$pos_mail_values[] = dbquote($text);

						$pos_mail_fields[] = "date_created";
						$pos_mail_values[] = "current_timestamp";

						$pos_mail_fields[] = "user_created";
						$pos_mail_values[] = dbquote($_SESSION["user_login"]);


						$sql = "insert into posmails (" . join(", ", $pos_mail_fields) . ") values (" . join(", ", $pos_mail_values) . ")";
						mysql_query($sql) or dberror($sql);



						$link = "pos_closing_assessment.php?id=" . id();
						redirect($link);
					}
				}
				else
				{
					$form->message("Your data has been saved!");
				}
			}
		}
	}
	elseif($form1->button("delete"))
	{
		
		$sql = "delete from posclosingassessmentfindates ". 
			   "where posclosingassessmentfindate_assessment_id = " . id();

		mysql_query($sql) or dberror($sql);


		$sql = "delete from posclosingassessments ". 
			   "where posclosingassessment_id = " . id();

		mysql_query($sql) or dberror($sql);

		$link = "pos_closing_assessments.php";
		redirect($link);


	}
	elseif($form1->button("savef"))
	{
		$error = 0;
		if(!$form1->value("posclosingassessment_approvaldate"))
		{
			$error = 1;
			$form->error("Please enter an approval date!");
		}
		if($form1->value("posclosingassessment_filesigned"))
		{
			$path_info = pathinfo($form1->value("posclosingassessment_filesigned"));
			if(strtolower($path_info['extension']) != 'pdf')
			{
				$error = 1;
				$form->error("Please upload only PDF-files!");
			}
		}

		if($error == 1) 
		{
			
		}
		else
		{
			$sql_u = "UPDATE posclosingassessments " . 
					 " SET " .
					 "posclosingassessment_filesigned = " . dbquote($form1->value("posclosingassessment_filesigned")) . ", "  .
				     "posclosingassessment_approvaldate = " . dbquote(from_system_date($form1->value("posclosingassessment_approvaldate"))) . ", " .
					 "user_modified = " . dbquote(user_login()) . ", " .
					 "date_modified = " . dbquote(date("Y-m-d H:i:s")) . 
					 " where posclosingassessment_id = " . id();

			$result = mysql_query($sql_u) or dberror($sql_u);

			
			$form->message("Your data has been saved!");
		}
	}
	

}





// Render page

$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title(id() ? "Edit POS Closing Assessment" : "Add POS Closing Assessment");
$form->render();

?>

<div id="lease_startdate" style="display:none;">
    Please indicate the date like:<br />
	<strong>dd.mm.yy</strong><br />
</div> 

<div id="lease_enddate" style="display:none;">
    Please indicate the date like:<br />
	<strong>dd.mm.yy</strong><br />
</div> 

<?php

if(id() > 0)
{
	$list->render();
	$form1->render();
}

$page->footer();

?>