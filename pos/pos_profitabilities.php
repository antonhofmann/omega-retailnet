<?php
/********************************************************************

    pos_profitabilities.php

    Lists of addresses (POS)

    reated by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2013-06-16
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2013-06-16
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

if(!has_access("has_access_to_pos_profitability") 
	and !has_access("can_edit_pos_profitability_of_his_pos")
	and !has_access("can_edit_pos_profitability_of_all_pos"))
{
	redirect("/pos");
}

$user = get_user(user_id());

$postype_filter = array();
$postype_filter["all"] = "All";
$sql = "select * from postypes order by postype_name";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$postype_filter[$row["postype_id"]] = $row["postype_name"];
}


$preselect_filter = "";
if(param("country"))
{
	$preselect_filter = "posaddress_country = " . param("country");
	register_param("country", param("country"));
}
else
{
	redirect("welcome.php");
}

if(param("province") and $preselect_filter)
{
	$preselect_filter .= " and place_province = " . param("province");
	register_param("province", param("province"));
}
elseif(param("province"))
{
	$preselect_filter = "place_province = " . param("province");
	register_param("province", param("province"));
}


$preselect_filter .= " and (posaddress_store_closingdate = '0000-00-00' or posaddress_store_closingdate is null) ";



if( has_access("can_edit_pos_profitability_of_his_pos"))
{
	if(!param("country"))
	{
		redirect("welcome.php");
	}

	$country_filter = "";
	$tmp = array();
	$sql = "select * from country_access " .
		   "where country_access_user = " . user_id();


	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{            
		$tmp[] = $row["country_access_country"];
	}

	if(count($tmp) > 0) {
		$country_filter = " posaddress_country IN (" . implode(",", $tmp) . ") ";
	}

	if($country_filter) 
	{
		if($preselect_filter)
		{
			$preselect_filter .= " and (posaddress_country = " . $user["country"] . " or " . $country_filter . ") "; 
		}
		else
		{
			$preselect_filter = " and (posaddress_country = " . $user["country"] . " or " . $country_filter . ") ";
		}
	}
	else
	{
		if($preselect_filter)
		{
			//$preselect_filter .= " and (posaddress_country = " . $user["country"];
		}
		else
		{
			$preselect_filter .= " and (posaddress_country = " . $user["country"];
		}
	}
}


if(has_access("has_access_to_retail_pos_only")) {
	$preselect_filter = "(" . $preselect_filter . ") and posaddress_ownertype in (1) ";
}

if(has_access("has_access_to_wholesale_pos")) {
	$preselect_filter = "(" . $preselect_filter . ") and posaddress_ownertype in (2,6) ";
}

//compose list

$sql = "select posaddress_id, if(posaddress_name <> '', posaddress_name, 'n.a.') as posname, " .
       "posaddress_address, posaddress_address2, posaddress_zip, " .
       "    posaddress_place, country_name, project_costtype_text, postype_name, " .
	   "posaddress_google_precision, posaddress_store_closingdate, province_canton, posaddress_country " . 
       "from posaddresses " .
	   "left join places on place_id = posaddress_place_id " .
	   "left join provinces on province_id = place_province " .
	   "left join countries on posaddress_country = country_id " . 
	   "left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
	   "left join postypes on postype_id = posaddress_store_postype ";


if(param('ltf')) 
{
	if($preselect_filter) {
		
		if(param('ltf') == "all") {
		}
		else
		{
			$preselect_filter .= " and posaddress_store_postype = " . param('ltf');
		}
	}
	else
	{
		if(param('ltf') == "all") {
		}
		else
		{
			$preselect_filter = "posaddress_store_postype = " . param('ltf');
		}
	}
}
else
{
	if($preselect_filter) {
		//$preselect_filter .= " and posaddress_store_postype <> 4";
	}
	else
	{
		//$preselect_filter = "posaddress_store_postype <> 4";
	}
}


/********************************************************************
    Create Form
*********************************************************************/

$form = new Form("posaddresses", "posaddress");

$form->add_section("List Filter Selection");

$form->add_list("ltf", "POS Type Filter", $postype_filter, SUBMIT | NOTNULL, param('ltf'));



$list = new ListView($sql);

$list->set_entity("posaddresses");
$list->set_filter($preselect_filter);
$list->set_order("country_name, posaddress_place, posaddress_name");

$list->add_hidden("country", param("country"));
$list->add_hidden("province", param("province"));


$list->add_column("country_name", "Country", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);
$list->add_column("province_canton", "Province", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("posaddress_place", "City", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("posname", "POS Name", "pos_profitability.php?pos_id={posaddress_id}&country=" . param("country") . '&province=' . param("province"). '&let=' . param('let') . '&ltf=' . param("ltf") . "&mode=1", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("posaddress_address", "Address", "", LIST_FILTER_FREE);
$list->add_column("postype_name", "POS Type", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);
$list->add_column("project_costtype_text", "Legal Type", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);


$list->add_button(FORM_BUTTON_BACK, "Back");
$list->populate();
$list->process();



$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title("POS Index: Profitability");
$form->render();
$list->render();


$page->footer();

?>
