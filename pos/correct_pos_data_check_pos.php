<?php
/********************************************************************

    correct_pos_data_check_pos.php

    Check Corrections POS Data and transfer to POS Index

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

/********************************************************************
    get neighbourhood data
*********************************************************************/
function get_neighbourhood($posaddress_id, $table)
{

	$posareas = "";

    $sql = "select * " .
           "from $table ".
           "where posarea_posaddress  = " . dbquote($posaddress_id);


    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        $posareas .= $row["posarea_area"];
    }

	return $posareas;
}

/********************************************************************
    get equipment data
*********************************************************************/
function get_equipment($posaddress_id, $table)
{

	$posequipment = "";

    $sql = "select * " .
           "from $table ".
           "where posequipment_posaddress  = " . dbquote($posaddress_id);


    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        $posequipment .= $row["posequipment_equipment"];
    }

	return $posequipment;
}


/********************************************************************
    compare POS Data and corrected POS Data
*********************************************************************/
function compare($array1, $array2)
{ 
	//if(count($array1)!= count($array2)) return false; 
	foreach($array1 as $name => $value)
	{ 
		if($name != "posaddress_data_check")
		{
			if($array1[$name] != $array2[$name]) 
			{	
				$output[$name] = 1;
			}
			else
			{
				$output[$name] = 0; 
			}
		}
	} 
	return $output; 
}


require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";


check_access("can_accept_pos_data_corrections");

set_referer("posprojects.php");

if(param("pos_id"))
{
	register_param("pos_id");
	param("id", param("pos_id"));
}
else
{
	register_param("pos_id");
	param("pos_id", id());
}

if(param("pos_id") > 0)
{
	$pos = get_poslocation(param("pos_id"), "posaddresses");
	$newpos = get_poslocation(param("pos_id"), "_posaddresses");

	$posareas = get_neighbourhood(param("pos_id"), "posareas");
	$newposareas = get_neighbourhood(param("pos_id"), "_posareas");

	$posequipment = get_equipment(param("pos_id"), "posequipments");
	$newposequipment = get_equipment(param("pos_id"), "_posequipments");

}


//get provinces
if(param("posaddress_country"))
{
	$sql_provinces = "select province_id, province_canton from provinces where province_country = " . dbquote(param("posaddress_country")) . " order by province_canton";
}
else
{
	$sql_provinces = "select province_id, province_canton from provinces where province_country = " . dbquote($newpos["posaddress_country"]) . " order by province_canton";
}
$res = mysql_query($sql_provinces);
while ($row = mysql_fetch_assoc($res))
{
	$provinces[$row["province_id"]] = $row["province_canton"];
}


$difference_in_pos_address = compare($pos, $newpos);
$difference_in_pos_areas = $posareas - $newposareas;
$difference_in_equipment = $posequipment - $newposequipment;


//copy record to table posaddresses_corrected

//sql for subclasses
$sql_subclasses = "select possubclass_id, possubclass_name " .
				  "from postype_subclasses " .
				  "left join possubclasses on possubclass_id = postype_subclass_subclass " .
				  "left join product_line_pos_types on  product_line_pos_type_id = postype_subclass_pl_pos_type " . 
				  "where  product_line_pos_type_pos_type = " . $pos["posaddress_store_postype"] .
				  " and product_line_pos_type_product_line = " . $pos["posaddress_store_furniture"] .
				  " order by possubclass_name";

$ratings = array();
$ratings[5] = "bad";
$ratings[4] = "poor";
$ratings[3] = "good";
$ratings[2] = "very good";
$ratings[1] = "excellent";
$ratings[0] = "";

//check if user can edit this company
$can_edit = false;
if(has_access("can_edit_posindex"))
{
	$can_edit = true;
}
elseif(has_access("can_edit_his_posindex"))
{
	$can_edit = get_user_edit_permission(user_id(), param("pos_id"));
}

// Build form
$form = new Form("_posaddresses", "posaddress");

$form->add_section("Roles");

$form->add_lookup("posaddress_client_id", "Client", "addresses", "concat(address_company, ', ', address_place, ' [', address_shortcut, ']') as company", 0, $pos["posaddress_client_id"]);

$form->add_lookup("posaddress_ownertype", "Legal Type", "project_costtypes", "project_costtype_text", 0, $pos["posaddress_ownertype"]);
$form->add_lookup("posaddress_store_furniture", "Product Line", "product_lines", "product_line_name");
$form->add_lookup("posaddress_store_furniture_subclass", "Product Line Subclass", "productline_subclasses", "productline_subclass_name");
$form->add_lookup("posaddress_store_postype", "POS Type", "postypes", "postype_name");

if($pos['posaddress_store_postype'] < 4)
{
	if($difference_in_pos_address["posaddress_store_subclass"] == 1)
	{
		$form->add_list("posaddress_store_subclass", "<span class='error'>POS Type Subclass</span>", $sql_subclasses);
	}
	else
	{
		$form->add_list("posaddress_store_subclass", "POS Type Subclass", $sql_subclasses);
	}
}
else
{
		$form->add_hidden("posaddress_store_subclass");
}
//$form->add_lookup("posaddress_store_subclass", "POS Type Subclass", "possubclasses", "possubclass_name");

if($pos['posaddress_store_postype'] < 4)
{
	$form->add_lookup("posaddress_franchisor_id", "Franchisor", "addresses", "concat(address_company, ', ', address_place) as company", 0, $pos["posaddress_franchisor_id"]);
}
else
{
	$form->add_hidden("posaddress_franchisor_id");
}


if($pos['posaddress_store_postype'] < 4)
{
	if($difference_in_pos_address["posaddress_franchisee_id"] == 1)
	{
		$form->add_list("posaddress_franchisee_id", "<span class='error'>Franchisee*</span>",
				"select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where address_showinposindex = 1 and country_name <> '' and address_company <> '' and address_place <> '' and address_canbefranchisee = 1 order by country_name, address_company", NOTNULL);
	}
	else
	{
		$form->add_list("posaddress_franchisee_id", "Franchisee*",
				"select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where address_showinposindex = 1 and country_name <> '' and address_company <> '' and address_place <> '' and address_canbefranchisee = 1 order by country_name, address_company", NOTNULL);
	}
}
else
{
	
	$sql_owners = "select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company " . 
			          " from addresses " . 
			          " left join countries on country_id = address_country " . 
			          " where address_showinposindex = 1 " . 
			          " and address_active = 1 " . 
					  " and country_name <> '' and address_company <> '' and address_place <> '' " . 
			          " and address_type = 7 order by country_name, address_company";

	
	if($difference_in_pos_address["posaddress_franchisee_id"] == 1)
	{
		$form->add_list("posaddress_franchisee_id", "<span class='error'>Franchisee*</span>",
				$sql_owners, NOTNULL);
	}
	else
	{
		$form->add_list("posaddress_franchisee_id", "Franchisee*", $sql_owners, NOTNULL);
	}
}

$form->add_section("Address Data");
if($difference_in_pos_address["posaddress_name"] == 1)
{
	$form->add_edit("posaddress_name", "<span class='error'>POS Name*</span>", NOTNULL);
}
else
{
	$form->add_edit("posaddress_name", "POS Name*", NOTNULL);
}

if($difference_in_pos_address["posaddress_name2"] == 1)
{
	$form->add_edit("posaddress_name2", "<span class='error'>POS Name2</span>");
}
else
{
	$form->add_edit("posaddress_name2", "POS Name2");
}

if($difference_in_pos_address["posaddress_address"] == 1)
{
	$form->add_edit("posaddress_address", "<span class='error'>Address*</span>", NOTNULL);
}
else
{
	$form->add_edit("posaddress_address", "Address*", NOTNULL);
}

if($difference_in_pos_address["posaddress_address2"] == 1)
{
	$form->add_edit("posaddress_address2", "<span class='error'>Address 2</span>");
}
else
{
	$form->add_edit("posaddress_address2", "Address 2");
}

if($difference_in_pos_address["posaddress_country"] == 1)
{
	$form->add_list("posaddress_country", "<span class='error'>Country*</span>",
	"select country_id, country_name from countries order by country_name", NOTNULL | SUBMIT);
}
else
{
	$form->add_list("posaddress_country", "Country*",
	"select country_id, country_name from countries order by country_name", NOTNULL);
}

if($difference_in_pos_address["posaddress_place_id"] == 1)
{
	
	if(param("posaddress_country"))
	{
		$form->add_list("posaddress_place_id", "<span class='error'>City*</span>",
			"select place_id, place_name from places where place_country = " . param("posaddress_country") . " order by place_name", NOTNULL | SUBMIT);
	}
	else
	{
		$form->add_list("posaddress_place_id", "<span class='error'>City*</span>",
			"select place_id, place_name from places where place_country = " . $newpos["posaddress_country"] . " order by place_name", NOTNULL | SUBMIT, $newpos["posaddress_place_id"]);
	}
	$form->add_hidden("posaddress_place", $newpos["posaddress_place"]);
}
else
{
	if(param("posaddress_country"))
	{
				$form->add_list("posaddress_place_id", "City*",
			"select place_id, place_name from places where place_country = " . param("posaddress_country") . " order by place_name", NOTNULL | SUBMIT);

	}
	else
	{
		$form->add_list("posaddress_place_id", "City*",
			"select place_id, place_name from places where place_country = " . $newpos["posaddress_country"] . " order by place_name", NOTNULL | SUBMIT, $newpos["posaddress_place_id"]);
	}
	$form->add_hidden("posaddress_place", $newpos["posaddress_place"]);
}

if($difference_in_pos_address["posaddress_zip"] == 1)
{
	$form->add_edit("posaddress_zip", "<span class='error'>Zip*</span>", NOTNULL);
}
else
{
	$form->add_edit("posaddress_zip", "Zip*", NOTNULL);
}



if($difference_in_pos_address["posaddress_place_id"] == 1)
{
	$form->add_list("select_province", "<span class='error'>Province*</span>", $provinces, NOTNULL | SUBMIT, $newpos["place_province"]);
}
else
{
	$form->add_list("select_province", "Province Selection*", $provinces, NOTNULL | SUBMIT, $newpos["place_province"]);
}


$form->add_section("Communication");

if($difference_in_pos_address["posaddress_phone"] == 1)
{
	$form->add_edit("posaddress_phone", "<span class='error'>Phone*</span>", NOTNULL);
}
else
{
	$form->add_edit("posaddress_phone", "Phone*", NOTNULL);
}

if($difference_in_pos_address["posaddress_fax"] == 1)
{
	$form->add_edit("posaddress_fax", "<span class='error'>Fax</span>");
}
else
{
	$form->add_edit("posaddress_fax", "Fax");
}

if($difference_in_pos_address["posaddress_email"] == 1)
{
	$form->add_edit("posaddress_email", "<span class='error'>Email*</span>", NOTNULL);
}
else
{
	$form->add_edit("posaddress_email", "Email*", NOTNULL);
}

if($pos['posaddress_store_postype'] < 4)
{
	$form->add_section("Dates");
	if($difference_in_pos_address["posaddress_store_openingdate"] == 1)
	{
		$form->add_edit("posaddress_store_openingdate", "<span class='error'>Opening Date*</span>", NOTNULL, "", TYPE_DATE);
	}
	else
	{
		$form->add_edit("posaddress_store_openingdate", "Opening Date*", NOTNULL, "", TYPE_DATE);
	}
}
else
{
	$form->add_section("Sales Agreement");
	if($difference_in_pos_address["posaddress_store_openingdate"] == 1)
	{
		$form->add_edit("posaddress_store_openingdate", "<span class='error'>Opening Date</span>", 0, "", TYPE_DATE);
	}
	else
	{
		$form->add_edit("posaddress_store_openingdate", "Opening Date", 0, "", TYPE_DATE);
	}
}



if($difference_in_pos_address["posaddress_store_closingdate"] == 1)
{
	$form->add_edit("posaddress_store_closingdate", "<span class='error'>Closing Date</span>", 0, "", TYPE_DATE);
}
else
{
	$form->add_edit("posaddress_store_closingdate", "Closing Date", 0, "", TYPE_DATE);
}

$form->add_section("Environment");
$sql = "select posareatype_id, posareatype_name " . 
		   "from posareatypes " . 
		   " order by posareatype_name";

if($difference_in_pos_areas)
{
	$form->add_checklist("area", "<span class='error'>Environment</span>", "_posareas",$sql, RENDER_HTML);
}
else
{
	$form->add_checklist("area", "Environment", "_posareas",$sql, RENDER_HTML);
}



$form->add_section("Area Perception");
$form->add_comment("Please rate by clicking a star: 5 stars are the best rating.");

if($difference_in_pos_address["posaddress_perc_class"] == 1)
{
	$form->add_rating_selector("posaddress_perc_class", "<span class='error'>Class/Image Area*</span>", $ratings, $ratings[$newpos["posaddress_perc_class"]], NOTNULL);
}
else
{
	$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, $ratings[$newpos["posaddress_perc_class"]], NOTNULL);
}

if($difference_in_pos_address["posaddress_perc_tourist"] == 1)
{
	$form->add_rating_selector("posaddress_perc_tourist", "<span class='error'>Tourist/Historical Area*</span>", $ratings, $ratings[$newpos["posaddress_perc_tourist"]], NOTNULL);
}
else
{
	$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, $ratings[$newpos["posaddress_perc_tourist"]], NOTNULL);
}

if($difference_in_pos_address["posaddress_perc_transport"] == 1)
{
	$form->add_rating_selector("posaddress_perc_transport", "<span class='error'>Public Transportation*</span>", $ratings, $ratings[$newpos["posaddress_perc_transport"]], NOTNULL);
}
else
{
	$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings, $ratings[$newpos["posaddress_perc_transport"]], NOTNULL);
}

if($difference_in_pos_address["posaddress_perc_people"] == 1)
{
	$form->add_rating_selector("posaddress_perc_people", "<span class='error'>People Traffic Area*</span>", $ratings, $ratings[$newpos["posaddress_perc_people"]], NOTNULL);
}
else
{
	$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings, $ratings[$newpos["posaddress_perc_people"]], NOTNULL);
}

if($difference_in_pos_address["posaddress_perc_parking"] == 1)
{
	$form->add_rating_selector("posaddress_perc_parking", "<span class='error'>Parking Possibilities*</span>", $ratings, $ratings[$newpos["posaddress_perc_parking"]], NOTNULL);
}
else
{
	$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings, $ratings[$newpos["posaddress_perc_parking"]], NOTNULL);
}

if($difference_in_pos_address["posaddress_perc_visibility1"] == 1)
{
	$form->add_rating_selector("posaddress_perc_visibility1", "<span class='error'>Visibility from Pavement*</span>", $ratings, $ratings[$newpos["posaddress_perc_visibility1"]], NOTNULL);
}
else
{
	$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings, $ratings[$newpos["posaddress_perc_visibility1"]], NOTNULL);
}

if($difference_in_pos_address["posaddress_perc_visibility2"] == 1)
{
	$form->add_rating_selector("posaddress_perc_visibility2", "<span class='error'>Visibility from accross the Street*</span>", $ratings, $ratings[$newpos["posaddress_perc_visibility2"]], NOTNULL);
}
else
{
	$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from accross the Street*", $ratings, $ratings[$newpos["posaddress_perc_visibility2"]], NOTNULL);
}


//$form->add_section("----------------------------------------------------------------------");
//$form->add_checkbox("checked2", "Address", "", 0, "revised");

$form->add_hidden("country", param("country"));


$form->add_section("POS Details Surfaces");

if($pos['posaddress_store_postype'] < 4)
{
	if($difference_in_pos_address["posaddress_store_grosssurface"] == 1)
	{
		$form->add_edit("posaddress_store_grosssurface", "<span class='error'>Gross Surface in sqms*</span>", NOTNULL);
	}
	else
	{
		$form->add_edit("posaddress_store_grosssurface", "Gross Surface in sqms*", NOTNULL);
	}

	if($difference_in_pos_address["posaddress_store_totalsurface"] == 1)
	{
		$form->add_edit("posaddress_store_totalsurface", "<span class='error'>Total Surface in sqms*</span>", NOTNULL);
	}
	else
	{
		$form->add_edit("posaddress_store_totalsurface", "Total Surface in sqms*", NOTNULL);
	}

	if($difference_in_pos_address["posaddress_store_retailarea"] == 1)
	{
		$form->add_edit("posaddress_store_retailarea", "<span class='error'>Sales Surface in sqms*</span>", NOTNULL);
	}
	else
	{
		$form->add_edit("posaddress_store_retailarea", "Sales Surface in sqms*", NOTNULL);
	}

	if($difference_in_pos_address["posaddress_store_backoffice"] == 1)
	{
		$form->add_edit("posaddress_store_backoffice", "<span class='error'>Back Office Surface in sqms</span>", 0);
	}
	else
	{
		$form->add_edit("posaddress_store_backoffice", "Back Office Surface in sqms", 0);
	}

	if($difference_in_pos_address["posaddress_store_numfloors"] == 1)
	{
		$form->add_edit("posaddress_store_numfloors", "<span class='error'>Number of Floors*</span>", NOTNULL);
	}
	else
	{
		$form->add_edit("posaddress_store_numfloors", "Number of Floors*", NOTNULL);
	}

	if($difference_in_pos_address["posaddress_store_floorsurface1"] == 1)
	{
		$form->add_edit("posaddress_store_floorsurface1", "<span class='error'>Floor 1: Surface  in sqms</span>", 0);
	}
	else
	{
		$form->add_edit("posaddress_store_floorsurface1", "Floor 1: Surface  in sqms", 0);
	}

	if($difference_in_pos_address["posaddress_store_floorsurface2"] == 1)
	{
		$form->add_edit("posaddress_store_floorsurface2", "<span class='error'>Floor 2: Surface  in sqms</span>", 0);
	}
	else
	{
		$form->add_edit("posaddress_store_floorsurface2", "Floor 2: Surface  in sqms", 0);
	}

	if($difference_in_pos_address["posaddress_store_floorsurface3"] == 1)
	{
		$form->add_edit("posaddress_store_floorsurface3", "<span class='error'>Floor 3: Surface  in sqms</span>", 0);
	}
	else
	{
		$form->add_edit("posaddress_store_floorsurface3", "Floor 3: Surface  in sqms", 0);
	}

	$form->add_section("POS Details Staff");
	if($difference_in_pos_address["posaddress_store_headcounts"] == 1)
	{
		$form->add_edit("posaddress_store_headcounts", "<span class='error'>Headcounts*</span>", NOTNULL, "", TYPE_INT);
	}
	else
	{
		$form->add_edit("posaddress_store_headcounts", "Headcounts*", NOTNULL, "", TYPE_INT);
	}

	if($difference_in_pos_address["posaddress_store_fulltimeeqs"] == 1)
	{
		$form->add_edit("posaddress_store_fulltimeeqs", "<span class='error'>Full Time Equivalents*</span>", NOTNULL, "", TYPE_INT);
	}
	else
	{
		$form->add_edit("posaddress_store_fulltimeeqs", "Full Time Equivalents*", NOTNULL, "", TYPE_INT);
	}
}
else
{
	
	if($difference_in_pos_address["posaddress_overall_sqms"] == 1)
	{
		$form->add_edit("posaddress_overall_sqms", "<span class='error'>Retailers overall sales surface</span>", 0, "", TYPE_DECIMAL, 8);
	}
	else
	{
		$form->add_edit("posaddress_overall_sqms", "Retailers overall sales surface", 0, "", TYPE_DECIMAL, 8);
	}

	if($difference_in_pos_address["posaddress_dedicated_sqms_wall"] == 1)
	{
		$form->add_edit("posaddress_dedicated_sqms_wall", "<span class='error'>Sales wall surface dedicated to " . BRAND . "</span>", 0, "", TYPE_DECIMAL, 8);
	}
	else
	{
		$form->add_edit("posaddress_dedicated_sqms_wall", "Sales wall surface dedicated to " . BRAND, 0, "", TYPE_DECIMAL, 8);
	}

	if($difference_in_pos_address["posaddress_dedicated_sqms_free"] == 1)
	{
		$form->add_edit("posaddress_dedicated_sqms_free", "<span class='error'>Sales freestanding surface dedicated to " . BRAND . "</span>", 0, "", TYPE_DECIMAL, 8);
	}
	else
	{
		$form->add_edit("posaddress_dedicated_sqms_free", "Sales freestanding surface dedicated to " . BRAND, 0, "", TYPE_DECIMAL, 8);
	}
	
}


$form->add_section("Google Data");

if($difference_in_pos_address["posaddress_google_lat"] == 1)
{
	$form->add_edit("posaddress_google_lat", "<span class='error'>Latitude*</span>", NOTNULL, "", TYPE_DECIMAL, 20, 15);
}
else
{
	$form->add_edit("posaddress_google_lat", "Latitude*", NOTNULL, "", TYPE_DECIMAL, 20, 15);
}

if($difference_in_pos_address["posaddress_google_long"] == 1)
{
	$form->add_edit("posaddress_google_long", "<span class='error'>Latitude*</span>", NOTNULL, "", TYPE_DECIMAL, 20, 15);
}
else
{
	$form->add_edit("posaddress_google_long", "Latitude*", NOTNULL, "", TYPE_DECIMAL, 20, 15);
}


$form->add_section("Website " . BRAND_WEBSITE);

if($difference_in_pos_address["posaddress_export_to_web"] == 1)
{
	$form->add_checkbox("posaddress_export_to_web", "Show this POS in the Store Locator", "", "", "<span class='error'>" . BRAND_WEBSITE . "</span>");
}
else
{
	$form->add_checkbox("posaddress_export_to_web", "Show this POS in the Store Locator", "", "", BRAND_WEBSITE);
}

if($difference_in_pos_address["posaddress_email_on_web"] == 1)
{
	$form->add_checkbox("posaddress_email_on_web", "Show Email Address in the Store Locator", "", "", "<span class='error'>" . BRAND_WEBSITE . "</span>");
}
else
{
	$form->add_checkbox("posaddress_email_on_web", "Show Email Address in the Store Locator", "", "", BRAND_WEBSITE);
}


$form->add_button("remove", "Remove Record");
$form->add_button(FORM_BUTTON_SAVE, "Accepts Corrections");
$form->add_button("back", "Back");

// Populate form and process button clicks

$form->populate();
$form->process();



/********************************************************************
    list leases
*********************************************************************/
// create sql
$sql = "select poslease_id, poslease_lease_type, poslease_landlord_name, poslease_startdate, poslease_enddate, poslease_type_name, " .
       "poslease_nexttermin_date, poslease_anual_rent, poslease_currency, poslease_salespercent, poslease_termination_time, " .
	   "poslease_startdate, poslease_enddate, poslease_extensionoption, poslease_exitoption " .
       "from _posleases " .
	   "left join poslease_types on poslease_type_id = poslease_lease_type ";

$list_filter = "poslease_posaddress = " . param("pos_id");

//get data
$lease_types = array();
$lease_start_dates = array();
$lease_end_dates = array();
$lease_extension_dates = array();
$lease_extit_dates = array();
$lease_termination_times = array();
$lease_currencies = array();
$lease_rents = array();
$lease_salespercent = array();
$sql_g = $sql . " where " . $list_filter;
$res = mysql_query($sql_g);
while ($row = mysql_fetch_assoc($res))
{
	$lease_types[$row["poslease_id"]] = $row["poslease_lease_type"];
	$lease_start_dates[$row["poslease_id"]] = to_system_date($row["poslease_startdate"]);
	$lease_end_dates[$row["poslease_id"]] = to_system_date($row["poslease_enddate"]);
	$lease_extension_dates[$row["poslease_id"]] = to_system_date($row["poslease_extensionoption"]);
	$lease_extit_dates[$row["poslease_id"]] = to_system_date($row["poslease_exitoption"]);
	$lease_termination_times[$row["poslease_id"]] = $row["poslease_termination_time"];
	$lease_currencies[$row["poslease_id"]] = $row["poslease_currency"];
	$lease_rents[$row["poslease_id"]] = $row["poslease_anual_rent"];
	$lease_salespercent[$row["poslease_id"]] = $row["poslease_salespercent"];
}

/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);
$list->set_entity("_posleases");
$list->set_order("poslease_startdate, poslease_type_name");
$list->set_filter($list_filter);
$list->set_title("Lease Data");

$list->add_hidden("pos_id", param("pos_id"));
$list->add_list_column("poslease_lease_type", "Type", "select poslease_type_id, poslease_type_name from poslease_types", NOTNULL, $lease_types);
$list->add_edit_column("start", "Start Date", 10, NOTNULL, $lease_start_dates);
$list->add_edit_column("end", "End Date", 10, NOTNULL, $lease_end_dates);
$list->add_edit_column("extensionoption", "Extension Option to Date", 10, NOTNULL, $lease_extension_dates);
$list->add_edit_column("exitoption", "Exit Option to Date", 10, NOTNULL, $lease_extit_dates);
$list->add_edit_column("poslease_termination_time", "Termination deadline", 10, NOTNULL, $lease_termination_times);
$list->add_list_column("poslease_currency", "Curreny", "select currency_id, currency_symbol from currencies order by currency_symbol", NOTNULL, $lease_currencies);
$list->add_edit_column("poslease_anual_rent", "Rent p.a.", 10, NOTNULL, $lease_rents);
$list->add_edit_column("poslease_salespercent", "Rent in % of Sales", 10, NOTNULL, $lease_salespercent);

$list->populate();
//$list->process();


if($form->button("back"))
{
	redirect("correct_pos_data.php?country=" . param("country"));
}
elseif($form->button("remove"))
{
	//delete working space
	$sql = "delete from _posareas where posarea_posaddress = " . dbquote(param("pos_id"));
	mysql_query($sql) or dberror($sql);
	
	$sql = "delete from _posequipments where posequipment_posaddress = " . dbquote(param("pos_id"));
	mysql_query($sql) or dberror($sql);

	$sql = "delete from _posaddresses where posaddress_id = " . dbquote(param("pos_id"));
	mysql_query($sql) or dberror($sql);

	$sql = "Delete from _posleases where poslease_posaddress = " . param("pos_id");
	mysql_query($sql) or dberror($sql);

	redirect("correct_pos_data_check.php?country=" . param("country"));
}
elseif($form->button("posaddress_place_id"))
{
	$sql = "select place_name, place_province " .
		   "from places " . 
		   "where place_id = " . dbquote($form->value("posaddress_place_id"));
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$form->value("posaddress_place", $row["place_name"]);
		$form->value("select_province", $row["place_province"]);
	}
}
elseif($form->button("posaddress_country"))
{
	$form->value("posaddress_place_id", "");
	$form->value("posaddress_place", "");
	$form->value("posaddress_zip", "");
	$form->value("select_province", "");
}
elseif($form->button(FORM_BUTTON_SAVE))
{
	$error = 0;
	if($pos['posaddress_store_postype'] < 4)
	{
		
		$form->add_validation("{posaddress_store_grosssurface} >= {posaddress_store_totalsurface}", "The gross surface must not be less than the total surface!");

		$form->add_validation("{posaddress_store_totalsurface} >= {posaddress_store_retailarea}", "The total surface must not be less than the sales surface!");
		
		
		if(!$form->value("posaddress_perc_class")){$error = 1;}
		if(!$form->value("posaddress_perc_tourist")){$error = 1;}
		if(!$form->value("posaddress_perc_transport")){$error = 1;}
		if(!$form->value("posaddress_perc_people")){$error = 1;}
		if(!$form->value("posaddress_perc_parking")){$error = 1;}
		if(!$form->value("posaddress_perc_visibility1")){$error = 1;}
		if(!$form->value("posaddress_perc_visibility2")){$error = 1;}
	}

	if($error == 1)
	{
		$form->error("Please rate all parameters in section 'Area Perception'!");
	}
	elseif(count($form->items["area"]["value"]) == 0)
	{
		$form->error("Please indicate the neighbourhood information.");
	}
	elseif($form->validate())
	{
		
		if($form->value("select_province") > 0) //update place
		{
			$sql = "update places set place_province = " . $form->value("select_province") . 
				   " where place_id = " . $form->value("posaddress_place_id");
			
			mysql_query($sql) or dberror($sql);
		}
		

		$l_error = 0;
		
		/*
		if($pos["posaddress_ownertype"] == 1 and $pos["posaddress_store_postype"] == 1) //corporate and store
		{
			//save all lease data
			$l_type = $list->values("poslease_lease_type");			
			$l_start = $list->values("start");
			$l_end = $list->values("end");
			$l_extensionoption = $list->values("extensionoption");
			$l_exitoption = $list->values("exitoption");
			$l_termination_time = $list->values("poslease_termination_time");
			$l_poslease_currency = $list->values("poslease_currency");
			$l_poslease_anual_rent = $list->values("poslease_anual_rent");
			$l_poslease_salespercent = $list->values("poslease_salespercent");
			$l_error = 0;

			foreach($list->values("poslease_lease_type") as $key=>$value)
			{
				if(!$l_start[$key] or !$l_end[$key] or !$l_poslease_currency[$key] or !$l_poslease_anual_rent[$key] or $l_termination_time[$key] === '')
				{
					$form->error("The following lease data is mandatory: Type, Start, End, Currency, Annual Rent.");
					$l_error = 1;
				}
			}
		
		}
		*/


		if($l_error == 0)
		{

			//update posaddress
			$fields = array();
			if($form->value("posaddress_franchisee_id"))
			{
				$value = trim($form->value("posaddress_franchisee_id")) == "" ? "0" : dbquote($form->value("posaddress_franchisee_id"));
				$fields[] = "posaddress_franchisee_id = " . $value;
			}

				
			$value = trim($form->value("posaddress_name")) == "" ? "null" : dbquote($form->value("posaddress_name"));
			$fields[] = "posaddress_name = " . $value;

			$value = trim($form->value("posaddress_name2")) == "" ? "null" : dbquote($form->value("posaddress_name2"));
			$fields[] = "posaddress_name2 = " . $value;

			$value = trim($form->value("posaddress_address")) == "" ? "null" : dbquote($form->value("posaddress_address"));
			$fields[] = "posaddress_address = " . $value;

			$value = trim($form->value("posaddress_address2")) == "" ? "null" : dbquote($form->value("posaddress_address2"));
			$fields[] = "posaddress_address2 = " . $value;

			$value = trim($form->value("posaddress_zip")) == "" ? "null" : dbquote($form->value("posaddress_zip"));
			$fields[] = "posaddress_zip = " . $value;

			$value = trim($form->value("posaddress_place")) == "" ? "null" : dbquote($form->value("posaddress_place"));
			$fields[] = "posaddress_place = " . $value;

			$value = trim($form->value("posaddress_place_id")) == "" ? "null" : dbquote($form->value("posaddress_place_id"));
			$fields[] = "posaddress_place_id = " . $value;

			$value = trim($form->value("posaddress_country")) == "" ? "null" : dbquote($form->value("posaddress_country"));
			$fields[] = "posaddress_country = " . $value;
			
			$value = trim($form->value("posaddress_phone")) == "" ? "null" : dbquote($form->value("posaddress_phone"));
			$fields[] = "posaddress_phone = " . $value;
			
			$value = trim($form->value("posaddress_fax")) == "" ? "null" : dbquote($form->value("posaddress_fax"));
			$fields[] = "posaddress_fax = " . $value;
			
			$value = trim($form->value("posaddress_email")) == "" ? "null" : dbquote($form->value("posaddress_email"));
			$fields[] = "posaddress_email = " . $value;

			

			$value = trim($form->value("posaddress_perc_class")) == "" ? "null" : dbquote($form->value("posaddress_perc_class"));
			$fields[] = "posaddress_perc_class = " . $value;

			$value = trim($form->value("posaddress_perc_tourist")) == "" ? "null" : dbquote($form->value("posaddress_perc_tourist"));
			$fields[] = "posaddress_perc_tourist = " . $value;

			$value = trim($form->value("posaddress_perc_transport")) == "" ? "null" : dbquote($form->value("posaddress_perc_transport"));
			$fields[] = "posaddress_perc_transport = " . $value;

			$value = trim($form->value("posaddress_perc_people")) == "" ? "null" : dbquote($form->value("posaddress_perc_people"));
			$fields[] = "posaddress_perc_people = " . $value;

			$value = trim($form->value("posaddress_perc_parking")) == "" ? "null" : dbquote($form->value("posaddress_perc_parking"));
			$fields[] = "posaddress_perc_parking = " . $value;

			$value = trim($form->value("posaddress_perc_visibility1")) == "" ? "null" : dbquote($form->value("posaddress_perc_visibility1"));
			$fields[] = "posaddress_perc_visibility1 = " . $value;

			$value = trim($form->value("posaddress_perc_visibility2")) == "" ? "null" : dbquote($form->value("posaddress_perc_visibility2"));
			$fields[] = "posaddress_perc_visibility2 = " . $value;

			
			if($pos['posaddress_store_postype'] < 4)
			{
				$value = trim($form->value("posaddress_store_grosssurface")) == "" ? "null" : dbquote($form->value("posaddress_store_grosssurface"));
				$fields[] = "posaddress_store_grosssurface = " . $value;
				
				$value = trim($form->value("posaddress_store_totalsurface")) == "" ? "null" : dbquote($form->value("posaddress_store_totalsurface"));
				$fields[] = "posaddress_store_totalsurface = " . $value;

				$value = trim($form->value("posaddress_store_retailarea")) == "" ? "null" : dbquote($form->value("posaddress_store_retailarea"));
				$fields[] = "posaddress_store_retailarea = " . $value;

				$value = trim($form->value("posaddress_store_backoffice")) == "" ? "null" : dbquote($form->value("posaddress_store_backoffice"));
				$fields[] = "posaddress_store_backoffice = " . $value;

				$value = trim($form->value("posaddress_store_numfloors")) == "" ? "null" : dbquote($form->value("posaddress_store_numfloors"));
				$fields[] = "posaddress_store_numfloors = " . $value;

				$value = trim($form->value("posaddress_store_floorsurface1")) == "" ? "null" : dbquote($form->value("posaddress_store_floorsurface1"));
				$fields[] = "posaddress_store_floorsurface1 = " . $value;

				$value = trim($form->value("posaddress_store_floorsurface2")) == "" ? "null" : dbquote($form->value("posaddress_store_floorsurface2"));
				$fields[] = "posaddress_store_floorsurface2 = " . $value;

				$value = trim($form->value("posaddress_store_floorsurface3")) == "" ? "null" : dbquote($form->value("posaddress_store_floorsurface3"));
				$fields[] = "posaddress_store_floorsurface3 = " . $value;

				$value = trim($form->value("posaddress_store_headcounts")) == "" ? "null" : dbquote($form->value("posaddress_store_headcounts"));
				$fields[] = "posaddress_store_headcounts = " . $value;

				$value = trim($form->value("posaddress_store_fulltimeeqs")) == "" ? "null" : dbquote($form->value("posaddress_store_fulltimeeqs"));
				$fields[] = "posaddress_store_fulltimeeqs = " . $value;
			}
			else
			{
				$value = trim($form->value("posaddress_overall_sqms")) == "" ? "null" : dbquote($form->value("posaddress_overall_sqms"));
				$fields[] = "posaddress_overall_sqms = " . $value;

				$value = trim($form->value("posaddress_dedicated_sqms_wall")) == "" ? "null" : dbquote($form->value("posaddress_dedicated_sqms_wall"));
				$fields[] = "posaddress_dedicated_sqms_wall = " . $value;

				$value = trim($form->value("posaddress_dedicated_sqms_free")) == "" ? "null" : dbquote($form->value("posaddress_dedicated_sqms_free"));
				$fields[] = "posaddress_dedicated_sqms_free = " . $value;

			}

			$value = trim($form->value("posaddress_google_lat")) == "" ? "null" : dbquote($form->value("posaddress_google_lat"));
			$fields[] = "posaddress_google_lat = " . $value;

			$value = trim($form->value("posaddress_google_long")) == "" ? "null" : dbquote($form->value("posaddress_google_long"));
			$fields[] = "posaddress_google_long = " . $value;

			$fields[] = "posaddress_google_precision = 1";

			$value = trim($form->value("posaddress_export_to_web")) == "" ? "null" : dbquote($form->value("posaddress_export_to_web"));
			$fields[] = "posaddress_export_to_web = " . $value;

			$fields[] = "posaddress_checkdate = " . dbquote(date("Y-m-d"));

			$value = 1;
			$fields[] = "posaddress_data_check = " . $value;
			
			$value = "current_timestamp";
			$fields[] = "date_modified = " . $value;

			if (isset($_SESSION["user_login"]))
			{
				$value = dbquote($_SESSION["user_login"]);
				$fields[] = "user_modified = " . $value;
			}

			
			$sql = "update posaddresses set " . join(", ", $fields) . " where posaddress_id = " . dbquote(param("pos_id"));
			mysql_query($sql) or dberror($sql);
			
			//update posareas
			$sql = "delete from posareas where posarea_posaddress = " . dbquote(param("pos_id"));
			mysql_query($sql) or dberror($sql);

			$sql = "select * from _posareas where posarea_posaddress = " . dbquote(param("pos_id"));

			
			$res = mysql_query($sql) or dberror($sql);
			while ($row = mysql_fetch_assoc($res))
			{
				$fields = array();
				$values = array();

				$fields[] = "posarea_posaddress";
				$values[] = dbquote(param("pos_id"));

				$fields[] = "posarea_area";
				$values[] = dbquote($row["posarea_area"]);

				$fields[] = "date_created";
				$values[] = dbquote(date("Y-m-d"));

				$fields[] = "date_modified";
				$values[] = dbquote(date("Y-m-d"));

				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				$fields[] = "user_modified";
				$values[] = dbquote(user_login());

				$sql = "insert into posareas (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);
			}


			//update posleases
			/*
			if($pos["posaddress_ownertype"] == 1 and $pos["posaddress_store_postype"] == 1) //corporate and store
			{
				$sql = "Delete from posleases where poslease_posaddress = " . param("pos_id");
				mysql_query($sql) or dberror($sql);
				
				foreach($list->values("poslease_lease_type") as $key=>$value)
				{
					if($value > 0)
					{
						$fields = array();
						$values = array();

						$values[] = dbquote(param("pos_id"));
						$fields[] = "poslease_posaddress";

						$values[] = dbquote($value);
						$fields[] = "poslease_lease_type";

						$values[] = dbquote(from_system_date($l_start[$key]));
						$fields[] = "poslease_startdate";

						$values[] = dbquote(from_system_date($l_end[$key]));
						$fields[] = "poslease_enddate";

						$values[] = dbquote(from_system_date($l_extensionoption[$key]));
						$fields[] = "poslease_extensionoption";

						$values[] = dbquote(from_system_date($l_exitoption[$key]));
						$fields[] = "poslease_exitoption";

						$values[] = dbquote($l_termination_time[$key]);
						$fields[] = "poslease_termination_time";

						$values[] = dbquote($l_poslease_currency[$key]);
						$fields[] = "poslease_currency";

						$values[] = dbquote($l_poslease_anual_rent[$key]);
						$fields[] = "poslease_anual_rent";

						$values[] = dbquote($l_poslease_salespercent[$key]);
						$fields[] = "poslease_salespercent";

						$values[] = dbquote($row["user_created"]);
						$fields[] = "user_created";

						$values[] = dbquote($row["date_created"]);
						$fields[] = "date_created";

						$values[] = dbquote($row["user_modified"]);
						$fields[] = "user_modified";

						$values[] = dbquote($row["date_modified"]);
						$fields[] = "date_modified";

						$sql = "insert into posleases (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);

						echo $sql;
					}
				}
			}
			*/
	

			//update posequipments
			/*
			$sql = "delete from posequipments where posequipment_posaddress = " . dbquote(param("pos_id"));
			mysql_query($sql) or dberror($sql);
			
			$sql = "select * from _posequipments where posequipment_posaddress = " . dbquote(param("pos_id"));

		
			$res = mysql_query($sql) or dberror($sql);
			while ($row = mysql_fetch_assoc($res))
			{
				$fields = array();
				$values = array();

				$fields[] = "posequipment_posaddress";
				$values[] = dbquote(param("pos_id"));

				$fields[] = "posequipment_equipment";
				$values[] = dbquote($row["posequipment_equipment"]);

				$fields[] = "date_created";
				$values[] = dbquote(date("Y-m-d"));

				$fields[] = "date_modified";
				$values[] = dbquote(date("Y-m-d"));

				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				$fields[] = "user_modified";
				$values[] = dbquote(user_login());

				$sql = "insert into posequipments (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);
			}

			*/

			//delete working space
			$sql = "delete from _posareas where posarea_posaddress = " . dbquote(param("pos_id"));
			mysql_query($sql) or dberror($sql);
			
			$sql = "delete from _posequipments where posequipment_posaddress = " . dbquote(param("pos_id"));
			mysql_query($sql) or dberror($sql);

			$sql = "delete from _posaddresses where posaddress_id = " . dbquote(param("pos_id"));
			mysql_query($sql) or dberror($sql);

			$sql = "Delete from _posleases where poslease_posaddress = " . param("pos_id");
			mysql_query($sql) or dberror($sql);

			
			
			//update store locator
			update_store_locator(param("pos_id"));

			
			redirect("correct_pos_data_check.php?country=" . param("country"));
		}
	}
}

// Render page
$page = new Page("posaddresses", "POS Data: Check Correction");
require "include/pos_page_actions.php";
$page->header();

$page->title("POS Data: Check Correction");

$form->render();

/*
if($pos["posaddress_ownertype"] == 1 and $pos["posaddress_store_postype"] == 1) //corporate and store
{
	echo "<br /><br /><br />";
	$list->render();
}
*/


echo "<br /><br /><br /><span class=\"section\">Map Selector</span><br />";

echo "<iframe src =\"correct_pos_data_map.php?id=" . param("pos_id") . "\" width=\"800\" height=\"800\" frameborder=\"0\" marginwidth=\"0\"></iframe>";

$page->footer();

?>