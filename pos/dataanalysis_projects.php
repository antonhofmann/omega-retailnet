<?php
/********************************************************************

    dataanalysis_projects.php

    Analyse posorders and show possible error

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_administrate_posindex");
set_referer("dataanalysis_assign_pos_address.php");


/********************************************************************
    projects not assigned to a POS Address
*********************************************************************/


$preselect_filter = "";
if(param("type"))
{
	$preselect_filter = " and project_postype = " . param("type");
	register_param("type", param("type"));

}

$preselect_filter2 = "";
if(param("country"))
{
	$preselect_filter2 = " and order_shop_address_country = " . param("country");
	register_param("country", param("country"));
}

$preselect_filter3 = "";
if(param("year"))
{
	$preselect_filter3 = " and year(orders.date_created) = " . param("year");
	register_param("year", param("year"));
}


$sql = "select order_id, order_number, country_name, order_actual_order_state_code, order_shop_address_country, " . 
       "concat(order_shop_address_company, ', ', order_shop_address_address) as shop, " .
	   "concat(order_shop_address_zip, ', ', order_shop_address_place) as place, " .
	   "project_id, product_line_name, postype_name, projectkind_name, order_archive_date, country_id  " . 
	   "from orders " .
	   "left join projects on project_order = order_id " . 
	   "left join product_lines on project_product_line = product_line_id ".
	   "left join postypes on postype_id = project_postype ".
	   "left join projectkinds on projectkind_id = project_projectkind ".
       "left join posorders on posorders.posorder_order = order_id " .
	   "left join posorderspipeline on posorderspipeline.posorder_order = order_id " .
	   "left join countries on country_id = order_shop_address_country";

$filter = "order_actual_order_state_code <> '900' and order_type = 1 and posorders.posorder_id is NUll and posorderspipeline.posorder_id is NUll" . $preselect_filter . $preselect_filter2 . $preselect_filter3;


//create dropdownfilter

$values = array();
$dropdown_filter = "";
$countries = array();

$sql1 = $sql . " where " . $filter;
$res = mysql_query($sql1) or dberror($sql1);
while ($row = mysql_fetch_assoc($res))
{
	$countries[$row["country_id"]] = $row["country_id"];
}

if(count($countries) > 0)
{
	$dropdown_filter .= ' where posaddress_country IN (' . implode (',' , $countries ) . ')';
}
$sql_dropdown = "select posaddress_id, concat(country_name, ', ', place_name, ', ', posaddress_name) as poslocation " .
                "from posaddresses " .
				"left join countries on country_id = posaddress_country " . 
				"left join places on place_id = posaddress_place_id " .
				$dropdown_filter . 
				" order by country_name, place_name, posaddress_name";




//create links to projects

$infolinks = array();
$sql_p = $sql . " where " . $filter;

$res_p = mysql_query($sql_p) or dberror($sql_p);

while ($row_p = mysql_fetch_assoc($res_p))
{

	if($row_p["order_archive_date"] == NULL or $row_p["order_archive_date"] == "0000-00-00")
	{
		$link = "<a href=\"/user/project_edit_pos_data.php?pid=" .  $row_p["project_id"] . "\" target=\"_blank\"><img src=\"/pictures/info.gif\" border='0'/></a>";
	}
	else
	{
		$link = "<a href=\"/archive/project_edit_pos_data.php?pid=" .  $row_p["project_id"] . "\" target=\"_blank\"><img src=\"/pictures/info.gif\" border='0'/></a>";
	}
	
	$infolinks[$row_p["order_id"]] = $link;
}


/********************************************************************
    Create List
*********************************************************************/ 
$list4 = new ListView($sql);

$list4->set_entity("orders");
$list4->set_filter($filter);
$list4->set_title("Projects not beeing assigned to a POS Address");
$list4->set_order("postype_name, country_name, shop");

$list4->add_hidden("type", param("type"));
$list4->add_hidden("country", param("country"));
//$list4->add_text_column("info", "", COLUMN_UNDERSTAND_HTML, $infolinks);

$list4->add_column("postype_name", "POS Type");
$list4->add_column("order_actual_order_state_code", "Status");
$list4->add_column("product_line_name", "Product Line", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list4->add_column("country_name", "Country", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);

$list4->add_column("order_number", "Project", "", "","", COLUMN_NO_WRAP);
$list4->add_column("projectkind_name", "Kind", "", "","", COLUMN_NO_WRAP);
$list4->add_column("shop", "Shop Address", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list4->add_column("place", "City", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list4->add_list_column("posaddress", "POS", $sql_dropdown, 0, $values);


$list4->add_button("save", "Save");
$list4->populate();
$list4->process();


if($list4->button("save"))
{
	foreach ($list4->values("posaddress") as $key=>$value)
    {
		
		// update record
        if ($value)
        {
			$sql = "select order_number, project_product_line, project_product_line_subclass, project_postype, " .
				   "project_pos_subclass, project_projectkind,project_cost_type, project_actual_opening_date, project_shop_closingdate  " .
				   "from orders " . 
				   "left join projects on project_order = order_id " .
				   "left join project_costs on project_cost_order = order_id " . 
			       " where order_id = " . $key;
			
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$year_of_order = substr($row["order_number"],0,2);
				if($year_of_order > 30 and $year_of_order <= 99)
				{
					$year_of_order = "19" . $year_of_order;
				}
				else
				{
					$year_of_order = substr($year_of_order, 0, 1) . '00' . substr($year_of_order, 1, 1);
				}
				
				if(substr($row["order_number"],3,1) == '.')
				{
					$year_of_order = substr($row["order_number"],0,1) . '0' . substr($row["order_number"],1,2);
				}


				
				$sql = "insert into posorders (" . 
					   "posorder_posaddress, " .
					   "posorder_order, " .
					   "posorder_ordernumber, " .
					   "posorder_year, " . 
					   "posorder_product_line, " . 
					   "posorder_product_line_subclass, " . 
					   "posorder_postype, " . 
					   "posorder_subclass, " . 
					   "posorder_project_kind, " . 
					   "posorder_legal_type, " . 
					   "posorder_opening_date, " . 
					   "posorder_closing_date, " . 
					   "posorder_type) Values ( " .
					   $value . ", " .
					   $key . ", " .
					   dbquote($row["order_number"]) . ", " .
					   dbquote($year_of_order) . ", " .
					   dbquote($row["project_product_line"]) . ", " .
					   dbquote($row["project_product_line_subclass"]) . ", " .
					   dbquote($row["project_postype"]) . ", " .
					   dbquote($row["project_pos_subclass"]) . ", " .
					   dbquote($row["project_projectkind"]) . ", " .
					   dbquote($row["project_cost_type"]) . ", " .
					   dbquote($row["project_actual_opening_date"]) . ", " .
					   dbquote($row["project_shop_closingdate"]) . ", " .
					   1  . ")";

				$result = mysql_query($sql) or dberror($sql);
			}
		}
	}

	redirect("dataanalysis_projects_preselect.php");
}



$page = new Page("posindex");
require "include/pos_page_actions.php";
$page->header();

$page->title("Project Orphans");

$list4->render();


$page->footer();
?>