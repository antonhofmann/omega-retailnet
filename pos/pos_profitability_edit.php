<?php
/********************************************************************

    pos_profitability_edit.php

    Edit Profitability data of POS Location

    created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2013-06-16
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2013-06-16
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

if(!has_access("has_access_to_pos_profitability") 
	and !has_access("can_edit_pos_profitability_of_his_pos")
	and !has_access("can_edit_pos_profitability_of_all_pos"))
{
	redirect("/pos");
}

require_once "include/check_access.php";

$pos_data = get_poslocation(param("pos_id"), "posaddresses");


/********************************************************************
    Create Form
*********************************************************************/

$form = new Form("possellouts", "possellouts");

$form->add_hidden("country", param("country"));
$form->add_hidden("province", param("province"));
$form->add_hidden("ostate", param("ostate"));
$form->add_hidden("ltf", param("ltf"));
$form->add_hidden("let", param("let"));
$form->add_hidden("pos_id", param("pos_id"));



$form->add_label("posaddress_name", "POS Name", 0, $pos_data["posaddress_name"]);
$form->add_label("posaddress_address", "Address", 0, $pos_data["posaddress_address"]);
$form->add_label("posaddress_place", "City", 0, $pos_data["place_name"]);
$form->add_label("posaddress_place", "City", 0, $pos_data["country_name"]);
$form->add_label("posaddress_store_openingdate", "Opening Date", 0, from_system_date($pos_data["posaddress_store_openingdate"]));
$form->add_label("posaddress_store_closingdate", "Closing Date", 0, from_system_date($pos_data["posaddress_store_closingdate"]));

$form->add_section("Profitability Data");
$form->add_comment("Please indicate real business date from passed years.");
$form->add_hidden("possellout_posaddress_id", param("pos_id"));

$form->add_edit("possellout_year", "Year*", NOTNULL, "", TYPE_INT, 4);

$form->add_edit("possellout_month", "Number of months selling", 0, "", TYPE_INT, 2);

$form->add_edit("possellout_watches_units", "Watches Units sold", 0, "", TYPE_INT, 12);
$form->add_edit("possellout_watches_grossales", "Watches Gross Sales", 0, "", TYPE_INT, 12);
$form->add_edit("possellout_bijoux_units", "Bijoux Units sold", 0, "", TYPE_INT, 12);
$form->add_edit("possellout_bijoux_grossales", "Bijoux Gross Sales", 0, "", TYPE_INT, 12);

$form->add_edit("possellout_net_sales", "Net Sales", 0, "", TYPE_INT, 12);
$form->add_edit("possellout_grossmargin", "Gross Margin", 0, "", TYPE_INT, 12);
$form->add_edit("possellout_operating_expenses", "Indirect operating expenses", 0, "", TYPE_INT, 12);
$form->add_edit("possellout_operating_income_excl", "Operating Income", 0, "", TYPE_INT, 12);
$form->add_edit("possellout_wholsale_margin", "Wholesale margin (%)", 0, "", TYPE_DECIMAL, 6, 2);
$form->add_edit("possellout_operating_income_incl", "Operating income incl. Whole Sale Margin", 0, "", TYPE_INT, 12);



$form->add_button("delete", "Delete");
$form->add_button("back", "Back");

if(has_access("can_edit_pos_profitability_of_his_pos")
	or has_access("can_edit_pos_profitability_of_all_pos"))
{
	$form->add_button("save", "Save");
}

$form->populate();
$form->process();


$link = "pos_profitability.php?country=" . param("country") . '&province=' . param("province") . '&let=' . param('let') . '&ltf=' . param("ltf"). '&ostate=' . param("ostate") . '&pos_id=' . param("pos_id");
if($form->button("back") or $form->button("save"))
{
	if($form->button("save")) {
		if($form->validate()) {
			$form->save();
			redirect($link);

		}
	}
	else {
		redirect($link);
	}
	
	
}

elseif($form->button("delete") and $form->validate())
{
	$sql = "delete from possellouts " .
	       "where possellout_id = " . id();

    $result = mysql_query($sql) or dberror($sql);
	redirect($link);
}



$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title("POS Index: Profitability");

$form->render();

$page->footer();

?>
