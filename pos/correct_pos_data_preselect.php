<?php
/********************************************************************

    correct_pos_data_preselect.php

    Preselection of POS List for data corrections

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_correct_pos_data");
set_referer("posindex.php");


//compose list
if(has_access("can_view_posindex") or has_access("can_edit_posindex"))
{
	$sql = "select DISTINCT country_id, country_name " .
		   "from _posaddresses " . 
		   "left join countries on posaddress_country = country_id " . 
		   "where country_name is not null " . 
		   "order by country_name";
}
else
{
	$user = get_user(user_id());

	$sql = "select DISTINCT posaddress_country " .
		   "from _posaddresses " . 
		   "where posaddress_client_id = " . $user["address"];

	$num_of_countries = 0;
	$res = mysql_query($sql) or dberror($sql);
	$num_rows = mysql_num_rows($res);
	if($num_rows < 2)
	{
		if($row = mysql_fetch_assoc($res))
		{
			redirect("correct_pos_data.php?country=" . $row["posaddress_country"]);
		}
	}


	$country_filter = "";
	$tmp = array();
	$sql = "select * from country_access " .
		   "where country_access_user = " . user_id();


	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{            
		$tmp[] = $row["country_access_country"];
	}

	if(count($tmp) > 0) {
		$country_filter = " posaddress_country IN (" . implode(",", $tmp) . ") ";
	}

	if($country_filter == "")
	{
		$sql = "select DISTINCT country_id, country_name " .
			   "from _posaddresses " . 
			   "left join countries on posaddress_country = country_id " .
			   "left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
			   "left join postypes on postype_id = posaddress_store_postype " .
			   "where country_name is not null " .
			   " and posaddress_client_id = " . $user["address"] . 
			   " order by country_name";
	}
	else
	{
		$sql = "select DISTINCT country_id, country_name " .
			   "from _posaddresses " . 
			   "left join countries on posaddress_country = country_id " .
			   "left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
			   "left join postypes on postype_id = posaddress_store_postype " .
			   "where country_name is not null " .
			   " and " . $country_filter .
			   " order by country_name";
	}
}

$form = new Form("posaddresses", "posaddress");

$form->add_section("Country Selection");

$form->add_list("country", "Country",$sql);

$form->add_button("show_pos", "Show List");

$form->populate();
$form->process();


if($form->button("show_pos"))
{
	redirect("correct_pos_data.php?country=" . $form->value("country"));
}

$page = new Page("posaddresses", "POS Index: Data Corrections");
require "include/pos_page_actions.php";
$page->header();

$page->title("POS Index: Data Corrections");
$form->render();


?>

	<script type="text/javascript">
		
		document.onkeydown = process_key;
		
		function process_key(e)
		{
		  if( !e ) 
		  {
			if( window.event ) 
			{
			  e = window.event;
			} 
			else 
			{
			  return;
			}
		  }

		  if(e.keyCode==13)
		  {
			  button('show_pos');
		  }
		}
	</script>

	<?php

$page->footer();

?>
