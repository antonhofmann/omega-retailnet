<?php
/********************************************************************

    posleases.php

    Entry page for the leses section.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

require_once "include/check_access.php";

set_referer("poslease.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
register_param("pos_id");

//check if user can edit this company
$can_edit = false;
if(has_access("can_edit_posindex"))
{
	$can_edit = true;
}
elseif(has_access("can_edit_his_posindex"))
{
	
	$user = get_user(user_id());

	$sql = "select posaddress_client_id " .
		   "from posaddresses " .
		   "where posaddress_id = " . param("pos_id") . 
		   "   and posaddress_client_id = " . $user["address"];

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);

	if($user["address"] == $row["posaddress_client_id"])
	{
		$can_edit = true;
	}
}

$pos = array();
if(param("pos_id") > 0)
{
	$pos = get_poslocation(param("pos_id"), "posaddresses");
}

/********************************************************************
    Header
*********************************************************************/
$form = new Form("posleases", "posleases");
require_once("include/poslocation_head.php");

$form->add_hidden("country", param("country"));

$form->add_hidden("ltf", param("ltf"));
$form->add_hidden("let", param("let"));
$form->add_hidden("ostate", param("ostate"));

$form->populate();
$form->process();


/********************************************************************
    list
*********************************************************************/
// create sql
$sql = "select poslease_id, poslease_landlord_name, poslease_startdate, poslease_enddate, poslease_type_name, " .
       "poslease_nexttermin_date, poslease_anual_rent, currency_symbol, poslease_termination_time, " .
	   "IF(poslease_isindexed=1, 'yes', 'no') as tacit, " .
	   "DATE_FORMAT(poslease_startdate,'%d.%m.%Y') as start, " .
	   "DATE_FORMAT(poslease_enddate,'%d.%m.%Y') as end, " .
	   "(TIMESTAMPDIFF(MONTH,poslease_startdate,poslease_enddate)+1) as months, " . 
       "IF(TIMESTAMPDIFF(MONTH,current_date(),poslease_enddate) >0, TIMESTAMPDIFF(MONTH,current_date(),poslease_enddate), 'expired') as restmonths, " .
	   "project_number " . 
       "from posleases " .
	   "left join poslease_types on poslease_type_id = poslease_lease_type " .
	   "left join projects on project_order = poslease_order " .
	   "left join currencies on currency_id = poslease_currency";

$list_filter = "poslease_posaddress = " . param("pos_id");

/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);
$list->set_entity("posleases");
$list->set_order("poslease_startdate, poslease_type_name");
$list->set_filter($list_filter);   

$link = "poslease.php?pos_id=" . param("pos_id") . "&country=" . param("country") . '&let=' . param('let') . "&ltf=" . param("ltf") . "&ostate=" . param("ostate");

$list->add_hidden("pos_id", param("pos_id"));
$list->add_column("poslease_type_name", "Type", $link, "", "", COLUMN_NO_WRAP);
$list->add_column("project_number", "Project", "", "", "", COLUMN_NO_WRAP);
//$list->add_column("poslease_landlord_name", "Landlord", "", LIST_FILTER_LIST, "", COLUMN_NO_WRAP);
$list->add_column("start", "Start Date", "", LIST_FILTER_LIST, "", COLUMN_NO_WRAP);
$list->add_column("end", "End Date", "", "", "", COLUMN_NO_WRAP);
//$list->add_column("poslease_nexttermin_date", "Next Termination", "", "", "", COLUMN_NO_WRAP);
$list->add_column("months", "Months", "", "", "", COLUMN_ALIGN_RIGHT);
$list->add_column("poslease_termination_time", "Termination", "", "", "", COLUMN_NO_WRAP);
$list->add_column("poslease_anual_rent", "Rent p.a.", "", "", "", COLUMN_ALIGN_RIGHT);
$list->add_column("currency_symbol", "", "", "", "", COLUMN_ALIGN_LEFT);
$list->add_column("tacit", "Tacit", "", "", "", COLUMN_ALIGN_LEFT);

if(has_access("can_edit_posindex") or has_access("can_edit_lease_data_of_his_country")
     or ($can_edit == true and has_access("can_edit_his_posindex")))
{
	$list->add_button("new", "New", $link);
}

$list->add_button("back", "Back to POS List");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();

if($list->button("back"))
{
	redirect("posindex.php?country=" . param("country"). '&let=' . param('let') . '&ltf=' . param("ltf"). '&ostate=' . param("ostate"));
}
elseif($list->button("new"))
{
	redirect($link);
}


/********************************************************************
    create page
*********************************************************************/
$page = new Page("posindex");
require "include/pos_page_actions.php";
$page->header();


$page->title("Leases: " . $poslocation["posaddress_name"]);

require_once("include/tabs.php");

$form->render();

$list->render();
$page->footer();
?>