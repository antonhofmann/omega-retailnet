<?php
/********************************************************************

    loc_possubclass.php

    Lists translations of POS Type Subclasses.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");
set_referer("loc_possubclass_possubclass.php");

$possubclass_id = 0;

if(param("id"))
{
	$possubclass_id = param("id");
}
elseif(param("possubclass_id"))
{
	$possubclass_id = param("possubclass_id");
}


//get possubclass name
$possubclass_name = "";
$sql = "select possubclass_name from possubclasses where possubclass_id = " . $possubclass_id;
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$possubclass_name = $row["possubclass_name"];
}

//sql for the list
$sql = "select loc_possubclass_id, loc_possubclass_name, language_name " .
       "from loc_possubclasses " . 
	   "left join languages on language_id = loc_possubclass_language ";

$list = new ListView($sql);

$list->set_entity("loc_possubclasses");
$list->set_order("language_name");

$list->add_hidden("possubclass_id", $possubclass_id);
$list->add_column("language_name", "Language", "loc_possubclass_possubclass.php?possubclass_id=" . $possubclass_id, LIST_FILTER_FREE);
$list->add_column("loc_possubclass_name", "POS Subclass");

$list->add_button(LIST_BUTTON_NEW, "Add Translation", "loc_possubclass_possubclass.php?possubclass_id=" . $possubclass_id);
$list->add_button(LIST_BUTTON_BACK, "Back");

$list->process();

$page = new Page("locales");

$page->header();
$page->title("Translations for Subclass " . $possubclass_name);
$list->render();
$page->footer();
?>
