<?php
	require_once "../include/frame.php";

	$location = APPLICATION_URL . "/pos/welcome.php";
	if (preg_match("/" . APPLICATION_URLPART . "/", $_SERVER["HTTP_HOST"]))
	{
		header("location: $location");
	}
	else
	{
		header("location: /pos/welcome.php");
	}
?>