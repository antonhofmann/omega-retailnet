<?php
/********************************************************************

    pos_sellouts_preselect.php

    Preselection of POS List

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2012-08-16
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2012-08-16
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("has_access_to_sellouts");
set_referer("pos_sellouts.php");


$states = array();
$states[1] = "Operating POS locations only";
$states[2] = "Closed POS locations only";

//compose list
if(has_access("can_view_alls_sellouts") or has_access("can_edit_all_sellouts"))
{
	$sql = "select DISTINCT country_id, country_name " .
		   "from posaddresses " . 
		   "left join countries on posaddress_country = country_id " . 
		   "where country_name is not null " . 
		   "order by country_name";
}
else
{

	$user = get_user(user_id());

	/*
	$sql = "select DISTINCT posaddress_country " .
		   "from posaddresses " . 
		   "where posaddress_client_id = " . $user["address"];

	$num_of_countries = 0;
	$res = mysql_query($sql) or dberror($sql);
	$num_rows = mysql_num_rows($res);

	
	if($num_rows < 2)
	{
		if($row = mysql_fetch_assoc($res))
		{
			redirect("posindex.php?country=" . $row["posaddress_country"]);
		}
	}
	*/


	$country_filter = "";
	$tmp = array();
	$sql = "select * from country_access " .
		   "where country_access_user = " . user_id();


	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{            
		$tmp[] = $row["country_access_country"];
	}

	if(count($tmp) > 0) {
		$country_filter = " country_id IN (" . implode(",", $tmp) . ") ";
	}

	if($country_filter == "")
	{
		$sql = "select DISTINCT country_id, country_name " .
			   "from posaddresses " . 
			   "left join countries on posaddress_country = country_id " .
			   "left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
			   "left join postypes on postype_id = posaddress_store_postype " .
			   "where country_name is not null " .
			   " and posaddress_client_id = " . $user["address"] . 
			   //" and (postype_id <> 2 or (postype_id = 2 and project_costtype_id = 1)) " . 
			   " order by country_name";
	}
	else
	{
		$sql = "select DISTINCT country_id, country_name " .
			   "from posaddresses " . 
			   "left join countries on posaddress_country = country_id " .
			   "left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
			   "left join postypes on postype_id = posaddress_store_postype " .
			   "where country_name is not null " .
			   " and " . $country_filter . 
			   //" and (postype_id <> 2 or (postype_id = 2 and project_costtype_id = 1)) " . 
			   " order by country_name";
	}

	
	
}

if(param("country"))
{
	$sql_p = "select province_id, province_canton from provinces  " . 
		     "where province_country = " . dbquote(param("country")) .
		     " order by province_canton";
}

$form = new Form("posaddresses", "posaddress");

$form->add_section("Country Selection");

$form->add_list("country", "Country",$sql, SUBMIT);
if(param("country"))
{
	$form->add_list("province", "Province",$sql_p);
}
else
{
	$form->add_hidden("province");
}

$form->add_list("states", "Filter",$states);

$form->add_button("show_pos", "Show List");

$form->populate();
$form->process();


if($form->button("show_pos"))
{
	redirect("pos_sellouts.php?country=" . $form->value("country") . "&province=" . $form->value("province"). "&ostate=" . $form->value("states"));
}

$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title("POS Sellouts");
$form->render();
?>

	<script type="text/javascript">
		
		document.onkeydown = process_key;
		
		function process_key(e)
		{
		  if( !e ) 
		  {
			if( window.event ) 
			{
			  e = window.event;
			} 
			else 
			{
			  return;
			}
		  }

		  if(e.keyCode==13)
		  {
			  button('show_pos');
		  }
		}
	</script>

	<?php

$page->footer();

?>
