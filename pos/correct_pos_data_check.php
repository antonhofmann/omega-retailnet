<?php
/********************************************************************

    correct_pos_data_check.php

    Lists of addresses (POS) for correction check

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_accept_pos_data_corrections");
set_referer("correct_pos_data_pos.php");


$preselect_filter = "";
if(param("country"))
{
	$preselect_filter = "posaddress_country = " . param("country");
	register_param("country", param("country"));
}

if(!param("country"))
{
	redirect("welcome.php");
}

if($preselect_filter)
{
	$preselect_filter .= " and posaddress_checked = 1 "; 
}
else
{
	$preselect_filter = " posaddress_checked = 1";
}


$googlemap = array();
$closed = array();
$pdfs = array();
$sql_images = "select posaddress_id, posaddress_checked, posaddress_store_closingdate, " . 
              "posaddress_google_lat, posaddress_google_precision from _posaddresses ";

$res = mysql_query($sql_images) or dberror($sql_images);

while ($row = mysql_fetch_assoc($res))
{
	if($row["posaddress_checked"] == 1)
	{
		$checked[$row["posaddress_id"]] = "/pictures/bullet_ball_glass_green.gif";
	}
	else
	{
		$checked[$row["posaddress_id"]] = "/pictures/bullet_ball_glass_red.gif";
	}
}

//compose list

$sql = "select posaddress_id, if(posaddress_name <> '', posaddress_name, 'n.a.') as posname, " .
       "posaddress_address, posaddress_address2, posaddress_zip, " .
       "    posaddress_place, country_name, project_costtype_text, postype_name " .
       "from _posaddresses " . 
	   "left join countries on posaddress_country = country_id " . 
	   "left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
	   "left join postypes on postype_id = posaddress_store_postype ";

$list = new ListView($sql);

$list->set_entity("_posaddresses");
$list->set_filter($preselect_filter);
$list->set_order("country_name, posaddress_place, posaddress_name");

$list->add_hidden("country", param("country"));

$list->add_image_column("checked", "checked", COLUMN_ALIGN_CENTER , $checked);
$list->add_column("country_name", "Country", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);
$list->add_column("posaddress_place", "City", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("posname", "POS Name", "correct_pos_data_check_pos.php?country=" . param("country") , LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("posaddress_address", "Address", "", LIST_FILTER_FREE);
$list->add_column("postype_name", "POS Type", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);
$list->add_column("project_costtype_text", "Legal Type", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);


$list->populate();
$list->process();

$page = new Page("posaddresses", "POS Index: Check Data Corrections");
require "include/pos_page_actions.php";
$page->header();

$page->title("POS Index: Check Data Corrections");
$list->render();


$page->footer();

?>
