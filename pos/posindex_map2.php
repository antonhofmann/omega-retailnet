<?php
/********************************************************************

    posindex_map2.php

    Show google Map of the POS.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2012-04-29
    Version:        2.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

check_access("can_use_posindex");

$pos = get_poslocation(param("id"), "posaddresses");

$latitude = 20;
$longitude = -10;
$zoom = 2;

if($pos["posaddress_google_lat"])
{
	$latitude = $pos["posaddress_google_lat"];
	$zoom = 18;
}
if($pos["posaddress_google_long"])
{
	$longitude = $pos["posaddress_google_long"];
	$zoom = 18;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>POS Index - World Map</title>


	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=<?php echo GOOGLE_API_KEY;?>"></script>

	<SCRIPT type="text/javascript">
		var map;
		  
		function load() 
		{
			var latlng = new google.maps.LatLng(<?php echo $latitude;?>, <?php echo $longitude;?>);
			var myOptions = {
			  zoom: <?php echo $zoom;?>,
			  center: latlng,
			  mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			var map = new google.maps.Map(document.getElementById("map"), myOptions);


			var bounds = new google.maps.LatLngBounds();

			
			var latlng = new google.maps.LatLng(<?php echo $latitude;?>, <?php echo $longitude;?>);
			bounds.extend(latlng)

			var marker = new google.maps.Marker({
			  position: latlng, 
			  map: map,
			  title:"",
			  draggable: true
			});


			google.maps.event.addListener(marker, 'dragend', function() {
				updateCoordinates(marker);
			});

   		   //map.set('streetViewControl', true);

	  }


	  function updateCoordinates(marker) 
	  {
		  var cords = marker.getPosition();
		  parent.document.getElementById("posaddress_google_lat").value = cords.lat();
		  parent.document.getElementById("posaddress_google_long").value= cords.lng();
	  }

	</SCRIPT>

	

	

	<style type="text/css">
  		
		body{
			font-family: Verdana, Geneva, sans-serif;
			font-size: 12px;
			color: #000000;
		}

		a {
				color: #006699;
				text-decoration: none;
				font-weight: bold;
			}


			a:hover {
				color: #FF0000;
				text-decoration: none;
			}
		
		#map{font-family:Arial, Helvetica, sans-serif; }
		
		.clear{ float:none; clear:both; height:0px; line-height:0px; font-size:0px; }
		
		fieldset legend{ margin-bottom:4px; color:#000; }
  
  </style>
</head>

<body onload="load()">

<br /><br />
Move the marker to get the latitude and longitude coordinates of the point.
<br /><br />

<div id="map" style="width:700px; height: 500px"></div>
  </body>
</html>