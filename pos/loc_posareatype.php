<?php
/********************************************************************

    loc_posareatype.php

    Lists translations of POS Area Types.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");
set_referer("loc_posareatype_posareatype.php");

$posareatype_id = 0;

if(param("id"))
{
	$posareatype_id = param("id");
}
elseif(param("posareatype_id"))
{
	$posareatype_id = param("posareatype_id");
}


//get posareatype name
$posareatype_name = "";
$sql = "select posareatype_name from posareatypes where posareatype_id = " . $posareatype_id;
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$posareatype_name = $row["posareatype_name"];
}

//sql for the list
$sql = "select loc_posareatype_id, loc_posareatype_name, language_name " .
       "from loc_posareatypes " . 
	   "left join languages on language_id = loc_posareatype_language ";

$list = new ListView($sql);

$list->set_entity("loc_posareatypes");
$list->set_order("language_name");

$list->add_hidden("posareatype_id", $posareatype_id);
$list->add_column("language_name", "Language", "loc_posareatype_posareatype.php?posareatype_id=" . $posareatype_id, LIST_FILTER_FREE);
$list->add_column("loc_posareatype_name", "POS Area Type");

$list->add_button(LIST_BUTTON_NEW, "Add Translation", "loc_posareatype_posareatype.php?posareatype_id=" . $posareatype_id);
$list->add_button(LIST_BUTTON_BACK, "Back");

$list->process();

$page = new Page("locales");

$page->header();
$page->title("Translations for Area Type " . $posareatype_name);
$list->render();
$page->footer();
?>
