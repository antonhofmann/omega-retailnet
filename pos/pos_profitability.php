<?php
/********************************************************************

    pos_profitability.php

    List Profitability data of POS Location

    created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2013-06-16
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2013-06-16
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

if(!has_access("has_access_to_pos_profitability") 
	and !has_access("can_edit_pos_profitability_of_his_pos")
	and !has_access("can_edit_pos_profitability_of_all_pos"))
{
	redirect("/pos");
}

require_once "include/check_access.php";

$pos_data = get_poslocation(param("pos_id"), "posaddresses");




/********************************************************************
    Create Form
*********************************************************************/

$form = new Form("possellouts", "possellouts");

$form->add_hidden("country", param("country"));
$form->add_hidden("province", param("province"));
$form->add_hidden("ostate", param("ostate"));
$form->add_hidden("pos_id", param("pos_id"));

$form->add_label("posaddress_name", "POS Name", 0, $pos_data["posaddress_name"]);
$form->add_label("posaddress_address", "Address", 0, $pos_data["posaddress_address"]);
$form->add_label("posaddress_place", "City", 0, $pos_data["place_name"]);
$form->add_label("posaddress_place", "City", 0, $pos_data["country_name"]);
$form->add_label("posaddress_store_openingdate", "Opening Date", 0, from_system_date($pos_data["posaddress_store_openingdate"]));
$form->add_label("posaddress_store_closingdate", "Closing Date", 0, from_system_date($pos_data["posaddress_store_closingdate"]));


$form->populate();

/********************************************************************
    Create List
*********************************************************************/
$sql = "select *, if(possellout_year > 0, possellout_year, 'n.a.') as year  from possellouts ";
$list_filter = "possellout_posaddress_id = " . param("pos_id");

$link = "pos_profitability_edit.php?country=" . param("country") . '&province=' . param("province") . '&let=' . param('let') . '&ltf=' . param("ltf"). '&ostate=' . param("ostate") . '&pos_id=' . param("pos_id");


$list = new ListView($sql);

$list->set_title("");
$list->set_entity("possellouts");
$list->set_filter($list_filter);
$list->set_order("possellout_year DESC");

$list->add_hidden("country", param("country"));
$list->add_hidden("province", param("province"));
$list->add_hidden("ostate", param("ostate"));
$list->add_hidden("ltf", param("ltf"));
$list->add_hidden("pos_id", param("pos_id"));

$list->add_column("year", "Year", $link, 0 ,'','', COLUMN_NO_WRAP);
$list->add_column("possellout_month", "Months", "", 0,'', COLUMN_NO_WRAP);
$list->add_column("possellout_watches_units", "Watches", "", "", "",COLUMN_BREAK |  COLUMN_ALIGN_RIGHT);
$list->add_column("possellout_bijoux_units", "Bijoux", "", "", "", COLUMN_BREAK | COLUMN_ALIGN_RIGHT);
$list->add_column("possellout_watches_grossales", "Gross Sales", "", "", "", COLUMN_BREAK | COLUMN_ALIGN_RIGHT);
$list->add_column("possellout_grossmargin", "Gross Margin", "", "", "", COLUMN_BREAK | COLUMN_ALIGN_RIGHT);
$list->add_column("possellout_operating_expenses", "Operating\nExpenses", "", "", "", COLUMN_BREAK | COLUMN_ALIGN_RIGHT);
$list->add_column("possellout_operating_income_excl", "Operating Income\nexcl. WS", "", "", "", COLUMN_BREAK | COLUMN_ALIGN_RIGHT);
$list->add_column("possellout_wholsale_margin", "Whole Sale\nMargin %", "", "", "", COLUMN_BREAK | COLUMN_ALIGN_RIGHT);
$list->add_column("possellout_operating_income_incl", "Operating Income\nincl. WS", "", "", "", COLUMN_BREAK | COLUMN_ALIGN_RIGHT);

if(has_access("can_edit_pos_profitability_of_his_pos")
	or has_access("can_edit_pos_profitability_of_all_pos"))
{
	$list->add_button("add","Add New Year");
}
$list->add_button("back","Back");

$list->populate();
$list->process();


if($list->button("back"))
{
	$link = "pos_profitabilities.php?country=" . param("country") . '&province=' . param("province") . '&let=' . param('let') . '&ltf=' . param("ltf"). '&ostate=' . param("ostate");
	redirect($link);
}
elseif($list->button("add"))
{
		$link = "pos_profitability_edit.php?country=" . param("country") . '&province=' . param("province") . '&let=' . param('let') . '&ltf=' . param("ltf"). '&ostate=' . param("ostate") . '&pos_id=' . param("pos_id");
		redirect($link);

}

$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title("POS Index: Profitability");

$form->render();
$list->render();

$page->footer();

?>
