<?php
/********************************************************************

    dataanalysis_assign_pos_address2.php

    Creation and mutation of address records.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_administrate_posindex");



/********************************************************************
    get POS Location Data
*********************************************************************/
function get_poslocation($posaddress_id, $table)
{

	$poslocation = array();

    $sql = "select * " .
           "from $table ".
		   "left join countries on country_id = posaddress_country " .
		   "left join addresses on address_id = posaddress_client_id " .
		   "left join agreement_types on agreement_type_id = posaddress_fagagreement_type " .
           "where posaddress_id  = " . dbquote($posaddress_id);


    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $poslocation = $row;
    }

	return $poslocation;
}

//get project data
$order = get_order(param("id"));

// get company's address
$client_address = get_address($order["order_client_address"]);

$form = new Form("posaddresses", "POS Address");

$form->add_section("Order Data");

$form->add_label("order_number", "Order Number", 0, $order["order_number"]);

$form->add_section("Client Address");
$form->add_label("client_address_company", "Company", 0, $client_address["company"]);
if ($client_address["company2"])
{
    $form->add_label("client_address_company2", "", 0, $client_address["company2"]);
}
$form->add_label("client_address_address", "Address", 0, $client_address["address"]);
if ($client_address["address2"])
{
    $form->add_label("client_address_address2", "", 0, $client_address["address2"]);
}
$form->add_label("client_address_place", "City", 0, $client_address["zip"] . " " . $client_address["place"]);
$form->add_lookup("client_address_country", "Country", "countries", "country_name", 0, $client_address["country"]);


$form->add_section("POS Location Address");
$form->add_label("shop_address_company", "Project Name", 0, $order["order_shop_address_company"]);

if ($order["order_shop_address_company2"])
{
    $form->add_label("shop_address_company2", "", 0, $order["order_shop_address_company2"]);
}


$form->add_label("shop_address_address", "Address", 0, $order["order_shop_address_address"]);


if ($order["order_shop_address_address2"])
{
    $form->add_label("shop_address_address2", "", 0, $order["order_shop_address_address2"]);
}

$form->add_label("shop_address_place", "City", 0, $order["order_shop_address_zip"] . " " . $order["order_shop_address_place"]);
$form->add_lookup("shop_address_country", "Country", "countries", "country_name", 0, $order["order_shop_address_country"]);

$form->add_section("Assign this Order to an existing POS Address");
$form->add_list("posaddress", "POS Address",
    "select posaddress_id, concat(country_name, ': ', posaddress_place, ', ', posaddress_name, ', ', posaddress_address) as posaddress from posaddresses left join countries on country_id = posaddress_country where posaddress_country = " . $order["order_shop_address_country"] . " order by country_name, posaddress_place, posaddress_name", SUBMIT);

$form->add_hidden("posaddress_id");
$form->add_hidden("posorder_id");

$form->add_section("Roles");

$form->add_list("posaddress_client_id", "Client*",
	"select address_id, concat(country_name, ': ', address_company, ', ', address_place, ' [', address_shortcut, ']') as company from addresses left join countries on country_id = address_country where address_showinposindex = 1 order by country_name, address_company", NOTNULL);

$form->add_list("posaddress_ownertype", "Legal Type*",
	"select project_costtype_id, project_costtype_text from project_costtypes where project_costtype_id IN (1, 2, 6)", NOTNULL);

$form->add_edit("posaddress_eprepnr", "Enterprise Reporting Number");
$form->add_edit("posaddress_sapnumber", "SAP Number");

$form->add_list("posaddress_franchisor_id", "Franchisor",
	"select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where address_showinposindex = 1 and country_name <> '' and address_company <> '' and address_place <> '' and address_canbefranchisor = 1 order by country_name, address_company");


if($poslocation["posaddress_ownertype"] != 6)
{
	$tmp = "Franchisee";
}
else
{
	$tmp = "Owner Company";
}

$form->add_list("posaddress_franchisee_id", $tmp,
	"select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where address_showinposindex = 1 and country_name <> '' and address_company <> '' and address_place <> '' and address_canbefranchisee = 1 order by country_name, address_company");


$form->add_section("POS Address Data");

$form->add_edit("posaddress_name", "POS Name*", NOTNULL);
$form->add_edit("posaddress_address", "Address*", NOTNULL);
$form->add_edit("posaddress_address2", "Address 2");
$form->add_edit("posaddress_zip", "Zip*", NOTNULL);
$form->add_edit("posaddress_place", "City*", NOTNULL);
$form->add_list("posaddress_country", "Country*",
	"select country_id, country_name from countries order by country_name", NOTNULL);

$form->add_section("Communication");
$form->add_edit("posaddress_phone", "Phone");
$form->add_edit("posaddress_fax", "Fax");
$form->add_edit("posaddress_email", "Email");
$form->add_edit("posaddress_website", "Website");

$form->add_section("Store");
$form->add_list("posaddress_store_postype", "POS Type*",
	"select postype_id, postype_name from postypes order by postype_name", NOTNULL);

$form->add_list("posaddress_store_subclass", "Sub Class",
	"select possubclass_id, possubclass_name from possubclasses order by possubclass_name");

$form->add_list("posaddress_store_furniture", "Furniture Type*",
	"select product_line_id, product_line_name from product_lines where product_line_posindex = 1 order by product_line_name", NOTNULL);

$form->add_section("Dates");
$form->add_edit("posaddress_store_openingdate", "Opening Date", 0, "", TYPE_DATE);
$form->add_edit("posaddress_store_closingdate", "Closing Date", 0, "", TYPE_DATE);


$form->add_section("----------------------------------------------------------------------");
$form->add_checkbox("checked2", "Address", "", 0, "revised");

$form->add_button("save_form", "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");

// Populate form and process button clicks

$form->populate();
$form->process();

if($form->button("posaddress"))
{
	if($form->value("posaddress") > 0)
	{
		$sql = "select * from posaddresses where posaddress_id = " . $form->value("posaddress");
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$form->value("posaddress_id", $row["posaddress_id"]);
			
			
			$form->value("posaddress_client_id", $row["posaddress_client_id"]);
			$form->value("posaddress_ownertype", $row["posaddress_ownertype"]);
			$form->value("posaddress_eprepnr", $row["posaddress_eprepnr"]);
			$form->value("posaddress_sapnumber", $row["posaddress_sapnumber"]);
			$form->value("posaddress_franchisor_id", $row["posaddress_franchisor_id"]);
			$form->value("posaddress_franchisee_id", $row["posaddress_franchisee_id"]);
			
			
			$form->value("posaddress_name", $row["posaddress_name"]);
			$form->value("posaddress_address", $row["posaddress_address"]);
			$form->value("posaddress_address2", $row["posaddress_address2"]);
			$form->value("posaddress_zip", $row["posaddress_zip"]);
			$form->value("posaddress_place", $row["posaddress_place"]);
			$form->value("posaddress_country", $row["posaddress_country"]);


			$form->value("posaddress_phone", $row["posaddress_phone"]);
			$form->value("posaddress_fax", $row["posaddress_fax"]);
			$form->value("posaddress_email", $row["posaddress_email"]);
			$form->value("posaddress_website", $row["posaddress_website"]);

			$form->value("posaddress_store_postype", $row["posaddress_store_postype"]);
			$form->value("posaddress_store_subclass", $row["posaddress_store_subclass"]);
			$form->value("posaddress_store_furniture", $row["posaddress_store_furniture"]);
			$form->value("posaddress_store_openingdate", $row["posaddress_store_openingdate"]);
			$form->value("posaddress_store_closingdate", $row["posaddress_store_closingdate"]);
		}
	}
	else
	{
			
			$form->value("posaddress_id", "");
			
			
			$form->value("posaddress_client_id", "");
			$form->value("posaddress_ownertype", "");
			$form->value("posaddress_eprepnr", "");
			$form->value("posaddress_sapnumber", "");
			$form->value("posaddress_franchisor_id", "");
			$form->value("posaddress_franchisee_id", "");
			
			
			$form->value("posaddress_name", "");
			$form->value("posaddress_address", "");
			$form->value("posaddress_address2", "");
			$form->value("posaddress_zip", "");
			$form->value("posaddress_place", "");
			$form->value("posaddress_country", "");


			$form->value("posaddress_phone", "");
			$form->value("posaddress_fax", "");
			$form->value("posaddress_email", "");
			$form->value("posaddress_website", "");

			$form->value("posaddress_store_postype", "");
			$form->value("posaddress_store_subclass", "");
			$form->value("posaddress_store_furniture", "");
			$form->value("posaddress_store_openingdate", "");
			$form->value("posaddress_store_closingdate", "");
	}


}
elseif($form->button("save_form"))
{
	
	if(!$form->value("posorder_id") and $form->value("posaddress_id") > 0)
	{
		$sql = "insert into posorders (" . 
			   "posorder_posaddress, " .
		       "posorder_order, " .
			   "posorder_ordernumber, " .
			   "posorder_type) Values ( " .
			   $form->value("posaddress_id") . ", " .
			   id() . ", " .
			   dbquote($order["order_number"]) . ", " .
			   2  . ")";

		$result = mysql_query($sql) or dberror($sql);
		$last_id = mysql_insert_id();
		$form->value("posorder_id", $last_id);
		

		$form->message("Your data has been saved.");
	}
	elseif(!$form->value("posorder_id") and $form->validate())
	{

		$subclass = $form->value("posaddress_store_subclass");
		if(!$subclass)
		{
			$subclass = 0;
		}
		$sql = "insert into posaddresses (" . 
			   "posaddress_client_id, " . 
			   "posaddress_ownertype, " . 
			   "posaddress_eprepnr, " .
			   "posaddress_sapnumber, " .
			   "posaddress_franchisor_id, " . 
			   "posaddress_franchisee_id, " . 
			   "posaddress_name, " . 
			   "posaddress_address, " . 
			   "posaddress_address2, " . 
			   "posaddress_zip, " . 
			   "posaddress_place, " . 
			   "posaddress_country, " . 
			   "posaddress_phone, " . 
			   "posaddress_fax, " . 
			   "posaddress_email, " . 
			   "posaddress_website, " . 
			   "posaddress_store_postype, " . 
			   "posaddress_store_subclass, " . 
			   "posaddress_store_furniture, " . 
			   "posaddress_store_openingdate, " . 
			   "posaddress_store_closingdate, " .
			   "checked2) Values (" . 
				$form->value("posaddress_client_id") . ", " . 
				$form->value("posaddress_ownertype") . ", " . 
				dbquote($form->value("posaddress_eprepnr")) . ", " .
			    dbquote($form->value("posaddress_sapnumber")) . ", " . 
				dbquote($form->value("posaddress_franchisor_id")) . ", " . 
				dbquote($form->value("posaddress_franchisee_id")) . ", " . 
				dbquote($form->value("posaddress_name")) . ", " . 
				dbquote($form->value("posaddress_address")) . ", " . 
				dbquote($form->value("posaddress_address2")) . ", " . 
				dbquote($form->value("posaddress_zip")) . ", " . 
				dbquote($form->value("posaddress_place")) . ", " . 
				$form->value("posaddress_country") . ", " . 
				dbquote($form->value("posaddress_phone")) . ", " . 
				dbquote($form->value("posaddress_fax")) . ", " . 
				dbquote($form->value("posaddress_email")) . ", " . 
				dbquote($form->value("posaddress_website")) . ", " . 
				dbquote($form->value("posaddress_store_postype")) . ", " . 
				dbquote($subclass) . ", " . 
				dbquote($form->value("posaddress_store_furniture")) . ", " . 
				dbquote(from_system_date($form->value("posaddress_store_openingdate"))) . ", " . 
				dbquote(from_system_date($form->value("posaddress_store_closingdate"))) . ", " . 
			    $form->value("checked2") . ")";
				
				$result = mysql_query($sql) or dberror($sql);

				$last_id = mysql_insert_id();

				$form->value("posaddress_id", $last_id);


		$sql = "insert into posorders (" . 
			   "posorder_posaddress, " .
		       "posorder_order, " .
			   "posorder_ordernumber, " .
			   "posorder_type) Values ( " .
			   $last_id . ", " .
			   id() . ", " .
			   dbquote($order["order_number"]) . ", " .
			   2  . ")";

		$result = mysql_query($sql) or dberror($sql);
		$last_id = mysql_insert_id();
		$form->value("posorder_id", $last_id);

		$form->message("Your data has been saved.");
	}
	elseif($form->value("posorder_id") and $form->validate())
	{

		$subclass = $form->value("posaddress_store_subclass");
		if(!$subclass)
		{
			$subclass = 0;
		}
		
		$sql = "update posaddresses SET " . 
			   "posaddress_client_id= " . $form->value("posaddress_client_id") . ", " . 
			   "posaddress_ownertype= " . $form->value("posaddress_ownertype") . ", " . 
			   "posaddress_eprepnr= " . dbquote($form->value("posaddress_eprepnr")) . ", " . 
			   "posaddress_sapnumber= " . dbquote($form->value("posaddress_sapnumber")) . ", " . 
			   "posaddress_franchisor_id= " . $form->value("posaddress_franchisor_id") . ", " . 
			   "posaddress_franchisee_id= " . $form->value("posaddress_franchisee_id") . ", " . 
			   "posaddress_name= " . dbquote($form->value("posaddress_name")) . ", " . 
			   "posaddress_address= " . dbquote($form->value("posaddress_address")) . ", " . 
			   "posaddress_address2= " . dbquote($form->value("posaddress_address2")) . ", " . 
			   "posaddress_zip= " . dbquote($form->value("posaddress_zip")) . ", " . 
			   "posaddress_place= " . dbquote($form->value("posaddress_place")) . ", " . 
			   "posaddress_country= " . $form->value("posaddress_country") . ", " . 
			   "posaddress_phone= " . dbquote($form->value("posaddress_phone")) . ", " . 
			   "posaddress_fax= " . dbquote($form->value("posaddress_fax")) . ", " . 
			   "posaddress_email= " . dbquote($form->value("posaddress_email")) . ", " . 
			   "posaddress_website= " . dbquote($form->value("posaddress_website")) . ", " . 
			   "posaddress_store_postype= " . $form->value("posaddress_store_postype") . ", " . 
			   "posaddress_store_subclass= " . dbquote($subclass) . ", " . 
			   "posaddress_store_furniture= " . $form->value("posaddress_store_furniture") . ", " . 
			   "posaddress_store_openingdate= " . dbquote(from_system_date($form->value("posaddress_store_openingdate"))) . ", " . 
			   "posaddress_store_closingdate= " . dbquote(from_system_date($form->value("posaddress_store_closingdate"))) . ", " .
			   "checked2 = " . $form->value("checked2")  . 
			   " where posaddress_id = " .$form->value("posaddress_id");
				
				
		$result = mysql_query($sql) or dberror($sql);

		$form->message("Your data has been saved.");
	}
}
elseif(!$form->value("posaddress_id"))
{
	
	$form->value("posaddress_client_id", $order["order_client_address"]);
	$form->value("posaddress_ownertype", "");
	$form->value("posaddress_eprepnr", "");
	$form->value("posaddress_sapnumber", "");
	$form->value("posaddress_franchisor_id", 466);
	$form->value("posaddress_franchisee_id", "");
	
	
	$form->value("posaddress_name", $order["order_shop_address_company"]);
	$form->value("posaddress_address", $order["order_shop_address_address"]);
	$form->value("posaddress_address2", $order["order_shop_address_address2"]);
	$form->value("posaddress_zip", $order["order_shop_address_zip"]);
	$form->value("posaddress_place", $order["order_shop_address_place"]);
	$form->value("posaddress_country", $order["order_shop_address_country"]);


	$form->value("posaddress_phone", $order["order_shop_address_phone"]);
	$form->value("posaddress_fax", $order["order_shop_address_fax"]);
	$form->value("posaddress_email", $order["order_shop_address_email"]);
	$form->value("posaddress_website", "");
}


// Render page
$poslocation = get_poslocation(id(), "posaddresses");

$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title("Assign POS Address to Projects");

$form->render();
$page->footer();

?>