<?php
/********************************************************************

    posfilegroup.php

    Creation and mutation of file group records.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");

$form = new Form("posfilegroups", "file group");

$form->add_section();
$form->add_edit("posfilegroup_name", "Name*", NOTNULL | UNIQUE);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("posfilegroups");
$page->header();
$page->title(id() ? "Edit File group" : "Add File group");
$form->render();
$page->footer();

?>