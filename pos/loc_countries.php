<?php
/********************************************************************

    loc_countries.php

    Lists countries for translations.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");
set_referer("loc_country.php");

//get all translations
$translations = array();
$sql = "select country_id,language_name " . 
       "from countries " . 
       "left join loc_countries on loc_country_country = country_id " . 
	   "left join languages on language_id = loc_country_language " . 
	   "order by language_name";


$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	if(array_key_exists($row["country_id"], $translations))
	{
		$translations[$row["country_id"]] = $translations[$row["country_id"]] . "/" . trim($row["language_name"]);
	}
	else
	{
		$translations[$row["country_id"]] = trim($row["language_name"]);
	}
}	   

$sql = "select country_id, country_name " .
       "from countries ";

$list = new ListView($sql);

$list->set_entity("countries");
$list->set_order("country_name");


$list->add_column("country_name", "Name", "loc_country.php", LIST_FILTER_FREE);
$list->add_text_column("translations", "Translations", COLUMN_UNDERSTAND_HTML, $translations);

$list->process();

$page = new Page("locales");

$page->header();
$page->title("Countries");
$list->render();
$page->footer();
?>
