<?php
/********************************************************************

    posorder.php

    Creation and mutation of posorder records.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

check_access("can_use_posindex");

$pos = array();
if(param("pos_id") > 0)
{
	$pos = get_poslocation(param("pos_id"), "posaddresses");
}

$system_currency = get_system_currency_fields();

/********************************************************************
    Check if a projects exists in projects
*********************************************************************/
$order_exists = 0;
$sql = "select posorder_id " . 
       "from posorders " .
	   "where posorder_id = " . id() . " and posorder_order > 0";

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$order_exists = 1;	
}


/********************************************************************
    Build Form
*********************************************************************/

$form = new Form("posorders", "POS Order");

$form->add_section("Name and address");

require_once("include/poslocation_head.php");

$form->add_hidden("pos_id", param("pos_id"));
$form->add_hidden("posorder_posaddress", param("pos_id"));
$form->add_hidden("posorder_type", 2);

$form->add_section("Order Details");
$form->add_label("posorder_ordernumber", "Order Number");
$form->add_hidden("country", param("country"));
$form->add_hidden("ltf", param("ltf"));
$form->add_hidden("let", param("let"));
$form->add_hidden("ostate", param("ostate"));
//$form->add_button("save_form", "Save");
$form->add_button("back", "Back");

if(has_access("can_edit_posindex"))
{
	$form->add_button("delete_order", "Delete Order");
}

// Populate form and process button clicks

$form->populate();
$form->process();


if($form->button("save_form"))
{
	if($form->validate())
	{
		$form->save();
		$form->message("The data has been saved.");
	}
}
elseif($form->button("back"))
{
	$link = "posorders.php?pos_id=" . param("pos_id")  . "&country=" . param("country"). '&let=' . param('let') . "&ltf=" . param("ltf"). "&ostate=" . param("ostate");
	redirect($link);
}
elseif($form->button("delete_order"))
{
	$sql = "delete from posorders where posorder_id = " . id();
	$res = mysql_query($sql) or dberror($sql);

	$link = "posorders.php?pos_id=" . param("pos_id")  . "&country=" . param("country"). '&let=' . param('let') . "&ltf=" . param("ltf"). "&ostate=" . param("ostate");
	redirect($link);
}


/********************************************************************
    Render page
*********************************************************************/ 

$page = new Page("posindex");
require "include/pos_page_actions.php";
$page->header();

$page->title("Edit Order Information");

require_once("include/tabs.php");

$form->render();
$page->footer();

?>