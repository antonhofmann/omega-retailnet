<?php
/********************************************************************

    loc_province.php

    Lists translations of provinces.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");
set_referer("loc_province_province.php");

$province_id = 0;

if(param("id"))
{
	$province_id = param("id");
}
elseif(param("province_id"))
{
	$province_id = param("province_id");
}


//get province name
$province_name = "";
$sql = "select province_country, province_canton from provinces where province_id = " . $province_id;
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$province_name = $row["province_canton"];
	$province_country = $row["province_country"];
}

//sql for the list
$sql = "select loc_province_id, loc_province_canton, language_name " .
       "from loc_provinces " . 
	   "left join languages on language_id = loc_province_language ";

$list = new ListView($sql);

$list->set_entity("loc_provinces");
$list->set_order("language_name");
$list->set_filter("loc_province_country = " . $province_country);

$list->add_hidden("province_id", $province_id);
$list->add_hidden("province_country", param("province_country"));
$list->add_column("language_name", "Language", "loc_province_province.php?province_id=" . $province_id, LIST_FILTER_FREE);
$list->add_column("loc_province_canton", "Province Name");

$list->add_button(LIST_BUTTON_NEW, "Add Translation", "loc_province_province.php?province_id=" . $province_id);
$list->add_button("back", "Back");

$list->process();

if($list->button("back"))
{
	$link = "loc_provinces.php?province_country= " . $province_country;
	redirect($link);
}

$page = new Page("locales");

$page->header();
$page->title("Translations for " . $province_name);
$list->render();
$page->footer();
?>
