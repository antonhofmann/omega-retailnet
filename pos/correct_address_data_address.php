<?php
/********************************************************************

    correct_address_data_address.php

    Correct Address Data

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_correct_pos_data");

if(param("address_id"))
{
	register_param("address_id");
	param("id", param("address_id"));
}
else
{
	register_param("address_id");
	param("address_id", id());
}

$sql_places = "select place_id, concat(place_name, ' (', province_canton, ')') as place " . 
				  "from places " .
				  "left join provinces on province_id = place_province " . 
				  "where place_country = {address_country} order by place_name";


$address_checked = 0;
if(param("address_checked") != 1)
{
	$sql = "select address_checked from _addresses where address_id = " . id();
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$address_checked = $row["address_checked"];
	}
}


// Build form
$form = new Form("_addresses", "address");
$form->add_hidden("country", param("country"));

/*
$form->add_section("Address Numbers");
$form->add_edit("address_number", "Address Number");
$form->add_edit("address_legnr", "Legal Number");
$form->add_edit("address_sapnr", "SAP Number");
*/

$form->add_section("Address Data");
//$form->add_edit("address_legal_entity_name", "Legal Entity Name");
$form->add_edit("address_company", "Company*", NOTNULL);
$form->add_edit("address_company2", "");
$form->add_edit("address_address", "Address*", NOTNULL);
$form->add_edit("address_address2", "Address 2");

$form->add_list("address_country", "Country*",
    "select country_id, country_name from countries order by country_name", NOTNULL | SUBMIT);


$form->add_list("address_place_id", "City*", $sql_places, NOTNULL | SUBMIT);
$form->add_edit("address_place", "", DISABLED);

$form->add_edit("address_zip", "Zip");

$form->add_section("Communication");
$form->add_edit("address_phone", "Phone*", NOTNULL);
$form->add_edit("address_fax", "Fax");
$form->add_edit("address_email", "Email*", NOTNULL);
$form->add_edit("address_contact_name", "Contact Name*", NOTNULL);
$form->add_edit("address_website", "Website");

$form->add_section("Address Check");
$form->add_checkbox("address_checked", "Adress is checked and correct", "", 0, "Address check*");


$form->add_button("save", "Save my Corrections");
$form->add_button("back", "Back");

// Populate form and process button clicks

$form->populate();
$form->process();


if($address_checked == 2)
{
	$form->value("address_checked", 0);
}


if($form->button("back"))
{
	redirect("correct_address_data.php?country=" . param("country"));
}
elseif($form->button("save"))
{
	
	//$form->add_validation("is_email_address({address_email})", "The email address is invalid.");
	//$form->add_validation("is_web_address({address_website})", "The website url is invalid Use the form 'http://www...'.");

	
	if(!$form->value("address_checked"))
	{
		$form->save();
		$sql = "update _addresses set address_checked = 2 where address_id = " . id();
		mysql_query($sql) or dberror($sql);
	}
	elseif($form->validate())
	{
		$form->save();
		redirect("correct_address_data.php?country=" . param("country"));
	}
}
elseif($form->button("address_place_id"))
{
	$sql = "select place_name from places where place_id = " . dbquote($form->value("address_place_id"));
	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$form->value("address_place", $row["place_name"]);
	}

}
elseif($form->button("address_country"))
{
	$form->value("address_place_id", "");
	$form->value("address_place", "");
}

// Render page
$page = new Page("posaddresses", "Companies: Data Correction");
require "include/pos_page_actions.php";
$page->header();

$page->title("Companies: Data Correction");

$form->render();

$page->footer();

?>