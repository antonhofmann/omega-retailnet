<?php
/********************************************************************

    loc_areaperception_type.php

    Lists translations of POS Area Perception Types.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");
set_referer("loc_areaperception_type_areaperception_type.php");

$areaperception_type_id = 0;

if(param("id"))
{
	$areaperception_type_id = param("id");
}
elseif(param("areaperception_type_id"))
{
	$areaperception_type_id = param("areaperception_type_id");
}


//get areaperception_type name
$areaperception_type_name = "";
$sql = "select areaperception_type_name from areaperception_types where areaperception_type_id = " . $areaperception_type_id;
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$areaperception_type_name = $row["areaperception_type_name"];
}

//sql for the list
$sql = "select loc_areaperception_type_id, loc_areaperception_type_name, language_name " .
       "from loc_areaperception_types " . 
	   "left join languages on language_id = loc_areaperception_type_language ";

$list = new ListView($sql);

$list->set_entity("loc_areaperception_types");
$list->set_order("language_name");

$list->add_hidden("areaperception_type_id", $areaperception_type_id);
$list->add_column("language_name", "Language", "loc_areaperception_type_areaperception_type.php?areaperception_type_id=" . $areaperception_type_id, LIST_FILTER_FREE);
$list->add_column("loc_areaperception_type_name", "POS Area Perception Type");

$list->add_button(LIST_BUTTON_NEW, "Add Translation", "loc_areaperception_type_areaperception_type.php?areaperception_type_id=" . $areaperception_type_id);
$list->add_button(LIST_BUTTON_BACK, "Back");

$list->process();

$page = new Page("locales");

$page->header();
$page->title("Translations for Area Perception Type " . $areaperception_type_name);
$list->render();
$page->footer();
?>
