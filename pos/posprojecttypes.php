<?php
/********************************************************************

    posprojecttypes.php

    Lists POS project types for editing.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-10-25
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-10-25
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_administrate_posindex");
set_referer("posprojecttype.php");

$sql = "select posproject_type_id, postype_name, project_costtype_text, projectkind_name, " .
	   "IF(posproject_type_needs_cer = 1, 'yes', 'no') as cer, " .
	   "IF(posproject_type_needs_af = 1, 'yes', 'no') as af, " .
	   "IF(posproject_type_needs_agreement = 1, 'yes', 'no') as agreement " .
       "from posproject_types " . 
	   "left join postypes on postype_id = posproject_type_postype " .
	   "left join project_costtypes on project_costtype_id = posproject_type_projectcosttype " .
	   "left join projectkinds on projectkind_id = posproject_type_projectkind";

$list = new ListView($sql);

$list->set_entity("posproject_types");
$list->set_order("postype_name, project_costtype_text, projectkind_name");

$list->add_column("postype_name", "POS Type", "posprojecttype.php");
$list->add_column("project_costtype_text", "Legal Type");
$list->add_column("projectkind_name", "Project Kind");
$list->add_column("cer", "CER");
$list->add_column("af", "AF");
$list->add_column("agreement", "Agreement");

$list->add_button(LIST_BUTTON_NEW, "New", "posprojecttype.php");

$list->process();

$page = new Page("posprojecttypes");

$page->header();
$page->title("POS Project Types");
$list->render();
$page->footer();

?>
