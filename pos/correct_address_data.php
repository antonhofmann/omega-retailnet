<?php
/********************************************************************

    correct_address_data.php

    Lists of addresses (POS)

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_correct_pos_data");
set_referer("correct_pos_data_pos.php");

//clean _addresses
$sql = "select _addresses.address_id as id " .
       "from _addresses " .
	   "left join addresses on addresses.address_id = _addresses.address_id " .
	   "where addresses.address_id is null ";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$sql = "delete from _addresses where address_id = " . $row["id"];
	$result = mysql_query($sql) or dberror($sql);

}

$preselect_filter = "";
if(param("country"))
{
	$preselect_filter = "address_country = " . param("country");
	register_param("country", param("country"));
}

if(has_access("can_view_his_posindex") or has_access("can_edit_his_posindex"))
{
	$user = get_user(user_id());
	
	if(!param("country"))
	{
		redirect("welcome.php");
	}

	$country_filter = "";
	$tmp = array();
	$sql = "select * from country_access " .
		   "where country_access_user = " . user_id();


	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{            
		$tmp[] = $row["country_access_country"];
	}

	if(count($tmp) > 0) {
		$country_filter = " address_country IN (" . implode(",", $tmp) . ") ";
	}

	if($country_filter) 
	{
		if($preselect_filter)
		{
			$preselect_filter .= " and (address_parent = " . $user["address"] . " or " . $country_filter . ") "; 
		}
		else
		{
			$preselect_filter = " and (address_parent = " . $user["address"] . " or " . $country_filter . ") ";
		}
	}
	else
	{
		if($preselect_filter)
		{
			$preselect_filter .= " and address_parent = " . $user["address"]; 
		}
		else
		{
			$preselect_filter = " and (address_parent = " . $user["address"] . " or address_country in (" . $user_countries . ")) ";
		}
	}
}
elseif(!has_access("can_view_his_posindex") 
       and !has_access("can_edit_his_posindex") 
	   and !has_access("can_view_posindex") 
	   and !has_access("can_edit_posindex"))
{
	redirect("welcome.php");
}

$checked = array();
$sql_images = "select address_id, address_checked  " . 
              "from _addresses ";

$res = mysql_query($sql_images) or dberror($sql_images);

while ($row = mysql_fetch_assoc($res))
{
	if($row["address_checked"] == 1)
	{
		$checked[$row["address_id"]] = "/pictures/bullet_ball_glass_green.gif";
	}
	elseif($row["address_checked"] == 2)
	{
		$checked[$row["address_id"]] = "/pictures/bullet_ball_glass_yellow.gif";
	}
	else
	{
		$checked[$row["address_id"]] = "/pictures/bullet_ball_glass_red.gif";
	}
}

//compose list

$sql = "select address_id, address_company, " .
       "address_address, address_address2, address_zip, " .
       "    address_place, country_name " .
       "from _addresses " . 
	   "left join countries on address_country = country_id ";

$list = new ListView($sql);

$list->set_entity("_addresses");
$list->set_filter("address_active = 1 and " . $preselect_filter);
$list->set_order("address_company, address_place");

$list->add_hidden("country", param("country"));

$list->add_image_column("checked", "checked", COLUMN_ALIGN_CENTER , $checked);
$list->add_column("address_company", "Company", "correct_address_data_address.php?country=" . param("country") , LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("address_place", "City", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("address_address", "Address", "", LIST_FILTER_FREE);


$list->populate();
$list->process();

$page = new Page("posaddresses", "Companies: Data Corrections");
require "include/pos_page_actions.php";
$page->header();

$page->title("Companies: Data Corrections");
$list->render();


$page->footer();

?>
