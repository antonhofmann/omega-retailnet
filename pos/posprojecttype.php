<?php
/********************************************************************

    posprojecttype.php

    Creation and mutation of POS project type records.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-10-25
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-10-25
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");



$form = new Form("posproject_types", "POS Project Type");

$form->add_list("posproject_type_postype", "POS Type",
		"select postype_id, postype_name from postypes order by postype_name");

$form->add_list("posproject_type_projectcosttype", "Legal Type",
		"select project_costtype_id, project_costtype_text from project_costtypes order by project_costtype_text");

$form->add_list("posproject_type_projectkind", "Project Kind",
		"select projectkind_id, projectkind_name from projectkinds where projectkind_id > 0 order by projectkind_name");

$form->add_checkbox("posproject_type_needs_cer", "needs CER", "", 0, "Capital Expenditure Request");
$form->add_checkbox("posproject_type_needs_af", "needs AF", "", 0, "Application Form");
$form->add_checkbox("posproject_type_needs_agreement", "needs franchisee agreement", "", 0, "Franchisee Agreement");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_DELETE, "Delete");
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();
$form->process();

$page = new Page("posprojecttypes");
$page->header();
$page->title(id() ? "Edit POS Project Type" : "Add POS Project Type");
$form->render();
$page->footer();

?>