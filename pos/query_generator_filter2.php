<?php
/********************************************************************

    query_generator_filter2.php

    Filter Selection for the Query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2009-01-11
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2009-01-11
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";


check_access("can_query_posindex");

require_once "include/query_get_functions.php";

/********************************************************************
    prepare all data needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("query_generator.php");
}

$query_id = param("query_id");

$posquery = get_query_name($query_id);


$posquery_area_perception = array();
$posquery_area_perception_values = array();
$perception_values = array();
for($i=1;$i<36;$i++)
{
	$perception_values[$i] = "";
}

//check if filter is present
$sql = "select posquery_area_perception from posqueries " .
	   "where posquery_id = " . $query_id;

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$posquery_area_perception = explode  ("@@" , $row["posquery_area_perception"]);
	if(array_key_exists(0,$posquery_area_perception))
	{
		$posquery_area_perception_values = explode  ("-" , $posquery_area_perception[0]);
		foreach($posquery_area_perception_values as $key=>$value)
		{
			$perception_values[$value] = 1;
		}
	}

}


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("posqueries", "query_generator");

$form->add_hidden("query_id", param("query_id"));
$form->add_section($posquery["name"]);

$form->add_section(" ");
$form->add_comment("Please select the the filter criteria of your query.");

$link = "javascript:open_selector('')";



//area perception

$form->add_section("Area Perception Filter");
$form->add_checkbox("perc_class1", "1", $perception_values[1], 0, "Class/Image Area");
$form->add_checkbox("perc_class2", "2", $perception_values[2], 0, "Class/Image Area");
$form->add_checkbox("perc_class3", "3", $perception_values[3], 0, "Class/Image Area");
$form->add_checkbox("perc_class4", "4", $perception_values[4], 0, "Class/Image Area");
$form->add_checkbox("perc_class5", "5", $perception_values[5], 0, "Class/Image Area");
$form->add_comment("");
$form->add_checkbox("perc_tourist1", "1", $perception_values[6], 0, "Tourist/Historical Area");
$form->add_checkbox("perc_tourist2", "2", $perception_values[7], 0, "Tourist/Historical Area");
$form->add_checkbox("perc_tourist3", "3", $perception_values[8], 0, "Tourist/Historical Area");
$form->add_checkbox("perc_tourist4", "4", $perception_values[9], 0, "Tourist/Historical Area");
$form->add_checkbox("perc_tourist5", "5", $perception_values[10], 0, "Tourist/Historical Area");

$form->add_comment("");
$form->add_checkbox("perc_transport1", "1", $perception_values[11], 0, "Public Transportation");
$form->add_checkbox("perc_transport2", "2", $perception_values[12], 0, "Public Transportation");
$form->add_checkbox("perc_transport3", "3", $perception_values[13], 0, "Public Transportation");
$form->add_checkbox("perc_transport4", "4", $perception_values[14], 0, "Public Transportation");
$form->add_checkbox("perc_transport5", "5", $perception_values[15], 0, "Public Transportation");

$form->add_comment("");
$form->add_checkbox("perc_people1", "1", $perception_values[16], 0, "People Traffic Area");
$form->add_checkbox("perc_people2", "2", $perception_values[17], 0, "People Traffic Area");
$form->add_checkbox("perc_people3", "3", $perception_values[18], 0, "People Traffic Area");
$form->add_checkbox("perc_people4", "4", $perception_values[19], 0, "People Traffic Area");
$form->add_checkbox("perc_people5", "5", $perception_values[20], 0, "People Traffic Area");

$form->add_comment("");
$form->add_checkbox("perc_parking1", "1", $perception_values[21], 0, "Parking Possibilities");
$form->add_checkbox("perc_parking2", "2", $perception_values[22], 0, "Parking Possibilities");
$form->add_checkbox("perc_parking3", "3", $perception_values[23], 0, "Parking Possibilities");
$form->add_checkbox("perc_parking4", "4", $perception_values[24], 0, "Parking Possibilities");
$form->add_checkbox("perc_parking5", "5", $perception_values[25], 0, "Parking Possibilities");

$form->add_comment("");
$form->add_checkbox("perc_visibility11", "1", $perception_values[26], 0, "Visibility from Pavement");
$form->add_checkbox("perc_visibility12", "2", $perception_values[27], 0, "Visibility from Pavement");
$form->add_checkbox("perc_visibility13", "3", $perception_values[28], 0, "Visibility from Pavement");
$form->add_checkbox("perc_visibility14", "4", $perception_values[29], 0, "Visibility from Pavement");
$form->add_checkbox("perc_visibility15", "5", $perception_values[30], 0, "Visibility from Pavement");

$form->add_comment("");
$form->add_checkbox("perc_visibility21", "1", $perception_values[31], 0, "Visibility from across the Street");
$form->add_checkbox("perc_visibility22", "2", $perception_values[32], 0, "Visibility from across the Street");
$form->add_checkbox("perc_visibility23", "3", $perception_values[33], 0, "Visibility from across the Street");
$form->add_checkbox("perc_visibility24", "4", $perception_values[34], 0, "Visibility from across the Street");
$form->add_checkbox("perc_visibility25", "5", $perception_values[35], 0, "Visibility from across the Street");

$form->add_button("save", "Save Filter");

if(check_if_query_has_fields($query_id) == true)
{
	$form->add_button("execute", "Execute Query");
}
$form->add_button("back", "Back to the List of Queries");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Process buttons
*********************************************************************/ 
if($form->button("save"))
{
	

	//Area perception filter
	$perc_values = "";
	$perc_filter = "";
	$tmp_filter = array();
	for($i=1;$i<6;$i++)
	{
		if($form->value("perc_class" . $i))
		{
			$tmp_filter[] .= $i;
			$perc_values .= $i . "-";
		}
	}
	if(count($tmp_filter) > 0)
	{
		$perc_filter = "posaddresses.posaddress_perc_class in (" . implode(",", $tmp_filter) . ")";
	}

	$tmp_filter = array();
	for($i=1;$i<6;$i++)
	{
		if($form->value("perc_tourist" . $i))
		{
			$tmp_filter[] .= $i;
			$perc_values .= $i+5 . "-";
		}
	}
	if(count($tmp_filter) > 0)
	{
		if($perc_filter)
		{
			$perc_filter .= " or posaddresses.posaddress_perc_tourist in (" . implode(",", $tmp_filter) . ")";
		}
		else
		{
			$perc_filter = "posaddresses.posaddress_perc_tourist in (" . implode(",", $tmp_filter) . ")";
		}
	}


	$tmp_filter = array();
	for($i=1;$i<6;$i++)
	{
		if($form->value("perc_transport" . $i))
		{
			$tmp_filter[] .= $i;
			$perc_values .= $i+10 . "-";
		}
	}
	if(count($tmp_filter) > 0)
	{
		if($perc_filter)
		{
			$perc_filter .= " or posaddresses.posaddress_perc_transport in (" . implode(",", $tmp_filter) . ")";
		}
		else
		{
			$perc_filter = "posaddresses.posaddress_perc_transport in (" . implode(",", $tmp_filter) . ")";
		}
	}


	$tmp_filter = array();
	for($i=1;$i<6;$i++)
	{
		if($form->value("perc_people" . $i))
		{
			$tmp_filter[] .= $i;
			$perc_values .= $i+15 . "-";
		}
	}
	if(count($tmp_filter) > 0)
	{
		if($perc_filter)
		{
			$perc_filter .= " or posaddresses.posaddress_perc_people in (" . implode(",", $tmp_filter) . ")";
		}
		else
		{
			$perc_filter = "posaddresses.posaddress_perc_people in (" . implode(",", $tmp_filter) . ")";
		}
	}

	$tmp_filter = array();
	for($i=1;$i<6;$i++)
	{
		if($form->value("perc_parking" . $i))
		{
			$tmp_filter[] .= $i;
			$perc_values .= $i+20 . "-";
		}
	}
	if(count($tmp_filter) > 0)
	{
		if($perc_filter)
		{
			$perc_filter .= " or posaddresses.posaddress_perc_parking in (" . implode(",", $tmp_filter) . ")";
		}
		else
		{
			$perc_filter = "posaddresses.posaddress_perc_parking in (" . implode(",", $tmp_filter) . ")";
		}
	}

	for($i=1;$i<6;$i++)
	{
		if($form->value("perc_visibility1" . $i))
		{
			$tmp_filter[] .= $i;
			$perc_values .= $i+25 . "-";
		}
	}
	if(count($tmp_filter) > 0)
	{
		if($perc_filter)
		{
			$perc_filter .= " or posaddresses.posaddress_perc_visibility1 in (" . implode(",", $tmp_filter) . ")";
		}
		else
		{
			$perc_filter = "posaddresses.posaddress_perc_visibility1 in (" . implode(",", $tmp_filter) . ")";
		}
	}

	for($i=1;$i<6;$i++)
	{
		if($form->value("perc_visibility2" . $i))
		{
			$tmp_filter[] .= $i;
			$perc_values .= $i+30 . "-";
		}
	}
	if(count($tmp_filter) > 0)
	{
		if($perc_filter)
		{
			$perc_filter .= " or posaddresses.posaddress_perc_visibility2 in (" . implode(",", $tmp_filter) . ")";
		}
		else
		{
			$perc_filter = "posaddresses.posaddress_perc_visibility2 in (" . implode(",", $tmp_filter) . ")";
		}
	}

	$sql = "update posqueries " . 
		   "set posquery_area_perception = " . dbquote($perc_values . "@@" . $perc_filter)  . ", " . 
		   "date_modified = " . dbquote(date("Y-m-d H:s:i")) . ", " . 
		    "user_modified = " . dbquote(user_login()) . 
		   " where posquery_id = " . param("query_id");

	$result = mysql_query($sql) or dberror($sql);
}
elseif($form->button("back"))
{
	redirect("query_generator.php");
}
elseif($form->button("execute"))
{
	redirect("query_generator_xls.php?query_id=" . param("query_id"));
}

/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page("query_generator");
$page->header();
$page->title("Edit POS Query - Filter");

require_once("include/query_tabs.php");

$form->render();


$page->footer();
?>