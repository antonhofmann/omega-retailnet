<?php
/********************************************************************

    dataanalysis_projects.php

    Analyse posorders and show possible error

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_administrate_posindex");
set_referer("dataanalysis_assign_pos_address.php");


/********************************************************************
    projects not assigned to a POS Address
*********************************************************************/


$preselect_filter = "";
$dropdown_filter = " where posaddress_country = 0 ";
if(param("country"))
{
	$preselect_filter = " and order_shop_address_country = " . param("country");
	$dropdown_filter = " where posaddress_country = " . param("country");
	register_param("country", param("country"));
}

$sql = "select order_id, order_number, country_name, order_archive_date, order_actual_order_state_code, " . 
       "concat(order_shop_address_company, ', ', order_shop_address_address) as shop, " .
	   "concat(order_shop_address_zip, ', ', order_shop_address_place) as place, order_shop_address_place " .
	   "from orders " . 
       "left join posorders on posorder_order = order_id " . 
	   "left join countries on country_id = order_shop_address_country";

$filter = "order_actual_order_state_code != '900' and order_type = 2 and posorder_id is NUll " . $preselect_filter;

$sqlpos = "select posaddress_id, concat(country_name, ', ', posaddress_place, ', ', posaddress_name, ', ', posaddress_address) as posname " . 
          "from posaddresses " . 
		  "left join countries on country_id = posaddress_country " .
		  $dropdown_filter .
		  " order by country_name, posaddress_place";

$values = array();


$infolinks = array();
$sql_p = $sql . " where " . $filter;
$res_p = mysql_query($sql_p) or dberror($sql_p);

while ($row_p = mysql_fetch_assoc($res_p))
{

	if($row_p["order_id"] > 0)
	{
		if($row_p["order_archive_date"] == NULL or $row_p["order_archive_date"] == "0000-00-00")
		{
			$link = "<a href=\"/archive/order_edit_request.php?oid=" .  $row_p["order_id"] . "\" target=\"_blank\"><img src=\"/pictures/info.gif\" border='0'/></a>";
		}
		else
		{
			$link = "<a href=\"/user/order_edit_request.php?oid=" .  $row_p["order_id"] . "\" target=\"_blank\"><img src=\"/pictures/info.gif\" border='0'/></a>";
		}
			
		$infolinks[$row_p["order_id"]] = $link;
	}
}


/********************************************************************
    Create List
*********************************************************************/ 
$list4 = new ListView($sql);
$list4->set_entity("orders");
$list4->set_filter($filter);
$list4->set_title("Orders not beeing assigned to a POS Address");
$list4->set_order("country_name, order_shop_address_place, shop");

$list4->add_hidden("country", param("country"));
$list4->add_hidden("order_id", "order_id");


$list4->add_column("order_number", "Order", "", "","", COLUMN_NO_WRAP);
$list4->add_column("order_actual_order_state_code", "State", "", "","", COLUMN_NO_WRAP);
$list4->add_column("country_name", "Country", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list4->add_column("order_shop_address_place", "City", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list4->add_column("shop", "Shop Address", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list4->add_text_column("info", "", COLUMN_UNDERSTAND_HTML, $infolinks);
$list4->add_list_column("posaddress", "POS", $sqlpos, 0, $values);


$list4->add_button("save", "Save");
$list4->populate();
$list4->process();


if($list4->button("save"))
{
	foreach ($list4->values("posaddress") as $key=>$value)
    {
		
		// update record
        if ($value)
        {
			$sql = "select order_number from orders where order_id = " . $key;
			
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{

				$sql = "insert into posorders (" . 
					   "posorder_posaddress, " .
					   "posorder_order, " .
					   "posorder_ordernumber, " .
					   "posorder_type) Values ( " .
					   $value . ", " .
					   $key . ", " .
					   dbquote($row["order_number"]) . ", " .
					   2  . ")";

				$result = mysql_query($sql) or dberror($sql);
			}
		}
	}
}


$page = new Page("posindex");
require "include/pos_page_actions.php";
$page->header();

$page->title("Order Orphans");

$list4->render();


$page->footer();
?>