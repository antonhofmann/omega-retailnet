<?php
/********************************************************************

    loc_province_province.php

    Creation and mutation of provinces translations.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");


$province_id = 0;

if(param("province_id"))
{
	$province_id = param("province_id");
}

//get province name
$province_name = "";
$sql = "select * " . 
       "from provinces " .
	   "left join countries on country_id = province_country " . 
	   "where province_id = " . $province_id;
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	
	$province_canton = $row["province_canton"];
	$province_region = $row["province_region"];
	$province_adminregion = $row["province_adminregion"];
	$province_shortcut = $row["province_shortcut"];

	$province_country = $row["province_country"];
	$country_name = $row["province_canton"];
}

$form = new Form("loc_provinces", "province");

$form->add_section("Basic Data");
$form->add_hidden("loc_province_country", $province_country);
$form->add_hidden("province_id", $province_id);
$form->add_hidden("loc_province_province", $province_id);
$form->add_label("province_country", "Country", 0, $country_name);
$form->add_label("province_canton", "Canton", 0, $province_canton);
$form->add_label("province_region", "Region", 0, $province_region);
$form->add_label("province_adminregion", "Administrative Region", 0, $province_adminregion);
$form->add_label("province_shortcut", "Shortcut", 0, $province_shortcut);

$form->add_section("Translation");
$form->add_list("loc_province_language", "Language*", 
    "select language_id, language_name from languages order by language_name", NOTNULL);

$form->add_edit("loc_province_canton", "Canton*", NOTNULL);
$form->add_edit("loc_province_region", "Region");
$form->add_edit("loc_province_adminregion", "Administrative Region");
$form->add_edit("loc_province_shortcut", "Shortcut");

$form->add_button("save", "Save");
$form->add_button("back", "Back");
$form->add_button("delete", "Delete");

$form->populate();
$form->process();

if($form->button("back"))
{
	$link = "loc_province.php?id= " . param("province_id");
	redirect($link);
}
elseif($form->button("save"))
{
	if($form->validate())
	{
		$form->save();
		$link = "loc_province.php?id= " . param("province_id");
		redirect($link);
	}
}
elseif($form->button("delete"))
{
		$sql = "delete from loc_provinces where loc_province_id =" . id();
		$result = mysql_query($sql) or dberror($sql);
		$link = "loc_province.php?id= " . param("province_id");
		redirect($link);

}

$page = new Page("locales");
$page->header();
$page->title(id() ? "Edit Province Translation" : "Add province Translation");
$form->render();
$page->footer();

?>