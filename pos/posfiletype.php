<?php
/********************************************************************

    posfiletype.php

    Creation and mutation of file type records.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");

$form = new Form("posfiletypes", "file type");

$form->add_section();
$form->add_edit("posfiletype_name", "Name*", NOTNULL | UNIQUE);
$form->add_edit("posfiletype_mime_type", "MIME Type*", NOTNULL);
$form->add_edit("posfiletype_extension", "Extension*", NOTNULL);

$form->add_section();
$form->add_multiline("posfiletype_description", "Description", 4);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("posfiletypes");
$page->header();
$page->title(id() ? "Edit File Type" : "Add File Type");
$form->render();
$page->footer();

?>