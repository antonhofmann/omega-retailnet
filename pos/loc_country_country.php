<?php
/********************************************************************

    loc_country_country.php

    Creation and mutation of country translations.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");


$country_id = 0;

if(param("country_id"))
{
	$country_id = param("country_id");
}

//get country name
$country_name = "";
$sql = "select country_name from countries where country_id = " . $country_id;
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$country_name = $row["country_name"];
}

$form = new Form("loc_countries", "country");

$form->add_section("Basic Data");
$form->add_hidden("country_id", $country_id);
$form->add_hidden("loc_country_country", $country_id);
$form->add_label("country_name", "Country", 0, $country_name);

$form->add_section("Translation");
$form->add_list("loc_country_language", "Language*", 
    "select language_id, language_name from languages order by language_name", NOTNULL);

$form->add_edit("loc_country_name", "Country Name*", NOTNULL);

$form->add_button("save", "Save");
$form->add_button("back", "Back");
$form->add_button("delete", "Delete");

$form->populate();
$form->process();

if($form->button("back"))
{
	$link = "loc_country.php?id= " . param("country_id");
	redirect($link);
}
elseif($form->button("save"))
{
	if($form->validate())
	{
		$form->save();
		$link = "loc_country.php?id= " . param("country_id");
		redirect($link);
	}
}
elseif($form->button("delete"))
{
		$sql = "delete from loc_countries where loc_country_id =" . id();
		$result = mysql_query($sql) or dberror($sql);
		$link = "loc_country.php?id= " . param("country_id");
		redirect($link);

}

$page = new Page("locales");
$page->header();
$page->title(id() ? "Edit Country Translation" : "Add Country Translation");
$form->render();
$page->footer();

?>