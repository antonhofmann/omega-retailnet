<?php
/********************************************************************

    loc_languages.php

    Lists laguages for translations.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");
set_referer("loc_language.php");

//get all translations
$translations = array();
$sql = "select language_id " . 
       "from languages " . 
	   "order by language_name";


$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$sql = "select language_name " . 
		   "from loc_languages " . 
		   "left join languages on loc_language_language = language_id " .
		   "where loc_language_language_id = " . $row["language_id"] . 
		   " order by language_name";

	$res2 = mysql_query($sql) or dberror($sql);
	while ($row2 = mysql_fetch_assoc($res2))
	{
	
		if(array_key_exists($row["language_id"], $translations))
		{
			$translations[$row["language_id"]] = $translations[$row["language_id"]] . "/" . trim($row2["language_name"]);
		}
		else
		{
			$translations[$row["language_id"]] = trim($row2["language_name"]);
		}
	}
}	   

$sql = "select language_id, language_name " .
       "from languages ";

$list = new ListView($sql);

$list->set_entity("languages");
$list->set_order("language_name");


$list->add_column("language_name", "Name", "loc_language.php", LIST_FILTER_FREE);
$list->add_text_column("translations", "Translations", COLUMN_UNDERSTAND_HTML, $translations);

$list->process();

$page = new Page("locales");

$page->header();
$page->title("Languages");
$list->render();
$page->footer();
?>
