<?php
/********************************************************************

    posclosing.php

    Creation and mutation of POS closing records.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2011-06-15
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2011-06-15
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

check_access("can_edit_pos_closings");

$pos = array();
if(param("pos_id") > 0)
{
	$pos = get_poslocation(param("pos_id"), "posaddresses");
}

//get all data
$projects = array();
$sql = "select project_id, project_number from posorders " . 
                "inner join projects on project_order = posorder_order " . 
				"where posorder_posaddress = " . param("pos_id") . 
				" order by project_number";

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
	$projects[$row["project_id"]] = $row["project_number"];
}

$sql = "select project_id, project_number from posorderspipeline " . 
	   "inner join projects on project_order = posorder_order " . 
	   "where posorder_parent_table = 'posaddresses' and posorder_posaddress = " . param("pos_id") . 
	   " order by project_number";

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
	$projects[$row["project_id"]] = $row["project_number"];
}

// Build form

$form = new Form("posclosings", "posclosure");

$form->add_section("Name and address");

require_once("include/poslocation_head.php");

$form->add_hidden("country", param("country"));
$form->add_hidden("pos_id", param("pos_id"));
$form->add_hidden("posclosing_posaddress", param("pos_id"));
$form->add_hidden("ltf", param("ltf"));
$form->add_hidden("let", param("let"));
$form->add_hidden("ostate", param("ostate"));

$form->add_section("Temporary Closure Details");
$form->add_list("posclosing_project", "Project Context*", $projects, NOTNULL);
$form->add_edit("posclosing_startdate", "Start Date*", NOTNULL, "", TYPE_DATE);
$form->add_edit("posclosing_enddate", "End Date*", NOTNULL, "", TYPE_DATE);
$form->add_edit("posclosing_reason", "Reason", 0);


$form->add_button('save', "Save");
if(id() > 0) {
	$form->add_button('delete', "Delete");
}

$form->add_button("back", "Back");

// Populate form and process button clicks

$form->populate();
$form->process();

if($form->button("save"))
{
	if($form->validate()) {
		$form->save();

		//update store locator
		update_store_locator(param("pos_id"));
		$form->message("Your data has been saved!");
	}
}
elseif($form->button("back"))
{
	$link = "posclosings.php?pos_id=" . param("pos_id")  . "&country=" . param("country") . '&let=' . param('let') . "&ltf=" . param("ltf") . "&ostate=" . param("ostate");
	redirect($link);
}
elseif($form->button('delete'))
{
	$sql = 'delete from posclosings where posclosing_id = ' . id();
	$res = mysql_query($sql) or dberror($sql);

	update_store_locator(param("pos_id"));
	
	$link = "posclosings.php?pos_id=" . param("pos_id")  . "&country=" . param("country") . '&let=' . param('let') . "&ltf=" . param("ltf") . "&ostate=" . param("ostate");
	redirect($link);
}

// Render page

$page = new Page("posindex");
require "include/pos_page_actions.php";
$page->header();

$page->title(id() ? "Edit Temporary Closure Information" : "Add Temporary Closure Information");

require_once("include/tabs.php");

$form->render();
$page->footer();

?>