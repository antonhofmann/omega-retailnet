<?php
/********************************************************************

    query_generator_fields.php

    Field Selection for the Query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2009-01-11
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2009-01-11
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_query_posindex");

require_once "include/query_get_functions.php";


/********************************************************************
    prepare all data needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("query_generator.php");
}

$query_id = param("query_id");

$posquery = get_query_name($query_id);
$icon = "../pictures/add_item.gif";
$link = "javascript:open_selector('')";


//get Query Fields
$fields = "";
$fields_posaddresses = array();
$fields_posleases = array();
$fields_posclosings = array();
$fields_clientaddresses = array();
$fields_franchiseeaddresses = array();
$fields_posorders = array();
$fields_latest_posorders = array();
$fields_intangibles = array();
$fields_distributionchannels = array();
$fields_statistics = array();

$sql = "select posquery_fields from posqueries " .
	   "where posquery_id = " . $query_id;

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$fields = array();
	
	$fields = unserialize($row["posquery_fields"]);
	foreach($fields as $field=>$caption)
	{
		if(array_key_exists("pl", $fields))
		{
			$fields_posaddresses = $fields["pl"];
		}
		if(array_key_exists("cl", $fields))
		{
			$fields_clientaddresses = $fields["cl"];
		}
		if(array_key_exists("fr", $fields))
		{
			$fields_franchiseeaddresses = $fields["fr"];
		}
		if(array_key_exists("po", $fields))
		{
			$fields_posorders = $fields["po"];
		}
		if(array_key_exists("lpo", $fields))
		{
			$fields_latest_posorders = $fields["lpo"];
		}
		if(array_key_exists("in", $fields))
		{
			$fields_intangibles = $fields["in"];
		}
		if(array_key_exists("le", $fields))
		{
			$fields_posleases = $fields["le"];
		}
		if(array_key_exists("clo", $fields))
		{
			$fields_posclosings = $fields["clo"];
		}
		if(array_key_exists("dcs", $fields))
		{
			$fields_distributionchannels = $fields["dcs"];
		}
		if(array_key_exists("stat", $fields))
		{
			$fields_statistics = $fields["stat"];
		}
	}

}

/********************************************************************
    create form
*********************************************************************/

$form = new Form("posqueries", "query_generator");

$form->add_hidden("query_id", param("query_id"));
$form->add_section($posquery["name"]);

$form->add_section(" ");
$form->add_comment("Please select the fields that should be shown in the query.");
$form->add_section("Selected Fields");

//POS Location Fields
$selected_posaddress_fields = "";
if($fields_posaddresses)
{
	foreach($fields_posaddresses as $key=>$caption)
	{
		//$caption = str_replace("POS ", "", $caption);
		if(array_key_exists($key,$fields_posaddresses))
		{
			$selected_posaddress_fields .= $caption . ", ";
		}
	}
	$selected_posaddress_fields = substr($selected_posaddress_fields, 0, strlen($selected_posaddress_fields) - 2);
}
$form->add_label_selector("posaddresses", "POS Location", 0, $selected_posaddress_fields, $icon, $link);

//POS Leases Fields
$selected_posleases_fields = "";
if($fields_posleases)
{
	foreach($fields_posleases as $key=>$caption)
	{
		//$caption = str_replace("POS ", "", $caption);
		if(array_key_exists($key,$fields_posleases))
		{
			$selected_posleases_fields .= $caption . ", ";
		}
	}
	$selected_posleases_fields = substr($selected_posleases_fields, 0, strlen($selected_posleases_fields) - 2);
}
$form->add_label_selector("posleases", "POS Leases", 0, $selected_posleases_fields, $icon, $link);


//POS Closings Fields
$selected_posclosings_fields = "";
if($fields_posclosings)
{
	foreach($fields_posclosings as $key=>$caption)
	{
		//$caption = str_replace("POS ", "", $caption);
		if(array_key_exists($key,$fields_posclosings))
		{
			$selected_posclosings_fields .= $caption . ", ";
		}
	}
	$selected_posclosings_fields = substr($selected_posclosings_fields, 0, strlen($selected_posclosings_fields) - 2);
}
$form->add_label_selector("posclosings", "POS Temporary Closures", 0, $selected_posclosings_fields, $icon, $link);

//POS Distribution Channel Fields Fields
$selected_distributionchannel_fields = "";
if($fields_distributionchannels)
{
	foreach($fields_distributionchannels as $key=>$caption)
	{
		//$caption = str_replace("POS ", "", $caption);
		if(array_key_exists($key,$fields_distributionchannels))
		{
			$selected_distributionchannel_fields .= $caption . ", ";
		}
	}
	$selected_distributionchannel_fields = substr($selected_distributionchannel_fields, 0, strlen($selected_distributionchannel_fields) - 2);
}
$form->add_label_selector("distributionchannel", "Distributionchannel", 0, $selected_distributionchannel_fields, $icon, $link);


//Client Adress Fields
$selected_clientaddress_fields = "";
if($fields_clientaddresses)
{
	foreach($fields_clientaddresses as $key=>$caption)
	{
		$caption = str_replace("Client ", "", $caption);
		if(array_key_exists($key,$fields_clientaddresses))
		{
			$selected_clientaddress_fields .= $caption . ", ";
		}
	}
	$selected_clientaddress_fields = substr($selected_clientaddress_fields, 0, strlen($selected_clientaddress_fields) - 2);
}
$form->add_label_selector("clientaddresses", "Client", 0, $selected_clientaddress_fields, $icon, $link);


//franchisee Adress Fields
$selected_franchiseeaddress_fields = "";
if($fields_franchiseeaddresses)
{
	foreach($fields_franchiseeaddresses as $key=>$caption)
	{
		$caption = str_replace("Franchisee ", "", $caption);
		if(array_key_exists($key,$fields_franchiseeaddresses))
		{
			$selected_franchiseeaddress_fields .= $caption . ", ";
		}
	}
	$selected_franchiseeaddress_fields = substr($selected_franchiseeaddress_fields, 0, strlen($selected_franchiseeaddress_fields) - 2);
}
$form->add_label_selector("franchiseeaddresses", "Owner Company/Franchisee", 0, $selected_franchiseeaddress_fields, $icon, $link);



//POS Order Fields of latest pos order
$selected_latest_posorder_fields = "";
if($fields_latest_posorders)
{
	foreach($fields_latest_posorders as $key=>$caption)
	{

		if(array_key_exists($key,$fields_latest_posorders))
		{
			$selected_latest_posorder_fields .= $caption . ", ";
		}
	}
	$selected_latest_posorder_fields = substr($selected_latest_posorder_fields, 0, strlen($selected_latest_posorder_fields) - 2);
}
$form->add_label_selector("latest_posorders", "Latest operating project", 0, $selected_latest_posorder_fields, $icon, $link);


//POS Order Fields
$selected_posorder_fields = "";
if($fields_posorders)
{
	foreach($fields_posorders as $key=>$caption)
	{
		//$caption = str_replace("Project ", "", $caption);
		if(array_key_exists($key,$fields_posorders))
		{
			$selected_posorder_fields .= $caption . ", ";
		}
	}
	$selected_posorder_fields = substr($selected_posorder_fields, 0, strlen($selected_posorder_fields) - 2);
}
$form->add_label_selector("posorders", "All Projects", 0, $selected_posorder_fields, $icon, $link);


//Intagible Fields
$selected_intangibles_fields = "";
if($fields_intangibles)
{
	foreach($fields_intangibles as $key=>$caption)
	{
		//$caption = str_replace("Project ", "", $caption);
		if(array_key_exists($key,$fields_intangibles))
		{
			$selected_intangibles_fields .= $caption . ", ";
		}
	}
	$selected_intangibles_fields = substr($selected_intangibles_fields, 0, strlen($selected_intangibles_fields) - 2);
}
$form->add_label_selector("intangibles", "Intangibles", 0, $selected_intangibles_fields, $icon, $link);


//Statistics Fields
$selected_statistics_fields = "";
if($fields_statistics)
{
	foreach($fields_statistics as $key=>$caption)
	{
		//$caption = str_replace("Project ", "", $caption);
		if(array_key_exists($key,$fields_statistics))
		{
			$selected_statistics_fields .= $caption . ", ";
		}
	}
	$selected_statistics_fields = substr($selected_statistics_fields, 0, strlen($selected_statistics_fields) - 2);
}
$form->add_label_selector("statistics", "Statistics", 0, $selected_statistics_fields, $icon, $link);


if(check_if_query_has_fields($query_id) == true)
{
	$form->add_button("execute", "Execute Query");
}
$form->add_button("back", "Back to the List of Queries");


/********************************************************************
    process form
*********************************************************************/
$form->populate();
$form->process();

if($form->button("back"))
{
	redirect("query_generator.php");
}
elseif($form->button("execute"))
{
	redirect("query_generator_xls.php?query_id=" . param("query_id"));
}

/********************************************************************
    render
*********************************************************************/

$page = new Page("query_generator");
$page->header();
$page->title("Edit POS Query - Fields");

require_once("include/query_tabs.php");

$form->render();

?>


<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#posaddresses_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/pos/query_generator_fields_selector.php?query_id=<?php echo $query_id;?>&t=pl'
    });
    return false;
  });
  $('#posleases_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/pos/query_generator_fields_selector.php?query_id=<?php echo $query_id;?>&t=le'
    });
    return false;
  });
  $('#posclosings_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/pos/query_generator_fields_selector.php?query_id=<?php echo $query_id;?>&t=clo'
    });
    return false;
  });
   $('#distributionchannel_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/pos/query_generator_fields_selector.php?query_id=<?php echo $query_id;?>&t=dcs'
    });
    return false;
  });
  $('#clientaddresses_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/pos/query_generator_fields_selector.php?query_id=<?php echo $query_id;?>&t=cl'
    });
    return false;
  });
  $('#franchiseeaddresses_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/pos/query_generator_fields_selector.php?query_id=<?php echo $query_id;?>&t=fr'
    });
    return false;
  });
  $('#posorders_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/pos/query_generator_fields_selector.php?query_id=<?php echo $query_id;?>&t=po'
    });
    return false;
  });
  $('#latest_posorders_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/pos/query_generator_fields_selector.php?query_id=<?php echo $query_id;?>&t=lpo'
    });
    return false;
  });
  $('#intangibles_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/pos/query_generator_fields_selector.php?query_id=<?php echo $query_id;?>&t=in'
    });
    return false;
  });
  $('#statistics_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/pos/query_generator_fields_selector.php?query_id=<?php echo $query_id;?>&t=stat'
    });
    return false;
  });
});
</script>


<?php

$page->footer();

?>