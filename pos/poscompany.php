<?php
/********************************************************************

    poscompany.php

    Creation and mutation of address records.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_use_posindex");

set_referer("poscompany_file.php");


$country = "";
if(id()) {
	$address = get_address(id());
	$country = $address['country'];
}

if(param("country")) {
	$country = param("country");
}

if(!$country and array_key_exists("country", $_SESSION) and $_SESSION["country"] > 0)
{
	$country = $_SESSION["country"];
}
else
{
	$_SESSION["country"] = $country;
}


//check if user can edit this company
$user = get_user(user_id());
$can_edit = false;
if(has_access("can_edit_posindex"))
{
	$can_edit = true;
}
elseif(has_access("can_edit_his_posindex") and $user["country"] == param("country"))
{
	$can_edit = true;
}


$sql_provinces = "select province_id, province_canton " . 
                 " from provinces " . 
				 " where province_country = " . dbquote(param('address_country')) .
				 " order by province_canton";



//get POS Data
//owner
$franchisors = get_posaddresses_franchisors(id());
$franchisees = get_posaddresses_franchisees(id());


if(param("address_country"))
{
	$sql_places = "select place_id, concat(place_name, ' (', province_canton, ')') as place " . 
				  "from places " .
				  "left join provinces on province_id = place_province " . 
				  "where place_country = " . dbquote(param("address_country")) . " order by place_name";
}
else
{

	$sql_places = "select place_id, concat(place_name, ' (', province_canton, ')') as place " . 
				  "from places " .
				  "left join provinces on province_id = place_province " . 
				   "where place_country = " . dbquote($country) . " order by place_name";
}

$places = array();

$res = mysql_query($sql_places) or dberror($sql_places);
while ($row = mysql_fetch_assoc($res))
{
	$places[$row['place_id']] = $row['place'];
}

if(param("address_country")) {
	$places['999999999999999999999'] = "New City not listed above";
}


if(has_access("can_view_posindex") or has_access("can_edit_posindex"))
{
	$sql_countries = "select DISTINCT country_id, country_name " .
		   "from countries " . 
		   "where country_name is not null " . 
		   "order by country_name";
}
else
{
	$user = get_user(user_id());

	


	$country_filter = "";
	$tmp = array();
	$sql = "select * from country_access " .
		   "where country_access_user = " . user_id();


	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{            
		$tmp[] = $row["country_access_country"];
	}

	if(count($tmp) > 0) {
		$country_filter = " country_id IN (" . implode(",", $tmp) . ") ";
	}


	if($country_filter == "")
	{
		$sql_countries = "select DISTINCT country_id, country_name " .
			   "from countries " . 
			   "where country_name is not null " .
			   " and country_id = " . $user["country"] . 
			   " order by country_name";
	}
	else
	{
		$sql_countries = "select DISTINCT country_id, country_name " .
			   "from countries " . 
			   "where country_name is not null " .
			   " and " . $country_filter .
			   " order by country_name";
	}
}

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("addresses", "address");

$form->add_hidden("active", param("active"));
$form->add_hidden("country", $country);
$form->add_hidden("address_showinposindex", 1);

$form->add_hidden("address_filter", param("address_filter"));

$form->add_section("Name and Address");

if(has_access("can_edit_posindex") or has_access("can_edit_his_posindex"))
{
	$form->add_edit("address_number", "Address Number");
	$form->add_edit("address_legnr", "Legal Number");
	$form->add_edit("address_sapnr", "SAP Number");
	$form->add_edit("address_shortcut", "Shortcut*", NOTNULL);
	
	
	//$form->add_edit("address_mps_customernumber", "Customer Number");

	
	$form->add_edit("address_legal_entity_name", "Legal Entity Name");
	$form->add_edit("address_company", "Company*", NOTNULL, "", TYPE_CHAR, 0, 0, 2, "address_company");
	$form->add_edit("address_company2", "", 0, "", TYPE_CHAR, 0, 0, 2, "address_company2");
	$form->add_edit("address_address", "Address*", NOTNULL, "", TYPE_CHAR, 0, 0, 2, "address_address");
	$form->add_edit("address_address2", "", 0, "", TYPE_CHAR, 0, 0, 2, "address_address2");
	
	
	$form->add_list("address_country", "Country*", $sql_countries, NOTNULL | SUBMIT);

	$form->add_edit("address_zip", "Zip");
	

	if(param('address_place_id') == 999999999999999999999) {
		
		$form->add_list("province", "Province*", $sql_provinces, NOTNULL);
		$form->add_edit("new_place", "City Name*", NOTNULL, "", TYPE_CHAR);
		$form->add_hidden("address_place_id", 999999999999999999999);
		$form->add_hidden("address_place", "");
	}
	else {
		$form->add_list("address_place_id", "City*", $places, NOTNULL | SUBMIT);
		$form->add_edit("address_place", "", DISABLED);
		$form->add_hidden("new_place","");
	}
	

	$form->add_section("Communication");
	$form->add_edit("address_phone", "Phone");
	$form->add_edit("address_fax", "Fax");
	$form->add_edit("address_email", "Company Email");
	$form->add_edit("address_website", "Website");
	$form->add_edit("address_contact_name", "Contact Name");
	$form->add_edit("address_contact_email", "Contact Email");

	
	$form->add_section("Other Information");
	$form->add_list("address_type", "Type",
		"select address_type_id, address_type_name from address_types  where (address_type_id = 7 or address_type_id = 1) order by address_type_name", NOTNULL);

	$form->add_section("");
	$sql_addresses = "select address_id, concat(country_name, ': ', address_company) as company from addresses " .
                 "left join countries on country_id = address_country " .
				 "where address_type = 1  order by country_name, address_company";

	$form->add_list("address_parent", "Parent*",$sql_addresses);

	$form->add_checkbox("address_active", "Address in Use", true);
	//$form->add_checkbox("address_canbefranchisor", "Can Be Franchisor", false);
	$form->add_checkbox("address_canbefranchisee", "Can Be Franchisee", true);
	$form->add_checkbox("address_canbefranchisee_worldwide", "Franchisee can have worldwide projects");
	$form->add_checkbox("address_canbejointventure", "Is a Joint Venture Partner");
	$form->add_checkbox("address_canbecooperation", "Is a Cooperation Partner");
	$form->add_checkbox("address_is_independent_retailer", "Company is an independent retailer");
	$form->add_checkbox("address_can_own_independent_retailers", "Company can own independent retailes");
	

	$form->add_section("Relation");


	$form->add_checkbox("address_is_independent_retailer", "Company is an independent retailer");
	$form->add_edit("address_company_is_partner_since", "Company is partner since", 0, "", TYPE_DATE);


	if(count($franchisors) > 0)
	{
		$form->add_section("The Company is Franchisor to the following Shops");
		foreach($franchisors as $key=>$franchisor)
		{
			$form->add_iconlink("fo" . $key, "", 0, $franchisor, "../pictures/info.gif", "posindex_pos.php?id=" . $key, "_blank");
		}
	}

	if(count($franchisees) > 0)
	{
		$form->add_section("The Company is Franchisee (Owner) of the following POS Locations");
		foreach($franchisees as $key=>$franchisee)
		{
			$form->add_iconlink("fe" . $key, "", RENDER_HTML, $franchisee, "../pictures/info.gif", "posindex_pos.php?id=" . $key, "_blank");
		}
	}

	

	$form->add_button("save", "Save");
	$form->add_button(FORM_BUTTON_DELETE, "Delete");
}
elseif($can_edit == true and has_access("can_edit_his_posindex"))
{
	$form->add_lookup("address_type", "Type", "address_types", "address_type_name");
	$form->add_label("address_shortcut", "Shortcut");
	$form->add_label("address_number", "Address Number");
	$form->add_label("address_legnr", "Legal Number");
	$form->add_label("address_legal_entity_name", "Legal Entity Name");
	$form->add_label("address_sapnr", "SAP Number");
	$form->add_edit("address_company", "Company*", NOTNULL);
	$form->add_edit("address_company2", "");
	$form->add_edit("address_address", "Address");
	$form->add_edit("address_address2", "");
	
	$form->add_list("address_country", "Country*", $sql_countries, NOTNULL | SUBMIT);

	$form->add_edit("address_zip", "Zip");
	
	if(param('address_place_id') == 999999999999999999999) {
		
		$form->add_list("province", "Province*", $sql_provinces, NOTNULL);
		$form->add_edit("new_place", "City Name*", NOTNULL, "", TYPE_CHAR);
		$form->add_hidden("address_place_id", 999999999999999999999);
		$form->add_hidden("address_place", "");
	}
	else {
		$form->add_list("address_place_id", "City*", $places, NOTNULL | SUBMIT);
		$form->add_edit("address_place", "", DISABLED);
		$form->add_hidden("new_place","");
	}

	$form->add_section("Communication");
	$form->add_edit("address_phone", "Phone");
	$form->add_edit("address_fax", "Fax");
	$form->add_edit("address_email", "Company Email");
	$form->add_edit("address_website", "Website");
	$form->add_edit("address_contact_name", "Contact Name");
	$form->add_edit("address_contact_email", "Contact Email");

	$form->add_section(" ");
	if(count($franchisors) > 0)
	{
		$form->add_section("The Company is Franchisor to the following Shops");
		foreach($franchisors as $key=>$franchisor)
		{
			$form->add_iconlink("fo" . $key, "", 0, $franchisor, "../pictures/info.gif", "posindex_pos.php?id=" . $key, "_blank");
		}
	}

	if(count($franchisees) > 0)
	{
		$form->add_section("The Company is Franchisee (Owner) of the following Shops");
		foreach($franchisees as $key=>$franchisee)
		{
			$form->add_iconlink("fe" . $key, "", 0, $franchisee, "../pictures/info.gif", "posindex_pos.php?id=" . $key, "_blank");
		}
	}

	$form->add_button("save", "Save");
}
elseif(has_access("can_view_posindex") or has_access("can_view_his_posindex"))
{
	$form->add_lookup("address_type", "Type", "address_types", "address_type_name");
	$form->add_label("address_shortcut", "Shortcut");
	$form->add_label("address_number", "Address Number");
	$form->add_label("address_legnr", "Legal Number");
	$form->add_label("address_legal_entity_name", "Legal Entity Name");
	$form->add_label("address_sapnr", "SAP Number");
	$form->add_label("address_company", "Company");
	$form->add_label("address_company2", "");
	$form->add_label("address_address", "Address");
	$form->add_label("address_address2", "");
	$form->add_label("address_zip", "Zip");
	$form->add_label("address_place", "City");
	$form->add_lookup("address_country", "Country", "countries", "country_name");

	$form->add_section("Communication");
	$form->add_label("address_phone", "Phone");
	$form->add_label("address_fax", "Fax");
	$form->add_label("address_email", "Company Email");
	$form->add_label("address_website", "Website");
	$form->add_label("address_contact_name", "Contact Name");
	$form->add_label("address_contact_email", "Contact Email");

	
	if(count($franchisors) > 0)
	{
		$form->add_section("The Company is Franchisor to the following Shops");
		foreach($franchisors as $key=>$franchisor)
		{
			$form->add_iconlink("fo" . $key, "", 0, $franchisor, "../pictures/info.gif", "posindex_pos.php?id=" . $key, "_blank");
		}
	}

	if(count($franchisees) > 0)
	{
		$form->add_section("The Company is Franchisee (Owner) of the following Shops");
		foreach($franchisees as $key=>$franchisee)
		{
			$form->add_iconlink("fe" . $key, "", 0, $franchisee, "../pictures/info.gif", "posindex_pos.php?id=" . $key, "_blank");
		}
	}
}

$form->add_button("back", "Back to Company List");

//$form->add_validation("is_email_address({address_email})", "The company's email address is invalid.");
//$form->add_validation("is_web_address({address_website})", "The website url is invalid.");
//$form->add_validation("is_email_address({address_contact_email})", "The contact's email address is invalid.");


/********************************************************************
    Create List
*********************************************************************/ 

$sql_files = "select addressfile_id, addressfile_title, addressfile_path " . 
			 "from addressfiles ";
$list_filter = " addressfile_address = " . id();

//get all files
$files = array();
$sql = $sql_files . " where " . $list_filter;
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$link = "http://" . $_SERVER["HTTP_HOST"] . "/" . $row["addressfile_path"];
	$link = "<a href=\"javascript:popup('" . $link . "',800,600);\"><img src=\"/pictures/view.gif\" border='0'/></a>";
		
		$files[$row["addressfile_id"]] = $link;
}


$list = new ListView($sql_files);
$list->set_title("Files");
$list->set_entity("addressfiles");
$list->set_filter($list_filter);
$list->set_order("addressfile_title");

$list->add_text_column("files", "", COLUMN_UNDERSTAND_HTML, $files);

$link = "poscompany_file.php?address=" . id();

if(has_access("can_edit_his_posindex") or has_access("can_edit_posindex"))
{
	$list->add_column("addressfile_title", "Title", $link, "", "", COLUMN_NO_WRAP);
}
else
{
	$list->add_column("addressfile_title", "Title", "", "", "", COLUMN_NO_WRAP);
}

if(has_access("can_edit_posindex"))
{
	$list->add_button("newfile", "Add File", "");
}
elseif($can_edit == true and has_access("can_edit_his_posindex"))
{
	$list->add_button("newfile", "Add File", "");
}


// Populate form and process button clicks

$form->populate();
$form->process();

$list->populate();
$list->process();

if($form->button("back"))
{
	redirect("poscompanies.php?country=" . $form->value("country") . "&address_filter=" . $form->value("address_filter"));
}
elseif($form->button("save"))
{
	
	if($form->validate())
	{
		if($form->value("new_place")) {
			

			$sql = "insert into places (place_country, place_province, place_name, date_created, user_created) " . 
				   " Values (" . 
				   dbquote($form->value('address_country')) . "," .
				   dbquote($form->value('province')) . "," .
				   dbquote($form->value('new_place')) . "," .
				   dbquote(date("Y-m-d H:i:s")) . "," .
				   dbquote(user_login()) .
				   ")";
		   $res = mysql_query($sql) or dberror($sql);

		   $place_id = mysql_insert_id();
		   $form->value("address_place_id", $place_id);
		   $form->value("address_place", $form->value("new_place"));
		
		}		
		
		$form->save();
		redirect("poscompanies.php?country=" . $form->value("address_country") . "&address_filter=" . $form->value("address_filter"));
		//$form->message("Your data has been saved.");
	}
}
elseif($form->button("address_place_id"))
{
	$sql = "select place_name from places where place_id = " . dbquote($form->value("address_place_id"));
	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$form->value("address_place", $row["place_name"]);
	}

}
elseif($list->button("newfile"))
{
	$link = "poscompany_file.php?address=" . id() . "&country=" . param("country");
	redirect($link);
}

// Render page

$page = new Page("poscompanies");
require "include/pos_page_actions.php";
$page->header();

if(has_access("can_edit_posindex") or has_access("can_edit_his_posindex"))
{
	$page->title(id() ? "Edit Company Address" : "Add Company Address");
}
else
{
	$page->title("Company");
}
$form->render();

echo "<br /><br />";

$list->render();


?>

<script language="javascript">
	$("#h_address_company").click(function() {
	   $('#address_company').val($('#address_company').val().toLowerCase());
	   var txt = $('#address_company').val();

	   $('#address_company').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	$("#h_address_company2").click(function() {
	   $('#address_company2').val($('#address_company2').val().toLowerCase());
	   var txt = $('#address_company2').val();

	   $('#address_company2').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	
	$("#h_address_address2").click(function() {
	   $('#address_address2').val($('#address_address2').val().toLowerCase());
	   var txt = $('#address_address2').val();

	   $('#address_address2').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	
	$("#h_address_address").click(function() {
	   $('#address_address').val($('#address_address').val().toLowerCase());
	   var txt = $('#address_address').val();

	   $('#address_address').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	

	
</script>
<?php
$page->footer();

?>