<?php
/********************************************************************

    posindex_pos.php

    Creation and mutation of address records.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

require_once "include/check_access.php";

set_referer("posprojects.php");

if(param("pos_id"))
{
	register_param("pos_id");
	param("id", param("pos_id"));
}
else
{
	register_param("pos_id");
	param("pos_id", id());
}

$pos = array();
if(param("pos_id") > 0)
{
	$pos = get_poslocation(param("pos_id"), "posaddresses");
}

//get info for relocated POS
$former_relocated_pos = get_relocated_pos_info2(param("pos_id"));
$relocated_pos = get_relocated_pos_info3(param("pos_id"));

$former_relocated_pos_locations = get_relocated_pos_info4(param("pos_id"));

//update latest project with closing date
if(array_key_exists("posaddress_id", $pos))
{
	$sql =	"select posorder_id, posorder_order " .  
			"from posorders " .
			"left join orders on order_id = posorder_order " . 
			"where (order_actual_order_state_code <> '900' or order_actual_order_state_code is null) " . 
			" and posorder_type = 1 and posorder_posaddress = " . dbquote($pos["posaddress_id"]) .
			" and posorder_opening_date is not null and posorder_opening_date <> '0000-00-00' " .  
			" order by posorder_year DESC, posorder_opening_date DESC";

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{

		if($row["posorder_order"] > 0)
		{
			$sql = "update projects " . 
				   "set project_shop_closingdate = " . dbquote($pos["posaddress_store_closingdate"]) . 
				   " where project_order = " . $row["posorder_order"];
			$result = mysql_query($sql) or dberror($sql);
		}
		else // fake project
		{
			$sql = "update posorders  " . 
				   "set posorder_closing_date  = " . dbquote($pos["posaddress_store_closingdate"]) . 
				   " where posorder_id = " . $row["posorder_id"];
			$result = mysql_query($sql) or dberror($sql);
		}
	}
}


$has_project = update_posdata_from_posorders(param("pos_id"));

$ratings = array();
$ratings[5] = "bad";
$ratings[4] = "poor";
$ratings[3] = "good";
$ratings[2] = "very good";
$ratings[1] = "excellent";
//$ratings[0] = "n.a.";


$sql_floors = "select floor_id, floor_name " . 
		     "from floors " . 
		      " order by  floor_id";

$floors = array();
$res = mysql_query($sql_floors) or dberror($sql_floors);
while ($row = mysql_fetch_assoc($res))
{
	if($row["floor_id"] == 6)
	{
		$floors[$row["floor_id"]] = $row["floor_name"] . '<img src="/pictures/car.png" />';
	}
	else
	{
		$floors[$row["floor_id"]] = $row["floor_name"];
	}
}


//check if user can edit this company
$can_edit = false;
if(has_access("can_edit_posindex"))
{
	$can_edit = true;
}
elseif(has_access("can_edit_his_posindex"))
{
	$can_edit = get_user_edit_permission(user_id(), param("pos_id"));
}


// Build form
$form = new Form("posaddresses", "posaddress");

if($can_edit == true)
{
	$form->add_section("Roles");
	
	if(has_access("can_administrate_posindex"))
	{
		$form->add_list("posaddress_client_id", "Client*",
			"select address_id, concat(country_name, ': ', address_company, ', ', address_place, ' [', address_shortcut, ']') as company from addresses left join countries on country_id = address_country where (address_active = 1 and (address_type = 1 or address_type = 4)) or address_id = {posaddress_client_id} order by country_name, address_company", NOTNULL);

		if($has_project == false)
		{
			$form->add_list("posaddress_ownertype", "Legal Type*",
				"select project_costtype_id, project_costtype_text from project_costtypes where project_costtype_id IN (1, 2, 6)", SUBMIT | NOTNULL);
		}
		else
		{
			$form->add_lookup("posaddress_ownertype", "Legal Type", "project_costtypes", "project_costtype_text", 0, $pos["posaddress_ownertype"]);
		}

		$form->add_edit("posaddress_eprepnr", "Enterprise Reporting Number");
		$form->add_edit("posaddress_sapnumber", "SAP Number");

		if(array_key_exists("posaddress_store_postype", $pos) and $pos["posaddress_store_postype"] != 4)
		{
			$form->add_list("posaddress_franchisor_id", "Franchisor",
				"select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where address_showinposindex = 1 and country_name <> '' and address_company <> '' and address_place <> '' and address_canbefranchisor = 1 order by country_name, address_company");
		}
		elseif(id() == 0 and (param("posaddress_ownertype") == 1 or param("posaddress_ownertype") == 2))
		{
			$form->add_list("posaddress_franchisor_id", "Franchisor",
				"select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where address_showinposindex = 1 and country_name <> '' and address_company <> '' and address_place <> '' and address_canbefranchisor = 1 order by country_name, address_company");
		}
		else
		{
			$form->add_hidden('posaddress_franchisor_id');
		}

		if(array_key_exists("posaddress_ownertype", $pos) and $pos["posaddress_ownertype"] != 6)
		{
			$tmp = "Franchisee*";
		}
		else
		{
			$tmp = "Owner Company*";
		}
		
		if(array_key_exists("posaddress_store_postype", $pos) and $pos["posaddress_store_postype"] == 4)
		{
			$form->add_list("posaddress_franchisee_id", $tmp,
			"select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where address_showinposindex = 1 and country_name <> '' and address_company <> '' and address_place <> '' and (address_is_independent_retailer = 1 or address_can_own_independent_retailers = 1) order by country_name, address_company", NOTNULL);
		}
		else
		{
			$form->add_list("posaddress_franchisee_id", $tmp,
			"select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where address_showinposindex = 1 and country_name <> '' and address_company <> '' and address_place <> '' and address_canbefranchisee = 1  order by country_name, address_company", NOTNULL);
		}
	}
	elseif(param("pos_id") > 0 and $pos["posaddress_store_closingdate"] != '' and $pos["posaddress_store_closingdate"] != '0000-00-00' and $pos["posaddress_client_id"] > 0)
	{
		$form->add_lookup("posaddress_client_id", "Client", "addresses", "concat(address_company, ', ', address_place, ' [', address_shortcut, ']') as company", 0, $pos["posaddress_client_id"]);

		$form->add_lookup("posaddress_ownertype", "Legal Type", "project_costtypes", "project_costtype_text", 0, $pos["posaddress_ownertype"]);

		$form->add_edit("posaddress_eprepnr", "Enterprise Reporting Number");
		$form->add_edit("posaddress_sapnumber", "SAP Number");

		$form->add_lookup("posaddress_franchisor_id", "Franchisor", "addresses", "concat(address_company, ', ', address_place) as company", 0, $pos["posaddress_franchisor_id"]);

		if($pos["posaddress_ownertype"] != 6)
		{
			$tmp = "Franchisee";
		}
		else
		{
			$tmp = "Owner Company";
		}
		$form->add_lookup("posaddress_franchisee_id", $tmp, "addresses", "concat(address_company, ', ', address_place) as company", 0, $pos["posaddress_franchisee_id"]);

	}
	else
	{
		$form->add_list("posaddress_client_id", "Client*",
			"select address_id, concat(country_name, ': ', address_company, ', ', address_place, ' [', address_shortcut, ']') as company from addresses left join countries on country_id = address_country where address_active = 1 and (address_type = 1 or address_type = 4) order by country_name, address_company", NOTNULL);

		if($has_project == false)
		{
			$form->add_list("posaddress_ownertype", "Legal Type*",
				"select project_costtype_id, project_costtype_text from project_costtypes where project_costtype_id IN(1,2,6)", NOTNULL);
		}
		else
		{
			$form->add_lookup("posaddress_ownertype", "Legal Type", "project_costtypes", "project_costtype_text", 0, $pos["posaddress_ownertype"]);
		}

		$form->add_edit("posaddress_eprepnr", "Enterprise Reporting Number");
		$form->add_edit("posaddress_sapnumber", "SAP Number");

		$form->add_list("posaddress_franchisor_id", "Franchisor",
			"select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where address_showinposindex = 1 and country_name <> '' and address_company <> '' and address_place <> '' and address_canbefranchisor = 1 order by country_name, address_company");

		if($pos["posaddress_ownertype"] != 6)
		{
			$tmp = "Franchisee*";
		}
		else
		{
			$tmp = "Owner Company*";
		}
		$form->add_list("posaddress_franchisee_id", $tmp,
			"select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where address_showinposindex = 1 and country_name <> '' and address_company <> '' and address_place <> '' and address_canbefranchisee = 1 and address_country = " . $pos["posaddress_country"] . " order by country_name, address_company", NOTNULL);
	}


	$form->add_section("Address Data");
	//$form->add_edit("posaddress_name", "POS Name*", NOTNULL);
	$form->add_edit("posaddress_name", "POS Name*", NOTNULL, "", TYPE_CHAR, 0, 0, 2, "posaddress_name");

	//$form->add_edit("posaddress_name2", "", 0);
	$form->add_edit("posaddress_name2", "", 0, "", TYPE_CHAR, 0, 0, 2, "posaddress_name2");

	//$form->add_edit("posaddress_address", "Address*", NOTNULL);
	$form->add_edit("posaddress_address", "Address*", NOTNULL, "", TYPE_CHAR, 0, 0, 2, "posaddress_address");
	
	//$form->add_edit("posaddress_address2", "Address 2");
	$form->add_edit("posaddress_address2", "Address 2", 0, "", TYPE_CHAR, 0, 0, 2, "posaddress_address2");
	

	
	if(param("id"))
	{
		$form->add_list("posaddress_country", "Country*",
			"select country_id, country_name from countries order by country_name", NOTNULL | SUBMIT);
	}
	else
	{
		$form->add_list("posaddress_country", "Country*",
			"select country_id, country_name from countries order by country_name", NOTNULL | SUBMIT, param("country"));
	}

	
	if(param("posaddress_country"))
	{
		$form->add_list("posaddress_place_id", "City Selection*",
			"select place_id, concat(place_name, ' (', province_canton, ')') from places left join provinces on province_id = place_province where place_country = " . dbquote(param("posaddress_country")) . " order by place_name", SUBMIT | NOTNULL);
	}
	elseif(param("id"))
	{
		$form->add_list("posaddress_place_id", "City Selection*",
			"select place_id, concat(place_name, ' (', province_canton, ')') from places left join provinces on province_id = place_province where place_country = " . dbquote($pos["posaddress_country"]) . " order by place_name", SUBMIT | NOTNULL);
	}
	else
	{
		$form->add_list("posaddress_place_id", "City Selection*",
			"select place_id, concat(place_name, ' (', province_canton, ')') from places left join provinces on province_id = place_province where place_country = " . dbquote(param("country")) . " order by place_name", SUBMIT | NOTNULL);
	}
	
	$form->add_edit("posaddress_place", "City", NOTNULL | DISABLED);

	
	$form->add_edit("posaddress_zip", "Zip*", NOTNULL);


	if(count($former_relocated_pos_locations) > 0)
	{
		/*
		$link = '<a href="/pos/posindex_pos.php?pos_id=' . $former_relocated_pos["posaddress_id"] . '&country=' . param("country") .'&ltf=' . param("ltf") .'&psc=' . param("psc") .'&ostate=' . param("ostate") .'&province=' . param("province") .'&let=' . param("let") .'" target="_blank">' . $former_relocated_pos["posaddress_name"] . ', ' . $former_relocated_pos["place_name"] . '</a>';
		
		$form->add_section("Relocation Info");
		$form->add_comment("This POS is a relocation of the following POS:");
		$form->add_label("former_relocated_pos", "Relocated POS", RENDER_HTML, $link);
        */

		$form->add_section("Relocation History");
		
		if(count($former_relocated_pos_locations) > 1)
		{
			$form->add_comment("This POS was relocated " . count($former_relocated_pos_locations) . " times");
		}
		else {
			$form->add_comment("This POS is a relocation of the following POS:");
		}
		$tmp_opening_date = to_system_date($pos["posaddress_store_openingdate"]);
		foreach($former_relocated_pos_locations as $key=>$former_relocated_pos_location) {
			
			$link = '<a href="/pos/posindex_pos.php?pos_id=' . $former_relocated_pos_location["posaddress_id"] . '&country=' . param("country") .'&ltf=' . param("ltf") .'&psc=' . param("psc") .'&ostate=' . param("ostate") .'&province=' . param("province") .'&let=' . param("let") .'" target="_blank">' . $former_relocated_pos_location["posaddress_name"] . ', ' . $former_relocated_pos_location["place_name"] . '</a>';

			$form->add_label("fp_" . $key, "Relocation on " . $tmp_opening_date ." from", RENDER_HTML, $link);
			$tmp_opening_date = to_system_date($former_relocated_pos_location["posaddress_store_openingdate"]);
		
		}
		
	}
	elseif(count($relocated_pos) > 0)
	{
		$tmp_opening_date = to_system_date($relocated_pos["posaddress_store_openingdate"]);
		
		$link = '<a href="/pos/posindex_pos.php?pos_id=' . $relocated_pos["posaddress_id"] . '&country=' . param("country") .'&ltf=' . param("ltf") .'&psc=' . param("psc").'&ostate=' . param("ostate") .'&province=' . param("province") .'&let=' . param("let") .'" target="_blank">' . $relocated_pos["posaddress_name"] . ', ' . $relocated_pos["place_name"] . '</a>';
		
		$form->add_section("Relocation Info");
		$form->add_comment("This POS was relocated to the following POS:");
		$form->add_label("former_relocated_pos", "Relocated on " . $tmp_opening_date . " to", RENDER_HTML, $link);
	}

	
	$form->add_section("Communication");
	$form->add_edit("posaddress_phone", "Phone");
	$form->add_edit("posaddress_fax", "Fax");
	$form->add_edit("posaddress_email", "Email");
	
	/*
	$form->add_edit("posaddress_contact_name", "Contact Name");
	$form->add_edit("posaddress_website", "Website");
    */

	$form->add_section("Dates");
	if($has_project)
	{
		$form->add_label("posaddress_store_openingdate", "Opening Date");
		$form->add_label("posaddress_store_planned_closingdate", "Planned Closing Date");
		$form->add_label("posaddress_store_closingdate", "Closing Date");
	}
	else
	{
		$form->add_edit("posaddress_store_openingdate", "Opening Date", 0, "", TYPE_DATE);
		$form->add_edit("posaddress_store_planned_closingdate", "Planned Closing Date", 0, "", TYPE_DATE);
		$form->add_edit("posaddress_store_closingdate", "Closing Date", 0, "", TYPE_DATE);
	}


	$form->add_section("Environment");
	$sql = "select posareatype_id, posareatype_name " . 
			   "from posareatypes " . 
			   " order by posareatype_name";

	$form->add_checklist("area", "Environment", "posareas",$sql, RENDER_HTML);

	$form->add_section("Area Perception");
	$form->add_comment("Please rate by clicking a star: 5 stars are the best rating.");
	if(count($pos) > 0 and array_key_exists($pos["posaddress_perc_class"], $ratings))
	{
		$form->add_rating_selector("posaddress_perc_class", "Class/Image Area", $ratings, $ratings[$pos["posaddress_perc_class"]], NOTNULL);
		$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area", $ratings, $ratings[$pos["posaddress_perc_tourist"]], NOTNULL);
		$form->add_rating_selector("posaddress_perc_transport", "Public Transportation", $ratings,  $ratings[$pos["posaddress_perc_transport"]], NOTNULL);
		$form->add_rating_selector("posaddress_perc_people", "People Traffic Area", $ratings,  $ratings[$pos["posaddress_perc_people"]], NOTNULL);
		$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities", $ratings,  $ratings[$pos["posaddress_perc_parking"]], NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement", $ratings,  $ratings[$pos["posaddress_perc_visibility1"]], NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from accross the Street", $ratings,  $ratings[$pos["posaddress_perc_visibility2"]], NOTNULL);
	}
	else
	{
		$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from accross the Street*", $ratings, 0, NOTNULL);
	}

	//$form->add_section("----------------------------------------------------------------------");
	//$form->add_checkbox("checked2", "Address", "", 0, "revised");

	$form->add_hidden("country", param("country"));

	$form->add_section("Website " . BRAND_WEBSITE);
	$form->add_checkbox("posaddress_export_to_web", "Show this POS in the Store Locator", "", "", BRAND_WEBSITE);
	$form->add_checkbox("posaddress_email_on_web", "Show Email Address in the Store Locator", "", "", BRAND_WEBSITE);

	$form->add_section("Address Check");
	$form->add_checkbox("posaddress_checked", "Address check", "", "", "Adress is ok");

	$form->add_button(FORM_BUTTON_SAVE, "Save");
}
elseif(has_access("can_view_posindex") or has_access("can_view_his_posindex"))
{
	$company = "";
	$sql = "select concat(country_name, ': ', address_company, ', ', address_place, ' [', address_shortcut, ']') as company " . 
		   "from posaddresses " .
		   "left join addresses on address_id = posaddress_client_id " . 
		   "left join countries on country_id = address_country " . 
		   "where posaddress_id = " . param("pos_id");

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$company = $row["company"];
	}
	
	$form->add_label("company", "Client", 0, $company);
	
	$form->add_lookup("posaddress_ownertype", "Legal Type", "project_costtypes", "project_costtype_text");
	$form->add_label("posaddress_eprepnr", "Enterprise Reporting Number");
	$form->add_label("posaddress_sapnumber", "SAP Number");

	$company = "";
	$sql = "select concat(country_name, ': ', address_company, ', ', address_place, ' [', address_shortcut, ']') as company " . 
		   "from posaddresses " .
		   "left join addresses on address_id = posaddress_franchisor_id " . 
		   "left join countries on country_id = address_country " . 
		   "where posaddress_id = " . param("pos_id");

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$company = $row["company"];
	}
	$form->add_label("franchisor", "Franchisor", 0, $company);


	$company = "";
	$sql = "select concat(country_name, ': ', address_company, ', ', address_place, ' [', address_shortcut, ']') as company " . 
		   "from posaddresses " .
		   "left join addresses on address_id = posaddress_franchisee_id " . 
		   "left join countries on country_id = address_country " . 
		   "where posaddress_id = " . param("pos_id");

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$company = $row["company"];
	}
	
	if($pos["posaddress_ownertype"] != 6)
	{
		$tmp = "Franchisee";
	}
	else
	{
		$tmp = "Owner Company";
	}
	$form->add_label("franchisee", $tmp, 0, $company);

	$form->add_section("Address Data");
	$form->add_label("posaddress_name", "POS Name");
	$form->add_label("posaddress_name2", "");
	$form->add_label("posaddress_address", "Address");
	$form->add_label("posaddress_address2", "Address 2");
	$form->add_label("posaddress_zip", "Zip");
	$form->add_label("posaddress_place", "City");
	$form->add_lookup("posaddress_country", "Legal Country", "countries", "country_name");


	if(count($former_relocated_pos) > 0)
	{
		$link = '<a href="/pos/posindex_pos.php?pos_id=' . $former_relocated_pos["posaddress_id"] . '&country=' . param("country") .'&ltf=' . param("ltf") .'&psc=' . param("psc") .'&ostate=' . param("ostate") .'&province=' . param("province") .'&let=' . param("let") .'" target="_blank">' . $former_relocated_pos["posaddress_name"] . ', ' . $former_relocated_pos["place_name"] . '</a>';
		
		$form->add_section("Relocation Info");
		$form->add_comment("This POS is a relocation of the following POS:");
		$form->add_label("former_relocated_pos", "Relocated POS", RENDER_HTML, $link);
	}
	elseif(count($relocated_pos) > 0)
	{
		$link = '<a href="/pos/posindex_pos.php?pos_id=' . $relocated_pos["posaddress_id"] . '&country=' . param("country") .'&ltf=' . param("ltf") .'&psc=' . param("psc") .'&ostate=' . param("ostate") .'&province=' . param("province") .'&let=' . param("let") .'" target="_blank">' . $relocated_pos["posaddress_name"] . ', ' . $relocated_pos["place_name"] . '</a>';
		
		$form->add_section("Relocation Info");
		$form->add_comment("This POS was relocated to the following POS:");
		$form->add_label("former_relocated_pos", "Relocated POS", RENDER_HTML, $link);
	}

	$form->add_section("Communication");
	$form->add_label("posaddress_phone", "Phone");
	$form->add_label("posaddress_fax", "Fax");
	$form->add_label("posaddress_email", "Email");
	
	
	/*
	$form->add_label("posaddress_contact_name", "Contact Name");
	$form->add_label("posaddress_website", "Website");
    */

	$form->add_section("Dates");
	$form->add_label("posaddress_store_openingdate", "Opening Date");
	$form->add_label("posaddress_store_planned_closingdate", "Planned Closing Date");
	$form->add_label("posaddress_store_closingdate", "Closing Date");


	$form->add_section("Environment");
	$sql = "select posareatype_id, posareatype_name " . 
			   "from posareatypes " . 
			   " order by  posareatype_name";

	$form->add_checklist("area", "Environment", "posareas",$sql, RENDER_HTML);
	$form->add_hidden("country", param("country"));

	$form->add_section("Area Perception");
	
	$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from accross the Street*", $ratings, 0, NOTNULL);
}

$form->add_hidden("ltf", param("ltf"));
$form->add_hidden("let", param("let"));
$form->add_hidden("ostate", param("ostate"));
$form->add_button("back", "Back to POS List");

if(has_access("can_edit_catalog"))
{
	$form->add_button("delete", "Delete POS from POS List");
}

// Populate form and process button clicks

$form->populate();
$form->process();



if($form->button(FORM_BUTTON_SAVE))
{
	if($form->validate())
	{
		//check if google map is created
		$sql = "select posaddress_google_precision from posaddresses where posaddress_id = " . id();
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			if(!$row["posaddress_google_precision"])
			{
				$result = google_maps_geo_encode_pos(id(), $context);
			}
		}

		if(id())
		{
			//update_store_locator(id());
			//mysql_select_db(RETAILNET_DB, $db);
		}
		redirect("posindex.php?country=" . param("country") . '&let=' . param('let') . '&ltf=' . param("ltf"). '&ostate=' . param("ostate"));
	}
}
elseif($form->button("back"))
{
	redirect("posindex.php?country=" . param("country") . '&let=' . param('let') . '&ltf=' . param("ltf"). '&ostate=' . param("ostate"));
}
elseif($form->button("delete"))
{
	redirect("posindex_pos_delete.php?pos_id=" . id() . "&country=" . param("country"));	
}
elseif($form->button("posaddress_place_id"))
{
	$sql = "select place_name " .
		   "from places " . 
		   "where place_id = " . dbquote($form->value("posaddress_place_id"));
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$form->value("posaddress_place", $row["place_name"]);
	}
}
elseif($form->button("posaddress_country"))
{
	$form->value("posaddress_place",  "");
	$form->value("posaddress_place_id", 0);
}


// Render page
$poslocation = get_poslocation(id(), "posaddresses");

$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title(id() ? "Basic Data: " . $poslocation["posaddress_name"] : "Add POS Location");

if(id())
{
	require_once("include/tabs.php");
}

$form->render();


if(param('id')) {
	$sql_u = "update posorders SET
	          posorder_year = 1*SUBSTRING(posorder_opening_date, 1, 4)
			  where posorder_posaddress = " . dbquote(param('id'));
	$result = mysql_query($sql_u) or dberror($sql_u);
}

?>

<script language="javascript">
	$("#h_posaddress_name").click(function() {
	   $('#posaddress_name').val($('#posaddress_name').val().toLowerCase());
	   var txt = $('#posaddress_name').val();

	   $('#posaddress_name').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	$("#h_posaddress_name2").click(function() {
	   $('#posaddress_name2').val($('#posaddress_name2').val().toLowerCase());
	   var txt = $('#posaddress_name2').val();

	   $('#posaddress_name2').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	$("#h_posaddress_address").click(function() {
	   $('#posaddress_address').val($('#posaddress_address').val().toLowerCase());
	   var txt = $('#posaddress_address').val();

	   $('#posaddress_address').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	$("#h_posaddress_address2").click(function() {
	   $('#posaddress_address2').val($('#posaddress_address2').val().toLowerCase());
	   var txt = $('#posaddress_address2').val();

	   $('#posaddress_address2').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});



	
</script>
<?php
$page->footer();

?>