<?php
/********************************************************************

    loc_country.php

    Lists translations of countries.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");
set_referer("loc_country_country.php");

$country_id = 0;

if(param("id"))
{
	$country_id = param("id");
}
elseif(param("country_id"))
{
	$country_id = param("country_id");
}


//get country name
$country_name = "";
$sql = "select country_name from countries where country_id = " . $country_id;
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$country_name = $row["country_name"];
}

//sql for the list
$sql = "select loc_country_id, loc_country_name, language_name " .
       "from loc_countries " . 
	   "left join languages on language_id = loc_country_language ";

$list = new ListView($sql);

$list->set_entity("loc_countries");
$list->set_order("language_name");

$list->add_hidden("country_id", $country_id);
$list->add_column("language_name", "Language", "loc_country_country.php?country_id=" . $country_id, LIST_FILTER_FREE);
$list->add_column("loc_country_name", "Country Name");

$list->add_button(LIST_BUTTON_NEW, "Add Translation", "loc_country_country.php?country_id=" . $country_id);
$list->add_button(LIST_BUTTON_BACK, "Back");

$list->process();

$page = new Page("locales");

$page->header();
$page->title("Translations for " . $country_name);
$list->render();
$page->footer();
?>
