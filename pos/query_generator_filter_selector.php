<?php
/********************************************************************

    query_generator_filter_selector.php

    Enter Filter Criteria for the Query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2009-01-11
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2009-01-11
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../include/page_modal.php";

check_access("can_query_posindex");

require_once "include/query_get_functions.php";

/********************************************************************
    prepare all data needed
*********************************************************************/
$query_id = param("query_id");

//get user_country access
$restricted_countries = array();
$sql = "select * from country_access " .
	   "where country_access_user = " . user_id();
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{            
	$restricted_countries[] = $row["country_access_country"];
}



if(count($restricted_countries) > 0)
{

	$sql_salesregion = "select DISTINCT salesregion_id, salesregion_name ".
		               "from countries " .
		               "left join salesregions on salesregion_id = country_salesregion " . 
		               "where country_id IN (" . implode(",", $restricted_countries) . ") " . 
					   "order by salesregion_name";

	$sql_region = "select DISTINCT region_id, region_name ".
		          "from countries " .
		          "left join regions on region_id = country_region " . 
		          "where country_id IN (" . implode(",", $restricted_countries) . ") " . 
				  "order by region_name";
}
else
{
	$sql_salesregion = "select salesregion_id, salesregion_name ".
						"from salesregions order by salesregion_name";

	$sql_region = "select region_id, region_name ".
						"from regions order by region_name";
}


$filter = array();
//get benchmark parameters
$salesregions = array();
$regions = array();


$filter = get_query_filter($query_id);

$tmp_filter = "";
if(count($filter["re"]) > 0)
{
	$salesregions = explode("-", $filter["re"]);
	foreach($salesregions as $key=>$salesregion)
	{
		$tmp_filter = $tmp_filter . $salesregion . ",";
	}
	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);

	if($tmp_filter)
	{
		$tmp_filter = "where country_salesregion in (" . $tmp_filter . ") ";
	}	
	
}



$sql_country = "select DISTINCT country_id, country_name " .
			   "from posaddresses " .
			   "left join countries on country_id = posaddress_country " .
			   $tmp_filter . 
			   " order by country_name";

$tmp_filter = "";
if(count($filter["gr"]) > 0)
{
	$regions = explode("-", $filter["gr"]);
	$tmp_filter = "";
	foreach($regions as $key=>$region)
	{
		$tmp_filter = $tmp_filter . $region . ",";
	}
	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);

	if($tmp_filter)
	{
		$tmp_filter = "where country_region in (" . $tmp_filter . ") ";
	}
}


if($tmp_filter and count($restricted_countries) > 0)
{
	$tmp_filter .= " and country_id IN(" . implode(",", $restricted_countries) . ") ";
}
elseif(count($restricted_countries) > 0)
{
	$tmp_filter = "where country_id IN(" . implode(",", $restricted_countries) . ") ";
}

$sql_country = "select DISTINCT country_id, country_name " .
			   "from posaddresses " .
			   "left join countries on country_id = posaddress_country " .
			   $tmp_filter . 
			   " order by country_name";


if(count($filter["pr"]) > 0)
{
	$provinces = explode("-", $filter["pr"]);
	$tmp_filter = "";
	foreach($provinces as $key=>$province)
	{
		$tmp_filter = $tmp_filter . $province . ",";
	}
	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);

	if($tmp_filter)
	{
		$tmp_filter = "where province_id in (" . $tmp_filter . ") ";
	}
}

$tmp_filter = "";
$tmp_filter1 = "";
$provice_country_filter = "";
if(count($filter["co"]) > 0)
{
	$countries = explode("-", $filter["co"]);
	
	foreach($countries as $key=>$country)
	{
		$tmp_filter = $tmp_filter . $country . ",";
	}

	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);
	
	$provice_country_filter = $tmp_filter;

	if($tmp_filter)
	{
		$tmp_filter = "where posaddress_country in (" .  $tmp_filter . ") ";
	}
	elseif(count($filter["re"]) > 0 and count($filter["gr"]) > 0)
	{
		$salesregions = explode("-", $filter["re"]);
		
		foreach($salesregions as $key=>$salesregion)
		{
			$tmp_filter = $tmp_filter . $salesregion . ",";
		}

		$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);

		if($tmp_filter)
		{
			$tmp_filter = "where country_salesregion in (" .  $tmp_filter . ") ";
		}

		
		
		$regions = explode("-", $filter["gr"]);
		
		foreach($regions as $key=>$region)
		{
			$tmp_filter1 = $tmp_filter1 . $region . ",";
		}

		$tmp_filter1 = substr($tmp_filter1, 0, strlen($tmp_filter1) - 2);

		if($tmp_filter and $tmp_filter1)
		{
			$tmp_filter .= " and country_region in (" .  $tmp_filter . ") ";
		}
		elseif($tmp_filter1)
		{
			$tmp_filter = "where country_region in (" .  $tmp_filter . ") ";
		}
	}
	elseif(count($filter["re"]) > 0)
	{
		$salesregions = explode("-", $filter["re"]);
		
		foreach($salesregions as $key=>$salesregion)
		{
			$tmp_filter = $tmp_filter . $salesregion . ",";
		}

		$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);

		if($tmp_filter)
		{
			$tmp_filter = "where country_salesregion in (" .  $tmp_filter . ") ";
		}
	}
	elseif(count($filter["gr"]) > 0)
	{
		$regions = explode("-", $filter["gr"]);
		
		foreach($regions as $key=>$region)
		{
			$tmp_filter = $tmp_filter . $region . ",";
		}

		$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);

		if($tmp_filter)
		{
			$tmp_filter = "where country_region in (" .  $tmp_filter . ") ";
		}
	}
}


$tmp_filter = "";

if($provice_country_filter)
{
	$tmp_filter = "where province_country in (" .  $provice_country_filter . ") ";
}
else
{
	$res = mysql_query($sql_country) or dberror($sql_country);
	while($row = mysql_fetch_assoc($res))
	{
		$tmp_filter = $tmp_filter . $row["country_id"] . ",";
	}
	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);
	$tmp_filter = "where province_country in (" .  $tmp_filter . ") ";
}


if($tmp_filter and count($restricted_countries) > 0)
{
	$tmp_filter .= " and province_country IN(" . implode(",", $restricted_countries) . ") ";
}
elseif(count($restricted_countries) > 0)
{
	$tmp_filter = "where province_country IN(" . implode(",", $restricted_countries) . ") ";
}

$sql_provinces = "select province_id, province_canton, country_name " . 
                 "from provinces " .
				 "left join countries on country_id = province_country " . 
				 $tmp_filter . 
				 "order by country_name,province_canton";


if($tmp_filter and count($restricted_countries) > 0)
{
	$tmp_filter .= " and province_country IN(" . implode(",", $restricted_countries) . ") ";
}
elseif(count($restricted_countries) > 0)
{
	$tmp_filter = "where posaddress_country IN(" . implode(",", $restricted_countries) . ") ";
}

$sql_cities = "select DISTINCT posaddress_place " .
			  "from posaddresses " .
			  "left join countries on country_id = posaddress_country " .
			  "left join provinces on province_country = country_id " . 
			  $tmp_filter . 
			  " order by posaddress_place";


$sql_areas = "select posareatype_id, posareatype_name ".
              "from posareatypes order by posareatype_name";



$sql_project_cost_types = "select project_costtype_id, project_costtype_text ".
						  "from project_costtypes " .
                          "order by project_costtype_text";

$sql_product_lines = "select product_line_id, product_line_name ".
                     "from product_lines " . 
                     "where product_line_posindex = 1 " . 
                     "   order by product_line_name";


$tmp_filter = "";
if(count($filter["pl"]) > 0)
{
	$product_lines = explode("-", $filter["pl"]);
	$tmp_filter = "";
	foreach($product_lines as $key=>$product_line)
	{
		$tmp_filter = $tmp_filter . $product_line . ",";
	}
	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);

	if($tmp_filter)
	{
		$tmp_filter = "where productline_subclass_productline in (" . $tmp_filter . ") ";
	}
}
$sql_furniture_subclasses = "select productline_subclass_id, productline_subclass_name ".
                     "from productline_subclasses " . 
					 $tmp_filter .
                     "   order by productline_subclass_name";


$sql_pos_types = "select DISTINCT postype_name ".
                     "from postypes " .
                     "order by postype_name";

$sql_possubclasses = "select possubclass_id, possubclass_name ".
                     "from possubclasses " .
                     "order by possubclass_name";

$sql_project_kinds = "select projectkind_id, projectkind_name ".
						 "from projectkinds " .
						 "where projectkind_id > 0 " .
						 "order by projectkind_name";


$distribution_channles = array();

$sql_distribution_channels = "select mps_distchannel_id, " . 
                             "concat(mps_distchannel_group, ' - ', mps_distchannel_name, ' - ', mps_distchannel_code) as distributionchannel ".
							"from mps_distchannels " .
							"order by mps_distchannel_group, mps_distchannel_name, mps_distchannel_code";

$design_objectives = array();
$sql_design_objectives = "select design_objective_item_id, design_objective_item_name, " .
						 "    design_objective_group_name, postype_name " .
						 "from design_objective_items left join design_objective_groups on design_objective_item_group = design_objective_group_id " .
						 "    left join postypes on design_objective_group_postype = postype_id " . 
						 " where postype_name <> '' " . 
						 " order by postype_name, design_objective_group_name, design_objective_item_name ";

/********************************************************************
    save data
*********************************************************************/
if(param("save_form"))
{
	$new_filter = array();
	
	$new_filter["re"] =  $filter["re"];
	$new_filter["gr"] =  $filter["gr"];
	$new_filter["co"] =  $filter["co"];
	$new_filter["pr"] =  $filter["pr"];
	$new_filter["ci"] =  $filter["ci"];
	$new_filter["ar"] =  $filter["ar"];

	$new_filter["pct"] =  $filter["pct"];
	$new_filter["pl"] =  $filter["pl"];
	$new_filter["fscs"] =  $filter["fscs"];
	$new_filter["pt"] =  $filter["pt"];
	$new_filter["sc"] =  $filter["sc"];
	$new_filter["pk"] =  $filter["pk"];
	$new_filter["cl"] =  $filter["cl"];
	$new_filter["fr"] =  $filter["fr"];
	$new_filter["lopr"] =  $filter["lopr"];
	$new_filter["nopr"] =  $filter["nopr"];
	$new_filter["exfp"] =  $filter["exfp"];
	$new_filter["incs"] =  $filter["incs"];
	$new_filter["ct"] =  $filter["ct"];
	$new_filter["dcs"] =  $filter["dcs"];
	$new_filter["dos"] =  $filter["dos"];
	$new_filter["fcy"] =  $filter["fcy"];
	$new_filter["fcm"] =  $filter["fcm"];
	$new_filter["tcy"] =  $filter["tcy"];
	$new_filter["tcm"] =  $filter["tcm"];

	$new_filter["www"] =  $filter["www"];
	
	$new_filter["latest_pos_lease"] =  $filter["latest_pos_lease"];
	$new_filter["pos_without_lease"] =  $filter["pos_without_lease"];
	

	$new_filter["aofy"] =  $filter["aofy"];
	$new_filter["aofm"] =  $filter["aofm"];
	$new_filter["aoty"] =  $filter["aoty"];
	$new_filter["aotm"] =  $filter["aotm"];


	$new_filter["clfy"] =  $filter["clfy"];
	$new_filter["clfm"] =  $filter["clfm"];
	$new_filter["clty"] =  $filter["clty"];
	$new_filter["cltm"] =  $filter["cltm"];

	$new_filter["opy"] =  $filter["opy"];
	$new_filter["opm"] =  $filter["opm"];

	if(param("save_form") == "re") // Geographical Regions
	{
		$RE = "";

		$res = mysql_query($sql_salesregion) or dberror($sql_salesregion);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["RE_" . $row["salesregion_id"]])
			{
				$RE .=$row["salesregion_id"] . "-";
			}
		}
		
		$new_filter["re"] = $RE;
	}
	elseif(param("save_form") == "gr") //Supplied Regions
	{
		$GR = "";

		$res = mysql_query($sql_region) or dberror($sql_region);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["GR_" . $row["region_id"]])
			{
				$GR .=$row["region_id"] . "-";
			}
		}
		
		$new_filter["gr"] = $GR;
	}
	elseif(param("save_form") == "pr") //Provinces
	{
		$PR = "";

		$res = mysql_query($sql_provinces) or dberror($sql_provinces);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PR_" . $row["province_id"]])
			{
				$PR .=$row["province_id"] . "-";
			}
		}
		
		$new_filter["pr"] = $PR;
	}
	elseif(param("save_form") == "co") //Countries
	{
		$CO = "";

		$res = mysql_query($sql_country) or dberror($sql_country);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["CO_" . $row["country_id"]])
			{
				$CO .=$row["country_id"] . "-";
			}
		}
		
		$new_filter["co"] = $CO;
	}
	elseif(param("save_form") == "ci") //Cities
	{
		$CI = "";

		$res = mysql_query($sql_cities) or dberror($sql_cities);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["CI_" . md5($row["posaddress_place"])])
			{
				$CI .= trim($row["posaddress_place"]) . "-";
			}
		}
	
		$new_filter["ci"] = $CI;

	}
	elseif(param("save_form") == "ar") //Areas
	{
		$AR = "";

		$res = mysql_query($sql_areas) or dberror($sql_areas);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["AR_" . $row["posareatype_id"]])
			{
				$AR .=$row["posareatype_id"] . "-";
			}
		}
		
		$new_filter["ar"] = $AR;
	}
	elseif(param("save_form") == "pct") //project cost types
	{
		$PCT = "";

		$res = mysql_query($sql_project_cost_types) or dberror($sql_project_cost_types);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PCT_" . $row["project_costtype_id"]])
			{
				$PCT .=$row["project_costtype_id"] . "-";
			}
		}
		$new_filter["pct"] = $PCT;
	}
	elseif(param("save_form") == "pl") //Product Lines
	{
		$PL = "";

		$res = mysql_query($sql_product_lines) or dberror($sql_product_lines);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PL_" . $row["product_line_id"]])
			{
				$PL .=$row["product_line_id"] . "-";
			}
		}
		
		$new_filter["pl"] = $PL;
	}
	elseif(param("save_form") == "fscs") //Furniture Subclasses
	{
		$FSCS = "";

		$res = mysql_query($sql_furniture_subclasses) or dberror($sql_furniture_subclasses);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["FSCS_" . $row["productline_subclass_id"]])
			{
				$FSCS .=$row["productline_subclass_id"] . "-";
			}
		}
		
		$new_filter["fscs"] = $FSCS;
	}
	elseif(param("save_form") == "pt") //POS Types
	{
		$PT = "";

		$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PT_" . str_replace(" ", "", $row["postype_name"])])
			{
				$PT .=$row["postype_name"] . "-";
			}
		}
		
		$new_filter["pt"] = $PT;
	}
	elseif(param("save_form") == "sc") //POS Type Subclasses
	{
		$SC = "";

		$res = mysql_query($sql_possubclasses) or dberror($sql_possubclasses);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["SC_" . $row["possubclass_id"]])
			{
				$SC .=$row["possubclass_id"] . "-";
			}
		}
		
		$new_filter["sc"] = $SC;
	}
	elseif(param("save_form") == "pk") //Product Kinds
	{
		$PK = "";

		$res = mysql_query($sql_project_kinds) or dberror($sql_project_kinds);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PK_" . $row["projectkind_id"]])
			{
				$PK .=$row["projectkind_id"] . "-";
			}
		}
		
		$new_filter["pk"] = $PK;
	}
	elseif(param("save_form") == "dcs") //Distribution Channels
	{
		$DCS = "";

		$res = mysql_query($sql_distribution_channels) or dberror($sql_distribution_channels);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["DCS_" . $row["mps_distchannel_id"]])
			{
				$DCS .=$row["mps_distchannel_id"] . "-";
			}
		}
		
		$new_filter["dcs"] = $DCS;
	}
	elseif(param("save_form") == "dos") //Design Objectives
	{
		$DOS = "";

		$res = mysql_query($sql_design_objectives) or dberror($sql_design_objectives);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["DOS_" . $row["design_objective_item_id"]])
			{
				$DOS .=$row["design_objective_item_id"] . "-";
			}
		}
		
		$new_filter["dos"] = $DOS;
	}



	$sql = "update posqueries " . 
		   "set posquery_filter = " . dbquote(serialize($new_filter)) . 
		   " where posquery_id = " . param("query_id");

	$result = mysql_query($sql) or dberror($sql);
	
}

/********************************************************************
    get existant filter
*********************************************************************/
$filter = array();
//get benchmark parameters
$salesregions = array();

$sql = "select posquery_filter from posqueries " .
	   "where posquery_id = " . $query_id;

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{

	$filter = unserialize($row["posquery_filter"]);
	$salesregions = explode("-", $filter["re"]);
	$regions = explode("-", $filter["gr"]);
	$countries = explode("-", $filter["co"]);
	$cities = explode("-", $filter["ci"]);
	$areas = explode("-", $filter["ar"]);
	$project_cost_types = explode("-", $filter["pct"]);
	$product_lines = explode("-", $filter["pl"]);
	$furniture_subclasses = explode("-", $filter["fscs"]);
	$pos_types = explode("-", $filter["pt"]);
	$subclasses = explode("-", $filter["sc"]);
	$project_kinds = explode("-", $filter["pk"]);
	$distribution_channels = explode("-", $filter["dcs"]);
	$design_objectives = explode("-", $filter["dos"]);
}


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new form("cer_benchmarks", "benchmark_selector");
$form->add_hidden("query_id", $query_id);

if(param("s") == "re") // Geographical regions
{
	$page_title = "Geographical Region Selector";
	$form->add_hidden("save_form", "re");

	$res = mysql_query($sql_salesregion) or dberror($sql_salesregion);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["salesregion_id"], $salesregions))
		{
			$form->add_checkbox("RE_" . $row["salesregion_id"], $row["salesregion_name"], true);
		}
		else
		{
			$form->add_checkbox("RE_" . $row["salesregion_id"], $row["salesregion_name"], false);
		}
		
	}
}
elseif(param("s") == "gr") // Supplied regions
{
	$page_title = "Supplied Region Selector";
	$form->add_hidden("save_form", "gr");

	$res = mysql_query($sql_region) or dberror($sql_region);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["region_id"], $regions))
		{
			$form->add_checkbox("GR_" . $row["region_id"], $row["region_name"], true);
		}
		else
		{
			$form->add_checkbox("GR_" . $row["region_id"], $row["region_name"], false);
		}
		
	}
}
elseif(param("s") == "pr") // provinces
{
	$page_title = "Province Selector";
	$form->add_hidden("save_form", "pr");
	$country_name = "";
	$i=0;

	$res = mysql_query($sql_provinces) or dberror($sql_provinces);
	while($row = mysql_fetch_assoc($res))
	{
		if($country_name != $row["country_name"])
		{
			$country_name = $row["country_name"];
			$i++;
			$form->add_label("country_name_" .$i, "", RENDER_HTML, "<strong>" .  $country_name . "</strong>");
		}
		if(in_array($row["province_id"], $provinces))
		{
			$form->add_checkbox("PR_" . $row["province_id"], $row["province_canton"], true);
		}
		else
		{
			$form->add_checkbox("PR_" . $row["province_id"], $row["province_canton"], false);
		}
		
	}
}
elseif(param("s") == "co") // countries
{
	$page_title = "Country Selector";
	$form->add_hidden("save_form", "co");

	$res = mysql_query($sql_country) or dberror($sql_country);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["country_id"], $countries))
		{
			$form->add_checkbox("CO_" . $row["country_id"], $row["country_name"], true);
		}
		else
		{
			$form->add_checkbox("CO_" . $row["country_id"], $row["country_name"], false);
		}
		
	}
}
elseif(param("s") == "ci") // cities
{
	$page_title = "City Selector";
	$form->add_hidden("save_form", "ci");

	$res = mysql_query($sql_cities) or dberror($sql_cities);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array(trim($row["posaddress_place"]), $cities))
		{
			$form->add_checkbox("CI_" . md5($row["posaddress_place"]), $row["posaddress_place"], true);
		}
		else
		{
			$form->add_checkbox("CI_" . md5($row["posaddress_place"]),  $row["posaddress_place"], false);
		}
		
	}
}
elseif(param("s") == "ar") // Neighbourhood Areas
{
	$page_title = "Neighbourhood Area Selector";
	$form->add_hidden("save_form", "ar");

	$res = mysql_query($sql_areas) or dberror($sql_areas);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["posareatype_id"], $areas))
		{
			$form->add_checkbox("AR_" . $row["posareatype_id"], $row["posareatype_name"], true);
		}
		else
		{
			$form->add_checkbox("AR_" . $row["posareatype_id"], $row["posareatype_name"], false);
		}
		
	}
}
elseif(param("s") == "pct") // Project Cost Types
{
	$page_title = "Legal Type Selector";
	$form->add_hidden("save_form", "pct");

	$res = mysql_query($sql_project_cost_types) or dberror($sql_project_cost_types);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["project_costtype_id"], $project_cost_types))
		{
			$form->add_checkbox("PCT_" . $row["project_costtype_id"], $row["project_costtype_text"], true);
		}
		else
		{
			$form->add_checkbox("PCT_" . $row["project_costtype_id"], $row["project_costtype_text"], false);
		}
		
	}
}
elseif(param("s") == "pl") // Product Lines
{
	$page_title = "Furniture Type Selector";
	$form->add_hidden("save_form", "pl");

	$res = mysql_query($sql_product_lines) or dberror($sql_product_lines);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["product_line_id"], $product_lines))
		{
			$form->add_checkbox("PL_" . $row["product_line_id"], $row["product_line_name"], true);
		}
		else
		{
			$form->add_checkbox("PL_" . $row["product_line_id"], $row["product_line_name"], false);
		}
		
	}
}
elseif(param("s") == "fscs") // Furniture Subclasses
{
	$page_title = "Furniture Subclass Selector";
	$form->add_hidden("save_form", "fscs");

	$res = mysql_query($sql_furniture_subclasses) or dberror($sql_furniture_subclasses);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["productline_subclass_id"], $furniture_subclasses))
		{
			$form->add_checkbox("FSCS_" . $row["productline_subclass_id"], $row["productline_subclass_name"], true);
		}
		else
		{
			$form->add_checkbox("FSCS_" . $row["productline_subclass_id"], $row["productline_subclass_name"], false);
		}
		
	}
}
elseif(param("s") == "pt") // POS Types
{
	$page_title = "POS Type Selector";
	$form->add_hidden("save_form", "pt");

	$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["postype_name"], $pos_types))
		{
			$form->add_checkbox("PT_" . str_replace(" ", "", $row["postype_name"]), $row["postype_name"], true);
		}
		else
		{
			$form->add_checkbox("PT_" . str_replace(" ", "", $row["postype_name"]), $row["postype_name"], false);
		}
	}
}
elseif(param("s") == "sc") // POS Type Subclass
{
	$page_title = "POS Type Subclass Selector";
	$form->add_hidden("save_form", "sc");

	$res = mysql_query($sql_possubclasses) or dberror($sql_possubclasses);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["possubclass_id"], $subclasses))
		{
			$form->add_checkbox("SC_" . $row["possubclass_id"], $row["possubclass_name"], true);
		}
		else
		{
			$form->add_checkbox("SC_" . $row["possubclass_id"], $row["possubclass_name"], false);
		}
		
	}
}
elseif(param("s") == "pk") // Project Types
{
	$page_title = "Project Type Selector";
	$form->add_hidden("save_form", "pk");

	$res = mysql_query($sql_project_kinds) or dberror($sql_project_kinds);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["projectkind_id"], $project_kinds))
		{
			$form->add_checkbox("PK_" . $row["projectkind_id"], $row["projectkind_name"], true);
		}
		else
		{
			$form->add_checkbox("PK_" . $row["projectkind_id"], $row["projectkind_name"], false);
		}
		
	}
}
elseif(param("s") == "dcs") // Distribution Channels
{
	$page_title = "Distribution Channel Selector";
	$form->add_hidden("save_form", "dcs");

	$res = mysql_query($sql_distribution_channels) or dberror($sql_distribution_channels);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["mps_distchannel_id"], $distribution_channels))
		{
			$form->add_checkbox("DCS_" . $row["mps_distchannel_id"], $row["distributionchannel"], true);
		}
		else
		{
			$form->add_checkbox("DCS_" . $row["mps_distchannel_id"], $row["distributionchannel"], false);
		}
		
	}
}
elseif(param("s") == "dos") // Design Objectives
{
	$page_title = "Design Objectives Selector";
	$form->add_hidden("save_form", "dos");

	$res = mysql_query($sql_design_objectives) or dberror($sql_design_objectives);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["design_objective_item_id"], $design_objectives))
		{
			$form->add_checkbox("DOS_" . $row["design_objective_item_id"], $row["postype_name"] . " - " . $row["design_objective_group_name"] . " - " . $row["design_objective_item_name"], true);
		}
		else
		{
			$form->add_checkbox("DOS_" . $row["design_objective_item_id"], $row["postype_name"] . " - " . $row["design_objective_group_name"] . " - " . $row["design_objective_item_name"], false);
		}
		
	}
}

$form->add_input_submit("submit", "Save Values", 0);

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Process buttons
*********************************************************************/ 

/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page_Modal("cer_benchmarks");

$page->header();
$page->title($page_title);

$form->render();

if(param("save_form"))
{
?>

<script languege="javascript">
var back_link = "query_generator_filter1.php?query_id=<?php echo $query_id;?>"; 
$.nyroModalRemove();
</script>

<?php
}
$page->footer();
