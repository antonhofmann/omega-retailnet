<?php
/********************************************************************

    correct_pos_address_check_preselect.php

    Preselection of POS List for data corrections

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_accept_pos_data_corrections");
set_referer("posindex.php");

//compose list
if(has_access("can_view_posindex") or has_access("can_edit_posindex"))
{
	$sql = "select DISTINCT country_id, country_name " .
		   "from _addresses " . 
		   "left join countries on address_country = country_id " . 
		   "where country_name is not null and address_checked = 1 " . 
		   "order by country_name";
}

$form = new Form("_addresses", "address");

$form->add_section("Country Selection");

$form->add_list("country", "Country",$sql);

$form->add_button("show_pos", "Show List");

$form->populate();
$form->process();


if($form->button("show_pos"))
{
	redirect("correct_address_data_check.php?country=" . $form->value("country"));
}

$page = new Page("posaddresses", "Companies: Check Data Corrections");
require "include/pos_page_actions.php";
$page->header();

$page->title("Companies: Check Data Corrections");
$form->render();

?>

	<script type="text/javascript">
		
		document.onkeydown = process_key;
		
		function process_key(e)
		{
		  if( !e ) 
		  {
			if( window.event ) 
			{
			  e = window.event;
			} 
			else 
			{
			  return;
			}
		  }

		  if(e.keyCode==13)
		  {
			  button('show_pos');
		  }
		}
	</script>

	<?php


$page->footer();

?>
