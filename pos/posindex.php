<?php
/********************************************************************

    posindex.php

    Lists of addresses (POS)

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

check_access("can_use_posindex");
set_referer("posindex_pos.php");

if(param("order")) {
	$_SESSION['poslist_sortorder'] = param('order');
}

$user = get_user(user_id());

$postype_filter = array();
$postype_filter["all"] = "All";
$sql = "select * from postypes order by postype_name";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$postype_filter[$row["postype_id"]] = $row["postype_name"];
}


$legaltype_filter = array();
$legaltype_filter["all"] = "All";
$sql = "select * from project_costtypes where project_costtype_id in (1, 2, 6) order by project_costtype_text";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$legaltype_filter[$row["project_costtype_id"]] = $row["project_costtype_text"];
}


$preselect_filter = "";
if(param("country"))
{
	$preselect_filter = "posaddress_country = " . param("country");
	register_param("country", param("country"));
}
else
{
	redirect("posindex_preselect.php");
}

if(param("province") and $preselect_filter)
{
	$preselect_filter .= " and place_province = " . param("province");
	register_param("province", param("province"));
}
elseif(param("province"))
{
	$preselect_filter = "place_province = " . param("province");
	register_param("province", param("province"));
}


if(param("ostate") and $preselect_filter)
{
	if(param("ostate") == 1) // only operating POS locations
	{
		$preselect_filter .= " and (posaddress_store_closingdate = '0000-00-00' or posaddress_store_closingdate is null) ";
	}
	elseif(param("ostate") == 2) // only closed POS locations
	{
		$preselect_filter .= " and posaddress_store_closingdate <> '0000-00-00' and posaddress_store_closingdate is not null";
	}

	register_param("ostate", param("ostate"));
}
elseif(param("ostate"))
{
	if(param("ostate") == 1) // only operating POS locations
	{
		$preselect_filter = " (posaddress_store_closingdate = '0000-00-00' or posaddress_store_closingdate is null) ";
	}
	elseif(param("ostate") == 2) // only closed POS locations
	{
		$preselect_filter = " posaddress_store_closingdate <> '0000-00-00' and posaddress_store_closingdate is not null";
	}

	register_param("ostate", param("ostate"));
}


if(has_access("can_edit_posindex") or has_access("can_view_posindex"))
{

}
elseif(has_access("can_view_his_posindex") or has_access("can_edit_his_posindex"))
{
	if(!param("country"))
	{
		redirect("posindex_preselect.php");
	}

	if($preselect_filter)
	{
		$preselect_filter .= " and posaddress_client_id = " . $user["address"];
	}
	else
	{
		$preselect_filter = "  posaddress_client_id = " . $user["address"];
	}

	$country_filter = "";
	$tmp = array();
	$sql = "select * from country_access " .
		   "where country_access_user = " . user_id();


	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{            
		$tmp[] = $row["country_access_country"];
	}

	if(count($tmp) > 0) {
		$country_filter = " posaddress_country IN (" . implode(",", $tmp) . ") ";
	}

	if($country_filter) 
	{
		if($preselect_filter)
		{
			if(in_array(param("country"), $tmp))
			{
				$preselect_filter .= " or (posaddress_country = " . param("country") . ") ";
			}
		}
		else
		{
			$preselect_filter = " or (posaddress_country = " . param("country") . ") ";
		}
	}
	else
	{
		if($preselect_filter)
		{
			//$preselect_filter .= " and (posaddress_country = " . $user["country"];
		}
		else
		{
			$preselect_filter .= " and (posaddress_country = " . $user["country"];
		}
	}
}


if(has_access("has_access_to_retail_pos_only")) {
	$preselect_filter = "(" . $preselect_filter . ") and posaddress_ownertype in (1) ";
}

if(has_access("has_access_to_wholesale_pos")) {
	$preselect_filter = "(" . $preselect_filter . ") and posaddress_ownertype in (2,6) ";
}

if(!has_access("can_view_his_posindex") 
       and !has_access("can_edit_his_posindex") 
	   and !has_access("can_view_posindex") 
	   and !has_access("can_edit_posindex"))
{
	redirect("posindex_preselect.php");
}


//compose list

$sql = "select posaddress_id, if(posaddress_name <> '', posaddress_name, 'n.a.') as posname, " .
       "posaddress_address, posaddress_address2, posaddress_zip, " .
       "    posaddress_place, country_name, project_costtype_text, postype_name, " .
	   "posaddress_google_precision, posaddress_store_closingdate, province_canton, posaddress_country " . 
       "from posaddresses " .
	   "left join places on place_id = posaddress_place_id " .
	   "left join provinces on province_id = place_province " .
	   "left join countries on posaddress_country = country_id " . 
	   "left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
	   "left join postypes on postype_id = posaddress_store_postype ";

if(has_access("can_edit_posindex") or has_access("can_view_posindex"))
{
}
elseif(has_access("can_view_his_posindex") or has_access("can_edit_his_posindex"))
{
	
	//$preselect_filter = "(postype_id <> 2 or (postype_id = 2 and project_costtype_id = 1)) and " . $preselect_filter;
}

//get image columns
$pictures = array();
$googlemap = array();
$closed = array();
$pdfs = array();
$onweb = array();
$closed2 = array();

$sql_u = "select posaddress_id, posaddress_country, posaddress_google_precision, posaddress_store_closingdate,  " .
		 "posfile_id, posfile_filegroup, posaddress_export_to_web, place_name, posaddress_address, posaddress_address2, posaddress_zip, posaddress_google_lat, posaddress_google_long, " . 
		 "posclosingassessment_id, posclosingassessment_filesigned " .
         "from posaddresses " . 
		 "left join places on place_id = posaddress_place_id " .
		 "left join provinces on province_id = place_province " .
		 "left join posfiles on posfile_posaddress = posaddress_id " . 
		 "left join posclosingassessments on posclosingassessment_posaddress_id = posaddress_id ";

//$sql_u = $sql_u . " where " . $preselect_filter . " and (posfile_filegroup = 1 or posfile_filegroup is null) ";
if($preselect_filter)
{
	$sql_u = $sql_u . " where " . $preselect_filter;
}

$res = mysql_query($sql_u) or dberror($sql_u);
while($row = mysql_fetch_assoc($res))
{
	//$result = update_posdata_from_posorders($row["posaddress_id"]);

	if($row["posaddress_export_to_web"] == 1 and ($row["posaddress_store_closingdate"] == NULL or $row["posaddress_store_closingdate"] == "0000-00-00"))
	{
		$onweb[$row["posaddress_id"]] = "<img src=\"/pictures/bullet_ball_glass_green.gif\" border='0'/>";
	}

	if($row["posaddress_google_precision"] == 1)
	{
		$link = "<a href=\"javascript:popup('http://maps.google.com/?daddr=" . $row["posaddress_google_lat"] . "," . $row["posaddress_google_long"] . "',800,600);\"><img src=\"/pictures/show_map.gif\" border='0'/></a>";
		
		$googlemap[$row["posaddress_id"]] = $link;
	}
	elseif($row["posaddress_google_precision"] == 2)
	{
		
		if(has_access("can_edit_posindex"))
		{
			$link = "<a href=\"javascript:popup('posindex_map3.php?id=" .  $row["posaddress_id"] . "',800,800);\"><img src=\"/pictures/show_map2.gif\" border='0'/></a>";

			$address = str_replace("'", "\'", $row["posaddress_address"]) . ', ' . str_replace("'", "\'", $row["posaddress_address2"]);

			$place = $row["posaddress_zip"] . " " . $row["place_name"];

			//$link = "<a href=\"javascript:popup('project_new_map.php?id=" . $row["posaddress_id"] . "&c=" . $row["posaddress_country"] . "&p=" . $place . "&a=" . $address . "', 700,650);\"><img src=\"/pictures/show_map2.gif\" border='0'/></a>";

			$googlemap[$row["posaddress_id"]] = $link;

		}
		elseif(has_access("can_view_his_posindex") or has_access("can_edit_his_posindex"))
		{
			$link = "<a href=\"javascript:popup('http://maps.google.com/?daddr=" . $row["posaddress_google_lat"] . "," . $row["posaddress_google_long"] . "',800,600);\"><img src=\"/pictures/show_map.gif\" border='0'/></a>";
			$googlemap[$row["posaddress_id"]] = $link;
	
		}

		
	}

	if($row["posaddress_store_closingdate"] != NULL and $row["posaddress_store_closingdate"] != "0000-00-00")
	{
		
		$closed[$row["posaddress_id"]] = "<img src=\"/pictures/closed.gif\" />";
	}

	if($row["posclosingassessment_id"] > 0)
	{
		
		if($row["posclosingassessment_filesigned"])
		{
			$link = "<a href=\"javascript:popup('" .  $row["posclosingassessment_filesigned"] . "',800,600);\"><img src=\"/pictures/ico_pdf.gif\" border='0'/></a>";
			$pdfs[$row["posclosingassessment_id"]] = $link;


			$link = "<a href=\"javascript:popup('pos_closing_assessment_pdf.php?id=" .  $row["posclosingassessment_id"] . "&b=2',800,600);\"><img src=\"/pictures/edit01.gif\" border='0'/></a>";

			$closed2[$row["posaddress_id"]] = $link;
		}
		else
		{
			$link = "<a href=\"javascript:popup('pos_closing_assessment_pdf.php?id=" .  $row["posclosingassessment_id"] . "',800,600);\"><img src=\"/pictures/ico_pdf.gif\" border='0'/></a>";
			$pdfs[$row["posclosingassessment_id"]] = $link;


			$link = "<a href=\"javascript:popup('pos_closing_assessment_pdf.php?id=" .  $row["posclosingassessment_id"] . "&b=1',800,600);\"><img src=\"/pictures/edit01.gif\" border='0'/></a>";

			$closed2[$row["posaddress_id"]] = $link;
		}
		
	}


	$link = "<a href=\"javascript:popup('possheet_pdf.php?id=" .  $row["posaddress_id"] . "',800,600);\"><img src=\"/pictures/ico_pdf.gif\" border='0'/></a>";
	$pdfs[$row["posaddress_id"]] = $link;
	
	//check if there are pictures for the POS Location
	$num_of_pix = 0;
	if($row["posfile_id"] and $row["posfile_filegroup"] == 1)
	{
		$num_of_pix = 1;
	}

	/*
	if($num_of_pix == 0)
	{
		$sql_p = "select count(order_file_id) as num_recs " . 
				 "from order_files " .
			     "inner join posorders on posorder_order = order_file_order " . 
				 "where posorder_posaddress = " . $row["posaddress_id"] . 
				 "  and order_file_category = 11";
		
		$res_p = mysql_query($sql_p) or dberror($sql_p);
		while ($row_p = mysql_fetch_assoc($res_p))
		{
			if($row_p["num_recs"] > 0)
			{
				$num_of_pix = $row_p["num_recs"];
			}
		}

	}
	*/


	if($num_of_pix == 0)
	{
		$sql_p = "select order_file_id " . 
				 "from order_files " .
			     "inner join posorders on posorder_order = order_file_order " . 
				 "where posorder_posaddress = " . $row["posaddress_id"] . 
				 "  and order_file_category = 11";
		
		$res_p = mysql_query($sql_p) or dberror($sql_p);
		$num_of_pix = mysql_num_rows($res_p); 
	}
	

	if($num_of_pix > 0)
	{
		$link = "<a id=\"p_" . $row["posaddress_id"] . "\" href=\"#\"><img border=\"0\" alt=\"\" src=\"../pictures/camera.gif\" /></a>";
		$pictures[$row["posaddress_id"]] = $link;
	}
}



if(param('ltf')) 
{
	if($preselect_filter) {
		
		if(param('ltf') == "all") {
		}
		else
		{
			$preselect_filter .= " and posaddress_store_postype = " . param('ltf');
		}
	}
	else
	{
		if(param('ltf') == "all") {
		}
		else
		{
			$preselect_filter = "posaddress_store_postype = " . param('ltf');
		}
	}
}
else
{
	/*
	if($preselect_filter) {
		$preselect_filter .= " and posaddress_store_postype <> 4";
	}
	else
	{
		$preselect_filter = "posaddress_store_postype <> 4";
	}
	*/
}


if(param('let')) 
{
	if($preselect_filter) {
		
		if(param('let') == "all") {
		}
		else
		{
			$preselect_filter .= " and posaddress_ownertype = " . param('let');
		}
	}
	else
	{
		if(param('let') == "all") {
		}
		else
		{
			$preselect_filter = "posaddress_ownertype = " . param('let');
		}
	}
}



/********************************************************************
    Create Form
*********************************************************************/

$list = new ListView($sql);

$list->set_entity("posaddresses");
$list->set_filter($preselect_filter);
if(array_key_exists('poslist_sortorder', $_SESSION) and $_SESSION['poslist_sortorder']) {
	$list->set_order($_SESSION['poslist_sortorder']);
}
else {
	$list->set_order("country_name, posaddress_place, posaddress_name");
}


$list->add_listfilters("let", "Legal Type", 'select', $legaltype_filter, param("let"));
$list->add_listfilters("ltf", "POS Type", 'select', $postype_filter, param("ltf"));


$list->add_hidden("country", param("country"));
$list->add_hidden("province", param("province"));
$list->add_hidden("ostate", param("ostate"));

$list->add_column("country_name", "Country", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);
$list->add_column("province_canton", "Province", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("posaddress_place", "City", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("posname", "POS Name", "posindex_pos.php?country=" . param("country") . '&let=' . param("let") . '&ltf=' . param("ltf") . '&ostate=' . param("ostate"), LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("posaddress_address", "Address", "", LIST_FILTER_FREE);
$list->add_column("postype_name", "POS Type", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);
$list->add_column("project_costtype_text", "Legal Type", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);


$list->add_text_column("web", "Web", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML, $onweb);
$list->add_text_column("pix", "Pix", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML, $pictures);

$list->add_text_column("pdfs", "Sheet", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML, $pdfs);
$list->add_text_column("map", "Map", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML, $googlemap);
$list->add_text_column("closed", "Closed", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML , $closed);
$list->add_text_column("closed2", "", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML , $closed2);

if(has_access("can_edit_posindex"))
{
	$list->add_button(LIST_BUTTON_NEW, "New", "posindex_pos.php?country=" . param("country"). '&ltf=' . param('ltf') . '&let=' . param('let'));
}
$list->add_button("print1", "Print POS List");
$list->add_button("print2", "Print POS List with Details");

$list->populate();
$list->process();

if($list->button("print1"))
{

	
	$link = "posindex_print_xls.php" .
			"?co=" . param("country") . "&list=1&pr=" . param("province") . '&let=' . param('let') . '&ltf=' . param('ltf'). '&ostate=' . param('ostate');
	
	redirect($link);
}
elseif($list->button("print2"))
{

	$link = "posindex_print_xls.php" .
			"?co=" . param("country") . "&list=2&pr=" . param("province") . '&let=' . param('let') . '&ltf=' . param('ltf'). '&ostate=' . param('ostate');

	redirect($link);
}


$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title("POS Index");
$list->render();


echo '<script type="text/javascript">';
echo 'jQuery(document).ready(function($) {';

foreach($pictures as $posaddress_id=>$value)
{
  echo '$("#p_' . $posaddress_id . '").click(function(e) {';
  echo 'e.preventDefault();';
  echo '$.nyroModalManual({';
  echo 'width: "860",';
  echo 'height: "700",';
  echo 'url: "/pos/pos_pictures.php?posaddress_id=' . $posaddress_id . '"';
  echo '});';
  echo 'return false;';
  echo '});';
}


echo '});';
echo '</script>';


$page->footer();

?>
