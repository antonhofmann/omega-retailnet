<?php
/********************************************************************

    project_new_map.php

    Show google Map of the POS.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

ini_set('allow_url_fopen', 'on');

$api_key = GOOGLE_API_KEY;
$api_key_geo = GOOGLE_API_KEY_GEO;
//check_access("can_use_posindex");
$latitude = "";
if(param("id"))
{
	if(param("table"))
	{
			$pos = get_poslocation(param("id"), param("table"));
			if(count($pos) == 0)
			{
				$pos = get_poslocation(param("id"),"posaddresses");
			}
	}
	else
	{
			$pos = get_poslocation(param("id"), "posaddresses");
	}
	

	if(isset($pos["posaddress_google_lat"]) and $pos["posaddress_google_lat"])
	{
		$latitude = $pos["posaddress_google_lat"];
		$zoom = 18;
		
	}
	if(isset($pos["posaddress_google_long"]) and $pos["posaddress_google_long"])
	{
		$longitude = $pos["posaddress_google_long"];
		$zoom = 18;
	}

	if(count($pos) > 0)
	{
		$address = $pos["posaddress_address"];
		$short_address = $pos["country_name"] . ", " .  $pos["place_name"];
		$country = $pos["country_name"];
	}
	else
	{
		$address = "";
		$short_address = "";
		$country = "";
	}
	

}


if(!$latitude or !param("id") or $latitude == 0 or $longitude == 0)
{
	$latitude = 0;
	$longitude = 0;
	$zoom = 18;

	$country_name = "";
	$place_name = "";
	$address = "";
	$sql = "select country_name from countries where country_id = '" . $_GET["c"] . "'";
	$res = mysql_query($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$country_name = $row["country_name"];
		$zoom = 6;
	}

	if($_GET["p"])
	{
		$place_name = $_GET["p"];
		$zoom = 10;
	}
	if($_GET["a"])
	{
		$address = $_GET["a"];
		$zoom = 16;
	}

	if($address)
	{
		$address = trim($country_name) . ", " . trim($place_name). ", " . trim($address);
		$short_address = trim($country_name) . ", " . trim($place_name);
		$country = trim($country_name);
	}
	elseif($place_name)
	{
		$address = trim($country_name) . ", " . trim($place_name);
		$short_address = trim($country_name) . ", " . trim($place_name);
		$country = trim($country_name);
	}
	else
	{
		$address = trim($country_name);
		$short_address = trim($country_name);
		$country = trim($country_name);
	}

	$geodata = urlencode($address);
	$url = "https://maps.google.com/maps/api/geocode/json?address=$geodata&sensor=false&key=" .  $api_key_geo;
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	if(PROXY_SERVER)
	{
		curl_setopt($ch, CURLOPT_PROXY, PROXY_SERVER);
	}
	$response = curl_exec($ch);
	curl_close($ch);
	$response_a = json_decode($response);

	if(isset($response_a->results[0]->geometry->location->lat))
	{
		$latitude = 1*$response_a->results[0]->geometry->location->lat;
		$longitude = 1*$response_a->results[0]->geometry->location->lng;
	}

}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>POS Index - World Map</title>

	<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $api_key;?>&v=3.exp&sensor=false"></script>
	
	<script type="text/javascript">
		
	  var address = "<?php echo $address;?>";
	  var short_address = "<?php echo $short_address;?>";
	  var country = "<?php echo $country;?>";

	  var zoom = <?php echo $zoom;?>;
	  var latitude = <?php echo $latitude;?>;
	  var longitude = <?php echo $longitude;?>;
	
		function save() 
		{
		   opener.document.getElementById("posaddress_google_lat").value = latitude;
		   opener.document.getElementById("posaddress_google_long").value = longitude;
		   opener.document.getElementById("gmcheck").value= "You have checked the map information.";
		   window.close();
		}

		function initialize() {
			  
			if(latitude == 0)
			{
			  var geo = new google.maps.Geocoder;
			  geo.geocode({'address':address},function(results, status){
				  if (status == google.maps.GeocoderStatus.OK)
				  {
					 latitude = results[0].geometry.location.lat();
					 longitude = results[0].geometry.location.lng();
					 
					  
					  var myLatlng = new google.maps.LatLng(latitude,longitude);
					  var mapOptions = {
						zoom: zoom,
						center: myLatlng,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					  }

					  var map = new google.maps.Map(document.getElementById('map'), mapOptions);

					  var contentString = '<div id="content"><br />'+
							latitude+', '+longitude+'</div>';

					  var infowindow = new google.maps.InfoWindow({
						  content: contentString
					  });

					  var marker = new google.maps.Marker({
						  position: myLatlng,
						  map: map,
						  draggable:true
					  });

					  google.maps.event.addListener(marker, 'click', function() {
						infowindow.open(map,marker);
					  });

					  google.maps.event.addListener(marker, 'dragend', function(evt) {
						latitude = evt.latLng.lat();
						longitude = evt.latLng.lng();

					  });
				  }
				  else
				  {
					  geo.geocode({'address':short_address},function(results, status){
						  if (status == google.maps.GeocoderStatus.OK)
						  {
							 latitude = results[0].geometry.location.lat();
							 longitude = results[0].geometry.location.lng();
							 							  
							  var myLatlng = new google.maps.LatLng(latitude,longitude);
							  var mapOptions = {
								zoom: zoom,
								center: myLatlng,
								mapTypeId: google.maps.MapTypeId.ROADMAP
							  }

							  var map = new google.maps.Map(document.getElementById('map'), mapOptions);

							  var contentString = '<div id="content"><br />'+
									latitude+', '+longitude+'</div>';

							  var infowindow = new google.maps.InfoWindow({
								  content: contentString
							  });

							  var marker = new google.maps.Marker({
								  position: myLatlng,
								  map: map,
								  draggable:true
							  });

							  google.maps.event.addListener(marker, 'click', function() {
								infowindow.open(map,marker);
							  });

							  google.maps.event.addListener(marker, 'dragend', function(evt) {
								latitude = evt.latLng.lat();
								longitude = evt.latLng.lng();

							  });
						  }
						  else
						  {
								geo.geocode({'address':country},function(results, status){
								  if (status == google.maps.GeocoderStatus.OK)
								  {
									 latitude = results[0].geometry.location.lat();
									 longitude = results[0].geometry.location.lng();
									 									  
									  var myLatlng = new google.maps.LatLng(latitude,longitude);
									  var mapOptions = {
										zoom: zoom,
										center: myLatlng,
										mapTypeId: google.maps.MapTypeId.ROADMAP
									  }

									  var map = new google.maps.Map(document.getElementById('map'), mapOptions);

									  var contentString = '<div id="content"><br />'+
											latitude+', '+longitude+'</div>';

									  var infowindow = new google.maps.InfoWindow({
										  content: contentString
									  });

									  var marker = new google.maps.Marker({
										  position: myLatlng,
										  map: map,
										  draggable:true
									  });

									  google.maps.event.addListener(marker, 'click', function() {
										infowindow.open(map,marker);
									  });

									  google.maps.event.addListener(marker, 'dragend', function(evt) {
										latitude = evt.latLng.lat();
										longitude = evt.latLng.lng();

									  });
								  }
							  }); 
						  }
					  });
				  }

			  });
			}
			else
			{

					var myLatlng = new google.maps.LatLng(latitude,longitude);
					  var mapOptions = {
						zoom: zoom,
						center: myLatlng,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					  }

					  var map = new google.maps.Map(document.getElementById('map'), mapOptions);

					  var contentString = '<div id="content"><br />'+
							latitude+', '+longitude+'</div>';

					  var infowindow = new google.maps.InfoWindow({
						  content: contentString
					  });

					  var marker = new google.maps.Marker({
						  position: myLatlng,
						  map: map,
						  draggable:true
					  });

					  google.maps.event.addListener(marker, 'click', function() {
						infowindow.open(map,marker);
					  });

					  google.maps.event.addListener(marker, 'dragend', function(evt) {
						latitude = evt.latLng.lat();
						longitude = evt.latLng.lng();

					  });
			
			}
		}
		google.maps.event.addDomListener(window, 'load', initialize);
    </script>
	
	
	
		
	<style type="text/css">
  		
		body{
			font-family: Verdana, Geneva, sans-serif;
			font-size: 12px;
			color: #000000;
		}

		a {
				color: #006699;
				text-decoration: none;
				font-weight: bold;
			}


			a:hover {
				color: #FF0000;
				text-decoration: none;
			}
		
		#map{font-family:Arial, Helvetica, sans-serif; }
		
		.clear{ float:none; clear:both; height:0px; line-height:0px; font-size:0px; }
		
		fieldset legend{ margin-bottom:4px; color:#000; }
  
  </style>
</head>

<body>

<br /><br />
1. Check the map information.<br />
2. Correct by moving the marker to the POS Location.<br />
3. Confirm by clicking the link at the bottom of this page.<br />
<br /><br />

<div id="map" style="width:700px; height: 500px"></div>
<br />
<a style="cursor:hand" href="javascript:void(0);" OnCLick="javascript:save()">Confirm the correctness of the map information</a>
</body>
</html>