<?php
/********************************************************************

    loc_language.php

    Lists translations of languages.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");
set_referer("loc_language_language.php");

$language_id = 0;

if(param("id"))
{
	$language_id = param("id");
}
elseif(param("language_id"))
{
	$language_id = param("language_id");
}


//get language name
$language_name = "";
$sql = "select language_name from languages where language_id = " . $language_id;
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$language_name = $row["language_name"];
}

//sql for the list
$sql = "select loc_language_id, loc_language_name, language_name " .
       "from loc_languages " . 
	   "left join languages on language_id = loc_language_language ";

$list = new ListView($sql);

$list->set_entity("loc_languages");
$list->set_order("language_name");

$list->add_hidden("language_id", $language_id);
$list->add_column("language_name", "Language", "loc_language_language.php?language_id=" . $language_id, LIST_FILTER_FREE);
$list->add_column("loc_language_name", "language Name");

$list->add_button(LIST_BUTTON_NEW, "Add Translation", "loc_language_language.php?language_id=" . $language_id);
$list->add_button(LIST_BUTTON_BACK, "Back");

$list->process();

$page = new Page("locales");

$page->header();
$page->title("Translations for " . $language_name);
$list->render();
$page->footer();
?>
