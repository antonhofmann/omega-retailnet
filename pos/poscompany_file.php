<?php
/********************************************************************

    poscompany_file.php

    Creation and mutation of item file records.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-09-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-09-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_use_posindex");

$form = new Form("addressfiles", "file");

/*
if (id())
{
    $sql = "select item_file_item from item_files where item_file_id = " . id();
    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_row($res);
    param("item", $row[0]);
}
*/

$form->add_hidden("addressfile_address", param("address"));
$form->add_hidden("address", param("address"));
$form->add_hidden("country", param("country"));


$form->add_section();
$form->add_lookup("address_company", "Company", "addresses", "address_company", 0, param("address"));
$form->add_lookup("address_place", "City", "addresses", "address_place", 0, param("address"));

$form->add_section();
$form->add_edit("addressfile_title", "Title", NOTNULL);

$sql = "select address_shortcut from addresses where address_id = " . param("address");
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_row($res);
$address_shortcut = make_valid_filename($row[0]);

$form->add_section();
$form->add_upload("addressfile_path", "File", "/files/addresses/$address_shortcut", NOTNULL);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("poscompanies");

$page->header();
$page->title(id() ? "Edit File" : "Add File");
$form->render();
$page->footer();

?>
