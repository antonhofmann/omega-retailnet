<?php
/********************************************************************

    reject_submission_update_session.php

    Reject Submission: POS Closing Assessment

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2012-07-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2012-07-27
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/
//set content of variables from session
session_name("retailnet");
session_start();

$user_id = $_SESSION["user_id"];
$data["post"] = $_POST;
$_SESSION["reject_action_" . $user_id] = $data;
echo "success";
?>