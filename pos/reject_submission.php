<?php
/********************************************************************

    reject_submission.php

    Reject Submission: POS Closing Assessment

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2012-07-20
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2012-07-20
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "../shared/func_posindex.php";

require_once "../include/page_modal.php";

check_access("can_reject_submissions");

if(param("id") > 0)
{
	$id = param("id");
}
else
{
	exit;
}

if(param("cancel_action") == 1)
{
	if(array_key_exists("reject_action_" . user_id(), $_SESSION))
	{
		unset($_SESSION["reject_action_" . user_id()]);
	}
		
	?>
		<script language="javascript">
			window.parent.$.window.hideAll();
		</script>

	<?php
	exit;
}


/********************************************************************
    prepare all data needed
*********************************************************************/
$recipient_name = "";
$recipient_text = "";
$recipient_id = 0;
$recipient_email = "";


$page_title= "Reject Closing Assessment";

$sql = "select posclosingassessment_posaddress_id, posclosingassessment_submissionuser, " .
       "posclosingassessment_resubmissionuser, posclosingassessment_rejected_by " . 
	   "from posclosingassessments " .
	   "where posclosingassessment_id = " . param("id");

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);


$pos_data = get_poslocation($row["posclosingassessment_posaddress_id"]);


$submitted_by = get_user($row["posclosingassessment_submissionuser"]);
$resubmitted_by = get_user($row["posclosingassessment_resubmissionuser"]);
$rejected_by = get_user($row["posclosingassessment_rejected_by"]);

if($row["posclosingassessment_resubmissionuser"])
{
	$recipient_text = "Closing Assessment resubmitted by";
	$recipient_name = $resubmitted_by["name"] . " " . $resubmitted_by["firstname"];
	$recipient_id = $resubmitted_by["id"]; 
	$recipient_email = $resubmitted_by["email"]; 
}
elseif($row["posclosingassessment_submissionuser"])
{
	$recipient_text = "Closing Assessment submitted by";
	$recipient_name = " by " . $submitted_by["name"] . " " . $submitted_by["firstname"];
	$recipient_id = $submitted_by["id"]; 
	$recipient_email = $submitted_by["email"]; 
}

$mail_group = "Submisson of Closing Assessment rejected for POS - ";
$subject = MAIL_SUBJECT_PREFIX . ": " . $mail_group . $pos_data["country_name"] . ": " . $pos_data["posaddress_name"];




$client_address = get_address($pos_data["posaddress_client_id"]);
$client = $client_address["company"] . ", " .
		  $client_address["zip"] . " " .
          $client_address["place"] . ", " .
          $client_address["country_name"];

$shop = $pos_data["posaddress_name"] . ", " .
        $pos_data["posaddress_address"] . ", " .
		$pos_data["posaddress_zip"] . " " .
        $pos_data["place_name"] . ", " .
        $pos_data["country_name"];


//get recipients
$cc_reciepients = array();
$cc_reciepients_ids = array();
$sql = "select user_id, concat(user_name , ' ', user_firstname) as user_name, " . 
	   "user_email, address_company, role_name " .
	   "from users " .
	   "left join user_roles on user_role_user = user_id " .
	   "left join addresses on address_id = user_address " . 
	   "left join roles on role_id = user_role_role " . 
	   "where user_active = 1 and user_role_role = 15 and address_country = " . $client_address["country"];

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	if(!in_array($row["user_id"], $cc_reciepients_ids))
	{
		$user = array();
		$user["name"] = $row["user_name"] . ' (' . $row["address_company"] . ')';
		$user["email"] = strtolower($row["user_email"]);
		$user["roles"] = "";
		$user["roles"] = $row["role_name"];
		
		$cc_reciepients[] = $user;
		$cc_reciepients_ids[] = $row["user_id"];
	}
}

//finance controllers
$sql = "select user_id, concat(user_name , ' ', user_firstname) as user_name, " . 
	   "user_email, address_company, role_name " .
	   "from users " .
	   "left join user_roles on user_role_user = user_id " .
	   "left join addresses on address_id = user_address " . 
	   "left join roles on role_id = user_role_role " . 
	   "where user_active = 1 and user_role_role = 22 ";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	if(!in_array($row["user_id"], $cc_reciepients_ids))
	{
		$user = array();
		$user["name"] = $row["user_name"] . ' (' . $row["address_company"] . ')';
		$user["email"] = strtolower($row["user_email"]);
		$user["roles"] = "";
		$user["roles"] = $row["role_name"];
		
		$cc_reciepients[] = $user;
		$cc_reciepients_ids[] = $row["user_id"];
	}
}


//get submission notificants
$sql = 'select * from projecttype_newproject_notifications ' . 
	   'where projecttype_newproject_notification_on_lnresubmission = 1 ' . 
	   ' and projecttype_newproject_notification_country = ' . $pos_data["posaddress_country"] . 
	   ' and projecttype_newproject_notification_postype = ' . $pos_data['posaddress_store_postype'];


$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	if($row["projecttype_newproject_notification_email"])
	{
		
		$sql = 'select * from users ' . 
			   'where user_email = ' . dbquote($row["projecttype_newproject_notification_email"]);

		$res_u = mysql_query($sql) or dberror($sql);
		if ($row_u = mysql_fetch_assoc($res_u))
		{
			if(!in_array($row_u["user_id"], $cc_reciepients_ids))
			{
				$cc_reciepients_ids[] = $row_u["user_id"];
				$cc_reciepients[] = array("name" => $row_u["user_name"] . " " . $row_u["user_firstname"], "email" => strtolower($row_u["user_email"]), "roles" => "Notification for Submissions");
			}
		}
		
		
	}

	if($row["projecttype_newproject_notification_emailcc1"])
	{
		
		$sql = 'select * from users ' . 
			   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc1"]);

		$res_u = mysql_query($sql) or dberror($sql);
		if ($row_u = mysql_fetch_assoc($res_u))
		{
			if(!in_array($row_u["user_id"], $cc_reciepients_ids))
			{
				$cc_reciepients_ids[] = $row_u["user_id"];
				$cc_reciepients[] = array("name" => $row_u["user_name"] . " " . $row_u["user_firstname"], "email" => strtolower($row_u["user_email"]), "roles" => "Notification for Submissions");
			}
		}
		
	}
	if($row["projecttype_newproject_notification_emailcc2"])
	{
		
		$sql = 'select * from users ' . 
			   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc2"]);

		$res_u = mysql_query($sql) or dberror($sql);
		if ($row_u = mysql_fetch_assoc($res_u))
		{
			if(!in_array($row_u["user_id"], $cc_reciepients_ids))
			{
				$cc_reciepients_ids[] = $row_u["user_id"];
				$cc_reciepients[] = array("name" => $row_u["user_name"] . " " . $row_u["user_firstname"], "email" => strtolower($row_u["user_email"]), "roles" => "Notification for Submissions");
			}
		}
		
		
	}
	if($row["projecttype_newproject_notification_emailcc3"])
	{
		
		$sql = 'select * from users ' . 
			   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc3"]);

		$res_u = mysql_query($sql) or dberror($sql);
		if ($row_u = mysql_fetch_assoc($res_u))
		{
			if(!in_array($row_u["user_id"], $cc_reciepients_ids))
			{
				$cc_reciepients_ids[] = $row_u["user_id"];
				$cc_reciepients[] = array("name" => $row_u["user_name"] . " " . $row_u["user_firstname"], "email" => strtolower($row_u["user_email"]), "roles" => "Notification for Submissions");
			}
		}
		
		
	}
	if($row["projecttype_newproject_notification_emailcc4"])
	{
		
		$sql = 'select * from users ' . 
			   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc4"]);

		$res_u = mysql_query($sql) or dberror($sql);
		if ($row_u = mysql_fetch_assoc($res_u))
		{
			if(!in_array($row_u["user_id"], $cc_reciepients_ids))
			{
				$cc_reciepients_ids[] = $row_u["user_id"];
				$cc_reciepients[] = array("name" => $row_u["user_name"] . " " . $row_u["user_firstname"], "email" => strtolower($row_u["user_email"]), "roles" => "Notification for Submissions");
			}
		}
		
	
	}
	if($row["projecttype_newproject_notification_emailcc5"])
	{
		
		$sql = 'select * from users ' . 
			   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc5"]);

		$res_u = mysql_query($sql) or dberror($sql);
		if ($row_u = mysql_fetch_assoc($res_u))
		{
			if(!in_array($row_u["user_id"], $cc_reciepients_ids))
			{
				$cc_reciepients_ids[] = $row_u["user_id"];
				$cc_reciepients[] = array("name" => $row_u["user_name"] . " " . $row_u["user_firstname"], "email" => strtolower($row_u["user_email"]), "roles" => "Notification for Submissions");
			}
		}
		
	
	}
	if($row["projecttype_newproject_notification_emailcc6"])
	{
		
		$sql = 'select * from users ' . 
			   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc6"]);

		$res_u = mysql_query($sql) or dberror($sql);
		if ($row_u = mysql_fetch_assoc($res_u))
		{
			if(!in_array($row_u["user_id"], $cc_reciepients_ids))
			{
				$cc_reciepients_ids[] = $row_u["user_id"];
				$cc_reciepients[] = array("name" => $row_u["user_name"] . " " . $row_u["user_firstname"], "email" => strtolower($row_u["user_email"]), "roles" => "Notification for Submissions");
			}
		}
		
	
	}
	if($row["projecttype_newproject_notification_emailcc7"])
	{
		
		$sql = 'select * from users ' . 
			   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc7"]);

		$res_u = mysql_query($sql) or dberror($sql);
		if ($row_u = mysql_fetch_assoc($res_u))
		{
			if(!in_array($row_u["user_id"], $cc_reciepients_ids))
			{
				$cc_reciepients_ids[] = $row_u["user_id"];
				$cc_reciepients[] = array("name" => $row_u["user_name"] . " " . $row_u["user_firstname"], "email" => strtolower($row_u["user_email"]), "roles" => "Notification for Submissions");
			}
		}
		
	}
}


$sender = get_user(user_id());
$cc_reciepients[] = array("name" => $sender["name"] . " " . $sender["firstname"], "email" => strtolower($sender["email"]), "roles" => "Copy to me");
$cc_reciepients_ids[] = user_id();



/********************************************************************
    save data
*********************************************************************/
$saved = 0;
$error = "";
if(param("save_form"))
{
	if(!$_POST["window_text"])
	{
		$error = "Please enter a mail text!";
	}
	elseif(count($_FILES) > 0 and $_FILES["mail_attachment1_path"]["name"] != '' and !$_POST["mail_attachment1_title"])
	{
		$error = "Please indicate a title for your attachment!";

	}
	else
	{
		$fields = array();
		$fields[] = "posclosingassessment_rejected = " . dbquote(date("Y-m-d"));
		$fields[] = "posclosingassessment_rejected_by = " . dbquote(user_id());

		$sql = "update posclosingassessments set " . join(", ", $fields) . " where posclosingassessment_id = " . param("id");
		mysql_query($sql) or dberror($sql);

		$saved = 1;
	}


	if($saved == 1)
	{
		//send mail
		$text = str_replace("\r\n", "\n", trim($_POST["window_text"]));

		if(count($_FILES) > 0 and $_FILES["mail_attachment1_path"]["name"] != '' )
		{
			$file_path = '/files/posclosings/pos_' . $pos_data["posaddress_id"];
			$path = combine_paths($_SERVER["DOCUMENT_ROOT"], $file_path);
			create_directory($path);
			$name = make_valid_filename($_FILES["mail_attachment1_path"]["name"]);
			
			$name = make_unique_filename($name, $_SERVER["DOCUMENT_ROOT"]);
			$file_path = '/files/posclosings/pos_' . $pos_data["posaddress_id"] . '/' . $name;
			
			copy($_FILES["mail_attachment1_path"]["tmp_name"], combine_paths($_SERVER["DOCUMENT_ROOT"], $file_path));

		 }


		$mail = new PHPMailer();
		$mail->Subject = $subject;
		$mail->SetFrom($sender["email"], $sender["firstname"] . " " . $sender["name"]);
		$mail->AddReplyTo($sender["email"], $sender["firstname"] . " " . $sender["name"]);

		
		$bodytext0 = str_replace("\r\n", "\n", $text) . "\n\n";
		$link = "pos_closing_assessment.php?id=". param("id");
		$bodytext = $bodytext0 . "Click below to have direct access to the project's Closing Assessment:\n";
		$bodytext = $bodytext .  APPLICATION_URL . "/pos/" . $link . "\n\n";
		

		$mail->Body = $bodytext;

		

		$rcpts = "";
		$rcptscc = "";
		$selected_reciepients = array();
		$mail->AddAddress($recipient_email, $recipient_name);
		$rcpts .= $recipient_email . "\n";


		$selected_reciepients[] = $recipient_email;

		foreach($cc_reciepients as $key=>$value)
		{
			if($_POST["R_" . $cc_reciepients_ids[$key]] == 1)
			{
				$mail->AddCC($value["email"], $value["name"]);
				$rcptscc .= $value["email"] . "\n";

				$selected_reciepients[] = $value["email"];

			}
		}

		if($rcptscc)
		{
			$rcpts .= "and CC-Mail to:" . "\n" . $rcptscc;
		}

		
		//get all ccmails
		if(array_key_exists('ccmails', $_POST))
		{
			
			$selected_ccreciepients = array();
			
			$ccmails =  explode(' ', preg_replace("'\r?\n'"," ",$_POST['ccmails'])); 
			foreach($ccmails as $ccmail) {
				if(is_email_address($ccmail)) {
					
					$selected_ccreciepients[strtolower($ccmail)] = strtolower($ccmail);
				}
			
			}

			foreach($selected_ccreciepients as $key=>$value)
			{
				if($value and !in_array($value, $selected_reciepients)) {
					$result = $mail->AddCC($value);
					$rcpts .= $value . "\n";
				}
			}
		}

		if(count($_FILES) > 0 and $_FILES["mail_attachment1_path"]["name"] != '' )
		{
			
			$filepath = $_SERVER["DOCUMENT_ROOT"] . $file_path;
			$mail->AddAttachment($filepath);
		}

		
		$mail->Send();

	
		//update mail history
		$fields = array();
		$values = array();

		$fields[] = "posmail_posaddress_id";
		$values[] = $pos_data["posaddress_id"];

		$fields[] = "posmail_sender_email";
		$values[] = dbquote($sender["email"]);

		$fields[] = "posmail_recipeint_email";
		$values[] = dbquote($rcpts);


		$fields[] = "posmail_subject";
		$values[] = dbquote($mail_group);

		$fields[] = "posmail_text";
		$values[] = dbquote($text);
		

		if(count($_FILES) > 0)
		{
			$fields[] = "posmail_file_title";
			$values[] = dbquote($_POST["mail_attachment1_title"]);
			
			$fields[] = "posmail_file";
			$values[] = dbquote($file_path);
		}

		$fields[] = "date_created";
		$values[] = "now()";

		$fields[] = "date_modified";
		$values[] = "now()";

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$fields[] = "user_modified";
		$values[] = dbquote(user_login());

		$sql = "insert into posmails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
		
		mysql_query($sql) or dberror($sql);


		if(array_key_exists("reject_action_" . user_id(), $_SESSION))
		{
			unset($_SESSION["reject_action_" . user_id()]);
		}
		
		?>
			<script language="javascript">
				window.parent.$.window.hideAll();
				window.parent.location.reload();
			</script>

		<?php

		
	}
}


//get session data for overlay window
$message = "";
if(array_key_exists("reject_action_" . user_id(), $_SESSION))
{
	
	$data = $_SESSION["reject_action_" . user_id()];
	
	if(count($data["post"]) > 0)
	{
		$message = $data["post"]["window_text"];
	}
}


/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_mails", "cer_mails");

$form->add_label("shop_address", "POS Location", RENDER_HTML, $shop);
$form->add_label("client_address", "Client", RENDER_HTML, $client);
$form->add_hidden("id", param("id"));
$form->add_hidden("recipient_id", $recipient_id);

$form->add_hidden("save_form", "1");
$form->add_hidden("cancel_action", "0");

$form->add_section("Mail Message");
$form->add_multiline("window_text", "Message*", 8, 0, $message);

$form->add_section("Attachments");
$form->add_edit("mail_attachment1_title", "File Title");
$form->add_upload("mail_attachment1_path", "Attachment", "/files/posclosings/pos_". $pos_data["posaddress_id"]);


$form->add_section("Recipient*");
$form->add_checkbox("recipient", $recipient_name, 1, DISABLED, $recipient_text);

$form->add_section("CC Recipients*");

foreach($cc_reciepients as $key=>$user)
{
	$form->add_checkbox("R_" . $cc_reciepients_ids[$key], $user["name"], "", "", $user["roles"]);
}

$form->add_section("CC Recipients");
$form->add_modal_selector("ccmails", "Selected Recipients", 8);


//$form->add_input_submit("send", "Send Mail", 0);
$link =  "javascript:document.forms['main'].submit();";
$form->add_button("send", "Send Mail", $link);

$link =  "javascript:$('#cancel_action').val('1');document.forms['main'].submit();";
$form->add_button("cancel", "Cancel", $link);

if($error)
{
	$form->error($error);
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Page_Modal("cer_projects");



$page->header();
$page->title($page_title);
$form->render();


?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#ccmails_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/shared/select_mail_recipients.php'
    });
    return false;
  });
});
</script>

<?php

$page->footer();

?>