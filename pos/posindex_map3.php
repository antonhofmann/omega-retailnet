<?php
/********************************************************************

    posindex_map3.php

    Show google Map of the POS and make corrections directly from the list of POS locations.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2012-02-01
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2012-02-01
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";
require_once "../include/page_modal.php";

check_access("can_use_posindex");


if(param("save")) {
	
	$sql = "update posaddresses set " . 
		   "posaddress_google_lat = " . param("posaddress_google_lat")  . ", ".
		   "posaddress_google_long = " . param("posaddress_google_long") . ", ".
	       "posaddress_google_precision = 1 " . 
		   "where posaddress_id = " . param("id");
	$result = mysql_query($sql) or dberror($sql);

	if(param("id"))
	{
		update_store_locator(id());
		mysql_select_db(RETAILNET_DB, $db);
	}
}

if(param("id") > 0)
{
	$pos = get_poslocation(param("id"), "posaddresses");
}

//check if user can edit this company
$can_edit = false;
if(has_access("can_edit_posindex"))
{
	$can_edit = true;
}
elseif(has_access("can_edit_his_posindex"))
{
	$can_edit = get_user_edit_permission(user_id(), param("id"));
}


// Build form
$form = new Form("posaddresses", "posaddress");

$form->add_hidden("country", param("country"));
$form->add_hidden("save", 1);
$form->add_hidden("id",  param("id"));

$form->add_lookup("posaddress_client_id", "Client", "addresses", "concat(address_company, ', ', address_place, ' [', address_shortcut, ']') as company", 0, $pos["posaddress_client_id"]);

$form->add_section("Address Data");
$form->add_label("posaddress_name", "POS Name");

$form->add_label("posaddress_address", "Address*", NOTNULL);
$form->add_label("posaddress_address2", "Address 2");
$form->add_label("posaddress_zip", "Zip*", NOTNULL);
$form->add_label("posaddress_place", "City*", NOTNULL);
$form->add_lookup("posaddress_country", "Country", "countries", "country_name", 0, $pos["posaddress_country"]);

$form->add_section("Google Data");


if(has_access("can_edit_posindex"))
{
	$form->add_edit("posaddress_google_lat", "Latitude*", NOTNULL, "", TYPE_DECIMAL, 20, 15);
	$form->add_edit("posaddress_google_long", "Longitude*", NOTNULL, "", TYPE_DECIMAL, 20, 15);
}
elseif($can_edit == true and has_access("can_edit_his_posindex"))
{
	$form->add_edit("posaddress_google_lat", "Latitude*", NOTNULL, "", TYPE_DECIMAL, 20, 15);
	$form->add_edit("posaddress_google_long", "Longitude*", NOTNULL, "", TYPE_DECIMAL, 20, 15);
}
elseif(has_access("can_view_posindex") or has_access("can_view_his_posindex"))
{
	$form->add_label("posaddress_google_lat", "Latitude");
	$form->add_label("posaddress_google_long", "Longitude");
}


// Populate form and process button clicks

$form->populate();
$form->process();


// Render page
$poslocation = get_poslocation(id(), "posaddresses");

$page = new Page_Modal("posaddresses");

$page->header();

//$page->title("POS Location - Google Map");


$form->render();

if(has_access("can_edit_posindex"))
{
	echo '<p><a href="#" onclick="javascript:document.forms.main.submit();">Save</a></p>';
}

echo "<iframe src =\"posindex_map31.php?id=" . param("id") . "\" width=\"800\" height=\"800\" frameborder=\"0\" marginwidth=\"0\"></iframe>";

$page->footer();

?>