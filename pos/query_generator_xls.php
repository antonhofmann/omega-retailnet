<?php
/********************************************************************

    query_generator_xls.php

    Generate Excel File from Query Definition

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2009-01-11
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2009-01-11
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_query_posindex");

require_once "../include/xls/Writer.php";
require_once "include/query_get_functions.php";
require_once "../shared/func_posindex.php";


/********************************************************************
    prepare all data needed
*********************************************************************/
if(!param("query_id"))
{
	exit;
}
$query_id = param("query_id");


//update filter in case elements are missing
if(isset($query_id))
{
	$filter = get_query_filter($query_id);

	if(!array_key_exists("gr", $filter))
	{
		$new_filter["gr"] =  "";
	}
	else
	{
		$new_filter["gr"] =  $filter["gr"];
	}

	
	if(!array_key_exists("incs", $filter))
	{
		$new_filter["incs"] =  "";
	}
	else
	{
		$new_filter["incs"] =  $filter["incs"];
	}

	if(!array_key_exists("ct", $filter))
	{
		$new_filter["ct"] =  "";
	}
	else
	{
		$new_filter["ct"] =  $filter["ct"];
	}

	if(!array_key_exists("at", $filter))
	{
		$new_filter["at"] =  "";
	}
	else
	{
		$new_filter["at"] =  $filter["at"];
	}

	if(!array_key_exists("pr", $filter))
	{
		$new_filter["pr"] =  "";
		
	}
	else
	{
		$new_filter["pr"] =  $filter["pr"];
	}

	if(!array_key_exists("fscs", $filter))
	{
		$new_filter["fscs"] =  "";
		
	}
	else
	{
		$new_filter["fscs"] =  $filter["fscs"];
	}

	


	


	if(!array_key_exists("latest_pos_lease", $filter))
	{
		$new_filter["latest_pos_lease"] =  "";
		$filter["latest_pos_lease"] = "";
		
	}
	else
	{
		$new_filter["latest_pos_lease"] =  $filter["latest_pos_lease"];
	}

	if(!array_key_exists("pos_without_lease", $filter))
	{
		$new_filter["pos_without_lease"] =  "";
		$filter["pos_without_lease"] = "";
		
	}
	else
	{
		$new_filter["pos_without_lease"] =  $filter["pos_without_lease"];
	}

	

	

	if(!array_key_exists("www", $filter))
	{
		$new_filter["www"] =  "";
		$filter["www"] = "";
		
	}
	else
	{
		$new_filter["www"] =  $filter["www"];
	}

	if(!array_key_exists("dcs", $filter))
	{
		$new_filter["dcs"] =  "";
		$filter["dcs"] = "";
		
	}
	else
	{
		$new_filter["dcs"] =  $filter["dcs"];
	}

	if(!array_key_exists("fcy", $filter))
	{
		$new_filter["fcy"] =  "";
		$filter["fcy"] = "";
		
	}
	else
	{
		$new_filter["fcy"] =  $filter["fcy"];
	}

	if(!array_key_exists("fcm", $filter))
	{
		$new_filter["fcm"] =  "";
		$filter["fcm"] = "";
		
	}
	else
	{
		$new_filter["fcm"] =  $filter["fcm"];
	}

	if(!array_key_exists("tcy", $filter))
	{
		$new_filter["tcy"] =  "";
		$filter["tcy"] = "";
		
	}
	else
	{
		$new_filter["tcy"] =  $filter["tcy"];
	}

	if(!array_key_exists("tcm", $filter))
	{
		$new_filter["tcm"] =  "";
		$filter["tcm"] = "";
		
	}
	else
	{
		$new_filter["tcm"] =  $filter["tcm"];
	}


	if(!array_key_exists("aofy", $filter))
	{
		$new_filter["aofy"] =  "";
		
	}
	else
	{
		$new_filter["aofy"] =  $filter["aofy"];
	}

	if(!array_key_exists("aofm", $filter))
	{
		$new_filter["aofm"] =  "";
		
	}
	else
	{
		$new_filter["aofm"] =  $filter["aofm"];
	}

	if(!array_key_exists("aoty", $filter))
	{
		$new_filter["aoty"] =  "";
		
	}
	else
	{
		$new_filter["aoty"] =  $filter["aoty"];
	}

	if(!array_key_exists("aotm", $filter))
	{
		$new_filter["aotm"] =  "";
		
	}
	else
	{
		$new_filter["aotm"] =  $filter["aotm"];
	}

	
	

	
	if(!array_key_exists("clfy", $filter))
	{
		$new_filter["clfy"] =  "";
		
	}
	else
	{
		$new_filter["clfy"] =  $filter["clfy"];
	}

	if(!array_key_exists("clfm", $filter))
	{
		$new_filter["clfm"] =  "";
		
	}
	else
	{
		$new_filter["clfm"] =  $filter["clfm"];
	}

	if(!array_key_exists("clty", $filter))
	{
		$new_filter["clty"] =  "";
		
	}
	else
	{
		$new_filter["clty"] =  $filter["clty"];
	}

	if(!array_key_exists("cltm", $filter))
	{
		$new_filter["cltm"] =  "";
		
	}
	else
	{
		$new_filter["cltm"] =  $filter["cltm"];
	}


	if(!array_key_exists("clm", $filter))
	{
		$new_filter["clm"] =  "";
		
	}
	else
	{
		$new_filter["clm"] =  $filter["clm"];
	}


	

	$new_filter["re"] =  $filter["re"];
	$new_filter["co"] =  $filter["co"];
	$new_filter["ci"] =  $filter["ci"];
	$new_filter["ar"] =  $filter["ar"];
	$new_filter["pct"] =  $filter["pct"];
	$new_filter["pl"] =  $filter["pl"];
	$new_filter["pt"] =  $filter["pt"];
	$new_filter["sc"] =  $filter["sc"];
	$new_filter["pk"] =  $filter["pk"];
	
	$new_filter["cl"] =  $filter["cl"];
	$new_filter["fr"] =  $filter["fr"];
	$new_filter["lopr"] =  $filter["lopr"];
	$new_filter["nopr"] =  $filter["nopr"];
	$new_filter["dcs"] =  $filter["dcs"];
	$new_filter["exfp"] =  $filter["exfp"];

	
	
		
	if(!array_key_exists("dos", $filter))
	{
		$new_filter["dos"] =  "";
		
	}
	else
	{
		$new_filter["dos"] =  $filter["dos"];
	}


	if(!array_key_exists("opy", $filter))
	{
		$new_filter["opy"] =  "";
		
	}
	else
	{
		$new_filter["opy"] =  $filter["opy"];
	}


	if(!array_key_exists("opm", $filter))
	{
		$new_filter["opm"] =  "";
		
	}
	else
	{
		$new_filter["opm"] =  $filter["opm"];
	}


	if(!array_key_exists("latest_pos_lease", $filter))
	{
		$new_filter["latest_pos_lease"] =  "";
		
	}
	else
	{
		$new_filter["latest_pos_lease"] =  $filter["latest_pos_lease"];
	}

	if(!array_key_exists("pos_without_lease", $filter))
	{
		$new_filter["pos_without_lease"] =  "";
		
	}
	else
	{
		$new_filter["pos_without_lease"] =  $filter["pos_without_lease"];
	}


	$sql = "update posqueries " . 
			   "set posquery_filter = " . dbquote(serialize($new_filter)) . 
			   " where posquery_id = " . param("query_id");

	$result = mysql_query($sql) or dberror($sql);
}

$posquery = get_query_name($query_id);
$query_name = str_replace(" ", "", $posquery["name"]);

include("query_generator_get_query_params.php");



/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$filename = "query_" . $query_name . "_" . date("Ymd") . ".xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename); 

$xls->setVersion(8);

$sheet =& $xls->addWorksheet("POS Query");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

//formats
$f_header =& $xls->addFormat();
$f_header->setSize(12);
$f_header->setAlign('left');
$f_header->setBold();

$f_title =& $xls->addFormat();
$f_title->setSize(10);
$f_title->setAlign('left');
$f_title->setBold();

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
//$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_group_title =& $xls->addFormat();
$f_group_title->setSize(10);
$f_group_title->setAlign('left');
$f_group_title->setBold();

$f_empty =& $xls->addFormat();
$f_empty->setSize(8);
$f_empty->setAlign('left');
$f_empty->setBorder(1);

$f_text_without_border =& $xls->addFormat();
$f_text_without_border->setSize(8);
$f_text_without_border->setAlign('left');
$f_text_without_border->setBorder(0);

$f_text =& $xls->addFormat();
$f_text->setSize(8);
$f_text->setAlign('left');
$f_text->setAlign('top');
$f_text->setBorder(1);
$f_text->setTextWrap();

$f_text_bold =& $xls->addFormat();
$f_text_bold->setSize(8);
$f_text_bold->setAlign('left');
$f_text_bold->setBorder(1);
$f_text_bold->setBold();

$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);
$f_number->setNumFormat(41);

$f_decimal2 =& $xls->addFormat();
$f_decimal2->setSize(8);
$f_decimal2->setAlign('right');
$f_decimal2->setBorder(1);
$f_decimal2->setNumFormat(4);

$f_percent =& $xls->addFormat();
$f_percent->setSize(8);
$f_percent->setAlign('right');
$f_percent->setBorder(1);
$f_percent->setNumFormat(10);

$f_decimal2_bold =& $xls->addFormat();
$f_decimal2_bold->setSize(8);
$f_decimal2_bold->setAlign('right');
$f_decimal2_bold->setBorder(1);
$f_decimal2_bold->setBold();
$f_decimal2_bold->setNumFormat(4);

$f_date =& $xls->addFormat();
$f_date->setSize(8);
$f_date->setAlign('left');
$f_date->setBorder(1);
$f_date->setNumFormat('14');

$f_date_bold =& $xls->addFormat();
$f_date_bold->setSize(8);
$f_date_bold->setAlign('left');
$f_date_bold->setBorder(1);
$f_date_bold->setBold();
$f_date_bold->setNumFormat('14');

$f_unused =& $xls->addFormat();
$f_unused->setSize(8);
$f_unused->setBorder(1);
$f_unused->setPattern(2);
$f_unused->setBgColor('silver');

$f_border_left=& $xls->addFormat();
$f_border_left->setTop(1);
$f_border_left->setBottom(1);
$f_border_left->setLeft(1);
$f_border_left->setSize(8);
$f_border_left->setAlign('left');
$f_border_left->setBold();

$f_border_right=& $xls->addFormat();
$f_border_right->setTop(1);
$f_border_right->setBottom(1);
$f_border_right->setRight(1);


$f_border_middle=& $xls->addFormat();
$f_border_middle->setTop(1);
$f_border_middle->setBottom(1);


/********************************************************************
    write all captions
*********************************************************************/
$row_index = 2;
$cell_index = 0;

//set initial column widths
$col_widths = array();
for($i=0;$i<count($captions);$i++)
{
	$col_widths[$i] = strlen($captions[$i])+1;
}

$sheet->write(0, 0, $posquery["name"] . "(" . date("d.m.Y") . ")", $f_header);

$query_has_filter = 0;
if($print_query_filter == 1)
{
		
	if($query_filter["cl"] > 0) // client address
	{
		$client = "";
		$sql_f = "select address_company, address_place, country_name " . 
			     "from addresses " . 
			     "left join countries on country_id = address_country " . 
			     "where address_id = " . $query_filter["cl"];
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$client = $row_f["address_company"] . ", " . $row_f["address_place"] . ", " . $row_f["country_name"];
		}

		if($client)
		{
			$sheet->write($row_index, 0, "Client: " . $client, $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}

	if($query_filter["ct"] > 0) // client type
	{
		$client_type = "";
		$sql_f = "select client_type_code " . 
			     "from client_types " . 
			     "where client_type_id = " . $query_filter["ct"];
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$client_type = $row_f["client_type_code"];
		}

		if($client_type)
		{
			$sheet->write($row_index, 0, "Client Type: " . $client_type, $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}


	if($query_filter["at"] > 0) // address type owner
	{
		$address_type = "";
		$sql_f = "select address_type_name " . 
			     "from address_types " . 
			     "where address_type_id = " . $query_filter["at"];
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$address_type = $row_f["address_type_name"];
		}

		if($address_type)
		{
			$sheet->write($row_index, 0, "Owner Address Type: " . $address_type, $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}

	if($query_filter["fr"] > 0) // franchisee address
	{
		$client = "";
		$sql_f = "select address_company, address_place, country_name " . 
			     "from addresses " . 
			     "left join countries on country_id = address_country " . 
			     "where address_id = " . $query_filter["fr"];
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$client = $row_f["address_company"] . ", " . $row_f["address_place"] . ", " . $row_f["country_name"];
		}

		if($client)
		{
			$sheet->write($row_index, 0, "Franchisee: " . $client, $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}

	if($query_filter["re"]) //Geographical region
	{
		$filter = "";
		$filter_salesregions = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["re"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_salesregions .= $value . ",";
			}
		}
		$filter_salesregions = "(" . substr($filter_salesregions, 0, strlen($filter_salesregions)-1) . ")";
		if($filter_salesregions != "()")
		{
			$filter .= "salesregion_id in " . $filter_salesregions . " ";
		}

		$sql_f = "select salesregion_name from salesregions where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["salesregion_name"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Geographical Regions: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}

	if($query_filter["gr"]) //Supplied region
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["gr"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "region_id in " . $filter_string . " ";
		}

		$sql_f = "select region_name from regions where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["region_name"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Supplied Regions: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}

	if($query_filter["pr"]) //provinces
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["pr"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "province_id in " . $filter_string . " ";
		}

		$sql_f = "select province_canton from provinces where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["province_canton"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Provinces: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}

	if($query_filter["co"]) //countries
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["co"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "country_id in " . $filter_string . " ";
		}

		$sql_f = "select country_name from countries where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["country_name"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Countries: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}

	if($query_filter["ci"]) //cities
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["ci"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}

		if($filter_string)
		{
			$filter_string = substr($filter_string, 0, strlen($filter_string)-1);
			$sheet->write($row_index, 0, "Cities: " . $filter_string, $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}

	if($query_filter["ar"]) //neighbourhood areas
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["ar"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "posareatype_id in " . $filter_string . " ";
		}

		$sql_f = "select posareatype_name from posareatypes where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["posareatype_name"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Neighbourhood Areas: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}


	
	if($query_filter["pct"]) //cost type, legal type
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["pct"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "project_costtype_id in " . $filter_string . " ";
		}

		$sql_f = "select project_costtype_text from project_costtypes where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["project_costtype_text"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Legal Types: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}

	if($query_filter["pl"]) // product line, furniture type
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["pl"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "product_line_id in " . $filter_string . " ";
		}

		$sql_f = "select product_line_name from product_lines where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["product_line_name"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Furnitue Types: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}

	if($query_filter["fscs"]) // furniture subclasses
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["fscs"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "productline_subclass_id in " . $filter_string . " ";
		}

		$sql_f = "select productline_subclass_name from productline_subclasses where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["productline_subclass_name"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Furnitue Subclasses: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}

	if($query_filter["pt"]) // product line, furniture type
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["pt"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= "'" . $value . "',";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "postype_name in " . $filter_string . " ";
		}

		$sql_f = "select DISTINCT postype_name from postypes where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["postype_name"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "POS Types: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}

	if($query_filter["sc"]) // POS Type Subclass
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["sc"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "possubclass_id in " . $filter_string . " ";
		}

		$sql_f = "select possubclass_name from possubclasses where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["possubclass_name"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "POS Type Subclasses: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}

	if($query_filter["pk"]) // project Type
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["pk"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "projectkind_id in " . $filter_string . " ";
		}

		$sql_f = "select projectkind_name from projectkinds where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["projectkind_name"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Project Types: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}

	
	if($query_filter["lopr"] > 0) // POS with locally produced projects
	{
		$sheet->write($row_index, 0, "Only POS with locally produced projects: yes", $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	

	if($query_filter["nopr"] > 0) // POS with no projects assigned
	{
		$sheet->write($row_index, 0, "Only POS with no projects assigned: yes", $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	if($query_filter["exfp"] > 0) // exlude fake projects
	{
		$sheet->write($row_index, 0, "Fake Projects excluded", $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	

	if($posquery_area_perception_filter)
	{
		$area_types = array();
		$area_types["Class/Image Area"] = array();
		$area_types["Tourist/Historical Area"] = array();
		$area_types["Public Transportation"] = array();
		$area_types["People Traffic Area"] = array();
		$area_types["Parking Possibilities"] = array();
		$area_types["Visibility from Pavement"] = array();
		$area_types["Visibility from across the Street"] = array();
		
		$perception_values = array();
		$posquery_area_perception_values = explode  ("-" , $posquery_area_perception[0]);
		foreach($posquery_area_perception_values as $key=>$value)
		{
			if($value and $value <= 5)
			{
				$area_types["Class/Image Area"][] = $value;
			}
			elseif($value and $value <= 10)
			{
				$area_types["Tourist/Historical Area"][] = $value-5;
			}
			elseif($value and $value <= 15)
			{
				$area_types["Public Transportation"][] = $value-10;
			}
			elseif($value and $value <= 20)
			{
				$area_types["People Traffic Area"][] = $value-15;
			}
			elseif($value and $value <= 25)
			{
				$area_types["Parking Possibilities"][] = $value-20;
			}
			elseif($value and $value <= 30)
			{
				$area_types["Visibility from Pavement"][] = $value-25;
			}
			elseif($value and $value <= 35)
			{
				$area_types["Visibility from across the Street"][] = $value-30;
			}
		}
		if(count($area_types["Class/Image Area"]) > 0)
		{
			$sheet->write($row_index, 0, "Class/Image Area: " . implode(", ", $area_types["Class/Image Area"]), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}
		if(count($area_types["Tourist/Historical Area"]) > 0)
		{
			
			$sheet->write($row_index, 0, "Tourist/Historical Area: " . implode(", ", $area_types["Tourist/Historical Area"]), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}
		if(count($area_types["Public Transportation"]) > 0)
		{
			
			$sheet->write($row_index, 0, "Public Transportation: " . implode(", ", $area_types["Public Transportation"]), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}
		if(count($area_types["People Traffic Area"]) > 0)
		{
			
			$sheet->write($row_index, 0, "People Traffic Area: " . implode(", ", $area_types["People Traffic Area"]), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}
		if(count($area_types["Parking Possibilities"]) > 0)
		{
			
			$sheet->write($row_index, 0, "Parking Possibilities: " . implode(", ", $area_types["Parking Possibilities"]), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}
		if(count($area_types["Visibility from Pavement"]) > 0)
		{
			
			$sheet->write($row_index, 0, "Visibility from Pavement: " . implode(", ", $area_types["Visibility from Pavement"]), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}
		if(count($area_types["Visibility from across the Street"]) > 0)
		{
			
			$sheet->write($row_index, 0, "Visibility from across the Street: " . implode(", ", $area_types["Visibility from accross the Street"]), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}
	}


	if($query_filter["dos"]) // Design Objectives
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["dos"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "design_objective_item_id in " . $filter_string . " ";
		}

		$sql_f = "select design_objective_item_id, design_objective_item_name, " .
						 "    design_objective_group_name, postype_name " .
						 "from design_objective_items left join design_objective_groups on design_objective_item_group = design_objective_group_id " .
						 "    left join postypes on design_objective_group_postype = postype_id " . 
						 " where " .  $filter .
						 " order by postype_name, design_objective_group_name, design_objective_item_name ";

		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["postype_name"] . " - " . $row_f["design_objective_group_name"] . " - " . $row_f["design_objective_item_name"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Design Objectives: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}


	if($query_filter["opy"] > 0) // Operating per
	{
		$sheet->write($row_index, 0, "Operating POS as per: " . $query_filter["opy"] . "/" . $query_filter["opm"] , $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}
	
	
}

if($query_has_filter == 1)
{
	$row_index++;
}

$sheet->write($row_index, $cell_index, "Counter", $f_caption);
$sheet->writeRow($row_index, $cell_index+1, $captions, $f_caption);

if($count_records == 1)
{
	
	$sheet->write($row_index, count($captions) + 1, "POS Count", $f_caption);	
}



if($sql)
{
	$counter = 1;
	$counter_group = 1;
	$counter_index_for_counter_array = 1;
	$counter_total_for_counter_array = 0;
	$counter_list_total_for_counter_array = 0;

	$old_group_01 = "";
	$old_group_02 = "";
	$goup_totals = array();
	$goup_totals_cellindex = array();

	$retail_area = 1;
	$goup_totals_retailarea = array();
	$total_retail_area = 1;


	foreach($totalisation_fields as $field=>$value)
	{
		$field_name = str_replace(".", "", $field);
		$goup_totals[$field] = 0;
		$goup_totals_cellindex[$field] = 0;
	}

	//update posaddresses from posorders
	/*
	$res = mysql_query($sql_u) or dberror($sql_u);
	while ($row = mysql_fetch_assoc($res))
	{
		$result = update_posdata_from_posorders($row["posaddress_id"]);
	}
	*/


	
	//print rows
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$cell_index = 1;
		$row_index++;

		foreach($selected_field_order as $key=>$field)
		{
			$field_name = str_replace(".", "", $db_info["attributes"][$field]);

			$output_text = 0;
			if($db_info["attributes"][$field] == "calculated_content")
			{
				$field_name = str_replace(".", "", $db_info["calculated_content_field"][$field]);
				
				$output = array();
				$sql_s = $db_info["calculated_content"][$field] . $row[$db_info["calculated_content_key"][$field]];
				
	
				if(array_key_exists($field, $db_info["calculated_content_sort_order"]) and $db_info["calculated_content_sort_order"][$field])
				{
					$sql_s .= ' order by ' . $db_info["calculated_content_sort_order"][$field];
				}
				$res_s = mysql_query($sql_s) or dberror($sql_s);

				while ($row_s = mysql_fetch_row($res_s))
				{
					$output[] = $row_s[0];
				}
				$output_text = implode(", ", $output);

			}
			elseif($db_info["attributes"][$field] == "content_by_function")
			{
				$field_name = str_replace(".", "", $field);
			}

			//echo $field . "->" . $field_name . "\r\n";


			//build totals
			if(array_key_exists($field_name, $totalisation_fields))
			{
				//list totals
				if($db_info["attributes"][$field] == "get_from_value_array")
				{
					$field_name = str_replace(".", "", $field);
					
					$tmp = $db_info["content_by_function_params"][$field];
					$array_name = $tmp[0];
					$value_array = $$array_name;
					if(array_key_exists($tmp[1], $value_array[$counter])
						and array_key_exists($tmp[2], $value_array[$counter][$tmp[1]])
					  )
					{
						$output_text = $value_array[$counter][$tmp[1]][$tmp[2]];
					}

					$totalisation_fields[$field_name] = $totalisation_fields[$field_name] + $output_text;
					$totalisation_fields_column_index[$field_name] = $cell_index;

					$goup_totals[$field_name] = $goup_totals[$field_name] + $output_text;
					$goup_totals_cellindex[$field_name] = $cell_index;
					
				}
				elseif($db_info["attributes"][$field] == "content_by_function")
				{
					$function_name = $db_info["content_by_function"][$field];
					$params = array();
					foreach($db_info["content_by_function_params"][$field] as $key=>$value)
					{
						if(array_key_exists($value, $row))
						{
							$params[] = "'" . $row[$value] . "'";
						}
						else
						{
							$params[] = "'" . $value . "'";
						}
					}

					$function = '$value = ' . str_replace('params', implode(', ', $params), $function_name) .';';
					eval($function);
					
					$totalisation_fields[$field_name] = $totalisation_fields[$field_name] + $value;
					$totalisation_fields_column_index[$field_name] = $cell_index;

					$goup_totals[$field_name] = $goup_totals[$field_name] + $value;
					$goup_totals_cellindex[$field_name] = $cell_index;
				}
				elseif($db_info["attributes"][$field] != "calculated_content")
				{
					$totalisation_fields[$field_name] = $totalisation_fields[$field_name] + $row[$field_name];
					$totalisation_fields_column_index[$field_name] = $cell_index;

					$goup_totals[$field_name] = $goup_totals[$field_name] + $row[$field_name];
					$goup_totals_cellindex[$field_name] = $cell_index;
				}
				else
				{
					if(is_numeric($output_text))
					{
						$field_name = str_replace(".", "", $db_info["calculated_content_field"][$field]);
						$totalisation_fields[$field_name] = $totalisation_fields[$field_name] + $output_text;
						$totalisation_fields_column_index[$field_name] = $cell_index;

						$goup_totals[$field_name] = $goup_totals[$field_name] + $output_text;
						$goup_totals_cellindex[$field_name] = $cell_index;

					}
					else
					{
						$field_name = str_replace(".", "", $db_info["calculated_content_field"][$field]);
						$totalisation_fields[$field_name] = $totalisation_fields[$field_name];
						$totalisation_fields_column_index[$field_name] = $cell_index;

						$goup_totals[$field_name] = $goup_totals[$field_name];
						$goup_totals_cellindex[$field_name] = $cell_index;
					}

				}
			}

			// add group footers
			$empty_lines_comittet = false;
			if($query_group01 > 0 and strpos($outputfields, $db_info["attributes"][$query_groups[$query_group01]]) !== false)
			{
				$group_field_name01 = str_replace(".", "", $db_info["attributes"][$query_groups[$query_group01]]);
				if($group_field_name01 == $field_name) // output only if there is grouping of rows
				{
					if($old_group_01 != $row[$field_name])
					{
						if($old_group_01)
						{
							foreach($goup_totals as $key=>$total)
							{
								$sheet->write($row_index, $goup_totals_cellindex[$key], $total, $f_decimal2_bold);
								$goup_totals[$key] = 0;
								$goup_totals_cellindex[$key] = 0;

							}
							if($count_records == 1)
							{
								
								
								if(count($poslocation_record_count) > 0)
								{
									$sheet->write($row_index, count($captions) + 1,$counter_total_for_counter_array, $f_caption);
								}
								else
								{
									$sheet->write($row_index, count($captions) + 1, $counter_group-1, $f_caption);
								}
							}

							$row_index++;
							$row_index++;
							$row_index++;
							$empty_lines_comittet = true;
							$sheet->write($row_index, 0, "Counter", $f_caption);
							$sheet->writeRow($row_index, $cell_index, $captions, $f_caption);

							if($count_records == 1)
							{
								$sheet->write($row_index, count($captions) + 1, "POS Count", $f_caption);	
							}

							$row_index++;
						}

						$old_group_01 = $row[$field_name];
						$counter_group = 1;
						
						$counter_list_total_for_counter_array = $counter_list_total_for_counter_array + $counter_total_for_counter_array;
						$counter_total_for_counter_array = 0;
					}
				}
			}
			if($query_group02 > 0 and array_key_exists($key+1, $selected_field_order ))
			{
				$next_field = $selected_field_order[$key+1];
				$next_field_name = str_replace(".", "", $db_info["attributes"][$next_field]);
				$group_field_name02 = str_replace(".", "", $db_info["attributes"][$query_groups[$query_group02]]);
				
				if($group_field_name02 == $next_field_name) // output only if there is grouping of rows
				{
					
					if($old_group_02 != $row[$next_field_name])
					{
						
						if($old_group_02 and $empty_lines_comittet == false)
						{
							foreach($goup_totals as $key=>$total)
							{
								$sheet->write($row_index, $goup_totals_cellindex[$key], $total, $f_decimal2_bold);
								$goup_totals[$key] = 0;
								$goup_totals_cellindex[$key] = 0;
							}
							
							if($count_records == 1)
							{
								$sheet->write($row_index, count($captions) + 1, $counter_group-1, $f_caption);
							}

							
							$row_index++;
							$row_index++;
							$empty_lines_comittet = true;
							$sheet->write($row_index, 0, "Counter", $f_caption);
							$sheet->writeRow($row_index, $cell_index, $captions, $f_caption);

							if($count_records == 1)
							{
								$sheet->write($row_index, count($captions) + 1, "POS Count", $f_caption);	
							}
							
							$row_index++;
						}

						$old_group_02 = $row[$next_field_name];
						$counter_group = 1;
					}
				}
			}
			
			
			if($db_info["attributes"][$field] == "content_by_function")
			{
				$field_name = str_replace(".", "", $field);
				$function_name = $db_info["content_by_function"][$field];
				$params = array();

				//$params[] = $row["posaddress_id"];
				

				foreach($db_info["content_by_function_params"][$field] as $key=>$value)
				{
					if(array_key_exists($value, $row))
					{
						$params[] = "'" . $row[$value] . "'";
					}
					else
					{
						$params[] = "'" . $value . "'";
					}
				}

				


				$function = '$output_text = ' . str_replace('params', implode(', ', $params), $function_name) .';';


				eval($function);

				
				if($db_info["datatypes"][$field] == 'integer')
				{	
					$sheet->write($row_index, $cell_index, $output_text, $f_number);
				}
				elseif($db_info["datatypes"][$field] == 'percent')
				{	
					$sheet->write($row_index, $cell_index, $output_text, $f_percent);
				}
				elseif($db_info["datatypes"][$field] == 'openinghrs')
				{
					$sheet->write($row_index, $cell_index, $output_text, $f_text);
					$col_widths[$cell_index-1] = 20;

				}
				elseif($db_info["datatypes"][$field] == 'text')
				{
					$sheet->write($row_index, $cell_index, $output_text, $f_text);

					if(array_key_exists($cell_index, $col_widths) and $col_widths[$cell_index] < strlen($output_text))
					{
						$col_widths[$cell_index] = strlen($output_text);
					}
				}
				elseif($db_info["datatypes"][$field] == 'date')
				{
					$sheet->write($row_index, $cell_index, mysql_date_to_xls_date($output_text), $f_date);
				}
				elseif($db_info["datatypes"][$field] == 'decimal2')
				{
					$sheet->write($row_index, $cell_index, $output_text, $f_decimal2);
				}

			}
			elseif($db_info["attributes"][$field] == "calculated_content")
			{
				if($db_info["datatypes"][$field] == 'text')
				{
					$sheet->write($row_index, $cell_index, $output_text, $f_text);
				}
				elseif($db_info["datatypes"][$field] == 'decimal2')
				{
					$sheet->write($row_index, $cell_index, $output_text, $f_decimal2);
				}

				if($col_widths[$cell_index-1] < strlen($output_text))
				{
					$col_widths[$cell_index-1] = strlen($output_text);
				}
			}
			elseif($db_info["attributes"][$field] == "get_from_value_array")
			{
				
				if($db_info["datatypes"][$field] == 'integer')
				{
					$sheet->write($row_index, $cell_index, $output_text, $f_text);
				}
				if($col_widths[$cell_index-1] < strlen($output_text))
				{
					$col_widths[$cell_index-1] = strlen($output_text);
				}
			}
			else
			{
				if($db_info["datatypes"][$field] == 'text')
				{
					if(!$row[$field_name])
					{
						$sheet->write($row_index, $cell_index, "", $f_text);
					}
					else
					{
						$sheet->write($row_index, $cell_index, $row[$field_name], $f_text);
					}
				}
				elseif($db_info["datatypes"][$field] == 'decimal2')
				{
					if(!$row[$field_name] or $row[$field_name] == 0)
					{
						$sheet->write($row_index, $cell_index, "", $f_decimal2);
					}
					else
					{
						$sheet->write($row_index, $cell_index, $row[$field_name], $f_decimal2);
					}
				}
				elseif($db_info["datatypes"][$field] == 'date')
				{
					$sheet->write($row_index, $cell_index, mysql_date_to_xls_date($row[$field_name]), $f_date);
				}
				elseif($db_info["datatypes"][$field] == 'boolean')
				{
					$output = "no";
					if($row[$field_name] == 1)
					{
						$output = "yes";
					}
					$sheet->write($row_index, $cell_index, $output, $f_text);
				}

				if($col_widths[$cell_index-1] < strlen($row[$field_name]))
				{
					$col_widths[$cell_index-1] = strlen($row[$field_name]);
				}
			}
			
			
			$cell_index++;
		}
		

		if($count_records == 1)
		{
			if(count($poslocation_record_count) > 0)
			{
				$sheet->write($row_index, count($captions) + 1, $poslocation_record_count[$counter_index_for_counter_array], $f_text);
				
				$counter_total_for_counter_array = $counter_total_for_counter_array  + $poslocation_record_count[$counter_index_for_counter_array];

				$counter_index_for_counter_array++;
			}
			else
			{
				$sheet->write($row_index, count($captions) + 1, $counter_group, $f_text);
				
			}					
		}

		$sheet->write($row_index, 0, $counter, $f_text); // counter column
		$counter++;
		$counter_group++;
		
		
	}
}

for($i=0;$i<count($captions);$i++)
{
	$sheet->setColumn($i+1, $i+1, $col_widths[$i]);
}


//print group total of last group
if($query_group01 > 0 and strpos($outputfields, $db_info["attributes"][$query_groups[$query_group01]]) !== false)
{
	$row_index++;
	if($count_records == 1)
	{
		
		if($count_records == 1)
		{
			if(count($poslocation_record_count) > 0)
			{
				$sheet->write($row_index, count($captions) + 1, $counter_total_for_counter_array, $f_caption);	
				$counter_list_total_for_counter_array = $counter_list_total_for_counter_array + $counter_total_for_counter_array;
			}
			else
			{
				$sheet->write($row_index, count($captions) + 1, $counter_group-1, $f_caption);	
				
			}					
		}		
	}
}


//print list totals
if(count($totalisation_fields) > 0 or $count_records == 1)
{
	
	$row_index++;
	$row_index++;
	$cell_index = 1;

	$sheet->write($row_index, 0, "", $f_text); // counter column

	foreach($selected_field_order as $key=>$field)
	{
		$field_name = str_replace(".", "", $field);
		if(array_key_exists($field_name, $totalisation_fields ))
		{
			$sheet->write($row_index, $totalisation_fields_column_index[$field_name],$totalisation_fields[$field_name], $f_decimal2_bold);
		}
		else
		{
			$sheet->write($row_index, $cell_index, "", $f_decimal2);
		}
		$cell_index++;
	}

	if($count_records == 1)
	{
		if(count($poslocation_record_count) > 0)
		{
			$sheet->write($row_index, count($captions) + 1, $counter_list_total_for_counter_array, $f_caption);	
		}
		else
		{
			$sheet->write($row_index, count($captions) + 1, $counter-1, $f_caption);	
		}
	}
}



$xls->close(); 

?>