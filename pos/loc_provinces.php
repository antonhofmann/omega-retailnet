<?php
/********************************************************************

    loc_provinces.php

    Lists provinces for translations.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");
set_referer("loc_province.php");



if(param("province_country") > 1)
{
	$province_country =	param("province_country");
}
else
{
	$province_country = 1;
}


/********************************************************************
    Create Form
*********************************************************************/ 
$form = new Form("provinces", "province");

$form->add_section("List Filter Selection");

$form->add_list("province_country", "Country",
    "select country_id, country_name from countries order by country_name", SUBMIT | NOTNULL, $province_country);


/********************************************************************
    Populate and process button clicks
*********************************************************************/ 
$form->populate();

/********************************************************************
    Create List
*********************************************************************/ 

//get all translations
$translations = array();
$sql = "select province_id,language_name " . 
       "from provinces " . 
       "left join loc_provinces on loc_province_province = province_id " . 
	   "left join languages on language_id = loc_province_language " . 
	   "order by language_name";


$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	if(array_key_exists($row["province_id"], $translations))
	{
		$translations[$row["province_id"]] = $translations[$row["province_id"]] . "/" . trim($row["language_name"]);
	}
	else
	{
		$translations[$row["province_id"]] = trim($row["language_name"]);
	}
}

$sql = "select province_id, province_canton " .
       "from provinces ";

$list_filter = "";

$list_filter =	"province_country = " . $province_country;

$list = new ListView($sql);

$list->set_entity("provinces");
$list->set_order("province_canton");
$list->set_filter($list_filter);

$list->add_column("province_canton", "Name", "loc_province.php", LIST_FILTER_FREE);
$list->add_text_column("translations", "Translations", COLUMN_UNDERSTAND_HTML, $translations);

$list->process();

$page = new Page("locales");

$page->header();
$page->title("Provinces");
$form->render();
$list->render();
$page->footer();
?>
