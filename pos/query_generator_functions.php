<?php
/********************************************************************

    query_generator_functions.php

    Set Grouping for the Query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2009-01-11
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2009-01-11
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_query_posindex");

require_once "include/query_get_functions.php";


/********************************************************************
    prepare all data needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("query_generator.php");
}

$query_id = param("query_id");
param("id", param("query_id"));

$posquery = get_query_name($query_id);
$icon = "../pictures/add_item.gif";
$link = "javascript:open_selector('')";


//get Query Fields
$fields = array();
$fieldstosumup = array();
$count_records = 0;

$sql = "select posquery_fields, posquery_fieldstosumup, posquery_countrecords from posqueries " .
	   "where posquery_id = " . $query_id;

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$fields = unserialize($row["posquery_fields"]);
	

	if(!array_key_exists("clo", $fields)) {
		$fields["clo"] = array();
	}

	if(!array_key_exists("dcs", $fields)) {
		$fields["dcs"] = array();
	}

	if(!array_key_exists("stat", $fields)) {
		$fields["stat"] = array();
	}

	if(!array_key_exists("le", $fields)) {
		$fields["le"] = array();
	}
	
	if($fields)
	{
		$fields = array_merge($fields["pl"], $fields["cl"], $fields["fr"], $fields["po"], $fields["lpo"], $fields["in"], $fields["le"], $fields["clo"], $fields["dcs"], $fields["stat"]);
	}

	$fieldstosumup = unserialize($row["posquery_fieldstosumup"]);
	$count_records = $row["posquery_countrecords"];
}

$groups = query_function_fields();
$choices = array();
foreach($groups as $key=>$field)
{
	if(array_key_exists($field, $fields))
	{
		$choices[$key] = $fields[$field];
	}
}


/********************************************************************
    create form
*********************************************************************/

$form = new Form("posqueries", "query_generator");

$form->add_hidden("query_id", param("query_id"));
$form->add_hidden("form_save", 1);
$form->add_section($posquery["name"]);

$form->add_section("");

if(count($choices) > 0)
{
	$form->add_comment("Build group and/or list totals of the following fields.");
}

foreach($choices as $key=>$caption)
{
	if(in_array($key,$fieldstosumup))
	{
		$form->add_checkbox("t_" . $key, $caption, true);
	}
	else
	{
		$form->add_checkbox("t_" .$key, $caption, false);
	}
}

$form->add_section("");
$form->add_comment("Show the number of records.");
$form->add_checkbox("posquery_countrecords", "Show the number of records.", $count_records, 0, "Count");

$form->add_button("submit", "Save Functions", 0);


if(check_if_query_has_fields($query_id) == true)
{
	$form->add_button("execute", "Execute Query");
}
$form->add_button("back", "Back to the List of Queries");


/********************************************************************
    process form
*********************************************************************/
$form->populate();
$form->process();

if($form->button("back"))
{
	redirect("query_generator.php");
}
elseif($form->button("submit"))
{
	
	if(count($choices) > 0)
	{

		$fieldstosumup = array();
		foreach($choices as $key=>$caption)
		{
			if($form->value("t_" . $key) == 1)
			{
				$fieldstosumup[] = $key;
			}
		}
		
		$sql = "Update posqueries SET " . 
			   "posquery_fieldstosumup = " . dbquote(serialize($fieldstosumup)) . 
			   " where posquery_id = " . param("query_id");

		$result = mysql_query($sql) or dberror($sql);
	}

	$sql = "Update posqueries SET " . 
		   "posquery_countrecords = " . dbquote($form->value("posquery_countrecords"))  . ", " . 
		   "date_modified = " . dbquote(date("Y-m-d H:s:i")) . ", " . 
		   "user_modified = " . dbquote(user_login()) .
		   " where posquery_id = " . param("query_id");

	$result = mysql_query($sql) or dberror($sql);

	redirect("query_generator_functions.php?query_id=" . param("query_id"));

}
elseif($form->button("execute"))
{
	redirect("query_generator_xls.php?query_id=" . param("query_id"));
}


/********************************************************************
    render
*********************************************************************/

$page = new Page("query_generator");
$page->header();
$page->title("Edit POS Query - Functions");

require_once("include/query_tabs.php");

$form->render();

$page->footer();

?>


