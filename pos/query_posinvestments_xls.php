<?php
/********************************************************************

    query_posinvestments_xls.php

    Generate Excel-File of POS Investments

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-06-09
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-06-09
    Version:        1.1.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_view_posindex");

require_once "../include/xls/Writer.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

/********************************************************************
    prepare Data Needed
*********************************************************************/

$pl = param("pl"); // product lines
$pls = param("pls"); // product line subclasses
$pk = param("pk"); // product kinds
$pt = param("pt"); // pos types
$sk = param("sk"); // POS Type Subclasses
$pct = param("pct"); // project cost type
$co = param("co"); // Countries
$re = param("re"); // Regions
$fr = param("fr"); // Franchisee ID
$cl = param("cl"); // Client ID
$lopr = param("lopr"); // Local Production

//$sc = param("sc"); // Store coordinators
//$odf = param("odf"); // Actual Opening Year From
//$odt = param("odt"); // Actual Opening Year To
//$cdf = param("cdf"); // Closing Year From
//$cdt = param("cdt"); // Closing Year To

//get product lines


$product_types = array();

$sql = "select postype_id, postype_name ".
       "from postypes order by postype_name";

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
    $pos_types[$row["postype_id"]] = $row["postype_name"];        
}

$project_kinds = array();

$sql = "select projectkind_id, projectkind_name ".
       "from projectkinds order by projectkind_name";

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
    $project_kinds[$row["projectkind_id"]] = $row["projectkind_name"];        
}


$pos_subclasses = array();

$sql = "select possubclass_id, possubclass_name ".
	   "from possubclasses " .
	   "order by possubclass_name";

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
    $pos_subclasses[$row["possubclass_id"]] = $row["possubclass_name"];        
}


$header = "";
$header = "POS Investments: " . $header . " (" . date("d.m.Y G:i") . ")";

/********************************************************************
    prepare Data
*********************************************************************/
$filter = "";
$filter_pk = "";

$pl = substr($pl,0, strlen($pl)-1); // remove last comma
$pl = str_replace("-", ",", $pl);
if($pl) // product line
{
    $filter =  " where (posaddress_store_furniture IN (" . $pl . "))";
}

$pls = substr($pls,0, strlen($pls)-1); // remove last comma
$pls = str_replace("-", ",", $pls);
if($pls and $filter) // product line subclasses
{
    $filter .=  " and posaddress_store_furniture_subclass IN (" . $pls . ")";
}
elseif($pls)
{
    $filter =  " where posaddress_store_furniture_subclass IN (" . $pls . ")";
}

$pk = substr($pk,0, strlen($pk)-1); // remove last comma
$pk = str_replace("-", ",", $pk);
if($pk) // project Type
{
    $filter_pk =  " and (posorder_project_kind IN (" . $pk . "))";
}


$pt = substr($pt,0, strlen($pt)-1); // remove last comma
$pt = str_replace("-", ",", $pt);
if($pt and $filter) // pos type
{
    $filter .=  " and (posaddress_store_postype IN (" . $pt . "))";
}
elseif($pt)
{
    $filter =  " where (posaddress_store_postype IN (" . $pt . "))";
}


$sk = substr($sk,0, strlen($sk)-1); // remove last comma
$sk = str_replace("-", ",", $sk);
if($sk and $filter) // POS Type Subclasses
{
    $filter .=  " and (posaddress_store_subclass IN (" . $sk . "))";
}
elseif($sk)
{
    $filter =  " where (posaddress_store_subclass IN (" . $sk . "))";
}

$pct = substr($pct,0, strlen($pct)-1); // remove last comma
$pct = str_replace("-", ",", $pct);
if($pct and $filter) // project cost type
{
    $filter .=  " and (posaddress_ownertype IN (" . $pct . "))";
}
elseif($pct)
{
   $filter =  " where (posaddress_ownertype IN (" . $pct . "))";
}


$re = substr($re,0, strlen($re)-1); // remove last comma
$re = str_replace("-", ",", $re);
if($re and $filter) // regiosn
{
    $filter .=  " and (country_salesregion IN (" . $re . "))";
}
elseif($re)
{
    $filter =  " where (country_salesregion IN (" . $re . "))";
}

$co = substr($co,0, strlen($co)-1); // remove last comma
$co = str_replace("-", ",", $co);
if($co and $filter) // country
{
    $filter .=  " and (posaddress_country IN (" . $co . "))";

}
elseif($co)
{
    $filter =  " where (posaddress_country IN (" . $co . "))";
}



if($cl and $filter) // Client
{
    $filter .=  " and (posaddress_client_id = " . $cl . ")";

}
elseif($cl)
{
    $filter =  " where (posaddress_client_id = " . $cl . ")";
}


if($fr and $filter) // franchisee
{
    $filter .=  " and (posaddress_franchisee_id = " . $fr . ")";

}
elseif($fr)
{
    $filter =  " where (posaddress_franchisee_id = " . $fr . ")";
}


$posorder_filter = "";
if($lopr == 1) // Local Production
{
	
	if($filter)
	{
		$filter2 = $filter . " and posorder_project_locally_produced = 1 ";
	}
	else
	{
		$filter2 = " where posorder_project_locally_produced = 1 ";
	}
	
	$sql = "select DISTINCT posaddress_id " . 
		   "from posaddresses " . 
		   "left join posorders on posorder_posaddress = posaddress_id " . 
		   $filter2;

	$res = mysql_query($sql) or dberror($sql);

	while($row = mysql_fetch_assoc($res))
	{
		$posorder_filter .= $row["posaddress_id"] . ",";
	}

	if($posorder_filter)
	{
		$posorder_filter = substr($posorder_filter, 0, strlen($posorder_filter) - 1);

		$posorder_filter = " posaddress_id IN (" .$posorder_filter . ") ";
	}

	if($filter)
	{
		$filter .= " and " . $posorder_filter;
	}
	else
	{
		$filter .= " where " . $posorder_filter;
	}
}

/*
$sc = substr($sc,0, strlen($sc)-1); // remove last comma
$sc = str_replace("-", ",", $sc);
if($sc and $filter) // store coordinator
{
    $filter .=  " and (project_retail_coordinator IN (" . $sc . "))";

}
elseif($sc)
{
    $filter =  " where (project_retail_coordinator IN (" . $sc . "))";
}
*/

$sql_d = "select * " . 
       "from posaddresses " . 
	   "left join countries on country_id = posaddress_country " . 
	   "left join salesregions on salesregion_id = country_salesregion " .
	   "left join product_lines on product_line_id = posaddress_store_furniture " .
	   "left join productline_subclasses on productline_subclass_id = posaddress_store_furniture_subclass " .
	   "left join postypes on postype_id = posaddress_store_postype " .
	   "left join possubclasses on possubclass_id = posaddress_store_subclass " . 
       "left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
	   $filter . 
	   " order by salesregion_name, country_name, posaddress_place";


/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$filename = "pos_investments_" . date("Ymd") . ".xls";
$xls =& new Spreadsheet_Excel_Writer(); 
$xls->send($filename);
$xls->setVersion(8);
$sheet =& $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

//formats
$header_row =& $xls->addFormat();
$header_row->setSize(10);
$header_row->setAlign('left');
$header_row->setBold();


$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);

$f_normal_bold =& $xls->addFormat();
$f_normal_bold->setSize(8);
$f_normal_bold->setAlign('left');
$f_normal_bold->setBorder(1);
$f_normal_bold->setBold();


$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_used =& $xls->addFormat();
$f_used->setSize(8);
$f_used->setBorder(1);
$f_used->setPattern(2);
$f_used->setBgColor('yellow');



//captions
$captions = array();
$captions[] = "Nr";
$captions[] = "Geographical Region";
$captions[] = "Country";
$captions[] = "City";
$captions[] = "POS Name";
$captions[] = "Environment";

$captions[] = "Intangibles CER CHF";
$captions[] = "Intangibles Real Cost CHF";
$captions[] = "Project";
$captions[] = "Furniture";
$captions[] = "Furniture Subclass";
$captions[] = "POS Type";
$captions[] = "POS Type Subclass";
$captions[] = "Legal Type";
$captions[] = "Project Type";
$captions[] = "Opening Date";
$captions[] = "Closing Date";
$captions[] = "Investment CER KL-approved CHF";
$captions[] = "Investment Real Cost CHF";

/********************************************************************
    write all captions
*********************************************************************/
$sheet->write(0, 0, $header, $header_row);
$sheet->writeRow(1, 0, $captions, $f_normal_bold);


$row_index = 2;
$cell_index = 0;
$counter = 0;
$col_widths = array();
for($i=0;$i<count($captions);$i++)
{
	$col_widths[$i] = strlen($captions[$i]);
}


$res = mysql_query($sql_d) or dberror($sql_d);
while ($row = mysql_fetch_assoc($res))
{
    //$result = update_posorders_year($row["posaddress_id"]);
	$result = update_posorders_from_projects($row["posaddress_id"]);
	
	//project Type
	$show_project = 1;
	if($filter_pk)
	{
		$sql_p =	"select posorder_project_kind " . 
					"from posorders " .
					"where posorder_type = 1 and posorder_posaddress = " . $row["posaddress_id"] .
					$filter_pk . 
		            " order by posorder_year DESC, posorder_opening_date DESC";

		$res_p = mysql_query($sql_p) or dberror($sql_p);
		if ($row_p = mysql_fetch_assoc($res_p))
		{
			$show_project = 1;
		}
		else
		{
			$show_project = 0;
		}
	}

	if($show_project == 1)
	{
	
		$counter++;
		$sheet->write($row_index, $cell_index, $counter, $f_normal);
		if($col_widths[$cell_index] < strlen($counter))
		{
			$col_widths[$cell_index] = strlen($counter);
		}
		$cell_index++;

		$sheet->write($row_index, $cell_index, $row["salesregion_name"], $f_normal);
		if($col_widths[$cell_index] < strlen($row["salesregion_name"]))
		{
			$col_widths[$cell_index] = strlen($row["salesregion_name"]);
		}
		$cell_index++;
		
		$sheet->write($row_index, $cell_index, $row["country_name"], $f_normal);
		if($col_widths[$cell_index] < strlen($row["country_name"]))
		{
			$col_widths[$cell_index] = strlen($row["country_name"]);
		}
		$cell_index++;

		$sheet->write($row_index, $cell_index, $row["posaddress_place"], $f_normal);
		if($col_widths[$cell_index] < strlen($row["posaddress_place"]))
		{
			$col_widths[$cell_index] = strlen($row["posaddress_place"]);
		}
		$cell_index++;

		
		//Store details

		$sheet->write($row_index, $cell_index, $row["posaddress_name"], $f_normal);
		if($col_widths[$cell_index] < strlen($row["posaddress_name"]))
		{
			$col_widths[$cell_index] = strlen($row["posaddress_name"]);
		}
		$cell_index++;

		$areas = array();
		$sql_a = "select posareatype_name " . 
			     "from posaddresses " . 
			     "left join posareas on posarea_posaddress " . 
			     "left join posareatypes on posareatype_id = posarea_area " . 
			     "where posaddress_id = " . $row["posaddress_id"];
		
		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
			$areas[] = $row_a["posareatype_name"];
		}
		
		$area = implode(", ", $areas);

		$sheet->write($row_index, $cell_index, $area, $f_normal);
		if($col_widths[$cell_index] < strlen($area))
		{
			$col_widths[$cell_index] = strlen($area);
		}
		$cell_index++;

				
		$total_cer = 0;
		$total_cms = 0;
		$sql_p = "select posinvestment_amount_cer, posinvestment_amount_cms " .
			     "from posinvestments " . 
				 "left join posinvestment_types on posinvestment_type_id = posinvestment_investment_type " .
				 "where posinvestment_type_intangible = 1 " . 
			     " and posinvestment_posaddress = " . $row["posaddress_id"] . 
				 " order by posinvestment_investment_type";

		$res_p = mysql_query($sql_p) or dberror($sql_p);
		while ($row_p = mysql_fetch_assoc($res_p))
		{
			$total_cer = $total_cer + $row_p["posinvestment_amount_cer"];
			$total_cms = $total_cms + $row_p["posinvestment_amount_cms"];
		}

		$sheet->write($row_index, $cell_index, number_format($total_cer, 2, ".", "'"), $f_number);
		if($col_widths[$cell_index] < strlen($total_cer))
		{
			$col_widths[$cell_index] = strlen($total_cer);
		}
		$cell_index++;

		$sheet->write($row_index, $cell_index, number_format($total_cms, 2, ".", "'"), $f_number);
		if($col_widths[$cell_index] < strlen($total_cms))
		{
			$col_widths[$cell_index] = strlen($total_cms);
		}
		$cell_index++;



		
		//projects
		if($filter_pk)
		{
			$sql_p = "select posorder_id, posorder_ordernumber, posorder_year, product_line_name, " . 
				     "projectkind_name, postype_name, possubclass_name, project_costtype_text, " .
				     "project_id, project_actual_opening_date, project_shop_closingdate, productline_subclass_name " . 
					 "from posorders " .
				     "left join orders on order_id = posorder_order " .
				     "left join projects on project_order = order_id " .
				     "left join product_lines on product_line_id = project_product_line " .
				     "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
				     "left join postypes on postype_id = project_postype ".
					 "left join projectkinds on projectkind_id = project_projectkind ".
				     "left join possubclasses on possubclass_id = project_pos_subclass ".
				     "left join project_costs on project_cost_order = order_id " .  
			         "left join project_costtypes on project_costtype_id = project_cost_type ".
					 "where posorder_type = 1 " . 
					 " and posorder_posaddress = " . $row["posaddress_id"] . 
				     $filter_pk . 
					 " order by posorder_year ASC";
		}
		else
		{
			$sql_p = "select posorder_id, posorder_ordernumber, posorder_year, product_line_name, " . 
				     "projectkind_name, postype_name, possubclass_name, project_costtype_text, " .
				     "project_id, project_actual_opening_date, project_shop_closingdate, productline_subclass_name " . 
					 "from posorders " .
				     "left join orders on order_id = posorder_order " .
				     "left join projects on project_order = order_id " .
				     "left join product_lines on product_line_id = project_product_line " .
				     "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
				     "left join postypes on postype_id = project_postype ".
					 "left join projectkinds on projectkind_id = project_projectkind ".
				     "left join possubclasses on possubclass_id = project_pos_subclass ".
				     "left join project_costs on project_cost_order = order_id " .  
			         "left join project_costtypes on project_costtype_id = project_cost_type ".
					 "where posorder_type = 1 " . 
					 " and posorder_posaddress = " . $row["posaddress_id"] . 
					 " order by posorder_year ASC";
		}

		

		$res_p = mysql_query($sql_p) or dberror($sql_p);
		while ($row_p = mysql_fetch_assoc($res_p))
		{
			$cell_index_p = $cell_index;

			$sheet->write($row_index, $cell_index_p, $row_p["posorder_ordernumber"], $f_normal);
			if($col_widths[$cell_index_p] < strlen($row_p["posorder_ordernumber"]))
			{
				$col_widths[$cell_index_p] = strlen($row_p["posorder_ordernumber"]);
			}
			$cell_index_p++;

			$product_line = "";
			$product_line_subclass = "";
			$project_kind = "";
			$pos_type = "";
			$project_subclass = "";
			$project_legaltype = "";
			$opening_date = "";
			$closing_date = "";

			if($row_p["product_line_name"])
			{
				$product_line = $row_p["product_line_name"];
				$product_line_subclass = $row_p["productline_subclass_name"];
				$project_kind = $row_p["projectkind_name"];
				$pos_type = $row_p["postype_name"];
				$project_subclass = $row_p["possubclass_name"];
				$project_legaltype = $row_p["project_costtype_text"];
				$opening_date = $row_p["project_actual_opening_date"];
				$closing_date = $row_p["project_shop_closingdate"];
			}
			else
			{
				if($filter_pk)
				{
				   $sql_a = "select posorder_ordernumber, product_line_name, projectkind_name, " .
						     "postype_name, possubclass_name, project_costtype_text,  " .
						     "posorder_opening_date, posorder_closing_date, productline_subclass_name " .
							 "from posorders " .
							 "left join product_lines on product_line_id = posorder_product_line " .
					         "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
						     "left join postypes on postype_id = posorder_postype ".
						     "left join projectkinds on projectkind_id = posorder_project_kind ".
						     "left join possubclasses on possubclass_id = posorder_subclass ".
						     "left join project_costtypes on project_costtype_id = posorder_legal_type ".
							 "where posorder_id = " . $row_p["posorder_id"] . 
					         $filter_pk . 
					         " order by posorder_year ASC";;
				}
				else
				{
					$sql_a = "select posorder_ordernumber, product_line_name, projectkind_name, " .
						     "postype_name, possubclass_name, project_costtype_text,  " .
						     "posorder_opening_date, posorder_closing_date, productline_subclass_name " .
							 "from posorders " .
							 "left join product_lines on product_line_id = posorder_product_line " .
						     "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
						     "left join postypes on postype_id = posorder_postype ".
						     "left join projectkinds on projectkind_id = posorder_project_kind ".
						     "left join possubclasses on possubclass_id = posorder_subclass ".
						     "left join project_costtypes on project_costtype_id = posorder_legal_type ".
							 "where posorder_id = " . $row_p["posorder_id"] . 
						     " order by posorder_year ASC";


					$res_a = mysql_query($sql_a) or dberror($sql_a);
					if ($row_a = mysql_fetch_assoc($res_a))
					{
						$product_line = $row_a["product_line_name"];
						$product_line_subclass = $row_a["productline_subclass_name"];
						$project_kind = $row_a["projectkind_name"];
						$pos_type = $row_a["postype_name"];
						$project_subclass = $row_a["possubclass_name"];
						$project_legaltype = $row_a["project_costtype_text"];
						$opening_date = $row_a["posorder_opening_date"];
						$closing_date = $row_a["posorder_closing_date"];
					}
				}

				
			
			}

			$sheet->write($row_index, $cell_index_p, $product_line, $f_normal);
			if($col_widths[$cell_index_p] < strlen($product_line))
			{
				$col_widths[$cell_index_p] = strlen($product_line);
			}
			$cell_index_p++;


			$sheet->write($row_index, $cell_index_p, $product_line_subclass, $f_normal);
			if($col_widths[$cell_index_p] < strlen($product_line_subclass))
			{
				$col_widths[$cell_index_p] = strlen($product_line_subclass);
			}
			$cell_index_p++;

			$sheet->write($row_index, $cell_index_p, $pos_type, $f_normal);
			if($col_widths[$cell_index_p] < strlen($pos_type))
			{
				$col_widths[$cell_index_p] = strlen($pos_type);
			}
			$cell_index_p++;

			$sheet->write($row_index, $cell_index_p, $project_subclass, $f_normal);
			if($col_widths[$cell_index_p] < strlen($project_subclass))
			{
				$col_widths[$cell_index_p] = strlen($project_subclass);
			}
			$cell_index_p++;

			$sheet->write($row_index, $cell_index_p, $project_legaltype, $f_normal);
			if($col_widths[$cell_index_p] < strlen($project_legaltype))
			{
				$col_widths[$cell_index_p] = strlen($project_legaltype);
			}
			$cell_index_p++;

			$sheet->write($row_index, $cell_index_p, $project_kind, $f_normal);
			if($col_widths[$cell_index_p] < strlen($project_kind))
			{
				$col_widths[$cell_index_p] = strlen($project_kind);
			}
			$cell_index_p++;

			$sheet->write($row_index, $cell_index_p, to_system_date($opening_date), $f_normal);
			if($col_widths[$cell_index_p] < strlen($opening_date))
			{
				$col_widths[$cell_index_p] = strlen($opening_date);
			}
			$cell_index_p++;

			$sheet->write($row_index, $cell_index_p, to_system_date($closing_date), $f_normal);
			if($col_widths[$cell_index_p] < strlen($closing_date))
			{
				$col_widths[$cell_index_p] = strlen($closing_date);
			}
			$cell_index_p++;

			//investments
			$reult = build_missing_investment_records($row_p["posorder_id"]);
			$total_cer = 0;
			$total_cms = 0;


			$sql_cer = "select * from cer_basicdata where cer_basicdata_project = " . dbquote($row_p["project_id"]);
			$res_cer = mysql_query($sql_cer) or dberror($sql_cer);

			$exchange_rate = 0;
			$factor = 1;
			if ($row_cer = mysql_fetch_assoc($res_cer))
			{
				$exchange_rate = $row_cer["cer_basicdata_exchangerate"];
				$factor = $row_cer["cer_basicdata_factor"];
			}

			$sql_cer = 'select sum(cer_investment_amount_cer_loc_approved) as kl_approved_total '.
					  'from cer_investments ' . 
						'where cer_investment_project = ' .  dbquote($row_p["project_id"]) . 
						' and cer_investment_type in (1, 3, 5, 7, 11, 13)';

			$res_cer = mysql_query($sql_cer) or dberror($sql_cer);
			if ($row_cer = mysql_fetch_assoc($res_cer) and $factor > 0) {

				$kl_approved_cer = $exchange_rate*$row_cer["kl_approved_total"]/$factor;

				$total_cer = $total_cer + $kl_approved_cer;
			}

			$sql_i = "select posorderinvestment_amount_cer, posorderinvestment_amount_cms " .
					 "from posorderinvestments " . 
					 "left join posinvestment_types on posinvestment_type_id = posorderinvestment_investment_type " .
					 "where posinvestment_type_intangible <> 1 " . 
					 " and posorderinvestment_posorder = " . $row_p["posorder_id"] .
					 " order by posorderinvestment_investment_type";

			$res_i = mysql_query($sql_i) or dberror($sql_i);
			while ($row_i = mysql_fetch_assoc($res_i))
			{
				
				//if($row_i["posorderinvestment_amount_cer"])
				//{
				//	$total_cer = $total_cer + $row_i["posorderinvestment_amount_cer"];
				//}

				
							
				if($row_i["posorderinvestment_amount_cms"])
				{
					$total_cms = $total_cms + $row_i["posorderinvestment_amount_cms"];
				}
			}


			$sheet->write($row_index, $cell_index_p, number_format($total_cer, 2, ".", "'"), $f_number);
			if($col_widths[$cell_index_p] < strlen($total_cer))
			{
				$col_widths[$cell_index_p] = strlen($total_cer);
			}
			$cell_index_p++;

			$sheet->write($row_index, $cell_index_p, number_format($total_cms, 2, ".", "'"), $f_number);
			if($col_widths[$cell_index_p] < strlen($total_cms))
			{
				$col_widths[$cell_index_p] = strlen($total_cms);
			}
			$cell_index_p++;


			

			$row_index++;
		
		}
		
		$cell_index = 0;
		$row_index++;
	}
}

for($i=0;$i<count($captions);$i++)
{
	$sheet->setColumn($i, $i, $col_widths[$i]);
}


$xls->close(); 

?>