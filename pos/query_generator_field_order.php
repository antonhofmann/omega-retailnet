<?php
/********************************************************************

    query_generator_field_order.php

    Field Order for the Query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2009-01-11
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2009-01-11
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_query_posindex");

require_once "include/query_get_functions.php";


/********************************************************************
    prepare all data needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("query_generator.php");
}

$query_id = param("query_id");

$posquery = get_query_name($query_id);
$icon = "../pictures/add_item.gif";
$link = "javascript:open_selector('')";


//get Query Fields
$fields = array();
$selected_field_order = array();

$sql = "select posquery_fields, posquery_field_order from posqueries " .
	   "where posquery_id = " . $query_id;

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$fields = unserialize($row["posquery_fields"]);
	if($fields)
	{
		if(!array_key_exists("clo", $fields)) {
			$fields["clo"] = array();
		}

		if(!array_key_exists("dcs", $fields)) {
			$fields["dcs"] = array();
		}
		if(!array_key_exists("stat", $fields)) {
			$fields["stat"] = array();
		}
		
		if(array_key_exists("pl", $fields) 
			and array_key_exists("cl", $fields)
			and array_key_exists("fr", $fields)
			and array_key_exists("po", $fields)
			and array_key_exists("in", $fields)
			and array_key_exists("le", $fields)
			and array_key_exists("clo", $fields)
			and array_key_exists("dcs", $fields)
			and array_key_exists("stat", $fields)
			)
		{
			$fields = array_merge($fields["pl"], $fields["cl"], $fields["fr"], $fields["po"], $fields["in"], $fields["le"], $fields["clo"], $fields["dcs"], $fields["stat"]);
		}
	}

	$fields_order = $row["posquery_field_order"];
	$fields_order_initial_value = $row["posquery_field_order"];

}

if($fields_order)
{
	$fields_order = str_replace("selected_field_order[]=", "", $fields_order);
	$selected_field_order = explode("&", $fields_order);

	$table = "";
	$table .= '<table id="selected_field_order" cellspacing="0" cellpadding="2">';
	foreach($selected_field_order as $key=>$field)
	{
		if(array_key_exists($field,$fields))
		{
			$table .= '<tr id="' . $field . '"><td>' . $fields[$field] . '</td></tr>';
		}
	}
	$table .= '</table>';
}
elseif($selected_field_order)
{
	$selected_field_order = $fields;
	$table = "";
	$table .= '<table id="selected_field_order" cellspacing="0" cellpadding="2">';
	foreach($selected_field_order as $key=>$caption)
	{
		if(array_key_exists($key,$fields))
		{
			$table .= '<tr id="' . $key . '"><td>' . $fields[$key] . '</td></tr>';
		}
	}
	$table .= '</table>';
}
else
{
	$table = "";
}



/********************************************************************
    create form
*********************************************************************/

$form = new Form("posqueries", "query_generator");

$form->add_hidden("query_id", param("query_id"));
$form->add_hidden("form_save", 1);
$form->add_hidden("posquery_field_order", $fields_order_initial_value);

$form->add_section($posquery["name"]);

$form->add_section(" ");
$form->add_comment("Please set the output order of the fields just by dragging and dropping the lines in the following list of selected fields.");

$form->add_table($table);

$form->add_button("submit", "Save Field Order", 0);
if(check_if_query_has_fields($query_id) == true)
{
	$form->add_button("execute", "Execute Query");
}
$form->add_button("back", "Back to the List of Queries");


/********************************************************************
    process form
*********************************************************************/
$form->populate();
$form->process();

if($form->button("back"))
{
	redirect("query_generator.php");
}
elseif($form->button("submit"))
{
	
	$sql = "Update posqueries SET " . 
		   "posquery_field_order = " . dbquote($form->value("posquery_field_order")) . ", " . 
		   "date_modified = " . dbquote(date("Y-m-d H:s:i")) . ", " . 
		    "user_modified = " . dbquote(user_login()) .
		   " where posquery_id = " . param("query_id");

	$result = mysql_query($sql) or dberror($sql);

	redirect("query_generator_field_order.php?query_id=" . param("query_id"));
}
elseif($form->button("execute"))
{
	redirect("query_generator_xls.php?query_id=" . param("query_id"));
}


/********************************************************************
    render
*********************************************************************/

$page = new Page("query_generator");
$page->header();
$page->title("Edit POS Query - Field Order");

require_once("include/query_tabs.php");

$form->render();

?>

<script type="text/javascript">
$(document).ready(function() {
    // Initialise the table
    $("#selected_field_order").tableDnD();

	$('#selected_field_order').tableDnD({
        onDragClass: "myDragClass",
		onDrop: function(table, row) {
			document.forms[0].posquery_field_order.value = $.tableDnD.serialize();
        }
    });
});
</script>

<?php

$page->footer();

?>


