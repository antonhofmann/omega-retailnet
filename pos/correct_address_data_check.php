<?php
/********************************************************************

    correct_address_data_check.php

    Lists of addresses for correction check

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_accept_pos_data_corrections");
set_referer("correct_pos_data_pos.php");


$preselect_filter = "";
if(param("country"))
{
	$preselect_filter = "address_country = " . param("country");
	register_param("country", param("country"));
}

if(!param("country"))
{
	redirect("welcome.php");
}

if($preselect_filter)
{
	$preselect_filter .= " and address_checked = 1 "; 
}
else
{
	$preselect_filter = " address_checked = 1";
}


$googlemap = array();
$closed = array();
$pdfs = array();
$sql_images = "select address_id, address_checked from _addresses ";

$res = mysql_query($sql_images) or dberror($sql_images);

while ($row = mysql_fetch_assoc($res))
{
	if($row["address_checked"] == 1)
	{
		$checked[$row["address_id"]] = "/pictures/bullet_ball_glass_green.gif";
	}
	else
	{
		$checked[$row["address_id"]] = "/pictures/bullet_ball_glass_red.gif";
	}
}

//compose list

$sql = "select address_id, address_company, " .
       "address_address, address_address2, address_zip, " .
       "    address_place, country_name " .
       "from _addresses " . 
	   "left join countries on address_country = country_id ";

$list = new ListView($sql);

$list->set_entity("_addresses");
$list->set_filter($preselect_filter);
$list->set_order("address_company, address_place");

$list->add_hidden("country", param("country"));

$list->add_image_column("checked", "checked", COLUMN_ALIGN_CENTER , $checked);
$list->add_column("address_company", "POS Name", "correct_address_data_check_address.php?country=" . param("country") , LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("address_place", "City", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("address_address", "Address", "", LIST_FILTER_FREE);


$list->populate();
$list->process();

$page = new Page("posaddresses", "Companies: Check Data Corrections");
require "include/pos_page_actions.php";
$page->header();

$page->title("Companies: Check Data Corrections");
$list->render();


$page->footer();

?>
