<?php
/********************************************************************

    logoff.php

    Entry page for the logoff section group.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2003-06-22
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-06-03
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

unset($_SESSION["user_id"]);
unset($_SESSION["user_login"]);
unset($_SESSION["user_permissions"]);

$page = new Page("logoff");
$page->header();
echo "<p>", "You have been logged off.", "</p>";
echo "<p>", "<a href=\"login.php\">Login again</a>", "</p>";
$page->footer();

?>