<?php
/********************************************************************

    dataanalysis_projects_preselect.php

    Preselection of Project List

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_administrate_posindex");
set_referer("posindex.php");

//compose list

$sql = "select DISTINCT postype_id, postype_name " . 
	   "from postypes " .
	   "where postype_id < 4 " .
	   " order by postype_name";

$sql2 = "select DISTINCT country_id, country_name " .
		"from orders " . 
		"left join countries on order_shop_address_country = country_id " . 
		"where order_actual_order_state_code <> '900' and order_type = 1 and country_name is not null " . 
		"order by country_name";

/*
$sql2 = "select DISTINCT country_id, country_name " .
		"from orders " . 
		"left join countries on order_shop_address_country = country_id " .
		"left join posorders on posorder_order = order_id " . 
		"where posorder_id is null and order_actual_order_state_code <> '900' and order_type = 1 and country_name is not null " . 
		"order by country_name";

echo $sql2;
abc();
*/

$sql3 = "select distinct year(date_created) from orders  " . 
        "where order_actual_order_state_code <> '900' and order_type = 1 " . 
		"order by year(date_created)";

$form = new Form("posaddresses", "posaddress");

$form->add_section("POS Type Selection");

$form->add_list("country", "Country",$sql2);
$form->add_list("type", "POS Type",$sql);
$form->add_list("year", "Year",$sql3);

$form->add_button("show_list", "Show List");

$form->populate();
$form->process();


if($form->button("show_list"))
{
	if($form->value("country")) {
		redirect("dataanalysis_projects.php?type=" . $form->value("type") . "&country=" . $form->value("country"). "&year=" . $form->value("year"));
	}
	else
	{
		$form->error("You must select a country");
	}
}

$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title("Project Orphans");
$form->render();

?>

	<script type="text/javascript">
		
		document.onkeydown = process_key;
		
		function process_key(e)
		{
		  if( !e ) 
		  {
			if( window.event ) 
			{
			  e = window.event;
			} 
			else 
			{
			  return;
			}
		  }

		  if(e.keyCode==13)
		  {
			  button('show_list');
		  }
		}
	</script>

	<?php

$page->footer();

?>
