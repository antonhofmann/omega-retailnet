<?php
/********************************************************************

    pos_closing_assessments.php

    List of POS Closing Assessments

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2012-02-03
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2012-02-03
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("has_access_to_posclosing_assessments");
set_referer("pos_closing_assessment.php");

$user = get_user(user_id());

//compose list

$sql = "select posclosingassessment_id, posclosingassessment_submissionuser, " . 
       "if(posaddress_name <> '', posaddress_name, 'n.a.') as posname, " .
       "posaddress_address, posaddress_address2, posaddress_zip, " .
       "    posaddress_place, country_name, project_costtype_text, postype_name, " .
	   "posaddress_google_precision, posaddress_store_closingdate, province_canton, posaddress_country, " .
	   "posclosingassessment_filesigned, posclosingassessment_submissiondate, " . 
	   "posclosingassessment_approvaldate, posclosingassessment_rejected " . 
       "from posclosingassessments " . 
	   "left join posaddresses on posaddress_id = posclosingassessment_posaddress_id " .
	   "left join places on place_id = posaddress_place_id " .
	   "left join provinces on province_id = place_province " .
	   "left join countries on posaddress_country = country_id " . 
	   "left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
	   "left join postypes on postype_id = posaddress_store_postype ";


$list_filter = "";
if(has_access("can_edit_posclosing_assessments"))
{
}
elseif(has_access("can_edit_his_posclosing_assessments") or has_access("can_view_only_his_posclosing_assessments"))
{
	$country_filter = "";
	$tmp = array();
	$sqlc = "select * from country_access " .
		   "where country_access_user = " . user_id();


	$resc = mysql_query($sqlc) or dberror($sqlc);

	while ($rowc = mysql_fetch_assoc($resc))
	{            
		$tmp[] = $rowc["country_access_country"];
	}

	if(count($tmp) > 0) {
		$country_filter = " posaddress_country IN (" . implode(",", $tmp) . ") ";
	}
	
	if($country_filter) 
	{
		$list_filter = "posaddress_client_id = " . $user["address"] . " or " . $country_filter;
	}
	else
	{
		$list_filter = "posaddress_client_id = " . $user["address"];
	}
}

$pdfs = array();
$booklets_pdfs = array();

$submissiondates = array();
$approvaldates = array();
$rejectiondates = array();

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	
	
	$submissiondates[$row["posclosingassessment_id"]] = to_system_date($row["posclosingassessment_submissiondate"]);
	$approvaldates[$row["posclosingassessment_id"]] = to_system_date($row["posclosingassessment_approvaldate"]);
	$rejectiondates[$row["posclosingassessment_id"]] = to_system_date($row["posclosingassessment_rejected"]);
	
	if($row["posclosingassessment_filesigned"])
	{
		$link = "<a href=\"javascript:popup('" .  $row["posclosingassessment_filesigned"] . "',800,600);\"><img src=\"/pictures/ico_pdf.gif\" border='0'/></a>";
		$pdfs[$row["posclosingassessment_id"]] = $link;


		$link = "<a href=\"javascript:popup('pos_closing_assessment_pdf.php?id=" .  $row["posclosingassessment_id"] . "&b=2',800,600);\"><img src=\"/pictures/accepted.gif\" border='0'/></a>";

		$booklets_pdfs[$row["posclosingassessment_id"]] = $link;
	}
	else
	{
		$link = "<a href=\"javascript:popup('pos_closing_assessment_pdf.php?id=" .  $row["posclosingassessment_id"] . "',800,600);\"><img src=\"/pictures/ico_pdf.gif\" border='0'/></a>";
		$pdfs[$row["posclosingassessment_id"]] = $link;


		$link = "<a href=\"javascript:popup('pos_closing_assessment_pdf.php?id=" .  $row["posclosingassessment_id"] . "&b=1',800,600);\"><img src=\"/pictures/edit01.gif\" border='0'/></a>";

		$booklets_pdfs[$row["posclosingassessment_id"]] = $link;
	}
}

/********************************************************************
    Create Form
*********************************************************************/

$list = new ListView($sql);

$list->set_entity("posclosingassessments");
$list->set_filter($list_filter);
$list->set_order("country_name, posaddress_place, posaddress_name");

$list->add_hidden("country", param("country"));
$list->add_hidden("province", param("province"));

$list->add_column("country_name", "Country", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);
$list->add_column("posname", "POS Name", "pos_closing_assessment.php" , LIST_FILTER_FREE,'', COLUMN_NO_WRAP);


$list->add_column("province_canton", "Province", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("posaddress_place", "City", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("posaddress_address", "Address", "", LIST_FILTER_FREE);
$list->add_column("postype_name", "POS Type", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);
$list->add_column("project_costtype_text", "Legal Type", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);

$list->add_text_column("submitted", "Submitted", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML, $submissiondates);
$list->add_text_column("rejected", "Rejected", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML, $rejectiondates);
$list->add_text_column("approved", "Approved", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML, $approvaldates);

$list->add_text_column("pdfs", "", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML, $pdfs);
$list->add_text_column("pdfs2", "", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML, $booklets_pdfs);



if(has_access("can_edit_his_posclosing_assessments") or has_access("can_edit_posclosing_assessments") )
{
	$list->add_button(LIST_BUTTON_NEW, "New", "pos_closing_assessment.php");
}

$list->populate();
$list->process();

$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title("POS Closing Assessments");

$list->render();


$page->footer();

?>
