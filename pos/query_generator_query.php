<?php
/********************************************************************

    query_generator_query.php

    Creation and mutation of queries.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2009-01-11
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2009-01-11
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_query_posindex");

set_referer("delete.php");

require_once "include/query_get_functions.php";

if(param("query_id"))
{
	param("id", param("query_id"));
	$query_id = param("query_id");
}
if(id())
{
	param("query_id",id());
	$query_id = id();
}

$user = get_user(user_id());

//update filter in case elements are missing
if(isset($query_id))
{
	$filter = get_query_filter($query_id);

	if(!array_key_exists("gr", $filter))
	{
		$new_filter["gr"] =  "";
	}
	else
	{
		$new_filter["gr"] =  $filter["gr"];
	}

	
		
	

	if(!array_key_exists("incs", $filter))
	{
		$new_filter["incs"] =  "";
		
	}
	else
	{
		$new_filter["incs"] =  $filter["incs"];
	}

	if(!array_key_exists("ct", $filter))
	{
		$new_filter["ct"] =  "";
		
	}
	else
	{
		$new_filter["ct"] =  $filter["ct"];
	}

	if(!array_key_exists("at", $filter))
	{
		$new_filter["at"] =  "";
		
	}
	else
	{
		$new_filter["at"] =  $filter["at"];
	}

	if(!array_key_exists("pr", $filter))
	{
		$new_filter["pr"] =  "";
		
	}
	else
	{
		$new_filter["pr"] =  $filter["pr"];
	}

	if(!array_key_exists("fscs", $filter))
	{
		$new_filter["fscs"] =  "";
		
	}
	else
	{
		$new_filter["fscs"] =  $filter["fscs"];
	}

	

	

	

	$new_filter["re"] =  $filter["re"];
	$new_filter["co"] =  $filter["co"];
	$new_filter["ci"] =  $filter["ci"];
	$new_filter["ar"] =  $filter["ar"];
	$new_filter["pct"] =  $filter["pct"];
	$new_filter["pl"] =  $filter["pl"];
	$new_filter["pt"] =  $filter["pt"];
	$new_filter["sc"] =  $filter["sc"];
	$new_filter["pk"] =  $filter["pk"];
	$new_filter["cl"] =  $filter["cl"];
	$new_filter["fr"] =  $filter["fr"];
	$new_filter["lopr"] =  $filter["lopr"];
	$new_filter["nopr"] =  $filter["nopr"];
	$new_filter["exfp"] =  $filter["exfp"];

	$new_filter["opy"] =  $filter["opy"];
	$new_filter["opm"] =  $filter["opm"];


	

		

	

	if(!array_key_exists("www", $filter))
	{
		$new_filter["www"] =  "";
		
	}
	else
	{
		$new_filter["www"] =  $filter["www"];
	}

	if(!array_key_exists("dcs", $filter))
	{
		$new_filter["dcs"] =  "";
		
	}
	else
	{
		$new_filter["dcs"] =  $filter["dcs"];
	}


	if(!array_key_exists("fcy", $filter))
	{
		$new_filter["fcy"] =  "";
		
	}
	else
	{
		$new_filter["fcy"] =  $filter["fcy"];
	}

	if(!array_key_exists("fcm", $filter))
	{
		$new_filter["fcm"] =  "";
		
	}
	else
	{
		$new_filter["fcm"] =  $filter["fcm"];
	}

	if(!array_key_exists("tcy", $filter))
	{
		$new_filter["tcy"] =  "";
		
	}
	else
	{
		$new_filter["tcy"] =  $filter["tcy"];
	}

	if(!array_key_exists("tcm", $filter))
	{
		$new_filter["tcm"] =  "";
		
	}
	else
	{
		$new_filter["tcm"] =  $filter["tcm"];
	}


	if(!array_key_exists("aofy", $filter))
	{
		$new_filter["aofy"] =  "";
		
	}
	else
	{
		$new_filter["aofy"] =  $filter["aofy"];
	}

	if(!array_key_exists("aofm", $filter))
	{
		$new_filter["aofm"] =  "";
		
	}
	else
	{
		$new_filter["aofm"] =  $filter["aofm"];
	}

	if(!array_key_exists("aoty", $filter))
	{
		$new_filter["aoty"] =  "";
		
	}
	else
	{
		$new_filter["aoty"] =  $filter["aoty"];
	}

	if(!array_key_exists("aotm", $filter))
	{
		$new_filter["aotm"] =  "";
		
	}
	else
	{
		$new_filter["aotm"] =  $filter["aotm"];
	}

	if(!array_key_exists("exfp", $filter))
	{
		$new_filter["exfp"] =  "";
		
	}
	else
	{
		$new_filter["exfp"] =  $filter["exfp"];
	}


	
	
	
	
	if(!array_key_exists("clfy", $filter))
	{
		$new_filter["clfy"] =  "";
		
	}
	else
	{
		$new_filter["clfy"] =  $filter["clfy"];
	}

	if(!array_key_exists("clfm", $filter))
	{
		$new_filter["clfm"] =  "";
		
	}
	else
	{
		$new_filter["clfm"] =  $filter["clfm"];
	}

	if(!array_key_exists("clty", $filter))
	{
		$new_filter["clty"] =  "";
		
	}
	else
	{
		$new_filter["clty"] =  $filter["clty"];
	}

	if(!array_key_exists("cltm", $filter))
	{
		$new_filter["cltm"] =  "";
		
	}
	else
	{
		$new_filter["cltm"] =  $filter["cltm"];
	}

	if(array_key_exists("dos", $filter))
	{
		$new_filter["dos"] =  $filter["dos"];
	}
	else
	{
		$new_filter["dos"] =  "";
	}

	

	if(!array_key_exists("latest_pos_lease", $filter))
	{
		$new_filter["latest_pos_lease"] =  "";
		$filter["latest_pos_lease"] = "";
		
	}
	else
	{
		$new_filter["latest_pos_lease"] =  $filter["latest_pos_lease"];
	}

	if(!array_key_exists("pos_without_lease", $filter))
	{
		$new_filter["pos_without_lease"] =  "";
		$filter["pos_without_lease"] = "";
		
	}
	else
	{
		$new_filter["pos_without_lease"] =  $filter["pos_without_lease"];
	}



	$sql = "update posqueries " . 
			   "set posquery_filter = " . dbquote(serialize($new_filter)) . 
			   " where posquery_id = " . param("query_id");

	$result = mysql_query($sql) or dberror($sql);
}

//prepare data
$access_roles = array();
$sql_access_roles = "select role_id from roles order by role_name";
$res = mysql_query($sql_access_roles) or dberror($sql_access_roles);
while ($row = mysql_fetch_assoc($res))
{
	$access_roles[] = $row["role_id"];
}

$sql_entities = "select posqueryentity_id, posqueryentity_name " .
                "from posqueryentities ";



if(count($access_roles) > 0)
{
	$sql_persons_hq = "select distinct user_id, concat(user_name, ' ' , user_firstname, '(', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " . 
		   "where user_id = 2 " . 
		   "or (address_id = 13 and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and (user_role_role IN (" . implode(",", $access_roles). ") or user_address = " . $user["address"] . "))" .
		   " or (address_type = 7 and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and (user_role_role IN (" . implode(",", $access_roles). ") or user_address = " . $user["address"] . "))" .
		   "order by username";

	$sql_persons_sub = "select distinct user_id, " . 
		   "concat(country_name, ' - ', user_name, ' ' , user_firstname, '(', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " .
		   " left join countries on country_id = address_country " . 
		   "where address_id <> 13 and address_client_type in (2, 3) and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and (user_role_role IN (" . implode(",", $access_roles). ") or user_address = " . $user["address"] . ") " . 
		   "order by country_name, username";

	$sql_persons_ag = "select distinct user_id, " . 
		   "concat(country_name, ' - ', user_name, ' ' , user_firstname, '(', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " .
		    " left join countries on country_id = address_country " .  
		   "where address_id <> 13 and address_client_type = 1 and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and (user_role_role IN (" . implode(",", $access_roles). ") or user_address = " . $user["address"] . ") " . 
		   "order by country_name, username";
}
else
{
	$sql_persons_hq = "select distinct user_id, concat(user_name, ' ' , user_firstname, ' (', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " .
		   "where user_id = 2 or (address_id = 13  and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and user_address = " . $user["address"] . ") " . 
		   "order by username";

	$sql_persons_sub = "select distinct user_id, " . 
		   "concat(country_name, ' - ', user_name, ' ' , user_firstname, ' (', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " .
		   " left join countries on country_id = address_country " . 
		   "where address_id <> 13 and address_client_type in (2, 3) and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and user_address = " . $user["address"] . " " . 
		   "order by country_name, username";

	$sql_persons_ag = "select distinct user_id, " . 
		   "concat(country_name, ' - ', user_name, ' ' , user_firstname, ' (', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " .
		   " left join countries on country_id = address_country " . 
		   "where address_id <> 13 and address_client_type = 1 and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and user_address = " . $user["address"] . " " . 
		   "order by country_name, username";
}



$qgroups = array();
$query_groups = "select DISTINCT posquery_id, posquery_group " . 
       "from posqueries " . 
	   " where posquery_group <> '' and posquery_owner = " . user_id() . 
	   " order by posquery_group";
$res = mysql_query($query_groups) or dberror($query_groups);
while ($row = mysql_fetch_assoc($res))
{
	if(!in_array($row["posquery_group"], $qgroups))
	{
		$qgroups[$row["posquery_id"]] = $row["posquery_group"];
	}
}

$form = new Form("posqueries", "Query");

$form->add_hidden("query_id", param("query_id"));




$form->add_edit("posquery_name", "Name*", NOTNULL);

$form->add_checkbox("posquery_print_filter", "Print Query Filter Information in Excel Sheet", "", "", "Filter");


$form->add_section("Group of Queries");
$form->add_comment("Please select to which group the query belongs or enter a new group.");
$form->add_list("existing_groups", "Existing Groups", $qgroups, SUBMIT);
$form->add_edit("posquery_group", "Group");


$form->add_section("Access Rights");
$form->add_comment("The following persons can have access to my queries");

$form->add_checklist("Persons0", "Persons_HQ", "posquery_permissions",
    $sql_persons_hq, 0, "", true);

$form->add_checklist("Persons1", "Subs and Affiliates", "posquery_permissions",
    $sql_persons_sub, 0, "", true);

$form->add_checklist("Persons2", "Agents", "posquery_permissions",
    $sql_persons_ag, 0, "", true);


$form->add_hidden("posquery_owner", user_id());


/*
if(param("query_id") > 0)
{
	$form->add_comment("The leading entity of this query is:");
	$form->add_lookup("posquery_leading_entity", "Leading Entity", "posqueryentities", "posqueryentity_name");
}
else
{
	$form->add_comment("Please choose the leading entity for your query.");
	$form->add_list("posquery_leading_entity", "Leading Entity*", $sql_entities, NOTNULL);
}
*/
$form->add_hidden("posquery_leading_entity", 2);

$form->add_button("save", "Save Query");
if(param("query_id") > 0)
{
	$form->add_button("copy_query", "Copy Query");
	$form->add_button(FORM_BUTTON_DELETE, "Delete Query");
}
$form->add_button(FORM_BUTTON_BACK, "Back to the List of Queries");

$form->populate();
$form->process();


if($form->button("existing_groups"))
{
	$form->value("posquery_group", $qgroups[$form->value("existing_groups")]);
	$form->value("existing_groups", "");
}
elseif($form->button("save"))
{
	if($form->validate())
	{
		$form->save();

		
		if(!param("query_id")) // new record
		{
			//create fields
			$fields = array();
			$fields["pl"] = array();
			$fields["cl"] = array();
			$fields["fr"] = array();
			$fields["po"] = array();
			$fields["in"] = array();
			
			$sql_u = "update posqueries " . 
					 "set posquery_fields = " . dbquote(serialize($fields)) . 
					 " where posquery_id = " . id();

			$result = mysql_query($sql_u) or dberror($sql_u);

			//create filter
			$filter = array();
			$filter["re"] =  "";
			$filter["gr"] =  "";
			$filter["co"] =  "";
			$filter["pr"] =  "";
			$filter["ci"] =  "";
			$filter["ar"] =  "";

			$filter["pct"] =  "";
			$filter["pl"] =  "";
			$filter["fscs"] =  "";
			$filter["pt"] =  "";
			$filter["sc"] =  "";
			$filter["cl"] =  "";
			$filter["fr"] =  "";
			$filter["pk"] =  "";
			$filter["in"] =  "";
			$filter["lopr"] =  "";
			$filter["nopr"] =  "";
			$filter["exfp"] =  "";
			$filter["incs"] =  "";
			$filter["ct"] =  "";
			$filter["at"] =  "";

			$filter["dcs"] =  "";
			$filter["fcy"] =  "";
			$filter["fcm"] =  "";
			$filter["tcy"] =  "";
			$filter["tcm"] =  "";

			$filter["aofy"] =  "";
			$filter["aofm"] =  "";
			$filter["aoty"] =  "";
			$filter["aotm"] =  "";


			$filter["clfy"] =  "";
			$filter["clfm"] =  "";
			$filter["clty"] =  "";
			$filter["cltm"] =  "";

			$filter["dos"] =  "";
			$filter["www"] =  "";

			$filter["opy"] =  "";
			$filter["opm"] =  "";

			$filter["latest_pos_lease"] =  "";
			$filter["latest_pos_lease"] =  "";

			

			//check if user can select Geographical region and country
			// and set filter
			if(has_access("can_view_his_posindex"))
			{
				$predefined_filter = user_predefined_filter(user_id());
				$filter["re"] =  $predefined_filter["re"];
				$filter["gr"] =  $predefined_filter["gr"];
				$filter["co"] =  $predefined_filter["co"];
			}

			$sql_u = "update posqueries " . 
				     "set posquery_filter = " . dbquote(serialize($filter)) . 
				     " where posquery_id = " . id();

			$result = mysql_query($sql_u) or dberror($sql_u);

			$fieldstosumup = array();
			$sql_u = "update posqueries " . 
				     "set posquery_fieldstosumup = " . dbquote(serialize($fieldstosumup)) . 
				     " where posquery_id = " . id();

			$result = mysql_query($sql_u) or dberror($sql_u);
		}


		redirect("query_generator_query.php?query_id=" .  id());
	}
}
elseif($form->button("copy_query"))
{
	if(param("query_id"))
	{
		
		$sql = "select * from posqueries where posquery_id = " . param("query_id");
		$res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
			
			$sql = "INSERT INTO posqueries (". 
				   "posquery_owner, posquery_name, posquery_print_filter, posquery_leading_entity, " . 
				   "posquery_fields, posquery_field_order, posquery_filter, posquery_order, posquery_grouping01, " . 
				   "posquery_grouping02, posquery_fieldstosumup, posquery_area_perception, " . 
				   "user_created, date_created) ". 
				   "VALUES (" . 
				   dbquote(user_id()) . ", " .
				   dbquote($row["posquery_name"]. " - copy") . ", " .
				   dbquote($row["posquery_print_filter"]) . ", " .
				   dbquote($row["posquery_leading_entity"]) . ", " .
				   dbquote($row["posquery_fields"]) . ", " .
				   dbquote($row["posquery_field_order"]) . ", " .
				   dbquote($row["posquery_filter"]) . ", " .
				   dbquote($row["posquery_order"]) . ", " .
				   dbquote($row["posquery_grouping01"]) . ", " .
				   dbquote($row["posquery_grouping02"]) . ", " .
				   dbquote($row["posquery_fieldstosumup"]) . ", " .
				   dbquote($row["posquery_area_perception"]) . ", " .
				   dbquote(user_login()) . ", " .
				   dbquote(date("Y-m-d")) . ")";

				   
			$result = mysql_query($sql) or dberror($sql);
		}
	}
	redirect("query_generator.php");
			
}

$page = new Page("query_generator");
$page->header();
$page->title(id() ? "Edit POS Query - Name" : "Add Query");

if(id() > 0 or param("query_id") > 0)
{
	require_once("include/query_tabs.php");
}

$form->render();
$page->footer();

?>