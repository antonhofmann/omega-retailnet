<?php
/********************************************************************

    loc_posaddress.php

    Lists translations of pos locations.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");
set_referer("loc_posaddress_posaddress.php");

$posaddress_id = 0;

if(param("id"))
{
	$posaddress_id = param("id");
}
elseif(param("posaddress_id"))
{
	$posaddress_id = param("posaddress_id");
}


//get posaddress name
$posaddress_name = "";
$sql = "select posaddress_place, posaddress_country, posaddress_name from posaddresses where posaddress_id = " . $posaddress_id;
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$posaddress_place = $row["posaddress_place"];
	$posaddress_name = $row["posaddress_name"];
	$posaddress_country = $row["posaddress_country"];
}

//sql for the list
$sql = "select loc_posaddress_id, loc_posaddress_name, language_name " .
       "from loc_posaddresses " .
	   "left join languages on language_id = loc_posaddress_language ";

$list = new ListView($sql);

$list->set_entity("loc_posaddresses");
$list->set_order("language_name");
$list->set_filter("loc_posaddress_posaddress = " . $posaddress_id);

$list->add_hidden("posaddress_id", $posaddress_id);
$list->add_hidden("posaddress_country", param("posaddress_country"));
$list->add_column("language_name", "Language", "loc_posaddress_posaddress.php?posaddress_id=" . $posaddress_id, LIST_FILTER_FREE);
$list->add_column("loc_posaddress_name", "posaddress Name");

$list->add_button(LIST_BUTTON_NEW, "Add Translation", "loc_posaddress_posaddress.php?posaddress_id=" . $posaddress_id);
$list->add_button("back", "Back");

$list->process();

if($list->button("back"))
{
	$link = "loc_posaddresses.php?posaddress_country= " . $posaddress_country;
	redirect($link);
}

$page = new Page("locales");

$page->header();
$page->title("Translations for " . $posaddress_place . ", " . $posaddress_name);
$list->render();
$page->footer();
?>
