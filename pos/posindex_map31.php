<?php
/********************************************************************

    posindex_map31.php

    Show google Map of the POS.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2012-02-01
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2012-02-01
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

check_access("can_use_posindex");

$api_key = GOOGLE_API_KEY;
$pos = get_poslocation(param("id"), "posaddresses");

$latitude = 0;
$longitude = 0;
$zoom = 18;

if($pos["posaddress_google_lat"])
{
	$latitude = $pos["posaddress_google_lat"];
	$zoom = 18;
}
if($pos["posaddress_google_long"])
{
	$longitude = $pos["posaddress_google_long"];
	$zoom = 18;
}


//get google coordinates
$short_address = $pos["country_name"] . ', ' . $pos["place_name"];
if($pos["posaddress_address2"])
{
	$address = $pos["country_name"] . ', ' . $pos["posaddress_zip"] . " " . $pos["place_name"] . ', ' . $pos["posaddress_address"] . ', ' . $pos["posaddress_address2"];

	
}
else
{
	$address = $pos["country_name"] . ', ' . $pos["posaddress_zip"] . " " . $pos["place_name"] . ', ' . $pos["posaddress_address"];
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>POS Index - World Map</title>

	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>

<?php
if($latitude != 0)
{

?>
	<script type="text/javascript">
		
				
		function initialize() {
			  

			  var zoom = <?php echo $zoom;?>;
			  var latitude = <?php echo $latitude;?>;
			  var longitude = <?php echo $longitude;?>;

			  
			  var myLatlng = new google.maps.LatLng(latitude,longitude);
			  var mapOptions = {
				zoom: zoom,
				center: myLatlng,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			  }

			  var map = new google.maps.Map(document.getElementById('map'), mapOptions);

			  var contentString = '<div id="content"><br />'+
					latitude+', '+longitude+'</div>';

			  var infowindow = new google.maps.InfoWindow({
				  content: contentString
			  });

			  var marker = new google.maps.Marker({
				  position: myLatlng,
				  map: map,
				  draggable:true
			  });

			  google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(map,marker);
			  });

			  google.maps.event.addListener(marker, 'dragend', function(evt) {
				parent.document.getElementById("posaddress_google_lat").value=evt.latLng.lat();
				parent.document.getElementById("posaddress_google_long").value=evt.latLng.lng();

			  });
		}
		google.maps.event.addDomListener(window, 'load', initialize);
	</script>
<?php
}
else
{
?>
	<script type="text/javascript">
		
				
		function initialize() {
			  var address = "<?php echo $address;?>";
			  var short_address = "<?php echo $short_address;?>";
			  var country = "<?php echo $pos['country_name'];?>";
			  var coords = 'undefined';

			  var zoom = <?php echo $zoom;?>;
			  var latitude = <?php echo $latitude;?>;
			  var longitude = <?php echo $longitude;?>;

			  
			  var geo = new google.maps.Geocoder;
			  geo.geocode({'address':address},function(results, status){
				  if (status == google.maps.GeocoderStatus.OK)
				  {
					 latitude = results[0].geometry.location.lat();
					 longitude = results[0].geometry.location.lng();
					 parent.document.getElementById("posaddress_google_lat").value=latitude;
					 parent.document.getElementById("posaddress_google_long").value=longitude;
					  
					  var myLatlng = new google.maps.LatLng(latitude,longitude);
					  var mapOptions = {
						zoom: zoom,
						center: myLatlng,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					  }

					  var map = new google.maps.Map(document.getElementById('map'), mapOptions);

					  var contentString = '<div id="content"><br />'+
							latitude+', '+longitude+'</div>';

					  var infowindow = new google.maps.InfoWindow({
						  content: contentString
					  });

					  var marker = new google.maps.Marker({
						  position: myLatlng,
						  map: map,
						  draggable:true
					  });

					  google.maps.event.addListener(marker, 'click', function() {
						infowindow.open(map,marker);
					  });

					  google.maps.event.addListener(marker, 'dragend', function(evt) {
						parent.document.getElementById("posaddress_google_lat").value=evt.latLng.lat();
						parent.document.getElementById("posaddress_google_long").value=evt.latLng.lng();

					  });
				  }
				  else
				  {
					  geo.geocode({'address':short_address},function(results, status){
						  if (status == google.maps.GeocoderStatus.OK)
						  {
							 latitude = results[0].geometry.location.lat();
							 longitude = results[0].geometry.location.lng();
							 parent.document.getElementById("posaddress_google_lat").value=latitude;
							 parent.document.getElementById("posaddress_google_long").value=longitude;
							  
							  var myLatlng = new google.maps.LatLng(latitude,longitude);
							  var mapOptions = {
								zoom: zoom,
								center: myLatlng,
								mapTypeId: google.maps.MapTypeId.ROADMAP
							  }

							  var map = new google.maps.Map(document.getElementById('map'), mapOptions);

							  var contentString = '<div id="content"><br />'+
									latitude+', '+longitude+'</div>';

							  var infowindow = new google.maps.InfoWindow({
								  content: contentString
							  });

							  var marker = new google.maps.Marker({
								  position: myLatlng,
								  map: map,
								  draggable:true
							  });

							  google.maps.event.addListener(marker, 'click', function() {
								infowindow.open(map,marker);
							  });

							  google.maps.event.addListener(marker, 'dragend', function(evt) {
								parent.document.getElementById("posaddress_google_lat").value=evt.latLng.lat();
								parent.document.getElementById("posaddress_google_long").value=evt.latLng.lng();

							  });
						  }
						  else
						  {
								geo.geocode({'address':country},function(results, status){
								  if (status == google.maps.GeocoderStatus.OK)
								  {
									 latitude = results[0].geometry.location.lat();
									 longitude = results[0].geometry.location.lng();
									 parent.document.getElementById("posaddress_google_lat").value=latitude;
									 parent.document.getElementById("posaddress_google_long").value=longitude;
									  
									  var myLatlng = new google.maps.LatLng(latitude,longitude);
									  var mapOptions = {
										zoom: zoom,
										center: myLatlng,
										mapTypeId: google.maps.MapTypeId.ROADMAP
									  }

									  var map = new google.maps.Map(document.getElementById('map'), mapOptions);

									  var contentString = '<div id="content"><br />'+
											latitude+', '+longitude+'</div>';

									  var infowindow = new google.maps.InfoWindow({
										  content: contentString
									  });

									  var marker = new google.maps.Marker({
										  position: myLatlng,
										  map: map,
										  draggable:true
									  });

									  google.maps.event.addListener(marker, 'click', function() {
										infowindow.open(map,marker);
									  });

									  google.maps.event.addListener(marker, 'dragend', function(evt) {
										parent.document.getElementById("posaddress_google_lat").value=evt.latLng.lat();
										parent.document.getElementById("posaddress_google_long").value=evt.latLng.lng();

									  });
								  }
							  }); 
						  }
					  });
				  }

			  });
		}
		google.maps.event.addDomListener(window, 'load', initialize);
    </script>
<?php
}
?>
	<style type="text/css">
  		
		body{
			font-family: Verdana, Geneva, sans-serif;
			font-size: 12px;
			color: #000000;
		}

		a {
				color: #006699;
				text-decoration: none;
				font-weight: bold;
			}


			a:hover {
				color: #FF0000;
				text-decoration: none;
			}
		
		#map{font-family:Arial, Helvetica, sans-serif; }
		
		.clear{ float:none; clear:both; height:0px; line-height:0px; font-size:0px; }
		
		fieldset legend{ margin-bottom:4px; color:#000; }
  
  </style>
</head>

<body>

<br /><br />
Move the marker to get the latitude and longitude coordinates of the point or <a href="#" onclick="javascript:initialize();">Propose Location</a>.
<br /><br />

<div id="map" style="width:700px; height: 500px"></div>
  </body>
</html>