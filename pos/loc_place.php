<?php
/********************************************************************

    loc_place.php

    Lists translations of places.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");
set_referer("loc_place_place.php");

$place_id = 0;

if(param("id"))
{
	$place_id = param("id");
}
elseif(param("place_id"))
{
	$place_id = param("place_id");
}


//get place name
$place_name = "";
$sql = "select place_country, place_province, place_name from places where place_id = " . $place_id;
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$place_name = $row["place_name"];
	$place_country = $row["place_country"];
	$place_province = $row["place_province"];
}

//sql for the list
$sql = "select loc_place_id, loc_place_name, language_name " .
       "from loc_places " . 
	   "left join languages on language_id = loc_place_language ";

$list = new ListView($sql);

$list->set_entity("loc_places");
$list->set_order("language_name");
$list->set_filter("loc_place_country = " . $place_country);

$list->add_hidden("place_id", $place_id);
$list->add_hidden("place_country", param("place_country"));
$list->add_hidden("place_province", param("place_province"));
$list->add_column("language_name", "Language", "loc_place_place.php?place_id=" . $place_id, LIST_FILTER_FREE);
$list->add_column("loc_place_name", "Place Name");

$list->add_button(LIST_BUTTON_NEW, "Add Translation", "loc_place_place.php?place_id=" . $place_id);
$list->add_button("back", "Back");

$list->process();

if($list->button("back"))
{
	$link = "loc_places.php?place_country= " . $place_country;
	redirect($link);
}

$page = new Page("locales");

$page->header();
$page->title("Translations for " . $place_name);
$list->render();
$page->footer();
?>
