<?php
/********************************************************************

    pos_pictures.php

    Show Pictures of the POS

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2009-07-19
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2009-07-19
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

if(!has_access("can_view_his_posindex") 
       and !has_access("can_edit_his_posindex") 
	   and !has_access("can_view_posindex") 
	   and !has_access("can_edit_posindex"))
{
	redirect("noaccess.php");
}

echo '<style>';
echo 'div#nyroModalLoading {background: #000;}';
echo 'div#nyroModalWrapper {background: #000;}';
echo '.description {color: #FFF; font-weight:bold;}';
echo '</style>';

if(param("posaddress_id") > 0)
{

	echo '<div style="text-align: center;">';
	echo '<br />';
	echo '<br />';
	

	$sql_p = "select order_file_path, order_file_title, posorder_ordernumber " . 
			 "from order_files " .
			 "inner join posorders on posorder_order = order_file_order " . 
			 "where posorder_posaddress = " . param("posaddress_id")  . 
			 "  and order_file_category = 11 " . 
		     " order by posorder_id DESC";
		
	$res_p = mysql_query($sql_p) or dberror($sql_p);
	while ($row_p = mysql_fetch_assoc($res_p))
	{
		if(strpos($row_p["order_file_path"], 'jpg') > 0 or strpos($row_p["order_file_path"], 'JPG') > 0) {
			echo "<span class='description'>" . $row_p["posorder_ordernumber"] . " - " . $row_p["order_file_title"] . "</span><br />";
			echo '<img style="max-width:800px;border: 2px solid #ffffff;" src="'. $row_p["order_file_path"] . '" /><br /><br /><br />';
		}
	}


	$sql_p = "select posfile_path, posfile_title, posorder_ordernumber " . 
			 "from posfiles " . 
			 "left join posorders on posorder_id = posfile_posorder " .
			 "where posfile_posaddress = " . param("posaddress_id") . 
			 "  and posfile_filegroup = 1 " . 
		     "order by posorder_order DESC";
	
	$res_p = mysql_query($sql_p) or dberror($sql_p);
	while ($row_p = mysql_fetch_assoc($res_p))
	{
		echo "<span class='description'>" . $row_p["posorder_ordernumber"] . " - " . $row_p["posfile_title"] . "</span><br />";
		echo '<img style="max-width:800px;border: 2px solid #ffffff;" src="'. $row_p["posfile_path"] . '" /><br /><br /><br />';
	}


	echo '</div>';

}

?>