<?php
/********************************************************************

    correct_send_mail_to_brand_managers.php

    Send Mail to brand managers

    Created by:     Anbton Hofmann (aho@mediaparx.ch)
    Date created:   2008-11-29
    Modified by:    Anbton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-11-29
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_mailalerts.php";

check_access("can_accept_pos_data_corrections");


$user = get_user(user_id());

$form = new Form("posmails", "posmail");


$form->add_section();

$form->add_edit("sender_name", "Sender Name", NOTNULL, $user["firstname"] . " " . $user["name"]);
$form->add_edit("sender_email", "Sender Email", NOTNULL, $user["email"]);
$form->add_edit("subject", "Subject", NOTNULL);
$form->add_section();
$form->add_comment("Use the strings {user_id} and {password} in your text to be replaced by the user_id and password on submission.");

$form->add_multiline("text", "Text*", 20, NOTNULL);

$form->add_comment("Attach the following File to the mail message:");
$form->add_upload("attachment", "File", "/files/mailattachments", NOTNULL);

$form->add_button("Send", "SendMail to Brand Managers");
$form->add_button("Send_Test", "Send Test Mail to Sender");


$form->populate();
$form->process();

if ($form->button("Send_Test"))
{
    
	if($form->validate())
	{
		$attachment = $form->value("attachment");
		$filename = $_SERVER["DOCUMENT_ROOT"] . $attachment;
		$handle = fopen ($filename, "r");
		$attachment_data = fread ($handle, filesize ($filename));
		fclose ($handle);
		
		$mail = new Mail();
		$mail->set_subject($form->value("subject"));
		$mail->set_sender($form->value("sender_email"), $form->value("sender_name"));
		$mail->add_recipient($form->value("sender_email"));
		
		$bodytext = "Dear " . $user["firstname"] . "\n\n";
		$bodytext .= $form->value("text");
		$bodytext = str_replace("{user_id}", $user["user_id"], $bodytext);
		$bodytext = str_replace("{password}", $user["password"], $bodytext);
		$mail->add_text($bodytext);

		
		$pathparts = explode("/", $attachment);
		if(isset($pathparts[3]))
		{
			$fileparts = explode("\.", $pathparts[3]);

			$mail->add_attachment($pathparts[3], $attachment_data, "application/" . $mimetype[$fileparts[1]]);
		}
		
		$mail->send();

		$form->message("The test mail was sent to " . $form->value("sender_email") .".");
	}
}
elseif ($form->button("Send"))
{
	
	if($form->validate())
	{
		
		$attachment = $form->value("attachment");
		$filename = $_SERVER["DOCUMENT_ROOT"] . $attachment;
		$handle = fopen ($filename, "r");
		$attachment_data = fread ($handle, filesize ($filename));
		fclose ($handle);

		$recipients = "";
		
		$sql = "select user_role_user " . 
			   "from user_roles " . 
			   "left join users on user_id =  user_role_user " . 
			   "where user_active = 1 and user_role_role = 15 " . 
			   "order by user_name ";


		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$recipient = get_user($row["user_role_user"]);
			
			$mail = new Mail();
			$mail->set_subject($form->value("subject"));
			$mail->set_sender($form->value("sender_email"), $form->value("sender_name"));
			
			$mail->add_recipient($recipient["email"]);
			$mail->add_cc($form->value("sender_email"));

			$bodytext = "Dear " . $recipient["firstname"] . "\n\n";
			$bodytext .= $form->value("text");
			$bodytext = str_replace("{user_id}", $recipient["user_id"], $bodytext);
			$bodytext = str_replace("{password}", $recipient["password"], $bodytext);
			$mail->add_text($bodytext);

			$pathparts = explode("/", $attachment);
			if(isset($pathparts[3]))
			{
				$fileparts = explode("\.", $pathparts[3]);

				$mail->add_attachment($pathparts[3], $attachment_data, "application/" . $mimetype[$fileparts[1]]);
			}
			
			$result = $mail->send();

			if($result == 1)
			{
				$pos_mail_fields = array();
				$pos_mail_values = array();

				$pos_mail_fields[] = "posmail_sender_email";
				$pos_mail_values[] = dbquote($form->value("sender_email"));;

				$pos_mail_fields[] = "posmail_recipeint_email";
				$pos_mail_values[] = dbquote($recipient["email"]);

				$pos_mail_fields[] = "posmail_subject";
				$pos_mail_values[] = dbquote($form->value("subject"));

				$pos_mail_fields[] = "posmail_file";
				$pos_mail_values[] = dbquote($form->value("attachment"));

				$pos_mail_fields[] = "posmail_text";
				$text = str_replace("\n\r", "<br>" , $bodytext);
				$text = str_replace("\n", "<br>" , $text);
				$text = str_replace("\r", "<br>" , $text);
				
				$pos_mail_values[] = dbquote($text);

				$pos_mail_fields[] = "date_created";
				$pos_mail_values[] = "current_timestamp";

				$pos_mail_fields[] = "user_created";
				$pos_mail_values[] = dbquote($_SESSION["user_login"]);


				$sql = "insert into posmails (" . join(", ", $pos_mail_fields) . ") values (" . join(", ", $pos_mail_values) . ")";
				mysql_query($sql) or dberror($sql);

				$recipients .= $recipient["name"] . " " . $recipient["firstname"] . "\n";
			}
		
		}

		$form->message("The mail was sent to the following brand managers:\n\n" . $recipients);
	}
}

$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title("Send Mail to Brand Managers");
$form->render();


$page->footer();


?>