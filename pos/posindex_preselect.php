<?php
/********************************************************************

    posindex_preselect.php

    Preselection of POS List

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_use_posindex");
set_referer("posindex.php");



$legal_type_filter = '';
if(has_access("has_access_to_retail_pos_only")) {
	$legal_type_filter = " where posaddress_ownertype in (1) ";
}

if($legal_type_filter and has_access("has_access_to_wholesale_pos")) {
	$legal_type_filter = " and posaddress_ownertype in (2,6) ";
}
else
{
	$legal_type_filter = " where posaddress_ownertype in (2,6) ";
}


$postype_filter = array();
$postype_filter["all"] = "All";
$sql = "select DISTINCT posaddress_store_postype, postype_name " . 
       "from posaddresses " . 
	   " left join postypes on postype_id = posaddress_store_postype " . $legal_type_filter . 
	   " order by postype_name";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$postype_filter[$row["posaddress_store_postype"]] = $row["postype_name"];
}


$states = array();
$states[1] = "Operating POS locations only";
$states[2] = "Closed POS locations only";



$legal_type_filter = '';
if(has_access("has_access_to_retail_pos_only")) {
	$legal_type_filter = " and posaddress_ownertype in (1) ";
}

if(has_access("has_access_to_wholesale_pos")) {
	$legal_type_filter = " and posaddress_ownertype in (2,6) ";
}

//compose list
if(has_access("can_view_posindex") or has_access("can_edit_posindex"))
{
	
	
	$sql = "select DISTINCT country_id, country_name " .
		   "from posaddresses " . 
		   "left join countries on posaddress_country = country_id " . $legal_type_filter .
		   "where country_name is not null " . 
		   "order by country_name";
}
else
{

	$user = get_user(user_id());

	/*
	$sql = "select DISTINCT posaddress_country " .
		   "from posaddresses " . 
		   "where posaddress_client_id = " . $user["address"];

	$num_of_countries = 0;
	$res = mysql_query($sql) or dberror($sql);
	$num_rows = mysql_num_rows($res);

	
	if($num_rows < 2)
	{
		if($row = mysql_fetch_assoc($res))
		{
			redirect("posindex.php?country=" . $row["posaddress_country"]);
		}
	}
	*/


	$country_filter = "";
	$tmp = array();
	$sql = "select * from country_access " .
		   "where country_access_user = " . user_id();


	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{            
		$tmp[] = $row["country_access_country"];
	}

	if(count($tmp) > 0) {
		$country_filter = " country_id IN (" . implode(",", $tmp) . ") ";
	}

	

	if($country_filter == "")
	{
		$sql = "select DISTINCT country_id, country_name " .
			   "from posaddresses " . 
			   "left join countries on posaddress_country = country_id " .
			   "left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
			   "left join postypes on postype_id = posaddress_store_postype " .
			   "where country_name is not null " .
			   " and posaddress_client_id = " . $user["address"] . $legal_type_filter .
			   //" and (postype_id <> 2 or (postype_id = 2 and project_costtype_id = 1)) " . 
			   " order by country_name";
	}
	else
	{
		$sql = "select DISTINCT country_id, country_name " .
			   "from posaddresses " . 
			   "left join countries on posaddress_country = country_id " .
			   "left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
			   "left join postypes on postype_id = posaddress_store_postype " .
			   "where country_name is not null " . $legal_type_filter .
			   " and " . $country_filter . 
			   //" and (postype_id <> 2 or (postype_id = 2 and project_costtype_id = 1)) " . 
			   " order by country_name";
	}


	
}

if(param("country"))
{
	$sql_p = "select province_id, province_canton from provinces  " . 
		     "where province_country = " . dbquote(param("country")) .
		     " order by province_canton";
}

$form = new Form("posaddresses", "posaddress");

$form->add_section("Country Selection");

$form->add_list("country", "Country",$sql, SUBMIT);
if(param("country"))
{
	$form->add_list("province", "Province",$sql_p);
}
else
{
	$form->add_hidden("province");
}

$selected = 'all';
if(param('ltf'))
{
	$selected = param('ltf');
}

$form->add_list("ltf", "POS Type Filter",$postype_filter, 0, $selected);

$form->add_list("states", "Filter",$states);

$form->add_button("show_pos", "Show List");

$form->populate();
$form->process();


if($form->button("show_pos"))
{
	redirect("posindex.php?country=" . $form->value("country") . "&province=" . $form->value("province"). "&ostate=" . $form->value("states") . '&let=' . param('let') . "&ltf=" . $form->value("ltf"));
}

$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title("POS Index");
$form->render();
?>

	<script type="text/javascript">
		
		document.onkeydown = process_key;
		
		function process_key(e)
		{
		  if( !e ) 
		  {
			if( window.event ) 
			{
			  e = window.event;
			} 
			else 
			{
			  return;
			}
		  }

		  if(e.keyCode==13)
		  {
			  button('show_pos');
		  }
		}
	</script>

	<?php

$page->footer();

?>
