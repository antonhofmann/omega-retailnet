<?php
/********************************************************************

    query_generator_fields_selector.php

    Enter selected fields for the query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2009-01-11
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2009-01-11
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../include/page_modal.php";

check_access("can_query_posindex");
require_once "include/query_get_functions.php";

/********************************************************************
    prepare all data needed
*********************************************************************/
$query_id = param("query_id");

$fields = array();
$selected_fields = array();
$oldfields = array();

$sql = "select posquery_fields from posqueries " .
	   "where posquery_id = " . $query_id;

if(param("t") == 'pl') // pos location
{
	$db_info = query_posaddress_fields();
	$fields = $db_info["fields"];
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["posquery_fields"]);
		if(is_array($oldfields) and array_key_exists("pl", $oldfields))
		{
			$selected_fields = $oldfields["pl"];
		}
	}
}
elseif(param("t") == 'le') // pos leases
{
	$db_info = query_posleases_fields();
	$fields = $db_info["fields"];
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["posquery_fields"]);
		if(is_array($oldfields) and array_key_exists("le", $oldfields))
		{
			$selected_fields = $oldfields["le"];
		}
	}
}
elseif(param("t") == 'clo') // pos closures
{
	$db_info = query_posclosings_fields();
	$fields = $db_info["fields"];
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["posquery_fields"]);
		if(is_array($oldfields) and array_key_exists("clo", $oldfields))
		{
			$selected_fields = $oldfields["clo"];
		}
	}
}
elseif(param("t") == 'dcs') // distibution channel
{
	$db_info = query_distribution_channel_fields();
	$fields = $db_info["fields"];
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["posquery_fields"]);
		if(is_array($oldfields) and array_key_exists("dcs", $oldfields))
		{
			$selected_fields = $oldfields["dcs"];
		}
	}
}
elseif(param("t") == 'cl') // client address
{
	$db_info = query_clientaddress_fields();
	$fields = $db_info["fields"];
	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["posquery_fields"]);
		if(is_array($oldfields) and array_key_exists("cl", $oldfields))
		{
			$selected_fields = $oldfields["cl"];
		}
	}
}
elseif(param("t") == 'fr') // franchisee address
{
	$db_info = query_franchiseeaddress_fields();
	$fields = $db_info["fields"];
	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["posquery_fields"]);
		if(is_array($oldfields) and array_key_exists("fr", $oldfields))
		{
			$selected_fields = $oldfields["fr"];
		}
	}
}
elseif(param("t") == 'po') // posorders
{
	$db_info = query_posorder_fields();
	$fields = $db_info["fields"];
	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["posquery_fields"]);
		if(is_array($oldfields) and array_key_exists("po", $oldfields))
		{
			$selected_fields = $oldfields["po"];
		}
	}
}
elseif(param("t") == 'lpo') // latest posorder
{
	$db_info = query_latest_posorder_fields();
	$fields = $db_info["fields"];
	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["posquery_fields"]);
		if(is_array($oldfields) and array_key_exists("lpo", $oldfields))
		{
			$selected_fields = $oldfields["lpo"];
		}
	}
}
elseif(param("t") == 'in') // intangibles
{
	$db_info = query_intangible_fields();
	$fields = $db_info["fields"];
	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["posquery_fields"]);
		if(is_array($oldfields) and array_key_exists("in", $oldfields))
		{
			$selected_fields = $oldfields["in"];
		}
	}
}
elseif(param("t") == 'stat') // statistics
{
	$db_info = query_statistics_fields();
	$fields = $db_info["fields"];
	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["posquery_fields"]);
		if(is_array($oldfields) and array_key_exists("stat", $oldfields))
		{
			$selected_fields = $oldfields["stat"];
		}
	}
}



if(!$selected_fields)
{
	$selected_fields = array();
}

/********************************************************************
    save data
*********************************************************************/
if(param("save_form"))
{
	$oldfields["pl"] = array();
	$oldfields["cl"] = array();
	$oldfields["fr"] = array();
	$oldfields["po"] = array();
	$oldfields["lpo"] = array();
	$oldfields["in"] = array();
	$oldfields["le"] = array();
	$oldfields["clo"] = array();
	$oldfields["dcs"] = array();

	//get Query Fields
	$sql = "select posquery_fields from posqueries " .
		   "where posquery_id = " . $query_id;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["posquery_fields"]);
	}
	
	if(!$oldfields["pl"])
	{
		$oldfields["pl"] = array();
	}
	if(!$oldfields["cl"])
	{
		$oldfields["cl"] = array();
	}
	if(!$oldfields["fr"])
	{
		$oldfields["fr"] = array();
	}
	if(!$oldfields["po"])
	{
		$oldfields["po"] = array();
	}
	if(!$oldfields["lpo"])
	{
		$oldfields["lpo"] = array();
	}
	if(!$oldfields["in"])
	{
		$oldfields["in"] = array();
	}
	if(!$oldfields["le"])
	{
		$oldfields["le"] = array();
	}
	if(!$oldfields["clo"])
	{
		$oldfields["clo"] = array();
	}
	if(!$oldfields["dcs"])
	{
		$oldfields["dcs"] = array();
	}
	if(!$oldfields["stat"])
	{
		$oldfields["stat"] = array();
	}
	
	$new_selected_fields = array();
	foreach($fields as $key=>$caption)
	{
		$post_key = str_replace(".", "_", $key);
		if($_POST[$post_key])
		{
			$new_selected_fields[$key] = $caption;
		}
	}
	
	if(param("t") == "pl")
	{
		$updatefields["pl"] = $new_selected_fields;
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["fr"] = $oldfields["fr"];
		$updatefields["po"] = $oldfields["po"];
		$updatefields["lpo"] = $oldfields["lpo"];
		$updatefields["in"] = $oldfields["in"];
		$updatefields["le"] = $oldfields["le"];
		$updatefields["clo"] = $oldfields["clo"];
		$updatefields["dcs"] = $oldfields["dcs"];
		$updatefields["stat"] = $oldfields["stat"];
	}
	elseif(param("t") == "cl")
	{
		$updatefields["pl"] = $oldfields["pl"];
		$updatefields["cl"] = $new_selected_fields;
		$updatefields["fr"] = $oldfields["fr"];
		$updatefields["po"] = $oldfields["po"];
		$updatefields["lpo"] = $oldfields["lpo"];
		$updatefields["in"] = $oldfields["in"];
		$updatefields["le"] = $oldfields["le"];
		$updatefields["clo"] = $oldfields["clo"];
		$updatefields["dcs"] = $oldfields["dcs"];
		$updatefields["stat"] = $oldfields["stat"];
	}
	elseif(param("t") == "fr")
	{
		$updatefields["pl"] = $oldfields["pl"];
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["fr"] = $new_selected_fields;
		$updatefields["po"] = $oldfields["po"];
		$updatefields["lpo"] = $oldfields["lpo"];
		$updatefields["in"] = $oldfields["in"];
		$updatefields["le"] = $oldfields["le"];
		$updatefields["clo"] = $oldfields["clo"];
		$updatefields["dcs"] = $oldfields["dcs"];
		$updatefields["stat"] = $oldfields["stat"];
	}
	elseif(param("t") == "po")
	{
		$updatefields["pl"] = $oldfields["pl"];
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["fr"] = $oldfields["fr"];
		$updatefields["po"] = $new_selected_fields;
		$updatefields["lpo"] = $oldfields["lpo"];
		$updatefields["in"] = $oldfields["in"];
		$updatefields["le"] = $oldfields["le"];
		$updatefields["clo"] = $oldfields["clo"];
		$updatefields["dcs"] = $oldfields["dcs"];
		$updatefields["stat"] = $oldfields["stat"];
	}
	elseif(param("t") == "lpo")
	{
		$updatefields["pl"] = $oldfields["pl"];
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["fr"] = $oldfields["fr"];
		$updatefields["po"] = $oldfields["po"];
		$updatefields["lpo"] = $new_selected_fields;
		$updatefields["in"] = $oldfields["in"];
		$updatefields["le"] = $oldfields["le"];
		$updatefields["clo"] = $oldfields["clo"];
		$updatefields["dcs"] = $oldfields["dcs"];
		$updatefields["stat"] = $oldfields["stat"];
	}
	elseif(param("t") == "in")
	{
		$updatefields["pl"] = $oldfields["pl"];
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["fr"] = $oldfields["fr"];
		$updatefields["po"] = $oldfields["po"];
		$updatefields["lpo"] = $oldfields["lpo"];
		$updatefields["in"] = $new_selected_fields;
		$updatefields["le"] = $oldfields["le"];
		$updatefields["clo"] = $oldfields["clo"];
		$updatefields["dcs"] = $oldfields["dcs"];
		$updatefields["stat"] = $oldfields["stat"];
	}
	elseif(param("t") == "le")
	{
		$updatefields["pl"] = $oldfields["pl"];
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["fr"] = $oldfields["fr"];
		$updatefields["po"] = $oldfields["po"];
		$updatefields["lpo"] = $oldfields["lpo"];
		$updatefields["in"] = $oldfields["in"];
		$updatefields["le"] = $new_selected_fields;
		$updatefields["clo"] = $oldfields["clo"];
		$updatefields["dcs"] = $oldfields["dcs"];
		$updatefields["stat"] = $oldfields["stat"];
	}
	elseif(param("t") == "clo")
	{
		$updatefields["pl"] = $oldfields["pl"];
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["fr"] = $oldfields["fr"];
		$updatefields["po"] = $oldfields["po"];
		$updatefields["lpo"] = $oldfields["lpo"];
		$updatefields["in"] = $oldfields["in"];
		$updatefields["le"] = $oldfields["le"];
		$updatefields["clo"] = $new_selected_fields;
		$updatefields["dcs"] = $oldfields["dcs"];
		$updatefields["stat"] = $oldfields["stat"];
	}
	elseif(param("t") == "dcs")
	{
		$updatefields["pl"] = $oldfields["pl"];
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["fr"] = $oldfields["fr"];
		$updatefields["po"] = $oldfields["po"];
		$updatefields["lpo"] = $oldfields["lpo"];
		$updatefields["in"] = $oldfields["in"];
		$updatefields["le"] = $oldfields["le"];
		$updatefields["clo"] = $oldfields["clo"];
		$updatefields["dcs"] = $new_selected_fields;
		$updatefields["stat"] = $oldfields["stat"];
	}
	elseif(param("t") == "stat")
	{
		$updatefields["pl"] = $oldfields["pl"];
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["fr"] = $oldfields["fr"];
		$updatefields["po"] = $oldfields["po"];
		$updatefields["lpo"] = $oldfields["lpo"];
		$updatefields["in"] = $oldfields["in"];
		$updatefields["le"] = $oldfields["le"];
		$updatefields["clo"] = $oldfields["clo"];
		$updatefields["dcs"] = $oldfields["dcs"];
		$updatefields["stat"] = $new_selected_fields;
	}


	$sql = "update posqueries " . 
		   "set posquery_fields = " . dbquote(serialize($updatefields)) . " " . 
		   " where posquery_id = " . param("query_id");

	$result = mysql_query($sql) or dberror($sql);

	//update field order and sortorder
	$sql = "select posquery_fields, posquery_field_order, posquery_order from posqueries " .
		   "where posquery_id = " . $query_id;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields["pl"] = array();
		$oldfields["cl"] = array();
		$oldfields["fr"] = array();
		$oldfields["po"] = array();
		$oldfields["lpo"] = array();
		$oldfields["in"] = array();
		$oldfields["le"] = array();
		$oldfields["clo"] = array();
		$oldfields["dcs"] = array();
		$oldfields["stat"] = array();

		$oldfields = unserialize($row["posquery_fields"]);
		$oldfields = array_merge($oldfields["pl"], $oldfields["cl"], $oldfields["fr"], $oldfields["po"], $oldfields["lpo"], $oldfields["in"], $oldfields["le"], $oldfields["clo"], $oldfields["dcs"], $oldfields["stat"]);

		$old_field_order = decode_field_array($row["posquery_field_order"]);
		$old_query_order = decode_field_array($row["posquery_order"]);

		//remove fields not present anymore
		foreach($old_field_order as $key=>$fieldname)
		{
			if(!array_key_exists($fieldname, $oldfields))
			{
				foreach($old_field_order as $key=>$value)
				{
					if($value == $fieldname)
					{
						unset($old_field_order[$key]);
					}
				}

				foreach($old_query_order as $key=>$value)
				{
					if($value == $fieldname)
					{
						unset($old_query_order[$key]);
					}
				}
			}
		}

		//add new elements at the end of the arrays
		foreach($oldfields as $fieldname=>$caption)
		{
			if(!in_array($fieldname, $old_field_order))
			{
				$old_field_order[] = $fieldname;
			}
			if(!in_array($fieldname, $old_query_order))
			{
				$old_query_order[] = $fieldname;
			}
		}

		$field_order = encode_field_array($old_field_order);
		$query_order = encode_field_array($old_query_order);
		
		//update query order and field order
		$sql = "Update posqueries SET " . 
			   "posquery_field_order = " . dbquote($field_order) . ", " . 
			   "posquery_order = " . dbquote($query_order) . ", " . 
			   "date_modified = " . dbquote(date("Y-m-d H:s:i")) . ", " . 
			   "user_modified = " . dbquote(user_login()) .
		       " where posquery_id = " . $query_id;

		$result = mysql_query($sql) or dberror($sql);
		

	}
}

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new form("posqueries", "Field Selector");
$form->add_hidden("save_form", "1");
$form->add_hidden("query_id", $query_id);
$form->add_hidden("t", param("t"));
$form->add_label("L1", "", "", "Select the Fields to be included in the Query");

foreach($fields as $key=>$caption)
{
	if(array_key_exists($key,$selected_fields))
	{
		$form->add_checkbox($key, $caption, true);
	}
	else
	{
		$form->add_checkbox($key, $caption, false);
	}
	
}

$form->add_input_submit("submit", "Save Selection", 0);

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Process buttons
*********************************************************************/ 

/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page_Modal("query_generator");

//require "include/benchmark_page_actions.php";

$page->header();
$page->title("Field Selector");

$form->render();

if(param("save_form"))
{
?>

<script languege="javascript">
var back_link = "query_generator_fields.php?query_id=<?php echo $query_id;?>"; 
$.nyroModalRemove();
</script>

<?php
}
$page->footer();
