<?php
/********************************************************************

    posindex_openinghr.php

    Edit/Show opening hours of the pos.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

check_access("can_use_posindex");


if(param("pos_id"))
{
	register_param("pos_id");
	param("id", param("pos_id"));
}
else
{
	register_param("pos_id");
	param("pos_id", id());
}

if(param("pos_id") > 0)
{
	$pos = get_poslocation(param("pos_id"), "posaddresses");
}


//get opening hours
$openinghrs = array();
$sql = "select weekday_name, openinghrsfrom.openinghr_time_24 as f24, openinghrsfrom.openinghr_time_12 as f12, " . 
       " openinghrsto.openinghr_time_24 as t24, openinghrsto.openinghr_time_12 as t12 " . 
       " from posopeninghrs " .
	   " left join weekdays on weekday_id = posopeninghr_weekday_id " .
	   " left join openinghrs as openinghrsfrom on openinghrsfrom.openinghr_id = posopeninghr_from_openinghr_id " .
	   " left join openinghrs as openinghrsto on openinghrsto.openinghr_id = posopeninghr_to_openinghr_id " .
	   " where posopeninghr_posaddress_id = " . param("pos_id");

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($pos["country_timeformat"] == 12)
	{
		if(array_key_exists($row["weekday_name"], $openinghrs))
		{
			$openinghrs[$row["weekday_name"]] = $openinghrs[$row["weekday_name"]] . ", " . $row["f12"] . "-" . $row["t12"];
		}
		else
		{
			$openinghrs[$row["weekday_name"]] = $row["f12"] . "-" . $row["t12"];
		}
	}
	else
	{
		if(array_key_exists($row["weekday_name"], $openinghrs))
		{
			$openinghrs[$row["weekday_name"]] = $openinghrs[$row["weekday_name"]] . ", " . $row["f24"] . "-" . $row["t24"];
		}
		else
		{
			$openinghrs[$row["weekday_name"]] = $row["f24"] . "-" . $row["t24"];
		}
	}
}

$closinghrs = "";
$sql = "select posclosinghr_text " . 
       " from posclosinghrs " . 
	   " where posclosinghr_posaddress_id = " .  param("pos_id");

$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	$closinghrs = $row["posclosinghr_text"];
	$closinghrs = str_replace("\r\n", "<br />", $closinghrs);
	$closinghrs = str_replace("\n", "<br />", $closinghrs);
	$closinghrs = str_replace("\r", "<br />", $closinghrs);
}



// Build form
$form = new Form("posaddresses", "posaddress");

$form->add_hidden("country", param("country"));
$form->add_hidden("ltf", param("ltf"));
$form->add_hidden("let", param("let"));
$form->add_hidden("ostate", param("ostate"));

$form->add_lookup("posaddress_client_id", "Client", "addresses", "concat(address_company, ', ', address_place, ' [', address_shortcut, ']') as company", 0, $pos["posaddress_client_id"]);

$form->add_section("Address Data");
$form->add_label("posaddress_name", "POS Name");

$form->add_label("posaddress_address", "Address*", NOTNULL);
$form->add_label("posaddress_address2", "Address 2");
$form->add_label("posaddress_zip", "Zip*", NOTNULL);
$form->add_label("posaddress_place", "City*", NOTNULL);
$form->add_lookup("posaddress_country", "Country", "countries", "country_name", 0, $pos["posaddress_country"]);


$form->add_section("Opening Hours");
foreach($openinghrs as $weekday=>$openinghr)
{
	$form->add_label($weekday, $weekday, 0, $openinghr);
}

$form->add_section("Days Closed in General");
$form->add_label("closed", "POS closed on", RENDER_HTML, $closinghrs);



if(has_access("can_edit_pos_opening_hours")) 
{
	$form->add_button("edit", "Edit Opening Hours");
}

$form->add_button("back", "Back to POS List");

// Populate form and process button clicks

$form->populate();
$form->process();



if($form->button("back"))
{
	redirect("posindex.php?country=" . param("country") . '&let=' . param('let') . "&ltf=" . param("ltf"). "&ostate=" . param("ostate"));
}


elseif($form->button("edit"))
{
	redirect("posopeninghr.php?id=" . param("pos_id") . "&country=" . param("country") . '&let=' . param('let') . "&ltf=" . param("ltf"). "&ostate=" . param("ostate"));
}



// Render page
$poslocation = get_poslocation(id(), "posaddresses");

$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title("Opening Hours: " . $poslocation["posaddress_name"]);

if(id())
{
	require_once("include/tabs.php");
}

$form->render();


$page->footer();

?>