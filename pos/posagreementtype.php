<?php
/********************************************************************

    posagreementtype.php

    Creation and mutation of POS Type Subclass records.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-08-20
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-20
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");

$form = new Form("agreement_types", "agreement_types");

$form->add_section();
$form->add_edit("agreement_type_name", "Name*", NOTNULL | UNIQUE);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("posagreementtypes");
$page->header();
$page->title(id() ? "Edit Agreement Type" : "Add Agreement Type");
$form->render();
$page->footer();

?>