<?php
/********************************************************************

    loc_possubclass_possubclass.php

    Creation and mutation of POS Type Subclass translations.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");


$possubclass_id = 0;

if(param("possubclass_id"))
{
	$possubclass_id = param("possubclass_id");
}

//get possubclass name
$possubclass_name = "";
$sql = "select possubclass_name from possubclasses where possubclass_id = " . $possubclass_id;
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$possubclass_name = $row["possubclass_name"];
}

$form = new Form("loc_possubclasses", "possubclass");

$form->add_section("Basic Data");
$form->add_hidden("possubclass_id", $possubclass_id);
$form->add_hidden("loc_possubclass_possubclass", $possubclass_id);
$form->add_label("possubclass_name", "POS Type Subclass", 0, $possubclass_name);

$form->add_section("Translation");
$form->add_list("loc_possubclass_language", "Language*", 
    "select language_id, language_name from languages order by language_name", NOTNULL);

$form->add_edit("loc_possubclass_name", "POS Type Subclass*", NOTNULL);

$form->add_button("save", "Save");
$form->add_button("back", "Back");
$form->add_button("delete", "Delete");

$form->populate();
$form->process();

if($form->button("back"))
{
	$link = "loc_possubclass.php?id= " . param("possubclass_id");
	redirect($link);
}
elseif($form->button("save"))
{
	if($form->validate())
	{
		$form->save();
		$link = "loc_possubclass.php?id= " . param("possubclass_id");
		redirect($link);
	}
}
elseif($form->button("delete"))
{
		$sql = "delete from loc_possubclasses where loc_possubclass_id =" . id();
		$result = mysql_query($sql) or dberror($sql);
		$link = "loc_possubclass.php?id= " . param("possubclass_id");
		redirect($link);

}

$page = new Page("locales");
$page->header();
$page->title(id() ? "Edit POS Type Subclass Translation" : "Add POS Type Subclass Translation");
$form->render();
$page->footer();

?>