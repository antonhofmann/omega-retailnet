<?php
/********************************************************************

    loc_areaperception_type_areaperception_type.php

    Creation and mutation of POS area perception types translations.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");


$areaperception_type_id = 0;

if(param("areaperception_type_id"))
{
	$areaperception_type_id = param("areaperception_type_id");
}

//get areaperception_type name
$areaperception_type_name = "";
$sql = "select areaperception_type_name from areaperception_types where areaperception_type_id = " . $areaperception_type_id;
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$areaperception_type_name = $row["areaperception_type_name"];
}

$form = new Form("loc_areaperception_types", "areaperception_type");

$form->add_section("Basic Data");
$form->add_hidden("areaperception_type_id", $areaperception_type_id);
$form->add_hidden("loc_areaperception_type_type", $areaperception_type_id);
$form->add_label("areaperception_type_name", "POS Area Perception Type", 0, $areaperception_type_name);

$form->add_section("Translation");
$form->add_list("loc_areaperception_type_language", "Language*", 
    "select language_id, language_name from languages order by language_name", NOTNULL);

$form->add_edit("loc_areaperception_type_name", "POS Area Perception Type*", NOTNULL);

$form->add_button("save", "Save");
$form->add_button("back", "Back");
$form->add_button("delete", "Delete");

$form->populate();
$form->process();

if($form->button("back"))
{
	$link = "loc_areaperception_type.php?id= " . param("areaperception_type_id");
	redirect($link);
}
elseif($form->button("save"))
{
	if($form->validate())
	{
		$form->save();
		$link = "loc_areaperception_type.php?id= " . param("areaperception_type_id");
		redirect($link);
	}
}
elseif($form->button("delete"))
{
		$sql = "delete from loc_areaperception_types where loc_areaperception_type_id =" . id();
		$result = mysql_query($sql) or dberror($sql);
		$link = "loc_areaperception_type.php?id= " . param("areaperception_type_id");
		redirect($link);

}

$page = new Page("locales");
$page->header();
$page->title(id() ? "Edit POS Area Perception Type Translation" : "Add POS Area Perception Type Translation");
$form->render();
$page->footer();

?>