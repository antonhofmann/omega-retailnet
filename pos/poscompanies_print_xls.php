<?php
/********************************************************************

    poscompanies_print_xls.php

    Generate Excel-File of POS Companies

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-06-09
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-06-09
    Version:        1.1.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_use_posindex");

require_once "../include/xls/Writer.php";
require_once "include/get_functions.php";

$user = get_user(user_id());

/********************************************************************
    prepare Data Needed
*********************************************************************/

$co = param("co"); // Countries
$address_filter = param("address_filter");

//get product lines

$header = "";
$header = "POS Companies";

/********************************************************************
    prepare Data
*********************************************************************/
$filter = "";

if($co)
{
    $filter =  " and (address_country IN (" . $co . "))";
}


if($address_filter) {
	
	if($address_filter == 'f') {
		$filter .= " and (address_canbefranchisee = 1 or address_canbefranchisee_worldwide = 1)";
		$header .= ' - Franchisees: ';
	}
	elseif($address_filter == 'o') {
		$filter .= " and address_is_independent_retailer = 1 ";
		$header .= ' - Independent retailers: ';
	}
	else {
		$filter .= " and (address_is_independent_retailer = 1 or address_canbefranchisee = 1 or address_canbefranchisee_worldwide = 1)";
		$header .= ' - All Companies: ';
	}
}
else
{
	$header = "POS Companies: ";
}

$header .= " (" . date("d.m.Y G:i") . ")";

if(has_access("can_view_his_posindex") or has_access("can_edit_his_posindex"))
{
	
	$country_filter = "";
	$tmp = array();
	$sql = "select * from country_access " .
		   "where country_access_user = " . user_id();


	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{            
		$tmp[] = $row["country_access_country"];
	}

	if(count($tmp) > 0) {
		$country_filter = " country_id IN (" . implode(",", $tmp) . ") ";
	}

	if($country_filter) 
	{
		$sql_d = "select * " .
				 "from addresses " . 
				 "left join countries on address_country = country_id " .
				 "left join places on place_id = address_place_id " .
				 "left join provinces on province_id = place_province " .
				 "where address_showinposindex = 1 and address_active = 1 " . $filter . 
				 " order by country_name, address_company";
	}
	else
	{
		$sql_d = "select * " .
				 "from addresses " . 
				 "left join countries on address_country = country_id " .
				 "left join places on place_id = address_place_id " .
				 "left join provinces on province_id = place_province " .
				 "where address_showinposindex = 1 and address_active = 1 " . $filter . 
				 " and address_parent = " . $user["address"] . 
				 " order by country_name, address_company";
	}
}
else
{
	$sql_d = "select * " .
			 "from addresses " . 
			 "left join countries on address_country = country_id " . 
		     "left join places on place_id = address_place_id " .
		     "left join provinces on province_id = place_province " .
			 "where address_showinposindex = 1 and address_active = 1 " . $filter . 
			 " order by country_name, address_company";

}

/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$filename = "pos_companies_" . date("Ymd") . ".xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename);
$xls->setVersion(8);

$sheet = $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

//formats
$header_row = $xls->addFormat();
$header_row->setSize(10);
$header_row->setAlign('left');
$header_row->setBold();


$f_normal = $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);

$f_normal_bold = $xls->addFormat();
$f_normal_bold->setSize(8);
$f_normal_bold->setAlign('left');
$f_normal_bold->setBorder(1);
$f_normal_bold->setBold();


$f_number = $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_center = $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption = $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_used = $xls->addFormat();
$f_used->setSize(8);
$f_used->setBorder(1);
$f_used->setPattern(2);
$f_used->setBgColor('yellow');



//captions
$captions = array();
//$captions[] = "Nr";
//$captions[] = "Geographical Region";
$captions[] = "Country";
$captions[] = "Company";
$captions[] = "Company 2";
$captions[] = "Address 1";
$captions[] = "Address 2";
$captions[] = "Province";
$captions[] = "Zip";
$captions[] = "City";
$captions[] = "Phone";
$captions[] = "Fax";
$captions[] = "Email";
$captions[] = "Website";
$captions[] = "Contact Name";
$captions[] = "Contact Email";


/********************************************************************
    write all captions
*********************************************************************/
$sheet->write(0, 0, $header, $header_row);
$sheet->writeRow(1, 0, $captions, $f_normal_bold);


$row_index = 2;
$cell_index = 0;
$counter = 0;
$col_widths = array();
for($i=0;$i<count($captions);$i++)
{
	$col_widths[$i] = strlen($captions[$i]);
}


$res = mysql_query($sql_d) or dberror($sql_d);
while ($row = mysql_fetch_assoc($res))
{

	$sheet->write($row_index, $cell_index, $row["country_name"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["country_name"]))
	{
		$col_widths[$cell_index] = strlen($row["country_name"]);
	}
	$cell_index++;

	
	$sheet->write($row_index, $cell_index, $row["address_company"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_company"]))
	{
		$col_widths[$cell_index] = strlen($row["address_company"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["address_company2"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_company2"]))
	{
		$col_widths[$cell_index] = strlen($row["address_company2"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["address_address"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_address"]))
	{
		$col_widths[$cell_index] = strlen($row["address_address"]);
	}
	$cell_index++;

	
	$sheet->write($row_index, $cell_index, $row["address_address2"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_address2"]))
	{
		$col_widths[$cell_index] = strlen($row["address_address2"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["province_canton"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["province_canton"]))
	{
		$col_widths[$cell_index] = strlen($row["province_canton"]);
	}
	$cell_index++;

	
	$sheet->write($row_index, $cell_index, $row["address_zip"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_zip"]))
	{
		$col_widths[$cell_index] = strlen($row["address_zip"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["place_name"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["place_name"]))
	{
		$col_widths[$cell_index] = strlen($row["place_name"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["address_phone"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_phone"]))
	{
		$col_widths[$cell_index] = strlen($row["address_phone"]);
	}
	$cell_index++;


	$sheet->write($row_index, $cell_index, $row["address_fax"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_fax"]))
	{
		$col_widths[$cell_index] = strlen($row["address_fax"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["address_email"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_email"]))
	{
		$col_widths[$cell_index] = strlen($row["address_email"]);
	}
	$cell_index++;
	
	$sheet->write($row_index, $cell_index, str_replace("http://", "", $row["address_website"]), $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_website"]))
	{
		$col_widths[$cell_index] = strlen($row["address_website"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, str_replace("http://", "", $row["address_contact_name"]), $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_contact_name"]))
	{
		$col_widths[$cell_index] = strlen($row["address_contact_name"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["address_contact_email"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_contact_email"]))
	{
		$col_widths[$cell_index] = strlen($row["address_contact_email"]);
	}


	
	$cell_index++;


		
	$cell_index = 0;
	$row_index++;

}

for($i=0;$i<count($captions);$i++)
{
	$sheet->setColumn($i, $i, $col_widths[$i]);
}


$xls->close(); 

?>