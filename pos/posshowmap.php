<?php
/********************************************************************

    posshowmap.php

    Lists of addresses (POS)

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_use_posindex");

if(param("id"))
{
	$sql = "select * from posaddresses where posaddress_id = " . param("id");
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$posname = $row["posaddress_name"];
		$address = $row["posaddress_address"];
		$zip = $row["posaddress_zip"];
		$place = $row["posaddress_place"];
		$latitude = $row["posaddress_google_lat"];
		$longitude = $row["posaddress_google_long"];

		$shop = $posname . ", " . $address . ", " . $zip  . " " . $place;
	}
}


$api_key = GOOGLE_API_KEY;

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title><?php echo $shop;?></title>
    <script src="http://maps.google.com/maps?file=api&amp;v=2.95&amp;key=<?php echo $api_key;?>"
            type="text/javascript">
	</script>
    <script type="text/javascript">

    function initialize() {
      if (GBrowserIsCompatible()) {
        var map = new GMap2(document.getElementById("map_canvas"));
        map.setCenter(new GLatLng(<?php echo $latitude;?>, <?php echo $longitude;?>), 13);
		
		map.addControl(new GLargeMapControl());

        map.addControl(new GMapTypeControl());

		var icon = new GIcon(); 
		icon.image = 'pictures/pin_green.png';
		icon.iconSize = new GSize(32, 32);
		icon.iconAnchor = new GPoint(6, 20);
		icon.infoWindowAnchor = new GPoint(10, 7);

		var latlng = new GLatLng(<?php echo $latitude;?>, <?php echo $longitude;?>, true);
        map.addOverlay(new GMarker(latlng, icon));
      }
    }

    </script>
  </head>

  <body onload="initialize()" onunload="GUnload()" style="background-color:#000000">
    <div id="map_canvas" style="width:800px; height: 600px"></div>
  </body>
</html>
