<?php
/********************************************************************

    possheet_pdf.php

    Creat a PDF of the POS Data

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";
check_access("can_use_posindex");


$result = update_posdata_from_posorders(param("id"));

/********************************************************************
    prepare all data needed
*********************************************************************/

$system_currency = get_system_currency_fields();

$sql = "select *, " . 
       "IF(TIMESTAMPDIFF(MONTH,posaddress_fagrstart,posaddress_fagrend) >0, TIMESTAMPDIFF(MONTH,posaddress_fagrstart,posaddress_fagrend), 'n.a.') as dmonths " . 
	   "from posaddresses " .
       "left join project_costtypes on project_costtype_id = posaddress_ownertype " .
	   "left join postypes on postype_id = posaddress_store_postype " .
	   "left join possubclasses on possubclass_id = posaddress_store_subclass " .
	   "left join product_lines on product_line_id = posaddress_store_furniture " . 
	   "left join productline_subclasses on productline_subclass_id = posaddress_store_furniture_subclass " . 
       "left join countries on country_id = posaddress_country " .
	   "left join currencies on currency_id = posaddress_takeover_currency " .
	   "left join agreement_types on agreement_type_id = posaddress_fagagreement_type " .
       "where posaddress_id = " . param("id");


$res = mysql_query($sql) or dberror($sql);

if ($row = mysql_fetch_assoc($res))
{

	global $page_title;
	$page_title = "POS Overview";

	
	//POS Basic Data
	$posname = $row["posaddress_name"];

	if($row["posaddress_name2"])
    {
		$posname .= ", " . $row["posaddress_name2"];
	}

	$posaddress = $row["posaddress_address"];
	
	if($row["posaddress_address2"])
    {
		$posaddress .= ", " . $row["posaddress_address2"];
	}
	if($row["posaddress_zip"])
    {
		$posaddress .= ", " . $row["posaddress_zip"] . " " . $row["posaddress_place"];
	}
	else
	{
		$posaddress .= ", " . $row["posaddress_place"];
	}
	if($row["country_name"])
    {
		$posaddress .= ", " . $row["country_name"];
	}

	$relocation_of = "";
	//check if there is a relocation project assigned to the POS
	$sql_r = "select project_projectkind, project_relocated_posaddress_id " . 
		     "from posorders " . 
		     "left join projects on project_order = posorder_order " . 
		     "where posorder_posaddress = " . $row["posaddress_id"];

	$res_r = mysql_query($sql_r) or dberror($sql_r);

	if ($row_r = mysql_fetch_assoc($res_r))
	{
		if($row_r["project_projectkind"] == 6 and $row_r["project_relocated_posaddress_id"] > 0) // relocation
		{
			$relocated_pos = get_relocated_pos_info($row_r["project_relocated_posaddress_id"]);
			$relocation_of = "Relocation of " . $relocated_pos["posaddress_name"];
		
		}
	}
	

	$latitude = $row["posaddress_google_lat"];
	$longitude = $row["posaddress_google_long"];

	$poslegaltype = $row["project_costtype_text"];
	$poserno = $row["posaddress_eprepnr"];
	$possapno = $row["posaddress_sapnumber"];
	$posphone = $row["posaddress_phone"];
	$posfax = $row["posaddress_fax"];
	$posemail = $row["posaddress_email"];
	$poswebsite = $row["posaddress_website"];
	$posopeningdate = to_system_date($row["posaddress_store_openingdate"]);
	$posclosingdate = to_system_date($row["posaddress_store_closingdate"]);


	if($row["posaddress_ownertype"] != 6)
	{
		$owner_type_name = "Franchisee";
	}
	else
	{
		$owner_type_name = "Owner Company";
	}


	$posareas = "";
	$sql_i = "select * from posareas " . 
		     "left join posareatypes on posareatype_id = posarea_area " .
	         "where posarea_posaddress = " . param("id");

	$res_i = mysql_query($sql_i) or dberror($sql_i);
	while ($row_i = mysql_fetch_assoc($res_i))
	{
		$posareas .= $row_i["posareatype_name"] . ", ";
	}
	$posareas = substr($posareas,0,strlen($posareas)-2);

	
	$postype = $row["postype_name"];
	$possubclass = $row["possubclass_name"];
	$posfurniture = $row["product_line_name"];
	$posfurniture_subclass = $row["productline_subclass_name"];

	$posgrosssqm = $row["posaddress_store_grosssurface"] ? number_format($row["posaddress_store_grosssurface"], 2) : "";
	$postotalsqm = $row["posaddress_store_totalsurface"] ? number_format($row["posaddress_store_totalsurface"], 2) : "";
	$posretailsqm = $row["posaddress_store_retailarea"] ? number_format($row["posaddress_store_retailarea"], 2) : "";
	$posofficesqm = $row["posaddress_store_backoffice"] ? number_format($row["posaddress_store_backoffice"], 2) : "";
	$posnumfloors = $row["posaddress_store_numfloors"] ? number_format($row["posaddress_store_numfloors"], 0) : "";
	$posfloor1sqm = $row["posaddress_store_floorsurface1"] ? number_format($row["posaddress_store_floorsurface1"], 2) : "";
	$posfloor2sqm = $row["posaddress_store_floorsurface2"] ? number_format($row["posaddress_store_floorsurface2"], 2) : "";
	$posfloor3sqm = $row["posaddress_store_floorsurface3"] ? number_format($row["posaddress_store_floorsurface3"], 2) : "";
	
	$posheads = $row["posaddress_store_headcounts"];
	$posfulltime = $row["posaddress_store_fulltimeeqs"] ? number_format($row["posaddress_store_fulltimeeqs"], 2) : "";


	$posequipments = "";
	$sql_i = "select * from posequipments " . 
		     "left join posequipmenttypes on posequipmenttype_id = posequipment_equipment " .
	         "where posequipment_posaddress = " . param("id");

	$res_i = mysql_query($sql_i) or dberror($sql_i);
	while ($row_i = mysql_fetch_assoc($res_i))
	{
		$posequipments .= $row_i["posequipmenttype_name"] . ", ";
	}
	$posequipments = substr($posequipments,0,strlen($posequipments)-2);

	/*
	if($posequipments)
	{
		$posfurniture .= ", " . $posequipments;
	}
	*/

	$posagreement = $row["agreement_type_name"];

	if($posagreement)
	{
		$posagreement .= ": ";
	}


	if($row["posaddress_fagrsent"] == 1 and $row["posaddress_fagrsigned"] == 1)
	{
		$posagreement .= "Agreement sent and signed";
	}
	elseif($row["posaddress_fagrsent"] == 1)
	{
		$posagreement .= "Agreement sent but not signed";
	}
	elseif($row["posaddress_fagrsigned"] == 1)
	{
		$posagreement .= "Agreement signed but not sent";
	}

	//calculate franchisee agreement duration
	$duration = "";
	
	$duration = $row["dmonths"];
	if($duration != "n.a.")
	{
		$duration = floor($duration/12);

		$months = 1 + $row["dmonths"] - $duration*12;


		if($months == 12)
		{
			$duration = $duration + 1 . " years";
		}
		
		elseif($months > 0)
		{
			$duration = $duration . " years and " . (1 + $row["dmonths"] - $duration*12) . " months";
		}
	}

	$posagreementstart = to_system_date($row["posaddress_fagrstart"]);
	$posagreementend = to_system_date($row["posaddress_fagrend"]);
	$posagreementduration = $posagreementstart . " - " . $posagreementend . " (Duration: " . $duration . ")";
	$posagreementcancellation = to_system_date($row["posaddress_fagcancellation"]);


	$posagreementcomment = $row["posaddress_fag_comment"];

	$postakeoverdate = to_system_date($row["posaddress_takeover_date"]);
	$postakeoveramount = $row["posaddress_takeover_amount"] ? number_format($row["posaddress_takeover_amount"], 2) : "";
	if($postakeoveramount)
	{
		$postakeoveramount .= " " . $row["currency_symbol"];
	}
	$postakeoverdetail = "";
	if($postakeoverdate)
	{
		$postakeoverdetail = $postakeoverdate;
	}
	if($row["posaddress_takeover_amount"] > 0)
	{
		$postakeoverdetail = $postakeoverdate . " for " . $postakeoveramount;
	}


	//Taken Over from Company
	$postakeoverfrom = "";
	$sql_i = "select * from addresses " . 
		     "left join countries on country_id = address_country " .
	         "where address_id = '" . $row["posaddress_takeover_from"] . "'";

	$res_i = mysql_query($sql_i) or dberror($sql_i);
	if ($row_i = mysql_fetch_assoc($res_i))
	{
		$postakeoverfrom = $row_i["address_company"];
		if($row_i["address_address"])
		{
			$postakeoverfrom .= ", " . $row_i["address_address"];
		}
		if($row_i["address_address2"])
		{
			$postakeoverfrom .= ", " . $row_i["address_address2"];
		}
		if($row_i["address_zip"])
		{
			$postakeoverfrom .= ", " . $row_i["address_zip"] . " " . $row_i["address_place"];
		}
		else
		{
			$postakeoverfrom .= ", " . $row_i["address_place"];
		}
		if($row_i["country_name"])
		{
			$postakeoverfrom .= ", " . $row_i["country_name"];
		}
	}

	$postakeovercomment = $row["posaddress_takeover_comment"];
	


	//Client Data
	$posclient = "";
	$sql_i = "select * from addresses " . 
		     "left join countries on country_id = address_country " .
	         "where address_id = " . $row["posaddress_client_id"];

	$res_i = mysql_query($sql_i) or dberror($sql_i);
	if ($row_i = mysql_fetch_assoc($res_i))
	{
		$posclient = $row_i["address_company"];
		if($row_i["address_address"])
		{
			$posclient .= ", " . $row_i["address_address"];
		}
		if($row_i["address_zip"])
		{
			$posclient .= ", " . $row_i["address_zip"] . " " . $row_i["address_place"];
		}
		else
		{
			$posclient .= ", " . $row_i["address_place"];
		}
		if($row_i["country_name"])
		{
			$posclient .= ", " . $row_i["country_name"];
		}
	}


	//Franchisor Data
	$posfranchisor = "";
	$sql_i = "select * from addresses " . 
		     "left join countries on country_id = address_country " .
	         "where address_id = " . $row["posaddress_franchisor_id"];

	$res_i = mysql_query($sql_i) or dberror($sql_i);
	if ($row_i = mysql_fetch_assoc($res_i))
	{
		$posfranchisor = $row_i["address_company"];
		if($row_i["address_address"])
		{
			$posfranchisor .= ", " . $row_i["address_address"];
		}
		if($row_i["address_address2"])
		{
			$posfranchisor .= ", " . $row_i["address_address2"];
		}
		if($row_i["address_zip"])
		{
			$posfranchisor .= ", " . $row_i["address_zip"] . " " . $row_i["address_place"];
		}
		else
		{
			$posfranchisor .= ", " . $row_i["address_place"];
		}
		if($row_i["country_name"])
		{
			$posfranchisor .= ", " . $row_i["country_name"];
		}
	}

	//Franchisee Data
	$posfranchisee = "";
	$sql_i = "select * from addresses " . 
		     "left join countries on country_id = address_country " .
	         "where address_id = " . $row["posaddress_franchisee_id"];

	$res_i = mysql_query($sql_i) or dberror($sql_i);
	if ($row_i = mysql_fetch_assoc($res_i))
	{
		$posfranchisee = $row_i["address_company"];
		if($row_i["address_address"])
		{
			$posfranchisee .= ", " . $row_i["address_address"];
		}
		if($row_i["address_address2"])
		{
			$posfranchisee .= ", " . $row_i["address_address2"];
		}
		if($row_i["address_zip"])
		{
			$posfranchisee .= ", " . $row_i["address_zip"] . " " . $row_i["address_place"];
		}
		else
		{
			$posfranchisee .= ", " . $row_i["address_place"];
		}
		if($row_i["country_name"])
		{
			$posfranchisee .= ", " . $row_i["country_name"];
		}
	}


	//leases
	$leases = array();
	$sql_i = "select poslease_id, poslease_landlord_name, poslease_startdate, poslease_enddate, poslease_type_name, " .
			   "poslease_nexttermin_date, poslease_anual_rent, currency_symbol, " .
			   "DATE_FORMAT(poslease_startdate,'%d.%m.%Y') as start, DATE_FORMAT(poslease_enddate,'%d.%m.%Y') as end, " .
		       "poslease_termination_time, " . 
			   "IF(TIMESTAMPDIFF(MONTH,current_date(),poslease_enddate) >0, TIMESTAMPDIFF(MONTH,current_date(),poslease_enddate), 'expired') as months,  " .
		       "DATE_FORMAT(poslease_extensionoption,'%d.%m.%Y') as extension, " . 
		       "DATE_FORMAT(poslease_exitoption,'%d.%m.%Y') as exitoption, poslease_salespercent " .
			   "from posleases " .
			   "left join poslease_types on poslease_type_id = poslease_lease_type " . 
			   "left join currencies on currency_id = poslease_currency " .
			   "where poslease_posaddress = " . param("id") . 
			   " order by poslease_startdate";

	$res_i = mysql_query($sql_i) or dberror($sql_i);
	while ($row_i = mysql_fetch_assoc($res_i))
	{
		$lease = array();

		$lease["type"] = $row_i["poslease_type_name"];
		$lease["start"] = $row_i["start"];
		$lease["end"] = $row_i["end"];
		$lease["months"] = $row_i["months"];
		if($row_i["poslease_anual_rent"] > 0)
		{
			$lease["rent"] = number_format($row_i["poslease_anual_rent"],0) . " " . $row_i["currency_symbol"];
		}
		else
		{
			$lease["rent"] = "";
		}

		$lease["extension"] = $row_i["extension"];
		$lease["exit"] = $row_i["exitoption"];
		$lease["termination"] = $row_i["poslease_termination_time"];
		if($row_i["poslease_salespercent"] > 0)
		{
			$lease["salespercent"] = number_format($row_i["poslease_salespercent"],2) . "%";
		}
		else
		{
			$lease["salespercent"] = "";
		}
		
		$leases[] = $lease;
	}
	

	//projects
	$projects = array();
	$projectinvestmentsCHF = array();
	$projectinvestmentsLOC = array();
	$projectinvestmentsCurrency = array();
	

	$sql_i = "select *, if(posorder_opening_date = '0000-00-00', '', DATE_FORMAT(posorder_opening_date,'%d.%m.%Y')) as opened, " .
		   "if(posorder_closing_date = '0000-00-00', '', DATE_FORMAT(posorder_closing_date,'%d.%m.%Y')) as closed, order_actual_order_state_code, " .
		   "order_actual_order_state_code, project_watches_displayed, project_watches_stored, project_bijoux_displayed, project_bijoux_stored " . 
		   "from posorders " . 
		   "left join orders on order_id = posorder_order " .
		   "left join projects on project_order = order_id " .  
		   "left join product_lines on product_line_id = posorder_product_line ".
		   "left join postypes on postype_id = posorder_postype ".
		   "left join projectkinds on projectkind_id = posorder_project_kind ".
		   "left join project_costtypes on project_costtype_id = posorder_legal_type " . 
		   "where posorder_type = 1 and posorder_posaddress = " . param("id") . 
		   " order by posorder_year";

	$res_i = mysql_query($sql_i) or dberror($sql_i);
	while ($row_i = mysql_fetch_assoc($res_i))
	{
		
	    //check and build missing investment records
		if($row_i["posorder_order"])
		{	
			$result = build_missing_investment_records($row_i["posorder_id"]);
			$result = update_investment_records_from_local_currency($row_i["posorder_id"], false);
			$result = update_investment_records_from_system_currency($row_i["posorder_id"], false);
		}
	
		
		$project = array();

		$project["order_number"] = $row_i["posorder_ordernumber"];
		$project["product_line"] = $row_i["product_line_name"];
		$project["project_postype"] = $row_i["postype_name"];
		$project["projectkind"] = $row_i["projectkind_name"];
		$project["project_costtype"] = $row_i["project_costtype_text"];
		$project["order_state"] = $row_i["order_actual_order_state_code"];
		$project["opened"] = $row_i["opened"];
		$project["closed"] = $row_i["closed"];


		$project["watches_displayed"] = $row_i["project_watches_displayed"];
		$project["watches_stored"] = $row_i["project_watches_stored"];
		$project["bijoux_displayed"] = $row_i["project_bijoux_displayed"];
		$project["bijoux_stored"] = $row_i["project_bijoux_stored"];

		$project["neighbour_left"] = $row_i["posorder_neighbour_left"];
		$project["neighbour_right"] = $row_i["posorder_neighbour_right"];
		$project["neighbour_acrleft"] = $row_i["posorder_neighbour_acrleft"];
		$project["neighbour_acrright"] = $row_i["posorder_neighbour_acrright"];
		$project["neighbour_brands"] = $row_i["posorder_neighbour_brands"];
		$project["neighbour_comment"] = $row_i["posorder_neighbour_comment"];

		$actual_order_state_code = $row_i["order_actual_order_state_code"];

	
		$projects[] = $project;


		
		//investments only for projects with state 820
		if(!$actual_order_state_code or $actual_order_state_code >= "820")
		{
			$investmentsCHF = array();
			$investmentsLOC = array();

			$sql_in = "select * from posorderinvestments " . 
						 "left join posinvestment_types on posinvestment_type_id = posorderinvestment_investment_type " . 
						 "where posinvestment_type_intangible <> 1 " . 
						 " and posorderinvestment_posorder = " . $row_i["posorder_id"] . 
						 " order by posinvestment_type_group DESC, posinvestment_type_sortorder";

			$res_in = mysql_query($sql_in) or dberror($sql_in);
			while ($row_in = mysql_fetch_assoc($res_in))
			{
				$investment = array();
				$investment_loc = array();

				$investment["name"] = $row_in["posinvestment_type_name"];
				$investment["cer"] = $row_in["posorderinvestment_amount_cer"];
				$investment["cms"] = $row_in["posorderinvestment_amount_cms"];
				$investment_loc["cer"] = $row_in["posorderinvestment_amount_cer_loc"];
				$investment_loc["cms"] = $row_in["posorderinvestment_amount_cms_loc"];
				

				$diff = "";
				$diffp = "";
				if($row_in["posorderinvestment_amount_cer"] - $row_in["posorderinvestment_amount_cms"] != 0)
				{
					$diff = $row_in["posorderinvestment_amount_cms"] - $row_in["posorderinvestment_amount_cer"];
					
					if($row_in["posorderinvestment_amount_cer"] != 0)
					{
						$diffp = ($row_in["posorderinvestment_amount_cms"] - $row_in["posorderinvestment_amount_cer"]) / $row_in["posorderinvestment_amount_cer"];
					}
				}
				$investment["diff"] = 1*$diff;
				$investment["diffp"] = 1*$diffp;

				$diff_loc = "";
				$diffp_loc = "";
				if($row_in["posorderinvestment_amount_cer_loc"] - $row_in["posorderinvestment_amount_cms_loc"] != 0)
				{
					$diff_loc = $row_in["posorderinvestment_amount_cms_loc"] - $row_in["posorderinvestment_amount_cer_loc"];
					
					if($row_in["posorderinvestment_amount_cer_loc"] != 0)
					{
						$diffp_loc = ($row_in["posorderinvestment_amount_cms_loc"] - $row_in["posorderinvestment_amount_cer_loc"]) / $row_in["posorderinvestment_amount_cer_loc"];
					}
				}
				$investment_loc["diff"] = 1*$diff_loc;
				$investment_loc["diffp"] = 1*$diffp_loc;


				$investmentsCHF[$row_in["posinvestment_type_group"]][$row_in["posinvestment_type_name"]] = $investment;
				$investmentsLOC[$row_in["posinvestment_type_group"]][$row_in["posinvestment_type_name"]] = $investment_loc;
			}

			$projectinvestmentsCHF[$row_i["posorder_ordernumber"]] = $investmentsCHF;
			$projectinvestmentsLOC[$row_i["posorder_ordernumber"]] = $investmentsLOC;
			$projectinvestmentsCurrency[$row_i["posorder_ordernumber"]] = $row_i["posorder_currency_symbol"];
		}

		
	}


	//intangible assets

	//check and build missing intangible investment records
	if(param("id"))
	{	
		$result = build_missing_intangible_investment_records(param("id"));
		$result = update_intangible_investment_records_from_local_currency(param("id"), false);
		$result = update_intangible_investment_records_from_system_currency(param("id"), false);
	}

	$intangiblesCHF = array();
	$intangiblesLOC = array();

	$sql_i = "select * from posinvestments " . 
		     "left join posinvestment_types on posinvestment_type_id = posinvestment_investment_type " . 
		     "where posinvestment_type_intangible = 1 " . 
		     " and posinvestment_posaddress = " . param("id") . 
		     " order by posinvestment_type_sortorder";

	$res_i = mysql_query($sql_i) or dberror($sql_i);
	while ($row_i = mysql_fetch_assoc($res_i))
	{

		$intangible = array();
		$intangible_loc = array();

		$intangible["name"] = $row_i["posinvestment_type_name"];
		$intangible["cer"] = $row_i["posinvestment_amount_cer"];
		$intangible["cms"] = $row_i["posinvestment_amount_cms"];
		$intangible_loc["cer"] = $row_i["posinvestment_amount_cer_loc"];
		$intangible_loc["cms"] = $row_i["posinvestment_amount_cms_loc"];
		

		$diff = "";
		$diffp = "";
		if($row_i["posinvestment_amount_cer"] - $row_i["posinvestment_amount_cms"] != 0)
		{
			$diff = $row_i["posinvestment_amount_cms"] - $row_i["posinvestment_amount_cer"];
			
			if($row_i["posinvestment_amount_cer"] != 0)
			{
				$diffp = ($row_i["posinvestment_amount_cms"] - $row_i["posinvestment_amount_cer"]) / $row_i["posinvestment_amount_cer"];
			}
		}
		$intangible["diff"] = 1*$diff;
		$intangible["diffp"] = 1*$diffp;

		$diff_loc = "";
		$diffp_loc = "";
		if($row_i["posinvestment_amount_cer_loc"] - $row_i["posinvestment_amount_cms_loc"] != 0)
		{
			$diff_loc = $row_i["posinvestment_amount_cms_loc"] - $row_i["posinvestment_amount_cer_loc"];
			
			if($row_i["posinvestment_amount_cer_loc"] != 0)
			{
				$diffp_loc = ($row_i["posinvestment_amount_cms_loc"] - $row_i["posinvestment_amount_cer_loc"]) / $row_i["posinvestment_amount_cer_loc"];
			}
		}
		$intangible_loc["diff"] = 1*$diff_loc;
		$intangible_loc["diffp"] = 1*$diffp_loc;


		$intangiblesLOC[$row_i["posinvestment_type_name"]] = $intangible_loc;
		$intangiblesCHF[$row_i["posinvestment_type_name"]] = $intangible;
	}

	$posaddress_currency = $row["posaddress_oldcurrency"];

	if($row["posaddress_keymoney_recoverable"] == 1)
	{
		$poskeymoney = "Keymoney is recoverable";
	}
	else
	{
		$poskeymoney = "Keymoney is not recoverable";
	}


	/********************************************************************
		prepare pdf
	*********************************************************************/

	require_once('../include/tcpdf/config/lang/eng.php');
	require_once('../include/tcpdf/tcpdf.php');
	
	class MYPDF extends TCPDF
	{
		//Page header
		function Header()
		{
			global $page_title;
			//Logo
			$this->Image('../pictures/logo.jpg',10,8,33);
			//Arial bold 15
			$this->SetFont('arialn','B',12);
			//Move to the right
			$this->Cell(80);
			//Title
			$this->Cell(0,34,$page_title,0,0,'R');
			//Line break
			$this->Ln(20);

		}

		//Page footer
		function Footer()
		{
			//Position at 1.5 cm from bottom
			$this->SetY(-15);
			//arialn italic 8
			$this->SetFont('arialn','I',8);
			//Page number
			$this->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$this->PageNo(),0,0,'R');
		}

	}

	//Instanciation of inherited class
	$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);
	$pdf->SetMargins(10, PDF_MARGIN_TOP, 11);

	$pdf->Open();


	$pdf->SetFillColor(220, 220, 220); 

	$pdf->AddFont('arialn');
	$pdf->AddFont('arialn', 'B');

	$pdf->AddPage();

	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190,7,"POS Name: " . $posname,1, 0, 'L', 1);
	$pdf->Ln();
	$pdf->SetFont('arialn','',10);
	$pdf->Cell(190,7,"POS Address: " . $posaddress,1, 0, 'L', 0);
	if($relocation_of)
	{
		$pdf->Ln();
		$pdf->SetFont('arialn','',9);
		$pdf->Cell(190,7,$relocation_of,1, 0, 'L', 0);
	}
	$pdf->Ln();
	$pdf->Ln();

	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"Phone",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(70, 5,$posphone,1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"Fax",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(70, 5,$posfax,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"Email",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(165, 5,$posemail,1, 0, 'L', 0);
	$pdf->Ln();
	
	/*
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"Website",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(70, 5,$poswebsite,1, 0, 'L', 0);
	$pdf->Ln();
    */

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"Areas",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(165, 5,$posareas,1, 0, 'L', 0);
	$pdf->Ln();
	$pdf->Ln();

	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"Opened",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(70, 5,$posopeningdate,1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"Closed",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(70, 5,$posclosingdate,1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"Legal Type",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(70, 5,$poslegaltype,1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"ERP / SAP No.",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	if($poserno and $possapno)
	{
		$pdf->Cell(70, 5, $poserno . " / " . $possapno,1, 0, 'L', 0);
	}
	elseif($poserno)
	{
		$pdf->Cell(70, 5, $poserno . " / - ",1, 0, 'L', 0);
	}
	elseif($possapno)
	{
		$pdf->Cell(70, 5, " - / " . $possapno,1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(70, 5, "",1, 0, 'L', 0);
	}

	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"POS Type",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(70, 5,$postype,1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"POS Type Subcl.",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(70, 5,$possubclass,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"Furniture",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(70, 5,$posfurniture,1, 0, 'L', 0);


	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"Furniture Subcl.",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(70, 5,$posfurniture_subclass,1, 0, 'L', 0);
	
	//$pdf->SetFont('arialn','B',8);
	//$pdf->Cell(25, 5,"",1, 0, 'L', 0);
	//$pdf->SetFont('arialn','',8);
	//$pdf->Cell(70, 5,"",1, 0, 'L', 0);
	$pdf->Ln();
	$pdf->Ln();

	
	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190, 5,"Surfaces in Square Meters",0, 0, 'L', 0);

	$pdf->Ln();
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"Gross",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(70, 5,$posgrosssqm,1, 0, 'L', 0);

	$pdf->Ln();
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"Total",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(70, 5,$postotalsqm,1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"Retail",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(70, 5,$posretailsqm,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"Office",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(70, 5,$posofficesqm,1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"1st Floor",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(70, 5,$posfloor1sqm,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"2nd Floor",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(70, 5,$posfloor2sqm,1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"3rd Floor",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(70, 5,$posfloor3sqm,1, 0, 'L', 0);
	$pdf->Ln();
	$pdf->Ln();


	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190, 5,"Staff",0, 0, 'L', 0);
	$pdf->Ln();
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"Heads",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(70, 5,$posheads,1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"Fulltime Eq.",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(70, 5,$posfulltime,1, 0, 'L', 0);
	$pdf->Ln();
	$pdf->Ln();

	
	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190, 5,"Ownership",0, 0, 'L', 0);
	$pdf->Ln();
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"Client",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(165, 5,$posclient,1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"Franchisor",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(165, 5,$posfranchisor,1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,$owner_type_name,1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(165, 5,$posfranchisee,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"Agreement",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(165, 5,$posagreement,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"Duration",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(165, 5,$posagreementduration,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"Cancellation",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(165, 5,$posagreementcancellation,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"Comment",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->MultiCell(165, 5, $posagreementcomment, 1, 'L', 0);
	

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"Take Over",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(165, 5,$postakeoverdetail,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"From",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(165, 5,$postakeoverfrom,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(25, 5,"Comment",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->MultiCell(165, 5, $postakeovercomment, 1, 'L', 0);
	$pdf->Ln();

	//leases
	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190, 5,"Leases",0, 0, 'L', 0);
	$pdf->Ln();
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(16, 5,"Type",1, 0, 'L', 0);
	$pdf->Cell(18, 5,"Start",1, 0, 'L', 0);
	$pdf->Cell(18, 5,"End",1, 0, 'L', 0);
	$pdf->Cell(14, 5,"Months",1, 0, 'L', 0);
	$pdf->Cell(26, 5,"Rent p.a.",1, 0, 'L', 0);
	$pdf->Cell(24, 5,"Rent Sales Ratio",1, 0, 'L', 0);
	$pdf->Cell(26, 5,"Extension Option",1, 0, 'L', 0);
	$pdf->Cell(18, 5,"Exit Option",1, 0, 'L', 0);
	$pdf->Cell(30, 5,"Termination deadline",1, 0, 'L', 0);
	$pdf->Ln();
	
	$pdf->SetFont('arialn','',8);
	foreach($leases as $key=>$lease)
	{
		$pdf->Cell(16, 5,$lease["type"],1, 0, 'L', 0);
		$pdf->Cell(18, 5,$lease["start"],1, 0, 'L', 0);
		$pdf->Cell(18, 5,$lease["end"],1, 0, 'L', 0);
		$pdf->Cell(14, 5,$lease["months"],1, 0, 'L', 0);
		$pdf->Cell(26, 5,$lease["rent"],1, 0, 'L', 0);
		$pdf->Cell(24, 5,$lease["salespercent"],1, 0, 'L', 0);
		$pdf->Cell(26, 5,$lease["extension"],1, 0, 'L', 0);
		$pdf->Cell(18, 5,$lease["exit"],1, 0, 'L', 0);
		$pdf->Cell(30, 5,$lease["termination"],1, 0, 'L', 0);
		
		$pdf->Ln();
	}
	$pdf->Ln();

	//projects
	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190, 5,"Projects",0, 0, 'L', 0);
	$pdf->Ln();
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(20, 5,"Number",1, 0, 'L', 0);
	$pdf->Cell(20, 5,"Product Line",1, 0, 'L', 0);
	$pdf->Cell(20, 5,"Project Typ",1, 0, 'L', 0);
	$pdf->Cell(35, 5,"Project Kind",1, 0, 'L', 0);
	$pdf->Cell(20, 5,"Legal Type",1, 0, 'L', 0);
	$pdf->Cell(20, 5,"Status",1, 0, 'L', 0);
	$pdf->Cell(30, 5,"Opened",1, 0, 'L', 0);
	$pdf->Cell(25, 5,"Closed",1, 0, 'L', 0);
	$pdf->Ln();
	
	$pdf->SetFont('arialn','',8);
	foreach($projects as $key=>$project)
	{
		$pdf->Cell(20, 5,$project["order_number"],1, 0, 'L', 0);
		$pdf->Cell(20, 5,$project["product_line"],1, 0, 'L', 0);
		$pdf->Cell(20, 5,$project["project_postype"],1, 0, 'L', 0);
		$pdf->Cell(35, 5,$project["projectkind"],1, 0, 'L', 0);
		$pdf->Cell(20, 5,$project["project_costtype"],1, 0, 'L', 0);
		$pdf->Cell(20, 5,$project["order_state"],1, 0, 'L', 0);
		$pdf->Cell(30, 5,$project["opened"],1, 0, 'L', 0);
		$pdf->Cell(25, 5,$project["closed"],1, 0, 'L', 0);
		
		$pdf->Ln();
	}
	$pdf->Ln();

	//Capacities requested by client
	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190, 5,"Capacity Request by Client",0, 0, 'L', 0);
	$pdf->Ln();
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(20, 5,"Project",1, 0, 'L', 0);
	$pdf->Cell(35, 5,"Watches on Display",1, 0, 'L', 0);
	$pdf->Cell(35, 5,"Watches on Storage",1, 0, 'L', 0);
	$pdf->Cell(35, 5,"Bijoux on Display",1, 0, 'L', 0);
	$pdf->Cell(35, 5,"Bijoux on Storage",1, 0, 'L', 0);
	$pdf->Ln();
	
	$pdf->SetFont('arialn','',8);
	foreach($projects as $key=>$project)
	{
		$pdf->Cell(20, 5,$project["order_number"],1, 0, 'L', 0);
		$pdf->Cell(35, 5,$project["watches_displayed"],1, 0, 'L', 0);
		$pdf->Cell(35, 5,$project["watches_stored"],1, 0, 'L', 0);
		$pdf->Cell(35, 5,$project["bijoux_displayed"],1, 0, 'L', 0);
		$pdf->Cell(35, 5,$project["bijoux_stored"],1, 0, 'L', 0);
		$pdf->Ln();
	}

	
	//page 2
	$pdf->AddPage();
	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190,7,"POS Name: " . $posname,1, 0, 'L', 1);
	$pdf->Ln();
	$pdf->SetFont('arialn','',10);
	$pdf->Cell(190,7,"POS Address: " . $posaddress,1, 0, 'L', 0);
	$pdf->Ln();
	$pdf->Ln();
	
	
	//Investments Intangibles

	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190, 5,"Investments - Intangibles (" . $poskeymoney . ")",0, 0, 'L', 0);
	$pdf->Ln();


	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(40, 5,"Type",1, 0, 'L', 0);
	$pdf->Cell(20, 5,"CER " . $system_currency["symbol"],1, 0, 'R', 0);
	$pdf->Cell(20, 5,"Real " . $system_currency["symbol"],1, 0, 'R', 0);
	$pdf->Cell(20, 5,"Difference",1, 0, 'R', 0);
	$pdf->Cell(15, 5,"in %",1, 0, 'R', 0);
	$pdf->Cell(20, 5,"CER " . $posaddress_currency,1, 0, 'R', 0);
	$pdf->Cell(20, 5,"Real " . $posaddress_currency,1, 0, 'R', 0);
	$pdf->Cell(20, 5,"Difference",1, 0, 'R', 0);
	$pdf->Cell(15, 5,"in %",1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','',8);
	$sum1 = 0;
	$sum2 = 0;
	$sum3 = 0;
	$sum4 = 0;
	$sum5 = 0;
	$sum6 = 0;
	foreach($intangiblesCHF as $name=>$intangible)
	{
		
		$pdf->Cell(40, 5,$name,1, 0, 'L', 0);
		$pdf->Cell(20, 5,number_format($intangible["cer"], 2, ".", "'"),1, 0, 'R', 0);
		$pdf->Cell(20, 5,number_format($intangible["cms"], 2, ".", "'"),1, 0, 'R', 0);
		$pdf->Cell(20, 5,number_format($intangible["diff"], 2, ".", "'"),1, 0, 'R', 0);
		$pdf->Cell(15, 5,number_format($intangible["diffp"]*100, 2, '.', ''),1, 0, 'R', 0);
		$pdf->Cell(20, 5,number_format($intangiblesLOC[$name]["cer"], 2, '.', ''),1, 0, 'R', 0);
		$pdf->Cell(20, 5,number_format($intangiblesLOC[$name]["cms"], 2, '.', ''),1, 0, 'R', 0);
		$pdf->Cell(20, 5,number_format($intangiblesLOC[$name]["diff"], 2, '.', ''),1, 0, 'R', 0);
		$pdf->Cell(15, 5,number_format($intangiblesLOC[$name]["diffp"]*100, 2, '.', ''),1, 0, 'R', 0);
		$pdf->Ln();

		$sum1 = $sum1 + $intangible["cer"];
		$sum2 = $sum2 + $intangible["cms"];
		$sum3 = $sum3 + $intangible["diff"];
		$sum4 = $sum4 + $intangiblesLOC[$name]["cer"];
		$sum5 = $sum5 + $intangiblesLOC[$name]["cms"];
		$sum6 = $sum6 + $intangiblesLOC[$name]["diff"];
	
	}

	$diffp1 = "0.00%";
	if($sum1 != 0)
	{
		$diffp1 = ($sum2 - $sum1) / $sum1;
		$diffp1 = number_format($diffp1*100, 2, ".", "'") . "%";
	}
	$diffp2 = "0.00%";
	if($sum4 != 0)
	{
		$diffp2 = ($sum5 - $sum4) / $sum4;
		$diffp2 = number_format($diffp2*100, 2, ".", "'") . "%";
	}
	
	$sum1 = number_format($sum1, 2, ".", "'");
	$sum2 = number_format($sum2, 2, ".", "'");
	$sum3 = number_format($sum3, 2, ".", "'");
	$sum4 = number_format($sum4, 2, ".", "'");
	$sum5 = number_format($sum5, 2, ".", "'");
	$sum6 = number_format($sum6, 2, ".", "'");



	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(40, 5,"Totals",1, 0, 'L', 0);
	$pdf->Cell(20, 5,$sum1,1, 0, 'R', 0);
	$pdf->Cell(20, 5,$sum2,1, 0, 'R', 0);
	$pdf->Cell(20, 5,$sum3,1, 0, 'R', 0);
	$pdf->Cell(15, 5,$diffp1,1, 0, 'R', 0);
	$pdf->Cell(20, 5,$sum4,1, 0, 'R', 0);
	$pdf->Cell(20, 5,$sum5,1, 0, 'R', 0);
	$pdf->Cell(20, 5,$sum6,1, 0, 'R', 0);
	$pdf->Cell(15, 5,$diffp2,1, 0, 'R', 0);
	$pdf->Ln();
	$pdf->Ln();


	//Investments for Projects
	foreach($projectinvestmentsCHF as $order_number=>$investments)
	{
		
		if($pdf->getY() > 228)
		{
			$pdf->AddPage();
			$pdf->SetFont('arialn','B',10);
			$pdf->Cell(190,7,"POS Name: " . $posname,1, 0, 'L', 1);
			$pdf->Ln();
			$pdf->SetFont('arialn','',10);
			$pdf->Cell(190,7,"POS Address: " . $posaddress,1, 0, 'L', 0);
			$pdf->Ln();
			$pdf->Ln();
		}
		
		$pdf->SetFont('arialn','B',10);
		$pdf->Cell(190, 5,"Investments - Project " . $order_number,0, 0, 'L', 0);
		$pdf->Ln();


		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(40, 5,"Type",1, 0, 'L', 0);
		$pdf->Cell(20, 5,"CER " . $system_currency["symbol"],1, 0, 'R', 0);
		$pdf->Cell(20, 5,"Real " . $system_currency["symbol"],1, 0, 'R', 0);
		$pdf->Cell(20, 5,"Difference",1, 0, 'R', 0);
		$pdf->Cell(15, 5,"in %",1, 0, 'R', 0);
		$pdf->Cell(20, 5,"CER " . $projectinvestmentsCurrency[$order_number],1, 0, 'R', 0);
		$pdf->Cell(20, 5,"Real " . $projectinvestmentsCurrency[$order_number],1, 0, 'R', 0);
		$pdf->Cell(20, 5,"Difference",1, 0, 'R', 0);
		$pdf->Cell(15, 5,"in %",1, 0, 'R', 0);
		$pdf->Ln();

		
		$pdf->SetFont('arialn','',8);
		$sum1 = 0;
		$sum2 = 0;
		$sum3 = 0;
		$sum4 = 0;
		$sum5 = 0;
		$sum6 = 0;
		$group_sum1 = 0;
		$group_sum2 = 0;
		$group_sum3 = 0;
		$group_sum4 = 0;
		$group_sum5 = 0;
		$group_sum6 = 0;
		$oldgroup = "";
		$printgroup_totals = 0;

		foreach($investments as $group=>$investment)
		{
			
			foreach($investment as $name=>$values)
			{
				if($oldgroup != $group and $printgroup_totals == 1)
				{
					$printgroup_totals = 0;
					
					$diffp1 = "0.00%";
					if($group_sum1 != 0)
					{
						$diffp1 = ($group_sum2 - $group_sum1) / $group_sum1;
						$diffp1 = number_format($diffp1*100, 2, '.','') . "%";
					}
					$diffp2 = "0.00%";
					if($group_sum4 != 0)
					{
						$diffp2 = ($group_sum5 - $group_sum4) / $group_sum4;
						$diffp2 = number_format($diffp2*100, 2, '.','') . "%";
					}
					
					$group_sum1 = number_format($group_sum1, 2, ".", "'");
					$group_sum2 = number_format($group_sum2, 2, ".", "'");
					$group_sum3 = number_format($group_sum3, 2, ".", "'");
					$group_sum4 = number_format($group_sum4, 2, ".", "'");
					$group_sum5 = number_format($group_sum5, 2, ".", "'");
					$group_sum6 = number_format($group_sum6, 2, ".", "'");
					
					
					
					
					$pdf->SetFont('arialn','B',8);
					$pdf->Cell(40, 5, $oldgroup . " Subtotal",1, 0, 'L', 0);
					$pdf->Cell(20, 5,$group_sum1,1, 0, 'R', 0);
					$pdf->Cell(20, 5,$group_sum2,1, 0, 'R', 0);
					$pdf->Cell(20, 5,$group_sum3,1, 0, 'R', 0);
					$pdf->Cell(15, 5,$diffp1,1, 0, 'R', 0);
					$pdf->Cell(20, 5,$group_sum4,1, 0, 'R', 0);
					$pdf->Cell(20, 5,$group_sum5,1, 0, 'R', 0);
					$pdf->Cell(20, 5,$group_sum6,1, 0, 'R', 0);
					$pdf->Cell(15, 5,$diffp2,1, 0, 'R', 0);
					$pdf->Ln();
					$pdf->SetFont('arialn','',8);
				}
				
				if($oldgroup != $group)
				{
					
					$group_sum1 = 0;
					$group_sum2 = 0;
					$group_sum3 = 0;
					$group_sum4 = 0;
					$group_sum5 = 0;
					$group_sum6 = 0;

					//$pdf->SetFont('arialn','B',8);
					//$pdf->Cell(190, 5,$group,1, 0, 'L', 0);
					//$pdf->Ln();
					//$pdf->SetFont('arialn','',8);
					$oldgroup = $group;
					$printgroup_totals = 1;
				}

				$pdf->Cell(40, 5,$name,1, 0, 'L', 0);
				$pdf->Cell(20, 5,number_format($values["cer"], 2, ".", "'"),1, 0, 'R', 0);
				$pdf->Cell(20, 5,number_format($values["cms"], 2, ".", "'"),1, 0, 'R', 0);
				$pdf->Cell(20, 5,number_format($values["diff"], 2, ".", "'"),1, 0, 'R', 0);
				$pdf->Cell(15, 5,number_format($values["diffp"]*100, 2, '.', ''),1, 0, 'R', 0);
				$pdf->Cell(20, 5,number_format($projectinvestmentsLOC[$order_number][$group][$name]["cer"], 2, '.', ''),1, 0, 'R', 0);
				$pdf->Cell(20, 5,number_format($projectinvestmentsLOC[$order_number][$group][$name]["cms"], 2, '.', ''),1, 0, 'R', 0);
				$pdf->Cell(20, 5,number_format($projectinvestmentsLOC[$order_number][$group][$name]["diff"], 2, '.', ''),1, 0, 'R', 0);
				$pdf->Cell(15, 5,number_format($projectinvestmentsLOC[$order_number][$group][$name]["diffp"]*100, 2, '.', ''),1, 0, 'R', 0);
				$pdf->Ln();

				$group_sum1 = $group_sum1 + $values["cer"];
				$group_sum2 = $group_sum2 + $values["cms"];
				$group_sum3 = $group_sum3 + $values["diff"];
				
				$group_sum4 = $group_sum4 + $projectinvestmentsLOC[$order_number][$group][$name]["cer"];
				$group_sum5 = $group_sum5 + $projectinvestmentsLOC[$order_number][$group][$name]["cms"];
				$group_sum6 = $group_sum6 + $projectinvestmentsLOC[$order_number][$group][$name]["diff"];
				
				$sum1 = $sum1 + $values["cer"];
				$sum2 = $sum2 + $values["cms"];
				$sum3 = $sum3 + $values["diff"];
				
				$sum4 = $sum4 + $projectinvestmentsLOC[$order_number][$group][$name]["cer"];
				$sum5 = $sum5 + $projectinvestmentsLOC[$order_number][$group][$name]["cms"];
				$sum6 = $sum6 + $projectinvestmentsLOC[$order_number][$group][$name]["diff"];

				
			}
		}

		//last group_total

		$diffp1 = "0.00%";
		if($group_sum1 != 0)
		{
			$diffp1 = ($group_sum2 - $group_sum1) / $group_sum1;
			$diffp1 = number_format($diffp1*100, 2, '.','') . "%";
		}
		$diffp2 = "0.00%";
		if($group_sum4 != 0)
		{
			$diffp2 = ($group_sum5 - $group_sum4) / $group_sum4;
			$diffp2 = number_format($diffp2*100, 2, '.','') . "%";
		}
		
		$group_sum1 = number_format($group_sum1, 2, ".", "'");
		$group_sum2 = number_format($group_sum2, 2, ".", "'");
		$group_sum3 = number_format($group_sum3, 2, ".", "'");
		$group_sum4 = number_format($group_sum4, 2, ".", "'");
		$group_sum5 = number_format($group_sum5, 2, ".", "'");
		$group_sum6 = number_format($group_sum6, 2, ".", "'");
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(40, 5,$oldgroup . " Subtotal",1, 0, 'L', 0);
		$pdf->Cell(20, 5,$group_sum1,1, 0, 'R', 0);
		$pdf->Cell(20, 5,$group_sum2,1, 0, 'R', 0);
		$pdf->Cell(20, 5,$group_sum3,1, 0, 'R', 0);
		$pdf->Cell(15, 5,$diffp1,1, 0, 'R', 0);
		$pdf->Cell(20, 5,$group_sum4,1, 0, 'R', 0);
		$pdf->Cell(20, 5,$group_sum5,1, 0, 'R', 0);
		$pdf->Cell(20, 5,$group_sum6,1, 0, 'R', 0);
		$pdf->Cell(15, 5,$diffp2,1, 0, 'R', 0);
		$pdf->Ln();

		//list Total

		$diffp1 = "0.00%";
		if($sum1 != 0)
		{
			$diffp1 = ($sum2 - $sum1) / $sum1;
			$diffp1 = number_format($diffp1*100, 2, '.','') . "%";
		}
		$diffp2 = "0.00%";
		if($sum4 != 0)
		{
			$diffp2 = ($sum5 - $sum4) / $sum4;
			$diffp2 = number_format($diffp2*100, 2, '.','') . "%";
		}
		
		$sum1 = number_format($sum1, 2, ".", "'");
		$sum2 = number_format($sum2, 2, ".", "'");
		$sum3 = number_format($sum3, 2, ".", "'");
		$sum4 = number_format($sum4, 2, ".", "'");
		$sum5 = number_format($sum5, 2, ".", "'");
		$sum6 = number_format($sum6, 2, ".", "'");



		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(40, 5,"Project Total",1, 0, 'L', 0);
		$pdf->Cell(20, 5,$sum1,1, 0, 'R', 0);
		$pdf->Cell(20, 5,$sum2,1, 0, 'R', 0);
		$pdf->Cell(20, 5,$sum3,1, 0, 'R', 0);
		$pdf->Cell(15, 5,$diffp1,1, 0, 'R', 0);
		$pdf->Cell(20, 5,$sum4,1, 0, 'R', 0);
		$pdf->Cell(20, 5,$sum5,1, 0, 'R', 0);
		$pdf->Cell(20, 5,$sum6,1, 0, 'R', 0);
		$pdf->Cell(15, 5,$diffp2,1, 0, 'R', 0);
		$pdf->Ln();
		$pdf->Ln();

	}



	//neighbours
	
	$pdf->SetFont('arialn','',8);
	foreach($projects as $key=>$project)
	{
		if($pdf->getY() > 228)
		{
			$pdf->AddPage();
			$pdf->SetFont('arialn','B',10);
			$pdf->Cell(190,7,"POS Name: " . $posname,1, 0, 'L', 1);
			$pdf->Ln();
			$pdf->SetFont('arialn','',10);
			$pdf->Cell(190,7,"POS Address: " . $posaddress,1, 0, 'L', 0);
			$pdf->Ln();
			$pdf->Ln();
		}

		$pdf->SetFont('arialn','B',10);
		$pdf->Cell(190, 5,"Project " . $project["order_number"] . " - Neighbourhood" ,0, 0, 'L', 0);
		$pdf->Ln();

		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(25, 5,"Left",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(165, 5,$project["neighbour_left"],1, 0, 'L', 0);
		$pdf->Ln();
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(25, 5,"Right",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(165, 5,$project["neighbour_right"],1, 0, 'L', 0);
		$pdf->Ln();

		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(25, 5,"Accross Left",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(165, 5,$project["neighbour_acrleft"],1, 0, 'L', 0);
		$pdf->Ln();

		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(25, 5,"Accross Right",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(165, 5,$project["neighbour_acrright"],1, 0, 'L', 0);
		$pdf->Ln();

		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(25, 5,"Other Brands",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->MultiCell(165, 5, $project["neighbour_brands"], 1, 'L', 0);

		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(25, 5,"Comment",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->MultiCell(165, 5, $project["neighbour_comment"], 1, 'L', 0);
		$pdf->Ln();
	}


	//Google Map
	if($pdf->getY() > 170)
	{
		$pdf->AddPage();
		$pdf->SetFont('arialn','B',10);
		$pdf->Cell(190,7,"POS Name: " . $posname,1, 0, 'L', 1);
		$pdf->Ln();
		$pdf->SetFont('arialn','',10);
		$pdf->Cell(190,7,"POS Address: " . $posaddress,1, 0, 'L', 0);
		$pdf->Ln();
		$pdf->Ln();
	}

	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190, 5, "Maps" ,0, 0, 'L', 0);
	$pdf->Ln();
	$pdf->Ln();

	if($latitude and $longitude)
	{
		

		$url = STATIC_MAPS_HOST;
		$url .= '?center=' . $latitude . ',' . $longitude;
		$url .= '&zoom=16';
		$url .= '&size=640x640';
		$url .= '&maptype=roadmap' . "\\";
		$url .= '&markers=color:red%7Clabel:P%7C' . $latitude . ',' . $longitude;
		$url .= '&format=jpg';
		$url .= '&key=' . GOOGLE_API_KEY;
		$url .= '&sensor=false';

		
		$tmpfilename1 = "map1" . time() . ".jpg";

		
		if(!$context) {
			
			$mapImage1 = @file_get_contents($url);
		}
		else
		{
			$mapImage1 = @file_get_contents($url, false, $context);
		}

		if($mapImage1) {
			
			$fh = fopen(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1, 'w') or die("can't open file");
			fwrite($fh, $mapImage1);
			fclose($fh);
			
			if(file_exists(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1)) {
				$pdf->Image(TMP_FILE_DIR_RELATIVE . $tmpfilename1, 10, $pdf->getY(), 90, 90);
			}
		}


		$url = STATIC_MAPS_HOST;
		$url .= '?center=' . $latitude . ',' . $longitude;
		$url .= '&zoom=14';
		$url .= '&size=640x640';
		$url .= '&maptype=roadmap' . "\\";
		$url .= '&markers=color:red%7Clabel:P%7C' . $latitude . ',' . $longitude;
		$url .= '&format=jpg';
		$url .= '&key=' . GOOGLE_API_KEY;
		$url .= '&sensor=false';

		$tmpfilename2 = "map2" . time() . ".jpg";
		
		
		if(!$context) {
			
			$mapImage2 = @file_get_contents($url);
		}
		else
		{
			$mapImage2 = @file_get_contents($url, false, $context);
		}

		if($mapImage2) {
			$fh = fopen(TMP_FILE_DIR_ABSOLUTE . $tmpfilename2, 'w') or die("can't open file");
			fwrite($fh, $mapImage2);
			fclose($fh);

			if(file_exists(TMP_FILE_DIR_ABSOLUTE . $tmpfilename2)) {
				$pdf->Image(TMP_FILE_DIR_RELATIVE . $tmpfilename2, 110, $pdf->getY(), 90, 90);
			}
		}
	}

	// write pdf
	$pdf->Output();

	if(file_exists(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1))
	{
		unlink(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1);
	}

	if(file_exists(TMP_FILE_DIR_ABSOLUTE . $tmpfilename2))
	{
		unlink(TMP_FILE_DIR_ABSOLUTE . $tmpfilename2);
	}
}

?>