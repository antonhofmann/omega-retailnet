<?php
/********************************************************************

    pos_closing_assessment_pdf.php

    Print PDF for POS Closing Assessment Form (Form IN-R03).

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2012-02-11
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2012-02-11
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();


$SUPPRESS_HEADERS = true;
define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 

require "../include/frame.php";
require "include/get_functions.php";

check_access("has_access_to_posclosing_assessments");


if(!param("id")) 
{
	exit;
}




$print_booklet = false;
$print_signed_form = false;
if(param('b') == 1)
{
	$print_booklet = true;
}
elseif(param('b') == 2)
{
	$print_booklet = true;
	$print_signed_form = true;
}

if(param('b') == 3)
{
	$print_booklet = true;
	$print_signed_form = false;
}

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');
require_once('../include/fpdi/fpdi.php'); 
require_once('../include/SetaPDF/Autoload.php');


// Create and setup PDF document

$pdf = new FPDI("P", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 23, 10);
$pdf->setPrintHeader(false);


$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');

$pdf->Open();


$approval_names = array();
// get all data needed from project
$sql = "select * " .
	   "from posclosingassessments " .
	   "left join posaddresses on posaddress_id = posclosingassessment_posaddress_id " .
	   "left join addresses on address_id = posaddress_client_id " .
	   "left join countries on country_id = posaddress_country " . 
	   "left join places on place_id = posaddress_place_id " . 
	   "left join users on user_id = posclosingassessment_submissionuser " . 
	   "where posclosingassessment_id = " . param("id");


//add approval names
$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	if($row["posclosingassessment_approval_name01"] == '' 
		or $row["posclosingassessment_approval_name02"] == '' 
		or $row["posclosingassessment_approval_name03"] == '' 
		or $row["posclosingassessment_approval_name04"] == ''
		or $row["posclosingassessment_approval_name05"] == ''
		or $row["posclosingassessment_approval_name06"] == '')
	{
		
		$sql_a = "select * from cer_approvalnames where cer_approvalname_id = 1";
		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if($row_a = mysql_fetch_assoc($res_a))
		{
			$fields = array();

			//ceo
			$value = dbquote($row_a["cer_approvalname_name2"]);
			$fields[] = "posclosingassessment_approval_name01 = " . $value;

			//vp finance
			$value = dbquote($row_a["cer_approvalname_name7"]);
			$fields[] = "posclosingassessment_approval_name02 = " . $value;

			//vp sales
			$value = dbquote($row_a["cer_approvalname_name3"]);
			$fields[] = "posclosingassessment_approval_name03 = " . $value;

			// country manager
			$fm = get_country_manager($row["posaddress_client_id"], $row["posaddress_country"]);
			$value = dbquote($fm["user_name"] . " " . $fm["user_firstname"]);
			$fields[] = "posclosingassessment_approval_name04 = " . $value;

			// finance manager
			$fm = get_finance_manager($row["posaddress_client_id"], $row["posaddress_country"]);
			$value = dbquote($fm["user_name"] . " " . $fm["user_firstname"]);
			$fields[] = "posclosingassessment_approval_name05 = " . $value;

			//brand manager
			$fm = get_brand_manager($row["posaddress_client_id"], $row["posaddress_country"]);
			$value = dbquote($fm["user_name"] . " " . $fm["user_firstname"]);
			$fields[] = "posclosingassessment_approval_name06 = " . $value;

			
			$sql_u = "update posclosingassessments set " . join(", ", $fields) . " where posclosingassessment_id = " . param("id");
			mysql_query($sql_u) or dberror($sql_u);
		}
		
	}
}

//start output
$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	
	$legal_entity = $row['address_company'];
	$project_name1 = $row["posaddress_name"];
	$project_name2 =  $row["country_name"] . ", " . $row["place_name"] . ", " . $row["posaddress_address"];

	$opening_date = to_system_date($row["posaddress_store_openingdate"]);
	$fag_ending_date = to_system_date($row["posaddress_fagrend"]);

	$contact_person = $row["posclosingassessment_contactperson"];

	$lease_enddate = to_system_date($row["posclosingassessment_lease_enddate"]);

	$image_consequences = $row["posclosingassessment_image_consequence"];
	$sales_consequences = $row["posclosingassessment_sales_consequence"];
	$legal_consequences = $row["posclosingassessment_legal_consequence"];
	$furniture_consequences = $row["posclosingassessment_furniture_consequence"];

	$planned_closinddate = to_system_date($row["posclosingassessment_planned_closingdate"]);

	$reason = $row["posclosingassessment_reason"];
	$landlord = $row["posclosingassessment_landlord"];

	$year = $row["posclosingassessment_year"];
	$year2 = substr($row["posclosingassessment_planned_closingdate"], 0, 4);

	$initial_investment = round($row["posclosingassessment_initial_investment"] / 1000);
	$book_value = round($row["posclosingassessment_book_value"] / 1000);
	$termination_costs = round($row["posclosingassessment_termination_costs"] / 1000);
	$rental_costs = round($row["posclosingassessment_rental_costs"] / 1000);
	$operating_loss = round($row["posclosingassessment_operating_loss"] / 1000);
	$operating_loss_ws = round($row["posclosingassessment_operating_loss_ws"] / 1000);
	$depr_years = $row["posclosingassessment_depr_years"];



	$financial_data = array();
	$sql_f = "select * from posclosingassessmentfindates " . 
			 "where posclosingassessmentfindate_assessment_id = " . $row["posclosingassessment_id"] . 
			 " order by posclosingassessmentfindate_fintype";

	$res_f = mysql_query($sql_f) or dberror($sql_f);
	while($row_f = mysql_fetch_assoc($res_f))
	{
		$type = array();
		$type[1] = round($row_f["posclosingassessmentfindate_amount01"]/1000, 0);
		$type[2] = round($row_f["posclosingassessmentfindate_amount02"]/1000, 0);
		$type[3] = round($row_f["posclosingassessmentfindate_amount03"]/1000, 0);
		$type[4] = round($row_f["posclosingassessmentfindate_amount04"]/1000, 0);
		$type[5] = round($row_f["posclosingassessmentfindate_amount05"]/1000, 0);

		$financial_data[$row_f["posclosingassessmentfindate_fintype"]] = $type;
	
	}

	$cost = array();
	$cost[1] = $row["posclosingassessment_terminationcost01"];
	$cost[2] = $row["posclosingassessment_terminationcost02"];
	$cost[3] = $row["posclosingassessment_terminationcost03"];
	$cost[4] = $row["posclosingassessment_terminationcost04"];
	$cost[5] = $row["posclosingassessment_terminationcost05"];
	$cost[6] = $row["posclosingassessment_terminationcost06"];
	$cost[7] = $row["posclosingassessment_terminationcost07"];
	$cost[8] = $row["posclosingassessment_terminationcost08"];
	$cost[9] = $row["posclosingassessment_terminationcost09"];
	$cost["t"] = $cost[1] + $cost[2] + $cost[3] + $cost[4] + $cost[5] + $cost[6] + $cost[7] + $cost[8]  + $cost[9];

	foreach($cost as $key=>$value) {
		$cost[$key] = round($cost[$key]/1000, 0);
	}

	$cost_comments = array();
	$cost_comments[1] = substr($row["posclosingassessment_terminationcost01_c"], 0,77);
	$cost_comments[2] = substr($row["posclosingassessment_terminationcost02_c"], 0,77);
	$cost_comments[3] = substr($row["posclosingassessment_terminationcost03_c"], 0,77);
	$cost_comments[4] = substr($row["posclosingassessment_terminationcost04_c"], 0,77);
	$cost_comments[5] = substr($row["posclosingassessment_terminationcost05_c"], 0,77);
	$cost_comments[6] = substr($row["posclosingassessment_terminationcost06_c"], 0,77);
	$cost_comments[7] = substr($row["posclosingassessment_terminationcost07_c"], 0,77);
	$cost_comments[8] = substr($row["posclosingassessment_terminationcost08_c"], 0,77);
	$cost_comments[9] = substr($row["posclosingassessment_terminationcost09_c"], 0,77);

	
	$currency_symbol = "";
	$sql_currency = "select currency_id, currency_symbol " . 
					"from countries " .
					"left join currencies on currency_id = country_currency " . 
					"where country_id = " . $row["country_id"];

	$res_f = mysql_query($sql_currency) or dberror($sql_currency);
	while($row_f = mysql_fetch_assoc($res_f))
	{
		$currency_symbol = $row_f["currency_symbol"];
	}

	$files = array();
	$files[1] = ".." . $row["posclosingassessment_file01"];
	$files[7] = ".." . $row["posclosingassessment_file07"];
	$files[2] = ".." . $row["posclosingassessment_file02"];
	$files[8] = ".." . $row["posclosingassessment_file08"];
	$files[3] = ".." . $row["posclosingassessment_file03"];
	$files[4] = ".." . $row["posclosingassessment_file04"];
	$files[9] = ".." . $row["posclosingassessment_file09"];
	$files[5] = ".." . $row["posclosingassessment_file06"];
	$files[6] = ".." . $row["posclosingassessment_file05"];

	

	$signed_file = ".." . $row["posclosingassessment_filesigned"];

	$approval_names[1] = $row["posclosingassessment_approval_name01"];
	$approval_names[2] = $row["posclosingassessment_approval_name02"];
	$approval_names[3] = $row["posclosingassessment_approval_name03"];
	$approval_names[4] = $row["posclosingassessment_approval_name04"];
	$approval_names[5] = $row["posclosingassessment_approval_name06"];
	$approval_names[6] = $row["posclosingassessment_approval_name05"];

}


//set pdf parameters

$margin_top = 10;
$margin_left = 12;
$y = $margin_top;

if($print_signed_form == false) 
{
	$pdf->SetTitle("Request - Closing of Corporate Retail Store");
	$pdf->SetAuthor(BRAND . " Retail Net");
	$pdf->SetDisplayMode(150);
	$pdf->AddPage();
	$pdf->SetAutoPageBreak(false, 0);


	// Title first line
	$y = $margin_top;
	$pdf->SetXY($margin_left,$margin_top);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(40, 8, "The Swatch Group", 1);

	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(82, 8, "Request - Closing of Corporate Retail Store", 1, "", "C");

	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(25, 8, "SUMMARY", 1, "", "C", false);


	$pdf->Cell(20, 8, date("d.m.Y"), 1, "", "C");


	$pdf->Cell(20, 8, "INR-04", 1, "", "C");

	
	$y = $pdf->getY()+8;
	$pdf->SetXY($margin_left,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(40, 0, "Legal Entity", 1, "", "L");
	$pdf->Cell(147, 0, $legal_entity, 1, "L");
	$pdf->Ln();
	$y = $pdf->getY();

	$pdf->SetXY($margin_left,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(40, 0, "Name of the Store", 1, "", "L");
	$pdf->Cell(147, 0, BRAND . ": " . $project_name1 . ', ' . $project_name2, 1, "L");
	$pdf->Ln();

	//$pdf->SetFont("arialn", "", 9);
	//$y = $pdf->getY()-10;
	//$pdf->SetXY($margin_left+40,$y);
	//$pdf->MultiCell(147, 5, BRAND . ": " . $project_name1 . ', ' . $project_name2, 0, "L");

	//$y = $pdf->getY();
	//$pdf->SetXY($margin_left+40,$y);
	//$pdf->MultiCell(147, 5, $project_name2, 0,  "L", false, 0);
	
	$pdf->SetFont("arialn", "B", 8);
	$y = $pdf->getY();
	$pdf->SetXY($margin_left,$y);
	$pdf->MultiCell(40, 20, "Reason for Termination", 1, "L", false, 0);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(82, 20, $reason, 1, "L", false, 0, "", "", true, 0, false, true, 20);

	$pdf->SetFont("arialn", "B", 8);
	$pdf->SetXY($margin_left+122,$y);
	$pdf->MultiCell(25, 5, "Landlord", 1, "L", false, 0);
	$pdf->SetXY($margin_left+122,$y+5);
	$pdf->MultiCell(25, 5, "Currency", 1, "L", false, 0);
	$pdf->SetXY($margin_left+122,$y+10);
	$pdf->MultiCell(25, 5, "Requested Closing", 1, "L", false, 0);

	$pdf->SetFont("arialn", "", 9);
	$pdf->SetXY($margin_left+147,$y);
	$pdf->MultiCell(40, 5, $landlord, 1, "L", false, 0, "", "", true, 0, false, true, 5);
	$pdf->SetXY($margin_left+147,$y+5);
	$pdf->MultiCell(40, 5, $currency_symbol, 1, "L", false, 0);
	$pdf->SetXY($margin_left+147,$y+10);
	$pdf->MultiCell(40, 5, $planned_closinddate, 1, "L", false, 1);


	$pdf->SetFont("arialn", "B", 8);
	$y = $pdf->getY()+5;
	$pdf->SetXY($margin_left,$y);
	$pdf->MultiCell(100, 5, "Initial Investment (k" . $currency_symbol . ")", 1, "L", false, 0);
	
	$pdf->SetFont("arialn", "", 9);
	$y = $pdf->getY();
	$pdf->SetXY($margin_left+100,$y);
	$pdf->MultiCell(22, 5, $initial_investment, 1, "L", false, 0);

	$pdf->SetFont("arialn", "B", 8);
	$y = $pdf->getY()-5;
	$pdf->SetXY($margin_left+122,$y);
	$pdf->MultiCell(25, 5, "Opening Date", 1, "L", false, 0);
	$pdf->SetFont("arialn", "", 9);
	$y = $pdf->getY();
	$pdf->SetXY($margin_left+147,$y);
	$pdf->MultiCell(40, 5, $opening_date, 1, "L", false, 1);

	$pdf->SetFont("arialn", "B", 8);
	$y = $pdf->getY()+5;
	$pdf->SetXY($margin_left,$y);
	$pdf->MultiCell(100, 5, "Projected Book Value (k" . $currency_symbol . ")", 1, "L", false, 0);
	$pdf->SetFont("arialn", "", 9);
	$y = $pdf->getY();
	$pdf->SetXY($margin_left+100,$y);
	$pdf->MultiCell(22, 5, $book_value, 1, "L", false, 0);

	$pdf->SetFont("arialn", "B", 8);
	$y = $pdf->getY()-5;
	$pdf->SetXY($margin_left+122,$y);
	$pdf->MultiCell(25, 5, "End of Lease", 1, "L", false, 0);
	$pdf->SetFont("arialn", "", 9);
	$y = $pdf->getY();
	$pdf->SetXY($margin_left+147,$y);
	$pdf->MultiCell(40, 5, $lease_enddate, 1, "L", false, 1);

	$pdf->SetFont("arialn", "B", 8);
	$y = $pdf->getY()+5;
	$pdf->SetXY($margin_left,$y);
	$pdf->MultiCell(100, 5, "Projected Termination costs incl. FA (k" . $currency_symbol . ")", 1, "L", false, 0);
	
	$pdf->SetFont("arialn", "", 9);
	$y = $pdf->getY();
	$pdf->SetXY($margin_left+100,$y);
	$pdf->MultiCell(22, 5, $termination_costs, 1, "L", false, 0);

	$pdf->SetFont("arialn", "B", 8);
	$y = $pdf->getY()-5;
	$pdf->SetXY($margin_left+122,$y);
	$pdf->MultiCell(25, 10, "Years of depreciation left", 1, "L", false, 0);
	$pdf->SetFont("arialn", "", 9);
	$y = $pdf->getY();
	$pdf->SetXY($margin_left+147,$y);
	$pdf->MultiCell(40, 10, $depr_years, 1, "L", false, 1);

	$pdf->SetFont("arialn", "B", 8);
	$y = $pdf->getY();
	$pdf->SetXY($margin_left,$y);
	$pdf->MultiCell(100, 5, "Projected Rents Costs Remaining Lease Period (k" . $currency_symbol . ") not Including Closing Costs", 1, "L", false, 0);

	$pdf->SetFont("arialn", "", 9);
	$y = $pdf->getY();
	$pdf->SetXY($margin_left+100,$y);
	$pdf->MultiCell(22, 5, $rental_costs, 1, "L", false, 0);
	$pdf->Ln();

	$y = $pdf->getY();
	$pdf->SetXY($margin_left,$y);
	$pdf->SetFont("arialn", "B", 8);
	$y = $pdf->getY();
	$pdf->SetXY($margin_left,$y);
	$pdf->MultiCell(100, 5, "Projected Operating Loss Remaining Lease Period (k" . $currency_symbol . ") not Including Closing Costs", 1, "L", false, 0);
	
	$pdf->SetFont("arialn", "", 9);
	$y = $pdf->getY();
	$pdf->SetXY($margin_left+100,$y);
	$pdf->MultiCell(22, 5, $operating_loss, 1, "L", false, 0);

	$pdf->SetFont("arialn", "B", 8);
	$y = $pdf->getY()-5;
	$pdf->SetXY($margin_left+122,$y);
	$pdf->MultiCell(25, 10, "Including WS Margin (k" . $currency_symbol . ")", 1, "L", false, 0);
	$pdf->SetFont("arialn", "", 9);
	$y = $pdf->getY();
	$pdf->SetXY($margin_left+147,$y);
	$pdf->MultiCell(40, 10, $operating_loss_ws, 1, "L", false, 1);


	
	
	//financial Data
	$x = $margin_left+1;
	$y = $y+12;
	$pdf->SetXY($x-1,$y);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->MultiCell(77, 10, "1. Sales - Historic + FC (in k" . $currency_symbol . ")", 1, "L", false, 0);
	
	$pdf->SetFont("arialn", "B", 9);
	$pdf->MultiCell(22, 10, $year-3, 1, "R", false, 0);
	$pdf->MultiCell(22, 10, $year-2, 1, "R", false, 0);
	$pdf->MultiCell(22, 10, $year-1, 1, "R", false, 0);
	$pdf->MultiCell(22, 10, "Continuing \nFC " . $year, 1, "R", false, 0);
	$pdf->MultiCell(22, 10, "Termination \nFC " . $year2, 1, "R", false, 1);

	$x = $margin_left;
	$y = $y+10;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(77, 5, "Gross Sales", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(22, 5, number_format($financial_data[1][1], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[1][2], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[1][3], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[1][4], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[1][5], 0, '', "'"), 1, "", "R");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(77, 5, "Net Sales", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(22, 5, number_format($financial_data[2][1], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[2][2], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[2][3], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[2][4], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5,number_format( $financial_data[2][5], 0, '', "'"), 1, "", "R");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(77, 5, "COGS", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(22, 5, number_format($financial_data[3][1], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[3][2], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[3][3], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[3][4], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[3][5], 0, '', "'"), 1, "", "R");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(77, 5, "Depreciation of Fixed Assets", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(22, 5, number_format($financial_data[4][1], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[4][2], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[4][3], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[4][4], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[4][5], 0, '', "'"), 1, "", "R");


	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(77, 5, "Indirect salaries & social benefits", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(22, 5, number_format($financial_data[5][1], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[5][2], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[5][3], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[5][4], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[5][5], 0, '', "'"), 1, "", "R");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(77, 5, "Rents, depreciation key money", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(22, 5, number_format($financial_data[6][1], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[6][2], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[6][3], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[6][4], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[6][5], 0, '', "'"), 1, "", "R");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(77, 5, "Other operating expenses", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(22, 5, number_format($financial_data[7][1], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[7][2], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[7][3], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[7][4], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[7][5], 0, '', "'"), 1, "", "R");


	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(77, 5, "Operating Profit", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(22, 5, number_format($financial_data[8][1], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[8][2], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[8][3], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[8][4], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[8][5], 0, '', "'"), 1, "", "R");


	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(77, 5, "Operating Profit (including WS Margin)", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(22, 5, number_format($financial_data[9][1], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[9][2], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[9][3], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[9][4], 0, '', "'"), 1, "", "R");
	$pdf->Cell(22, 5, number_format($financial_data[9][5], 0, '', "'"), 1, "", "R");




	//Termination Costs
	$x = $margin_left+1;
	$y = $y+8;
	$pdf->SetXY($x-1,$y);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->MultiCell(187, 6, "2. Total Termination Costs (in k" . $currency_symbol . ")", 1, "L", false, 0);
	
	

	$x = $margin_left;
	$y = $y+6;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(77, 5, "Unamortized Fixed Assets", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(22, 5, number_format($cost[1], 0, '', "'"), 1, "", "R");
	$pdf->Cell(88, 5, $cost_comments[1], 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(77, 5, "Redundancy Costs", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(22, 5, number_format($cost[7], 0, '', "'"), 1, "", "R");
	$pdf->Cell(88, 5, $cost_comments[7], 1, "", "L");
	
	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(77, 5, "Penalties", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(22, 5, number_format($cost[2], 0, '', "'"), 1, "", "R");
	$pdf->Cell(88, 5, $cost_comments[2], 1, "", "L");

	// form before 2020
	if ($cost[3] > 0) {
		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(77, 5, "Broker Fees", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(22, 5, number_format($cost[3], 0, '', "'"), 1, "", "R");
		$pdf->Cell(88, 5, $cost_comments[3], 1, "", "L");
	}


	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(77, 5, "Rent Contribution New Tenant", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(22, 5, number_format($cost[8], 0, '', "'"), 1, "", "R");
	$pdf->Cell(88, 5, $cost_comments[2], 1, "", "L");


	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(77, 5, "Dismantling Costs", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(22, 5, number_format($cost[4], 0, '', "'"), 1, "", "R");
	$pdf->Cell(88, 5, $cost_comments[4], 1, "", "L");


	// form before 2020
	if ($cost[6] > 0) {
		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(77, 5, "HR Costs", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(22, 5, number_format($cost[6], 0, '', "'"), 1, "", "R");
		$pdf->Cell(88, 5, $cost_comments[6], 1, "", "L");
	}

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(77, 5, "Deposit (in case deposit is lost)", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(22, 5, number_format($cost[5], 0, '', "'"), 1, "", "R");
	$pdf->Cell(88, 5, $cost_comments[5], 1, "", "L");


	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(77, 5, "Other", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(22, 5, number_format($cost[5], 0, '', "'"), 1, "", "R");
	$pdf->Cell(88, 5, $cost_comments[5], 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(77, 5, "Total", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(22, 5, number_format($cost["t"], 0, '', "'"), 1, "", "R");
	$pdf->Cell(88, 5, "", 1, "", "L");


	//Lageal Conseqeunces
	$x = $margin_left+1;
	$y = $y+8;
	$pdf->SetXY($x-1,$y);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->MultiCell(187, 6, "3. Consequences of closing", 1, "L", false, 1);

	
	$x = $margin_left;
	$y = $y+6;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->MultiCell(30, 0, 'Image', 1, "L", false, 0, "", "", true, 0, false, true, 20);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(157, 0, $image_consequences, 1, "L", false, 1, "", "", true, 0, false, true, 20);
	
	$pdf->SetX($x);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->MultiCell(30, 0, 'Sales/Distribution', 1, "L", false, 0, "", "", true, 0, false, true, 20);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(157, 0, $sales_consequences, 1, "L", false, 1, "", "", true, 0, false, true, 20);

	$pdf->SetX($x);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->MultiCell(30, 0, 'Legal', 1, "L", false, 0, "", "", true, 0, false, true, 20);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(157, 0, $legal_consequences, 1, "L", false, 1, "", "", true, 0, false, true, 20);

	$pdf->SetX($x);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->MultiCell(30, 0, 'Furniture', 1, "L", false, 0, "", "", true, 0, false, true, 20);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(157, 0, $furniture_consequences, 1, "L", false, 1, "", "", true, 0, false, true, 20);

		
	//Signature
	$x = $margin_left+1;
	$y = $pdf->getY()+2;
	$pdf->SetXY($x-1,$y);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->MultiCell(187, 6, "4. Signatures", 1, "L", false, 1);

	$x = $margin_left;

	//title bar 1
	$y = $y+6;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 10);
	$pdf->Cell(187,6, "Brand", 1, "", "L");
	
	$y = $y+6;
	$pdf->SetXY($x,$y);
	
	$pdf->SetFont("arialn", "B", 9);
	$pdf->MultiCell(40,10,"CEO\n" . $approval_names[1], 1, "L", false, 0);

	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(80,10,"Remarks:", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->Cell(20,10,"Date:", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->Cell(47,10,"Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');


	$y = $y+10;
	$pdf->SetXY($x,$y);
	
	$pdf->SetFont("arialn", "B", 9);
	$pdf->MultiCell(40,10,"Vp Finance\n" . $approval_names[2], 1, "L", false, 0);

	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(80,10,"Remarks:", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->Cell(20,10,"Date:", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->Cell(47,10,"Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');

	$y = $y+10;
	$pdf->SetXY($x,$y);
	
	$pdf->SetFont("arialn", "B", 9);
	$pdf->MultiCell(40,10,"VP Sales\n" . $approval_names[3], 1, "L", false, 0);

	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(80,10,"Remarks:", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->Cell(20,10,"Date:", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->Cell(47,10,"Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');



	//title bar 1
	$y = $y+10;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 10);
	$pdf->Cell(187,6, "Country", 1, "", "L");
	
	$y = $y+6;
	$pdf->SetXY($x,$y);
	
	$pdf->SetFont("arialn", "B", 9);
	$pdf->MultiCell(40,10,"Country Manager\n" . $approval_names[4], 1, "L", false, 0);

	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(80,10,"Remarks:", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->Cell(20,10,"Date:", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->Cell(47,10,"Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');


	$y = $y+10;
	$pdf->SetXY($x,$y);
	
	$pdf->SetFont("arialn", "B", 9);
	$pdf->MultiCell(40,10,"Finance Manager\n" . $approval_names[6], 1,  "L", false, 0);

	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(80,10,"Remarks:", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->Cell(20,10,"Date:", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->Cell(47,10,"Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');

	$y = $y+10;
	$pdf->SetXY($x,$y);
	
	$pdf->SetFont("arialn", "B", 9);
	$pdf->MultiCell(40,10,"Brand Manager\n" . $approval_names[5], 1, "L", false, 0);

	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(80,10,"Remarks:", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->Cell(20,10,"Date:", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->Cell(47,10,"Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');



	

}

	


$pdf_data_was_generated = true;

if($print_booklet == true)
{
	foreach($files as $key=>$filename)
	{
		$source_file = $filename;
		if(file_exists($source_file))
		{
			//PDF
			if(substr($source_file, strlen($source_file)-3, 3) == "pdf" or substr($source_file, strlen($source_file)-3, 3) == "PDF")
			{
				if($pdf_data_was_generated == true)
				{
					$pdfString = $pdf->output('', 'S');
					$tmp = SetaPDF_Core_Document::loadByString($pdfString);
				}
				
				if(!isset($merger)) {
					$merger = new SetaPDF_Merger();
				}
				if($pdf_data_was_generated == true)
				{
					$merger->addDocument($tmp);
					$pdf_data_was_generated = false;
				}

				$merger->addFile(array(
					'filename' => $source_file,
					'copyLayers' => true
				));

				$pdf = new TCPDF("P", "mm", "A4", true, 'UTF-8', false);
				$pdf->SetMargins(10, 23, 10);
				$pdf->setPrintHeader(false);

				$pdf->AddFont('arialn');
				$pdf->AddFont('arialn', 'B');
				$pdf->AddFont('arialn','');
				$pdf->AddFont('arialn','B');
				$pdf->AddFont('arialn','I');
				$pdf->AddFont('arialn','BI');

				$PDFmerger_was_used = true;
				$pdf_data_was_generated = false;
			}
		}
	}

}





$file_name = 'request_for_retail_store_closing_' . str_replace(" ", "_", $project_name1) . '.pdf';
if($PDFmerger_was_used == true)
{

	if($pdf_data_was_generated == true)
	{
		$pdfString = $pdf->output('', 'S');
		$tmp = SetaPDF_Core_Document::loadByString($pdfString);
	}
	
	if(!isset($merger)) {
		$merger = new SetaPDF_Merger();
	}
	if($pdf_data_was_generated == true)
	{
		$merger->addDocument($tmp);
	}
	
	$merger->merge();
	$document = $merger->getDocument();

	$document->setWriter(new SetaPDF_Core_Writer_Http($file_name, true));
	$document->save()->finish();
}
else
{
	$pdf->Output($file_name, 'I');
}
?>