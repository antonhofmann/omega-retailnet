<?php
/********************************************************************

    query_posdetails.php.php

    Generate Excel-File of POS Details

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-06-09
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-06-09
    Version:        1.1.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_view_posindex");



/********************************************************************
    prepare all data needed
*********************************************************************/
$user = get_user(user_id());
if(has_access("can_edit_posindex") or has_access("can_view_posindex"))
{
	// create sql for listboxes
	$sql_product_lines = "select product_line_id, product_line_name ".
						 "from product_lines " . 
						 "where product_line_posindex = 1 " . 
						 "   order by product_line_name";


	$sql_product_line_subclasses = "select productline_subclass_id, productline_subclass_name ".
						 "from productline_subclasses " . 
						 "   order by productline_subclass_name";

	$sql_project_kinds = "select projectkind_id, projectkind_name ".
						 "from projectkinds " .
						 "where projectkind_id > 0 " .
						 "order by projectkind_name";

	$sql_subclasse = "select possubclass_id, possubclass_name ".
						 "from possubclasses " .
						 "order by possubclass_name";

	$sql_pos_types = "select postype_id, postype_name ".
						 "from postypes " .
						 "order by postype_name";


	$sql_product_costtypes = "select project_costtype_id, project_costtype_text ".
							 "from project_costtypes " .
							 "where project_costtype_id IN(1,2,6) " . 
							 "order by project_costtype_text";


	$sql_salesregion = "select salesregion_id, salesregion_name ".
				   "from salesregions order by salesregion_name";


	$sql_country = "select DISTINCT country_id, country_name ".
				   "from posaddresses " . 
				   "inner join countries on country_id = posaddress_country " . 
				   "order by country_name";

	$sql_franchisees = "select DISTINCT posaddress_franchisee_id, concat(country_name, ', ', address_company) as company  " .
					   "from posaddresses " . 
					   "left join addresses on address_id = posaddress_franchisee_id ".
					   "left join countries on country_id = address_country " .
					   "where posaddress_franchisee_id > 0 ".
					   "order by country_name, address_company";

	$sql_clients = "select DISTINCT posaddress_client_id, concat(country_name, ', ', address_company) as company  " .
					   "from posaddresses " . 
					   "left join addresses on address_id = posaddress_client_id ".
					   "left join countries on country_id = address_country " .
					   "where posaddress_client_id > 0 ".
					   "order by country_name, address_company";
}
else
{

	$sql_franchisees = "select DISTINCT posaddress_franchisee_id, concat(country_name, ', ', address_company) as company  " .
					   "from posaddresses " . 
					   "left join addresses on address_id = posaddress_franchisee_id ".
					   "left join countries on country_id = address_country " .
					   "where posaddress_franchisee_id > 0 " .
		               "   and posaddress_client_id = " . $user["address"] .
					   " order by country_name, address_company";

	$sql_country = "select DISTINCT country_id, country_name ".
				   "from posaddresses " . 
				   "inner join countries on country_id = posaddress_country " . 
		           "where posaddress_country = " . $user["country"] . 
				   " order by country_name";

}

/*
$sql_store_coordinators = "select user_id, concat(user_name, ' ', user_firstname) as username " . 
                          "from users " . 
						  "left join user_roles on user_role_user = user_id " . 
						  "where user_role_role = 3 or user_role_role = 10 " .
						  " order by username";

$sql_open = "select distinct YEAR(project_actual_opening_date) as year " .
            "from projects " .
            "where YEAR(project_actual_opening_date) > 0 ".
            "order by YEAR(project_actual_opening_date)";

$sql_closed = "select distinct YEAR(project_shop_closingdate) as year " .
              "from projects " .
              "where YEAR(project_shop_closingdate) > 0 ".
              "order by YEAR(project_shop_closingdate)";

*/



if(isset($_POST["show_benchmark"]) and $_POST["show_benchmark"] == 1)
{
    //Franchisee
	$FR = "";
	if($_POST["L9"])
    {
        $FR = $_POST["L9"];
    }

	//Client
	$CL = "";
	if($_POST["L10"])
    {
        $CL = $_POST["L10"];
    }

	//local Production
	$LOPR = "";
	if($_POST["LOPR"])
    {
        $LOPR = $_POST["LOPR"];
    }
	


	//Product Lines
    $PL = "";

    if($_POST["PL_0"])
    {
        $PL = "";
    }
    else
    {
        $res = mysql_query($sql_product_lines) or dberror($sql_product_lines);
        while($row = mysql_fetch_assoc($res))
        {
            if($_POST["PL_" . $row["product_line_id"]])
            {
                $PL .= $row["product_line_id"] . "-";
            }
        }
    }

	//Product Line subclasses
    $PLS = "";

    if($_POST["PLS_0"])
    {
        $PLS = "";
    }
    else
    {
        $res = mysql_query($sql_product_line_subclasses) or dberror($sql_product_line_subclasses);
        while($row = mysql_fetch_assoc($res))
        {
            if($_POST["PLS_" . $row["productline_subclass_id"]])
            {
                $PLS .= $row["productline_subclass_id"] . "-";
            }
        }
    }


	//Product Kindes
    $PK = "";
    if($_POST["PK_0"])
    {
        $PK = "";
    }
    else
    {
        $res = mysql_query($sql_project_kinds) or dberror($sql_project_kinds);
        while($row = mysql_fetch_assoc($res))
        {
            if($_POST["PK_" . $row["projectkind_id"]])
            {
                $PK .= $row["projectkind_id"] . "-";
            }
        }
    }


	//POS Type Subclasses
    $SK = "";
    if($_POST["SK_0"])
    {
        $SK = "";
    }
    else
    {
        $res = mysql_query($sql_subclasse) or dberror($sql_subclasse);
        while($row = mysql_fetch_assoc($res))
        {
            if($_POST["SK_" . $row["possubclass_id"]])
            {
                $SK .= $row["possubclass_id"] . "-";
            }
        }
    }

    //Product Types
    $PT = "";

    if($_POST["PT_0"])
    {
        $PT = "";
    }
    else
    {
        $res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
        while($row = mysql_fetch_assoc($res))
        {
            if($_POST["PT_" . $row["postype_id"]])
            {
                $PT .=$row["postype_id"] . "-";
            }
        }
    }

	

    //Product Legale Types
    $PCT = "";

    if($_POST["PCT_0"])
    {
        $PCT = "";
    }
    else
    {
        $res = mysql_query($sql_product_costtypes) or dberror($sql_product_costtypes);
        while($row = mysql_fetch_assoc($res))
        {
            if($_POST["PCT_" . $row["project_costtype_id"]])
            {
                $PCT .=$row["project_costtype_id"] . "-";
            }
        }
    }

    //salesregions
    $RE = "";

    if($_POST["RE_0"])
    {
        $RE = "";
    }
    else
    {
        $res = mysql_query($sql_salesregion) or dberror($sql_salesregion);
        while($row = mysql_fetch_assoc($res))
        {
            if($_POST["RE_" . $row["salesregion_id"]])
            {
                $RE .=$row["salesregion_id"] . "-";
            }
        }
    }


    //Countries
    $CO = "";

    if($_POST["CO_0"])
    {
        $CO = "";
    }
    else
    {
        $res = mysql_query($sql_country) or dberror($sql_country);
        while($row = mysql_fetch_assoc($res))
        {
            if($_POST["CO_" . $row["country_id"]] == 1)
            {
				$CO .=$row["country_id"] . "-";
            }
        }
    }


	/*
	//Store Coordinatros
    $SC = "";

    if($_POST["SC_0"])
    {
        $SC = "";
    }
    else
    {
        $res = mysql_query($sql_store_coordinators) or dberror($sql_store_coordinators);
        while($row = mysql_fetch_assoc($res))
        {
            if($_POST["SC_" . $row["user_id"]])
            {
                $SC .=$row["user_id"] . "-";
            }
        }
    }
    */
    
    $link = "query_posdetails_xls.php" .
            "?pt=" . $PT .
		    "&pk=" . $PK .
		    "&sk=" . $SK .
            "&pl=" . $PL .
		    "&pls=" . $PLS .
            "&pct=" . $PCT .
		    "&co=" . $CO .
            "&re=" . $RE .
//		    "&sc=" . $SC. 
		    "&fr=" . $FR . 
	        "&cl=" . $CL . 
			"&lopr=" . $LOPR;

	
	redirect($link);
}

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new form("projects", "projects");

if(has_access("can_edit_posindex") or has_access("can_view_posindex"))
{
	$form->add_list("L10", "Client",	$sql_clients);
	$form->add_list("L9", "Owner Company/Franchisee",	$sql_franchisees);
	$form->add_checkbox("LOPR", "Show only POS with locally produced projects", "", 0, "Local Production");

	$form->add_comment("");
	$form->add_label("L1", "Product Lines", "", "Select the Product Lines to be included in the Report");
	$form->add_checkbox("PL_0", "Every Product Line", false);
	$res = mysql_query($sql_product_lines) or dberror($sql_product_lines);
	while($row = mysql_fetch_assoc($res))
	{
		$form->add_checkbox("PL_" . $row["product_line_id"], $row["product_line_name"], false);
	}


	$form->add_comment("");
	$form->add_label("L1S", "Product Line Subclasses", "", "Select the Product Line Subclasseto be included in the Report");
	$form->add_checkbox("PLS_0", "Every Product Line Subclass", false);
	$res = mysql_query($sql_product_line_subclasses) or dberror($sql_product_line_subclasses);
	while($row = mysql_fetch_assoc($res))
	{
		$form->add_checkbox("PLS_" . $row["productline_subclass_id"], $row["productline_subclass_name"], false);
	}

	$form->add_comment("");
	$form->add_label("L6", "Project Type", "", "Select the Project Type to be included in the Report");
	$form->add_checkbox("PK_0", "Every Project Type", false);
	$res = mysql_query($sql_project_kinds) or dberror($sql_project_kinds);
	while($row = mysql_fetch_assoc($res))
	{
		$form->add_checkbox("PK_" . $row["projectkind_id"], $row["projectkind_name"], false);
	}


	$form->add_comment("");
	$form->add_label("L2", "POS Types", "", "Select the POS Types to be included in the Report");
	$form->add_checkbox("PT_0", "Every POS Type", false);
	$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
	while($row = mysql_fetch_assoc($res))
	{
		$form->add_checkbox("PT_" . $row["postype_id"], $row["postype_name"], false);
	}


	$form->add_comment("");
	$form->add_label("L8", "POS Type Subclass", "", "Select the Subclasses to be included in the Report");
	$form->add_checkbox("SK_0", "Every POS Type Subclass", false);
	$res = mysql_query($sql_subclasse) or dberror($sql_subclasse);
	while($row = mysql_fetch_assoc($res))
	{
		$form->add_checkbox("SK_" . $row["possubclass_id"], $row["possubclass_name"], false);
	}


	$form->add_comment("");
	$form->add_label("L3", "Project Legal Type", "", "Select the Legal Types to be included in the Report");
	$form->add_checkbox("PCT_0", "Every Legal Type", false);
	$res = mysql_query($sql_product_costtypes) or dberror($sql_product_costtypes);
	while($row = mysql_fetch_assoc($res))
	{
		$form->add_checkbox("PCT_" . $row["project_costtype_id"], $row["project_costtype_text"], false);
	}

	$form->add_comment("");
	$form->add_label("L4", "Geographical Region", "", "Select the Geographical Regions to be included in the Report");
	$res = mysql_query($sql_salesregion) or dberror($sql_salesregion);
	$form->add_checkbox("RE_0", "Every Geographical Region", false);
	while($row = mysql_fetch_assoc($res))
	{
		$form->add_checkbox("RE_" . $row["salesregion_id"], $row["salesregion_name"], false);
	}

	$form->add_comment("");
	$form->add_label("L5", "Country", "", "Select the Countries to be included in the Report");
	$form->add_checkbox("CO_0", "Every Country", false);
	$res = mysql_query($sql_country) or dberror($sql_country);
	while($row = mysql_fetch_assoc($res))
	{
		$form->add_checkbox("CO_" . $row["country_id"], $row["country_name"], false);
	}
}
else
{
	$form->add_hidden("L10", $user["address"]);

	$form->add_list("L9", "Owner Company/Franchisee",	$sql_franchisees);

	$form->add_hidden("CO_0", 1);
	$form->add_hidden("PL_0", 1);
	$form->add_hidden("PK_0", 1);
	$form->add_hidden("PT_0", 1);
	$form->add_hidden("SK_0", 1);
	$form->add_hidden("PCT_0", 1);
	$form->add_hidden("RE_0", 1);
	$form->add_hidden("CO_0", 1);

}
/*
$form->add_comment("");
$form->add_label("L7", "Store Coordinator", "", "Select the Coordinators to be included in the Report");
$form->add_checkbox("SC_0", "Every Store Coordinator", false);
$res = mysql_query($sql_store_coordinators) or dberror($sql_store_coordinators);
while($row = mysql_fetch_assoc($res))
{
    $form->add_checkbox("SC_" . $row["user_id"], $row["username"], false);
}
*/


//$form->add_list("country_id", "Country", $sql_country, SUBMIT);

//$form->add_section("Actual Opening Date");
//$form->add_list("odf", "Opened From", $sql_open, SUBMIT);
//$form->add_list("odt", "Opened To", $sql_open, SUBMIT);


//$form->add_section("Closing Date");
//$form->add_list("cdf", "Closed From", $sql_closed, SUBMIT);
//$form->add_list("cdt", "Closed To", $sql_closed, SUBMIT);

$form->add_hidden("show_benchmark", 1, 0);
$form->add_button("benchmark_xls", "Generate List", "");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Process buttons
*********************************************************************/ 

/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page("queries");


require_once "../include/xls/Writer.php";

$page->header();
$page->title("POS Details");

$form->render();

$page->footer();
