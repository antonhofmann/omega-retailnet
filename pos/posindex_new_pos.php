<?php
/********************************************************************

    posindex_new_pos.php

    Creation of a new POS.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2013-06-16
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2013-06-16
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

if(has_access("can_edit_posindex") or has_access("can_edit_his_posindex"))
{
}
else
{
	redirect("noaccess.php");
}


set_referer("posprojects.php");


$user = get_user(user_id());

$has_project = update_posdata_from_posorders(param("pos_id"));

$ratings = array();
$ratings[5] = "bad";
$ratings[4] = "poor";
$ratings[3] = "good";
$ratings[2] = "very good";
$ratings[1] = "excellent";
//$ratings[0] = "n.a.";


$sql_floors = "select floor_id, floor_name " . 
		     "from floors " . 
		      " order by  floor_id";

$floors = array();
$res = mysql_query($sql_floors) or dberror($sql_floors);
while ($row = mysql_fetch_assoc($res))
{
	if($row["floor_id"] == 6)
	{
		$floors[$row["floor_id"]] = $row["floor_name"] . '<img src="/pictures/car.png" />';
	}
	else
	{
		$floors[$row["floor_id"]] = $row["floor_name"];
	}
}


// Build form
$form = new Form("posaddresses", "posaddress");

$form->add_section("Roles");

if(has_access("can_edit_posindex"))
{
	$sql_clients = "select address_id, concat(country_name, ': ', address_company, ', ', address_place, ' [', address_shortcut, ']') as company from addresses left join countries on country_id = address_country where (address_active = 1 and (address_type = 1 or address_type = 4)) or address_id = {posaddress_client_id} order by country_name, address_company";


	$sql_franchisees = "select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where address_showinposindex = 1 and country_name <> '' and address_company <> '' and address_place <> '' and (address_canbefranchisee = 1 or address_canbefranchisee_worldwide = 1) order by country_name, address_company";

	$sql_franchisors = "select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where address_showinposindex = 1 and country_name <> '' and address_company <> '' and address_place <> '' and address_canbefranchisor = 1 order by country_name, address_company";
}
else
{
	$user = get_user(user_id());

	$country_filter = "";
	$tmp = array();
	$sql = "select * from country_access " .
		   "where country_access_user = " . user_id();


	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{            
		$tmp[] = $row["country_access_country"];
	}

	if(count($tmp) > 0) {
		$country_filter = " or country_id IN (" . implode(",", $tmp) . ") ";
	}

	if($country_filter == "")
	{
		$sql_clients = "select address_id, " . 
			"concat(country_name, ': ', address_company, ', ', address_place, ' [', address_shortcut, ']') as company " . 
			" from addresses " . 
			" left join countries on country_id = address_country " . 
			" where address_active = 1 and (address_type = 1 or address_type = 4)  " .
			" and address_id =  " . $user["address"] . 
		    " order by country_name, address_company";


		$sql_franchisees = "select address_id, " .
			               " concat(country_name, ': ', address_company, ', ', address_place) as company " .
			               " from addresses " .
			               " left join countries on country_id = address_country " . 
			               " where address_active = 1  " . 
			               " and (address_canbefranchisee = 1 or address_canbefranchisee_worldwide = 1) " .
			               " and (address_parent =  " . $user["address"] . 
			               "  or address_id = " . $user["address"] . ") " . 
			               " order by country_name, address_company";

		$sql_franchisors = "select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where address_showinposindex = 1 and country_name <> '' and address_company <> '' and address_place <> '' and address_canbefranchisor = 1 order by country_name, address_company";
	}
	else
	{
		
		$sql_clients = "select address_id, concat(country_name, ': ', address_company, ', ', address_place, ' [', address_shortcut, ']') as company " . 
			" from addresses " . 
			"left join countries on country_id = address_country " . 
			" where address_active = 1 and (address_type = 1 or address_type = 4)  " .
			" and (address_id =  " . $user["address"] . " " . $country_filter . " ) " .
		    " order by country_name, address_company";


		$sql_franchisees = "select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company " .
			               " from addresses " . 
			               " left join countries on country_id = address_country " . 
			               " where address_showinposindex = 1 " . 
			               " and (address_canbefranchisee = 1 or address_canbefranchisee_worldwide = 1) " .
			               " and (address_id =  " . $user["address"] . " " . $country_filter . " ) " . 
			               " order by country_name, address_company";

		$sql_franchisors = "select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where address_showinposindex = 1 and country_name <> '' and address_company <> '' and address_place <> '' and address_canbefranchisor = 1 order by country_name, address_company";
	}
}



$sql_provinces = "select province_id, province_canton " . 
                 " from provinces " . 
				 " where province_country = " . dbquote(param('posaddress_country')) .
				 " order by province_canton";

if(param("posaddress_country"))
{
	$sql_places = "select place_id, concat(place_name, ' (', province_canton, ')') as place " . 
				  "from places " .
				  "left join provinces on province_id = place_province " . 
				  "where place_country = " . dbquote(param("posaddress_country")) . " order by place_name";
}
else
{

	$sql_places = "select place_id, concat(place_name, ' (', province_canton, ')') as place " . 
				  "from places " .
				  "left join provinces on province_id = place_province " . 
				   "where place_country = " . dbquote($user['country']) . " order by place_name";
}

$places = array();

$res = mysql_query($sql_places) or dberror($sql_places);
while ($row = mysql_fetch_assoc($res))
{
	$places[$row['place_id']] = $row['place'];
}

if(param("posaddress_country")) {
	$places['999999999999999999999'] = "New City not listed above";
}

$form->add_list("posaddress_client_id", "Client*", $sql_clients	, NOTNULL);



$form->add_list("posaddress_ownertype", "Legal Type*",
	"select project_costtype_id, project_costtype_text from project_costtypes where project_costtype_id IN (1, 2, 6)", SUBMIT | NOTNULL);


$form->add_edit("posaddress_eprepnr", "Enterprise Reporting Number");
$form->add_edit("posaddress_sapnumber", "SAP Number");


$form->add_list("posaddress_franchisor_id", "Franchisor", $sql_franchisors);

if(param("posaddress_ownertype") != 6)
{
	$tmp = "Franchisee*";
}
else
{
	$tmp = "Owner Company*";
}


$form->add_list("posaddress_franchisee_id", $tmp, $sql_franchisees, NOTNULL);


$form->add_list("posaddress_store_furniture", "Product Line*",
		"select product_line_id, product_line_name from product_lines where product_line_posindex = 1 order by product_line_name", NOTNULL);

$form->add_list("posaddress_store_postype", "POS Type*",
	"select postype_id, postype_name from postypes order by postype_name", NOTNULL);

$form->add_list("posaddress_store_subclass", "POS Type Subclass",
		"select possubclass_id, possubclass_name from possubclasses order by possubclass_name");



$form->add_section("Address Data");
//$form->add_edit("posaddress_name", "POS Name*", NOTNULL);
$form->add_edit("posaddress_name", "POS Name*", NOTNULL, "", TYPE_CHAR, 0, 0, 2, "posaddress_name");

//$form->add_edit("posaddress_name2", "", 0);
$form->add_edit("posaddress_name2", "", 0, "", TYPE_CHAR, 0, 0, 2, "posaddress_name2");

//$form->add_edit("posaddress_address", "Address*", NOTNULL);
$form->add_edit("posaddress_address", "Address*", NOTNULL, "", TYPE_CHAR, 0, 0, 2, "posaddress_address");

//$form->add_edit("posaddress_address2", "Address 2");
$form->add_edit("posaddress_address2", "Address 2", 0, "", TYPE_CHAR, 0, 0, 2, "posaddress_address2");




$form->add_list("posaddress_country", "Country*",
	"select country_id, country_name from countries order by country_name", NOTNULL | SUBMIT, param("country"));



$form->add_edit("posaddress_zip", "Zip*", NOTNULL);

if(param('posaddress_place_id') == 999999999999999999999) {
		
	$form->add_list("province", "Province*", $sql_provinces, NOTNULL);
	$form->add_edit("new_place", "City Name*", NOTNULL, "", TYPE_CHAR);
	$form->add_hidden("posaddress_place_id", 999999999999999999999);
	$form->add_hidden("posaddress_place", "");
}
else {
	$form->add_list("posaddress_place_id", "City*", $places, NOTNULL | SUBMIT);
	$form->add_edit("posaddress_place", "City", NOTNULL | DISABLED);
	$form->add_hidden("new_place","");
}





$form->add_section("Communication");
$form->add_edit("posaddress_phone", "Phone");
$form->add_edit("posaddress_fax", "Fax");
$form->add_edit("posaddress_email", "Email");


$form->add_edit("posaddress_contact_name", "Contact Name");
$form->add_edit("posaddress_website", "Website");


$form->add_section("Dates");
$form->add_edit("posaddress_store_openingdate", "On what date was the initial opening?", 0, to_system_date(param('posaddress_store_openingdate')), TYPE_DATE);
$form->add_edit("latest_renovation", "On what date was the latest renovation?", 0, to_system_date(param('latest_renovation')), TYPE_DATE, 10);
//$form->add_edit("posaddress_store_closingdate", "Closing Date", 0, "", TYPE_DATE);


$form->add_section("Environment");
$sql = "select posareatype_id, posareatype_name " . 
		   "from posareatypes " . 
		   " order by posareatype_name";

$form->add_checklist("area", "Environment", "posareas",$sql, RENDER_HTML);

$form->add_section("Area Perception");
$form->add_comment("Please rate by clicking a star: 5 stars are the best rating.");

$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, 0, NOTNULL);
$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, 0, NOTNULL);
$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings, 0, NOTNULL);
$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings, 0, NOTNULL);
$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings, 0, NOTNULL);
$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings, 0, NOTNULL);
$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from accross the Street*", $ratings, 0, NOTNULL);



$form->add_section("Website " . BRAND_WEBSITE);
$form->add_checkbox("posaddress_export_to_web", "Show this POS in the Store Locator", "", "", BRAND_WEBSITE);
$form->add_checkbox("posaddress_email_on_web", "Show Email Address in the Store Locator", "", "", BRAND_WEBSITE);

$form->add_section("Address Check");
$form->add_checkbox("posaddress_checked", "Address check", "", "", "Adress is ok");
$form->add_hidden("saved", 1);

$form->add_button("save", "Save");



$form->add_hidden("ltf", param("ltf"));
$form->add_hidden("let", param("let"));
$form->add_hidden("ostate", param("ostate"));


// Populate form and process button clicks

$form->populate();
$form->process();

if($form->button("save"))
{
	if($form->validate())
	{

		if($form->value("new_place")) {
			
			$sql = "insert into places (place_country, place_province, place_name, date_created, user_created) " . 
				   " Values (" . 
				   dbquote($form->value('posaddress_country')) . "," .
				   dbquote($form->value('province')) . "," .
				   dbquote($form->value('new_place')) . "," .
				   dbquote(date("Y-m-d H:i:s")) . "," .
				   dbquote(user_login()) .
				   ")";
		   $res = mysql_query($sql) or dberror($sql);

		   $place_id = mysql_insert_id();
		   $form->value("posaddress_place_id", $place_id);
		   $form->value("posaddress_place", $form->value("new_place"));
		
		}
		
		$form->save();
		
		//check if google map is created
		$sql = "select posaddress_google_precision from posaddresses where posaddress_id = " . id();
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			if(!$row["posaddress_google_precision"])
			{
				$result = google_maps_geo_encode_pos(id(), $context);
			}
		}


		//crate fake projects
		if(id()) {

			if($form->value("posaddress_store_openingdate")) {

				$sql_u = "insert into posorders (";
				$sql_u .= "posorder_posaddress, posorder_type, posorder_ordernumber, posorder_product_line, ";
				$sql_u .= "posorder_postype, posorder_project_kind, posorder_year, ";
				$sql_u .= "posorder_legal_type, posorder_opening_date) VALUES(";
				$sql_u .= id() . ", 1, ";
				$sql_u .= "'1st install', '" . $_POST["posaddress_store_furniture"] . "', ";
				$sql_u .= "'" . $_POST["posaddress_store_postype"] . "', ";
				$sql_u .= "1, '" . substr(from_system_date($_POST["posaddress_store_openingdate"]), 0, 4) . "', ";
				$sql_u .= "'" . $_POST["posaddress_ownertype"] . "', ";
				$sql_u .= "'" . from_system_date($_POST["posaddress_store_openingdate"]) . "') ";
				
				$result = mysql_query($sql_u) or dberror($sql_u);

			}

			if($form->value("latest_renovation")) {

				$sql_u = "insert into posorders (";
				$sql_u .= "posorder_posaddress, posorder_type, posorder_ordernumber, posorder_product_line, ";
				$sql_u .= "posorder_postype, posorder_project_kind, posorder_year, ";
				$sql_u .= "posorder_legal_type, posorder_opening_date) VALUES(";
				$sql_u .= id() . ", 1, ";
				$sql_u .= "'renov', '" . $_POST["posaddress_store_furniture"] . "', ";
				$sql_u .= "'" . $_POST["posaddress_store_postype"] . "', ";
				$sql_u .= "2, '" . substr(from_system_date($_POST["latest_renovation"]), 0, 4) . "', ";
				$sql_u .= "'" . $_POST["posaddress_ownertype"] . "', ";
				$sql_u .= "'" . from_system_date($_POST["latest_renovation"]) . "') ";
				$result = mysql_query($sql_u) or dberror($sql_u);

			}
			
			redirect("posindex_pos.php?country=" . $_POST["posaddress_country"] . "&id=" . id());
		}

		if(id())
		{
			
			//update_store_locator(id());
			//mysql_select_db(RETAILNET_DB, $db);
		}

	}
}
elseif($form->button("posaddress_place_id"))
{
	$sql = "select place_name " .
		   "from places " . 
		   "where place_id = " . dbquote($form->value("posaddress_place_id"));
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$form->value("posaddress_place", $row["place_name"]);
	}
}
elseif($form->button("posaddress_country"))
{
	$form->value("posaddress_place",  "");
	$form->value("posaddress_place_id", 0);
}


// Render page
$poslocation = get_poslocation(id(), "posaddresses");

$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title(id() ? "Basic Data: " . $poslocation["posaddress_name"] : "Add POS Location");

if(id())
{
	require_once("include/tabs.php");
}

$form->render();

?>

<script language="javascript">
	$("#h_posaddress_name").click(function() {
	   $('#posaddress_name').val($('#posaddress_name').val().toLowerCase());
	   var txt = $('#posaddress_name').val();

	   $('#posaddress_name').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	$("#h_posaddress_name2").click(function() {
	   $('#posaddress_name2').val($('#posaddress_name2').val().toLowerCase());
	   var txt = $('#posaddress_name2').val();

	   $('#posaddress_name2').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	$("#h_posaddress_address").click(function() {
	   $('#posaddress_address').val($('#posaddress_address').val().toLowerCase());
	   var txt = $('#posaddress_address').val();

	   $('#posaddress_address').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	$("#h_posaddress_address2").click(function() {
	   $('#posaddress_address2').val($('#posaddress_address2').val().toLowerCase());
	   var txt = $('#posaddress_address2').val();

	   $('#posaddress_address2').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});



	
</script>
<?php
$page->footer();

?>