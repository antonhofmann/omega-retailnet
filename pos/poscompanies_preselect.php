<?php
/********************************************************************

    addresses_preselect.php

    Preselection of POS List

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_use_posindex");
set_referer("posindex.php");


$address_filter = array();
$address_filter["a"] = "All";
$address_filter["f"] = "Franchisees";
$address_filter["o"] = "Independent retailers";



if(array_key_exists("address_filter", $_SESSION) and $_SESSION["address_filter"] > 0)
{
	unset($_SESSION["address_filter"]);
}

//has_access("can_view_his_posindex")

//compose list
if(has_access("can_view_posindex") or has_access("can_edit_posindex"))
{
	$sql = "select DISTINCT country_id, country_name " .
		   "from countries " . 
		   "where country_name is not null " . 
		   "order by country_name";
}
else
{
	$user = get_user(user_id());

	/*
	$sql = "select DISTINCT posaddress_country " .
		   "from posaddresses " . 
		   "where posaddress_client_id = " . $user["address"];

	$num_of_countries = 0;
	$res = mysql_query($sql) or dberror($sql);
	$num_rows = mysql_num_rows($res);
	if($num_rows < 2)
	{
		if($row = mysql_fetch_assoc($res))
		{
			redirect("poscompanies.php?country=" . $row["posaddress_country"]);
		}
	}
	*/


	$country_filter = "";
	$tmp = array();
	$sql = "select * from country_access " .
		   "where country_access_user = " . user_id();


	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{            
		$tmp[] = $row["country_access_country"];
	}

	if(count($tmp) > 0) {
		$country_filter = " country_id IN (" . implode(",", $tmp) . ") ";
	}


	if($country_filter == "")
	{
		$sql = "select DISTINCT country_id, country_name " .
			   "from countries " . 
			   "where country_name is not null " .
			   " and country_id = " . $user["country"] . 
			   " order by country_name";
	}
	else
	{
		$sql = "select DISTINCT country_id, country_name " .
			   "from countries " . 
			   "where country_name is not null " .
			   " and " . $country_filter .
			   " order by country_name";
	}
}

$form = new Form("posaddresses", "posaddress");

$form->add_section("Country Selection");

$form->add_list("country", "Country",$sql);

if(array_key_exists("address_filter", $_SESSION) and $_SESSION["address_filter"] > 0)
{
	$form->add_list("address_filter", "Address Filter", $address_filter, 0, $_SESSION["address_filter"]);
}
else
{
	$form->add_list("address_filter", "Address Filter", $address_filter, 0, "a");
}

$form->add_button("show_pos", "Show List");

$form->populate();
$form->process();


if($form->button("show_pos"))
{
	redirect("poscompanies.php?country=" . $form->value("country") . "&address_filter=" . $form->value("address_filter"));
}

$page = new Page("poscompanies");
require "include/pos_page_actions.php";
$page->header();

$page->title("Companies Active");
$form->render();

?>

	<script type="text/javascript">
		
		document.onkeydown = process_key;
		
		function process_key(e)
		{
		  if( !e ) 
		  {
			if( window.event ) 
			{
			  e = window.event;
			} 
			else 
			{
			  return;
			}
		  }

		  if(e.keyCode==13)
		  {
			  button('show_pos');
		  }
		}
	</script>

	<?php


$page->footer();

?>
