<?php
/********************************************************************

    correct_pos_data_pos.php

    Correct POS Data

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

check_access("can_correct_pos_data");

set_referer("posprojects.php");

if(param("pos_id"))
{
	register_param("pos_id");
	param("id", param("pos_id"));
}
else
{
	register_param("pos_id");
	param("pos_id", id());
}

if(param("pos_id") > 0)
{
	$pos = get_poslocation(param("pos_id"), "_posaddresses");
}

$user = get_user(user_id());

//copy record to table posaddresses_corrected

//sql for subclasses
$sql_subclasses = "select possubclass_id, possubclass_name " .
				  "from postype_subclasses " .
				  "left join possubclasses on possubclass_id = postype_subclass_subclass " .
				  "left join product_line_pos_types on  product_line_pos_type_id = postype_subclass_pl_pos_type " . 
				  "where  product_line_pos_type_pos_type = " . $pos["posaddress_store_postype"] .
				  " and product_line_pos_type_product_line = " . $pos["posaddress_store_furniture"] .
				  " order by possubclass_name";

$ratings = array();
$ratings[5] = "bad";
$ratings[4] = "poor";
$ratings[3] = "good";
$ratings[2] = "very good";
$ratings[1] = "excellent";


//check if user can edit this company
$can_edit = false;
if(has_access("can_edit_posindex"))
{
	$can_edit = true;
}
elseif(has_access("can_edit_his_posindex"))
{
	$can_edit = get_user_edit_permission(user_id(), param("pos_id"));
}

// Build form
$form = new Form("_posaddresses", "posaddress");

$form->add_section("Roles");

$form->add_lookup("posaddress_client_id", "Client", "addresses", "concat(address_company, ', ', address_place, ' [', address_shortcut, ']') as company", 0, $pos["posaddress_client_id"]);

$form->add_lookup("posaddress_ownertype", "Legal Type", "project_costtypes", "project_costtype_text", 0, $pos["posaddress_ownertype"]);
$form->add_lookup("posaddress_store_furniture", "Product Line", "product_lines", "product_line_name");
$form->add_lookup("posaddress_store_furniture_subclass", "Product Line Subclass", "productline_subclasses", "productline_subclass_name");
$form->add_lookup("posaddress_store_postype", "POS Type", "postypes", "postype_name");


if($pos['posaddress_store_postype'] < 4)
{
	$form->add_list("posaddress_store_subclass", "POS Type Subclass", $sql_subclasses);
}
else
{
	$form->add_hidden("posaddress_store_subclass");
}

//$form->add_lookup("posaddress_store_subclass", "POS Type Subclass", "possubclasses", "possubclass_name");

if($pos['posaddress_store_postype'] < 4)
{
	$form->add_lookup("posaddress_franchisor_id", "Franchisor", "addresses", "concat(address_company, ', ', address_place) as company", 0, $pos["posaddress_franchisor_id"]);
}
else
{
	$form->add_hidden("posaddress_franchisor_id");
}


if(has_access("can_view_his_posindex") or has_access("can_edit_his_posindex"))
{
	if($pos['posaddress_store_postype'] < 4)
	{
		$form->add_list("posaddress_franchisee_id", "Franchisee*",
				"select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where address_showinposindex = 1 and country_name <> '' and address_company <> '' and address_place <> '' and (address_canbefranchisee = 1 and (address_parent = " . $user["address"] . " or address_id = " . $user["address"] .")) or (address_canbefranchisee_worldwide = 1 and address_active = 1) order by country_name, address_company", NOTNULL);
	}
	else
	{
		$sql_owners = "select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company " . 
			          "from addresses " . 
			          "left join countries on country_id = address_country " .
			          "where address_showinposindex = 1 " . 
			          " and country_name <> '' and address_company <> '' and address_place <> '' " . 
			          " and address_active = 1 " . 
			          " and address_type = 7 " . 
			          " and (address_parent = " . $user["address"] . " or address_id = " . $user["address"] .") " . 
			          " order by country_name, address_company";
		
		$form->add_list("posaddress_franchisee_id", "Owner Company*", $sql_owners , NOTNULL);
	}
}
else
{
	if($pos['posaddress_store_postype'] < 4)
	{
		$form->add_list("posaddress_franchisee_id", "Franchisee*",
				"select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where address_showinposindex = 1 and country_name <> '' and address_company <> '' and address_place <> '' and address_canbefranchisee = 1 order by country_name, address_company", NOTNULL);
	}
	else
	{
		$sql_owners = "select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company " . 
			          " from addresses " . 
			          " left join countries on country_id = address_country " . 
			          " where address_showinposindex = 1 " . 
			          " and address_active = 1 " . 
					  " and country_name <> '' and address_company <> '' and address_place <> '' " . 
			          " and address_type = 7 order by country_name, address_company";

		$form->add_list("posaddress_franchisee_id", "Owner Company*", $sql_owners , NOTNULL);
	
	}
}

$form->add_section("Address Data");
$form->add_edit("posaddress_name", "POS Name*", NOTNULL);
$form->add_edit("posaddress_name2", "POS Name2");
$form->add_edit("posaddress_address", "Address*", NOTNULL);
$form->add_edit("posaddress_address2", "Address 2");

$form->add_list("posaddress_country", "Country*",
	"select country_id, country_name from countries order by country_name", NOTNULL | SUBMIT);


if(param("posaddress_country"))
{
	$form->add_list("posaddress_place_id", "City Selection*",
			"select place_id, place_name from places where place_country = " . param("posaddress_country") . " order by place_name", NOTNULL | SUBMIT, $pos["posaddress_place_id"]);
}
else
{
	$form->add_list("posaddress_place_id", "City Selection*",
			"select place_id, place_name from places where place_country = " . $pos["posaddress_country"] . " order by place_name", NOTNULL | SUBMIT, $pos["posaddress_place_id"]);
}

$form->add_hidden("posaddress_place", $pos["posaddress_place"]);

$form->add_edit("posaddress_zip", "Zip*", NOTNULL);

$provinces = array();
$provinces[999999999] = "Other province not listed below";

if(param("posaddress_country"))
{
	$sql_provinces = "select province_id, province_canton from provinces where province_country = " . dbquote(param("posaddress_country")) . " order by province_canton";
}
else
{
	$sql_provinces = "select province_id, province_canton from provinces where province_country = " . dbquote($pos["posaddress_country"]) . " order by province_canton";
}
$res = mysql_query($sql_provinces);
while ($row = mysql_fetch_assoc($res))
{
	$provinces[$row["province_id"]] = $row["province_canton"];
}

$form->add_list("select_province", "Province Selection*", $provinces, NOTNULL | SUBMIT, $pos["place_province"]);

if(param("select_province") == 999999999)
{
	$form->add_edit("province", "Province*", NOTNULL, "", TYPE_CHAR, 50, 0, 1, "province");
}




$form->add_section("Communication");
$form->add_edit("posaddress_phone", "Phone*", NOTNULL);
$form->add_edit("posaddress_fax", "Fax");
$form->add_edit("posaddress_email", "Email*", NOTNULL);
//$form->add_edit("posaddress_contact_name", "Contact Name*", NOTNULL);
//$form->add_edit("posaddress_website", "Website");


if($pos['posaddress_store_postype'] < 4)
{
	$form->add_section("Dates");
	$form->add_edit("posaddress_store_openingdate", "Opening Date*", NOTNULL, "", TYPE_DATE);
	$form->add_edit("posaddress_store_closingdate", "Closing Date", 0, "", TYPE_DATE);
}
else
{
	$form->add_section("Sales Agreement");
	$form->add_edit("posaddress_store_openingdate", "Opening Date", 0, "", TYPE_DATE);
	$form->add_edit("posaddress_store_closingdate", "Closing Date", 0, "", TYPE_DATE);
}

$form->add_hidden("country", param("country"));



$form->add_section("Environment");
$sql = "select posareatype_id, posareatype_name " . 
		   "from posareatypes " . 
		   " order by posareatype_name";

$form->add_checklist("area", "Environment*", "_posareas",$sql, RENDER_HTML);

$form->add_section("Area Perception");
$form->add_comment("Please rate by clicking a star: 5 stars are the best rating.");
if(array_key_exists($pos["posaddress_perc_class"], $ratings))
{
	$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, $ratings[$pos["posaddress_perc_class"]], NOTNULL);
	$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, $ratings[$pos["posaddress_perc_tourist"]], NOTNULL);
	$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings,  $ratings[$pos["posaddress_perc_transport"]], NOTNULL);
	$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings,  $ratings[$pos["posaddress_perc_people"]], NOTNULL);
	$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings,  $ratings[$pos["posaddress_perc_parking"]], NOTNULL);
	$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings,  $ratings[$pos["posaddress_perc_visibility1"]], NOTNULL);
	$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from accross the Street*", $ratings,  $ratings[$pos["posaddress_perc_visibility2"]], NOTNULL);
}
else
{
	$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from accross the Street*", $ratings, 0, NOTNULL);
}



if($pos['posaddress_store_postype'] < 4)
{
	$form->add_section("POS Details Surfaces");

	$form->add_edit("posaddress_store_grosssurface", "Gross Surface in sqms*", NOTNULL, "", TYPE_DECIMAL, 8);
	$form->add_edit("posaddress_store_totalsurface", "Total Surface in sqms*", NOTNULL, "", TYPE_DECIMAL, 8);
	$form->add_edit("posaddress_store_retailarea", "Sales Surface in sqms*", NOTNULL, "", TYPE_DECIMAL, 8);
	$form->add_edit("posaddress_store_backoffice", "Back Office Surface in sqms", 0, "", TYPE_DECIMAL, 8);
	$form->add_edit("posaddress_store_numfloors", "Number of Floors*", NOTNULL, "", TYPE_INT);
	$form->add_edit("posaddress_store_floorsurface1", "Floor 1: Surface  in sqms", 0, "", TYPE_DECIMAL, 8);
	$form->add_edit("posaddress_store_floorsurface2", "Floor 2: Surface  in sqms", 0, "", TYPE_DECIMAL, 8);
	$form->add_edit("posaddress_store_floorsurface3", "Floor 3: Surface  in sqms", 0, "", TYPE_DECIMAL, 8);

	$form->add_section("POS Details Staff");
	$form->add_edit("posaddress_store_headcounts", "Headcounts*", NOTNULL, "", TYPE_INT);
	$form->add_edit("posaddress_store_fulltimeeqs", "Full Time Equivalents*", NOTNULL, "", TYPE_DECIMAL, 8);


	
}
else
{
	$form->add_section("POS Details Surfaces");

	$form->add_edit("posaddress_overall_sqms", "Retailers overall sales surface", 0, "", TYPE_DECIMAL, 8);
	$form->add_edit("posaddress_dedicated_sqms_wall", "Sales wall surface dedicated to " . BRAND, 0, "", TYPE_DECIMAL, 8);
	$form->add_edit("posaddress_dedicated_sqms_free", "Sales freestanding surface dedicated to "  . BRAND, 0, "", TYPE_DECIMAL, 8);	
}

$form->add_section("Google Data");

$form->add_edit("posaddress_google_lat", "Latitude*", NOTNULL, "", TYPE_DECIMAL, 20, 15);
$form->add_edit("posaddress_google_long", "Longitude*", NOTNULL, "", TYPE_DECIMAL, 20, 15);


$form->add_section("Website " . BRAND_WEBSITE);
$form->add_checkbox("posaddress_export_to_web", "Show this POS in the Store Locator", "", "", BRAND_WEBSITE);
$form->add_checkbox("posaddress_email_on_web", "Show Email Address in the Store Locator", "", "", BRAND_WEBSITE);


$form->add_section("Address Check");
$form->add_checkbox("posaddress_checked", "POS data are checked and correct", "", "", "POS Data Check");


$form->add_button("save", "Save my Corrections");
$form->add_button("back", "Back");

// Populate form and process button clicks

$form->populate();
$form->process();


if($pos['posaddress_checked'] == 2 and param("posaddress_checked") !=1)
{
	$form->value("posaddress_checked", 0);
}

/********************************************************************
    list leases
*********************************************************************/
// create sql
$sql = "select poslease_id, poslease_lease_type, poslease_landlord_name, poslease_startdate, poslease_enddate, poslease_type_name, " .
       "poslease_nexttermin_date, poslease_anual_rent, poslease_currency, poslease_salespercent, poslease_termination_time, " .
	   "poslease_startdate, poslease_enddate, poslease_extensionoption, poslease_exitoption " .
       "from _posleases " .
	   "left join poslease_types on poslease_type_id = poslease_lease_type ";

$list_filter = "poslease_posaddress = " . param("pos_id");

//get data
$lease_types = array();
$lease_start_dates = array();
$lease_end_dates = array();
$lease_extension_dates = array();
$lease_extit_dates = array();
$lease_termination_times = array();
$lease_currencies = array();
$lease_rents = array();
$lease_salespercent = array();
$sql_g = $sql . " where " . $list_filter;

$res = mysql_query($sql_g);
while ($row = mysql_fetch_assoc($res))
{
	$lease_types[$row["poslease_id"]] = $row["poslease_lease_type"];
	$lease_start_dates[$row["poslease_id"]] = to_system_date($row["poslease_startdate"]);
	$lease_end_dates[$row["poslease_id"]] = to_system_date($row["poslease_enddate"]);
	$lease_extension_dates[$row["poslease_id"]] = to_system_date($row["poslease_extensionoption"]);
	$lease_extit_dates[$row["poslease_id"]] = to_system_date($row["poslease_exitoption"]);
	$lease_termination_times[$row["poslease_id"]] = $row["poslease_termination_time"];
	$lease_currencies[$row["poslease_id"]] = $row["poslease_currency"];
	$lease_rents[$row["poslease_id"]] = $row["poslease_anual_rent"];
	$lease_salespercent[$row["poslease_id"]] = $row["poslease_salespercent"];
}

/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);
$list->set_entity("_posleases");
$list->set_order("poslease_startdate, poslease_type_name");
$list->set_filter($list_filter);   

$list->add_hidden("pos_id", param("pos_id"));
$list->add_list_column("poslease_lease_type", "Type", "select poslease_type_id, poslease_type_name from poslease_types", NOTNULL, $lease_types);
$list->add_edit_column("start", "Start Date", 10, NOTNULL, $lease_start_dates);
$list->add_edit_column("end", "End Date", 10, NOTNULL, $lease_end_dates);
$list->add_edit_column("extensionoption", "Extension Option to Date", 10, NOTNULL, $lease_extension_dates);
$list->add_edit_column("exitoption", "Exit Option to Date", 10, NOTNULL, $lease_extit_dates);
$list->add_edit_column("poslease_termination_time", "Termination deadline", 10, NOTNULL, $lease_termination_times);
$list->add_list_column("poslease_currency", "Curreny", "select currency_id, currency_symbol from currencies order by currency_symbol", NOTNULL, $lease_currencies);
$list->add_edit_column("poslease_anual_rent", "Rent p.a.", 10, NOTNULL, $lease_rents);
$list->add_edit_column("poslease_salespercent", "Rent in % of Sales", 10, NOTNULL, $lease_salespercent);


//$list->add_edit_column($name, $caption, $width, $flags = 0, $values = array())

$list->add_button("newlease", "Add New Lease Contract");

$list->populate();
$list->process();



/********************************************************************
    Process Buttons
*********************************************************************/ 
if($form->button("back"))
{
	redirect("correct_pos_data.php?country=" . param("country"));
}
elseif($form->button("posaddress_place_id"))
{
	$sql = "select place_name, place_province " .
		   "from places " . 
		   "where place_id = " . dbquote($form->value("posaddress_place_id"));
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$form->value("posaddress_place", $row["place_name"]);
		$form->value("select_province", $row["place_province"]);
	}
}
elseif($form->button("posaddress_country"))
{
	$form->value("posaddress_place_id", "");
	$form->value("posaddress_place", "");
	$form->value("posaddress_zip", "");
	$form->value("select_province", "");
}
elseif($form->button("save"))
{
	$error = 0;
	
	if($pos['posaddress_store_postype'] < 4)
	{
		
		$form->add_validation("{posaddress_store_grosssurface} >= {posaddress_store_totalsurface}", "The gross surface must not be less than the total surface!");

		$form->add_validation("{posaddress_store_totalsurface} >= {posaddress_store_retailarea}", "The total surface must not be less than the sales surface!");

		
		
		if(!$form->value("posaddress_perc_class")){$error = 1;}
		if(!$form->value("posaddress_perc_tourist")){$error = 1;}
		if(!$form->value("posaddress_perc_transport")){$error = 1;}
		if(!$form->value("posaddress_perc_people")){$error = 1;}
		if(!$form->value("posaddress_perc_parking")){$error = 1;}
		if(!$form->value("posaddress_perc_visibility1")){$error = 1;}
		if(!$form->value("posaddress_perc_visibility2")){$error = 1;}
	}

	if(param("posaddress_checked") !=1)
	{
		$form->save();
		$sql = "update _posaddresses set posaddress_checked = 2 where posaddress_id = " . id();
		mysql_query($sql) or dberror($sql);

	}
	elseif($error == 1)
	{
		$form->error("Please rate all parameters in section 'Area Perception'!");
	}
	elseif(count($form->items["area"]["value"]) == 0)
	{
		$form->error("Please indicate the neighbourhood information.");
	}
	elseif($form->validate())
	{
		
		if($form->value("select_province") == 999999999)
		{

			//insert new record into provinces
			$province_id = "";
			
			$fields = array();
			$values = array();

			$fields[] = "province_country";
			$values[] = dbquote($form->value("posaddress_country"));

			$fields[] = "province_canton";
			$values[] = dbquote($form->value("province"));

			$fields[] = "date_created";
			$values[] = "current_timestamp";

			$fields[] = "user_created";
			$values[] = dbquote($_SESSION["user_login"]);

			$sql = "insert into provinces (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			mysql_query($sql) or dberror($sql);

			$province_id =  mysql_insert_id();

			//update place
			$fields = array();
			$values = array();

			$value = dbquote($province_id);
			$fields[] = "place_province = " . $value;
			
			$value = dbquote(date("Y-m-d"));
			$fields[] = "date_modified = " . $value;

			$value = dbquote(user_login());
			$fields[] = "user_modified = " . $value;

			$sql = "update places set " . join(", ", $fields) . " where place_id = " . $form->value("posaddress_place_id");
			mysql_query($sql) or dberror($sql);

		}
		elseif($form->value("select_province") > 0) //update place
		{
			$sql = "update places set place_province = " . $form->value("select_province") . 
				   " where place_id = " . $form->value("posaddress_place_id");
			
			mysql_query($sql) or dberror($sql);
		}

		$form->save();
		redirect("correct_pos_data.php?country=" . param("country"));

		/*
		
		if($pos["posaddress_ownertype"] == 1 and $pos["posaddress_store_postype"] == 1) //corporate and store
		{

			//save all lease data
			$l_type = $list->values("poslease_lease_type");			
			$l_start = $list->values("start");
			$l_end = $list->values("end");
			$l_extensionoption = $list->values("extensionoption");
			$l_exitoption = $list->values("exitoption");
			$l_termination_time = $list->values("poslease_termination_time");
			$l_poslease_currency = $list->values("poslease_currency");
			$l_poslease_anual_rent = $list->values("poslease_anual_rent");
			$l_poslease_salespercent = $list->values("poslease_salespercent");
			$l_error = 0;

			if(count($list->values("poslease_lease_type")) == 0)
			{
				$l_error = 1;
				$form->value("posaddress_checked", 0);
				$form->error("Please add at least one lease contract to the POS!");
			}
			
			
			foreach($list->values("poslease_lease_type") as $key=>$value)
			{
				//if($value > 0)
				//{
					if(!$l_start[$key] or !$l_end[$key] or !$l_poslease_currency[$key] or !$l_poslease_anual_rent[$key] or !$l_termination_time[$key])
					{
						$form->error("The following lease data is mandatory: Type, Start, End, Termination deadline, Currency, Annual Rent.");
						$l_error = 1;
						$form->value("posaddress_checked", 0);
					}
				//}
			}
						
			
			if($l_error == 0)
			{
				$form->save();
				$sql = "Delete from _posleases where poslease_posaddress = " . param("pos_id");
				mysql_query($sql) or dberror($sql);
				
				foreach($list->values("poslease_lease_type") as $key=>$value)
				{
					if($value > 0)
					{
						$fields = array();
						$values = array();

						$values[] = dbquote(param("pos_id"));
						$fields[] = "poslease_posaddress";

						$values[] = dbquote($value);
						$fields[] = "poslease_lease_type";

						$values[] = dbquote(from_system_date($l_start[$key]));
						$fields[] = "poslease_startdate";

						$values[] = dbquote(from_system_date($l_end[$key]));
						$fields[] = "poslease_enddate";

						$values[] = dbquote(from_system_date($l_extensionoption[$key]));
						$fields[] = "poslease_extensionoption";

						$values[] = dbquote(from_system_date($l_exitoption[$key]));
						$fields[] = "poslease_exitoption";

						$values[] = dbquote($l_termination_time[$key]);
						$fields[] = "poslease_termination_time";

						$values[] = dbquote($l_poslease_currency[$key]);
						$fields[] = "poslease_currency";

						$values[] = dbquote($l_poslease_anual_rent[$key]);
						$fields[] = "poslease_anual_rent";

						$values[] = dbquote($l_poslease_salespercent[$key]);
						$fields[] = "poslease_salespercent";

						$values[] = dbquote($row["user_created"]);
						$fields[] = "user_created";

						$values[] = dbquote($row["date_created"]);
						$fields[] = "date_created";

						$values[] = dbquote($row["user_modified"]);
						$fields[] = "user_modified";

						$values[] = dbquote($row["date_modified"]);
						$fields[] = "date_modified";

						$sql = "insert into _posleases (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}
				}
				
				redirect("correct_pos_data.php?country=" . param("country"));
			}
		}
		else
		{
			
			$form->save();
			redirect("correct_pos_data.php?country=" . param("country"));
		}
		*/
	}
}

if($list->button("newlease"))
{
	
	$form->add_validation("{posaddress_store_grosssurface} >= {posaddress_store_totalsurface}", "The gross surface must not be less than the total surface!");

	$form->add_validation("{posaddress_store_totalsurface} >= {posaddress_store_retailarea}", "The total surface must not be less than the sales surface!");
	
	$error = 0;
	if(!$form->value("posaddress_perc_class")){$error = 1;}
	if(!$form->value("posaddress_perc_tourist")){$error = 1;}
	if(!$form->value("posaddress_perc_transport")){$error = 1;}
	if(!$form->value("posaddress_perc_people")){$error = 1;}
	if(!$form->value("posaddress_perc_parking")){$error = 1;}
	if(!$form->value("posaddress_perc_visibility1")){$error = 1;}
	if(!$form->value("posaddress_perc_visibility2")){$error = 1;}

	if($error == 1)
	{
		$form->error("Please rate all parameters in section 'Area Perception'!");
	}
	elseif(count($form->items["area"]["value"]) == 0)
	{
		$form->error("Please indicate the neighbourhood information.");
	}
	else
	{
		$form->save();

		$sql = "insert into _posleases (poslease_posaddress) values (" .  param("pos_id") . ")";
		mysql_query($sql) or dberror($sql);

		$link = "correct_pos_data_pos.php?country=" . param("country") . "&id=" . param("pos_id");
		redirect($link);
	}
}

/********************************************************************
    Render Page
*********************************************************************/ 

$page = new Page("posaddresses", "POS Data: Correction");
require "include/pos_page_actions.php";
$page->header();

$page->title("POS Data: Correction");

$form->render();

/*
if($pos["posaddress_ownertype"] == 1 and $pos["posaddress_store_postype"] == 1) //corporate and store
{
	echo "<br /><br /><br /><span class=\"section\">Lease Data</span><br /><br />";
	$list->render();
}
*/


echo "<br /><br /><br /><span class=\"section\">Map Selector</span><br />";

if(isset($_POST["posaddress_google_lat"]))
{
	echo "<iframe src =\"correct_pos_data_map.php?id=" . param("pos_id") . "&lat=" . $_POST["posaddress_google_lat"] . "&long=" . $_POST["posaddress_google_long"] . "\" width=\"800\" height=\"800\" frameborder=\"0\" marginwidth=\"0\"></iframe>";
}
else
{
	echo "<iframe src =\"correct_pos_data_map.php?id=" . param("pos_id") . "\" width=\"800\" height=\"800\" frameborder=\"0\" marginwidth=\"0\"></iframe>";
}

$page->footer();

?>