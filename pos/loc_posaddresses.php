<?php
/********************************************************************

    loc_posaddreses.php

    Lists pos locations for translations.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");
set_referer("loc_posaddress.php");



if(param("posaddress_country") > 1)
{
	$posaddress_country =	param("posaddress_country");
}
else
{
	$posaddress_country = 1;
}


/********************************************************************
    Create Form
*********************************************************************/ 
$form = new Form("posaddresses", "posaddress");

$form->add_section("List Filter Selection");

$form->add_list("posaddress_country", "Country",
    "select country_id, country_name from countries order by country_name", SUBMIT | NOTNULL, $posaddress_country);


/********************************************************************
    Populate and process button clicks
*********************************************************************/ 
$form->populate();

/********************************************************************
    Create List
*********************************************************************/ 

//get all translations
$translations = array();
$sql = "select posaddress_id,language_name " . 
       "from posaddresses " . 
       "left join loc_posaddresses on loc_posaddress_posaddress = posaddress_id " . 
	   "left join languages on language_id = loc_posaddress_language " .
	   "order by language_name";


$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	if(array_key_exists($row["posaddress_id"], $translations))
	{
		$translations[$row["posaddress_id"]] = $translations[$row["posaddress_id"]] . "/" . trim($row["language_name"]);
	}
	else
	{
		$translations[$row["posaddress_id"]] = trim($row["language_name"]);
	}
}

$sql = "select posaddress_id, posaddress_place, posaddress_name, posaddress_address " .
       "from posaddresses ";

$list_filter =	"posaddress_country = " . $posaddress_country;
$list_filter .= " and posaddress_export_to_web = 1 ";
$list_filter .= " and (posaddress_store_closingdate is NULL or posaddress_store_closingdate = '0000-00-00')";

$list = new ListView($sql);

$list->set_entity("posaddresses");
$list->set_order("posaddress_place, posaddress_name");
$list->set_filter($list_filter);

$list->add_column("posaddress_place", "Place", "loc_posaddress.php", LIST_FILTER_FREE);
$list->add_column("posaddress_name", "Name");
$list->add_column("posaddress_address", "Address");
$list->add_text_column("translations", "Translations", COLUMN_UNDERSTAND_HTML, $translations);

$list->process();

$page = new Page("locales");

$page->header();
$page->title("POS Locations");
$form->render();
$list->render();
$page->footer();
?>
