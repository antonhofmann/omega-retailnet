
<?php
/********************************************************************

    pos_profitability_preselect.php

    Preselection of POS List

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2013-06-16
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2013-06-16
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once('../include/phpexcel/PHPExcel.php');

if(!has_access("can_import_pos_profitability"))
{
	redirect("/pos");
}

$can_process_imported_file = false;
$file_name = '';
if(param('rawdata')) {
	$file_info = param('rawdata');

	if(array_key_exists('name', $file_info)) {
		$file_parts = pathinfo($file_info['name']);
		if($file_parts['extension'] == 'xlsx') {
			$file_name = $file_info['name'];
			$can_process_imported_file = true;
		}
	}
}

$form = new Form("posaddresses", "posaddress");
$form->add_section("Import Raw Data from Excel<br /><br />");

	
$form->add_upload("rawdata", "File", "/tmp", NOTNULL);
$form->add_button("import_files", "Import Raw Data");

$form->populate();
$form->process();


if($form->button('import_files')) {
	$file_parts = pathinfo($form->value('rawdata'));
	
	if($file_parts['extension'] != 'xlsx') {
		
		$form->error('You must upload an Excel-File.');
		if(file_exists($form->value('rawdata'))) {
			unlink('..' . $form->value('rawdata'));
		}
		$form->value('rawdata', '');
	}
	elseif(!$form->value('rawdata')) {
		$form->error('Please upload a file first.');
	}
	
}

if($file_name and $can_process_imported_file == true) {


	$inputFileName = "../tmp/" . strtolower($file_info['name']);
	$objReader = PHPExcel_IOFactory::createReader('Excel2007');
	$objReader->setReadDataOnly(true);
	$objPHPExcel = $objReader->load($inputFileName);
	$objWorksheet = $objPHPExcel->getActiveSheet();

	$highestRow = $objWorksheet->getHighestRow();
	$highestColumn = $objWorksheet->getHighestColumn();
	$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
	$rows = array();
	for ($row = 1; $row <= $highestRow; ++$row) {
	  for ($col = 0; $col <= $highestColumnIndex; ++$col) {
		
		$value = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
		if($value == '�') {
			$value = '';
		}
		
		
		$rows[$row][$col] = $value;
	  }
	}

	//get year
	$errors = array();
	$output = array();
	$pos_locations = array();
	$reporting_numbers = array();


	if(array_key_exists(2, $rows) and array_key_exists(1, $rows[2])) {
		
		$row21 = str_replace("\r\n", ' ', $rows[2][1]);
		$row21 = str_replace("\r", ' ', $row21);
		$row21 = str_replace("\n", ' ', $row21);
		$tmp = explode(' ', $row21);
		
		if(array_key_exists(3, $tmp) and $tmp[3] == 'kLOC') {
			$output['kLoc'] = 'ok';
		}
		else {
			$errors[] = 'It seems that the amounts in the file are not in kLOC.';
		}
		
		if(count($errors) == 0) {
			if(array_key_exists(6, $tmp)) {
				$year = $tmp[6];
				$output['year'] = $year;
			}
			else {
				$errors[] = 'The year could not be extracted.';
			}
		}

		if(count($errors) == 0) {
			
			if($year == date('Y') or $year == (date("Y")-1)) {
			
			}
			else {
				$errors[] = 'Sorry, the sales figures for ' . $year . ' cannot be imported. Allowed years are: ' . date('Y') . " and " . (date("Y")-1);
			}
		}

		if(array_key_exists(5, $tmp)) {
			$month = $tmp[5];
		}
		else {
			$errors[] = 'The month could not be extracted';
		}


		if(count($errors) == 0) {
			
			$date = date_parse($month);
			$output['month'] = $date['month'];
		}
		
		if(count($errors) == 0) {

			for($i=2;$i<count($rows[2]);$i++) {
				if(array_key_exists($i, $rows[2]) and strlen($rows[2][$i]) > 4) {
					$pos_locations[$i] = $rows[2][$i];

					$rowvalue = str_replace("\r\n", ' ', $rows[2][$i]);
					$rowvalue = str_replace("\r", ' ', $rowvalue);
					$rowvalue = str_replace("\n", ' ', $rowvalue);
					$tmp = explode(' ', $rowvalue);
					if(array_key_exists(0, $tmp)) {
						$reporting_numbers[$i] = $tmp[0];
					}
				}
			}
		}
	}
	else {
		$errors[] = 'The file could not be processed since the structure has not the expected format.';
	}


	//watches units sold
	$watches_units_sold = array();
	$line = 13;
	if(count($errors) == 0) {
		
		if(array_key_exists($line, $rows)) {
			for($i=2;$i<count($rows[$line]);$i++) {
				if(array_key_exists($i, $rows[$line])) {
					$watches_units_sold[$i] = $rows[$line][$i];
				}
			}
		}
	}

	//watches gross sales
	$watches_gross_sales = array();
	$line = 9;
	if(count($errors) == 0) {
		
		if(array_key_exists($line, $rows)) {
			for($i=2;$i<count($rows[$line]);$i++) {
				if(array_key_exists($i, $rows[$line])) {
					$watches_gross_sales[$i] = round(1000*$rows[$line][$i], 0);
				}
			}
		}
	}


	//watches gross sales
	$bijoux_gross_sales = array();
	$line = 10;
	if(count($errors) == 0) {
		
		if(array_key_exists($line, $rows)) {
			for($i=2;$i<count($rows[$line]);$i++) {
				if(array_key_exists($i, $rows[$line])) {
					$bijoux_gross_sales[$i] = round(1000*$rows[$line][$i], 0);
				}
			}
		}
	}


	//net sales
	$net_sales = array();
	$line = 40;
	if(count($errors) == 0) {
		
		if(array_key_exists($line, $rows)) {
			for($i=2;$i<count($rows[$line]);$i++) {
				if(array_key_exists($i, $rows[$line])) {
					$net_sales[$i] = round(1000*$rows[$line][$i], 0);
				}
			}
		}
	}

	//gross margin
	$gross_margin = array();
	$line = 41;
	if(count($errors) == 0) {
		
		if(array_key_exists($line, $rows)) {
			for($i=2;$i<count($rows[$line]);$i++) {
				if(array_key_exists($i, $rows[$line])) {
					$gross_margin[$i] = round(1000*$rows[$line][$i], 0);
				}
			}
		}
	}


	//expenses
	$expenses = array();
	$line = 51;
	if(count($errors) == 0) {
		
		if(array_key_exists($line, $rows)) {
			for($i=2;$i<count($rows[$line]);$i++) {
				if(array_key_exists($i, $rows[$line])) {
					$expenses[$i] = round(1000*$rows[$line][$i], 0);
				}
			}
		}
	}


	//operating income
	$operating_income = array();
	$line = 53;
	if(count($errors) == 0) {
		
		if(array_key_exists($line, $rows)) {
			for($i=2;$i<count($rows[$line]);$i++) {
				if(array_key_exists($i, $rows[$line])) {
					$operating_income[$i] = round(1000*$rows[$line][$i], 0);
				}
			}
		}
	}


	//whs
	$whs = array();
	$line = 56;
	if(count($errors) == 0) {
		
		if(array_key_exists($line, $rows)) {
			for($i=2;$i<count($rows[$line]);$i++) {
				if(array_key_exists($i, $rows[$line])) {
					$whs[$i] = round(100*$rows[$line][$i], 2);
				}
			}
		}
	}


	//operating income incl.whs
	$operating_income_incl_whs = array();
	$line = 57;
	if(count($errors) == 0) {
		
		if(array_key_exists($line, $rows)) {
			for($i=2;$i<count($rows[$line]);$i++) {
				if(array_key_exists($i, $rows[$line])) {
					$operating_income_incl_whs[$i] = round(1000*$rows[$line][$i], 0);
				}
			}
		}
	}

	if(count($pos_locations)!= count($reporting_numbers)) {
		$errors[] = 'We could not import the data - error due to reporting number.';
	}


	if(count($errors) > 0) {
		$tmp = '';
		foreach($errors as $key=>$value) {
			$tmp .= $value . "<br />";
		}

		unlink($inputFileName);

		$form->value("rawdata", "");
		$form->error($tmp);
		
	}
	else {
		
		
		
		$data = array();
		foreach($pos_locations as $key=>$value) {
		
			$tmp = array();
			$tmp['reporting_number'] = $reporting_numbers[$key];
			if(array_key_exists($key, $watches_units_sold)) {
				$tmp['unit_of_watches_sold'] = $watches_units_sold[$key];
			}
			else{
				$tmp['unit_of_watches_sold'] = '';
			}
			if(array_key_exists($key, $watches_gross_sales)) {
				$tmp['watches_gross_sales'] = $watches_gross_sales[$key];
			}
			else{
				$tmp['watches_gross_sales'] = '';
			}
			if(array_key_exists($key, $bijoux_gross_sales)) {
				$tmp['bijoux_gross_sales'] = $bijoux_gross_sales[$key];
			}
			else{
				$tmp['bijoux_gross_sales'] = '';
			}

			if(array_key_exists($key, $net_sales)) {
				$tmp['net_sales'] = $net_sales[$key];
			}
			else{
				$tmp['net_sales'] = '';
			}

			if(array_key_exists($key, $gross_margin)) {
				$tmp['gross_margin'] = $gross_margin[$key];
			}
			else{
				$tmp['gross_margin'] = '';
			}

			if(array_key_exists($key, $expenses)) {
				$tmp['expenses'] = $expenses[$key];
			}
			else{
				$tmp['expenses'] = '';
			}

			if(array_key_exists($key, $operating_income)) {
				$tmp['operating_income'] = $operating_income[$key];
			}
			else{
				$tmp['operating_income'] = '';
			}

			if(array_key_exists($key, $whs)) {
				$tmp['whs'] = $whs[$key];
			}
			else{
				$tmp['whs'] = '';
			}

			if(array_key_exists($key, $operating_income_incl_whs)) {
				$tmp['operating_income_incl_whs'] = $operating_income_incl_whs[$key];
			}
			else{
				$tmp['operating_income_incl_whs'] = '';
			}

			$data[] = $tmp;
		}
		
		//echo '<pre>';
		//print_r($data);

		unlink($inputFileName);

		foreach($data as $key=>$values) {
			$sql = "select posaddress_id from posaddresses
			        where posaddress_eprepnr = " . dbquote($values['reporting_number']);
		
			$res = mysql_query($sql) or dberror($sql);
			if ($pos_data = mysql_fetch_assoc($res))
			{
			
				$sql = 'select count(possellout_id) as num_recs
				       from possellouts
				       where possellout_posaddress_id = ' . dbquote($pos_data['posaddress_id']) . 
						   ' and possellout_year = ' . dbquote($output['year']);

		
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				if ($row['num_recs'] == 0 
					and $output['year'] > 0
					and $output['month'] > 0
					and $values["watches_gross_sales"] > 0)
				{
					$fields = array();
					$field_values = array();

					$field_values[] = dbquote($pos_data['posaddress_id']);
					$fields[] = "possellout_posaddress_id";

					$field_values[] = dbquote($output['year']);
					$fields[] = "possellout_year";

					$field_values[] = dbquote($output['month']);
					$fields[] = "possellout_month";

					$field_values[] = dbquote($values["unit_of_watches_sold"]);
					$fields[] = "possellout_watches_units";

					$field_values[] = dbquote($values["watches_gross_sales"]);
					$fields[] = "possellout_watches_grossales";

					$field_values[] = dbquote($values["bijoux_gross_sales"]);
					$fields[] = "possellout_bijoux_grossales";

					$field_values[] = dbquote($values["net_sales"]);
					$fields[] = "possellout_net_sales";
					
					$field_values[] = dbquote($values["gross_margin"]);
					$fields[] = "possellout_grossmargin";

					$field_values[] = dbquote($values["expenses"]);
					$fields[] = "possellout_operating_expenses";

					$field_values[] = dbquote($values["operating_income"]);
					$fields[] = "possellout_operating_income_excl";

					$field_values[] = dbquote($values["whs"]);
					$fields[] = "possellout_wholsale_margin";

					$field_values[] = dbquote($values["operating_income_incl_whs"]);
					$fields[] = "possellout_operating_income_incl";

					$field_values[] = dbquote(user_login());
					$fields[] = "user_created";

					$field_values[] = dbquote(date('Y:m:d H:i:s'));
					$fields[] = "date_created";
					
					$sql = "insert into possellouts (" . join(", ", $fields) . ") values (" . join(", ", $field_values) . ")";
					mysql_query($sql) or dberror($sql);
				}
				elseif ($output['year'] > 0
					and $output['month'] > 0
					and $values["watches_gross_sales"] > 0) {

					$fields = array();
					$value = dbquote($output['month']);
					$fields[] = "possellout_month = " . $value;

					$value = dbquote($values["unit_of_watches_sold"]);
					$fields[] = "possellout_watches_units = " . $value;

					$value = dbquote($values["watches_gross_sales"]);
					$fields[] = "possellout_watches_grossales = " . $value;

					$value = dbquote($values["bijoux_gross_sales"]);
					$fields[] = "possellout_bijoux_grossales = " . $value;

					$value = dbquote($values["net_sales"]);
					$fields[] = "possellout_net_sales = " . $value;

					$value = dbquote($values["gross_margin"]);
					$fields[] = "possellout_grossmargin = " . $value;

					$value = dbquote($values["expenses"]);
					$fields[] = "possellout_operating_expenses = " . $value;

					$value = dbquote($values["operating_income"]);
					$fields[] = "possellout_operating_income_excl = " . $value;

					$value = dbquote($values["whs"]);
					$fields[] = "possellout_wholsale_margin = " . $value;

					$value = dbquote($values["operating_income_incl_whs"]);
					$fields[] = "possellout_operating_income_incl = " . $value;

					$value = dbquote(date("Y-m-d H:i:s"));
					$fields[] = "date_modified = " . $value;

					$value = dbquote(user_login());
					$fields[] = "user_modified = " . $value;
					
					$sql = "update possellouts set " . join(", ", $fields) . "  where possellout_posaddress_id = " . dbquote($pos_data['posaddress_id']) . 
						   " and possellout_year = " . dbquote($output['year']);
					mysql_query($sql) or dberror($sql);

				}
			}
		}

		$form->value("rawdata", "");
		$form->message('<span class="highlite_value_bold">The data was imported successfully.</span>');
		//print_r($output);
	}
}


$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title("POS Index: Import Profitability");
$form->render();
$page->footer();

?>