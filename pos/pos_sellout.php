<?php
/********************************************************************

    pos_sellout.php

    Enter Sellout Data for the POS Location

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2012-08-16
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2012-08-16
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

check_access("has_access_to_sellouts");



if(!id())
{
	redirect("pos_sellouts.php?country=" . param("country") . '&let=' . param('let') . '&ltf=' . param("ltf"). '&ostate=' . param("ostate"));
}



if(param("did") > 0)
{
	$sql_d = "delete from possellouts " . 
		   "where possellout_id = " . param("did");

	$result = mysql_query($sql_d) or dberror($sql_d);

}


$pos_data = get_poslocation(id(), "posaddresses");
$has_project = update_posdata_from_posorders(id());


$opening_date = $pos_data["posaddress_store_openingdate"];
$closing_date = $pos_data["posaddress_store_closingdate"];

$opening_year = substr($opening_date, 0, 4);
$closing_year = substr($closing_date, 0, 4);
$actual_year = date("Y");

//check if sellout records exist else create
$starting_year = $opening_year;
if($closing_year > 0)
{
	$ending_year = $closing_year;
}
else
{
	$ending_year = $actual_year;
}


$sql = "select count(possellout_id) as num_recs " . 
	   "from possellouts " . 
	   "where possellout_posaddress_id = " . id();

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);
if($row["num_recs"] == 0)
{
	for($year=$starting_year;$year<=$ending_year;$year++)
	{
		$sql = "select count(possellout_id) as num_recs " . 
			   "from possellouts " . 
			   "where possellout_posaddress_id = " . id() . 
			   " and possellout_year = " . $year;

		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);
		
		if($row["num_recs"] == 0)
		{
		
			$sql_i = "INSERT INTO possellouts (" . 
					 "possellout_posaddress_id, possellout_year, date_created, user_created" . 
					 ") VALUES (" . 
					 id() . ", " . 
					 $year . ", " . 
					 dbquote(date("Y-m-d H:i:s")) . ", " . 
					 dbquote(user_login()) . 
					 ")";
			
			$result = mysql_query($sql_i) or dberror($sql_i);

		}
	}
}

$watches = array();
$bijoux = array();
$months = array();
$delete_record = array();

$sql = "select * from possellouts where possellout_posaddress_id = " . param("id");
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$watches[$row["possellout_id"]] = $row["possellout_watches_units"];
	$bijoux[$row["possellout_id"]] = $row["possellout_bijoux_units"];
	$months[$row["possellout_id"]] = $row["possellout_month"];

	
	$link = "pos_sellout.php?id=" . param("id") . "&country=" . param("country") . '&let=' . param('let') . '&ltf=' . param("ltf"). '&ostate=' . param("ostate") . "&did=" . $row["possellout_id"];

	$delete_record[$row["possellout_id"]] = '<a href="' . $link . '"><img src="/pictures/closed.gif" border="0"/></a>';
}
    

/********************************************************************
    Create Form
*********************************************************************/

$form = new Form("posaddresses", "posaddress");

$form->add_hidden("country", param("country"));
$form->add_hidden("province", param("province"));
$form->add_hidden("ostate", param("ostate"));
$form->add_hidden("id", id());

$form->add_label("posaddress_name", "POS Name");
$form->add_label("posaddress_address", "Address");
$form->add_label("posaddress_place", "City");
$form->add_lookup("posaddress_country", "Legal Country", "countries", "country_name");
$form->add_label("posaddress_store_openingdate", "Opening Date");
$form->add_label("posaddress_store_closingdate", "Closing Date");

$form->add_section("Add a new month to the list for one of the following years:");
$form->add_button("y" . $starting_year, $starting_year);
for($year=$starting_year+1;$year<=$ending_year;$year++)
{
	$form->add_button("y" . $year, $year);
}

$form->populate();
$form->process();

for($year=$starting_year;$year<=$ending_year;$year++)
{
	
	if($form->button("y" . $year))
	{

		$sql_i = "INSERT INTO possellouts (" . 
				 "possellout_posaddress_id, possellout_year, date_created, user_created" . 
				 ") VALUES (" . 
				 id() . ", " . 
				 $year . ", " . 
				 dbquote(date("Y-m-d H:i:s")) . ", " . 
				 dbquote(user_login()) . 
				 ")";
		
		$result = mysql_query($sql_i) or dberror($sql_i);
		redirect("pos_sellout.php?id=" . param("id") . "&country=" . param("country") . '&let=' . param('let') . '&ltf=' . param("ltf"). '&ostate=' . param("ostate"));
	}

}


/********************************************************************
    Create List
*********************************************************************/
$list = new ListView("select * from possellouts");

$list->set_title("Sellouts in Units");
$list->set_entity("possellouts");
$list->set_filter("possellout_posaddress_id = " . param("id"));
$list->set_order("possellout_year DESC, possellout_month DESC");

if(has_access("can_edit_his_sellouts") or has_access("can_edit_all_sellouts"))
{
	$list->add_column("possellout_year", "Year", "", 0 ,'', COLUMN_NO_WRAP);
	$list->add_number_edit_column("possellout_month", "Month", "2", 0, $months);

	$list->add_number_edit_column("possellout_watches_units", "Watches", "10", 0, $watches);
	$list->add_number_edit_column("possellout_bijoux_units", "Bijoux", "10", 0, $bijoux);

	$list->add_text_column("delete", "", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML , $delete_record);
	$list->add_button("save", "Save");
}
else
{
	$list->add_column("possellout_year", "Year", "", 0 ,'', COLUMN_NO_WRAP);
	$list->add_column("possellout_month", "Month", "", 0,'', COLUMN_NO_WRAP);
	$list->add_column("possellout_watches_units", "Watches", "", COLUMN_ALIGN_RIGHT);
	$list->add_column("possellout_bijoux_units", "Bijoux", "", COLUMN_ALIGN_RIGHT);
}


$list->add_button("back", "Back");


$list->populate();
$list->process();



if($list->button("back"))
{
	redirect("pos_sellouts.php?country=" . param("country") . '&let=' . param('let') . '&ltf=' . param("ltf"). '&ostate=' . param("ostate"));
}
elseif($list->button("save"))
{
	$error = "";
	$error_text = "";
	foreach($list->columns as $key=>$column)
	{
	
		if($column["name"] == "possellout_month")
		{
			foreach($column["values"] as $possellout_id=>$month)
			{
				if($month) 
				{
					if($month >= 1 and $month <= 12)
					{
					
						$fields = array();
						$fields[] = "possellout_month = " . dbquote($month);
						$sql = "update possellouts set " . join(", ", $fields) . " where possellout_id = " . $possellout_id;
						mysql_query($sql) or dberror($sql);
					}
					else
					{
						$error_text = "The month mus be a number from 1 to 12!";
					}

				}

				$fields = array();
				$fields[] = "date_modified = " . dbquote(date("Y-m-d H:i:s"));
				$fields[] = "user_modified = " . dbquote(user_login());
				$sql = "update possellouts set " . join(", ", $fields) . " where possellout_id = " . $possellout_id;
				mysql_query($sql) or dberror($sql);
			}
		}
		elseif($column["name"] == "possellout_watches_units")
		{
			foreach($column["values"] as $possellout_id=>$watches)
			{
				if($watches) 
				{
					if($watches >= 1 and $watches <= 9999999999)
					{
						$fields = array();
						$fields[] = "possellout_watches_units = " . dbquote($watches);
						$sql = "update possellouts set " . join(", ", $fields) . " where possellout_id = " . $possellout_id;
						mysql_query($sql) or dberror($sql);
					}
					else
					{
						$error_text = "The amount of watches must be a number!";
					}
				}
			}
		}
		elseif($column["name"] == "possellout_bijoux_units")
		{
			foreach($column["values"] as $possellout_id=>$bjoux)
			{
				if($bjoux) 
				{
					if($bjoux >= 1 and $bjoux <= 9999999999)
					{
						$fields = array();
						$fields[] = "possellout_bijoux_units = " . dbquote($bjoux);
						$sql = "update possellouts set " . join(", ", $fields) . " where possellout_id = " . $possellout_id;
						mysql_query($sql) or dberror($sql);
					}
					else
					{
						$error_text = "The amount of bijoux must be a number!";
					}

				}
			}
		}
	}

	if($error_text)
	{
		$form->error($error_text);
	}
	else
	{
		$form->message("Your sellout data was saved.");
	}

}



$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title("POS Sellouts");
$form->render();
$list->render();

$page->footer();

?>
