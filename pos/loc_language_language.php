<?php
/********************************************************************

    loc_language_language.php

    Creation and mutation of language translations.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");


$language_id = 0;

if(param("language_id"))
{
	$language_id = param("language_id");
}

//get language name
$language_name = "";
$sql = "select language_name from languages where language_id = " . $language_id;
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$language_name = $row["language_name"];
}

$form = new Form("loc_languages", "language");

$form->add_section("Basic Data");
$form->add_hidden("language_id", $language_id);
$form->add_hidden("loc_language_language_id", $language_id);
$form->add_label("language_name", "language", 0, $language_name);

$form->add_section("Translation");
$form->add_list("loc_language_language", "Language*", 
    "select language_id, language_name from languages order by language_name", NOTNULL);

$form->add_edit("loc_language_name", "language Name*", NOTNULL);

$form->add_button("save", "Save");
$form->add_button("back", "Back");
$form->add_button("delete", "Delete");

$form->populate();
$form->process();

if($form->button("back"))
{
	$link = "loc_language.php?id= " . param("language_id");
	redirect($link);
}
elseif($form->button("save"))
{
	if($form->validate())
	{
		$form->save();
		$link = "loc_language.php?id= " . param("language_id");
		redirect($link);
	}
}
elseif($form->button("delete"))
{
		$sql = "delete from loc_languages where loc_language_id =" . id();
		$result = mysql_query($sql) or dberror($sql);
		$link = "loc_language.php?id= " . param("language_id");
		redirect($link);

}

$page = new Page("locales");
$page->header();
$page->title(id() ? "Edit language Translation" : "Add language Translation");
$form->render();
$page->footer();

?>