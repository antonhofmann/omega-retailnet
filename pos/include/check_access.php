<?php
check_access("can_use_posindex");

if(has_access("can_edit_posindex") or has_access("can_view_posindex"))
{
}
elseif(has_access("can_view_his_posindex") or has_access("can_edit_his_posindex"))
{
	$user = get_user(user_id());
	$filter = "posaddress_client_id = " . $user["address"];

	$country_filter = "";
	$tmp = array();
	$sql = "select * from country_access " .
		   "where country_access_user = " . user_id();


	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{            
		$tmp[] = $row["country_access_country"];
	}

	if(count($tmp) > 0) {
		$country_filter = " posaddress_country IN (" . implode(",", $tmp) . ") ";
	}

	if($country_filter) 
	{
		if($filter)
		{
			if(in_array(param("country"), $tmp))
			{
				$filter .= " or (posaddress_country = " . param("country") . ") ";
			}
		}
		else
		{
			$filter = " or (posaddress_country = " . param("country") . ") ";
		}
	}
	else
	{
		if($filter)
		{
			//$filter .= " and (posaddress_country = " . $user["country"];
		}
		else
		{
			$filter .= " and (posaddress_country = " . $user["country"];
		}
	}


	if(has_access("has_access_to_retail_pos_only")) {
		$filter = "(" . $filter . ") and posaddress_ownertype in (1) ";
	}

	if(has_access("has_access_to_wholesale_pos")) {
		$filter = "(" . $filter . ") and posaddress_ownertype in (2,6) ";
	}

	if(param("pos_id"))
	{
		$filter .= " and posaddress_id = " . param("pos_id");
	}
	elseif(param("id"))
	{
		$filter .= " and posaddress_id = " . param("id");
	}

	$sql = "select posaddress_id " .
		   "from posaddresses where " . $filter;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
	}
	else
	{
		redirect("noaccess.php");
	}
}
?>