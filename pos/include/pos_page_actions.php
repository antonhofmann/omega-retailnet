<?php
/********************************************************************

    pos_page_actions.php

    Defines the possible page actions depending on access rights

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-07-18
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-07-18
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

/*
if(has_access("has_access_to_sellouts"))
{
	$page->register_action('sellouts', 'Sellouts', "pos_sellouts_preselect.php");
}
*/

if(has_access("can_edit_posindex") or has_access("can_edit_his_posindex"))
{
	$page->register_action('new_pos', ' - Add new POS', "posindex_new_pos.php");
}

if(has_access("has_access_to_poslocations"))
{
	$page->register_action('worldmap', 'World Map', "posshow_worldmap.php");
}


if(has_access("has_access_to_posclosing_assessments"))
{
	$page->register_action('posclosingassessments', 'Closing Assessments', "pos_closing_assessments.php");
}



if(has_access("has_access_to_pos_profitability") or has_access("has_access_to_pos_profitability"))
{
	$page->register_action('profitability', 'POS Profitability', "pos_profitability_preselect.php");
}

if(has_access("can_import_pos_profitability"))
{
	$page->register_action('import_profitability', 'Import Profitability', "pos_profitability_import.php");
}

/*
if(has_access("can_edit_pos_opening_hours") or has_access("can_view_pos_opening_hours"))
{
	$page->register_action('openinghrs', 'Opening Hours', "pos_openinghrs_preselect.php");
}
*/

if(has_access("can_correct_pos_data"))
{
	$page->register_action('dummy0', '', "");
	$page->register_action('correctposdata', 'Data Check Companies', "correct_address_data_preselect.php");
}
if(has_access("can_accept_pos_data_corrections"))
{
	$page->register_action('comparecorrections', '-- Check Corrections', "correct_pos_address_check_preselect.php");
}

if(has_access("can_correct_pos_data"))
{
	$page->register_action('correctposdata', 'Data Check POS Data', "correct_pos_data_preselect.php");
}
if(has_access("can_accept_pos_data_corrections"))
{
	$page->register_action('comparecorrections', '-- Check Corrections', "correct_pos_data_check_preselect.php");
}

/*
if(has_access("can_correct_pos_data"))
{
	$page->register_action('storelocator', 'Store Locator', "storelocator_pos_data_preselect.php");
}


if(has_access("can_enter_opening_dates"))
{
	$page->register_action('openingdates', 'Missing Opening Dates', "storelocator_opening_dates_preselect.php");
}
*/

if (has_access("can_administrate_posindex"))
{
    
	//$page->register_action('dummy2', '', "");
	//$page->register_action('sendmailtobrandmanager', 'Send Mail to Brand Managers', "correct_send_mail_to_brand_managers.php");
	//$page->register_action('dummy3', '', "");
	//$page->register_action('projectorphans', 'Project Orphans', "dataanalysis_projects_preselect.php");
	//$page->register_action('orderorphans', 'Order Orphans', "dataanalysis_orders_preselect.php");
}


?>