<?php
$page->add_tab("basicdata", "Basic Data", "posindex_pos.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&let=' . param('let') . '&ltf=' . param('ltf') . '&ostate=' . param('ostate'), $target = "_self", $flags = 0);

$page->add_tab("store", "POS", "posstore.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&let=' . param('let') . '&ltf=' . param('ltf') . '&ostate=' . param('ostate'), $target = "_self", $flags = 0);

if($pos["posaddress_store_postype"] != 4) //independent retailer
{
	
	$page->add_tab("ownership", "Ownership", "posownership.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&let=' . param('let') . '&ltf=' . param('ltf') . '&ostate=' . param('ostate'), $target = "_self", $flags = 0);
	$page->add_tab("leases", "Leases", "posleases.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&let=' . param('let') . '&ltf=' . param('ltf') . '&ostate=' . param('ostate'), $target = "_self", $flags = 0);
	//$page->add_tab("investment", "Intangible Assets", "posinvestments.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&let=' . param('let') . '&ltf=' . param('ltf') . '&ostate=' . param('ostate'), $target = "_self", $flags = 0);
	//$page->add_tab("closings", "Closures", "posclosings.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&let=' . param('let') . '&ltf=' . param('ltf') . '&ostate=' . param('ostate'), $target = "_self", $flags = 0);
	$page->add_tab("projects", "Projects", "posprojects.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&let=' . param('let') . '&ltf=' . param('ltf') . '&ostate=' . param('ostate'), $target = "_self", $flags = 0);
	
	//$page->add_tab("orders", "Orders", "posorders.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&let=' . param('let') . '&ltf=' . param('ltf') . '&ostate=' . param('ostate'), $target = "_self", $flags = 0);
	$page->add_tab("files", "Files", "posfiles.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&let=' . param('let') . '&ltf=' . param('ltf') . '&ostate=' . param('ostate'), $target = "_self", $flags = 0);
}
else
{
	$page->add_tab("projects", "Projects", "posprojects.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&let=' . param('let') . '&ltf=' . param('ltf') . '&ostate=' . param('ostate'), $target = "_self", $flags = 0);
	
	//$page->add_tab("orders", "Orders", "posorders.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&let=' . param('let') . '&ltf=' . param('ltf') . '&ostate=' . param('ostate'), $target = "_self", $flags = 0);
	$page->add_tab("files", "Files", "posfiles.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&let=' . param('let') . '&ltf=' . param('ltf') . '&ostate=' . param('ostate'), $target = "_self", $flags = 0);
}

$page->add_tab("googlemap", "Google Map", "posindex_map.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&let=' . param('let') . '&ltf=' . param('ltf') . '&ostate=' . param('ostate'), $target = "_self", $flags = 0);

/*
if(has_access("can_edit_pos_opening_hours") or has_access("can_view_pos_opening_hours")) 
{
	$page->add_tab("openinghrs", "Opening Hours", "posindex_openinghr.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&let=' . param('let') . '&ltf=' . param('ltf') . '&ostate=' . param('ostate'), $target = "_self", $flags = 0);
}
*/

$page->tabs();


?>