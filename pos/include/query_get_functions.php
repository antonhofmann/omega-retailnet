<?php
/********************************************************************

    query_get_functions.php

    Various utility functions to get information from tables.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2009-01-11
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2009-01-11
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.
*********************************************************************/

/********************************************************************
    Check if query has fields
*********************************************************************/
function check_if_query_has_fields($query_id)
{
	$has_fields = false;

	$sql = "select posquery_id, posquery_name, posqueryentity_name, posquery_fields, posquery_filter " . 
		   "from posqueries " .
		   "left join posqueryentities on posqueryentity_id = posquery_leading_entity " . 
		   " where posquery_id = " . $query_id;
	

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$fields = unserialize($row["posquery_fields"]);
		$filter = unserialize($row["posquery_filter"]);


		if(count($fields) > 0)
		{
			foreach($filter as $key=>$value)
			{
				if($value)
				{
					$has_fields = true;
				}
			}
			
		}
	}

	return $has_fields;
}


/********************************************************************
    get DB Info for POS Address Fields
*********************************************************************/
function query_group_fields()
{
	$groups = array();

	$groups[1] = "posaddresses.posaddress_salesregion";
	$groups[2] = "posaddresses.posaddress_region";
	$groups[3] = "posaddresses.posaddress_country";
	$groups[4] = "posaddresses.posaddress_place";
	$groups[5] = "posaddresses.posaddress_name";
	$groups[6] = "posorders.posorder_ordernumber";
	$groups[7] = "clients.address_company";
	$groups[8] = "posaddresses.posaddress_distribution_channel";
	$groups[9] = "posaddresses.posaddress_store_postype";
	$groups[10] = "posaddresses.posaddress_ownertype";

	
	

	return $groups;
}

/********************************************************************
    get DB Info for POS Address Fields
*********************************************************************/
function get_query_filter($query_id)
{
	$filter = array();
	
	$sql = "select posquery_filter from posqueries " .
	       "where posquery_id = " . $query_id;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{

		$filter = unserialize($row["posquery_filter"]);

	}
	return $filter;
}

/********************************************************************
    get predefined filter for a user in queries
*********************************************************************/
function user_predefined_filter($user_id)
{
	$predefined_filter["re"] = "";
	$predefined_filter["gr"] = "";
	$predefined_filter["co"] = "";
	$predefined_filter["user_address_id_filter"] = "";
	$countries = array();
	$salesregions = array();
	$regions = array();
	$user_address_ids = array();

	$sql = "select * from users " .
		   "left join addresses on address_id = user_address " .
		   "left join countries on country_id = address_country " . 
		   "left join salesregions on salesregion_id = country_salesregion " .
		   "left join regions on region_id = country_region " .
		   "where user_id = " . $user_id;

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$countries[$row["country_id"]] = $row["country_id"];
		$salesregions[$row["salesregion_id"]] = $row["salesregion_id"];
		$regions[$row["region_id"]] = $row["region_id"];
		$user_address_ids[$row["address_id"]] = $row["address_id"];
	
	}

	$predefined_filter["re"] = implode("-",$salesregions) . "-";
	$predefined_filter["gr"] = implode("-",$regions) . "-";
	$predefined_filter["co"] = implode("-",$countries) . "-";
	$predefined_filter["user_address_id_filter"] = "(" . implode(",",$user_address_ids) . ")";

	return $predefined_filter;
}

/********************************************************************
    get DB Info for Fields that can be totalized
*********************************************************************/
function query_function_fields()
{
	$function_fields = array();

	$function_fields[] = "posaddresses.posaddress_store_grosssurface";
	$function_fields[] = "posaddresses.posaddress_store_totalsurface";
	$function_fields[] = "posaddresses.posaddress_store_retailarea";
	$function_fields[] = "posaddresses.posaddress_store_backoffice";
	$function_fields[] = "posaddresses.posaddress_store_headcounts";
	$function_fields[] = "posaddresses.posaddress_store_fulltimeeqs";
	
	$function_fields[] ="cer_investments.amount1_cer_chf_requested";
	$function_fields[] ="cer_investments.amount3_cer_chf_requested";
	$function_fields[] ="cer_investments.amount5_cer_chf_requested";
	$function_fields[] ="cer_investments.amount7_cer_chf_requested";
	$function_fields[] ="cer_investments.amount11_cer_chf_requested";
	$function_fields[] ="cer_investments.amount18_cer_chf_requested";
	$function_fields[] ="cer_investments.amount19_cer_chf_requested";
	$function_fields[] ="cer_investments.total_amount_cer_chf_requested";
	
	
	$function_fields[] ="cer_investments.amount1_cer_loc_requested";
	$function_fields[] ="cer_investments.amount3_cer_loc_requested";
	$function_fields[] ="cer_investments.amount5_cer_loc_requested";
	$function_fields[] ="cer_investments.amount7_cer_loc_requested";
	$function_fields[] ="cer_investments.amount11_cer_loc_requested";
	$function_fields[] ="cer_investments.amount18_cer_loc_requested";
	$function_fields[] ="cer_investments.amount19_cer_loc_requested";
	$function_fields[] ="cer_investments.total_amount_cer_loc_requested";
	
	$function_fields[] ="cer_investments.total_amount_cer_chf_approved";
	$function_fields[] ="posorderinvestments.total_amount_cms_chf";
	$function_fields[] ="cer_investments.total_amount_cer_loc_approved";
	$function_fields[] ="posorderinvestments.total_amount_cms_loc";


	$function_fields[] = "cer_revenues.total_revenue";
	$function_fields[] = "cer_revenues.total_revenue_loc";
	$function_fields[] = "cer_revenues.total_revenue_first_year";
	$function_fields[] = "cer_revenues.total_revenue_first_year_loc";


	$function_fields[] ="cer_investments.amount1_cer_chf_requested2";
	$function_fields[] ="cer_investments.amount3_cer_chf_requested2";
	$function_fields[] ="cer_investments.amount5_cer_chf_requested2";
	$function_fields[] ="cer_investments.amount7_cer_chf_requested2";
	$function_fields[] ="cer_investments.amount11_cer_chf_requested2";
	$function_fields[] ="cer_investments.amount18_cer_chf_requested2";
	$function_fields[] ="cer_investments.amount19_cer_chf_requested2";
	$function_fields[] ="cer_investments.total_amount_cer_chf_requested2";
	
	
	$function_fields[] ="cer_investments.amount1_cer_loc_requested2";
	$function_fields[] ="cer_investments.amount3_cer_loc_requested2";
	$function_fields[] ="cer_investments.amount5_cer_loc_requested2";
	$function_fields[] ="cer_investments.amount7_cer_loc_requested2";
	$function_fields[] ="cer_investments.amount11_cer_loc_requested2";
	$function_fields[] ="cer_investments.amount18_cer_loc_requested2";
	$function_fields[] ="cer_investments.amount19_cer_loc_requested2";
	$function_fields[] ="cer_investments.total_amount_cer_loc_requested2";
	
	$function_fields[] ="cer_investments.total_amount_cer_chf_approved2";
	$function_fields[] ="posorderinvestments.total_amount_cms_chf2";
	$function_fields[] ="cer_investments.total_amount_cer_loc_approved2";
	$function_fields[] ="posorderinvestments.total_amount_cms_loc2";


	$function_fields[] = "cer_revenues.total_revenue2";
	$function_fields[] = "cer_revenues.total_revenue_loc2";
	$function_fields[] = "cer_revenues.total_revenue_first_year2";
	$function_fields[] = "cer_revenues.total_revenue_first_year_loc2";




	$function_fields[] = "total_corporate_stores";
	$function_fields[] = "total_franchisee_stores";
	$function_fields[] = "total_corporate_sis";
	$function_fields[] = "total_other_sis";
	$function_fields[] = "total_corporate_kiosks";
	$function_fields[] = "total_franchisee_kiosks";
	$function_fields[] = "total_independent_retailer";

	return $function_fields;

	
}

/********************************************************************
    get DB Info for POS Address Fields
*********************************************************************/
function query_posaddress_fields()
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$groups = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$content_by_function = array();
	$content_by_function_params = array();

	
	$fields["posaddresses.posaddress_salesregion"] = "Geographical Region";
	$fields["posaddresses.posaddress_region"] = "Supplied Region";
	$fields["posaddresses.posaddress_name"] = "POS Name";
	$fields["posaddresses.posaddress_name2"] = "POS Name 2";
	$fields["posaddresses.posaddress_address"] = "POS Address";
	$fields["posaddresses.posaddress_address2"] = "POS Address 2";
	$fields["posaddresses.posaddress_zip"] = "POS Zip";
	$fields["posaddresses.posaddress_place"] = "POS City";
	$fields["posaddresses.posaddress_province"] = "POS Province";
	$fields["posaddresses.posaddress_country"] = "POS Country";
	$fields["posaddresses.posaddress_phone"] = "POS Phone";
	$fields["posaddresses.posaddress_fax"] = "POS Fax";
	$fields["posaddresses.posaddress_email"] = "POS Email";
	$fields["posaddresses.posaddress_website"] = "POS Website";
	//$fields["posaddresses.posaddress_contact_name"] = "POS Contact Name";
	//$fields["posaddresses.posaddress_contact_email"] = "POS Contact Email";

	$fields["posaddresses.posaddress_store_openingdate"] = "POS Opening Date";
	$fields["posaddresses.posaddress_store_closingdate"] = "POS Closing Date";

	$fields["posaddresses.posaddress_eprepnr"] = "(Enterprise) Reporting Number";
	$fields["posaddresses.posaddress_sapnumber"] = "SAP Customer Code";

	$fields["posaddresses.posaddress_ownertype"] = "POS Legal Type";
	$fields["posaddresses.posaddress_store_postype"] = "POS Type";
	$fields["posaddresses.posaddress_store_subclass"] = "POS Type Subclass";
	
	$fields["posaddresses.posaddress_store_furniture"] = "POS Furniture Type";
	$fields["posaddresses.posaddress_store_furniture_subclass"] = "POS Furniture Subclass";
	$fields["posaddresses.posaddress_store_grosssurface"] = "POS Gross Surface";
	$fields["posaddresses.posaddress_store_totalsurface"] = "POS Total Surface";
	$fields["posaddresses.posaddress_store_retailarea"] = "POS Sales Surface";
	$fields["posaddresses.posaddress_store_backoffice"] = "POS Other Surface";
	$fields["posaddresses.posaddress_store_numfloors"] = "POS Number of Floors";
	$fields["posaddresses.posaddress_store_floorsurface1"] = "POS Surface Floor 1";
	$fields["posaddresses.posaddress_store_floorsurface2"] = "POS Surface Floor 2";
	$fields["posaddresses.posaddress_store_floorsurface3"] = "POS Surface Floor 3";
	$fields["posaddresses.posaddress_store_headcounts"] = "POS Headcounts";
	$fields["posaddresses.posaddress_store_fulltimeeqs"] = "POS Full Time Equivalents";
	
	$fields["posaddresses.neighbourhood"] = "Neighbourhood Location";

	$fields["posaddresses.neighbourhood"] = "Neighbourhood Areas";
	$fields["posaddresses.posaddress_neighbour_left"] = "Neighbours left";
	$fields["posaddresses.posaddress_neighbour_right"] = "Neighbours right";
	$fields["posaddresses.posaddress_neighbour_acrleft"] = "Neighbours across left";
	$fields["posaddresses.posaddress_neighbour_acrright"] = "Neighbours across left";
	$fields["posaddresses.posaddress_neighbour_brands"] = "Neighbours other Brands";

	
	$fields["posaddresses.posaddress_perc_class"] = "Class/Image Area";
	$fields["posaddresses.posaddress_perc_tourist"] = "Tourist/Historical Area";
	$fields["posaddresses.posaddress_perc_transport"] = "Public Transportation";
	$fields["posaddresses.posaddress_perc_people"] = "People Traffic Area";
	$fields["posaddresses.posaddress_perc_parking"] = "Parking Possibilities";
	$fields["posaddresses.posaddress_perc_visibility1"] = "Visibility from Pavement";
	$fields["posaddresses.posaddress_perc_visibility2"] = "Visibility from accross the Street";
	
	
	$fields["posaddresses.posaddress_export_to_web"] = "Visible in Store Locator";
	
	$fields["posaddresses.posaddress_local_production"] = "Local Production";
	
	$fields["posaddresses.projecthistory"] = "Project History";

	$fields["posaddresses.takoverdate"] = "Take Over Date";
	$fields["posaddresses.datelatestrenovation"] = "Date of Latest Renovation";
	$fields["posaddresses.posaddress_distribution_channel"] = "Distibution Channel";

	$fields["posaddresses.posaddress_google_lat"] = "Google Latitude";
	$fields["posaddresses.posaddress_google_long"] = "Google Longitude";



	$fields["posaddresses.comment"] = "Comment";

	$fields["posaddresses.openinghours"] = "Opening Hours";
	$fields["posaddresses.closinghours"] = "Closing Information";

	
	


	$attributes["posaddresses.posaddress_salesregion"] = "salesregions.salesregion_name";
	$attributes["posaddresses.posaddress_region"] = "regions.region_name";
	$attributes["posaddresses.posaddress_name"] = "posaddresses.posaddress_name";
	$attributes["posaddresses.posaddress_name2"] = "posaddresses.posaddress_name2";
	$attributes["posaddresses.posaddress_address"] = "posaddresses.posaddress_address";
	$attributes["posaddresses.posaddress_address2"] = "posaddresses.posaddress_address2";
	$attributes["posaddresses.posaddress_zip"] = "posaddresses.posaddress_zip";
	$attributes["posaddresses.posaddress_place"] = "posaddresses.posaddress_place";
	$attributes["posaddresses.posaddress_country"] = "poscountries.country_name";
	$attributes["posaddresses.posaddress_province"] = "posprovinces.province_canton";
	$attributes["posaddresses.posaddress_phone"] = "posaddresses.posaddress_phone";
	$attributes["posaddresses.posaddress_fax"] = "posaddresses.posaddress_fax";
	$attributes["posaddresses.posaddress_email"] = "posaddresses.posaddress_email";
	$attributes["posaddresses.posaddress_website"] = "posaddresses.posaddress_website";
	//$attributes["posaddresses.posaddress_contact_name"] = "posaddresses.posaddress_contact_name";
	//$attributes["posaddresses.posaddress_contact_email"] = "posaddresses.posaddress_contact_email";

	$attributes["posaddresses.posaddress_store_openingdate"] = "posaddresses.posaddress_store_openingdate";
	$attributes["posaddresses.posaddress_store_closingdate"] = "posaddresses.posaddress_store_closingdate";

	$attributes["posaddresses.posaddress_eprepnr"] = "posaddresses.posaddress_eprepnr";
	$attributes["posaddresses.posaddress_sapnumber"] = "posaddresses.posaddress_sapnumber";

	$attributes["posaddresses.posaddress_ownertype"] = "poscosttypes.project_costtype_text";
	$attributes["posaddresses.posaddress_store_postype"] = "postypes.postype_name";
	$attributes["posaddresses.posaddress_store_subclass"] = "possubclasses.possubclass_name";

	
	$attributes["posaddresses.posaddress_store_furniture"] = "posproductlines.product_line_name";
	$attributes["posaddresses.posaddress_store_furniture_subclass"] = "posproductline_subclasses.productline_subclass_name";
	$attributes["posaddresses.posaddress_store_grosssurface"] = "posaddresses.posaddress_store_grosssurface";
	$attributes["posaddresses.posaddress_store_totalsurface"] = "posaddresses.posaddress_store_totalsurface";
	$attributes["posaddresses.posaddress_store_retailarea"] = "posaddresses.posaddress_store_retailarea";
	$attributes["posaddresses.posaddress_store_backoffice"] = "posaddresses.posaddress_store_backoffice";
	$attributes["posaddresses.posaddress_store_numfloors"] = "posaddresses.posaddress_store_numfloors";
	$attributes["posaddresses.posaddress_store_floorsurface1"] = "posaddresses.posaddress_store_floorsurface1";
	$attributes["posaddresses.posaddress_store_floorsurface2"] = "posaddresses.posaddress_store_floorsurface2";
	$attributes["posaddresses.posaddress_store_floorsurface3"] = "posaddresses.posaddress_store_floorsurface3";
	$attributes["posaddresses.posaddress_store_headcounts"] = "posaddresses.posaddress_store_headcounts";
	$attributes["posaddresses.posaddress_store_fulltimeeqs"] = "posaddresses.posaddress_store_fulltimeeqs";

	$attributes["posaddresses.neighbourhood"] = "calculated_content";
	$attributes["posaddresses.posaddress_neighbour_left"] = "posaddresses.posaddress_neighbour_left";
	$attributes["posaddresses.posaddress_neighbour_right"] = "posaddresses.posaddress_neighbour_right";
	$attributes["posaddresses.posaddress_neighbour_acrleft"] = "posaddresses.posaddress_neighbour_acrleft";
	$attributes["posaddresses.posaddress_neighbour_acrright"] = "posaddresses.posaddress_neighbour_acrright";
	$attributes["posaddresses.posaddress_neighbour_brands"] = "posaddresses.posaddress_neighbour_brands";

	$attributes["posaddresses.posaddress_perc_class"] = "posaddresses.posaddress_perc_class";
	$attributes["posaddresses.posaddress_perc_tourist"] = "posaddresses.posaddress_perc_tourist";
	$attributes["posaddresses.posaddress_perc_transport"] = "posaddresses.posaddress_perc_transport";
	$attributes["posaddresses.posaddress_perc_people"] = "posaddresses.posaddress_perc_people";
	$attributes["posaddresses.posaddress_perc_parking"] = "posaddresses.posaddress_perc_parking";
	$attributes["posaddresses.posaddress_perc_visibility1"] = "posaddresses.posaddress_perc_visibility1";
	$attributes["posaddresses.posaddress_perc_visibility2"] = "posaddresses.posaddress_perc_visibility2";

	$attributes["posaddresses.posaddress_export_to_web"] = "posaddresses.posaddress_export_to_web";

	$attributes["posaddresses.posaddress_local_production"] = "posaddresses.posaddress_local_production";
	
	$attributes["posaddresses.projecthistory"] = "calculated_content";


	$attributes["posaddresses.takoverdate"] = "calculated_content";
	$attributes["posaddresses.datelatestrenovation"] = "calculated_content";
	$attributes["posaddresses.posaddress_distribution_channel"] = "mps_distchannels.mps_distchannel_code";

	$attributes["posaddresses.posaddress_google_lat"] = "posaddresses.posaddress_google_lat";
	$attributes["posaddresses.posaddress_google_long"] = "posaddresses.posaddress_google_long";




	$attributes["posaddresses.comment"] = "content_by_function";

	$attributes["posaddresses.openinghours"] = "content_by_function";
	$attributes["posaddresses.closinghours"] = "content_by_function";

	

	$datatypes["posaddresses.posaddress_salesregion"] = "text";
	$datatypes["posaddresses.posaddress_region"] = "text";
	$datatypes["posaddresses.posaddress_name"] = "text";
	$datatypes["posaddresses.posaddress_name2"] = "text";
	$datatypes["posaddresses.posaddress_address"] = "text";
	$datatypes["posaddresses.posaddress_address2"] = "text";
	$datatypes["posaddresses.posaddress_zip"] = "text";
	$datatypes["posaddresses.posaddress_place"] = "text";
	$datatypes["posaddresses.posaddress_country"] = "text";
	$datatypes["posaddresses.posaddress_province"] = "text";
	$datatypes["posaddresses.posaddress_phone"] = "text";
	$datatypes["posaddresses.posaddress_fax"] = "text";
	$datatypes["posaddresses.posaddress_email"] = "text";
	$datatypes["posaddresses.posaddress_website"] = "text";
	//$datatypes["posaddresses.posaddress_contact_name"] = "text";
	//$datatypes["posaddresses.posaddress_contact_email"] = "text";

	$datatypes["posaddresses.posaddress_store_openingdate"] = "date";
	$datatypes["posaddresses.posaddress_store_closingdate"] = "date";
	

	$datatypes["posaddresses.posaddress_eprepnr"] = "text";
	$datatypes["posaddresses.posaddress_sapnumber"] = "text";

	$datatypes["posaddresses.posaddress_ownertype"] = "text";
	$datatypes["posaddresses.posaddress_store_postype"] = "text";
	$datatypes["posaddresses.posaddress_store_subclass"] = "text";


	
	$datatypes["posaddresses.posaddress_store_furniture"] = "text";
	$datatypes["posaddresses.posaddress_store_furniture_subclass"] = "text";
	$datatypes["posaddresses.posaddress_store_grosssurface"] = "decimal2";
	$datatypes["posaddresses.posaddress_store_totalsurface"] = "decimal2";
	$datatypes["posaddresses.posaddress_store_retailarea"] = "decimal2";
	$datatypes["posaddresses.posaddress_store_backoffice"] = "decimal2";
	$datatypes["posaddresses.posaddress_store_numfloors"] = "decimal2";
	$datatypes["posaddresses.posaddress_store_floorsurface1"] = "decimal2";
	$datatypes["posaddresses.posaddress_store_floorsurface2"] = "decimal2";
	$datatypes["posaddresses.posaddress_store_floorsurface3"] = "decimal2";
	$datatypes["posaddresses.posaddress_store_headcounts"] = "decimal2";
	$datatypes["posaddresses.posaddress_store_fulltimeeqs"] = "decimal2";

	$datatypes["posaddresses.neighbourhood"] = "text";
	$datatypes["posaddresses.posaddress_neighbour_left"] = "text";
	$datatypes["posaddresses.posaddress_neighbour_right"] = "text";
	$datatypes["posaddresses.posaddress_neighbour_acrleft"] = "text";
	$datatypes["posaddresses.posaddress_neighbour_acrright"] = "text";
	$datatypes["posaddresses.posaddress_neighbour_brands"] = "text";


	$datatypes["posaddresses.posaddress_perc_class"] = "text";
	$datatypes["posaddresses.posaddress_perc_tourist"] = "text";
	$datatypes["posaddresses.posaddress_perc_transport"] = "text";
	$datatypes["posaddresses.posaddress_perc_people"] = "text";
	$datatypes["posaddresses.posaddress_perc_parking"] = "text";
	$datatypes["posaddresses.posaddress_perc_visibility1"] = "text";
	$datatypes["posaddresses.posaddress_perc_visibility2"] = "text";

	$datatypes["posaddresses.posaddress_export_to_web"] = "boolean";

	$datatypes["posaddresses.posaddress_local_production"] = "boolean";
	
	$datatypes["posaddresses.projecthistory"] = "text";

	$datatypes["posaddresses.takoverdate"] = "text";
	$datatypes["posaddresses.datelatestrenovation"] = "text";
	$datatypes["posaddresses.posaddress_distribution_channel"] = "text";


	$datatypes["posaddresses.posaddress_google_lat"] = "text";
	$datatypes["posaddresses.posaddress_google_long"] = "text";




	$datatypes["posaddresses.comment"] = "text";

	$datatypes["posaddresses.openinghours"] = "openinghrs";
	$datatypes["posaddresses.closinghours"] = "text";



	$calculated_content["posaddresses.neighbourhood"] = "select posareatype_name from posareas left join posareatypes on posareatype_id = posarea_area where posarea_posaddress = ";
	$calculated_content_key["posaddresses.neighbourhood"] = "posaddress_id";
	$calculated_content_field["posaddresses.neighbourhood"] = "posaddresses.neighbourhood";
	$calculated_content_sort_order["posaddresses.neighbourhood"] = "";



	

	$calculated_content["posaddresses.projecthistory"] = "select concat(projectkind_name, ': ', DAY(posorder_opening_date), '.', MONTH(posorder_opening_date), '.', YEAR(posorder_opening_date)) from posorders left join projectkinds on projectkind_id = posorder_project_kind where posorder_type = 1 and posorder_opening_date is not NULL and posorder_opening_date <> '0000-00-00' and posorder_posaddress = ";
	$calculated_content_key["posaddresses.projecthistory"] = "posaddress_id";
	$calculated_content_field["posaddresses.projecthistory"] = "posaddresses.projecthistory";
	$calculated_content_sort_order["posaddresses.projecthistory"] = "posorder_opening_date";

	
	$calculated_content["posaddresses.takoverdate"] = "select Concat(DAY(posorder_opening_date), '.', MONTH(posorder_opening_date), '.', YEAR(posorder_opening_date)) from posorders where posorder_type = 1 and posorder_opening_date is not NULL and posorder_opening_date <> '0000-00-00' and posorder_project_kind in (3,4) and posorder_posaddress = ";
	$calculated_content_key["posaddresses.takoverdate"] = "posaddress_id";
	$calculated_content_field["posaddresses.takoverdate"] = "posaddresses.takoverdate";
	$calculated_content_sort_order["posaddresses.takoverdate"] = "";


	$calculated_content["posaddresses.datelatestrenovation"] = "select Concat(DAY(posorder_opening_date), '.', MONTH(posorder_opening_date), '.', YEAR(posorder_opening_date)) from posorders where posorder_type = 1 and posorder_opening_date is not NULL and posorder_opening_date <> '0000-00-00' and posorder_project_kind in (2, 3) and posorder_posaddress = ";
	$calculated_content_key["posaddresses.datelatestrenovation"] = "posaddress_id";
	$calculated_content_field["posaddresses.datelatestrenovation"] = "posaddresses.datelatestrenovation";
	$calculated_content_sort_order["posaddresses.datelatestrenovation"] = "posorder_opening_date DESC limit 0,1";


	$content_by_function["posaddresses.comment"] = "get_empty_column(params)";
	$content_by_function_params["posaddresses.comment"] = array('posaddress_id');
	


	$content_by_function["posaddresses.openinghours"] = "get_pos_opening_hours(params)";
	$content_by_function_params["posaddresses.openinghours"] = array('posaddress_id');

	$content_by_function["posaddresses.closinghours"] = "get_pos_closing_hours(params)";
	$content_by_function_params["posaddresses.closinghours"] = array('posaddress_id');


	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["calculated_content_sort_order"] = $calculated_content_sort_order;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;
	
	return $db_info;
}

/********************************************************************
    get DB Info for posleases fields
*********************************************************************/
function query_posleases_fields()
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$calculated_content_field = array();
	$content_by_function = array();
	$content_by_function_params = array();

	$fields["posleases.poslease_lease_type"] = "Lease Type";
	$fields["posleases.poslease_startdate"] = "Start Date";
	$fields["posleases.poslease_enddate"] = "Expiry Date";
	$fields["posleases.poslease_extensionoption"] = "Extension Option to Date";
	$fields["posleases.poslease_exitoption"] = "Exit Option to Date";
	$fields["posleases.poslease_termination_time"] = "Termination deadline";
	$fields["posleases.poslease_anual_rent"] = "Annual Rent";
	$fields["posleases.poslease_salespercent"] = "Annual Rent in % of Sales";
	$fields["posleases.poslease_isindexed"] = "Tacit Renewal Clause";
	$fields["posleases.poslease_indexclause_in_contract"] = "Index Clause in Contract";
	$fields["posleases.poslease_handoverdate"] = "Handover Date (Key)";
	$fields["posleases.poslease_firstrentpayed"] = "First Rent Paid on";
	$fields["posleases.poslease_freeweeks"] = "Rent Free Period in Weeks";
	$fields["posleases.poslease_rent_review_date"] = "Rent Review Date";

	$attributes["posleases.poslease_lease_type"] = "poslease_types.poslease_type_name";
	$attributes["posleases.poslease_startdate"] = "posleases.poslease_startdate";
	$attributes["posleases.poslease_enddate"] = "posleases.poslease_enddate";
	$attributes["posleases.poslease_extensionoption"] = "posleases.poslease_extensionoption";
	$attributes["posleases.poslease_exitoption"] = "posleases.poslease_exitoption";
	$attributes["posleases.poslease_termination_time"] = "posleases.poslease_termination_time";
	$attributes["posleases.poslease_anual_rent"] = "posleases.poslease_anual_rent";
	$attributes["posleases.poslease_salespercent"] = "posleases.poslease_salespercent";
	$attributes["posleases.poslease_isindexed"] = "posleases.poslease_isindexed";
	$attributes["posleases.poslease_indexclause_in_contract"] = "posleases.poslease_indexclause_in_contract";
	$attributes["posleases.poslease_handoverdate"] = "posleases.poslease_handoverdate";
	$attributes["posleases.poslease_firstrentpayed"] = "posleases.poslease_firstrentpayed";
	$attributes["posleases.poslease_freeweeks"] = "posleases.poslease_freeweeks";
	$attributes["posleases.poslease_rent_review_date"] = "posleases.poslease_rent_review_date";


	$datatypes["posleases.poslease_lease_type"] = "text";
	$datatypes["posleases.poslease_startdate"] = "date";
	$datatypes["posleases.poslease_enddate"] = "date";
	$datatypes["posleases.poslease_extensionoption"] = "date";
	$datatypes["posleases.poslease_exitoption"] = "date";
	$datatypes["posleases.poslease_termination_time"] = "text";
	$datatypes["posleases.poslease_anual_rent"] = "decimal2";
	$datatypes["posleases.poslease_salespercent"] = "decimal2";
	$datatypes["posleases.poslease_isindexed"] = "boolean";
	$datatypes["posleases.poslease_indexclause_in_contract"] = "boolean";
	$datatypes["posleases.poslease_handoverdate"] = "date";
	$datatypes["posleases.poslease_firstrentpayed"] = "date";
	$datatypes["posleases.poslease_freeweeks"] = "text";
	$datatypes["posleases.poslease_rent_review_date"] = "date";

	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;

	return $db_info;
}


/********************************************************************
    get DB Info for posclosing fields
*********************************************************************/
function query_posclosings_fields()
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$calculated_content_field = array();
	$content_by_function = array();
	$content_by_function_params = array();

	$fields["posclosings.posclosing_project"] = "Project";
	$fields["posclosings.posclosing_startdate"] = "Start Date";
	$fields["posclosings.posclosing_enddate"] = "End Date";
	$fields["posclosings.posclosing_reason"] = "Reason";

	$attributes["posclosings.posclosing_project"] = "projects.project_number";
	$attributes["posclosings.posclosing_startdate"] = "posclosings.posclosing_startdate";
	$attributes["posclosings.posclosing_enddate"] = "posclosings.posclosing_enddate";
	$attributes["posclosings.posclosing_reason"] = "posclosings.posclosing_reason";

	$datatypes["posclosings.posclosing_project"] = "text";
	$datatypes["posclosings.posclosing_startdate"] = "date";
	$datatypes["posclosings.posclosing_enddate"] = "date";
	$datatypes["posclosings.posclosing_reason"] = "text";

	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;

	return $db_info;
}

/********************************************************************
    get DB Info for Client Address Fields
*********************************************************************/
function query_clientaddress_fields()
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$content_by_function = array();
	$content_by_function_params = array();

	$fields["clients.address_client_type"] = "Client Type";
	$fields["clients.address_company"] = "Client Company";
	$fields["clients.address_company2"] = "Client Company 2";
	$fields["clients.address_address"] = "Client Address";
	$fields["clients.address_address2"] = "Client Address 2";
	$fields["clients.address_zip"] = "Client Zip";
	$fields["clients.address_place"] = "Client City";
	$fields["clients.address_country"] = "Client Country";
	$fields["clients.address_phone"] = "Client Phone";
	$fields["clients.address_fax"] = "Client Fax";
	$fields["clients.address_email"] = "Client Email";
	$fields["clients.address_website"] = "Client Website";
	$fields["clients.address_contact_name"] = "Client Contact Name";
	$fields["clients.address_contact_email"] = "Client Contact Email";

	$attributes["clients.address_client_type"] = "clienttypes.client_type_code";
	$attributes["clients.address_company"] = "clients.address_company";
	$attributes["clients.address_company2"] = "clients.address_company2";
	$attributes["clients.address_address"] = "clients.address_address";
	$attributes["clients.address_address2"] = "clients.address_address2";
	$attributes["clients.address_zip"] = "clients.address_zip";
	$attributes["clients.address_place"] = "clients.address_place";
	$attributes["clients.address_country"] = "clientcountries.country_name";
	$attributes["clients.address_phone"] = "clients.address_phone";
	$attributes["clients.address_fax"] = "clients.address_fax";
	$attributes["clients.address_email"] = "clients.address_email";
	$attributes["clients.address_website"] = "clients.address_website";
	$attributes["clients.address_contact_name"] = "clients.address_contact_name";
	$attributes["clients.address_contact_email"] = "clients.address_contact_email";

	$datatypes["clients.address_client_type"] = "text";
	$datatypes["clients.address_company"] = "text";
	$datatypes["clients.address_company2"] = "text";
	$datatypes["clients.address_address"] = "text";
	$datatypes["clients.address_address2"] = "text";
	$datatypes["clients.address_zip"] = "text";
	$datatypes["clients.address_place"] = "text";
	$datatypes["clients.address_country"] = "text";
	$datatypes["clients.address_phone"] = "text";
	$datatypes["clients.address_fax"] = "text";
	$datatypes["clients.address_email"] = "text";
	$datatypes["clients.address_website"] = "text";
	$datatypes["clients.address_contact_name"] = "text";
	$datatypes["clients.address_contact_email"] = "text";


	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;

	return $db_info;
}

/********************************************************************
    get DB Info for franchisee Address Fields
*********************************************************************/
function query_franchiseeaddress_fields()
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$content_by_function = array();
	$content_by_function_params = array();

	$fields["franchisees.address_company"] = "Franchisee Company";
	$fields["franchisees.address_company2"] = "Franchisee Company 2";
	$fields["franchisees.address_address"] = "Franchisee Address";
	$fields["franchisees.address_address2"] = "Franchisee Address 2";
	$fields["franchisees.address_zip"] = "Franchisee Zip";
	$fields["franchisees.address_place"] = "Franchisee City";
	$fields["franchisees.address_country"] = "Franchisee Country";
	$fields["franchisees.address_phone"] = "Franchisee Phone";
	$fields["franchisees.address_fax"] = "Franchisee Fax";
	$fields["franchisees.address_email"] = "Franchisee Email";
	$fields["franchisees.address_website"] = "Franchisee Website";
	$fields["franchisees.address_contact_name"] = "Franchisee Contact Name";
	$fields["franchisees.address_contact_email"] = "Franchisee Contact Email";

	$fields["posaddresses.posaddress_fagagreement_type"] = "Franchisee Agreement Type";
	$fields["posaddresses.posaddress_fagrsent"] = "Franchisee Agreement Sent";
	$fields["posaddresses.posaddress_fagrsigned"] = "Franchisee Agreement Signed";
	$fields["posaddresses.posaddress_fagrstart"] = "Franchisee Agreement Start";
	$fields["posaddresses.posaddress_fagrend"] = "Franchisee Agreement End";
	$fields["posaddresses.posaddress_fargrduration"] = "Franchisee Agreement Duration";
	$fields["posaddresses.posaddress_fagcancellation"] = "Franchisee Agreement Cancelled";

	$attributes["franchisees.address_company"] = "franchisees.address_company";
	$attributes["franchisees.address_company2"] = "franchisees.address_company2";
	$attributes["franchisees.address_address"] = "franchisees.address_address";
	$attributes["franchisees.address_address2"] = "franchisees.address_address2";
	$attributes["franchisees.address_zip"] = "franchisees.address_zip";
	$attributes["franchisees.address_place"] = "franchisees.address_place";
	$attributes["franchisees.address_country"] = "franchiseecountries.country_name";
	$attributes["franchisees.address_phone"] = "franchisees.address_phone";
	$attributes["franchisees.address_fax"] = "franchisees.address_fax";
	$attributes["franchisees.address_email"] = "franchisees.address_email";
	$attributes["franchisees.address_website"] = "franchisees.address_website";
	$attributes["franchisees.address_contact_name"] = "franchisees.address_contact_name";
	$attributes["franchisees.address_contact_email"] = "franchisees.address_contact_email";

	$attributes["posaddresses.posaddress_fagagreement_type"] = "posagreements.agreement_type_name";
	$attributes["posaddresses.posaddress_fagrsent"] = "posaddress_fagrsent";
	$attributes["posaddresses.posaddress_fagrsigned"] = "posaddress_fagrsigned";
	$attributes["posaddresses.posaddress_fagrstart"] = "posaddress_fagrstart";
	$attributes["posaddresses.posaddress_fagrend"] = "posaddress_fagrend";
	$attributes["posaddresses.posaddress_fargrduration"] = "posaddress_fargrduration";
	$attributes["posaddresses.posaddress_fagcancellation"] = "posaddress_fagcancellation";
	
	$datatypes["franchisees.address_company"] = "text";
	$datatypes["franchisees.address_company2"] = "text";
	$datatypes["franchisees.address_address"] = "text";
	$datatypes["franchisees.address_address2"] = "text";
	$datatypes["franchisees.address_zip"] = "text";
	$datatypes["franchisees.address_place"] = "text";
	$datatypes["franchisees.address_country"] = "text";
	$datatypes["franchisees.address_phone"] = "text";
	$datatypes["franchisees.address_fax"] = "text";
	$datatypes["franchisees.address_email"] = "text";
	$datatypes["franchisees.address_website"] = "text";
	$datatypes["franchisees.address_contact_name"] = "text";
	$datatypes["franchisees.address_contact_email"] = "text";

	$datatypes["posaddresses.posaddress_fagagreement_type"] = "text";
	$datatypes["posaddresses.posaddress_fagrsent"] = "boolean";
	$datatypes["posaddresses.posaddress_fagrsigned"] = "boolean";
	$datatypes["posaddresses.posaddress_fagrstart"] = "date";
	$datatypes["posaddresses.posaddress_fagrend"] = "date";
	$datatypes["posaddresses.posaddress_fargrduration"] = "text";
	$datatypes["posaddresses.posaddress_fagcancellation"] = "date";

	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;

	return $db_info;
}


/********************************************************************
    get DB Info for posorder fields
*********************************************************************/
function query_posorder_fields($table_posorders_involved = 1)
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$calculated_content_field = array();
	$content_by_function = array();
	$content_by_function_params = array();

	$fields["posorders.posorder_ordernumber"] = "Project Number";
	$fields["posorders.posorder_product_line"] = "Product Line";
	$fields["posorders.posorder_product_line_subclass"] = "Product Line Subclass";
	$fields["posorders.posorder_postype"] = "POS Type";
	$fields["posorders.posorder_subclass"] = "POS Type Subclass";
	$fields["posorders.posorder_project_kind"] = "Project Type";
	$fields["posorders.posorder_legal_type"] = "Project Legal Type";
	$fields["projects.project_furniture_height_mm"] = "Furniture Height";
	$fields["project_costs.project_cost_sqms"] = "Project Sales Surface";
	$fields["projects.project_actual_opening_date"] = "Actual Opening Date";
	$fields["projects.project_shop_closingdate"] = "Closing Date";

	$fields["cer_investments.amount1_cer_chf_requested"] = "Construction Requested Amount in CHF";
	$fields["cer_investments.amount3_cer_chf_requested"] = "Store fixturing Requested Amount in CHF";
	$fields["cer_investments.amount5_cer_chf_requested"] = "Architectural costs Requested Amount in CHF";
	$fields["cer_investments.amount7_cer_chf_requested"] = "Equipment Requested Amount in CHF";
	$fields["cer_investments.amount11_cer_chf_requested"] = "Other Requested Amount in CHF";
	$fields["cer_investments.amount18_cer_chf_requested"] = "Merchandising Material Requested Amount in CHF";
	$fields["cer_investments.amount19_cer_chf_requested"] = "Transportation Requested Amount in CHF";
	$fields["cer_investments.total_amount_cer_chf_requested"] = "Investment Total Requested Amount in CHF";
	
	
	$fields["cer_investments.amount1_cer_loc_requested"] = "Construction Requested Amount in LOC";
	$fields["cer_investments.amount3_cer_loc_requested"] = "Store fixturing Requested Amount in LOC";
	$fields["cer_investments.amount5_cer_loc_requested"] = "Architectural costs Requested Amount in LOC";
	$fields["cer_investments.amount7_cer_loc_requested"] = "Equipment Requested Amount in LOC";
	$fields["cer_investments.amount11_cer_loc_requested"] = "Other Requested Amount in LOC";
	$fields["cer_investments.amount18_cer_loc_requested"] = "Merchandising Material Requested Amount in LOC";
	$fields["cer_investments.amount19_cer_loc_requested"] = "Transportation Requested Amount in LOC";
	$fields["cer_investments.total_amount_cer_loc_requested"] = "Investment Total Requested Amount in LOC";
	
	
	
	
	$fields["cer_investments.total_amount_cer_chf_approved"] = "Investment Total Amount KL-approved in CHF";
	$fields["posorderinvestments.total_amount_cms_chf"] = "Investment Real Amounts in CHF";
	$fields["cer_investments.total_amount_cer_loc_approved"] = "Investment Total Amount KL-approved in LOC";
	$fields["posorderinvestments.total_amount_cms_loc"] = "Investment Real Amounts in LOC";

	
	$fields["cer_investments.delta_requested_real"] = "Delta Investment Requested/Real Amounts";

	$fields["cer_revenues.total_revenue"] = "Projected Sales in CHF";
	$fields["cer_revenues.total_revenue_loc"] = "Projected Sales in LOC";
	$fields["cer_revenues.total_revenue_first_year"] = "Projected Sales First Full Year in CHF";
	$fields["cer_revenues.total_revenue_first_year_loc"] = "Projected Sales First Full Year in LOC";

	
	$attributes["posorders.posorder_ordernumber"] = "posorders.posorder_ordernumber";
	$attributes["posorders.posorder_product_line"] = "posorderproductlines.product_line_name";
	$attributes["posorders.posorder_product_line_subclass"] = "posorderproductlinesubclasses.productline_subclass_name";
	$attributes["posorders.posorder_postype"] = "posorderprojecttypes.postype_name";
	$attributes["posorders.posorder_subclass"] = "posordersubclasses.possubclass_name";
	$attributes["posorders.posorder_project_kind"] = "posorderprojectkinds.projectkind_name";
	$attributes["posorders.posorder_legal_type"] = "posordercosttypes.project_costtype_text";
	$attributes["projects.project_furniture_height_mm"] = "projects.project_furniture_height_mm";
	$attributes["project_costs.project_cost_sqms"] = "project_costs.project_cost_sqms";
	$attributes["projects.project_actual_opening_date"] = "projects.project_actual_opening_date";
	$attributes["projects.project_shop_closingdate"] = "projects.project_shop_closingdate";

	
	$attributes["cer_investments.amount1_cer_chf_requested"] = "content_by_function";
	$attributes["cer_investments.amount3_cer_chf_requested"] = "content_by_function";
	$attributes["cer_investments.amount5_cer_chf_requested"] = "content_by_function";
	$attributes["cer_investments.amount7_cer_chf_requested"] = "content_by_function";
	$attributes["cer_investments.amount11_cer_chf_requested"] = "content_by_function";
	$attributes["cer_investments.amount18_cer_chf_requested"] = "content_by_function";
	$attributes["cer_investments.amount19_cer_chf_requested"] = "content_by_function";
	$attributes["cer_investments.total_amount_cer_chf_requested"] = "content_by_function";


	$attributes["cer_investments.amount1_cer_loc_requested"] = "content_by_function";
	$attributes["cer_investments.amount3_cer_loc_requested"] = "content_by_function";
	$attributes["cer_investments.amount5_cer_loc_requested"] = "content_by_function";
	$attributes["cer_investments.amount7_cer_loc_requested"] = "content_by_function";
	$attributes["cer_investments.amount11_cer_loc_requested"] = "content_by_function";
	$attributes["cer_investments.amount18_cer_loc_requested"] = "content_by_function";
	$attributes["cer_investments.amount19_cer_loc_requested"] = "content_by_function";
	$attributes["cer_investments.total_amount_cer_loc_requested"] = "content_by_function";

	$attributes["cer_investments.total_amount_cer_chf_approved"] = "content_by_function";
	$attributes["posorderinvestments.total_amount_cms_chf"] = "content_by_function";
	$attributes["cer_investments.total_amount_cer_loc_approved"] = "content_by_function";
	$attributes["posorderinvestments.total_amount_cms_loc"] = "content_by_function";

	$attributes["cer_investments.delta_requested_real"] = "content_by_function";


	$attributes["cer_revenues.total_revenue"] = "content_by_function";
	$attributes["cer_revenues.total_revenue_loc"] = "content_by_function";
	$attributes["cer_revenues.total_revenue_first_year"] = "content_by_function";
	$attributes["cer_revenues.total_revenue_first_year_loc"] = "content_by_function";

	$datatypes["posorders.posorder_ordernumber"] = "text";
	$datatypes["posorders.posorder_product_line"] = "text";
	$datatypes["posorders.posorder_product_line_subclass"] = "text";
	$datatypes["posorders.posorder_postype"] = "text";
	$datatypes["posorders.posorder_subclass"] = "text";
	$datatypes["posorders.posorder_project_kind"] = "text";
	$datatypes["posorders.posorder_legal_type"] = "text";
	$datatypes["projects.project_furniture_height_mm"] = "integer";
	$datatypes["project_costs.project_cost_sqms"] = "decimal2";
	$datatypes["projects.project_actual_opening_date"] = "date";
	$datatypes["projects.project_shop_closingdate"] = "date";

	
	$datatypes["cer_investments.amount1_cer_chf_requested"] = "decimal2";
	$datatypes["cer_investments.amount3_cer_chf_requested"] = "decimal2";
	$datatypes["cer_investments.amount5_cer_chf_requested"] = "decimal2";
	$datatypes["cer_investments.amount7_cer_chf_requested"] = "decimal2";
	$datatypes["cer_investments.amount11_cer_chf_requested"] = "decimal2";
	$datatypes["cer_investments.amount18_cer_chf_requested"] = "decimal2";
	$datatypes["cer_investments.amount19_cer_chf_requested"] = "decimal2";
	$datatypes["cer_investments.total_amount_cer_chf_requested"] = "decimal2";


	$datatypes["cer_investments.amount1_cer_loc_requested"] = "decimal2";
	$datatypes["cer_investments.amount3_cer_loc_requested"] = "decimal2";
	$datatypes["cer_investments.amount5_cer_loc_requested"] = "decimal2";
	$datatypes["cer_investments.amount7_cer_loc_requested"] = "decimal2";
	$datatypes["cer_investments.amount11_cer_loc_requested"] = "decimal2";
	$datatypes["cer_investments.amount18_cer_loc_requested"] = "decimal2";
	$datatypes["cer_investments.amount19_cer_loc_requested"] = "decimal2";
	$datatypes["cer_investments.total_amount_cer_loc_requested"] = "decimal2";


	$datatypes["cer_investments.total_amount_cer_chf_approved"] = "decimal2";
	$datatypes["posorderinvestments.total_amount_cms_chf"] = "decimal2";
	$datatypes["cer_investments.total_amount_cer_loc_approved"] = "decimal2";
	$datatypes["posorderinvestments.total_amount_cms_loc"] = "decimal2";

	$datatypes["cer_investments.delta_requested_real"] = "percent";

	$datatypes["cer_revenues.total_revenue"] = "decimal2";
	$datatypes["cer_revenues.total_revenue_loc"] = "decimal2";
	$datatypes["cer_revenues.total_revenue_first_year"] = "decimal2";
	$datatypes["cer_revenues.total_revenue_first_year_loc"] = "decimal2";

	
	
	$content_by_function["cer_investments.amount1_cer_chf_requested"] = "get_project_data(params)";
	$content_by_function["cer_investments.amount3_cer_chf_requested"] = "get_project_data(params)";
	$content_by_function["cer_investments.amount5_cer_chf_requested"] = "get_project_data(params)";
	$content_by_function["cer_investments.amount7_cer_chf_requested"] = "get_project_data(params)";
	$content_by_function["cer_investments.amount11_cer_chf_requested"] = "get_project_data(params)";
	$content_by_function["cer_investments.amount18_cer_chf_requested"] = "get_project_data(params)";
	$content_by_function["cer_investments.amount19_cer_chf_requested"] = "get_project_data(params)";
	$content_by_function["cer_investments.total_amount_cer_chf_requested"] = "get_project_data(params)";


	$content_by_function["cer_investments.amount1_cer_loc_requested"] = "get_project_data(params)";
	$content_by_function["cer_investments.amount3_cer_loc_requested"] = "get_project_data(params)";
	$content_by_function["cer_investments.amount5_cer_loc_requested"] = "get_project_data(params)";
	$content_by_function["cer_investments.amount7_cer_loc_requested"] = "get_project_data(params)";
	$content_by_function["cer_investments.amount11_cer_loc_requested"] = "get_project_data(params)";
	$content_by_function["cer_investments.amount18_cer_loc_requested"] = "get_project_data(params)";
	$content_by_function["cer_investments.amount19_cer_loc_requested"] = "get_project_data(params)";
	$content_by_function["cer_investments.total_amount_cer_loc_requested"] = "get_project_data(params)";
	

	$content_by_function["cer_investments.total_amount_cer_chf_approved"] = "get_project_data(params)";
	$content_by_function["posorderinvestments.total_amount_cms_chf"] = "get_project_data(params)";
	$content_by_function["cer_investments.total_amount_cer_loc_approved"] = "get_project_data(params)";
	$content_by_function["posorderinvestments.total_amount_cms_loc"] = "get_project_data(params)";

	$content_by_function["cer_investments.delta_requested_real"] = "get_project_data(params)";

	$content_by_function["cer_revenues.total_revenue"] = "get_project_data(params)";
	$content_by_function["cer_revenues.total_revenue_loc"] = "get_project_data(params)";
	$content_by_function["cer_revenues.total_revenue_first_year"] = "get_project_data(params)";
	$content_by_function["cer_revenues.total_revenue_first_year_loc"] = "get_project_data(params)";

	
	$content_by_function_params["cer_investments.amount1_cer_chf_requested"] =  array('posorder_id', 'total_amount_cer_chf_requested', 1);
	$content_by_function_params["cer_investments.amount3_cer_chf_requested"] =  array('posorder_id', 'total_amount_cer_chf_requested', 3);
	$content_by_function_params["cer_investments.amount5_cer_chf_requested"] =  array('posorder_id', 'total_amount_cer_chf_requested', 5);
	$content_by_function_params["cer_investments.amount7_cer_chf_requested"] =  array('posorder_id', 'total_amount_cer_chf_requested', 7);
	$content_by_function_params["cer_investments.amount11_cer_chf_requested"] =  array('posorder_id', 'total_amount_cer_chf_requested', 11);
	$content_by_function_params["cer_investments.amount18_cer_chf_requested"] =  array('posorder_id', 'total_amount_cer_chf_requested', 18);
	$content_by_function_params["cer_investments.amount19_cer_chf_requested"] =  array('posorder_id', 'total_amount_cer_chf_requested', 19);
	$content_by_function_params["cer_investments.total_amount_cer_chf_requested"] = array('posorder_id', 'total_amount_cer_chf_requested');


	$content_by_function_params["cer_investments.amount1_cer_loc_requested"] = array('posorder_id', 'total_amount_cer_loc_requested', 1);
	$content_by_function_params["cer_investments.amount3_cer_loc_requested"] = array('posorder_id', 'total_amount_cer_loc_requested', 3);
	$content_by_function_params["cer_investments.amount5_cer_loc_requested"] = array('posorder_id', 'total_amount_cer_loc_requested', 5);
	$content_by_function_params["cer_investments.amount7_cer_loc_requested"] = array('posorder_id', 'total_amount_cer_loc_requested', 7);
	$content_by_function_params["cer_investments.amount11_cer_loc_requested"] = array('posorder_id', 'total_amount_cer_loc_requested', 11);
	$content_by_function_params["cer_investments.amount18_cer_loc_requested"] = array('posorder_id', 'total_amount_cer_loc_requested', 18);
	$content_by_function_params["cer_investments.amount19_cer_loc_requested"] = array('posorder_id', 'total_amount_cer_loc_requested', 19);
	$content_by_function_params["cer_investments.total_amount_cer_loc_requested"] = array('posorder_id', 'total_amount_cer_loc_requested');
	
	$content_by_function_params["cer_investments.total_amount_cer_chf_approved"] = array('posorder_id', 'total_amount_cer_chf_approved');
	$content_by_function_params["posorderinvestments.total_amount_cms_chf"] = array('posorder_id', 'total_amount_cms_chf');
	$content_by_function_params["cer_investments.total_amount_cer_loc_approved"] = array('posorder_id', 'total_amount_cer_loc_approved');
	$content_by_function_params["posorderinvestments.total_amount_cms_loc"] = array('posorder_id', 'total_amount_cms_loc');

	$content_by_function_params["cer_investments.delta_requested_real"] = array('posorder_id', 'delta_requested_real');


	$content_by_function_params["cer_revenues.total_revenue"] = array('posorder_id', 'total_revenue');
	$content_by_function_params["cer_revenues.total_revenue_loc"] = array('posorder_id', 'total_revenue_loc');
	$content_by_function_params["cer_revenues.total_revenue_first_year"] = array('posorder_id', 'total_revenue_first_year');
	$content_by_function_params["cer_revenues.total_revenue_first_year_loc"] = array('posorder_id', 'total_revenue_first_year_loc');


	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;

	return $db_info;
}



/********************************************************************
    get DB Info for posorder fields
*********************************************************************/
function query_latest_posorder_fields($table_posorders_involved = 1)
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$calculated_content_field = array();
	$content_by_function = array();
	$content_by_function_params = array();

	$fields["posorders2.posorder_ordernumber"] = "Project Number";
	$fields["posorders2.posorder_product_line"] = "Product Line";
	$fields["posorders2.posorder_product_line_subclass"] = "Product Line Subclass";
	$fields["posorders2.posorder_postype"] = "POS Type";
	$fields["posorders2.posorder_subclass"] = "POS Type Subclass";
	$fields["posorders2.posorder_project_kind"] = "Project Type";
	$fields["posorders2.posorder_legal_type"] = "Project Legal Type";
	$fields["projects.project_furniture_height_mm"] = "Furniture Height";
	$fields["project_costs.project_cost_sqms"] = "Project Sales Surface";
	$fields["projects.project_actual_opening_date"] = "Actual Opening Date";

	
	$fields["cer_investments.amount1_cer_chf_requested2"] = "Construction Requested Amount in CHF";
	$fields["cer_investments.amount3_cer_chf_requested2"] = "Store fixturing Requested Amount in CHF";
	$fields["cer_investments.amount5_cer_chf_requested2"] = "Architectural costs Requested Amount in CHF";
	$fields["cer_investments.amount7_cer_chf_requested2"] = "Equipment Requested Amount in CHF";
	$fields["cer_investments.amount11_cer_chf_requested2"] = "Other Requested Amount in CHF";
	$fields["cer_investments.amount18_cer_chf_requested2"] = "Merchandising Material Requested Amount in CHF";
	$fields["cer_investments.amount19_cer_chf_requested2"] = "Transportation Requested Amount in CHF";
	$fields["cer_investments.total_amount_cer_chf_requested2"] = "Investment Total Requested Amount in CHF";
	
	
	$fields["cer_investments.amount1_cer_loc_requested2"] = "Construction Requested Amount in LOC";
	$fields["cer_investments.amount3_cer_loc_requested2"] = "Store fixturing Requested Amount in LOC";
	$fields["cer_investments.amount5_cer_loc_requested2"] = "Architectural costs Requested Amount in LOC";
	$fields["cer_investments.amount7_cer_loc_requested2"] = "Equipment Requested Amount in LOC";
	$fields["cer_investments.amount11_cer_loc_requested2"] = "Other Requested Amount in LOC";
	$fields["cer_investments.amount18_cer_loc_requested2"] = "Merchandising Material Requested Amount in LOC";
	$fields["cer_investments.amount19_cer_loc_requested2"] = "Transportation Requested Amount in LOC";
	$fields["cer_investments.total_amount_cer_loc_requested2"] = "Investment Total Requested Amount in LOC";

	$fields["cer_investments.total_amount_cer_chf_approved2"] = "Investment Total Amount KL-approved in CHF";
	$fields["posorderinvestments.total_amount_cms_chf2"] = "Investment Real Amounts in CHF";
	$fields["cer_investments.total_amount_cer_loc_approved2"] = "Investment Total Amount KL-approved in LOC";
	$fields["posorderinvestments.total_amount_cms_loc2"] = "Investment Real Amounts in LOC";

	$fields["cer_investments.delta_requested_real2"] = "Delta Investment Requested/Real Amounts";

	$fields["cer_revenues.total_revenue2"] = "Projected Sales in CHF";
	$fields["cer_revenues.total_revenue_loc2"] = "Projected Sales in LOC";
	$fields["cer_revenues.total_revenue_first_year2"] = "Projected Sales First Full Year in CHF";
	$fields["cer_revenues.total_revenue_first_year_loc2"] = "Projected Sales First Full Year in LOC";
	
	$attributes["posorders2.posorder_ordernumber"] = "content_by_function";
	$attributes["posorders2.posorder_product_line"] = "content_by_function";
	$attributes["posorders2.posorder_product_line_subclass"] = "content_by_function";
	$attributes["posorders2.posorder_postype"] = "content_by_function";
	$attributes["posorders2.posorder_subclass"] = "content_by_function";
	$attributes["posorders2.posorder_project_kind"] = "content_by_function";
	$attributes["posorders2.posorder_legal_type"] = "content_by_function";
	$attributes["projects.project_furniture_height_mm"] = "content_by_function";
	$attributes["project_costs.project_cost_sqms"] = "content_by_function";
	$attributes["projects.project_actual_opening_date"] = "content_by_function";

	
	$attributes["cer_investments.amount1_cer_chf_requested2"] = "content_by_function";
	$attributes["cer_investments.amount3_cer_chf_requested2"] = "content_by_function";
	$attributes["cer_investments.amount5_cer_chf_requested2"] = "content_by_function";
	$attributes["cer_investments.amount7_cer_chf_requested2"] = "content_by_function";
	$attributes["cer_investments.amount11_cer_chf_requested2"] = "content_by_function";
	$attributes["cer_investments.amount18_cer_chf_requested2"] = "content_by_function";
	$attributes["cer_investments.amount19_cer_chf_requested2"] = "content_by_function";
	$attributes["cer_investments.total_amount_cer_chf_requested2"] = "content_by_function";

	$attributes["cer_investments.amount1_cer_loc_requested2"] = "content_by_function";
	$attributes["cer_investments.amount3_cer_loc_requested2"] = "content_by_function";
	$attributes["cer_investments.amount5_cer_loc_requested2"] = "content_by_function";
	$attributes["cer_investments.amount7_cer_loc_requested2"] = "content_by_function";
	$attributes["cer_investments.amount11_cer_loc_requested2"] = "content_by_function";
	$attributes["cer_investments.amount18_cer_loc_requested2"] = "content_by_function";
	$attributes["cer_investments.amount19_cer_loc_requested2"] = "content_by_function";
	$attributes["cer_investments.total_amount_cer_loc_requested2"] = "content_by_function";

	$attributes["cer_investments.total_amount_cer_chf_approved2"] = "content_by_function";
	$attributes["posorderinvestments.total_amount_cms_chf2"] = "content_by_function";
	$attributes["cer_investments.total_amount_cer_loc_approved2"] = "content_by_function";
	$attributes["posorderinvestments.total_amount_cms_loc2"] = "content_by_function";

	$attributes["cer_investments.delta_requested_real2"] = "content_by_function";

	$attributes["cer_revenues.total_revenue2"] = "content_by_function";
	$attributes["cer_revenues.total_revenue_loc2"] = "content_by_function";
	$attributes["cer_revenues.total_revenue_first_year2"] = "content_by_function";
	$attributes["cer_revenues.total_revenue_first_year_loc2"] = "content_by_function";

	$datatypes["posorders2.posorder_ordernumber"] = "text";
	$datatypes["posorders2.posorder_product_line"] = "text";
	$datatypes["posorders2.posorder_product_line_subclass"] = "text";
	$datatypes["posorders2.posorder_postype"] = "text";
	$datatypes["posorders2.posorder_subclass"] = "text";
	$datatypes["posorders2.posorder_project_kind"] = "text";
	$datatypes["posorders2.posorder_legal_type"] = "text";
	$datatypes["projects.project_furniture_height_mm"] = "integer";
	$datatypes["project_costs.project_cost_sqms"] = "decimal2";
	$datatypes["projects.project_actual_opening_date"] = "date";

	
	$datatypes["cer_investments.amount1_cer_chf_requested2"] = "decimal2";
	$datatypes["cer_investments.amount3_cer_chf_requested2"] = "decimal2";
	$datatypes["cer_investments.amount5_cer_chf_requested2"] = "decimal2";
	$datatypes["cer_investments.amount7_cer_chf_requested2"] = "decimal2";
	$datatypes["cer_investments.amount11_cer_chf_requested2"] = "decimal2";
	$datatypes["cer_investments.amount18_cer_chf_requested2"] = "decimal2";
	$datatypes["cer_investments.amount19_cer_chf_requested2"] = "decimal2";
	$datatypes["cer_investments.total_amount_cer_chf_requested2"] = "decimal2";

	$datatypes["cer_investments.amount1_cer_loc_requested2"] = "decimal2";
	$datatypes["cer_investments.amount3_cer_loc_requested2"] = "decimal2";
	$datatypes["cer_investments.amount5_cer_loc_requested2"] = "decimal2";
	$datatypes["cer_investments.amount7_cer_loc_requested2"] = "decimal2";
	$datatypes["cer_investments.amount11_cer_loc_requested2"] = "decimal2";
	$datatypes["cer_investments.amount18_cer_loc_requested2"] = "decimal2";
	$datatypes["cer_investments.amount19_cer_loc_requested2"] = "decimal2";
	$datatypes["cer_investments.total_amount_cer_loc_requested2"] = "decimal2";

	$datatypes["cer_investments.total_amount_cer_chf_approved2"] = "decimal2";
	$datatypes["posorderinvestments.total_amount_cms_chf2"] = "decimal2";
	$datatypes["cer_investments.total_amount_cer_loc_approved2"] = "decimal2";
	$datatypes["posorderinvestments.total_amount_cms_loc2"] = "decimal2";

	$datatypes["cer_investments.delta_requested_real2"] = "percent";

	$datatypes["cer_revenues.total_revenue2"] = "decimal2";
	$datatypes["cer_revenues.total_revenue_loc2"] = "decimal2";
	$datatypes["cer_revenues.total_revenue_first_year2"] = "decimal2";
	$datatypes["cer_revenues.total_revenue_first_year_loc2"] = "decimal2";

	$content_by_function["posorders2.posorder_ordernumber"] = "get_project_data2(params)";
	$content_by_function["posorders2.posorder_product_line"] = "get_project_data2(params)";
	$content_by_function["posorders2.posorder_product_line_subclass"] = "get_project_data2(params)";
	$content_by_function["posorders2.posorder_postype"] = "get_project_data2(params)";
	$content_by_function["posorders2.posorder_subclass"] = "get_project_data2(params)";
	$content_by_function["posorders2.posorder_project_kind"] = "get_project_data2(params)";
	$content_by_function["posorders2.posorder_legal_type"] = "get_project_data2(params)";
	$content_by_function["projects.project_furniture_height_mm"] = "get_project_data2(params)";
	$content_by_function["project_costs.project_cost_sqms"] = "get_project_data2(params)";
	$content_by_function["projects.project_actual_opening_date"] = "get_project_data2(params)";

	
	
	$content_by_function["cer_investments.amount1_cer_chf_requested2"] = "get_project_data2(params)";
	$content_by_function["cer_investments.amount3_cer_chf_requested2"] ="get_project_data2(params)";
	$content_by_function["cer_investments.amount5_cer_chf_requested2"] = "get_project_data2(params)";
	$content_by_function["cer_investments.amount7_cer_chf_requested2"] = "get_project_data2(params)";
	$content_by_function["cer_investments.amount11_cer_chf_requested2"] = "get_project_data2(params)";
	$content_by_function["cer_investments.amount18_cer_chf_requested2"] = "get_project_data2(params)";
	$content_by_function["cer_investments.amount19_cer_chf_requested2"] = "get_project_data2(params)";
	$content_by_function["cer_investments.total_amount_cer_chf_requested2"] = "get_project_data2(params)";
	
	$content_by_function["cer_investments.amount1_cer_loc_requested2"] = "get_project_data2(params)";
	$content_by_function["cer_investments.amount3_cer_loc_requested2"] = "get_project_data2(params)";
	$content_by_function["cer_investments.amount5_cer_loc_requested2"] = "get_project_data2(params)";
	$content_by_function["cer_investments.amount7_cer_loc_requested2"] = "get_project_data2(params)";
	$content_by_function["cer_investments.amount11_cer_loc_requested2"] = "get_project_data2(params)";
	$content_by_function["cer_investments.amount18_cer_loc_requested2"] = "get_project_data2(params)";
	$content_by_function["cer_investments.amount19_cer_loc_requested2"] = "get_project_data2(params)";
	$content_by_function["cer_investments.total_amount_cer_loc_requested2"] = "get_project_data2(params)";

	$content_by_function["cer_investments.total_amount_cer_chf_approved2"] = "get_project_data2(params)";
	$content_by_function["posorderinvestments.total_amount_cms_chf2"] = "get_project_data2(params)";
	$content_by_function["cer_investments.total_amount_cer_loc_approved2"] = "get_project_data2(params)";
	$content_by_function["posorderinvestments.total_amount_cms_loc2"] = "get_project_data2(params)";

	$content_by_function["cer_investments.delta_requested_real2"] = "get_project_data2(params)";

	
	$content_by_function["cer_revenues.total_revenue2"] = "get_project_data2(params)";
	$content_by_function["cer_revenues.total_revenue_loc2"] = "get_project_data2(params)";
	$content_by_function["cer_revenues.total_revenue_first_year2"] = "get_project_data2(params)";
	$content_by_function["cer_revenues.total_revenue_first_year_loc2"] = "get_project_data2(params)";
	

	$content_by_function_params["posorders2.posorder_ordernumber"] = array('posaddress_id', 'posorder_ordernumber');
	$content_by_function_params["posorders2.posorder_product_line"] = array('posaddress_id', 'product_line_name');
	$content_by_function_params["posorders2.posorder_product_line_subclass"] = array('posaddress_id', 'productline_subclass_name');
	$content_by_function_params["posorders2.posorder_postype"] = array('posaddress_id', 'postype_name');
	$content_by_function_params["posorders2.posorder_subclass"] = array('posaddress_id', 'possubclass_name');
	$content_by_function_params["posorders2.posorder_project_kind"] = array('posaddress_id', 'projectkind_name');
	$content_by_function_params["posorders2.posorder_legal_type"] = array('posaddress_id', 'project_costtype_text');
	$content_by_function_params["projects.project_furniture_height_mm"] = array('posaddress_id', 'project_furniture_height_mm');
	$content_by_function_params["project_costs.project_cost_sqms"] = array('posaddress_id', 'project_cost_sqms');
	$content_by_function_params["projects.project_actual_opening_date"] = array('posaddress_id', 'project_actual_opening_date');
	$content_by_function_params["projects.project_shop_closingdate"] = array('posaddress_id', 'project_shop_closingdate');

	
	$content_by_function_params["cer_investments.amount1_cer_chf_requested2"] = array('posaddress_id', 'total_amount_cer_chf_requested2', 1);
	$content_by_function_params["cer_investments.amount3_cer_chf_requested2"] = array('posaddress_id', 'total_amount_cer_chf_requested2', 3);
	$content_by_function_params["cer_investments.amount5_cer_chf_requested2"] = array('posaddress_id', 'total_amount_cer_chf_requested2', 5);
	$content_by_function_params["cer_investments.amount7_cer_chf_requested2"] = array('posaddress_id', 'total_amount_cer_chf_requested2', 7);
	$content_by_function_params["cer_investments.amount11_cer_chf_requested2"] = array('posaddress_id', 'total_amount_cer_chf_requested2', 11);
	$content_by_function_params["cer_investments.amount18_cer_chf_requested2"] = array('posaddress_id', 'total_amount_cer_chf_requested2', 18);
	$content_by_function_params["cer_investments.amount19_cer_chf_requested2"] = array('posaddress_id', 'total_amount_cer_chf_requested2', 19);
	$content_by_function_params["cer_investments.total_amount_cer_chf_requested2"] = array('posaddress_id', 'total_amount_cer_chf_requested2');

	$content_by_function_params["cer_investments.amount1_cer_loc_requested2"] = array('posaddress_id', 'total_amount_cer_loc_requested2', 1);
	$content_by_function_params["cer_investments.amount3_cer_loc_requested2"] = array('posaddress_id', 'total_amount_cer_loc_requested2', 3);
	$content_by_function_params["cer_investments.amount5_cer_loc_requested2"] = array('posaddress_id', 'total_amount_cer_loc_requested2', 5);
	$content_by_function_params["cer_investments.amount7_cer_loc_requested2"] = array('posaddress_id', 'total_amount_cer_loc_requested2', 7);
	$content_by_function_params["cer_investments.amount11_cer_loc_requested2"] = array('posaddress_id', 'total_amount_cer_loc_requested2', 11);
	$content_by_function_params["cer_investments.amount18_cer_loc_requested2"] = array('posaddress_id', 'total_amount_cer_loc_requested2', 18);
	$content_by_function_params["cer_investments.amount19_cer_loc_requested2"] = array('posaddress_id', 'total_amount_cer_loc_requested2', 19);
	$content_by_function_params["cer_investments.total_amount_cer_loc_requested2"] = array('posaddress_id', 'total_amount_cer_loc_requested2');
	
	
	$content_by_function_params["cer_investments.total_amount_cer_chf_approved2"] = array('posaddress_id', 'total_amount_cer_chf_approved2');
	$content_by_function_params["posorderinvestments.total_amount_cms_chf2"] = array('posaddress_id', 'total_amount_cms_chf2');
	$content_by_function_params["cer_investments.total_amount_cer_loc_approved2"] = array('posaddress_id', 'total_amount_cer_loc_approved2');
	$content_by_function_params["posorderinvestments.total_amount_cms_loc2"] = array('posaddress_id', 'total_amount_cms_loc2');

	$content_by_function_params["cer_investments.delta_requested_real2"] = array('posaddress_id', 'delta_requested_real2');


	$content_by_function_params["cer_revenues.total_revenue2"] = array('posaddress_id', 'total_revenue2');
	$content_by_function_params["cer_revenues.total_revenue_loc2"] = array('posaddress_id', 'total_revenue_loc2');
	$content_by_function_params["cer_revenues.total_revenue_first_year2"] = array('posaddress_id', 'total_revenue_first_year2');
	$content_by_function_params["cer_revenues.total_revenue_first_year_loc2"] = array('posaddress_id', 'total_revenue_first_year_loc2');


	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;

	return $db_info;
}



/********************************************************************
    get DB Info for investments
*********************************************************************/
function query_intangible_fields()
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$content_by_function = array();
	$content_by_function_params = array();

	$fields["posinvestments.posinvestment_investment_type"] = "Intangible Name";
	$fields["posinvestments.posinvestment_amount_cer"] = "Intangible Amount CER in CHF";
	$fields["posinvestments.posinvestment_amount_cms"] = "Intangible Amount CMS in CHF";
	$fields["posinvestments.posinvestment_amount_cer_loc"] = "Intangible Amount CER in LOC";
	$fields["posinvestments.posinvestment_amount_cms_loc"] = "Intangible Amount CMS in LOC";
	$fields["posinvestments.total_amount_cer_chf"] = "Intangible Total Amount CER in CHF";
	$fields["posinvestments.total_amount_cms_chf"] = "Intangible Total Amount CMS in CHF";
	$fields["posinvestments.total_amount_cer_loc"] = "Intangible Total Amount CER in LOC";
	$fields["posinvestments.total_amount_cms_loc"] = "Intangible Total Amount CMS in LOC";

	$attributes["posinvestments.posinvestment_investment_type"] = "intangibletypes.posinvestment_type_name";
	$attributes["posinvestments.posinvestment_amount_cer"] = "posinvestments.posinvestment_amount_cer";
	$attributes["posinvestments.posinvestment_amount_cms"] = "posinvestments.posinvestment_amount_cms";
	$attributes["posinvestments.posinvestment_amount_cer_loc"] = "posinvestments.posinvestment_amount_cer_loc";
	$attributes["posinvestments.posinvestment_amount_cms_loc"] = "posinvestments.posinvestment_amount_cms_loc";
	$attributes["posinvestments.total_amount_cer_chf"] = "calculated_content";
	$attributes["posinvestments.total_amount_cms_chf"] = "calculated_content";
	$attributes["posinvestments.total_amount_cer_loc"] = "calculated_content";
	$attributes["posinvestments.total_amount_cms_loc"] = "calculated_content";
	
	$datatypes["posinvestments.posinvestment_investment_type"] = "text";
	$datatypes["posinvestments.posinvestment_amount_cer"] = "decimal2";
	$datatypes["posinvestments.posinvestment_amount_cms"] = "decimal2";
	$datatypes["posinvestments.posinvestment_amount_cer_loc"] = "decimal2";
	$datatypes["posinvestments.posinvestment_amount_cms_loc"] = "decimal2";
	$datatypes["posinvestments.total_amount_cer_chf"] = "decimal2";
	$datatypes["posinvestments.total_amount_cms_chf"] = "decimal2";
	$datatypes["posinvestments.total_amount_cer_loc"] = "decimal2";
	$datatypes["posinvestments.total_amount_cms_loc"] = "decimal2";

	$calculated_content["posinvestments.total_amount_cer_chf"] = "select sum(posinvestment_amount_cer) as total from posinvestments where posinvestment_posaddress = ";
	$calculated_content_key["posinvestments.total_amount_cer_chf"] = "posaddress_id";
	$calculated_content_field["posinvestments.total_amount_cer_chf"] = "posinvestments.total_amount_cer_chf";

	$calculated_content["posinvestments.total_amount_cms_chf"] = "select sum(posinvestment_amount_cms) as total from posinvestments where posinvestment_posaddress = ";
	$calculated_content_key["posinvestments.total_amount_cms_chf"] = "posaddress_id";
	$calculated_content_field["posinvestments.total_amount_cms_chf"] = "posinvestments.total_amount_cms_chf";

	$calculated_content["posinvestments.total_amount_cer_loc"] = "select sum(posinvestment_amount_cer_loc) as total from posinvestments where posinvestment_posaddress = ";
	$calculated_content_key["posinvestments.total_amount_cer_loc"] = "posaddress_id";
	$calculated_content_field["posinvestments.total_amount_cer_loc"] = "posinvestments.total_amount_cer_loc";

	$calculated_content["posinvestments.total_amount_cms_loc"] = "select sum(posinvestment_amount_cms_loc) as total from posinvestments where posinvestment_posaddress = ";
	$calculated_content_key["posinvestments.total_amount_cms_loc"] = "posaddress_id";
	$calculated_content_field["posinvestments.total_amount_cms_loc"] = "posinvestments.total_amount_cms_loc";


	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;

	return $db_info;
}

/********************************************************************
    get DB Info for distribution channel fields
*********************************************************************/
function query_distribution_channel_fields()
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$calculated_content_field = array();
	$content_by_function = array();
	$content_by_function_params = array();

	$fields["mps_distchannels.mps_distchannel_group"] = "Distribution Channel Group";
	$fields["mps_distchannels.mps_distchannel_code"] = "Distribution Channel Code";
	$fields["mps_distchannels.mps_distchannel_name"] = "Distribution Channel Name";

	$attributes["mps_distchannels.mps_distchannel_group"] = "mps_distchannels.mps_distchannel_group";
	$attributes["mps_distchannels.mps_distchannel_code"] = "mps_distchannels.mps_distchannel_code";
	$attributes["mps_distchannels.mps_distchannel_name"] = "mps_distchannels.mps_distchannel_name";


	$datatypes["mps_distchannels.mps_distchannel_group"] = "text";
	$datatypes["mps_distchannels.mps_distchannel_code"] = "text";
	$datatypes["mps_distchannels.mps_distchannel_name"] = "text";


	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;

	return $db_info;
}


/********************************************************************
    get DB Info for statistics fields
*********************************************************************/
function query_statistics_fields()
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$calculated_content_field = array();
	$content_by_function = array();
	$content_by_function_params = array();

	
	$fields["total_corporate_stores"] = "Total Corporate Stores";
	$fields["total_franchisee_stores"] = "Total Franchisee Stores";
	$fields["total_corporate_sis"] = "Total Corporate SIS";
	$fields["total_other_sis"] = "Total Other SIS";
	$fields["total_corporate_kiosks"] = "Total Corporate Kiosks";
	$fields["total_franchisee_kiosks"] = "Total Franchisee Kiosks";
	$fields["total_independent_retailer"] = "Total Independent Retailes";


	$attributes["total_corporate_stores"] = "get_from_value_array";
	$attributes["total_franchisee_stores"] = "get_from_value_array";
	$attributes["total_corporate_sis"] = "get_from_value_array";
	$attributes["total_other_sis"] = "get_from_value_array";
	$attributes["total_corporate_kiosks"] = "get_from_value_array";
	$attributes["total_franchisee_kiosks"] = "get_from_value_array";
	$attributes["total_independent_retailer"] = "get_from_value_array";


	$datatypes["total_corporate_stores"] = "integer";
	$datatypes["total_franchisee_stores"] = "integer";
	$datatypes["total_corporate_sis"] = "integer";
	$datatypes["total_other_sis"] = "integer";
	$datatypes["total_corporate_kiosks"] = "integer";
	$datatypes["total_franchisee_kiosks"] = "integer";
	$datatypes["total_independent_retailer"] = "integer";

	$content_by_function_params["total_corporate_stores"] = array("poslocation_count", 1, 1);
	$content_by_function_params["total_franchisee_stores"] = array("poslocation_count", 2, 1);
	$content_by_function_params["total_corporate_sis"] = array("poslocation_count", 1, 2);
	$content_by_function_params["total_other_sis"] = array("poslocation_count", 6, 2);
	$content_by_function_params["total_corporate_kiosks"] = array("poslocation_count", 1, 3);
	$content_by_function_params["total_franchisee_kiosks"] = array("poslocation_count", 2, 3);
	$content_by_function_params["total_independent_retailer"] = array("poslocation_count", 6, 4);


	
		

	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;

	return $db_info;
}

/********************************************************************
    get query name
*********************************************************************/
function get_query_name($id)
{
    $posquery = array();
	
	$sql = "select * from posqueries " .
		   "where posquery_id = " . $id;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$posquery["name"] = $row["posquery_name"];
	}
    return $posquery;
}


/********************************************************************
    decode query filter
*********************************************************************/
function decode_field_array($field_order)
{
	$fields = array();
	
	if($field_order)
	{
		$code = str_replace("selected_field_order[]=", "", $field_order);
		$fields = explode  ( "&"  , $code);
	}
	return $fields;
}

/********************************************************************
    encode query filter
*********************************************************************/
function encode_field_array($field_array)
{
	$string = "";
	
	foreach($field_array as $key=>$fieldname)
	{
		$string .= "selected_field_order[]=";
		$string .= $fieldname;
		$string .= "&";

	}
	$string = substr($string, 0, strlen($string)-1);
	
	return $string;
}

/********************************************************************
   get fields from projects
*********************************************************************/
function get_project_data($posorder_id = 0, $field_name = '', $investment_type = 0)
{
	$investment_filter = "";
	$revenue_filter = "";
	$is_revenue_field = "";

	//get project
	$project_id = 0;
	$order_id = 0;
	$sql = "select project_id, posorder_order " . 
		   "from posorders " .
		   "left join orders on order_id = posorder_order " . 
		   "left join projects on project_order = order_id " . 
		   " where posorder_id = " . dbquote($posorder_id);

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$project_id = $row["project_id"];
		$order_id = $row["posorder_order"];
	}

	if($field_name == 'total_amount_cer_chf_requested')
	{
		$sql  = "select order_client_exchange_rate*sum(cer_investment_amount_cer_loc)/currency_factor + order_client_exchange_rate*sum(ifnull(cer_investment_amount_additional_cer_loc, 0))/currency_factor as " . $field_name;

		if($investment_type > 0)
		{
			$investment_filter = " and cer_investment_type in(" . $investment_type . ")";
		}
		else
		{
			$investment_filter = " and cer_investment_type in(1, 3, 5, 7, 11, 18, 19)";
		}
	}
	elseif($field_name == 'total_amount_cer_loc_requested')
	{
		$sql  = "select sum(cer_investment_amount_cer_loc) + sum(ifnull(cer_investment_amount_additional_cer_loc, 0)) as " . $field_name;
		if($investment_type > 0)
		{
			$investment_filter = " and cer_investment_type in(" . $investment_type . ")";
		}
		else
		{
			$investment_filter = " and cer_investment_type in(1, 3, 5, 7, 11, 18, 19)";
		}
		
	}
	elseif($field_name == 'total_amount_cer_chf_approved')
	{
		$sql  = "select order_client_exchange_rate*sum(cer_investment_amount_cer_loc_approved)/currency_factor + order_client_exchange_rate*sum(ifnull(cer_investment_amount_additional_cer_loc_approved, 0))/currency_factor as " . $field_name;

		if($investment_type > 0)
		{
			$investment_filter = " and cer_investment_type in(" . $investment_type . ")";
		}
		else
		{
			$investment_filter = " and cer_investment_type in(1, 3, 5, 7, 11, 18, 19)";
		}
	}
	elseif($field_name == 'total_amount_cms_chf')
	{
		$sql  = "select sum(order_client_exchange_rate*costsheet_real_amount/currency_factor) as " . $field_name;
	}
	elseif($field_name == 'total_amount_cer_loc_approved')
	{
		$sql  = "select sum(cer_investment_amount_cer_loc_approved) + sum(ifnull(cer_investment_amount_additional_cer_loc_approved, 0)) as " . $field_name;
		if($investment_type > 0)
		{
			$investment_filter = " and cer_investment_type in(" . $investment_type . ")";
		}
		else
		{
			$investment_filter = " and cer_investment_type in(1, 3, 5, 7, 11, 18, 19)";
		}
	}
	elseif($field_name == 'total_amount_cms_loc')
	{
		$sql  = "select sum(costsheet_real_amount) as " . $field_name;
	}
	elseif($field_name == 'total_revenue')
	{
		$is_revenue_field = 1;
		$sql  = "select cer_basicdata_exchangerate*
			(sum(cer_revenue_watches) 
			+ sum(ifnull(cer_revenue_jewellery, 0))
			+ sum(ifnull(cer_revenue_accessories + cer_revenue_customer_service, 0))
			+ sum(ifnull(cer_revenue_customer_service, 0)))/cer_basicdata_factor as " . $field_name;
	}
	elseif($field_name == 'total_revenue_loc')
	{
		$is_revenue_field = 1;
		$sql  = "select 
			sum(cer_revenue_watches) 
			+ sum(ifnull(cer_revenue_jewellery, 0))
			+ sum(ifnull(cer_revenue_accessories + cer_revenue_customer_service, 0))
			+ sum(ifnull(cer_revenue_customer_service, 0)) as " . $field_name;
	}
	elseif($field_name == 'total_revenue_first_year')
	{
		//get first full year
		$sql = "select cer_basicdata_firstyear, cer_basicdata_firstmonth ";
		$sql .= " from cer_basicdata ";
		$sql .= " where cer_basicdata_project = " . dbquote($project_id) ;
		$sql .= " and cer_basicdata_version = 0 ";
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			if($row['cer_basicdata_firstmonth'] == 1) {
				$revenue_filter = ' and cer_revenue_year = ' . dbquote($row['cer_basicdata_firstyear']);
			}
			else {
				$revenue_filter = ' and cer_revenue_year = ' . dbquote(1+$row['cer_basicdata_firstyear']);
			}
		}
		
		$is_revenue_field = 1;

		$sql  = "select cer_basicdata_exchangerate*
			(sum(cer_revenue_watches) 
			+ sum(ifnull(cer_revenue_jewellery, 0))
			+ sum(ifnull(cer_revenue_accessories + cer_revenue_customer_service, 0))
			+ sum(ifnull(cer_revenue_customer_service, 0)))/cer_basicdata_factor as " . $field_name;
	}
	elseif($field_name == 'total_revenue_first_year_loc')
	{
		
		//get first full year
		$sql = "select cer_basicdata_firstyear, cer_basicdata_firstmonth ";
		$sql .= " from cer_basicdata ";
		$sql .= " where cer_basicdata_project = " . dbquote($project_id) ;
		$sql .= " and cer_basicdata_version = 0 ";
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			if($row['cer_basicdata_firstmonth'] == 1) {
				$revenue_filter = ' and cer_revenue_year = ' . dbquote($row['cer_basicdata_firstyear']);
			}
			else {
				$revenue_filter = ' and cer_revenue_year = ' . dbquote(1+$row['cer_basicdata_firstyear']);
			}
		}

		$is_revenue_field = 1;

		$sql  = "select 
			sum(cer_revenue_watches) 
			+ sum(ifnull(cer_revenue_jewellery, 0))
			+ sum(ifnull(cer_revenue_accessories + cer_revenue_customer_service, 0))
			+ sum(ifnull(cer_revenue_customer_service, 0)) as " . $field_name;
	}
	elseif($field_name == '_tmp_delta_requested_real') {

		$sql  = "select ((order_client_exchange_rate*sum(cer_investment_amount_cer_loc)/currency_factor + order_client_exchange_rate*sum(ifnull(cer_investment_amount_additional_cer_loc, 0))/currency_factor) - (sum(order_client_exchange_rate*costsheet_real_amount/currency_factor)))/((order_client_exchange_rate*sum(cer_investment_amount_cer_loc)/currency_factor + order_client_exchange_rate*sum(ifnull(cer_investment_amount_additional_cer_loc, 0))/currency_factor)) as " . $field_name;
		$investment_filter = " and cer_investment_type in(1, 3, 5, 7, 11, 18, 19)";

	}
	elseif($field_name == 'delta_requested_real') {
		
		$sql  = "select ((
			SELECT
				order_client_exchange_rate*sum(costsheet_real_amount)/currency_factor 
				from costsheets
				left join projects on project_id = costsheet_project_id
				left join orders on order_id = project_order
				left join currencies on currency_id = order_client_currency
			WHERE
				costsheet_project_id = " . dbquote($project_id) . " 
		) - (cer_basicdata_exchangerate*(sum(cer_investment_amount_cer_loc) + sum(ifnull(cer_investment_amount_additional_cer_loc, 0))))/cer_basicdata_factor)/((cer_basicdata_exchangerate*(sum(cer_investment_amount_cer_loc) + sum(ifnull(cer_investment_amount_additional_cer_loc, 0))))/cer_basicdata_factor) as " . $field_name;
		$investment_filter = " and cer_investment_type in(1, 3, 5, 7, 11, 18, 19)";

	}
	else
	{
		$sql  = "select " . $field_name;
	}
	

	
	if($investment_filter)
	{
		$sql .= " from cer_investments ";
		$sql .= " left join projects on project_id = cer_investment_project";
		$sql .= " left join cer_basicdata on project_id = cer_basicdata_project";
		$sql .= " left join orders on order_id = project_order ";
		$sql .= "left join currencies on currency_id = order_client_currency ";
		$sql .= " where cer_investment_project = " . dbquote($project_id) ;
		$sql .= " and cer_basicdata_version = 0 and cer_investment_cer_version = 0 ";
		$sql .= $investment_filter;

	}
	elseif($is_revenue_field)
	{
		$sql .= " from cer_revenues ";
		$sql .= " left join cer_basicdata on cer_basicdata_project = cer_revenue_project ";
		$sql .= " where cer_revenue_project = " . dbquote($project_id) ;
		$sql .= " and cer_basicdata_version = 0 and cer_revenue_cer_version = 0 ";
		$sql .= $revenue_filter;

	}
	else
	{
		$sql .= " from posorders ";
		$sql .= "left join postypes as posorderprojecttypes on posorderprojecttypes.postype_id = posorder_postype ";
		$sql .= "left join product_lines as posorderproductlines on posorderproductlines.product_line_id = posorder_product_line ";
		$sql .= "left join productline_subclasses as posorderproductlinesubclasses on posorderproductlinesubclasses.productline_subclass_id = posorder_product_line_subclass ";
		$sql .= "left join possubclasses as posordersubclasses on posordersubclasses.possubclass_id = posorder_subclass ";
		$sql .= "left join project_costtypes as posordercosttypes on posordercosttypes.project_costtype_id = posorder_legal_type ";
		$sql .= "left join projectkinds as posorderprojectkinds on posorderprojectkinds.projectkind_id = posorder_project_kind ";
		$sql .= "left join projects on project_order = posorder_order ";
		$sql .= "left join orders on order_id = project_order ";
		$sql .= "left join project_costs on project_cost_order = order_id ";
		$sql .= "left join costsheets on costsheet_project_id = project_id ";
		$sql .= "left join currencies on currency_id = order_client_currency ";
		$sql .= " where posorder_id = " . dbquote($posorder_id);
	}


	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{	
		return $row[$field_name];
	}
	
	return "";
}


/********************************************************************
   get fields from projects the latest project of a POS
*********************************************************************/

function get_project_data2($posaddress_id = 0, $field_name = '', $investment_type = 0)
{
	$investment_filter = "";
	$revenue_filter = "";
	$is_revenue_field = "";

	//get latest project
	$sql = "select posorder_id, posorder_order, project_id " . 
		   "from posorders " .
		   "left join orders on order_id = posorder_order " . 
		   "left join projects on project_order = order_id " . 
		   " where posorder_posaddress = " . dbquote($posaddress_id) . 
		   " and posorder_type = 1 " . 
		   " and (order_actual_order_state_code is null or order_actual_order_state_code <= '890') " . 
		   " and project_state in (1,4) " . 
		   " order by posorder_ordernumber DESC ";



	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$project_id = $row['project_id'];
		$order_id = $row["posorder_id"];
		
		if($field_name == 'total_amount_cer_chf_requested2')
		{
			$sql  = "select order_client_exchange_rate*sum(cer_investment_amount_cer_loc)/currency_factor + order_client_exchange_rate*sum(ifnull(cer_investment_amount_additional_cer_loc, 0))/currency_factor as " . $field_name;

			if($investment_type > 0)
			{
				$investment_filter = " and cer_investment_type in(" . $investment_type . ")";
			}
			else
			{
				$investment_filter = " and cer_investment_type in(1, 3, 5, 7, 11, 18, 19)";
			}
		}
		elseif($field_name == 'total_amount_cer_loc_requested2')
		{
			$sql  = "select sum(cer_investment_amount_cer_loc) + sum(ifnull(cer_investment_amount_additional_cer_loc, 0)) as " . $field_name;
			if($investment_type > 0)
			{
				$investment_filter = " and cer_investment_type in(" . $investment_type . ")";
			}
			else
			{
				$investment_filter = " and cer_investment_type in(1, 3, 5, 7, 11, 18, 19)";
			}
		}
		elseif($field_name == 'total_amount_cer_chf_approved2')
		{
			$sql  = "select order_client_exchange_rate*sum(cer_investment_amount_cer_loc_approved)/currency_factor + order_client_exchange_rate*sum(ifnull(cer_investment_amount_additional_cer_loc_approved, 0))/currency_factor as " . $field_name;

			if($investment_type > 0)
			{
				$investment_filter = " and cer_investment_type in(" . $investment_type . ")";
			}
			else
			{
				$investment_filter = " and cer_investment_type in(1, 3, 5, 7, 11, 18, 19)";
			}
		}
		elseif($field_name == 'total_amount_cms_chf2')
		{
			$sql  = "select sum(order_client_exchange_rate*costsheet_real_amount/currency_factor) as " . $field_name;
		}
		elseif($field_name == 'total_amount_cer_loc_approved2')
		{
			$sql  = "select sum(cer_investment_amount_cer_loc_approved) + sum(ifnull(cer_investment_amount_additional_cer_loc_approved, 0)) as " . $field_name;
			if($investment_type > 0)
			{
				$investment_filter = " and cer_investment_type in(" . $investment_type . ")";
			}
			else
			{
				$investment_filter = " and cer_investment_type in(1, 3, 5, 7, 11, 18, 19)";
			}
		}
		elseif($field_name == 'total_amount_cms_loc2')
		{
			$sql  = "select sum(costsheet_real_amount) as " . $field_name;
			
		}
		elseif($field_name == 'total_revenue2')
		{
			$is_revenue_field = 1;
			$sql  = "select cer_basicdata_exchangerate*
				(sum(cer_revenue_watches) 
				+ sum(ifnull(cer_revenue_jewellery, 0))
				+ sum(ifnull(cer_revenue_accessories + cer_revenue_customer_service, 0))
				+ sum(ifnull(cer_revenue_customer_service, 0)))/cer_basicdata_factor as " . $field_name;
		}
		elseif($field_name == 'total_revenue_loc2')
		{
			$is_revenue_field = 1;
			$sql  = "select 
				sum(cer_revenue_watches) 
				+ sum(ifnull(cer_revenue_jewellery, 0))
				+ sum(ifnull(cer_revenue_accessories + cer_revenue_customer_service, 0))
				+ sum(ifnull(cer_revenue_customer_service, 0)) as " . $field_name;
		}
		elseif($field_name == 'total_revenue_first_year2')
		{
			//get first full year
			$sql = "select cer_basicdata_firstyear, cer_basicdata_firstmonth ";
			$sql .= " from cer_basicdata ";
			$sql .= " where cer_basicdata_project = " . dbquote($project_id) ;
			$sql .= " and cer_basicdata_version = 0 ";
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				if($row['cer_basicdata_firstmonth'] == 1) {
					$revenue_filter = ' and cer_revenue_year = ' . dbquote($row['cer_basicdata_firstyear']);
				}
				else {
					$revenue_filter = ' and cer_revenue_year = ' . dbquote(1+$row['cer_basicdata_firstyear']);
				}
			}
			
			$is_revenue_field = 1;

			$sql  = "select cer_basicdata_exchangerate*
				(sum(cer_revenue_watches) 
				+ sum(ifnull(cer_revenue_jewellery, 0))
				+ sum(ifnull(cer_revenue_accessories + cer_revenue_customer_service, 0))
				+ sum(ifnull(cer_revenue_customer_service, 0)))/cer_basicdata_factor as " . $field_name;
		}
		elseif($field_name == 'total_revenue_first_year_loc2')
		{
			
			//get first full year
			$sql = "select cer_basicdata_firstyear, cer_basicdata_firstmonth ";
			$sql .= " from cer_basicdata ";
			$sql .= " where cer_basicdata_project = " . dbquote($project_id) ;
			$sql .= " and cer_basicdata_version = 0 ";
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				if($row['cer_basicdata_firstmonth'] == 1) {
					$revenue_filter = ' and cer_revenue_year = ' . dbquote($row['cer_basicdata_firstyear']);
				}
				else {
					$revenue_filter = ' and cer_revenue_year = ' . dbquote(1+$row['cer_basicdata_firstyear']);
				}
			}

			$is_revenue_field = 1;

			$sql  = "select 
				sum(cer_revenue_watches) 
				+ sum(ifnull(cer_revenue_jewellery, 0))
				+ sum(ifnull(cer_revenue_accessories + cer_revenue_customer_service, 0))
				+ sum(ifnull(cer_revenue_customer_service, 0)) as " . $field_name;
		}
		elseif($field_name == 'delta_requested_real2') {
			$sql  = "select ((
				SELECT
					order_client_exchange_rate*sum(costsheet_real_amount)/currency_factor 
					from costsheets
					left join projects on project_id = costsheet_project_id
					left join orders on order_id = project_order
					left join currencies on currency_id = order_client_currency
				WHERE
					costsheet_project_id = " . dbquote($project_id) . " 
			) - (cer_basicdata_exchangerate*(sum(cer_investment_amount_cer_loc) + sum(ifnull(cer_investment_amount_additional_cer_loc, 0))))/cer_basicdata_factor)/((cer_basicdata_exchangerate*(sum(cer_investment_amount_cer_loc) + sum(ifnull(cer_investment_amount_additional_cer_loc, 0))))/cer_basicdata_factor) as " . $field_name;
			$investment_filter = " and cer_investment_type in(1, 3, 5, 7, 11, 18, 19)";

		}
		else
		{
			$sql  = "select " . $field_name;
		}

		if($investment_filter)
		{
			$sql .= " from cer_investments ";
			$sql .= " left join projects on project_id = cer_investment_project";
			$sql .= " left join cer_basicdata on project_id = cer_basicdata_project";
			$sql .= " left join orders on order_id = project_order ";
			$sql .= "left join currencies on currency_id = order_client_currency ";

			$sql .= " where cer_investment_project = " . dbquote($project_id) ;
			$sql .= " and cer_basicdata_version = 0 and cer_investment_cer_version = 0 ";
			$sql .= $investment_filter;

		}
		elseif($is_revenue_field)
		{
			$sql .= " from cer_revenues ";
			$sql .= " left join cer_basicdata on cer_basicdata_project = cer_revenue_project ";
			$sql .= " where cer_revenue_project = " . dbquote($project_id) ;
			$sql .= " and cer_basicdata_version = 0 and cer_revenue_cer_version = 0 ";
			$sql .= $revenue_filter;

		}
		else
		{
			$sql .= " from posorders ";
			$sql .= "left join postypes as posorderprojecttypes on posorderprojecttypes.postype_id = posorder_postype ";
			$sql .= "left join product_lines as posorderproductlines on posorderproductlines.product_line_id = posorder_product_line ";
			$sql .= "left join productline_subclasses as posorderproductlinesubclasses on posorderproductlinesubclasses.productline_subclass_id = posorder_product_line_subclass ";
			$sql .= "left join possubclasses as posordersubclasses on posordersubclasses.possubclass_id = posorder_subclass ";
			$sql .= "left join project_costtypes as posordercosttypes on posordercosttypes.project_costtype_id = posorder_legal_type ";
			$sql .= "left join projectkinds as posorderprojectkinds on posorderprojectkinds.projectkind_id = posorder_project_kind ";
			$sql .= "left join projects on project_order = posorder_order ";
			$sql .= "left join orders on order_id = project_order ";
			$sql .= "left join project_costs on project_cost_order = order_id ";
			$sql .= "left join costsheets on costsheet_project_id = project_id ";
			$sql .= "left join currencies on currency_id = order_client_currency ";
			$sql .= " where posorder_id = " . dbquote($order_id);
		}



		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			return $row[$field_name];
		}
	}

	
	return "";
}


/********************************************************************
   get the opening hoours of a POS
*********************************************************************/

function get_pos_opening_hours($posaddress_id = 0)
{
	$info = "";
	$weekdays = array();

	$sql = "select weekday_name,  " . 
		   " ofrom.openinghr_daytime as fdt, ofrom.openinghr_time_24 as foh,  " . 
		   " oto.openinghr_daytime as odt, oto.openinghr_time_24 as ooh  " .
		   " from posopeninghrs " . 
		   " left join weekdays on weekday_id = posopeninghr_weekday_id " . 
		   " left join openinghrs as ofrom on ofrom.openinghr_id =  posopeninghr_from_openinghr_id " . 
		   " left join openinghrs as oto on oto.openinghr_id =  posopeninghr_to_openinghr_id " . 
		   " where posopeninghr_posaddress_id = " . dbquote($posaddress_id);

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$weekdays[] = $row["weekday_name"] . ":" . $row["foh"] . " - " . $row["ooh"];
	}

	if(count($weekdays) > 0)
	{
		$info = implode("\n", $weekdays);
	}
	
	return $info;
}

/********************************************************************
   get the closing hours of a POS
*********************************************************************/

function get_pos_closing_hours($posaddress_id = 0)
{
	$info = "";

	$sql = "select posclosinghr_text from posclosinghrs " . 
		   " where posclosinghr_posaddress_id = " . dbquote($posaddress_id);

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$info = $row["posclosinghr_text"];
	}
	
	return $info;
}


/********************************************************************
   add empty column to the XLS
*********************************************************************/

function get_empty_column($posaddress_id = 0)
{
	return "";
}




?>