<?php
if (preg_match("/retailnet.swatch/", $_SERVER["HTTP_HOST"]))
{

    $db = mysql_pconnect("192.168.63.36", "retailnet", "B7aeWuv:r7pquNVR");
    mysql_select_db("db_retailnet", $db);

}
else
{
    $db = mysql_pconnect("localhost", "root", "");
    mysql_select_db("retail", $db);
}

// Get parameters from URL
//$center_lat = mysql_real_escape_string($_GET["lat"]);
//$center_lng = mysql_real_escape_string($_GET["lng"]);
$countryId = (isset($_GET["countryId"])) ? (int)($_GET["countryId"]) : 0;

// Start XML file, create parent node
$dom = new DOMDocument("1.0");
$node = $dom->createElement("markers");
$parnode = $dom->appendChild($node);

if($countryId === 0)
{
	//Select the rows in the markers table worldwide
	$query =	"SELECT country_name, posaddress_id, posaddress_name, posaddress_address, posaddress_zip, posaddress_place, posaddress_phone, 
				posaddress_fax, posaddress_email, posaddress_google_lat, posaddress_google_long FROM countries, posaddresses 
				WHERE (posaddress_store_closingdate IS NULL or posaddress_store_closingdate = '0000-00-00') AND posaddress_google_lat != '' AND posaddress_google_long != '' AND 
				countries.country_id = posaddress_country ORDER BY country_name, posaddress_name ASC";
}
else
{
		//Select the rows in the markers table from the selected country  
		$query =	"SELECT country_name, posaddress_id, posaddress_name, posaddress_address, posaddress_zip, posaddress_place, posaddress_phone, 
				posaddress_fax, posaddress_email, posaddress_google_lat, posaddress_google_long FROM countries, posaddresses 
				WHERE (posaddress_store_closingdate IS NULL or posaddress_store_closingdate = '0000-00-00') AND posaddress_google_lat != '' AND posaddress_google_long != '' AND 
				countries.country_id =".$countryId." AND countries.country_id = posaddress_country ORDER BY posaddress_name ASC";
}

$result = mysql_query($query);
if (!$result) {
  die('Invalid query: ' . mysql_error());
}

header("Content-type: text/xml;charset=utf-8");

// Iterate through the rows, printing XML nodes for each
while ($row = @mysql_fetch_assoc($result)){
  	// ADD TO XML DOCUMENT NODE
  	$node = $dom->createElement("marker");
  	$newnode = $parnode->appendChild($node);	
	$newnode->setAttribute("pos_id", $row['posaddress_id']);
	$newnode->setAttribute("name", utf8_encode($row['posaddress_name']));
	$newnode->setAttribute("address", utf8_encode($row['posaddress_address']));
	$newnode->setAttribute("zip", utf8_encode($row['posaddress_zip']));
	$newnode->setAttribute("place", utf8_encode($row['posaddress_place']));
	$newnode->setAttribute("phone", utf8_encode(trim($row['posaddress_phone'], "\",'")));
	$newnode->setAttribute("fax", utf8_encode(trim($row['posaddress_fax'], "\",'")));
	$newnode->setAttribute("email", utf8_encode(trim($row['posaddress_email'], "\",'")));
	$newnode->setAttribute("lat", $row['posaddress_google_lat']);
	$newnode->setAttribute("lng", $row['posaddress_google_long']);
	$newnode->setAttribute("country", utf8_encode($row['country_name']));
}

// End XML file
echo $dom->saveXML();
?>