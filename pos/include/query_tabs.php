<?php

$showquery_link = "javascript:popup('query_generator_xls.php?query_id=" .  param("query_id") . "',800,600);";

$page->add_tab("q_name", "Name", "query_generator_query.php?query_id=" . param("query_id"), $target = "_self", $flags = 0);
$page->add_tab("q_fields", "Fields", "query_generator_fields.php?query_id=" . param("query_id"), $target = "_self", $flags = 0);
$page->add_tab("q_fieldorder", "Field Order", "query_generator_field_order.php?query_id=" . param("query_id"), $target = "_self", $flags = 0);
$page->add_tab("q_sortorder", "Sort Order", "query_generator_sortorder.php?query_id=" . param("query_id"), $target = "_self", $flags = 0);
$page->add_tab("q_filter1", "Basic Filter", "query_generator_filter1.php?query_id=" . param("query_id"), $target = "_self", $flags = 0);
$page->add_tab("q_filter2", "Extended Filter", "query_generator_filter2.php?query_id=" . param("query_id"), $target = "_self", $flags = 0);
$page->add_tab("q_grouping", "Grouping", "query_generator_grouping.php?query_id=" . param("query_id"), $target = "_self", $flags = 0);
$page->add_tab("q_function", "Functions", "query_generator_functions.php?query_id=" . param("query_id"), $target = "_self", $flags = 0);

$page->tabs();


?>