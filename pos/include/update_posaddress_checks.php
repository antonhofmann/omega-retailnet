<?php
/********************************************************************

	update_posaddress_checks.php

    update all new additional posaddresses to _posaddresses

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2009-02-01
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2009-02-01
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/
if(param("country"))
{
	check_access("can_edit_catalog");

	
	$sql_p = "select * from posaddresses " . 
		     "where (posaddress_store_closingdate is null " .
		     "or posaddress_store_closingdate = '0000-00-00') " . 
		     "and posaddress_country = " . param("country");

	$res_p = mysql_query($sql_p);
	while ($row_p = mysql_fetch_assoc($res_p))
	{
		//insert or update records
		$sql = "select * from _posaddresses where posaddress_id = " . $row_p["posaddress_id"];
		$res = mysql_query($sql);
		if ($row = mysql_fetch_assoc($res))
		{

			$sql = "select * from posaddresses where posaddress_id = " . $row_p["posaddress_id"];
			$res = mysql_query($sql);
			if ($row = mysql_fetch_assoc($res))
			{
			
				//update posaddress
				$fields = array();
				if($row["posaddress_franchisee_id"])
				{
					$value = dbquote($row["posaddress_franchisee_id"]);
					$fields[] = "posaddress_franchisee_id = " . $value;
				}

				$value = dbquote($row["posaddress_ownertype"]);
				$fields[] = "posaddress_ownertype = " . $value;

				$value = dbquote($row["posaddress_name"]);
				$fields[] = "posaddress_name = " . $value;

				$value = dbquote($row["posaddress_address"]);
				$fields[] = "posaddress_address = " . $value;

				$value = dbquote($row["posaddress_address2"]);
				$fields[] = "posaddress_address2 = " . $value;

				$value = dbquote($row["posaddress_zip"]);
				$fields[] = "posaddress_zip = " . $value;

				$value =  dbquote($row["posaddress_place"]);
				$fields[] = "posaddress_place = " . $value;

				$value = dbquote($row["posaddress_country"]);
				$fields[] = "posaddress_country = " . $value;
				
				$value = dbquote($row["posaddress_phone"]);
				$fields[] = "posaddress_phone = " . $value;
				
				$value = dbquote($row["posaddress_fax"]);
				$fields[] = "posaddress_fax = " . $value;
				
				$value = dbquote($row["posaddress_email"]);
				$fields[] = "posaddress_email = " . $value;

				$value = dbquote($row["posaddress_perc_class"]);
				$fields[] = "posaddress_perc_class = " . $value;

				$value = dbquote($row["posaddress_perc_tourist"]);
				$fields[] = "posaddress_perc_tourist = " . $value;

				$value = dbquote($row["posaddress_perc_transport"]);
				$fields[] = "posaddress_perc_transport = " . $value;

				$value =  dbquote($row["posaddress_perc_people"]);
				$fields[] = "posaddress_perc_people = " . $value;

				$value = dbquote($row["posaddress_perc_parking"]);
				$fields[] = "posaddress_perc_parking = " . $value;

				$value = dbquote($row["posaddress_perc_visibility1"]);
				$fields[] = "posaddress_perc_visibility1 = " . $value;

				$value = dbquote($row["posaddress_perc_visibility2"]);
				$fields[] = "posaddress_perc_visibility2 = " . $value;


				$value =  dbquote($row["posaddress_store_postype"]);
				$fields[] = "posaddress_store_postype = " . $value;


				$value =  dbquote($row["posaddress_store_subclass"]);
				$fields[] = "posaddress_store_subclass = " . $value;


				$value =  dbquote($row["posaddress_store_furniture"]);
				$fields[] = "posaddress_store_furniture = " . $value;

				$value =  dbquote($row["posaddress_store_furniture_subclass"]);
				$fields[] = "posaddress_store_furniture_subclass = " . $value;
				
				$value = dbquote($row["posaddress_store_grosssurface"]);
				$fields[] = "posaddress_store_grosssurface = " . $value;

				$value = dbquote($row["posaddress_store_totalsurface"]);
				$fields[] = "posaddress_store_totalsurface = " . $value;

				$value =  dbquote($row["posaddress_store_retailarea"]);
				$fields[] = "posaddress_store_retailarea = " . $value;

				$value = dbquote($row["posaddress_store_backoffice"]);
				$fields[] = "posaddress_store_backoffice = " . $value;

				$value = dbquote($row["posaddress_store_numfloors"]);
				$fields[] = "posaddress_store_numfloors = " . $value;

				$value = dbquote($row["posaddress_store_floorsurface1"]);
				$fields[] = "posaddress_store_floorsurface1 = " . $value;

				$value =  dbquote($row["posaddress_store_floorsurface2"]);
				$fields[] = "posaddress_store_floorsurface2 = " . $value;

				$value = dbquote($row["posaddress_store_floorsurface3"]);
				$fields[] = "posaddress_store_floorsurface3 = " . $value;

				$value = dbquote($row["posaddress_store_headcounts"]);
				$fields[] = "posaddress_store_headcounts = " . $value;

				$value = dbquote($row["posaddress_store_fulltimeeqs"]);
				$fields[] = "posaddress_store_fulltimeeqs = " . $value;

				$value = dbquote($row["posaddress_google_lat"]);
				$fields[] = "posaddress_google_lat = " . $value;

				$value =  dbquote($row["posaddress_google_long"]);
				$fields[] = "posaddress_google_long = " . $value;

				$fields[] = "posaddress_google_precision = 1";

				$value = dbquote($row["posaddress_export_to_web"]);
				$fields[] = "posaddress_export_to_web = " . $value;

				$value = dbquote($row["posaddress_store_openingdate"]);
				$fields[] = "posaddress_store_openingdate = " . $value;

				$value = dbquote($row["posaddress_store_closingdate"]);
				$fields[] = "posaddress_store_closingdate = " . $value;

				$sql = "update _posaddresses set " . join(", ", $fields) . " where posaddress_id = " . dbquote($row_p["posaddress_id"]);
				mysql_query($sql) or dberror($sql);


				//posareas
				
				$sql = "delete from _posareas where posarea_posaddress = " . $row_p["posaddress_id"];
				mysql_query($sql) or dberror($sql);

				$sql = "select * from posareas where posarea_posaddress = " . $row_p["posaddress_id"];
				$res = mysql_query($sql);
				while ($row = mysql_fetch_assoc($res))
				{
				
					$fields = array();
					$values = array();

					$values[] = dbquote($row["posarea_posaddress"]);
					$fields[] = "posarea_posaddress";

					$values[] = dbquote($row["posarea_area"]);
					$fields[] = "posarea_area";

					$values[] = dbquote($row["user_created"]);
					$fields[] = "user_created";

					$values[] = dbquote($row["date_created"]);
					$fields[] = "date_created";

					$values[] = dbquote($row["user_modified"]);
					$fields[] = "user_modified";

					$values[] = dbquote($row["date_modified"]);
					$fields[] = "date_modified";

					$sql = "insert into _posareas (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					mysql_query($sql) or dberror($sql);
				}
			}
		}
		else
		{
				$fields = array();
				$values = array();
				
				//_posaddresses
				$fields = array();
				
				$values[] = dbquote($row_p["posaddress_id"]);
				$fields[] = "posaddress_id";


				$values[] = dbquote($row_p["posaddress_client_id"]);
				$fields[] = "posaddress_client_id";

				if($row_p["posaddress_franchisee_id"])
				{
					$values[] = dbquote($row_p["posaddress_franchisee_id"]);
					$fields[] = "posaddress_franchisee_id";
				}

				$values[] = dbquote($row_p["posaddress_ownertype"]);
				$fields[] = "posaddress_ownertype";

				$values[] = dbquote($row_p["posaddress_name"]);
				$fields[] = "posaddress_name";

				$values[] = dbquote($row_p["posaddress_address"]);
				$fields[] = "posaddress_address";

				$values[] = dbquote($row_p["posaddress_address2"]);
				$fields[] = "posaddress_address2";

				$values[] = dbquote($row_p["posaddress_zip"]);
				$fields[] = "posaddress_zip";

				$values[] =  dbquote($row_p["posaddress_place"]);
				$fields[] = "posaddress_place";

				$values[] = dbquote($row_p["posaddress_country"]);
				$fields[] = "posaddress_country";
				
				$values[] = dbquote($row_p["posaddress_phone"]);
				$fields[] = "posaddress_phone";
				
				$values[] = dbquote($row_p["posaddress_fax"]);
				$fields[] = "posaddress_fax";
				
				$values[] = dbquote($row_p["posaddress_email"]);
				$fields[] = "posaddress_email";

				$values[] = dbquote($row_p["posaddress_perc_class"]);
				$fields[] = "posaddress_perc_class";

				$values[] = dbquote($row_p["posaddress_perc_tourist"]);
				$fields[] = "posaddress_perc_tourist";

				$values[] = dbquote($row_p["posaddress_perc_transport"]);
				$fields[] = "posaddress_perc_transport";

				$values[] =  dbquote($row_p["posaddress_perc_people"]);
				$fields[] = "posaddress_perc_people";

				$values[] = dbquote($row_p["posaddress_perc_parking"]);
				$fields[] = "posaddress_perc_parking";

				$values[] = dbquote($row_p["posaddress_perc_visibility1"]);
				$fields[] = "posaddress_perc_visibility1";

				$values[] = dbquote($row_p["posaddress_perc_visibility2"]);
				$fields[] = "posaddress_perc_visibility2";


				$values[] =  dbquote($row_p["posaddress_store_postype"]);
				$fields[] = "posaddress_store_postype";


				$values[] =  dbquote($row_p["posaddress_store_subclass"]);
				$fields[] = "posaddress_store_subclass";


				$values[] =  dbquote($row_p["posaddress_store_furniture"]);
				$fields[] = "posaddress_store_furniture";

				$values[] =  dbquote($row_p["posaddress_store_furniture_subclass"]);
				$fields[] = "posaddress_store_furniture_subclass";

				$values[] = dbquote($row_p["posaddress_store_grosssurface"]);
				$fields[] = "posaddress_store_grosssurface";

				$values[] = dbquote($row_p["posaddress_store_totalsurface"]);
				$fields[] = "posaddress_store_totalsurface";

				$values[] =  dbquote($row_p["posaddress_store_retailarea"]);
				$fields[] = "posaddress_store_retailarea";

				$values[] = dbquote($row_p["posaddress_store_backoffice"]);
				$fields[] = "posaddress_store_backoffice";

				$values[] = dbquote($row_p["posaddress_store_numfloors"]);
				$fields[] = "posaddress_store_numfloors";

				$values[] = dbquote($row_p["posaddress_store_floorsurface1"]);
				$fields[] = "posaddress_store_floorsurface1";

				$values[] =  dbquote($row_p["posaddress_store_floorsurface2"]);
				$fields[] = "posaddress_store_floorsurface2";

				$values[] = dbquote($row_p["posaddress_store_floorsurface3"]);
				$fields[] = "posaddress_store_floorsurface3";

				$values[] = dbquote($row_p["posaddress_store_headcounts"]);
				$fields[] = "posaddress_store_headcounts";

				$values[] = dbquote($row_p["posaddress_store_fulltimeeqs"]);
				$fields[] = "posaddress_store_fulltimeeqs";

				$values[] = dbquote($row_p["posaddress_google_lat"]);
				$fields[] = "posaddress_google_lat";

				$values[] =  dbquote($row_p["posaddress_google_long"]);
				$fields[] = "posaddress_google_long";

				$values[] =  1;
				$fields[] = "posaddress_google_precision";

				$values[] = dbquote($row_p["posaddress_export_to_web"]);
				$fields[] = "posaddress_export_to_web";

				$values[] = dbquote($row_p["posaddress_store_openingdate"]);
				$fields[] = "posaddress_store_openingdate";

				$values[] = dbquote($row_p["posaddress_store_closingdate"]);
				$fields[] = "posaddress_store_closingdate";

				$values[] = dbquote($row_p["user_created"]);
				$fields[] = "user_created";

				$values[] = dbquote($row_p["date_created"]);
				$fields[] = "date_created";

				$values[] = dbquote($row_p["user_modified"]);
				$fields[] = "user_modified";

				$values[] = dbquote($row_p["date_modified"]);
				$fields[] = "date_modified";


				$sql = "insert into _posaddresses (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);


				//posareas
				
				$sql = "delete from _posareas where posarea_posaddress = " . $row_p["posaddress_id"];
				mysql_query($sql) or dberror($sql);

				$sql = "select * from posareas where posarea_posaddress = " . $row_p["posaddress_id"];
				$res = mysql_query($sql);
				while ($row = mysql_fetch_assoc($res))
				{
				
					$fields = array();
					$values = array();

					$values[] = dbquote($row["posarea_posaddress"]);
					$fields[] = "posarea_posaddress";

					$values[] = dbquote($row["posarea_area"]);
					$fields[] = "posarea_area";

					$values[] = dbquote($row["user_created"]);
					$fields[] = "user_created";

					$values[] = dbquote($row["date_created"]);
					$fields[] = "date_created";

					$values[] = dbquote($row["user_modified"]);
					$fields[] = "user_modified";

					$values[] = dbquote($row["date_modified"]);
					$fields[] = "date_modified";

					$sql = "insert into _posareas (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					mysql_query($sql) or dberror($sql);
				}


		}
			
			
	}
}

?>