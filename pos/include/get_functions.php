<?php
/********************************************************************

    get_functions.php

    Various utility functions to get information from tables.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
/********************************************************************
    get the field values of a user
*********************************************************************/
function get_user($id)
{
    $user = array();

    if ($id == '')
    {
        $user["id"] = 0;
		$user["firstname"] = "";
        $user["name"] = "";
        $user["contact"] = "";
        $user["phone"] = "";
        $user["fax"] = "";
        $user["email"] = "";
        $user["cc"] = "";
        $user["deputy"] = "";
        $user["address"] = "";
		$user["country"] = "";
		$user["user_id"] = "";
		$user["password"] = "";
		$user["address"] = "";
    }
    else
    {
        $sql = "select * from users " .
			   "left join addresses on address_id = user_address " . 
			   "where user_id = " . $id;

        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $user["id"] = $row["user_id"];
			$user["firstname"] = $row["user_firstname"];
            $user["name"] = $row["user_name"];
            $user["contact"] = $row["user_name"] . " " . $row["user_firstname"];
            $user["phone"] = $row["user_phone"];
            $user["fax"] = $row["user_fax"];
            $user["email"] = $row["user_email"];
            $user["cc"] = $row["user_email_cc"];
            $user["deputy"] = $row["user_email_deputy"];
            $user["address"] = $row["user_address"];
			$user["country"] = $row["address_country"];
			$user["user_id"] = $row["user_login"];
			$user["password"] = $row["user_password"];
			$user["address"] =  $row["user_address"];
        }
    }
    return $user;
}


/********************************************************************
    see if user can edit POS DATA
*********************************************************************/
function get_user_edit_permission($user_id, $pos_id = 0)
{
	
	$user = get_user($user_id);

	$sql = "select posaddress_client_id " .
		   "from posaddresses " .
		   "where posaddress_id = " . $pos_id;

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);

	if($user["address"] == $row["posaddress_client_id"])
	{
		return true;
	}
	else
	{
		return false;
	}

	return false;
}


/********************************************************************
    get all POS where company is franchisor
*********************************************************************/
function get_posaddresses_franchisors($company_id)
{
	$posaddresses = array();

	if($company_id == 0) {
		return $posaddresses;
	}


	$sql = "select posaddress_id, posaddress_name, posaddress_zip ,posaddress_place , country_name, posaddress_store_closingdate " . 
	       "from posaddresses " .
		   "left join countries on country_id = posaddress_country " .
		   "where posaddress_franchisor_id = " . $company_id  . 
		   " order by posaddress_name";

	$res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
		$caption = $row["posaddress_name"] . ", " . $row["posaddress_zip"] . " " . $row["posaddress_place"] . ", " . $row["country_name"];

		if($row["posaddress_store_closingdate"] and $row["posaddress_store_closingdate"] != "0000-00-00")
		{
			$caption .= "<br /> closed: " . to_system_date($row["posaddress_store_closingdate"]);
		}
		$posaddresses[$row["posaddress_id"]] = $caption;
    }

	return $posaddresses;
}

/********************************************************************
    get all POS where company is franchisee
*********************************************************************/
function get_posaddresses_franchisees($company_id)
{
	$posaddresses = array();

	if($company_id == 0) {
		return $posaddresses;
	}


	$sql = "select posaddress_id, posaddress_name, posaddress_zip ,posaddress_place , " . 
		   "country_name, posaddress_store_closingdate, postype_name, project_costtype_text " . 
	       "from posaddresses " .
		   "left join countries on country_id = posaddress_country " .
		   "left join postypes on postype_id = posaddress_store_postype " .
		   "left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
		   "where posaddress_franchisee_id = " . $company_id  . 
		   " order by posaddress_name";

	$res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
		$caption = $row['project_costtype_text'] . ' ' . $row['postype_name'] . ': ' . $row["posaddress_name"] . ", " . $row["posaddress_zip"] . " " . $row["posaddress_place"] . ", " . $row["country_name"];

		if($row["posaddress_store_closingdate"] and $row["posaddress_store_closingdate"] != "0000-00-00")
		{
			$caption .= "<br /> closed: " . to_system_date($row["posaddress_store_closingdate"]);
		}
		$posaddresses[$row["posaddress_id"]] = $caption;
    }
	

	return $posaddresses;
}

/********************************************************************
    get system currency informations
*********************************************************************/
function get_system_currency_fields()
{
    $system_currency = array();

    $sql = "select * from currencies where currency_system=1";
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $system_currency["id"] = $row["currency_id"];
        $system_currency["symbol"] = $row["currency_symbol"];
        $system_currency["exchange_rate"] = $row["currency_exchange_rate"];
        $system_currency["factor"] = $row["currency_factor"];
    }
    
    return $system_currency;
}

/********************************************************************
    get project and order data 
*********************************************************************/
function get_project($id)
{
	$project = array();

    $sql = "select *, projects.date_created as date_created ".
           "from projects ".
           "left join orders on project_order = order_id ".
  		   "left join product_lines on project_product_line = product_line_id ".
		   "left join postypes on postype_id = project_postype ".
	       "left join projectkinds on projectkind_id = project_projectkind ".
	       "left join project_costs on project_cost_order = order_id " . 
	       "left join project_costtypes on project_costtype_id = project_cost_type " .
           "left join transportation_types on order_preferred_transportation = transportation_type_id ".
           "where order_id  = " . $id;

	$res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $project = $row;
    }

	if(isset($project["order_shop_address_country"]))
	{
		$project["order_shop_address_country_name"] = "";

		$sql = "select country_name ".
			   "from countries ".
			   "where country_id  = " . $project["order_shop_address_country"];

		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$project["order_shop_address_country_name"] = $row["country_name"];
		}
	}
	else
	{
		$project["order_shop_address_country_name"] = "";
	}

        

    return $project;
}


/********************************************************************
    get order data 
*********************************************************************/
function get_order($id)
{
	$project = array();

    $sql = "select *, orders.date_created as date_created ".
           "from orders ".
           "where order_id  = " . $id;


	$res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $project = $row;
    }

	if(isset($project["order_shop_address_country"]))
	{
		$project["order_shop_address_country_name"] = "";

		$sql = "select country_name ".
			   "from countries ".
			   "where country_id  = " . $project["order_shop_address_country"];

		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$project["order_shop_address_country_name"] = $row["country_name"];
		}
	}
	else
	{
		$project["order_shop_address_country_name"] = "";
	}

        

    return $project;
}


/********************************************************************
    get the field values of an address
*********************************************************************/
function get_address($id)
{
    $address = array();

    if ($id == '')
    {
            $address["shortcut"] = "";
            $address["company"] = "";
            $address["company2"] = "";
            $address["address"] = "";
            $address["address2"] = "";
            $address["zip"] = "";
            $address["place"] = "";
            $address["country"] = "";
            $address["country_name"] = "";
            $address["currency"] = "";
            $address["phone"] = "";
            $address["fax"] = "";
            $address["email"] = "";
            $address["contact"] = "";
            $address["client_type"] = "";
    }
    else
    {
        $sql = "select * from addresses where address_id = " . $id;
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $address["shortcut"] = $row["address_shortcut"];
            $address["company"] = $row["address_company"];
            $address["company2"] = $row["address_company2"];
            $address["address"] = $row["address_address"];
            $address["address2"] = $row["address_address2"];
            $address["zip"] = $row["address_zip"];
            $address["place"] = $row["address_place"];
            $address["country"] = $row["address_country"];
            $address["country_name"] = "";
            $address["currency"] = $row["address_currency"];
            $address["phone"] = $row["address_phone"];
            $address["fax"] = $row["address_fax"];
            $address["email"] = $row["address_email"];
            $address["contact"] = $row["address_contact"];
            $address["client_type"] = $row["address_client_type"];

            $sql = "select country_id, country_name ".
                   "from countries ".
                   "where country_id = " . $address["country"];

            $res = mysql_query($sql);
            if ($res)
            {
                $row = mysql_fetch_assoc($res);
                $address["country_name"] = $row['country_name'];
            }

        }
    }
    return $address;
}


/********************************************************************
    encode google map coordinates
*********************************************************************/
function google_maps_geo_encode()
{
		
	echo "<strong>Searching for Country, City and Address</strong><br />";
	$api_key = GOOGLE_API_KEY_GEO;


	// Address and Place
	$query = "SELECT * FROM posaddresses left join countries on country_id = posaddress_country " . 
		     "where posaddress_google_precision is NUll or posaddress_google_precision <> 1";
	
	$result = mysql_query($query);

	if (!$result)
	{
		die("Invalid query: " . mysql_error());
	}
    
	
	// Iterate through the rows, geocoding each address
	while ($row = @mysql_fetch_assoc($result)) 
	{
	    $pos_id = $row["posaddress_id"];
		$address = $row["country_name"];
		$address .= "+" . $row["posaddress_place"];
		$address .= "+" . $row["posaddress_address"];
		$address = urlencode($address);

		$lat = "";
		$long = "";

		//$url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false";
		$url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . $address."&sensor=false&key=" . $api_key;
		
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

		if(PROXY_SERVER)
		{
			curl_setopt($ch, CURLOPT_PROXY, PROXY_SERVER);
		}
		$response = curl_exec($ch);

		
		
		curl_close($ch);
		$response_a = json_decode($response);

		if(isset($response_a->results[0]->geometry->location->lat))
		{
			$lat = $response_a->results[0]->geometry->location->lat;
			$long = $response_a->results[0]->geometry->location->lng;
		}

		if(is_numeric($lat) and is_numeric($long))
		{
			$query = "UPDATE posaddresses " .
					 " SET posaddress_google_precision = 1, " . 
					 " posaddress_google_lat = '" . $lat . "', " . 
					 " posaddress_google_long = '" . $long . "' " .
					 " WHERE posaddress_id = " . $pos_id;
					  
			$update_result = mysql_query($query);
		}
		else
		{
			$address = $row["country_name"];
			$address .= "+" . $row["posaddress_place"];
			$address = urlencode($address);

			$url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&key=" . $api_key;;
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			if(PROXY_SERVER)
			{
				curl_setopt($ch, CURLOPT_PROXY, PROXY_SERVER);
			}
			$response = curl_exec($ch);
			curl_close($ch);
			$response_a = json_decode($response);
			
			if(isset($response_a->results[0]->geometry->location->lat))
			{
				$lat = $response_a->results[0]->geometry->location->lat;
				$long = $response_a->results[0]->geometry->location->lng;
			}

			if(is_numeric($lat) and is_numeric($long))
			{
				$query = "UPDATE posaddresses " .
						 " SET posaddress_google_precision = 1, " . 
						 " posaddress_google_lat = '" . $lat . "', " . 
						 " posaddress_google_long = '" . $long . "' " .
						 " WHERE posaddress_id = " . $pos_id;
					  
						$update_result = mysql_query($query);
			}
			else
			{
				$address = $row["country_name"];
				$address = urlencode($address);

				$url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&key=" . $api_key;;
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				if(PROXY_SERVER)
				{
					curl_setopt($ch, CURLOPT_PROXY, PROXY_SERVER);
				}
				$response = curl_exec($ch);
				curl_close($ch);
				$response_a = json_decode($response);
				
				if(isset($response_a->results[0]->geometry->location->lat))
				{
					$lat = $response_a->results[0]->geometry->location->lat;
					$long = $response_a->results[0]->geometry->location->lng;
				}

				if(is_numeric($lat) and is_numeric($long))
				{
					$query = "UPDATE posaddresses " .
							 " SET posaddress_google_precision = 1, " . 
							 " posaddress_google_lat = '" . $lat . "', " . 
							 " posaddress_google_long = '" . $long . "' " .
							 " WHERE posaddress_id = " . $pos_id;
							  
					$update_result = mysql_query($query);
				}
			}
		}

		echo $address . " google map created<br />";

	}

	return true;
}

/********************************************************************
    get brand manager
*********************************************************************/
function get_brand_manager($address_id = 0, $country_id = 0)
{
	$user_data = array();
	
	
	$sql = "select user_id, user_email, user_firstname, user_name " .
		   "from user_roles " . 
		   "left join users on user_id = user_role_user " .
		   "left join addresses on address_id = user_address " . 
		   " where user_role_role = 15 " . 
		   " and address_id = " . dbquote($address_id) . 
		   " and user_active = 1";

	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$user_data = $row;
		return $user_data;
	}
	
	//sub or agent
	$sql = "select user_id, user_email, user_firstname, user_name " .
		   "from user_roles " . 
		   "left join users on user_id = user_role_user " .
		   "left join addresses on address_id = user_address " . 
		   " where user_role_role = 15 " . 
		   " and address_country = " . dbquote($country_id) . 
		   " and user_active = 1 " . 
		   " and address_client_type in (1, 2)";
	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$user_data = $row;
		return $user_data;
	}


	//address has no brand manager
	$sql = "select user_id, user_email, user_firstname, user_name " .
		   "from country_access " . 
		   "left join users on user_id = country_access_user " .
		   "left join user_roles on user_role_user = user_id " . 
		   "left join addresses on address_id = user_address " . 
		   " where user_role_role = 15 " . 
		   " and address_country = " . dbquote($country_id) . 
		   " and user_active = 1";

	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$user_data = $row;
	}

	return $user_data;
}

/********************************************************************
    get head of controlling
*********************************************************************/
function get_head_of_controlling($address_id = 0, $country_id = 0)
{
	$user_data = array();
	
	
	$sql = "select user_id, user_email, user_firstname, user_name " .
		   "from user_roles " . 
		   "left join users on user_id = user_role_user " .
		   "left join addresses on address_id = user_address " . 
		   " where user_role_role = 62 " . 
		   " and address_id = " . dbquote($address_id) . 
		   " and user_active = 1";

	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$user_data = $row;
		return $user_data;
	}
	
	//sub or agent
	$sql = "select user_id, user_email, user_firstname, user_name " .
		   "from user_roles " . 
		   "left join users on user_id = user_role_user " .
		   "left join addresses on address_id = user_address " . 
		   " where user_role_role = 62 " . 
		   " and address_country = " . dbquote($country_id) . 
		   " and user_active = 1 " . 
		   " and address_client_type in (1, 2)";
	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$user_data = $row;
		return $user_data;
	}


	return $user_data;
}


/********************************************************************
    get finance manager
*********************************************************************/
function get_finance_manager($address_id = 0, $country_id = 0)
{
	$user_data = array();
	
	$sql = "select user_id, user_email, user_firstname, user_name " .
		   "from user_roles " . 
		   "left join users on user_id = user_role_user " .
		   "left join addresses on address_id = user_address " . 
		   " where user_role_role = 45 " . 
		   " and address_id = " . dbquote($address_id) . 
		   " and user_active = 1";

	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$user_data = $row;
		return $user_data;
	}

	//sub or agent
	$sql = "select user_id, user_email, user_firstname, user_name " .
		   "from user_roles " . 
		   "left join users on user_id = user_role_user " .
		   "left join addresses on address_id = user_address " . 
		   " where user_role_role = 45 " . 
		   " and address_country = " . dbquote($country_id) . 
		   " and user_active = 1 " . 
		   " and address_client_type in (1, 2)";

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$user_data = $row;
		return $user_data;
	}


	//address has no brand manager
	$sql = "select user_id, user_email, user_firstname, user_name " .
		   "from country_access " . 
		   "left join users on user_id = country_access_user " .
		   "left join user_roles on user_role_user = user_id " . 
		   "left join addresses on address_id = user_address " . 
		   " where user_role_role = 45 " . 
		   " and address_country = " . dbquote($country_id) . 
		   " and user_active = 1";

	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$user_data = $row;
	}

	return $user_data;
}


/********************************************************************
    get country manager
*********************************************************************/
function get_country_manager($address_id = 0, $country_id = 0)
{
	$user_data = array();
	
	$sql = "select user_id, user_email, user_firstname, user_name " .
		   "from user_roles " . 
		   "left join users on user_id = user_role_user " .
		   "left join addresses on address_id = user_address " . 
		   " where user_role_role = 61 " . 
		   " and address_id = " . dbquote($address_id) . 
		   " and user_active = 1";

	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$user_data = $row;
		return $user_data;
	}

	//sub or agent
	$sql = "select user_id, user_email, user_firstname, user_name " .
		   "from user_roles " . 
		   "left join users on user_id = user_role_user " .
		   "left join addresses on address_id = user_address " . 
		   " where user_role_role = 61 " . 
		   " and address_country = " . dbquote($country_id) . 
		   " and user_active = 1 " . 
		   " and address_client_type in (1, 2)";

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$user_data = $row;
		return $user_data;
	}


	//address has no county manager
	$sql = "select user_id, user_email, user_firstname, user_name " .
		   "from country_access " . 
		   "left join users on user_id = country_access_user " .
		   "left join user_roles on user_role_user = user_id " . 
		   "left join addresses on address_id = user_address " . 
		   " where user_role_role = 61 " . 
		   " and address_country = " . dbquote($country_id) . 
		   " and user_active = 1";

	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$user_data = $row;
	}

	return $user_data;
}

/********************************************************************
    get POS Info of relocated POS
*********************************************************************/
function get_relocated_pos_info($pos_id = 0)
{
	$pod_data = array();

	$sql = "select posaddress_id, posaddress_name, place_name " .
		   "from posaddresses " . 
		   "left join places on place_id = posaddress_place_id " .
		   "where posaddress_id = " . $pos_id;

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$pos_data = $row;
	}

	return $pos_data;
}

/********************************************************************
    get info of POS that was relocated
*********************************************************************/
function get_relocated_pos_info2($pos_id = 0)
{
	$pos_data = array();

	
	$sql = "select project_relocated_posaddress_id " .
		   " from posorders " . 
		   " left join projects on project_order = posorder_order " . 
		   " where project_relocated_posaddress_id > 0 " . 
		   " and posorder_posaddress = " . $pos_id;

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$sql = "select posaddress_id, posaddress_name, place_name " .
			   "from posaddresses " . 
			   "left join places on place_id = posaddress_place_id " .
			   "where posaddress_id = " . $row["project_relocated_posaddress_id"];

		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$pos_data = $row;
		}
	}

	return $pos_data;
}

/********************************************************************
    get info of relocated POS
*********************************************************************/
function get_relocated_pos_info3($pos_id = 0)
{
	$pos_data = array();

	if($pos_id == 0)
	{
		return $pos_data;
	}

	
	$sql = "select posorder_posaddress " .
		   " from projects " . 
		   " left join posorders on project_order = posorder_order " . 
		   " where posorder_posaddress > 0 and project_relocated_posaddress_id = " . $pos_id; 


	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$sql = "select posaddress_id, posaddress_name, place_name, posaddress_store_openingdate " .
			   "from posaddresses " . 
			   "left join places on place_id = posaddress_place_id " .
			   "where posaddress_id = " . $row["posorder_posaddress"];

		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$pos_data = $row;
		}
	}

	return $pos_data;
}

/********************************************************************
    get info of cacsaced of POS being relocates
*********************************************************************/
function get_relocated_pos_info4($pos_id = 0)
{
	$pos_data = array();

	$sql = "select project_relocated_posaddress_id " .
		   " from posorders " . 
		   " left join projects on project_order = posorder_order " . 
		   " where project_relocated_posaddress_id > 0 " . 
		   " and posorder_posaddress = " . $pos_id;

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$sql = "select posaddress_id, posaddress_name, place_name, posaddress_store_openingdate " .
			   "from posaddresses " . 
			   "left join places on place_id = posaddress_place_id " .
			   "where posaddress_id = " . $row["project_relocated_posaddress_id"];

		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$pos_data[] = $row;
			$tmp_posaddress_id = $row['posaddress_id'];

			$tmp = get_relocated_pos_info4($tmp_posaddress_id);
			
			if(count($tmp) > 0) {
				$pos_data[] = $tmp[0];
			}
			else {
				$do_loop = false;
			}
		}
	}

	return $pos_data;
}

?>