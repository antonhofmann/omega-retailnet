<?php
/********************************************************************

    query_generator_filter1.php

    Filter Selection for the Query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2009-01-11
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2009-01-11
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";


check_access("can_query_posindex");

require_once "include/query_get_functions.php";

/********************************************************************
    prepare all data needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("query_generator.php");
}

$query_id = param("query_id");

$posquery = get_query_name($query_id);
$icon = "../pictures/add_item.gif";
$link = "javascript:open_selector('')";


$filter = get_query_filter($query_id);

// create sql for filter criteria

$sql_salesregion = "select salesregion_id, salesregion_name ".
               "from salesregions order by salesregion_name";

$sql_region = "select region_id, region_name ".
			   "from regions order by region_name";


$sql_country = "select DISTINCT country_id, country_name " .
			   "from posaddresses " .
			   "left join countries on country_id = posaddress_country " . 
			   "order by country_name";

$sql_provinces = "select DISTINCT province_id, province_canton ".
			  "from provinces order by province_canton";

$sql_cities = "select DISTINCT posaddress_place ".
			  "from posaddresses order by posaddress_place";

$sql_areas = "select posareatype_id, posareatype_name ".
              "from posareatypes order by posareatype_name";


$sql_product_lines = "select product_line_id, product_line_name ".
                     "from product_lines " . 
                     "where product_line_posindex = 1 " . 
                     "   order by product_line_name";

$sql_furniture_subclasses = "select productline_subclass_id, productline_subclass_name ".
                     "from productline_subclasses " . 
                     "   order by productline_subclass_name";

$sql_pos_types = "select DISTINCT postype_name ".
                     "from postypes " .
                     "order by postype_name";

$sql_possubclasses = "select possubclass_id, possubclass_name ".
                     "from possubclasses " .
                     "order by possubclass_name";

$sql_project_cost_types = "select project_costtype_id, project_costtype_text ".
						  "from project_costtypes " .
                          "order by project_costtype_text";


$sql_project_kinds = "select projectkind_id, projectkind_name ".
						 "from projectkinds " .
						 "where projectkind_id > 0 " .
						 "order by projectkind_name";


$sql_years = "select DISTINCT YEAR(order_date) as year1, YEAR(order_date) as year2 " .
             "from orders " . 
			 "where order_type = 1 " . 
			 "order by year1";


$sql_distribution_channels = "select mps_distchannel_id, " . 
                             "concat(mps_distchannel_group, ' - ', mps_distchannel_name, ' - ', mps_distchannel_code) as distributionchannel ".
							"from mps_distchannels " .
							"order by mps_distchannel_group, mps_distchannel_name, mps_distchannel_code";


$sql_design_objectives = "select design_objective_item_id, design_objective_item_name, " .
						 "    design_objective_group_name, postype_name " .
						 "from design_objective_items left join design_objective_groups on design_objective_item_group = design_objective_group_id " .
						 "    left join postypes on design_objective_group_postype = postype_id " . 
						 " where postype_name <> '' " . 
						 " order by postype_name, design_objective_group_name, design_objective_item_name ";

$franchisee_filter = "";
if(param("client_id") > 0)
{
	$franchisee_filter = " and address_parent = " . param("client_id") . " ";
}

if(has_access("can_view_posindex"))
{

	$sql_franchisees = "select DISTINCT posaddress_franchisee_id, concat(country_name, ', ', address_company) as company  " .
					   "from posaddresses " . 
					   "left join addresses on address_id = posaddress_franchisee_id ".
					   "left join countries on country_id = address_country " .
					   "where posaddress_franchisee_id > 0 ".
		               $franchisee_filter . 
					   "order by country_name, address_company";
}
else
{
	$predefined_filter = user_predefined_filter(user_id());

	$sql_franchisees = "select DISTINCT posaddress_franchisee_id, concat(country_name, ', ', address_company) as company  " .
					   "from posaddresses " . 
					   "left join addresses on address_id = posaddress_franchisee_id ".
					   "left join countries on country_id = address_country " .
					   "where posaddress_franchisee_id > 0 " .
		               $franchisee_filter . 
		               " and address_parent IN " . $predefined_filter["user_address_id_filter"] . 
					   " order by country_name, address_company";
}

if(has_access("can_view_posindex"))
{
	$sql_clients = "select DISTINCT posaddress_client_id, concat(country_name, ', ', address_company) as company  " .
				   "from posaddresses " . 
				   "left join addresses on address_id = posaddress_client_id ".
				   "left join countries on country_id = address_country " .
				   "where posaddress_client_id > 0 ".
				   "order by country_name, address_company";
}
else
{
	$sql_clients = "select DISTINCT posaddress_client_id, concat(country_name, ', ', address_company) as company  " .
				   "from posaddresses " . 
				   "left join addresses on address_id = posaddress_client_id ".
				   "left join countries on country_id = address_country " .
				   "left join users on user_address = address_id " . 
				   "where posaddress_client_id > 0 " .
		           " and user_id = " . user_id() . 
				   " order by country_name, address_company";
}

$sql_client_types = "select client_type_id, client_type_code from client_types order by client_type_code";
$sql_address_types = "select address_type_id, address_type_name from address_types 
where address_type_id in (1, 7) order by address_type_name";

//get closing years
$from_closing_years = array();
$from_closing_months = array();
$to_closing_years = array();
$to_closing_months = array();
$sql_y = "select DISTINCT year(posclosing_startdate) as fyear, year(posclosing_enddate) as tyear " . 
         "from posclosings " . 
		  "order by year(posclosing_startdate) DESC";
$res_y = mysql_query($sql_y) or dberror($sql_y);
while ($row_y = mysql_fetch_assoc($res_y))
{
	$from_closing_years[$row_y["fyear"]] = $row_y["fyear"];
	$to_closing_years[$row_y["tyear"]] = $row_y["tyear"];
}


//get actual opening dates
$from_opening_years = array();
$from_opening_months = array();
$to_opening_years = array();
$to_opening_months = array();

$operating_per_years = array();
$operating_per_months = array();

$sql_y = "select DISTINCT year(posaddress_store_openingdate) as fyear " . 
         "from posaddresses " . 
		 "where year(posaddress_store_openingdate) > 0 " . 
		 "order by year(posaddress_store_openingdate) DESC";
$res_y = mysql_query($sql_y) or dberror($sql_y);
while ($row_y = mysql_fetch_assoc($res_y))
{
	$from_opening_years[$row_y["fyear"]] = $row_y["fyear"];
	$to_opening_years[$row_y["fyear"]] = $row_y["fyear"];
	$operating_per_years[$row_y["fyear"]] = $row_y["fyear"];
}


for($i=1;$i<13;$i++)
{
	$from_opening_months[$i] = $i;
	$to_opening_months[$i] = $i;
	$operating_per_months[$i] = $i;

	$from_closing_months[$i] = $i;
	$to_closing_months[$i] = $i;
}


//get actual opening dates
$from_closed_years = array();
$from_closed_months = array();
$to_closed_years = array();
$to_closed_months = array();
$sql_y = "select DISTINCT year(posaddress_store_closingdate) as fyear " . 
         "from posaddresses " . 
		 "where year(posaddress_store_closingdate) > 0 " . 
		 "order by year(posaddress_store_closingdate) DESC";
$res_y = mysql_query($sql_y) or dberror($sql_y);
while ($row_y = mysql_fetch_assoc($res_y))
{
	$from_closed_years[$row_y["fyear"]] = $row_y["fyear"];
	$to_closed_years[$row_y["fyear"]] = $row_y["fyear"];
}


for($i=1;$i<13;$i++)
{
	$from_closed_months[$i] = $i;
	$to_closed_months[$i] = $i;
}

//get benchmark parameters
$salesregions = array();
$regions = array();
$countries = array();
$provinces = array();
$cities = array();
$areas = array();
$product_lines = array();
$furniture_subclasses = array();
$pos_types = array();
$subclasses = array();
$project_kinds = array();
$project_cost_types = array();
$distribution_channels = array();
$design_objectives = array();

$from_closing_year = "";
$from_closing_month ="";
$to_closing_year = "";
$to_closing_month ="";

$from_opening_year ="";
$from_opening_month ="";
$to_opening_year ="";
$to_opening_month ="";


$from_planned_closed_year ="";
$from_planned_closed_month ="";
$to_planned_closed_year ="";
$to_planned_closed_month ="";


$from_closed_year ="";
$from_closed_month ="";
$to_closed_year ="";
$to_closed_month ="";


$operating_per_year ="";
$operating_per_month ="";

$client_type ="";
$address_type ="";

$www = "";

$latest_pos_lease = 0;
$pos_without_lease = 0;

//check if filter is present
$sql = "select posquery_fields, posquery_filter, posquery_area_perception from posqueries " .
	   "where posquery_id = " . $query_id;

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$fields_posaddresses = unserialize($row["posquery_fields"]);

	$filters = array();
	$filters = unserialize($row["posquery_filter"]);

	foreach($filters as $key=>$value)
	{
		if(array_key_exists("re", $filters))
		{
			$salesregions = explode("-", $filters["re"]);
		}
		if(array_key_exists("gr", $filters))
		{
			$regions = explode("-", $filters["gr"]);
		}
		if(array_key_exists("co", $filters))
		{
			$countries = explode("-", $filters["co"]);
		}
		if(array_key_exists("pr", $filters))
		{
			$provinces = explode("-", $filters["pr"]);
		}
		if(array_key_exists("ci", $filters))
		{
			$cities = explode("-", $filters["ci"]);
		}
		if(array_key_exists("ar", $filters))
		{
			$areas = explode("-", $filters["ar"]);
		}
		if(array_key_exists("pct", $filters))
		{
			$project_cost_types = explode("-", $filters["pct"]);
		}
		if(array_key_exists("pl", $filters))
		{
			$product_lines = explode("-", $filters["pl"]);
		}
		if(array_key_exists("fscs", $filters))
		{
			$furniture_subclasses = explode("-", $filters["fscs"]);
		}
		if(array_key_exists("pt", $filters))
		{
			$pos_types = explode("-", $filters["pt"]);
		}
		if(array_key_exists("sc", $filters))
		{
			$subclasses = explode("-", $filters["sc"]);
		}
		if(array_key_exists("pk", $filters))
		{
			$project_kinds = explode("-", $filters["pk"]);
		}

		
		if(array_key_exists("cl", $filters))
		{
			$client = $filters["cl"];

		}
		if(array_key_exists("at", $filters))
		{
			$address_type = $filters["at"];

		}
		if(array_key_exists("ct", $filters))
		{
			$client_type = $filters["ct"];

		}
		if(array_key_exists("fr", $filters))
		{
			$franchisee = $filters["fr"];

		}
		if(array_key_exists("lopr", $filters))
		{
			$lopr = $filters["lopr"];

		}
		
				
		if(array_key_exists("nopr", $filters))
		{
			$nopr = $filters["nopr"];

		}
		if(array_key_exists("incs", $filters))
		{
			$incs = $filters["incs"];
		}

		if(array_key_exists("exfp", $filters))
		{
			$exfp = $filters["exfp"];

		}
		else {
			$exfp = '';
		}
		
		
		
		
		if(array_key_exists("latest_pos_lease", $filters))
		{
			$latest_pos_lease = $filters["latest_pos_lease"];
		}

		if(array_key_exists("pos_without_lease", $filters))
		{
			$pos_without_lease = $filters["pos_without_lease"];
		}

		
		
		if(array_key_exists("www", $filters))
		{
			$www = $filters["www"];
		}

		if(array_key_exists("dcs", $filters))
		{
			$distribution_channels =  explode("-", $filters["dcs"]);
		}


		
		if(array_key_exists("fcy", $filters))
		{
			$from_closing_year =  $filters["fcy"];
		}

		if(array_key_exists("fcm", $filters))
		{
			$from_closing_month =  $filters["fcm"];
		}

		if(array_key_exists("tcy", $filters))
		{
			$to_closing_year =  $filters["tcy"];
		}

		if(array_key_exists("tcm", $filters))
		{
			$to_closing_month =  $filters["tcm"];
		}


		if(array_key_exists("aofy", $filters))
		{
			$from_opening_year =  $filters["aofy"];
		}

		if(array_key_exists("aofm", $filters))
		{
			$from_opening_month =  $filters["aofm"];
		}

		if(array_key_exists("aoty", $filters))
		{
			$to_opening_year =  $filters["aoty"];
		}

		if(array_key_exists("aotm", $filters))
		{
			$to_opening_month =  $filters["aotm"];
		}


		

		if(array_key_exists("clfy", $filters))
		{
			$from_closed_year =  $filters["clfy"];
		}

		if(array_key_exists("clfm", $filters))
		{
			$from_closed_month =  $filters["clfm"];
		}

		if(array_key_exists("clty", $filters))
		{
			$to_closed_year =  $filters["clty"];
		}

		if(array_key_exists("cltm", $filters))
		{
			$to_closed_month =  $filters["cltm"];
		}

		if(array_key_exists("dos", $filters))
		{
			$design_objectives = explode("-", $filters["dos"]);
		}

		

		if(array_key_exists("opy", $filters))
		{
			$operating_per_year =  $filters["opy"];
		}
		if(array_key_exists("opm", $filters))
		{
			$operating_per_month =  $filters["opm"];
		}

		
	}
}


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("posqueries", "query_generator");

$form->add_hidden("query_id", param("query_id"));
$form->add_section($posquery["name"]);

$form->add_section(" ");
$form->add_comment("Please select the the filter criteria of your query.");

$link = "javascript:open_selector('')";



$form->add_list("client_id", "Client",	$sql_clients, SUBMIT, $client);
$form->add_list("address_type", "Owner Address Type",	$sql_address_types, 0, $address_type);
$form->add_list("client_type", "Client Type",	$sql_client_types, 0, $client_type);
$form->add_list("franchisee_id", "Owner Company/Franchisee",	$sql_franchisees, 0, $franchisee);
$form->add_checkbox("LOPR", "Show only POS with locally produced projects", $lopr, 0, "Local Production");
$form->add_checkbox("NOPR", "Show only POS with no projects assigned", $nopr, 0, "Missing Projects");
$form->add_checkbox("INCS", "Include closed POS Locations", $incs, 0, "Closed POS");
$form->add_checkbox("WWW", "Show only POS visible in the store locator", $www, 0, "Store Locator");
$form->add_checkbox("exfp", "Exclude Fake Projects", $exfp, 0, "Fake Projects");


$form->add_section("Lease Parameters");
$form->add_checkbox("latest_pos_lease", "Show only the latest lease contract", $latest_pos_lease, 0, "Leases");
$form->add_checkbox("pos_without_lease", "Show only POS without lease", $pos_without_lease, 0, "Leases");

$form->add_section("Actual Opening Date");
$form->add_list("aofy", "From",	$from_opening_years, 0, $from_opening_year);
$form->add_list("aofm", "",	$from_opening_months, 0, $from_opening_month);
$form->add_list("aoty", "To",	$to_opening_years, 0, $to_opening_year);
$form->add_list("aotm", "",	$to_opening_months, 0, $to_opening_month);


$form->add_section("POS Closing Date");
$form->add_list("clfy", "From",	$from_closed_years, 0, $from_closed_year);
$form->add_list("clfm", "",	$from_closed_months, 0, $from_closed_month);
$form->add_list("clty", "To",	$to_closed_years, 0, $to_closed_year);
$form->add_list("cltm", "",	$to_closed_months, 0, $to_closed_month);


$form->add_section("Operating per");
$form->add_list("opy", "as per",	$operating_per_years, 0, $operating_per_year);
$form->add_list("opm", "",	$operating_per_months, 0, $operating_per_month);

$form->add_section("Temporary Closures");
$form->add_list("fcy", "From",	$from_closing_years, 0, $from_closing_year);
$form->add_list("fcm", "",	$from_closing_months, 0, $from_closing_month);
$form->add_list("tcy", "To",	$to_closing_years, 0, $to_closing_year);
$form->add_list("tcm", "",	$to_closing_months, 0, $to_closing_month);


$form->add_Section("Selected Filter Criteria");

$selected_salesregions = "";
if(count($salesregions) > 0)
{
	$res = mysql_query($sql_salesregion) or dberror($sql_salesregion);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["salesregion_id"], $salesregions))
		{
			$selected_salesregions .= $row["salesregion_name"] . ", ";
		}
	}
	$selected_salesregions = substr($selected_salesregions, 0, strlen($selected_salesregions) - 2);
}

if(has_access("can_view_posindex") or has_access("can_view_his_posindex"))
{
	$form->add_label_selector("salesregions", "Geographical Regions", 0, $selected_salesregions, $icon, $link);
}
else
{
	$form->add_label("salesregions", "Geographical Regions", 0, $selected_salesregions);
	
}

$selected_regions = "";
if(count($regions) > 0)
{
	$res = mysql_query($sql_region) or dberror($sql_region);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["region_id"], $regions))
		{
			$selected_regions .= $row["region_name"] . ", ";
		}
	}
	$selected_regions = substr($selected_regions, 0, strlen($selected_regions) - 2);
}

if(has_access("can_view_posindex") or has_access("can_view_his_posindex"))
{
	$form->add_label_selector("regions", "Supplied Regions", 0, $selected_regions, $icon, $link);
}
else
{
	$form->add_label("regions", "Supplied Regions", 0, $selected_regions);
	
}


$selected_countries = "";
if(count($countries) > 0)
{
	$res = mysql_query($sql_country) or dberror($sql_country);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["country_id"], $countries))
		{
			$selected_countries .= $row["country_name"] . ", ";
		}
	}
	$selected_countries = substr($selected_countries, 0, strlen($selected_countries) - 2);
}
if(has_access("can_view_posindex") or has_access("can_view_his_posindex"))
{
	$form->add_label_selector("countries", "Countries", 0, $selected_countries, $icon, $link);
}
else
{
	$form->add_label("countries", "Countries", 0, $selected_countries);	
}


$selected_provinces = "";
if(count($provinces) > 0)
{
	$res = mysql_query($sql_provinces) or dberror($sql_provinces);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["province_id"], $provinces))
		{
			$selected_provinces .= $row["province_canton"] . ", ";
		}
	}
	$selected_provinces = substr($selected_provinces, 0, strlen($selected_provinces) - 2);
}
if(has_access("can_view_posindex") or has_access("can_view_his_posindex"))
{
	$form->add_label_selector("provinces", "Provinces", 0, $selected_provinces, $icon, $link);
}
else
{
	$form->add_label("provinces", "Provinces", 0, $selected_provinces);
}


$selected_cities = "";
if(count($cities) > 0)
{
	$res = mysql_query($sql_cities) or dberror($sql_cities);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array(trim($row["posaddress_place"]), $cities))
		{
			$selected_cities .= $row["posaddress_place"] . ", ";
		}
	}
	$selected_cities = substr($selected_cities, 0, strlen($selected_cities) - 2);
}
if(has_access("can_view_posindex") or has_access("can_view_his_posindex"))
{
	$form->add_label_selector("cities", "Cities", 0, $selected_cities, $icon, $link);
}
else
{
	$form->add_label("cities", "Provinces", 0, $selected_cities);
}


$selected_areas = "";
if(count($areas) > 0)
{
	$res = mysql_query($sql_areas) or dberror($sql_areas);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["posareatype_id"], $areas))
		{
			$selected_areas .= $row["posareatype_name"] . ", ";
		}
	}
	$selected_areas = substr($selected_areas, 0, strlen($selected_areas) - 2);
}
$form->add_label_selector("areas", "Neighbourhood Areas", 0, $selected_areas, $icon, $link);


$selected_pcts = "";
if(count($project_cost_types) > 0)
{
	$res = mysql_query($sql_project_cost_types) or dberror($sql_project_cost_types);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["project_costtype_id"], $project_cost_types))
		{
			$selected_pcts .= $row["project_costtype_text"] . ", ";
		}
	}
	$selected_pcts = substr($selected_pcts, 0, strlen($selected_pcts) - 2);
}
$form->add_label_selector("pcts", "Legal Types", 0, $selected_pcts, $icon, $link);


$selected_pls = "";
if(count($product_lines) > 0)
{
	$res = mysql_query($sql_product_lines) or dberror($sql_product_lines);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["product_line_id"], $product_lines))
		{
			$selected_pls .= $row["product_line_name"] . ", ";
		}
	}
	$selected_pls = substr($selected_pls, 0, strlen($selected_pls) - 2);
}
$form->add_label_selector("pls", "Furniture Types", 0, $selected_pls, $icon, $link);


$selected_fscs = "";
if(count($furniture_subclasses) > 0)
{
	$res = mysql_query($sql_furniture_subclasses) or dberror($sql_furniture_subclasses);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["productline_subclass_id"], $furniture_subclasses))
		{
			$selected_fscs .= $row["productline_subclass_name"] . ", ";
		}
	}
	$selected_fscs = substr($selected_fscs, 0, strlen($selected_fscs) - 2);
}
$form->add_label_selector("fscs", "Furniture Subclasses", 0, $selected_fscs, $icon, $link);



$selected_pts = "";
if(count($pos_types) > 0)
{
	$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["postype_name"], $pos_types))
		{
			$selected_pts .= $row["postype_name"] . ", ";
		}
	}
	$selected_pts = substr($selected_pts, 0, strlen($selected_pts) - 2);
}
$form->add_label_selector("pts", "POS Types", 0, $selected_pts, $icon, $link);

$selected_subclasses = "";
if(count($subclasses) > 0)
{
	$res = mysql_query($sql_possubclasses) or dberror($sql_possubclasses);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["possubclass_id"], $subclasses))
		{
			$selected_subclasses .= $row["possubclass_name"] . ", ";
		}
	}
	$selected_subclasses = substr($selected_subclasses, 0, strlen($selected_subclasses) - 2);
}
$form->add_label_selector("subclasses", "POS Type Subclasses", 0, $selected_subclasses, $icon, $link);


$selected_pks = "";
if(count($project_kinds) > 0)
{
	$res = mysql_query($sql_project_kinds) or dberror($sql_project_kinds);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["projectkind_id"], $project_kinds))
		{
			$selected_pks .= $row["projectkind_name"] . ", ";
		}
	}
	$selected_pks = substr($selected_pks, 0, strlen($selected_pks) - 2);
}
$form->add_label_selector("projectkinds", "Project Types", 0, $selected_pks, $icon, $link);


$selected_dcs = "";
if(count($distribution_channels) > 0)
{
	$res = mysql_query($sql_distribution_channels) or dberror($sql_distribution_channels);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["mps_distchannel_id"], $distribution_channels))
		{
			$selected_dcs .= $row["distributionchannel"] . ", ";
		}
	}
	$selected_dcs = substr($selected_dcs, 0, strlen($selected_dcs) - 2);
}
$form->add_label_selector("distributionchannel", "Distribution Channels", 0, $selected_dcs, $icon, $link);


$selected_design_objectives = "";
if(count($design_objectives) > 0)
{
	$res = mysql_query($sql_design_objectives) or dberror($sql_design_objectives);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["design_objective_item_id"], $design_objectives))
		{
			$selected_design_objectives .= $row["postype_name"] . " - " . $row["design_objective_group_name"] . " - " . $row["design_objective_item_name"] . ", ";
		}
	}
	$selected_design_objectives = substr($selected_design_objectives, 0, strlen($selected_design_objectives) - 2);
}
$form->add_label_selector("design_objectives", "Design Objectives", 0, $selected_design_objectives, $icon, $link);


$form->add_button("save", "Save Filter");

if(check_if_query_has_fields($query_id) == true)
{
	$form->add_button("execute", "Execute Query");
}
$form->add_button("back", "Back to the List of Queries");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Process buttons
*********************************************************************/ 
if($form->button("save"))
{
	

	$new_filter = array();
	$new_filter["re"] =  $filter["re"];
	$new_filter["gr"] =  $filter["gr"];
	$new_filter["co"] =  $filter["co"];
	$new_filter["pr"] =  $filter["pr"];
	$new_filter["ci"] =  $filter["ci"];
	$new_filter["ar"] =  $filter["ar"];

	$new_filter["pct"] =  $filter["pct"];
	$new_filter["pl"] =  $filter["pl"];
	$new_filter["pt"] =  $filter["pt"];
	$new_filter["sc"] =  $filter["sc"];
	$new_filter["pk"] =  $filter["pk"];
	$new_filter["dcs"] =  $filter["dcs"];
	$new_filter["dos"] =  $filter["dos"];

	
	
	$new_filter["cl"] = $form->value("client_id");
	$new_filter["fr"] = $form->value("franchisee_id");
	$new_filter["lopr"] = $form->value("LOPR");
	$new_filter["nopr"] = $form->value("NOPR");
	$new_filter["incs"] = $form->value("INCS");
	$new_filter["exfp"] = $form->value("exfp");

	
	$new_filter["www"] = $form->value("WWW");

	$new_filter["latest_pos_lease"] = $form->value("latest_pos_lease");
	$new_filter["pos_without_lease"] = $form->value("pos_without_lease");
	

	$new_filter["fcy"] = $form->value("fcy");
	$new_filter["fcm"] = $form->value("fcm");
	$new_filter["tcy"] = $form->value("tcy");
	$new_filter["tcm"] = $form->value("tcm");

	$new_filter["aofy"] = $form->value("aofy");
	$new_filter["aofm"] = $form->value("aofm");
	$new_filter["aoty"] = $form->value("aoty");
	$new_filter["aotm"] = $form->value("aotm");


	$new_filter["clfy"] = $form->value("clfy");
	$new_filter["clfm"] = $form->value("clfm");
	$new_filter["clty"] = $form->value("clty");
	$new_filter["cltm"] = $form->value("cltm");


	$new_filter["ct"] = $form->value("client_type");
	$new_filter["at"] = $form->value("address_type");

	$new_filter["opy"] = $form->value("opy");
	$new_filter["opm"] = $form->value("opm");
	

	$sql = "update posqueries " . 
		   "set posquery_filter = '" . serialize($new_filter)  . "', " . 
		   "date_modified = " . dbquote(date("Y-m-d H:s:i")) . ", " . 
		    "user_modified = " . dbquote(user_login()) .
		   " where posquery_id = " . param("query_id");

	

	$result = mysql_query($sql) or dberror($sql);

	//echo $sql;abc();

	
}
elseif($form->button("back"))
{
	redirect("query_generator.php");
}
elseif($form->button("execute"))
{
	redirect("query_generator_xls.php?query_id=" . param("query_id"));
}

/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page("query_generator");
$page->header();
$page->title("Edit POS Query - Filter");

require_once("include/query_tabs.php");

$form->render();

?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#salesregions_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/pos/query_generator_filter_selector.php?query_id=<?php echo $query_id;?>&s=re'
    });
    return false;
  });
  $('#regions_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/pos/query_generator_filter_selector.php?query_id=<?php echo $query_id;?>&s=gr'
    });
    return false;
  });
  $('#provinces_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/pos/query_generator_filter_selector.php?query_id=<?php echo $query_id;?>&s=pr'
    });
    return false;
  });
  $('#countries_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/pos/query_generator_filter_selector.php?query_id=<?php echo $query_id;?>&s=co'
    });
    return false;
  });
  $('#cities_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/pos/query_generator_filter_selector.php?query_id=<?php echo $query_id;?>&s=ci'
    });
    return false;
  });
  $('#areas_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/pos/query_generator_filter_selector.php?query_id=<?php echo $query_id;?>&s=ar'
    });
    return false;
  });
  $('#pcts_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/pos/query_generator_filter_selector.php?query_id=<?php echo $query_id;?>&s=pct'
    });
    return false;
  });
  $('#pls_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/pos/query_generator_filter_selector.php?query_id=<?php echo $query_id;?>&s=pl'
    });
    return false;
  });
  $('#fscs_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/pos/query_generator_filter_selector.php?query_id=<?php echo $query_id;?>&s=fscs'
    });
    return false;
  });
  $('#pts_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/pos/query_generator_filter_selector.php?query_id=<?php echo $query_id;?>&s=pt'
    });
    return false;
  });
  $('#subclasses_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/pos/query_generator_filter_selector.php?query_id=<?php echo $query_id;?>&s=sc'
    });
    return false;
  });
  $('#projectkinds_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/pos/query_generator_filter_selector.php?query_id=<?php echo $query_id;?>&s=pk'
    });
    return false;
  });
  
   $('#distributionchannel_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/pos/query_generator_filter_selector.php?query_id=<?php echo $query_id;?>&s=dcs'
    });
    return false;
  });
  $('#design_objectives_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/pos/query_generator_filter_selector.php?query_id=<?php echo $query_id;?>&s=dos'
    });
    return false;
  });
  $('#customer_services_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/pos/query_generator_filter_selector.php?query_id=<?php echo $query_id;?>&s=cs'
    });
    return false;
  });



  
});
</script>


<?php
$page->footer();
?>