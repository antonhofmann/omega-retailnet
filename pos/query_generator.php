<?php
/********************************************************************

    query_generator.php

    Lists all POS Queries.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2009-01-11
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2009-01-11
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_query_posindex");
set_referer("query_generator_query.php");

require_once "include/query_get_functions.php";


//create a copy of an existing query
if(param("copy_id"))
{
	$sql = "select * from posqueries where posquery_id = " . param("copy_id");
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$sql_i = "Insert into posqueries (" . 
			     "posquery_owner, " . 
			     "posquery_name, " . 
			     "posquery_print_filter, " . 
			     "posquery_leading_entity, " . 
			     "posquery_fields, " . 
			     "posquery_field_order, " . 
			     "posquery_filter, " . 
			     "posquery_order, " . 
			     "posquery_grouping01, " . 
			     "posquery_grouping02, " . 
			     "posquery_fieldstosumup, " . 
			     "posquery_area_perception, " . 
			     "posquery_countrecords, " . 
			     "user_created, " . 
			     "date_created) VALUES (" . 
			     user_id() . ", " .
				 dbquote($row["posquery_name"] .  " - copy") . ", " . 
			     dbquote($row["posquery_print_filter"]) . ", " . 
			     dbquote($row["posquery_leading_entity"]) . ", " . 
			     dbquote($row["posquery_fields"]) . ", " . 
			     dbquote($row["posquery_field_order"]) . ", " . 
			     dbquote($row["posquery_filter"]) . ", " . 
			     dbquote($row["posquery_order"]) . ", " . 
			     dbquote($row["posquery_grouping01"]) . ", " . 
			     dbquote($row["posquery_grouping02"]) . ", " . 
			     dbquote($row["posquery_fieldstosumup"]) . ", " . 
			     dbquote($row["posquery_area_perception"]) . ", " . 
			     dbquote($row["posquery_countrecords"]) . ", " . 
			     dbquote(user_login()) . ", " .
			     "CURRENT_TIMESTAMP)";
		$result = mysql_query($sql_i);
	}


}
elseif(param("remove_id"))
{
	$sql_d = "delete from posquery_permissions " . 
		   " where posquery_permission_query = " . param("remove_id") . 
		   " and posquery_permission_user = " . user_id();
	$result = mysql_query($sql_d);

}


//build query for the list

$sql = "select posquery_id, posquery_group, posquery_name, posqueryentity_name, posquery_fields, posquery_owner, " .
       "concat(user_name, ' ', user_firstname) as uname, posqueries.date_created as cdate, posqueries.date_modified as mdate " . 
       "from posqueries " .
	   "left join posqueryentities on posqueryentity_id = posquery_leading_entity " . 
	   "left join users on user_id = posquery_owner ";

$list_filter = "posquery_owner = " . user_id();

$list_filter2 = $list_filter;


if(param('group_filter')) {
	$list_filter .= " and posquery_group  = " . dbquote(param('group_filter')) . " ";
}




$query_permissions = array();
$queries = array();
$query_links = array();
$copy_links = array();
$delete_links = array();


//$sql_a = $sql . " where posquery_owner = " . user_id();

$sql_p = $sql;

if(param('group_filter')) {
	$sql_p .= " where posquery_group  = " . dbquote(param('group_filter')) . " ";
}

$res = mysql_query($sql_p) or dberror($sql_p);

while ($row = mysql_fetch_assoc($res))
{
	if(check_if_query_has_fields($row["posquery_id"]) == true)
	{
		$link = "<a href='query_generator_xls.php?query_id=" .  $row["posquery_id"] . "'><img src=\"/pictures/ico_xls.gif\" border='0'/></a>";
		$queries[$row["posquery_id"]] = $link;
	}
	else
	{
		$queries[$row["posquery_id"]] = "<img src=\"/pictures/wf_warning.gif\" border='0'/>";
	}

	if(user_id() == $row["posquery_owner"])
	{
		$query_links[$row["posquery_id"]] = "<a href='query_generator_query.php?id=" .  $row["posquery_id"] . "'>" . $row["posquery_name"] . "</a>";
	}
	else
	{
		$query_links[$row["posquery_id"]] = $row["posquery_name"];
		
		$delete_links[$row["posquery_id"]] = '<a title="Remove query from my list" href="query_generator.php?remove_id=' .  $row["posquery_id"] . '"><img src="/pictures/remove.gif" border="0" style="margin-top:3px;" alet="Remove"/></a>';
	}

	$copy_links[$row["posquery_id"]] = "<a href='query_generator.php?copy_id=" .  $row["posquery_id"] . "'>create copy</a>";

	//get permissions
	
	$sql_p = "select * from posquery_permissions " . 
			 "where posquery_permission_query = " . $row["posquery_id"];

	$res_p = mysql_query($sql_p) or dberror($sql_p);
	while ($row_p = mysql_fetch_assoc($res_p))
	{
		if($row_p["posquery_permission_user"] == user_id())
		{
			$query_permissions[$row_p["posquery_permission_query"]] = $row_p["posquery_permission_query"];
		}
	}
}

if(count($query_permissions) > 0)
{
	$permission_filter =  implode(',', $query_permissions);
	$permission_filter = " or posquery_id in (" . $permission_filter . ") ";
	$list_filter .= $permission_filter;
}


//compose group filter
$group_filter = array();

$sql_g = "select DISTINCT posquery_group, posquery_group " .
                "from posqueries " .
			    "left join posqueryentities on posqueryentity_id = posquery_leading_entity " . 
			    "left join users on user_id = posquery_owner " .
				" where posquery_group is not NULL " . 
				" and posquery_group <> '' " . 
				" and (" . $list_filter2 . ") " . 
				" order by posquery_group ";


$res = mysql_query($sql_g) or dberror($sql_g);

while ($row = mysql_fetch_assoc($res))
{
	$group_filter[$row["posquery_group"]] = $row["posquery_group"];
}





$list = new ListView($sql);

$list->set_entity("posqueries");
$list->set_order("posquery_group, posquery_name");
$list->set_filter($list_filter);
$list->set_group("posquery_group");

if(count($group_filter) > 0)
{
	$list->add_listfilters("group_filter", "Group", 'select', $group_filter, param("group_filter"));
}
$list->add_text_column("queries", " ", COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $queries);
$list->add_text_column("querylinks", "Query Name", COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $query_links);
$list->add_column("uname", "Owner", "");

$list->add_column("cdate", "Created", "");
$list->add_column("mdate", "Modified", "");


$list->add_text_column("querylinks", "", COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $copy_links);
$list->add_text_column("removelinks", "", COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $delete_links);

$list->add_button(LIST_BUTTON_NEW, "New Query", "query_generator_query.php");

$list->process();

$page = new Page("query_generator");

$page->header();
$page->title("POS Queries");
$list->render();
$page->footer();

?>
