<?php
/********************************************************************

    posindex_pos_delete.php

    Delete POS DATA from DATABASE.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

check_access("can_use_posindex");

set_referer("posprojects.php");

if(param("pos_id") > 0)
{
	$pos = get_poslocation(param("pos_id"), "posaddresses");
}


if(param("d") and param("d") == 1)
{

	$sql = "delete from posareas where posarea_posaddress = " . param("pos_id");
	$result = mysql_query($sql) or dberror($sql);

	$sql = "delete from posequipments where posequipment_posaddress = " . param("pos_id");
	$result = mysql_query($sql) or dberror($sql);

	$sql = "delete from posfiles where posfile_posaddress = " . param("pos_id");
	$result = mysql_query($sql) or dberror($sql);

	$sql = "delete from posinvestments where posinvestment_posaddress = " . param("pos_id");
	$result = mysql_query($sql) or dberror($sql);

	$sql = "delete from posleases where poslease_posaddress = " . param("pos_id");
	$result = mysql_query($sql) or dberror($sql);

	$sql = "select * from posorders where posorder_posaddress = " . param("pos_id");
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$sql = "delete from posorderinvestments where posorderinvestment_posorder = " .$row["posorder_id"];
		$result = mysql_query($sql) or dberror($sql);
	
	}

	$sql = "delete from posorders where posorder_posaddress = " . param("pos_id");
	$result = mysql_query($sql) or dberror($sql);

	$sql = "delete from posaddresses where posaddress_id = " . param("pos_id");
	$result = mysql_query($sql) or dberror($sql);

	$sql = "delete from posaddresses where posaddress_id = " . param("pos_id");
	$result = mysql_query($sql) or dberror($sql);
	
	$sql = "delete from posaddresses where posaddress_id = " . param("pos_id");
	$result = mysql_query($sql) or dberror($sql);


	$db2 = mysql_connect(STORE_LOCATOR_SERVER, STORE_LOCATOR_USER, STORE_LOCATOR_PASSWORD);
	$dbname2 = STORE_LOCATOR_DB;
	mysql_query( "SET NAMES 'utf8'");
	mysql_select_db($dbname2, $db2);
	$sql = "delete from posaddresses where posaddress_id = " . param("pos_id");
	$result = mysql_query($sql) or dberror($sql);


	$db = mysql_pconnect(RETAILNET_SERVER, RETAILNET_USER, RETAILNET_PASSWORD);
	$dbname = RETAILNET_DB;
	mysql_query( "SET NAMES 'utf8'");
	mysql_select_db($dbname, $db);

	redirect("posindex.php?country=" . param("country"));
}

// Build form
$form = new Form("posaddresses", "posaddress");

$form->add_section("Address Data");
$form->add_hidden("pos_id", param("pos_id"));
$form->add_hidden("country", param("country"));
$form->add_label("posaddress_name", "POS Name", 0, $pos["posaddress_name"]);
$form->add_label("posaddress_address", "Address", 0, $pos["posaddress_address"]);
$form->add_label("posaddress_address2", "Address 2", 0, $pos["posaddress_address2"]);
$form->add_label("posaddress_zip", "Zip", 0, $pos["posaddress_zip"]);
$form->add_label("posaddress_place", "City", 0, $pos["posaddress_place"]);
$form->add_lookup("posaddress_country", "Country", "countries", "country_name", 0, $pos["posaddress_country"]);


if(has_access("can_edit_catalog"))
{
	$form->add_button("delete", "YES, delete POS from POS List");
	$form->error("All data of the following POS will be deleted.");
}

$form->add_button("back", "NO, go back to POS Data");


// Populate form and process button clicks

$form->populate();
$form->process();

if($form->button("back"))
{
	redirect("posindex_pos.php?pos_id=" .param("pos_id") . "&country=" . param("country") . '&let=' . param('let') . "&ltf=" . param("ltf"));
}
elseif($form->button("delete"))
{
	redirect("posindex_pos_delete.php?pos_id=" . param("pos_id") . "&country=" . param("country") . "&d=1"  . '&let=' . param('let') . "&ltf=" . param("ltf"));	
}

// Render page
$poslocation = get_poslocation(id(), "posaddresses");

$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title(id() ? "DELETE: " . $poslocation["posaddress_name"] : "Add POS Location");

$form->render();
$page->footer();

?>