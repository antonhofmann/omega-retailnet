<?php
/********************************************************************

    posorders.php

    Entry page for the orders section.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

check_access("can_use_posindex");

$pos = array();
if(param("pos_id") > 0)
{
	$pos = get_poslocation(param("pos_id"), "posaddresses");
}

/********************************************************************
    Header
*********************************************************************/
$form = new Form("posaddresses", "posaddress");
require_once("include/poslocation_head.php");
$form->add_hidden("country", param("country"));
$form->add_hidden("ltf", param("ltf"));
$form->add_hidden("let", param("let"));
$form->add_hidden("ostate", param("ostate"));

$form->populate();
$form->process();


/********************************************************************
    prepare all data needed
*********************************************************************/
register_param("pos_id");


// create sql for the list of orders
$sql = "select distinct posorder_id, order_id, order_number, order_actual_order_state_code, order_archive_date, order_number, left(orders.date_created, 10) " .
       "from posorders " .
       "left join orders on order_id = posorder_order ";

$list_filter = "order_type = 2 and posorder_posaddress = " .param("pos_id");

$infolinks = array();
$sql_p = $sql . " where " . $list_filter;
$res_p = mysql_query($sql_p) or dberror($sql_p);

while ($row_p = mysql_fetch_assoc($res_p))
{
	if($row_p["order_id"] > 0)
	{
		if($row_p["order_archive_date"] == NULL or $row_p["order_archive_date"] == "0000-00-00")
		{
			$link = "<a href=\"/archive/order_view_client_data.php?oid=" .  $row_p["order_id"] . "\" target=\"_blank\"><img src=\"/pictures/info.gif\" border='0'/></a>";
		}
		else
		{
			$link = "<a href=\"/user/order_view_client_data.php?oid=" .  $row_p["order_id"] . "\" target=\"_blank\"><img src=\"/pictures/info.gif\" border='0'/></a>";
		}
			
		$infolinks[$row_p["posorder_id"]] = $link;
	}
}


/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);
$list->set_entity("posorders");
$list->set_order("orders.date_created desc");
$list->set_filter($list_filter);  

$link = "posorder.php?pos_id=" . param("pos_id") . "&country=" . param("country") . '&let=' . param('let') . "&ltf=" . param("ltf") . "&ostate=" . param("ostate");

$list->add_hidden("pos_id", param("pos_id"));

//$list->add_column("order_number", "Order Number", "order_task_center.php?oid={order_id}", LIST_FILTER_NONE, "", COLUMN_NO_WRAP);
$list->add_column("order_number", "Order Number", $link, LIST_FILTER_NONE, "", COLUMN_NO_WRAP);
if(has_access("can_view_orders"))
{
	$list->add_text_column("info", "", COLUMN_UNDERSTAND_HTML, $infolinks);
}
$list->add_column("left(orders.date_created, 10)", "Submitted", "", LIST_FILTER_NONE, "", COLUMN_NO_WRAP);
$list->add_column("order_actual_order_state_code", "Status");

$list->add_button("back", "Back to POS List");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->process();

if($list->button("back"))
{
	redirect("posindex.php?country=" . param("country") . '&let=' . param('let') . "&ltf=" . param("ltf"). '&ostate=' . param("ostate"));
}

$page = new Page("posindex");
require "include/pos_page_actions.php";
$page->header();
$page->title('Catalog Orders: '. $poslocation["posaddress_name"]);

require_once("include/tabs.php");

$form->render();
$list->render();
$page->footer();

?>