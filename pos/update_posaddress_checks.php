<?php
/********************************************************************

	update_posaddress_checks.php

    update all new additional posaddresses to _posaddresses

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2009-02-01
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2009-02-01
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_edit_catalog");
set_referer("update_posaddress_checks.php");



if(param("country"))
{
	
	$sql_companies = "select * " . 
		             "from addresses where address_country = " . param("country") . 
			         " and address_active = 1 and address_showinposindex = 1 order by address_place, address_company";

	
	if(param("postype") > 0)
	{
		$sql_pos = "select * " .
				   "from posaddresses " . 
				   "where (posaddress_store_closingdate is null " .
				   " or posaddress_store_closingdate = '0000-00-00') " . 
				   " and posaddress_store_postype = " . param("postype") .
				   " and posaddress_country = " . param("country") . 
				   " order by posaddress_place, posaddress_name";
	}
	else
	{
		$sql_pos = "select * " .
				   "from posaddresses " . 
				   "where (posaddress_store_closingdate is null " .
				   " or posaddress_store_closingdate = '0000-00-00') " . 
				   " and posaddress_country = " . param("country") . 
				   " order by posaddress_place, posaddress_name";
	}
}


if(param("country") > 0 and param("execute_transfer") == 1)
{
	if(param("remove_companies") and param("remove_companies")> 0) 
	{
		if(param("address_id") and param("address_id")> 0) 
		{
			$sql_p = "Delete from _addresses where address_id = " . param("address_id");
		}
		else
		{
			$sql_p = "Delete from _addresses where address_country = " . param("country");
		}
		$result = mysql_query($sql_p);
	}

	if(param("remove_poslocations") and param("remove_poslocations")> 0) 
	{
		if(param("posaddress_id") and param("posaddress_id")> 0) 
		{
			$sql_p = "Delete from _posaddresses where posaddress_id = " . param("posaddress_id");
		}
		else
		{
			$sql_p = "Delete from _posaddresses where posaddress_country = " . param("country");
		}
		$result = mysql_query($sql_p);

	}
	
	//transfer one single company
	$transfer_companies = 0;
	if(param("POSCOMPANIES") and param("POSCOMPANIES") == 1) // transfer pos companies
	{
		$sql_p = "select * from addresses where address_country = " . param("country") . 
			     " and address_active = 1 and address_showinposindex = 1";
		$transfer_companies = 1;
		
	}
	else
	{
		$filter = "";
		$res = mysql_query($sql_companies);
		while ($row = mysql_fetch_assoc($res))
		{
			if(param("address_id_" . $row["address_id"]) == 1)
			{
				$filter .= $row["address_id"] . ",";
			}
		}
		
		if($filter)
		{
			$filter = " where address_id IN (" . substr($filter, 0, strlen($filter)-1) . ") ";
			$sql_p = "select * from addresses " . $filter;
			$transfer_companies = 1;
		}
	}
	
	if($transfer_companies == 1)
	{
		
		$res_p = mysql_query($sql_p);
		while ($row_p = mysql_fetch_assoc($res_p))
		{

			$sql = "select * from _addresses where address_id = " . $row_p["address_id"];
			
			$res = mysql_query($sql);
			if ($row = mysql_fetch_assoc($res)) // update
			{
				$fields = array();

				$value = dbquote($row_p["address_company"]);
				$fields[] = "address_company = " . $value;

				$value = dbquote($row_p["address_company2"]);
				$fields[] = "address_company2 = " . $value;

				$value = dbquote($row_p["address_address"]);
				$fields[] = "address_address = " . $value;

				$value = dbquote($row_p["address_address2"]);
				$fields[] = "address_address2 = " . $value;

				$value = dbquote($row_p["address_zip"]);
				$fields[] = "address_zip = " . $value;

				$value = dbquote($row_p["address_place"]);
				$fields[] = "address_place = " . $value;

				$value = dbquote($row_p["address_country"]);
				$fields[] = "address_country = " . $value;

				$value = dbquote($row_p["address_phone"]);
				$fields[] = "address_phone = " . $value;

				$value = dbquote($row_p["address_fax"]);
				$fields[] = "address_fax = " . $value;

				$value = dbquote($row_p["address_email"]);
				$fields[] = "address_email = " . $value;

				$value = dbquote($row_p["address_contact_name"]);
				$fields[] = "address_contact_name = " . $value;

				$value = dbquote($row_p["address_website"]);
				$fields[] = "address_website = " . $value;

				$value = dbquote($row_p["address_parent"]);
				$fields[] = "address_parent = " . $value;

				$value = dbquote($row_p["address_active"]);
				$fields[] = "address_active = " . $value;

				$value = dbquote($row_p["address_canbefranchisee"]);
				$fields[] = "address_canbefranchisee = " . $value;

				$fields[] = "address_checked = 0";

				$sql = "update _addresses set " . join(", ", $fields) . " where address_id = " . dbquote($row_p["address_id"]);
				mysql_query($sql) or dberror($sql);

			}
			else // insert
			{
				$fields = array();
				$values = array();
				
				//_posaddresses
				
				$values[] = dbquote($row_p["address_id"]);
				$fields[] = "address_id";

				$values[] = dbquote($row_p["address_company"]);
				$fields[] = "address_company";

				$values[] = dbquote($row_p["address_company2"]);
				$fields[] = "address_company2";

				$values[] = dbquote($row_p["address_address"]);
				$fields[] = "address_address";

				$values[] = dbquote($row_p["address_address2"]);
				$fields[] = "address_address2";

				$values[] = dbquote($row_p["address_zip"]);
				$fields[] = "address_zip";

				$values[] = dbquote($row_p["address_place"]);
				$fields[] = "address_place";

				$values[] = dbquote($row_p["address_country"]);
				$fields[] = "address_country";

				$values[] = dbquote($row_p["address_phone"]);
				$fields[] = "address_phone";

				$values[] = dbquote($row_p["address_fax"]);
				$fields[] = "address_fax";

				$values[] = dbquote($row_p["address_email"]);
				$fields[] = "address_email";

				$values[] = dbquote($row_p["address_contact_name"]);
				$fields[] = "address_contact_name";

				$values[] = dbquote($row_p["address_website"]);
				$fields[] = "address_website";

				$values[] = dbquote($row_p["address_canbefranchisee"]);
				$fields[] = "address_canbefranchisee";

				$values[] = dbquote($row_p["address_active"]);
				$fields[] = "address_active";

				$values[] = dbquote($row_p["address_parent"]);
				$fields[] = "address_parent";

				$values[] = "0";
				$fields[] = "address_checked";
				
				$sql = "insert into _addresses (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);

			}

		}
	}

	//transfer pos locations
	$transfer_poslocations = 0;
	if(param("POSLOCATIONS") and param("POSLOCATIONS") == 1) // transfer pos locations
	{
		if(param("postype") and param("postype") > 0)
		{
			$sql_p = "select * from posaddresses " . 
					 "where (posaddress_store_closingdate is null " .
					 "or posaddress_store_closingdate = '0000-00-00') " . 
					 "and posaddress_store_postype = " . param("postype") .
					 " and posaddress_country = " . param("country");
			$transfer_poslocations = 1;
		}
		else
		{
			$sql_p = "select * from posaddresses " . 
					 "where (posaddress_store_closingdate is null " .
					 "or posaddress_store_closingdate = '0000-00-00') " . 
					 "and posaddress_country = " . param("country");
			$transfer_poslocations = 1;
		}
	}
	else
	{
		$filter = "";
		$res = mysql_query($sql_pos);
		while ($row = mysql_fetch_assoc($res))
		{
			if(param("posaddress_id_" . $row["posaddress_id"]) == 1)
			{
				$filter .= $row["posaddress_id"] . ",";
			}
		}
		
		if($filter)
		{
			if(param("postype") and param("postype") > 0)
			{
				$filter = " and posaddress_id IN (" . substr($filter, 0, strlen($filter)-1) . ") ";
				$filter = "where (posaddress_store_closingdate is null " .
						  "or posaddress_store_closingdate = '0000-00-00') " . 
					      "and posaddress_store_postype = " . param("postype") .
					      " and posaddress_country = " . param("country") . 
					      " " . $filter;
			
				$sql_p = "select * from posaddresses " . $filter;
				$transfer_poslocations = 1;
			}
			else
			{
				$filter = " and posaddress_id IN (" . substr($filter, 0, strlen($filter)-1) . ") ";
				$filter = "where (posaddress_store_closingdate is null " .
						  "or posaddress_store_closingdate = '0000-00-00') " . 
					      " and posaddress_country = " . param("country") . 
					      " " . $filter;
			
				$sql_p = "select * from posaddresses " . $filter;
				$transfer_poslocations = 1;
			}
		}
	}
	

	if($transfer_poslocations == 1)
	{
		$res_p = mysql_query($sql_p);
		while ($row_p = mysql_fetch_assoc($res_p))
		{
			$sql = "select * from _posaddresses where posaddress_id = " . $row_p["posaddress_id"];
			$res = mysql_query($sql);
			if ($row = mysql_fetch_assoc($res)) // update records
			{

				$sql = "select * from posaddresses where posaddress_id = " . $row_p["posaddress_id"];
				$res = mysql_query($sql);
				if ($row = mysql_fetch_assoc($res))
				{
				
					//update posaddress
					$fields = array();
					if($row["posaddress_franchisee_id"])
					{
						$value = dbquote($row["posaddress_franchisee_id"]);
						$fields[] = "posaddress_franchisee_id = " . $value;
					}

					$value = dbquote($row["posaddress_ownertype"]);
					$fields[] = "posaddress_ownertype = " . $value;

					$value = dbquote($row["posaddress_name"]);
					$fields[] = "posaddress_name = " . $value;

					$value = dbquote($row["posaddress_address"]);
					$fields[] = "posaddress_address = " . $value;

					$value = dbquote($row["posaddress_address2"]);
					$fields[] = "posaddress_address2 = " . $value;

					$value = dbquote($row["posaddress_zip"]);
					$fields[] = "posaddress_zip = " . $value;

					$value =  dbquote($row["posaddress_place"]);
					$fields[] = "posaddress_place = " . $value;

					$value =  dbquote($row["posaddress_place_id"]);
					$fields[] = "posaddress_place_id = " . $value;

					$value = dbquote($row["posaddress_country"]);
					$fields[] = "posaddress_country = " . $value;
					
					$value = dbquote($row["posaddress_phone"]);
					$fields[] = "posaddress_phone = " . $value;
					
					$value = dbquote($row["posaddress_fax"]);
					$fields[] = "posaddress_fax = " . $value;
					
					$value = dbquote($row["posaddress_email"]);
					$fields[] = "posaddress_email = " . $value;

					$value = dbquote($row["posaddress_perc_class"]);
					$fields[] = "posaddress_perc_class = " . $value;

					$value = dbquote($row["posaddress_perc_tourist"]);
					$fields[] = "posaddress_perc_tourist = " . $value;

					$value = dbquote($row["posaddress_perc_transport"]);
					$fields[] = "posaddress_perc_transport = " . $value;

					$value =  dbquote($row["posaddress_perc_people"]);
					$fields[] = "posaddress_perc_people = " . $value;

					$value = dbquote($row["posaddress_perc_parking"]);
					$fields[] = "posaddress_perc_parking = " . $value;

					$value = dbquote($row["posaddress_perc_visibility1"]);
					$fields[] = "posaddress_perc_visibility1 = " . $value;

					$value = dbquote($row["posaddress_perc_visibility2"]);
					$fields[] = "posaddress_perc_visibility2 = " . $value;


					$value =  dbquote($row["posaddress_store_postype"]);
					$fields[] = "posaddress_store_postype = " . $value;


					$value =  dbquote($row["posaddress_store_subclass"]);
					$fields[] = "posaddress_store_subclass = " . $value;


					$value =  dbquote($row["posaddress_store_furniture"]);
					$fields[] = "posaddress_store_furniture = " . $value;

					$value =  dbquote($row["posaddress_store_furniture_subclass"]);
					$fields[] = "posaddress_store_furniture_subclass = " . $value;

					$value = dbquote($row["posaddress_store_grosssurface"]);
					$fields[] = "posaddress_store_grosssurface = " . $value;

					$value = dbquote($row["posaddress_store_totalsurface"]);
					$fields[] = "posaddress_store_totalsurface = " . $value;

					$value =  dbquote($row["posaddress_store_retailarea"]);
					$fields[] = "posaddress_store_retailarea = " . $value;

					$value = dbquote($row["posaddress_store_backoffice"]);
					$fields[] = "posaddress_store_backoffice = " . $value;

					$value = dbquote($row["posaddress_store_numfloors"]);
					$fields[] = "posaddress_store_numfloors = " . $value;

					$value = dbquote($row["posaddress_store_floorsurface1"]);
					$fields[] = "posaddress_store_floorsurface1 = " . $value;

					$value =  dbquote($row["posaddress_store_floorsurface2"]);
					$fields[] = "posaddress_store_floorsurface2 = " . $value;

					$value = dbquote($row["posaddress_store_floorsurface3"]);
					$fields[] = "posaddress_store_floorsurface3 = " . $value;

					$value = dbquote($row["posaddress_store_headcounts"]);
					$fields[] = "posaddress_store_headcounts = " . $value;

					$value = dbquote($row["posaddress_store_fulltimeeqs"]);
					$fields[] = "posaddress_store_fulltimeeqs = " . $value;

					$value = dbquote($row["posaddress_google_lat"]);
					$fields[] = "posaddress_google_lat = " . $value;

					$value =  dbquote($row["posaddress_google_long"]);
					$fields[] = "posaddress_google_long = " . $value;

					$fields[] = "posaddress_google_precision = 1";

					$value = dbquote($row["posaddress_export_to_web"]);
					$fields[] = "posaddress_export_to_web = " . $value;

					$value = dbquote($row["posaddress_store_openingdate"]);
					$fields[] = "posaddress_store_openingdate = " . $value;

					$value = dbquote($row["posaddress_store_closingdate"]);
					$fields[] = "posaddress_store_closingdate = " . $value;

					$fields[] = "posaddress_checked = 0";

					$sql = "update _posaddresses set " . join(", ", $fields) . " where posaddress_id = " . dbquote($row_p["posaddress_id"]);
					mysql_query($sql) or dberror($sql);


					//posareas
					
					$sql = "delete from _posareas where posarea_posaddress = " . $row_p["posaddress_id"];
					mysql_query($sql) or dberror($sql);

					$sql = "select * from posareas where posarea_posaddress = " . $row_p["posaddress_id"];
					$res = mysql_query($sql);
					while ($row = mysql_fetch_assoc($res))
					{
					
						$fields = array();
						$values = array();

						$values[] = dbquote($row["posarea_posaddress"]);
						$fields[] = "posarea_posaddress";

						$values[] = dbquote($row["posarea_area"]);
						$fields[] = "posarea_area";

						$values[] = dbquote($row["user_created"]);
						$fields[] = "user_created";

						$values[] = dbquote($row["date_created"]);
						$fields[] = "date_created";

						$values[] = dbquote($row["user_modified"]);
						$fields[] = "user_modified";

						$values[] = dbquote($row["date_modified"]);
						$fields[] = "date_modified";

						$sql = "insert into _posareas (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}

					//posleases
					
					$sql = "delete from _posleases where poslease_posaddress = " . $row_p["posaddress_id"];
					mysql_query($sql) or dberror($sql);

					$sql = "select * from posleases where poslease_posaddress = " . $row_p["posaddress_id"];
					$res = mysql_query($sql);
					while ($row = mysql_fetch_assoc($res))
					{
					
						$fields = array();
						$values = array();

						$values[] = dbquote($row["poslease_posaddress"]);
						$fields[] = "poslease_posaddress";

						$values[] = dbquote($row["poslease_lease_type"]);
						$fields[] = "poslease_lease_type";

						$values[] = dbquote($row["poslease_startdate"]);
						$fields[] = "poslease_startdate";

						$values[] = dbquote($row["poslease_enddate"]);
						$fields[] = "poslease_enddate";

						$values[] = dbquote($row["poslease_extensionoption"]);
						$fields[] = "poslease_extensionoption";

						$values[] = dbquote($row["poslease_exitoption"]);
						$fields[] = "poslease_exitoption";

						$values[] = dbquote($row["poslease_termination_time"]);
						$fields[] = "poslease_termination_time";

						$values[] = dbquote($row["poslease_anual_rent"]);
						$fields[] = "poslease_anual_rent";

						$values[] = dbquote($row["poslease_currency"]);
						$fields[] = "poslease_currency";

						$values[] = dbquote($row["poslease_salespercent"]);
						$fields[] = "poslease_salespercent";

						$values[] = dbquote($row["user_created"]);
						$fields[] = "user_created";

						$values[] = dbquote($row["date_created"]);
						$fields[] = "date_created";

						$values[] = dbquote($row["user_modified"]);
						$fields[] = "user_modified";

						$values[] = dbquote($row["date_modified"]);
						$fields[] = "date_modified";

						$sql = "insert into _posleases (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}
				}
			}
			else // insert records
			{
				$fields = array();
				$values = array();
				
				//_posaddresses
				
				$values[] = dbquote($row_p["posaddress_id"]);
				$fields[] = "posaddress_id";


				$values[] = dbquote($row_p["posaddress_client_id"]);
				$fields[] = "posaddress_client_id";

				if($row_p["posaddress_franchisee_id"])
				{
					$values[] = dbquote($row_p["posaddress_franchisee_id"]);
					$fields[] = "posaddress_franchisee_id";
				}

				$values[] = dbquote($row_p["posaddress_ownertype"]);
				$fields[] = "posaddress_ownertype";

				$values[] = dbquote($row_p["posaddress_name"]);
				$fields[] = "posaddress_name";

				$values[] = dbquote($row_p["posaddress_address"]);
				$fields[] = "posaddress_address";

				$values[] = dbquote($row_p["posaddress_address2"]);
				$fields[] = "posaddress_address2";

				$values[] = dbquote($row_p["posaddress_zip"]);
				$fields[] = "posaddress_zip";

				$values[] =  dbquote($row_p["posaddress_place"]);
				$fields[] = "posaddress_place";

				$values[] =  dbquote($row_p["posaddress_place_id"]);
				$fields[] = "posaddress_place_id";

				$values[] = dbquote($row_p["posaddress_country"]);
				$fields[] = "posaddress_country";
				
				$values[] = dbquote($row_p["posaddress_phone"]);
				$fields[] = "posaddress_phone";
				
				$values[] = dbquote($row_p["posaddress_fax"]);
				$fields[] = "posaddress_fax";
				
				$values[] = dbquote($row_p["posaddress_email"]);
				$fields[] = "posaddress_email";

				$values[] = dbquote($row_p["posaddress_perc_class"]);
				$fields[] = "posaddress_perc_class";

				$values[] = dbquote($row_p["posaddress_perc_tourist"]);
				$fields[] = "posaddress_perc_tourist";

				$values[] = dbquote($row_p["posaddress_perc_transport"]);
				$fields[] = "posaddress_perc_transport";

				$values[] =  dbquote($row_p["posaddress_perc_people"]);
				$fields[] = "posaddress_perc_people";

				$values[] = dbquote($row_p["posaddress_perc_parking"]);
				$fields[] = "posaddress_perc_parking";

				$values[] = dbquote($row_p["posaddress_perc_visibility1"]);
				$fields[] = "posaddress_perc_visibility1";

				$values[] = dbquote($row_p["posaddress_perc_visibility2"]);
				$fields[] = "posaddress_perc_visibility2";


				$values[] =  dbquote($row_p["posaddress_store_postype"]);
				$fields[] = "posaddress_store_postype";


				$values[] =  dbquote($row_p["posaddress_store_subclass"]);
				$fields[] = "posaddress_store_subclass";


				$values[] =  dbquote($row_p["posaddress_store_furniture"]);
				$fields[] = "posaddress_store_furniture";

				$values[] =  dbquote($row_p["posaddress_store_furniture_subclass"]);
				$fields[] = "posaddress_store_furniture_subclass";
				
				$values[] = dbquote($row_p["posaddress_store_grosssurface"]);
				$fields[] = "posaddress_store_grosssurface";

				$values[] = dbquote($row_p["posaddress_store_totalsurface"]);
				$fields[] = "posaddress_store_totalsurface";

				$values[] =  dbquote($row_p["posaddress_store_retailarea"]);
				$fields[] = "posaddress_store_retailarea";

				$values[] = dbquote($row_p["posaddress_store_backoffice"]);
				$fields[] = "posaddress_store_backoffice";

				$values[] = dbquote($row_p["posaddress_store_numfloors"]);
				$fields[] = "posaddress_store_numfloors";

				$values[] = dbquote($row_p["posaddress_store_floorsurface1"]);
				$fields[] = "posaddress_store_floorsurface1";

				$values[] =  dbquote($row_p["posaddress_store_floorsurface2"]);
				$fields[] = "posaddress_store_floorsurface2";

				$values[] = dbquote($row_p["posaddress_store_floorsurface3"]);
				$fields[] = "posaddress_store_floorsurface3";

				$values[] = dbquote($row_p["posaddress_store_headcounts"]);
				$fields[] = "posaddress_store_headcounts";

				$values[] = dbquote($row_p["posaddress_store_fulltimeeqs"]);
				$fields[] = "posaddress_store_fulltimeeqs";

				$values[] = dbquote($row_p["posaddress_google_lat"]);
				$fields[] = "posaddress_google_lat";

				$values[] =  dbquote($row_p["posaddress_google_long"]);
				$fields[] = "posaddress_google_long";

				$values[] =  1;
				$fields[] = "posaddress_google_precision";

				$values[] = dbquote($row_p["posaddress_export_to_web"]);
				$fields[] = "posaddress_export_to_web";

				$values[] = dbquote($row_p["posaddress_store_openingdate"]);
				$fields[] = "posaddress_store_openingdate";

				$values[] = dbquote($row_p["posaddress_store_closingdate"]);
				$fields[] = "posaddress_store_closingdate";

				$values[] = dbquote($row_p["user_created"]);
				$fields[] = "user_created";

				$values[] = dbquote($row_p["date_created"]);
				$fields[] = "date_created";

				$values[] = dbquote($row_p["user_modified"]);
				$fields[] = "user_modified";

				$values[] = dbquote($row_p["date_modified"]);
				$fields[] = "date_modified";


				$sql = "insert into _posaddresses (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);

				//posareas
				
				$sql = "delete from _posareas where posarea_posaddress = " . $row_p["posaddress_id"];
				mysql_query($sql) or dberror($sql);

				$sql = "select * from posareas where posarea_posaddress = " . $row_p["posaddress_id"];
				$res = mysql_query($sql);
				while ($row = mysql_fetch_assoc($res))
				{
				
					$fields = array();
					$values = array();

					$values[] = dbquote($row["posarea_posaddress"]);
					$fields[] = "posarea_posaddress";

					$values[] = dbquote($row["posarea_area"]);
					$fields[] = "posarea_area";

					$values[] = dbquote($row["user_created"]);
					$fields[] = "user_created";

					$values[] = dbquote($row["date_created"]);
					$fields[] = "date_created";

					$values[] = dbquote($row["user_modified"]);
					$fields[] = "user_modified";

					$values[] = dbquote($row["date_modified"]);
					$fields[] = "date_modified";

					$sql = "insert into _posareas (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					mysql_query($sql) or dberror($sql);
				}

				//posleases
				
				$sql = "delete from _posleases where poslease_posaddress = " . $row_p["posaddress_id"];
				mysql_query($sql) or dberror($sql);

				$sql = "select * from posleases where poslease_posaddress = " . $row_p["posaddress_id"];
				$res = mysql_query($sql);
				while ($row = mysql_fetch_assoc($res))
				{
				
					$fields = array();
					$values = array();

					$values[] = dbquote($row["poslease_posaddress"]);
					$fields[] = "poslease_posaddress";

					$values[] = dbquote($row["poslease_lease_type"]);
					$fields[] = "poslease_lease_type";

					$values[] = dbquote($row["poslease_startdate"]);
					$fields[] = "poslease_startdate";

					$values[] = dbquote($row["poslease_enddate"]);
					$fields[] = "poslease_enddate";

					$values[] = dbquote($row["poslease_extensionoption"]);
					$fields[] = "poslease_extensionoption";

					$values[] = dbquote($row["poslease_exitoption"]);
					$fields[] = "poslease_exitoption";

					$values[] = dbquote($row["poslease_termination_time"]);
					$fields[] = "poslease_termination_time";

					$values[] = dbquote($row["poslease_anual_rent"]);
					$fields[] = "poslease_anual_rent";

					$values[] = dbquote($row["poslease_currency"]);
					$fields[] = "poslease_currency";

					$values[] = dbquote($row["poslease_salespercent"]);
					$fields[] = "poslease_salespercent";

					$values[] = dbquote($row["user_created"]);
					$fields[] = "user_created";

					$values[] = dbquote($row["date_created"]);
					$fields[] = "date_created";

					$values[] = dbquote($row["user_modified"]);
					$fields[] = "user_modified";

					$values[] = dbquote($row["date_modified"]);
					$fields[] = "date_modified";

					$sql = "insert into _posleases (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					mysql_query($sql) or dberror($sql);
				}
			}
		}
	}
}

$sql = "select DISTINCT country_id, country_name " .
	   "from posaddresses " . 
	   "left join countries on posaddress_country = country_id " . 
	   "where country_name is not null " . 
	   "order by country_name";


$sql_postypes = "select DISTINCT posaddress_store_postype, postype_name " .
			   "from posaddresses " . 
			   "left join postypes on postype_id = posaddress_store_postype " . 
			   " where postype_name <> '' " .
			   "order by postype_name";


$form = new Form("posaddresses", "posaddress");

$form->add_section("Country Selection");

$form->add_list("country", "Country*",$sql, NOTNULL | SUBMIT);

$form->add_section("POS Companies");
$form->add_checkbox("POSCOMPANIES", "Transfer all POS Companies", 0, 0, "POS Companies");
if(param("country"))
{
	$form->add_section("Single POS Companies");
	$form->add_comment("The date in red shows the date of the latest data correction.");
	$res = mysql_query($sql_companies);
	while ($row = mysql_fetch_assoc($res))
	{
		if($row["address_checkdate"] != NULL and $row["address_checkdate"] != '0000-00-00')
		{
			$caption2 = $row["address_place"] . " <span style='color: #FF0000;'> (" . to_system_date($row["address_checkdate"]) . ")</span>";
		}
		else
		{
			$caption2 = $row["address_place"];
		}
		$form->add_checkbox("address_id_" . $row["address_id"] , $row["address_company"], 0, RENDER_HTML, $caption2);
	}

}

$form->add_section("POS Locations");
$form->add_list("postype", "POS Type",$sql_postypes, SUBMIT, 0, 1, "postype");
$form->add_checkbox("POSLOCATIONS", "Transfer all POS Locations (of selected type)", 0, 0, "POS Locations");


if(param("country"))
{
	$form->add_section("Single POS Locations");
	$form->add_comment("The date in red shows the date of the latest data correction.");
	$res = mysql_query($sql_pos);
	while ($row = mysql_fetch_assoc($res))
	{
		
		if($row["posaddress_checkdate"] != NULL and $row["posaddress_checkdate"] != '0000-00-00')
		{
			$caption2 = $row["posaddress_place"] . " <span style='color: #FF0000;'> (" . to_system_date($row["posaddress_checkdate"]) . ")</span>";
		}
		else
		{
			$caption2 = $row["posaddress_place"];
		}
		$form->add_checkbox("posaddress_id_" . $row["posaddress_id"] ,  $row["posaddress_name"], 0, 0, $caption2);
	}
}

$form->add_section("Data Removal");
$form->add_comment("Use this section to remove existing data in the correction section. <br />All existing data will be deleted before the transfer takes place.");
$form->add_checkbox("remove_companies", "Remove POS Companies", 0, 0, "POS Companies");
$form->add_checkbox("remove_poslocations", "Remove POS Locations", 0, 0, "POS Locations");


$form->add_button("transfer", "Execute");

$form->add_hidden("execute_transfer", 0);

$form->populate();
$form->process();

if($form->button("country"))
{
	$form->value("execute_transfer", 1);
}
elseif($form->button("transfer"))
{
	if($form->validate())
	{
		
		$form->message("The operation was executed successfully.");
		
	}
}

$page = new Page("posdatacorrections");
$page->header();

$page->title("Provide POS Data for Corrections");
$form->render();

?>

<div id="postype" style="display:none;">
    Leave the pos type empty if you want to transfer all pos location of the country indicated.
</div> 

<?php
$page->footer();
?>