<?php
/********************************************************************

    loc_posareatypes.php

    Lists pos area types for translations.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");
set_referer("loc_posareatype.php");

//get all translations
$translations = array();
$sql = "select posareatype_id,language_name " . 
       "from posareatypes " . 
       "left join loc_posareatypes on loc_posareatype_posareatype = posareatype_id " . 
	   "left join languages on language_id = loc_posareatype_language " . 
	   "order by language_name";


$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	if(array_key_exists($row["posareatype_id"], $translations))
	{
		$translations[$row["posareatype_id"]] = $translations[$row["posareatype_id"]] . "/" . trim($row["language_name"]);
	}
	else
	{
		$translations[$row["posareatype_id"]] = trim($row["language_name"]);
	}
}	   

$sql = "select posareatype_id, posareatype_name " .
       "from posareatypes ";

$list = new ListView($sql);

$list->set_entity("posareatypes");
$list->set_order("posareatype_name");


$list->add_column("posareatype_name", "POS Area Type", "loc_posareatype.php", LIST_FILTER_FREE);
$list->add_text_column("translations", "Translations", COLUMN_UNDERSTAND_HTML, $translations);

$list->process();

$page = new Page("locales");

$page->header();
$page->title("POS Area Types");
$list->render();
$page->footer();
?>
