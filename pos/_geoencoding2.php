<?php
/********************************************************************

    _geoencoding2.php

    Get Google Map Coordinates of a Point

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";

check_access("can_administrate_posindex");


$page = new Page("googlemaps2");
$page->header();

$page->title("Get the Latitude and Longitude of a Point");


$api_key = GOOGLE_API_KEY;


?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=<?php echo $api_key;?>"></script>

<script type="text/javascript">
	

	var zoom = 6;
	var latitude = 46.8;
	var longitude = 7.8;
	
	$(document).keypress(function(e) {
		if(e.which == 13) {
			get_coords();
		}
	});
	
	

	function get_coords()
	{
		var address =  $("#address").val();


		var geo = new google.maps.Geocoder;
		geo.geocode({'address':address},function(results, status){
		  if (status == google.maps.GeocoderStatus.OK)
		  {
			 latitude = results[0].geometry.location.lat();
			 longitude = results[0].geometry.location.lng();
			 document.getElementById("latbox").value=latitude;
			 document.getElementById("lonbox").value=longitude;

			 zoom = 16;
			  
			  var myLatlng = new google.maps.LatLng(latitude,longitude);
			  var mapOptions = {
				zoom: zoom,
				center: myLatlng,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			  }

			  var map = new google.maps.Map(document.getElementById('map'), mapOptions);

			  var contentString = '<div id="content"><br />'+
					latitude+', '+longitude+'</div>';

			  var infowindow = new google.maps.InfoWindow({
				  content: contentString
			  });

			  var marker = new google.maps.Marker({
				  position: myLatlng,
				  map: map,
				  draggable:true
			  });

			  google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(map,marker);
			  });
		  }
		  else
		  {
		  }
		});
	}
	
	
	function initialize() {
	  
	  
	  var myLatlng = new google.maps.LatLng(latitude,longitude);
	  var mapOptions = {
		zoom: zoom,
		center: myLatlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	  }

	  var map = new google.maps.Map(document.getElementById('map'), mapOptions);

	  var contentString = '<div id="content"><br />'+
			latitude+', '+longitude+'</div>';

	  var infowindow = new google.maps.InfoWindow({
		  content: contentString
	  });

	  var marker = new google.maps.Marker({
		  position: myLatlng,
		  map: map,
		  draggable:true
	  });

	  google.maps.event.addListener(marker, 'click', function() {
		infowindow.open(map,marker);
	  });

	  google.maps.event.addListener(marker, 'dragend', function(evt) {
		document.getElementById("latbox").value=evt.latLng.lat();
		document.getElementById("lonbox").value=evt.latLng.lng();

	  });
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>

Enter an address

<div id="content">
	<table>
	<tr>
		<td><input size="100" type="text" id="address" name="address" value="" /></td>
	</tr>
	</table>
</div>

Or move the marker to get the latitude and longitude coordinates of the point.
<br /><br />

<div id="map" style="width: 800px; height: 450px"></div>

<br /><br />
<div id="content">
	<table>
	<tr>
		<td>Latitude:</td>
		<td><input size="20" type="text" id="latbox" name="latbox" value="" /></td>
	</tr>
	<tr>
		<td>Longitude:</td>
		<td><input size="20" type="text" id="lonbox" name="lonbox" value="" /></td>
	</tr>
	</table>
</div>


<?php
	$page->footer();
?>