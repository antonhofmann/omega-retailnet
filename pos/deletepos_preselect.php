<?php
/********************************************************************

    deletepos_preselect.php

    Preselection of POS List

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_use_posindex");
set_referer("posindex.php");

//compose list

$sql = "select DISTINCT country_id, country_name " .
       "from posaddresses " . 
	   "left join countries on posaddress_country = country_id " . 
	   "where country_name is not null " . 
	   "order by country_name";

$form = new Form("posaddresses", "posaddress");

$form->add_section("Country Selection");

$form->add_list("country", "Country",$sql);

$form->add_button("show_pos", "Show List");

$form->populate();
$form->process();


if($form->button("show_pos"))
{
	redirect("deletepos.php?country=" . $form->value("country"));
}

$page = new Page("deletepos");
$page->header();

$page->title("Delete POS from POS Index");
$form->render();


$page->footer();

?>
