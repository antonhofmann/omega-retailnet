<?php
/********************************************************************

    posshow_worldmap.php

    Shows the world map

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2010-11-20
    Version:        2.0.0

    Copyright (c) 2008-2010, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
check_access("can_use_posindex");

$link = "";
//prepare data


if(has_access("can_view_posindex") or has_access("can_edit_posindex"))
{
	$sql_salesregions = "select salesregion_id, salesregion_name " . 
						"from salesregions " . 
						"order by salesregion_name";



	if(param("salesregion"))
	{
		$sql_countries = "select country_id, country_name " . 
					   "from countries " . 
					   "where country_salesregion = " . param("salesregion") .  
					   " order by country_name";
	}
	else
	{
		$sql_countries = "select country_id, country_name " . 
					   "from countries " . 
					   " order by country_name";
	}
}
else
{
	$user = get_user(user_id());

	$country_filter = "";
	$tmp = array();
	$sql = "select * from country_access " .
		   "where country_access_user = " . user_id();


	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{            
		$tmp[] = $row["country_access_country"];
	}

	if(count($tmp) > 0) {
		$country_filter = " country_id IN (" . implode(",", $tmp) . ") ";
	}

	

	if($country_filter == "")
	{
		$sql_countries = "select DISTINCT country_id, country_name " .
			   "from posaddresses " . 
			   "left join countries on posaddress_country = country_id " .
			   "left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
			   "left join postypes on postype_id = posaddress_store_postype " .
			   "where country_name is not null " .
			   " and posaddress_client_id = " . $user["address"] . 
			   //" and (postype_id <> 2 or (postype_id = 2 and project_costtype_id = 1)) " . 
			   " order by country_name";
	}
	else
	{
		$sql_countries = "select DISTINCT country_id, country_name " .
			   "from posaddresses " . 
			   "left join countries on posaddress_country = country_id " .
			   "left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
			   "left join postypes on postype_id = posaddress_store_postype " .
			   "where country_name is not null " .
			   " and " . $country_filter . 
			   //" and (postype_id <> 2 or (postype_id = 2 and project_costtype_id = 1)) " . 
			   " order by country_name";
	}
}

//get pos types
$pos_types = array();
$sql_postypes = "select postype_id, postype_name " . 
			    "from postypes " . 
			    " order by postype_name";

$res = mysql_query($sql_postypes) or dberror($postype_name);

while ($row = mysql_fetch_assoc($res))
{
    $pos_types[$row["postype_id"]] = $row["postype_name"];
}

//get legal types
$legal_types = array();
$sql_legaltypes = "select project_costtype_id, project_costtype_text " . 
					"from project_costtypes " . 
					"where project_costtype_id IN(1,2,6) " . 
					" order by project_costtype_text";


$res = mysql_query($sql_legaltypes) or dberror($sql_legaltypes);

while ($row = mysql_fetch_assoc($res))
{
    $legal_types[$row["project_costtype_id"]] = $row["project_costtype_text"];
}


//get distribution channels
$tmp_filter = '';
if(param("country"))
{
	$dc = array();
	$sql = "select distinct posaddress_distribution_channel " .
		   "from posaddresses " . 
		   " where posaddress_distribution_channel > 0 and posaddress_country = " . param("country");

	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{
		$dc[] = $row['posaddress_distribution_channel'];
	}

	if(count($dc) > 0)
	{
		$tmp_filter = " and mps_distchannel_id IN (" . implode(', ', $dc) . ") ";
	}
	else
	{
		$tmp_filter = " and mps_distchannel_id = 9999999999 ";
	}


}

$distribution_channels = array();
$sql_distchannels = "select mps_distchannel_id, concat(mps_distchannel_code, ' ', mps_distchannel_name) as dname " . 
					"from mps_distchannels" . 
					" where mps_distchannel_active = 1 " .
					$tmp_filter . 
					" order by mps_distchannel_code";

$res = mysql_query($sql_distchannels) or dberror($sql_distchannels);

while ($row = mysql_fetch_assoc($res))
{
    $distribution_channels[$row["mps_distchannel_id"]] = $row["dname"];
}


//get turn over classes watches

$tmp_filter = '';
if(param("country"))
{
	$tcw = array();
	$sql = "select distinct posaddress_turnoverclass_watches " .
		   "from posaddresses " . 
		   " where posaddress_turnoverclass_watches > 0 and posaddress_country = " . param("country");

	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{
		$tcw[] = $row['posaddress_turnoverclass_watches'];
	}

	if(count($tcw) > 0)
	{
		$tmp_filter = " and mps_turnoverclass_id IN (" . implode(', ', $tcw) . ") ";
	}
	else
	{
		$tmp_filter = " and mps_turnoverclass_id = 9999999999 ";
	}

}

$turnover_classes = array();
$sql_turnoverclasses = "select mps_turnoverclass_id, mps_turnoverclass_code " . 
					"from mps_turnoverclasses" . 
					" where mps_turnoverclass_group_id = 1 " . 
					$tmp_filter . 
					" order by mps_turnoverclass_code";


$res = mysql_query($sql_turnoverclasses) or dberror($sql_turnoverclasses);

while ($row = mysql_fetch_assoc($res))
{
    $turnover_classes[$row["mps_turnoverclass_id"]] = $row["mps_turnoverclass_code"];
}


/********************************************************************
Render Form
*********************************************************************/

$form = new Form("posaddresses", "World Map");

$form->add_section("Selection Criteria");

if(has_access("can_view_posindex") or has_access("can_edit_posindex"))
{
	$form->add_list("salesregion", "Sales Region",$sql_salesregions, SUBMIT);
}
else
{
	$form->add_hidden("salesregion");
}
$form->add_list("country", "Country",$sql_countries, SUBMIT);



$form->add_section("Legal Types");
$l_check_box_names = array();
foreach ($legal_types as $id=>$name)
{
	$form->add_checkbox("L" . $id, $name, 1, 0, "");
	$l_check_box_names["L" . $id] = "L" . $id;
}

$form->add_section("Pos Types");
$p_check_box_names = array();
foreach ($pos_types as $id=>$name)
{
	$form->add_checkbox("P" . $id, $name, 1, 0, "");
	$p_check_box_names["P" . $id] = "P" . $id;
}


$form->add_section("Distribution Channels");
$d_check_box_names = array();
$form->add_label("select0", "Distribution Channels", RENDER_HTML, "<div id='select0'><a href='javascript:select_all_dchannels();'>select all distribution channels</a></div>");
foreach ($distribution_channels as $id=>$name)
{
	$form->add_checkbox("D" . $id, $name, 0, 0, "");
	$d_check_box_names["D" . $id] = "D" . $id;
}


$form->add_section("Turnover Class Watches");
$t_check_box_names = array();
$form->add_label("select1", "Turnover Classes", RENDER_HTML, "<div id='select1'><a href='javascript:select_all_tclasses();'>select all classes</a></div>");
foreach ($turnover_classes as $id=>$name)
{
	$form->add_checkbox("T" . $id, $name, 0, 0, "");
	$t_check_box_names["T" . $id] = "T" . $id;
}

$form->add_section("Map Options");
$form->add_checkbox("closed", "Only show operating POS locations", false, 0, "POS Locations");

$form->add_checkbox("lb1", "Add labels for POS names", false, 0, "POS Name");
$form->add_checkbox("lb2", "Add labels for distribution channels", false, 0, "Distribution Channel");
$form->add_checkbox("lb3", "Add lables for turn over classes", false, 0, "Turnover Class");

$form->add_button("show_map", "Show Map");

$form->populate();
$form->process();


if($form->validate() and $form->button("show_map") or $form->button("show_map1"))
{
	
	$getparams = '';

	if(param("salesregion"))
	{
		$getparams = "?r=" . param("salesregion");
	}


	if($getparams and param("country"))
	{
		$getparams .= "&c=" . param("country");
	}
	elseif(param("country"))
	{
		$getparams = "?c=" . param("country");
	}

	
	
	$s_ptypes = array();
	foreach($p_check_box_names as $key=>$name)
	{
		if($form->value($name) == 1)
		{
			$s_ptypes[] = substr($name, 1, strlen($name));
		}
	}
	
	if($getparams)
	{
		$getparams .= "&t=" . implode("-", $s_ptypes);
	}
	else
	{
		$getparams = "?t=" . implode("-", $s_ptypes);
	}


	$s_ltypes = array();
	foreach($l_check_box_names as $key=>$name)
	{
		if($form->value($name) == 1)
		{
			$s_ltypes[] = substr($name, 1, strlen($name));
		}
	}

	if($getparams)
	{
		$getparams .= "&l=" . implode("-", $s_ltypes);
	}
	else
	{
		$getparams = "?l=" . implode("-", $s_ltypes);
	}


	$s_distributionchannles = array();
	foreach($d_check_box_names as $key=>$name)
	{
		if($form->value($name) == 1)
		{
			$s_distributionchannles[] = substr($name, 1, strlen($name));
		}
	}

	if($getparams)
	{
		$getparams .= "&d=" . implode("-", $s_distributionchannles);
	}
	else
	{
		$getparams = "?d=" . implode("-", $s_distributionchannles);
	}


	$s_turnoverclasses = array();
	foreach($t_check_box_names as $key=>$name)
	{
		if($form->value($name) == 1)
		{
			$s_turnoverclasses[] = substr($name, 1, strlen($name));
		}
	}

	if($getparams)
	{
		$getparams .= "&tc=" . implode("-", $s_turnoverclasses);
	}
	else
	{
		$getparams = "?tc=" . implode("-", $s_turnoverclasses);
	}


	
	//Map Options
	
	
	if($getparams and param("closed"))
	{
		$getparams .= "&o=" . param("closed");
	}
	elseif(param("closed"))
	{
		$getparams = "?o=" . param("closed");
	}


	if($getparams and param("lb1"))
	{
		$getparams .= "&lb1=1";
	}
	elseif(param("lb1"))
	{
		$getparams = "&lb1=1";
	}

	if($getparams and param("lb2"))
	{
		$getparams .= "&lb2=1";
	}
	elseif(param("lb2"))
	{
		$getparams = "&lb2=1";
	}

	if($getparams and param("lb3"))
	{
		$getparams .= "&lb3=1";
	}
	elseif(param("lb3"))
	{
		$getparams = "&lb3=1";
	}


	if(count($s_ltypes) == 0 or $s_ptypes == 0)
	{
		$form->error("Please select at least one Legal Type and one POS Type");
	}
	else
	{
		$link = "posworldmap.php" . $getparams;
		//redirect($link);
	}
}


// Render page

$page = new Page("posindex");
require "include/pos_page_actions.php";
$page->header();

$page->title("POS Index: World Map");
$form->render();



?>
<script language='javascript'>

	function select_all_dchannels()
	{
		<?php
		foreach($distribution_channels as $id=>$name)
		{
			echo "$('input[name=D" . $id . "]').attr('checked', true);";
		}
		?>

		var div = document.getElementById('select0');
		div.innerHTML = "<a href='javascript:deselect_all_dchannels();'>deselect all distirbution channels</a>";
		
		
	}


	function deselect_all_dchannels()
	{
		<?php
		foreach($distribution_channels as $id=>$name)
		{
			echo "$('input[name=D" . $id . "]').attr('checked', false);";
		}
		?>

		var div = document.getElementById('select0');
		div.innerHTML = "<a href='javascript:select_all_dchannels();'>select all distirbution channels</a>";
	}

	function select_all_tclasses()
	{
		<?php
		foreach($turnover_classes as $id=>$name)
		{
			echo "$('input[name=T" . $id . "]').attr('checked', true);";
		}
		?>

		var div = document.getElementById('select1');
		div.innerHTML = "<a href='javascript:deselect_all_tclasses();'>deselect all classes</a>";
		
		
	}


	function deselect_all_tclasses()
	{
		<?php
		foreach($turnover_classes as $id=>$name)
		{
			echo "$('input[name=T" . $id . "]').attr('checked', false);";
		}
		?>

		var div = document.getElementById('select1');
		div.innerHTML = "<a href='javascript:select_all_tclasses();'>select all classes</a>";
	}
</script>

<?php
if($link) 
{
?>
<script>
	window.open('<?php echo $link;?>');
</script>
<?php 
}

$page->footer();

?>