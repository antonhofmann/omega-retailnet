<?php
/********************************************************************

    locales.php

    Main entry page for POS Locales Functions.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");


$page = new Page("locales");

$page->header();
echo "<p>Welcome to the Management Section of " . APPLICATION_NAME . " POS-Index.</p>";
echo "<p>", "This section allows you to localize POS Data in other languages.", "</p>";

$page->footer();


?>