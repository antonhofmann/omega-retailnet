<?php
/********************************************************************

    posfile.php

    Creation and mutation of POS file records.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";


require_once "include/check_access.php";


$pos = array();
if(param("pos_id") > 0)
{
	$pos = get_poslocation(param("pos_id"), "posaddresses");
}

$num_of_old_projects = 0;
$sql_projects = "select count(posorder_id) as num_recs " .
                "from posorders  " . 
				"where posorder_order is Null " . 
				" and posorder_posaddress = " . param("pos_id") . 
				" order by posorder_ordernumber";

$res = mysql_query($sql_projects) or dberror($sql_projects);
if($row = mysql_fetch_assoc($res))
{
		$num_of_old_projects = $row["num_recs"];
}

$sql_projects = "select posorder_id, posorder_ordernumber " .
                "from posorders  " . 
				"where posorder_order is Null " . 
				" and posorder_posaddress = " . param("pos_id") . 
				" order by posorder_ordernumber";

$form = new Form("posfiles", "posfile");

$form->add_section("Name and address");

require_once("include/poslocation_head.php");

$form->add_hidden("country", param("country"));
$form->add_hidden("pos_id", param("pos_id"));
$form->add_hidden("posfile_posaddress", param("pos_id"));
$form->add_hidden("posfile_owner", user_id());

$form->add_hidden("ltf", param("ltf"));
$form->add_hidden("let", param("let"));
$form->add_hidden("ostate", param("ostate"));

if($num_of_old_projects > 0)
{
	$form->add_section("Project");
	$form->add_comment("Please do only select a project if you want to attach the file to a specific project not beiing in retail net but in the POS Index.");
	$form->add_list("posfile_posorder", "Projects", $sql_projects);
}

$form->add_section("File Description");


$form->add_list("posfile_filegroup", "File Group*",
    "select posfilegroup_id, posfilegroup_name from posfilegroups order by posfilegroup_name", NOTNULL);

$form->add_list("posfile_filetype", "Type*",
    "select posfiletype_id, posfiletype_name from posfiletypes order by posfiletype_name", NOTNULL);

$form->add_edit("posfile_title", "Title*", NOTNULL);

$form->add_multiline("posfile_description", "Description", 4);

$form->add_section("File");
$file_code = make_valid_filename("POS_". param("pos_id"));
$form->add_upload("posfile_path", "File", "/files/posindex/$file_code");

$form->add_section();

$form->add_button(FORM_BUTTON_SAVE, "Save");
if(id() > 0 and has_access("can_edit_posindex"))
{
	$form->add_button("delete", "Delete");
}
$form->add_button("back", "Back");

$form->populate();
$form->process();

if($form->button("back"))
{
	$link = "posfiles.php?pos_id=" . param("pos_id") . "&country=" . param("country"). '&let=' . param('let') . "&ltf=" . param("ltf"). "&ostate=" . param("ostate");
	redirect($link);
}
elseif($form->button("delete"))
{
	$sql = "delete from posfiles where posfile_id = " . id();
    $res = mysql_query($sql) or dberror($sql);
	$link = "posfiles.php?pos_id=" . param("pos_id") . "&country=" . param("country"). '&let=' . param('let') . "&ltf=" . param("ltf"). "&ostate=" . param("ostate");
	redirect($link);
}

$page = new Page("posindex");
require "include/pos_page_actions.php";
$page->header();

$page->title(id() ? "Edit File" : "Add File");

require_once("include/tabs.php");

$form->render();
$page->footer();

?>
