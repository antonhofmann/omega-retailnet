<?php
/********************************************************************

    posareatypes.php

    Lists store area types for editing.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");
set_referer("posareatype.php");

$list = new ListView("select posareatype_id, posareatype_name, " .
                     "posareatype_email1, posareatype_email2, posareatype_email3, posareatype_email4, posareatype_email5, posareatype_email6 " .
                     "from posareatypes");

$list->set_entity("posareatypes");


$list->set_order("posareatype_name");

$list->add_column("posareatype_name", "Name", "posareatype.php");

$list->add_column("posareatype_email1", "Recipient 1");
$list->add_column("posareatype_email2", "Recipient 2");
$list->add_column("posareatype_email3", "Recipient 3");
$list->add_column("posareatype_email4", "Recipient 4");
$list->add_column("posareatype_email5", "Recipient 5");
$list->add_column("posareatype_email6", "Recipient 6");


$list->add_button(LIST_BUTTON_NEW, "New", "posareatype.php");

$list->process();

$page = new Page("posareatypes");

$page->header();
$page->title("Area Types");
$list->render();
$page->footer();

?>
