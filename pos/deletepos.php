<?php
/********************************************************************

    deletepos.php

    Lists of addresses (POS) to be deleted

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_use_posindex");
set_referer("posindex_pos.php");


$preselect_filter = "";
if(param("country"))
{
	$preselect_filter = "posaddress_country = " . param("country");
	register_param("country", param("country"));
}


$form = new Form("posaddresses", "posaddress");
$form->populate();


//compose list

$sql = "select posaddress_id, if(posaddress_name <> '', posaddress_name, 'n.a.') as posname, " .
       "posaddress_address, posaddress_address2, posaddress_zip, " .
       "    posaddress_place, country_name, project_costtype_text, postype_name " .
       "from posaddresses " . 
	   "left join countries on posaddress_country = country_id " . 
	   "left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
	   "left join postypes on postype_id = posaddress_store_postype ";

$list = new ListView($sql);

$list->set_entity("posaddresses");
$list->set_filter($preselect_filter);
$list->set_order("country_name, posaddress_place, posaddress_name");



$list->add_hidden("country", param("country"));

$list->add_checkbox_column("posaddress_id", "", 0);
$list->add_column("country_name", "Country", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);
$list->add_column("posaddress_place", "City", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("posname", "POS Name", "" , LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("posaddress_address", "Address", "", LIST_FILTER_FREE);
$list->add_column("postype_name", "POS Type", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);
$list->add_column("project_costtype_text", "Legal Type", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);

$list->add_button("delete", "Delete Selected", "posindex_pos.php?country=" . param("country"));

if(param("d") and param("d") == 1)
{
	$form->message("The selected POS have been removed successfully from the POS Index.");
}

$list->populate();
$list->process();

if($list->button("delete"))
{
	foreach ($list->values("posaddress_id") as $key=>$value)
    {
		if($value == 1)
		{
			
			$sql = "delete from posareas where posarea_posaddress = " . $key;
			mysql_query($sql) or dberror($sql);

			$sql = "delete from posfiles where posfile_posaddress = " . $key;
			mysql_query($sql) or dberror($sql);

			$sql = "delete from posinvestments where posinvestment_posaddress = " . $key;
			mysql_query($sql) or dberror($sql);

			$sql = "delete from posleases where poslease_posaddress = " . $key;
			mysql_query($sql) or dberror($sql);

			$sql = "delete from posaddresses where posaddress_id = " . $key;
			mysql_query($sql) or dberror($sql);

			$sql = "select posorder_id from posorders where posorder_posaddress = " . $key;
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$sql = "delete from posorderinvestments where posorderinvestment_posorder = " . $row["posorder_id"];
				mysql_query($sql) or dberror($sql);
			}

			$sql = "delete from posorders where posorder_posaddress = " . $key;
			mysql_query($sql) or dberror($sql);

			$sql = "delete from posaddresses where posaddress_id = " . $key;
			mysql_query($sql) or dberror($sql);

			
		}
	}

	$link = "deletepos.php?d=1&country=" . param("country");;
	redirect($link);

}


$page = new Page("deletepos");
$page->header();

$page->title("Delete POS from POS Index");
$form->render();
$list->render();


$page->footer();

?>
