<?php
/********************************************************************

    areaperception_types

    Lists pos area perception types for translations.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");
set_referer("loc_areaperception_type.php");

//get all translations
$translations = array();
$sql = "select areaperception_type_id,language_name " . 
       "from areaperception_types " . 
       "left join loc_areaperception_types on loc_areaperception_type_type = areaperception_type_id " . 
	   "left join languages on language_id = loc_areaperception_type_language " . 
	   "order by language_name";


$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	if(array_key_exists($row["areaperception_type_id"], $translations))
	{
		$translations[$row["areaperception_type_id"]] = $translations[$row["areaperception_type_id"]] . "/" . trim($row["language_name"]);
	}
	else
	{
		$translations[$row["areaperception_type_id"]] = trim($row["language_name"]);
	}
}	   

$sql = "select areaperception_type_id, areaperception_type_name " .
       "from areaperception_types ";

$list = new ListView($sql);

$list->set_entity("areaperception_types");
$list->set_order("areaperception_type_name");
$list->set_filter("areaperception_type_show_on_web = 1");

$list->add_column("areaperception_type_name", "POS Area Perception Type", "loc_areaperception_type.php", LIST_FILTER_FREE);
$list->add_text_column("translations", "Translations", COLUMN_UNDERSTAND_HTML, $translations);

$list->process();

$page = new Page("locales");

$page->header();
$page->title("POS Area Perception Types");
$list->render();
$page->footer();
?>
