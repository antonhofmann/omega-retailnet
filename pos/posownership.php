<?php
/********************************************************************

    posownership.php

    Creation and mutation of posaddress records.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

require_once "include/check_access.php";

if(param("pos_id"))
{
	register_param("pos_id");
	param("id", param("pos_id"));
}
else
{
	register_param("pos_id");
	param("pos_id", id());
}

if(param("pos_id") > 0)
{
	$pos = get_poslocation(param("pos_id"), "posaddresses");
}

$has_project = update_posdata_from_posorders(param("pos_id"));

//calculate franchisee agreement duration
$duration = calculate_aggreement_duration(param("pos_id"));


// Build form

$form = new Form("posaddresses", "posaddress");

//check if user can edit this company
$user = get_user(user_id());
$can_edit = false;
if(has_access("can_edit_posindex"))
{
	$can_edit = true;
}
elseif(has_access("can_edit_his_posindex"))
{
	$can_edit = get_user_edit_permission(user_id(), param("pos_id"));
}


$form->add_section("Roles");

if(has_access("can_edit_posindex"))
{
	if(param("pos_id") > 0 and $pos["posaddress_store_closingdate"] != '' and $pos["posaddress_store_closingdate"] != '0000-00-00' and $pos["posaddress_client_id"] > 0)
	{
		$form->add_lookup("posaddress_client_id", "Client", "addresses", "concat(address_company, ', ', address_place, ' [', address_shortcut, ']') as company", 0, $pos["posaddress_client_id"]);

		$form->add_lookup("posaddress_ownertype", "Legal Type", "project_costtypes", "project_costtype_text", 0, $pos["posaddress_ownertype"]);

		$form->add_edit("posaddress_eprepnr", "Enterprise Reporting Number");
		$form->add_edit("posaddress_sapnumber", "SAP Number");

		$form->add_lookup("posaddress_franchisor_id", "Franchisor", "addresses", "concat(address_company, ', ', address_place) as company", 0, $pos["posaddress_franchisor_id"]);

		if($pos["posaddress_ownertype"] != 6)
		{
			$tmp = "Franchisee";
		}
		else
		{
			$tmp = "Owner Company";
		}
		$form->add_lookup("posaddress_franchisee_id", $tmp, "addresses", "concat(address_company, ', ', address_place) as company", 0, $pos["posaddress_franchisee_id"]);

	}
	else
	{
		$form->add_list("posaddress_client_id", "Client*",
			"select address_id, concat(country_name, ': ', address_company, ', ', address_place, ' [', address_shortcut, ']') as company from addresses left join countries on country_id = address_country where address_active = 1 and (address_type = 1 or address_type = 4) order by country_name, address_company", NOTNULL);

		if($has_project = false)
		{
			$form->add_list("posaddress_ownertype", "Legal Type*",
				"select project_costtype_id, project_costtype_text from project_costtypes where project_costtype_id IN (1, 2, 6)", NOTNULL);

		}
		else
		{
			$form->add_lookup("posaddress_ownertype", "Legal Type", "project_costtypes", "project_costtype_text", 0, $pos["posaddress_ownertype"]);
		}

		$form->add_edit("posaddress_eprepnr", "Enterprise Reporting Number");
		$form->add_edit("posaddress_sapnumber", "SAP Number");

		$form->add_list("posaddress_franchisor_id", "Franchisor",
			"select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where address_active = 1 and country_name <> '' and address_company <> '' and address_place <> '' and address_canbefranchisor = 1 order by country_name, address_company");

		
		if($pos["posaddress_ownertype"] != 6)
		{
			$tmp = "Franchisee";
		}
		else
		{
			$tmp = "Owner Company";
		}
		$form->add_list("posaddress_franchisee_id", $tmp,
			"select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where address_showinposindex = 1 and country_name <> '' and address_company <> '' and address_place <> '' and address_canbefranchisee = 1 and address_parent = " . $pos["posaddress_client_id"] . " order by country_name, address_company");
	}
	$form->add_section("Franchise Data");
	$form->add_list("posaddress_fagagreement_type", "Agreement Type*", "select agreement_type_id, agreement_type_name from agreement_types", 0);
	$form->add_checkbox("posaddress_fagrsent", "Sent", "", 0, "Franchisee Agreement");
	$form->add_checkbox("posaddress_fagrsigned", "Signed", "", 0, "Franchisee Agreement");
	$form->add_label("dummy", "");
	$form->add_edit("posaddress_fagrstart", "Agreement Starting Date", 0, "", TYPE_DATE);
	$form->add_edit("posaddress_fagrend", "Agreement Ending Date", 0, "", TYPE_DATE);
	
	$form->add_label("fargrduration", "Agreement Duration", 0, $duration);
	$form->add_edit("posaddress_fagcancellation", "Cancellation Date", 0, "", TYPE_DATE);
	$form->add_multiline("posaddress_fag_comment", "Comment", 4);

	$form->add_section("Takeover Data");
	$form->add_edit("posaddress_takeover_date", "Take Over Date", 0, "", TYPE_DATE);
	$form->add_edit("posaddress_takeover_amount", "Take Over Amount", 0, "", TYPE_DATE);
	$form->add_list("posaddress_takeover_currency", "Currency",
		"select currency_id, currency_symbol from currencies  order by currency_symbol");

	$form->add_list("posaddress_takeover_from", "Take Over From",
		"select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where country_name <> '' and address_company <> '' and address_place <> '' and address_canbefranchisee = 1 order by country_name, address_company");

	$form->add_multiline("posaddress_takeover_comment", "Comment", 4);
	$form->add_hidden("country", param("country"));

	$form->add_button(FORM_BUTTON_SAVE, "Save");
}
elseif($can_edit == true and has_access("can_edit_his_posindex"))
{
	$company = "";
	$sql = "select concat(country_name, ': ', address_company, ', ', address_place, ' [', address_shortcut, ']') as company " . 
		   "from posaddresses " .
		   "left join addresses on address_id = posaddress_client_id " . 
		   "left join countries on country_id = address_country " . 
		   "where posaddress_id = " . param("pos_id");

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$company = $row["company"];
	}
	
	$form->add_label("company", "Client", 0, $company);
	
	$form->add_lookup("posaddress_ownertype", "Legal Type", "project_costtypes", "project_costtype_text");
	$form->add_label("posaddress_eprepnr", "Enterprise Reporting Number");
	$form->add_label("posaddress_sapnumber", "SAP Number");

	$company = "";
	$sql = "select concat(country_name, ': ', address_company, ', ', address_place, ' [', address_shortcut, ']') as company " . 
		   "from posaddresses " .
		   "left join addresses on address_id = posaddress_franchisor_id " . 
		   "left join countries on country_id = address_country " . 
		   "where posaddress_id = " . param("pos_id");

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$company = $row["company"];
	}
	$form->add_label("franchisor", "Franchisor", 0, $company);


	$company = "";
	$sql = "select concat(country_name, ': ', address_company, ', ', address_place, ' [', address_shortcut, ']') as company " . 
		   "from posaddresses " .
		   "left join addresses on address_id = posaddress_franchisee_id " . 
		   "left join countries on country_id = address_country " . 
		   "where posaddress_id = " . param("pos_id");

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$company = $row["company"];
	}
	
	if($pos["posaddress_ownertype"] != 6)
	{
		$tmp = "Franchisee";
	}
	else
	{
		$tmp = "Owner Company";
	}
	$form->add_label("franchisee", $tmp, 0, $company);

	$form->add_section("Franchise Data");
	$form->add_checkbox("posaddress_fagrsent", "Sent", "", 0, "Franchisee Agreement");
	$form->add_checkbox("posaddress_fagrsigned", "Signed", "", 0, "Franchisee Agreement");
	$form->add_label("dummy", "");
	$form->add_edit("posaddress_fagrstart", "Agreement Starting Date", 0, "", TYPE_DATE);
	$form->add_edit("posaddress_fagrend", "Agreement Ending Date", 0, "", TYPE_DATE);
	$form->add_label("fargrduration", "Agreement Duration", 0, $duration);
	$form->add_edit("posaddress_fagcancellation", "Cancellation Date", 0, "", TYPE_DATE);
	$form->add_multiline("posaddress_fag_comment", "Comment", 4);

	$form->add_section("Takeover Data");
	$form->add_edit("posaddress_takeover_date", "Take Over Date", 0, "", TYPE_DATE);
	$form->add_edit("posaddress_takeover_amount", "Take Over Amount", 0, "", TYPE_DATE);
	$form->add_list("posaddress_takeover_currency", "Currency",
		"select currency_id, currency_symbol from currencies  order by currency_symbol");

	$form->add_list("posaddress_takeover_from", "Take Over From",
		"select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where country_id = " . $user["country"] . " and country_name <> '' and address_company <> '' and address_place <> '' and address_canbefranchisee = 1 order by country_name, address_company");

	$form->add_multiline("posaddress_takeover_comment", "Comment", 4);
	$form->add_hidden("country", param("country"));

	$form->add_button(FORM_BUTTON_SAVE, "Save");
}
elseif(has_access("can_view_posindex") or has_access("can_view_his_posindex"))
{
	
	$company = "";
	$sql = "select concat(country_name, ': ', address_company, ', ', address_place, ' [', address_shortcut, ']') as company " . 
		   "from posaddresses " .
		   "left join addresses on address_id = posaddress_client_id " . 
		   "left join countries on country_id = address_country " . 
		   "where posaddress_id = " . param("pos_id");

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$company = $row["company"];
	}
	
	$form->add_label("company", "Client", 0, $company);
	
	$form->add_lookup("posaddress_ownertype", "Legal Type", "project_costtypes", "project_costtype_text");
	$form->add_label("posaddress_eprepnr", "Enterprise Reporting Number");
	$form->add_label("posaddress_sapnumber", "SAP Number");

	$company = "";
	$sql = "select concat(country_name, ': ', address_company, ', ', address_place, ' [', address_shortcut, ']') as company " . 
		   "from posaddresses " .
		   "left join addresses on address_id = posaddress_franchisor_id " . 
		   "left join countries on country_id = address_country " . 
		   "where posaddress_id = " . param("pos_id");

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$company = $row["company"];
	}
	$form->add_label("franchisor", "Franchisor", 0, $company);


	$company = "";
	$sql = "select concat(country_name, ': ', address_company, ', ', address_place, ' [', address_shortcut, ']') as company " . 
		   "from posaddresses " .
		   "left join addresses on address_id = posaddress_franchisee_id " . 
		   "left join countries on country_id = address_country " . 
		   "where posaddress_id = " . param("pos_id");

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$company = $row["company"];
	}
	
	if($pos["posaddress_ownertype"] != 6)
	{
		$tmp = "Franchisee";
	}
	else
	{
		$tmp = "Owner Company";
	}
	$form->add_label("franchisee", $tmp, 0, $company);

	$form->add_section("Franchise Data");
	$form->add_checkbox("posaddress_fagrsent", "Sent", "", 0, "Franchisee Agreement");
	$form->add_checkbox("posaddress_fagrsigned", "Signed", "", 0, "Franchisee Agreement");
	$form->add_label("dummy", "");
	$form->add_label("posaddress_fagrstart", "Agreement Starting Date");
	$form->add_label("posaddress_fagrend", "Agreement Ending Date");
	$form->add_label("fargrduration", "Agreement Duration", 0, $duration);
	$form->add_label("posaddress_fagcancellation", "Cancellation Date");
	$form->add_label("posaddress_fag_comment", "Comment");

	$form->add_section("Takeover Data");
	$form->add_label("posaddress_takeover_date", "Take Over Date");
	$form->add_label("posaddress_takeover_amount", "Take Over Amount");
	$form->add_lookup("posaddress_takeover_currency", "Currency", "currencies", "currency_symbol");

	$company = "";
	$sql = "select concat(country_name, ': ', address_company, ', ', address_place, ' [', address_shortcut, ']') as company " . 
		   "from posaddresses " .
		   "left join addresses on address_id = posaddress_takeover_from " . 
		   "left join countries on country_id = address_country " . 
		   "where posaddress_id = " . param("pos_id");

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$company = $row["company"];
	}
	$form->add_label("takenover_from", "Take over from", 0, $company);
	
	

	$form->add_label("posaddress_takeover_comment", "Comment");
	$form->add_hidden("country", param("country"));
}


$form->add_hidden("ltf", param("ltf"));
$form->add_hidden("let", param("let"));
$form->add_hidden("ostate", param("ostate"));
$form->add_hidden("posaddress_fargrduration");
$form->add_button("back", "Back to POS List");

// Populate form and process button clicks

$form->populate();
$form->process();

if($form->button(FORM_BUTTON_SAVE))
{
	
	//calculate franchisee agreement duration
	$duration = calculate_aggreement_duration(param("pos_id"));
	$form->value("posaddress_fargrduration", $duration);
	$form->value("fargrduration", $duration);

	redirect("posindex.php?country=" . param("country"). '&let=' . param('let') . '&ltf=' . param("ltf"). '&ostate=' . param("ostate"));
}

if($form->button("back"))
{
	redirect("posindex.php?country=" . param("country"). '&let=' . param('let') . '&ltf=' . param("ltf"). '&ostate=' . param("ostate"));
}


// Render page
$poslocation = get_poslocation(id(), "posaddresses");

$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title(id() ? "Ownership Details: " . $poslocation["posaddress_name"] : "Add POS Location");

require_once("include/tabs.php");

$form->render();
$page->footer();

?>