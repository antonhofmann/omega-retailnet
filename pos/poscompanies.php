<?php
/********************************************************************

    poscompanies.php

    Lists of addresses (company addresses)

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_use_posindex");

set_referer("poscompany.php");

$preselect_filter = "";


$country = 1;
if( param("country") > 0)
{
	$country =  param("country");
}
elseif(array_key_exists("country", $_SESSION) and $_SESSION["country"] > 0)
{
	$country = $_SESSION["country"];
	unset($_SESSION["country"]);
}

$preselect_filter = " and address_country = " . $country;
register_param("country", $country);


if(has_access("can_edit_posindex") or has_access("can_view_posindex"))
{
}
elseif(has_access("can_view_his_posindex") or has_access("can_edit_his_posindex"))
{
	if(!param("country"))
	{
		redirect("welcome.php");
	}
	
	$user = get_user(user_id());

	$country_filter = "";
	$tmp = array();
	$sql = "select * from country_access " .
		   "where country_access_user = " . user_id();


	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{            
		$tmp[] = $row["country_access_country"];
	}

	if(count($tmp) > 0) {
		$country_filter = " address_country IN (" . implode(",", $tmp) . ") ";
	}

	if($country_filter) 
	{
		if($preselect_filter)
		{
			$preselect_filter .= " and (address_parent = " . $user["address"] . " or " . $country_filter . ") "; 
		}
		else
		{
			$preselect_filter = " and (address_parent = " . $user["address"] . " or " . $country_filter . ") ";
		}
	}
	else
	{
		if($preselect_filter)
		{
			$preselect_filter .= " and address_parent = " . $user["address"]; 
		}
		else
		{
			$preselect_filter = " and (address_parent = " . $user["address"] . " or address_country in (" . $user_countries . ")) ";
		}
	}
	
}
elseif(!has_access("can_view_his_posindex") 
       and !has_access("can_edit_his_posindex") 
	   and !has_access("can_view_posindex") 
	   and !has_access("can_edit_posindex"))
{
	redirect("welcome.php");
}

$sql = "select address_id, address_company, address_zip, " .
	 "    address_place, country_name, address_type_name, province_canton, " .
	  "IF(address_canbefranchisee = 1 or address_canbefranchisee_worldwide = 1, 'yes', '') as franchisee, " .
	   "IF(address_is_independent_retailer = 1, 'yes', '') as retailer " .
	 "from addresses " . 
	 " left join address_types on address_type_id = address_type " .
	 "left join countries on address_country = country_id " . 
	 "left join places on place_id = address_place_id " .
	 "left join provinces on province_id = place_province";

//prepare PDF-Links
$pdfs = array();
$sql_images = $sql . " where address_showinposindex = 1 and address_active = 1" . $preselect_filter;
$res = mysql_query($sql_images) or dberror($sql_images);
while ($row = mysql_fetch_assoc($res))
{

	$link = "<a href=\"javascript:popup('companysheet_pdf.php?id=" .  $row["address_id"] . "',800,600);\"><img src=\"/pictures/ico_pdf.gif\" border='0'/></a>";
	$pdfs[$row["address_id"]] = $link;
}


/********************************************************************
    Create Form
*********************************************************************/
$address_filter = array();
$address_filter["a"] = "All";
$address_filter["f"] = "Franchisees";
$address_filter["o"] = "Independent retailers";

$form = new Form("addresses", "address");

$form->add_section("List Filter Selection");
$form->add_hidden("country", $country);

if(param("address_filter"))
{
	$form->add_list("address_filter", "Address Filter", $address_filter, SUBMIT | NOTNULL, param("address_filter"));
	$_SESSION["address_filter"] = param("address_filter");
}
elseif(array_key_exists("address_filter", $_SESSION) and $_SESSION["address_filter"] != '')
{
	$form->add_list("address_filter", "Address Filter", $address_filter, SUBMIT | NOTNULL, $_SESSION["address_filter"]);
}
else
{
	$form->add_list("address_filter", "Address Filter", $address_filter, SUBMIT | NOTNULL, "f");
	$_SESSION["address_filter"] = 'f';
}

/********************************************************************
    Create List
*********************************************************************/

if(param('address_filter')) {
	
	if(param('address_filter') == 'f') {
		$list_filter = "address_showinposindex = 1 and address_active = 1 and (address_canbefranchisee = 1 or address_canbefranchisee_worldwide = 1)" . $preselect_filter;
	}
	elseif(param('address_filter') == 'o') {
		$list_filter = "address_showinposindex = 1 and address_active = 1 and address_is_independent_retailer = 1 " . $preselect_filter;
	}
	else {
		$list_filter = "address_showinposindex = 1 and address_active = 1 and (address_canbefranchisee = 1 or address_canbefranchisee_worldwide = 1 or address_is_independent_retailer = 1) " . $preselect_filter;
	}
}
elseif(array_key_exists("address_filter", $_SESSION) and $_SESSION["address_filter"] != '')
{
	
	if($_SESSION["address_filter"] == 'f') {
		$list_filter = "address_showinposindex = 1 and address_active = 1 and (address_canbefranchisee = 1 or address_canbefranchisee_worldwide = 1)" . $preselect_filter;
	}
	elseif($_SESSION["address_filter"] == 'o') {
		$list_filter = "address_showinposindex = 1 and address_active = 1 and address_is_independent_retailer = 1 " . $preselect_filter;
	}
	else {
		$list_filter = "address_showinposindex = 1 and address_active = 1 and (address_canbefranchisee = 1 or address_canbefranchisee_worldwide = 1 or address_is_independent_retailer = 1) " . $preselect_filter;
	}
}
else
{
	$list_filter = "address_showinposindex = 1 and address_active = 1 and (address_canbefranchisee = 1 or address_canbefranchisee_worldwide = 1)" . $preselect_filter;
}


$list = new ListView($sql);

$list->set_entity("addresses");
$list->set_order("country_name, address_company");
$list->set_filter($list_filter);

$list->add_column("country_name", "Country", "", LIST_FILTER_LIST,
    "select country_name from countries order by country_name");
$list->add_column("address_type_name", "Type", "", LIST_FILTER_LIST, "select address_type_name from address_types where and address_type_id <> 7 order by address_type_name");
$list->add_column("address_company", "Company", "poscompany.php?country=" . param("country") . "&address_filter=" . param('address_filter'), LIST_FILTER_FREE);
$list->add_column("franchisee", "Franchisee");
$list->add_column("retailer", "Retailer");
$list->add_column("province_canton", "Province", "", LIST_FILTER_FREE);
$list->add_column("address_zip", "Zip", "", LIST_FILTER_FREE);
$list->add_column("address_place", "City", "", LIST_FILTER_FREE);
$list->add_text_column("pdfs", "Company Sheet", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML, $pdfs);

if(has_access("can_edit_posindex") or has_access("can_edit_his_posindex"))
{
	$list->add_button(LIST_BUTTON_NEW, "New", "poscompany.php");
}

$list->add_button("print1", "Print Company List");

$list->process();

if($list->button("print1"))
{

	
	$link = "poscompanies_print_xls.php" .
			"?co=" . param("country") . "&list=1&address_filter=" . param('address_filter');
	
	redirect($link);
}

/********************************************************************
    Populate and process button clicks
*********************************************************************/ 
$form->populate();
$list->process();

$page = new Page("poscompanies");
require "include/pos_page_actions.php";
$page->header();

$page->title("Companies");
$form->render();
$list->render();


$page->footer();

?>
