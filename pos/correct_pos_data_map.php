<?php
/********************************************************************

    correct_pos_data_map.php

    Show google Map of the POS.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

check_access("can_use_posindex");

$poslocation = array();

$sql = "select *, " . 
   "IF(TIMESTAMPDIFF(MONTH,posaddress_fagrstart,posaddress_fagrend) >0, TIMESTAMPDIFF(MONTH,posaddress_fagrstart,posaddress_fagrend), 'n.a.') as dmonths " .  
	   "from _posaddresses ".
	   "left join countries on country_id = posaddress_country " .
	   "left join addresses on address_id = posaddress_client_id " .
	   "left join agreement_types on agreement_type_id = posaddress_fagagreement_type " . 
	   "where posaddress_id  = " . dbquote(param("id"));


$res = mysql_query($sql) or dberror($sql);

if ($row = mysql_fetch_assoc($res))
{
	$pos = $row;
}

//google map
$api_key = GOOGLE_API_KEY;

$latitude = 20;
$longitude = -10;
$zoom = 2;

if($pos["posaddress_google_lat"])
{
	$latitude = $pos["posaddress_google_lat"];
	$zoom = 13;
}
if($pos["posaddress_google_long"])
{
	$longitude = $pos["posaddress_google_long"];
	$zoom = 13;
}

if(isset($_GET["lat"]))
{
	$latitude = $_GET["lat"];
	$longitude = $_GET["long"];
	$zoom = 13;
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>POS Index - World Map</title>

	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>


	<script type="text/javascript">
		
		function initialize() {
			  var myLatlng = new google.maps.LatLng(<?php echo $latitude;?>,<?php echo $longitude;?>);
			  var mapOptions = {
				zoom: <?php echo $zoom;?>,
				center: myLatlng,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			  }

			  var map = new google.maps.Map(document.getElementById('map'), mapOptions);

			  var contentString = '<div id="content"><br />'+
					<?php echo $latitude;?>+', '+<?php echo $longitude;?>+'</div>';

			  var infowindow = new google.maps.InfoWindow({
				  content: contentString
			  });

			  var marker = new google.maps.Marker({
				  position: myLatlng,
				  map: map,
				  draggable:true
			  });
			  google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(map,marker);
			  });

			  google.maps.event.addListener(marker, 'dragend', function(evt) {
				parent.document.getElementById("posaddress_google_lat").value=evt.latLng.lat();
				parent.document.getElementById("posaddress_google_long").value=evt.latLng.lng();

			  });
			}
			google.maps.event.addDomListener(window, 'load', initialize);
    </script>




	

	<style type="text/css">
  		
		body{
			font-family: Verdana, Geneva, sans-serif;
			font-size: 12px;
			color: #000000;
		}

		a {
				color: #006699;
				text-decoration: none;
				font-weight: bold;
			}


			a:hover {
				color: #FF0000;
				text-decoration: none;
			}
		
		#map{font-family:Arial, Helvetica, sans-serif; }
		
		.clear{ float:none; clear:both; height:0px; line-height:0px; font-size:0px; }
		
		fieldset legend{ margin-bottom:4px; color:#000; }
  
  </style>
</head>

<body>

<br /><br />
Move the marker to get the latitude and longitude coordinates of the POS location.
<br /><br />

<div id="map" style="width:700px; height: 500px"></div>
  </body>
</html>