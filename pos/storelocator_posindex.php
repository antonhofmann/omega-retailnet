<?php
/********************************************************************

    storelocator_posindex.php

    Publish POS Locations at storelocator

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2009-11-07
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2009-11-07
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

check_access("can_correct_pos_data");
set_referer("storelocator_posindex.php");

$user = get_user(user_id());


$preselect_filter = "";
if(param("country"))
{
	$preselect_filter = "posaddress_country = " . param("country");
	register_param("country", param("country"));
}
else
{
	redirect("welcome.php");
}

if(param("province") and $preselect_filter)
{
	$preselect_filter .= " and place_province = " . param("province");
	register_param("province", param("province"));
}
elseif(param("province") and $preselect_filter)
{
	$preselect_filter = "place_province = " . param("province");
	register_param("province", param("province"));
}

if(param("postype") and $preselect_filter)
{
	$preselect_filter .= " and posaddress_store_postype = " . param("postype");
	register_param("postype", param("postype"));
}
elseif(param("postype") and $preselect_filter)
{
	$preselect_filter = "posaddress_store_postype = " . param("postype");
	register_param("postype", param("postype"));
}

if(param("legaltype") and $preselect_filter)
{
	$preselect_filter .= " and posaddress_ownertype = " . param("legaltype");
	register_param("legaltype", param("legaltype"));
}
elseif(param("legaltype") and $preselect_filter)
{
	$preselect_filter = "posaddress_ownertype = " . param("legaltype");
	register_param("legaltype", param("legaltype"));
}


if(has_access("can_edit_posindex") or has_access("can_view_posindex"))
{
}
elseif(has_access("can_view_his_posindex") or has_access("can_edit_his_posindex"))
{
	if(!param("country"))
	{
		redirect("welcome.php");
	}

	$country_filter = "";
	$tmp = array();
	$sql = "select * from country_access " .
		   "where country_access_user = " . user_id();


	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{            
		$tmp[] = $row["country_access_country"];
	}

	if(count($tmp) > 0) {
		$country_filter = " posaddress_country IN (" . implode(",", $tmp) . ") ";
	}

	
	if($country_filter) 
	{
		if($preselect_filter)
		{
			$preselect_filter .= " and (posaddress_country = " . $user["country"] . " or " . $country_filter . ") "; 
		}
		else
		{
			$preselect_filter = " and (posaddress_country = " . $user["country"] . " or " . $country_filter . ") ";
		}
	}
	else
	{
		if($preselect_filter)
		{
			//$preselect_filter .= " and (posaddress_country = " . $user["country"];
		}
		else
		{
			$preselect_filter .= " and (posaddress_country = " . $user["country"];
		}
	}
}

if(!has_access("can_view_his_posindex") 
       and !has_access("can_edit_his_posindex") 
	   and !has_access("can_view_posindex") 
	   and !has_access("can_edit_posindex"))
{
	redirect("welcome.php");
}


//compose list

$sql = "select posaddress_id, if(posaddress_name <> '', posaddress_name, 'n.a.') as posname, " .
       "posaddress_address, posaddress_address2, posaddress_zip, " .
       "    posaddress_place, country_name, project_costtype_text, postype_name, " .
	   "posaddress_google_precision, posaddress_store_closingdate, province_canton " . 
       "from posaddresses " .
	   "left join places on place_id = posaddress_place_id " .
	   "left join provinces on province_id = place_province " .
	   "left join countries on posaddress_country = country_id " . 
	   "left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
	   "left join postypes on postype_id = posaddress_store_postype ";

if(has_access("can_edit_posindex") or has_access("can_view_posindex"))
{
}
elseif(has_access("can_view_his_posindex") or has_access("can_edit_his_posindex"))
{
	
	//$preselect_filter = "(postype_id <> 2 or (postype_id = 2 and project_costtype_id = 1)) and " . $preselect_filter;
}


//get image columns
$onweb = array();
$onweb_values = array();
$onweb_values2 = array();

$sql_u = "select posaddress_id, posaddress_google_precision, posaddress_store_closingdate,  " .
		 "posfile_id, posfile_filegroup, posaddress_export_to_web, posaddress_email_on_web, posaddress_store_openingdate " .
         "from posaddresses " . 
		 "left join places on place_id = posaddress_place_id " .
		 "left join provinces on province_id = place_province " .
		 "left join posfiles on posfile_posaddress = posaddress_id ";

//$sql_u = $sql_u . " where " . $preselect_filter . " and (posfile_filegroup = 1 or posfile_filegroup is null) ";
if($preselect_filter)
{
	$sql_u = $sql_u . " where " . $preselect_filter;
}

$res = mysql_query($sql_u) or dberror($sql_u);
while($row = mysql_fetch_assoc($res))
{
	//$result = update_posdata_from_posorders($row["posaddress_id"]);

	$onweb_values[$row["posaddress_id"]] = $row["posaddress_export_to_web"];
	$onweb_values2[$row["posaddress_id"]] = $row["posaddress_email_on_web"];
}

$preselect_filter .= " and (posaddress_store_closingdate is NULL or posaddress_store_closingdate = '0000-00-00')";

$list = new ListView($sql);

$list->set_entity("posaddresses");
$list->set_filter($preselect_filter);
$list->set_order("country_name, posaddress_place, posaddress_name");

$list->add_hidden("country", param("country"));
$list->add_hidden("province", param("province"));
$list->add_hidden("postype", param("postype"));
$list->add_hidden("legaltype", param("legaltype"));

$list->add_column("country_name", "Country", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);
$list->add_column("province_canton", "Province", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("posaddress_place", "City", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("posname", "POS Name", "" , LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("posaddress_address", "Address", "", LIST_FILTER_FREE);
$list->add_column("postype_name", "POS Type", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);
$list->add_column("project_costtype_text", "Legal Type", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);
$list->add_checkbox_column("posaddress_export_to_web", "POS on Web", COLUMN_ALIGN_CENTER, $onweb_values);
$list->add_checkbox_column("posaddress_email_on_web", "Email on Web", COLUMN_ALIGN_CENTER, $onweb_values2);

$list->add_button("save", "Save");
$list->add_button("back", "Back");

$list->populate();
$list->process();

if($list->button("save"))
{
	foreach($onweb_values as $key=>$value)
	{
		$field = "__posaddresses_posaddress_export_to_web_" . $key;
		if(array_key_exists($field, $_POST))
		{
			$sql_u = "update posaddresses set posaddress_export_to_web = 1 " .
				   " where posaddress_id = " . dbquote($key);
			
		}
		else
		{
			$sql_u = "update posaddresses set posaddress_export_to_web = 0 " .
				   " where posaddress_id = " . dbquote($key);
		}
		$result = mysql_query($sql_u) or dberror($sql_u);

		$field = "__posaddresses_posaddress_email_on_web_" . $key;
		if(array_key_exists($field, $_POST))
		{
			$sql_u = "update posaddresses set posaddress_email_on_web = 1 " .
				   " where posaddress_id = " . dbquote($key);
			
		}
		else
		{
			$sql_u = "update posaddresses set posaddress_email_on_web = 0 " .
				   " where posaddress_id = " . dbquote($key);
		}
		$result = mysql_query($sql_u) or dberror($sql_u);


		
		update_store_locator($key);
	
	}

	



	$link = "storelocator_posindex.php?country=". param("country") . "&province=" . param("province") . "&postype=" . param("postype") . "&legaltype=" . param("legaltype");
	redirect($link);
}
elseif($list->button("back"))
{
	$link = "storelocator_pos_data_preselect.php?country=". param("country");
	redirect($link);
}

$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title("POS Index: Publish POS Locations at " . BRAND_WEBSITE);
$list->render();


$page->footer();

?>
