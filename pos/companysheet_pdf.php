<?php
/********************************************************************

    companysheet_pdf.php

    Creat a POS Overview PDF of a company

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-06-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-06-14
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";
check_access("can_use_posindex");


/********************************************************************
    prepare all data needed
*********************************************************************/
global $page_title;
$page_title = "Company Overview";

$address_id = 0;
if(isset($_GET["id"]))
{
	$address_id = $_GET["id"];
}


$sql = "select * " . 
       "from addresses " . 
	   "left join countries on country_id = address_country " .
	   "left join currencies on currency_id = address_currency " . 
	   "left join address_types on address_type_id = address_type " .
	   "left join client_types on client_type_id = address_client_type " .
	   "left join users on user_id = address_contact " . 
	   "where address_id = " . dbquote($address_id);

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$company = $row["address_company"] . ", " . $row["address_place"] . ", " . $row["country_name"];

	$classification = "";
	if($row["address_canbefranchisee"])
	{
		$classification .= "Franchisee, ";
	}
	if($row["address_canbefranchisee_worldwide"])
	{
		$classification .= "Franchisee of world wide projects, ";
	}
	if($row["address_canbejointventure"])
	{
		$classification .= "Joint Venture Partner, ";
	}
	if($row["address_canbecooperation"])
	{
		$classification .= "Cooperation Partner, ";
	}
	if($row["address_is_independent_retailer"])
	{
		$classification .= BRAND . " Retailer, ";
	}
	$classification = substr($classification, 0, strlen($classification)-2);
	
	/********************************************************************
		prepare pdf
	*********************************************************************/

	require_once('../include/tcpdf/config/lang/eng.php');
	require_once('../include/tcpdf/tcpdf.php');
	
	class MYPDF extends TCPDF
	{
		//Page header
		function Header()
		{
			global $page_title;
			//Logo
			$this->Image('../pictures/logo.jpg',10,8,33);
			//Arial bold 15
			$this->SetFont('arialn','B',12);
			//Move to the right
			$this->Cell(80);
			//Title
			$this->Cell(0,34,$page_title,0,0,'R');
			//Line break
			$this->Ln(20);

		}

		//Page footer
		function Footer()
		{
			//Position at 1.5 cm from bottom
			$this->SetY(-15);
			//arialn italic 8
			$this->SetFont('arialn','I',8);
			//Page number
			$this->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$this->PageNo().'/'.$this->getAliasNbPages(),0,0,'R');
		}

	}

	//Instanciation of inherited class
	$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);
	$pdf->SetMargins(10, PDF_MARGIN_TOP, 11);

	$pdf->Open();


	$pdf->SetFillColor(220, 220, 220); 

	$pdf->AddFont('arialn');
	$pdf->AddFont('arialn', 'B');

	$pdf->AddPage();

	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190,7,"Company: " . $company,1, 0, 'L', 1);
	$pdf->Ln();
	$pdf->SetFont('arialn','',10);
	$pdf->Ln();	
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(30, 5,"Company Name 1",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(65, 5,$row["address_company"],1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(30, 5,"Company Name 2",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(65, 5,$row["address_company2"],1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(30, 5,"Address 1",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(65, 5,$row["address_address"],1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(30, 5,"Address 2",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(65, 5,$row["address_address2"],1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(30, 5,"Zip/City",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(65, 5,$row["address_zip"] . " " . $row["address_place"],1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(30, 5,"Country",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(65, 5,$row["country_name"],1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(30, 5,"Phone",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(65, 5,$row["address_phone"],1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(30, 5,"Fax",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(65, 5,$row["address_fax"],1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(30, 5,"Email",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(65, 5,$row["address_email"],1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(30, 5,"Website",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(65, 5,$row["address_website"],1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(30, 5,"Contact",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(65, 5,$row["user_name"] . " " . $row["user_firstname"],1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(30, 5,"Contact Name",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(65, 5,$row["address_contact_name"],1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(30, 5,"Contact Email",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(65, 5,$row["address_contact_email"],1, 0, 'L', 0);

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(30, 5,"Currency",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(65, 5,$row["currency_symbol"],1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(30, 5,"Address Type",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(65, 5,$row["address_type_name"],1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(30, 5,"Client Type",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(65, 5,$row["client_type_code"],1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(30, 5,"Address Number",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(65, 5,$row["address_number"],1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(30, 5,"Legal Number",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(65, 5,$row["address_legnr"],1, 0, 'L', 0);
	$pdf->Ln();
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(30, 5,"Classifications",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(160, 5,$classification,1, 0, 'L', 0);
	$pdf->Ln();
	$pdf->Ln();

	
	$print_title = 1;

	$sql_f = "select * from posaddresses " .
		     "left join countries on country_id = posaddress_country " . 
		     "where posaddress_franchisee_id = " . $row["address_id"] . 
		     " order by country_name, posaddress_place, posaddress_name";

	$res_f = mysql_query($sql_f) or dberror($sql_f);
	while ($row_f = mysql_fetch_assoc($res_f))
	{
		
		if($print_title == 1)
		{
			$pdf->SetFont('arialn','B',10);
			$pdf->Cell(190, 5,"The Company is Franchisee the context of the following POS Locations",0, 0, 'L', 0);
			$pdf->Ln();
			$pdf->SetFont('arialn','B',8);
			$print_title = 0;
		}

		$posaddress = $row_f["country_name"];

		if($row_f["posaddress_place"])
		{
			$posaddress .= ", " . $row_f["posaddress_place"];
		}

		$posaddress .= ", " .  $row_f["posaddress_name"];

		if($row_f["posaddress_name2"])
		{
			$posaddress .= ", " . $row_f["posaddress_name2"];
		}

		$posaddress .= ", " . $row_f["posaddress_address"];
		
		if(strlen($posaddress) > 120)
		{
			$posaddress = substr($posaddress, 0, 120) . "...";
		}
		
		
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(165, 5,$posaddress,1, 0, 'L', 0);

		if(to_system_date($row_f["posaddress_store_closingdate"]))
		{
			$pdf->Cell(25, 5,"closed: " . to_system_date($row_f["posaddress_store_closingdate"]),1, 0, 'L', 0);
		}
		else
		{
			$pdf->Cell(25, 5,"" ,1, 0, 'L', 0);
		}
		$pdf->Ln();

		if($pdf->getY() > 260)
		{
			$pdf->AddPage();
			$pdf->SetFont('arialn','B',10);
			$pdf->Cell(190,7,"Company: " . $company,1, 0, 'L', 1);
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Cell(190, 5,"The Company is Franchisee the context of the following POS Locations",0, 0, 'L', 0);
			$pdf->SetFont('arialn','',8);
			$pdf->Ln();	
		}

	}

	$print_title = 1;

	$sql_f = "select * from posaddresses " .
		     "left join countries on country_id = posaddress_country " . 
		     "where posaddress_franchisor_id = " . $row["address_id"] . 
		     " order by country_name, posaddress_place, posaddress_name";

	$res_f = mysql_query($sql_f) or dberror($sql_f);
	while ($row_f = mysql_fetch_assoc($res_f))
	{
		
		if($print_title == 1)
		{
			$pdf->SetFont('arialn','B',10);
			$pdf->Cell(190, 5,"The Company is Franchisor the context of the following POS Locations",0, 0, 'L', 0);
			$pdf->Ln();
			$pdf->SetFont('arialn','B',8);
			$print_title = 0;
		}

		$posaddress = $row_f["country_name"];

		if($row_f["posaddress_place"])
		{
			$posaddress .= ", " . $row_f["posaddress_place"];
		}

		$posaddress .= ", " .  $row_f["posaddress_name"];

		if($row_f["posaddress_name2"])
		{
			$posaddress .= ", " . $row_f["posaddress_name2"];
		}

		$posaddress .= ", " . $row_f["posaddress_address"];
		
		if(strlen($posaddress) > 120)
		{
			$posaddress = substr($posaddress, 0, 120) . "...";
		}
		
		
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(165, 5,$posaddress,1, 0, 'L', 0);

		if(to_system_date($row_f["posaddress_store_closingdate"]))
		{
			$pdf->Cell(25, 5,"closed: " . to_system_date($row_f["posaddress_store_closingdate"]),1, 0, 'L', 0);
		}
		else
		{
			$pdf->Cell(25, 5,"" ,1, 0, 'L', 0);
		}
		$pdf->Ln();

		if($pdf->getY() > 260)
		{
			$pdf->AddPage();
			$pdf->SetFont('arialn','B',10);
			$pdf->Cell(190,7,"Company: " . $company,1, 0, 'L', 1);
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Cell(190, 5,"The Company is Franchisor the context of the following POS Locations",0, 0, 'L', 0);
			$pdf->SetFont('arialn','',8);
			$pdf->Ln();	
		}

	}

	

	// write pdf
	$pdf->Output();
}

?>