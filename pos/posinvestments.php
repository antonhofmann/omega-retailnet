<?php
/********************************************************************

    posinvestments.php

    Entry page for the investment section.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

check_access("can_use_posindex");

/********************************************************************
    prepare all data needed
*********************************************************************/
register_param("pos_id");
param("id", param("pos_id"));

$system_currency = get_system_currency_fields();

//check if user can edit this company
$can_edit = false;
if(has_access("can_edit_posindex"))
{
	$can_edit = true;
}
elseif(has_access("can_edit_his_posindex"))
{
	$can_edit = get_user_edit_permission(user_id(), param("pos_id"));
}

$pos = array();
if(param("pos_id") > 0)
{
	$pos = get_poslocation(param("pos_id"), "posaddresses");
}

/********************************************************************
    Header
*********************************************************************/
$form = new Form("posaddresses", "posaddresses");
require_once("include/poslocation_head.php");

$form->add_hidden("ltf", param("ltf"));
$form->add_hidden("let", param("let"));
$form->add_hidden("ostate", param("ostate"));

if(has_access("can_edit_posindex"))
{
	$form->add_section("Local Currency before Euro");
	$form->add_hidden("posaddress_id", param("pos_id"));

	$form->add_edit("posaddress_oldcurrency", "Currency Name*", NOTNULL);
	$form->add_edit("posaddress_oldexchangerate", "Exchange Rate*", NOTNULL, "", TYPE_DECIMAL, 12, 6);
	$form->add_section("Key Money");
	$form->add_checkbox("posaddress_keymoney_recoverable", "recoverable", "", 0, "Key Money");

	$form->add_hidden("country", param("country"));
	$form->add_button("save_form", "Save");
	$form->add_button("back", "Back to POS List");
}
elseif(has_access("can_view_posindex") or has_access("can_view_his_posindex"))
{
	$form->add_section("Local Currency before Euro");
	$form->add_hidden("posaddress_id", param("pos_id"));

	$form->add_label("posaddress_oldcurrency", "Currency Name");
	$form->add_label("posaddress_oldexchangerate", "Exchange Rate");
	$form->add_section("Key Money");
	$form->add_checkbox("posaddress_keymoney_recoverable", "recoverable", "", 0, "Key Money");

	$form->add_hidden("country", param("country"));
	$form->add_button("back", "Back to POS List");
}

$form->populate();
$form->process();

if ($form->button("save_form") and $form->validate())
{
	$sql = "Update posaddresses set " . 
		   "posaddress_oldcurrency = " . dbquote($form->value("posaddress_oldcurrency")) .
		   ", posaddress_oldexchangerate = " . dbquote($form->value("posaddress_oldexchangerate")) .
		   ", posaddress_keymoney_recoverable = " . $form->value("posaddress_keymoney_recoverable") . 
		   " where posaddress_id = " . param("pos_id");
	
	mysql_query($sql) or dberror($sql);

	$link = "posinvestments.php?pos_id=" . param("pos_id") . '&let=' . param('let') . "&ltf=" . param("ltf"). '&ostate=' . param("ostate");
    redirect($link);
}
elseif($form->button("back"))
{
	redirect("posindex.php?country=" . param("country") . '&let=' . param('let') . "&ltf=" . param("ltf"). '&ostate=' . param("ostate"));
}


/********************************************************************
	check an build missing investment records
*********************************************************************/
$result = build_missing_intangible_investment_records(param("pos_id"));
//$result = update_intangible_investment_records_from_system_currency(param("pos_id"));
$result = update_intangible_investment_records_from_local_currency(param("pos_id"));
/********************************************************************
    list of Intangible Assets in system currency
*********************************************************************/
// create sql
$sql_insc = "select * from posinvestments " . 
          "left join posinvestment_types on posinvestment_type_id = posinvestment_investment_type"; 

$list_filter_insc = "posinvestment_type_intangible =1 and posinvestment_posaddress = " . param("pos_id");

$cers = array();
$cmss = array();
$diff = array();
$diffp = array();
$cer_totals = 0;
$cms_totals = 0;
$diff_totals = 0;

$sql = $sql_insc . " where " . $list_filter_insc;
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$cers[$row["posinvestment_id"]] = $row["posinvestment_amount_cer"];
	$cmss[$row["posinvestment_id"]] = $row["posinvestment_amount_cms"];

	$cer_totals = $cer_totals + $row["posinvestment_amount_cer"];
	$cms_totals = $cms_totals + $row["posinvestment_amount_cms"];

	if($row["posinvestment_amount_cer"] - $row["posinvestment_amount_cms"] != 0)
	{
		$diff[$row["posinvestment_id"]] = number_format($row["posinvestment_amount_cms"] - $row["posinvestment_amount_cer"],2,".","'");
		
		if($row["posinvestment_amount_cer"] != 0)
		{
			$diffp[$row["posinvestment_id"]] = ($row["posinvestment_amount_cms"] - $row["posinvestment_amount_cer"]) / $row["posinvestment_amount_cer"];
			$diffp[$row["posinvestment_id"]] = number_format($diffp[$row["posinvestment_id"]]*100,2,".","") ."%";
		}
	}
}

/********************************************************************
    Create List
*********************************************************************/ 
$list_insc = new ListView($sql_insc, LIST_HAS_HEADER | LIST_HAS_FOOTER);
$list_insc->set_title("Intangible Assets in ". $system_currency["symbol"]);
$list_insc->set_entity("posinvestments");
$list_insc->set_order("posinvestment_type_sortorder");
$list_insc->set_filter($list_filter_insc);   

//$link = "posinvestment.php?pos_id=" . param("pos_id");

$list_insc->add_hidden("pos_id", param("pos_id"));
$list_insc->add_column("posinvestment_type_name", "Type", "", "", "", COLUMN_NO_WRAP);

if(has_access("can_edit_posindex"))
{
	$list_insc->add_number_edit_column("cer", "Amount CER", "12", COLUMN_ALIGN_RIGHT, $cers);
	$list_insc->add_number_edit_column("cms", "Real Cost", "12", COLUMN_ALIGN_RIGHT, $cmss);
}
elseif(has_access("can_view_posindex") or has_access("can_view_his_posindex"))
{
	$list_insc->add_text_column("cer", "Amount CER", COLUMN_ALIGN_RIGHT, $cers);
	$list_insc->add_text_column("cms", "Real Cost", COLUMN_ALIGN_RIGHT, $cmss);
}

if(count($diff) > 0)
{
	$list_insc->add_text_column("diff", "Difference", COLUMN_ALIGN_RIGHT, $diff);
	$list_insc->add_text_column("diffp", "in %", COLUMN_ALIGN_RIGHT, $diffp);
}

$list_insc->set_footer("posinvestment_type_name", "Total");
$list_insc->set_footer("cer", number_format($cer_totals,2, ".", "'"));
$list_insc->set_footer("cms", number_format($cms_totals,2, ".", "'"));

if(count($diff) > 0)
{
	$diff_totals = $cms_totals - $cer_totals;
	$list_insc->set_footer("diff", number_format($diff_totals,2, ".", "'"));
}

if($cer_totals != 0)
{
	$difft = ($cms_totals - $cer_totals) / $cer_totals;
	$difft = number_format($difft*100,2,".","'");
	$list_insc->set_footer("diffp", number_format($difft,2, ".", "") ."%");
}

if(has_access("can_edit_posindex"))
{
	$list_insc->add_button("save_insc", "Save List Values");
}
elseif($can_edit == true and has_access("can_edit_his_posindex"))
{
	$list_insc->add_button("save_insc", "Save List Values");
}



/********************************************************************
    list of Intangible Assets in local currency
*********************************************************************/
// create sql

$cers = array();
$cmss = array();
$diff = array();
$diffp = array();
$cer_totals = 0;
$cms_totals = 0;
$diff_totals = 0;


$sql_inlc = "select * from posinvestments " . 
          "left join posinvestment_types on posinvestment_type_id = posinvestment_investment_type"; 

$list_filter_inlc = "posinvestment_type_intangible = 1 and posinvestment_posaddress = " . param("pos_id");

$cers = array();
$cmss = array();

$sql = $sql_inlc . " where " . $list_filter_inlc;
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$cers[$row["posinvestment_id"]] = $row["posinvestment_amount_cer_loc"];
	$cmss[$row["posinvestment_id"]] = $row["posinvestment_amount_cms_loc"];

	$cer_totals = $cer_totals + $row["posinvestment_amount_cer_loc"];
	$cms_totals = $cms_totals + $row["posinvestment_amount_cms_loc"];

	if($row["posinvestment_amount_cer_loc"] - $row["posinvestment_amount_cms_loc"] != 0)
	{
		$diff[$row["posinvestment_id"]] = number_format($row["posinvestment_amount_cms_loc"] - $row["posinvestment_amount_cer_loc"],2,".","'");
		
		if($row["posinvestment_amount_cer_loc"] != 0)
		{
			$diffp[$row["posinvestment_id"]] = ($row["posinvestment_amount_cms_loc"] - $row["posinvestment_amount_cer_loc"]) / $row["posinvestment_amount_cer"];
			$diffp[$row["posinvestment_id"]] = number_format($diffp[$row["posinvestment_id"]]*100,2,".","") ."%";
		}
	}
}

/********************************************************************
    Create List
*********************************************************************/ 
$list_inlc = new ListView($sql_inlc, LIST_HAS_HEADER | LIST_HAS_FOOTER);
$list_inlc->set_title("Intangible Assets in Local Currency");
$list_inlc->set_entity("posinvestments");
$list_inlc->set_order("posinvestment_type_sortorder");
$list_inlc->set_filter($list_filter_inlc);   

//$link = "posinvestment.php?pos_id=" . param("pos_id");

$list_inlc->add_hidden("pos_id", param("pos_id"));
$list_inlc->add_column("posinvestment_type_name", "Type", "", "", "", COLUMN_NO_WRAP);

if(has_access("can_edit_posindex"))
{
	$list_inlc->add_number_edit_column("cer_loc", "Amount CER", "12", COLUMN_ALIGN_RIGHT, $cers);
	$list_inlc->add_number_edit_column("cms_loc", "Real Cost", "12", COLUMN_ALIGN_RIGHT, $cmss);
}
elseif(has_access("can_view_posindex") or has_access("can_view_his_posindex"))
{
	$list_inlc->add_text_column("cer_loc", "Amount CER", COLUMN_ALIGN_RIGHT, $cers);
	$list_inlc->add_text_column("cms_loc", "Real Cost", COLUMN_ALIGN_RIGHT, $cmss);
}

if(count($diff) > 0)
{
	$list_inlc->add_text_column("diff_loc", "Difference", COLUMN_ALIGN_RIGHT, $diff);
	$list_inlc->add_text_column("diffp_loc", "in %", COLUMN_ALIGN_RIGHT, $diffp);
}

$list_inlc->set_footer("posinvestment_type_name", "Total");
$list_inlc->set_footer("cer_loc", number_format($cer_totals,2, ".", "'"));
$list_inlc->set_footer("cms_loc", number_format($cms_totals,2, ".", "'"));

if(count($diff) > 0)
{
	$diff_totals = $cms_totals - $cer_totals;
	$list_inlc->set_footer("diff_loc", number_format($diff_totals,2, ".", "'"));
}

if($cer_totals != 0)
{
	$difft = ($cms_totals - $cer_totals) / $cer_totals;
	$difft = number_format($difft*100,2,".","");
	$list_inlc->set_footer("diffp_loc", number_format($difft,2, ".", "") ."%");
}

if(has_access("can_edit_posindex"))
{
	$list_inlc->add_button("save_inlc", "Save List Values");
}
elseif($can_edit == true and has_access("can_edit_his_posindex"))
{
	$list_inlc->add_button("save_inlc", "Save List Values");
}


/********************************************************************
    Populate lists and process button clicks
*********************************************************************/ 
//system currency
$list_insc->populate();
$list_insc->process();

//local currency
$list_inlc->populate();
$list_inlc->process();

//process buttons system currency
if ($list_insc->button("save_insc"))
{
   foreach ($list_insc->values("cer") as $key=>$value)
   {
		$fields = array();
		if($value > 0 and (is_int_value($value) or is_decimal_value($value, 12, 2)))
		{
			$fields[] = "posinvestment_amount_cer = " . dbquote($value);
		}
		else
		{
			$fields[] = "posinvestment_amount_cer = NULL";
		}
		$fields[] = "user_modified = " . dbquote(user_login());
		$fields[] = "date_modified = current_timestamp";

		$sql = "update posinvestments set " . join(", ", $fields) . " where posinvestment_id = " . $key;
		mysql_query($sql) or dberror($sql);
   }

   foreach ($list_insc->values("cms") as $key=>$value)
   {
		$fields = array();
		if($value > 0 and (is_int_value($value) or is_decimal_value($value, 12, 2)))
		{
			$fields[] = "posinvestment_amount_cms = " . dbquote($value);
		}
		else
		{
			$fields[] = "posinvestment_amount_cms = NULL";
		}
		$fields[] = "user_modified = " . dbquote(user_login());
		$fields[] = "date_modified = current_timestamp";

		$sql = "update posinvestments set " . join(", ", $fields) . " where posinvestment_id = " . $key;
		mysql_query($sql) or dberror($sql);
   }

   $result = update_intangible_investment_records_from_system_currency(param("pos_id"));

   $link = "posinvestments.php?pos_id=" . param("pos_id");
   redirect($link);
}



//process buttons local currency
if ($list_inlc->button("save_inlc"))
{
   foreach ($list_inlc->values("cer_loc") as $key=>$value)
   {
		$fields = array();
		if(is_int_value($value) or is_decimal_value($value, 12, 2))
		{
			$fields[] = "posinvestment_amount_cer_loc = " . dbquote($value);
		}
		else
		{
			$fields[] = "posinvestment_amount_cer_loc = NULL";
		}
		$fields[] = "user_modified = " . dbquote(user_login());
		$fields[] = "date_modified = current_timestamp";

		$sql = "update posinvestments set " . join(", ", $fields) . " where posinvestment_id = " . $key;
		mysql_query($sql) or dberror($sql);
   }


   foreach ($list_inlc->values("cms_loc") as $key=>$value)
   {
		$fields = array();
		if(is_int_value($value) or is_decimal_value($value, 12, 2))
		{
			$fields[] = "posinvestment_amount_cms_loc = " . dbquote($value);
		}
		else
		{
			$fields[] = "posinvestment_amount_cms_loc = NULL";
		}
		$fields[] = "user_modified = " . dbquote(user_login());
		$fields[] = "date_modified = current_timestamp";

		$sql = "update posinvestments set " . join(", ", $fields) . " where posinvestment_id = " . $key;
		mysql_query($sql) or dberror($sql);
   }

   $result = update_intangible_investment_records_from_local_currency(param("pos_id"));

   $link = "posinvestments.php?pos_id=" . param("pos_id");
   redirect($link);
}


/********************************************************************
    create page
*********************************************************************/
$page = new Page("posindex");
require "include/pos_page_actions.php";
$page->header();
$page->title("Investments: " . $poslocation["posaddress_name"]);

require_once("include/tabs.php");

$form->render();
echo "<p>&nbsp;</p>";
$list_insc->render();
echo "<p>&nbsp;</p>";
$list_inlc->render();
$page->footer();
?>