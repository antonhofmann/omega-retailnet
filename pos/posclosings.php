<?php
/********************************************************************

    posclosings.php

    Entry page for closing dates

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2011-06-15
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2011-06-15
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

check_access("can_use_posindex");

set_referer("poslease.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
register_param("pos_id");

//check if user can edit this company
$can_edit = false;
if(has_access("can_edit_posindex"))
{
	$can_edit = true;
}
elseif(has_access("can_edit_his_posindex"))
{
	
	$user = get_user(user_id());

	$sql = "select posaddress_client_id " .
		   "from posaddresses " .
		   "where posaddress_id = " . param("pos_id") . 
		   "   and posaddress_client_id = " . $user["address"];

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);

	if($user["address"] == $row["posaddress_client_id"])
	{
		$can_edit = true;
	}
}

$pos = array();
if(param("pos_id") > 0)
{
	$pos = get_poslocation(param("pos_id"), "posaddresses");
}


//get closing assessment
$assessment_link = "";
$sql = "select posclosingassessment_id, posclosingassessment_filesigned " .
       "from posclosingassessments " . 
	   " where posclosingassessment_posaddress_id = " . param("pos_id");
$res = mysql_query($sql) or dberror($sql);

if($row = mysql_fetch_assoc($res))
{
	
	if($row["posclosingassessment_filesigned"])
	{
		$assessment_link = "<a href=\"javascript:popup('pos_closing_assessment_pdf.php?id=" .  $row["posclosingassessment_id"] . "&b=2',800,600);\">Download Closing Assessment</a>";

		
	}
	else
	{
		$assessment_link = "<a href=\"javascript:popup('pos_closing_assessment_pdf.php?id=" .  $row["posclosingassessment_id"] . "&b=1',800,600);\">Download Closing Assessment</a>";
	}
}

/********************************************************************
    Header
*********************************************************************/
$form = new Form("posleases", "posleases");
require_once("include/poslocation_head.php");

$form->add_hidden("country", param("country"));
$form->add_hidden("ltf", param("ltf"));
$form->add_hidden("let", param("let"));
$form->add_hidden("ostate", param("ostate"));

if($assessment_link)
{
	$form->add_section("Closing Assessment");
	$form->add_label("closing_assessment", "Closing Assessment", RENDER_HTML, $assessment_link);
}

$form->populate();
$form->process();


/********************************************************************
    list
*********************************************************************/
// create sql
$sql = "select posclosing_id, posclosing_reason, project_number, " . 
	   "DATE_FORMAT(posclosing_startdate,'%d.%m.%Y') as start, " .
	   "DATE_FORMAT(posclosing_enddate,'%d.%m.%Y') as end " .
       "from posclosings " .
	   "left join projects on project_id = posclosing_project ";

$list_filter = "posclosing_posaddress = " . param("pos_id");

/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);
$list->set_entity("posclosings");
$list->set_order("posclosing_startdate");
$list->set_filter($list_filter);   

$link = "posclosing.php?pos_id=" . param("pos_id") . "&country=" . param("country"). '&let=' . param('let') . "&ltf=" . param("ltf"). "&ostate=" . param("ostate");

$list->add_hidden("pos_id", param("pos_id"));
$list->add_column("project_number", "Project", $link, "", "", COLUMN_NO_WRAP);
$list->add_column("start", "Start Date", "", LIST_FILTER_LIST, "", COLUMN_NO_WRAP);
$list->add_column("end", "End Date", "", "", "", COLUMN_NO_WRAP);
$list->add_column("posclosing_reason", "Reason", "", "", "", COLUMN_NO_WRAP);

if(has_access("can_edit_pos_closings"))
{
	$list->add_button("new", "New", $link);
}

$list->add_button("back", "Back to POS List");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();

if($list->button("back"))
{
	redirect("posindex.php?country=" . param("country") . '&let=' . param('let') . "&ltf=" . param("ltf"). '&ostate=' . param("ostate"));
}
elseif($list->button("new"))
{
	redirect($link);
}


/********************************************************************
    create page
*********************************************************************/
$page = new Page("posindex");
require "include/pos_page_actions.php";
$page->header();


$page->title("Temporary Closures: " . $poslocation["posaddress_name"]);

require_once("include/tabs.php");

$form->render();

$list->render();
$page->footer();
?>