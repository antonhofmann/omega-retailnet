<?php
/********************************************************************

    posworldmap_print.php

    Create Map of POS Locations

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-06-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-06-14
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";
check_access("can_use_posindex");


/********************************************************************
    prepare all data needed
*********************************************************************/
$api_key = GOOGLE_API_KEY;

//get parameters
$filter = "";
$only_regiion = true;

if(array_key_exists("r", $_GET) and $_GET["r"] > 0)
{
	$filter = "country_salesregion = " . $_GET["r"]; 
}

if(array_key_exists("c", $_GET) and $_GET["c"] > 0)
{
	if($filter)
	{
		$filter .= " and posaddress_country = " . $_GET["c"];
	}
	else
	{
		$filter = "posaddress_country = " . $_GET["c"]; 
	}
}


if(array_key_exists("t", $_GET) and $_GET["t"] > 0)
{
	if($filter)
	{
		$filter .= " and posaddress_store_postype = " . $_GET["t"];
	}
	else
	{
		$filter = "posaddress_store_postype = " . $_GET["t"]; 
	}
	$only_regiion = false;
}

if(array_key_exists("l", $_GET) and $_GET["l"] > 0)
{
	if($filter)
	{
		$filter .= " and posaddress_ownertype = " . $_GET["l"];
	}
	else
	{
		$filter = "posaddress_ownertype = " . $_GET["l"]; 
	}
}

if(array_key_exists("o", $_GET) and $_GET["o"] > 0)
{
	if($filter)
	{
		$filter .= " and (posaddress_store_closingdate = '0000-00-00' or posaddress_store_closingdate is null)";
	}
	else
	{
		$filter = "posaddress_store_closingdate = '0000-00-00' or posaddress_store_closingdate is null"; 
	}
}



if($filter) 
{
	$filter = "where " . $filter;
}

$sql = "select posaddress_id, posaddress_name, posaddress_name2, posaddress_address, posaddress_address2, " .
	   "posaddress_zip, posaddress_place, " . 
	   "posaddress_store_postype, posaddress_ownertype, posaddress_store_closingdate, " . 
	   "posaddress_google_lat, posaddress_google_long " .
	   "from posaddresses " . 
	   "left join countries on country_id = posaddress_country " . 
	   $filter;


//get markes
$markers = array();
$marker_icons = array();
$marker_icons[1] = 'pictures/marker_store_corporate.png';
$marker_icons[2] = 'pictures/marker_sis_corporate.png';
$marker_icons[3] = 'pictures/marker_kiosk_corporate.png';
$marker_icons[4] = 'pictures/marker_store_franchisee.png';
$marker_icons[5] = 'pictures/marker_sis_franchisee.png';
$marker_icons[6] = 'pictures/marker_kiosk_franchisee.png';
$marker_icons[7] = 'pictures/marker_store_closed.png';
$marker_icons[8] = 'pictures/marker_sis_closed.png';
$marker_icons[9] = 'pictures/marker_kiosk_closed.png';
$marker_icon_types = array();

$marker_icon_legends = array();
$marker_icon_legends[1] = 'Corporate Store&nbsp;&nbsp;&nbsp;';
$marker_icon_legends[2] = 'Corporate SIS&nbsp;&nbsp;&nbsp;';
$marker_icon_legends[3] = 'Corporate Kiosk&nbsp;&nbsp;&nbsp;';
$marker_icon_legends[4] = 'Franchisee Store&nbsp;&nbsp;&nbsp;';
$marker_icon_legends[5] = 'Franchisee SIS&nbsp;&nbsp;&nbsp;';
$marker_icon_legends[6] = 'Franchisee Kiosk<br />';
$marker_icon_legends[7] = 'Closed Store&nbsp;&nbsp;&nbsp;';
$marker_icon_legends[8] = 'Closed SIS&nbsp;&nbsp;&nbsp;';
$marker_icon_legends[9] = 'Closed Kiosk&nbsp;&nbsp;&nbsp;';



$res = mysql_query($sql);
$i = 0;
while ($row = mysql_fetch_assoc($res)) 
{
	if($row["posaddress_google_lat"] != 0 and $row["posaddress_google_long"] != 0)
	{
		$markers[$i] = array("lat"=>$row["posaddress_google_lat"], "long"=>$row["posaddress_google_long"]);
		if($row["posaddress_store_closingdate"] == NULL or $row["posaddress_store_closingdate"] == '0000-00-00')
		{
			
			if($row["posaddress_ownertype"] == 1)
			{
				if($row["posaddress_store_postype"] == 1)
				{
					$marker_icon_types[$i] = $marker_icons[1];
				}
				elseif($row["posaddress_store_postype"] == 2)
				{
					$marker_icon_types[$i] = $marker_icons[2];
				}
				else
				{
					$marker_icon_types[$i] = $marker_icons[3];
				}
			}
			else
			{
				if($row["posaddress_store_postype"] == 1)
				{
					$marker_icon_types[$i] = $marker_icons[4];
				}
				elseif($row["posaddress_store_postype"] == 2)
				{
					$marker_icon_types[$i] = $marker_icons[5];
				}
				else
				{
					$marker_icon_types[$i] = $marker_icons[6];
				}
			}
		}
		else
		{
			if($row["posaddress_store_postype"] == 1)
			{
				$marker_icon_types[$i] = $marker_icons[7];
			}
			elseif($row["posaddress_store_postype"] == 2)
			{
				$marker_icon_types[$i] = $marker_icons[8];
			}
			else
			{
				$marker_icon_types[$i] = $marker_icons[9];
			}
			//echo $row["posaddress_name"] . " " . $i . " ";
		}
	}
	
	$i++;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>POS Index - World Map</title>

	<script language="javascript" src="../include/main.js" type="text/javascript"></script>
	<script src="http://maps.google.com/maps?file=api&v=2.105&key=<?php echo $api_key;?>" type="text/javascript"></script>
    <script type="text/javascript">
		var map;
		var geocoder;

		geocoder = new GClientGeocoder();
		
		var icon = new GIcon(); 
		icon.iconAnchor = new GPoint(6, 20);

		function load() {
		  if (GBrowserIsCompatible()) 
		  {
			map = new GMap2(document.getElementById("map"));
			map.setCenter(new GLatLng(20.00, 12.00), 2);
			map.enableContinuousZoom();
			map.enableScrollWheelZoom();

			map.clearOverlays();

			var bounds = new GLatLngBounds();
			<?php
			foreach($markers as $key=>$cords)
			{
				echo "icon.image = '" . $marker_icon_types[$key] . "';";
				echo "var point = new GLatLng(" . $cords["lat"] . "," . $cords["long"] . ");";
				echo "map.addOverlay(new GMarker(point, icon));";
				echo "bounds.extend(point);";
			}
			
			
			if($filter and $only_regiion == false)
			{
			?>
				map.setCenter(bounds.getCenter(), map.getBoundsZoomLevel(bounds));
			<?php
			}
			?>
		}
		
	   }
  </script>

  <style type="text/css" media="screen">
		body{font-family: Verdana, Geneva, sans-serif;font-size: 12px;color: #000;}
		a{color: #006699;text-decoration: none;font-weight: bold;}
		a:hover{color: #FF0000;text-decoration: none;}
		#printtitle{
			display:block;
		}
  </style>
  <style type="text/css" media="print">
		body{font-family: Verdana, Geneva, sans-serif;font-size: 12px;color: #000000;}
		#printlink{
			display:none;
		}
		a{display:none;}
		#printtitle{
			display:block;
		}
  </style>

  </head>

  <body onload="load()" onunload="GUnload()">
	
	<div id="printlink"><a href="#" onclick="javascript:print();">Print this Map</a></div>
	
	<br />
	<div id="map" style="width: 8.25in;height:6in;"></div>


	<?php
	//compoase legend
		if(array_key_exists("o", $_GET) and $_GET["o"] > 0)
		{
			unset($marker_icons[7]);
			unset($marker_icons[8]);
			unset($marker_icons[9]);
		}
		if(array_key_exists("l", $_GET) and $_GET["l"] == 1)
		{
			unset($marker_icons[4]);
			unset($marker_icons[5]);
			unset($marker_icons[6]);
		}
		if(array_key_exists("l", $_GET) and $_GET["l"] == 2)
		{
			unset($marker_icons[1]);
			unset($marker_icons[2]);
			unset($marker_icons[3]);
		}
		if(array_key_exists("t", $_GET) and $_GET["t"] == 1)
		{
			unset($marker_icons[2]);
			unset($marker_icons[5]);
			unset($marker_icons[8]);

			unset($marker_icons[3]);
			unset($marker_icons[6]);
			unset($marker_icons[9]);
		}
		if(array_key_exists("t", $_GET) and $_GET["t"] == 2)
		{
			unset($marker_icons[1]);
			unset($marker_icons[4]);
			unset($marker_icons[7]);

			unset($marker_icons[3]);
			unset($marker_icons[6]);
			unset($marker_icons[9]);

			
			
		}
		if(array_key_exists("t", $_GET) and $_GET["t"] == 3)
		{
			unset($marker_icons[1]);
			unset($marker_icons[4]);
			unset($marker_icons[7]);	

			unset($marker_icons[2]);
			unset($marker_icons[5]);
			unset($marker_icons[8]);
		}
    ?>
		<div><br /><br />
	<?php
		foreach($marker_icons as $key=>$icon)
		{
			echo '<img src="' . str_replace(".png", "_small.png", $icon) . '" />&nbsp;' . $marker_icon_legends[$key];
		}
	?>
	</div>
  </body>
</html>