<?php
/********************************************************************

    posopeninghrs.php

    Lists of addresses (POS)

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2013-01-03
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2013-01-03
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

if(!has_access("can_edit_pos_opening_hours") and !has_access("can_view_pos_opening_hours"))
{
	redirect("/pos");
}

$user = get_user(user_id());

$postype_filter = array();
$postype_filter["all"] = "All";
$sql = "select * from postypes order by postype_name";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$postype_filter[$row["postype_id"]] = $row["postype_name"];
}


$preselect_filter = "";
if(param("country"))
{
	$preselect_filter = "posaddress_country = " . param("country");
	register_param("country", param("country"));
}
else
{
	redirect("welcome.php");
}

if(param("province") and $preselect_filter)
{
	$preselect_filter .= " and place_province = " . param("province");
	register_param("province", param("province"));
}
elseif(param("province"))
{
	$preselect_filter = "place_province = " . param("province");
	register_param("province", param("province"));
}


$preselect_filter .= " and (posaddress_store_closingdate = '0000-00-00' or posaddress_store_closingdate is null) ";



if(has_access("can_edit_posindex") or has_access("can_view_posindex"))
{

}
elseif(has_access("can_view_his_posindex") or has_access("can_edit_his_posindex"))
{
	if(!param("country"))
	{
		redirect("welcome.php");
	}

	$country_filter = "";
	$tmp = array();
	$sql = "select * from country_access " .
		   "where country_access_user = " . user_id();


	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{            
		$tmp[] = $row["country_access_country"];
	}

	if(count($tmp) > 0) {
		$country_filter = " posaddress_country IN (" . implode(",", $tmp) . ") ";
	}

	if($country_filter) 
	{
		if($preselect_filter)
		{
			$preselect_filter .= " and (posaddress_country = " . $user["country"] . " or " . $country_filter . ") "; 
		}
		else
		{
			$preselect_filter = " and (posaddress_country = " . $user["country"] . " or " . $country_filter . ") ";
		}
	}
	else
	{
		if($preselect_filter)
		{
			//$preselect_filter .= " and (posaddress_country = " . $user["country"];
		}
		else
		{
			$preselect_filter .= " and (posaddress_country = " . $user["country"];
		}
	}
}

if(!has_access("can_view_his_posindex") 
       and !has_access("can_edit_his_posindex") 
	   and !has_access("can_view_posindex") 
	   and !has_access("can_edit_posindex"))
{
	redirect("welcome.php");
}


//compose list

$sql = "select posaddress_id, if(posaddress_name <> '', posaddress_name, 'n.a.') as posname, " .
       "posaddress_address, posaddress_address2, posaddress_zip, " .
       "    posaddress_place, country_name, project_costtype_text, postype_name, " .
	   "posaddress_google_precision, posaddress_store_closingdate, province_canton, posaddress_country " . 
       "from posaddresses " .
	   "left join places on place_id = posaddress_place_id " .
	   "left join provinces on province_id = place_province " .
	   "left join countries on posaddress_country = country_id " . 
	   "left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
	   "left join postypes on postype_id = posaddress_store_postype ";

if(has_access("can_edit_posindex") or has_access("can_view_posindex"))
{
}
elseif(has_access("can_view_his_posindex") or has_access("can_edit_his_posindex"))
{
	
	//$preselect_filter = "(postype_id <> 2 or (postype_id = 2 and project_costtype_id = 1)) and " . $preselect_filter;
}


//get image columns
$onweb = array();
$opening_hours_indicated = array();

$sql_u = "select posaddress_id, posaddress_country, posaddress_google_precision, posaddress_store_closingdate,  " .
		 "posfile_id, posfile_filegroup, posaddress_export_to_web, place_name, posaddress_address, posaddress_address2, posaddress_zip, posaddress_google_lat, posaddress_google_long, " . 
		 "posclosingassessment_id, posclosingassessment_filesigned " .
         "from posaddresses " . 
		 "left join places on place_id = posaddress_place_id " .
		 "left join provinces on province_id = place_province " .
		 "left join posfiles on posfile_posaddress = posaddress_id " . 
		 "left join posclosingassessments on posclosingassessment_posaddress_id = posaddress_id ";

//$sql_u = $sql_u . " where " . $preselect_filter . " and (posfile_filegroup = 1 or posfile_filegroup is null) ";
if($preselect_filter)
{
	$sql_u = $sql_u . " where " . $preselect_filter;
}

$res = mysql_query($sql_u) or dberror($sql_u);
while($row = mysql_fetch_assoc($res))
{
	//$result = update_posdata_from_posorders($row["posaddress_id"]);

	
	if($row["posaddress_export_to_web"] == 1 and ($row["posaddress_store_closingdate"] == NULL or $row["posaddress_store_closingdate"] == "0000-00-00"))
	{
		$onweb[$row["posaddress_id"]] = "<img src=\"/pictures/bullet_ball_glass_green.gif\" border='0'/>";
	}


	$sql_o = "select count(posopeninghr_id) as num_recs from posopeninghrs " . 
		     " where posopeninghr_posaddress_id = " . $row["posaddress_id"];

	$res_o = mysql_query($sql_o) or dberror($sql_o);
	$row_o = mysql_fetch_assoc($res_o);
	if($row_o["num_recs"] > 0)
	{
		$opening_hours_indicated[$row["posaddress_id"]] = "<img src=\"/pictures/bullet_ball_glass_green.gif\" border='0'/>";
	}

}



if(param('ltf')) 
{
	if($preselect_filter) {
		
		if(param('ltf') == "all") {
		}
		else
		{
			$preselect_filter .= " and posaddress_store_postype = " . param('ltf');
		}
	}
	else
	{
		if(param('ltf') == "all") {
		}
		else
		{
			$preselect_filter = "posaddress_store_postype = " . param('ltf');
		}
	}
}
else
{
	if($preselect_filter) {
		$preselect_filter .= " and posaddress_store_postype <> 4";
	}
	else
	{
		$preselect_filter = "posaddress_store_postype <> 4";
	}
}


/********************************************************************
    Create Form
*********************************************************************/

$form = new Form("posaddresses", "posaddress");

$form->add_section("List Filter Selection");

$form->add_list("ltf", "POS Type Filter", $postype_filter, SUBMIT | NOTNULL, param('ltf'));



$list = new ListView($sql);

$list->set_entity("posaddresses");
$list->set_filter($preselect_filter);
$list->set_order("country_name, posaddress_place, posaddress_name");

$list->add_hidden("country", param("country"));
$list->add_hidden("province", param("province"));


$list->add_column("country_name", "Country", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);
$list->add_column("province_canton", "Province", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("posaddress_place", "City", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("posname", "POS Name", "posopeninghr.php?country=" . param("country") . '&let=' . param('let') . '&ltf=' . param("ltf"), LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("posaddress_address", "Address", "", LIST_FILTER_FREE);
$list->add_column("postype_name", "POS Type", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);
$list->add_column("project_costtype_text", "Legal Type", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);

$list->add_text_column("web", "Web", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML, $onweb);
$list->add_text_column("ohrs", "Opening hrs", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML, $opening_hours_indicated);


$list->add_button(FORM_BUTTON_BACK, "Back");
$list->populate();
$list->process();



$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title("POS Index: Opening Hours");
$form->render();
$list->render();


$page->footer();

?>
