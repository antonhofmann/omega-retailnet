<?php
/********************************************************************

    loc_place_place.php

    Creation and mutation of places translations.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");


$place_id = 0;

if(param("place_id"))
{
	$place_id = param("place_id");
}

//get place name
$place_name = "";
$sql = "select * " . 
       "from places " .
	   "left join countries on country_id = place_country " .
	   "left join provinces on province_id = place_province " . 
	   "where place_id = " . $place_id;
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	
	$place_name = $row["place_name"];
	
	$place_country = $row["place_country"];
	$country_name = $row["place_name"];
	$place_province = $row["place_province"];
	$province_name = $row["province_canton"];
}

$form = new Form("loc_places", "place");

$form->add_section("Basic Data");
$form->add_hidden("loc_place_country", $place_country);
$form->add_hidden("loc_place_province", $place_province);
$form->add_hidden("place_id", $place_id);
$form->add_hidden("loc_place_place", $place_id);
$form->add_label("place_country", "Country", 0, $country_name);
$form->add_label("place_province", "Province", 0, $province_name);
$form->add_label("place_name", "Place Name", 0, $place_name);

$form->add_section("Translation");
$form->add_list("loc_place_language", "Language*", 
    "select language_id, language_name from languages order by language_name", NOTNULL);

$form->add_edit("loc_place_name", "Place Name*", NOTNULL);

$form->add_button("save", "Save");
$form->add_button("back", "Back");
$form->add_button("delete", "Delete");

$form->populate();
$form->process();

if($form->button("back"))
{
	$link = "loc_place.php?id= " . param("place_id");
	redirect($link);
}
elseif($form->button("save"))
{
	if($form->validate())
	{
		$form->save();
		$link = "loc_place.php?id= " . param("place_id");
		redirect($link);
	}
}
elseif($form->button("delete"))
{
		$sql = "delete from loc_places where loc_place_id =" . id();
		$result = mysql_query($sql) or dberror($sql);
		$link = "loc_place.php?id= " . param("place_id");
		redirect($link);

}

$page = new Page("locales");
$page->header();
$page->title(id() ? "Edit place Translation" : "Add place Translation");
$form->render();
$page->footer();

?>