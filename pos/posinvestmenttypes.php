<?php
/********************************************************************

    posinvestment_types.php

    Lists store investment types for editing.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_administrate_posindex");
set_referer("posinvestmenttype.php");

$currency = get_system_currency_fields();


$sql = "select posinvestment_type_id, posinvestment_type_group, posinvestment_type_name, " .
	   "if(posinvestment_type_intangible = 1, 'Intangible', 'Investment') as intangible " . 
       "from posinvestment_types ";

$list = new ListView($sql);

$list->set_entity("posinvestment_types");
$list->set_order("posinvestment_type_sortorder");
$list->set_sorter("posinvestment_type_sortorder");

$list->add_column("posinvestment_type_name", "Name", "posinvestmenttype.php");
$list->add_column("posinvestment_type_group", "Group");
$list->add_column("intangible", "Type");


$list->add_button(LIST_BUTTON_NEW, "New", "posinvestmenttype.php");

$list->process();

$page = new Page("posinvestmenttypes");

$page->header();
$page->title("Investment Types");
$list->render();
$page->footer();

?>
