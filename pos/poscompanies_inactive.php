<?php
/********************************************************************

    addresses_inactive.php

    Lists of addresses (company addresses)

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_use_posindex");
check_access("can_edit_posindex");

set_referer("poscompany.php");


//compose list
$list = new ListView("select address_id, address_company, address_zip, " .
                     "    address_place, country_name, address_type_name, province_canton " .
                     "from addresses " . 
					 " left join address_types on address_type_id = address_type " .
					 "left join countries on address_country = country_id " . 
					 "left join places on place_id = address_place_id " .
					 "left join provinces on province_id = place_province");

$list->set_entity("addresses");
$list->set_order("country_name, address_company");
$list->set_filter("address_showinposindex = 1 and address_active <> 1");

$list->add_column("country_name", "Country", "", LIST_FILTER_LIST,
    "select country_name from countries order by country_name");
$list->add_column("address_type_name", "Type", "", LIST_FILTER_LIST, "select address_type_name from address_types where (address_type_id = 7 or address_type_id = 1) order by address_type_name");
$list->add_column("address_company", "Company", "poscompany.php", LIST_FILTER_FREE);
$list->add_column("province_canton", "Province", "", LIST_FILTER_FREE);
$list->add_column("address_zip", "Zip", "", LIST_FILTER_FREE);
$list->add_column("address_place", "City", "", LIST_FILTER_FREE);


$list->add_button(LIST_BUTTON_FILTER, "Filter");
$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove Filter");

$list->process();

$page = new Page("poscompanies_inactive");
require "include/pos_page_actions.php";
$page->header();

$page->title("Companies Inactive");
$list->render();


$page->footer();

?>
