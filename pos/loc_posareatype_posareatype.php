<?php
/********************************************************************

    loc_posareatype_posareatype.php

    Creation and mutation of POS area types translations.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-16
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");


$posareatype_id = 0;

if(param("posareatype_id"))
{
	$posareatype_id = param("posareatype_id");
}

//get posareatype name
$posareatype_name = "";
$sql = "select posareatype_name from posareatypes where posareatype_id = " . $posareatype_id;
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$posareatype_name = $row["posareatype_name"];
}

$form = new Form("loc_posareatypes", "posareatype");

$form->add_section("Basic Data");
$form->add_hidden("posareatype_id", $posareatype_id);
$form->add_hidden("loc_posareatype_posareatype", $posareatype_id);
$form->add_label("posareatype_name", "POS Area Type", 0, $posareatype_name);

$form->add_section("Translation");
$form->add_list("loc_posareatype_language", "Language*", 
    "select language_id, language_name from languages order by language_name", NOTNULL);

$form->add_edit("loc_posareatype_name", "POS Area Type*", NOTNULL);

$form->add_button("save", "Save");
$form->add_button("back", "Back");
$form->add_button("delete", "Delete");

$form->populate();
$form->process();

if($form->button("back"))
{
	$link = "loc_posareatype.php?id= " . param("posareatype_id");
	redirect($link);
}
elseif($form->button("save"))
{
	if($form->validate())
	{
		$form->save();
		$link = "loc_posareatype.php?id= " . param("posareatype_id");
		redirect($link);
	}
}
elseif($form->button("delete"))
{
		$sql = "delete from loc_posareatypes where loc_posareatype_id =" . id();
		$result = mysql_query($sql) or dberror($sql);
		$link = "loc_posareatype.php?id= " . param("posareatype_id");
		redirect($link);

}

$page = new Page("locales");
$page->header();
$page->title(id() ? "Edit POS Area Type Translation" : "Add POS Area Type Translation");
$form->render();
$page->footer();

?>