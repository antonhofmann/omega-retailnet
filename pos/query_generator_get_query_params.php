<?php
/********************************************************************

    query_generator_get_query_params.php

    Get Query Params

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2009-01-11
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2009-01-11
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

$captions = array();
$sql = "";
$outputfields = "";
$order_clause = "";
$count_records = 0;
$total_records = 0;
$total_records_per_group = 0;

$sql = "select * from posqueries where posquery_id =" . $query_id;
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$print_query_filter = $row["posquery_print_filter"];
	$posquery_field_order = $row["posquery_field_order"];
	$selected_field_order = decode_field_array($posquery_field_order);

	$posquery_area_perception_filter = "";
	$posquery_area_perception = explode  ("@@" , $row["posquery_area_perception"]);
	if(array_key_exists(1,$posquery_area_perception))
	{
		$posquery_area_perception_filter = $posquery_area_perception[1];
	}
	
	$db_info_pos = query_posaddress_fields();
	$db_info_posleases = query_posleases_fields();
	$db_info_posclosings = query_posclosings_fields();
	$db_info_client = query_clientaddress_fields();
	$db_info_franchisee = query_franchiseeaddress_fields();
	$db_info_distributionchannels = query_distribution_channel_fields();
	$db_info_statistics = query_statistics_fields();

		
	//check if table posorders is involved
	$table_posorders_involved = 0;
	foreach($selected_field_order as $key=>$field)
	{
		if(substr($field, 0, 10) == 'posorders.')
		{
			$table_posorders_involved = 1;
		}
	}
	$db_info_posorder = query_posorder_fields($table_posorders_involved);
	$db_info_latest_posorder = query_latest_posorder_fields($table_posorders_involved);

	
	$db_info_intagibles = query_intangible_fields();

	$db_info["fields"] = array_merge($db_info_pos["fields"], $db_info_client["fields"], $db_info_franchisee["fields"], $db_info_posorder["fields"], $db_info_latest_posorder["fields"], $db_info_intagibles["fields"], $db_info_posleases["fields"], $db_info_posclosings["fields"], $db_info_distributionchannels["fields"], $db_info_statistics["fields"]);

	$db_info["attributes"] = array_merge($db_info_pos["attributes"], $db_info_client["attributes"], $db_info_franchisee["attributes"], $db_info_posorder["attributes"], $db_info_latest_posorder["attributes"], $db_info_intagibles["attributes"], $db_info_posleases["attributes"], $db_info_posclosings["attributes"], $db_info_distributionchannels["attributes"], $db_info_statistics["attributes"]);
	
	$db_info["datatypes"] = array_merge($db_info_pos["datatypes"], $db_info_client["datatypes"], $db_info_franchisee["datatypes"], $db_info_posorder["datatypes"], $db_info_latest_posorder["datatypes"], $db_info_intagibles["datatypes"], $db_info_posleases["datatypes"], $db_info_posclosings["datatypes"], $db_info_distributionchannels["datatypes"], $db_info_statistics["datatypes"]);
	
	$db_info["calculated_content"] = array_merge($db_info_pos["calculated_content"], $db_info_client["calculated_content"], $db_info_franchisee["calculated_content"], $db_info_posorder["calculated_content"], $db_info_latest_posorder["calculated_content"], $db_info_intagibles["calculated_content"], $db_info_posleases["calculated_content"], $db_info_posclosings["calculated_content"], $db_info_distributionchannels["calculated_content"], $db_info_statistics["calculated_content"]);

	$db_info["calculated_content_key"] = array_merge($db_info_pos["calculated_content_key"], $db_info_client["calculated_content_key"], $db_info_franchisee["calculated_content_key"], $db_info_posorder["calculated_content_key"], $db_info_latest_posorder["calculated_content_key"], $db_info_intagibles["calculated_content_key"], $db_info_posleases["calculated_content_key"], $db_info_posclosings["calculated_content_key"], $db_info_distributionchannels["calculated_content_key"], $db_info_statistics["calculated_content_key"]);

	$db_info["calculated_content_field"] = array_merge($db_info_pos["calculated_content_field"], $db_info_client["calculated_content_field"], $db_info_franchisee["calculated_content_field"], $db_info_posorder["calculated_content_field"], $db_info_latest_posorder["calculated_content_field"], $db_info_intagibles["calculated_content_field"], $db_info_posleases["calculated_content_field"], $db_info_posclosings["calculated_content_field"], $db_info_distributionchannels["calculated_content_field"], $db_info_statistics["calculated_content_field"]);

	$db_info["calculated_content_sort_order"] = array_merge($db_info_pos["calculated_content_sort_order"]);

	$db_info["content_by_function"] = array_merge($db_info_pos["content_by_function"], $db_info_client["content_by_function"], $db_info_franchisee["content_by_function"], $db_info_posorder["content_by_function"], $db_info_latest_posorder["content_by_function"], $db_info_intagibles["content_by_function"], $db_info_posleases["content_by_function"], $db_info_posclosings["content_by_function"], $db_info_distributionchannels["content_by_function"], $db_info_statistics["content_by_function"]);

	$db_info["content_by_function_params"] = array_merge($db_info_pos["content_by_function_params"], $db_info_client["content_by_function_params"], $db_info_franchisee["content_by_function_params"], $db_info_posorder["content_by_function_params"], $db_info_latest_posorder["content_by_function_params"], $db_info_intagibles["content_by_function_params"], $db_info_posleases["content_by_function_params"], $db_info_posclosings["content_by_function_params"], $db_info_distributionchannels["content_by_function_params"], $db_info_statistics["content_by_function_params"]);


	$query_groups = query_group_fields();
	$query_group01 = $row["posquery_grouping01"];
	$query_group02 = $row["posquery_grouping02"];


	$query_filter = unserialize($row["posquery_filter"]);

	$functionfields = query_function_fields();
	$fieldstosumup = unserialize($row["posquery_fieldstosumup"]);
	$totalisation_fields = array();
	$totalisation_fields_column_index = array();

	if($fieldstosumup)
	{
		foreach($fieldstosumup as $key=>$index)
		{
			
			if(in_array($functionfields[$index], $db_info["calculated_content_field"]))
			{
				$new_field_name = str_replace(".", "", $functionfields[$index]);
				$totalisation_fields[$new_field_name] = "0";
				$totalisation_fields_column_index[$new_field_name] = "0";
			}
			elseif(array_key_exists($functionfields[$index], $db_info["content_by_function"]))
			{
				$new_field_name = str_replace(".", "", $functionfields[$index]);
				$totalisation_fields[$new_field_name] = "0";
				$totalisation_fields_column_index[$new_field_name] = "0";
			}
			elseif(array_key_exists($functionfields[$index], $db_info["attributes"])
				and $db_info["attributes"][$functionfields[$index]] == 'get_from_value_array')
			{
				$new_field_name = str_replace(".", "", $functionfields[$index]);
				$totalisation_fields[$new_field_name] = "0";
				$totalisation_fields_column_index[$new_field_name] = "0";
			}
			elseif(in_array($functionfields[$index], $selected_field_order))
			{
				$new_field_name = str_replace(".", "", $db_info["attributes"][$functionfields[$index]]);
				$totalisation_fields[$new_field_name] = "0";
				$totalisation_fields_column_index[$new_field_name] = "0";
			}
		}
	}

	
	$count_records = $row["posquery_countrecords"];

	
	//create captions and fields for the sql
	$get_from_value_array_is_used = false;
	foreach($selected_field_order as $key=>$field)
	{
		$captions[] = $db_info["fields"][$field];
		if($db_info["attributes"][$field] == "content_by_function")
		{
			
		}
		elseif($db_info["attributes"][$field] == "calculated_content")
		{

		}
		elseif($db_info["attributes"][$field] == "get_from_value_array")
		{
			$get_from_value_array_is_used = true;
		}
		else
		{
			
			$outputfields .= $db_info["attributes"][$field] . " as " . str_replace(".", "", $db_info["attributes"][$field]) . ", ";

		}
	}
	$outputfields = substr($outputfields, 0, strlen($outputfields)-2);

	
	//create filter
		
	$filter = "";
	$filter_salesregions = "";
	$filter_regions = "";
	$filter_provinces = "";
	$filter_countries = "";
	$filter_cities = "";
	$filter_areas = "";
	$filter_customer_services = "";
	$filter_project_costtypes = "";
	$filter_product_lines = "";
	$filter_furniture_subclasses = "";
	$filter_pos_types = "";
	$filter_possubclasses = "";
	$filter_project_kinds = "";
	$filter_distribution_channels = "";
	$filter_design_objectives = "";
	$filter_fcy = "";
	$filter_tcy = "";

	$filter_aofy = "";
	$filter_aofm = "";
	$filter_aoty = "";
	$filter_aotm = "";

	
	if($posquery_area_perception_filter)
	{
		$filter .= "(" . $posquery_area_perception_filter . ")";
	}
	
	if($query_filter["re"]) //Geographical region
	{
		$tmp = explode("-", $query_filter["re"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_salesregions .= $value . ",";
			}
		}
		$filter_salesregions = "(" . substr($filter_salesregions, 0, strlen($filter_salesregions)-1) . ")";
		if($filter_regions != "()")
		{
			
			if($filter)
			{
				$filter .= " and poscountries.country_salesregion in " . $filter_salesregions . " ";
			}
			else
			{
				$filter .= "poscountries.country_salesregion in " . $filter_salesregions . " ";
			}
			
		}
	}

	if($query_filter["gr"]) //Supplied region
	{
		$tmp = explode("-", $query_filter["gr"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_regions .= $value . ",";
			}
		}
		$filter_regions = "(" . substr($filter_regions, 0, strlen($filter_regions)-1) . ")";
		if($filter_regions != "()")
		{
			if($filter)
			{
				$filter .= " and clientcountries.country_region in " . $filter_regions . " ";
			}
			else
			{
				$filter .= "clientcountries.country_region in " . $filter_regions . " ";
			}
		}
	}
	
	if($query_filter["pr"]) // Province
	{
		$tmp = explode("-", $query_filter["pr"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_provinces .= $value . ",";
			}
		}
		$filter_provinces = "(" . substr($filter_provinces, 0, strlen($filter_provinces)-1) . ")";
		if($filter_provinces != "()")
		{
			if($filter)
			{
				$filter .= " and province_id in " . $filter_provinces . " ";
			}
			else
			{
				$filter .= "province_id in " . $filter_provinces . " ";
			}
		}
	}

	if($query_filter["co"]) // country
	{
		$tmp = explode("-", $query_filter["co"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_countries .= $value . ",";
			}
		}
		$filter_countries = "(" . substr($filter_countries, 0, strlen($filter_countries)-1) . ")";
		if($filter_countries != "()")
		{
			if($filter)
			{
				$filter .= " and posaddress_country in " . $filter_countries . " ";
			}
			else
			{
				$filter .= "posaddress_country in " . $filter_countries . " ";
			}
		}
	}

	if($query_filter["ci"]) // city
	{
		$tmp = explode("-", $query_filter["ci"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_cities .= "'" . $value . "',";
			}
		}
		$filter_cities = "(" . substr($filter_cities, 0, strlen($filter_cities)-1) . ")";
		if($filter_cities != "()")
		{
			if($filter)
			{
				$filter .= " and posaddress_place in " . $filter_cities . " ";
			}
			else
			{
				$filter .= "posaddress_place in " . $filter_cities . " ";
			}
		}
	}

	
	
	if($query_filter["pct"]) //cost type, legal type
	{
		$tmp = explode("-", $query_filter["pct"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_project_costtypes .= $value . ",";
			}
		}
		$filter_project_costtypes = "(" . substr($filter_project_costtypes, 0, strlen($filter_project_costtypes)-1) . ")";
		if($filter_project_costtypes != "()")
		{
			if($filter)
			{
				if(in_array('posorders.posorder_legal_type', $selected_field_order))
				{
					$filter .= " and posorder_legal_type in " . $filter_project_costtypes . " ";
				}
				else
				{
					$filter .= " and posaddress_ownertype in " . $filter_project_costtypes . " ";
				}
			}
			else
			{
				if(in_array('posorders.posorder_legal_type', $selected_field_order))
				{
					$filter .= "posorder_legal_type in " . $filter_project_costtypes . " ";
				}
				else
				{
					$filter .= "posaddress_ownertype in " . $filter_project_costtypes . " ";
				}
			}
		}
	}

	if($query_filter["pl"]) // product line, furniture type
	{
		$tmp = explode("-", $query_filter["pl"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_product_lines .= $value . ",";
			}
		}
		$filter_product_lines = "(" . substr($filter_product_lines, 0, strlen($filter_product_lines)-1) . ")";
		if($filter_product_lines != "()")
		{
			if($filter)
			{
				if(in_array('posorders.posorder_product_line', $selected_field_order))
				{
					$filter .= " and posorders.posorder_product_line in " . $filter_product_lines . " ";
				}
				else
				{
					$filter .= " and posaddress_store_furniture in " . $filter_product_lines . " ";
				}
			}
			else
			{
				if(in_array('posorders.posorder_product_line', $selected_field_order))
				{
					$filter .= "posorders.posorder_product_line in " . $filter_product_lines . " ";
				}
				else
				{
					$filter .= "posaddress_store_furniture in " . $filter_product_lines . " ";
				}
			}
		}
	}

	if($query_filter["dcs"]) //Distribution Channel
	{
		$tmp = explode("-", $query_filter["dcs"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_distribution_channels .= $value . ",";
			}
		}
		$filter_distribution_channels = "(" . substr($filter_distribution_channels, 0, strlen($filter_distribution_channels)-1) . ")";
		
		if($filter_distribution_channels != "()")
		{
			if($filter)
			{
				$filter .= " and posaddress_distribution_channel in " . $filter_distribution_channels . " ";
			}
			else
			{
				$filter .= " posaddress_distribution_channel in " . $filter_distribution_channels . " ";
			}
		}
	}

	
	if($query_filter["fscs"]) // furniture subclasses
	{
		$tmp = explode("-", $query_filter["fscs"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_furniture_subclasses .= $value . ",";
			}
		}
		
		$filter_furniture_subclasses = "(" . substr($filter_furniture_subclasses, 0, strlen($filter_furniture_subclasses)-1) . ")";
		if($filter_furniture_subclasses != "()")
		{
				if($filter)
				{
					$filter .= " and posaddress_store_furniture_subclass in " . $filter_furniture_subclasses . " ";
				}
				else
				{
					$filter .= " posaddress_store_furniture_subclass in " . $filter_furniture_subclasses . " ";
				}
		}
		
	}

	

	if($query_filter["pt"]) // pos type
	{
		$tmp = explode("-", $query_filter["pt"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_pos_types .= "'" . $value . "',";
			}
		}
		$filter_pos_types = "(" . substr($filter_pos_types, 0, strlen($filter_pos_types)-1) . ")";
		if($filter_pos_types != "()")
		{
			if($filter)
			{
				if(in_array('posorders.posorder_postype', $selected_field_order))
				{
					$filter .= " and (posorderprojecttypes.postype_name in " . $filter_pos_types . " ";
					$filter .= " or posorderprojecttypes.postype_name in " . $filter_pos_types . ") ";
				}
				else
				{
					$filter .= " and (postypes.postype_name in " . $filter_pos_types . " ";
					$filter .= " or postypes.postype_name in " . $filter_pos_types . ") ";
				}
			}
			else
			{
				if(in_array('posorders.posorder_postype', $selected_field_order))
				{
					$filter .= "posorderprojecttypes.postype_name in " . $filter_pos_types . " ";
				}
				else
				{
					$filter .= "postypes.postype_name in " . $filter_pos_types . " ";
				}
			}
		}
	}

	if($query_filter["sc"]) // POS Type Subclass
	{
		$tmp = explode("-", $query_filter["sc"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_possubclasses .= $value . ",";
			}
		}
		$filter_possubclasses = "(" . substr($filter_possubclasses, 0, strlen($filter_possubclasses)-1) . ")";
		if($filter_possubclasses != "()")
		{
			if($filter)
			{
				if(in_array('posorders.posorder_subclass', $selected_field_order))
				{
					$filter .= " and posorders.posorder_subclass in " . $filter_possubclasses . " ";
				}
				else
				{
					$filter .= " and posaddress_store_subclass in " . $filter_possubclasses . " ";
				}
			}
			else
			{
				if(in_array('posorders.posorder_subclass', $selected_field_order))
				{
					$filter .= "posorders.posorder_subclass in " . $filter_possubclasses . " ";
				}
				else
				{
					$filter .= "posaddress_store_subclass in " . $filter_possubclasses . " ";
				}
			}
		}
	}

	
	if($query_filter["cl"] > 0) // client address
	{
		if($filter)
		{
			$filter .= " and posaddress_client_id = " . $query_filter["cl"] . " ";
		}
		else
		{
			$filter .= "posaddress_client_id = " . $query_filter["cl"] . " ";
		}
	}

	if($query_filter["ct"] > 0) // client type
	{
		if($filter)
		{
			$filter .= " and clients.address_client_type = " . $query_filter["ct"] . " ";
		}
		else
		{
			$filter .= "clients.address_client_type = " . $query_filter["ct"] . " ";
		}
	}

	if($query_filter["at"] > 0) // address type
	{
		if($filter)
		{
			$filter .= " and franchisees.address_type = " . $query_filter["at"] . " ";
		}
		else
		{
			$filter .= "franchisees.address_type = " . $query_filter["at"] . " ";
		}
	}

	if($query_filter["fr"] > 0) // franchisee address
	{
		if($filter)
		{
			$filter .= " and posaddress_franchisee_id = " . $query_filter["fr"] . " ";
		}
		else
		{
			$filter .= "posaddress_franchisee_id = " . $query_filter["fr"] . " ";
		}
	}

	if($query_filter["lopr"] > 0) // POS with locally produced projects
	{
		if($filter)
		{
			$filter .= " and posorder_project_locally_produced = 1 ";
		}
		else
		{
			$filter .= "posorder_project_locally_produced = 1 ";
		}
	}


	
			
	if($query_filter["nopr"] > 0) // POS with no projects assigned
	{
		
		if($filter)
		{
			$filter .= " and ((select count(posorder_id) from posorders where posorder_type = 1 and posorder_posaddress = posaddress_id)  = 0) ";
		}
		else
		{
			$filter .= "((select count(posorder_id) from posorders where posorder_type = 1 and posorder_posaddress = posaddress_id)  = 0) ";
		}

	}

	if($query_filter["exfp"] > 0) // exclude fake projects
	{
		
		if($filter)
		{
			$filter .= " and posorder_order > 0 ";
		}
		else
		{
			$filter .= " posorder_order > 0 ";
		}

	}

	
	if($query_filter["fcy"] > 0 and $query_filter["fcm"] > 0) // from closing year, month
	{
		if($filter)
		{
			$filter .= " and posclosing_startdate >= '" . $query_filter["fcy"] . "-" . $query_filter["fcm"] . "-01'";
		}
		else
		{
			$filter .= " posclosing_startdate >= '" . $query_filter["fcy"] . "-" . $query_filter["fcm"] . "-01'";
		}
	}
	elseif($query_filter["fcy"] > 0) // from closing years
	{
		if($filter)
		{
			$filter .= " and year(posclosing_startdate) >= " . $query_filter["fcy"] . " ";
		}
		else
		{
			$filter .= " year(posclosing_startdate) >= " . $query_filter["fcy"] . " ";
		}
	}

	
		
	if($query_filter["tcy"] > 0 and $query_filter["tcm"] > 0) // to closing year, month
	{
		if($filter)
		{
			$filter .= " and posclosing_enddate <= '" . $query_filter["tcy"] . "-" . $query_filter["tcm"] . "-31'";
		}
		else
		{
			$filter .= " posclosing_enddate <= '" . $query_filter["tcy"] . "-" . $query_filter["tcm"] . "-31'";
		}
	}
	elseif($query_filter["tcy"] > 0) // from closing years
	{
		if($filter)
		{
			$filter .= " and year(posclosing_enddate) <= " . $query_filter["tcy"] . " ";
		}
		else
		{
			$filter .= " year(posclosing_enddate) <= " . $query_filter["tcy"] . " ";
		}
	}

		
	if($query_filter["fcy"] > 0 or $query_filter["tcy"] > 0) // from closing years
	{
		//$filter .= " AND posaddress_store_closingdate is not null and posaddress_store_closingdate <> '0000-00-00' ";
	}

	
	
	
	
	if($query_filter["aofy"] > 0 and $query_filter["aofm"] > 0) // from opening year
	{
		if($filter)
		{
			$filter .= " and posaddress_store_openingdate >= '" . $query_filter["aofy"] . "-" . $query_filter["aofm"] . "-01'";
		}
		else
		{
			$filter .= " posaddress_store_openingdate >= '" . $query_filter["aofy"] . "-" . $query_filter["aofm"] . "-01'";
		}
	}
	elseif($query_filter["aofy"] > 0) // from opening year
	{
		if($filter)
		{
			$filter .= " and year(posaddress_store_openingdate) >= " . $query_filter["aofy"] . " ";
		}
		else
		{
			$filter .= " year(posaddress_store_openingdate) >= " . $query_filter["aofy"] . " ";
		}
	}

	if($query_filter["aoty"] > 0 and $query_filter["aotm"] > 0) // to opening year
	{
		if($filter)
		{
			$filter .= " and posaddress_store_openingdate <= '" . $query_filter["aoty"] . "-" . $query_filter["aotm"] . "-31'";
		}
		else
		{
			$filter .= " posaddress_store_openingdate <= '" . $query_filter["aoty"] . "-" . $query_filter["aotm"] . "-31'";
		}
	}
	elseif($query_filter["aoty"] > 0) // from opening year
	{
		if($filter)
		{
			$filter .= " and year(posaddress_store_openingdate) <= " . $query_filter["aoty"] . " ";
		}
		else
		{
			$filter .= " year(posaddress_store_openingdate) <= " . $query_filter["aoty"] . " ";
		}
	}


	

	if($query_filter["incs"] != 1
		and !$query_filter["opy"] 
		and $query_filter["clfy"] == 0 
		and $query_filter["clfm"] == 0 
		and $query_filter["clty"] == 0
		and $query_filter["clm"] == 0) // exclude closed shops
	{
		if($filter)
		{
			$filter .= " and (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') ";
		}
		else
		{
			$filter .= "(posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') ";
		}
	}
	
	
		
	if($query_filter["clfy"] > 0 and $query_filter["clfm"] > 0) // from closing year
	{
		if($filter)
		{
			$filter .= " and posaddress_store_closingdate >= '" . $query_filter["clfy"] . "-" . $query_filter["clfm"] . "-01'";
		}
		else
		{
			$filter .= "  posaddress_store_closingdate >= '" . $query_filter["clfy"] . "-" . $query_filter["clfm"] . "-01'";	
		}
	}
	elseif($query_filter["clfy"] > 0) // from closing year
	{
		if($filter)
		{
			$filter .= " and year(posaddress_store_closingdate) >= " . $query_filter["clfy"] . " ";
		}
		else
		{
			$filter .= " year(posaddress_store_closingdate) >= " . $query_filter["clfy"] . " ";
		}
	}

	

	if($query_filter["clty"] > 0 and $query_filter["cltm"] > 0) // to closing year
	{
		if($filter)
		{
			$filter .= " and posaddress_store_closingdate <= '" . $query_filter["clty"] . "-" . $query_filter["cltm"] . "-31'";
		}
		else
		{
			$filter .= "  posaddress_store_closingdate <= '" . $query_filter["clty"] . "-" . $query_filter["cltm"] . "-31'";	
		}
	}
	elseif($query_filter["clty"] > 0) // from closing year
	{
		if($filter)
		{
			$filter .= " and year(posaddress_store_closingdate) <= " . $query_filter["clty"] . " ";
		}
		else
		{
			$filter .= " year(posaddress_store_closingdate) <= " . $query_filter["clty"] . " ";
		}
	}


	if($query_filter["clfy"] > 0
		or $query_filter["clfm"] > 0
		or $query_filter["clty"] > 0
		or $query_filter["cltm"] > 0
		) // from closing years
	{
		$filter .= " AND posaddress_store_closingdate is not null and posaddress_store_closingdate <> '0000-00-00' ";
	}



	//operating as per
	if($query_filter["opy"] > 0 and $query_filter["opm"] > 0) // operating per year
	{
		if($filter)
		{
			$filter .= "AND (YEAR (posaddress_store_openingdate) < " . $query_filter["opy"] . " OR (YEAR (posaddress_store_openingdate) = " . $query_filter["opy"] . " AND MONTH (posaddress_store_openingdate) <= " . $query_filter["opm"] . ")) AND ( posaddress_store_closingdate IS NULL OR posaddress_store_closingdate = '0000-00-00' OR YEAR (posaddress_store_closingdate) > " . $query_filter["opy"] . " OR (YEAR (posaddress_store_closingdate) = " . $query_filter["opy"] . " and MONTH (posaddress_store_closingdate) > " . $query_filter["opm"] . "))";
			
		}
		else
		{
			$filter = " (YEAR (posaddress_store_openingdate) < " . $query_filter["opy"] . " OR (YEAR (posaddress_store_openingdate) = " . $query_filter["opy"] . " AND MONTH (posaddress_store_openingdate) <= " . $query_filter["opm"] . ")) AND ( posaddress_store_closingdate IS NULL OR posaddress_store_closingdate = '0000-00-00' OR YEAR (posaddress_store_closingdate) > " . $query_filter["opy"] . " OR (YEAR (posaddress_store_closingdate) = " . $query_filter["opy"] . " and MONTH (posaddress_store_closingdate) > " . $query_filter["opm"] . "))";
			
		}
	}
	

	

	//get only POS visible in store locator
	if($query_filter["www"] > 0) 
	{
		if($filter)
		{
			$filter .= "AND posaddress_export_to_web = 1 and (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') ";
			
		}
		else
		{
			$filter = "posaddress_export_to_web = 1  and (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') ";
			
		}
	}
	

	if(substr_count  ( $outputfields  , "posorders") > 0 or substr_count  ( $outputfields  , "posorderinvestments") > 0)
	{
		if($filter)
		{
			$filter = " where posorders.posorder_type = 1 and " . $filter;
		}
		else
		{
			$filter = " where posorders.posorder_type = 1 ";
		}
	}
	else
	{
		if($filter)
		{
			$filter = " where " . $filter;
		}
	}

	
	
	// COMPOSE SQL

	if(substr_count  ( $outputfields  , "posorders") > 0 or substr_count  ( $outputfields  , "posorderinvestments") > 0)
	{
		$outputfields = $outputfields . ", posorder_id";
	}
	
	if(substr_count  ( $outputfields  , "posaddress") > 0)
	{
		$outputfields = "posaddress_id, posaddress_country,  " . $outputfields;
		$sql = "select  DISTINCT " . $outputfields . " from posaddresses ";
		//$sql = "select DISTINCT " . $outputfields . " from posaddresses ";
	}
	elseif(substr_count  ( $outputfields  , "project_number") > 0)
	{
		$outputfields = "project_id, " . $outputfields;
		$sql = "select  DISTINCT " . $outputfields . " from posaddresses ";
	}
	else
	{
		$sql = "select DISTINCT " . $outputfields . " from posaddresses ";
	}

	


	$sql_join = "";
	
		
		
	$sql_join .= "left join countries as poscountries on poscountries.country_id = posaddress_country ";
	$sql_join .= "left join salesregions on salesregion_id = poscountries.country_salesregion ";

	$sql_join .= "left join postypes as postypes on postypes.postype_id = posaddress_store_postype ";
	$sql_join .= "left join product_lines as posproductlines on posproductlines.product_line_id = posaddress_store_furniture ";
	$sql_join .= "left join productline_subclasses as posproductline_subclasses on posproductline_subclasses.productline_subclass_id = posaddress_store_furniture_subclass ";
	$sql_join .= "left join possubclasses as possubclasses on possubclasses.possubclass_id = posaddress_store_subclass ";
	$sql_join .= "left join project_costtypes as poscosttypes on poscosttypes.project_costtype_id = posaddress_ownertype ";
	$sql_join .= "left join addresses as clients on clients.address_id = posaddress_client_id ";
	$sql_join .= "left join client_types as clienttypes on clienttypes.client_type_id = address_client_type ";
	$sql_join .= "left join countries as clientcountries on clientcountries.country_id = clients.address_country ";

	if($query_filter["pr"] or substr_count  ( $outputfields  , "province") > 0) // Province
	{
		$sql_join .= "left join places as posplaces on posplaces.place_id = posaddress_place_id ";
		$sql_join .= "left join provinces as posprovinces on posprovinces.province_id = posplaces.place_province ";
	}

	$sql_join .= "left join regions on region_id = clientcountries.country_region ";
	$sql_join .= "left join addresses as franchisees on franchisees.address_id = posaddress_franchisee_id ";
	$sql_join .= "left join countries as franchiseecountries on franchiseecountries.country_id = franchisees.address_country ";
	$sql_join .= "left join agreement_types as posagreements on posagreements.agreement_type_id = posaddress_fagagreement_type "; 
	
	
    $pos_orders_included = false;
	if((substr_count  ( $outputfields  , "projects") > 0 
		or substr_count  ( $outputfields  , "posorders") > 0 
		or substr_count  ( $filter  , "posorders") > 0
		or substr_count  ( $outputfields  , "projectkind") > 0
		or substr_count  ( $outputfields  , "product_line") > 0
		or substr_count  ( $outputfields  , "poslease") > 0)
		and substr_count  ( $outputfields  , "posorderinvestments") == 0)
	{
		$sql_join .= "left join posorders on posorder_posaddress = posaddress_id ";
		$sql_join .= "left join postypes as posorderprojecttypes on posorderprojecttypes.postype_id = posorder_postype ";
		$sql_join .= "left join product_lines as posorderproductlines on posorderproductlines.product_line_id = posorder_product_line ";
		$sql_join .= "left join productline_subclasses as posorderproductlinesubclasses on posorderproductlinesubclasses.productline_subclass_id = posorder_product_line_subclass ";
		$sql_join .= "left join possubclasses as posordersubclasses on posordersubclasses.possubclass_id = posorder_subclass ";
		$sql_join .= "left join project_costtypes as posordercosttypes on posordercosttypes.project_costtype_id = posorder_legal_type ";
		$sql_join .= "left join projectkinds as posorderprojectkinds on posorderprojectkinds.projectkind_id = posorder_project_kind ";
		$sql_join .= "left join projects on project_order = posorder_order ";
		$sql_join .= "left join orders on order_id = project_order ";
		$sql_join .= "left join project_costs on project_cost_order = order_id ";
		$pos_orders_included = true;
	}
	elseif($query_filter["lopr"] or $query_filter["pk"])
	{
		$sql_join .= "left join posorders on posorder_posaddress = posaddress_id ";
	}
	

	if($pos_orders_included == false)
	{
		if(substr_count  ( $outputfields  , "posorderinvestments") > 0 
			or substr_count  ( $outputfields  , "posinvestment") > 0
			or substr_count  ( $filter  , "posorderinvestments") > 0)
		{
			
			$sql_join .= "left join posorders on posorder_posaddress = posaddress_id ";
			$sql_join .= "left join postypes as posorderprojecttypes on posorderprojecttypes.postype_id = posorder_postype ";
			$sql_join .= "left join product_lines as posorderproductlines on posorderproductlines.product_line_id = posorder_product_line ";
			$sql_join .= "left join possubclasses as posordersubclasses on posordersubclasses.possubclass_id = posorder_subclass ";
			$sql_join .= "left join project_costtypes as posordercosttypes on posordercosttypes.project_costtype_id = posorder_legal_type ";
			$sql_join .= "left join projectkinds as posorderprojectkinds on posorderprojectkinds.projectkind_id = posorder_project_kind ";
			$sql_join .= "left join projects on project_order = posorder_order ";
			$sql_join .= "left join orders on order_id = project_order ";
			$sql_join .= "left join project_costs on project_cost_order = order_id ";
			$sql_join .= "left join posinvestments on posinvestment_posaddress = posaddress_id ";
			$sql_join .= "left join posinvestment_types as intangibletypes on intangibletypes.posinvestment_type_id = posinvestments.posinvestment_investment_type "; 
			$sql_join .= "left join posorderinvestments on posorderinvestment_posorder = posorder_id ";
			$sql_join .= "left join posinvestment_types as investmenttypes on investmenttypes.posinvestment_type_id = posorderinvestments.posorderinvestment_investment_type ";
		}
	}
	elseif(substr_count  ( $outputfields  , "posorderinvestments") > 0 
			or substr_count  ( $outputfields  , "posinvestment") > 0
			or substr_count  ( $filter  , "posorderinvestments") > 0)
	{
			
			$sql_join .= "left join posinvestments on posinvestment_posaddress = posaddress_id ";
			$sql_join .= "left join posinvestment_types as intangibletypes on intangibletypes.posinvestment_type_id = posinvestments.posinvestment_investment_type "; 
			$sql_join .= "left join posorderinvestments on posorderinvestment_posorder = posorder_id ";
			$sql_join .= "left join posinvestment_types as investmenttypes on investmenttypes.posinvestment_type_id = posorderinvestments.posorderinvestment_investment_type ";
			
	}

	if(substr_count  ( $outputfields  , "cer_investments") > 0 or substr_count  ( $filter  , "cer_investments") > 0)
	{
		$sql_join .= 'left join cer_investments on cer_investment_project = project_id and cer_investment_type = posorderinvestment_investment_type ';
	}



	if(substr_count  ( $outputfields  , "posclosing") > 0 or $query_filter["fcy"] > 0 or $query_filter["tcy"] > 0)
	{
		if(substr_count  ( $sql_join  , "posorders") === 0) {
			$sql_join .= "left join posorders on posorder_posaddress = posaddress_id ";
		}
		if(substr_count  ( $sql_join  , "projects") === 0) {
			$sql_join .= "left join projects on project_order = posorder_order ";
		}
		$sql_join .= "inner join posclosings on posclosing_project = project_id ";
	}

	

	if(substr_count  ( $outputfields  , "mps_distchannels") > 0)
	{
		$sql_join .= " left join mps_distchannels on mps_distchannel_id = posaddress_distribution_channel ";
	}


	if(substr_count  ( $outputfields  , "poslease") > 0)
	{
		$sql_join .= "left join posleases ON poslease_posaddress = posaddress_id ";
		$sql_join .= "left join poslease_types on poslease_type_id = poslease_lease_type ";
	}

	
	


	//project Types
	$posaddress_ids_with_project_kinds_indicated = array();
	if($query_filter["pk"]) // project Type
	{
		$tmp = explode("-", $query_filter["pk"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_project_kinds .= $value . ",";
			}
		}

				
		$filter_project_kinds = "(" . substr($filter_project_kinds, 0, strlen($filter_project_kinds)-1) . ")";
		if($filter_project_kinds != "()")
		{
			
			$sql_p = "select DISTINCT posorder_posaddress " . 
				     "from posorders " . 
					 "where posorder_type = 1 and posorder_project_kind IN " . $filter_project_kinds; 
			
			$res_p = mysql_query($sql_p) or dberror($sql_p);
			while ($row_p = mysql_fetch_assoc($res_p))
			{
				$posaddress_ids_with_project_kinds_indicated[$row_p["posorder_posaddress"]] = $row_p["posorder_posaddress"];
			}
		}


		if(count($posaddress_ids_with_project_kinds_indicated) > 0)
		{
			$filter .= " and posaddress_id IN (" . implode(",", $posaddress_ids_with_project_kinds_indicated) . ") ";
			$filter .= " and posorder_project_kind IN " . $filter_project_kinds; 
		}
	}

	

	//design objectives
	$posaddress_ids_with_desigen_objectives_indicated = array();
	if($query_filter["dos"]) //Design Objectives
	{
		
		$tmp = explode("-", $query_filter["dos"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_design_objectives .= $value . ",";
			}
		}
		
		$filter_design_objectives = "(" . substr($filter_design_objectives, 0, strlen($filter_design_objectives)-1) . ")";
		if($filter_design_objectives != "()")
		{
			
			$sql_p = "select DISTINCT posaddress_id " . 
				     "from posaddresses " . 
					 "inner join posorders on posorder_posaddress = posaddress_id " . 
					 "inner join projects on posorder_order = project_order " . 
					 "left join project_items on project_item_project = project_id " . 
					 "where posaddress_store_openingdate = project_actual_opening_date " . 
					 "and project_item_item IN " . $filter_design_objectives; 
			
			$res_p = mysql_query($sql_p) or dberror($sql_p);
			while ($row_p = mysql_fetch_assoc($res_p))
			{
				$posaddress_ids_with_desigen_objectives_indicated[$row_p["posaddress_id"]] = $row_p["posaddress_id"];
			}
		}


		if(count($posaddress_ids_with_desigen_objectives_indicated) > 0)
		{
			$filter .= " and posaddress_id IN (" . implode(",", $posaddress_ids_with_desigen_objectives_indicated) . ") ";
		}

	}
	

	//Neighbourhood Area
	$posaddress_ids_with_areas_indicated = array();
	if($query_filter["ar"]) //POS Areas
	{
		
		$tmp = explode("-", $query_filter["ar"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_areas .= $value . ",";
			}
		}
		
		$filter_areas = "(" . substr($filter_areas, 0, strlen($filter_areas)-1) . ")";
		if($filter_areas != "()")
		{
			
			$sql_p = "select DISTINCT posarea_posaddress " . 
				     "from posareas " . 
					 "where posarea_area IN " . $filter_areas; 
			
			$res_p = mysql_query($sql_p) or dberror($sql_p);
			while ($row_p = mysql_fetch_assoc($res_p))
			{
				$posaddress_ids_with_areas_indicated[$row_p["posarea_posaddress"]] = $row_p["posarea_posaddress"];
			}
		}


		if(count($posaddress_ids_with_areas_indicated) > 0)
		{
			$filter .= " and posaddress_id IN (" . implode(",", $posaddress_ids_with_areas_indicated) . ") ";
		}

	}


	
	//prevent from showing leases of cancelled projects
	if(substr_count  ( $outputfields  , "poslease") > 0)
	{
		$filter .= "and (order_actual_order_state_code < '900' or order_actual_order_state_code is null) ";

		
		if($query_filter["pos_without_lease"] > 0) // Show only POS without lease
		{
			$filter .= " and poslease_id is null ";
		}
		elseif($query_filter["latest_pos_lease"] > 0) // Show only the latest POS lease
		{
			$filter .= " and poslease_id in (select max(pl.poslease_id) from posleases as pl where pl.poslease_posaddress = posaddress_id order by pl.poslease_startdate DESC)";
		}

		
	}

	

	$sql .= $sql_join . $filter;
	$sql_u = str_replace("DISTINCT", "DISTINCT posaddress_id,", $sql);
	
	//determin sort order of the data rows
	$posquery_order = $row["posquery_order"];
	$query_sortorder = decode_field_array($posquery_order);

	//create captions and fields for the sql
	foreach($query_sortorder as $key=>$field)
	{
		if($db_info["attributes"][$field] == 'calculated_content')
		{

		}
		elseif($db_info["attributes"][$field] == 'content_by_function')
		{

		}
		elseif($db_info["attributes"][$field] == 'get_from_value_array')
		{

		}
		else
		{
			$order_clause .= $db_info["attributes"][$field] . ", ";
		}
	}

	
	$order_clause = substr($order_clause, 0, strlen($order_clause)-2);

	
	//add grouping as primary sort order
	if($query_group02 > 0 and strpos($outputfields, $db_info["attributes"][$query_groups[$query_group02]]) !== false)
	{
		$new_order_field = $db_info["attributes"][$query_groups[$query_group02]] . ", ";
		$order_clause = str_replace($new_order_field, "", $order_clause);
		$order_clause = $db_info["attributes"][$query_groups[$query_group02]] . ", " . $order_clause;

		//move grouping field to first position of output fields
		$old_position = array_search  ( $query_groups[$query_group02]  , $selected_field_order );
		if($old_position > 0)
		{
			$group_field_caption = $captions[$old_position];
			for($i=$old_position;$i>0;$i--)
			{
				$selected_field_order[$i] = $selected_field_order[$i-1];
				$captions[$i] = $captions[$i-1];
			}
			$selected_field_order[0] = $query_groups[$query_group02];
			$captions[0] = $group_field_caption;
		}
		if(substr($order_clause, strlen($order_clause)-2, 2) == ", ")
		{
			$order_clause = substr($order_clause, 0, strlen($order_clause)-2);
		}
	}
	if($query_group01 > 0 and strpos($outputfields, $db_info["attributes"][$query_groups[$query_group01]]) !== false)
	{
		$new_order_field = $db_info["attributes"][$query_groups[$query_group01]] . ", ";
		$order_clause = str_replace($new_order_field, "", $order_clause);
		$order_clause = $db_info["attributes"][$query_groups[$query_group01]] . ", " . $order_clause;

		//move grouping field to first position of output fields
		$old_position = array_search  ( $query_groups[$query_group01]  , $selected_field_order );
		if($old_position > 0)
		{
			$group_field_caption = $captions[$old_position];
			for($i=$old_position;$i>0;$i--)
			{
				$selected_field_order[$i] = $selected_field_order[$i-1];
				$captions[$i] = $captions[$i-1];
			}
			$selected_field_order[0] = $query_groups[$query_group01];
			$captions[0] = $group_field_caption;
		}

		if(substr($order_clause, strlen($order_clause)-2, 2) == ", ")
		{
			$order_clause = substr($order_clause, 0, strlen($order_clause)-2);
		}

	}
	
	if($order_clause)
	{
		$sql .= " order by " . $order_clause;
	}


	
	
	//create record counts
	$poslocation_record_count = array();
	$i=1;
	if($query_group01 > 0 and $count_records == 1  and strpos($outputfields, $db_info["attributes"][$query_groups[$query_group01]]) !== false)
	{
		$sql_c = "select $order_clause, " . 
				 " count(DISTINCT posaddress_id) as num_recs "  .
				 "from posaddresses " .
				 $sql_join .
				 $filter . 
				 " group by " . $order_clause .
				 " order by " . $order_clause;


		//echo $sql_c . "<br /><br />";abc();
		//echo $query_groups[$query_group01];cde();

		$res_c = mysql_query($sql_c) or dberror($sql_c);
		while ($row_c = mysql_fetch_assoc($res_c))
		{
			$poslocation_record_count[$i] = $row_c["num_recs"];
			$i++;
		}
	}

	//var_dump($poslocation_record_count);abc();
	
	
	//create counts
	$poslocation_count = array();
	$i=1;
	$filter_tmp = "";

	if($get_from_value_array_is_used == true)
	{
		
		$sql_c = "select $order_clause, posaddress_ownertype, posaddress_store_postype, " . 
				 " count(DISTINCT posaddress_id) as num_recs "  .
				 "from posaddresses " .
				 $sql_join .
				 $filter . 
				 " group by " . $order_clause . ", posaddress_ownertype, posaddress_store_postype " . 
				 " order by " . $order_clause;


		//echo $sql_c . "<br /><br />";abc();

		$res_c = mysql_query($sql_c) or dberror($sql_c);
		while ($row_c = mysql_fetch_assoc($res_c))
		{
			$poslocation_count[$i][$row_c["posaddress_ownertype"]][$row_c["posaddress_store_postype"]] = $row_c["num_recs"];
			$i++;
		}
		

		//echo $sql_c . "<br /><br />";
	}





	//echo $sql_c;cde();
	
	/*
	echo "<pre>";
	print_r($poslocation_count);
	echo "</pre>";
	acb();
	*/
	
	
	//echo $sql;die;

	//var_dump($query_filter);
	//abc();

	//var_dump($selected_field_order);
	//abc();
	
	//echo '<pre>';
	//print_r($captions);die;


}
else
{

}

?>