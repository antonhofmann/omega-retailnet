<?php
/********************************************************************

    stats_page_actions.php

    Defines the possible page actions depending on access rights

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-04-22
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-04-22
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

$page->register_action('traffic_by_ip', 'Traffic by IP', "traffic_by_ip.php");
$page->register_action('traffic_by_user', 'Traffic by User', "traffic_by_user.php");

$page->register_action('nothing', '', "");
$page->register_action('home', 'Home', "welcome.php");

?>