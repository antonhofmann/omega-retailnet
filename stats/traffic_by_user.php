<?php
/********************************************************************

    traffic_by_user.php

    Enter parameters for the query of traffic

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-07-13
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-07-13
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_perform_queries");



/********************************************************************
    prepare all data needed
*********************************************************************/
$sql = "select distinct statistic_user, address_shortcut, ".
           "concat(user_name, ' ', user_firstname) as user_fullname, ".
           "statistic_ip, statistic_duration, count(statistic_id) as num_recs " .
           "from statistics " .
           "left join users on user_id = statistic_user " .
           "left join addresses on address_id = user_address " .
           "group by statistic_user, address_shortcut, user_fullname, " .
           "   statistic_ip, statistic_duration " .
           "having statistic_user > 0 and statistic_user <>'' " .
           "order by user_fullname, statistic_ip, statistic_duration DESC" ;


// user list
$list = new ListView($sql, LIST_HAS_HEADER | LIST_SHOW_COUNT);

$list->set_entity("statistics");
//$list->set_order("user_fullname");
//$list->set_filter("statistic_user > 0 and statistic_user <>''");

$list->add_column("user_fullname", "User", "", "", "", COLUMN_NO_WRAP);
$list->add_column("address_shortcut", "Company", "", "", "", COLUMN_NO_WRAP);
$list->add_column("statistic_ip", "IP", "", "", "", COLUMN_NO_WRAP);

$list->add_column("statistic_duration", "Seconds", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list->add_column("num_recs", "Count", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

$list->process();


/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page("traffic");

require "include/stats_page_actions.php";

$page->header();
$page->title("Traffic by User since Monday 25th April 2005");

$list->render();

$page->footer();

?>