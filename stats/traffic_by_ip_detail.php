<?php
/********************************************************************

    traffic_by_ip_detail.php

    Enter parameters for the query of traffic

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-04-22
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-04-22
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_perform_queries");

/********************************************************************
    prepare all data needed
*********************************************************************/
$sql_ips = "select statistic_ip, statistic_date, statistic_url, if(statistic_duration = '', '-', statistic_duration) as seconds, " .
           "concat(user_name, ' ', user_firstname) as user_fullname ".
           "from statistics " .
           "left join users on user_id = statistic_user ";

$list_filter = "statistic_user <>'' and statistic_user = " . id();



// ip list
$list = new ListView($sql_ips, LIST_HAS_HEADER | LIST_SHOW_COUNT);

$list->set_entity("statistics");
$list->set_order("statistic_duration desc, statistic_date desc");
$list->set_filter($list_filter);

$list->add_column("statistic_ip", "IP", "", "", "", COLUMN_NO_WRAP);
$list->add_column("user_fullname", "User", "", "", "", COLUMN_NO_WRAP);
$list->add_column("statistic_date", "Time", "", "", "", COLUMN_NO_WRAP);
$list->add_column("statistic_url", "URL", "", "", "", COLUMN_NO_WRAP);
$list->add_column("seconds", "Seconds" , "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

$list->process();


/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page("traffic");

require "include/stats_page_actions.php";

$page->header();
$page->title("Traffic by IP");

$list->render();

$page->footer();

?>