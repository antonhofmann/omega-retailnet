<?php
/********************************************************************
    SQL INJECTION
*********************************************************************/

$excluded_strings = array();
$excluded_strings[] = "select ";
$excluded_strings[] = "union ";
$excluded_strings[] = "insert ";
$excluded_strings[] = "update ";
$excluded_strings[] = "delete ";
$excluded_strings[] = "drop ";
$excluded_strings[] = "create ";
$excluded_strings[] = "alter ";
$excluded_strings[] = "exec ";

$excluded_strings[] = "select%20";
$excluded_strings[] = "union%20";
$excluded_strings[] = "insert%20";
$excluded_strings[] = "update%20";
$excluded_strings[] = "delete%20";
$excluded_strings[] = "drop%20";
$excluded_strings[] = "create%20";
$excluded_strings[] = "alter%20";
$excluded_strings[] = "exec%20";
$excluded_strings[] = "unhex(";

$excluded_strings[] = "into_outfile";
$excluded_strings[] = "load_file";
$excluded_strings[] = "utl_http";
$excluded_strings[] = "request ";
$excluded_strings[] = "script ";
$excluded_strings[] = "src=";
$excluded_strings[] = "load_file";
$excluded_strings[] = "information_schema";
$excluded_strings[] = "table_name";

$excluded_strings[] = "%09";
$excluded_strings[] = "%08";
$excluded_strings[] = "%0A";
$excluded_strings[] = "%0D";



$querytring = strtolower($_SERVER['QUERY_STRING']);

foreach($excluded_strings as $key=>$value)
{
	if (strpos($querytring,$value) !== false)
	{
		require_once($_SERVER["DOCUMENT_ROOT"] . '/page_not_found.php');
		//echo "4->	" . $value;
		die;
	}
}


if(strpos($_SERVER["SCRIPT_FILENAME"],'new_map') !== false) 
{
	if(substr_count($querytring, '+') > 0
	or substr_count($querytring, '@') > 0
	or substr_count($querytring, '|') > 0
	or substr_count($querytring, '**') > 0
	)
	{
		require_once($_SERVER["DOCUMENT_ROOT"] . '/page_not_found.php');
		//echo "2->" . $querytring;
		die;
	}
}
elseif(substr_count($querytring, ' ') > 4 
	or substr_count($querytring, '%20') > 4
	or substr_count($querytring, '+') > 0
	or substr_count($querytring, '@') > 0
	or substr_count($querytring, '|') > 0
	or substr_count($querytring, '**') > 0
	or substr_count($querytring, '%') > 3
	)
{
	if(strpos ( $querytring , 'filter=') == false)
	{
		require_once($_SERVER["DOCUMENT_ROOT"] . '/page_not_found.php');
		//echo "3->" . $querytring;
		die;
	}
}


?>