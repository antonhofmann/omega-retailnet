<?php
/********************************************************************

    popup_page.php

    Classes and functions to render popup pages.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-07-31
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-04-22
    Version:        1..0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/



/********************************************************************
    Site navigation structure
*********************************************************************/

/********************************************************************
    Page class
*********************************************************************/

class Popup_Page
{
    var $section;
    var $area;
    var $title;

    function Page($name = PAGE_LAST, $title = "")
    {
        global $_SERVER;

        $this->title = $title;
        $this->actions = array();
        $this->section = "";

        $this->area = PAGE_POPUP;

        
    }


    function header()
    {
        global $_SERVER;
        global $__needs_form;
        global $__needs_multipart_form;

        // HTML header with title

        if ($this->title)
        {
            $title = $this->title;
        }
        else if ($this->section)
        {
            $title = $this->section["title"];
        }
        else
        {
            $title = "";
        }

        $title = htmlspecialchars(smart_join(": ", array(BRAND ." Retailnet", $title)));

        echo "<html>";
        echo "<head>";
        echo "<title>$title</title>";
        echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=iso-8859-1\">";
		echo "<meta http-equiv=\"imagetoolbar\" content=\"no\">";
        echo "<link href=\"../include/retailnet.css?v=24\" rel=\"stylesheet\" type=\"text/css\">";
        echo "<script language=\"javascript\" src=\"../include/main.js?v=24\"></script>";

        // Image preloader

        if ($this->section)
        {
            echo "<script language=\"javascript\">\n";
            echo "<!--\n";

            foreach ($this->navigation as $section)
            {
                if (!isset($section["parent"]))
                {
                    if (!isset($section["hide"]))
                    {
                        echo $section["name"], "_on = new Image();\n";
                        echo $section["name"], "_on.src = 'pictures/navigation/", $section["name"], "_on.gif';\n";
                    }

                    echo $section["name"], "_title = new Image();\n";
                    echo $section["name"], "_title.src = 'pictures/titles/", $section["name"], ".gif';\n";
                    echo $section["name"], "_back = new Image();\n";
                    echo $section["name"], "_back.src = 'pictures/titles/", $section["name"], "_back.gif';\n";
                }
            }

            echo "//-->\n";
            echo "</script>\n";
        }

        echo "</head>";

        // Body tag (popup pages end here)

        if ($__needs_form)
        {
            $enctype = $__needs_multipart_form ? "enctype=\"multipart/form-data\" " : "";
            $form = "<form ${enctype}name=\"main\" method=\"post\" action=\"" . $_SERVER["PHP_SELF"] . "\">";
            $form .= "<input type=\"hidden\" name=\"action\" value=\"\" />";
        }
        else
        {
            $form = "";
        }

        if (!$this->section)
        {
            echo "<body bgcolor=\"#FFFFFF\" leftmargin=\"20\" topmargin=\"20\" marginwidth=\"20\" marginheight=\"20\">";
            echo $form;
            return;
        }

        echo "<body bgcolor=\"#FFFFFF\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">";
        echo $form;

        // Dermine master section

        $master = isset($this->section["parent"]) ? $this->section["parent"] : $this->section["name"];

        // Logo with image

        echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
        echo "<tr>";
        echo "<td><img src=\"../pictures/logo2012.gif\" width=\"400\" height=\"48\"></td>";
        echo "</tr>";

        // Navigation bar

        echo "<tr>";
        echo "<td background=\"../pictures/menu_spacer.gif\" nowrap>";

        if (!isset($this->section["nomenu"]))
        {
            echo "<img src=\"../pictures/menu_spacer.gif\" width=\"159\" height=\"19\">";

            foreach ($this->navigation as $section)
            {
                if (!isset($section["parent"]) && !isset($section["hide"]) && has_access($section["permission"]))
                {
                    $suffix = $master == $section["name"] ? "_on.gif" : "_off.gif";
                    $image = "pictures/navigation/" . $section["name"] . $suffix;
                    $url = "/" . $section["url"] != $_SERVER["PHP_SELF"] ? $section["url"] : "";

                    $name = $section["name"];
                    $script = "onmouseover=\"document.images['$name'].src = 'pictures/navigation/${name}_on.gif'\" " .
                              "onmouseout=\"document.images['$name'].src = '$image'\"";

                    if ($url) echo "<a href=\"$url\" $script>";
                    echo "<img name=\"$name\" src=\"$image\" width=\"100\" height=\"19\" border=\"0\">";
                    if ($url) echo "</a>";
                }
            }

            echo "<img src=\"../pictures/menu_end.gif\" width=\"2\" height=\"19\">";
        }
        else
        {
            echo "&nbsp;";
        }

        echo "</td>";
        echo "</tr>";

        // Title image

        echo "<tr>";
        echo "<td background=\"pictures/titles/${master}_back.gif\">";
        echo "<img src=\"pictures/titles/$master.gif\" width=\"400\" height=\"40\">";
        echo "</td>";
        echo "</tr>";
        echo "</table>";

        // Open area below navigation

        echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
        echo "<tr valign=\"top\">";
        echo "<td width=\"160\">";

        // Actions and side navigation

        echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";

        $actions = array();

        foreach ($this->navigation as $section)
        {
            if (isset($section["parent"]) && $section["parent"] == $master && has_access($section["permission"]))
            {
                $actions[] = array("name" => $section["title"],
                                   "caption" => $section["title"],
                                   "url" => $section["url"], 
                                   "target" => "");
            }
        }

        $actions = array_merge($actions, $this->actions);

        foreach ($actions as $action)
        {
            echo "<tr valign=\"top\">";

            echo "<td width=\"22\" bgcolor=\"#EEEEEE\">";
            echo "<img src=\"../pictures/spacer.gif\" width=\"22\" height=\"1\">";
            echo "</td>";

            echo "<td width=\"133\" class=\"action\" bgcolor=\"#EEEEEE\">";
            echo "<img src=\"../pictures/spacer.gif\" width=\"133\" height=\"3\"><br>";
            echo "<a href=\"", $action["url"], "\" target=\"" . $action["target"] . "\">";
            echo htmlspecialchars($action["caption"]);
            echo "</a><br>";
            echo "<img src=\"../pictures/spacer.gif\" width=\"133\" height=\"3\">";
            echo "</td>";
    
            echo "<td width=\"4\" bgcolor=\"#EEEEEE\">";
            echo "<img src=\"../pictures/spacer.gif\" width=\"4\" height=\"1\">";
            echo "</td>";

            echo "<td width=\"1\" bgcolor=\"#999999\">";
            echo "<img src=\"../pictures/spacer.gif\" width=\"1\" height=\"1\">";
            echo "</td>";

            echo "</tr>";

            echo "<tr>";

            echo "<td colspan=\"4\">";
            echo "<img src=\"../pictures/menu_line.gif\" width=\"160\" height=\"1\">";
            echo "</td>";

            echo "</tr>";
        }

        echo "<tr>";
        echo "<td colspan=\"4\" width=\"160\">";
        echo "<img src=\"../pictures/spacer.gif\" width=\"160\" height=\"1\">";
        echo "</td>";
        echo "</tr>";

        echo "</table>";

        // Content area

        echo "</td>";
        echo "<td width=\"20\">";
        echo "<img src=\"../pictures/spacer.gif\" width=\"20\" height=\"1\">";
        echo "</td>";

        echo "<td>";
        echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
        echo "<tr>";
        echo "<td height=\"20\"></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>";
    }

    function footer()
    {
        global $__needs_form;

        if ($this->section)
        {
            echo "</td>";
            echo "</tr>";
            echo "</table>";

            echo "</td>";
            echo "<td width=\"20\">";
            echo "<img src=\"../pictures/spacer.gif\" width=\"20\" height=\"1\">";
            echo "</td>";

            echo "<tr>";
            echo "<td colspan=\"3\" height=\"20\">";
            echo "</td>";
            echo "</tr>";

            echo "</td>";
            echo "</tr>";
            echo "</table>";
        }

        if ($__needs_form)
        {
            echo "</form>";
        }

        echo "</body>";
        echo "</html>";

        global $start_time;

        $end_time = time();
        $diff = $end_time - $start_time;

		if(array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
			$ips = $_SERVER['HTTP_X_FORWARDED_FOR'];
			if(is_array ( $ips )) {
				$i = count($ips);
				$ip = $ips[$i-1];
			}
			else
			{
				$ip = $ips;
			}
		}
		else
		{
			$ip = $_SERVER['REMOTE_ADDR'];
		}

        $sql = "Insert into statistics (" .
               "statistic_user, statistic_ip, statistic_date, " .
               "statistic_url, statistic_duration) " .
               "values ('" .
               user_id() . "', '" .
               $ip . "', '" .
               date("Y-m-d-H-i-s") . "', '" .
               $_SERVER["SCRIPT_FILENAME"] . "', " .
               $diff . ")";

          $res = mysql_query($sql);

    }

    function title($text)
    {
        echo "<p class=\"title\">";
        echo htmlspecialchars($text);
        echo "</p>";
    }
}

?>