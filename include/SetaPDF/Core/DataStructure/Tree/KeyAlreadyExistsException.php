<?php
/**
 * This file is part of the SetaPDF-Core Component
 *
 * @copyright  Copyright (c) 2013 Setasign - Jan Slabon (http://www.setasign.de)
 * @category   SetaPDF
 * @package    SetaPDF_Core
 * @license    http://www.setasign.de/ Commercial
 * @version    $Id: KeyAlreadyExistsException.php 542 2013-10-14 08:41:40Z maximilian.kresse $
 */

/**
 * Exception class which is thrown if a key that should be set already exists in a tree
 *
 * @copyright  Copyright (c) 2013 Setasign - Jan Slabon (http://www.setasign.de)
 * @category   SetaPDF
 * @package    SetaPDF_Core
 * @license    http://www.setasign.de/ Commercial
 */
class SetaPDF_Core_DataStructure_Tree_KeyAlreadyExistsException
    extends SetaPDF_Core_Exception
{
}