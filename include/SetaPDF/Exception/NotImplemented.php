<?php
/**
 * This file is part of the SetaPDF library
 * 
 * @copyright  Copyright (c) 2013 Setasign - Jan Slabon (http://www.setasign.de)
 * @category   SetaPDF
 * @package    SetaPDF
 * @license    http://www.setasign.de/ Commercial
 * @version    $Id: NotImplemented.php 542 2013-10-14 08:41:40Z maximilian.kresse $
 */

/**
 * Not implemented exception
 * 
 * @copyright  Copyright (c) 2013 Setasign - Jan Slabon (http://www.setasign.de)
 * @category   SetaPDF
 * @package    SetaPDF
 * @license    http://www.setasign.de/ Commercial
 */
class SetaPDF_Exception_NotImplemented extends SetaPDF_Exception
{}