<?php
/**
 * This file is part of the SetaPDF-Core Component
 *
 * @copyright  Copyright (c) 2013 Setasign - Jan Slabon (http://www.setasign.de)
 * @category   SetaPDF
 * @package    SetaPDF_Core
 * @license    http://www.setasign.de/ Commercial
 * @version    $Id: Core.php 543 2013-10-14 08:46:53Z maximilian.kresse $
 */

/**
 * Class for main properties of the SetaPDF-Core Component
 *
 * @copyright  Copyright (c) 2013 Setasign - Jan Slabon (http://www.setasign.de)
 * @category   SetaPDF
 * @package    SetaPDF_Core
 * @license    http://www.setasign.de/ Commercial
 */
class SetaPDF_Core
{
    /**
     * The version
     *
     * @var string
     */
    const VERSION = '2.9.0.545';

    /**
     * A float comparsion precision
     *
     * @var float
     */
    const FLOAT_COMPARSION_PRECISION = 1e-5;
}