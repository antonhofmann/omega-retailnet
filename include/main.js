function button(name)
{
    document.forms.main.elements.action.value = name;
    document.forms.main.submit();
}


function popup(url, width, height)
{
	
	var d = new Date();
	var extension = d.getMilliseconds()
	var Windowname = 'popup' + extension;
	var left = 10;
    var top = 10;

    width += 40;
    height += 40;

    if (screen.width > 0)
    {
        left = (screen.width - width) / 2;
        top = (screen.height - height) / 2;
    }

    var features = 'width=' + width + ',height=' + height + ',left=' + left + ',top=' + top + ',' +
        'scrollbars=yes,resizable=yes,location=no,directories=no,status=no,menubar=no';

    win = open(url, Windowname, features);
    setTimeout('win.focus();', 250);
}

function picture(name, width, height)
{
    var features = 'width=' + width + ',height=' + height + ',left=10,top=10';

    win = window.open('', 'popup', features);

    win.document.open();
    win.document.write('<html><head><title>Image</title></head>');
    win.document.write('<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" onBlur="self.close()">');
    win.document.write('<img src="' + name + '" width="' + width + '" height="' + height + '" onClick="self.close()">');
    win.document.write('</body>');
    win.document.write('</html>');
    win.document.close();

    win.focus();
    setTimeout('win.focus();', 250);
}

var overlayWindow = "";
function createWindowWithRemotingUrl2(title, url)
{
	
	$.window.prepare({
	   dock: 'bottom',       // change the dock direction: 'left', 'right', 'top', 'bottom'
	   animationSpeed: 200,  // set animation speed
	   minWinLong: 160       // set minimized window long dimension width in pixel
	});

	overlayWindow = $.window({
	   title: title,
	   url: url,
	   icon: "http://retailnet/cer/pictures/favicon.ico",
	   containerClass: "my_container",
	   headerClass: "my_header",
	   frameClass: "my_frame",
	   footerClass: "my_footer",
	   selectedHeaderClass: "my_selected_header",
	   showRoundCorner: true,
	   bookmarkable: false,
	   maximizable: true,
	   resizable: true,
	   showFooter: false,
	   width: 920,
	   height: 600,
	   x: 200,
	   y: 80,
	   handleScrollbar: true,
	   onClose: function(wnd) {

	   },
	   onOpen: function(wnd) {  
		 
	   }
	});
}

jQuery(document).ready(function($) {
	$('#window_text').focusout(function() {
		$.ajax({
			type: "POST",
			data: $(".nyroModal").serialize(),
			url: "/cer/reject_submission_update_session.php",
			success: function(msg){

			}
		});
	});

	$('#send').click(function() {
		$('.nyroModal').fadeTo('slow', 0.0, function() {
		});
	});
});


