<?php
/********************************************************************

    mail.php

    A class to send e-mails over an SMTP server.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-09-18
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-10-24
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

/********************************************************************
    Mail class
*********************************************************************/

class Mail
{
    var $subject = "";
    var $sender = "";
    var $recipients = array();
    var $ccs = array();
    var $bccs = array();
    var $attachments = array();
    var $width = 80;
    var $indent = 20;
    var $text = "";

    function Mail($subject = "", $sender = "", $sender_name = "")
    {
        $this->subject = $this->__quoted_printable_encode($subject);
        $this->sender = $this->__complete_address($sender, $sender_name);
    }

    function set_subject($subject)
    {
        $this->subject = $this->__quoted_printable_encode($subject);
    }

    function set_sender($sender, $name)
    {
        $this->sender = $this->__complete_address($sender, $name);
    }

    function set_format($width = 80, $indent = 20)
    {
        $this->width = $width;
        $this->indent = $indent;
    }

    function add_recipient($recipient, $name = "")
    {
        $this->recipients[] = $this->__complete_address($recipient, $name);
    }

    function add_cc($cc, $name = "")
    {
        $this->ccs[] = $this->__complete_address($cc, $name);;
    }

    function add_bcc($bcc, $name = "")
    {
        $this->bccs[] = $this->__complete_address($bcc, $name);;
    }

    function add_attachment($name, $data, $type = "application/octet-stream")
    {
        $this->attachments[] = array("name" => $name, "data" => $data, "type" => $type);
    }

    function add_text($text, $optional = false)
    {
        if ($text || !$optional)
        {
            $this->text .= $this->__wrap($text, $this->width);
            $this->text .= "\r\n";
        }
    }

    function add_section($section)
    {
        if ($this->text)
        {
            $this->text .= "\r\n";
        }

        $this->text .= "$section\r\n";
        $this->text .= str_repeat("-", $this->width);
        $this->text .= "\r\n";
    }

    function add_detail($label, $text, $optional = false)
    {
        $text = trim($text);

        if ($text || !$optional)
        {
            if ($label)
            {
                $label .= ":";
            }

            $this->text .= str_pad($label, $this->indent);
            $this->text .= $this->__wrap($text, $this->width, $this->indent, false);
            $this->text .= "\r\n";
        }
    }

    function send()
    {
        global $senmail_activated;

		if($senmail_activated == false)
		{
			return false;
		}

		$text = $this->text;
		

        if (count($this->attachments))
        {
            $uid = md5(uniqid(time()));
						
			$headers = "From: ".$this->sender." <".$this->sender.">\r\n";
			$headers .= "Reply-To: ".$this->sender."\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
			$headers .= "This is a multi-part message in MIME format.\r\n";
			$headers .= "--".$uid."\r\n";
			$headers .= "Content-type:text/plain; charset=utf-8\r\n";
			$headers .= "Content-Transfer-Encoding: 8bit\r\n\r\n";
			$headers .= $text."\r\n\r\n";
			//$headers .= "--".$uid."\r\n";
			//$headers .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n"; // use different content types here
			//$headers .= "Content-Transfer-Encoding: base64\r\n";
			//$headers .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
			//$headers .= $content."\r\n\r\n";
			//$headers .= "--".$uid."--";


			foreach ($this->attachments as $attachment)
            {
                $name = $attachment["name"];
                $data = chunk_split(base64_encode($attachment["data"]));
                $type = $attachment["type"];

                $headers .= "--".$uid."\r\n";
                $headers .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n"; // use different content types here
				$headers .= "Content-Transfer-Encoding: base64\r\n";
				$headers .= "Content-Disposition: attachment; filename=\"".$name."\"\r\n\r\n";
                $headers .= $data."\r\n\r\n";
				$headers .= "--".$uid."--";
            }
			
        }
		else
		{
			$headers = "From: " . $this->sender . "\r\n";
			$headers .= "Content-Type: text/plain; charset=utf-8\r\n";
			$headers .= "Content-Transfer-Encoding: 8bit";

			

			if (count($this->ccs))
			{
				$headers .= "\r\nCc: " . join(", ", $this->ccs);
			}

			if (count($this->bccs))
			{
				$headers .= "\r\nBcc: " . join(", ", $this->ccs);
			}
			$headers .= $text;
		}

        //$result = mail(join(", ", $this->recipients), $this->subject, $text, $headers);
		$result = mail(join(", ", $this->recipients), $this->subject, '', $headers);
        
		return $result;


    }

    function __complete_address($address, $name)
    {
        if (trim($name) == "")
        {
            return $address;
        }
        else
        {
            return $this->__quoted_printable_encode($name) . " <" .$address . ">";
        }
    }

    function __quoted_printable_encode($str)
    {
        $enc = preg_replace_callback("/[=\?\_\<\>\(\)\x7F-\xFF]/", array($this, "__quoted_printable_encode_char"), $str);
        $enc = preg_replace("/[\x01-\x20]/", "_", $enc);

        if (strlen($enc) == strlen($str))
        {
            return $str;
        }

        $str = $enc;
        $enc = "";

        while (strlen($str) > 58)
        {
            $chunk = substr($str, 0, 58);

            if ($str[56] == "=")
            {
                $chunk = substr($chunk, 0, 56);
            }
            else if ($str[57] == "=")
            {
                $chunk = substr($chunk, 0, 57);
            }

            $enc .= "=?utf-8?Q?$chunk?= ";
            $str = substr($str, strlen($chunk));
        }

        $enc .= "=?utf-8?Q?$str?=";

        return $enc;
    }

    function __quoted_printable_encode_char($elems)
    {
        return sprintf("=%02X", ord($elems[0]));
    }

    function __wrap($text, $width = 80, $indent = 0, $firstline = true)
    {
        $text = wordwrap($text, $width - $indent);
        $lines = preg_split("/\n/", $text);
        $text = "";

        foreach ($lines as $line)
        {
            if ($text)
            {
                $text .= "\n";
            }

            if ($indent && (($text == "" && $firstline) || $text <> ""))
            {
                $text .= str_repeat(" ", $indent);
            }

            $text .= $line;
        }

        return $text;
    }
}

?>