<?php
/********************************************************************

    db.php

    Database utility functions.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-28
    Modified by:    Anton Hofmann (anton.hofmann@perron2.ch)
    Date modified:  2004-09-27
    Version:        1.3.6

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

/********************************************************************
    Constant definitions
*********************************************************************/

define("ITEM_TYPE_STANDARD", 1);
define("ITEM_TYPE_SPECIAL", 2);
define("ITEM_TYPE_COST_ESTIMATION", 3);
define("ITEM_TYPE_EXCLUSION", 4);
define("ITEM_TYPE_NOTIFICATION", 5);
define("ITEM_TYPE_LOCALCONSTRUCTIONCOST", 6);
define("ITEM_TYPE_SERVICES", 7);

/********************************************************************
    Structure of database including dependencies on other tables
*********************************************************************/

$__db_structure = array(
    "account_numbers" => array(
        "key" => "account_number_id",
        "references" => array()),
    "actual_order_states" => array(
        "key" => "actual_order_state_id",
        "references" => array(
            array("table" => "orders",
                  "key" => "actual_order_state_order",
                  "cascade" => true),
            array("table" => "order_states",
                  "key" => "actual_order_state_state"),
            array("table" => "users",
                  "key" => "actual_order_state_user"))),
    "additional_rental_cost_types" => array(
        "key" => "additional_rental_cost_type_id",
        "references" => array()),
	"addons" => array(
        "key" => "addon_id",
        "references" => array(
            array("table" => "items",
                  "key" => "addon_parent"),
            array("table" => "items",
                  "key" => "addon_child",
                  "cascade" => true))),
    "addresses" => array(
        "key" => "address_id",
        "references" => array(
            array("table" => "countries",
                  "key" => "address_country"),
            array("table" => "currencies",
                  "key" => "address_currency"),
            array("table" => "address_types",
                  "key" => "address_type"),
            array("table" => "addresses",
                  "key" => "address_parent"),
			array("table" => "addresses",
                  "key" => "address_invoice_recipient"),
            array("table" => "places",
                  "key" => "address_place_id"),
			array("table" => "users",
                  "key" => "address_contact",))),
	"_addresses" => array(
        "key" => "address_id",
        "references" => array(
            array("table" => "countries",
                  "key" => "address_country"),
            array("table" => "currencies",
                  "key" => "address_currency"),
            array("table" => "address_types",
                  "key" => "address_type"),
            array("table" => "addresses",
                  "key" => "address_parent"),
			array("table" => "addresses",
                  "key" => "address_invoice_recipient"),
		    array("table" => "places",
                  "key" => "address_place_id"),
            array("table" => "users",
                  "key" => "address_contact",))),
    "addressfiles" => array(
        "key" => "addressfile_id",
        "references" => array(
            array("table" => "addresses",
                  "key" => "addressfile_address"))),
	"address_types" => array(
        "key" => "address_type_id",
        "references" => array()),
    "agreement_types" => array(
        "key" => "agreement_type_id",
        "references" => array()),
	"areaperception_types" => array(
        "key" => "areaperception_type_id",
        "references" => array()),
	"baskets" => array(
        "key" => "basket_id",
        "references" => array(
            array("table" => "users",
                  "key" => "basket_user",
                  "cascade" => true))),
    "basket_items" => array(
        "key" => "basket_item_id",
        "references" => array(
            array("table" => "baskets",
                  "key" => "basket_item_basket",
                  "cascade" => true),
            array("table" => "items",
                  "key" => "basket_item_item",
                  "cascade" => true),
            array("table" => "categories",
                  "key" => "basket_item_category",
                  "cascade" => true))),
	"budget_approvals" => array(
        "key" => "budget_approval_id",
        "references" => array(
            array("table" => "users",
                  "key" => "budget_approval_user",
                  "cascade" => true))),
    "business_units" => array(
        "key" => "business_unit_id",
        "references" => array(
            array("table" => "users",
                  "key" => "business_unit_responsible",
                  "cascade" => true))),
	"ceiling_heights" => array(
        "key" => "ceiling_height_id",
       "references" => array(
            array("table" => "product_lines",
                  "key" => "ceiling_height_product_line",
                  "cascade" => true))),
	"cer_brands" => array(
        "key" => "cer_brand_id",
       ),
	"cer_draft_additional_rental_cost_amounts" => array(
        "key" => "cer_additional_rental_cost_amount_id",
        "references" => array(
			array("table" => "cer_drafts",
                  "key" => "cer_additional_rental_cost_amount_draft_id",
                  "cascade" => true),
			array("table" => "additional_rental_cost_types",
                  "key" => "cer_additional_rental_cost_amount_costtype_id",
                  "cascade" => true))),
	"cer_draft_additional_rental_costs" => array(
        "key" => "cer_additional_rental_cost_id",
        "references" => array(
			array("table" => "additional_rental_cost_types",
                  "key" => "cer_additional_rental_cost_type_id",
                  "cascade" => true),
			array("table" => "cer_drafts",
                  "key" => "cer_additional_rental_cost_draft_id",
                  "cascade" => true))),
	"cer_drafts" => array(
        "key" => "cer_basicdata_id",
        "references" => array(
            array("table" => "users",
                  "key" => "cer_basicdata_user_id",
                  "cascade" => true))),
	"cer_draft_files" => array(
        "key" => "cer_draft_file_id",
        "references" => array(
            array("table" => "cer_drafts",
                  "key" => "cer_draft_file_draft",
                  "cascade" => true))),
	"cer_draft_fixed_rents" => array(
        "key" => "cer_fixed_rent_id",
        "references" => array(
            array("table" => "cer_drafts",
                  "key" => "cer_fixed_rent_draft_id",
                  "cascade" => true))),
	"cer_draft_investments" => array(
        "key" => "cer_investment_id",
        "references" => array(
			array("table" => "posinvestment_types",
                  "key" => "cer_investment_type",
                  "cascade" => true),
			array("table" => "cer_drafts",
                  "key" => "cer_investment_draft_id",
                  "cascade" => true))),
	"cer_draft_expenses" => array(
        "key" => "cer_expense_id",
        "references" => array(
			array("table" => "cer_expense_types",
                  "key" => "cer_expense_type",
                  "cascade" => true),
			array("table" => "cer_drafts",
                  "key" => "cer_expense_draft_id",
                  "cascade" => true))),
	"cer_draft_paymentterms" => array(
        "key" => "cer_paymentterm_id",
        "references" => array(
			array("table" => "cer_drafts",
                  "key" => "cer_paymentterm_draft_id",
                  "cascade" => true))),
	"cer_draft_rent_percent_from_sales" => array(
        "key" => "cer_rent_percent_from_sale_id",
        "references" => array(
			array("table" => "cer_drafts",
                  "key" => "cer_rent_percent_from_sale_draft_id",
                  "cascade" => true))),
	"cer_draft_revenues" => array(
        "key" => "cer_revenue_id",
        "references" => array(
			array("table" => "cer_drafts",
                  "key" => "cer_revenue_draft_id",
                  "cascade" => true), 
			array("table" => "cer_brands",
                  "key" => "cer_revenue_brand_id",
                  "cascade" => true))),
	"cer_draft_salaries" => array(
        "key" => "cer_salary_id",
        "references" => array(
			array("table" => "cer_drafts",
                  "key" => "cer_salary_draft_id",
                  "cascade" => true), 
		    array("table" => "cer_staff_types",
                  "key" => "cer_salary_staff_type",
                  "cascade" => true))),
	"cer_draft_stocks" => array(
        "key" => "cer_stock_id",
        "references" => array(
			array("table" => "cer_drafts",
                  "key" => "cer_stock_draft_id",
                  "cascade" => true))),
	"cer_approvalnames" => array(
        "key" => "cer_approvalname_id",
        "references" => array()),
	"cer_additional_rental_cost_amounts" => array(
        "key" => "cer_additional_rental_cost_amount_id",
        "references" => array(
			array("table" => "projects",
                  "key" => "cer_additional_rental_cost_amount_project",
                  "cascade" => true),
			array("table" => "additional_rental_cost_types",
                  "key" => "cer_additional_rental_cost_amount_costtype_id",
                  "cascade" => true))),
	"cer_additional_rental_costs" => array(
        "key" => "cer_additional_rental_cost_id",
        "references" => array(
			array("table" => "additional_rental_cost_types",
                  "key" => "cer_additional_rental_cost_type_id",
                  "cascade" => true),
			array("table" => "projects",
                  "key" => "cer_additional_rental_cost_project",
                  "cascade" => true))),
	"cer_basicdata" => array(
        "key" => "cer_basicdata_id",
        "references" => array(
			array("table" => "currencies",
                  "key" => "cer_basicdata_currency",
                  "cascade" => true),
			array("table" => "users",
                  "key" => "cer_basicdata_submitted_by",
                  "cascade" => true),
			array("table" => "users",
                  "key" => "cer_basicdata_resubmitted_by",
                  "cascade" => true),
			array("table" => "users",
                  "key" => "cer_basicdata_rejected_by",
                  "cascade" => true),
			array("table" => "projects",
                  "key" => "cer_basicdata_project",
                  "cascade" => true))),
	"cer_benchmarkpermissions" => array(
        "key" => "cer_benchmarkpermission_id",
        "references" => array(
            array("table" => "cer_benchmarks",
                  "key" => "cer_benchmarkpermission_benchmark",
                  "cascade" => true),
			array("table" => "users",
                  "key" => "cer_benchmarkpermission_user"))),
	"cer_benchmarks" => array(
        "key" => "cer_benchmark_id",
        "references" => array(
			array("table" => "project_states",
                  "key" => "cer_benchmark_project_state",
                  "cascade" => true))),
	"cer_downloads" => array(
        "key" => "cer_download_id",
        "references" => array()),
	"cer_expense_types" => array(
        "key" => "cer_expense_type_id",
        "references" => array()),
	"cer_expenses" => array(
        "key" => "cer_expense_id",
        "references" => array(
			array("table" => "cer_expense_types",
                  "key" => "cer_expense_type",
                  "cascade" => true),
			array("table" => "projects",
                  "key" => "cer_expense_project",
                  "cascade" => true))),
	"cer_financialsummary_captions" => array(
        "key" => "cer_financialsummary_caption_id",
        "references" => array()),
	"cer_fixed_rents" => array(
        "key" => "cer_fixed_rent_id",
        "references" => array(
			array("table" => "projects",
                  "key" => "cer_fixed_rent_project_id",
                  "cascade" => true))),
	"cer_inflationrates" => array(
        "key" => "inflationrate_id",
        "references" => array(
			array("table" => "countries",
                  "key" => "inflationrate_country",
                  "cascade" => true))),
	"cer_interestrates" => array(
        "key" => "interestrate_id",
        "references" => array(
			array("table" => "countries",
                  "key" => "interestrate_country",
                  "cascade" => true))),
	"cer_investments" => array(
        "key" => "cer_investment_id",
        "references" => array(
			array("table" => "posinvestmen_types",
                  "key" => "cer_investment_type",
                  "cascade" => true),
			array("table" => "projects",
                  "key" => "cer_investmentproject",
                  "cascade" => true))),
	"cer_mails" => array(
        "key" => "cer_mail_id",
        "references" => array(
			array("table" => "users",
                  "key" => "cer_mail_sender",
                  "cascade" => true),
			array("table" => "users",
                  "key" => "cer_mail_reciepient",
                  "cascade" => true),
			array("table" => "projects",
                  "key" => "cer_mail_project",
                  "cascade" => true))),
	"cer_paymentterms" => array(
        "key" => "cer_paymentterm_id",
        "references" => array(
			array("table" => "projects",
                  "key" => "cer_paymentterm_project",
                  "cascade" => true))),
	"cer_rent_percent_from_sales" => array(
        "key" => "cer_rent_percent_from_sale_id",
        "references" => array(
			array("table" => "projects",
                  "key" => "cer_rent_percent_from_sale_project",
                  "cascade" => true))),
	"cer_revenues" => array(
        "key" => "cer_revenue_id",
        "references" => array(
			array("table" => "projects",
                  "key" => "cer_revenue_project",
                  "cascade" => true), 
			array("table" => "cer_brands",
                  "key" => "cer_revenue_brand_id",
                  "cascade" => true))),
	"cer_salaries" => array(
        "key" => "cer_salary_id",
        "references" => array(
			array("table" => "projects",
                  "key" => "cer_salary_project",
                  "cascade" => true), 
		    array("table" => "cer_staff_types",
                  "key" => "cer_salary_staff_type",
                  "cascade" => true))),
    "cer_staff_types" => array(
        "key" => "cer_staff_type_id",
        "references" => array()),
	"cer_stocks" => array(
        "key" => "cer_stock_id",
        "references" => array(
			array("table" => "projects",
                  "key" => "cer_stock_project",
                  "cascade" => true))),
	"cer_standardparameters" => array(
        "key" => "cer_standardparameter_id",
        "references" => array()),
	"cer_summary" => array(
        "key" => "cer_summary_id",
        "references" => array(
			array("table" => "projects",
                  "key" => "cer_summary_project",
                  "cascade" => true))),
	"cer_refundings" => array(
        "key" => "cer_refunding_id",
        "references" => array(
			 array("table" => "currencies",
                  "key" => "cer_refunding_currency_id",
                  "cascade" => true),
			 array("table" => "users",
                  "key" => "cer_refunding_sumbitted_by",
                  "cascade" => true),
			 array("table" => "users",
                  "key" => "cer_refunding_resubmitted_by",
                  "cascade" => true),
			 array("table" => "users",
                  "key" => "cer_refunding_rejected_by",
                  "cascade" => true),
			array("table" => "projects",
                  "key" => "cer_refunding_project",
                  "cascade" => true))),
	"cer_version_leases" => array(
        "key" => "poslease_id",
        "references" => array(
			array("table" => "cer_basicdata",
                  "key" => "poslease_cer_basicdata_id",
                  "cascade" => true),
			array("table" => "poslease_types",
                  "key" => "poslease_lease_type",
                  "cascade" => true))),
	"categories" => array(
        "key" => "category_id",
        "references" => array(
            array("table" => "product_lines",
                  "key" => "category_product_line",
                  "cascade" => true))),
    "category_files" => array(
        "key" => "category_file_id",
        "references" => array(
            array("table" => "items",
                  "key" => "category_file_category",
                  "cascade" => true),
            array("table" => "file_types",
                  "key" => "category_file_type"),
            array("table" => "file_purposes",
                  "key" => "category_file_purpose")),
        "files" => array(
            array("field" => "category_file_path"))),
    "category_items" => array(
        "key" => "category_item_id",
        "references" => array(
            array("table" => "categories",
                  "key" => "category_item_category",
                  "cascade" => true),
            array("table" => "items",
                  "key" => "category_item_item"))),
    "client_types" => array(
        "key" => "client_type_id",
        "references" => array()),
	"closing_dates" => array(
        "key" => "closing_date_id",
        "references" => array()),
    "comments" => array(
        "key" => "comment_id",
        "references" => array(
            array("table" => "orders",
                  "key" => "comment_order",
                  "cascade" => true),
            array("table" => "order_items",
                  "key" => "comment_order_item",
                  "cascade" => true),
            array("table" => "users",
                  "key" => "comment_user"),
            array("table" => "comment_categories",
                  "key" => "comment_category"),
            array("table" => "actual_order_states",
                  "key" => "comment_actual_order_state"))),
    "comment_addresses" => array(
        "key" => "comment_address_id",
        "references" => array(
            array("table" => "comments",
                  "key" => "comment_address_comment",
                  "cascade" => true),
            array("table" => "addresses",
                  "key" => "comment_address_address"))),
    "comment_categories" => array(
        "key" => "comment_category_id",
        "references" => array(
            array("table" => "order_types",
                  "key" => "comment_category_order_type"))),
    "cost_centers" => array(
        "key" => "cost_center_id",
        "references" => array()),
    "costmonitoringgroups" => array(
        "key" => "costmonitoringgroup_id",
        "references" => array()),
    "costsheet_bids" => array(
        "key" => "costsheet_bid_id",
        "references" => array(
            array("table" => "projects",
                  "key" => "costsheet_bid_project_id",
                  "cascade" => true),
		    array("table" => "pcost_groups",
                  "key" => "costsheet_bid_pcost_group_id",
                  "cascade" => true))),
	"costsheet_bid_positions" => array(
        "key" => "costsheet_bid_position_id",
        "references" => array(
			array("table" => "costsheet_bids",
                  "key" => "costsheet_bid_position_costsheet_bid_id",
                  "cascade" => true),
			array("table" => "projects",
                  "key" => "costsheet_bid_position_project_id",
                  "cascade" => true),
		    array("table" => "costsheets",
                  "key" => "costsheet_bid_position_costsheet_id",
                  "cascade" => true),
			 array("table" => "pcost_groups",
                  "key" => "costsheet_bid_position_pcost_group_id",
                  "cascade" => true),
			array("table" => "pcost_subgroups",
                  "key" => "costsheet_bid_position_pcost_subgroup_id",
                  "cascade" => true))),
	"costsheets" => array(
        "key" => "costsheet_id",
        "references" => array(
            array("table" => "projects",
                  "key" => "costsheet_project_id"),
			array("table" => "pcost_groups",
                  "key" => "costsheet_pcost_group_id"),
		    array("table" => "pcost_subgroups",
                  "key" => "costsheet_pcost_subgroup_id"))),
	"countries" => array(
        "key" => "country_id",
        "references" => array(
            array("table" => "regions",
                  "key" => "country_region"),
		    array("table" => "salesregions",
                  "key" => "country_salesregion"))),
    "country_access" => array(
        "key" => "country_access_id",
        "references" => array(
            array("table" => "users",
                  "key" => "country_access_user",
                  "cascade" => true),
            array("table" => "countries",
                  "key" => "country_access_country",
                  "cascade" => true))),
    "currencies" => array(
        "key" => "currency_id",
        "references" => array()),
    "dates" => array(
        "key" => "date_id",
        "references" => array(
            array("table" => "order_items",
                  "key" => "date_order_item",
                  "cascade" => true),
            array("table" => "date_types",
                  "key" => "date_type"))),
    "date_types" => array(
        "key" => "date_type_id",
        "references" => array()),
    "design_objective_groups" => array(
        "key" => "design_objective_group_id",
        "references" => array(
            array("table" => "postypes",
                  "key" => "design_objective_group_postype",
                  "cascade" => true),
			array("table" => "product_lines",
                  "key" => "design_objective_group_product_line",
                  "cascade" => true))),
    "design_objective_items" => array(
        "key" => "design_objective_item_id",
        "references" => array(
            array("table" => "design_objective_groups",
                  "key" => "design_objective_item_group",
                  "cascade" => true))),
	"doc_mailalerts" => array(
        "key" => "doc_mailalert_id",
        "references" => array()),
    "documents" => array(
        "key" => "document_id",
        "references" => array(
            array("table" => "orders",
                  "key" => "document_order",
                  "cascade" => true),
            array("table" => "order_file_categories",
                  "key" => "document_category"))),
    "download_countries" => array(
        "key" => "download_countries_id",
        "references" => array(
            array("table" => "links",
                  "key" => "download_countries_download",
                  "cascade" => true),
            array("table" => "countries",
                  "key" => "download_countries_country",
                  "cascade" => true))),
    "download_roles" => array(
        "key" => "download_roles_id",
        "references" => array(
            array("table" => "links",
                  "key" => "download_roles_download",
                  "cascade" => true),
            array("table" => "roles",
                  "key" => "download_roles_role",
                  "cascade" => true))),
    "eventmails" => array(
        "key" => "eventmail_id",
        "references" => array(
            array("table" => "users",
                  "key" => "eventmail_user01"),
            array("table" => "users",
                  "key" => "eventmail_user02"),
		    array("table" => "users",
                  "key" => "eventmail_user03"),
            array("table" => "users",
                  "key" => "eventmail_user04"))),
	"file_purposes" => array(
        "key" => "file_purpose_id",
        "references" => array()),
    "file_types" => array(
        "key" => "file_type_id",
        "references" => array()),
	"invoice_addresses" => array(
        "key" => "invoice_address_id",
        "references" => array(
			array("table" => "addresses",
                  "key" => "invoice_address_address_id",
                  "cascade" => true),
			array("table" => "countries",
                  "key" => "invoice_address_country"),
			array("table" => "places",
                  "key" => "invoice_address_place_id"))),
	"issuetypes" => array(
        "key" => "issuetype_id",
        "references" => array()),
    "issuestates" => array(
        "key" => "issuestate_id",
        "references" => array()),
    "issues" => array(
        "key" => "issue_id",
        "references" => array(
            array("table" => "users",
                  "key" => "issue_owner"),
            array("table" => "issuestates",
                  "key" => "issuestate_id"),
            array("table" => "issuetypes",
                  "key" => "issue_type"))),
    "items" => array(
        "key" => "item_id",
        "references" => array(
            array("table" => "item_types",
                  "key" => "item_type"), 
			array("table" => "units",
                  "key" => "item_unit"), 
            array("table" => "item_categories",
                  "key" => "item_category"))),
    "item_categories" => array(
        "key" => "item_category_id",
        "references" => array()),
    "item_compositions" => array(
        "key" => "item_composition_id",
        "references" => array(
            array("table" => "items",
                  "key" => "item_composition_item"),
            array("table" => "item_groups",
                  "key" => "item_composition_group",
                  "cascade" => true))),
    "item_files" => array(
        "key" => "item_file_id",
        "references" => array(
            array("table" => "items",
                  "key" => "item_file_item",
                  "cascade" => true),
            array("table" => "file_types",
                  "key" => "item_file_type"),
            array("table" => "file_purposes",
                  "key" => "item_file_purpose")),
        "files" => array(
            array("field" => "item_file_path"))),
    "item_groups" => array(
        "key" => "item_group_id",
        "references" => array()),
    "item_group_items" => array(
        "key" => "item_group_item_id",
        "references" => array(
            array("table" => "item_groups",
                  "key" => "item_group_item_group"),
            array("table" => "items",
                  "key" => "item_group_item_item",
                  "cascade" => true))),
    "item_group_files" => array(
        "key" => "item_group_file_id",
        "references" => array(
            array("table" => "item_groups",
                  "key" => "item_group_file_group",
                  "cascade" => true),
            array("table" => "file_types",
                  "key" => "item_group_file_type"),
            array("table" => "file_purposes",
                  "key" => "item_group_file_purpose")),
        "files" => array(
            array("field" => "item_group_file_path"))),
    "item_group_options" => array(
        "key" => "item_group_option_id",
        "references" => array(
            array("table" => "item_groups",
                  "key" => "item_group_option_group"),
            array("table" => "items",
                  "key" => "item_group_option_item1",
                  "cascade" => true),
            array("table" => "items",
                  "key" => "item_group_option_item2",
                  "cascade" => true))),
    "item_pos_types" => array(
        "key" => "item_pos_type_id",
        "references" => array(
            array("table" => "postypes",
                  "key" => "item_pos_type_pos_type"),
			array("table" => "product_lines",
                  "key" => "item_pos_type_product_line"),
            array("table" => "items",
                  "key" => "item_pos_type_item"), 
				  "cascade" => true)),
	"item_options" => array(
        "key" => "item_option_id",
        "references" => array(
            array("table" => "items",
                  "key" => "item_option_parent"),
            array("table" => "items",
                  "key" => "item_option_child",
                  "cascade" => true))),
    "item_option_files" => array(
        "key" => "item_option_file_id",
        "references" => array(
            array("table" => "item_options",
                  "key" => "item_option_file_option",
                  "cascade" => true),
            array("table" => "file_types",
                  "key" => "item_option_file_type"),
            array("table" => "file_purposes",
                  "key" => "item_option_file_purpose")),
        "files" => array(
            array("field" => "item_option_file_path"))),
    "item_regions" => array(
        "key" => "item_region_id",
        "references" => array(
            array("table" => "items",
                  "key" => "item_region_item",
                  "cascade" => true),
            array("table" => "regions",
                  "key" => "item_region_region"))),
    "item_types" => array(
        "key" => "item_type_id",
        "references" => array()),
    "languages" => array(
        "key" => "language_id",
        "references" => array()),
	"links" => array(
        "key" => "link_id",
        "references" => array(
            array("table" => "link_topics",
                  "key" => "link_topic",
                  "cascade" => false),
			array("table" => "link_categories",
                  "key" => "link_category")),
        "files" => array(
            array("field" => "link_path"))),
    "link_categories" => array(
        "key" => "link_category_id",
        "references" => array()),
	"link_topics" => array(
        "key" => "link_topic_id",
        "references" => array()),
	"ln_basicdata" => array(
        "key" => "ln_basicdata_id",
        "references" => array(
			array("table" => "users",
                  "key" => "ln_basicdata_submitted_by",
                  "cascade" => true),
			array("table" => "users",
                  "key" => "ln_basicdata_resubmitted_by",
                  "cascade" => true),
			array("table" => "users",
                  "key" => "ln_basicdata_rejected_by",
                  "cascade" => true),
			array("table" => "projects",
                  "key" => "ln_basicdata_project",
                  "cascade" => true))),
    "ln_basicdata_inr03" => array(
        "key" => "ln_basicdata_lnr03_id",
        "references" => array(
			array("table" => "ln_basicdata",
                  "key" => "ln_basicdata_lnr03_lnbasicdata_id",
                  "cascade" => true),
			array("table" => "possellouts",
                  "key" => "ln_basicdata_lnr03_sellout_id",
                  "cascade" => true),
			array("table" => "possellouts",
                  "key" => "ln_basicdata_lnr03_profitability_id",
                  "cascade" => true))),
    "ln_basicdata_lnr03_brands" => array(
        "key" => "ln_basicdata_lnr03_brand_id",
        "references" => array(
            array("table" => "cer_basicdata",
                  "key" => "ln_basicdata_lnr03_brand_cerbasicdata_id",
                  "cascade" => true),
			array("table" => "sg_brands",
                  "key" => "ln_basicdata_lnr03_brand_sg_brand_id",
                  "cascade" => true),
			array("table" => "postypes",
                  "key" => "ln_basicdata_lnr03_brand_postype_id",
                  "cascade" => true),
			array("table" => "posowner_types",
                  "key" => "ln_basicdata_lnr03_brand_legaltype_id"))),
	"ln_basicdata_lnr03_brands_dummies" => array(
        "key" => "ln_basicdata_lnr03_brand_id",
        "references" => array()),
	"ln_version_leases" => array(
        "key" => "poslease_id",
        "references" => array(
			array("table" => "ln_basicdata",
                  "key" => "poslease_ln_basicdata_id",
                  "cascade" => true),
			array("table" => "poslease_types",
                  "key" => "poslease_lease_type",
                  "cascade" => true))),
    "loc_areaperception_types" => array(
        "key" => "loc_areaperception_type_id",
        "references" => array(
            array("table" => "areaperception_types",
                  "key" => "loc_areaperception_type_type",
                  "cascade" => true),
            array("table" => "languages",
                  "key" => "loc_areaperception_type_language"))),
	"loc_languages" => array(
        "key" => "loc_language_id",
        "references" => array(
            array("table" => "languages",
                  "key" => "loc_language_language_id",
                  "cascade" => true),
            array("table" => "languages",
                  "key" => "loc_language_language"))),
	"loc_countries" => array(
        "key" => "loc_country_id",
        "references" => array(
            array("table" => "countries",
                  "key" => "loc_country_country",
                  "cascade" => true),
            array("table" => "languages",
                  "key" => "loc_country_language"))),
	"loc_places" => array(
        "key" => "loc_place_id",
        "references" => array(
            array("table" => "countries",
                  "key" => "loc_place_country",
                  "cascade" => true),
			array("table" => "provinces",
                  "key" => "loc_place_province",
                  "cascade" => true),
		    array("table" => "places",
                  "key" => "loc_place_place",
                  "cascade" => true),
            array("table" => "languages",
                  "key" => "loc_place_language"))),
	"loc_posaddresses" => array(
        "key" => "loc_posaddress_id",
        "references" => array(
            array("table" => "posaddresses",
                  "key" => "loc_posaddress_posaddress"),
			array("table" => "languages",
                  "key" => "loc_posaddress_language"))),
	"loc_posareatypes" => array(
        "key" => "loc_posareatype_id",
        "references" => array(
            array("table" => "posareatypes",
                  "key" => "loc_posareatype_posareatype",
                  "cascade" => true),
            array("table" => "languages",
                  "key" => "loc_posareatype_language"))),
	"loc_possubclasses" => array(
        "key" => "loc_possubclass_id",
        "references" => array(
            array("table" => "possubclasses",
                  "key" => "loc_possubclass_possubclass",
                  "cascade" => true),
            array("table" => "languages",
                  "key" => "loc_possubclass_language"))),
	"loc_provinces" => array(
        "key" => "loc_province_id",
        "references" => array(
            array("table" => "countries",
                  "key" => "loc_province_country",
                  "cascade" => true),
			array("table" => "provinces",
                  "key" => "loc_province_province",
                  "cascade" => true),
            array("table" => "languages",
                  "key" => "loc_province_language"))),
	"loc_weekdays" => array(
        "key" => "loc_weekday_id",
        "references" => array(
            array("table" => "weekdays",
                  "key" => "loc_weekday_weekday_id",
                  "cascade" => true),
            array("table" => "languages",
                  "key" => "loc_weekday_language_id"))),
	"location_types" => array(
        "key" => "location_type_id",
        "references" => array()),
    "mail_alert_types" => array(
        "key" => "mail_alert_type_id",
        "references" => array()),
	"mail_alerts" => array(
        "key" => "mail_alert_id",
        "references" => array(
            array("table" => "orders",
                  "key" => "order_id",
                  "cascade" => true),
            array("table" => "mail_alert_types",
                  "key" => "mail_alert_type_id",
                  "cascade" => true))),
	"messages" => array(
        "key" => "message_id",
        "references" => array()),
    "message_email_sent" => array(
        "key" => "message_email_sent_id",
        "references" => array(
            array("table" => "messages",
                  "key" => "message_email_sent_message",
                  "cascade" => true),
            array("table" => "users",
                  "key" => "message_email_sent_user",
                  "cascade" => true),
            array("table" => "users",
                  "key" => "message_email_sent_touser",
                  "cascade" => true))),
    "message_emails" => array(
        "key" => "message_email_id",
        "references" => array(
            array("table" => "messages",
                  "key" => "message_email_message",
                  "cascade" => true),
            array("table" => "users",
                  "key" => "message_email_user",
                  "cascade" => true))),
    "message_roles" => array(
        "key" => "message_roles_id",
        "references" => array(
            array("table" => "messages",
                  "key" => "message_roles_message",
                  "cascade" => true),
            array("table" => "roles",
                  "key" => "message_roles_role",
                  "cascade" => true))),
    "milestones" => array(
        "key" => "milestone_id",
        "references" => array()),
    "mis_queries" => array(
        "key" => "mis_query_id",
		"cascade" => true, 
        "references" => array(
		)),
	"mis_querypermissions" => array(
        "key" => "mis_querypermission_id",
        "references" => array(
            array("table" => "mis_queries",
                  "key" => "mis_querypermission_query",
                  "cascade" => true),
			array("table" => "users",
                  "key" => "mis_querypermission_user"))),
	"mps_distchannels" => array(
        "key" => "mps_distchannel_id",
        "references" => array()),
	"oldproject_numbers" => array(
        "key" => "oldproject_number_id",
        "references" => array(
            array("table" => "projects",
                  "key" => "oldproject_number_project_id",
                  "cascade" => true),
			array("table" => "users",
                  "key" => "oldproject_number_user_id"))),
	"openinghrs" => array(
        "key" => "openinghr_id",
        "references" => array()),
	"orders" => array(
        "key" => "order_id",
        "references" => array(
            array("table" => "order_types",
                  "key" => "order_type"),
            array("table" => "users",
                  "key" => "order_retail_operator"),
            array("table" => "users",
                  "key" => "order_delivery_confirmation_by"),
            array("table" => "addresses",
                  "key" => "order_client_address"),
            array("table" => "addresses",
                  "key" => "order_supplier_address"),
            array("table" => "addresses",
                  "key" => "order_forwarder_address"),
            array("table" => "currencies",
                  "key" => "order_client_currency"),
            array("table" => "currencies",
                  "key" => "order_system_currency"),
            array("table" => "transportation_types",
                  "key" => "order_preferred_transportation_arranged"),
			array("table" => "transportation_types",
                  "key" => "order_preferred_transportation_mode"),
            array("table" => "voltages",
                  "key" => "order_voltage"),
			 array("table" => "invoice_addresses",
                  "key" => "order_direct_invoice_address_id"))),
    "order_addresses" => array(
        "key" => "order_address_id",
        "references" => array(
            array("table" => "orders",
                  "key" => "order_address_order",
                  "cascade" => true),
            array("table" => "order_items",
                  "key" => "order_address_order_item",
                  "cascade" => true),
            array("table" => "order_address_types",
                  "key" => "order_address_type"),
            array("table" => "countries",
                  "key" => "order_address_country"),
			 array("table" => "places",
                  "key" => "order_address_place_id"),
            array("table" => "addresses",
                  "key" => "order_address_parent"))),
    "order_address_types" => array(
        "key" => "order_address_type_id",
        "references" => array()),
    "order_files" => array(
        "key" => "order_file_id",
        "references" => array(
            array("table" => "orders",
                  "key" => "order_file_order",
                  "cascade" => true),
            array("table" => "files_types",
                  "key" => "order_file_type"),
            array("table" => "order_file_categories",
                  "key" => "order_file_category"),
            array("table" => "users",
                  "key" => "order_file_owner")),
        "files" => array(
            array("field" => "order_file_path"))),
    "order_file_addresses" => array(
        "key" => "order_file_address_id",
        "references" => array(
            array("table" => "order_files",
                  "key" => "order_file_address_file",
                  "cascade" => true),
            array("table" => "addresses",
                  "key" => "order_file_address_address"))),
    "order_file_categories" => array(
        "key" => "order_file_category_id",
        "references" => array()),
    "order_items" => array(
        "key" => "order_item_id",
        "references" => array(
            array("table" => "orders",
                  "key" => "order_item_order",
                  "cascade" => true),
            array("table" => "items",
                  "key" => "order_item_item"),
            array("table" => "item_types",
                  "key" => "order_item_type"),
            array("table" => "currencies",
                  "key" => "order_item_supplier_currency"),
            array("table" => "transportation_types",
                  "key" => "order_item_transportation"),
            array("table" => "addresses",
                  "key" => "order_item_supplier_address"),
            array("table" => "addresses",
                  "key" => "order_item_forwarder_address"),
            array("table" => "project_cost_groupnames",
                  "key" => "order_item_cost_group"),
		     array("table" => "costmonitoringgroups",
                  "key" => "item_costmonitoring_group"))),
    "order_item_files" => array(
        "key" => "order_item_file_id",
        "references" => array(
            array("table" => "order_items",
                  "key" => "order_item_file_order_item",
                  "cascade" => true),
            array("table" => "files_types",
                  "key" => "order_item_file_type"),
            array("table" => "order_file_categories",
                  "key" => "order_item_file_category"),
            array("table" => "users",
                  "key" => "order_item_file_owner"))),
    "order_item_file_roles" => array(
        "key" => "order_item_file_role_id",
        "references" => array(
            array("table" => "order_item_files",
                  "key" => "order_item_file_role_file",
                  "cascade" => true),
            array("table" => "roles",
                  "key" => "order_item_file_role_role"))),
    "order_mails" => array(
        "key" => "order_mail_id",
        "references" => array(
            array("table" => "orders",
                  "key" => "order_mail_order",
                  "cascade" => true),
            array("table" => "users",
                  "key" => "order_mail_user"),
            array("table" => "users",
                  "key" => "order_mail_from_user"),
            array("table" => "order_states",
                  "key" => "order_mail_order_state"))),
    "order_types" => array(
        "key" => "order_type_id",
        "references" => array()),
    "order_state_groups" => array(
        "key" => "order_state_group_id",
        "references" => array(
            array("table" => "order_types",
                  "key" => "order_state_group_order_type"))),
    "order_states" => array(
        "key" => "order_state_id",
        "references" => array(
            array("table" => "order_types",
                  "key" => "order_state_order_type",
                  "cascade" => true),
            array("table" => "order_state_groups",
                  "key" => "order_state_group"),
            array("table" => "notification_recipients",
                  "key" => "order_state_notification_recipient"))),
    "parts" => array(
        "key" => "part_id",
        "references" => array(
            array("table" => "items",
                  "key" => "part_parent"),
            array("table" => "items",
                  "key" => "part_child",
                  "cascade" => true))),
	"pcost_groups" => array(
        "key" => "pcost_group_id",
        "references" => array(
            array("table" => "posinvestment_types",
                  "key" => "pcost_group_posinvestment_type_id"))),
	"pcost_positions" => array(
        "key" => "pcost_position_id",
        "references" => array(
            array("table" => "pcost_templates",
                  "key" => "pcost_position_pcost_template_id",
                  "cascade" => true),
            array("table" => "pcost_groups",
                  "key" => "pcost_position_pcostgroup_id",
                  "cascade" => true),
			array("table" => "pcost_subgroups",
                  "key" => "pcost_position_pcost_subgroup_id"))),
	"pcost_subgroups" => array(
        "key" => "pcost_subgroup_id",
        "references" => array(
            array("table" => "pcost_groups",
                  "key" => "pcost_subgroup_pcostgroup_id"))),
	"pcost_templates" => array(
        "key" => "pcost_template_id",
        "references" => array()),
	"permissions" => array(
        "key" => "permission_id",
        "references" => array()),
	"places" => array(
        "key" => "place_id",
        "references" => array(
            array("table" => "countries",
                  "key" => "place_country",
                  "cascade" => true),
            array("table" => "provinces",
                  "key" => "place_province"))),
	"_posaddresses" => array(
        "key" => "posaddress_id",
        "references" => array(
			array("table" => "addresses",
                  "key" => "posaddress_client_id"),
			array("table" => "addresses",
                  "key" => "posaddress_franchisor_id"),
			array("table" => "addresses",
                  "key" => "posaddress_franchisee_id"),
			array("table" => "posowner_types",
                  "key" => "posaddress_ownertype"),
			array("table" => "possubclasses",
                  "key" => "posaddress_store_subclass"),
			array("table" => "postypes",
                  "key" => "posaddress_store_postype"),
			array("table" => "posfurnituretypes",
                  "key" => "posaddress_store_furniture"),
			array("table" => "productline_subclasses",
                  "key" => "posaddress_store_furniture_subclass"),
			array("table" => "countries",
                  "key" => "posaddress_country"),
			array("table" => "places",
                  "key" => "posaddress_place_id"))),
	"_posareas" => array(
        "key" => "posarea_id",
        "references" => array(
            array("table" => "posareatypes",
                  "key" => "posarea_area",
                  "cascade" => true),
            array("table" => "_posaddresses",
                  "key" => "posarea_posaddress",
                  "cascade" => true))),
	"_posequipments" => array(
        "key" => "posequipment_id",
        "references" => array(
            array("table" => "posequipmenttypes",
                  "key" => "posequipment_equipment",
                  "cascade" => true),
            array("table" => "_posaddresses",
                  "key" => "posequipment_posaddress",
                  "cascade" => true))),
	"posaddresses" => array(
        "key" => "posaddress_id",
        "references" => array(
			array("table" => "addresses",
                  "key" => "posaddress_client_id"),
			array("table" => "addresses",
                  "key" => "posaddress_franchisor_id"),
			array("table" => "addresses",
                  "key" => "posaddress_franchisee_id"),
			array("table" => "posowner_types",
                  "key" => "posaddress_ownertype"),
			array("table" => "possubclasses",
                  "key" => "posaddress_store_subclass"),
			array("table" => "postypes",
                  "key" => "posaddress_store_postype"),
			array("table" => "posfurnituretypes",
                  "key" => "posaddress_store_furniture"),
			array("table" => "productline_subclasses",
                  "key" => "posaddress_store_furniture_subclass"),
			array("table" => "countries",
                  "key" => "posaddress_country"),
			array("table" => "places",
                  "key" => "posaddress_place_id"))),
	"posareas" => array(
        "key" => "posarea_id",
        "references" => array(
            array("table" => "posareatypes",
                  "key" => "posarea_area",
                  "cascade" => true),
            array("table" => "posaddresses",
                  "key" => "posarea_posaddress",
                  "cascade" => true))),
	"posclosingassessments" => array(
        "key" => "posclosingassessment_id",
        "references" => array(
			array("table" => "posaddresses",
                  "key" => "posclosingassessment_posaddress_id",
                  "cascade" => true))),
    "posclosingassessmentfindates" => array(
        "key" => "posclosingassessmentfindate_id",
        "references" => array(
			array("table" => "posclosingassessments",
                  "key" => "posclosingassessmentfindatesassessment_id",
                  "cascade" => true))),
	"posclosinghrs" => array(
        "key" => "posclosinghr_id",
        "references" => array(
			array("table" => "posaddresses",
                  "key" => "posclosinghr_posaddress_id",
                  "cascade" => true))),
	"posclosings" => array(
        "key" => "posclosing_id",
        "references" => array(
            array("table" => "posaddresses",
                  "key" => "posclosing_posaddress",
                  "cascade" => true),
			array("table" => "projects",
                  "key" => "posclosing_project",
                  "cascade" => true))),
    "posdistributionchannels" => array(
        "key" => "posdistributionchannel_id",
        "references" => array(
            array("table" => "posowner_types",
                  "key" => "posdistributionchannel_legaltype_id",
                  "cascade" => true),
		     array("table" => "postypes",
                  "key" => "posdistributionchannel_postype_id",
                  "cascade" => true),
			 array("table" => "possubclasses",
                  "key" => "posdistributionchannel_possubclass_id",
                  "cascade" => true),
			array("table" => "mps_distchannels",
                  "key" => "posdistributionchannel_dchannel_id",
                  "cascade" => true))),
	"posfilegroups" => array(
        "key" => "posfilegroup_id",
        "references" => array()),
	"posfiles" => array(
        "key" => "posfile_id",
        "references" => array()),
	"posfiletypes" => array(
        "key" => "posfiletype_id",
        "references" => array(
			array("table" => "posfiletypes",
                  "key" => "posfile_filetype",
                  "cascade" => true),
			array("table" => "posfilegroups",
                  "key" => "posfile_filegroup"))),
	"posmails" => array(
        "key" => "posmail_id",
        "references" => array()),
	"posowner_types" => array(
        "key" => "posowner_type_id",
        "references" => array()),
	"posinvestment_types" => array(
        "key" => "posinvestment_type_id",
        "references" => array()),
	"posinvestments" => array(
        "key" => "posinvestment_id",
        "references" => array(
			array("table" => "posaddresses",
                  "key" => "posinvestment_posaddress",
                  "cascade" => true),
			array("table" => "posinvestment_types",
                  "key" => "posinvestment_investment_type"))),
	"posorderinvestments" => array(
        "key" => "posorderinvestment_id",
        "references" => array(
			array("table" => "posorders",
                  "key" => "posorderinvestment_posorder",
                  "cascade" => true),
			array("table" => "posinvestment_types",
                  "key" => "posinvestment_investment_type"))),
	"poslease_types" => array(
        "key" => "poslease_type_id",
        "references" => array()),
	"posleases" => array(
        "key" => "poslease_id",
        "references" => array(
            array("table" => "posaddresses",
                  "key" => "poslease_posaddress",
                  "cascade" => true),
			 array("table" => "orders",
                  "key" => "poslease_order",
                  "cascade" => true),
			array("table" => "poslease_types",
                  "key" => "poslease_lease_type"))),
	"_posleases" => array(
        "key" => "poslease_id",
        "references" => array(
            array("table" => "_posaddresses",
                  "key" => "_poslease_posaddress",
                  "cascade" => true),
			array("table" => "orders",
                  "key" => "poslease_order",
                  "cascade" => true),
			array("table" => "poslease_types",
                  "key" => "poslease_lease_type"))),
	"posopeninghrs" => array(
        "key" => "posopeninghr_id",
        "references" => array(
            array("table" => "posaddresses",
                  "key" => "posopeninghr_posaddress_id",
                  "cascade" => true),
			 array("table" => "weekdays",
                  "key" => "posopeninghr_weekday_id",
                  "cascade" => true),
			 array("table" => "openinghrs",
                  "key" => "posopeninghr_morning_from",
                  "cascade" => true),
			 array("table" => "openinghrs",
                  "key" => "posopeninghr_morning_to",
                  "cascade" => true),
			 array("table" => "openinghrs",
                  "key" => "posopeninghr_afternoon_from",
                  "cascade" => true),
			 array("table" => "openinghrs",
                  "key" => "posopeninghr_afternoon_to",
                  "cascade" => true),
			 array("table" => "openinghrs",
                  "key" => "posopeninghr_evening_from",
                  "cascade" => true),
			 array("table" => "openinghrs",
                  "key" => "posopeninghr_evening_to",
                  "cascade" => true),
			 array("table" => "openinghrs",
                  "key" => "posopeninghr_night_from",
                  "cascade" => true),
			array("table" => "openinghrs",
                  "key" => "posopeninghr_night_to",
                  "cascade" => true))),
	"posorders" => array(
        "key" => "posorder_id",
        "references" => array(
            array("table" => "orders",
                  "key" => "posorder_order",
                  "cascade" => true),
		    array("table" => "product_lines",
                  "key" => "posorder_product_line",
                  "cascade" => true),
		    array("table" => "productline_subclasses",
                  "key" => "posorder_product_line_subclass",
                  "cascade" => true),
			array("table" => "postypes",
                  "key" => "posorder_postype",
                  "cascade" => true),
			array("table" => "projectkinds",
                  "key" => "posorder_project_kind",
                  "cascade" => true),
			array("table" => "project_costtypes",
                  "key" => "posorder_legal_type",
                  "cascade" => true),
			array("table" => "floors",
                  "key" => "posorder_floor",
                  "cascade" => true),
            array("table" => "posaddresses",
                  "key" => "posorder_posaddress",
                  "cascade" => true))),
	"posorderspipeline" => array(
        "key" => "posorder_id",
        "references" => array(
            array("table" => "orders",
                  "key" => "posorder_order",
                  "cascade" => true),
		    array("table" => "product_lines",
                  "key" => "posorder_product_line",
                  "cascade" => true),
			 array("table" => "productline_subclasses",
                  "key" => "posorder_product_line_subclass",
                  "cascade" => true),
			array("table" => "postypes",
                  "key" => "posorder_postype",
                  "cascade" => true),
			array("table" => "projectkinds",
                  "key" => "posorder_project_kind",
                  "cascade" => true),
			array("table" => "project_costtypes",
                  "key" => "posorder_legal_type",
                  "cascade" => true),
			array("table" => "floors",
                  "key" => "posorder_floor",
                  "cascade" => true),
            array("table" => "posaddresses",
                  "key" => "posorder_posaddress",
                  "cascade" => true))),
	"posproject_types" => array(
        "key" => "posproject_type_id",
        "references" => array(
            array("table" => "postypes",
                  "key" => "posproject_type_postype",
                  "cascade" => true),
		    array("table" => "project_costtypes",
                  "key" => "posproject_type_projectcosttype",
                  "cascade" => true),
			array("table" => "projectkinds",
                  "key" => "posproject_type_projectkind",
                  "cascade" => true))),
	"posqueryentities" => array(
        "key" => "posqueryentity_id",
        "references" => array()),
	"posqueries" => array(
        "key" => "posquery_id",
		"cascade" => true,
        "references" => array(
		array("table" => "posqueryentities",
                  "key" => "posquery_leading_entity",
                  "cascade" => true)
		)),
	"posquery_permissions" => array(
        "key" => "posquery_permission_id",
        "references" => array(
            array("table" => "posqueries",
                  "key" => "posquery_permission_query",
                  "cascade" => true),
			array("table" => "users",
                  "key" => "posquery_permission_user"))),
	"possellouts" => array(
        "key" => "possellout_id",
        "references" => array(
		array("table" => "posaddresses",
                  "key" => "possellout_posaddress_id",
                  "cascade" => true)
		)),
	"possubclasses" => array(
        "key" => "possubclass_id",
        "references" => array()),
	"posfurnituretypes" => array(
        "key" => "posfurnituretype_id",
        "references" => array()),
	"postypes" => array(
        "key" => "postype_id",
        "references" => array()),
	 "postype_notifications" => array(
        "key" => "postype_notification_id",
        "references" => array(
            array("table" => "postypes",
                  "key" => "postype_notification_postype",
                  "cascade" => true),
			array("table" => "product_lines",
                  "key" => "postype_notification_prodcut_line"))),
	"postype_subclasses" => array(
        "key" => "postype_subclass_id",
        "references" => array(
            array("table" => "product_line_pos_types",
                  "key" => "postype_subclass_pl_pos_type",
                  "cascade" => true),
            array("table" => "possubclasses",
                  "key" => "postype_subclass_subclass",
                  "cascade" => true))),
	"posequipmenttypes" => array(
        "key" => "posequipmenttype_id",
        "references" => array()),
	"posequipments" => array(
        "key" => "posequipment_id",
        "references" => array(
            array("table" => "posequipmenttypes",
                  "key" => "posequipment_equipment",
                  "cascade" => true),
            array("table" => "posaddresses",
                  "key" => "posequipment_posaddress",
                  "cascade" => true))),
	"posareatypes" => array(
        "key" => "posareatype_id",
        "references" => array()),
	"posprofitability" => array(
        "key" => "posprofitability_id",
        "references" => array(
		array("table" => "posaddresses",
                  "key" => "posprofitability_posaddress_id",
                  "cascade" => false)
		)),
	"product_lines" => array(
        "key" => "product_line_id",
        "references" => array()),
    "product_line_files" => array(
        "key" => "product_line_file_id",
        "references" => array(
            array("table" => "items",
                  "key" => "product_line_file_product_line",
                  "cascade" => true),
            array("table" => "file_types",
                  "key" => "product_line_file_type"),
            array("table" => "file_purposes",
                  "key" => "product_line_file_purpose")),
        "files" => array(
            array("field" => "product_line_file_path"))),
    "productline_regions" => array(
        "key" => "productline_region_id",
        "references" => array(
            array("table" => "product_lines",
                  "key" => "productline_region_productline",
                  "cascade" => true),
            array("table" => "regions",
                  "key" => "productline_region_region"))),
    "productline_subclasses" => array(
        "key" => "productline_subclass_id",
        "references" => array(
            array("table" => "product_lines",
                  "key" => "productline_subclass_productline"))),
	"product_line_pos_types" => array(
        "key" => "product_line_pos_type_id",
        "references" => array(
            array("table" => "postypes",
                  "key" => "product_line_pos_type_pos_type",
                  "cascade" => true),
            array("table" => "product_lines",
                  "key" => "product_line_pos_type_product_line"))),
	"project_cost_groupnames" => array(
        "key" => "project_cost_groupname_id",
        "references" => array()),
    "project_costs" => array(
        "key" => "project_cost_id", 
        "references" => array(
            array("table" => "orders",
               "key" => "project_cost_order",
               "cascade" => true))),
    "project_costtypes" => array(
        "key" => "project_costtype_id",
        "references" => array()),
    "project_cost_remarks" => array(
        "key" => "project_cost_remark_id", 
        "references" => array(
            array("table" => "orders",
               "key" => "project_cost_remark_order",
               "cascade" => true))),
    "projectkinds" => array(
        "key" => "projectkind_id",
        "references" => array()),
	"project_states" => array(
        "key" => "project_state_id",
        "references" => array()),
	"projects" => array(
        "key" => "project_id",
        "references" => array(
			array("table" => "postypes",
                  "key" => "project_postype"),
		    array("table" => "possubclasses",
                  "key" => "project_pos_subclass"),
			array("table" => "projectkinds",
                  "key" => "project_projectkind"),
            array("table" => "orders",
                  "key" => "project_order",
                  "cascade" => true),
            array("table" => "users",
                  "key" => "project_retail_coordinator"),
            array("table" => "users",
                  "key" => "project_design_contractor"),
            array("table" => "users",
                  "key" => "project_design_supervisor"),
            array("table" => "users",
                  "key" => "project_requester"),
			array("table" => "users",
                  "key" => "project_cms_approver"),
            array("table" => "product_lines",
                  "key" => "project_product_line"),
			array("table" => "productline_subclasses",
                  "key" => "project_product_line_subclass"),
            array("table" => "business_units",
                  "key" => "project_business_unit"),
			 array("table" => "posaddresses",
                  "key" => "project_relocated_posaddress_id"),
			array("table" => "floors",
                  "key" => "project_floor",
                  "cascade" => true),
            array("table" => "location_types",
                  "key" => "project_location_type"))),
    "project_financial_justifications" => array(
        "key" => "project_financial_justification_id", 
        "references" => array(
            array("table" => "projects",
               "key" => "project_financial_justification_project",
               "cascade" => true))),
    "project_items" => array(
        "key" => "project_item_id",
        "references" => array(
            array("table" => "projects",
                  "key" => "project_item_project",
                  "cascade" => true),
            array("table" => "design_objective_items",
                  "key" => "project_item_item"))),
    "project_milestones" => array(
        "key" => "project_milestone_id",
        "references" => array(
            array("table" => "projects",
                  "key" => "project_milestone_project",
                  "cascade" => true),
            array("table" => "milestones",
                  "key" => "project_milestone_milestone"))),
	"projecttype_newproject_notifications" => array(
        "key" => "projecttype_newproject_notification_id",
        "references" => array(
            array("table" => "countries",
                  "key" => "projecttype_newproject_notification_country",
                  "cascade" => true),
			array("table" => "postypes",
                  "key" => "projecttype_newproject_notification_projecttype"))),
	"provinces" => array(
        "key" => "province_id",
       "references" => array(
            array("table" => "countries",
                  "key" => "province_country",
                  "cascade" => true))),
	"ps_product_lines" => array(
        "key" => "ps_product_line_id",
        "references" => array()),
    "ps_spent_fors" => array(
        "key" => "ps_spent_for_id",
        "references" => array()),
    "regions" => array(
        "key" => "region_id",
        "references" => array()),
    "roles" => array(
        "key" => "role_id",
        "references" => array()),
    "role_permissions" => array(
        "key" => "role_permission_id",
        "references" => array(
            array("table" => "roles",
                  "key" => "role_permission_role",
                  "cascade" => true),
            array("table" => "permissions",
                  "key" => "role_permission_permission",
                  "cascade" => true))),
    "role_file_categories" => array(
        "key" => "role_file_category_id",
        "references" => array(
            array("table" => "roles",
                  "key" => "role_file_category_role_id",
                  "cascade" => true),
            array("table" => "order_file_categories",
                  "key" => "role_file_category_file_category_id",
                  "cascade" => true))),
    "salesregions" => array(
        "key" => "salesregion_id",
        "references" => array()),
	"sec_excluded_ips" => array(
        "key" => "sec_excluded_ip_id",
        "references" => array()),
	"sec_loginfailures" => array(
        "key" => "sec_loginfailure_id",
        "references" => array()),
	"sec_lockedips" => array(
        "key" => "sec_lockedip_id",
        "references" => array()),
	"sg_brands" => array(
        "key" => "sg_brand_id",
        "references" => array()),
	"standard_delivery_addresses" => array(
        "key" => "delivery_address_id",
        "references" => array(
            array("table" => "addresses",
                  "key" => "delivery_address_address_id")
            )),
	"standarddesigncontractors" => array(
        "key" => "standarddesigncontractor_id",
        "references" => array(
			array("table" => "product_lines",
                  "key" => "standarddesigncontractor_productline"),
			array("table" => "postypes",
                  "key" => "standarddesigncontractor_postype"),
            array("table" => "users",
                  "key" => "standarddesigncontractor_dcon_user"))),
	"standardroles" => array(
        "key" => "standardrole_id",
        "references" => array(
            array("table" => "countries",
                  "key" => "standardrole_country"),
			array("table" => "postypes",
                  "key" => "standardrole_postype"),
			array("table" => "users",
                  "key" => "standardrole_rtco_user"),
			array("table" => "users",
                  "key" => "standardrole_dsup_user"),
			array("table" => "users",
                  "key" => "standardrole_rtop_user"),
            array("table" => "users",
                  "key" => "standardrole_cms_approver_user"))),
	"statistics" => array(
        "key" => "statistic_id",
        "references" => array(
			array("table" => "users",
				  "key" => "statistic_user"))),
    "store_global_orders" => array(
        "key" => "store_global_order_id",
        "references" => array(
            array("table" => "stores",
                  "key" => "store_global_order_store"))),
    "stores" => array(
        "key" => "store_id",
        "references" => array(
            array("table" => "items",
                  "key" => "store_item",
                  "cascade" => true),
            array("table" => "addresses",
                  "key" => "store_address"))),
    "strings" => array(
        "key" => "string_id",
        "references" => array()),
	"supplier_client_invoices" => array(
        "key" => "supplier_client_invoice_id",
        "references" => array(
            array("table" => "addresses",
                  "key" => "supplier_client_invoice_supplier"),
            array("table" => "addresses",
                  "key" => "supplier_client_invoice_client"))),
    "suppliers" => array(
        "key" => "supplier_id",
        "references" => array(
            array("table" => "addresses",
                  "key" => "supplier_address"),
            array("table" => "items",
                  "key" => "supplier_item"))),
    "supervisingteam" => array(
        "key" => "supervisingteam_id",
        "references" => array(
            array("table" => "users",
                  "key" => "supervisingteam_user"))),
	"taskworks" => array(
        "key" => "taskwork_id",
		"references" => array(
			array("table" => "product_lines",
                  "key" => "taskwork_productline",
                  "cascade" => true),
            array("table" => "postypes",
                  "key" => "taskwork_postype"),
			array("table" => "items",
                  "key" => "taskwork_item"),
            array("table" => "adresses",
                  "key" => "taskwork_address"))),
	"tasks" => array(
        "key" => "task_id",
        "references" => array(
            array("table" => "orders",
                  "key" => "task_order",
                  "cascade" => true),
            array("table" => "users",
                  "key" => "task_user"), 
            array("table" => "users",
                  "key" => "task_from_user"))),
	"transportation_types" => array(
        "key" => "transportation_type_id",
        "references" => array()),
    "users" => array(
        "key" => "user_id",
        "references" => array(
            array("table" => "addresses",
                  "key" => "user_address",
                  "cascade" => true),
			array("table" => "users",
                  "key" => "user_set_to_inactive_by",
                  "cascade" => true),
			array("table" => "users",
                  "key" => "user_added_by",
                  "cascade" => true),
            array("table" => "users",
                  "key" => "user_substitute"))),
    "user_roles" => array(
        "key" => "user_role_id",
        "references" => array(
            array("table" => "users",
                  "key" => "user_role_user",
                  "cascade" => true),
            array("table" => "roles",
                  "key" => "user_role_role",
                  "cascade" => true))),
    "voltages" => array(
        "key" => "voltage_id",
        "references" => array()),
	"weekdays" => array(
        "key" => "weekday_id",
        "references" => array())
);


/********************************************************************
    dberror
*********************************************************************/

function dberror($sql)
{
    error(mysql_error() . "<br><br>" . $sql);
    exit;
}

/********************************************************************
    dbquote
*********************************************************************/

function dbquote($str, $withnull = false)
{
    if (trim($str) == "" && $withnull)
    {
        return "null";
    }
    else
    {
        $str = str_replace("\"", "\"\"", $str);
        return "\"" . $str. "\"";
    }
}

/********************************************************************
    is_table_name
*********************************************************************/

function is_table_name($table)
{
    global $__db_structure;

    return isset($__db_structure[$table]);
}

/********************************************************************
    table_key
*********************************************************************/

function table_key($table)
{
    global $__db_structure;

    if (!isset($__db_structure[$table]))
    {
        error("Invalid table \"$table\" (table_key).");
    }

    return $__db_structure[$table]["key"];
}

/********************************************************************
    table_reference
*********************************************************************/

function table_reference($table, $reftable, $index = 0)
{
    global $__db_structure;

    if (!isset($__db_structure[$table]))
    {
        error("Invalid table \"$table\" (table_reference).");
    }

    foreach ($__db_structure[$table]["references"] as $reference)
    {
        if ($reference["table"] == $reftable)
        {
            if ($index-- == 0)
            {
                return $reference["key"];
            }
        }
    }

    return null;
}

/********************************************************************
    can_delete_record
*********************************************************************/

function can_delete_record($table, $id)
{
    global $__db_structure;

    if (!isset($__db_structure[$table]))
    {
        error("Invalid table \"$table\" (can_delete_record).");
    }

    foreach (array_keys($__db_structure) as $dbtable)
    {
        foreach ($__db_structure[$dbtable]["references"] as $ref)
        {
            if ($ref["table"] == $table)
            {
                $key = $__db_structure[$dbtable]["key"];
                $sql = "select $key from $dbtable where " . $ref["key"] . " = $id";
                $res = mysql_query($sql) or dberror($sql);

                if (mysql_num_rows($res) > 0)
                {
                    if (isset($ref["cascade"]))
                    {
                        while ($row = mysql_fetch_row($res))
                        {
                            if (!can_delete_record($dbtable, $row[0]))
                            {
								return false;
                            }
                        }
                    }
                    else
                    {
                        // shows dependencies in case of deletion not beeing possible
                       
						/*
						print_r($ref);

						echo "<br />";
                        
						while ($row = mysql_fetch_row($res))
                        {
							echo $dbtable . "->" . $key;
							echo "<br />";
						}
						*/
						

                        return false;
                    }
                }
            }
        }
    }

    return true;
}

/********************************************************************
    delete_record
*********************************************************************/

function delete_record($table, $id, $check = true)
{
    global $__db_structure;

    if (can_delete_record($table, $id))
    {
        foreach (array_keys($__db_structure) as $dbtable)
        {
            foreach ($__db_structure[$dbtable]["references"] as $ref)
            {
                if ($ref["table"] == $table)
                {
                    $key = $__db_structure[$dbtable]["key"];
                    $sql = "select $key from $dbtable where " . $ref["key"] . " = $id";
                    $res = mysql_query($sql) or dberror($sql);

                    if (mysql_num_rows($res) > 0)
                    {
                        if (isset($ref["cascade"]))
                        {
                            while ($row = mysql_fetch_row($res))
                            {
								delete_record($dbtable, $row[0], false);
                            }
                        }
                    }
                }
            }
        }

        if (isset($__db_structure[$table]["files"]))
        {
            $sql = "select * from $table where " .  $__db_structure[$table]["key"] . " = $id";
            $res = mysql_query($sql) or dberror($sql);
            $row = mysql_fetch_assoc($res);

            foreach ($__db_structure[$table]["files"] as $file)
            {
                if ($row[$file["field"]])
                {
                    $path = combine_paths($_SERVER["DOCUMENT_ROOT"], $row[$file["field"]]);

                    if (file_exists($path))
                    {
                        unlink($path);
                    }
                }
            }
        }

        $sql = "delete from $table where " . $__db_structure[$table]["key"] . " = $id";

        mysql_query($sql) or dberror($sql);

        return true;
    }
    else
    {
        return false;
    }
}

/********************************************************************
    is_unique_value
*********************************************************************/

function is_unique_value($table, $field, $value, $id = 0)
{
    global $__db_structure;

    if (!isset($__db_structure[$table]))
    {
        error("Invalid table \"$table\" (is_unique_value).");
    }

    $sql = "select count(*) from $table where $field = " . dbquote($value);

    if ($id)
    {
        $sql .= " and " . $__db_structure[$table]["key"] . " <> $id";
    }

    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_row($res);

    return $row[0] == 0;
}

/********************************************************************
    is_unique_value_multiple
*********************************************************************/

function is_unique_value_multiple($table, $fields, $values, $id = 0)
{
    global $__db_structure;

    if (!isset($__db_structure[$table]))
    {
        error("Invalid table \"$table\" (is_unique_value).");
    }

    if (!is_array($fields))
    {
        error("Parameter \"fields\" is supposed to be an array.");
    }

    if (!is_array($values))
    {
        error("Parameter \"values\" is supposed to be an array.");
    }

    if (count($fields) == 0 || count($values) == 0 || count($fields) != count($values))
    {
        error("The array parameters \"fields\" and \"values\" must contain an equal number of elements and at least one.");
    }

    $sql = "select count(*) from $table where ";
    $parts = array();

    for ($i = 0; $i < count($fields); $i++)
    {
        $parts[] = $fields[$i] . " = " . dbquote($values[$i]);
    }

    $sql .= join(" and ", $parts);

    if ($id)
    {
        $sql .= " and " . $__db_structure[$table]["key"] . " <> $id";
    }

    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_row($res);

    return $row[0] == 0;
}

/********************************************************************
    get_system_currency
*********************************************************************/

function get_system_currency()
{
    $sql = "select currency_id " .
           "from currencies " .
           "where currency_system = 1";

    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_row($res);

    return $row[0];
}

/********************************************************************
    get_system_currency_symbol
*********************************************************************/

function get_system_currency_symbol()
{
    $sql = "select currency_symbol " .
           "from currencies " .
           "where currency_system = 1";

    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_row($res);

    return $row[0];
}


/********************************************************************
    get_number_of records
*********************************************************************/

function get_number_of_records($table_name, $field_name)
{
    $sql = "select count(\"" . $field_name . "\") as num_of_recs " .
           "from " . $table_name;
    
    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_row($res);

    return $row[0];
}


/********************************************************************
    trace login failure
*********************************************************************/

function trace_login_failure($ip, $source, $user, $password)
{
    $sql = "insert into sec_loginfailures " .
		   "(sec_loginfailure_ip, sec_loginfailure_source, sec_loginfailure_date, sec_loginfailure_user, sec_loginfailure_password) " . 
		   "values (" . 
		   dbquote($ip) . ", " .
		   dbquote($source) . ", " .
           dbquote(date("Y-m-d:H-i-s")) . ", " .
		   dbquote(str_replace('"', "", $user)) . ", " .
		   dbquote(str_replace('"', "", $password)) . ")";
    
    $result = mysql_query($sql) or dberror($sql);

	$sql = "insert into sec_lockedips " .
		   "(sec_lockedip_ip, sec_lockedip_date) " . 
		   "values (" . 
		   dbquote($ip) . ", " .
           dbquote(date("Y-m-d:H-i-s")) . ")";
    
    $result = mysql_query($sql) or dberror($sql);

	return true;
}

?>