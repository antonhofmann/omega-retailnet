<?php
/********************************************************************

    page_modal.php

    Classes and functions to render pages.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-13
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2007-12-07
    Version:        1.1.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

/********************************************************************
    Page class
*********************************************************************/

class Page_Modal
{
    var $section;
    var $area;
    var $navigation;
    var $title;
    var $actions;
	var $tabs;

    function Page($name = PAGE_LAST, $title = "")
    {
        global $_SERVER;
        
        $this->title = $title;
        $this->actions = array();
        $this->section = "";

    }

	function register_action($name, $caption, $url = "", $target = "")
    {
		return;
	}

	   

    function header()
    {
        global $_SERVER;
        global $__needs_form;
        global $__needs_multipart_form;

        // HTML header with title

        if ($this->title)
        {
            $title = $this->title;
        }
        else if ($this->section)
        {
            $title = $this->section["title"];
        }
        else
        {
            $title = "";
        }

        $title = htmlspecialchars(smart_join(": ", array(BRAND . " Retail Net", $title)));


		echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
		echo "<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\">";
        echo "<head>";
        echo "<title>$title</title>";
		echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />";
		echo "<meta http-equiv=\"imagetoolbar\" content=\"no\" />";

        echo "<link href=\"../include/retailnet.css?v=24\" rel=\"stylesheet\" type=\"text/css\" />";
		echo "<link href=\"../include/modal.css\" rel=\"stylesheet\" type=\"text/css\" />";
        

		echo "<link href=\"../js/jquery.bettertip.css\" rel=\"stylesheet\" type=\"text/css\" />";
        echo "<script language=\"javascript\" src=\"../js/jquery-1.4.2.min.js\" type=\"text/javascript\"></script>";
		
		echo "<script type=\"text/javascript\" src=\"../js/nyroModal/js/jquery.nyroModal-1.2.8.pack.js\"></script>";
		echo "<link rel=\"stylesheet\" href=\"../js/nyroModal/styles/nyroModal.css\" type=\"text/css\" media=\"screen\" />";

		echo "<script language=\"javascript\" src=\"../js/jquery.bettertip.js\" type=\"text/javascript\"></script>";
		echo "<script language=\"javascript\" src=\"../include/main.js?v=24\" type=\"text/javascript\"></script>";

        

        echo "</head>";
		

        // Body tag (popup pages end here)

        if ($__needs_form)
        {
            $enctype = $__needs_multipart_form ? "enctype=\"multipart/form-data\" " : "";
            $form = "<form class=\"nyroModal\" ${enctype}name=\"main\" method=\"post\" action=\"" . $_SERVER["PHP_SELF"] . "\">";
        }
        else
        {
            $form = "";
        }

        if (!$this->section)
        {
            echo "<body id=\"maindoc\" bgcolor=\"#FFFFFF\" style=\"margin-left:20px;margin-top:20px;\">";
            echo $form;
            return;
        }


        echo "<body id=\"maindoc\" bgcolor=\"#FFFFFF\" style=\"margin-left:0px;margin-top:0px;\">";
        echo $form;

        // Dermine master section

        echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
        echo "<tr valign=\"top\">";
        echo "<td width=\"160\">";

        // Content area

        echo "</td>";
        echo "<td width=\"20\">";
        echo "<img src=\"../pictures/spacer.gif\" width=\"20\" height=\"1\" alt=\"\" />";
        echo "</td>";

        echo "<td>";
        echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
        echo "<tr>";
        echo "<td height=\"20\"></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>";
    }

    function footer()
    {
        global $__needs_form;

        if ($this->section)
        {
            echo "</td>";
            echo "</tr>";
        }

        if ($__needs_form)
        {
			echo "</form>";
        }

        echo "</body>";
        echo "</html>";

        global $start_time;

        $end_time = time();
        $diff = $end_time - $start_time;

		if(array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
			$ips = $_SERVER['HTTP_X_FORWARDED_FOR'];
			if(is_array ( $ips )) {
				$i = count($ips);
				$ip = $ips[$i-1];
			}
			else
			{
				$ip = $ips;
			}
		}
		else
		{
			$ip = $_SERVER['REMOTE_ADDR'];
		}

        $sql = "Insert into statistics (" .
               "statistic_user, statistic_ip, statistic_date, " .
               "statistic_url, statistic_duration) " .
               "values ('" .
               user_id() . "', '" .
               $ip . "', '" .
               date("Y-m-d-H-i-s") . "', '" .
               $_SERVER["SCRIPT_FILENAME"] . "', " .
               $diff . ")";

          $res = mysql_query($sql);

    }

    function title($text)
    {
        echo "<p class=\"title_modal\">";
        echo htmlspecialchars($text);
        echo "</p>";
    }
	
}

?>