<?php
/********************************************************************

    page_module_navigation.php

    Renders Modul navigation at the top right of the page

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-11-29
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-8--26
    Version:        1.0.0

    Copyright (c) 2000, Swatch AG, All Rights Reserved.

*********************************************************************/

if(isset($_SESSION["user_login"]))
{
	echo "<div style=\"float:right;padding-top:20px;\">";
	
	echo "|&nbsp;";
	if(has_access("can_view_projects"))
	{
		echo "<a href=\"/user/projects.php\" target=\"_blank\">Projects</a>&nbsp;|&nbsp;";
	}
	if(has_access("has_access_to_archive"))
	{
		echo "<a href=\"/archive/projects_archive.php\" target=\"_blank\">Project Archive</a>&nbsp;|&nbsp;";
	}
	if(has_access("can_use_posindex"))
	{
		echo "<a href=\"/pos/index.php\" target=\"_blank\">POS Index</a>&nbsp;|&nbsp;";
	}
	if(has_access("can_perform_queries"))
	{
		echo "<a href=\"/mis/index.php\" target=\"_blank\">MIS</a>&nbsp;|&nbsp;";
	}
	if(has_access("has_access_to_cer"))
	{
		echo "<a href=\"/cer/index.php\" target=\"_blank\">CER/AF</a>&nbsp;|&nbsp;";
	}
	
	
	/*
	if(has_access("has_access_to_mps"))
	{
		echo "<a href=\"/mps\" target=\"_blank\">MPS</a>&nbsp;|&nbsp;";
	}
	
	
	if(has_access("can_view_all_red_projects") or has_access("can_view_only_his_red_projects"))
	{
		echo "<a href=\"/red\" target=\"_blank\">RED</a>&nbsp;|&nbsp;";
	}
	*/
	

	if(has_access("can_edit_catalog"))
	{
		echo "<a href=\"/admin/index.php\" target=\"_blank\">Admin</a>&nbsp;|&nbsp;";
	}
	elseif(has_access("can_browse_catalog_in_admin"))
	{
		echo "<a href=\"/admin/index.php\" target=\"_blank\">Browse Catalog</a>&nbsp;|&nbsp;";
	}
	echo "<a href=\"/user/my_company.php\" target=\"_blank\">My Company</a>&nbsp;|&nbsp;";

	echo "</div>";
}

?>