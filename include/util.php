<?php
/********************************************************************

    util.php

    Various utility functions.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-28
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-09-05
    Version:        1.4.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

/********************************************************************
    Global variables
*********************************************************************/

$__params = array();

/********************************************************************
    error
*********************************************************************/

function error($msg)
{
    
	$uri = $_SERVER['PHP_SELF'];
	$first = true;
	foreach($_GET as $key=>$value)
	{
		if($first == true)
		{
			$uri .= '?' . $key . '=' . $value;
		}
		else
		{
			$uri .= '&' . $key . '=' . $value;
		}
	}
	
	$sql = "insert into error_tracking " . 
		   "(error_tracking_user, error_tracking_date, error_tracking_error, error_tracking_uri) VALUES (" . 
		   user_id() . ", " .
		   dbquote(date("Y-m-d H:i:s")) . ", " .
		   dbquote($msg) . ", " .
		   dbquote($uri). ")";


	$res = mysql_query($sql);
	
	echo "<p>";
    echo "<b>An error occcured. Please contact your webmaster.</b>";
    echo "</p>";
    exit;
}

/********************************************************************
    redirect
*********************************************************************/

function redirect($url)
{
    header("Location: $url");
    exit;
}

/********************************************************************
    version
*********************************************************************/

function version($version)
{
    $phpversion = preg_split("/\./", PHP_VERSION);
    $version = preg_split("/\./", $version);

    return $phpversion[0] >= $version[0] && $phpversion[1] >= $version[1] && $phpversion[2] >= $version[2];
}

/********************************************************************
    set_session_value
*********************************************************************/

function set_session_value($name, $value)
{
    if (isset($_SESSION))
    {
        $_SESSION[$name] = $value;
    }
    else
    {
        global $HTTP_SESSION_VARS;

        $GLOBALS[$name] = $value;
        session_register($name);
        $HTTP_SESSION_VARS[$name] = &$GLOBALS[$name];
    }
}

/********************************************************************
    get_session_value
*********************************************************************/

function get_session_value($name)
{
    if (isset($_SESSION))
    {
        if (isset($_SESSION[$name]))
        {
            return $_SESSION[$name];
        }
    }
    else
    {
        global $HTTP_SESSION_VARS;

        if (isset($HTTP_SESSION_VARS[$name]))
        {
            return $HTTP_SESSION_VARS[$name];
        }
    }

    return null;
}

/********************************************************************
    id
*********************************************************************/

function id()
{
    global $_REQUEST;

    return isset($_REQUEST["id"]) ? $_REQUEST["id"] : 0;
}

/********************************************************************
    param
*********************************************************************/

function param($name, $value = null)
{
    global $_REQUEST;

    if (!is_null($value))
    {
        $_REQUEST[$name] = $value;
    }

    return isset($_REQUEST[$name]) ? $_REQUEST[$name] : null;
}

/********************************************************************
    user_id
*********************************************************************/

function user_id()
{
    return get_session_value("user_id");
}

/********************************************************************
    user_login
*********************************************************************/

function user_login()
{
    return get_session_value("user_login");
}

/********************************************************************
    build_access
*********************************************************************/

function build_access()
{
    if (user_id())
    {
        // Determine all available permissions and combine them to a
        // string (permission names separated with space character)

        $sql = "select permission_name " .
               "from permissions";

        $res = mysql_query($sql) or dberror($sql);
        $permissions = array();

        while ($row = mysql_fetch_assoc($res))
        {
            $permissions[] = $row["permission_name"];
        }

        set_session_value("permissions", join(" ", $permissions));

        // Do the same thing with the permissions assigned to the currently
        // logged in user

        $sql = "select distinct permission_name " .
               "from user_roles left join roles on user_role_role = role_id " .
               "    left join role_permissions on role_id = role_permission_role " .
               "    left join permissions on role_permission_permission = permission_id " .
               "where user_role_user = " . user_id();

        $res = mysql_query($sql) or dberror($sql);
        $permissions = array();

        while ($row = mysql_fetch_assoc($res))
        {
            $permissions[] = $row["permission_name"];
        }

        set_session_value("user_permissions", join(" ", $permissions));
    }
}

/********************************************************************
    check_access
*********************************************************************/

function check_access($permission = "")
{
    global $_SERVER;

    $url = $_SERVER["PHP_SELF"];

    if (isset($_SERVER["QUERY_STRING"]))
    {
        $url .= "?" . $_SERVER["QUERY_STRING"];
    }

    if (!user_id())
    {
        set_session_value("login_page", $url);
        redirect("login.php");
    }

    if ($permission)
    {
        if (!preg_match("/\b$permission\b/", get_session_value("permissions")))
        {
            error("Invalid permission \"$permission\" (check_access).");
        }

        if (!preg_match("/\b$permission\b/", get_session_value("user_permissions")))
        {
            redirect("noaccess.php");
        }
    }
}

/********************************************************************
    has_access
*********************************************************************/

function has_access($permission = "")
{
    if (!user_id())
    {
        return false;
    }

    if ($permission)
    {
        if (!preg_match("/\b$permission\b/", get_session_value("permissions")))
        {
            error("Invalid permission \"$permission\" (check_access).");
        }

        if (!preg_match("/\b$permission\b/", get_session_value("user_permissions")))
        {
			return false;
        }
    }

    return true;
}

/********************************************************************
    get_string
*********************************************************************/

function get_string($name)
{
    $sql = "select string_text " .
           "from strings " .
           "where string_name = " . dbquote($name);

    $res = mysql_query($sql) or dberror($sql);

    if (mysql_num_rows($res) == 0)
    {
        error("Invalid string name \"$name\" (get_string).");
    }

    $row = mysql_fetch_assoc($res);
    
    return $row["string_text"];
}

/********************************************************************
    build_url
*********************************************************************/

function build_url($page, $params)
{
    if (!is_array($params))
    {
        error("Second argument must be an array (build_url).");
    }

    if (strpos($page, "?") === false)
    {
        $page .= "?";
    }
    else
    {
        $page .= "&";
    }

    $list = array();

    foreach (array_keys($params) as $key)
    {
        //$list[] = urlencode($key) . "=" . urlencode($params[$key]);
		$list[] = $key . "=" . $params[$key];
    }

    $page .= join("&", $list);

    return $page;
}

/********************************************************************
    build_page_url
*********************************************************************/

function build_page_url($new = null)
{
    global $_REQUEST;
    global $_SERVER;
    global $__params;

    $params = array();

    foreach ($__params as $param)
    {
        if (isset($_REQUEST[$param]))
        {
            //$params[$param] = "$param=" . urlencode($_REQUEST[$param]);
			$params[$param] = "$param=" . $_REQUEST[$param];
        }
    }

    if (is_array($new))
    {
        foreach (array_keys($new) as $key)
        {
            //$params[$key] = "$key=" . urlencode($new[$key]);
			$params[$key] = "$key=" . $new[$key];
        }
    }

    $url = $_SERVER["PHP_SELF"];

    if (count($params))
    {
        $url .= "?" . join("&", $params);
    }

    return $url;
}

/********************************************************************
    remove_param
*********************************************************************/

function remove_param($url, $param)
{
    $parts = explode("\?", $url);

    if (count($parts) == 1)
    {
        return $parts[0];
    }

    $params = explode("&", $parts[1]);

    foreach (array_keys($params) as $key)
    {
        if (preg_match("/$param=/", $params[$key]))
        {
            unset($params[$key]);
            break;
        }
    }

    $url = $parts[0];

    if (count($params))
    {
        $url .= "?" . join("&", $params);
    }

    return $url;
}

/********************************************************************
    register_param
*********************************************************************/

function register_param($param)
{
    global $__params;

    $__params[] = $param;
}

/********************************************************************
    set_referer
*********************************************************************/

function set_referer($view)
{
    $referers = get_session_value("referers");

    if (is_null($referers))
    {
        $referers = array();
    }

    $referers[$view] = build_page_url();

    set_session_value("referers", $referers);
}

/********************************************************************
    get_referer
*********************************************************************/

function get_referer()
{
    global $_SERVER;

    $view = preg_replace("/^\/(.*\/)*/", "", $_SERVER["PHP_SELF"]);
    $referers = get_session_value("referers");

    if (!is_null($referers) && isset($referers[$view]))
    {
        return $referers[$view];
    }
    else
    {
        return "welcome.php";
    }
}

/********************************************************************
    strtodate
*********************************************************************/

function strtodate($str)
{
    $parts = preg_split("/[.-]+/", $str);

    if (count($parts) != 3)
    {
        return false;
    }

    list($day, $month, $year) = $parts;

    if ($day > 31 && $year <= 31)
    {
        $temp = $day;
        $day = $year;
        $year = $temp;
    }

    if ($year == 0)
    {
        $year = 2000;
    }

    if (!checkdate((int)$month, (int)$day, (int)$year))
    {
        return false;
    }
	$day = substr($day,0,2);

	//echo "->" . $month . " " . $day . " " . $year;
    return mktime(0, 0, 0, $month, $day, $year);
}

/********************************************************************
    get_file_ext
*********************************************************************/

function get_file_ext($file)
{
    return substr($file, strrpos($file, ".") + 1);
}

/********************************************************************
    combine_paths
*********************************************************************/

function combine_paths($path1, $path2)
{
    $path1 = preg_replace("/[\\/]+$/", "", $path1);
    $path2 = preg_replace("/^[\\/]+/", "", $path2);

    if ($path1 && $path2)
    {
        return join("/", array($path1, $path2));
    }
    else if ($path1)
    {
        return $path1;
    }
    else if ($path2)
    {
        return $path2;
    }
}

/********************************************************************
    create_directory
*********************************************************************/

function create_directory($dir)
{
    umask(0);

    if (@is_dir($dir))
    {
        return true;
    }
    else if (@mkdir($dir, 06777))
    {
        return true;
    }
    else
    {
        $parts = preg_split("/[\\\\\\/]/", $dir);

        if (count($parts) == 1)
        {
            return false;
        }
        else
        {
            array_pop($parts);
            $root = join("/", $parts);

            if (create_directory($root))
            {
                if (@mkdir($dir, 06777))
                {
                    return true;
                }
            }
        }
    }

    return false;
}

/********************************************************************
    make_unique_filename
*********************************************************************/

function make_unique_filename($file, $dir = "")
{
    $index_pos = strrpos($file, '.');

    if ($index_pos === false)
    {
        $index_pos = strlen($file);
    }

    $path = $file;
    $index = 0;

    while (file_exists(combine_paths($dir, $path)))
    {
        $index++;
        $path = substr_replace($file, "_" . $index, $index_pos, 0);
    }

    return $path;
}

/********************************************************************
    make_valid_filename
*********************************************************************/

function make_valid_filename($file)
{
    $map = array("/\s/" => "_",
                 "/�/" => "ae",
                 "/�/" => "a",
                 "/�/" => "a",
                 "/�/" => "a",
                 "/�/" => "a",
                 "/�/" => "c",
                 "/�/" => "e",
                 "/�/" => "e",
                 "/�/" => "e",
                 "/�/" => "e",
                 "/�/" => "i",
                 "/�/" => "i",
                 "/�/" => "i",
                 "/�/" => "i",
                 "/�/" => "n",
                 "/�/" => "oe",
                 "/�/" => "o",
                 "/�/" => "o",
                 "/�/" => "o",
                 "/�/" => "o",
                 "/�/" => "o",
                 "/�/" => "ue",
                 "/�/" => "u",
                 "/�/" => "u",
                 "/�/" => "u",
                 "/�/" => "y",
                 "/�/" => "y",
                 "/\\//" => "_",
                 "/\\\\/" => "_",
                 "/\*/" => "_",
                 "/\?/" => "_",
		         "/\&/" => "_");

    $file = strtolower($file);
    $file = preg_replace(array_keys($map), array_values($map), $file);
    $file = preg_replace("/[\x80-\xff]/", ".", $file);

    return $file;
}

/********************************************************************
    is_shortcut
*********************************************************************/

function is_shortcut($str, $length = 20)
{
    if (strlen($str) > $length)
    {
        return false;
    }
    else if (!preg_match("/^[a-z0-9_]+$/", $str))
    {
        return false;
    }

    return true;
}

/********************************************************************
    make_file_link
*********************************************************************/

function make_file_link($path)
{
    global $_SERVER;

    $viewable = array("gif", "jpeg", "jpg", "png", "bmp");

    $link = $path;
	$link = "javascript:popup('$path', 580, 380)";

	
	if(strpos($path, "orders") > 0)
	{
		//$link = "https://" . $_SERVER["HTTP_HOST"] . "/include/openfile.php?path=$path";
		$link = "http://" . $_SERVER["HTTP_HOST"] . "/include/openfile.php?path=$path";
	}

    $ext = get_file_ext($path);

    if ($ext)
    {
        $key = array_search($ext, $viewable);

        if ($key !== null && $key !== false)
        {
            $size = 0;
            if(array_search($ext, $viewable))
            {
                $size = getimagesize(combine_paths($_SERVER["DOCUMENT_ROOT"], $path));
            }

			//$patha = "https://" . $_SERVER["HTTP_HOST"] . $path;
			$patha = "http://" . $_SERVER["HTTP_HOST"] . $path;

            if ($size)
            {
                $width = $size[0];
                $height = $size[1];
                $link = "javascript:picture('$patha', $width, $height)";
            }
            else
            {
                 $link = "javascript:popup('$patha', 580, 380)";

            }
        }
    }

    return "<a href=\"$link\">" . htmlspecialchars(basename($path)) . "</a>";
}

/********************************************************************
    is_email_address
*********************************************************************/

function is_email_address($email, $strict = false)
{
    if (!$strict && trim($email) == "")
    {
        return true;
    }

    return preg_match("/^[a-zA-Z0-9\._-]+\@[a-zA-Z0-9\._-]+(\.[a-zA-Z0-9_-]+)+$/", $email);
}

/********************************************************************
    is_web_address
*********************************************************************/

function is_web_address($web, $strict = false)
{
    if (!$strict && trim($web) == "")
    {
        return true;
    }

    return preg_match("/^https?:\/\/[a-zA-Z0-9\/\._-]+(\??[a-zA-Z0-9\/\&\._=\+-]+)$/", $web);
}

/********************************************************************
    is_int_value
*********************************************************************/

function is_int_value($value, $length, $strict = false)
{
    if (!$strict && trim($value) == "")
    {
        return true;
    }

    $length--;
    return preg_match("/^-?\d{1,$length}$/", $value);
}

/********************************************************************
    is_real_value
*********************************************************************/

function is_real_value($value, $strict = false)
{
    if (!$strict && trim($value) == "")
    {
        return true;
    }

    //return preg_match("/^\d*(\.\d*)$/", $value);
	return preg_match("/^-{0,1}\d*\.{0,1}\d+$/", $value);
}

/********************************************************************
    is_decimal_value
*********************************************************************/

function is_decimal_value($value, $length, $fraction, $strict = false)
{
    if (!$strict && trim($value) == "")
    {
        return true;
    }

    $digits_pos = $length - $fraction;
    $digits_neg = $digits_pos - 1;

    return preg_match("/^((\d){0,$digits_pos}|-(\d){0,$digits_neg})(\.(\d){0,$fraction})?$/", $value);
}

/********************************************************************
    is_date_value
*********************************************************************/

function is_date_value($value, $strict = false)
{
    if (!$strict && trim($value) == "")
    {
        return true;
    }

    return @strtodate($value) > 0;
}

/********************************************************************
    is_time_value
*********************************************************************/

function is_time_value($value, $strict = false)
{
    if (!$strict && trim($value) == "")
    {
        return true;
    }

    if (preg_match("/^(\d\d):(\d\d)(?::(\d\d))?$/", $value, $matches))
    {
        return $matches[1] < 24 && $matches[2] < 60 && (count($matches) == 3 || $matches[3] < 60);
    }

    return false;
}

/********************************************************************
    is_datetime_value
*********************************************************************/

function is_datetime_value($value, $strict = false)
{
    if (!$strict && trim($value) == "")
    {
        return true;
    }

    $parts = preg_split("/\s+/", $value);

    return is_date_value($parts[0]) && (count($parts) == 1 || is_time_value($parts[1]));
}

/********************************************************************
    is_upper_case
*********************************************************************/

function is_upper_case($str, $strict = false)
{
    if (!$strict && trim($str) == "")
    {
        return true;
    }

    return $str == strtoupper($str);
}

/********************************************************************
    is_lower_case
*********************************************************************/

function is_loser_case($str, $strict = false)
{
    if (!$strict && trim($str) == "")
    {
        return true;
    }

    return $str == strtolower($str);
}


/********************************************************************
    is_valid_password
*********************************************************************/

function is_valid_password($password, $username)
{
    
	
	if(strpos($password, $username) or $password == $username)
	{
		return false;
	}
	
	$charset = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9");

	$characters = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");

	$numbers = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");


	$has_number = false;
	$has_character = false;

	for($i=0;$i<strlen($password);$i++)
	{
		if(!in_array(substr($password, $i, 1), $charset))
		{
			return false;
		}

		if(in_array(substr($password, $i, 1), $numbers))
		{
			$has_number = true;
		}
		elseif(in_array(substr($password, $i, 1), $characters))
		{
			$has_character = true;
		}
	}


	if($has_number == false or $has_character == false)
	{
		return false;
	}

	return true;
}


/********************************************************************
    password already used
*********************************************************************/
function password_already_used($password, $email)
{
	$sql = "select user_used_passwords from users " . 
		   "where user_email = " . dbquote($email);
	
	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$oldpasswords = unserialize($row["user_used_passwords"]);

		if(is_array($oldpasswords) and in_array($password, $oldpasswords))
		{
			return true;
		}
	}
	
	return false;
}

/********************************************************************
    smart_join
*********************************************************************/

function smart_join($separator, $list)
{
    $str = "";

    foreach($list as $elem)
    {
        if ($elem)
        {
            if ($str)
            {
                $str .= $separator;
            }

            $str .= $elem;
        }
    }

    return $str;
}

/********************************************************************
    smart
*********************************************************************/

function smart($str)
{
    $str = preg_replace("/&brvbar;/", "|", $str);
    $str = htmlspecialchars($str);

    $str = preg_replace("/^\n*/", "", $str);
    $str = preg_replace("/\n*$/", "", $str);
    $str = preg_replace("/\n\s*(\n\s*)+/", "\n\n", $str);

    $str = preg_replace('/(?:\n|^)-\s+(.*(\n|$)(\s*(\n|$))*)/m', "<li>$1</li>", $str);
    
    $str = preg_replace("/((<li>.*<\/li>)+)/s", "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">$1</table>", $str);
    $str = preg_replace("/<li>(.*?)<\/li>/s", "<tr valign=\"top\"><td class=\"text\" nowrap>�&nbsp;&nbsp;</td><td class=\"text\">$1</td></tr>", $str);
    $str = preg_replace("/<table/", "<br><table", $str);

    $str = preg_replace("/\n/", "<br>", $str);

    $str = preg_replace("/\{\s*([^\|\�\}]+?)\s*[\|\�]\s*(http:\/\/.*?)\s*\}/", "<a href=\"$2\" target=\"_blank\">$1</a>", $str);
    $str = preg_replace("/\{\s*([^\|\�\}]+?)\s*[\|\�]\s*(.*?)\s*\}/", "<a href=\"$2\">$1</a>", $str);

    $str = preg_replace("/\{(.*?)\}/", "<b>$1</b>", $str);

    return $str;
}

/********************************************************************
    to_system_date
*********************************************************************/

function to_system_date($date)
{
	if ($date == "0000-00-00" or $date == NULL)
    {
        
		return "";
		
    }

    if ($date)
    {
        return date("d.m.y", strtodate($date));
    }
    else
    {
        return "";
    }
}

/********************************************************************
    from_system_date
*********************************************************************/

function from_system_date($date)
{
    if ($date and $date != '0000-00-00')
    {
        return date("Y-m-d", strtodate($date));
    }
    else
    {
        return null;
    }
}

/********************************************************************
    feet to sqm
*********************************************************************/

function feet_to_sqm($number)
{
    return  round(0.09290304*$number, 2);
}


/********************************************************************
    sqm to feet
*********************************************************************/

function sqm_to_feet($number)
{
    return  round($number*10.76392, 2);
}

/********************************************************************
    sqm to feet
*********************************************************************/

function mysql_date_to_xls_date($mysql_date)
{
    if(substr($mysql_date, 0, 4) == '0000'){return "";}
	if($mysql_date == NULL) {return "";}
	if($mysql_date == '') {return "";}
	
	if(strlen($mysql_date) > 10)
	{
		$mysql_date = substr($mysql_date, 0, 10);	
	}

	$date = new DateTime($mysql_date);
	$ExcelDate = round(25569 + ($date->getTimestamp() / 86400), 0);
	return $ExcelDate;
}

/********************************************************************
    convert hex color to rgb color
*********************************************************************/
function hex2rgb($hex) 
{
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   //return implode(",", $rgb); // returns the rgb values separated by commas
   return $rgb; // returns an array with the rgb values
}

?>