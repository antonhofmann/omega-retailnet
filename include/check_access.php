<?php
//check if user has access to a project
if(param("pid") and user_id())
{
	if(has_access("has_access_to_all_projects") 
		or has_access("has_full_access_to_cer"))
	{
		
	}
	else
	{
		//get all projects where the user has access
		$user_id = user_id();
		$user_address = 0;
		$user_country = 0;
		$user_email = "nomail";

		$project_ids = array();

		// get user's address
		$sql = "select user_address, address_country, user_can_only_see_his_projects, user_email " . 
			   "from users ".
			   "lfte join addresses on address_id = user_address " . 
			   "where user_id = " . $user_id;

		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{            
			$user_address = $row["user_address"];
			$user_country = $row["address_country"];
			$user_email = $row["user_email"];
			
			$project_access_filter = "";
			if($row["user_can_only_see_his_projects"] == 1)
			{
				$project_access_filter = " and order_user = " . user_id() . " ";
			}
		}

		//get user's roles
		$user_roles = array();
    
		$sql = "select user_role_role ".
			   "from user_roles ".
			   "left join roles on user_role_role = role_id ".
			   "where user_role_user = " . $user_id;

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$user_roles[] = $row["user_role_role"];
		}

		// check client
		$sql = "select project_id " .
			   "from projects ".
			   "left join orders on order_id = project_order " .
			   "inner join project_costs on project_cost_order = project_order " .
			   "where order_user = " . $user_id . 
			   "   and order_client_address = " . $user_address;

		if(has_access("has_access_to_retail_only")) {
			$sql .= " and project_cost_type in (1) ";
		}

		if(has_access("has_access_to_wholesale")) {
			$sql .= " and project_cost_type in (2, 6) ";
		}
		
		$res = mysql_query($sql) or dberror($sql);

		while ($row = mysql_fetch_assoc($res))
		{            
			$project_ids[] = $row["project_id"];
		}


		//check company access
		if(has_access("has_access_to_all_projects_of_his_company"))
		{
			$sql = "select project_id " .
				   "from projects ".
				   "left join orders on order_id = project_order " .
				   "inner join project_costs on project_cost_order = project_order " .
				   "where order_client_address = " . $user_address;

			
			if(has_access("has_access_to_retail_only")) {
				$sql .= " and project_cost_type in (1) ";
			}

			if(has_access("has_access_to_wholesale")) {
				$sql .= " and project_cost_type in (2, 6) ";
			}
			
			
			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{            
				$project_ids[] = $row["project_id"];
			}

		}

		// local agent
		
		/*
		if (has_access("has_access_to_all_projects_of_his_country")
			or has_access("has_access_to_all_projects_of_his_country"))
		{
		*/

			$country_filter = "";
			$sql = "select * from country_access " .
				   "where country_access_user = " . user_id();


			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{            
				$country_filter.= " or address_country = " . $row["country_access_country"];
			}

			if($country_filter)
			{
				$country_filter = "address_country  = " . $user_country . $country_filter;
			}
			else
			{
				$country_filter = "address_country  = " . $user_country;
			}
			
			if($country_filter)
			{
				$sql = "select project_id " .
					   "from projects ".
					   "left join orders on order_id = project_order " .
					   "inner join project_costs on project_cost_order = project_order " .
					   "left join addresses on address_id = order_client_address " .
					   "where (" . $country_filter . ") ";
			}
			else
			{
				$sql = "select project_id " .
					   "from projects ".
					   "left join orders on order_id = project_order " .
					   "inner join project_costs on project_cost_order = project_order " .
					   "left join addresses on address_id = order_client_address " .
					   "where (" . $country_filter . ") " . 
					   $project_access_filter;
			}


			if(has_access("has_access_to_retail_only")) {
				$sql .= " and project_cost_type in (1) ";
			}

			if(has_access("has_access_to_wholesale")) {
				$sql .= " and project_cost_type in (2, 6) ";
			}

			

			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{            
				$project_ids[] = $row["project_id"];
			}
		
		/*
		}
		*/
		
		// retail coordinator, design contractor, design supervisor
		$sql = "select project_id " .
			   "from projects ".
			   "left join orders on order_id = project_order " .
			   "inner join project_costs on project_cost_order = project_order " .
			   "where order_actual_order_state_code >= 220 " . 
			   "   and (project_retail_coordinator = " . $user_id . 
			   "   or project_design_contractor = " . $user_id .
			   "   or project_design_supervisor = " . $user_id . 
			   ")";

		if(has_access("has_access_to_retail_only")) {
			$sql .= " and project_cost_type in (1) ";
		}

		if(has_access("has_access_to_wholesale")) {
			$sql .= " and project_cost_type in (2, 6) ";
		}

		$res = mysql_query($sql) or dberror($sql);

		while ($row = mysql_fetch_assoc($res))
		{            
			$project_ids[] = $row["project_id"];
		}

	   
		// check supplier or forwarder
		$sql = "select distinct project_id, order_item_order " .
			   "from projects ".
		       "left join order_items on order_item_order = project_order " .
			   "where order_item_supplier_address = " .$user_address . 
			   "   or order_item_forwarder_address = " . $user_address;
		
		$res = mysql_query($sql) or dberror($sql);
	
		while ($row = mysql_fetch_assoc($res))
		{            
			$project_ids[] = $row["project_id"];
		}


		//get projects from attachments
		$sql_order_addresses = 'select DISTINCT project_id from order_files ' . 
						   'left join order_file_addresses on order_file_address_file = order_file_id ' .
			               'left join projects on project_order = order_file_order ' . 
			               "left join orders on order_id = order_file_order " .
						   "inner join project_costs on project_cost_order = order_file_order " .
						   'where order_file_address_address = ' . $user_address;

		if(has_access("has_access_to_retail_only")) {
			$sql_order_addresses .= " and project_cost_type in (1) ";
		}

		if(has_access("has_access_to_wholesale")) {
			$sql_order_addresses .= " and project_cost_type in (2, 6) ";
		}

		$res = mysql_query($sql_order_addresses) or dberror($sql_order_addresses);
		while ($row = mysql_fetch_assoc($res))
		{
			 $project_ids[] = $row["project_id"];
		}


		//check cer access
		if(has_access("cer_has_full_access_to_his_projects"))
		{
			$sql = "select distinct project_id " .
				   "from projects ".
				   "left join orders on order_id = project_order " .
				   "inner join project_costs on project_cost_order = project_order " .
				   "where order_shop_address_country = " . $user_country;
			
			if(has_access("has_access_to_retail_only")) {
				$sql .= " and project_cost_type in (1) ";
			}

			if(has_access("has_access_to_wholesale")) {
				$sql .= " and project_cost_type in (2, 6) ";
			}
			
			$res = mysql_query($sql) or dberror($sql);
		
			while ($row = mysql_fetch_assoc($res))
			{            
				$project_ids[] = $row["project_id"];
			}
		}



		//check travelling projects
		$sql = "select * from posareatypes " . 
				   "where posareatype_id IN (4, 5) and " . 
				   "posareatype_email1 = " . dbquote($user_email) . 
				   " or posareatype_email2 = " . dbquote($user_email) .
				   " or posareatype_email3 = " . dbquote($user_email) .
				   " or posareatype_email4 = " . dbquote($user_email) .
				   " or posareatype_email5 = " . dbquote($user_email) .
				   " or posareatype_email6 = " . dbquote($user_email);
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			
			$sql = "select distinct project_id " .
				   "from posorderspipeline ".
				   "left join orders on order_id = posorder_order " .
				   "left join projects on project_order = order_id " .
				   "left join posareaspipeline on posarea_posaddress = posorder_posaddress " .
				   "where (order_archive_date is null or order_archive_date = '0000-00-00') " . 
				   " and posarea_area IN (4,5) and posorder_order > 0";


			$res = mysql_query($sql) or dberror($sql);
		
			while ($row = mysql_fetch_assoc($res))
			{            
				$project_ids[] = $row["project_id"];
			}


			$sql = "select distinct project_id " .
				   "from posorders ".
				   "left join orders on order_id = posorder_order " .
				   "left join projects on project_order = order_id " .
				   "left join posareas on posarea_posaddress = posorder_posaddress " .
				   "where (order_archive_date is null or order_archive_date = '0000-00-00') " . 
				   " and posarea_area IN (4,5) and posorder_order > 0";


			$res = mysql_query($sql) or dberror($sql);
		
			while ($row = mysql_fetch_assoc($res))
			{            
				$project_ids[] = $row["project_id"];
			}
		}


		if(!in_array( param("pid"), $project_ids))
		{
			redirect("noaccess.php");
		}

	}
}
?>