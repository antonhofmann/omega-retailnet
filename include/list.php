<?php
/********************************************************************

    list.php

    Classes and functions used to render lists.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-31
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-02-11
    Version:        1.6.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

/********************************************************************
    Constants
*********************************************************************/

define("LIST_FILTER_NONE", 0);
define("LIST_FILTER_FREE", 1);
define("LIST_FILTER_LIST", 2);

define("LIST_BUTTON_BACK", 1);
define("LIST_BUTTON_NEW", 2);
define("LIST_BUTTON_SAVE", 3);
define("LIST_BUTTON_FILTER", 4);
define("LIST_BUTTON_REMOVE_FILTER", 5);

define("LIST_HAS_HEADER", 1 << 0);
define("LIST_HAS_FOOTER", 1 << 1);
define("LIST_SORTABLE", 1 << 2);
define("LIST_SHOW_COUNT", 1 << 3);
define("LIST_HAS_SEPARATOR", 1 << 4);
define("LIST_IN_MODAL_WINDOW", 1 << 5);

define("LIST_DEFAULT_FLAGS", LIST_HAS_HEADER | LIST_SHOW_COUNT | LIST_SORTABLE);

define("LIST_MOVE_UP", 1);
define("LIST_MOVE_DOWN", 2);

define("COLUMN_ALIGN_LEFT", 0);
define("COLUMN_ALIGN_RIGHT", 1 << 0);
define("COLUMN_ALIGN_CENTER", 1 << 1);
define("COLUMN_NO_WRAP", 1 << 2);
define("COLUMN_HIDDEN", 1 << 3);
define("COLUMN_BREAK", 1 << 4);
define("COLUMN_HILITE", 1 << 5);
// only for the add_text_column method
define("COLUMN_UNDERSTAND_HTML", 1 << 6);

define("COLUMN_TYPE_NORMAL", 1);
define("COLUMN_TYPE_TEXT", 2);
define("COLUMN_TYPE_EDIT", 3);
define("COLUMN_TYPE_IMAGE", 4);
define("COLUMN_TYPE_BUTTON", 5);
define("COLUMN_TYPE_LIST", 6);
define("COLUMN_TYPE_CHECKBOX", 7);
define("COLUMN_TYPE_DATE_EDIT", 8);
define("COLUMN_TYPE_NUMBER_EDIT", 9);
define("COLUMN_TYPE_LABEL", 10);

/********************************************************************
    List class
*********************************************************************/

class ListView
{
    var $sql;
    var $key;
    var $edit_key;
    var $entity;
    var $title;
	var $comment;
    var $filter;
    var $order;
    var $group;
    var $table;
    var $table_key;
    var $check_table;
    var $check_table_id;
    var $check_table_master;
    var $check_table_lookup;
    var $check_addonly;
    var $master_table;
    var $master_key;
    var $sorter_field;
    var $delete_button;
    var $flags;
    var $colspan;
    var $hidden;
    var $columns;
    var $buttons;

    function ListView($sql, $flags = LIST_DEFAULT_FLAGS)
    {
        $this->sql = $sql;
        $this->key = "";
        $this->edit_key = "";
        $this->entity = "record";
        $this->title = "";
		$this->comment = "";
        $this->filter = "";
        $this->order = "";
        $this->group = "";
        $this->table = "";
        $this->table_key = "";
        $this->check_table = "";
        $this->check_table_id = "";
        $this->check_table_master = "";
        $this->check_table_lookup = "";
        $this->check_addonly = false;
        $this->master_table = "";
        $this->master_key = "";
        $this->sorter_field = "";
        $this->delete_button = "";
        $this->flags = $flags;
        $this->colspan = 0;

        $this->hidden = array();
        $this->columns = array();
        $this->buttons = array();
		$this->listfilters = array();

        if (preg_match("/\s*(show|from)\s+(.*?)(\s+|$)/i", $sql, $matches))
        {
            $this->table = $matches[2];
            $this->table_key = table_key($this->table);
        }
        else
        {
            error("Invalid SQL statement (ListView::ListView).");
        }
    }

    function set_key($key)
    {
        $this->key = $key;
    }

    function set_edit_key($key)
    {
        $this->edit_key = $key;
    }

    function set_entity($entity)
    {
        $this->entity = $entity;
    }

    function set_title($title)
    {
        $this->title = $title;
    }

	function set_comment($comment)
    {
        $this->comment = $comment;
    }

    function set_filter($filter)
    {
        $this->filter = $filter;
    }

    function set_order($order)
    {
        $this->order = $order;
    }

    function set_group($group, $lookup = "")
    {
        $this->group = $group;

        if ($lookup)
        {
            $this->group_lookup = $lookup;
        }
        else
        {
            $this->group_lookup = $group;
        }
    }

    function set_checkbox($check_table, $master_table, $master_key, $addonly = true)
    {
        $this->check_table = $check_table;
        $this->master_table = $master_table;
        $this->master_key = $master_key;
        $this->check_addonly = $addonly;

        $this->check_table_id = table_key($check_table);
        $this->check_table_master = table_reference($check_table, $master_table);
        $this->check_table_lookup = table_reference($check_table, $this->table);

        if ($this->table == $master_table)
        {
            $this->check_table_lookup = table_reference($check_table, $this->table, 1);

            if ($this->check_table_lookup == null)
            {
                error("Master and lookup tables are equal but checkbox doesn't have two references to \"" . $this->table . "\" (ListView::set_checkbox).");
            }
        }
    }

    function set_sorter($field)
    {
        $this->sorter_field = $field;
    }

    function set_delete_button($name)
    {
        $this->delete_button = $name;
    }

    function set_footer($column, $value)
    {
        for ($i = 0; $i < count($this->columns); $i++)
        {
            if ($this->columns[$i]["name"] == $column)
            {
                $this->columns[$i]["footer"] = $value;
                return;
            }
        }
    }

    function set_group_footer($column, $group, $value)
    {
        for ($i = 0; $i < count($this->columns); $i++)
        {
            if ($this->columns[$i]["name"] == $column)
            {
                $this->columns[$i]["footers"][$group] = $value;
                return;
            }
        }
    }

    function add_hidden($field, $value)
    {
        $this->hidden[$field] = $value;
    }

    function add_column($field, $caption, $view = "", $filter = LIST_FILTER_NONE, $filter_sql = "", $flags = 0, $altfield = "")
    {
        global $__needs_form;

        $__needs_form = true;

        if ($altfield == "")
        {
            $altfield = $field;
        }

        $this->columns[] = array("type" => COLUMN_TYPE_NORMAL,
                                 "name" => $field,
                                 "field" => $field,
                                 "altfield" => $altfield,
                                 "caption" => $caption,
                                 "view" => $view,
                                 "filter" => $filter,
                                 "filter_sql" => $filter_sql,
                                 "footer" => "",
                                 "footers" => array(),
                                 "flags" => $flags);
    }

    function add_text_column($name, $caption, $flags = 0, $values = array())
    {
        global $__needs_form;

        $__needs_form = true;

        $this->columns[] = array("type" => COLUMN_TYPE_TEXT,
                                 "name" => $name,
                                 "caption" => $caption,
                                 "values" => $values,
                                 "footer" => "",
                                 "footers" => array(),
                                 "flags" => $flags);
    }


    function add_list_column($name, $caption, $sql, $flags = 0, $values, $dynamic_sql_array = array())
    {
        global $__needs_form;

        $__needs_form = true;

        $this->columns[] = array("type" => COLUMN_TYPE_LIST,
                                 "name" => $name,
                                 "caption" => $caption,
                                 "sql" => $sql,
                                 "values" => $values,
                                 "footer" => "",
                                 "footers" => array(),
                                 "flags" => $flags,
			                     "dynamic_sql_array" => $dynamic_sql_array);
    }


    function add_edit_column($name, $caption, $width, $flags = 0, $values = array())
    {
        global $__needs_form;

        $__needs_form = true;

        $this->columns[] = array("type" => COLUMN_TYPE_EDIT,
                                 "name" => $name,
                                 "caption" => $caption,
                                 "width" => $width,
                                 "values" => $values,
                                 "footer" => "",
                                 "footers" => array(),
                                 "flags" => $flags);
    }

	//aho
	function add_date_edit_column($name, $caption, $width, $flags = 0, $values = array())
    {
        global $__needs_form;

        $__needs_form = true;

        $this->columns[] = array("type" => COLUMN_TYPE_DATE_EDIT,
                                 "name" => $name,
                                 "caption" => $caption,
                                 "width" => $width,
                                 "values" => $values,
                                 "footer" => "",
                                 "footers" => array(),
                                 "flags" => $flags);
    }

	//aho
	function add_number_edit_column($name, $caption, $width, $flags = 0, $values = array())
    {
        global $__needs_form;

        $__needs_form = true;

        $this->columns[] = array("type" => COLUMN_TYPE_NUMBER_EDIT,
                                 "name" => $name,
                                 "caption" => $caption,
                                 "width" => $width,
                                 "values" => $values,
                                 "footer" => "",
                                 "footers" => array(),
                                 "flags" => $flags);
    }


    function add_checkbox_column($name, $caption, $flags = 0, $values = array(), $checkbox_caption = "")
    {
        global $__needs_form;

        $__needs_form = true;

        $this->columns[] = array("type" => COLUMN_TYPE_CHECKBOX,
                                 "name" => $name,
                                 "caption" => $caption,
                                 "values" => $values,
			                     "checkbox_caption" => $checkbox_caption,
                                 "footer" => "",
                                 "footers" => array(),
                                 "flags" => $flags);
    }

    function add_label_column($field, $name, $caption, $flags = 0, $values = array())
    {
        global $__needs_form;

        $__needs_form = true;

        $this->columns[] = array("type" => COLUMN_TYPE_LABEL,
                                 "field" => $field,
                                 "name" => $name,
                                 "caption" => $caption,
                                 "values" => $values,
                                 "footer" => "",
                                 "footers" => array(),
                                 "flags" => $flags);
    }

    function add_image_column($name, $caption, $flags = 0, $values = array())
    {
        global $__needs_form;

        $__needs_form = true;

        $this->columns[] = array("type" => COLUMN_TYPE_IMAGE,
                                 "name" => $name,
                                 "caption" => $caption,
                                 "values" => $values,
                                 "footer" => "",
                                 "footers" => array(),
                                 "flags" => $flags);
    }

// neo
    function add_button_column($field, $name, $view, $flags = 0)
    {
        global $__needs_form;

        $__needs_form = true;

        $this->columns[] = array("type" => COLUMN_TYPE_BUTTON,
                                 "name" => $name ,
                                 "field" => $field,
                                 "caption" => "",
                                 "view" => $view,
                                 "flags" => $flags);
    }

    function add_button($name, $caption, $view = "")
    {
        global $__needs_form;

        $__needs_form = true;

        $this->buttons[$name] = array("name" => $name,
                                      "caption" => $caption,
                                      "view" => $view);
    }


	function add_listfilters($name, $caption, $type = '', $data = '', $value = '')
    {

        $this->listfilters[$name] = array("name" => $name,
                                      "caption" => $caption,
                                      "type" => $type, 
			                          "data" => $data,
			                          "value" => $value);
    }

    function button($name)
    {
        global $_REQUEST;
        return isset($_REQUEST["action"]) && $_REQUEST["action"] == $name;
    }


    function values($name, $values = null)
    {
		for ($i = 0; $i < count($this->columns); $i++)
        {

			if ($this->columns[$i]["name"] == $name)
            {
                if ($this->columns[$i]["type"] == COLUMN_TYPE_TEXT || $this->columns[$i]["type"] == COLUMN_TYPE_EDIT || $this->columns[$i]["type"] == COLUMN_TYPE_DATE_EDIT || $this->columns[$i]["type"] == COLUMN_TYPE_NUMBER_EDIT || $this->columns[$i]["type"] == COLUMN_TYPE_LIST || $this->columns[$i]["type"] == COLUMN_TYPE_CHECKBOX)
                {
					if (!is_null($values))
                    {
						
						$this->columns[$i]["values"] = $values;
                    }
					
					return $this->columns[$i]["values"];
                }
                else
                {
                    error("Column \"$name\" is not a text, list or edit column (ListView::values).");
                }
            }
        }

        error("Unknown column \"$name\" (ListView::values).");
    }

    function can_process()
    {
        global $_REQUEST;

        if ($_REQUEST["action"] == LIST_BUTTON_BACK ||
            $_REQUEST["action"] == LIST_BUTTON_NEW ||
            $_REQUEST["action"] == LIST_BUTTON_SAVE ||
            $_REQUEST["action"] == LIST_BUTTON_FILTER ||
            $_REQUEST["action"] == LIST_BUTTON_REMOVE_FILTER)
        {
            return true;
        }

        if (preg_match("/__" . $this->entity . "_delete_\d+/i", $_REQUEST["action"]) ||
            preg_match("/__" . $this->entity . "_edit(\d+)_\d+/i", $_REQUEST["action"]) ||
            preg_match("/__" . $this->entity . "_up_\d+/i", $_REQUEST["action"]) ||
            preg_match("/__" . $this->entity . "_down_\d+/i", $_REQUEST["action"]))
        {
            return true;
        }

        return false;
    }

    function populate()
    {
        global $_REQUEST;

        foreach (array_keys($_REQUEST) as $key)
        {
            for ($i = 0; $i < count($this->columns); $i++)
            {
                if ($this->columns[$i]["type"] == COLUMN_TYPE_NUMBER_EDIT || $this->columns[$i]["type"] == COLUMN_TYPE_DATE_EDIT || $this->columns[$i]["type"] == COLUMN_TYPE_EDIT || $this->columns[$i]["type"] == COLUMN_TYPE_LIST)
                {
                    if (preg_match("/__" . $this->entity . "_" . $this->columns[$i]["name"] . "_(\d+)/i", $key, $matches))
                    {
                        $this->columns[$i]["values"][$matches[1]] = $_REQUEST[$key];
                    }
                }
                elseif ($this->columns[$i]["type"] == COLUMN_TYPE_CHECKBOX)
                {
                    if (preg_match("/__" . $this->entity . "_" . $this->columns[$i]["name"] . "_(\d+)/i", $key, $matches))
                    {
                        $this->columns[$i]["values"][$matches[1]] = $_REQUEST[$key];
                    }
                }
            }
        }
    }

    function process()
    {
        global $_REQUEST;

        if ($this->button(LIST_BUTTON_BACK))
        {
            $view = $this->buttons[LIST_BUTTON_BACK]["view"];

            if ($view)
            {
                redirect($view);
            }
            else
            {
                redirect(get_referer());
            }
        }
        else if ($this->button(LIST_BUTTON_NEW))
        {
			$view = $this->buttons[LIST_BUTTON_NEW]["view"];

            if ($view)
            {
                redirect($view);
            }
            else
            {
                
                error("Missing view parameter for button \"LIST_BUTTON_NEW\" (ListView::process).");
            }
        }
        else if ($this->button(LIST_BUTTON_SAVE))
        {
            $this->save();

            $view = $this->buttons[LIST_BUTTON_BACK]["view"];

            if ($view)
            {
                redirect($view);
            }
            else
            {
                redirect(get_referer());
            }
        }
        else if ($this->button(LIST_BUTTON_FILTER))
        {
            $this->filter();
        }
        else if ($this->button(LIST_BUTTON_REMOVE_FILTER))
        {
            unset($_REQUEST["filter"]);
            return true;
        }
        else if (isset($_REQUEST["action"]))
        {
            if (preg_match("/__" . $this->entity . "_edit(\d+)_(\d+)/i", $_REQUEST["action"], $matches))
            {
                $index = $matches[1];
                $id = $matches[2];

                $column = $this->columns[$index];

                $sql = $this->sql . " where " . $this->table . "." . $this->table_key . " = " . $id;
                $res = mysql_query($sql) or dberror($sql);
                $row = mysql_fetch_assoc($res);

                $view = $this->expand_fields($res, $row, $column["view"]);
                $view = build_url($view, array("id" => $id));

                redirect($view);
            }
            else if (preg_match("/__" . $this->entity . "_delete_(\d+)/i", $_REQUEST["action"], $matches))
            {
                $sql = "delete from " . $this->table . " " .
                       "where " . $this->table_key . " = " . $matches[1];

                mysql_query($sql) or dberror($sql);
                return true;
            }
            else if (preg_match("/__" . $this->entity . "_up_(\d+)/i", $_REQUEST["action"], $matches))
            {
                $this->adjust_priority($matches[1], LIST_MOVE_UP);
            }
            else if (preg_match("/__" . $this->entity . "_down_(\d+)/i", $_REQUEST["action"], $matches))
            {
                $this->adjust_priority($matches[1], LIST_MOVE_DOWN);
            }
        }

        return false;
    }

    function save()
    {
		global $_REQUEST;

        $check_ids = array();

        foreach (array_keys($_REQUEST) as $key)
        {
            if (preg_match("/__check__(\d+)/i", $key, $matches))
            {
                $check_ids[$matches[1]] = true;
            }
        }

        if ($this->check_addonly)
        {
            $sql = "select " . $this->check_table_lookup . " from " . $this->check_table . " where " . $this->check_table_master . " = " . $this->master_key;
            $res = mysql_query($sql) or dberror($sql);

            while ($row = mysql_fetch_row($res))
            {
                unset($check_ids[$row[0]]);
            }
        }
        else
        {
            $sql = "delete from " . $this->check_table . " where " . $this->check_table_master . " = " . $this->master_key;
            mysql_query($sql) or dberror($sql);
        }

        foreach (array_keys($check_ids) as $id)
        {
            $fields = array($this->check_table_master, $this->check_table_lookup, "date_created", "user_created", "date_modified", "user_modified");
            $values = array($this->master_key, $id, "current_timestamp", dbquote(user_login()), "current_timestamp", dbquote(user_login()));

            $sql = "insert into " . $this->check_table . "(" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
            mysql_query($sql) or dberror($sql);
        }
    }

    function filter()
    {
        global $__params;

        $filter = new FilterAction(build_page_url(), $this->entity);

        foreach ($this->columns as $column)
        {
            if ($column["type"] == COLUMN_TYPE_NORMAL)
            {
                if ($column["filter"] != LIST_FILTER_NONE)
                {
                    $filter->add_item($column["caption"], $column["field"], $column["altfield"], $column["filter"], $column["filter_sql"]);
                }
            }
        }

        set_session_value("filter_object", $filter);
        redirect("filter.php");
    }

    function render()
    {
        global $_REQUEST;
        global $_SERVER;

        // Add hidden parameters

        if (isset($_REQUEST["order"]))
        {
            echo "<input type=\"hidden\" name=\"order\" value=\"", htmlspecialchars($_REQUEST["order"]), "\" />";
        }

        if (isset($_REQUEST["filter"]))
        {
            echo "<input type=\"hidden\" name=\"filter\" value=\"", htmlspecialchars($_REQUEST["filter"]), "\" />";
        }

        foreach (array_keys($this->hidden) as $key)
        {
            echo "<input type=\"hidden\" name=\"$key\" value=\"", htmlspecialchars($this->hidden[$key]), "\" />";
        }

        //echo "<p>";

        // Build SQL select statement and select records

        $sql = $this->sql;

        $where = array();

        if ($this->filter)
        {
            $where[] = "(" . $this->filter . ")";
        }

        if (isset($_REQUEST["filter"]))
        {
            $where[] = "(" . $_REQUEST["filter"] . ")";
        }

        if (count($where))
        {
            $sql .= " where " . join(" and ", $where);
        }

        if ($this->sorter_field)
        {
			if ($this->group)
            {
                $sql .= " order by " . $this->group . ", " . $this->sorter_field;
            }
            else
            {
                $sql .= " order by " . $this->sorter_field;
            }
        }
        else if (isset($_REQUEST["order"]) and strpos($this->sql, $_REQUEST["order"]) > 0)
        {
			if ($this->group)
            {
                $sql .= " order by " . $this->group . ", " . $_REQUEST["order"];
            }
            else
            {
                $sql .= " order by " . $_REQUEST["order"];
            }
        }
        else if ($this->order)
        {

			if ($this->group)
            {
                $sql .= " order by " . $this->group . ", " . $this->order;
            }
            else
            {
                $sql .= " order by " . $this->order;
            }
        }

        $res = mysql_query($sql) or dberror($sql);

        // Calculate layout parameters

        $colcount = 0;

        foreach ($this->columns as $column)
        {
            if (!($column["flags"] & COLUMN_HIDDEN))
            {
                $colcount++;
            }
        }

        $this->colspan = 2 * $colcount - 1;
        $has_buttons = $this->sorter_field || $this->delete_button;

        if (mysql_num_rows($res) || count($this->buttons))
        {
            // Open table

            if( $this->flags & LIST_IN_MODAL_WINDOW)
			{
				echo "<table class=\"list_modal\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
			}
			else
			{
				echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
			}

            // Write first row with width definitions

            $check_ids = array();

            echo "<tr>";

            if ($this->check_table)
            {
                $this->colspan += 2;

                echo "<td></td>";

                echo "<td width=\"4\">";
                echo "<img src=\"/pictures/spacer.gif\" width=\"4\" height=\"1\" alt=\"\" />";
                echo "</td>";

                if (!$this->check_addonly)
                {
                    $sql = "select " . $this->check_table_lookup . " from " . $this->check_table . " where " . $this->check_table_master . " = " . $this->master_key;
                    $res2 = mysql_query($sql) or dberror($sql);

                    while ($row2 = mysql_fetch_row($res2))
                    {
                        $check_ids[$row2[0]] = true;
                    }
                }
            }

            for ($index = 0; $index < $colcount; $index++)
            {
                if ($index > 0)
                {
                    echo "<td width=\"10\">";
                    echo "<img src=\"/pictures/spacer.gif\" width=\"10\" height=\"1\" alt=\"\" />";
                    echo "</td>";
                }

                echo "<td></td>";
            }

            if ($has_buttons)
            {
                $this->colspan += 2;

                echo "<td width=\"4\">";
                echo "<img src=\"/pictures/spacer.gif\" width=\"4\" height=\"1\" alt=\"\" />";
                echo "</td>";

                echo "<td></td>";
            }

            echo "</tr>";

            // Write title if available

            if ($this->title && (mysql_num_rows($res) || $this->flags & LIST_SHOW_COUNT))
            {
                echo "<tr>";
                echo "<td colspan=\"", $this->colspan, "\" class=\"section\">";
                //echo htmlspecialchars($this->title);
				echo $this->title;
                echo "</td>";
                echo "</tr>";

                echo "<tr>";
                echo "<td colspan=\"", $this->colspan, "\" height=\"10\">";
                echo "</td>";
                echo "</tr>";
            }

			// Write comment if available

            if ($this->comment && (mysql_num_rows($res) || $this->flags & LIST_SHOW_COUNT))
            {
                echo "<tr>";
                echo "<td colspan=\"", $this->colspan, "\" class=\"text\">";
                //echo htmlspecialchars($this->comment);
				echo $this->comment;
                echo "</td>";
                echo "</tr>";

                echo "<tr>";
                echo "<td colspan=\"", $this->colspan, "\" height=\"10\">";
                echo "</td>";
                echo "</tr>";
            }




			// Draw list filters
			if (count($this->listfilters) > 0)
            {
				echo "<tr>";
                echo "<td bgcolor=\"#F6F6FF\" nowrap=\"nowrap\" colspan=\"", $this->colspan, "\" class=\"button\" nowrap='nowrap'>";
                echo "<h3>List Filter Selection</h3>";
				
				foreach($this->listfilters as $key=>$listfilter)
				{
					if($listfilter['type'] == 'select')
					{
						echo '<strong>' . $listfilter['caption'] . "</strong>&nbsp;&nbsp;";

						$content = "<select name=\"" . $listfilter['name'] . "\" id=\"" . $listfilter['name'] . "\" onchange=\"javascript:document.forms['main'].submit();\">";
						$content .= "<option value=\"\"></option>";

						if (is_array($listfilter['data']))
						{
							foreach ($listfilter['data'] as $key=>$value)
							{
								if($listfilter["value"] == 0 and $listfilter["value"] == $key)
								{
									$selected = " selected='selected'";
								}
								else
								{
									$selected = $listfilter["value"] && $listfilter["value"] == $key ? " selected='selected'" : "";
								}
								$content .= "<option value=\"" . htmlspecialchars($key) . "\"$selected>" . htmlspecialchars($value) . "</option>";
							}
						}
						$content .= '</select>';
						$content .= '&nbsp;&nbsp;';
						echo $content;

					}
					elseif($listfilter['type'] == 'input')
					{
						echo '<strong>' . $listfilter['caption'] . "</strong>&nbsp;&nbsp;";
						$content = "<input type=\"text\" name=\"" . $listfilter['name'] . "\" id=\"" . $listfilter['name'] . "\" value=\"" . $listfilter['value'] . "\" onchange=\"javascript:document.forms['main'].submit();\">";
						$content .= '&nbsp;&nbsp;';
						echo $content;
					}
				
				}

				echo "<br /><br />";

                echo "</td>";
                echo "</tr>";

                echo "<tr>";
                echo "<td colspan=\"", $this->colspan, "\" height=\"10\">";
                echo "</td>";
                echo "</tr>";
			}


            // Draw main buttons

            if (count($this->buttons))
            {
                $buttons = "";

                foreach ($this->buttons as $button)
                {
                    if ($button["name"] == LIST_BUTTON_REMOVE_FILTER && !isset($_REQUEST["filter"]))
                    {
                        continue;
                    }

                    if (isset($script))
                    {
                        $buttons .= " | ";
                    }

                    if (substr($button["view"], 0, 11) == "javascript:")
					{
						
						$script = $button["view"];
						$buttons .= "<a href=\"$script\">" . htmlspecialchars($button["caption"]) . "</a>";
						$script .= $button["view"];
					}
					else
					{
						$script = "javascript:button('" . $button["name"] . "')";
						$buttons .= "<a href=\"$script\">" . htmlspecialchars($button["caption"]) . "</a>";
					}
                }


                echo "<tr>";
                echo "<td colspan=\"", $this->colspan, "\" class=\"button\" nowrap='nowrap'>";
                echo $buttons;
                echo "</td>";
                echo "</tr>";

                echo "<tr>";
                echo "<td colspan=\"", $this->colspan, "\" height=\"10\">";
                echo "</td>";
                echo "</tr>";
            }

            // Display filter if one has been applied

            if (isset($_REQUEST["filter"]) && count($this->buttons))
            {
                $filter = preg_replace("/\blike\b/i", "=", $_REQUEST["filter"]);
                $filter = preg_replace("/\%/i", "*", $filter);

                foreach ($this->columns as $column)
                {
                    if ($column["type"] == COLUMN_TYPE_NORMAL)
                    {
                        $filter = preg_replace("/" . preg_quote($column["altfield"]) . " = /i", $column["caption"] . " = ", $filter);
                    }
                }

                echo "<tr>";
                echo "<td colspan=\"", $this->colspan, "\" class=\"small\">";
                echo "<b>Filtered</b>: ", htmlspecialchars($filter);
                echo "</td>";
                echo "</tr>";

                echo "<tr>";
                echo "<td colspan=\"", $this->colspan, "\" height=\"10\">";
                echo "</td>";
                echo "</tr>";
            }
        }

        // Draw records

        if (mysql_num_rows($res) > 0)
        {
            // Write header if necessary

            if ($this->flags & LIST_HAS_HEADER)
            {
                $this->draw_line();

                echo "<tr valign=\"top\">";

                if ($this->check_table)
                {
                    echo "<td class=\"list_header\" colspan=\"2\">";
                    echo "&nbsp;";
                    echo "</td>";
                }

				for ($index = 0; $index < count($this->columns); $index++)
                {
                    $column = $this->columns[$index];

                    if ($column["flags"] & COLUMN_HIDDEN)
                    {
                        continue;
                    }

                    if ($index > 0)
                    {
                        echo "<td class=\"list_header\">";
                        echo "&nbsp;";
                        echo "</td>";
                    }

                    $align = $this->make_align($column);
                    $NOWRAP = $column["flags"] & COLUMN_NO_WRAP ? " nowrap='nowrap'" : "";

                    echo "<td class=\"list_header\"$align$NOWRAP>";

                    if ($this->flags & LIST_SORTABLE && in_array($column["type"], array(COLUMN_TYPE_NORMAL,COLUMN_TYPE_LABEL) ) )
                    {
                        echo "<a href=\"", build_page_url(array("order" => $column["field"])), "\" class=\"list_header\">";
                    }

                    if ($column["caption"])
                    {
                        
						if ($column["flags"] & COLUMN_UNDERSTAND_HTML)
                        {
							$value = $column["caption"];
						}
						else
						{
							$value = htmlspecialchars($column["caption"]);
						}

                        if ($column["flags"] & COLUMN_BREAK)
                        {
                            $value = nl2br($value);
                        }

                        echo $value;
                    }
                    else
                    {
                        echo "&nbsp;";
                    }

                    if ($this->flags & LIST_SORTABLE && in_array($column["type"], array(COLUMN_TYPE_NORMAL,COLUMN_TYPE_LABEL) ) )
                    {
                        echo "</a>";
                    }

                    echo "</td>";
                }

                if ($has_buttons)
                {
                    echo "<td></td>";
                    echo "<td></td>";
                }

                echo "</tr>";
            }

            // Write records

            $this->draw_line();
            $line = 1;
            $group = null;

            if (!$this->key)
            {
                $this->key = mysql_field_name($res, 0);
            }

            while ($row = mysql_fetch_assoc($res))
            {
                
				//prevent from XSS
				foreach ($row as $key=>$value)
				{
					$tmp = str_replace(' ', '', $value);
					$tmp = strtolower($tmp);

					$matches = array();
					if(preg_match('/(bgsound|<xml|script>|<body|background=|url|rel=|<style|http-equiv=|<base|<object|<embed|<xml|<?xml|&#)/', $tmp, $matches)) {
						$row[$key] = strip_tags($value);
					}
					elseif(preg_match('/(http:|https)/', $tmp, $matches)) {
						$this->items[$key]['value'] = strip_tags($item['value']);
					}
				}

				$key = $row[$this->key];

                if ($this->group && $row[$this->group] != $group)
                {
                    if ($group)
                    {
                        $this->draw_group_footer($group, $has_buttons);
                    }

                    echo "<tr valign=\"top\">";

                    if ($this->check_table)
                    {
                        $width = $this->colspan - 2;

                        echo "<td class=\"list_group\" colspan=\"2\">";
                        echo "&nbsp;";
                        echo "</td>";
                    }
                    else
                    {
                        $width = $this->colspan;
                    }

                    echo "<td class=\"list_group\" colspan=\"$width\">";
                    echo htmlspecialchars($row[$this->group_lookup]);
					//echo $row[$this->group_lookup];
                    echo "</td>";
                    echo "</tr>";

                    $group = $row[$this->group];
                    $line = 1;
                }

                if ($this->flags & LIST_HAS_SEPARATOR && $line > 1)
                {
                    $this->draw_line();
                }

                $class = $line % 2 ? "list_text_odd" : "list_text_even";

                echo "<tr valign=\"top\">";

                if ($this->check_table)
                {
                    $name = "__check__" . $key;
                    $checked = isset($check_ids[$key]) ? " checked='checked'" : "";

                    echo "<td class\"$class\">";
                    echo "<input type=\"checkbox\" name=\"$name\" value=\"1\"$checked />";
                    echo "</td>";

                    echo "<td class=\"$class\">&nbsp;</td>";
                }

                for ($index = 0; $index < count($this->columns); $index++)
                {
                    $column = $this->columns[$index];

                    if ($column["flags"] & COLUMN_HIDDEN)
                    {
                        continue;
                    }

                    if ($index > 0)
                    {
                        echo "<td class=\"$class\">";
                        echo "&nbsp;";
                        echo "</td>";
                    }

					
                    $align = $this->make_align($column);
                    $NOWRAP = $column["flags"] & COLUMN_NO_WRAP ? " nowrap='nowrap'" : "";
                    $link = "";
                    $target = "";

                    $font1 = "<font color='#FF0000'>";
                    $font2 = "</font>";

                    if ($column["type"] == COLUMN_TYPE_NORMAL)
                    {
						$name = "__" . $this->entity . "_" . $column["name"] . "_" . $key;

						if(is_numeric($row[$column["field"]]) and !$align) {
							$align = " align=\"right\"";
						}
						
						if ($column["flags"] & COLUMN_UNDERSTAND_HTML)
                        {
							$value = $row[$column["field"]] ? $row[$column["field"]] : "&nbsp;";
						}
						else
						{
							$value = $row[$column["field"]] ? htmlspecialchars($row[$column["field"]]) : "&nbsp;";
						}
                        
						if ($column["flags"] & COLUMN_BREAK)
                        {
                            $value = nl2br($value);
                        }

                        if ($column["view"])
                        {
                            $view = $this->expand_fields($res, $row, $column["view"]);

                            if (substr($view, 0, 7) == "http://")
                            {
                                $link = $view;

								if(strpos($column["view"], "/files") > 0)
								{
									$target = "_blank";
								}
								elseif(strpos($column["view"], "/openfile") > 0)
								{
									$target = "_blank";
								}
                            }
							elseif (substr($view, 0, 8) == "https://")
                            {
                                $link = $view;
                                if(strpos($column["view"], "/files") > 0)
								{
									$target = "_blank";
								}
								elseif(strpos($column["view"], "/openfile") > 0)
								{
									$target = "_blank";
								}
                            }
                            else if (substr($view, 0, 6) == "popup:")
                            {
                                $view = substr($view, 6);
                                $link = "javascript:popup('$view', 500, 400)";
                            }
                            else if (substr($view, 0, 7) == "popup1:")
                            {
                                $view = substr($view, 7);
                                $link = "javascript:popup('$view', 800, 640)";
                            }
							else if (substr($view, 0, 8) == "mvclink:")
                            {
                                $link = "";
								$data_artibute = "";
								$tmp = explode(' ', $view);
								
								if(array_key_exists(0, $tmp))
								{
									$link = $tmp[0];
								}
								if(array_key_exists(1, $tmp))
								{
									$data_artibute = $tmp[1];
								}
                                $target = "_blank";
                            }
                            else if (substr($view, 0, 1) == "/")
                            {
                                $link = $view;
                                $target = "_blank";
                            }
                            else
                            {
                                $edit_key = $this->edit_key ? $row[$this->edit_key] : $key;
                                $name = "__" . $this->entity . "_edit${index}_" . $edit_key;
                                $link = "javascript:button('$name')";
                            }
                        }
						else
						{
							$value = '<span id="' . $name . '">' .$value . '</span>';
						}
                    }
                    else if ($column["type"] == COLUMN_TYPE_TEXT)
                    {
                        $name = "__" . $this->entity . "_" . $column["name"] . "_" . $key;
						$value = "";

                        if (isset($column["values"][$key]))
                        {
                            if ($column["flags"] & COLUMN_UNDERSTAND_HTML)
                            {
								$value = $column["values"][$key];
                            }
                            else
                            {
                                $value = htmlspecialchars($column["values"][$key]);
                            }

                            if ($column["flags"] & COLUMN_BREAK)
                            {
                                $value = nl2br($value);
                            }

                        }
						$value = '<span id="' . $name . '">' .$value . '</span>';
                    }
                    else if ($column["type"] == COLUMN_TYPE_LIST)
                    {
						
						if(is_array($column["sql"]))
						{

							$name = "__" . $this->entity . "_" . $column["name"] . "_" . $key;
							$value1 = "";

							
							if (isset($column["values"][$key]))
							{
								$value1 = htmlspecialchars($column["values"][$key]);
							}

							$value = "<select name='" . $name . "' id='" . $name . "'>";
							$value.= "<option value='0'></option>";
							foreach($column["sql"] as $akey=>$acontent)
							{
								if($akey == $value1)
								{
									$value.= "<option value='" .$akey . "' selected='selected'>" . $acontent . "</option>";
								}
								else
								{
									$value.= "<option value='" . $akey . "'>" . $acontent . "</option>";
								}
							}

							$value.= "</select>";
						}
						else
						{
							if(count($column["dynamic_sql_array"]) > 0)
							{
								$sql_l = $column["dynamic_sql_array"][$key];
							}
							else
							{
								$sql_l = $column["sql"];
							}
							
							$res_l = mysql_query($sql_l) or dberror($sql_l);


							$name = "__" . $this->entity . "_" . $column["name"] . "_" . $key;
							$value1 = "";

							
							if (isset($column["values"][$key]))
							{
								$value1 = htmlspecialchars($column["values"][$key]);
							}


							$value = "<select name='" . $name . "'>";
							$value.= "<option value='0'></option>";
							while ($row_l = mysql_fetch_row($res_l))
							{
								if($row_l[0] == $value1)
								{
									$value.= "<option value='" . $row_l[0] . "' selected='selected'>" . $row_l[1] . "</option>";
								}
								else
								{
									$value.= "<option value='" . $row_l[0] . "'>" . $row_l[1] . "</option>";
								}
							}

							$value.= "</select>";
						}
                    }
                    else if ($column["type"] == COLUMN_TYPE_EDIT)
                    {
                        $name = "__" . $this->entity . "_" . $column["name"] . "_" . $key;
                        $value = "";

                        if (isset($column["values"][$key]))
                        {
                            $value = htmlspecialchars($column["values"][$key]);
                        }

						if ($column["flags"] & DISABLED)
						{
	                        $disabled = " style=\"border:0px;background:#EEEEEE;\" readonly=\"readonly\" ";
							$value = "<input type=\"text\" " . $disabled . "name=\"$name\" id=\"$name\" value=\"$value\" size=\"" . $column["width"] . "\" />";
						}
						else
						{
							$value = "<input type=\"text\" name=\"$name\" id=\"$name\" value=\"$value\" size=\"" . $column["width"] . "\" />";
						}

                    }
					else if ($column["type"] == COLUMN_TYPE_NUMBER_EDIT)
                    {
                        $name = "__" . $this->entity . "_" . $column["name"] . "_" . $key;
                        $value = "";

                        if (isset($column["values"][$key]))
                        {
                            $value = htmlspecialchars($column["values"][$key]);
                        }

                        $value = "<input style=\"text-align:right;\" type=\"text\" name=\"$name\" id=\"$name\" value=\"$value\" size=\"" . $column["width"] . "\" />";

                    }
					else if ($column["type"] == COLUMN_TYPE_DATE_EDIT)
                    {
                        $name = "__" . $this->entity . "_" . $column["name"] . "_" . $key;
                        $value = "";

                        if (isset($column["values"][$key]))
                        {
                            $value = to_system_date($column["values"][$key]);
                        }

                        $value = "<input type=\"text\" name=\"$name\" value=\"$value\" size=\"" . $column["width"] . "\" />";
                    }
                    else if ($column["type"] == COLUMN_TYPE_CHECKBOX)
                    {
                        $name = "__" . $this->entity . "_" . $column["name"] . "_" . $key;
                        $value = "0";

                        if (isset($column["values"][$key]))
                        {
                            $value = $column["values"][$key];
                        }
        
                        if($value == 1)
                        {
                            $value = "<input type=\"checkbox\" name=\"$name\" id=\"$name\" value=\"1\"" . " checked='checked' />";
                        }
                        else
                        {
                            $value = "<input type=\"checkbox\" name=\"$name\" id=\"$name\" value=\"1\"" . " />";
                        }
						
						if (isset($column["checkbox_caption"]))
                        {
							$value .= " " . $column["checkbox_caption"];
						}

                    }
                    else if ($column["type"] == COLUMN_TYPE_LABEL)
                    {
                        $name = "__" . $this->entity . "_" . $column["name"] . "_" . $key;

                        if (isset($column["values"][$key]))
                        {
                            $value = htmlspecialchars($column["values"][$key]);
                        }
        
                        $value = "<label for=\"$name\">" . $value . "</label>";

                    }
                    else if ($column["type"] == COLUMN_TYPE_IMAGE)
                    {

                        $value = "";

                        if (isset($column["values"][$key]))
                        {
                            $path = combine_paths($_SERVER["DOCUMENT_ROOT"], $column["values"][$key]);
                            $size = getimagesize($path);

                            $value = "<img src=\"" . $column["values"][$key] . "\" " . $size[3] . " alt=\"\" />";
                        }

                    }
                    // new by neo
                    else if ($column["type"] == COLUMN_TYPE_BUTTON)
                    {
                        $value = $column['name'];
                        $edit_key = $this->edit_key ? $row[$this->edit_key] : $key;
                        $name = "__" . $this->entity . "_edit${index}_" . $edit_key;
                        $link = "javascript:button('$name')";
                    }
                    else
                    {
                        error("Invalid column type " . $column["type"] . " in column \"" . $column["name"] . "\".");
                    }
                    if ($target)
                    {
                        $target = " target=\"" . $target . "\"";
                    }

                    echo "<td class=\"$class\"${align}${NOWRAP}>";
                    if ($link) {
						
						if(isset($data_artibute) and $data_artibute)
						{
							$link = str_replace('mvclink:', '', $link);
							echo "<a href=\"$link\" $data_artibute>";
						}
						else
						{
							echo "<a href=\"$link\"$target>";
						}
					}
                    
                    if ($column["flags"] & COLUMN_HILITE)
                    {
                        echo "<font color = '#FF0000'>" . $value . "</font>";
                    }
                    else
                    {
                        echo $value;
                    }
                    if ($link) echo "</a>";
                    echo "</td>";
                }


                if ($has_buttons)
                {
                    $buttons = array();

                    if ($this->sorter_field)
                    {
                        $name = "__" . $this->entity . "_up_" . $key;
                        $buttons[] = "<a href=\"javascript:button('$name')\">Up</a>";

                        $name = "__" . $this->entity . "_down_" . $key;
                        $buttons[] = "<a href=\"javascript:button('$name')\">Down</a>";
                    }

                    if ($this->delete_button)
                    {
                        $name = "__" . $this->entity . "_delete_" . $key;
                        $buttons[] = "<a href=\"javascript:button('$name')\">" . htmlspecialchars($this->delete_button) . "</a>";
                    }

                    echo "<td></td>";
                    echo "<td class=\"list_text_odd\" nowrap='nowrap'>", join(" | ", $buttons), "</td>";
                }

                echo "</tr>";
                $line++;
            }

            if ($group)
            {
                $this->draw_group_footer($group, $has_buttons);
            }

            if ($this->flags & LIST_HAS_FOOTER)
            {
                $this->draw_line();

                echo "<tr valign=\"top\">";

                if ($this->check_table)
                {
                    echo "<td colspan=\"2\">";
                    echo "&nbsp;";
                    echo "</td>";
                }

                for ($index = 0; $index < count($this->columns); $index++)
                {
                    $column = $this->columns[$index];
					$name = "__footer_" . $this->columns[$index]["name"];

                    if ($column["flags"] & COLUMN_HIDDEN)
                    {
                        continue;
                    }

                    $align = $this->make_align($column);
                    $NOWRAP = $column["flags"] & COLUMN_NO_WRAP ? " nowrap='nowrap'" : "";

                    if ($index > 0)
                    {
                        echo "<td class=\"list_footer\">";
                        echo "&nbsp;";
                        echo "</td>";
                    }

                    echo "<td class=\"list_footer\"${align}${NOWRAP}>";
                    //echo nl2br(htmlspecialchars($column["footer"]));
					echo '<span id="' . $name . '">' . $column["footer"] . '</span>';
                    echo "</td>";
                }

                if ($has_buttons)
                {
                    echo "<td></td>";
                    echo "<td></td>";
                }

                echo "</tr>";
            }

            $this->draw_line();
        }

        // Display number of records

		if ($this->flags & LIST_SHOW_COUNT)
        {
            if (mysql_num_rows($res) == 0)
            {
                //$count = "No records";
				$count = "";
			
            }
            else if (mysql_num_rows($res) == 1)
            {
                $count = "1 record";
            }
            else
            {
                $count = mysql_num_rows($res) . " records";
            }
            
			if($count != "") {
				echo "<tr>";
				echo "<td colspan=\"", $this->colspan, "\" height=\"10\">";
				echo "</td>";
				echo "</tr>";

				echo "<tr>";
				echo "<td colspan=\"", $this->colspan, "\" class=\"small\">";
				echo htmlspecialchars($count);
				echo "</td>";
				echo "</tr>";
			}
        }

        if (mysql_num_rows($res) || count($this->buttons))
        {
            echo "</table>";
        }

        //echo "</p>";

        return mysql_num_rows($res);
    }

    function draw_line()
    {
        $colspan = $this->colspan;

        if ($this->sorter_field || $this->delete_button)
        {
            $colspan -= 2;
        }

        echo "<tr>";
        echo "<td colspan=\"", $colspan, "\" bgcolor=\"#CCCCCC\">";
        echo "<img src=\"/pictures/spacer.gif\" width=\"1\" height=\"1\" alt=\"\" />";
        echo "</td>";

        if ($this->sorter_field || $this->delete_button)
        {
            echo "<td height=\"1\"></td>";
            echo "<td height=\"1\"></td>";
        }

        echo "</tr>";
    }

    function draw_gap()
    {
        echo "<tr>";
        echo "<td colspan=\"", $this->colspan, "\" class=\"list_footer\" height=\"10\"></td>";
        echo "</tr>";
    }

    function draw_group_footer($group, $has_buttons)
    {
        
		$draw_footer = false;

        foreach ($this->columns as $column)
        {
			if($column['type'] != COLUMN_TYPE_BUTTON)
			{
				foreach (array_keys($column["footers"]) as $key)
				{
					if ($key == $group)
					{
						$draw_footer = true;
						break;
					}
				}

				if ($draw_footer)
				{
					break;
				}
			}
        }

        if ($draw_footer)
        {
            $this->draw_line();

            echo "<tr valign=\"top\">";

            if ($this->check_table)
            {
                echo "<td class=\"list_footer\" colspan=\"2\">";
                echo "&nbsp;";
                echo "</td>";
            }

            for ($index = 0; $index < count($this->columns); $index++)
            {
                $column = $this->columns[$index];

                if ($index > 0)
                {
                    echo "<td class=\"list_footer\">";
                    echo "&nbsp;";
                    echo "</td>";
                }

                $align = $this->make_align($column);
                $nowrap = $column["flags"] & COLUMN_NO_WRAP ? " nowrap='nowrap'" : "";

                echo "<td class=\"list_footer\"${align}${nowrap}>";

                if (isset($column["footers"][$group]))
                {
                    //echo htmlspecialchars($column["footers"][$group]);
					echo $column["footers"][$group];
                }
                else
                {
                    echo "&nbsp;";
                }

                echo "</td>";
            }

            if ($has_buttons)
            {
                echo "<td></td>";
                echo "<td></td>";
            }

            echo "</tr>";
            $this->draw_gap();
        }
    }

    function adjust_priority($key, $direction)
    {
        if ($direction == LIST_MOVE_UP)
        {
            $operator = "<";
            $direction = "desc";
        }
        else
        {
            $operator = ">";
            $direction = "asc";
        }

        $sql = "select " . $this->sorter_field . " " .
               "from " . $this->table . " " .
               "where " . $this->table_key . " = " . $key;

        $res = mysql_query($sql) or dberror($sql);
        $row = mysql_fetch_row($res);
        $priority = $row[0];

        if ($this->filter)
        {
            $filter = "(" . $this->filter . ") and (" . $this->sorter_field . " $operator " . $priority . ")";
        }
        else
        {
            $filter = $this->sorter_field . " $operator " . $priority;
        }

        $sql = "select " . $this->table_key . ", " . $this->sorter_field . " " .
               "from " . $this->table . " " .
               "where " . $filter . " " .
               "order by " . $this->sorter_field . " $direction " .
               "limit 1";

        $res = mysql_query($sql) or dberror($sql);
        $row = mysql_fetch_row($res);
        $key2 = $row[0];
        $priority2 = $row[1];

        if ($key2)
        {
            $sql = "update " . $this->table . " " .
                   "set " . $this->sorter_field . " = " . $priority2 . " " .
                   "where " . $this->table_key . " = " . $key;

            mysql_query($sql) or dberror($sql);

            $sql = "update " . $this->table . " " .
                   "set " . $this->sorter_field . " = " . $priority . " " .
                   "where " . $this->table_key . " = " . $key2;

            mysql_query($sql) or dberror($sql);           
        }
    }

    function expand_fields($res, $row, $view)
    {
        $match = array();
        $replace = array();

        for ($i = 0; $i < mysql_num_fields($res); $i++)
        {
            $field = mysql_field_name($res, $i);
            $match[] = "/{" . $field . "}/i";
            $replace[] = $row[$field];
        }

        return preg_replace($match, $replace, $view);
    }

    function make_align($column)
    {
        if ($column["flags"] & COLUMN_ALIGN_RIGHT)
        {
            return " align=\"right\"";
        }
        else if ($column["flags"] & COLUMN_ALIGN_CENTER)
        {
            return " align=\"center\"";
        }
        else
        {
            return "";
        }
    }
}

/********************************************************************
    FilterAction class
*********************************************************************/

class FilterAction
{
    var $view;
    var $entity;
    var $items;

    function FilterAction($view, $entity)
    {
        $this->view = $view;
        $this->entity = $entity;
        $this->items = array();
    }

    function add_item($caption, $field, $altfield, $type, $sql)
    {
        $this->items[] = array("caption" => $caption,
                               "field" => $field,
                               "altfield" => $altfield,
                               "type" => $type,
                               "sql" => $sql);
    }
}

?>