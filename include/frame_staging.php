<?php
/********************************************************************

    frame.php

    Performs global initialization and includes other modules.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-28
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-11-25
    Version:        1.1.4
    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);


require_once("check_url_parameters.php");

define("BRAND", "OMEGA");
define("APPLICATION_NAME", "OMEGA Retail Net");
define("MAIL_SUBJECT_PREFIX", "OMEGA retail net");
define("APPLICATION_URL", "http://sgr-retailnet-sandbox.swatchgroup.net/");
define("APPLICATION_URLPART", "sgr-retailnet-sandbox.swatchgroup");
define("MAPS_HOST", "maps.google.com");
define("STATIC_MAPS_HOST", "http://maps.googleapis.com/maps/api/staticmap");
define("BRAND_WEBSITE", "www.omegawatches.com");



/********************************************************************
    Developper Vars
*********************************************************************/


global $senmail_activated; // true to tell the system to send mails
if (preg_match("/" . APPLICATION_URLPART . "/", $_SERVER["HTTP_HOST"]))
{
	$senmail_activated = false;
}
else
{
	$senmail_activated = false;
}
ini_set("include_path", "./");

/********************************************************************
    Pre-Initialization
*********************************************************************/
global $start_time;
$start_time = time();

//ini_set("magic_quotes_gpc", 0);
//ini_set("magic_quotes_runtime", 0);
//set_magic_quotes_runtime(false);

if (!isset($_SERVER))
{
    $_SERVER = $_SERVER;
}

if (!isset($_REQUEST))
{
    $_REQUEST = array_merge($HTTP_GET_VARS, $HTTP_POST_VARS, $HTTP_POST_FILES);
}
else
{
    $_REQUEST = array_merge($_REQUEST, $_FILES);
}

ini_set("include_path", ini_get("include_path") . ":" . $_SERVER["DOCUMENT_ROOT"] . "/include");

/********************************************************************
    Includes
*********************************************************************/


require_once "util.php";
require_once "db.php";
require_once "page.php";
require_once "popup_page.php";
require_once "form.php";
require_once "list.php";
require_once "mail.php";
require_once "phpmailer/class.phpmailer.php";

/********************************************************************
    Initialization
*********************************************************************/

// Determine browser capabilities
/*
$bad_browser = preg_match("/^Mozilla\/1/i", $_SERVER["HTTP_USER_AGENT"])
    || preg_match("/^Mozilla\/2/i", $_SERVER["HTTP_USER_AGENT"])
    || preg_match("/^Mozilla\/3/i", $_SERVER["HTTP_USER_AGENT"])
    || preg_match("/^Mozilla\/4/i", $_SERVER["HTTP_USER_AGENT"]);

$msie_browser = preg_match("/MSIE (\d+\.\d+)/i", $_SERVER["HTTP_USER_AGENT"], $matches)
    && $matches[1] >= 5.0
    && !preg_match("/Opera/i", $_SERVER["HTTP_USER_AGENT"]);

*/


// Open database

$context = "";

ini_set("default_charset", "utf-8");
if (preg_match("/" . APPLICATION_URLPART . "/", $_SERVER["HTTP_HOST"]))
{

	$db = mysql_connect("localhost", "retailnet", "JI_6hoNs+eg+");
    $result = mysql_select_db("db_retailnet", $db);

	if(!$result) {
		redirect("/service_not_available.php");
	}
	
	define("RETAILNET_SERVER", "localhost");
	define("RETAILNET_DB", "db_retailnet");
	define("RETAILNET_USER", "retailnet");
	define("RETAILNET_PASSWORD", "6n-JLla-3Vqw");

	define("STORE_LOCATOR_SERVER", "localhost");
	define("STORE_LOCATOR_DB", "db_storelocator");
	define("STORE_LOCATOR_USER", "retailnet");
	define("STORE_LOCATOR_PASSWORD", "6n-JLla-3Vqw");


	//proxy server Swatchgroup
	define("PROXY_SERVER", "its-ch-webproxy.swatchgroup.net:8084");
	$auth = base64_encode('HofmannA:Welcome2013'); 
	$opts	= array(
		'http'	=> array(
			'proxy'				=> 'its-ch-webproxy.swatchgroup.net:8084',
			'request_fulluri'	=> true,
			'header'=> array("Proxy-Authorization: Basic $auth", "Authorization: Basic $auth")
		)
	);

	$context = stream_context_create($opts);
}
else
{
	$db = mysql_connect("localhost", "root", "root");
    $result = mysql_select_db("omega_db_retailnet", $db);

	if(!$result) {
		redirect("/service_not_available.php");
	}

	define("RETAILNET_SERVER", "localhost");
	define("RETAILNET_DB", "omega_db_retailnet");
	define("RETAILNET_USER", "root");
	define("RETAILNET_PASSWORD", "root");

	define("STORE_LOCATOR_SERVER", "localhost");
	define("STORE_LOCATOR_DB", "omega_db_storelocator");
	define("STORE_LOCATOR_USER", "root");
	define("STORE_LOCATOR_PASSWORD", "root");

	define("PROXY_SERVER", "");
}

mysql_query( "SET NAMES 'utf8'");
mysql_query( "set sql_mode = ''");

date_default_timezone_set ( "Europe/Zurich" );


if (preg_match("/" . APPLICATION_URLPART . "/", $_SERVER["HTTP_HOST"]))
{
	define("GOOGLE_API_KEY", "ABQIAAAANMWovGK6w72eKOOPCsyWJBQYbBwJ19z5CTqfy4EzqmUXT4RmWRSBYDSkI9_RpnvDmY499rI4YcKymg");

	define("TMP_FILE_DIR_ABSOLUTE", "/data/www/retailnet/htdocs/files/tmp/");
	define("TMP_FILE_DIR_RELATIVE", "../files/tmp/");

		
}
elseif (preg_match("/retailnet.mpx02/", $_SERVER["HTTP_HOST"]))
{
	define("GOOGLE_API_KEY", "AIzaSyABVUjzkmGcInjeeoE-iMnFuTgsvqheJHU");
	define("TMP_FILE_DIR_ABSOLUTE","/var/www/vhosts/retailnet.mpx02.ch/htdocs/files/tmp/");
	define("TMP_FILE_DIR_RELATIVE", "../files/tmp/");
}
else
{
	define("GOOGLE_API_KEY", "ABQIAAAANMWovGK6w72eKOOPCsyWJBTIE1u1rkD0AV69z6wMUCuqr_LikRRrmkhJgaCxFROqt4Pkuys8jceQ4g");
	define("TMP_FILE_DIR_ABSOLUTE", "c:/webs/sg_sb_retailnet/tmp/");
	define("TMP_FILE_DIR_RELATIVE", "../tmp/");
}

// Prepare REQUEST variable

if (isset($_COOKIE))
{
    foreach (array_keys($_COOKIE) as $cookie)
    {
        unset($_REQUEST[$cookie]);
    }
}

// Start session
session_name("retailnet");
if (!isset($SUPPRESS_HEADERS))
{
	session_start();
/*
    if (!isset($_SESSION))
    {
        $_SESSION = &$HTTP_SESSION_VARS;
    }*/
}



if(isset($_GET)) {
	foreach($_GET as $key => $value) {
		$myvar = $key;
		$$myvar = empty($value) ? "" : trim($value);
	}
}
/*
if(isset($_POST)) {
	foreach($_POST as $key => $value) {
		$myvar = $key;
		$$myvar = empty($value) ? "" : trim($value);
	}
}
*/

// Register commonly used parameters

register_param("id");
register_param("order");
register_param("filter");


require_once("check_access.php");


?>