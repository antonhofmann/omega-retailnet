<?php
/********************************************************************

    page.php

    Classes and functions to render pages.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-31
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-8--26
    Version:        1.1.4

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

/********************************************************************
    Constants
*********************************************************************/
define("PAGE_LAST", 1);
define("PAGE_POPUP", 2);

/********************************************************************
    Site navigation structure
*********************************************************************/

$__navigation_admin = array(
    array("name"       => "welcome",
          "url"        => "welcome.php",
          "title"      => "Admin",
          "hide"       => true,
          "permission" => ""),

    array("name"       => "login",
          "url"        => "login.php",
          "title"      => "Login",
          "hide"       => true,
          "nomenu"     => true,
          "permission" => ""),
    array("name"       => "noaccess",
          "url"        => "noaccess.php",
          "title"      => "No Access",
          "hide"       => true,
          "nomenu"     => true,
          "permission" => ""),
	array("name"       => "catalog",
          "url"        => "catalog.php",
          "title"      => "Catalog",
          "permission" => "can_browse_catalog_in_admin"),
	array("name"       => "product_lines",
          "url"        => "product_lines.php",
          "title"      => "Product Lines",
          "parent"     => "catalog",
          "permission" => "can_edit_catalog"),
    array("name"       => "product_line_subclasses",
          "url"        => "product_line_subclasses.php",
          "title"      => "Product Line Subclasses",
          "parent"     => "catalog",
		  "permission" => "can_edit_catalog"),
	array("name"       => "categories",
          "url"        => "categories.php",
          "title"      => "Categories",
          "parent"     => "catalog",
          "permission" => "can_edit_catalog"),
    array("name"       => "categories_old",
          "url"        => "categories_old.php",
          "title"      => "Categories not in Use",
          "parent"     => "catalog",
          "permission" => "can_edit_catalog"),
    array("name"       => "item_groups",
          "url"        => "item_groups.php",
          "title"      => "Item Groups",
          "parent"     => "catalog",
          "permission" => "can_edit_catalog"),
	array("name"       => "costmonitoringgroups",
          "url"        => "costmonitoringgroups.php",
          "title"      => "Cost Monitoring Groups",
          "parent"     => "catalog",
          "permission" => "can_edit_catalog"),
    array("name"       => "item_categories",
          "url"        => "item_categories.php",
          "title"      => "Item Categories",
          "parent"     => "catalog",
          "permission" => "can_edit_catalog"),
    array("name"       => "items",
          "url"        => "items.php",
          "title"      => "Items",
          "parent"     => "catalog",
          "permission" => "can_browse_catalog_in_admin"),
    array("name"       => "items_niu",
          "url"        => "items_niu.php",
          "title"      => "Items not in Use",
          "parent"     => "catalog",
          "permission" => "can_edit_catalog"),
	array("name"       => "services",
          "url"        => "services.php",
          "title"      => "Services",
          "parent"     => "catalog",
          "permission" => "can_edit_catalog"),
    array("name"       => "design_objective_groups",
          "url"        => "design_objective_groups.php",
          "title"      => "Design Objective Groups",
          "parent"     => "catalog",
          "permission" => "can_edit_catalog"),
    array("name"       => "design_objectives",
          "url"        => "design_objectives.php",
          "title"      => "Design Objectives",
          "parent"     => "catalog",
          "permission" => "can_edit_catalog"),

    array("name"       => "people",
          "url"        => "people.php",
          "title"      => "People",
          "permission" => "can_edit_catalog"),
    array("name"       => "addresses",
          "url"        => "addresses.php",
          "title"      => "Retail Net Addresses",
          "parent"     => "people",
          "permission" => "can_edit_catalog"),
	array("name"       => "3rdparty_addresses",
          "url"        => "addresses3rd.php",
          "title"      => "3rd Party Addresses",
          "parent"     => "people",
          "permission" => "can_edit_catalog"),
    array("name"       => "standard_delivery_addresses",
          "url"        => "standard_delivery_addresses.php",
          "title"      => "Standrad Delivery Addresses",
          "parent"     => "people",
          "permission" => "can_edit_catalog"),
	array("name"       => "users",
          "url"        => "users.php",
          "title"      => "Users Active",
          "parent"     => "people",
          "permission" => "can_edit_catalog"),
	array("name"       => "supervisingteam",
          "url"        => "supervisors.php",
          "title"      => "Retail Supervising Team",
          "parent"     => "people",
          "permission" => "can_edit_catalog"),
    array("name"       => "roles",
          "url"        => "roles.php",
          "title"      => "Roles",
          "parent"     => "people",
          "permission" => "can_edit_catalog"),
   array("name"       => "userroles",
          "url"        => "userroles.php",
          "title"      => "Userroles",
          "parent"     => "people",
          "permission" => "can_edit_catalog"),
    array("name"       => "Replace User",
          "url"        => "replace_user.php",
          "title"      => "Replace User",
          "parent"     => "people",
          "permission" => "can_edit_catalog"), 
	array("name"       => "dummy_inactive",
          "url"        => "",
          "title"      => "",
          "parent"     => "people",
          "permission" => "can_edit_catalog"),
	array("name"       => "addresses_inactive",
          "url"        => "addresses_inactive.php",
          "title"      => "Retail Net Addresses Inactive",
          "parent"     => "people",
          "permission" => "can_edit_catalog"),
	array("name"       => "3rdparty_addresses_inactive",
          "url"        => "addresses3rd_inactive.php",
          "title"      => "3rd Party Addresses Inactive",
          "parent"     => "people",
          "permission" => "can_edit_catalog"),
	array("name"       => "users_inactive",
          "url"        => "users_inactive.php",
          "title"      => "Users Inactive",
          "parent"     => "people",
          "permission" => "can_edit_catalog"),
	array("name"       => "dummy",
          "url"        => "",
          "title"      => "",
          "parent"     => "people",
          "permission" => "can_edit_catalog"),
	array("name"       => "print_roles",
          "url"        => "roles_users_xls.php",
          "title"      => "Print User Roles",
          "parent"     => "people",
          "permission" => "can_edit_catalog"),
	array("name"       => "print_permissions",
          "url"        => "roles_permissions_xls.php",
          "title"      => "Print Permissions",
          "parent"     => "people",
          "permission" => "can_edit_catalog"),
    array("name"       => "locales",
          "url"        => "locales.php",
          "title"      => "Locales",
          "permission" => "can_edit_catalog"),
    array("name"       => "regions",
          "url"        => "regions.php",
          "title"      => "Supplying Regions",
          "parent"     => "locales",
          "permission" => "can_edit_catalog"),
    array("name"       => "salesregions",
          "url"        => "salesregions.php",
          "title"      => "Geographical Regions",
          "parent"     => "locales",
          "permission" => "can_edit_catalog"),
	array("name"       => "countries",
          "url"        => "countries.php",
          "title"      => "Countries",
          "parent"     => "locales",
          "permission" => "can_edit_catalog"),
    array("name"       => "provinces",
          "url"        => "provinces.php",
          "title"      => "Provinces",
          "parent"     => "locales",
          "permission" => "can_edit_catalog"),
	array("name"       => "places",
          "url"        => "places.php",
          "title"      => "Cities",
          "parent"     => "locales",
          "permission" => "can_edit_catalog"),
    array("name"       => "currencies",
          "url"        => "currencies.php",
          "title"      => "Currencies",
          "parent"     => "locales",
          "permission" => "can_edit_catalog"),
	 array("name"       => "languages",
          "url"        => "languages.php",
          "title"      => "Languages",
          "parent"     => "locales",
          "permission" => "can_edit_catalog"),
	 array("name"       => "dummy",
		  "url"        => "",
		  "title"      => "",
		  "parent"     => "locales",
		  "permission" => "can_edit_catalog"),
	 array("name"       => "countryrepl",
		  "url"        => "replace_country.php",
		  "title"      => "Replace Country",
		  "parent"     => "locales",
		  "permission" => "can_edit_catalog"),
    array("name"       => "information",
          "url"        => "information.php",
          "title"      => "Information",
          "permission" => "can_edit_catalog"),
    array("name"       => "messages",
          "url"        => "messages.php",
          "title"      => "Messages",
          "parent"     => "information",
          "permission" => "can_edit_catalog"),
    array("name"       => "messages_emails_sent",
          "url"        => "messages_emails_sent.php",
          "title"      => "Messages - Emails sent",
          "parent"     => "information",
          "permission" => "can_edit_catalog"),
    array("name"       => "links",
          "url"        => "links.php",
          "title"      => "Downloads & Links",
          "parent"     => "information",
          "permission" => "can_edit_catalog"),
    array("name"       => "link_topics",
          "url"        => "link_topics.php",
          "title"      => "Download/Link Topics",
          "parent"     => "information",
          "permission" => "can_edit_catalog"),
	array("name"       => "link_categories",
          "url"        => "link_categories.php",
          "title"      => "Download/Link Categories",
          "parent"     => "information",
          "permission" => "can_edit_catalog"),
	array("name"       => "dummy",
          "url"        => "",
          "title"      => "",
          "parent"     => "information",
          "permission" => "can_edit_catalog"),
	array("name"       => "resetmailalerts",
          "url"        => "reset_mail_alerts.php",
          "title"      => "Reset Mailalerts",
          "parent"     => "information",
          "permission" => "can_edit_catalog"),
	array("name"       => "dummy",
          "url"        => "",
          "title"      => "",
          "parent"     => "information",
          "permission" => "can_edit_catalog"),
    array("name"       => "system",
          "url"        => "system.php",
          "title"      => "System",
          "permission" => "can_edit_catalog"),
	array("name"       => "product_line_pos_types",
          "url"        => "product_line_pos_types.php",
          "title"      => "POS Project Types",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
	array("name"       => "possubclasses",
          "url"        => "possubclasses.php",
          "title"      => "POS Subclasses",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
	array("name"       => "standardroles",
          "url"        => "pos_type_standardroles.php",
          "title"      => "POS Type Standard Roles",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
	array("name"       => "standarddesigncontractors",
          "url"        => "pos_type_standarddesigncontractors.php",
          "title"      => "Standard Design Contractors",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
	array("name"       => "projecttype_newproject_notifications",
          "url"        => "pos_type_new_project_notifications.php",
          "title"      => "Notifications for Submissions",
          "parent"     => "system",
          "permission" => "can_edit_catalog"), 
	array("name"       => "postype_notifications",
          "url"        => "pos_type_notifications.php",
          "title"      => "Project Notifications",
          "parent"     => "system",
          "permission" => "can_edit_catalog"), 
	array("name"       => "project_cost_types",
          "url"        => "project_legal_types.php",
          "title"      => "Legal Types Notifications",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
	array("name"       => "project_kinds",
          "url"        => "project_kinds.php",
          "title"      => "Project Kind Notifications",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
	array("name"       => "posdistributionchannels",
          "url"        => "standard_distribution_channels.php",
          "title"      => "Standard Distribution Channels",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
	array("name"       => "dummy",
          "url"        => "",
          "title"      => "",
          "parent"     => "system",
          "permission" => ""),
	array("name"       => "pcost_templates",
          "url"        => "pcost_templates.php",
          "title"      => "Project Cost Templates",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
	array("name"       => "pcost_groups",
          "url"        => "pcost_groups.php",
          "title"      => "Project Cost Groups",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
	array("name"       => "pcost_subgroups",
          "url"        => "pcost_subgroups.php",
          "title"      => "Project Cost Subgroups",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
	array("name"       => "dummy",
          "url"        => "",
          "title"      => "",
          "parent"     => "system",
          "permission" => ""),
	array("name"       => "strings",
          "url"        => "strings.php",
          "title"      => "Strings",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
    array("name"       => "file_types",
          "url"        => "file_types.php",
          "title"      => "File Types",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
    array("name"       => "budget_positions",
          "url"        => "budget_positions.php",
          "title"      => "Exclusions And Notifications",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
    array("name"       => "order_states",
          "url"        => "order_states.php",
          "title"      => "Order States",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
    array("name"       => "taskworks",
          "url"        => "taskworks.php",
          "title"      => "Project State Standard Texts",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
    array("name"       => "dummy",
          "url"        => "",
          "title"      => "",
          "parent"     => "system",
          "permission" => ""),
    array("name"       => "account_numbers",
          "url"        => "account_numbers.php",
          "title"      => "Account Numbers",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
    array("name"       => "business_units",
          "url"        => "business_units.php",
          "title"      => "Business Units",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
    array("name"       => "project_budget_approvals",
          "url"        => "project_budget_approvals.php",
          "title"      => "Budget Approval",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
    array("name"       => "closing_dates",
          "url"        => "closing_dates.php",
          "title"      => "Closing Dates",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
    array("name"       => "cost_centers",
          "url"        => "cost_centers.php",
          "title"      => "Cost Centers",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
    array("name"       => "ps_product_lines",
          "url"        => "ps_product_lines.php",
          "title"      => "Project Sheet Product Lines",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
    array("name"       => "ps_spent_fors",
          "url"        => "ps_spent_fors.php",
          "title"      => "Project Sheet Spent For",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
    array("name"       => "dummy",
          "url"        => "",
          "title"      => "",
          "parent"     => "system",
          "permission" => ""),
	array("name"       => "milestones",
          "url"        => "milestones.php",
          "title"      => "Project Milestones",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
	array("name"       => "mail_alert_types",
          "url"        => "mailalerts.php",
          "title"      => "Mail Alerts",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
    array("name"       => "eventmails",
          "url"        => "eventmails.php",
          "title"      => "Eventmails",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
	array("name"       => "reset_project_numbers",
      "url"        => "reset_project_numbers.php",
      "title"      => "Reset Project Numbers",
      "parent"     => "system",
      "permission" => "can_edit_catalog"),
	array("name"       => "order_addresses",
          "url"        => "warehouse_addresses.php",
          "title"      => "Clean Supplier's Warehouse Pick Up Addresses",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
     /*
	 array("name"       => "order_addresses1",
          "url"        => "delivery_addresses.php",
          "title"      => "Clean Delivery Addresses",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
     array("name"       => "orders",
          "url"        => "invoice_addresses.php",
          "title"      => "Clean Invoice Addresses",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
	 array("name"       => "dummy",
	      "url"        => "",
          "title"      => "",
          "parent"     => "system",
          "permission" => ""),
	array("name"       => "house_keeping",
          "url"        => "house_keeping.php",
          "title"      => "House Keeping",
          "parent"     => "system",
          "permission" => "has_access_to_housekeeping"),
	*/
	array("name"       => "security",
          "url"        => "security.php",
          "title"      => "Security",
          "permission" => "can_edit_catalog"),
	 array("name"       => "lockedips",
          "url"        => "security.php",
          "title"      => "Locked IPs",
          "parent"     => "security",
          "permission" => "can_edit_catalog"),
	array("name"       => "login_failures",
          "url"        => "security_loginfailures.php",
          "title"      => "Login Failures",
          "parent"     => "security",
          "permission" => "can_edit_catalog"),
	array("name"       => "excluded_ips",
          "url"        => "security_excludeips.php",
          "title"      => "IP Exceptions",
          "parent"     => "security",
          "permission" => "can_unlock_ips"),
	array("name"       => "documentation",
          "url"        => "documentation.php",
          "title"      => "Documentation",
          "nomenu"     => true,
          "permission" => "can_edit_catalog"),
	 array("name"       => "mailalerts",
          "url"        => "doc_mailalerts.php",
          "title"      => "Mailalerts",
          "parent"     => "documentation",
          "permission" => "can_edit_catalog"),
	array("name"       => "logoff",
          "url"        => "logoff.php",
          "title"      => "Logoff",
          "nomenu"     => true,
          "permission" => ""),
);


$__navigation_user = array(
    array("name"       => "welcome",
          "url"        => "welcome.php",
          "title"      => "",
          "hide"       => true,
          "permission" => ""),

    array("name"       => "login",
          "url"        => "login.php",
          "title"      => "Login",
          "hide"       => true,
          "nomenu"     => true,
          "permission" => ""),

    array("name"       => "noaccess",
          "url"        => "noaccess.php",
          "title"      => "No Access",
          "hide"       => true,
          "nomenu"     => true,
          "permission" => ""),
	/*
    array("name"       => "catalog",
          "url"        => "catalog.php",
          "title"      => "Catalog",
          "permission" => "can_view_catalog"),

    array("name"       => "orders",
          "url"        => "orders.php",
          "title"      => "Orders",
          "permission" => "can_view_orders"),
	*/
    array("name"       => "projects",
          "url"        => "projects.php",
          "title"      => "Projects",
          "permission" => "can_view_projects"),

    array("name"       => "delivery",
          "url"        => "delivery.php",
          "title"      => "Delivery",
          "permission" => "can_view_delivery"),

    /*
	array("name"       => "stock",
          "url"        => "stock.php",
          "title"      => "Stock",
          "permission" => "can_view_stock"),
    */
    array("name"       => "tasks",
          "url"        => "tasks.php",
          "title"      => "Tasks",
          "permission" => "can_view_tasks"),

    array("name"       => "links",
          "url"        => "links.php",
          "title"      => "Links",
          "permission" => "can_view_links"),

    array("name"       => "logoff",
          "url"        => "logoff.php",
          "title"      => "Logoff",
          "nomenu"     => true,
          "permission" => ""),
);



$__navigation_mis = array(
    array("name"       => "queries",
          "url"        => "welcome.php",
          "title"      => "MIS",
          "hide"       => true,
          "permission" => ""),

    array("name"       => "login",
          "url"        => "login.php",
          "title"      => "MIS: Login",
          "hide"       => true,
          "nomenu"     => true,
          "permission" => ""),

    array("name"       => "noaccess",
          "url"        => "noaccess.php",
          "title"      => "MIS: No Access",
          "hide"       => true,
          "nomenu"     => true,
          "permission" => ""),

    array("name"       => "logoff",
          "url"        => "logoff.php",
          "title"      => "MIS: Logoff",
          "nomenu"     => true,
          "permission" => ""),
);


$__navigation_pos = array(
    
	array("name"       => "posindex",
		  "url"        => "welcome.php",
		  "title"      => "POS",
		  "permission" => "can_use_posindex"),
	 array("name"       => "poscompanies",
          "url"        => "poscompanies_preselect.php",
          "title"      => "Companies",
          "parent"     => "posindex",
          "permission" => "has_access_to_companies"),
    array("name"       => "poscompanies_inactive",
          "url"        => "poscompanies_inactive.php",
          "title"      => "Companies Inactive",
          "parent"     => "posindex",
          "permission" => "can_administrate_posindex"),
	
	array("name"       => "posaddresses",
          "url"        => "posindex_preselect.php",
          "title"      => "POS Locations",
          "parent"     => "posindex",
          "permission" => "has_access_to_poslocations"),

	
	array("name"       => "queries",
          "url"        => "query_generator.php",
          "title"      => "Queries",
          "permission" => "can_query_posindex"),
	array("name"       => "query_poscompanies",
          "url"        => "query_poscompanies.php",
          "title"      => "Companies",
          "parent"     => "queries",
          "permission" => "can_view_posindex"),
	array("name"       => "query_posaddresses",
          "url"        => "query_posaddresses.php",
          "title"      => "Addresses",
          "parent"     => "queries",
          "permission" => "can_view_posindex"),
	array("name"       => "query_posdetails",
          "url"        => "query_posdetails.php",
          "title"      => "POS Details",
          "parent"     => "queries",
          "permission" => "can_view_posindex"),
	array("name"       => "query_generator",
          "url"        => "query_generator.php",
          "title"      => "Query Generator",
          "parent"     => "queries",
          "permission" => "can_view_posindex"),
	array("name"       => "system",
          "url"        => "system.php",
          "title"      => "System",
          "permission" => "can_administrate_posindex"),
	array("name"       => "posfilegroups",
          "url"        => "posfilegroups.php",
          "title"      => "File Groups",
          "parent"     => "system",
          "permission" => "can_administrate_posindex"),
	array("name"       => "posfiletypes",
          "url"        => "posfiletypes.php",
          "title"      => "File Types",
          "parent"     => "system",
          "permission" => "can_administrate_posindex"),
    /*
	array("name"       => "posequipmenttypes",
          "url"        => "posequipmenttypes.php",
          "title"      => "Equipment",
          "parent"     => "system",
          "permission" => "can_administrate_posindex"),
    */
	array("name"       => "posareatypes",
          "url"        => "posareatypes.php",
          "title"      => "Area Types",
          "parent"     => "system",
          "permission" => "can_administrate_posindex"),
	/*
	array("name"       => "posinvestmenttypes",
          "url"        => "posinvestmenttypes.php",
          "title"      => "Investment Types",
          "parent"     => "system",
          "permission" => "can_administrate_posindex"),
    */
	array("name"       => "posagreementtypes",
          "url"        => "posagreementtypes.php",
          "title"      => "Agreement Types",
          "parent"     => "system",
          "permission" => "can_administrate_posindex"),
	array("name"       => "posprojecttypes",
          "url"        => "posprojecttypes.php",
          "title"      => "POS Project Types",
          "parent"     => "system",
          "permission" => "can_administrate_posindex"),
	array("name"       => "posaddresses",
          "url"        => "",
          "title"      => "",
          "parent"     => "system",
          "permission" => "can_administrate_posindex"),
    array("name"       => "posdatacorrections",
          "url"        => "update_posaddress_checks.php",
          "title"      => "Provide POS Data for Corrections",
          "parent"     => "system",
          "permission" => "can_administrate_posindex"),
	array("name"       => "deletepos",
          "url"        => "deletepos_preselect.php",
          "title"      => "Delete POS Data",
          "parent"     => "system",
          "permission" => "can_administrate_posindex"),
	array("name"       => "posaddresses",
          "url"        => "",
          "title"      => "",
          "parent"     => "system",
          "permission" => "can_administrate_posindex"),
	array("name"       => "googlemaps2",
          "url"        => "_geoencoding2.php",
          "title"      => "Get Google Coordinates",
          "parent"     => "system",
          "permission" => "can_administrate_posindex"),
	array("name"       => "googlemaps",
          "url"        => "_geoencoding.php",
          "title"      => "Create Google Maps",
          "parent"     => "system",
          "permission" => "can_administrate_posindex"),
	array("name"       => "dataanalysis",
		  "url"        => "dataanalysis.php",
		  "title"      => "Data Analysis",
		  "parent"     => "system",
		  "permission" => "can_administrate_posindex"),
	array("name"       => "login",
          "url"        => "login.php",
          "title"      => "Login",
          "hide"       => true,
          "nomenu"     => true,
          "permission" => ""),
   
	/*
	array("name"       => "locales",
          "url"        => "locales.php",
          "title"      => "Locales",
          "permission" => "can_administrate_posindex"),
    array("name"       => "loc_languages",
          "url"        => "loc_languages.php",
          "title"      => "Languages",
          "parent"     => "locales",
          "permission" => "can_administrate_posindex"),
	array("name"       => "loc_countries",
          "url"        => "loc_countries.php",
          "title"      => "Countries",
          "parent"     => "locales",
          "permission" => "can_administrate_posindex"),
	array("name"       => "loc_provinces",
          "url"        => "loc_provinces.php",
          "title"      => "Provinces",
          "parent"     => "locales",
          "permission" => "can_administrate_posindex"),
	array("name"       => "loc_places",
          "url"        => "loc_places.php",
          "title"      => "Places",
          "parent"     => "locales",
          "permission" => "can_administrate_posindex"),
    array("name"       => "loc_posaddresses",
          "url"        => "loc_posaddresses.php",
          "title"      => "POS Locations",
          "parent"     => "locales",
          "permission" => "can_administrate_posindex"),
	array("name"       => "loc_dummy",
          "url"        => "",
          "title"      => "",
          "parent"     => "locales",
          "permission" => "can_administrate_posindex"),
	array("name"       => "loc_possubclasses",
          "url"        => "loc_possubclasses.php",
          "title"      => "POS Type Subclass",
          "parent"     => "locales",
          "permission" => "can_administrate_posindex"),
	array("name"       => "loc_posareatypes",
          "url"        => "loc_posareatypes.php",
          "title"      => "POS Area Types",
          "parent"     => "locales",
          "permission" => "can_administrate_posindex"),
	array("name"       => "loc_areaperception_types",
          "url"        => "loc_areaperception_types.php",
          "title"      => "Area Perception Types",
          "parent"     => "locales",
          "permission" => "can_administrate_posindex"),
    */
	array("name"       => "noaccess",
          "url"        => "noaccess.php",
          "title"      => "No Access",
          "hide"       => true,
          "nomenu"     => true,
          "permission" => ""),

    array("name"       => "logoff",
          "url"        => "logoff.php",
          "title"      => "Logoff",
          "nomenu"     => true,
          "permission" => ""),
);

$__navigation_archive = array(
    array("name"       => "login",
          "url"        => "login.php",
          "title"      => "Archive",
          "hide"       => true,
          "nomenu"     => true,
          "permission" => ""),
    
    /*
	array("name"       => "orders",
          "url"        => "orders.php",
          "title"      => "Orders",
          "nomenu"     => true,
          "permission" => "can_view_orders"),
    */
    array("name"       => "projects",
          "url"        => "projects.php",
          "title"      => "Projects",
          "nomenu"     => true,
          "permission" => "can_view_projects"),
);


$__navigation_stats = array(
    array("name"       => "traffic",
          "url"        => "welcome.php",
          "title"      => "Satistics",
          "hide"       => true,
          "permission" => ""),

    array("name"       => "login",
          "url"        => "login.php",
          "title"      => "Satistics: Login",
          "hide"       => true,
          "nomenu"     => true,
          "permission" => ""),

    array("name"       => "noaccess",
          "url"        => "noaccess.php",
          "title"      => "Satistics: No Access",
          "hide"       => true,
          "nomenu"     => true,
          "permission" => ""),

    array("name"       => "logoff",
          "url"        => "logoff.php",
          "title"      => "Satistics: Logoff",
          "nomenu"     => true,
          "permission" => ""),
);


$__navigation_issues = array(
    array("name"       => "issues",
          "url"        => "welcome.php",
          "title"      => "Issues",
          "hide"       => true,
          "permission" => ""),

    array("name"       => "login",
          "url"        => "login.php",
          "title"      => "Issues: Login",
          "hide"       => true,
          "nomenu"     => true,
          "permission" => ""),

    array("name"       => "noaccess",
          "url"        => "noaccess.php",
          "title"      => "Issues: No Access",
          "hide"       => true,
          "nomenu"     => true,
          "permission" => ""),

    array("name"       => "logoff",
          "url"        => "logoff.php",
          "title"      => "Issues: Logoff",
          "nomenu"     => true,
          "permission" => ""),
);


$__navigation_cer = array(
	array("name"       => "cer_projects",
          "url"        => "welcome.php",
          "title"      => "CER/AF",
          "permission" => "has_access_to_cer"),
	array("name"       => "cer_archive",
          "url"        => "projects_archive.php",
          "title"      => "Archive",
          "permission" => "has_access_to_cer_archive"),
	array("name"       => "cer_drafts",
          "url"        => "cer_drafts.php",
          "title"      => "Business Plan Drafts",
          "permission" => "has_access_to_cer_drafts"),
    array("name"       => "cer_benchmarks",
          "url"        => "cer_benchmarks.php",
          "title"      => "Benchmarks",
          "permission" => "has_access_to_cer_benchmarks"),
	array("name"       => "downloads",
          "url"        => "cer_downloads.php",
          "title"      => "Downloads",
          "permission" => "has_access_to_cer"),
	array("name"       => "system",
          "url"        => "system.php",
          "title"      => "System",
          "permission" => "has_full_access_to_cer"),
	array("name"       => "sg_brands",
          "url"        => "sg_brands.php",
          "title"      => "Swatch Group Brands",
          "parent"     => "system",
          "permission" => "can_edit_catalog"),
	array("name"       => "inflationrates",
          "url"        => "inflationrates.php",
          "title"      => "Inflation Rates",
          "parent"     => "system",
          "permission" => "has_full_access_to_cer"),
	array("name"       => "interestrates",
          "url"        => "interestrates.php",
          "title"      => "Interest Rates",
          "parent"     => "system",
          "permission" => "has_full_access_to_cer"),
	array("name"       => "standardparameters",
          "url"        => "standardparameters.php",
          "title"      => "Standard Parameters",
          "parent"     => "system",
          "permission" => "has_full_access_to_cer"),
	array("name"       => "posprojecttypes",
          "url"        => "posprojecttypes.php",
          "title"      => "POS Projecttypes",
          "parent"     => "system",
          "permission" => "has_full_access_to_cer"),
	array("name"       => "approvalnames",
          "url"        => "cer_approvalnames.php",
          "title"      => "CER Approval Names",
          "parent"     => "system",
          "permission" => "has_full_access_to_cer"),
	array("name"       => "cer_brands",
          "url"        => "cer_brands.php",
          "title"      => "Brands Involved",
          "parent"     => "system",
          "permission" => "has_full_access_to_cer"),
	array("name"       => "cer_additional_rental_cost_types",
          "url"        => "cer_additional_rental_cost_types.php",
          "title"      => "Additional Rental Cost types",
          "parent"     => "system",
          "permission" => "has_full_access_to_cer"),
	array("name"       => "login",
          "url"        => "login.php",
          "title"      => "Login",
          "hide"       => true,
          "nomenu"     => true,
          "permission" => ""),
    array("name"       => "noaccess",
          "url"        => "noaccess.php",
          "title"      => "No Access",
          "hide"       => true,
          "nomenu"     => true,
          "permission" => ""),
    array("name"       => "logoff",
          "url"        => "logoff.php",
          "title"      => "Logoff",
          "nomenu"     => true,
          "permission" => ""),
);


/********************************************************************
    Page class
*********************************************************************/

class Page
{
    var $section;
    var $area;
    var $navigation;
    var $title;
    var $actions;
	var $tabs;
	
    function Page($name = PAGE_LAST, $title = "")
    {
        global $_SERVER;
        global $__navigation_admin;
        global $__navigation_user;
        global $__navigation_mis;
		global $__navigation_pos;
        global $__navigation_archive;
        global $__navigation_stats;
        global $__navigation_issues;
		global $__navigation_cer;

        $this->title = $title;
        $this->actions = array();
        $this->section = "";

        // Determine area (admin or user)

        if (preg_match("/\/admin\//", $_SERVER["PHP_SELF"]))
        {
            $this->area = "admin";
            $this->navigation = &$__navigation_admin;
        }
        else if (preg_match("/\/user\//", $_SERVER["PHP_SELF"]))
        {
            $this->area = "user";
            $this->navigation = &$__navigation_user;
        }
        else if (preg_match("/\/pos\//", $_SERVER["PHP_SELF"]))
        {
            $this->area = "pos";
            $this->navigation = &$__navigation_pos;
        }
		else if (preg_match("/\/mis\//", $_SERVER["PHP_SELF"]))
        {
            $this->area = "mis";
            $this->navigation = &$__navigation_mis;
        }
        else if (preg_match("/\/archive\//", $_SERVER["PHP_SELF"]))
        {
            $this->area = "archive";
            $this->navigation = &$__navigation_archive;
        }
        else if (preg_match("/\/stats\//", $_SERVER["PHP_SELF"]))
        {
            $this->area = "stats";
            $this->navigation = &$__navigation_stats;
        }
        else if (preg_match("/\/issues\//", $_SERVER["PHP_SELF"]))
        {
            $this->area = "issues";
            $this->navigation = &$__navigation_issues;
        }
		else if (preg_match("/\/cer\//", $_SERVER["PHP_SELF"]))
        {
            $this->area = "cer";
            $this->navigation = &$__navigation_cer;
        }
		else if (preg_match("/\/sec\//", $_SERVER["PHP_SELF"]))
        {
            $this->area = "sec";
            $this->navigation = &$__navigation_cer;
        }
        else
        {
            $this->area = PAGE_POPUP;
        }

        // If no name has been given, try to generate the page within
        // the same section as the last page

        if ($name == PAGE_LAST)
        {
            $name = get_session_value("section");

            if (!$name)
            {
                $name = "welcome";
            }
        }

        // Lookup section name in navigation tree

        if ($name != PAGE_POPUP)
        {
            // Check validity of section name

            foreach ($this->navigation as $section)
            {
                if ($section["name"] == $name)
                {
                    $this->section = $section;
                    set_session_value("section", $name);
                    break;
                }
            }

            if (!$this->section)
            {
                error("Invalid section \"$name\" (Page::new).");
            }
        }


		//Windows Title Options
		$this->title_extension = "";
		if(param('pid') > 0) {
			
			$sql = 'select project_number, order_shop_address_company from projects ' . 
				   'left join orders on order_id = project_order ' .
				   'where project_id = ' . param('pid');
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				
				$this->title_extension = $row["project_number"] . ', ' . $row["order_shop_address_company"];
			}
		}
		elseif(param('oid') > 0) {
			
			$sql = 'select order_number from orders where order_id = ' . param('oid');
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				
				$this->title_extension = 'Order: ' . $row["order_number"];
			}
		}
		elseif(param('pos_id') > 0) {
			
			$sql = 'select posaddress_name from posaddresses where posaddress_id = ' . param('pos_id');
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				
				$this->title_extension = "" . $row["posaddress_name"];
			}
		}
		elseif(is_array($this->section) and array_key_exists('name', $this->section) and $this->section["name"] == 'posaddresses' and array_key_exists('id', $_GET) and $_GET['id'] > 0) {
			
			$sql = 'select posaddress_name from posaddresses where posaddress_id = ' . $_GET['id'];
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				
				$this->title_extension = "" . $row["posaddress_name"];
			}
		}
		
    }

    function register_action($name, $caption, $url = "", $target = "", $modal_id = "")
    {
        if (!$url and !$modal_id)
        {
            $url = "javascript:button('$name')";
        }

        $this->actions[] = array("name" => $name,
                                 "caption" => $caption,
                                 "url" => $url,
                                 "target" => $target,
		                         "modal_id" => $modal_id);


    }

    function header()
    {
        global $_SERVER;
        global $__needs_form;
        global $__needs_multipart_form;

        // HTML header with title

        if ($this->title)
        {
            $title = $this->title;
        }
        else if ($this->section)
        {
            if($this->title_extension) {
				$title = $this->title_extension;
			}
			else
			{
				$title = $this->section["title"];
			}
			
        }
        else
        {
            $title = "";
        }

		if($this->navigation[0]["title"] == $title) {
			if($this->navigation[0]["title"] == '') {

				$title = "Welcome";
			}
			else
			{
				$title = $this->navigation[0]["title"] . ": Welcome";
			}
		}
		else {
			if($this->navigation[0]["title"] == '') {

				$title = $title;
			}
			else
			{
				$title = $this->navigation[0]["title"] . ": " . $title;
			}
		}

        if($this->title_extension)
		{
			$title = htmlspecialchars(smart_join(": ", array("", $title)));
		}
		else
		{
			$title = htmlspecialchars(smart_join(": ", array("", $title)));
		}

		//echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
		//echo "<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\">";
       
		echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
		echo "<html>";
		echo "<head>";
        echo "<title>$title</title>";
		echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />";
		echo "<meta http-equiv=\"imagetoolbar\" content=\"no\" />";
		echo "<meta name=\"robots\" content=\"noindex\">";

		echo "<link href=\"../include/retailnet.css?v=" . time() . "\" rel=\"stylesheet\" type=\"text/css\" />";
		echo "<link rel=\"stylesheet\" href=\"../js/nyroModal/styles/nyroModal.css\" type=\"text/css\" media=\"screen\" />";
		echo "<link href=\"../js/jquery.bettertip.css\" rel=\"stylesheet\" type=\"text/css\" />";
		echo "<link rel=\"stylesheet\" href=\"../js/jquery-window-5.03/css/jquery.window.css\" type=\"text/css\">";
		//echo "<link rel=\"stylesheet\" href=\"../js/thickbox.css\" type=\"text/css\" media=\"screen\" />";
		
		echo "<link href=\"../include/font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\" />";
		echo "<link rel=\"shortcut icon\" href=\"favicon.ico\" type=\"image/x-icon\" />";
		echo "<link rel=\"apple-touch-icon\" href=\"apple-touch-icon.png\" />";

		
		

        echo "<script language=\"javascript\" src=\"../js/jquery-1.4.2.min.js\" type=\"text/javascript\"></script>";
		echo "<script type=\"text/javascript\" src=\"../js/nyroModal/js/jquery.nyroModal-1.2.8.retailnet.js\"></script>";
		echo "<script language=\"javascript\" src=\"../js/jquery.tablednd_0_5.js\" type=\"text/javascript\"></script>";
		echo "<script language=\"javascript\" src=\"../js/jquery.bettertip.js\" type=\"text/javascript\"></script>";
		echo "<script language=\"javascript\" src=\"../js/rating.js\" type=\"text/javascript\"></script>";
		echo "<script type=\"text/javascript\" src=\"../js/jquery-window-5.03/jquery-ui-1.8.16.custom.min.js\"></script>";
		echo "<script type=\"text/javascript\" src=\"../js/jquery-window-5.03/jquery.window.min.js\"></script>";
		//echo "<script language=\"javascript\" src=\"../js/thickbox.js\" type=\"text/javascript\"></script>";

		echo "<script type=\"text/javascript\" src=\"../js/numeral_min/numeral.min.js\"></script>";
		

		echo "<script language=\"javascript\" src=\"../include/main.js?v=" . time() . "\" type=\"text/javascript\"></script>";

        // Image preloader

        if ($this->section)
        {
            echo "<script language=\"javascript\" type=\"text/javascript\">\n";
            echo "<!--\n";

            foreach ($this->navigation as $section)
            {
                if (!isset($section["parent"]))
                {
                    if (!isset($section["hide"]))
                    {
                        echo $section["name"], "_on = new Image();\n";
                        echo $section["name"], "_on.src = 'pictures/navigation/", $section["name"], "_on.gif';\n";
                    }

                    echo $section["name"], "_title = new Image();\n";
                    echo $section["name"], "_title.src = 'pictures/titles/", $section["name"], ".gif';\n";
                    echo $section["name"], "_back = new Image();\n";
                    echo $section["name"], "_back.src = 'pictures/titles/", $section["name"], "_back.gif';\n";
                }
            }

            echo "//-->\n";
            echo "</script>\n";
        }

        echo "</head>";

        // Body tag (popup pages end here)

        if ($__needs_form)
        {
            $enctype = $__needs_multipart_form ? "enctype=\"multipart/form-data\" " : "";
            $form = "<form ${enctype}name=\"main\" id=\"main\" method=\"post\" action=\"" . $_SERVER["PHP_SELF"] . "\">";
            $form .= "<input type=\"hidden\" name=\"action\" value=\"\"  />";
        }
        else
        {
            $form = "";
        }

        if (!$this->section)
        {
            echo "<body id=\"maindoc\" bgcolor=\"#FFFFFF\" style=\"margin-left:20px;margin-top:20px;\">";
            echo $form;
            return;
        }

        echo "<body id=\"maindoc\" bgcolor=\"#FFFFFF\" style=\"margin-left:0px;margin-top:0px;\">";
        echo $form;

        // Dermine master section

        $master = isset($this->section["parent"]) ? $this->section["parent"] : $this->section["name"];

        // Logo with image

        echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
        echo "<tr>";
        echo "<td>";
		echo "<div style=\"float:left\"><img src=\"../pictures/logo2012.gif\" width=\"400\" height=\"48\" alt=\"\" /></div>";
		require_once("page_module_navigation.php");
		echo "</td>";
        echo "</tr>";

        // Navigation bar

        echo "<tr>";
        //echo "<td background=\"../pictures/menu_spacer.gif\" nowrap='nowrap'>";

		echo "<td style=\"background-image:url('../pictures/menu_spacer.gif');\" nowrap='nowrap'>";

        if (!isset($this->section["nomenu"]))
        {
            echo "<img src=\"../pictures/menu_spacer.gif\" width=\"159\" height=\"19\" alt=\"\" />";

            foreach ($this->navigation as $section)
            {
                if (!isset($section["parent"]) && !isset($section["hide"]) && has_access($section["permission"]))
                {
                    $suffix = $master == $section["name"] ? "_on.gif" : "_off.gif";
                    $image = "pictures/navigation/" . $section["name"] . $suffix;
                    $url = "/" . $section["url"] != $_SERVER["PHP_SELF"] ? $section["url"] : "";

                    $name = $section["name"];
                    $script = "onmouseover=\"document.images['$name'].src = 'pictures/navigation/${name}_on.gif'\" " .
                              "onmouseout=\"document.images['$name'].src = '$image'\"";

                    if ($url) echo "<a href=\"$url\" $script>";
                    echo "<img name=\"$name\" src=\"$image\" width=\"100\" height=\"19\" border=\"0\" alt=\"\" />";
                    if ($url) echo "</a>";
                }
            }

            echo "<img src=\"../pictures/menu_end.gif\" width=\"2\" height=\"19\" alt=\"\" />";
        }
        else
        {
            echo "&nbsp;";
        }

        echo "</td>";
        echo "</tr>";

        // Title image

        echo "<tr>";
        //echo "<td background=\"pictures/titles/${master}_back.gif\">";
		echo "<td style=\"background-image:url('pictures/titles/${master}_back.gif');\" nowrap='nowrap'>";
        echo "<img src=\"pictures/titles/$master.gif\" width=\"400\" height=\"40\" alt=\"\" />";
        echo "</td>";
        echo "</tr>";
        echo "</table>";

        // Open area below navigation

        echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
        echo "<tr valign=\"top\">";
        echo "<td width=\"160\">";

        // Actions and side navigation

        echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";

        $actions = array();

        foreach ($this->navigation as $section)
        {
            if (isset($section["parent"]) && $section["parent"] == $master && has_access($section["permission"]))
            {
                $actions[] = array("name" => $section["title"],
                                   "caption" => $section["title"],
                                   "url" => $section["url"], 
                                   "target" => "");
            }
        }

        $actions = array_merge($actions, $this->actions);

        foreach ($actions as $action)
        {
            echo "<tr valign=\"top\">";

            echo "<td width=\"22\" bgcolor=\"#EEEEEE\">";
            echo "<img src=\"../pictures/spacer.gif\" width=\"22\" height=\"1\" alt=\"\" />";
            echo "</td>";

            echo "<td width=\"133\" class=\"action\" bgcolor=\"#EEEEEE\">";
            echo "<img src=\"../pictures/spacer.gif\" width=\"133\" height=\"3\" alt=\"\" /><br />";
            
			if($action["url"] != "")
			{
				if($action["target"] != "")
				{
					echo "<a href=\"", $action["url"], "\" target=\"" . $action["target"] . "\">";
				}
				else
				{
					echo "<a href=\"", $action["url"], "\">";
				}
				echo htmlspecialchars($action["caption"]);
	            echo "</a><br />";
			}
			elseif(array_key_exists("modal_id", $action) and $action["modal_id"] != "")
			{
				
				echo  '<a id="' . $action["modal_id"] . '" href="#">' .$action["caption"] . '</a>'; 
			}
			else
			{
				echo "&nbsp;<br />";
			}
            
            echo "<img src=\"../pictures/spacer.gif\" width=\"133\" height=\"3\" alt=\"\" />";
            echo "</td>";
    
            echo "<td width=\"4\" bgcolor=\"#EEEEEE\">";
            echo "<img src=\"../pictures/spacer.gif\" width=\"4\" height=\"1\" alt=\"\" />";
            echo "</td>";

            echo "<td width=\"1\" bgcolor=\"#999999\">";
            echo "<img src=\"../pictures/spacer.gif\" width=\"1\" height=\"1\" alt=\"\" />";
            echo "</td>";

            echo "</tr>";

            echo "<tr>";

            echo "<td colspan=\"4\">";
            echo "<img src=\"../pictures/menu_line.gif\" width=\"160\" height=\"1\" alt=\"\" />";
            echo "</td>";

            echo "</tr>";
        }

        echo "<tr>";
        echo "<td colspan=\"4\" width=\"160\">";
        echo "<img src=\"../pictures/spacer.gif\" width=\"160\" height=\"1\" alt=\"\" />";
        echo "</td>";
        echo "</tr>";

        echo "</table>";

        // Content area

        echo "</td>";
        echo "<td width=\"20\">";
        echo "<img src=\"../pictures/spacer.gif\" width=\"20\" height=\"1\" alt=\"\" />";
        echo "</td>";

        echo "<td>";
        echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
        echo "<tr>";
        echo "<td height=\"20\"></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>";
    }

    function footer()
    {
        global $__needs_form;

        if ($this->section)
        {
            echo "</td>";
            echo "</tr>";
            echo "</table>";

            echo "</td>";
            echo "<td width=\"20\">";
            echo "<img src=\"../pictures/spacer.gif\" width=\"20\" height=\"1\" alt=\"\" />";
            echo "</td>";

            echo "</tr>";
			echo "<tr>";
            echo "<td colspan=\"3\" height=\"20\">";
            echo "</td>";
            echo "</tr>";

            //echo "</td>";
            //echo "</tr>";
            echo "</table>";
        }

        if ($__needs_form)
        {
            echo "</form>";
        }

        echo "</body>";
        echo "</html>";

        global $start_time;

        $end_time = time();
        $diff = $end_time - $start_time;

		
		if(array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
			$ips = $_SERVER['HTTP_X_FORWARDED_FOR'];
			if(is_array ( $ips )) {
				$i = count($ips);
				$ip = $ips[$i-1];
			}
			else
			{
				$ip = $ips;
			}
		}
		else
		{
			$ip = $_SERVER['REMOTE_ADDR'];
		}

        $sql = "Insert into statistics (" .
               "statistic_user, statistic_ip, statistic_date, " .
               "statistic_url, statistic_duration) " .
               "values ('" .
               user_id() . "', '" .
               $ip . "', '" .
               date("Y-m-d-H-i-s") . "', '" .
               $_SERVER["SCRIPT_FILENAME"] . "', " .
               $diff . ")";

          $res = mysql_query($sql);
    }

    function title($text)
    {
        echo "<p class=\"title\">";
        echo htmlspecialchars($text);
        echo "</p>";
    }


	function add_tab($name, $caption, $view = "", $target = "_self", $flags = 0)
    {
        $this->tabs[$name] = array("name" => $name,
								   "caption" => $caption,
								   "view" => $view,
								   "target" => $target,
								   "flags" => $flags, 
			                       "type" => "link");
    }


	function add_tab_list($name, $caption, $view = "", $value = "", $flags = 0, $values = array())
    {
        $this->tabs[$name] = array("name" => $name,
                                   "caption" => $caption,
                                   "view" => $view,
								   "value" => $value,
                                   "flags" => $flags,
			                       "values" => $values, 
			                       "type" => "list");
    }
	
	function tabs()
    {
        $tabs = "";
		$tabs .= "<div class=\"tab\">";
		$tabs .= "|&nbsp;";
		foreach ($this->tabs as $tab)
        {
			if($tab["type"] == 'link')
			{
				$tabs .= "<a href=\"" . $tab["view"] . "\" target=\"" . $tab["target"] . "\">" . htmlspecialchars($tab['caption']) . "</a>";
				$tabs .= "&nbsp;&nbsp;&nbsp;|&nbsp;";
			}
			elseif($tab["type"] == 'list')
			{
				$tabs .= '<select name = ' . $tab["name"] . ' id = ' . $tab["name"] . '>';
				$tabs .= '<option value="">' . $tab["caption"] . '</option>';
				foreach($tab["values"] as $key=>$value)
				{
					$selected = "";
					if($tab["value"] == $key)
					{
						$selected = "selected";
					}
					$tabs .= '<option value="' . $key . '" ' . $selected .'>' . $tab["caption"] . ' ' . $value  . '</option>';
				}

				$tabs .= '</select>';
				$tabs .= "&nbsp;&nbsp;&nbsp;|&nbsp;";
			}
		}
		
		$tabs .= "</div>";
		$tabs .= "<br class=\"celar\" />";
		echo $tabs;
    }
}

?>