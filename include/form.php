<?php
/********************************************************************

    form.php

    Classes and functions used to render forms.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-29
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-06-27
    Version:        1.2.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

/********************************************************************
    Constants
*********************************************************************/

define("NOTNULL", 1 << 0);
define("UNIQUE", 1 << 1);
define("OPTIONAL", 1 << 2);
define("DATABASE", 1 << 3);
define("EDITABLE", 1 << 4);
define("VERTICAL", 1 << 5);
define("SHORT", 1 << 6);
define("DELETABLE", 1 << 7);
define("SORTABLE", 1 << 8);
define("HIDEEMPTY", 1 << 9);
define("SUBMIT", 1 << 10);
define("STRING", 1 << 11);
define("RENDER_HTML", 1 << 12);
define("DISABLED", 1 << 13);
define("ALLOW_NULL_VALUE", 1 << 14);
define("IS_UPLOAD_BUTTON", 1 << 15);

define("FORM_BUTTON_BACK", 1);
define("FORM_BUTTON_SAVE", 2);
define("FORM_BUTTON_DELETE", 3);

define("TYPE_CHAR", 1);
define("TYPE_INT", 2);
define("TYPE_REAL", 3);
define("TYPE_DECIMAL", 4);
define("TYPE_DATE", 5);
define("TYPE_TIME", 6);
define("TYPE_DATETIME", 7);

/********************************************************************
    Form class
*********************************************************************/

class Form
{
    var $items;
    var $subtables;
    var $buttons;
    var $rules;

    var $table;
    var $entity;
    var $error;
    var $message;
    var $noname_index;
    var $has_vgap;
    var $has_hgap;
    var $width;

	var $invalid_file_extensions = array();

    function Form($table = "", $entity = "", $width = "100%")
    {
        global $__needs_form;

        $__needs_form = true;

        $this->items = array();
        $this->subtables = array();
        $this->buttons = array();
        $this->rules = array();

        $this->table = $table;
        $this->entity = $entity;
        $this->error = "";
        $this->message = "";
        $this->noname_index = 0;
        $this->width = $width;

		$this->invalid_file_extensions[] = 'php';
		 $this->invalid_file_extensions[] = 'exe';
		 $this->invalid_file_extensions[] = 'bat';
		 $this->invalid_file_extensions[] = 'so';
		 $this->invalid_file_extensions[] = 'o';
		 $this->invalid_file_extensions[] = 'elf';
		 $this->invalid_file_extensions[] = 'bin';
 	     $this->invalid_file_extensions[] = 'sh';
	     $this->invalid_file_extensions[] = 'deb';
    }

    function error($error = null)
    {
        if (!is_null($error))
        {
            $this->error = $error;
        }

        return $this->error;
    }

    function message($message)
    {
        if (!is_null($message))
        {
            $this->message = $message;
        }

        return $this->message;
    }

    function process()
    {
        global $_SERVER;
        global $_REQUEST;
        global $__params;

        if ($this->button(FORM_BUTTON_BACK))
        {
            if (!id())
            {
                foreach ($this->items as $item)
                {
                    if ($item["type"] == "upload")
                    {
                        $path = combine_paths($_SERVER["DOCUMENT_ROOT"], $item["value"]); 

                        if ($item["value"] && file_exists($path))
                        {
                            unlink($path);
                        }
                    }
                }
            }

            $view = $this->buttons[FORM_BUTTON_BACK]["view"];

            if ($view)
            {
                redirect($view);
            }
            else
            {
                redirect(get_referer());
            }
        }
        else if ($this->button(FORM_BUTTON_SAVE))
        {
            if ($this->validate())
            {
                $this->save();
                $this->message("The data has been saved.");
            }

            return true;
        }
        else if ($this->button(FORM_BUTTON_DELETE))
        {
            $delete = new DeleteAction($this->table, id());

            $delete->entity($this->entity);
            $delete->back_on_success(get_referer());

            set_session_value("delete_object", $delete);
            redirect("delete.php");
        }
        else if (isset($_REQUEST["action"]))
        {
            foreach ($this->buttons as $button)
            {
                if ($this->button($button["name"]) && $button["view"])
                {
                    if ($this->validate())
                    {
                        $this->save();
                        redirect($button["view"]);
                    }

                    return true;
                }
            }

            foreach ($this->subtables as $subtable)
            {
                if ($subtable["list"]->can_process())
                {
                    if ($this->validate())
                    {
                        $this->save();
                        $subtable["list"]->process();
                    }

                    return true;
                }
            }
        }

        return false;
    }

    function render()
    {
        global $_REQUEST;
        global $msie_browser;
        
		// Remember record id

        if (isset($_REQUEST["id"]))
        {
            echo "<input type=\"hidden\" name=\"id\" value=\"", (int)$_REQUEST["id"], "\" />";
        }
		// Open table

        echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"", $this->width, "\">";

        // Initialize gap reminders

        $this->last_item = null;
        $this->has_vgap = false;
        $this->has_hgap = false;

        // Output error message if available
  
        if ($this->error)
        {
            $this->draw_line($this->error, "error");
            $this->last_item = 1;
            $this->draw_gap();
        }

        // Output informational message if available

        if ($this->message)
        {
            $this->draw_line($this->message, "message");
            $this->last_item = 1;
            $this->draw_gap();
        }

		//prevent from XSS
		foreach ($this->items as $key=>$item)
        {
			if($this->items[$key]['type'] == 'edit' or $this->items[$key]['type'] == 'multiline')
			{
				if ( $this->items[$key]['value'] != strip_tags($this->items[$key]['value']) )
				{

					$this->items[$key]['value'] =htmlspecialchars(strip_tags($item['value']), ENT_QUOTES, "UTF-8");
				}
				else
				{
					$tmp = str_replace(' ', '', $this->items[$key]['value']);
					$tmp = strtolower($tmp);

					$matches = array();
					if(preg_match('/(size|link|import|meta|@|frame|background|on|dynsrc|lowsrc|iframe|java|src|<img|style|http|https|script|javascript|bgsound|xml|body|url|rel|equiv|base|object|embed|xml|&#|div|exec|cmd|<?|redirect)/', $tmp, $matches) 
						and ($this->items[$key]['type'] == 'edit' or $this->items[$key]['value'] == 'multiline')) {
							$this->items[$key]['value'] =htmlspecialchars(strip_tags($item['value']), ENT_QUOTES, "UTF-8");
					}
				}
			}
		}

        // Output all invisible items

        foreach ($this->items as $item)
        {
            if ($item["type"] == "hidden")
            {
                echo "<tr><td colspan=\"4\">";
				echo "<input type=\"hidden\" id=\"", $item["name"], "\" name=\"", $item["name"], "\" value=\"", $item["value"], "\" />";
				echo "</td></tr>";
            }
        }

        // Output all visible items

        foreach ($this->items as $item)
        {
            // Skip invisible items

            if ($item["type"] == "hidden")
            {
                continue;
            }

            // Skip optional items if the record has not been stored yet

            if (!id() && $item["flags"] & OPTIONAL)
            {
                continue;
            }

            // Draw the current item

            switch ($item["type"])
            {
                case "section":
                    $this->draw_gap();

                    if ($item["caption"])
                    {
                        $this->draw_line($item["caption"], "section");
                        $this->last_item = 1;
                        $this->draw_gap();
                    }

                    break;

                case "comment":
					$this->draw_gap();
                    //$this->draw_line($item["caption"], "text");
					$this->draw_line_lbr($item["caption"]);
                    $this->draw_gap();
                    break;
				
				case "table":
                    $this->draw_gap();
					$this->draw_table_line($item["caption"], "table_line");
                    $this->draw_gap();
                    break;
				case "label":
                    
				    if (!$item["value"] && $item["flags"] & HIDEEMPTY)
                    {
                        break;
                    }
                    
                    if ($item["value"])
                    {
                        if($item["flags"] & RENDER_HTML)
                        {
                            $value = "<span class=\"label\">" . str_replace("\n", "<br />", $item["value"]) . "</span>";
                        }
                        else
                        {
                            //$value = smart(str_replace("\n", "<br />", htmlspecialchars($item["value"])));
							$value = htmlspecialchars($item["value"]);
                        }
                    }
                    else
                    {
                        $value = "&nbsp;";
                    }

                    if($item["flags"] & RENDER_HTML)
                    {
						//$content = "<span class=\"label\">" . $value . "</span>";                        
						$content = $value;                        
                    }
                    else
                    {
                        $content = "<input type=\"hidden\" id=\"" . $item["name"] . "\" name=\"" . $item["name"] . "\" value=\"" . $item["value"] . "\" />";
                        $content .= "<span class=\"value\">" . $value . "</span>";

                    }

                    $this->draw_line2_lbr($item["caption"], $content);

                    break;
				case "label_selector":
                    if (!$item["value"] && $item["flags"] & HIDEEMPTY)
                    {
                        break;
                    }
                    
                    if ($item["value"])
                    {
                        if($item["flags"] & RENDER_HTML)
                        {
                            $value = "<span class=\"label\">" . str_replace("\n", "<br />", $item["value"]) . "</span>";
                        }
                        else
                        {
                            //$value = smart(str_replace("\n", "<br />", htmlspecialchars($item["value"])));
							$value = smart(str_replace("\n", "<br />", $item["value"]));
                        }
                    }
                    else
                    {
                        $value = "&nbsp;";
                    }

                    
					if($item["flags"] & RENDER_HTML)
                    {
						//$content = "<span class=\"label\">" . $value . "</span>";                        
						$content = $value;
                    }
                    else
                    {
                        $content = "<input type=\"hidden\" id=\"" . $item["name"] . "\" name=\"" . $item["name"] . "\" value=\"" . $item["value"] . "\" />";
                        $content .= "<span class=\"value\">" . $value . "</span>";
                    }

					if($item["icon"] and $item["link"])
					{
						$content = "<div><a id=\"" . $item["name"] . "_selector\" href=\"#\"><img style=\"vertical-align: -20%;padding-right:4px;\" border=\"0\" src=\"" . $item["icon"] . "\" alt=\"\" /></a>" . $content . "</div>"; 
					}

                    $this->draw_line2($item["caption"], $content);

                    break;

				case "modal_selector":
					
					$caption = '<a id="' . $item["name"] . '_selector" href="#">' .$item["caption"] . '</a>'; 

                    
					$content = "<table>";
				    $content .= "<tr valign=\"top\"><td>";
					$content .= "<textarea  id=\"" . $item["name"] . "\" name=\"" . $item["name"] . "\" cols=\"48\" rows=\"" . $item["height"] . "\" class=\"textarea\">";
                    if($item["value"]) {
						
						if($item["flags"] & RENDER_HTML)
                        {
							$content .= $item["value"];
						}
						else
						{
							$content .= htmlspecialchars($item["value"]);
						}
					}
                    $content .= "</textarea>";
					
					$content .= "</td>";
					
					$content .= "</tr></table>";

                    $this->draw_line2($caption, $content);

                    break;
				case "iconlink":
                    if (!$item["value"] && $item["flags"] & HIDEEMPTY)
                    {
                        break;
                    }
                    
					if ($item["value"])
                    {
                        if($item["flags"] & RENDER_HTML)
                        {
                            $value = "<span class=\"value\">" . str_replace("\n", "<br />", $item["value"]) . "</span>";
                        }
                        else
                        {
                            $value = smart(str_replace("\n", "<br />", htmlspecialchars($item["value"])));
                        }
                    }
                    else
                    {
                        $value = "&nbsp;";
                    }

                    if($item["flags"] & RENDER_HTML)
                    {
						//$content = "<span class=\"label\">" . $value . "</span>";                        
						$content = $value;                        
                    }
                    else
                    {
                        $content = "<input type=\"hidden\" name=\"" . $item["name"] . "\" value=\"" . $item["value"] . "\" />";
                        $content .= "<span class=\"value\">" . $value . "</span>";
                    }

					
					$href = "";
					if($item["icon"] and $item["link"])
					{
						if($item["target"])
						{
							$href = "<a href=\"" . $item["link"] .  "\" target=\"" . $item["target"] . "\"><img style=\"vertical-align: -20%;\" src=\"" . $item["icon"] ."\" border=\"0\"   alt=\"\"/>&nbsp;";
						}
						else
						{
							$href = "<a href=\"" . $item["link"] . "\"><img style=\"vertical-align: -20%;\" src=\"" . $item["icon"] ."\" border=\"0\" alt=\"\" />&nbsp;";
						}
					}

					if($href)
					{
						$content = "<div style=\"padding-bottom:4px;\">" . $href . "</a>" . $content . "</div>";
					}

                    $this->draw_line($content);

                    break;
                case "lookup":
                    $key = table_key($item["table"]);

                    $sql = "select " . $item["field"] . " from " . $item["table"] . " where $key = '" . $item["value"] . "'";
                    $res = mysql_query($sql) or dberror($sql);
                    $row = mysql_fetch_row($res);

                    if (!$row[0] && $item["flags"] & HIDEEMPTY)
                    {
                        break;
                    }


                    //$value = $row[0] ? smart(htmlspecialchars($row[0])) : "&nbsp;";

					$value = $row[0] ? smart($row[0]) : "&nbsp;";

                    $content = "<input type=\"hidden\" name=\"" . $item["name"] . "\" value=\"" . $item["value"] . "\" />";
                    $content .= "<span class=\"value\">$value</span>";

                    $this->draw_line2($item["caption"], $content);
                    break;

                case "edit":
                    $script = $item["flags"] & SUBMIT ? 
                              " onchange=\"button('" . $item["name"] . "')\"" : "";

                    $length = $item["length"] == 0 ? "" : " maxlength=\"" . $item["length"] . "\"";
                    $size = ($item["length"] == 0 || $item["length"] > 20) ? 50 : 20;

                    $disabled = "";
					if ($item["flags"] & DISABLED)
                    {
						$disabled = " style=\"border:0px;background:#EEEEEE;\" readonly=\"readonly\" ";
						$content = "<input type=\"text\" " . $disabled . "name=\"" . $item["name"] . "\" id=\"" . $item["name"] . "\" value=\"" . $item["value"] . "\"" . "$length size=\"$size\"$script />";
					}
					else
					{
						$content = "<input type=\"text\" name=\"" . $item["name"] . "\" id=\"" . $item["name"] . "\" value=\"" . $item["value"] . "\"" . "$length size=\"$size\"$script />";
					}
					
					$content = "<input type=\"text\" " . $disabled . "name=\"" . $item["name"] . "\" id=\"" . $item["name"] . "\" value=\"" . $item["value"] . "\"" . "$length size=\"$size\"$script />";

                    
					
					if($item["helper"] == 1 and $item["helper_name"])
					{
						$content .= "<a id=\"h_" . $item["helper_name"] . "\" href=\"\$" . $item["helper_name"] . "?width=auto\" class=\"betterTip\" title=\"\$none\">";
						$content .=	"<img class=\"img_helper\"  border=\"none\" src='/pictures/info.gif' alt=\"\" /></a>";
					}
					elseif($item["helper"] == 2 and $item["helper_name"])
					{
						$content .= "<a id=\"h_" . $item["helper_name"] . "\" href=\"javascript:void(0);\">";
						$content .=	"<img class=\"img_helper\" border=\"none\" src=\"/pictures/key_a.png\" alt=\"\" /></a>";
					}

					
					if($item["print_out"])
					{
						$content .= "&nbsp;&nbsp;" . $item["print_out"];
					}
					
					$this->draw_line2($item["caption"], $content);
                    break;
				case "input_submit":
					$content = "<input onmouseover=\"this.style.color='#FF0000';this.style.cursor='pointer'\" onmouseout=\"this.style.color='#006699'\" class=\"modalbutton\" type=\"submit\" value=\"" . $item["caption"] .  "\" />";
					$this->draw_line2("", $content);
                    break;
                case "multiline":
                	$content = "<table>";
				    $content .= "<tr valign=\"top\"><td>";
					if($item["cols"] > 0)
					{
						$content .= "<textarea id=\"" . $item["name"] . "\" name=\"" . $item["name"] . "\" rows=\"" . $item["height"] . "\" cols=\"" . $item["cols"] . "\"class=\"textarea\">";
					}
					else
					{
						$content .= "<textarea id=\"" . $item["name"] . "\" name=\"" . $item["name"] . "\" cols=\"48\" rows=\"" . $item["height"] . "\" class=\"textarea\">";
					}
				    
                    $content .= htmlspecialchars($item["value"]);
                    $content .= "</textarea>";
					
					$content .= "</td>";
					if($item["helper"] == 1 and $item["helper_name"])
					{
						$content .= "<td>";
						$content .= "<a id=\"h_" . $item["helper_name"] . "\" href=\"\$" . $item["helper_name"] . "?width=auto\" class=\"betterTip\" title=\"\$none\">";
						$content .=	"<img class=\"img_helper\" border=\"none\" src='/pictures/info.gif' alt=\"\" /></a>";
						$content .= "</td>";
					}
					$content .= "</tr></table>";

                    $this->draw_line2($item["caption"], $content);
                    break;

                case "date":
                    $actual_day = (isset($_REQUEST[$item["name"] . "_day"]) &&
                                         $_REQUEST[$item["name"] . "_day"]) ? 
                                   $_REQUEST[$item["name"] . "_day"] : "";
                    $actual_month = (isset($_REQUEST[$item["name"] . "_month"]) &&
                                     $_REQUEST[$item["name"] . "_month"]) ?
                                     $_REQUEST[$item["name"] . "_month"] : "";
                    $actual_year = (isset($_REQUEST[$item["name"] . "_year"]) &&
                                    $_REQUEST[$item["name"] . "_year"])?
                                    $_REQUEST[$item["name"] . "_year"] : "";

                    if (!empty($item["value"]))
                    {
                        $unix_date = strtodate($item['value']);
                        if ($unix_date)
                        {
                            list($actual_day, $actual_month, $actual_year) = 
                                explode("-", date("d-m-Y",$unix_date));
                        }
                    }

                    $content = "<select name=\"" . $item["name"] ."_day\" size=\"1\">";
                    $content .= "<option value=\"\"></option>";

                    for ($day = 1; $day <= 31; $day++)
                    {
                        $selected = ($day == $actual_day) ? " selected='selected'": "";
                        $day = sprintf("%02d", $day);
                        $content .= "<option value=\"" . $day . "\"$selected>" . $day . "</option>";
                    }
                    $content .= "</select>&nbsp;";

                    $content .= "<select name=\"" . $item["name"] ."_month\" size=\"1\">";
                    $content .= "<option value=\"\"></option>";

                    for ($month = 1; $month <= 12; $month++)
                    {
                        $selected = ($month == $actual_month) ? " selected='selected'": "";
                        $month = sprintf("%02d", $month);
                        $content .= "<option value=\"" . $month . "\"$selected>" . $month . "</option>";
                    }
                    $content .= "</select>&nbsp;";
                    
                    $content .= "<select name=\"" . $item["name"] ."_year\" size=\"1\">";
                    $content .= "<option value=\"\"></option>";

                    for ($year = (date("Y") -2); $year < date("Y") + 10; $year++)
                    {
                        $selected = ($year == $actual_year) ? " selected='selected'": "";
                        $content .= "<option value=\"" . $year . "\"$selected>" . $year . "</option>";
                    }
                    $content .= "</select>";

                    $this->draw_line2($item["caption"], $content);
                    break;

                case "checkbox":
                   $script = $item["flags"] & SUBMIT ? 
                          " onchange=\"button('" . $item["name"] . "')\"" : "";

                    $checked = $item["value"] ? " checked='checked'" : "";

                    $disabled = "";
                    if($item["flags"] & DISABLED)
                    {
                        $disabled = " disabled=\"disabled\"";
                        //$checked = " checked='checked'";
                    }
                

                    $content = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
                    $content .= "<tr valign=\"top\">";

                    $content .= "<td>";
                    $content .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
                    $content .= "<tr><td height=\"24\">";
                    
					if($item["render_input_field"] == true) {
						$content .= "<input type=\"hidden\" name=\"" . $item["name"] . "\" value=\"0\" />";
					}

                    $content .= "<input type=\"checkbox\" name=\"" . $item["name"] . "\" id=\"" . $item["name"] . "\" value=\"1\" $script $disabled $checked />";
                    $content .= "&nbsp;";
                    $content .= "</td></tr>";
                    $content .= "</table>";
                    $content .= "</td>";

                    $content .= "<td nowrap='nowrap'>";
                    $content .= "<label for=\"" . $item["name"] . "\" class=\"value\">";

                    if($item["flags"] & RENDER_HTML)
                    {
                        $content .= str_replace("\n", "<br />", $item["caption"]);
                    }
                    else
                    {
                        $content .= str_replace("\n", "<br />", htmlspecialchars($item["caption"]));
                    }
                    
                    $content .= "</label>";
                    $content .= "</td>";

                    $content .= "</tr>";
                    $content .= "</table>";

                    $this->draw_line2($item["caption1"], $content);
                    break;

                case "radiolist":
                    if (!($item["flags"] & VERTICAL)) // horizontal with captions
                    {
						$content = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
                        
                        $content .= "<tr>";
                        foreach (array_keys($item["choices"]) as $key)
                        {
                             $script = $item["flags"] & SUBMIT ? 
                              " onClick=\"button('" . $item["name"] . "')\"" : "";

                            $content .= "<td class=\"value\">";
                            $checked = $item["value"] == $key ? " checked='checked'" : "";

                            $content .= "<input type=\"radio\" name=\"" . $item["name"] . "\" id=\"" . $item["name"] . "_". $key . "\" value=\"$key\"" . $checked .  " " . $script . " />";
                            
                            $content .= "&nbsp;";
                            $content .= "</td>";

                            $content .= "<td class=\"value\"><label for=\"" . $item["name"] . "_". $key . "\">";

                            if($item["flags"] & RENDER_HTML)
                            {
                                $content .= str_replace("\n", "<br />", $item["choices"][$key]);
                            }
                            else
                            {
                                $content .= str_replace("\n", "<br />", htmlspecialchars($item["choices"][$key]));
                            }
                        
                            $content .= "&nbsp;";
                            $content .= "</label></td>";
                        }
                        
                        $content .= "</tr>";
                        $content .= "</table>";
                        $this->draw_line2($item["caption"], $content);
                        break;
                    }
                    else // vertical radio buttons
                    {
						foreach (array_keys($item["choices"]) as $key)
                        {
                            $script = $item["flags"] & SUBMIT ? 
                              " onClick=\"button('" . $item["name"] . "')\"" : "";

                            $checked = $item["value"] == $key ? " checked='checked'" : "";

                            $content = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
                        
                            $content .= "<tr valign=\"top\">";

                            $content .= "<td class=\"value\">";
                            $content .= "<input type=\"radio\" name=\"" . $item["name"] . "\" id=\"" . $item["name"] . "_" . $key . "\" value=\"$key\"" . $checked .  " " . $script . " />";

                            $content .= "&nbsp;";
                            $content .= "</td>";

                            $content .= "<td class=\"value\"><label for=\"" . $item["name"] . "_". $key . "\">";
                            
                            if($item["flags"] & RENDER_HTML)
                            {
                                $content .= str_replace("\n", "<br />", $item["choices"][$key]);
                            }
                            else
                            {
                                $content .= str_replace("\n", "<br />", htmlspecialchars($item["choices"][$key]));
                            }

                            $content .= "&nbsp;";
                            $content .= "</label></td>";
                            $content .= "</tr>";
                            
                            $content .= "</table>";
                            
							$this->draw_line2($item["caption"], $content);
                        }

                        break;

                    }

                case "list":
                    
					$script = $item["flags"] & SUBMIT ? " onchange=\"button('" . $item["name"] . "')\"" : "";

                    $content = "<select id=\"" . $item["name"] . "\" name=\"" . $item["name"] . "\"$script>";
                    $content .= "<option value=\"\"></option>";
					

                    if (is_array($item["choices"]))
                    {
                        foreach (array_keys($item["choices"]) as $key)
                        {
                            if(is_numeric($item["choices"][$key]) and $item["value"] == 0 and $item["value"] == $key)
							{
								$selected = " selected='selected'";
							}
							elseif(is_numeric($key) and $item["value"] == 0 and $item["value"] == $key)
							{
								$selected = " selected='selected'";
							}
							else
							{
								
								$selected = $item["value"] && $item["value"] == $key ? " selected='selected'" : "";
							}
							

                            $content .= "<option value=\"" . htmlspecialchars($key) . "\"$selected>" . htmlspecialchars($item["choices"][$key]) . "</option>";
                        }
                    }
                    else
                    {
                        $sql = $this->prepare_sql($item["choices"]);
                        $res = mysql_query($sql) or dberror($sql);

                        while ($row = mysql_fetch_row($res))
                        {
                            $value = $row[0];
                            $text = mysql_num_fields($res) == 1 ? $row[0] : $row[1];
                            $selected = $item["value"] && $item["value"] == $value ? " selected='selected'" : "";

                            $content .= "<option value=\"" . htmlspecialchars($value) . "\"$selected>" . htmlspecialchars($text) . "</option>";
                        }
                    }

                    $content .= "</select>";
					if($item["helper"] == 1 and $item["helper_name"])
					{
						$content .= "<a id=\"h_" . $item["helper_name"] . "\" href=\"\$" . $item["helper_name"] . "?width=auto0\" class=\"betterTip\" title=\"\$none\">";
						$content .=	"<img class=\"img_helper\" border=\"none\" src='/pictures/info.gif' alt=\"\" /></a>";
					}
					
                    $this->draw_line2($item["caption"], $content);
                    break;

                case "checklist":
                    $res = mysql_query($item["sql"]) or dberror($item["sql"]);

                    


					if($item["accordeon"] == false)
					{
						
						
						$content = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"; 
					}
					else
				    {
						
						$table_id = "accordeon_". $item["name"];
						$link = "<a href=\"javascript:void(0);\" onclick=\"javascript:$('#" . $table_id. "').show('slow');\">";
						$content = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
						$content .= "<tr>";
						$content .= "<td>";
						$content .= "<span class=\"label\">" . $link . "show selection</a></span>";
						$content .= "</td>";
						$content .= "</tr>";
						
						$content .= "</table>"; 
						$content .= "<table id=\"". $table_id. "\" style=\"display:none;\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
						
					}
					
					

                    while ($row = mysql_fetch_row($res))
					{
						$checked = is_array($item["value"]) && is_integer(array_search($row[0], $item["value"])) ? " checked='checked'" : "";


						$content .= "<tr valign=\"top\">";
						$content .= "<td>";
						$content .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
						$content .= "<tr><td height=\"24\">";
						$content .= "<input type=\"checkbox\" name=\"" . $item["name"] . "[]\" id=\"" . $item["name"] . "_" . $row[0] . "\" value=\"" . $row[0] . "\"" . $checked . " />";
						$content .= "&nbsp;";
						$content .= "</td></tr>";
						$content .= "</table>";
						$content .= "</td>";

						$content .= "<td>";
						$content .= "<label for=\"" . $item["name"] . "_" . $row[0] . "\" class=\"value\">";
						
						if($item["flags"] & RENDER_HTML)
						{
							$content .= str_replace("\n", "<br />", $row[1]);
						}
						else
						{
							$content .= str_replace("\n", "<br />", htmlspecialchars($row[1]));
						}
						//$content .= htmlspecialchars($row[1]);
						$content .= "</label>";
						$content .= "</td>";

						$content .= "</tr>";
					}
					

                    $content .= "</table>";
                    $this->draw_line2($item["caption"], $content);
                    break;
                
                case "upload":
                    $content = "<input type=\"hidden\" id=\"__". $item["name"] . "\" name=\"__". $item["name"] . "\" value=\"" . htmlspecialchars($item["value"]) . "\" />";
                    $content .= "<input type=\"file\" name=\"" . $item["name"] . "\" size=\"50\" />";


					if($item["helper"] == 1 and $item["helper_name"])
					{
						$content .= "<a id=\"h_" . $item["helper_name"] . "\" href=\"\$" . $item["helper_name"] . "?width=auto\" class=\"betterTip\" title=\"\$none\">";
						$content .=	"<img class=\"img_helper\" border=\"none\" src='/pictures/info.gif' alt=\"\" /></a>";
					}

                    $this->draw_line2($item["caption"], $content);
                    
                    $content = "<span class=\"value\">";

                    if ($item["value"])
                    {
                        $tmp_id = "__". $item["name"];
						$content .= "<span id=\"" . $tmp_id . "_f\">" .make_file_link($item["value"]);


						if($item["flags"] & DELETABLE)
						{
							
							if(file_exists(combine_paths($_SERVER["DOCUMENT_ROOT"], $item["value"])))
							{
								$tmp_id = "__". $item["name"];
								$content .= "<img onclick=\"javascript:$('#" . $tmp_id . "').val('');$('#" . $tmp_id . "_f').html('');\" style=\"padding-left:4px;vertical-align:middle;\" border=\"none\" src=\"/pictures/remove.gif\" />";
							}
							elseif($item["value"])
							{
								$content = '<img src="../pictures/problem.gif" style="padding-right:6px;vertical-align:bottom;" border="none"/><span class="error" style="line-height:18px;">There is a problem. Please upload the file again!</span>';
							}
						}

						 $content .= "</span>";
                    }
                    else
                    {
                        $content .= "<not available>";
                    }

                    $content .= "</span>";

                    $this->draw_line2("", $content);

                    break;

                case "rating":

                    $star_count = 0;
					$content = "";
					$pix = "<img style=\"padding-top:4px;padding-left:4px;\" border=\"none\" src='/pictures/rating_star.gif' alt=\"\" />";
					
					if(isset($item["value"]))
                    {
						$star_count = $item["value"];
					}
					
					for($i=1;$i<=$star_count;$i++)
					{
						$content .= $pix;
					}

					$content .= "<input type=\"hidden\" name=\"" . $item["name"] . "\" id=\"" . $item["name"] . "\" value=\"" . $item["value"] . "\" />";
					
                    $this->draw_line2($item["caption"], $content);
                    break;

				case "rating_selector":
					$content = "";
					$i = 1;

					if($item["value"])
					{
						$initial_value = count($item["ratings"]) + 1 - $item["value"];
						foreach($item["ratings"] as $value=>$text_value)
						{
							if($i <= $initial_value)
							{
								$pix = "/pictures/rating_star.gif";
							}
							else
							{
								$pix = "/pictures/rating_star_off.gif";
							}
							
							$content .= "<img style=\"vertical-align:-30%;padding-left:4px;\" border=\"none\" id=\"" . $item["name"] . "_" . $i . "\" src=\"". $pix . "\"  onmousedown=\"star_click('" . $item["name"] . "', ". $i . ", " . $value . ", " . count($item["ratings"]) . ", '" .  $text_value . "')\" alt=\"\" />";
							$i++;
						}
						$content .= "&nbsp;&nbsp;<span id=\"" . $item["name"] . "_rating_text\">" . $item["ratings"][$item["value"]]. "</span>";
					}
					else
					{
						foreach($item["ratings"] as $value=>$text_value)
						{
							$content .= "<img style=\"vertical-align:-30%;padding-left:4px;\" border=\"none\" id=\"" . $item["name"] . "_" . $i . "\" src=\"/pictures/rating_star_off.gif\"  onmousedown=\"star_click('" . $item["name"] . "', ". $i . ", " . $value . ", " . count($item["ratings"]) . ", '" .  $text_value . "')\" alt=\"\"/>";
							$i++;
						}
						$content .= "&nbsp;&nbsp;<span id=\"" . $item["name"] . "_rating_text\"></span>";
					}
					
					
					$content .= "<input type=\"hidden\" name=\"" . $item["name"] . "\" id=\"" . $item["name"] . "\" value=\"" . $item["value"] . "\" />";
					
                    $this->draw_line2($item["caption"], $content);
                    break;

				default:
                    echo "Unknown item type" . $item["type"];
                    die;
            }

            $this->last_item = $item;
        }

        // Draw buttons

        $buttons = "";

        foreach ($this->buttons as $button)
        {
            
			if($button["flags"] == IS_UPLOAD_BUTTON)
			{
				if ($buttons)
                {
                    $buttons .= " | ";
                }

				$buttons .= "<a href=\"javascript:void(0);\" id=\"modal_file_upload\">" . $button["caption"] . "</a>";
			}
			elseif (id() || !($button["flags"] & OPTIONAL))
            {
                if ($buttons)
                {
                    $buttons .= " | ";
                }

                $target = "";

                if (substr($button["view"], 0, 11) == "javascript:")
                {
                    $script = $button["view"];
					$buttons .= "<a id=\"" . $button["name"] . "\" href=\"$script\"$target>" .$button["caption"] . "</a>";
                }
                else if (substr($button["view"], 0, 7) == "http://")
                {
                    $script = $button["view"];
                    $target = " target=\"_blank\"";
					$buttons .= "<a id=\"" . $button["name"] . "\" href=\"$script\"$target>" .$button["caption"] . "</a>";
                }
                else

                {
                    $script = "javascript:button('" . $button["name"] . "')";
					$buttons .= "<a id=\"" . $button["name"] . "\" href=\"$script\"$target>" .$button["caption"] . "</a>";
                }

                //$buttons .= "<a href=\"$script\"$target>" . htmlspecialchars($button["caption"]) . "</a>";
				
            }
        }

        $this->draw_gap();
        $this->draw_line("<span class=\"button\">" . $buttons . "</span>");
        $this->draw_gap();

        // Close table

        echo "</table>";

        // Draw subtables

        if (id() && count($this->subtables))
        {
            foreach ($this->subtables as $subtable)
            {
                $rows = $subtable["list"]->render();
            }
        }
    }

    function validate()
    {
        if (!$this->fresh())
        {
			foreach ($this->items as $item)
            {
                if ($item["flags"] & EDITABLE)
                {
                    if ($item["flags"] & NOTNULL)
                    {
                        if ($item["flags"] & STRING && trim($item["value"]) == "")
                        {
                            //$this->error = "The " . strtolower($item["caption"]) . " must not be empty.";
							$this->error = "The " . $item["caption"] . " must not be empty.";
                            return false;
                        }
                        else if ((!($item["flags"] & STRING) && $item["value"] == 0) and !($item["flags"] & ALLOW_NULL_VALUE))
                        {
							//$this->error = "The " . strtolower($item["caption"]) . " has to be indicated.";
							$this->error = "The " . $item["caption"] . " has to be indicated.";
                            return false;
                        }
                    }
					
					
                    if ($item["flags"] & UNIQUE)
                    {
                        if (!is_unique_value($this->table, $item["name"], $item["value"], id()))
                        {
                            //$this->error = "The " . strtolower($item["caption"]) . " already exists - it is supposed to be unique.";
							$this->error = "The " . $item["caption"] . " already exists - it is supposed to be unique.";
                            return false;
                        }
                    }
					

                    if ($item["flags"] & EDITABLE)
                    {
                        if (!$this->check_value($item))
                        {
							//$this->error = "The " . strtolower($item["caption"]) . " is invalid.";
							$this->error = "The " . $item["caption"] . " is invalid.";
							return false;
                        }
                    }
                }
            }

			

            foreach ($this->rules as $rule)
            {
                $expr = $rule["rule"];
                $vars = array();

				 

                foreach ($this->items as $item)
                {
                   
					if (isset($item["value"]))
                    {
                        if (is_array($item["value"]))
                        {
                            $args = array();

                            foreach ($item["value"] as $arg)
                            {
                                $args[] = "'" . addslashes($arg) . "'";
                            }

                            $vars[] = "\$__var__" . $item["name"] . " = array(" . join(", ", $args) . ");";
                        }
                        else
                        {
                            $vars[] = "\$__var__" . $item["name"] . " = '" . addslashes($item["value"]) . "';";
                        }

                        $expr = preg_replace("/{" . $item["name"] . "}/", "\$__var__" . $item["name"], $expr);
                    }
                }
				
				

                $expr = join("\n", $vars) . "return $expr;";

				if (!eval($expr))
                {
                    $this->error = $rule["error"];
                    return false;
                }
            }
        }

        return true;
    }

    function save()
    {
        global $_REQUEST;

		if ($this->table)
        {
            if (id())
            {
                $fields = array();

                foreach ($this->items as $item)
                {
                    if ($item["flags"] & DATABASE)
                    {
                        $value = $item["dbtype"] == TYPE_DATE ? from_system_date($item["value"]) : $item["value"];
                        $fields[] = $item["name"] . " = " . dbquote($value, true);
                    }
                }

                $fields[] = "user_modified = " . dbquote(user_login());
                $fields[] = "date_modified = current_timestamp";

                
				$key = table_key($this->table);
                $sql = "update $this->table set " . join(", ", $fields) . " where $key = " . id();
				
					
				mysql_query($sql) or dberror($sql);

				
                foreach ($this->items as $item)
                {
                    if ($item["type"] == "checklist")
                    {
                        $sql = "delete from " . $item["table"] . " where " . $item["master_key"] . " = " . id();
                        mysql_query($sql) or dberror($sql);
                    }
                }
            }
            else
            {
                $fields = array();
                $values = array();

                foreach ($this->items as $item)
                {
                    if ($item["flags"] & DATABASE)
                    {
                        $value = $item["dbtype"] == TYPE_DATE ? from_system_date($item["value"]) : $item["value"];
                        $fields[] = $item["name"];
                        $values[] = dbquote($value, true);
                    }
                }

                $fields[] = "user_created";
                $values[] = dbquote(user_login());

                $fields[] = "date_created";
                $values[] = "current_timestamp";

                $fields[] = "user_modified";
                $values[] = dbquote(user_login());

                $fields[] = "date_modified";
                $values[] = "current_timestamp";
                
                $sql = "insert into $this->table (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

                mysql_query($sql) or dberror($sql);
                $_REQUEST["id"] = mysql_insert_id();
            }
        }

        foreach ($this->items as $item)
        {
            if ($item["type"] == "checklist")
            {
                if (count($item["value"]) > 0 and is_array($item["value"]))
                {
					foreach ($item["value"] as $value)
                    {
                        $fields = array($item["master_key"], $item["lookup_key"], "date_created", "user_created", "date_modified", "user_modified");
                        $values = array(id(), $value, "current_timestamp", dbquote(user_login()), "current_timestamp", dbquote(user_login()));

                        $sql = "insert into " . $item["table"] . "(" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
                        mysql_query($sql) or dberror($sql);
                    }
                }
            }
        }

    }

    function populate()
    {
        global $_SERVER;
        global $_REQUEST;

        $this->read_types();

        if ($this->fresh())
        {
            if (isset($_REQUEST["id"]))
            {
                $fields = array();

                foreach ($this->items as $item)
                {
                    if ($item["flags"] & DATABASE)
                    {
                        $fields[] = $item["name"];
                    }
                }

                if (count($fields))
                {
                    $sql = "select " . join(", ", $fields) . " " .
                           "from $this->table " .
                           "where " . table_key($this->table) . " = " . (int)$_REQUEST["id"];

                    $res = mysql_query($sql) or dberror($sql);

                    if (mysql_num_rows($res))
                    {
                        $row = mysql_fetch_assoc($res);

                        foreach (array_keys($row) as $field)
                        {
                            $value = $this->format_value($this->items[$field], $row[$field]);
                            $this->items[$field]["value"] = $value;
                        }
                    }

                    foreach ($this->items as $item)
                    {
                        if ($item["type"] == "checklist")
                        {
                            $values = array();

                            $sql = "select " . $item["lookup_key"] . " from " . $item["table"] . " where " . $item["master_key"] . " = " . id();
                            $res = mysql_query($sql) or dberror($sql);

                            while ($row = mysql_fetch_row($res))
                            {
                                $values[] = $row[0];
                            }

                            $this->items[$item["name"]]["value"] = $values;
                        }
                    }
                }
            }
        }
        else
        {
            foreach ($this->items as $item)
            {
                $name = preg_replace("/[ \.]/", "_", $item["name"]);

                if (isset($item["name"]) && isset($_REQUEST[$name]))
                {
                    if ($item["type"] == "upload")
                    {
                        $file = $_REQUEST[$item["name"]];
                        $current_name = $_REQUEST["__" . $item["name"]];

                        if ($file["name"])
                        {
                            
							if(!in_array(strtolower(pathinfo( $file["name"], PATHINFO_EXTENSION)), $this->invalid_file_extensions))
							{
									
							
								$path = combine_paths($_SERVER["DOCUMENT_ROOT"], $item["path"]);
								//$path = preg_replace("/\\//", "\\", $path);

								if (!create_directory($path))
								{
									error("Could not create directory \"$path\" (Form::populate).");
								}

								$name = make_valid_filename($file["name"]);
								$name = combine_paths($item["path"], $name);
								
								if(file_exists(combine_paths($_SERVER["DOCUMENT_ROOT"], $name)))
								{
									$name = make_unique_filename($name, $_SERVER["DOCUMENT_ROOT"]);
								}
								
								/*
								if ($name != $current_name)
								{
									$name = make_unique_filename($name, $_SERVER["DOCUMENT_ROOT"]);
									if ($current_name)
									{
										if(file_exists(combine_paths($_SERVER["DOCUMENT_ROOT"], $current_name)))
										{
											unlink(combine_paths($_SERVER["DOCUMENT_ROOT"], $current_name));
										}
									}
								}
								*/

								copy($file["tmp_name"], combine_paths($_SERVER["DOCUMENT_ROOT"], $name));

								$this->items[$item["name"]]["value"] = $name;
							}
							else
							{
								$this->items[$item["name"]]["value"] = "";
							}
                        }
                        else
                        {
                            $this->items[$item["name"]]["value"] = $current_name;
                        }
                    }
                    else
                    {
                        $this->items[$item["name"]]["value"] = $_REQUEST[$name];
                    }
                }
                // handle date selector - fields
                else if (isset($item["name"]) && isset($_REQUEST[$name. "_year"]))
                {
                    if (empty($_REQUEST[$name ."_day"]) 
                        && empty($_REQUEST[$name ."_month"])
                        && empty($_REQUEST[$name ."_year"]))
                    {
                        $this->items[$item["name"]]["value"] = "";
                    }
                    else
                    {
                        $this->items[$item["name"]]["value"] = sprintf("%02d.%02d.%02d",
                                                                $_REQUEST[$name ."_day"],
                                                                $_REQUEST[$name ."_month"], 
                                                                $_REQUEST[$name ."_year"]);
                    }
                }
            }
        }
    }

    function fresh()
    {
        global $_REQUEST;

        return !isset($_REQUEST["action"]);
    }

    function button($name)
    {
        global $_REQUEST;

        return isset($_REQUEST["action"]) && $_REQUEST["action"] == $name;
    }

    function value($name, $value = null)
    {
        if (!isset($this->items[$name]))
        {
			error("Invalid item \"$name\" (Form::value).");
        }

        if (!is_null($value))
        {
            $this->items[$name]["value"] = $value;
        }

        return $this->items[$name]["value"];
    }

    function add_hidden($name, $value = "", $flags = 0)
    {
        $this->items[$name] = array("type" => "hidden",
                                    "name" => $name,
                                    "value" => $value,
                                    "flags" => $flags);
    }

    function add_section($title = "", $flags = 0)
    {
        $name = "__noname__" . ++$this->noname_index;

        $this->items[$name] = array("type" => "section",
                                    "name" => $name,
                                    "caption" => $title,
                                    "flags" => $flags);
    }

    function add_comment($comment, $flags = 0)
    {
        $name = "__noname__" . ++$this->noname_index;

        $this->items[$name] = array("type" => "comment",
                                    "name" => $name,
                                    "caption" => $comment,
                                    "flags" => $flags);
    }

	function add_table($tabledata, $flags = 0)
    {
        $name = "__noname__" . ++$this->noname_index;

        $this->items[$name] = array("type" => "table",
                                    "name" => $name,
                                    "caption" => $tabledata,
                                    "flags" => $flags);
    }

    function add_label($name, $caption, $flags = 0, $value = "")
    {
        $this->items[$name] = array("type" => "label",
                                    "name" => $name,
                                    "caption" => $caption,
                                    "value" => $value,
                                    "flags" => $flags);
    }

	function add_label_selector($name, $caption, $flags = 0, $value = "", $icon = "", $link="")
    {
        $this->items[$name] = array("type" => "label_selector",
                                    "name" => $name,
                                    "caption" => $caption,
                                    "value" => $value,
                                    "icon" => $icon, 
									"link" => $link,
									"flags" => $flags);
    }
	

	function add_modal_selector($name, $caption, $height, $flags = 0, $value = "")
    {
        $this->items[$name] = array("type" => "modal_selector",
                                    "name" => $name,
                                    "height" => $height, 
			                        "caption" => $caption,
			                        "value" => $value,
                                    "flags" => $flags);
    }


	function add_iconlink($name, $caption, $flags = 0, $value = "", $icon = "", $link = "", $target="")
    {
        $this->items[$name] = array("type" => "iconlink",
                                    "name" => $name,
                                    "caption" => $caption,
                                    "value" => $value,
                                    "flags" => $flags, 
									"icon" => $icon, 
			                        "link" => $link, 
			                        "target" => $target);
    }

    function add_lookup($name, $caption, $table, $field, $flags = 0, $value = 0)
    {
        $this->items[$name] = array("type" => "lookup",
                                    "name" => $name,
                                    "caption" => $caption,
                                    "table" => $table,
                                    "field" => $field,
                                    "value" => $value,
                                    "flags" => $flags);
    }

    function add_edit($name, $caption, $flags = 0, $value = "", $type = TYPE_CHAR, $length = 0, $length2 = 0, $helper = 0, $helper_name = "", $print_out = "")
    {
        $this->items[$name] = array("type" => "edit",
                                    "name" => $name,
                                    "caption" => $caption,
                                    "value" => $value,
                                    "dbtype" => $type,
                                    "length" => $length,
                                    "length2" => $length2,
                                    "helper" => $helper,
			                        "helper_name" => $helper_name,
									"print_out" => $print_out,
									"flags" => $flags | EDITABLE | STRING);
    }

	function add_input_submit($name, $caption, $flags = "")
    {
        $this->items[$name] = array("type" => "input_submit",
                                    "name" => $name,
                                    "caption" => $caption, 
			                        "flags" => $flags);
    }

    function add_multiline($name, $caption, $height, $flags = 0, $value = "", $helper = 0, $helper_name = "", $cols = "")
    {
        $this->items[$name] = array("type" => "multiline",
                                    "name" => $name,
                                    "caption" => $caption,
                                    "value" => $value,
                                    "dbtype" => TYPE_CHAR,
                                    "length" => 0,
                                    "height" => $height,
			                        "helper" => $helper,
                                    "helper_name" => $helper_name,
                                    "cols" => $cols,
                                    "flags" => $flags | EDITABLE | STRING);
    }

    function add_checkbox($name, $caption, $value = 0, $flags = 0, $caption1 = "", $render_input_field = true)
    {
        $this->items[$name] = array("type" => "checkbox",
                                    "name" => $name,
                                    "caption" => $caption,
                                    "value" => $value,
                                    "dbtype" => TYPE_INT,
                                    "length" => 4,
                                    "flags" => $flags | EDITABLE, 
                                    "caption1" => $caption1, 
			                        "render_input_field" => $render_input_field);
    }

    function add_radiolist($name, $caption, $choices, $flags = 0, $value = 0)
    {
        $this->items[$name] = array("type" => "radiolist",
                                    "name" => $name,
                                    "caption" => $caption,
                                    "value" => $value,
                                    "dbtype" => TYPE_INT,
                                    "length" => 10,
                                    "choices" => $choices,
                                    "flags" => $flags | EDITABLE);
    }

    function add_radiolist_2($name, $caption, $choices, $flags = 0, $value = 0)
    {
        $this->items[$name] = array("type" => "radiolist_2",
                                    "name" => $name,
                                    "caption" => $caption,
                                    "value" => $value,
                                    "dbtype" => TYPE_INT,
                                    "length" => 10,
                                    "choices" => $choices,
                                    "flags" => $flags | EDITABLE);
    }

    function add_list($name, $caption, $choices, $flags = 0, $value = 0, $helper = 0, $helper_name = "")
    {
        $this->items[$name] = array("type" => "list",
                                    "name" => $name,
                                    "caption" => $caption,
                                    "value" => $value,
                                    "choices" => $choices,
                                    "dbtype" => TYPE_INT,
                                    "length" => 10,
			                        "helper" => $helper,
			                        "helper_name" => $helper_name,
                                    "flags" => $flags | EDITABLE);
    }

	function add_charlist($name, $caption, $choices, $flags = 0, $value = 0, $helper = 0, $helper_name = "")
    {
        $this->items[$name] = array("type" => "list",
                                    "name" => $name,
                                    "caption" => $caption,
                                    "value" => $value,
                                    "choices" => $choices,
                                    "dbtype" => TYPE_CHAR,
                                    "length" => 10,
			                        "helper" => $helper,
			                        "helper_name" => $helper_name,
                                    "flags" => $flags | EDITABLE);
    }

    function add_checklist($name, $caption, $table, $sql, $flags = 0, $values = array(), $accordeon = false)
    {
        if (preg_match("/\s+show|from\s+(.*?)(\s+|$)/i", $sql, $matches))
        {
            $lookup_table = $matches[1];
        }
        else
        {
            error("Invalid SQL statement (Form::add_checklist).");
        }

        if ($this->table)
        {
            $master_key = table_reference($table, $this->table) or error("Table \"$table\" has no reference to master table (Form::add_checklist).");
        }
        else
        {
            $master_key = "";
        }

        $lookup_key = table_reference($table, $lookup_table) or error("Table \"$table\" has no reference to lookup table (Form::add_checklist).");

        $this->items[$name] = array("type" => "checklist",
                                     "name" => $name,
                                     "caption" => $caption,
                                     "table" => $table,
                                     "sql" => $sql,
                                     "master_key" => $master_key,
                                     "lookup_key" => $lookup_key,
                                     "value" => $values,
                                     "flags" => $flags, 
			                         "accordeon" => $accordeon);
    }

    function add_upload($name, $caption, $path, $flags = 0, $filname = "", $helper = 0, $helper_name = "")
    {
        global $__needs_multipart_form;

        $__needs_multipart_form = true;

        $this->items[$name] = array("type" => "upload",
                                    "name" => $name,
                                    "caption" => $caption,
                                    "value" => $filname,
                                    "dbtype" => TYPE_CHAR,
									"path" => $path,
			                        "helper" => $helper,
			                        "helper_name" => $helper_name,
                                    "flags" => $flags | EDITABLE | STRING);
    }

    // new 2003-05-09
    function add_date($name, $caption, $flags = 0, $value = 0)
    {
        $this->items[$name] = array("type" => "date",
                                    "name" => $name,
                                    "caption" => $caption,
                                    "value" => $value,
                                    "flags" => $flags | EDITABLE,
                                    "dbtype" => TYPE_DATE);
    }

    function add_subtable($name, $caption, $view, $edit, $sql, $sort_field = "", $delete_button = "")
    {
        if (preg_match("/\bwhere\s+(.*)\s+(group|order)/i", $sql, $matches))
        {
            $filter = $matches[1];
        }
        else if (preg_match("/\bwhere\s+(.*)$/i", $sql, $matches))
        {
            $filter = $matches[1];
        }
        else
        {
            $filter = "";
        }
            
        if (preg_match("/\border\s+by\s+(.*)$/i", $sql, $matches))
        {
            $order = $matches[1];
        }
        else
        {
            $order = "";
        }

        if (preg_match("/^(.*?)\s+(where|order by)\b/i", $sql, $matches))
        {
            $sql = $matches[1];
        }

        $list = new ListView($sql, LIST_HAS_HEADER);

        $list->set_filter($filter);
        $list->set_order($order);

        if ($sort_field)
        {
            $list->set_sorter($sort_field);
        }

        if ($delete_button)
        {
            $list->set_delete_button($delete_button);
        }

        $list->set_title($caption);
        $list->set_entity($name);

        if (preg_match("/^\s*select\b(.*)\bfrom\b/i", $sql, $matches))
        {
            $fields = $matches[1];

            if (preg_match("/^\s*\S+\s*,\s*(\S+)\s*,/i", $fields, $matches))
            {
                $list->set_edit_key($matches[1]);
            }

            preg_match_all("/\bas\s*[\"\']?([^\"\',]*)[\"\']?/i", $fields, $matches);

            foreach ($matches[1] as $match)
            {
                $field = trim($match);
                $url = $edit == $field ? $view : "";
                $list->add_column($field, $field, $url);
            }
        }
        else
        {
            error("SQL queries for subtables must contain \"as\" column titles (Form::render).");
        }

        $this->subtables[$name] = array("name" => $name,
                                        "caption" => $caption,
                                        "view" => $view,
                                        "edit" => $edit,
                                        "list" => $list,
                                        "sql" => $sql);
    }

    function add_button($name, $caption, $view = "", $flags = 0)
    {
        $this->buttons[$name] = array("name" => $name,
                                      "caption" => $caption,
                                      "view" => $view,
                                      "flags" => $flags);
    }

    function add_validation($rule, $error)
    {
        $this->rules[] = array("rule" => $rule,
                               "error" => $error);
    }

	function add_rating($name, $caption, $value = 0, $flags = 0)
    {
        $this->items[$name] = array("type" => "rating",
                                    "name" => $name,
                                    "caption" => $caption,
			                        "value" => $value,
			                        "flags" => $flags);
    }

	function add_rating_selector($name, $caption, $ratings = array(), $value, $flags = 0)
    {
        $this->items[$name] = array("type" => "rating_selector",
                                    "name" => $name,
									"caption" => $caption,
			                        "ratings" => $ratings,
									"value" => $value,
			                        "flags" => $flags);
    }

    function read_types()
    {
        if ($this->table)
        {
            $sql = "show fields from " . $this->table;
            $res = mysql_query($sql) or dberror($sql);

            while ($row = mysql_fetch_assoc($res))
            {
                foreach ($this->items as $item)
                {
                    if ($row["Field"] == $item["name"])
                    {
                        $type = $row["Type"];
                        $length = 0;
                        $length2 = 0;

                        if (preg_match("/(?:var)?char\((\d+)\)/i", $type, $matches))
                        {
                            $type = TYPE_CHAR;
                            $length = $matches[1];
                        }
                        else if (preg_match("/(tinytext|tinyblob|text|blob|mediumtext|mediumblob|longtext|longblob)/i", $type))
                        {
                            $type = TYPE_CHAR;
                        }
                        else if (preg_match("/(?:tinyint|smallint|mediumint|bigint|int|year)\((\d+)\)/i", $type, $matches))
                        {
                            $type = TYPE_INT;
                            $length = $matches[1];
                        }
                        else if (preg_match("/(float|double)/i", $type))
                        {
                            $type = TYPE_REAL;
                            $length = 0;
                        }
                        else if (preg_match("/decimal\((\d+),(\d+)\)/i", $type, $matches))
                        {
                            $type = TYPE_DECIMAL;
                            $length = $matches[1];
                            $length2 = $matches[2];
                        }
                        else if ($type == "date")
                        {
                            $type = TYPE_DATE;
                            $length = 10;
                        }
                        else if ($type == "time")
                        {
                            $type = TYPE_TIME;
                            $length = 8;
                        }
                        else if ($type == "datetime")
                        {
                            $type = TYPE_DATETIME;
                            $length = 19;
                        }
                        else
                        {
                            error("Unknown field type \"$type\" (Form::read_types).");
                        }

                        $this->items[$item["name"]]["dbtype"] = $type;
                        $this->items[$item["name"]]["length"] = $length;
                        $this->items[$item["name"]]["length2"] = $length2;
                        $this->items[$item["name"]]["flags"] = $item["flags"] | DATABASE;
                    }
                }
            }
        }
    }

    function format_value($item, $value)
    {
        if ($item["dbtype"] == TYPE_TIME && $item["flags"] & SHORT)
        {
            $value = substr($value, 5);
        }
        else if ($item["dbtype"] == TYPE_DATE)
        {
            $value = to_system_date($value);
        }

        return $value;
    }

    function check_value($item)
    {
		$valid = true;

        switch ($item["dbtype"])
        {
            case TYPE_CHAR:
                break;
            case TYPE_INT:
                $valid = is_int_value($item["value"], $item["length"]);
                break;
            case TYPE_REAL:
                $valid = is_real_value($item["value"]);
                break;
            case TYPE_DECIMAL:
                $valid = is_decimal_value($item["value"], $item["length"], $item["length2"]);
                break;
            case TYPE_DATE:
                $valid = is_date_value($item["value"]);
                break;

            case TYPE_TIME:
                $valid = is_time_value($item["value"]);
                break;

            case TYPE_DATETIME:
                $valid = is_datetime_value($item["value"]);
                break;
        }

        return $valid;
    }

    function prepare_sql($sql)
    {
        foreach ($this->items as $item)
        {
            if ($item["flags"] & DATABASE)
            {
                $sql = preg_replace("/\{" . $item["name"] . "\}/i", dbquote($item["value"]), $sql);
            }
        }

        return $sql;
    }

    function draw_gap()
    {
        if (!$this->has_vgap && $this->last_item)
        {
            echo "<tr>";
            echo "<td colspan=\"3\" height=\"10\"></td>";
            echo "</tr>";

            $this->has_vgap = true;
        }
    }

    function draw_line($content, $class = "")
    {
        echo "<tr>";

        if ($class)
        {

			echo "<td nowrap='nowrap' colspan=\"4\" class=\"$class\">";
            //echo smart(htmlspecialchars($content));
			echo $content;
            echo "</td>";
        }
        else
        {
            echo "<td  colspan=\"4\">";
            echo $content;
            echo "</td>";
        }

        echo "</tr>";

        $this->has_vgap = false;
    }

	function draw_line_lbr($content, $class = "")
    {
        echo "<tr>";

        if ($class)
        {
            echo "<td colspan=\"4\" class=\"$class\">";
            echo smart(htmlspecialchars($content));
            echo "</td>";
        }
        else
        {
            echo "<td  colspan=\"4\">";
            echo $content;
            echo "</td>";
        }

        echo "</tr>";

        $this->has_vgap = false;
    }

    /*
	function draw_line2($label, $content)
    {
        if (trim($label))
        {
            //$label = htmlspecialchars($label) . ":";
			$label = $label . ":";
        }
        else
        {
            $label = "&nbsp;";
        }

        echo "<tr valign=\"top\">";
        echo "<td nowrap='nowrap' bgcolor=\"#F6F6FF\" height=\"24\" nowrap='nowrap'>";
        echo "<span class=\"label\">";
        echo $label;
        echo "</span>";
        echo "</td>";

        echo "<td nowrap='nowrap' width=\"10\" bgcolor=\"#F6F6FF\" nowrap='nowrap'>";
        echo $this->has_hgap ? "&nbsp;" : "<img src=\"/pictures/spacer.gif\" width=\"10\" height=\"1\" alt=\"\" />";
        echo "</td>";

        echo "<td nowrap='nowrap' bgcolor=\"#F6F6FF\" width=\"100%\">";
        echo $content;
        echo "</td>";

        echo "<td nowrap='nowrap' width=\"10\" bgcolor=\"#F6F6FF\" nowrap='nowrap'>";
        echo $this->has_hgap ? "&nbsp;" : "<img src=\"/pictures/spacer.gif\" width=\"10\" height=\"1\" alt=\"\" />";
        echo "</td>";

        echo "</tr>";

        $this->has_hgap = true;
        $this->has_vgap = false;
    }
	*/

	function draw_line2($label, $content)
    {
        if(is_array($label))
		{
			$label = "";
		}
		elseif (trim($label))
        {
            //$label = htmlspecialchars($label) . ":";
			$label = $label . ":";
        }
        else
        {
            $label = "&nbsp;";
        }

        echo "<tr valign=\"top\">";
        echo "<td bgcolor=\"#F6F6FF\" height=\"24\" nowrap='nowrap'>";
        echo "<span class=\"label\">";
        echo $label;
        echo "</span>";
        echo "</td>";

        echo "<td width=\"10\" bgcolor=\"#F6F6FF\" nowrap='nowrap'>";
        echo $this->has_hgap ? "&nbsp;" : "<img src=\"/pictures/spacer.gif\" width=\"10\" height=\"1\" alt=\"\" />";
        echo "</td>";

        echo "<td bgcolor=\"#F6F6FF\" width=\"100%\" nowrap='nowrap'>";
        echo $content;
        echo "</td>";

        echo "<td width=\"10\" bgcolor=\"#F6F6FF\" nowrap='nowrap'>";
        echo $this->has_hgap ? "&nbsp;" : "<img src=\"/pictures/spacer.gif\" width=\"10\" height=\"1\" alt=\"\" />";
        echo "</td>";

        echo "</tr>";

        $this->has_hgap = true;
        $this->has_vgap = false;
    }


	function draw_line2_lbr($label, $content)
    {
        if (trim($label))
        {
            //$label = htmlspecialchars($label) . ":";
			$label = $label . ":";
        }
        else
        {
            $label = "&nbsp;";
        }

        echo "<tr valign=\"top\">";
        echo "<td bgcolor=\"#F6F6FF\" height=\"24\">";
        echo "<span class=\"label\">";
        echo $label;
        echo "</span>";
        echo "</td>";

        echo "<td width=\"10\" bgcolor=\"#F6F6FF\">";
        echo $this->has_hgap ? "&nbsp;" : "<img src=\"/pictures/spacer.gif\" width=\"10\" height=\"1\" alt=\"\" />";
        echo "</td>";

        echo "<td bgcolor=\"#F6F6FF\" width=\"100%\">";
        echo $content;
        echo "</td>";

        echo "<td width=\"10\" bgcolor=\"#F6F6FF\">";
        echo $this->has_hgap ? "&nbsp;" : "<img src=\"/pictures/spacer.gif\" width=\"10\" height=\"1\" alt=\"\" />";
        echo "</td>";

        echo "</tr>";

        $this->has_hgap = true;
        $this->has_vgap = false;
    }


	function draw_table_line($content, $class = "")
    {
        echo "<tr>";

        if ($class)
        {
            echo "<td colspan=\"4\" class=\"$class\">";
            //echo smart(htmlspecialchars($content));
			echo $content;
            echo "</td>";
        }
        else
        {
            echo "<td colspan=\"4\">";
            echo $content;
            echo "</td>";
        }

        echo "</tr>";
		

        $this->has_vgap = false;
    }
	

}

/********************************************************************
    DeleteAction class
*********************************************************************/

class DeleteAction
{
    var $table;
    var $id;
    var $entity;
    var $back_on_success;
    var $back_on_failure;
    var $back_on_abort;
    var $question;
    var $confirmation;
    var $failure;

    function DeleteAction($table, $id)
    {
        global $_SERVER;

        $this->table = $table;
        $this->id = $id;

        $this->back_on_success = $_SERVER["HTTP_REFERER"];
        $this->back_on_failure = build_page_url();
        $this->back_on_abort = build_page_url();

        $this->entity("record");
    }

    function entity($name)
    {
        $this->entity = $name;
        $this->question = "Do you really want to delete this $name?";
        $this->confirmation = "The $name has been deleted.";
        $this->failure = "The $name could not be deleted because there are other records depending on it.";
    }

    function back_on_success($url)
    {
        $this->back_on_success = $url;
    }

    function back_on_failure($url)
    {
        $this->back_on_failure = $url;
    }

    function back_on_abort($url)
    {
        $this->back_on_abort = $url;
    }
}

?>