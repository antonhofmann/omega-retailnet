<?php

/********************************************************************
    openfile.php

    Opens a file

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2010-02-26
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2010-02-26
    Version:        1.0.0

    Copyright (c) 2010, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";

if(array_key_exists("REQUEST_URI", $_SERVER)
    and strpos($_SERVER["REQUEST_URI"], 'links') > 0
	and user_id() > 0
  )
{
	$finfo = new finfo; 
	$tmp = $finfo->file($_SERVER["DOCUMENT_ROOT"] . $_SERVER["REQUEST_URI"], FILEINFO_MIME);
	$tmp = str_replace(' ', '', $tmp);
	$parts = explode(";", $tmp);
	if(array_key_exists(0, $parts))
	{
	
		$mimetype = $parts[0];

		$path_parts = explode("/", $_SERVER["REQUEST_URI"]);
		$filename = $path_parts[count($path_parts)-1];

		$filepath = $_SERVER["DOCUMENT_ROOT"] . $_SERVER["REQUEST_URI"];

		header("Content-Type: $mimetype");

		header("Content-Disposition: attachment; filename=$filename");
		
		header('Pragma: no-cache');
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Accept-Ranges: bytes');
		echo file_get_contents($filepath);
	}
	
}
elseif(has_access("can_view_attachments_in_orders") 
	or has_access("can_view_attachments_in_projects")
	or has_access("has_access_to_flikflak")
	or has_access("can_use_posindex")
	or has_access("has_access_to_cer")) 
{
	if(array_key_exists("path", $_GET) or array_key_exists("id", $_GET))
	{
		//allowed Get-parameters
		if(array_key_exists("path", $_GET))
		{
			if (strpos(strtolower($_GET["path"]),'.php') == true)
			{
				require_once($_SERVER["DOCUMENT_ROOT"] . '/page_not_found.php');
				die;
			}
		}
	}
	else
	{
		
		require_once($_SERVER["DOCUMENT_ROOT"] . '/page_not_found.php');
		die;
	}


	$filename = "";
	$filepath = "";
	$mimetype = "";

	if(isset($_GET["id"]))
	{
		$sql = "select * from order_files " . 
			   "left join file_types on file_type_id = order_file_type " . 
			   "where order_file_id = '" . $_GET["id"] ."'";
	}
	else
	{
		$sql = "select * from order_files " . 
			   "left join file_types on file_type_id = order_file_type " . 
			   "where order_file_path = '" . $_GET["path"] . "'";

	}

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$path_parts = explode("/", $row["order_file_path"]);
		$filename = $path_parts[count($path_parts)-1];
		$mimetype = $row["file_type_mime_type"];
		$filepath = $_SERVER["DOCUMENT_ROOT"] . $row["order_file_path"];
	}
	else
	{
		$filepath = $_SERVER["DOCUMENT_ROOT"] . $_SERVER['REQUEST_URI'];
	}

	//redirect($filepath);
	if(file_exists($filepath))
	{
		header("Content-Type: $mimetype");
		/*
		if (strpos($_SERVER["HTTP_USER_AGENT"],'MSIE')!==false)
		{
			header("Content-Disposition: inline; filename=$filename");
		}
		else
		{
			header("Content-Disposition: attachment; filename=$filename");
		}
		*/
		header("Content-Disposition: inline; filename=$filename");
		
		header('Pragma: no-cache');
		header('Content-Transfer-Encoding: binary');
		header('Accept-Ranges: bytes');
		echo file_get_contents($filepath);
	}
	exit;
}
else
{
    redirect("/user/login.php");

}
die;

?>