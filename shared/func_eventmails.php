<?php
/********************************************************************

    func_eventmails.php

    Various functions for Event Mails

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2011-12-18
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2011-12-18
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/


/********************************************************************
    Send event mail for projects
*********************************************************************/
function send_project_event_mail($sender_id, $code, $project)
{

	$today = date("Y-m-d");

	//get sender
	$sql = "select user_id, user_email, user_name, user_firstname , " .
		   "concat(user_name, ' ', user_firstname) as sender_name " .
		   "from users where user_id = " . dbquote($sender_id);


	$res = mysql_query($sql) or dberror($sql);
    if($row = mysql_fetch_assoc($res))
	{
		$sender_name = $row['sender_name'];
		$sender_firstname = $row['user_firstname'];
		$sender_lastname = $row['user_name'];
		$sender_email = $row['user_email'];

		$sql = " select * from eventmails where eventmail_code = " . dbquote($code);

		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$subject = $row["eventmail_subject"];
			$text = $row["eventmail_text"];
			$user01 = $row["eventmail_user01"];
			$user02 = $row["eventmail_user02"];
			$user03 = $row["eventmail_user03"];
			$user04 = $row["eventmail_user04"];

			if($user01 == NULL) {$user01 = 0;}
			if($user02 == NULL) {$user02 = 0;}
			if($user03 == NULL) {$user03 = 0;}
			if($user04 == NULL) {$user04 = 0;}

			$subject = MAIL_SUBJECT_PREFIX . ": Project " . $project["order_number"] . ": " . $subject . " for " .  $project["order_shop_address_company"];


			$text = str_replace("\r\n", "\n", $text) . "\n\n";
			$link ="project_task_center.php?pid=" . $project["project_id"];
			$link = APPLICATION_URL ."/user/" . $link; 
			
			$text = str_replace("{link}", $link, $text);
			$text = str_replace("{sender_firstname}", $sender_firstname, $text);
			$text = str_replace("{sender_name}", $sender_lastname, $text);

			
			$mail = new PHPMailer();
			$mail->Subject = $subject;
			$mail->SetFrom($sender_email, $sender_name);
			$mail->AddReplyTo($sender_email, $sender_name);

			$rcpts = "";
			$recipient_ids = array();
			
			$i = 1;
			$sql = "select user_id, user_email, user_firstname, " . 
				   "concat(user_name, ' ', user_firstname) as user_name " .
		           "from users where user_id IN (" . $user01 . "," . $user02 . "," . $user03 . "," . $user04 . ")";

			$res = mysql_query($sql) or dberror($sql);
			while($row = mysql_fetch_assoc($res))
			{
				if($row["user_email"] and $i == 1) {
					$mail->AddAddress($row["user_email"], $row["user_name"]);
					$rcpts .= $value . "\n";
					$i++;

					$text = str_replace("{recipient_fistname}",  $row["user_firstname"], $text);
					$recipient_ids[] = $row["user_id"];

					
				}
				elseif($row["user_email"] and $i > 1) {
					$mail->AddCC($row["user_email"], $row["user_name"]);
					$rcpts .= $value . "\n";
					$i++;
					$recipient_ids[] = $row["user_id"];
				}

			}

			$mail->Body = $text;

			if($rcpts) {			
				$mail->Send();
				
				foreach($recipient_ids as $key=>$recipient_id) {
					append_mail($project['project_order'], $recipient_id, $sender_id, $text, '900', 1,0);
				}
			}

		}	
	}

	return true;
}



?>