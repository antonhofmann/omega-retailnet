<?php
/********************************************************************

    func_mailalerts.php

    Various functions for Mail Alerts

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-11-16
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-11-16
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

$mimetypes = array();
$mimetype['ai']='application/postscript';
$mimetype['aif']='audio/x-aiff';
$mimetype['aifc']='audio/x-aiff';
$mimetype['aiff']='audio/x-aiff';
$mimetype['arj']='application/x-arj-compressed';
$mimetype['asc']='text/plain';
$mimetype['asc txt']='text/plain';
$mimetype['asf']='video/x-ms-asf';
$mimetype['asx']='video/x-ms-asx';
$mimetype['au']='audio/basic';
$mimetype['au']='audio/ulaw';
$mimetype['avi']='video/x-msvideo';
$mimetype['bat']='application/x-msdos-program';
$mimetype['bcpio']='application/x-bcpio';
$mimetype['bin']='application/octet-stream';
$mimetype['c']='text/plain';
$mimetype['cc']='text/plain';
$mimetype['ccad']='application/clariscad';
$mimetype['cdf']='application/x-netcdf';
$mimetype['class']='application/octet-stream';
$mimetype['cod']='application/vnd.rim.cod';
$mimetype['com']='application/x-msdos-program';
$mimetype['cpio']='application/x-cpio';
$mimetype['cpt']='application/mac-compactpro';
$mimetype['csh']='application/x-csh';
$mimetype['css']='text/css';
$mimetype['dcr']='application/x-director';
$mimetype['deb']='application/x-debian-package';
$mimetype['dir']='application/x-director';
$mimetype['dl']='video/dl';
$mimetype['dms']='application/octet-stream';
$mimetype['doc']='application/msword';
$mimetype['drw']='application/drafting';
$mimetype['dvi']='application/x-dvi';
$mimetype['dwg']='application/acad';
$mimetype['dxf']='application/dxf';
$mimetype['dxr']='application/x-director';
$mimetype['eps']='application/postscript';
$mimetype['etx']='text/x-setext';
$mimetype['exe']='application/octet-stream';
$mimetype['exe']='application/x-msdos-program';
$mimetype['ez']='application/andrew-inset';
$mimetype['f']='text/plain';
$mimetype['f90']='text/plain';
$mimetype['fli']='video/fli';
$mimetype['fli']='video/x-fli';
$mimetype['flv']='video/flv';
$mimetype['gif']='image/gif';
$mimetype['gl']='video/gl';
$mimetype['gtar']='application/x-gtar';
$mimetype['gz']='application/x-gunzip';
$mimetype['gz']='application/x-gzip';
$mimetype['h']='text/plain';
$mimetype['hdf']='application/x-hdf';
$mimetype['hh']='text/plain';
$mimetype['hqx']='application/mac-binhex40';
$mimetype['htm']='text/html';
$mimetype['html']='text/html';
$mimetype['html htm']='text/html';
$mimetype['ice']='x-conference/x-cooltalk';
$mimetype['ief']='image/ief';
$mimetype['iges']='model/iges';
$mimetype['igs']='model/iges';
$mimetype['ips']='application/x-ipscript';
$mimetype['ipx']='application/x-ipix';
$mimetype['jad']='text/vnd.sun.j2me.app-descriptor';
$mimetype['jar']='application/java-archive';
$mimetype['jpe']='image/jpeg';
$mimetype['jpeg']='image/jpeg';
$mimetype['jpg']='image/jpeg';
$mimetype['js']='application/x-javascript';
$mimetype['kar']='audio/midi';
$mimetype['latex']='application/x-latex';
$mimetype['lha']='application/octet-stream';
$mimetype['lsp']='application/x-lisp';
$mimetype['lzh']='application/octet-stream';
$mimetype['m']='text/plain';
$mimetype['m3u']='audio/x-mpegurl';
$mimetype['man']='application/x-troff-man';
$mimetype['me']='application/x-troff-me';
$mimetype['mesh']='model/mesh';
$mimetype['mid']='audio/midi';
$mimetype['midi']='audio/midi';
$mimetype['mif']='application/vnd.mif';
$mimetype['mif']='application/x-mif';
$mimetype['mime']='www/mime';
$mimetype['mov']='video/quicktime';
$mimetype['movie']='video/x-sgi-movie';
$mimetype['mp2']='audio/mpeg';
$mimetype['mp2']='video/mpeg';
$mimetype['mp3']='audio/mpeg';
$mimetype['mpe']='video/mpeg';
$mimetype['mpeg']='video/mpeg';
$mimetype['mpg']='video/mpeg';
$mimetype['mpga']='audio/mpeg';
$mimetype['ms']='application/x-troff-ms';
$mimetype['msh']='model/mesh';
$mimetype['nc']='application/x-netcdf';
$mimetype['oda']='application/oda';
$mimetype['ogg']='application/ogg';
$mimetype['ogm']='application/ogg';
$mimetype['pbm']='image/x-portable-bitmap';
$mimetype['pdb']='chemical/x-pdb';
$mimetype['pdf']='application/pdf';
$mimetype['pgm']='image/x-portable-graymap';
$mimetype['pgn']='application/x-chess-pgn';
$mimetype['pgp']='application/pgp';
$mimetype['pl']='application/x-perl';
$mimetype['pm']='application/x-perl';
$mimetype['png']='image/png';
$mimetype['pnm']='image/x-portable-anymap';
$mimetype['pot']='application/mspowerpoint';
$mimetype['ppm']='image/x-portable-pixmap';
$mimetype['pps']='application/mspowerpoint';
$mimetype['ppt']='application/mspowerpoint';
$mimetype['ppz']='application/mspowerpoint';
$mimetype['pre']='application/x-freelance';
$mimetype['prt']='application/pro_eng';
$mimetype['ps']='application/postscript';
$mimetype['qt']='video/quicktime';
$mimetype['ra']='audio/x-realaudio';
$mimetype['ram']='audio/x-pn-realaudio';
$mimetype['rar']='application/x-rar-compressed';
$mimetype['ras']='image/cmu-raster';
$mimetype['ras']='image/x-cmu-raster';
$mimetype['rgb']='image/x-rgb';
$mimetype['rm']='audio/x-pn-realaudio';
$mimetype['roff']='application/x-troff';
$mimetype['rpm']='audio/x-pn-realaudio-plugin';
$mimetype['rtf']='application/rtf';
$mimetype['rtf']='text/rtf';
$mimetype['rtx']='text/richtext';
$mimetype['scm']='application/x-lotusscreencam';
$mimetype['set']='application/set';
$mimetype['sgm']='text/sgml';
$mimetype['sgml']='text/sgml';
$mimetype['sh']='application/x-sh';
$mimetype['shar']='application/x-shar';
$mimetype['silo']='model/mesh';
$mimetype['sit']='application/x-stuffit';
$mimetype['skd']='application/x-koan';
$mimetype['skm']='application/x-koan';
$mimetype['skp']='application/x-koan';
$mimetype['skt']='application/x-koan';
$mimetype['smi']='application/smil';
$mimetype['smil']='application/smil';
$mimetype['snd']='audio/basic';
$mimetype['sol']='application/solids';
$mimetype['spl']='application/x-futuresplash';
$mimetype['src']='application/x-wais-source';
$mimetype['step']='application/STEP';
$mimetype['stl']='application/SLA';
$mimetype['stp']='application/STEP';
$mimetype['sv4cpio']='application/x-sv4cpio';
$mimetype['sv4crc']='application/x-sv4crc';
$mimetype['swf']='application/x-shockwave-flash';
$mimetype['t']='application/x-troff';
$mimetype['tar']='application/x-tar';
$mimetype['tar.gz']='application/x-tar-gz';
$mimetype['tcl']='application/x-tcl';
$mimetype['tex']='application/x-tex';
$mimetype['texi']='application/x-texinfo';
$mimetype['texinfo']='application/x-texinfo';
$mimetype['tgz']='application/x-tar-gz';
$mimetype['tif']='image/tiff';
$mimetype['tif tiff']='image/tiff';
$mimetype['tiff']='image/tiff';
$mimetype['tr']='application/x-troff';
$mimetype['tsi']='audio/TSP-audio';
$mimetype['tsp']='application/dsptype';
$mimetype['tsv']='text/tab-separated-values';
$mimetype['txt']='text/plain';
$mimetype['unv']='application/i-deas';
$mimetype['ustar']='application/x-ustar';
$mimetype['vcd']='application/x-cdlink';
$mimetype['vda']='application/vda';
$mimetype['viv']='video/vnd.vivo';
$mimetype['vivo']='video/vnd.vivo';
$mimetype['vrm']='x-world/x-vrml';
$mimetype['vrml']='model/vrml';
$mimetype['vrml']='x-world/x-vrml';
$mimetype['wav']='audio/x-wav';
$mimetype['wax']='audio/x-ms-wax';
$mimetype['wma']='audio/x-ms-wma';
$mimetype['wmv']='video/x-ms-wmv';
$mimetype['wmx']='video/x-ms-wmx';
$mimetype['wrl']='model/vrml';
$mimetype['wvx']='video/x-ms-wvx';
$mimetype['xbm']='image/x-xbitmap';
$mimetype['xlc']='application/vnd.ms-excel';
$mimetype['xll']='application/vnd.ms-excel';
$mimetype['xlm']='application/vnd.ms-excel';
$mimetype['xls']='application/excel';
$mimetype['xls']='application/vnd.ms-excel';
$mimetype['xlw']='application/vnd.ms-excel';
$mimetype['xml']='text/xml';
$mimetype['xpm']='image/x-xpixmap';
$mimetype['xwd']='image/x-xwindowdump';
$mimetype['xyz']='chemical/x-pdb';
$mimetype['zip']='application/x-zip-compressed';
$mimetype['zip']='application/zip';

/********************************************************************
    append records to table order_mails
*********************************************************************/
function append_mail($order_id, $recepient_id, $user_id, $text, $order_state_code, $type, $order_revision=0)
{

    $text = str_replace("\n\n", "\n", $text);
	$text = str_replace("\n\n", "\n", $text);

	$sql = "select order_state_id " .
           "from order_states ".
           "left join order_state_groups on order_state_group = order_state_group_id ".
           "where order_state_code = " . dbquote($order_state_code) .
           "    and order_state_group_order_type = " .$type;

    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);

    $order_mail_fields = array();
    $order_mail_values = array();
    
    // insert record into table tasks

    $order_mail_fields[] = "order_mail_order";
    $order_mail_values[] = $order_id;

    if ($recepient_id != "")
    {
        $order_mail_fields[] = "order_mail_user";
        $order_mail_values[] = $recepient_id;
    }

    $order_mail_fields[] = "order_mail_from_user";
    $order_mail_values[] = $user_id;

    $order_mail_fields[] = "order_mail_text";
    $order_mail_values[] = dbquote($text);

    if ($order_state_code != "")
    {
        $order_mail_fields[] = "order_mail_order_state";
        $order_mail_values[] = $row["order_state_id"];
    }

	$order_mail_fields[] = "order_revisioned";
    $order_mail_values[] = $order_revision;

    $order_mail_fields[] = "date_created";
    $order_mail_values[] = "current_timestamp";

    $order_mail_fields[] = "date_modified";
    $order_mail_values[] = "current_timestamp";

    if (isset($_SESSION["user_login"]))
    {
        $order_mail_fields[] = "user_created";
        $order_mail_values[] = dbquote($_SESSION["user_login"]);

        $order_mail_fields[] = "user_modified";
        $order_mail_values[] = dbquote($_SESSION["user_login"]);
    }

    $sql = "insert into order_mails (" . join(", ", $order_mail_fields) . ") values (" . join(", ", $order_mail_values) . ")";
    mysql_query($sql) or dberror($sql);
}


/********************************************************************
    Ask Retail Coordintator/Retail Operator 
	for entering/adapting the agreed shop opening date
*********************************************************************/
function ma_enter_realistic_sopd()
{

	$today = date("Y-m-d");

	//send mails for new projects
	$sql = "select * from mail_alert_types where mail_alert_type_id = 8";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email = $row["mail_alert_type_sender_email"];
	$sender_name = $row["mail_alert_type_sender_name"];
	$mail_text = $row["mail_alert_mail_text"];
    $mail_alert_type_id = 8;

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id = $row["user_id"];


	$sql = "select * from mail_alert_types where mail_alert_type_id = 12";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email2 = $row["mail_alert_type_sender_email"];
	$sender_name2 = $row["mail_alert_type_sender_name"];
	$mail_text2 = $row["mail_alert_mail_text"];
	$mail_alert_type_id2 = 12;

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email2 . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id2 = $row["user_id"];

	$sql = "select * from mail_alert_types where mail_alert_type_id = 14";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email3 = $row["mail_alert_type_sender_email"];
	$sender_name3 = $row["mail_alert_type_sender_name"];
	$mail_text3 = $row["mail_alert_mail_text"];
	$mail_alert_type_id3 = 14;

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email3 . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id3 = $row["user_id"];



	$sql = "select project_id, project_projectkind, project_real_opening_date, project_retail_coordinator, " .
		   "order_id, order_archive_date, order_number, order_retail_operator, " .
		   "order_shop_address_place,order_shop_address_company, order_actual_order_state_code, " .
		   "country_name " . 
		   "from projects " .
		   "left join orders on order_id = project_order " .
		   "left join countries on country_id = order_shop_address_country " . 
		   "where (select count(mail_alert_id) from mail_alerts where mail_alert_order = order_id and mail_alert_type IN (8, 12, 14))  = 0 " . 
		   " and (LEFT(order_number, 2) >= 26 or LEFT(order_number, 3) >= 210) " . 
		   " and project_state <> 2 " . 
		   " and order_actual_order_state_code <= '800' " . 
		   " and (project_real_opening_date is null " . 
		   "   or project_real_opening_date = '0000-00-00') " . 
		   " and (project_actual_opening_date is null " . 
		   "   or project_actual_opening_date = '0000-00-00') ";
	

    $res = mysql_query($sql) or dberror($sql);

    while($row = mysql_fetch_assoc($res))
    {
		if($row["project_retail_coordinator"] > 0)
		{
			
			if($row['project_projectkind'] == 4 ) //take over 
			{
				$subject = "Project's planned take over date is missing - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];

				$sender_email = $sender_email2; 
				$sender_name = $sender_name2;
				$mail_text = $mail_text2;
				$sender_id =  $sender_id2;
				$mail_alert_type_id = $mail_alert_type_id2;

			}
			elseif($row['project_projectkind'] == 5) //lease renewal
			{
				$subject = "Project's lease starting date is missing - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];

				$sender_email = $sender_email3; 
				$sender_name = $sender_name3;
				$mail_text = $mail_text3;
				$sender_id =  $sender_id3;
				$mail_alert_type_id = $mail_alert_type_id3;

			}
			else
			{
				$subject = "Agreed POS opening date is missing - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];
			}
			

			$mail = new Mail();
			$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
			$mail->set_sender($sender_email, $sender_name);


			//add cc and deputee
			$sql = "select user_id, user_firstname, user_email, user_email_cc, user_email_deputy ".
				   "from users ".
				   "where (user_id = " . $row["project_retail_coordinator"] . 
				   "   and user_active = 1)";
			
			$res1 = mysql_query($sql) or dberror($sql);
			$row1 = mysql_fetch_assoc($res1);
			$rtc_id = $row1["user_id"];
			$rtc_name = $row1["user_firstname"];
			$rtc_email = $row1["user_email"];

			$mail->add_recipient($row1["user_email"]);

			if($row1["user_email_cc"])
			{
				$mail->add_cc($row1["user_email_cc"]);
			}
			if($row1["user_email_deputy"])
			{
				$mail->add_cc($row1["user_email_deputy"]);
			}
			$mail->add_cc($sender_email);

			$sql = "select user_id, user_email ".
				   "from users ".
				   "where (user_id = " . $row["order_retail_operator"] . 
				   "   and user_active = 1)";
			$res1 = mysql_query($sql) or dberror($sql);
			$row1 = mysql_fetch_assoc($res1);
			$mail->add_cc($row1["user_email"]);
			$rto_id = $row1["user_id"];


			$bodytext0 = "Dear " . $rtc_name . "\n\n" . $mail_text;
			$link ="project_edit_pos_data.php?pid=" . $row["project_id"];
			$bodytext = $bodytext0 . "\nClick below to have direct access to the project:\n";
			$bodytext = $bodytext .  APPLICATION_URL ."/user/" . $link . "\n\n";           
			
			$bodytext .= "Thank you very much.\nKind regards, " . $sender_name;

			$mail->add_text($bodytext);

			
			
			//check if mail was sent meanwhile
			$sql_m = 'select count(mail_alert_id) as num_recs ' .
				     'from mail_alerts where mail_alert_type = 8 and mail_alert_order = ' . $row["order_id"];
			$res_m = mysql_query($sql_m) or dberror($sql_m);
			$row_m = mysql_fetch_assoc($res_m);

			if($row_m['num_recs'] == 0) {
			
				$result = $mail->send();

				if($result == 1)
				{
					append_mail($row["order_id"], $rtc_id, $sender_id, $bodytext0, "900", 1);
					append_mail($row["order_id"], $rto_id, $sender_id, $bodytext0, "900", 1);

					//update mail_alerts
					$fields = array();
					$values = array();

					$fields[] = "mail_alert_type";
					$values[] = $mail_alert_type_id;

					$fields[] = "mail_alert_order";
					$values[] = $row["order_id"];

					$fields[] = "mail_alert_date";
					$values[] = dbquote($today);

					$fields[] = "mail_alert_sender";
					$values[] = dbquote($sender_email);

					$fields[] = "mail_alert_recipient";
					$values[] = dbquote($rtc_email);

					$fields[] = "date_created";
					$values[] = "current_timestamp";

					$fields[] = "date_modified";
					$values[] = "current_timestamp";

					$fields[] = "user_created";
					$values[] = dbquote(user_login());

					$fields[] = "user_modified";
					$values[] = dbquote(user_login());

					$sql = "insert into mail_alerts (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					mysql_query($sql) or dberror($sql);
				}
			}
		}
    }

	//send mails for existing projects
	$sql = "select * from mail_alert_types where mail_alert_type_id = 1";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email = $row["mail_alert_type_sender_email"];
	$sender_name = $row["mail_alert_type_sender_name"];
	$mail_text = $row["mail_alert_mail_text"];
	$mail_alert_type_id = 1;

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id = $row["user_id"];

	$sql = "select * from mail_alert_types where mail_alert_type_id = 13";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email2 = $row["mail_alert_type_sender_email"];
	$sender_name2 = $row["mail_alert_type_sender_name"];
	$mail_text2 = $row["mail_alert_mail_text"];
	$mail_alert_type_id2 = 13;

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email2 . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id2 = $row["user_id"];


	$sql = "select * from mail_alert_types where mail_alert_type_id = 15";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email3 = $row["mail_alert_type_sender_email"];
	$sender_name3 = $row["mail_alert_type_sender_name"];
	$mail_text3 = $row["mail_alert_mail_text"];
	$mail_alert_type_id3 = 15;

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email3 . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id3 = $row["user_id"];


	


	$sql = "select project_id, project_projectkind, project_real_opening_date, project_retail_coordinator, " .
		   "order_id, order_archive_date, order_number, order_retail_operator, " .
		   "order_shop_address_place,order_shop_address_company, order_actual_order_state_code, " .
		   "country_name " . 
		   "from projects " .
		   "left join orders on order_id = project_order " .
		   "left join countries on country_id = order_shop_address_country " . 
		   "where (select count(mail_alert_id) from mail_alerts where mail_alert_order = order_id and mail_alert_type IN (1, 13, 15))  = 0 " . 
		   " and (LEFT(order_number, 2) >= 26 or LEFT(order_number, 3) >= 210) " . 
		   " and order_actual_order_state_code <= '800' " . 
		   " and project_real_opening_date <= '" . $today . "' " .
		   " and project_state <> 2 " .
		   " and (project_actual_opening_date is null " . 
		   "   or project_actual_opening_date = '0000-00-00') " .
		    " and (project_projectkind <> 4 and project_projectkind <> 5)";


    $res = mysql_query($sql) or dberror($sql);

    while($row = mysql_fetch_assoc($res))
    {
		if($row["project_retail_coordinator"] > 0)
		{
			
			if($row['project_projectkind'] == 4) //take over
			{
				$subject = "Project's planned take date is past due - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];

				$sender_email = $sender_email2; 
				$sender_name = $sender_name2;
				$mail_text = $mail_text2;
				$sender_id =  $sender_id2;
				$mail_alert_type_id = $mail_alert_type_id2;

			}
			elseif($row['project_projectkind'] == 5) //lease renewal
			{
				$subject = "Project's lease starting date is past due - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];

				$sender_email = $sender_email3; 
				$sender_name = $sender_name3;
				$mail_text = $mail_text3;
				$sender_id =  $sender_id3;
				$mail_alert_type_id = $mail_alert_type_id3;

			}
			else
			{
				$subject = "Agreed POS opening date is past due - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];
			}

			$mail = new Mail();
			$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
			$mail->set_sender($sender_email, $sender_name);


			//add cc and deputee
			$sql = "select user_id, user_firstname, user_email, user_email_cc, user_email_deputy ".
				   "from users ".
				   "where (user_id = " . $row["project_retail_coordinator"] . 
				   "   and user_active = 1)";
			
			$res1 = mysql_query($sql) or dberror($sql);
			$row1 = mysql_fetch_assoc($res1);
			$rtc_id = $row1["user_id"];
			$rtc_name = $row1["user_firstname"];
			$rtc_email = $row1["user_email"];

			$mail->add_recipient($row1["user_email"]);

			if($row1["user_email_cc"])
			{
				$mail->add_cc($row1["user_email_cc"]);
			}
			if($row1["user_email_deputy"])
			{
				$mail->add_cc($row1["user_email_deputy"]);
			}
			$mail->add_cc($sender_email);

			$sql = "select user_id, user_email ".
				   "from users ".
				   "where (user_id = " . $row["order_retail_operator"] . 
				   "   and user_active = 1)";
			$res1 = mysql_query($sql) or dberror($sql);
			$row1 = mysql_fetch_assoc($res1);
			$mail->add_cc($row1["user_email"]);
			$rto_id = $row1["user_id"];


			$bodytext0 = "Dear " . $rtc_name . "\n\n" . $mail_text;
			$link ="project_edit_pos_data.php?pid=" . $row["project_id"];
			$bodytext = $bodytext0 . "\nClick below to have direct access to the project:\n";
			$bodytext = $bodytext .  APPLICATION_URL ."/user/" . $link . "\n\n";           
			
			$bodytext .= "Thank you very much.\nKind regards, " . $sender_name;

			$mail->add_text($bodytext);

			$sql_m = 'select count(mail_alert_id) as num_recs ' .
				     'from mail_alerts where mail_alert_type = 1 and mail_alert_order = ' . $row["order_id"];
			$res_m = mysql_query($sql_m) or dberror($sql_m);
			$row_m = mysql_fetch_assoc($res_m);

			if($row_m['num_recs'] == 0) {
				$result = $mail->send();

				if($result == 1)
				{
					append_mail($row["order_id"], $rtc_id, $sender_id, $bodytext0, "900", 1);
					append_mail($row["order_id"], $rto_id, $sender_id, $bodytext0, "900", 1);

					//update mail_alerts
					$fields = array();
					$values = array();

					$fields[] = "mail_alert_type";
					$values[] = $mail_alert_type_id;

					$fields[] = "mail_alert_order";
					$values[] = $row["order_id"];

					$fields[] = "mail_alert_date";
					$values[] = dbquote($today);

					$fields[] = "mail_alert_sender";
					$values[] = dbquote($sender_email);

					$fields[] = "mail_alert_recipient";
					$values[] = dbquote($rtc_email);

					$fields[] = "date_created";
					$values[] = "current_timestamp";

					$fields[] = "date_modified";
					$values[] = "current_timestamp";

					$fields[] = "user_created";
					$values[] = dbquote(user_login());

					$fields[] = "user_modified";
					$values[] = dbquote(user_login());

					$sql = "insert into mail_alerts (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					mysql_query($sql) or dberror($sql);
				}
			}
		}
    }

	return true;
}

/********************************************************************
    Ask Client
	for entering/adapting the actual shop opening date
*********************************************************************/
function ma_enter_actual_sopd()
{

	
	$today = date("Y-m-d");

	$sql = "select * from mail_alert_types where mail_alert_type_id = 2";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email = $row["mail_alert_type_sender_email"];
	$sender_name = $row["mail_alert_type_sender_name"];
	$mail_text = $row["mail_alert_mail_text"];
	$mail_alert_type_id = 2;

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id = $row["user_id"];



	$sql = "select project_id, project_real_opening_date, project_retail_coordinator, " .
		   "order_id, order_archive_date, order_number, order_client_address, " .
		   "order_shop_address_place,order_shop_address_company, order_actual_order_state_code, " .
		   "country_name, order_user " . 
		   "from projects " .
		   "left join orders on order_id = project_order " .
		   "left join countries on country_id = order_shop_address_country " . 
		   "where (select count(mail_alert_id) from mail_alerts where mail_alert_order = order_id and mail_alert_type IN (2))  = 0 " . 
		   " and (LEFT(order_number, 2) >= 26 or LEFT(order_number, 2) >= 210) " . 
		   " and (order_actual_order_state_code = '800' " . 
		   "   or order_actual_order_state_code = '810' " . 
		   "   or order_actual_order_state_code = '820')" .
		   " and (project_real_opening_date is null " . 
		   "   or project_real_opening_date = '0000-00-00' " . 
		   "   or project_real_opening_date <= '" . $today . "') " .
		   " and project_state <> 2 " .
		   " and (project_actual_opening_date is null " . 
		   "   or project_actual_opening_date = '0000-00-00') " . 
		    " and (project_projectkind <> 4 and project_projectkind <> 5)";

    $res = mysql_query($sql) or dberror($sql);

    while($row = mysql_fetch_assoc($res))
    {
		if($row["order_client_address"] > 0)
		{
			$subject = "Actual POS opening date is missing and realisitc POS opening date is past due - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];

			$mail = new Mail();
			$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
			$mail->set_sender($sender_email, $sender_name);

			
			$sql = "select user_id, user_firstname, user_email, user_email_cc, user_email_deputy, user_can_only_see_his_projects ".
				   "from users ".
				   "where  user_id = " . $row["project_retail_coordinator"] . 
				   " and user_active = 1";
			
			$res1 = mysql_query($sql) or dberror($sql);
			$row1 = mysql_fetch_assoc($res1);

			$cli_id = $row1["user_id"];
			$cli_name = $row1["user_firstname"];
			$cli_email = $row1["user_email"];

			$mail->add_recipient($cli_email);

			if($row1["user_email_cc"])
			{
				$mail->add_cc($row1["user_email_cc"]);
			}
			if($row1["user_email_deputy"])
			{
				$mail->add_cc($row1["user_email_deputy"]);
			}

			$mail->add_cc($sender_email);

		
			$bodytext0 = "Dear " . $cli_name . "\n\n" . $mail_text;
			$link ="project_edit_pos_data.php?pid=" . $row["project_id"];
			$bodytext = $bodytext0 . "\nClick below to have direct access to the project:\n";
			$bodytext = $bodytext .  APPLICATION_URL ."/user/" . $link . "\n\n";           
			
			$bodytext .= "Thank you very much.\nKind regards, " . $sender_name;

			$mail->add_text($bodytext);

			
			$sql_m = 'select count(mail_alert_id) as num_recs ' .
				     'from mail_alerts where mail_alert_type = 2 and mail_alert_order = ' . $row["order_id"];
			$res_m = mysql_query($sql_m) or dberror($sql_m);
			$row_m = mysql_fetch_assoc($res_m);

			if($row_m['num_recs'] == 0) {
				$result = $mail->send();

				if($result == 1)
				{
					append_mail($row["order_id"], $cli_id, $sender_id, $bodytext0, "900", 1);

					//update mail_alerts
					$fields = array();
					$values = array();

					$fields[] = "mail_alert_type";
					$values[] = $mail_alert_type_id;

					$fields[] = "mail_alert_order";
					$values[] = $row["order_id"];

					$fields[] = "mail_alert_date";
					$values[] = dbquote($today);

					$fields[] = "mail_alert_sender";
					$values[] = dbquote($sender_email);

					$fields[] = "mail_alert_recipient";
					$values[] = dbquote($cli_email);

					$fields[] = "date_created";
					$values[] = "current_timestamp";

					$fields[] = "date_modified";
					$values[] = "current_timestamp";

					$fields[] = "user_created";
					$values[] = dbquote(user_login());

					$fields[] = "user_modified";
					$values[] = dbquote(user_login());

					$sql = "insert into mail_alerts (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					mysql_query($sql) or dberror($sql);
				}
			}
		}
    }


	//take over and lease renewal


	$sql = "select * from mail_alert_types where mail_alert_type_id = 17";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email = $row["mail_alert_type_sender_email"];
	$sender_name = $row["mail_alert_type_sender_name"];
	$mail_text = $row["mail_alert_mail_text"];
	$mail_alert_type_id = 17;

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id = $row["user_id"];


	$sql = "select * from mail_alert_types where mail_alert_type_id = 18";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email2 = $row["mail_alert_type_sender_email"];
	$sender_name2 = $row["mail_alert_type_sender_name"];
	$mail_text2 = $row["mail_alert_mail_text"];
	$mail_alert_type_id2 = 18;

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email2 . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id2 = $row["user_id"];


	$sql = "select project_id, project_projectkind, project_real_opening_date, project_retail_coordinator, " .
		   "order_id, order_archive_date, order_number, order_client_address, " .
		   "order_shop_address_place,order_shop_address_company, order_actual_order_state_code, " .
		   "country_name, order_user " . 
		   "from projects " .
		   "left join orders on order_id = project_order " .
		   "left join countries on country_id = order_shop_address_country " . 
		   "where (select count(mail_alert_id) from mail_alerts where mail_alert_order = order_id and mail_alert_type IN (17,18))  = 0 " . 
		   " and (LEFT(order_number, 2) >= 26 or LEFT(order_number, 2) >= 210) " . 
		   " and (project_real_opening_date is null " . 
		   "   or project_real_opening_date = '0000-00-00' " . 
		   "   or project_real_opening_date <= '" . $today . "') " . 
		   " and project_state <> 2 " .
		   " and order_actual_order_state_code < '820' " .
		   " and (project_actual_opening_date is null " . 
		   "   or project_actual_opening_date = '0000-00-00') " . 
		    " and (project_projectkind = 4 or project_projectkind = 5)";

    $res = mysql_query($sql) or dberror($sql);

    while($row = mysql_fetch_assoc($res))
    {
		if($row["order_client_address"] > 0)
		{
			
			
			if($row['project_projectkind'] == 5) //lease renewal
			{
				$subject = "Actual lease starting date is missing and planned lease starting date date is past due - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];

				$sender_email = $sender_email2; 
				$sender_name = $sender_name2;
				$mail_text = $mail_text2;
				$sender_id =  $sender_id2;
				$mail_alert_type_id = $mail_alert_type_id2;

			}
			else // take over
			{
				$subject = "Actual take over date is missing and planned take over date date date is past due - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];
			}
			
			

			$mail = new Mail();
			$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
			$mail->set_sender($sender_email, $sender_name);

			

			$sql = "select user_id, user_firstname, user_email, user_email_cc, user_email_deputy, user_can_only_see_his_projects ".
				   "from users ".
				   "where  user_id = " . $row["project_retail_coordinator"] . 
				   " and user_active = 1";
			
			$res1 = mysql_query($sql) or dberror($sql);
			$row1 = mysql_fetch_assoc($res1);

			$cli_id = $row1["user_id"];
			$cli_name = $row1["user_firstname"];
			$cli_email = $row1["user_email"];

			$mail->add_recipient($cli_email);

			if($row1["user_email_cc"])
			{
				$mail->add_cc($row1["user_email_cc"]);
			}
			if($row1["user_email_deputy"])
			{
				$mail->add_cc($row1["user_email_deputy"]);
			}

			$mail->add_cc($sender_email);

		
			$bodytext0 = "Dear " . $cli_name . "\n\n" . $mail_text;
			$link ="project_edit_pos_data.php?pid=" . $row["project_id"];
			$bodytext = $bodytext0 . "\nClick below to have direct access to the project:\n";
			$bodytext = $bodytext .  APPLICATION_URL ."/user/" . $link . "\n\n";           
			
			$bodytext .= "Thank you very much.\nKind regards, " . $sender_name;

			$mail->add_text($bodytext);

			
			$sql_m = 'select count(mail_alert_id) as num_recs ' .
				     'from mail_alerts where mail_alert_type = 2 and mail_alert_order = ' . $row["order_id"];
			$res_m = mysql_query($sql_m) or dberror($sql_m);
			$row_m = mysql_fetch_assoc($res_m);

			if($row_m['num_recs'] == 0) {
				$result = $mail->send();

				if($result == 1)
				{
					append_mail($row["order_id"], $cli_id, $sender_id, $bodytext0, "900", 1);

					//update mail_alerts
					$fields = array();
					$values = array();

					$fields[] = "mail_alert_type";
					$values[] = $mail_alert_type_id;

					$fields[] = "mail_alert_order";
					$values[] = $row["order_id"];

					$fields[] = "mail_alert_date";
					$values[] = dbquote($today);

					$fields[] = "mail_alert_sender";
					$values[] = dbquote($sender_email);

					$fields[] = "mail_alert_recipient";
					$values[] = dbquote($cli_email);

					$fields[] = "date_created";
					$values[] = "current_timestamp";

					$fields[] = "date_modified";
					$values[] = "current_timestamp";

					$fields[] = "user_created";
					$values[] = dbquote(user_login());

					$fields[] = "user_modified";
					$values[] = dbquote(user_login());

					$sql = "insert into mail_alerts (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					mysql_query($sql) or dberror($sql);
				}
			}
		}
    }
	return true;
}

/********************************************************************
    Ask Client
	to resignate lease contract/extend lease contract
*********************************************************************/
function ma_resignation_deadline()
{

	$today = date("Y-m-d");

	
	//extenstion option
	$sql = "select * from mail_alert_types where mail_alert_type_id = 28";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email = $row["mail_alert_type_sender_email"];
	$sender_name = $row["mail_alert_type_sender_name"];
	$mail_text_original = $row["mail_alert_mail_text"];

	$cc1 = $row["mail_alert_type_cc1"];
	$cc2 = $row["mail_alert_type_cc2"];
	$cc3 = $row["mail_alert_type_cc3"];
	$cc4 = $row["mail_alert_type_cc4"];

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id = $row["user_id"];
	
	
	
    $sql = "select * from posleases " . 
			"left join posaddresses on posaddress_id = poslease_posaddress " . 
			"left join countries on country_id = posaddress_country " . 
			"where PERIOD_DIFF(DATE_FORMAT(poslease_extensionoption, '%Y%m'), DATE_FORMAT('" . date("Y-m-d") . "', '%Y%m'))  <= (poslease_termination_time+1) " . 
			"and (posaddress_store_closingdate is null " .
            "or posaddress_store_closingdate = '0000-00-00') " . 
            " and poslease_mailalert is not null and poslease_mailalert <> ''  ". 
            "and poslease_termination_time > 0 " . 
            "and poslease_extensionoption >= '" . date("Y-m-d") . "' " . 
		    "and poslease_extensionoption is not null " .
		    "and poslease_extensionoption <> '0000-00-00' " .
		    "and poslease_mailalert is not null " . 
		    "and poslease_mailalert2 is null " .
		    "and poslease_extensionoption is not null " . 
		    "and poslease_extensionoption <> '0000-00-00' " .
		    " order by poslease_posaddress, poslease_enddate DESC";

	
	$res = mysql_query($sql) or dberror($sql);

    while($row = mysql_fetch_assoc($res))
    {

		//check if lease was already renewed
		$send_mail = true;
		$sql_l = "select count(poslease_id) as num_recs " .
			     " from posleases " . 
			     " where poslease_posaddress = " . $row["poslease_posaddress"] . 
			     " and poslease_id <> " . $row["poslease_id"] . 
			     " and year(poslease_enddate) >= year(poslease_startdate) ";

		$res_l = mysql_query($sql_l) or dberror($sql_l);
		$row_l = mysql_fetch_assoc($res_l);
		if($row_l["num_recs"] > 0)
		{
			$send_mail = false;
			//echo "->" . $row["poslease_posaddress"] . "<br />";
		}

		if($send_mail == true)
		{
			//get brand manager
			$bm_email = "";
			$sql_c = "select user_firstname, user_name, user_email from users " .
					 "left join user_roles on user_role_user = user_id " . 
					 "where user_address = " . dbquote($row["posaddress_client_id"]) . 
					 " and user_role_role = 15 and user_active = 1";

			$res_c = mysql_query($sql_c) or dberror($sql_c);

			if($row_c = mysql_fetch_assoc($res_c))
			{
				$bm_firstname = $row_c["user_firstname"];
				$bm_name = $row_c["user_name"];
				$bm_email = $row_c["user_email"];
			
			}
			else // get brand manager in case of an affiliate
			{
				$sql_c = "select address_client_type, address_parent " . 
						 " from addresses " . 
						 " where address_id = " . dbquote($row["posaddress_client_id"]);
				$res_c = mysql_query($sql_c) or dberror($sql_c);
				if($row_c = mysql_fetch_assoc($res_c))
				{
					if($row_c["address_client_type"] == 3) //Affiliate
					{
						$sql_c = "select user_firstname, user_name, user_email from users " .
								 "left join user_roles on user_role_user = user_id " . 
								 "where user_address = " . dbquote($row_c["address_parent"]) . 
								 " and user_role_role = 15 and user_active = 1";

						$res_c = mysql_query($sql_c) or dberror($sql_c);

						if($row_c = mysql_fetch_assoc($res_c))
						{
							$bm_firstname = $row_c["user_firstname"];
							$bm_name = $row_c["user_name"];
							$bm_email = $row_c["user_email"];
						
						}
					}
				}
			}


			//get retail manager
			
			$rm_firstname = array();
			$rm_name = array();
			$rm_email = array();
			$sql_c = "select user_firstname, user_name, user_email from users " .
					 "left join user_roles on user_role_user = user_id " . 
					 "where user_address = " . dbquote($row["posaddress_client_id"]) . 
					 " and user_role_role = 16 and user_active = 1";

			$res_c = mysql_query($sql_c) or dberror($sql_c);

			while($row_c = mysql_fetch_assoc($res_c))
			{
				$rm_firstname[] = $row_c["user_firstname"];
				$rm_name[] = $row_c["user_name"];
				$rm_email[] = $row_c["user_email"];
			
			}

			if($bm_email or count($rm_email) > 0)
			{
				$poslocation = $row["posaddress_name"] . ", " . $row["country_name"];
				$subject = "Lease contract alert - POS Location " . $poslocation;

				$mail_text = str_replace("{pos}", $poslocation, $mail_text_original);

				$mail = new Mail();
				$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
				$mail->set_sender($sender_email, $sender_name);

				if($bm_email)
				{
					$mail->add_recipient($bm_email);
					
					foreach($rm_email as $key=>$email)
					{
						$mail->add_cc($email);
					}
					$mail_text = str_replace("{name}", $bm_firstname, $mail_text);
				}
				else
				{
					foreach($rm_email as $key=>$email)
					{
						$mail->add_recipient($email);
						$mail_text = str_replace("{name}", $rm_firstname[$key], $mail_text);
					}
				}

				if($cc1) {$mail->add_cc($cc1);}
				if($cc2) {$mail->add_cc($cc2);}
				if($cc3) {$mail->add_cc($cc3);}
				if($cc4) {$mail->add_cc($cc4);}
				
				
				$mail->add_text($mail_text);

				//echo $row["poslease_id"] . ' ' . $row["poslease_enddate"] . ' ' . $row['poslease_termination_time'] . ' ' . $mail_text . '<br /><br /><br />';

				$result = $mail->send();
				
				if($result == 1)
				{
					//update mail_alerts
					$mailalert = $mail_text;
					$mailalert .= "\r\n" . "Mail sent " . to_system_date(date("Y-m-d")) . " by " . $sender_name;
					
					if($bm_email and count($rm_email) > 0)
					{
						$mailalert .= "\r\n" . "To: " . $bm_email . " and cc to ";
						foreach($rm_email as $key=>$email)
						{
							$mailalert .= $email . "\r\n";
						}
					}
					elseif($bm_email)
					{
						$mailalert .= "\r\n" . "To: " . $bm_email;
					}
					else
					{
						$mailalert .= "\r\n" . "To: ";
						foreach($rm_email as $key=>$email)
						{
							$mailalert .= $email . "\r\n";
						}
					}
					
					$sql = "update posleases set " . 
						   "poslease_mailalert2 = " . dbquote($mailalert) . 
						   " where poslease_id = " . $row["poslease_id"];

					mysql_query($sql) or dberror($sql);
				}
			}
			//echo  $row["poslease_posaddress"] . "<br />";
		}

		
    }
	
	//resignation deadline
	$sql = "select * from mail_alert_types where mail_alert_type_id = 5";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email = $row["mail_alert_type_sender_email"];
	$sender_name = $row["mail_alert_type_sender_name"];
	$mail_text_original = $row["mail_alert_mail_text"];

	$cc1 = $row["mail_alert_type_cc1"];
	$cc2 = $row["mail_alert_type_cc2"];
	$cc3 = $row["mail_alert_type_cc3"];
	$cc4 = $row["mail_alert_type_cc4"];

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id = $row["user_id"];
	
	
	
    $sql = "select * from posleases " . 
			"left join posaddresses on posaddress_id = poslease_posaddress " . 
			"left join countries on country_id = posaddress_country " . 
			"where PERIOD_DIFF(DATE_FORMAT(poslease_enddate, '%Y%m'), DATE_FORMAT('" . date("Y-m-d") . "', '%Y%m'))  <= (poslease_termination_time+1) " . 
			"and (posaddress_store_closingdate is null " .
            "or posaddress_store_closingdate = '0000-00-00') " . 
            " and (poslease_mailalert is null or poslease_mailalert = '' ) ". 
            "and poslease_enddate >= '" . date("Y-m-d") . "' " .
		    " order by poslease_posaddress, poslease_enddate DESC";
	
	//echo $sql . "<br />";
	$res = mysql_query($sql) or dberror($sql);

    while($row = mysql_fetch_assoc($res))
    {
		//check if lease was already renewed
		$send_mail = true;
		$sql_l = "select count(poslease_id) as num_recs " .
			     " from posleases " . 
			     " where poslease_posaddress = " . $row["poslease_posaddress"] . 
			     " and poslease_id <> " . $row["poslease_id"] . 
			     " and year(poslease_enddate) >= year(poslease_startdate) ";

		$res_l = mysql_query($sql_l) or dberror($sql_l);
		$row_l = mysql_fetch_assoc($res_l);
		if($row_l["num_recs"] > 0)
		{
			$send_mail = false;
			//echo "->" . $row["poslease_posaddress"] . ": " . $row["poslease_enddate"] . "(" . $row_l["num_recs"] . ")<br />";
		}

		if($send_mail == true)
		{
		
			//get brand manager
			$bm_email = "";
			$sql_c = "select user_firstname, user_name, user_email from users " .
					 "left join user_roles on user_role_user = user_id " . 
					 "where user_address = " . dbquote($row["posaddress_client_id"]) . 
					 " and user_role_role = 15 and user_active = 1";

			$res_c = mysql_query($sql_c) or dberror($sql_c);

			if($row_c = mysql_fetch_assoc($res_c))
			{
				$bm_firstname = $row_c["user_firstname"];
				$bm_name = $row_c["user_name"];
				$bm_email = $row_c["user_email"];
			
			}
			else // get brand manager in case of an affiliate
			{
				$sql_c = "select address_client_type, address_parent " . 
						 " from addresses " . 
						 " where address_id = " . dbquote($row["posaddress_client_id"]);
				$res_c = mysql_query($sql_c) or dberror($sql_c);
				if($row_c = mysql_fetch_assoc($res_c))
				{
					if($row_c["address_client_type"] == 3) //Affiliate
					{
						$sql_c = "select user_firstname, user_name, user_email from users " .
								 "left join user_roles on user_role_user = user_id " . 
								 "where user_address = " . dbquote($row_c["address_parent"]) . 
								 " and user_role_role = 15 and user_active = 1";

						$res_c = mysql_query($sql_c) or dberror($sql_c);

						if($row_c = mysql_fetch_assoc($res_c))
						{
							$bm_firstname = $row_c["user_firstname"];
							$bm_name = $row_c["user_name"];
							$bm_email = $row_c["user_email"];
						
						}
					}
				}
			}

			//get retail manager
			$rm_firstname = array();
			$rm_name = array();
			$rm_email = array();

			$sql_c = "select user_firstname, user_name, user_email from users " .
					 "left join user_roles on user_role_user = user_id " . 
					 "where user_address = " . dbquote($row["posaddress_client_id"]) . 
					 " and user_role_role = 16 and user_active = 1";

			$res_c = mysql_query($sql_c) or dberror($sql_c);

			if($row_c = mysql_fetch_assoc($res_c))
			{
				$rm_firstname[] = $row_c["user_firstname"];
				$rm_name[] = $row_c["user_name"];
				$rm_email[] = $row_c["user_email"];
			
			}
			

			if($bm_email or count($rm_email) > 0)
			{
				$poslocation = $row["posaddress_name"] . ", " . $row["country_name"];
				$subject = "Lease contract alert - POS Location " . $poslocation;

				$mail_text = str_replace("{pos}", $poslocation, $mail_text_original);

				$mail = new Mail();
				$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
				$mail->set_sender($sender_email, $sender_name);

				if($bm_email)
				{
					$mail->add_recipient($bm_email);
					
					foreach($rm_email as $key=>$email)
					{
						$mail->add_cc($email);
					}

					$mail_text = str_replace("{name}", $bm_firstname, $mail_text);
				}
				else
				{
					foreach($rm_email as $key=>$email)
					{
						$mail->add_recipient($email);
						$mail_text = str_replace("{name}", $rm_firstname[$key], $mail_text);
					}
				}

				if($cc1) {$mail->add_cc($cc1);}
				if($cc2) {$mail->add_cc($cc2);}
				if($cc3) {$mail->add_cc($cc3);}
				if($cc4) {$mail->add_cc($cc4);}
				
				
				$mail->add_text($mail_text);

				//echo $row["poslease_id"] . ' ' . $row["poslease_enddate"] . ' ' . $row['poslease_termination_time'] . ' ' . $mail_text . '<br /><br /><br />';

				$result = $mail->send();

				if($result == 1)
				{
					//update mail_alerts
					$mailalert = $mail_text;
					$mailalert .= "\r\n" . "Mail sent " . to_system_date(date("Y-m-d")) . " by " . $sender_name;
					
					if($bm_email and count($rm_email) > 0)
					{
						$mailalert .= "\r\n" . "To: " . $bm_email . " and cc to ";
						foreach($rm_email as $key=>$email)
						{
							$mailalert .= $email . "\r\n";
						}
					}
					elseif($bm_email)
					{
						$mailalert .= "\r\n" . "To: " . $bm_email;
					}
					else
					{
						$mailalert .= "\r\n" . "To: ";
						foreach($rm_email as $key=>$email)
						{
							$mailalert .= $email . "\r\n";
						}
					}
					
					$sql = "update posleases set " . 
						   "poslease_mailalert = " . dbquote($mailalert) . 
						   " where poslease_id = " . $row["poslease_id"];

					mysql_query($sql) or dberror($sql);
				}
			}
			//echo "send mail: " . $row["poslease_posaddress"] . "<br />";
		}
    }

	return true;
}


/********************************************************************
    Mail Alert one month before lease start
*********************************************************************/
function ma_lease_start()
{

	$today = date("Y-m-d");

	$sql = "select * from mail_alert_types where mail_alert_type_id = 23";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email = $row["mail_alert_type_sender_email"];
	$sender_name = $row["mail_alert_type_sender_name"];
	$mail_text_original = $row["mail_alert_mail_text"];

	$cc1 = $row["mail_alert_type_cc1"];
	$cc2 = $row["mail_alert_type_cc2"];
	$cc3 = $row["mail_alert_type_cc3"];
	$cc4 = $row["mail_alert_type_cc4"];

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id = $row["user_id"];
	
	
	
    $sql = "select * from posleases " . 
			"left join posaddresses on posaddress_id = poslease_posaddress " . 
			"left join countries on country_id = posaddress_country " . 
			"where (posaddress_store_closingdate is null " .
            "or posaddress_store_closingdate = '0000-00-00') " . 
            " and (poslease_mailalert2 is null or poslease_mailalert2 = '' ) ". 
            "and poslease_startdate <= '" . date('Y-m-d', strtotime("+30 days")) . "' " .
	        "and poslease_startdate >= '" . date('Y-m-d') . "'";
	

	$res = mysql_query($sql) or dberror($sql);

    while($row = mysql_fetch_assoc($res))
    {
		
		//get brand manager
		$bm_email = "";
		$sql_c = "select user_firstname, user_name, user_email from users " .
			     "left join user_roles on user_role_user = user_id " . 
		         "where user_address = " . $row["posaddress_client_id"] . 
			     " and user_role_role = 15 and user_active = 1";

		$res_c = mysql_query($sql_c) or dberror($sql_c);

		if($row_c = mysql_fetch_assoc($res_c))
		{
			$bm_firstname = $row_c["user_firstname"];
			$bm_name = $row_c["user_name"];
			$bm_email = $row_c["user_email"];
		
		}

		//get project manager
		$rm_email = "";
		$sql_c = "select user_firstname, user_name, user_email from users " .
			     "left join user_roles on user_role_user = user_id " . 
		         "where user_address = " . $row["posaddress_client_id"] . 
			     " and user_role_role = 3 and user_active = 1";

		$res_c = mysql_query($sql_c) or dberror($sql_c);

		if($row_c = mysql_fetch_assoc($res_c))
		{
			$rm_firstname = $row_c["user_firstname"];
			$rm_name = $row_c["user_name"];
			$rm_email = $row_c["user_email"];
		
		}
		

		if($bm_email or $rm_email)
		{
			$poslocation = $row["posaddress_name"] . ", " . $row["country_name"];
			$subject = "Lease contract alert - POS Location " . $poslocation;

			$mail_text = str_replace("{pos}", $poslocation, $mail_text_original);
			$mail_text = str_replace("{start_date}", to_system_date($row["poslease_startdate"]), $mail_text);

			
			$mail = new Mail();
			$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
			$mail->set_sender($sender_email, $sender_name);

			if($bm_email)
			{
				$mail->add_recipient($bm_email);
				$mail->add_cc($rm_email);

				$mail_text = str_replace("{name}", $bm_firstname, $mail_text);
			}
			else
			{
				$mail->add_recipient($rm_email);
				$mail_text = str_replace("{name}", $rm_firstname, $mail_text);
			}

			if($cc1) {$mail->add_cc($cc1);}
			if($cc2) {$mail->add_cc($cc2);}
			if($cc3) {$mail->add_cc($cc3);}
			if($cc4) {$mail->add_cc($cc4);}
			
			
			$mail->add_text($mail_text);

			//echo $row["poslease_id"] . ' ' . $row["poslease_enddate"] . ' ' . $row['poslease_termination_time'] . ' ' . $mail_text . '<br /><br /><br />';

			$result = $mail->send();

			if($result == 1)
			{
				//update mail_alerts
				$mailalert = $mail_text;
				$mailalert .= "\r\n" . "Mail sent " . to_system_date(date("Y-m-d")) . " by " . $sender_name;
				
				if($bm_email and $rm_email)
				{
					$mailalert .= "\r\n" . "To: " . $bm_email . " and cc to " . $rm_email;
				}
				elseif($bm_email)
				{
					$mailalert .= "\r\n" . "To: " . $bm_email;
				}
				else
				{
					$mailalert .= "\r\n" . "To: " . $rm_email;
				}
				
				$sql = "update posleases set " . 
					   "poslease_mailalert2 = " . dbquote($mailalert) . 
					   " where poslease_id = " . $row["poslease_id"];

				mysql_query($sql) or dberror($sql);
			}
		}
    }


	//new projects in pipeline
	$sql = "select * from posleasespipeline " . 
			"left join posaddressespipeline on posaddress_id = poslease_posaddress " . 
			"left join countries on country_id = posaddress_country " . 
			"where (poslease_mailalert2 is null or poslease_mailalert2 = '' ) ".
            "and poslease_startdate <= '" . date('Y-m-d', strtotime("+30 days")) . "' " .
	        "and poslease_startdate >= '" . date('Y-m-d') . "'";
	

	$res = mysql_query($sql) or dberror($sql);

    while($row = mysql_fetch_assoc($res))
    {
		
		//get brand manager
		$bm_email = "";
		$sql_c = "select user_firstname, user_name, user_email from users " .
			     "left join user_roles on user_role_user = user_id " . 
		         "where user_address = " . $row["posaddress_client_id"] . 
			     " and user_role_role = 15 and user_active = 1";

		$res_c = mysql_query($sql_c) or dberror($sql_c);

		if($row_c = mysql_fetch_assoc($res_c))
		{
			$bm_firstname = $row_c["user_firstname"];
			$bm_name = $row_c["user_name"];
			$bm_email = $row_c["user_email"];
		
		}

		//get project manager
		$rm_email = "";
		$sql_c = "select user_firstname, user_name, user_email from users " .
			     "left join user_roles on user_role_user = user_id " . 
		         "where user_address = " . $row["posaddress_client_id"] . 
			     " and user_role_role = 3 and user_active = 1";

		$res_c = mysql_query($sql_c) or dberror($sql_c);

		if($row_c = mysql_fetch_assoc($res_c))
		{
			$rm_firstname = $row_c["user_firstname"];
			$rm_name = $row_c["user_name"];
			$rm_email = $row_c["user_email"];
		
		}
		

		if($bm_email or $rm_email)
		{
			$poslocation = $row["posaddress_name"] . ", " . $row["country_name"];
			$subject = "Lease contract alert - POS Location " . $poslocation;

			$mail_text = str_replace("{pos}", $poslocation, $mail_text_original);
			$mail_text = str_replace("{start_date}", to_system_date($row["poslease_startdate"]), $mail_text);

			
			$mail = new Mail();
			$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
			$mail->set_sender($sender_email, $sender_name);

			if($bm_email)
			{
				$mail->add_recipient($bm_email);
				$mail->add_cc($rm_email);

				$mail_text = str_replace("{name}", $bm_firstname, $mail_text);
			}
			else
			{
				$mail->add_recipient($rm_email);
				$mail_text = str_replace("{name}", $rm_firstname, $mail_text);
			}

			if($cc1) {$mail->add_cc($cc1);}
			if($cc2) {$mail->add_cc($cc2);}
			if($cc3) {$mail->add_cc($cc3);}
			if($cc4) {$mail->add_cc($cc4);}
			
			
			$mail->add_text($mail_text);

			//echo $row["poslease_id"] . ' ' . $row["poslease_enddate"] . ' ' . $row['poslease_termination_time'] . ' ' . $mail_text . '<br /><br /><br />';

			$result = $mail->send();

			if($result == 1)
			{
				//update mail_alerts
				$mailalert = $mail_text;
				$mailalert .= "\r\n" . "Mail sent " . to_system_date(date("Y-m-d")) . " by " . $sender_name;
				
				if($bm_email and $rm_email)
				{
					$mailalert .= "\r\n" . "To: " . $bm_email . " and cc to " . $rm_email;
				}
				elseif($bm_email)
				{
					$mailalert .= "\r\n" . "To: " . $bm_email;
				}
				else
				{
					$mailalert .= "\r\n" . "To: " . $rm_email;
				}
				
				$sql = "update posleasespipeline set " . 
					   "poslease_mailalert2 = " . dbquote($mailalert) . 
					   " where poslease_id = " . $row["poslease_id"];

				mysql_query($sql) or dberror($sql);
			}
		}
    }

	
	return true;
}

?>