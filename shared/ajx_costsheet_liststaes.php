<?php
/********************************************************************

    ajx_costsheet_liststaes.php

    Save CSS Display Properties of Lists in file project_costs.php

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-01-09
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2012, OMEGA SA, All Rights Reserved.

*********************************************************************/

session_name("retailnet");
session_start();

$listname = "";
$visibility = "";

if(array_key_exists("listname", $_POST))
{
	$listname = $_POST["listname"];
	$visibility = $_POST["visibility"];
}

$_SESSION["costsheet"][$listname] = $visibility;
echo "success";



?>