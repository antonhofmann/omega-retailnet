<?php
/********************************************************************

    func_posindex.php

    Various utility functions for the POSIndex

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

/********************************************************************
    calculate duration of franchisee agreement
*********************************************************************/
function calculate_aggreement_duration($posaddress_id)
{

	$duration = "";
	$sql = "select IF(TIMESTAMPDIFF(MONTH,posaddress_fagrstart,posaddress_fagrend) >0, TIMESTAMPDIFF(MONTH,posaddress_fagrstart,posaddress_fagrend), 'n.a.') as months from posaddresses where posaddress_id = " . $posaddress_id;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		if($row["months"] > 0)
		{
			$duration = $row["months"];
			$duration = floor($duration/12);

			$months = 1 + $row["months"] - $duration*12;


			if($months == 12)
			{
				$duration = $duration + 1 . " years";
			}
			
			elseif($months > 0)
			{
				$duration = $duration . " years and " . (1 + $row["months"] - $duration*12) . " months";
			}
		}
	}
	
	return $duration;

}

/********************************************************************
    get POS Location Data
*********************************************************************/
function get_poslocation($posaddress_id, $table = "posaddresses")
{

	$poslocation = array();

    $sql = "select *, " . 
       "IF(TIMESTAMPDIFF(MONTH,posaddress_fagrstart,posaddress_fagrend) >0, TIMESTAMPDIFF(MONTH,posaddress_fagrstart,posaddress_fagrend), 'n.a.') as dmonths " .  
           "from " . $table .
		   " left join countries on country_id = posaddress_country " .
		   "left join addresses on address_id = posaddress_client_id " .
		   "left join agreement_types on agreement_type_id = posaddress_fagagreement_type " .
		   "left join places on place_id = posaddress_place_id " . 
		   "left join provinces on province_id = place_province " .
		   "left join currencies on currency_id = posaddress_takeover_currency " .
           "where posaddress_id  = " . dbquote($posaddress_id);

	$res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $poslocation = $row;
    }

	return $poslocation;
}

/********************************************************************
    get POS Location Data
*********************************************************************/
function get_poslocation_from_pipeline($posaddress_id, $order_id)
{
	
	$table = "posaddressespipeline";

	$sql = "select posorder_parent_table from posorderspipeline where posorder_order = " . dbquote($order_id);
	$res = mysql_query($sql) or dberror($sql);
	
	if ($row = mysql_fetch_assoc($res))
    {
		$table = $row["posorder_parent_table"];
	}
	
	$poslocation = array();

    $sql = "select *, " . 
       "IF(TIMESTAMPDIFF(MONTH,posaddress_fagrstart,posaddress_fagrend) >0, TIMESTAMPDIFF(MONTH,posaddress_fagrstart,posaddress_fagrend), 'n.a.') as dmonths " . 
		   "from  " . $table . " " . 
		   "left join countries on country_id = posaddress_country " .
		   "left join addresses on address_id = posaddress_client_id " .
		   "left join agreement_types on agreement_type_id = posaddress_fagagreement_type " .
		   "left join places on place_id = posaddress_place_id " . 
		   "left join provinces on province_id = place_province " .
		    "left join currencies on currency_id = posaddress_takeover_currency " .
           "where posaddress_id  = " . dbquote($posaddress_id);
	
    
	
	$res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $poslocation = $row;
    }

	return $poslocation;
}


/********************************************************************
    get the field values of an address
*********************************************************************/
function google_maps_geo_encode_pos($pos_id = 0, $context)
{

	$api_key = GOOGLE_API_KEY_GEO;

	// Address and Place
	$filter = "";
	if($pos_id > 0)
	{
		$filter = " and posaddress_id = " . $pos_id;
	}

	$query = "SELECT * FROM posaddresses left join countries on country_id = posaddress_country " . 
		     "where (posaddress_google_precision is null or posaddress_google_precision =2) " . $filter;
	$res = mysql_query($query);


	
	if (!$res)
	{
		return false;
	}

	if ($row = mysql_fetch_assoc($res))
	{

		$address = $row["country_name"];
		$address .= "+" . $row["posaddress_place"];
		$address .= "+" . $row["posaddress_address"];
		$address = urlencode($address);

		$lat = "";
		$long = "";

		$url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&key=" . $api_key;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		if(PROXY_SERVER)
		{
			curl_setopt($ch, CURLOPT_PROXY, PROXY_SERVER);
		}
		$response = curl_exec($ch);
		curl_close($ch);
		$response_a = json_decode($response);

		if(isset($response_a->results[0]->geometry->location->lat))
		{
			$lat = $response_a->results[0]->geometry->location->lat;
			$long = $response_a->results[0]->geometry->location->lng;
		}

		if(is_numeric($lat) and is_numeric($long))
		{
			$query = "UPDATE posaddresses " .
					 " SET posaddress_google_precision = 1, " . 
					 " posaddress_google_lat = '" . $lat . "', " . 
					 " posaddress_google_long = '" . $long . "' " .
					 " WHERE posaddress_id = " . $pos_id;
					  
			$update_result = mysql_query($query);
		}
		else
		{
			$address = $row["country_name"];
			$address .= "+" . $row["posaddress_place"];
			$address = urlencode($address);

			$url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&key=" . $api_key;;
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			if(PROXY_SERVER)
			{
				curl_setopt($ch, CURLOPT_PROXY, PROXY_SERVER);
			}
			$response = curl_exec($ch);
			curl_close($ch);
			$response_a = json_decode($response);
			
			if(isset($response_a->results[0]->geometry->location->lat))
			{
				$lat = $response_a->results[0]->geometry->location->lat;
				$long = $response_a->results[0]->geometry->location->lng;
			}

			if(is_numeric($lat) and is_numeric($long))
			{
				$query = "UPDATE posaddresses " .
						 " SET posaddress_google_precision = 1, " . 
						 " posaddress_google_lat = '" . $lat . "', " . 
						 " posaddress_google_long = '" . $long . "' " .
						 " WHERE posaddress_id = " . $pos_id;
					  
						$update_result = mysql_query($query);
			}
			else
			{
				$address = $row["country_name"];
				$address = urlencode($address);

				$url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&key=" . $api_key;;
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				if(PROXY_SERVER)
				{
					curl_setopt($ch, CURLOPT_PROXY, PROXY_SERVER);
				}
				$response = curl_exec($ch);
				curl_close($ch);
				$response_a = json_decode($response);
				
				if(isset($response_a->results[0]->geometry->location->lat))
				{
					$lat = $response_a->results[0]->geometry->location->lat;
					$long = $response_a->results[0]->geometry->location->lng;
				}

				if(is_numeric($lat) and is_numeric($long))
				{
					$query = "UPDATE posaddresses " .
							 " SET posaddress_google_precision = 1, " . 
							 " posaddress_google_lat = '" . $lat . "', " . 
							 " posaddress_google_long = '" . $long . "' " .
							 " WHERE posaddress_id = " . $pos_id;
							  
					$update_result = mysql_query($query);
				}
			}
		}
		
	}

		return true;
}


/********************************************************************
   update pos data
*********************************************************************/
function  project_update_pos_data($form, $is_in_archive = 0)
{
	
	$project_fields = array();
    // update record in table projects

    if($form->value("shop_closing_date"))
	{
		$value = 5;
		$project_fields[] = "project_state = " . $value;
	}
	elseif($form->value("shop_actual_opening_date"))
	{
		$project_fields[] = "project_state = 4"; //open
	}
	else
	{
		$value = trim($form->value("project_state")) == "" ? "null" : dbquote($form->value("project_state"));
		if($form->value("project_state") == 4 or $form->value("project_state") == 5) 
		{
			$value = 1; // set to in progress
		}
		
		$project_fields[] = "project_state = " . $value;
	}
	
		
    $value = trim($form->value("shop_real_opening_date")) == "" ? "null" : dbquote(from_system_date($form->value("shop_real_opening_date")));
    $project_fields[] = "project_real_opening_date = " . $value;

    $value = trim($form->value("shop_actual_opening_date")) == "" ? "null" : dbquote(from_system_date($form->value("shop_actual_opening_date")));
    $project_fields[] = "project_actual_opening_date = " . $value;

	$value = trim($form->value("shop_closing_date")) == "" ? "null" : dbquote(from_system_date($form->value("shop_closing_date")));
    $project_fields[] = "project_shop_closingdate = " . $value;


	//Franchisee Agreement Data
    if(array_key_exists("project_fagagreement_type", $form->items))
	{
		$value = trim($form->value("project_fagagreement_type")) == "" ? "0" : dbquote($form->value("project_fagagreement_type"));
		$project_fields[] = "project_fagagreement_type = " . $value;

		$value = trim($form->value("project_fagrsent")) == "" ? "0" : dbquote($form->value("project_fagrsent"));
		$project_fields[] = "project_fagrsent = " . $value;

		$value = trim($form->value("project_fagrsigned")) == "" ? "0" : dbquote($form->value("project_fagrsigned"));
		$project_fields[] = "project_fagrsigned = " . $value;

		$value = trim($form->value("project_fagrstart")) == "" ? "NULL" : dbquote(from_system_date($form->value("project_fagrstart")));
		$project_fields[] = "project_fagrstart = " . $value;

		$value = trim($form->value("project_fagrend")) == "" ? "NULL" : dbquote(from_system_date($form->value("project_fagrend")));
		$project_fields[] = "project_fagrend = " . $value;

		$value = trim($form->value("project_fag_comment")) == "" ? "null" : dbquote($form->value("project_fag_comment"));
		$project_fields[] = "project_fag_comment = " . $value;
	}

	$value = trim($form->value("posaddress_distribution_channel")) == "" ? "null" : dbquote($form->value("posaddress_distribution_channel"));
    $project_fields[] = "project_distribution_channel = " . $value;

	$value = trim($form->value("project_floor")) == "" ? "0" : dbquote($form->value("project_floor"));
	$project_fields[] = "project_floor = " . $value;

	$value = "current_timestamp";
    $project_fields[] = "date_modified = " . $value;
    
    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $project_fields[] = "user_modified = " . $value;
    }


    $sql = "update projects set " . join(", ", $project_fields) . " where project_id = " . $form->value("pid");
    mysql_query($sql) or dberror($sql);


    //create new franchisee address
	$franchisee_id = 0;
	if($form->value("create_new_franchisee_address") == 1 and !$form->value("order_franchisee_address_id")) 
	{
		$fields = array();
		$values = array();

		$fields[] = "address_shortcut";
		$values[] = dbquote(substr(strtolower($form->value("franchisee_address_company")), 0, 15));

		$fields[] = "address_company";
		$values[] = dbquote($form->value("franchisee_address_company"));

		$fields[] = "address_company2";
		$values[] = dbquote($form->value("franchisee_address_company2"));

		$fields[] = "address_address";
		$values[] = dbquote($form->value("franchisee_address_address"));

		$fields[] = "address_address2";
		$values[] = dbquote($form->value("franchisee_address_address2"));

		$fields[] = "address_zip";
		$values[] = dbquote($form->value("franchisee_address_zip"));

		$fields[] = "address_place";
		$values[] = dbquote($form->value("franchisee_address_place"));

		$fields[] = "address_place_id";
		$values[] = dbquote($form->value("franchisee_address_place_id"));

		$fields[] = "address_country";
		$values[] = dbquote($form->value("franchisee_address_country"));

		$fields[] = "address_phone";
		$values[] = dbquote($form->value("franchisee_address_phone"));

		$fields[] = "address_fax";
		$values[] = dbquote($form->value("franchisee_address_fax"));

		$fields[] = "address_email";
		$values[] = dbquote($form->value("franchisee_address_email"));

		$fields[] = "address_contact_name";
		$values[] = dbquote($form->value("franchisee_address_contact"));

		$fields[] = "address_website";
		$values[] = dbquote($form->value("franchisee_address_website"));

		$fields[] = "address_type";
		$values[] = 7;

		$fields[] = "address_canbefranchisee";
		$values[] = 1;

		$fields[] = "address_showinposindex";
		$values[] = 1;

		$fields[] = "address_active";
		$values[] = 1;

		$fields[] = "address_parent";
		$values[] = dbquote($form->value("franchisee_address_parent"));;

		$fields[] = "address_checked";
		$values[] = "0";

		$fields[] = "date_created";
		$values[] = "current_timestamp";

		$fields[] = "date_modified";
		$values[] = "current_timestamp";

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$fields[] = "user_modified";
		$values[] = dbquote(user_login());

		$sql = "insert into addresses (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
		
		mysql_query($sql) or dberror($sql);

		$franchisee_id = mysql_insert_id();

	}
	elseif($form->value("order_franchisee_address_id") > 0)
	{
		$fields = array();
		$value = trim($form->value("franchisee_address_company")) == "" ? "null" : dbquote($form->value("franchisee_address_company"));
		$fields[] = "address_company = " . $value;

		$value = trim($form->value("franchisee_address_company2")) == "" ? "null" : dbquote($form->value("franchisee_address_company2"));
		$fields[] = "address_company2 = " . $value;

		$value = trim($form->value("franchisee_address_address")) == "" ? "null" : dbquote($form->value("franchisee_address_address"));
		$fields[] = "address_address = " . $value;

		$value = trim($form->value("franchisee_address_address2")) == "" ? "null" : dbquote($form->value("franchisee_address_address2"));
		$fields[] = "address_address2 = " . $value;

		$value = trim($form->value("franchisee_address_zip")) == "" ? "null" : dbquote($form->value("franchisee_address_zip"));
		$fields[] = "address_zip = " . $value;

		$value = trim($form->value("franchisee_address_place")) == "" ? "null" : dbquote($form->value("franchisee_address_place"));
		$fields[] = "address_place = " . $value;

		$value = trim($form->value("franchisee_address_place_id")) == "" ? "null" : dbquote($form->value("franchisee_address_place_id"));
		$fields[] = "address_place_id = " . $value;

		$value = trim($form->value("franchisee_address_country")) == "" ? "null" : dbquote($form->value("franchisee_address_country"));
		$fields[] = "address_country = " . $value;

		$value = trim($form->value("franchisee_address_phone")) == "" ? "null" : dbquote($form->value("franchisee_address_phone"));
		$fields[] = "address_phone = " . $value;

		$value = trim($form->value("franchisee_address_fax")) == "" ? "null" : dbquote($form->value("franchisee_address_fax"));
		$fields[] = "address_fax = " . $value;

		$value = trim($form->value("franchisee_address_email")) == "" ? "null" : dbquote($form->value("franchisee_address_email"));
		$fields[] = "address_email = " . $value;

		$value = trim($form->value("franchisee_address_contact")) == "" ? "null" : dbquote($form->value("franchisee_address_contact"));
		$fields[] = "address_contact_name = " . $value;

		$value = trim($form->value("franchisee_address_website")) == "" ? "null" : dbquote($form->value("franchisee_address_website"));
		$fields[] = "address_website = " . $value;

		$value = "current_timestamp";
		$fields[] = "date_modified = " . $value;

		if (isset($_SESSION["user_login"]))
		{
			$value = dbquote($_SESSION["user_login"]);
			$fields[] = "user_modified = " . $value;
		}
		
		$sql = "update addresses set " . join(", ", $fields) . " where address_id = " . dbquote($form->value("order_franchisee_address_id"));
		mysql_query($sql) or dberror($sql);

	}

	// POS address
	$order_fields = array();
    $value = trim($form->value("shop_address_company")) == "" ? "null" : dbquote($form->value("shop_address_company"));
    $order_fields[] = "order_shop_address_company = " . $value;

    $value = trim($form->value("shop_address_company2")) == "" ? "null" : dbquote($form->value("shop_address_company2"));
    $order_fields[] = "order_shop_address_company2 = " . $value;

    $value = trim($form->value("shop_address_address")) == "" ? "null" : dbquote($form->value("shop_address_address"));
    $order_fields[] = "order_shop_address_address = " . $value;

    $value = trim($form->value("shop_address_address2")) == "" ? "null" : dbquote($form->value("shop_address_address2"));
    $order_fields[] = "order_shop_address_address2 = " . $value;

    $value = trim($form->value("shop_address_zip")) == "" ? "null" : dbquote($form->value("shop_address_zip"));
    $order_fields[] = "order_shop_address_zip = " . $value;

    $value = trim($form->value("shop_address_place")) == "" ? "null" : dbquote($form->value("shop_address_place"));
    $order_fields[] = "order_shop_address_place = " . $value;

    $value = trim($form->value("shop_address_country")) == "" ? "null" : dbquote($form->value("shop_address_country"));
    $order_fields[] = "order_shop_address_country = " . $value;
    
    $value = trim($form->value("shop_address_phone")) == "" ? "null" : dbquote($form->value("shop_address_phone"));
    $order_fields[] = "order_shop_address_phone = " . $value;
    
    $value = trim($form->value("shop_address_fax")) == "" ? "null" : dbquote($form->value("shop_address_fax"));
    $order_fields[] = "order_shop_address_fax = " . $value;
    
    $value = trim($form->value("shop_address_email")) == "" ? "null" : dbquote($form->value("shop_address_email"));
    $order_fields[] = "order_shop_address_email = " . $value;

	// franchisee address
	
	if($franchisee_id > 0)
	{
		$value = $franchisee_id;
	}
	else
	{
		$value = trim($form->value("order_franchisee_address_id")) == "" ? "0" : dbquote($form->value("order_franchisee_address_id"));
	}
    $order_fields[] = "order_franchisee_address_id = " . $value;

	$value = trim($form->value("franchisee_address_company")) == "" ? "null" : dbquote($form->value("franchisee_address_company"));
    $order_fields[] = "order_franchisee_address_company = " . $value;

     $value = trim($form->value("franchisee_address_company2")) == "" ? "null" : dbquote($form->value("franchisee_address_company2"));
    $order_fields[] = "order_franchisee_address_company2 = " . $value;

    $value = trim($form->value("franchisee_address_address")) == "" ? "null" : dbquote($form->value("franchisee_address_address"));
    $order_fields[] = "order_franchisee_address_address = " . $value;

    $value = trim($form->value("franchisee_address_address2")) == "" ? "null" : dbquote($form->value("franchisee_address_address2"));
    $order_fields[] = "order_franchisee_address_address2 = " . $value;

    $value = trim($form->value("franchisee_address_zip")) == "" ? "null" : dbquote($form->value("franchisee_address_zip"));
    $order_fields[] = "order_franchisee_address_zip = " . $value;

    $value = trim($form->value("franchisee_address_place")) == "" ? "null" : dbquote($form->value("franchisee_address_place"));
    $order_fields[] = "order_franchisee_address_place = " . $value;

    $value = trim($form->value("franchisee_address_country")) == "" ? "null" : dbquote($form->value("franchisee_address_country"));
    $order_fields[] = "order_franchisee_address_country = " . $value;

    $value = trim($form->value("franchisee_address_phone")) == "" ? "null" : dbquote($form->value("franchisee_address_phone"));
    $order_fields[] = "order_franchisee_address_phone = " . $value;

    $value = trim($form->value("franchisee_address_fax")) == "" ? "null" : dbquote($form->value("franchisee_address_fax"));
    $order_fields[] = "order_franchisee_address_fax = " . $value;

    $value = trim($form->value("franchisee_address_email")) == "" ? "null" : dbquote($form->value("franchisee_address_email"));
    $order_fields[] = "order_franchisee_address_email = " . $value;

    $value = trim($form->value("franchisee_address_contact")) == "" ? "null" : dbquote($form->value("franchisee_address_contact"));
    $order_fields[] = "order_franchisee_address_contact = " . $value;

	$value = trim($form->value("franchisee_address_website")) == "" ? "null" : dbquote($form->value("franchisee_address_website"));
    $order_fields[] = "order_franchisee_address_website = " . $value;

    $value = "current_timestamp";
    $order_fields[] = "date_modified = " . $value;

    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $order_fields[] = "user_modified = " . $value;
    }

    $sql = "update orders set " . join(", ", $order_fields) . " where order_id = " . $form->value("oid");
    mysql_query($sql) or dberror($sql);

    
    // update record in table project_costs
    $value = dbquote($form->value("shop_sqms"));
    $project_cost_fields[] = "project_cost_sqms = " . $value;

	$value = dbquote($form->value("shop_gross_sqms"));
    $project_cost_fields[] = "project_cost_gross_sqms = " . $value;

	$value = dbquote($form->value("shop_totalsqms"));
    $project_cost_fields[] = "project_cost_totalsqms = " . $value;

	$value = dbquote($form->value("shop_bakoffocesqms"));
    $project_cost_fields[] = "project_cost_backofficesqms = " . $value;

	$value = dbquote($form->value("shop_numfloors"));
    $project_cost_fields[] = "project_cost_numfloors = " . $value;

	$value = dbquote($form->value("shop_floorsurface1"));
    $project_cost_fields[] = "project_cost_floorsurface1 = " . $value;

	$value = dbquote($form->value("shop_floorsurface2"));
    $project_cost_fields[] = "project_cost_floorsurface2 = " . $value;

	$value = dbquote($form->value("shop_floorsurface3"));
    $project_cost_fields[] = "project_cost_floorsurface3 = " . $value;

    $value = "current_timestamp";
    $project_cost_fields[] = "date_modified = " . $value;
    
    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $project_cost_fields[] = "user_modified = " . $value;
    }

    $sql = "update project_costs set " . join(", ", $project_cost_fields) . " where project_cost_order = " . $form->value("oid");
    mysql_query($sql) or dberror($sql);

	
	// update Posaddresses (POS Index)
	if($form->value("posaddress_id") > 0)
	{
		//check if posorder is already there
		$sql = "select posorder_posaddress " . 
			   "from posorders " . 
			   "where posorder_order = " . $form->value("oid");
		
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$action = "update posaddress";
		}
		else
		{
			//check if a temporary posorder exists
			$sql = "select posorder_posaddress, posorder_parent_table " . 
				   "from posorderspipeline " . 
				   "where posorder_order = " . $form->value("oid");
		
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				if($row["posorder_parent_table"] == "posaddresses")
				{
					$action = "update posaddress";
				}
				else
				{
					$action = "update posaddresspipeline";
				}
			}
			else //create new posorder record
			{
				$action = "create new posorder";
			}
		}


		//update posaddress
		$fields = array();

		if($form->value("order_franchisee_address_id"))
		{
			$value = trim($form->value("order_franchisee_address_id")) == "" ? "0" : dbquote($form->value("order_franchisee_address_id"));
			$fields[] = "posaddress_franchisee_id = " . $value;
		}
		
		
		$value = trim($form->value("eprepnr")) == "" ? "''" : dbquote($form->value("eprepnr"));
		$fields[] = "posaddress_eprepnr = " . $value;

		$value = trim($form->value("sapnr")) == "" ? "''" : dbquote($form->value("sapnr"));
		$fields[] = "posaddress_sapnumber = " . $value;

		if(array_key_exists("project_fagagreement_type", $form->items))
		{
			if($form->value("project_fagagreement_type"))
			{
				$value = trim($form->value("project_fagagreement_type")) == "" ? "0" : dbquote($form->value("project_fagagreement_type"));
				$fields[] = "posaddress_fagagreement_type = " . $value;
			}
			
			if($form->value("project_fagrsent"))
			{
				$value = trim($form->value("project_fagrsent")) == "" ? "0" : dbquote($form->value("project_fagrsent"));
				$fields[] = "posaddress_fagrsent = " . $value;
			}
			
			if($form->value("project_fagrsigned"))
			{
				$value = trim($form->value("project_fagrsigned")) == "" ? "0" : dbquote($form->value("project_fagrsigned"));
				$fields[] = "posaddress_fagrsigned = " . $value;
			}

			if($form->value("project_fagrstart"))
			{
				$value = trim($form->value("project_fagrstart")) == "" ? "1" : dbquote(from_system_date($form->value("project_fagrstart")));
				$fields[] = "posaddress_fagrstart = " . $value;
			}

			if($form->value("project_fagrend"))
			{
				$value = trim($form->value("project_fagrend")) == "" ? "1" : dbquote(from_system_date($form->value("project_fagrend")));
				$fields[] = "posaddress_fagrend = " . $value;
			}

			if($form->value("project_fagrend"))
			{
				$value = trim($form->value("project_fag_comment")) == "" ? "null" : dbquote($form->value("project_fag_comment"));
				$fields[] = "posaddress_fag_comment = " . $value;

			}
		}

		// POS address
		//$fields = array();
		if($form->value('overwrite_posaddress_data') == 1)
		{
			$value = trim($form->value("shop_address_company")) == "" ? "null" : dbquote($form->value("shop_address_company"));
			$fields[] = "posaddress_name = " . $value;

			$value = trim($form->value("shop_address_company2")) == "" ? "null" : dbquote($form->value("shop_address_company2"));
			$fields[] = "posaddress_name2 = " . $value;

			$value = trim($form->value("shop_address_address")) == "" ? "null" : dbquote($form->value("shop_address_address"));
			$fields[] = "posaddress_address = " . $value;

			$value = trim($form->value("shop_address_address2")) == "" ? "null" : dbquote($form->value("shop_address_address2"));
			$fields[] = "posaddress_address2 = " . $value;

			$value = trim($form->value("shop_address_zip")) == "" ? "null" : dbquote($form->value("shop_address_zip"));
			$fields[] = "posaddress_zip = " . $value;

			$value = trim($form->value("shop_address_place")) == "" ? "null" : dbquote($form->value("shop_address_place"));
			$fields[] = "posaddress_place = " . $value;

			$value = trim($form->value("posaddress_place_id")) == "" ? "null" : dbquote($form->value("posaddress_place_id"));
			$fields[] = "posaddress_place_id = " . $value;

			$value = trim($form->value("shop_address_country")) == "" ? "null" : dbquote($form->value("shop_address_country"));
			$fields[] = "posaddress_country = " . $value;
			
			$value = trim($form->value("shop_address_phone")) == "" ? "null" : dbquote($form->value("shop_address_phone"));
			$fields[] = "posaddress_phone = " . $value;
			
			$value = trim($form->value("shop_address_fax")) == "" ? "null" : dbquote($form->value("shop_address_fax"));
			$fields[] = "posaddress_fax = " . $value;
			
			$value = trim($form->value("shop_address_email")) == "" ? "null" : dbquote($form->value("shop_address_email"));
			$fields[] = "posaddress_email = " . $value;
			
		}

		$value = trim($form->value("posaddress_google_lat")) == "" ? "null" : dbquote($form->value("posaddress_google_lat"));
		$fields[] = "posaddress_google_lat = " . $value;

		$value = trim($form->value("posaddress_google_long")) == "" ? "null" : dbquote($form->value("posaddress_google_long"));
		$fields[] = "posaddress_google_long = " . $value;

		$value = trim($form->value("posaddress_google_precision")) == "" ? "null" : dbquote($form->value("posaddress_google_precision"));
		$fields[] = "posaddress_google_precision = " . $value;

		if(array_key_exists("posaddress_perc_class", $form->items))
		{
			$value = trim($form->value("posaddress_perc_class")) == "" ? "0" : dbquote($form->value("posaddress_perc_class"));
			$fields[] = "posaddress_perc_class = " . $value;

			$value = trim($form->value("posaddress_perc_tourist")) == "" ? "0" : dbquote($form->value("posaddress_perc_tourist"));
			$fields[] = "posaddress_perc_tourist = " . $value;

			$value = trim($form->value("posaddress_perc_transport")) == "" ? "0" : dbquote($form->value("posaddress_perc_transport"));
			$fields[] = "posaddress_perc_transport = " . $value;

			$value = trim($form->value("posaddress_perc_people")) == "" ? "0" : dbquote($form->value("posaddress_perc_people"));
			$fields[] = "posaddress_perc_people = " . $value;

			$value = trim($form->value("posaddress_perc_parking")) == "" ? "0" : dbquote($form->value("posaddress_perc_parking"));
			$fields[] = "posaddress_perc_parking = " . $value;

			$value = trim($form->value("posaddress_perc_visibility1")) == "" ? "0" : dbquote($form->value("posaddress_perc_visibility1"));
			$fields[] = "posaddress_perc_visibility1 = " . $value;

			$value = trim($form->value("posaddress_perc_visibility2")) == "" ? "0" : dbquote($form->value("posaddress_perc_visibility2"));
			$fields[] = "posaddress_perc_visibility2 = " . $value;
		}

		if(array_key_exists("shop_totalsqms", $form->items) and $action == "update posaddresspipeline")
		{

			$value = dbquote($form->value("shop_gross_sqms"));
			$fields[] = "posaddress_store_grosssurface = " . $value;

			$value = dbquote($form->value("shop_totalsqms"));
			$fields[] = "posaddress_store_totalsurface = " . $value;

			$value = dbquote($form->value("shop_sqms"));
			$fields[] = "posaddress_store_retailarea = " . $value;

			$value = dbquote($form->value("shop_bakoffocesqms"));
			$fields[] = "posaddress_store_backoffice = " . $value;

			$value = dbquote($form->value("shop_numfloors"));
			$fields[] = "posaddress_store_numfloors = " . $value;

			$value = dbquote($form->value("shop_floorsurface1"));
			$fields[] = "posaddress_store_floorsurface1 = " . $value;

			$value = dbquote($form->value("shop_floorsurface2"));
			$fields[] = "posaddress_store_floorsurface2 = " . $value;

			$value = dbquote($form->value("shop_floorsurface3"));
			$fields[] = "posaddress_store_floorsurface3 = " . $value;
		}


		$value = "current_timestamp";
		$fields[] = "date_modified = " . $value;

		if (isset($_SESSION["user_login"]))
		{
			$value = dbquote($_SESSION["user_login"]);
			$fields[] = "user_modified = " . $value;
		}

		if($action == "update posaddress")
		{
			$sql = "update posaddresses set " . join(", ", $fields) . " where posaddress_id = " . dbquote($form->value("posaddress_id"));
			mysql_query($sql) or dberror($sql);
		}
		elseif($action == "update posaddresspipeline")
		{
			
			$value = trim($form->value("posaddress_distribution_channel")) == "" ? "null" : dbquote($form->value("posaddress_distribution_channel"));
			$fields[] = "posaddress_distribution_channel = " . $value;

			$sql = "update posaddressespipeline set " . join(", ", $fields) . " where posaddress_id = " . dbquote($form->value("posaddress_id"));
			mysql_query($sql) or dberror($sql);

		}

		$pos_id = $form->value("posaddress_id");
	
		//posorders
		if($form->value("pipeline") == 0)
		{
			
			if($action == "create new posorder" and $form->value("posaddress_id") > 0)
			{
				$pos_id = $form->value("posaddress_id");

				$fields = array();
				$values = array();

				$fields[] = "posorder_posaddress";
				$values[] = dbquote($pos_id);

				$fields[] = "posorder_order";
				$values[] = dbquote($form->value("oid"));

				$fields[] = "posorder_type";
				$values[] = 1;

				if(array_key_exists("project_floor", $form->items))
				{
					$fields[] = "posorder_floor";
					$values[] = dbquote($form->value("project_floor"));
				}


				if(array_key_exists("posorder_neighbour_left", $form->items))
				{
					$fields[] = "posorder_neighbour_left";
					$values[] = dbquote($form->value("posorder_neighbour_left"));

					$fields[] = "posorder_neighbour_right";
					$values[] = dbquote($form->value("posorder_neighbour_right"));

					$fields[] = "posorder_neighbour_acrleft";
					$values[] = dbquote($form->value("posorder_neighbour_acrleft"));

					$fields[] = "posorder_neighbour_acrright";
					$values[] = dbquote($form->value("posorder_neighbour_acrright"));

					$fields[] = "posorder_neighbour_brands";
					$values[] = dbquote($form->value("posorder_neighbour_brands"));
				}

				
				$fields[] = "date_created";
				$values[] = dbquote(date("Y-m-d H:i:s"));

				$fields[] = "date_modified";
				$values[] = dbquote(date("Y-m-d H:i:s"));

				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				$fields[] = "user_modified";
				$values[] = dbquote(user_login());
				
				$sql = "insert into posorders (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);
				$result = update_posorders_from_projects($pos_id);
			}
			else
			{
				$sql = "select posorder_id " . 
					   "from posorders " . 
					   "where posorder_order = " . $form->value("oid");
				
				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
				
					$sql = "Update posorders set " . 
							   "posorder_posaddress = " . $form->value("posaddress_id") . 
							   " where posorder_id = " . $row["posorder_id"];

					
					if(array_key_exists("posorder_neighbour_left", $form->items))
					{
						$sql = "Update posorders set " . 
							   "posorder_posaddress = " . $form->value("posaddress_id") . ", " .
							   "posorder_neighbour_left = " . dbquote($form->value("posorder_neighbour_left")) . ", " .
							   "posorder_neighbour_right = " . dbquote($form->value("posorder_neighbour_right")) . ", " .
							   "posorder_neighbour_acrleft = " . dbquote($form->value("posorder_neighbour_acrleft")) . ", " .
							   "posorder_neighbour_acrright = " . dbquote($form->value("posorder_neighbour_acrright")) . ", " .
							   "posorder_neighbour_brands = " . dbquote($form->value("posorder_neighbour_brands")) . 
							   " where posorder_id = " . $row["posorder_id"];
					}

					mysql_query($sql) or dberror($sql);

					if(array_key_exists("project_floor", $form->items))
					{
						$sql = "Update posorders set " . 
							   "posorder_floor = " . $form->value("project_floor") . 
							   " where posorder_id = " . $row["posorder_id"];
						mysql_query($sql) or dberror($sql);
					}
				}
				$result = update_posdata_from_posorders($form->value("posaddress_id"));
			}
		}
		else
		{
			$sql = "select posorder_id " . 
				   "from posorderspipeline " . 
				   "where posorder_order = " . $form->value("oid");
			
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
			
				$sql = "Update posorderspipeline set " . 
					   "posorder_posaddress = " . $form->value("posaddress_id") . 
					   " where posorder_id = " . $row["posorder_id"];

				if(array_key_exists("posorder_neighbour_left", $form->items))
				{
					$sql = "Update posorderspipeline set " . 
						   "posorder_posaddress = " . $form->value("posaddress_id") . ", " .
						   "posorder_neighbour_left = " . dbquote($form->value("posorder_neighbour_left")) . ", " .
						   "posorder_neighbour_right = " . dbquote($form->value("posorder_neighbour_right")) . ", " .
						   "posorder_neighbour_acrleft = " . dbquote($form->value("posorder_neighbour_acrleft")) . ", " .
						   "posorder_neighbour_acrright = " . dbquote($form->value("posorder_neighbour_acrright")) . ", " .
						   "posorder_neighbour_brands = " . dbquote($form->value("posorder_neighbour_brands")) . 
						   " where posorder_id = " . $row["posorder_id"];
				}

				mysql_query($sql) or dberror($sql);

				if(array_key_exists("project_floor", $form->items))
				{
					$sql = "Update posorderspipeline set " . 
						   "posorder_floor = " . $form->value("project_floor") . 
						   " where posorder_id = " . $row["posorder_id"];

					mysql_query($sql) or dberror($sql);
				
				}
			}
		}
	}
	
	// create new posaddress if new project and upon entering an actual shop opening date
	if($form->value("shop_actual_opening_date"))
	{
		$project = get_project($form->value("pid"));
		$order = get_order($form->value("oid"));
		
		$action = "";

		//check if pos is already there
		$sql = "select posorder_posaddress " . 
			   "from posorders " . 
			   "where posorder_order = " . $form->value("oid");
		
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$action = "update posorder and posaddress";
			$posaddress_in_pipeline = false;
			
		}
		else
		{
			//check if a temporary posorder exists
			$sql = "select posorder_posaddress, posorder_parent_table " . 
				   "from posorderspipeline " . 
				   "where posorder_order = " . $form->value("oid");

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				if($row["posorder_parent_table"] == "posaddresses")
				{
					$action = "transfer posorder and update posaddress";
					$posaddress_in_pipeline = false;
				}
				else
				{
					$action = "transfer posorder and transfer posaddress";
					$posaddress_in_pipeline = true;
				}
			}
			else // Project does not exist in POS Index (before introduction of multiform)
			{
				$action = "update project older than october 2008";
				$posaddress_in_pipeline = false;
			}
			
		}


		//update franchisee
		$fields = array();
		if($form->value("order_franchisee_address_id"))
		{
			$value = trim($form->value("order_franchisee_address_id")) == "" ? "0" : dbquote($form->value("order_franchisee_address_id"));
			$fields[] = "posaddress_franchisee_id = " . $value;

			if($posaddress_in_pipeline == false) {
				$sql = "update posaddresses set " . join(", ", $fields) . " where posaddress_id = " . dbquote($form->value("posaddress_id"));
			}
			else
			{
				$sql = "update posaddressespipeline set " . join(", ", $fields) . " where posaddress_id = " . dbquote($form->value("posaddress_id"));
			}
			mysql_query($sql) or dberror($sql);
		}
		
		//do processing
		if($action == "update posorder and posaddress")
		{
			$result = update_posdata_from_posorders($form->value("posaddress_id"));
			$pos_id = $form->value("posaddress_id");

		}
		elseif($action == "transfer posorder and update posaddress")
		{
			$sql = "select * from posorderspipeline where posorder_order = " . $form->value("oid");
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$pos_id = $row["posorder_posaddress"];

				$fields = array();
				$values = array();

				$fields[] = "posorder_posaddress";
				$values[] = dbquote($pos_id);

				$fields[] = "posorder_order";
				$values[] = dbquote($row["posorder_order"]);

				$fields[] = "posorder_type";
				$values[] = dbquote($row["posorder_type"]);

				$fields[] = "posorder_ordernumber";
				$values[] = dbquote($row["posorder_ordernumber"]);

				$fields[] = "posorder_year";
				$values[] = dbquote($row["posorder_year"]);

				$fields[] = "posorder_product_line";
				$values[] = dbquote($row["posorder_product_line"]);

				$fields[] = "posorder_product_line_subclass";
				$values[] = dbquote($row["posorder_product_line_subclass"]);

				$fields[] = "posorder_postype";
				$values[] = dbquote($row["posorder_postype"]);

				$fields[] = "posorder_subclass";
				$values[] = dbquote($row["posorder_subclass"]);

				$fields[] = "posorder_project_kind";
				$values[] = dbquote($row["posorder_project_kind"]);

				$fields[] = "posorder_legal_type";
				$values[] = dbquote($row["posorder_legal_type"]);

				$fields[] = "posorder_system_currency";
				$values[] = dbquote($row["posorder_system_currency"]);

				$fields[] = "posorder_budget_approved_sc";
				$values[] = dbquote($row["posorder_budget_approved_sc"]);

				$fields[] = "posorder_real_cost_sc";
				$values[] = dbquote($row["posorder_real_cost_sc"]);

				$fields[] = "posorder_client_currency";
				$values[] = dbquote($row["posorder_client_currency"]);

				$fields[] = "posorder_budget_approved_cc";
				$values[] = dbquote($row["posorder_budget_approved_cc"]);

				$fields[] = "posorder_real_cost_cc";
				$values[] = dbquote($row["posorder_real_cost_cc"]);

				$fields[] = "posorder_floor";
				$values[] = dbquote($row["posorder_floor"]);

				$fields[] = "posorder_neighbour_left";
				$values[] = dbquote($row["posorder_neighbour_left"]);

				$fields[] = "posorder_neighbour_right";
				$values[] = dbquote($row["posorder_neighbour_right"]);

				$fields[] = "posorder_neighbour_acrleft";
				$values[] = dbquote($row["posorder_neighbour_acrleft"]);

				$fields[] = "posorder_neighbour_acrright";
				$values[] = dbquote($row["posorder_neighbour_acrright"]);

				$fields[] = "posorder_neighbour_brands";
				$values[] = dbquote($row["posorder_neighbour_brands"]);

				$fields[] = "posorder_neighbour_comment";
				$values[] = dbquote($row["posorder_neighbour_comment"]);

				$fields[] = "posorder_currency_symbol";
				$values[] = dbquote($row["posorder_currency_symbol"]);

				$fields[] = "posorder_exchangerate";
				$values[] = dbquote($row["posorder_exchangerate"]);

				$fields[] = "posorder_remark";
				$values[] = dbquote($row["posorder_remark"]);

				$fields[] = "posorder_project_locally_produced";
				$values[] = dbquote($row["posorder_project_locally_produced"]);

				$fields[] = "date_created";
				$values[] = dbquote(date("Y-m-d H:i:s"));

				$fields[] = "date_modified";
				$values[] = dbquote(date("Y-m-d H:i:s"));

				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				$fields[] = "user_modified";
				$values[] = dbquote(user_login());
				
				$sql = "insert into posorders (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);
				

				$sql = "delete from posorderspipeline where posorder_order = " . $form->value("oid");
				mysql_query($sql) or dberror($sql);

				$result = update_posdata_from_posorders($pos_id);

			}

			//transfer posleases
			$sql = "select * from posleasespipeline where poslease_posaddress = " . dbquote($pos_id);

			$res = mysql_query($sql) or dberror($sql);
			while ($row = mysql_fetch_assoc($res))
			{
				$fields = array();
				$values = array();


				$fields[] = "poslease_posaddress";
				$values[] = dbquote($pos_id);

				$fields[] = "poslease_order";
				$values[] = dbquote($row["poslease_ordere"]);

				$fields[] = "poslease_lease_type";
				$values[] = dbquote($row["poslease_lease_type"]);
				
				$fields[] = "poslease_anual_rent";
				$values[] = dbquote($row["poslease_anual_rent"]);

				$fields[] = "poslease_salespercent";
				$values[] = dbquote($row["poslease_salespercent"]);

				$fields[] = "poslease_indexclause_in_contract";
				$values[] = dbquote($row["poslease_indexclause_in_contract"]);

				$fields[] = "poslease_isindexed";
				$values[] = dbquote($row["poslease_isindexed"]);

				$fields[] = "poslease_indexrate";
				$values[] = dbquote($row["poslease_indexrate"]);

				$fields[] = "poslease_average_increase";
				$values[] = dbquote($row["poslease_average_increase"]);

				$fields[] = "poslease_realestate_fee";
				$values[] = dbquote($row["poslease_realestate_fee"]);

				$fields[] = "poslease_annual_charges";
				$values[] = dbquote($row["poslease_annual_charges"]);

				$fields[] = "poslease_other_fees";
				$values[] = dbquote($row["poslease_other_fees"]);
				
				$fields[] = "poslease_startdate";
				$values[] = dbquote($row["poslease_startdate"]);

				$fields[] = "poslease_enddate";
				$values[] = dbquote($row["poslease_enddate"]);

				$fields[] = "poslease_extensionoption";
				$values[] = dbquote($row["poslease_extensionoption"]);

				$fields[] = "poslease_handoverdate";
				$values[] = dbquote($row["poslease_handoverdate"]);

				$fields[] = "poslease_firstrentpayed";
				$values[] = dbquote($row["poslease_firstrentpayed"]);

				$fields[] = "poslease_freeweeks";
				$values[] = dbquote($row["poslease_freeweeks"]);

				$fields[] = "poslease_hasfixrent";
				$values[] = dbquote($row["poslease_hasfixrent"]);

				$fields[] = "poslease_breakpoint_percent";
				$values[] = dbquote($row["poslease_breakpoint_percent"]);

				$fields[] = "poslease_breakpoint_amount";
				$values[] = dbquote($row["poslease_breakpoint_amount"]);

				$fields[] = "poslease_breakpoint_percent2";
				$values[] = dbquote($row["poslease_breakpoint_percent2"]);

				$fields[] = "poslease_breakpoint_amount2";
				$values[] = dbquote($row["poslease_breakpoint_amount2"]);

				$fields[] = "poslease_breakpoint_percent3";
				$values[] = dbquote($row["poslease_breakpoint_percent3"]);

				$fields[] = "poslease_breakpoint_amount3";
				$values[] = dbquote($row["poslease_breakpoint_amount3"]);

				$fields[] = "poslease_exitoption";
				$values[] = dbquote($row["poslease_exitoption"]);

				$fields[] = "poslease_negotiator";
				$values[] = dbquote($row["poslease_negotiator"]);

				$fields[] = "poslease_landlord_name";
				$values[] = dbquote($row["poslease_landlord_name"]);

				$fields[] = "poslease_negotiated_conditions";
				$values[] = dbquote($row["poslease_negotiated_conditions"]);

				$fields[] = "poslease_termination_time";
				$values[] = dbquote($row["poslease_termination_time"]);

				$fields[] = "poslease_mailalert2";
				$values[] = dbquote($row["poslease_mailalert2"]);

				$fields[] = "date_created";
				$values[] = dbquote(date("Y-m-d H:i:s"));

				$fields[] = "date_modified";
				$values[] = dbquote(date("Y-m-d H:i:s"));

				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				$fields[] = "user_modified";
				$values[] = dbquote(user_login());

				$sql = "insert into posleases (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);
		
			}

			$sql = "delete from posleasespipeline where poslease_posaddress = " . dbquote($pos_id);
			mysql_query($sql) or dberror($sql);
		}
		elseif($action == "transfer posorder and transfer posaddress")
		{
			$sql = "select * from posorderspipeline where posorder_order = " . $form->value("oid");
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$posorder_pos_id = $row["posorder_posaddress"];

				$sql1 = "select * from posaddressespipeline where posaddress_id = " . $posorder_pos_id;
				$res1 = mysql_query($sql1) or dberror($sql1);
				if ($row1 = mysql_fetch_assoc($res1))
				{
					//pos address
					$fields = array();
					$values = array();

					$fields[] = "posaddress_client_id";
					$values[] = dbquote($row1["posaddress_client_id"]);

					$fields[] = "posaddress_sapnumber";
					$values[] = dbquote($row1["posaddress_sapnumber"]);

					$fields[] = "posaddress_eprepnr";
					$values[] = dbquote($row1["posaddress_eprepnr"]);

					$fields[] = "posaddress_ownertype";
					$values[] = dbquote($row1["posaddress_ownertype"]);

					$fields[] = "posaddress_franchisor_id";
					$values[] = dbquote($row1["posaddress_franchisor_id"]);

					$fields[] = "posaddress_franchisee_id";
					$values[] = dbquote($row1["posaddress_franchisee_id"]);

					$fields[] = "posaddress_name";
					$values[] = dbquote($row1["posaddress_name"]);

					$fields[] = "posaddress_name2";
					$values[] = dbquote($row1["posaddress_name2"]);

					$fields[] = "posaddress_address";
					$values[] = dbquote($row1["posaddress_address"]);

					$fields[] = "posaddress_address2";
					$values[] = dbquote($row1["posaddress_address2"]);

					$fields[] = "posaddress_zip";
					$values[] = dbquote($row1["posaddress_zip"]);

					$fields[] = "posaddress_place";
					$values[] = dbquote($row1["posaddress_place"]);

					$fields[] = "posaddress_place_id";
					$values[] = dbquote($row1["posaddress_place_id"]);

					$fields[] = "posaddress_country";
					$values[] = dbquote($row1["posaddress_country"]);
					
					$fields[] = "posaddress_phone";
					$values[] = dbquote($row1["posaddress_phone"]);

					$fields[] = "posaddress_fax";
					$values[] = dbquote($row1["posaddress_fax"]);

					$fields[] = "posaddress_email";
					$values[] = dbquote($row1["posaddress_email"]);

					$fields[] = "posaddress_google_lat";
					$values[] = dbquote($row1["posaddress_google_lat"]);

					$fields[] = "posaddress_google_long";
					$values[] = dbquote($row1["posaddress_google_long"]);

					$fields[] = "posaddress_google_precision";
					$values[] = dbquote($row1["posaddress_google_precision"]);

					$fields[] = "posaddress_store_postype";
					$values[] = dbquote($row1["posaddress_store_postype"]);

					$fields[] = "posaddress_store_subclass";
					$values[] = dbquote($row1["posaddress_store_subclass"]);

					$fields[] = "posaddress_store_furniture";
					$values[] = dbquote($row1["posaddress_store_furniture"]);

					$fields[] = "posaddress_store_grosssurface";
					$values[] = dbquote($row1["posaddress_store_grosssurface"]);

					$fields[] = "posaddress_store_totalsurface";
					$values[] = dbquote($row1["posaddress_store_totalsurface"]);

					$fields[] = "posaddress_store_retailarea";
					$values[] = dbquote($row1["posaddress_store_retailarea"]);

					$fields[] = "posaddress_store_backoffice";
					$values[] = dbquote($row1["posaddress_store_backoffice"]);

					$fields[] = "posaddress_store_numfloors";
					$values[] = dbquote($row1["posaddress_store_numfloors"]);

					$fields[] = "posaddress_store_floorsurface1";
					$values[] = dbquote($row1["posaddress_store_floorsurface1"]);

					$fields[] = "posaddress_store_floorsurface2";
					$values[] = dbquote($row1["posaddress_store_floorsurface2"]);

					$fields[] = "posaddress_store_floorsurface3";
					$values[] = dbquote($row1["posaddress_store_floorsurface3"]);


					$fields[] = "posaddress_fagagreement_type";
					$values[] = dbquote($row1["posaddress_fagagreement_type"]);

					$fields[] = "posaddress_fagrsent";
					$values[] = dbquote($row1["posaddress_fagrsent"]);

					$fields[] = "posaddress_fagrsigned";
					$values[] = dbquote($row1["posaddress_fagrsigned"]);

					$fields[] = "posaddress_fagrstart";
					$values[] = dbquote($row1["posaddress_fagrstart"]);

					$fields[] = "posaddress_fagrend";
					$values[] = dbquote($row1["posaddress_fagrend"]);

					$fields[] = "posaddress_fag_comment";
					$values[] = dbquote($row1["posaddress_fag_comment"]);

					$fields[] = "posaddress_perc_class";
					$values[] = dbquote($row1["posaddress_perc_class"]);

					$fields[] = "posaddress_perc_tourist";
					$values[] = dbquote($row1["posaddress_perc_tourist"]);

					$fields[] = "posaddress_perc_transport";
					$values[] = dbquote($row1["posaddress_perc_transport"]);

					$fields[] = "posaddress_perc_people";
					$values[] = dbquote($row1["posaddress_perc_people"]);

					$fields[] = "posaddress_perc_parking";
					$values[] = dbquote($row1["posaddress_perc_parking"]);

					$fields[] = "posaddress_perc_visibility1";
					$values[] = dbquote($row1["posaddress_perc_visibility1"]);

					$fields[] = "posaddress_perc_visibility2";
					$values[] = dbquote($row1["posaddress_perc_visibility2"]);

					$fields[] = "posaddress_export_to_web";
					$values[] = dbquote($row1["posaddress_export_to_web"]);

					$fields[] = "posaddress_email_on_web";
					$values[] = dbquote($row1["posaddress_email_on_web"]);

					$sql = "insert into posaddresses (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					mysql_query($sql) or dberror($sql);

					$new_posaddress_id = mysql_insert_id();
					$pos_id = mysql_insert_id();

					$sql = "delete from posaddressespipeline where posaddress_id = " . $posorder_pos_id;
					mysql_query($sql) or dberror($sql);

					//posorder

					$fields = array();
					$values = array();

					$fields[] = "posorder_posaddress";
					$values[] = dbquote($new_posaddress_id);

					$fields[] = "posorder_order";
					$values[] = dbquote($row["posorder_order"]);

					$fields[] = "posorder_type";
					$values[] = dbquote($row["posorder_type"]);

					$fields[] = "posorder_ordernumber";
					$values[] = dbquote($row["posorder_ordernumber"]);

					$fields[] = "posorder_year";
					$values[] = dbquote($row["posorder_year"]);

					$fields[] = "posorder_product_line";
					$values[] = dbquote($row["posorder_product_line"]);

					$fields[] = "posorder_product_line_subclass";
					$values[] = dbquote($row["posorder_product_line_subclass"]);

					$fields[] = "posorder_postype";
					$values[] = dbquote($row["posorder_postype"]);

					$fields[] = "posorder_subclass";
					$values[] = dbquote($row["posorder_subclass"]);

					$fields[] = "posorder_project_kind";
					$values[] = dbquote($row["posorder_project_kind"]);

					$fields[] = "posorder_legal_type";
					$values[] = dbquote($row["posorder_legal_type"]);

					$fields[] = "posorder_system_currency";
					$values[] = dbquote($row["posorder_system_currency"]);

					$fields[] = "posorder_budget_approved_sc";
					$values[] = dbquote($row["posorder_budget_approved_sc"]);

					$fields[] = "posorder_real_cost_sc";
					$values[] = dbquote($row["posorder_real_cost_sc"]);

					$fields[] = "posorder_client_currency";
					$values[] = dbquote($row["posorder_client_currency"]);

					$fields[] = "posorder_budget_approved_cc";
					$values[] = dbquote($row["posorder_budget_approved_cc"]);

					$fields[] = "posorder_real_cost_cc";
					$values[] = dbquote($row["posorder_real_cost_cc"]);

					$fields[] = "posorder_floor";
					$values[] = dbquote($row["posorder_floor"]);

					$fields[] = "posorder_neighbour_left";
					$values[] = dbquote($row["posorder_neighbour_left"]);

					$fields[] = "posorder_neighbour_right";
					$values[] = dbquote($row["posorder_neighbour_right"]);

					$fields[] = "posorder_neighbour_acrleft";
					$values[] = dbquote($row["posorder_neighbour_acrleft"]);

					$fields[] = "posorder_neighbour_acrright";
					$values[] = dbquote($row["posorder_neighbour_acrright"]);

					$fields[] = "posorder_neighbour_brands";
					$values[] = dbquote($row["posorder_neighbour_brands"]);

					$fields[] = "posorder_neighbour_comment";
					$values[] = dbquote($row["posorder_neighbour_comment"]);

					$fields[] = "posorder_currency_symbol";
					$values[] = dbquote($row["posorder_currency_symbol"]);

					$fields[] = "posorder_exchangerate";
					$values[] = dbquote($row["posorder_exchangerate"]);

					$fields[] = "posorder_remark";
					$values[] = dbquote($row["posorder_remark"]);

					$fields[] = "posorder_project_locally_produced";
					$values[] = dbquote($row["posorder_project_locally_produced"]);

					$fields[] = "date_created";
					$values[] = dbquote(date("Y-m-d H:i:s"));

					$fields[] = "date_modified";
					$values[] = dbquote(date("Y-m-d H:i:s"));

					$fields[] = "user_created";
					$values[] = dbquote(user_login());

					$fields[] = "user_modified";
					$values[] = dbquote(user_login());
					
					$sql = "insert into posorders (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					mysql_query($sql) or dberror($sql);


					$sql = "delete from posorderspipeline where posorder_order = " . $form->value("oid");
					mysql_query($sql) or dberror($sql);

					
					//posareas
					$sql = "select * from posareaspipeline where posarea_posaddress = " . $posorder_pos_id;

					$res = mysql_query($sql) or dberror($sql);
					while ($row = mysql_fetch_assoc($res))
					{
						$fields = array();
						$values = array();

						$fields[] = "posarea_posaddress";
						$values[] = dbquote($new_posaddress_id);

						$fields[] = "posarea_area";
						$values[] = dbquote($row["posarea_area"]);

						$fields[] = "date_created";
						$values[] = dbquote(date("Y-m-d H:i:s"));

						$fields[] = "date_modified";
						$values[] = dbquote(date("Y-m-d H:i:s"));

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posareas (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}

					$sql = "delete from posareaspipeline where posarea_posaddress = " . $posorder_pos_id;
					mysql_query($sql) or dberror($sql);
					
					//posleases
					$sql = "select * from posleasespipeline where poslease_posaddress = " . $posorder_pos_id;

					$res = mysql_query($sql) or dberror($sql);
					while ($row = mysql_fetch_assoc($res))
					{
						$fields = array();
						$values = array();


						$fields[] = "poslease_posaddress";
						$values[] = dbquote($new_posaddress_id);

						$fields[] = "poslease_order";
						$values[] = dbquote($row["poslease_order"]);

						$fields[] = "poslease_lease_type";
						$values[] = dbquote($row["poslease_lease_type"]);
						
						$fields[] = "poslease_anual_rent";
						$values[] = dbquote($row["poslease_anual_rent"]);

						$fields[] = "poslease_salespercent";
						$values[] = dbquote($row["poslease_salespercent"]);

						$fields[] = "poslease_indexclause_in_contract";
						$values[] = dbquote($row["poslease_indexclause_in_contract"]);

						$fields[] = "poslease_isindexed";
						$values[] = dbquote($row["poslease_isindexed"]);

						$fields[] = "poslease_indexrate";
						$values[] = dbquote($row["poslease_indexrate"]);

						$fields[] = "poslease_average_increase";
						$values[] = dbquote($row["poslease_average_increase"]);

						$fields[] = "poslease_realestate_fee";
						$values[] = dbquote($row["poslease_realestate_fee"]);

						$fields[] = "poslease_annual_charges";
						$values[] = dbquote($row["poslease_annual_charges"]);

						$fields[] = "poslease_other_fees";
						$values[] = dbquote($row["poslease_other_fees"]);
						
						$fields[] = "poslease_startdate";
						$values[] = dbquote($row["poslease_startdate"]);

						$fields[] = "poslease_enddate";
						$values[] = dbquote($row["poslease_enddate"]);

						$fields[] = "poslease_extensionoption";
						$values[] = dbquote($row["poslease_extensionoption"]);

						$fields[] = "poslease_exitoption";
						$values[] = dbquote($row["poslease_exitoption"]);

						$fields[] = "poslease_handoverdate";
						$values[] = dbquote($row["poslease_handoverdate"]);

						$fields[] = "poslease_firstrentpayed";
						$values[] = dbquote($row["poslease_firstrentpayed"]);

						$fields[] = "poslease_freeweeks";
						$values[] = dbquote($row["poslease_freeweeks"]);

						$fields[] = "poslease_hasfixrent";
						$values[] = dbquote($row["poslease_hasfixrent"]);

						$fields[] = "poslease_breakpoint_percent";
						$values[] = dbquote($row["poslease_breakpoint_percent"]);

						$fields[] = "poslease_breakpoint_amount";
						$values[] = dbquote($row["poslease_breakpoint_amount"]);

						$fields[] = "poslease_breakpoint_percent2";
						$values[] = dbquote($row["poslease_breakpoint_percent2"]);

						$fields[] = "poslease_breakpoint_amount2";
						$values[] = dbquote($row["poslease_breakpoint_amount2"]);

						$fields[] = "poslease_breakpoint_percent3";
						$values[] = dbquote($row["poslease_breakpoint_percent3"]);

						$fields[] = "poslease_breakpoint_amount3";
						$values[] = dbquote($row["poslease_breakpoint_amount3"]);

						$fields[] = "poslease_negotiator";
						$values[] = dbquote($row["poslease_negotiator"]);

						$fields[] = "poslease_landlord_name";
						$values[] = dbquote($row["poslease_landlord_name"]);

						$fields[] = "poslease_negotiated_conditions";
						$values[] = dbquote($row["poslease_negotiated_conditions"]);

						$fields[] = "poslease_termination_time";
						$values[] = dbquote($row["poslease_termination_time"]);

						$fields[] = "poslease_mailalert2";
						$values[] = dbquote($row["poslease_mailalert2"]);

						$fields[] = "date_created";
						$values[] = dbquote(date("Y-m-d H:i:s"));

						$fields[] = "date_modified";
						$values[] = dbquote(date("Y-m-d H:i:s"));

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posleases (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					
					}

					$sql = "delete from posleasespipeline where poslease_posaddress = " . $posorder_pos_id;
					mysql_query($sql) or dberror($sql);

					$result = update_posdata_from_posorders($new_posaddress_id);
				}
			}
		}
		elseif($action == "update project older than october 2008")
		{
			//create new pos order
			$pos_id = $form->value("posaddress_id");
			
			if($form->value("posaddress_id") > 0)
			{
				
				$fields = array();
				$values = array();

				$fields[] = "posorder_posaddress";
				$values[] = dbquote($form->value("posaddress_id"));

				$fields[] = "posorder_order";
				$values[] = dbquote($form->value("oid"));

				$fields[] = "posorder_type";
				$values[] = 1;

				$fields[] = "posorder_ordernumber";
				$values[] = dbquote($form->value("project_number"));

				$year_of_order = substr($order["order_number"],0,2);
				if($year_of_order > 30 and $year_of_order <= 99)
				{
					$year_of_order = "19" . $year_of_order;
				}
				else
				{
					$year_of_order = substr($year_of_order, 0, 1) . '00' . substr($year_of_order, 1, 1);
				}
				
				if(substr($order["order_number"],3,1) == '.')
				{
					$year_of_order = substr($order["order_number"],0,1) . '0' . substr($order["order_number"],1,2);
				}
				

				$fields[] = "posorder_year";
				$values[] = dbquote($year_of_order);

				$fields[] = "posorder_product_line";
				$values[] = dbquote($project["project_product_line"]);

				$fields[] = "posorder_product_line_subclass";
				$values[] = dbquote($project["project_product_line_subclass"]);

				$fields[] = "posorder_postype";
				$values[] = dbquote($project["project_postype"]);

				$fields[] = "posorder_subclass";
				$values[] = dbquote($project["project_pos_subclass"]);

				$fields[] = "posorder_project_kind";
				$values[] = dbquote($project["project_projectkind"]);

				$fields[] = "posorder_legal_type";
				$values[] = dbquote($project["project_cost_type"]);
				
				if(array_key_exists("project_floor", $form->items))
				{
					$fields[] = "posorder_floor";
					$values[] = dbquote($form->value("project_floor"));
				}
				
				if(array_key_exists("posorder_neighbour_left", $form->items))
				{
					$fields[] = "posorder_neighbour_left";
					$values[] = dbquote($form->value("posorder_neighbour_left"));

					$fields[] = "posorder_neighbour_right";
					$values[] = dbquote($form->value("posorder_neighbour_right"));

					$fields[] = "posorder_neighbour_acrleft";
					$values[] = dbquote($form->value("posorder_neighbour_acrleft"));

					$fields[] = "posorder_neighbour_acrright";
					$values[] = dbquote($form->value("posorder_neighbour_acrright"));

					$fields[] = "posorder_neighbour_brands";
					$values[] = dbquote($form->value("posorder_neighbour_brands"));
				}
				
				$fields[] = "date_created";
				$values[] = dbquote(date("Y-m-d H:i:s"));

				$fields[] = "date_modified";
				$values[] = dbquote(date("Y-m-d H:i:s"));

				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				$fields[] = "user_modified";
				$values[] = dbquote(user_login());
				
				$sql = "insert into posorders (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);

				$result = update_posdata_from_posorders($form->value("posaddress_id"));
				
			}
			else // create new posaddress and new posorder
			{
				//pos address
				$fields = array();
				$values = array();

				$fields[] = "posaddress_client_id";
				$values[] = $project["order_client_address"];

				$fields[] = "posaddress_ownertype";
				$values[] = dbquote($project["project_cost_type"]);

				$fields[] = "posaddress_franchisor_id";
				$values[] = 13;

				$fields[] = "posaddress_franchisee_id";
				$values[] = $project["order_franchisee_address_id"];

				$fields[] = "posaddress_name";
				$values[] = dbquote($form->value("shop_address_company"));

				$fields[] = "posaddress_name2";
				$values[] = dbquote($form->value("shop_address_company2"));

				$fields[] = "posaddress_address";
				$values[] = dbquote($form->value("shop_address_address"));

				$fields[] = "posaddress_address2";
				$values[] = dbquote($form->value("shop_address_address2"));

				$fields[] = "posaddress_zip";
				$values[] = dbquote($form->value("shop_address_zip"));

				$fields[] = "posaddress_place";
				$values[] = dbquote($form->value("shop_address_place"));

				$fields[] = "posaddress_place_id";
				$values[] = dbquote($form->value("posaddress_place_id"));

				$fields[] = "posaddress_country";
				$values[] = dbquote($form->value("shop_address_country"));

				$fields[] = "posaddress_phone";
				$values[] = dbquote($form->value("shop_address_phone"));

				$fields[] = "posaddress_fax";
				$values[] = dbquote($form->value("shop_address_fax"));

				$fields[] = "posaddress_email";
				$values[] = dbquote($form->value("shop_address_email"));

				$fields[] = "posaddress_google_lat";
				$values[] = dbquote($form->value("posaddress_google_lat"));

				$fields[] = "posaddress_google_long";
				$values[] = dbquote($form->value("posaddress_google_long"));

				$fields[] = "posaddress_google_precision";
				$values[] = dbquote($form->value("posaddress_google_precision"));

    			if(array_key_exists("project_fagagreement_type", $form->items))
				{
					$fields[] = "posaddress_fagagreement_type";
					$values[] = dbquote($form->value("project_fagagreement_type"));

					$fields[] = "posaddress_fagrsent";
					$values[] = dbquote($form->value("project_fagrsent"));

					$fields[] = "posaddress_fagrsigned";
					$values[] = dbquote($form->value("project_fagrsigned"));

					$fields[] = "posaddress_fagrstart";
					$values[] = dbquote(from_system_date($form->value("project_fagrstart")));
					
					$fields[] = "posaddress_fagrend";
					$values[] = dbquote(from_system_date($form->value("project_fagrend")));

					$fields[] = "posaddress_fag_comment";
					$values[] = dbquote($form->value("project_fag_comment"));
				}

				$fields[] = "posaddress_store_postype";
				$values[] = dbquote($project["project_postype"]);

				$fields[] = "posaddress_store_subclass";
				$values[] = dbquote($project["project_pos_subclass"]);

				$fields[] = "posaddress_store_furniture";
				$values[] = dbquote($project["project_product_line"]);

				$fields[] = "posaddress_store_furniture_subclass";
				$values[] = dbquote($project["project_product_line_subclass"]);

				$fields[] = "posaddress_store_retailarea";
				$values[] = dbquote($project["project_cost_sqms"]);

				$fields[] = "posaddress_store_openingdate";
				$values[] = dbquote(from_system_date($form->value("shop_actual_opening_date")));


				$fields[] = "date_created";
				$values[] = "current_timestamp";

				$fields[] = "date_modified";
				$values[] = "current_timestamp";

				if (isset($_SESSION["user_login"]))
				{
					$fields[] = "user_created";
					$values[] = dbquote($_SESSION["user_login"]);

					$fields[] = "user_modified";
					$values[] = dbquote($_SESSION["user_login"]);
				}

				$sql = "insert into posaddresses (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);

				$pos_id = mysql_insert_id();


				//pos order
				$fields = array();
				$values = array();


				$fields[] = "posorder_posaddress";
				$values[] = dbquote($pos_id);

				$fields[] = "posorder_order";
				$values[] = dbquote($form->value("oid"));

				$fields[] = "posorder_type";
				$values[] = 1;

				$fields[] = "posorder_ordernumber";
				$values[] = dbquote($form->value("project_number"));

				$year_of_order = substr($order["order_number"],0,2);
				if($year_of_order > 30 and $year_of_order <= 99)
				{
					$year_of_order = "19" . $year_of_order;
				}
				else
				{
					$year_of_order = substr($year_of_order, 0, 1) . '00' . substr($year_of_order, 1, 1);
				}

				if(substr($order["order_number"],3,1) == '.')
				{
					$year_of_order = substr($order["order_number"],0,1) . '0' . substr($order["order_number"],1,2);
				}

				$fields[] = "posorder_year";
				$values[] = dbquote($year_of_order);

				$fields[] = "posorder_product_line";
				$values[] = dbquote($project["project_product_line"]);

				$fields[] = "posorder_product_line";
				$values[] = dbquote($project["project_product_line_subclass"]);

				$fields[] = "posorder_postype";
				$values[] = dbquote($project["project_postype"]);

				$fields[] = "posorder_subclass";
				$values[] = dbquote($project["project_pos_subclass"]);

				$fields[] = "posorder_project_kind";
				$values[] = dbquote($project["project_projectkind"]);

				$fields[] = "posorder_legal_type";
				$values[] = dbquote($project["project_cost_type"]);

				if(array_key_exists("project_floor", $form->items))
				{
					$fields[] = "posorder_floor";
					$values[] = dbquote($form->value("project_floor"));
				}

				
				if(array_key_exists("posorder_neighbour_left", $form->items))
				{
					$fields[] = "posorder_neighbour_left";
					$values[] = dbquote($form->value("posorder_neighbour_left"));

					$fields[] = "posorder_neighbour_right";
					$values[] = dbquote($form->value("posorder_neighbour_right"));

					$fields[] = "posorder_neighbour_acrleft";
					$values[] = dbquote($form->value("posorder_neighbour_acrleft"));

					$fields[] = "posorder_neighbour_acrright";
					$values[] = dbquote($form->value("posorder_neighbour_acrright"));

					$fields[] = "posorder_neighbour_brands";
					$values[] = dbquote($form->value("posorder_neighbour_brands"));
				}
				
				$fields[] = "date_created";
				$values[] = dbquote(date("Y-m-d H:i:s"));

				$fields[] = "date_modified";
				$values[] = dbquote(date("Y-m-d H:i:s"));

				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				$fields[] = "user_modified";
				$values[] = dbquote(user_login());

				$sql = "insert into posorders (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);
				
				$result = google_maps_geo_encode_pos($pos_id);
				$result = update_posdata_from_posorders($pos_id);
			}
		}

		
	}

	
	
	//update posindex with distribution channel and data from CER/AF
	if($form->value("shop_actual_opening_date") and $form->value("posaddress_id"))
	{
		
		$fields = array();
		$value = trim($form->value("posaddress_distribution_channel")) == "" ? "null" : dbquote($form->value("posaddress_distribution_channel"));
		$fields[] = "posaddress_distribution_channel = " . $value;

	
		$sql = "update posaddresses set " . join(", ", $fields) . " where posaddress_id = " . dbquote($pos_id);
		mysql_query($sql) or dberror($sql);


		
		//$pos_id = $form->value("posaddress_id");
		$fields = array();
		//get head counts
		$headcounts = 0;
		$fulltime = 0;
		$percentage = 0;
		$sql_cer = "select * from " . 
				   "cer_salaries " . 
				   "where cer_salary_cer_version = 0 and cer_salary_project = " . dbquote($project["project_id"]);
		
		$res_cer = mysql_query($sql_cer) or dberror($sql_cer);
		while ($row_cer = mysql_fetch_assoc($res_cer))
		{
			$headcounts++;
			$percentage = $percentage + $row_cer["cer_salary_headcount_percent"];
		}
		$fulltime = $percentage / 100;
		
		$sql_cer = "select posaddress_store_headcounts " . 
				   "from posaddresses " . 
				   "where posaddress_id = " . $pos_id;
		$res_cer = mysql_query($sql_cer) or dberror($sql_cer);
		if ($row_cer = mysql_fetch_assoc($res_cer))
		{
			//if($row_cer["posaddress_store_headcounts"] == 0 and $headcounts > 0)
			if($headcounts > 0)
			{
				$fields[] = "posaddress_store_headcounts = " . dbquote($headcounts);
				$fields[] = "posaddress_store_fulltimeeqs = " . dbquote($fulltime);

				$fields[] = "user_modified = " . dbquote(user_login());
				$fields[] = "date_modified = current_timestamp";

				$sql_u = "update posaddresses set " . join(", ", $fields) . " where posaddress_id = " . $pos_id;
				mysql_query($sql_u) or dberror($sql_u);
			}
		}
		
		//get leases rental details
		$fields = array();
		$sql = "select * from posleases " .
			   "where poslease_posaddress = " . $pos_id .
			   " and poslease_order = " . $form->value("oid") . 
			   " order by poslease_startdate DESC ";

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			if($row["poslease_anual_rent"] == NULL or $row["poslease_anual_rent"])
			{
				$poslease_id = $row["poslease_id"];
				$duration_in_years = $row["poslease_enddate"] - $row["poslease_startdate"];
				$tmp1 = 13 - substr($row["poslease_startdate"], 5,2) + substr($row["poslease_enddate"], 5,2);
				$tmp2 = ($duration_in_years - 1)*12;
				$duration_in_years = round(($tmp1 + $tmp2) / 12, 1);

				$total_lease_commitment = 0;
				$sql_cer = "select * from cer_expenses " .
							  "where cer_expense_cer_version = 0 and cer_expense_project = " . dbquote($project["project_id"]) .
							  " and (cer_expense_type = 2 or cer_expense_type = 3)";

				$res_cer = mysql_query($sql_cer) or dberror($sql_cer);
				while ($row_cer = mysql_fetch_assoc($res_cer))
				{
					$total_lease_commitment = $total_lease_commitment + 1*$row_cer["cer_expense_amount"];
				}

				$total_sales = 0;
				$sql =  "select * from cer_revenues " .
						"where cer_revenue_cer_version = 0 and cer_revenue_project = " . dbquote($project["project_id"]) ;

				$res = mysql_query($sql) or dberror($sql);
				while ($row = mysql_fetch_assoc($res))
				{
					$sales_watches_values = $row["cer_revenue_watches"];
					$sales_jewellery_values = $row["cer_revenue_jewellery"];
					$sales_customer_service_values = $row["cer_revenue_customer_service"];
					$total_sales = $total_sales + 1*$sales_watches_values + 1*$sales_jewellery_values + 1*$sales_customer_service_values;

				}

				$average_rent = "";
				if($duration_in_years > 0)
				{
					$average_rent = $total_lease_commitment / $duration_in_years;
				}
				$salespercent = "";
				if($total_sales > 0)
				{ 
					$salespercent = 100 * $average_rent / $total_sales;
				}
				$curreny = 0;
				$sql = "select cer_basicdata_currency from cer_basicdata where cer_basicdata_version = 0 and cer_basicdata_project = " . dbquote($project["project_id"]);
				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					$curreny = $row["cer_basicdata_currency"];
				}

				$fields[] = "poslease_anual_rent = " . dbquote($average_rent);
				$fields[] = "poslease_currency = " . dbquote($curreny);
				$fields[] = "poslease_salespercent = " . dbquote($salespercent);

				$fields[] = "user_modified = " . dbquote(user_login());
				$fields[] = "date_modified = current_timestamp";

				$sql_u = "update posleases set " . join(", ", $fields) . " where poslease_id = " . $poslease_id;
				//mysql_query($sql_u) or dberror($sql_u);
			}
		}
	}

	//update Store Locator
	if($form->value("shop_actual_opening_date") and $pos_id)
	{
		//$pos_id = $form->value("posaddress_id");
		
		//show on store locator if not Franchisee SIS
		
		/*
		if($project["project_postype"] == 2 and $project["project_cost_type"] == 2)
		{
			$sql_u = "update posaddresses " . 
					 "set posaddress_export_to_web = 0 " . 
					 "where posaddress_export_to_web <> 1 " . 
					 "and posaddress_id = " . dbquote($pos_id);
		}
		else
		{
			$sql_u = "update posaddresses " . 
					 "set posaddress_export_to_web = 1 " . 
					 "where posaddress_id = " . dbquote($pos_id);
		}
		mysql_query($sql_u) or dberror($sql_u);
        */

		update_store_locator($pos_id);
	}



	//calculate agreement date
    if($form->value("shop_real_opening_date") != NULL or $form->value("shop_real_opening_date") != '0000-00-00')
    {
		$realistic_opening_date = $form->value("shop_real_opening_date");

		//get the posaddress
		$sql = "select posaddress_id, posaddress_fagrstart " . 
			   "from posorderspipeline " . 
			   "left join posaddressespipeline on posaddress_id = posorder_posaddress " .
			   " where posorder_order = " . dbquote($form->value("oid"));

	
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$pos_id = $row["posaddress_id"];

			if($row["posaddress_fagrstart"] == NULL or $row["posaddress_fagrstart"] == '0000-00-00')
			{
			
				$start = to_system_date($realistic_opening_date);
				$start = "01." . substr($start, 3, strlen($start)-1);


				$months = 13 - substr($realistic_opening_date, 5,2);
				$duration = "3 years and " . $months . " months";


				$end = to_system_date($realistic_opening_date);
				if(substr($end, 6, strlen($end)-1) < 10) {
					$end = "31.12.0" . (substr($end, 6, strlen($end)-1) + 3);
				}
				else
				{
					$end = "31.12." . (substr($end, 6, strlen($end)-1) + 3);
				}

				
				$sql = "update posaddressespipeline set " . 
					   "posaddress_fagrstart = " . dbquote(from_system_date($start)) . ', ' .
					   "posaddress_fagrend = " . dbquote(from_system_date($end)) . ', ' .
					   "posaddress_fargrduration = " . dbquote($duration) . 
					   " where posaddress_id = " . $pos_id;


				$result = mysql_query($sql) or dberror($sql);

				
				$sql = "update projects set " . 
					   "project_fagrstart = " . dbquote(from_system_date($start)) . ', ' .
					   "project_fagrend = " . dbquote(from_system_date($end)) .  
					   " where project_id = " . $form->value("pid");

				$result = mysql_query($sql) or dberror($sql);
				
			}
		}
    }


	return true;
}

/********************************************************************
    update pos data from posorders
*********************************************************************/
function update_posdata_from_posorders($pos_id)
{
	if(!$pos_id){return false;}
	//get the latest project
	$result = update_posorders_from_projects($pos_id);

	$sql =	"select posorder_id, posorder_order, project_id, project_product_line, project_product_line_subclass, project_cost_type, project_postype, project_pos_subclass , " .
		    "project_actual_opening_date, project_shop_closingdate, project_cost_sqms, project_is_local_production, project_is_special_production, " .
		    "project_cost_gross_sqms, project_cost_totalsqms, project_cost_backofficesqms, project_cost_numfloors, " . 
		    "project_cost_floorsurface1, project_cost_floorsurface2, project_cost_floorsurface3, " . 
		    "posorder_legal_type, posorder_product_line, posorder_product_line_subclass, posorder_postype, posorder_subclass, posorder_closing_date " . 
			"from posorders " .
			"left join orders on order_id = posorder_order " . 
			"left join projects on project_order = order_id " .
			"left join postypes on postype_id = project_postype ".
			"left join projectkinds on projectkind_id = project_projectkind ".
			"left join project_costs on project_cost_order = order_id " . 
			"where (order_actual_order_state_code <> '900' or order_actual_order_state_code is null) " . 
		    "and posorder_type = 1 and posorder_posaddress = " . $pos_id .
		    " order by posorder_year DESC, posorder_opening_date DESC";

	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		
		
		if($row["posorder_id"] > 0)
		{
			//get the oldest project
			$update_record = 0;

			$sql_o =	"select posorder_id, posorder_opening_date, project_actual_opening_date, " .
				        "posorder_legal_type, posorder_product_line, posorder_product_line_subclass, posorder_postype, posorder_subclass, posorder_closing_date, " .
				        "posorder_neighbour_left, posorder_neighbour_right, posorder_neighbour_acrleft, posorder_neighbour_acrright, " . 
				        "posorder_neighbour_brands " . 
						"from posorders " .
						"left join orders on order_id = posorder_order " . 
						"left join projects on project_order = order_id " .
						"where (order_actual_order_state_code <> '900' or order_actual_order_state_code is null) " .
				        " and posorder_opening_date is not NULL and posorder_opening_date <> '0000-00-00' " . 
						"and posorder_type = 1 and posorder_posaddress = " . $pos_id .
						" order by posorder_year ASC, project_actual_opening_date ASC";

			$res_o = mysql_query($sql_o) or dberror($sql_o);
			if ($row_o = mysql_fetch_assoc($res_o))
			{
				
				if($row_o["posorder_id"] > 0)
				{
					$fields = array();

					if($row_o["project_actual_opening_date"] != NULL and $row_o["project_actual_opening_date"] != '0000-00-00')
					{
						$fields[] = "posaddress_store_openingdate = " . dbquote($row_o["project_actual_opening_date"]);
						$update_record = 1;
						
					}
					else
					{
						$fields[] = "posaddress_store_openingdate = " . dbquote($row_o["posorder_opening_date"]);
						$update_record = 1;
					}
					

					$sql_u = "update posaddresses set " . join(", ", $fields) . " where posaddress_id = " . $pos_id;
					mysql_query($sql_u) or dberror($sql_u);
				}
			}
			
			if($update_record == 1) // update record with the latest project opened
			{
				$fields = array();
				
				$sql_o =	"select posorder_id, posorder_opening_date, project_actual_opening_date, " .
							"posorder_legal_type, posorder_product_line, posorder_product_line_subclass, posorder_postype, posorder_subclass, posorder_closing_date, " .
							"posorder_neighbour_left, posorder_neighbour_right, posorder_neighbour_acrleft, posorder_neighbour_acrright, " . 
							"posorder_neighbour_brands, posorder_closing_date " . 
							"from posorders " .
							"left join orders on order_id = posorder_order " . 
							"left join projects on project_order = order_id " .
							"where (order_actual_order_state_code <> '900' or order_actual_order_state_code is null) " .
							" and project_actual_opening_date is not NULL and project_actual_opening_date <> '0000-00-00' " . 
							"and posorder_type = 1 and posorder_posaddress = " . $pos_id .
							" order by posorder_year DESC, posorder_opening_date DESC";
				
				$res_o = mysql_query($sql_o) or dberror($sql_o);
				if ($row_o = mysql_fetch_assoc($res_o))
				{
					if($row["project_actual_opening_date"] != NULL and $row["project_actual_opening_date"] != '0000-00-00')
					{
						//get head counts
						$headcounts = 0;
						$fulltime = 0;
						$percentage = 0;
						$sql_cer = "select * from " . 
							       "cer_salaries " . 
							       "where cer_salary_cer_version = 0 and cer_salary_project = " . dbquote($row["project_id"]);
						
						$res_cer = mysql_query($sql_cer) or dberror($sql_cer);
						while ($row_cer = mysql_fetch_assoc($res_cer))
						{
							$headcounts++;
							$percentage = $percentage + $row_cer["cer_salary_headcount_percent"];
						}
						$fulltime = $percentage / 100;
						

						$fields[] = "posaddress_ownertype = " . dbquote($row["project_cost_type"]);
						$fields[] = "posaddress_store_furniture = " . dbquote($row["project_product_line"]);
						$fields[] = "posaddress_store_furniture_subclass = " . dbquote($row["project_product_line_subclass"]);
						$fields[] = "posaddress_store_postype = " . dbquote($row["project_postype"]);
						$fields[] = "posaddress_store_subclass = " . dbquote($row["project_pos_subclass"]);
						$fields[] = "posaddress_store_closingdate = " . dbquote($row["project_shop_closingdate"]);
						$fields[] = "posaddress_store_retailarea = " . dbquote($row["project_cost_sqms"]);
						$fields[] = "posaddress_store_grosssurface = " . dbquote($row["project_cost_gross_sqms"]);
						$fields[] = "posaddress_store_totalsurface = " . dbquote($row["project_cost_totalsqms"]);
						$fields[] = "posaddress_store_backoffice = " . dbquote($row["project_cost_backofficesqms"]);
						$fields[] = "posaddress_store_numfloors = " . dbquote($row["project_cost_numfloors"]);
						$fields[] = "posaddress_store_floorsurface1 = " . dbquote($row["project_cost_floorsurface1"]);
						$fields[] = "posaddress_store_floorsurface2 = " . dbquote($row["project_cost_floorsurface2"]);
						$fields[] = "posaddress_store_floorsurface3 = " . dbquote($row["project_cost_floorsurface3"]);

						$fields[] = "posaddress_neighbour_left = " . dbquote($row_o["posorder_neighbour_left"]);
						$fields[] = "posaddress_neighbour_right = " . dbquote($row_o["posorder_neighbour_right"]);
						$fields[] = "posaddress_neighbour_acrleft = " . dbquote($row_o["posorder_neighbour_acrleft"]);
						$fields[] = "posaddress_neighbour_acrright = " . dbquote($row_o["posorder_neighbour_acrright"]);
						$fields[] = "posaddress_neighbour_brands = " . dbquote($row_o["posorder_neighbour_brands"]);

						$fields[] = "posaddress_local_production = " . dbquote($row["project_is_local_production"]);
						$fields[] = "posaddress_special_project = " . dbquote($row["project_is_special_production"]);

						
						$sql_cer = "select posaddress_store_headcounts " . 
							       "from posaddresses " . 
							       "where posaddress_id = " . $pos_id;
						$res_cer = mysql_query($sql_cer) or dberror($sql_cer);
						if ($row_cer = mysql_fetch_assoc($res_cer))
						{
							if($row_cer["posaddress_store_headcounts"] == 0 and $headcounts > 0)
							{
								$fields[] = "posaddress_store_headcounts = " . dbquote($headcounts);
								$fields[] = "posaddress_store_fulltimeeqs = " . dbquote($fulltime);
							}
						}						

					}
					else // no projects in table projects only projects in table posorders
					{
						//get oldest project
						$sql_o =	"select posorder_id, posorder_opening_date, posorder_opening_date, posorder_project_locally_produced, " .
								"posorder_legal_type, posorder_product_line, posorder_product_line_subclass, posorder_postype, posorder_subclass, posorder_closing_date, " .
								"posorder_neighbour_left, posorder_neighbour_right, posorder_neighbour_acrleft, posorder_neighbour_acrright, " . 
								"posorder_neighbour_brands, posorder_closing_date, posorder_project_special_project " . 
								"from posorders " .
								"where posorder_opening_date is not NULL and posorder_opening_date <> '0000-00-00' " . 
								"and posorder_type = 1 and posorder_posaddress = " . $pos_id .
								" order by posorder_year ASC, posorder_opening_date ASC";
					
						$res_o = mysql_query($sql_o) or dberror($sql_o);
						if ($row_o = mysql_fetch_assoc($res_o))
						{
							$fields[] = "posaddress_store_openingdate = " . dbquote($row_o["posorder_opening_date"]);
						}

						$sql_o ="select posorder_id, posorder_opening_date, posorder_opening_date, posorder_project_locally_produced, " .
								"posorder_legal_type, posorder_product_line, posorder_product_line_subclass, posorder_postype, posorder_subclass, posorder_closing_date, " .
								"posorder_neighbour_left, posorder_neighbour_right, posorder_neighbour_acrleft, posorder_neighbour_acrright, " . 
								"posorder_neighbour_brands, posorder_closing_date, posorder_project_special_project " .
								"from posorders " .
								"where posorder_opening_date is not NULL and posorder_opening_date <> '0000-00-00' " . 
								"and posorder_type = 1 and posorder_posaddress = " . $pos_id .
								" order by posorder_year DESC, posorder_opening_date DESC";
					
						$res_o = mysql_query($sql_o) or dberror($sql_o);
						if ($row_o = mysql_fetch_assoc($res_o))
						{
							$fields[] = "posaddress_ownertype = " . dbquote($row_o["posorder_legal_type"]);
							$fields[] = "posaddress_store_furniture = " . dbquote($row_o["posorder_product_line"]);
							$fields[] = "posaddress_store_furniture_subclass = " . dbquote($row_o["posorder_product_line_subclass"]);
							$fields[] = "posaddress_store_postype = " . dbquote($row_o["posorder_postype"]);
							$fields[] = "posaddress_store_subclass = " . dbquote($row_o["posorder_subclass"]);
							$fields[] = "posaddress_store_closingdate = " . dbquote($row_o["posorder_closing_date"]);

							$fields[] = "posaddress_neighbour_left = " . dbquote($row_o["posorder_neighbour_left"]);
							$fields[] = "posaddress_neighbour_right = " . dbquote($row_o["posorder_neighbour_right"]);
							$fields[] = "posaddress_neighbour_acrleft = " . dbquote($row_o["posorder_neighbour_acrleft"]);
							$fields[] = "posaddress_neighbour_acrright = " . dbquote($row_o["posorder_neighbour_acrright"]);
							$fields[] = "posaddress_neighbour_brands = " . dbquote($row_o["posorder_neighbour_brands"]);

							$fields[] = "posaddress_local_production = " . dbquote($row_o["posorder_project_locally_produced"]);
							$fields[] = "posaddress_special_project = " . dbquote($row_o["posorder_project_special_project"]);
					
							
						}
					}
					
				}
				else // no projects in table projects only projects in table posorders
				{
					$sql_o =	"select posorder_id, posorder_opening_date, posorder_opening_date, posorder_project_locally_produced, " .
								"posorder_legal_type, posorder_product_line, posorder_product_line_subclass, posorder_postype, posorder_subclass, posorder_closing_date, " .
								"posorder_neighbour_left, posorder_neighbour_right, posorder_neighbour_acrleft, posorder_neighbour_acrright, " . 
								"posorder_neighbour_brands, posorder_closing_date, posorder_project_special_project " .
								"from posorders " .
								"where posorder_opening_date is not NULL and posorder_opening_date <> '0000-00-00' " . 
								"and posorder_type = 1 and posorder_posaddress = " . $pos_id .
								" order by posorder_year DESC, posorder_opening_date DESC";
					
					
					$res_o = mysql_query($sql_o) or dberror($sql_o);
					if ($row_o = mysql_fetch_assoc($res_o))
					{
						$fields[] = "posaddress_ownertype = " . dbquote($row_o["posorder_legal_type"]);
						$fields[] = "posaddress_store_furniture = " . dbquote($row_o["posorder_product_line"]);
						$fields[] = "posaddress_store_furniture_subclass = " . dbquote($row_o["posorder_product_line_subclass"]);
						$fields[] = "posaddress_store_postype = " . dbquote($row_o["posorder_postype"]);
						$fields[] = "posaddress_store_subclass = " . dbquote($row_o["posorder_subclass"]);
						$fields[] = "posaddress_store_closingdate = " . dbquote($row_o["posorder_closing_date"]);

						$fields[] = "posaddress_neighbour_left = " . dbquote($row_o["posorder_neighbour_left"]);
						$fields[] = "posaddress_neighbour_right = " . dbquote($row_o["posorder_neighbour_right"]);
						$fields[] = "posaddress_neighbour_acrleft = " . dbquote($row_o["posorder_neighbour_acrleft"]);
						$fields[] = "posaddress_neighbour_acrright = " . dbquote($row_o["posorder_neighbour_acrright"]);
						$fields[] = "posaddress_neighbour_brands = " . dbquote($row_o["posorder_neighbour_brands"]);

						$fields[] = "posaddress_local_production = " . dbquote($row_o["posorder_project_locally_produced"]);
						$fields[] = "posaddress_special_project = " . dbquote($row_o["posorder_project_special_project"]);
					
					}
				}

				
				$fields[] = "user_modified = " . dbquote(user_login());
				$fields[] = "date_modified = current_timestamp";

				$sql_u = "update posaddresses set " . join(", ", $fields) . " where posaddress_id = " . $pos_id;
				mysql_query($sql_u) or dberror($sql_u);
				
				
				//get leases rental details
				$fields = array();
				$project_id = $row["project_id"];

				$sql = "select * from posleases " .
					   "where poslease_posaddress = " . $pos_id .
					   " order by poslease_startdate DESC ";

				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					if($row["poslease_anual_rent"] == NULL or $row["poslease_anual_rent"])
					{
						$poslease_id = $row["poslease_id"];
						$duration_in_years = $row["poslease_enddate"] - $row["poslease_startdate"];
						$tmp1 = 13 - substr($row["poslease_startdate"], 5,2) + substr($row["poslease_enddate"], 5,2);
						$tmp2 = ($duration_in_years - 1)*12;
						$duration_in_years = round(($tmp1 + $tmp2) / 12, 1);

						$total_lease_commitment = 0;
						$sql_cer = "select * from cer_expenses " .
									  "where cer_expense_cer_version = 0 and cer_expense_project = " . dbquote($project_id) .
									  " and (cer_expense_type = 2 or cer_expense_type = 3)";

						$res_cer = mysql_query($sql_cer) or dberror($sql_cer);
						while ($row_cer = mysql_fetch_assoc($res_cer))
						{
							$total_lease_commitment = $total_lease_commitment + 1*$row_cer["cer_expense_amount"];
						}

						$total_sales = 0;
						$sql =  "select * from cer_revenues " .
								"where cer_revenue_cer_version = 0 and cer_revenue_project = " . dbquote($project_id) ;

						$res = mysql_query($sql) or dberror($sql);
						while ($row = mysql_fetch_assoc($res))
						{
							$sales_watches_values = $row["cer_revenue_watches"];
							$sales_jewellery_values = $row["cer_revenue_jewellery"];
							$sales_customer_service_values = $row["cer_revenue_customer_service"];
							$total_sales = $total_sales + 1*$sales_watches_values + 1*$sales_jewellery_values + 1*$sales_customer_service_values;

						}

						$average_rent = "";
						if($duration_in_years > 0)
						{
							$average_rent = $total_lease_commitment / $duration_in_years;
						}
						$salespercent = "";
						if($total_sales > 0)
						{ 
							$salespercent = 100 * $average_rent / $total_sales;
						}
						$curreny = 0;
						$sql = "select cer_basicdata_currency from cer_basicdata where cer_basicdata_version = 0 and cer_basicdata_project = " . dbquote($project_id);
						$res = mysql_query($sql) or dberror($sql);
						if ($row = mysql_fetch_assoc($res))
						{
							$curreny = $row["cer_basicdata_currency"];
						}

						$fields[] = "poslease_anual_rent = " . dbquote($average_rent);
						$fields[] = "poslease_currency = " . dbquote($curreny);
						$fields[] = "poslease_salespercent = " . dbquote($salespercent);

						$fields[] = "user_modified = " . dbquote(user_login());
						$fields[] = "date_modified = current_timestamp";

						$sql_u = "update posleases set " . join(", ", $fields) . " where poslease_id = " . $poslease_id;
						//mysql_query($sql_u) or dberror($sql_u);
					}
				}
			
			}
			return true;
		}
		else
		{
			return false;
		}

	}
	return false;

}

/********************************************************************
    update records in posorders from orders
*********************************************************************/
function update_posorders_from_orders($pos_id)
{
	$sql_p = "select distinct posorder_id, order_archive_date, " .
			 "left(orders.date_created, 10), " .
			 "    order_id, order_number, order_actual_order_state_code,  ".
			 "clientcurrency.currency_symbol as cc, systemcurrency.currency_symbol as sc " . 
			 "from posorders " .
			 "left join orders on order_id = posorder_order " . 
			 "left join currencies as clientcurrency on clientcurrency.currency_id = order_client_currency " .
			 "left join currencies as systemcurrency on systemcurrency.currency_id = order_system_currency " .
			 "where order_type = 2 and posorder_posaddress = " . $pos_id;

	$res_p = mysql_query($sql_p) or dberror($sql_p);

	while ($row_p = mysql_fetch_assoc($res_p))
	{

		$sql_u = "Update posorders SET " .
				 "posorder_type = 2, " . 
				 "posorder_ordernumber = " . dbquote($row_p["order_number"]) . ", " .
			     "posorder_system_currency = " . dbquote($row_p["sc"]) . ", " .
				 "posorder_client_currency = " . dbquote($row_p["cc"]) . " " .
				 "where posorder_order = " . $row_p["order_id"];
				 
		$result = mysql_query($sql_u) or dberror($sql_u);

	}

	return true;
}


/********************************************************************
    update records in posorders from projects
*********************************************************************/
function update_posorders_from_projects($pos_id)
{
	$sql_p = "select distinct posorder_ordernumber, project_id, project_number, project_pos_subclass, posorder_id, order_archive_date, " .
		     "project_furniture_height , project_furniture_height_mm, " . 
			 "project_postype, " .
			 "left(projects.date_created, 10), project_product_line, project_product_line_subclass, postype_id, projectkind_id, project_costtype_id, project_floor, ".
			 "product_line_name, project_actual_opening_date, project_shop_closingdate, project_is_local_production, " . 
			 "    order_id, order_number, order_actual_order_state_code, projectkind_name, project_costtype_text, ".
			 "clientcurrency.currency_symbol as cc, systemcurrency.currency_symbol as sc " . 
			 "from posorders " .
			 "left join orders on order_id = posorder_order " . 
			 "left join projects on project_order = order_id " .
			 "left join product_lines on project_product_line = product_line_id ".
			 "left join postypes on postype_id = project_postype ".
			 "left join projectkinds on projectkind_id = project_projectkind ".
			 "left join project_costs on project_cost_order = order_id " . 
			 "left join project_costtypes on project_costtype_id = project_cost_type " .
			 "left join currencies as clientcurrency on clientcurrency.currency_id = order_client_currency " .
			 "left join currencies as systemcurrency on systemcurrency.currency_id = order_system_currency " .
			 "where order_type = 1 and posorder_posaddress = " . $pos_id;

	$res_p = mysql_query($sql_p) or dberror($sql_p);

	$infolinks = array();
	while ($row_p = mysql_fetch_assoc($res_p))
	{

		if($row_p["project_id"] > 0)
		{
			if($row_p["order_archive_date"] == NULL or $row_p["order_archive_date"] == "0000-00-00")
			{
				$link = "<a href=\"/user/project_view_client_data.php?pid=" .  $row_p["project_id"] . "\" target=\"_blank\"><img src=\"/pictures/info.gif\" border=0/></a>";
			}
			else
			{
				$link = "<a href=\"/archive/project_view_client_data.php?pid=" .  $row_p["project_id"] . "\" target=\"_blank\"><img src=\"/pictures/info.gif\" border=0/></a>";
			}
				
			$infolinks[$row_p["posorder_id"]] = $link;
		}


		

		$sql_u = "Update posorders SET " .
				 "posorder_type = 1, " . 
				 "posorder_ordernumber = " . dbquote($row_p["order_number"]) . ", " .
				 "posorder_product_line = " . dbquote($row_p["project_product_line"]) . ", " . 
			     "posorder_product_line_subclass = " . dbquote($row_p["project_product_line_subclass"]) . ", " . 
				 "posorder_postype = " . dbquote($row_p["postype_id"]) . ", " . 
			     "posorder_subclass = " . dbquote($row_p["project_pos_subclass"]) . ", " . 
				 "posorder_project_kind = " . dbquote($row_p["projectkind_id"]) . ", " . 
				 "posorder_legal_type = " . dbquote($row_p["project_costtype_id"]) . ", " .
				 "posorder_opening_date = " . dbquote($row_p["project_actual_opening_date"]) . ", " .
				 "posorder_closing_date = " . dbquote($row_p["project_shop_closingdate"]) . ", " .
				 "posorder_system_currency = " . dbquote($row_p["sc"]) . ", " .
			     "posorder_furniture_height = " . dbquote($row_p["project_furniture_height"]) . ", " .
			     "posorder_furniture_height_mm = " . dbquote($row_p["project_furniture_height_mm"]) . ", " .
			     "posorder_project_locally_produced = " . dbquote($row_p["project_is_local_production"]) . ", " .
			     "posorder_floor = " . dbquote($row_p["project_floor"]) . ", " .
				 "posorder_client_currency = " . dbquote($row_p["cc"]) . " " .
				 "where posorder_order = " . $row_p["order_id"];
				 
				 $result = mysql_query($sql_u) or dberror($sql_u);
	}

	$result = update_posorders_year($pos_id);

	return $infolinks;
}

/********************************************************************
    update posorders_year
*********************************************************************/
function update_posorders_year($pos_id)
{

	$sql_p = "select posorder_id, posorder_ordernumber " .
			 "from posorders " .
			 "where posorder_type = 1 and posorder_posaddress = " . $pos_id;


	$res_p = mysql_query($sql_p) or dberror($sql_p);

	$infolinks = array();
	while ($row_p = mysql_fetch_assoc($res_p))
	{
		
		if($row_p["posorder_ordernumber"])
		{
			$year_of_order = substr($row_p["posorder_ordernumber"],0,2);
			if(strpos($row_p["posorder_ordernumber"], '.') == 3)
			{
				$year_of_order = substr($row_p["posorder_ordernumber"],0,1) . "0" . substr($row_p["posorder_ordernumber"],1,2);
			}
			else
			{
				if($year_of_order > 30 and $year_of_order <= 99)
				{
					$year_of_order = "19" . $year_of_order;
				}
				else
				{
					$year_of_order = substr($year_of_order, 0, 1) . '00' . substr($year_of_order, 1, 1);
				}
			}

			if(substr($row_p["posorder_ordernumber"],3,1) == '.')
			{
				$year_of_order = substr($row_p["posorder_ordernumber"],0,1) . '0' . substr($row_p["posorder_ordernumber"],1,2);
			}
			
		}
		else
		{
			$year_of_order = "0000";
		}
		

		$sql_u = "Update posorders SET " .
				 "posorder_year = " . dbquote($year_of_order) . 
				 " where posorder_id = " . $row_p["posorder_id"];
					 
		$result = mysql_query($sql_u) or dberror($sql_u);
	}

	return true;

}

/********************************************************************
    build missing intangible investment records
*********************************************************************/
function build_missing_intangible_investment_records($pos_id)
{
	$sql = "select * from posinvestment_types where posinvestment_type_active = 1 and posinvestment_type_intangible = 1";

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$sql_i = "select posinvestment_id from posinvestments " .
				 "where posinvestment_posaddress = " .  $pos_id . 
				 " and posinvestment_investment_type = " . $row["posinvestment_type_id"];

		$res_i = mysql_query($sql_i) or dberror($sql_i);
		if (!$row_i = mysql_fetch_assoc($res_i))
		{
			$sql = "insert into posinvestments (" .
				   "posinvestment_posaddress, " .
				   "posinvestment_investment_type, " .
				   "user_created, date_created) values (".
				   $pos_id . ", " .
				   $row["posinvestment_type_id"] . ", " . 
				   dbquote(user_login()) . ", " . 
				   "current_timestamp)";

			$result = mysql_query($sql) or dberror($sql);
		}

	}

	return true;
}

/********************************************************************************************
    update missing intangible investment records from local currency
********************************************************************************************/
function update_intangible_investment_records_from_local_currency($pos_id, $overwrite = true)
{
	$sql = "select posaddress_oldexchangerate " . 
		   "from posaddresses " . 
		   "where posaddress_id = " . $pos_id;

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{

		$exchange_rate =  $row["posaddress_oldexchangerate"];
		if($exchange_rate > 0)
		{
			$sql = "select * " .
				   "from posinvestments " .
				   "left join posinvestment_types on posinvestment_type_id = posinvestment_investment_type " .
				   " where posinvestment_type_intangible = 1 " . 
				   " and posinvestment_posaddress = " . $pos_id;

			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{
				$cer = round($row["posinvestment_amount_cer_loc"] * $exchange_rate,2);
				$cms = round($row["posinvestment_amount_cms_loc"] * $exchange_rate,2);

				$fields = array();

				$value = $cer;
				$fields[] = "posinvestment_amount_cer = " . dbquote($value);

				$value = $cms;
				$fields[] = "posinvestment_amount_cms = " . dbquote($value);

				$fields[] = "user_modified = " . dbquote(user_login());
				$fields[] = "date_modified = current_timestamp";

				$sql = "update posinvestments set " . join(", ", $fields) . 
					   " where posinvestment_id = " . $row["posinvestment_id"];
				mysql_query($sql) or dberror($sql);
			}
		}
	}
}


/*********************************************************************************************
    update missing intangible investment records from system currency
*********************************************************************************************/
function update_intangible_investment_records_from_system_currency($pos_id, $overwrite = true)
{
	$sql = "select posaddress_oldexchangerate " . 
		   "from posaddresses " . 
		   "where posaddress_id = " . $pos_id;

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{

		$exchange_rate =  $row["posaddress_oldexchangerate"];
		if($exchange_rate > 0)
		{
			$sql = "select * " .
				   "from posinvestments " .
				   "left join posinvestment_types on posinvestment_type_id = posinvestment_investment_type " .
				   " where posinvestment_type_intangible = 1 " . 
				   " and posinvestment_posaddress = " . $pos_id;

			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{
				if($exchange_rate)
				{
					$cer = round($row["posinvestment_amount_cer"] / $exchange_rate,2);
					$cms = round($row["posinvestment_amount_cms"] / $exchange_rate,2);
				}
				else
				{
					$cer = "0.00";
					$cms = "0.00";
				}

				$fields = array();

				$value = $cer;
				$fields[] = "posinvestment_amount_cer_loc = " . dbquote($value);

				$value = $cms;
				$fields[] = "posinvestment_amount_cms_loc = " . dbquote($value);

				$fields[] = "user_modified = " . dbquote(user_login());
				$fields[] = "date_modified = current_timestamp";

				$sql = "update posinvestments set " . join(", ", $fields) . 
					   " where posinvestment_id = " . $row["posinvestment_id"];
				mysql_query($sql) or dberror($sql);
			}
		}
	}
}


/********************************************************************
    build missing investment records
*********************************************************************/
function build_missing_investment_records($id)
{

	//get exchange_rate
	$exchange_rate = 0;
	$order_id = 0;
	$updated = 0;
	
	$sql = "select posorder_order, posorder_exchangerate, order_client_exchange_rate " . 
		   "from posorders " .
		   "left join orders on order_id = posorder_order " . 
		   "where posorder_id = " . $id;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$exchange_rate = $row["order_client_exchange_rate"];
		$order_id = $row["posorder_order"];
	}

	//check if there is a project
	$sql = "select count(order_id) as num_recs from orders where order_id = " . dbquote($order_id);
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	if ($row["num_recs"] > 0)
	{
		$sql = "select * from posinvestment_types where posinvestment_type_intangible = 0 and posinvestment_type_active = 1";

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$sql_i = "select count(posorderinvestment_id) as num_recs " .
					 "from posorderinvestments " . 
					 "where posorderinvestment_investment_type = " . $row["posinvestment_type_id"] .
					 " and posorderinvestment_posorder = " . $id;

			$res_i = mysql_query($sql_i) or dberror($sql_i);
			$row_i = mysql_fetch_assoc($res_i);

			if ($row_i["num_recs"] == 0)
			{
				$sql = "insert into posorderinvestments (" .
					   "posorderinvestment_posorder, " .
					   "posorderinvestment_investment_type, " .
					   "user_created, date_created) values (".
					   $id . ", " .
					   $row["posinvestment_type_id"] . ", " . 
					   dbquote(user_login()) . ", " . 
					   "current_timestamp)";

				$result = mysql_query($sql) or dberror($sql);
				
			}
		}

		//get CER-Values from porject cost
		$CERS = array();
		$sql_c =  "select * from project_costs " .
				"where project_cost_order = " . dbquote($order_id);

		$res_c = mysql_query($sql_c) or dberror($sql_c);
		$row_c = mysql_fetch_assoc($res_c);

		$CERS[1] = $row_c["project_cost_construction"];
		$CERS[3] = $row_c["project_cost_fixturing"];
		$CERS[5] = $row_c["project_cost_architectural"];
		$CERS[7] = $row_c["project_cost_equipment"];
		$CERS[9] = $row_c["project_cost_other"];


		foreach($CERS as $key=>$value)
		{
			$fields = array();
		
			$fields[] = "posorderinvestment_amount_cer = " .  dbquote($CERS[$key]);

			$fields[] = "posorderinvestment_amount_cer_loc = " .  dbquote($CERS[$key]*$exchange_rate);

			$sql = "update posorderinvestments set " . join(", ", $fields) . " where posorderinvestment_investment_type = " . $key . " and posorderinvestment_posorder = " . $id;
			mysql_query($sql) or dberror($sql);
		}
		
		//update investment types by figures of the cost monitoring sheet

		$sql = "select * from posinvestment_types  " .
			   "where posinvestment_type_id in (1, 3, 5, 7, 9, 11)";

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			//costgroups
			$group_total = 0;
			$cost_groups = explode(",", $row["posinvestment_type_cms_sumcostgroups"]);

			foreach($cost_groups as $key=>$cost_group)
			{
				$sql_i = "select sum(order_item_system_price*order_item_quantity) as total_in_system_currency " . 
						 "from order_items ".
						 "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
						 "   and order_item_order = " . $order_id . 
						 "   and order_item_cost_group =  " . dbquote($cost_group);

				$res_i = mysql_query($sql_i) or dberror($sql_i);
				if ($row_i = mysql_fetch_assoc($res_i))
				{
					$group_total = $group_total + $row_i["total_in_system_currency"];
				}
			}
				
			$fields = array();
		
			$fields[] = "posorderinvestment_amount_cms = " .  dbquote($group_total);

			$fields[] = "posorderinvestment_amount_cms_loc = " .  dbquote($group_total*$exchange_rate);

			$value1 = "current_timestamp";
			$project_fields[] = "date_modified = " . dbquote($value1);

			if (isset($_SESSION["user_login"]))
			{
				$value1 = dbquote($_SESSION["user_login"]);
				$project_fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update posorderinvestments set " . join(", ", $fields) . " where posorderinvestment_investment_type = " . $row["posinvestment_type_id"] . " and posorderinvestment_posorder = " . $id;
			mysql_query($sql) or dberror($sql);

		}
	}
	else // no project in retail net
	{
		$sql = "select * from posinvestment_types where posinvestment_type_intangible = 0 and posinvestment_type_active = 1";

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$sql_i = "select count(posorderinvestment_id) as num_recs " .
					 "from posorderinvestments " . 
					 "where posorderinvestment_investment_type = " . $row["posinvestment_type_id"] .
					 " and posorderinvestment_posorder = " . $id;

			$res_i = mysql_query($sql_i) or dberror($sql_i);
			$row_i = mysql_fetch_assoc($res_i);

			if ($row_i["num_recs"] == 0)
			{
				$sql = "insert into posorderinvestments (" .
					   "posorderinvestment_posorder, " .
					   "posorderinvestment_investment_type, " .
					   "user_created, date_created) values (".
					   $id . ", " .
					   $row["posinvestment_type_id"] . ", " . 
					   dbquote(user_login()) . ", " . 
					   "current_timestamp)";

				$result = mysql_query($sql) or dberror($sql);
				
			}
		}
	}

	return true;
}

/***********************************************************************************
    update missing investment records from local currency
************************************************************************************/
function update_investment_records_from_local_currency($pos_order, $overwrite = true)
{
	$sql = "select posorder_exchangerate " . 
		   "from posorders " . 
		   "where posorder_id = " . $pos_order;

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{

		$exchange_rate =  $row["posorder_exchangerate"];
		if($exchange_rate > 0)
		{
			$sql = "select * " .
				   "from posorderinvestments " .
				   "left join posinvestment_types on posinvestment_type_id = posorderinvestment_investment_type " .
				   " where posinvestment_type_intangible <> 1 " . 
				   " and posorderinvestment_posorder = " . $pos_order;

			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{
				$cer = round($row["posorderinvestment_amount_cer_loc"] * $exchange_rate,2);
				$cms = round($row["posorderinvestment_amount_cms_loc"] * $exchange_rate,2);
	
				$fields = array();

				$value = $cer;
				$fields[] = "posorderinvestment_amount_cer = " . dbquote($value);

				$value = $cms;
				$fields[] = "posorderinvestment_amount_cms = " . dbquote($value);

				$fields[] = "user_modified = " . dbquote(user_login());
				$fields[] = "date_modified = current_timestamp";

				$sql = "update posorderinvestments set " . join(", ", $fields) . 
					   " where posorderinvestment_id = " . $row["posorderinvestment_id"];
				mysql_query($sql) or dberror($sql);
			}
		}
	}
		
}

/*************************************************************************************
    update missing investment records from system currency
*************************************************************************************/
function update_investment_records_from_system_currency($pos_order, $overwrite = true)
{
	$sql = "select posorder_exchangerate " . 
		   "from posorders " . 
		   "where posorder_id = " . $pos_order;

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$exchange_rate =  $row["posorder_exchangerate"];

		if($exchange_rate > 0)
		{
			$sql = "select * " .
				   "from posorderinvestments " .
				   "left join posinvestment_types on posinvestment_type_id = posorderinvestment_investment_type " .
				   " where posinvestment_type_intangible <> 1 " . 
				   " and posorderinvestment_posorder = " . $pos_order;

			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{
				if($exchange_rate)
				{
					$cer = round($row["posorderinvestment_amount_cer"] / $exchange_rate,2);
					$cms = round($row["posorderinvestment_amount_cms"] / $exchange_rate,2);
				}
				else
				{
					$cer = "0.00";
					$cms = "0.00";
				}

				
				$fields = array();

				$value = $cer;
				$fields[] = "posorderinvestment_amount_cer_loc = " . dbquote($value);

				$value = $cms;
				$fields[] = "posorderinvestment_amount_cms_loc = " . dbquote($value);

				$fields[] = "user_modified = " . dbquote(user_login());
				$fields[] = "date_modified = current_timestamp";

				$sql = "update posorderinvestments set " . join(", ", $fields) . 
					   " where posorderinvestment_id = " . $row["posorderinvestment_id"];
				mysql_query($sql) or dberror($sql);
			}
		}
	}
}

/********************************************************************
   move POS to pipeline on deleting the actual opening date
*********************************************************************/
function  move_pos_data_to_pipeline($form)
{
	
	if($form->value("posaddress_id") > 0)
	{
		$pos_id = $form->value("posaddress_id");
		$num_recs = 0;
		//check if the project is the only project
		$sql = "select count(posorder_id) as num_recs " . 
			   "from posorders " . 
			   "where posorder_posaddress = " . $pos_id;
		
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$num_recs = $row["num_recs"];
		}

		if($num_recs > 1) // POS has several projects
		{
			//move project to pipeline if project kind = new project
			$sql = "select * from posorders where posorder_order = " . $form->value("oid");
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				if($row["posorder_project_kind"] == 1) // new project
				{
					$pos_id = $row["posorder_posaddress"];

					$fields = array();
					$values = array();

					$fields[] = "posorder_parent_table";
					$values[] = dbquote("posaddresses");

					$fields[] = "posorder_posaddress";
					$values[] = dbquote($pos_id);

					$fields[] = "posorder_order";
					$values[] = dbquote($row["posorder_order"]);

					$fields[] = "posorder_type";
					$values[] = dbquote($row["posorder_type"]);

					$fields[] = "posorder_ordernumber";
					$values[] = dbquote($row["posorder_ordernumber"]);

					$fields[] = "posorder_year";
					$values[] = dbquote($row["posorder_year"]);

					$fields[] = "posorder_product_line";
					$values[] = dbquote($row["posorder_product_line"]);

					$fields[] = "posorder_product_line_subclass";
					$values[] = dbquote($row["posorder_product_line_subclass"]);

					$fields[] = "posorder_postype";
					$values[] = dbquote($row["posorder_postype"]);

					$fields[] = "posorder_subclass";
					$values[] = dbquote($row["posorder_subclass"]);

					$fields[] = "posorder_project_kind";
					$values[] = dbquote($row["posorder_project_kind"]);

					$fields[] = "posorder_legal_type";
					$values[] = dbquote($row["posorder_legal_type"]);

					$fields[] = "posorder_floor";
					$values[] = dbquote($row["posorder_floor"]);

					$fields[] = "posorder_neighbour_left";
					$values[] = dbquote($row["posorder_neighbour_left"]);

					$fields[] = "posorder_neighbour_right";
					$values[] = dbquote($row["posorder_neighbour_right"]);

					$fields[] = "posorder_neighbour_acrleft";
					$values[] = dbquote($row["posorder_neighbour_acrleft"]);

					$fields[] = "posorder_neighbour_acrright";
					$values[] = dbquote($row["posorder_neighbour_acrright"]);

					$fields[] = "posorder_neighbour_brands";
					$values[] = dbquote($row["posorder_neighbour_brands"]);

					$fields[] = "date_created";
					$values[] = dbquote(date("Y-m-d H:i:s"));

					$fields[] = "date_modified";
					$values[] = dbquote(date("Y-m-d H:i:s"));

					$fields[] = "user_created";
					$values[] = dbquote(user_login());

					$fields[] = "user_modified";
					$values[] = dbquote(user_login());
					
					$sql = "insert into posorderspipeline (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					mysql_query($sql) or dberror($sql);

					$sql = "delete from posorders where posorder_order = " . $form->value("oid");
					mysql_query($sql) or dberror($sql);

					$result = update_posdata_from_posorders($pos_id);
				}
			}

		}
		elseif($num_recs == 1) // POS has only one project
		{
			//move all data to pipeline
			$sql = "select * from posorders where posorder_order = " . $form->value("oid");
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				
				if($row["posorder_project_kind"] == 1) // new project
				{
					$posorder_pos_id = $row["posorder_posaddress"];
					$posorder_id = $row["posorder_id"];

					$sql1 = "select * from posaddresses where posaddress_id = " . $posorder_pos_id;
					$res1 = mysql_query($sql1) or dberror($sql1);
					if ($row1 = mysql_fetch_assoc($res1))
					{
						//pos address
						$fields = array();
						$values = array();

						$fields[] = "posaddress_client_id";
						$values[] = dbquote($row1["posaddress_client_id"]);

						$fields[] = "posaddress_ownertype";
						$values[] = dbquote($row1["posaddress_ownertype"]);

						$fields[] = "posaddress_franchisor_id";
						$values[] = dbquote($row1["posaddress_franchisor_id"]);

						$fields[] = "posaddress_franchisee_id";
						$values[] = dbquote($row1["posaddress_franchisee_id"]);

						$fields[] = "posaddress_name";
						$values[] = dbquote($row1["posaddress_name"]);

						$fields[] = "posaddress_name2";
						$values[] = dbquote($row1["posaddress_name2"]);

						$fields[] = "posaddress_address";
						$values[] = dbquote($row1["posaddress_address"]);

						$fields[] = "posaddress_address2";
						$values[] = dbquote($row1["posaddress_address2"]);

						$fields[] = "posaddress_zip";
						$values[] = dbquote($row1["posaddress_zip"]);

						$fields[] = "posaddress_place";
						$values[] = dbquote($row1["posaddress_place"]);

						$fields[] = "posaddress_place_id";
						$values[] = dbquote($row1["posaddress_place_id"]);

						$fields[] = "posaddress_country";
						$values[] = dbquote($row1["posaddress_country"]);
						
						$fields[] = "posaddress_phone";
						$values[] = dbquote($row1["posaddress_phone"]);

						$fields[] = "posaddress_fax";
						$values[] = dbquote($row1["posaddress_fax"]);

						$fields[] = "posaddress_email";
						$values[] = dbquote($row1["posaddress_email"]);

						$fields[] = "posaddress_store_postype";
						$values[] = dbquote($row1["posaddress_store_postype"]);

						$fields[] = "posaddress_store_subclass";
						$values[] = dbquote($row1["posaddress_store_subclass"]);

						$fields[] = "posaddress_store_furniture";
						$values[] = dbquote($row1["posaddress_store_furniture"]);

						$fields[] = "posaddress_store_grosssurface";
						$values[] = dbquote($row1["posaddress_store_grosssurface"]);

						$fields[] = "posaddress_store_totalsurface";
						$values[] = dbquote($row1["posaddress_store_totalsurface"]);

						$fields[] = "posaddress_store_retailarea";
						$values[] = dbquote($row1["posaddress_store_retailarea"]);

						$fields[] = "posaddress_store_backoffice";
						$values[] = dbquote($row1["posaddress_store_backoffice"]);

						$fields[] = "posaddress_store_numfloors";
						$values[] = dbquote($row1["posaddress_store_numfloors"]);

						$fields[] = "posaddress_store_floorsurface1";
						$values[] = dbquote($row1["posaddress_store_floorsurface1"]);

						$fields[] = "posaddress_store_floorsurface2";
						$values[] = dbquote($row1["posaddress_store_floorsurface2"]);

						$fields[] = "posaddress_store_floorsurface3";
						$values[] = dbquote($row1["posaddress_store_floorsurface3"]);


						$fields[] = "posaddress_fagagreement_type";
						$values[] = dbquote($row1["posaddress_fagagreement_type"]);

						$fields[] = "posaddress_fagrsent";
						$values[] = dbquote($row1["posaddress_fagrsent"]);

						$fields[] = "posaddress_fagrsigned";
						$values[] = dbquote($row1["posaddress_fagrsigned"]);

						$fields[] = "posaddress_fagrstart";
						$values[] = dbquote($row1["posaddress_fagrstart"]);

						$fields[] = "posaddress_fagrend";
						$values[] = dbquote($row1["posaddress_fagrend"]);

						$fields[] = "posaddress_fag_comment";
						$values[] = dbquote($row1["posaddress_fag_comment"]);


						$fields[] = "posaddress_google_lat";
						$values[] = dbquote($row1["posaddress_google_lat"]);
						
						$fields[] = "posaddress_google_long";
						$values[] = dbquote($row1["posaddress_google_long"]);

						$fields[] = "posaddress_google_precision";
						$values[] = 1;

						$fields[] = "posaddress_perc_class";
						$values[] = dbquote($row1["posaddress_perc_class"]);

						$fields[] = "posaddress_perc_tourist";
						$values[] = dbquote($row1["posaddress_perc_tourist"]);

						$fields[] = "posaddress_perc_transport";
						$values[] = dbquote($row1["posaddress_perc_transport"]);

						$fields[] = "posaddress_perc_people";
						$values[] = dbquote($row1["posaddress_perc_people"]);

						$fields[] = "posaddress_perc_parking";
						$values[] = dbquote($row1["posaddress_perc_parking"]);

						$fields[] = "posaddress_perc_visibility1";
						$values[] = dbquote($row1["posaddress_perc_visibility1"]);

						$fields[] = "posaddress_perc_visibility2";
						$values[] = dbquote($row1["posaddress_perc_visibility2"]);

						$sql = "insert into posaddressespipeline (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);

						$new_posaddress_id = mysql_insert_id();

						$sql = "delete from posaddresses where posaddress_id = " . $posorder_pos_id;
						mysql_query($sql) or dberror($sql);

						
						//posorder
						$fields = array();
						$values = array();

						$fields[] = "posorder_parent_table";
						$values[] = dbquote("posaddressespipeline");

						$fields[] = "posorder_posaddress";
						$values[] = dbquote($new_posaddress_id);

						$fields[] = "posorder_order";
						$values[] = dbquote($row["posorder_order"]);

						$fields[] = "posorder_type";
						$values[] = dbquote($row["posorder_type"]);

						$fields[] = "posorder_ordernumber";
						$values[] = dbquote($row["posorder_ordernumber"]);

						$fields[] = "posorder_year";
						$values[] = dbquote($row["posorder_year"]);

						$fields[] = "posorder_product_line";
						$values[] = dbquote($row["posorder_product_line"]);

						$fields[] = "posorder_product_line_subclass";
						$values[] = dbquote($row["posorder_product_line_subclass"]);

						$fields[] = "posorder_postype";
						$values[] = dbquote($row["posorder_postype"]);

						$fields[] = "posorder_subclass";
						$values[] = dbquote($row["posorder_subclass"]);

						$fields[] = "posorder_project_kind";
						$values[] = dbquote($row["posorder_project_kind"]);

						$fields[] = "posorder_legal_type";
						$values[] = dbquote($row["posorder_legal_type"]);

						$fields[] = "posorder_system_currency";
						$values[] = dbquote($row["posorder_system_currency"]);

						$fields[] = "posorder_budget_approved_sc";
						$values[] = dbquote($row["posorder_budget_approved_sc"]);

						$fields[] = "posorder_real_cost_sc";
						$values[] = dbquote($row["posorder_real_cost_sc"]);

						$fields[] = "posorder_client_currency";
						$values[] = dbquote($row["posorder_client_currency"]);

						$fields[] = "posorder_budget_approved_cc";
						$values[] = dbquote($row["posorder_budget_approved_cc"]);

						$fields[] = "posorder_real_cost_cc";
						$values[] = dbquote($row["posorder_real_cost_cc"]);

						$fields[] = "posorder_floor";
						$values[] = dbquote($row["posorder_floor"]);

						$fields[] = "posorder_neighbour_left";
						$values[] = dbquote($row["posorder_neighbour_left"]);

						$fields[] = "posorder_neighbour_right";
						$values[] = dbquote($row["posorder_neighbour_right"]);

						$fields[] = "posorder_neighbour_acrleft";
						$values[] = dbquote($row["posorder_neighbour_acrleft"]);

						$fields[] = "posorder_neighbour_acrright";
						$values[] = dbquote($row["posorder_neighbour_acrright"]);

						$fields[] = "posorder_neighbour_brands";
						$values[] = dbquote($row["posorder_neighbour_brands"]);

						$fields[] = "posorder_neighbour_comment";
						$values[] = dbquote($row["posorder_neighbour_comment"]);

						$fields[] = "posorder_currency_symbol";
						$values[] = dbquote($row["posorder_currency_symbol"]);

						$fields[] = "posorder_exchangerate";
						$values[] = dbquote($row["posorder_exchangerate"]);

						$fields[] = "posorder_remark";
						$values[] = dbquote($row["posorder_remark"]);

						$fields[] = "posorder_project_locally_produced";
						$values[] = dbquote($row["posorder_project_locally_produced"]);

						$fields[] = "date_created";
						$values[] = dbquote(date("Y-m-d H:i:s"));

						$fields[] = "date_modified";
						$values[] = dbquote(date("Y-m-d H:i:s"));

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());
						
						$sql = "insert into posorderspipeline (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);


						$sql = "delete from posorders where posorder_order = " . $form->value("oid");
						mysql_query($sql) or dberror($sql);

						
						//posareas
						$sql = "select * from posareas where posarea_posaddress = " . $posorder_pos_id;

						$res = mysql_query($sql) or dberror($sql);
						while ($row = mysql_fetch_assoc($res))
						{
							$fields = array();
							$values = array();

							$fields[] = "posarea_posaddress";
							$values[] = dbquote($new_posaddress_id);

							$fields[] = "posarea_area";
							$values[] = dbquote($row["posarea_area"]);

							$fields[] = "date_created";
							$values[] = dbquote(date("Y-m-d H:i:s"));

							$fields[] = "date_modified";
							$values[] = dbquote(date("Y-m-d H:i:s"));

							$fields[] = "user_created";
							$values[] = dbquote(user_login());

							$fields[] = "user_modified";
							$values[] = dbquote(user_login());

							$sql = "insert into posareaspipeline (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
							mysql_query($sql) or dberror($sql);
						}

						$sql = "delete from posareas where posarea_posaddress = " . $posorder_pos_id;
						mysql_query($sql) or dberror($sql);
						
						//posleases
						$sql = "select * from posleases where poslease_posaddress = " . $posorder_pos_id;

						$res = mysql_query($sql) or dberror($sql);
						while ($row = mysql_fetch_assoc($res))
						{
							$fields = array();
							$values = array();


							$fields[] = "poslease_posaddress";
							$values[] = dbquote($new_posaddress_id);

							$fields[] = "poslease_lease_type";
							$values[] = dbquote($row["poslease_lease_type"]);
							
							$fields[] = "poslease_anual_rent";
							$values[] = dbquote($row["poslease_anual_rent"]);

							$fields[] = "poslease_salespercent";
							$values[] = dbquote($row["poslease_salespercent"]);

							$fields[] = "poslease_indexclause_in_contract";
							$values[] = dbquote($row["poslease_indexclause_in_contract"]);

							$fields[] = "poslease_isindexed";
							$values[] = dbquote($row["poslease_isindexed"]);

							$fields[] = "poslease_indexrate";
							$values[] = dbquote($row["poslease_indexrate"]);

							$fields[] = "poslease_average_increase";
							$values[] = dbquote($row["poslease_average_increase"]);

							$fields[] = "poslease_realestate_fee";
							$values[] = dbquote($row["poslease_realestate_fee"]);

							$fields[] = "poslease_annual_charges";
							$values[] = dbquote($row["poslease_annual_charges"]);

							$fields[] = "poslease_other_fees";
							$values[] = dbquote($row["poslease_other_fees"]);
							
							$fields[] = "poslease_startdate";
							$values[] = dbquote($row["poslease_startdate"]);

							$fields[] = "poslease_enddate";
							$values[] = dbquote($row["poslease_enddate"]);

							$fields[] = "poslease_extensionoption";
							$values[] = dbquote($row["poslease_extensionoption"]);

							$fields[] = "poslease_exitoption";
							$values[] = dbquote($row["poslease_exitoption"]);

							$fields[] = "poslease_negotiator";
							$values[] = dbquote($row["poslease_negotiator"]);

							$fields[] = "poslease_landlord_name";
							$values[] = dbquote($row["poslease_landlord_name"]);

							$fields[] = "poslease_negotiated_conditions";
							$values[] = dbquote($row["poslease_negotiated_conditions"]);

							$fields[] = "poslease_termination_time";
							$values[] = dbquote($row["poslease_termination_time"]);

							$fields[] = "poslease_mailalert2";
							$values[] = dbquote($row["poslease_mailalert2"]);

							$fields[] = "date_created";
							$values[] = dbquote(date("Y-m-d H:i:s"));

							$fields[] = "date_modified";
							$values[] = dbquote(date("Y-m-d H:i:s"));

							$fields[] = "user_created";
							$values[] = dbquote(user_login());

							$fields[] = "user_modified";
							$values[] = dbquote(user_login());

							$sql = "insert into posleasespipeline (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
							mysql_query($sql) or dberror($sql);
						
						}

						$sql = "delete from posleases where poslease_posaddress = " . $posorder_pos_id;
						mysql_query($sql) or dberror($sql);

						//remove investments
						$sql = "delete from posinvestments where posinvestment_posaddress = " . $posorder_pos_id;
						mysql_query($sql) or dberror($sql);

						$sql = "delete from posorderinvestments where posorderinvestment_posorder = " . $posorder_id;
						mysql_query($sql) or dberror($sql);

						//remove pos files
						$sql = "delete from posfiles where posfile_posaddress = " . $posorder_pos_id;
						mysql_query($sql) or dberror($sql);

						//remove pos equipments
						$sql = "delete from posfiles where posfile_posaddress = " . $posorder_pos_id;
						mysql_query($sql) or dberror($sql);

						$result = update_posdata_from_posorders($new_posaddress_id);
					}
				}
			}
		}
	}
	
	return true;
}

/***********************************************************************************
    Update_tables of the sore locator
************************************************************************************/
function update_store_locator($pos_id)
{
	
	$db = mysql_pconnect(RETAILNET_SERVER, RETAILNET_USER, RETAILNET_PASSWORD);
	$dbname = RETAILNET_DB;
	mysql_query( "SET NAMES 'utf8'");
	mysql_select_db($dbname, $db);
    
	
	$posdata = array();
	//get data
	$sql = "select posaddress_place_id, place_province from posaddresses " . 
		   "left join places on place_id = posaddress_place_id " .
		   "where posaddress_id = " . dbquote($pos_id);
		   
	$res = mysql_query($sql) or dberror($sql);


	if ($row = mysql_fetch_assoc($res))
	{
		$posdata = $row;
	}
	
	
	
	//update provinces
	$sql = "select * from provinces " . 
		   "left join countries on country_id = province_country " . 
		   "where province_id = " . dbquote($posdata["place_province"]);
	$res = mysql_query($sql) or dberror($sq);


	if ($row = mysql_fetch_assoc($res))
	{
		$sql2 = "select * from provinces where province_id = " . $row["province_id"];
		$db2 = mysql_connect(STORE_LOCATOR_SERVER, STORE_LOCATOR_USER, STORE_LOCATOR_PASSWORD);
		$dbname2 = STORE_LOCATOR_DB;
		mysql_query( "SET NAMES 'utf8'");
		mysql_select_db($dbname2, $db2);
		$res2 = mysql_query($sql2) or dberror($sql2);

		if ($row2 = mysql_fetch_assoc($res2)) //update
		{
			$sql_u = "Update provinces set " . 
					 "province_country_id = " . dbquote($row["country_store_locator_id"]) . ", " . 
					 "province_canton = " . dbquote($row["province_canton"]) . ", " . 
					 "province_region = " . dbquote($row["province_region"]) . ", " . 
					 "province_adminregion = " . dbquote($row["province_adminregion"]) . ", " . 
					 "province_shortcut = " . dbquote($row["province_shortcut"]) . " " .
					 "where province_id = " . dbquote($row2["province_id"]);

			$result = mysql_query($sql_u) or dberror($sql_u);
		}
		else //insert
		{
			$sql_i = "INSERT INTO provinces (province_id, province_country_id, province_canton, province_region, province_adminregion, province_shortcut ) " . 
					 "VALUES (" . 
					 dbquote($row["province_id"]) . ", " .
					 dbquote($row["country_store_locator_id"]) . ", " .
					 dbquote($row["province_canton"]) . ", " . 
					 dbquote($row["province_region"]) . ", " . 
					 dbquote($row["province_adminregion"]) . ", " . 
					 dbquote($row["province_shortcut"]) . ")";
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}
	}

	//update places
	$db = mysql_pconnect(RETAILNET_SERVER, RETAILNET_USER, RETAILNET_PASSWORD);
	$dbname = RETAILNET_DB;
	mysql_query( "SET NAMES 'utf8'");
	mysql_select_db($dbname, $db);
	$sql = "select * from places " . 
		   "left join countries on country_id = place_country " .
	       "where place_id = " . dbquote($posdata["posaddress_place_id"]);

	$res = mysql_query($sql, $db) or dberror($sql, $db);

	if ($row = mysql_fetch_assoc($res))
	{
		
		$sql2 = "select * from places where place_id = " . $row["place_id"];
		$db2 = mysql_connect(STORE_LOCATOR_SERVER, STORE_LOCATOR_USER, STORE_LOCATOR_PASSWORD);
		$dbname2 = STORE_LOCATOR_DB;
		mysql_query( "SET NAMES 'utf8'");
		mysql_select_db($dbname2, $db2);
		$res2 = mysql_query($sql2) or dberror($sql2);

		if ($row2 = mysql_fetch_assoc($res2)) //update
		{
			$sql_u = "Update places set " . 
					 "place_country_id = " . dbquote($row["country_store_locator_id"]) . ", " . 
					 "place_province_id = " . dbquote($row["place_province"]) . ", " . 
					 "place_name = " . dbquote($row["place_name"]) . " " .
					  " where place_id = " . dbquote($row2["place_id"]);

			$result = mysql_query($sql_u) or dberror($sql_u);
					 
		}
		else //insert
		{
			$sql_i = "INSERT INTO places (place_id, place_country_id, place_province_id, place_name) " . 
					 "VALUES (" . 
					 dbquote($row["place_id"]) . ", " .
					 dbquote($row["country_store_locator_id"]) . ", " .
					 dbquote($row["place_province"]) . ", " . 
					 dbquote($row["place_name"]) . ")";
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}
	}


	

	//update posaddresses
	$db = mysql_pconnect(RETAILNET_SERVER, RETAILNET_USER, RETAILNET_PASSWORD);
	$dbname = RETAILNET_DB;
	mysql_query( "SET NAMES 'utf8'");
	mysql_select_db($dbname, $db);



	//get airport info
	$airportinfo = "";
	$sql = "select * from posareas " . 
			"where posarea_posaddress = " . dbquote($pos_id) . 
			" and (posarea_area = 4 OR posarea_area = 5)";
	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res)) 
	{
		if($row["posarea_area"] == 4) 
		{
			$airportinfo = "Airport Landside";
		}
		elseif($row["posarea_area"] == 5) 
		{
			$airportinfo = "Airport Airside";
		}
	}
	
	$sql = "select * from posaddresses " . 
		   "left join countries on country_id = posaddress_country " . 
		   "where posaddress_id = " . dbquote($pos_id);

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$sql2 = "select * from posaddresses where posaddress_id = " . $row["posaddress_id"];
		$db2 = mysql_connect(STORE_LOCATOR_SERVER, STORE_LOCATOR_USER, STORE_LOCATOR_PASSWORD);
		$dbname2 = STORE_LOCATOR_DB;
		mysql_query( "SET NAMES 'utf8'");
		mysql_select_db($dbname2, $db2);
		$res2 = mysql_query($sql2) or dberror($sql2);

		if ($row2 = mysql_fetch_assoc($res2)) //update
		{
			
			if($row["posaddress_export_to_web"] == 0)
			{
				$sql_u = "delete from loc_posaddresses where loc_posaddress_posaddress_id = " . dbquote($pos_id);
				$result = mysql_query($sql_u) or dberror($sql_u);
				
				$sql_u = "delete from posaddresses where posaddress_id = " . dbquote($pos_id);
				$result = mysql_query($sql_u) or dberror($sql_u);

				$sql_u = "delete from posclosinghrs where posclosinghr_posaddress_id = " . dbquote($pos_id);
				$result = mysql_query($sql_u) or dberror($sql_u);

				$sql_u = "delete from posopeninghrs where posopeninghr_posaddress_id = " . dbquote($pos_id);
				$result = mysql_query($sql_u) or dberror($sql_u);


			}
			elseif($row["posaddress_store_closingdate"] != NULL and $row["posaddress_store_closingdate"] != '0000-00-00')
			{
				$sql_u = "delete from loc_posaddresses where loc_posaddress_posaddress_id = " . dbquote($pos_id);
				$result = mysql_query($sql_u) or dberror($sql_u);
				
				$sql_u = "delete from posaddresses where posaddress_id = " . dbquote($pos_id);
				$result = mysql_query($sql_u) or dberror($sql_u);
			}
			elseif($row["posaddress_store_openingdate"] == NULL or $row["posaddress_store_openingdate"] == '0000-00-00')
			{
				$sql_u = "delete from loc_posaddresses where loc_posaddress_posaddress_id = " . dbquote($pos_id);
				$result = mysql_query($sql_u) or dberror($sql_u);
				
				$sql_u = "delete from posaddresses where posaddress_id = " . dbquote($pos_id);
				$result = mysql_query($sql_u) or dberror($sql_u);
			}
			else
			{
			
				if($row['posaddress_email_on_web'] == 1) {
					$sql_u = "Update posaddresses set " . 
							 "posaddress_name = " . dbquote($row["posaddress_name"]) . ", " . 
							 "posaddress_name2 = " . dbquote($row["posaddress_name2"]) . ", " . 
							 "posaddress_address = " . dbquote($row["posaddress_address"]) . ", " . 
							 "posaddress_address2 = " . dbquote($row["posaddress_address2"]) . ", " . 
							 "posaddress_zip = " . dbquote($row["posaddress_zip"]) . ", " .
							 "posaddress_place_id = " . dbquote($row["posaddress_place_id"]) . ", " .
							 "posaddress_country_id = " . dbquote($row["country_store_locator_id"]) . ", " . 
							 "posaddress_phone = " . dbquote($row["posaddress_phone"]) . ", " . 
							 "posaddress_fax = " . dbquote($row["posaddress_fax"]) . ", " . 
							 "posaddress_email = " . dbquote($row["posaddress_email"]) . ", " .
							 "posaddress_website = " . dbquote($row["posaddress_website"]) . ", " .
							 "posaddress_store_postype = " . dbquote($row["posaddress_store_postype"]) . ", " .
							 "posaddress_google_lat = " . dbquote($row["posaddress_google_lat"]) . ", " .
							 "posaddress_google_long = " . dbquote($row["posaddress_google_long"]) . ", " .
						     "posaddress_airportinfo = " . dbquote($airportinfo) . " " .
							 "where posaddress_id = " . dbquote($row2["posaddress_id"]);
				}
				else {
					
					$sql_u = "Update posaddresses set " . 
							 "posaddress_name = " . dbquote($row["posaddress_name"]) . ", " . 
							 "posaddress_name2 = " . dbquote($row["posaddress_name2"]) . ", " . 
							 "posaddress_address = " . dbquote($row["posaddress_address"]) . ", " . 
							 "posaddress_address2 = " . dbquote($row["posaddress_address2"]) . ", " . 
							 "posaddress_zip = " . dbquote($row["posaddress_zip"]) . ", " .
							 "posaddress_place_id = " . dbquote($row["posaddress_place_id"]) . ", " .
							 "posaddress_country_id = " . dbquote($row["country_store_locator_id"]) . ", " . 
							 "posaddress_phone = " . dbquote($row["posaddress_phone"]) . ", " . 
							 "posaddress_fax = " . dbquote($row["posaddress_fax"]) . ", " . 
							 "posaddress_email = '', " .
							 "posaddress_website = " . dbquote($row["posaddress_website"]) . ", " .
							 "posaddress_store_postype = " . dbquote($row["posaddress_store_postype"]) . ", " .
							 "posaddress_google_lat = " . dbquote($row["posaddress_google_lat"]) . ", " .
							 "posaddress_google_long = " . dbquote($row["posaddress_google_long"]) . ", " .
						     "posaddress_airportinfo = " . dbquote($airportinfo) . " " .
							 "where posaddress_id = " . dbquote($row2["posaddress_id"]);
				
				}
				
				$result = mysql_query($sql_u) or dberror($sql_u);
			}
		}
		elseif($row["posaddress_export_to_web"] == 1 and (($row["posaddress_store_closingdate"] == null) or ($row["posaddress_store_closingdate"] == '0000-00-00'))) // insert
		{
			
			if($row['posaddress_email_on_web'] == 1) {
			
				$sql_i = "INSERT INTO posaddresses (posaddress_id, posaddress_name, posaddress_name2, posaddress_address, posaddress_address2, posaddress_zip, " . 
								 "posaddress_place_id, posaddress_country_id, posaddress_phone, posaddress_fax, posaddress_email, " . 
								 "posaddress_website, posaddress_store_postype, posaddress_google_lat , posaddress_google_long, posaddress_airportinfo) " . 
						 "VALUES (" . 
						 dbquote($row["posaddress_id"]) . ", " .
						 dbquote($row["posaddress_name"]) . ", " .
						 dbquote($row["posaddress_name2"]) . ", " . 
						 dbquote($row["posaddress_address"]) . ", " . 
						 dbquote($row["posaddress_address2"]) . ", " .
						 dbquote($row["posaddress_zip"]) . ", " .
						 dbquote($row["posaddress_place_id"]) . ", " .
						 dbquote($row["country_store_locator_id"]) . ", " . 
						 dbquote($row["posaddress_phone"]) . ", " . 
						 dbquote($row["posaddress_fax"]) . ", " . 
						 dbquote($row["posaddress_email"]) . ", " .
						 dbquote($row["posaddress_website"]) . ", " .
						 dbquote($row["posaddress_store_postype"]) . ", " . 
						 dbquote($row["posaddress_google_lat"]) . ", " .
					     dbquote($row["posaddress_google_long"]) . ", " .
						 dbquote($airportinfo) . ")";
			}
			else
			{
				$sql_i = "INSERT INTO posaddresses (posaddress_id, posaddress_name, posaddress_name2, posaddress_address, posaddress_address2, posaddress_zip, " . 
								 "posaddress_place_id, posaddress_country_id, posaddress_phone, posaddress_fax, posaddress_email, " . 
								 "posaddress_website, posaddress_store_postype, posaddress_google_lat , posaddress_google_long, posaddress_airportinfo) " . 
						 "VALUES (" . 
						 dbquote($row["posaddress_id"]) . ", " .
						 dbquote($row["posaddress_name"]) . ", " .
						 dbquote($row["posaddress_name2"]) . ", " . 
						 dbquote($row["posaddress_address"]) . ", " . 
						 dbquote($row["posaddress_address2"]) . ", " .
						 dbquote($row["posaddress_zip"]) . ", " .
						 dbquote($row["posaddress_place_id"]) . ", " .
						 dbquote($row["country_store_locator_id"]) . ", " . 
						 dbquote($row["posaddress_phone"]) . ", " . 
						 dbquote($row["posaddress_fax"]) . ", " . 
						 "'', " .
						 dbquote($row["posaddress_website"]) . ", " .
						 dbquote($row["posaddress_store_postype"]) . ", " . 
						 dbquote($row["posaddress_google_lat"]) . ", " .
					     dbquote($row["posaddress_google_long"]) . ", " .
						 dbquote($airportinfo) . ")";
			}
			
			$result = mysql_query($sql_i) or dberror($sql_i);
		}
	}


	//update posclosings
	//delete
	$sql2 = "delete from posclosings where posclosing_posaddress = " . $pos_id;
	$db2 = mysql_connect(STORE_LOCATOR_SERVER, STORE_LOCATOR_USER, STORE_LOCATOR_PASSWORD);
	$dbname2 = STORE_LOCATOR_DB;
	mysql_query( "SET NAMES 'utf8'");
	mysql_select_db($dbname2, $db2);
	mysql_query($sql2) or dberror($sql2);


	//insert
	$db = mysql_pconnect(RETAILNET_SERVER, RETAILNET_USER, RETAILNET_PASSWORD);
	$dbname = RETAILNET_DB;
	mysql_query( "SET NAMES 'utf8'");
	mysql_select_db($dbname, $db);

	$sql = "select * from posclosings where posclosing_posaddress = " . $pos_id;;
	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{
	
		$sql_i = 'INSERT INTO posclosings (posclosing_posaddress, posclosing_project, posclosing_startdate, posclosing_enddate ) VALUES (' . 
			     $pos_id . ', ' .
			     $row["posclosing_project"] . ', ' .
			     dbquote($row["posclosing_startdate"]) . ', ' .
			     dbquote($row["posclosing_enddate"]) . ')';
		
		$db2 = mysql_connect(STORE_LOCATOR_SERVER, STORE_LOCATOR_USER, STORE_LOCATOR_PASSWORD);
		$dbname2 = STORE_LOCATOR_DB;
		mysql_query( "SET NAMES 'utf8'");
		mysql_select_db($dbname2, $db2);
		mysql_query($sql_i) or dberror($sql_i);

	}


	$db = mysql_pconnect(RETAILNET_SERVER, RETAILNET_USER, RETAILNET_PASSWORD);
	$dbname = RETAILNET_DB;
	mysql_query( "SET NAMES 'utf8'");
	mysql_select_db($dbname, $db);

}



/***********************************************************************************
    Update_tables of the sore locator
************************************************************************************/
function get_standard_distribution_channel($legal_type, $pos_type, $pos_subclass)
{
			
	if($legal_type and $pos_type and $pos_subclass)
	{
		$sql = "select posdistributionchannel_dchannel_id " . 
			   "from posdistributionchannels " . 
			   " where posdistributionchannel_legaltype_id = " . $legal_type . 
			   " and posdistributionchannel_postype_id = " . $pos_type .
			   " and posdistributionchannel_possubclass_id = " . $pos_subclass;
	
	
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			return $row["posdistributionchannel_dchannel_id"];
		
		}
	}
	elseif($legal_type and $pos_type)
	{
		$sql = "select posdistributionchannel_dchannel_id " . 
			   "from posdistributionchannels " . 
			   " where posdistributionchannel_legaltype_id = " . $legal_type . 
			   " and posdistributionchannel_postype_id = " . $pos_type . 
			   " and (posdistributionchannel_possubclass_id IS NULL " . 
			   " or posdistributionchannel_possubclass_id = 0)";
	
	
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			return $row["posdistributionchannel_dchannel_id"];
		
		}
	}

	return "";
}


?>