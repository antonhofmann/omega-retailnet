<?php
//compose array with texts for selected filter criteria
$_filter_strings = array();

function get_filter_string($filter_type, $id_string)
{
	$_filter_string = "";
	$_filter_array = array();
	if($filter_type == "clt")
	{
		$sql = "select client_type_code from client_types " . 
			   " where client_type_id IN (" . $id_string . ")" .
			   " order by client_type_code";


		$res = mysql_query($sql) or dberror($sql);
        while ($row = mysql_fetch_assoc($res))
        {
			$_filter_array[] = $row["client_type_code"];
		}
	}
	elseif($filter_type == "owners")
	{
		$sql = "select address_company from addresses " . 
			   " where address_id IN (" . $id_string . ")" .
			   " order by address_company";


		$res = mysql_query($sql) or dberror($sql);
        while ($row = mysql_fetch_assoc($res))
        {
			$_filter_array[] = $row["address_company"];
		}
	}
	elseif($filter_type == "pl")
	{
		$sql = "select product_line_name from product_lines " . 
			   " where product_line_id IN (" . $id_string . ")" .
			   " order by product_line_name";


		$res = mysql_query($sql) or dberror($sql);
        while ($row = mysql_fetch_assoc($res))
        {
			$_filter_array[] = $row["product_line_name"];
		}
	}
	elseif($filter_type == "pls")
	{
		$sql = "select productline_subclass_name from productline_subclasses " . 
			   " where productline_subclass_id IN (" . $id_string . ")" .
			   " order by productline_subclass_name";


		$res = mysql_query($sql) or dberror($sql);
        while ($row = mysql_fetch_assoc($res))
        {
			$_filter_array[] = $row["productline_subclass_name"];
		}
	}
	elseif($filter_type == "pk")
	{
		$sql = "select projectkind_name from projectkinds " . 
			   " where projectkind_id IN (" . $id_string . ")" .
			   " order by projectkind_name";


		$res = mysql_query($sql) or dberror($sql);
        while ($row = mysql_fetch_assoc($res))
        {
			$_filter_array[] = $row["projectkind_name"];
		}
	}
	elseif($filter_type == "pt")
	{
		$sql = "select postype_name from postypes " . 
			   " where postype_id IN (" . $id_string . ")" .
			   " order by postype_name";


		$res = mysql_query($sql) or dberror($sql);
        while ($row = mysql_fetch_assoc($res))
        {
			$_filter_array[] = $row["postype_name"];
		}
	}
	elseif($filter_type == "suc")
	{
		$sql = "select possubclass_name from possubclasses " . 
			   " where possubclass_id IN (" . $id_string . ")" .
			   " order by possubclass_name";


		$res = mysql_query($sql) or dberror($sql);
        while ($row = mysql_fetch_assoc($res))
        {
			$_filter_array[] = $row["possubclass_name"];
		}
	}
	elseif($filter_type == "lpt")
	{
		$sql = "select project_costtype_text from project_costtypes " . 
			   " where project_costtype_id IN (" . $id_string . ")" .
			   " order by project_costtype_text";


		$res = mysql_query($sql) or dberror($sql);
        while ($row = mysql_fetch_assoc($res))
        {
			$_filter_array[] = $row["project_costtype_text"];
		}
	}
	elseif($filter_type == "gr")
	{
		$sql = "select salesregion_name from salesregions " . 
			   " where salesregion_id IN (" . $id_string . ")" .
			   " order by salesregion_name";


		$res = mysql_query($sql) or dberror($sql);
        while ($row = mysql_fetch_assoc($res))
        {
			$_filter_array[] = $row["salesregion_name"];
		}
	}
	elseif($filter_type == "re")
	{
		$sql = "select region_name from regions " . 
			   " where region_id IN (" . $id_string . ")" .
			   " order by region_name";


		$res = mysql_query($sql) or dberror($sql);
        while ($row = mysql_fetch_assoc($res))
        {
			$_filter_array[] = $row["region_name"];
		}
	}
	elseif($filter_type == "cnt")
	{
		$sql = "select country_name from countries " . 
			   " where country_id IN (" . $id_string . ")" .
			   " order by country_name";


		$res = mysql_query($sql) or dberror($sql);
        while ($row = mysql_fetch_assoc($res))
        {
			$_filter_array[] = $row["country_name"];
		}
	}
	elseif($filter_type == "sc" or $filter_type == "lrtc" or $filter_type == "rto")
	{
		$sql = "select concat(user_name, ' ', user_firstname) as username from users " . 
			   " where user_id IN (" . $id_string . ")" .
			   " order by user_name, user_firstname";


		$res = mysql_query($sql) or dberror($sql);
        while ($row = mysql_fetch_assoc($res))
        {
			$_filter_array[] = $row["username"];
		}
	}
	elseif($filter_type == "areas")
	{
		$sql = "select posareatype_name from posareatypes " . 
			   " where posareatype_id IN (" . $id_string . ")" .
			   " order by posareatype_name";


		$res = mysql_query($sql) or dberror($sql);
        while ($row = mysql_fetch_assoc($res))
        {
			$_filter_array[] = $row["posareatype_name"];
		}
	}
	elseif($filter_type == "dos")
	{
		$sql = "select design_objective_item_id, design_objective_item_name, " .
						 "    design_objective_group_name, postype_name " .
						 "from design_objective_items left join design_objective_groups on design_objective_item_group = design_objective_group_id " .
						 "    left join postypes on design_objective_group_postype = postype_id " . 
						 " where design_objective_item_id IN (" . $id_string . ")" .
						 " order by postype_name, design_objective_group_name, design_objective_item_name ";

		$res = mysql_query($sql) or dberror($sql);
        while ($row = mysql_fetch_assoc($res))
        {
			$_filter_array[] = $row["postype_name"] . " - " . $row["design_objective_group_name"] . " - " . $row["design_objective_item_name"];
		}
	}
	

	return implode(", ",  $_filter_array);
	

}

