<?php
/********************************************************************

    projects_query_6_xls.php

    Enter Filter Criteria for the Query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2011-07-22
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2011-07-22
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_perform_queries");

require_once "../include/phpexcel/PHPExcel.php";
require_once "include/query_get_functions.php";
require_once("projects_query_filter_strings.php");


/********************************************************************
    prepare all data needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("projects_queries.php");
}

//get costmonitoring furniture groups
$costmonitoring_groups = array();
$sql = "select costmonitoringgroup_id from costmonitoringgroups " . 
       "where costmonitoringgroup_include_in_masterplan = 1 ";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$costmonitoring_groups[] = $row["costmonitoringgroup_id"];
}

$costmonitoring_group_filter = "";


if(count($costmonitoring_groups)  > 0) {

	$costmonitoring_group_filter = " or order_items_costmonitoring_group IN (" . implode(",", $costmonitoring_groups) .") ";
	
}


//check if filter is present
$query_title = "";
$print_query_filter = 0;
$sql = "select mis_query_name, mis_query_filter, mis_print_filter from mis_queries " .
	   "where mis_query_id = " . param("query_id");

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$query_title = $row["mis_query_name"] . ' (' . date("d.m.Y H:i:s"). ')';

	if($row["mis_print_filter"] == 1)
	{
		$print_query_filter = 1;
	}
	

	$filters = array();
	$filters = unserialize($row["mis_query_filter"]);

	foreach($filters as $key=>$value)
	{
		
		$st =  $filters["ptst"]; // Project State


		$fosc = $filters["fst"]; //from order state
		$tosc = $filters["tst"]; // to order state

		$clt = $filters["clt"]; // Client Types
		$pl = $filters["pl"]; // product lines
		$pls = $filters["plsc"]; // product line subclasses
		$pk = $filters["pk"]; // product kinds
		$pt = $filters["pt"]; // project types
		$suc = $filters["ptsc"]; // POS Type Subclass
		$lpt = $filters["pct"]; // project cost type
		$gr = $filters["gr"]; // Geografical Regions
		$re = $filters["re"]; // Supplying Regions
		$cnt = $filters["co"]; // Countries
		$sc = $filters["rtc"]; // Retail Coordinators
		$lsc = $filters["lrtc"]; // Local Retail Coordinators

		$fdy = $filters["fdy"]; // Closing Year From
		$fdm = $filters["fdm"]; // Closing Month From
		$tdy = $filters["tdy"]; // Closing Year To
		$tdm = $filters["tdm"]; // Closing Month To

		$rto = $filters["rto"]; // Retail Operators

		$show_detail = $filters["detail"]; // Show second Sheet (Details for Suppliers and Forwarders
		

		if(array_key_exists("areas", $filters))
		{
			$areas = $filters["areas"]; // Neighbourhood Locations
		
		}
		else
		{
			$areas = "";
		}

		if(array_key_exists("owners", $filters))
		{
			$owners = $filters["owners"]; // Owners
		
		}
		else
		{
			$owners = "";
		}

		if(array_key_exists("dos", $filters))
		{
			$dos = $filters["dos"]; // Design Objectives
		
		}
		else
		{
			$dos = "";
		}

		

		

	}
}
else
{
	redirect("projects_queries.php");
}

$states = array();
$states[1] = "Projects in progress";
$states[2] = "Projects on hold";
$states[4] = "Operating POS Locations";
$states[6] = "Projects cancelled";



$filter = "";

$owners = substr($owners,0, strlen($owners)-1); // remove last comma
$owners = str_replace("-", ",", $owners);
if($owners) // owners
{
    $filter =  " where (order_franchisee_address_id IN (" . $owners . "))";
	$_filter_strings["Owners"] = get_filter_string("owners", $owners);
}


$clt = substr($clt,0, strlen($clt)-1); // remove last comma
$clt = str_replace("-", ",", $clt);
if($clt and $filter) // client type
{
    $filter .=  " and (address_client_type IN (" . $clt . "))";
	$_filter_strings["Client Types"] = get_filter_string("clt", $clt);
}
elseif($clt)
{
	$filter =  " where (address_client_type IN (" . $clt . "))";
	$_filter_strings["Client Types"] = get_filter_string("clt", $clt);
}

$pl = substr($pl,0, strlen($pl)-1); // remove last comma
$pl = str_replace("-", ",", $pl);
if($pl and $filter) // product line
{
	$filter .=  " and (project_product_line IN (" . $pl . "))";
	$_filter_strings["Product Lines"] = get_filter_string("pl", $pl);
}
elseif($pl) // product line
{
    $filter =  " where (project_product_line IN (" . $pl . "))";
	$_filter_strings["Product Lines"] = get_filter_string("pl", $pl);
}

$pls = substr($pls,0, strlen($pls)-1); // remove last comma
$pls = str_replace("-", ",", $pls);
if($pls and $filter) // product line subclasses
{
	$filter .=  " and (project_product_line_subclass IN (" . $pls . "))";
	$_filter_strings["Product Line Subclasses"] = get_filter_string("pls", $pls);
}
elseif($pls) // product line
{
    $filter =  " where (project_product_line_subclass IN (" . $pls . "))";
	$_filter_strings["Product Line Subclasses"] = get_filter_string("pls", $pls);
}

$pk = substr($pk,0, strlen($pk)-1); // remove last comma
$pk = str_replace("-", ",", $pk);

if($pk and $filter) // product line
{
	$filter .=  " and (project_projectkind IN (" . $pk . "))";
	$_filter_strings["Project Kinds"] = get_filter_string("pk", $pk);
}
elseif($pk) // product kind
{
    $filter =  " where (project_projectkind IN (" . $pk . "))";
	$_filter_strings["Project Kinds"] = get_filter_string("pk", $pk);
}


$pt = substr($pt,0, strlen($pt)-1); // remove last comma
$pt = str_replace("-", ",", $pt);
if($pt and $filter) // project type
{
    $filter .=  " and (project_postype IN (" . $pt . "))";
	$_filter_strings["POS Types"] = get_filter_string("pt", $pt);
}
elseif($pt)
{
    $filter =  " where (project_postype IN (" . $pt . "))";
	$_filter_strings["POS Types"] = get_filter_string("pt", $pt);
}

$suc = substr($suc,0, strlen($suc)-1); // remove last comma
$suc = str_replace("-", ",", $suc);
if($suc and $filter) // Pos Type Sub classe
{
    $filter .=  " and (project_pos_subclass IN (" . $suc . "))";
	$_filter_strings["POS Type Subclasses"] = get_filter_string("suc", $suc);
}
elseif($suc)
{
    $filter =  " where (project_pos_subclass IN (" . $suc . "))";
	$_filter_strings["POS Type Subclasses"] = get_filter_string("suc", $suc);
}

$lpt = substr($lpt,0, strlen($lpt)-1); // remove last comma
$lpt = str_replace("-", ",", $lpt);
if($lpt and $filter) // project cost type
{
    $filter .=  " and (project_cost_type IN (" . $lpt . "))";
	$_filter_strings["Legal Types"] = get_filter_string("lpt", $lpt);
}
elseif($lpt)
{
   $filter =  " where (project_cost_type IN (" . $lpt . "))";
   $_filter_strings["Legal Types"] = get_filter_string("lpt", $lpt);
}


$gr = substr($gr,0, strlen($gr)-1); // remove last comma
$gr = str_replace("-", ",", $gr);
$re = substr($re,0, strlen($re)-1); // remove last comma
$re = str_replace("-", ",", $re);

if($filter) {

	if($gr and $re) 
	{
		$filter .=  " and (country_salesregion IN (" . $gr . ") OR country_region IN (" . $re . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
		$_filter_strings["Supplying Regions"] = get_filter_string("re", $re);
	}
	elseif($gr) // geografical regions
	{
		$filter .=  " and (country_salesregion IN (" . $gr . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
	}
	elseif($re) //supplying
	{
		$filter .=  " and (country_region IN (" . $re . "))";
		$_filter_strings["Supplying Regions"] = get_filter_string("re", $re);
	}
}
else
{
	if($gr and $re) 
	{
		$filter .=  " where (country_salesregion IN (" . $gr . ") OR country_region IN (" . $re . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
		$_filter_strings["Supplying Regions"] = get_filter_string("re", $re);
	}
	elseif($gr) // geografical regions
	{
		$filter .=  " where (country_salesregion IN (" . $gr . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
	}
	elseif($re) //supplying
	{
		$filter .=  " where (country_region IN (" . $re . "))";
		$_filter_strings["Supplying Regions"] = get_filter_string("re", $re);
	}
}

$cnt = substr($cnt,0, strlen($cnt)-1); // remove last comma
$cnt = str_replace("-", ",", $cnt);
if($cnt and $filter) // country
{
    $filter .=  " and (order_shop_address_country IN (" . $cnt . "))";
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);

}
elseif($cnt)
{
    $filter =  " where (order_shop_address_country IN (" . $cnt . "))";
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);
}


$sc = substr($sc,0, strlen($sc)-1); // remove last comma
$sc = str_replace("-", ",", $sc);
if($sc and $filter) // Retail Coordinator
{
    $filter .=  " and (project_retail_coordinator IN (" . $sc . "))";
	$_filter_strings["Project Manager"] = get_filter_string("sc", $sc);

}
elseif($sc)
{
    $filter =  " where (project_retail_coordinator IN (" . $sc . "))";
	$_filter_strings["Project Manager"] = get_filter_string("sc", $sc);
}


$lsc = substr($lsc,0, strlen($lsc)-1); // remove last comma
$lsc = str_replace("-", ",", $lsc);

if($lsc and $filter) // Local Retail Coordinator
{
	$filter .=  " and (project_local_retail_coordinator IN (" . $lsc . "))";
	$_filter_strings["Local Project Manager"] = get_filter_string("lrtc", $lsc);

}
elseif($lsc)
{
    $filter =  " where (project_local_retail_coordinator IN (" . $lsc . "))";
	$_filter_strings["Local Project Manager"] = get_filter_string("lrtc", $lsc);

}

$rto = substr($rto,0, strlen($rto)-1); // remove last comma
$rto = str_replace("-", ",", $rto);
if($rto and $filter) // Retail Operator
{
    $filter .=  " and (order_retail_operator IN (" . $rto . "))";
	$_filter_strings["Retail Operator"] = get_filter_string("rto", $rto);

}
elseif($rto)
{
    $filter =  " where (order_retail_operator IN (" . $rto . "))";
	$_filter_strings["Retail Operator"] = get_filter_string("rto", $rto);
}


if($st == 4) // projects operating
{
	//only projects with an opening date
	if($filter) // closed from year
	{
		$filter.=  " and (project_actual_opening_date <> '0000-00-00' and project_actual_opening_date is not null) ";
	}
	else
	{
		$filter =  "where (project_actual_opening_date <> '0000-00-00' and project_actual_opening_date is not null) ";
	}

	//only projects without an closing date

	if($filter) // closed from year
	{
		$filter.=  " and order_actual_order_state_code <> '900' and (project_shop_closingdate = '0000-00-00' or project_shop_closingdate is null) ";
	}
	else
	{
		$filter =  "where order_actual_order_state_code <> '900' and project_shop_closingdate = '0000-00-00' or project_shop_closingdate is null) ";
	}
	$_filter_strings["Treatment State"] = "operating";
}
elseif($st == 1) // projects in progress
{
	
	if($filter) // closed from year
	{
		$filter.=  " and project_state = 1 ";
	}
	else
	{
		$filter =  "where project_state = 1 ";
	}

	if($filter) // closed from year
	{
		$filter.=  " and (order_archive_date = '0000-00-00' or order_archive_date is null) ";
	}
	else
	{
		$filter =  "where (order_archive_date = '0000-00-00' or order_archive_date is null) ";
	}
	$_filter_strings["Treatment State"] = "in progress";
	
	/*
	if($filter) // closed from year
	{
		$filter.=  " and (project_actual_opening_date = '0000-00-00' or project_actual_opening_date is null) ";
	}
	else
	{
		$filter =  "where (project_actual_opening_date = '0000-00-00' or project_actual_opening_date is null) ";
	}

	if($filter) // closed from year
	{
		$filter.=  " and order_actual_order_state_code <> '900' and (project_shop_closingdate = '0000-00-00' or project_shop_closingdate is null) ";
	}
	else
	{
		$filter =  "where order_actual_order_state_code <> '900' and project_shop_closingdate = '0000-00-00' or project_shop_closingdate is null) ";
	}
	*/
}
elseif($st == 2) // projects on hold
{
	
	if($filter) // closed from year
	{
		$filter.=  " and project_state = 2 ";
	}
	else
	{
		$filter =  "where project_state = 2 ";
	}

	if($filter) // closed from year
	{
		$filter.=  " and order_actual_order_state_code <> '900' and (project_shop_closingdate = '0000-00-00' or project_shop_closingdate is null) ";
	}
	else
	{
		$filter =  "where order_actual_order_state_code <> '900' and project_shop_closingdate = '0000-00-00' or project_shop_closingdate is null) ";
	}
	$_filter_strings["Treatment State"] = "on hold";
}
elseif($st == 6) // cancelled
{
	
	if($filter)
	{
		$filter.=  " and order_actual_order_state_code = '900'";
	}
	else
	{
		$filter =  "where order_actual_order_state_code = '900'";
	}
	$_filter_strings["Treatment State"] = "cancelled";
}


if($filter and $fosc) // from order state
{
    $filter.=  " and order_actual_order_state_code >= '" . $fosc . "' ";
	$_filter_strings["From Order State Code"] = $fosc;
}
elseif($fosc)
{
	 $filter.=  " where order_actual_order_state_code >= '" . $fosc . "' ";
	 $_filter_strings["From Order State Code"] = $fosc;
}

if($filter and $tosc) // to order state
{
    $filter.=  " and order_actual_order_state_code <= '" . $tosc . "' ";
	$_filter_strings["To Order State Code"] = $tosc;
}
elseif($tosc)
{
	$filter.=  " where order_actual_order_state_code <= '" . $tosc . "' ";
	$_filter_strings["To Order State Code"] = $tosc;
}


if($gr and $re) 
{
	$order = " order by region_name, salesregion_name, country_name, project_number, order_shop_address_company, order_shop_address_place ";

	$order2 = " order by order_actual_order_state_code, region_name, salesregion_name, country_name, project_number, order_shop_address_company, order_shop_address_place ";
}
elseif($gr) 
{
	$order = " order by salesregion_name, country_name, project_number, order_shop_address_company, order_shop_address_place ";
	$order2 = " order by order_actual_order_state_code, salesregion_name, country_name, project_number, order_shop_address_company, order_shop_address_place ";
}
elseif($re) 
{
	$order = " order by region_name, country_name, project_number, order_shop_address_company, order_shop_address_place ";
	$order2 = " order by  order_actual_order_state_code, oregion_name, country_name, project_number, order_shop_address_company, order_shop_address_place ";
}
else
{
	$order = " order by country_name, project_number, order_shop_address_company, order_shop_address_place ";
	$order2 = " order by order_actual_order_state_code, country_name, project_number, order_shop_address_company, order_shop_address_place ";
}



//filter posareas get all matching orders
$areas = substr($areas,0, strlen($areas)-1); // remove last comma
$areas = str_replace("-", ",", $areas);

if($areas)
{
	
	$order_ids = array();
	$sql = "select project_order " . 
	       "from projects " .
		   "inner join orders on order_id = project_order " .
		   "left join project_costs on project_cost_order = project_order " .
		   "left join project_costtypes on project_costtype_id = project_cost_type " .
		   "left join projectkinds on projectkind_id = project_projectkind " .
		   "left join postypes on postype_id = project_postype " . 
		   "left join product_lines on product_line_id = project_product_line " .
		   "left join productline_subclasses on productline_subclass_id = project_product_line_subclass " .
		   "left join possubclasses on possubclass_id = project_pos_subclass " .
		   "left join addresses on address_id = order_client_address " .
		   "left join countries on country_id = order_shop_address_country " .
		   "left join salesregions on salesregion_id = country_salesregion " .
		   "left join regions on region_id = country_region " .
		   "left join users on user_id = project_retail_coordinator " . 
		   "left join transportation_types as transportation_types1 on order_preferred_transportation_arranged = transportation_types1. transportation_type_id ".
		   "left join transportation_types as transportation_types2 on order_preferred_transportation_mode = transportation_types2. transportation_type_id ".
		   $filter;
	
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$sql_a = "select DISTINCT posorder_order " . 
			     "from posorders " . 
			     "left join posareas on posorder_posaddress = posarea_posaddress " .
			     "where posorder_order = " . $row["project_order"] . 
			     " and posarea_area IN (" . $areas . ")";

		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
			$order_ids[$row["project_order"]] = $row["project_order"];
		}

		$sql_a = "select DISTINCT posorder_order " . 
			     "from posorderspipeline " . 
			     "left join posareaspipeline on posorder_posaddress = posarea_posaddress " .
			     "where posorder_order = " . $row["project_order"] . 
			     " and posarea_area IN (" . $areas . ")";

		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
			$order_ids[$row["project_order"]] = $row["project_order"];
		}
	
	}

	if(count($order_ids) > 0)
	{
	
		
		if($filter) 
		{
			$filter.=  " and project_order IN (" . implode ( ',' , $order_ids ) . ") ";
		}
		else
		{
			$filter =  " where project_order IN (" . implode ( ',' , $order_ids ) . ") ";
		}
	}
	$_filter_strings["Neighbourhood Locations"] = get_filter_string("areas", $areas);
}

//filter design objectives get all matching orders
$dos = substr($dos,0, strlen($dos)-1); // remove last comma
$dos = str_replace("-", ",", $dos);

if($dos)
{
	
	$order_ids2 = array();
	$sql = "select project_order " . 
	       "from project_items " .
		   "left join projects on project_id = project_item_project " .
		   "where project_item_item IN (" . $dos . ")";
	
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$order_ids2[$row["project_order"]] = $row["project_order"];
	}

	if(count($order_ids2) > 0)
	{
		if($filter) // Design Objectives
		{
			$filter.=  " and project_order IN (" . implode ( ',' , $order_ids2 ) . ") ";
		}
		else
		{
			$filter =  " where project_order IN (" . implode ( ',' , $order_ids2 ) . ") ";
		}
	}
	$_filter_strings["Design Objectives"] = get_filter_string("dos", $dos);
}

$sql_d = "select project_id, project_order, project_number, project_postype, " .
		   "user_name, project_cost_CER, project_cost_milestone_turnaround, " .
		   "project_projectkind, project_order, project_costtype_text, " .
		   "DATE_FORMAT(projects.project_construction_startdate, '%d.%m.%Y') as construction_startdate, " .
		   "DATE_FORMAT(projects.date_created, '%d.%m.%Y') as submission_date, " .
		   "DATE_FORMAT(project_planned_opening_date, '%d.%m.%Y') as preferred_opening_date, " .
		   "DATE_FORMAT(project_actual_opening_date, '%d.%m.%Y') as actual_opening_date, " .
		   "DATE_FORMAT(project_real_opening_date, '%d.%m.%Y') as realistic_opening_date, " .
		   "project_cost_type, project_costtype_text, projectkind_code, postype_name, " .
		   "country_name,  project_cost_sqms, order_shop_address_company, product_line_name, " .
		   "order_shop_address_place, order_shop_address_address, order_actual_order_state_code, " . 
		   "order_shop_address_country, country_region, possubclass_name, productline_subclass_name, " . 
		   "transportation_types1.transportation_type_name as transportation_type_name_arranged, " . 
		   "transportation_types2.transportation_type_name as transportation_type_name_mode, " . 
		   "region_name, salesregion_name, " .
		   " address_company " .
		   "from projects " .
		   "inner join orders on order_id = project_order " .
		   "left join project_costs on project_cost_order = project_order " .
		   "left join project_costtypes on project_costtype_id = project_cost_type " .
		   "left join projectkinds on projectkind_id = project_projectkind " .
		   "left join postypes on postype_id = project_postype " . 
		   "left join product_lines on product_line_id = project_product_line " .
		   "left join productline_subclasses on productline_subclass_id = project_product_line_subclass " .
		   "left join possubclasses on possubclass_id = project_pos_subclass " .
		   "left join addresses on address_id = order_franchisee_address_id " .
		   "left join countries on country_id = order_shop_address_country " .
		   "left join salesregions on salesregion_id = country_salesregion " .
		   "left join regions on region_id = country_region " .
		   "left join users on user_id = project_retail_coordinator " . 
		   "left join transportation_types as transportation_types1 on order_preferred_transportation_arranged = transportation_types1. transportation_type_id ".
		   "left join transportation_types as transportation_types2 on order_preferred_transportation_mode = transportation_types2. transportation_type_id ".
		   $filter . 
		   $order;


/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$captions = array();

$captions['A'] = "Nr";
$captions['B'] = "Project Number";
$captions['C'] = "POS Type";

if($gr) 
{
	$captions['D'] = "Sales Region/Country";
}
elseif($re) 
{
	$captions['D'] = "Supplying Region/Country";
}
else
{
	$captions['D'] = "Country";
}
$captions['E'] = "POS Location";
if($owners) // owners
{
	$captions['F'] = "Owner";
	$captions['G'] = "N=New, R=Renovation, TR=Take Over/Renovation, \nT=Take Over, L=Lease Renewal";
	$captions['H'] = "Legal Type";
	$captions['I'] = "sqm";
	$captions['J'] = "Product Line";
	$captions['K'] = "Product Line Subclass";
	$captions['L'] = "Project Status";
	$captions['M'] = "Lease Process";
	$captions['S'] = "HQ Project Preparation";
	$captions['X'] = BRAND . " President Approval";
	$captions['Z'] = "Swatch Group Approval";
	$captions['AB'] = "Operations Preparation";
	$captions['AE'] = "Construction Starting Date";
	$captions['AF'] = "On-site Work Process";
	$captions['AK'] = "Actual Opening Date";

	$captions2 = array();
	$captions2['A'] = "";
	$captions2['B'] = "";
	$captions2['C'] = "";
	$captions2['D'] = "";
	$captions2['E'] = "";
	$captions2['F'] = "";
	$captions2['G'] = "";
	$captions2['H'] = "";
	$captions2['I'] = "";
	$captions2['J'] = "";
	$captions2['K'] = "";
	$captions2['L'] = "";
	$captions2['M'] = "Project Submitted";
	$captions2['N'] = "LN Approved (0020)";
	$captions2['O'] = "Handover Date";
	$captions2['P'] = "Rent Start Date";
	$captions2['Q'] = "Client's Preferred Opening Date";
	$captions2['R'] = "HQ Feedback \non Agreed Opening Date";
	$captions2['S'] = "Preliminary Plan (220)";
	$captions2['T'] = "1st Layout (260)";
	$captions2['U'] = "Layout Approved (280)";
	$captions2['V'] = "Booklet Approved (460)";
	$captions2['W'] = "Budget Approval (620)";
	$captions2['X'] = "AF/CER (1200)";
	$captions2['Y'] = "CER below 50KCHF (1250)";
	$captions2['Z'] = "CER to SG (1300)";
	$captions2['AA'] = "KL Notice Received (1400)";
	$captions2['AB'] = "Order Date (700)";
	$captions2['AC'] = "Order Confirmation Date (720)";
	$captions2['AD'] = "Preferred Transportation";
	$captions2['AE'] = "";
	$captions2['AF'] = "Floor Arrival Date";
	$captions2['AG'] = "Frames Arrival Date";
	$captions2['AH'] = "Visuals Arrival Date";
	$captions2['AI'] = "Illumination Arrival Date";
	$captions2['AJ'] = "Furniture Arrival Date";
	$captions2['AK'] = "";


	$colwidth = array();
	$colwidth['A'] = "5";
	$colwidth['B'] = "5";
	$colwidth['C'] = "1";
	$colwidth['D'] = "5";
	$colwidth['E'] = "5";
	$colwidth['F'] = "10";
	$colwidth['G'] = "5";
	$colwidth['H'] = "5";
	$colwidth['I'] = "5";
	$colwidth['J'] = "5";
	$colwidth['K'] = "5";
	$colwidth['L'] = "10";
	$colwidth['M'] = "10";
	$colwidth['N'] = "10";
	$colwidth['O'] = "10";
	$colwidth['P'] = "10";
	$colwidth['Q'] = "10";
	$colwidth['R'] = "10";
	$colwidth['S'] = "10";
	$colwidth['T'] = "10";
	$colwidth['U'] = "10";
	$colwidth['V'] = "10";
	$colwidth['W'] = "10";
	$colwidth['X'] = "10";
	$colwidth['Y'] = "10";
	$colwidth['Z'] = "10";
	$colwidth['AA'] = "10";
	$colwidth['AB'] = "10";
	$colwidth['AC'] = "10";
	$colwidth['AD'] = "10";
	$colwidth['AE'] = "10";
	$colwidth['AF'] = "10";
	$colwidth['AG'] = "10";
	$colwidth['AH'] = "10";
	$colwidth['AI'] = "10";
	$colwidth['AJ'] = "10";
	$colwidth['AK'] = "10";

}
else
{
	$captions['F'] = "N=New, R=Renovation, TR=Take Over/Renovation, \nT=Take Over, L=Lease Renewal";
	$captions['G'] = "Legal Type";
	$captions['H'] = "sqm";
	$captions['I'] = "Product Line";
	$captions['J'] = "Product Line Subclass";
	$captions['K'] = "Project Status";
	$captions['L'] = "Lease Process";
	$captions['R'] = "HQ Project Preparation";
	$captions['W'] = BRAND . " President Approval";
	$captions['Y'] = "Swatch Group Approval";
	$captions['AA'] = "Operations Preparation";
	$captions['AD'] = "Construction Starting Date";
	$captions['AE'] = "On-site Work Process";
	$captions['AJ'] = "Actual Opening Date";

	$captions2 = array();
	$captions2['A'] = "";
	$captions2['B'] = "";
	$captions2['C'] = "";
	$captions2['D'] = "";
	$captions2['E'] = "";
	$captions2['F'] = "";
	$captions2['G'] = "";
	$captions2['H'] = "";
	$captions2['I'] = "";
	$captions2['J'] = "";
	$captions2['K'] = "";
	$captions2['L'] = "Project Submitted";
	$captions2['M'] = "LN Approved (0020)";
	$captions2['N'] = "Handover Date";
	$captions2['O'] = "Rent Start Date";
	$captions2['P'] = "Client's Preferred Opening Date";
	$captions2['Q'] = "HQ Feedback \non Agreed Opening Date";
	$captions2['R'] = "Preliminary Plan (220)";
	$captions2['S'] = "1st Layout (260)";
	$captions2['T'] = "Layout Approved (280)";
	$captions2['U'] = "Booklet Approved (460)";
	$captions2['V'] = "Budget Approval (620)";
	$captions2['W'] = "AF/CER (1200)";
	$captions2['X'] = "CER below 50KCHF (1250)";
	$captions2['Y'] = "CER to SG (1300)";
	$captions2['Z'] = "KL Notice Received (1400)";
	$captions2['AA'] = "Order Date (700)";
	$captions2['AB'] = "Order Confirmation Date (720)";
	$captions2['AC'] = "Preferred Transportation";
	$captions2['AD'] = "";
	$captions2['AE'] = "Floor Arrival Date";
	$captions2['AF'] = "Frames Arrival Date";
	$captions2['AG'] = "Visuals Arrival Date";
	$captions2['AH'] = "Illumination Arrival Date";
	$captions2['AI'] = "Furniture Arrival Date";
	$captions2['AJ'] = "";


	$colwidth = array();
	$colwidth['A'] = "5";
	$colwidth['B'] = "5";
	$colwidth['C'] = "1";
	$colwidth['D'] = "5";
	$colwidth['E'] = "5";
	$colwidth['F'] = "10";
	$colwidth['G'] = "5";
	$colwidth['H'] = "5";
	$colwidth['I'] = "5";
	$colwidth['J'] = "5";
	$colwidth['K'] = "5";
	$colwidth['L'] = "10";
	$colwidth['M'] = "10";
	$colwidth['N'] = "10";
	$colwidth['O'] = "10";
	$colwidth['P'] = "10";
	$colwidth['Q'] = "10";
	$colwidth['R'] = "10";
	$colwidth['S'] = "10";
	$colwidth['T'] = "10";
	$colwidth['U'] = "10";
	$colwidth['V'] = "10";
	$colwidth['W'] = "10";
	$colwidth['X'] = "10";
	$colwidth['Y'] = "10";
	$colwidth['Z'] = "10";
	$colwidth['AA'] = "10";
	$colwidth['AB'] = "10";
	$colwidth['AC'] = "10";
	$colwidth['AD'] = "10";
	$colwidth['AE'] = "10";
	$colwidth['AF'] = "10";
	$colwidth['AG'] = "10";
	$colwidth['AH'] = "10";
	$colwidth['AI'] = "10";
	$colwidth['AJ'] = "10";
}




$captions3 = array();
$captions3['A'] = "Nr";
$captions3['B'] = "Project";
$captions3['C'] = "POS Type";
$captions3['D'] = "Status";
if($gr) 
{
	$captions3['E'] = "Sales Region/Country";
}
elseif($re) 
{
	$captions3['E'] = "Supplying Region/Country";
}
else
{
	$captions3['E'] = "Country";
}
$captions3['F'] = "POS Location";
$captions3['G'] = "Supplier";
$captions3['H'] = "Category";
$captions3['I'] = "Order Date";
$captions3['J'] = "Ready for Pickup Date";
$captions3['K'] = "Pickup Date";
$captions3['L'] = "Shipment Code";
$captions3['M'] = "Forwarder";
$captions3['N'] = "Expected Arrival Date";
$captions3['O'] = "Arrival Date";

$colwidth2 = array();
$colwidth2['A'] = "5";
$colwidth2['B'] = "14";
$colwidth2['C'] = "10";
$colwidth2['D'] = "8";
$colwidth2['E'] = "15";
$colwidth2['F'] = "50";
$colwidth2['G'] = "20";
$colwidth2['H'] = "20";
$colwidth2['I'] = "20";
$colwidth2['J'] = "22";
$colwidth2['K'] = "20";
$colwidth2['L'] = "25";
$colwidth2['M'] = "20";
$colwidth2['N'] = "22";
$colwidth2['O'] = "20";


/********************************************************************
    Start output
*********************************************************************/
$objPHPExcel = new PHPExcel();




$logo = new PHPExcel_Worksheet_Drawing();
$logo->setName('Logo');
$logo->setDescription('Logo');
$logo->setPath('../pictures/omega_logo.jpg');
$logo->setHeight(36);
$logo->setWidth(113);


$logo2 = new PHPExcel_Worksheet_Drawing();
$logo2->setName('Logo');
$logo2->setDescription('Logo');
$logo2->setPath('../pictures/omega_logo.jpg');
$logo2->setHeight(36);
$logo2->setWidth(113);

$sheet = $objPHPExcel->getActiveSheet();
$objPHPExcel->getActiveSheet()->setTitle('Masterplan');

$logo->setWorksheet($objPHPExcel->getActiveSheet());

if($show_detail == 1) 
{
	$sheet2 = $objPHPExcel->createSheet();
	$sheet2->setTitle('Delivery');
	$objPHPExcel->setActiveSheetIndex(1);
	$logo2->setWorksheet($objPHPExcel->getActiveSheet());
	$objPHPExcel->setActiveSheetIndex(0);
}


//output formats
$default_border = array(
    'style' => PHPExcel_Style_Border::BORDER_THIN,
    'color' => array('rgb'=>'1006A3')
);

$style_normal_border = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '1006A3'),
		),
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
);


$style_normal_border_red = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '1006A3'),
		),
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
	'font' => array(
        'bold' => true,
		'color' => array('rgb'=>'ff1c23')
    ),
	
);


$style_title = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
    'font' => array(
        'bold' => true,
		'size' => 16
    )
);

$style_header = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'font' => array(
        'bold' => true,
    )
);

$style_header_center = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	),
    'font' => array(
        'bold' => true,
    )
);


$style_lease_normal = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb'=>'bfe3ff'),
    ),
    'font' => array(
        'bold' => false,
    )
);


$style_preparation_normal = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb'=>'fdfa9b'),
    ),
    'font' => array(
        'bold' => false,
    )
);

$style_approval_normal = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb'=>'fdccc2'),
    ),
    'font' => array(
        'bold' => false,
    )
);


$style_construction_normal = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb'=>'a2fd9b'),
    ),
    'font' => array(
        'bold' => false,
    )
);


$style_lease = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb'=>'bfe3ff'),
    ),
    'font' => array(
        'bold' => true,
    )
);


$style_preparation = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb'=>'fdfa9b'),
    ),
    'font' => array(
        'bold' => true,
    )
);

$style_approval = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb'=>'fdccc2'),
    ),
    'font' => array(
        'bold' => true,
    )
);


$style_construction = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb'=>'a2fd9b'),
    ),
    'font' => array(
        'bold' => true,
    )
);


$style_zebra = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb'=>'fdfa9b'),
    ),
    'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
	'font' => array(
        'bold' => false,
    )
);

$style_zebra_off = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
    'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
	'font' => array(
        'bold' => false,
    )
);



//default styles
$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(8); 


// HEADRES ROW 1
$sheet->setCellValue('D1', 'PROJECTS MASTERPLAN: ' . $query_title);
$sheet->getStyle('D1')->applyFromArray( $style_title );
$sheet->getRowDimension('1')->setRowHeight(36);


$row_index = 2;
if($print_query_filter == 1)
{
	foreach($_filter_strings as $key=>$value)
	{
		 $sheet->setCellValue('A' .$row_index , $key . ": " . $value);
		 $row_index++;
	}
	$row_index++;
}



if($owners) // owners
{
	// HEADRES ROW 2
	$sheet->setCellValue('M' .$row_index, 'PROJECT IMPLEMENTATION PHASE');
	$sheet->mergeCells('M' .$row_index . ':W' .$row_index);
	$sheet->getStyle('M' .$row_index . ':W' .$row_index)->applyFromArray( $style_header_center );

	$sheet->setCellValue('X' .$row_index, 'APPROVAL PHASE');
	$sheet->mergeCells('X' .$row_index . ':AA' .$row_index);
	$sheet->getStyle('X' .$row_index . ':AA' .$row_index)->applyFromArray( $style_header_center );

	$sheet->setCellValue('AB' .$row_index, 'CONSTRUCTION PHASE');
	$sheet->mergeCells('AB' .$row_index . ':AK' .$row_index);
	$sheet->getStyle('AB' .$row_index . ':AK' .$row_index)->applyFromArray( $style_header_center );
	$row_index++;

	// HEADRES ROW 3
	foreach($captions as $col=>$caption){
		$sheet->setCellValue($col . $row_index, $caption);
		$sheet->getStyle($col . $row_index)->applyFromArray( $style_header );
	}
	$cellrange = 'A' .$row_index . ':AK' .$row_index;
	$sheet->getStyle($cellrange)->getAlignment()->setWrapText(true);
	$sheet->getStyle($cellrange)->getAlignment()->setTextRotation(90);
	$sheet->getRowDimension($row_index)->setRowHeight(150);

	$sheet->mergeCells('M' .$row_index . ':R' .$row_index);
	$sheet->mergeCells('S' .$row_index . ':W' .$row_index);
	$sheet->mergeCells('X' .$row_index . ':Y' .$row_index);
	$sheet->mergeCells('Z' .$row_index . ':AA' .$row_index);
	$sheet->mergeCells('AB' .$row_index . ':AD' .$row_index);
	$sheet->mergeCells('AF' .$row_index . ':AJ' .$row_index);

	$sheet->getStyle('M' .$row_index . ':R' .$row_index)->applyFromArray( $style_lease );
	$sheet->getStyle('S' .$row_index . ':W' .$row_index)->applyFromArray( $style_preparation );
	$sheet->getStyle('X' .$row_index . ':AA' .$row_index)->applyFromArray( $style_approval );
	$sheet->getStyle('AB' .$row_index . ':AK' .$row_index)->applyFromArray( $style_construction );
	$row_index++;

	// HEADRES ROW 4
	foreach($captions2 as $col=>$caption){
		$sheet->setCellValue($col . $row_index, $caption);
		$sheet->getStyle($col . $row_index)->applyFromArray( $style_header );
	}
	$cellrange = 'A' .$row_index . ':AK' .$row_index;
	$sheet->getStyle($cellrange)->getAlignment()->setWrapText(true);
	$sheet->getStyle($cellrange)->getAlignment()->setTextRotation(90);
	$sheet->getRowDimension($row_index)->setRowHeight(140);

	$sheet->getStyle('M' .$row_index . ':R' .$row_index)->applyFromArray( $style_lease );
	$sheet->getStyle('S' .$row_index . ':W' .$row_index)->applyFromArray( $style_preparation );
	$sheet->getStyle('X' .$row_index . ':AA' .$row_index)->applyFromArray( $style_approval );
	$sheet->getStyle('AB' .$row_index . ':AK' .$row_index)->applyFromArray( $style_construction );
	$row_index++;
}
else
{
	// HEADRES ROW 2
	$sheet->setCellValue('L' .$row_index, 'PROJECT IMPLEMENTATION PHASE');
	$sheet->mergeCells('L' .$row_index . ':V' .$row_index);
	$sheet->getStyle('L' .$row_index . ':V' .$row_index)->applyFromArray( $style_header_center );

	$sheet->setCellValue('W' .$row_index, 'APPROVAL PHASE');
	$sheet->mergeCells('W' .$row_index . ':Z' .$row_index);
	$sheet->getStyle('W' .$row_index . ':Z' .$row_index)->applyFromArray( $style_header_center );

	$sheet->setCellValue('AA' .$row_index, 'CONSTRUCTION PHASE');
	$sheet->mergeCells('AA' .$row_index . ':AJ' .$row_index);
	$sheet->getStyle('AA' .$row_index . ':AJ' .$row_index)->applyFromArray( $style_header_center );
	$row_index++;

	// HEADRES ROW 3
	foreach($captions as $col=>$caption){
		$sheet->setCellValue($col . $row_index, $caption);
		$sheet->getStyle($col . $row_index)->applyFromArray( $style_header );
	}
	$cellrange = 'A' .$row_index . ':AJ' .$row_index;
	$sheet->getStyle($cellrange)->getAlignment()->setWrapText(true);
	$sheet->getStyle($cellrange)->getAlignment()->setTextRotation(90);
	$sheet->getRowDimension($row_index)->setRowHeight(150);

	$sheet->mergeCells('L' .$row_index . ':Q' .$row_index);
	$sheet->mergeCells('R' .$row_index . ':V' .$row_index);
	$sheet->mergeCells('W' .$row_index . ':X' .$row_index);
	$sheet->mergeCells('Y' .$row_index . ':Z' .$row_index);
	$sheet->mergeCells('AA' .$row_index . ':AC' .$row_index);
	$sheet->mergeCells('AE' .$row_index . ':AI' .$row_index);

	$sheet->getStyle('L' .$row_index . ':Q' .$row_index)->applyFromArray( $style_lease );
	$sheet->getStyle('R' .$row_index . ':V' .$row_index)->applyFromArray( $style_preparation );
	$sheet->getStyle('W' .$row_index . ':Z' .$row_index)->applyFromArray( $style_approval );
	$sheet->getStyle('AA' .$row_index . ':AJ' .$row_index)->applyFromArray( $style_construction );
	$row_index++;

	// HEADRES ROW 4
	foreach($captions2 as $col=>$caption){
		$sheet->setCellValue($col . $row_index, $caption);
		$sheet->getStyle($col . $row_index)->applyFromArray( $style_header );
	}
	$cellrange = 'A' .$row_index . ':AJ' .$row_index;
	$sheet->getStyle($cellrange)->getAlignment()->setWrapText(true);
	$sheet->getStyle($cellrange)->getAlignment()->setTextRotation(90);
	$sheet->getRowDimension($row_index)->setRowHeight(140);

	$sheet->getStyle('L' .$row_index . ':Q' .$row_index)->applyFromArray( $style_lease );
	$sheet->getStyle('R' .$row_index . ':V' .$row_index)->applyFromArray( $style_preparation );
	$sheet->getStyle('W' .$row_index . ':Z' .$row_index)->applyFromArray( $style_approval );
	$sheet->getStyle('AA' .$row_index . ':AJ' .$row_index)->applyFromArray( $style_construction );
	$row_index++;
}

// SHEET 2 HEADRES ROW 1
if($show_detail == 1) 
{
	$sheet2->setCellValue('D1', 'Overview Project Delivery Schedule: ' . $query_title);
	$sheet2->getStyle('D1')->applyFromArray( $style_title );
	$sheet2->getRowDimension('1')->setRowHeight(36);

	
	if($print_query_filter == 1)
	{
		$row_index = 2;
		foreach($_filter_strings as $key=>$value)
		{
			 $sheet2->setCellValue('A' .$row_index , $key . ": " . $value);
			 $row_index++;
		}
		$row_index++;
	}
	
	
	//SHEET 2 HEADER ROW 1
	foreach($captions3 as $col=>$caption){
		$sheet2->setCellValue($col . $row_index, $caption);
		$sheet2->getStyle($col . $row_index)->applyFromArray( $style_header );
	}
	$row_index++;
}


if($print_query_filter == 1)
{
	$row_index++;
	$row_index++;
	$row_index++;
}


//OUTPUT DATA
$zebra_counter = 0;
$i = 1;

$unapproved_cer_projects = array();

$res = mysql_query($sql_d) or dberror($sql_d);
while ($row = mysql_fetch_assoc($res))
{
	$cell_index = 0;

	$posaddress = $row["order_shop_address_company"] . ', ' . $row["order_shop_address_place"];
	
	// get project's milestones
	$milestones = array();
	$sql_m = "select project_milestone_milestone,  " . 
  			 "DATE_FORMAT(project_milestone_date, '%d.%m.%Y') as milestone_date " .
		     "from project_milestones " . 
		     "where project_milestone_project = " . $row["project_id"];

	$res_m = mysql_query($sql_m) or dberror($sql_m);
	while ($row_m = mysql_fetch_assoc($res_m))
	{
		$milestones[$row_m["project_milestone_milestone"]] = $row_m["milestone_date"];
	}

	//get Lease data
	$free_rental_period = "";
	$handover_date = "";
	$rent_start_date = "";

	$sql_m = 'select poslease_startdate, poslease_handoverdate, poslease_firstrentpayed, ' .
		     'DATE_FORMAT(poslease_handoverdate, "%d.%m.%Y") as handover_date, ' .
		     'DATE_FORMAT(poslease_startdate, "%d.%m.%Y") as rent_start_date, ' .
		     "period_diff(date_format(poslease_firstrentpayed, '%Y%m'), date_format(poslease_startdate, '%Y%m')) as free_rental_period " .
		     'from posleases ' . 
		     'where poslease_order = ' . $row["project_order"];

	$res_m = mysql_query($sql_m) or dberror($sql_m);
	if ($row_m = mysql_fetch_assoc($res_m))
	{
		$handover_date = $row_m["handover_date"];
		$free_rental_period = $row_m["free_rental_period"];
		$rent_start_date = $row_m["rent_start_date"];
	}
	else
	{
		$sql_m = 'select poslease_startdate, poslease_handoverdate, poslease_firstrentpayed, ' .
				 'DATE_FORMAT(poslease_handoverdate, "%d.%m.%Y") as handover_date, ' .
			     'DATE_FORMAT(poslease_startdate, "%d.%m.%Y") as rent_start_date, ' .
				 "period_diff(date_format(poslease_firstrentpayed, '%Y%m'), date_format(poslease_startdate, '%Y%m')) as free_rental_period " .
				 'from posleasespipeline ' . 
				 'where poslease_order = ' . $row["project_order"];

		$res_m = mysql_query($sql_m) or dberror($sql_m);
		if ($row_m = mysql_fetch_assoc($res_m))
		{
			$handover_date = $row_m["handover_date"];
			$free_rental_period = $row_m["free_rental_period"];
			$rent_start_date = $row_m["rent_start_date"];
		}
	}

	if($free_rental_period <= 0) {
		$free_rental_period = "";
	}
	if($handover_date == '00.00.0000') {
		$handover_date = "";
	}


	//get project steps
	$order_states = array();

	$sql_m = 'select actual_order_state_state, ' . 
	         'DATE_FORMAT(date_created, "%d.%m.%Y") as step_date ' . 
		     ' from actual_order_states ' . 
		     ' where actual_order_state_order = ' . $row['project_order'] . 
		     ' order by date_created ASC';
	
	$res_m = mysql_query($sql_m) or dberror($sql_m);
	while ($row_m = mysql_fetch_assoc($res_m))
	{
		$order_states[$row_m["actual_order_state_state"]] = $row_m["step_date"];
	}





	//get the date of floor delivery
	$floor_delivery_date = "";
	$floor_ordered = false;
	$floor_exists_in_the_list_of_materials = 0;
	$sql_m = 'select order_item_id, order_item_ordered ' .
		     'from order_items ' . 
		     'left join items on item_id = order_item_item ' . 
		     'where order_item_order = ' . $row["project_order"]  .
		     ' and item_category = 3';

	$res_m = mysql_query($sql_m) or dberror($sql_m);
	while ($row_m = mysql_fetch_assoc($res_m))
	{
		$sql_d = 'select DATE_FORMAT(date_date, "%d.%m.%Y") as delivery_date ' .
			     'from dates ' . 
			     'where date_type = 4 and date_order_item = ' . $row_m['order_item_id'] . 
			     ' order by date_date ASC';

		$res_d = mysql_query($sql_d) or dberror($sql_d);
		while ($row_d = mysql_fetch_assoc($res_d))
		{
			$floor_delivery_date = $row_d["delivery_date"];
		}

		if($row_m["order_item_ordered"] != '0000-00-00' or $row_m["order_item_ordered"] != NULL) 
		{
			$floor_ordered = true;
		}

		$floor_exists_in_the_list_of_materials = 1;
		
	}

	//get the date of furniture delivery
	$furniture_delivery_date = "";
	$sql_m = 'select order_item_id ' .
		     'from order_items ' . 
		     'left join items on item_id = order_item_item ' . 
		     'where order_item_order = ' . $row["project_order"]  .
		     ' and (item_category IN (1, 4, 5, 12) '. $costmonitoring_group_filter . ')';

	$res_m = mysql_query($sql_m) or dberror($sql_m);
	while ($row_m = mysql_fetch_assoc($res_m))
	{
		$sql_d = 'select DATE_FORMAT(date_date, "%d.%m.%Y") as delivery_date ' .
			     'from dates ' . 
			     'where date_type = 4 and date_order_item = ' . $row_m['order_item_id'] . 
			     ' order by date_date ASC';

		$res_d = mysql_query($sql_d) or dberror($sql_d);
		while ($row_d = mysql_fetch_assoc($res_d))
		{
			$furniture_delivery_date = $row_d["delivery_date"];
		}
	}

	//get the date of frame delivery
	$frame_delivery_date = "";
	$sql_m = 'select order_item_id ' .
		     'from order_items ' . 
		     'left join items on item_id = order_item_item ' . 
		     'where order_item_order = ' . $row["project_order"]  .
		     ' and item_category IN (24)';

	$res_m = mysql_query($sql_m) or dberror($sql_m);
	while ($row_m = mysql_fetch_assoc($res_m))
	{
		$sql_d = 'select DATE_FORMAT(date_date, "%d.%m.%Y") as delivery_date ' .
			     'from dates ' . 
			     'where date_type = 4 and date_order_item = ' . $row_m['order_item_id'] . 
			     ' order by date_date ASC';

		$res_d = mysql_query($sql_d) or dberror($sql_d);
		while ($row_d = mysql_fetch_assoc($res_d))
		{
			$frame_delivery_date = $row_d["delivery_date"];
		}
	}

	//get the date of illumination delivery
	$illumination_delivery_date = "";
	$sql_m = 'select order_item_id ' .
		     'from order_items ' . 
		     'left join items on item_id = order_item_item ' . 
		     'where order_item_order = ' . $row["project_order"]  .
		     ' and item_category IN (16)';

	$res_m = mysql_query($sql_m) or dberror($sql_m);
	while ($row_m = mysql_fetch_assoc($res_m))
	{
		$sql_d = 'select DATE_FORMAT(date_date, "%d.%m.%Y") as delivery_date ' .
			     'from dates ' . 
			     'where date_type = 4 and date_order_item = ' . $row_m['order_item_id'] . 
			     ' order by date_date ASC';

		$res_d = mysql_query($sql_d) or dberror($sql_d);
		while ($row_d = mysql_fetch_assoc($res_d))
		{
			$illumination_delivery_date = $row_d["delivery_date"];
		}
	}

	//get the date of visuals delivery
	$visuals_delivery_date = "";
	$sql_m = 'select order_item_id ' .
		     'from order_items ' . 
		     'left join items on item_id = order_item_item ' . 
		     'where order_item_order = ' . $row["project_order"]  .
		     ' and item_category IN (6, 7)';

	$res_m = mysql_query($sql_m) or dberror($sql_m);
	while ($row_m = mysql_fetch_assoc($res_m))
	{
		$sql_d = 'select DATE_FORMAT(date_date, "%d.%m.%Y") as delivery_date ' .
			     'from dates ' . 
			     'where date_type = 4 and date_order_item = ' . $row_m['order_item_id'] . 
			     ' order by date_date ASC';

		$res_d = mysql_query($sql_d) or dberror($sql_d);
		while ($row_d = mysql_fetch_assoc($res_d))
		{
			$visuals_delivery_date = $row_d["delivery_date"];
		}
	}
	
	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $i);
	if($colwidth['A'] < strlen($i)){$colwidth['A'] = strlen($i);}
	$cell_index++;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["project_number"]);
	if($colwidth['B'] < strlen($row["project_number"])){$colwidth['B'] = 2+strlen($row["project_number"]);}
	$cell_index++;


	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["postype_name"]);
	if($colwidth['C'] < strlen($row["postype_name"])){$colwidth['C'] = 2+strlen($row["postype_name"]);}
	$cell_index++;

	if($gr) 
	{
		$tmp = $row["salesregion_name"] . " - " . $row["country_name"];
	}
	elseif($gr) 
	{
		$tmp = $row["region_name"] . " - " . $row["country_name"];
	}
	else
	{
		$tmp = $row["country_name"];
	}
	
	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $tmp);
	if($colwidth['D'] < strlen($tmp)){$colwidth['D'] = 3+strlen($tmp);}
	$cell_index++;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index,$posaddress);
	if($colwidth['E'] < strlen($posaddress)){$colwidth['E'] = strlen($posaddress);}
	$cell_index++;
		

	
	if($owners) // owners
	{
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index,$row["address_company"]);
		if($colwidth['F'] < strlen($row["address_company"])){$colwidth['F'] = strlen($row["address_company"]);}
		$cell_index++;
		
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["projectkind_code"]);
		$cell_index++;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["project_costtype_text"]);
		if($colwidth['H'] < strlen($row["project_costtype_text"])){$colwidth['H'] = 2+strlen($row["project_costtype_text"]);}
		$cell_index++;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["project_cost_sqms"]);
		if($colwidth['I'] < strlen($row["project_cost_sqms"])){$colwidth['I'] = 2+strlen($row["project_cost_sqms"]);}
		$cell_index++;
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["product_line_name"]);
		if($colwidth['J'] < strlen($row["product_line_name"])){$colwidth['J'] = 2+strlen($row["product_line_name"]);}
		$cell_index++;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["productline_subclass_name"]);
		if($colwidth['K'] < strlen($row["productline_subclass_name"])){$colwidth['K'] = strlen($row["productline_subclass_name"]);}
		$cell_index++;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["order_actual_order_state_code"]);
		$cell_index++;


		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["submission_date"]);
		$cell_index++;

		//LN Approved
		if(array_key_exists(13, $milestones)) {
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $milestones[13]);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell_index++;

		//Hand over date
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $handover_date);
		$cell_index++;

		//Rent Start Date
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $rent_start_date);
		$cell_index++;

		//preferred opening date
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row['preferred_opening_date']);
		$cell_index++;

		//realisitc opening date
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row['realistic_opening_date']);
		$cell_index++;


		//order states
		if(array_key_exists(5, $order_states)) //step 220
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $order_states[5]);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell_index++;

		if(array_key_exists(9, $order_states)) //step 260
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $order_states[9]);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell_index++;

		if(array_key_exists(11, $order_states)) //step 280
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $order_states[11]);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell_index++;

		if(array_key_exists(24, $order_states)) //step 460
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $order_states[24]);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell_index++;

		
		if(array_key_exists(34, $order_states)) //step 620
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $order_states[34]);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell_index++;



		//AF approved/CER back from President (1200)
		if(array_key_exists(10, $milestones)) {
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $milestones[10]);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell_index++;

		//CER approved/below 50KCHF (1250)
		$cer_approved = false;
		if(array_key_exists(21, $milestones)) {
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $milestones[21]);
			if($row["order_actual_order_state_code"] == '620' and $milestones[21] != NULL and $milestones[21] != '0000-00-0')
			{
				$cer_approved = true;
			}
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell_index++;

		//CER to Swatch Group
		if(array_key_exists(11, $milestones)) {
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $milestones[11]);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell_index++;

		//KL noticed received

		if(array_key_exists(12, $milestones)) {
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $milestones[12]);
			if($row["order_actual_order_state_code"] == '620' and $milestones[12] != NULL and $milestones[12] != '0000-00-0')
			{
				$cer_approved = true;
			}
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell_index++;


		


		//order states
		if(array_key_exists(35, $order_states)) //step 700
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $order_states[35]);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell_index++;

		if(array_key_exists(37, $order_states)) //step 720
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $order_states[37]);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell_index++;

		//preferred transportation by client
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row['transportation_type_name_arranged'] . " " . $row['transportation_type_name_mode']);
		if($colwidth['AD'] < strlen($row['transportation_type_name_arranged'] . " " . $row['transportation_type_name_mode'])){$colwidth['AD'] = 2+strlen($row['transportation_type_name_arranged'] . " " . $row['transportation_type_name_mode']);}
		$cell_index++;

		//construction starting date
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["construction_startdate"]);
		$cell_index++;

		//floor delivery
		if($row["order_actual_order_state_code"] >= '600')
		{
			if($floor_exists_in_the_list_of_materials == false) 
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "local");
			}
			elseif($floor_ordered == false and $floor_delivery_date == '') 
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "n.a.");
			}
			elseif($floor_ordered == true and $floor_delivery_date == '') 
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "ordered");
			}
			else
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $floor_delivery_date);
			}
		}
		else
		{
			if($floor_exists_in_the_list_of_materials == false) 
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "n.a.");
			}
			elseif($floor_ordered == false and $floor_delivery_date == '') 
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "n.a.");
			}
			elseif($floor_ordered == true and $floor_delivery_date == '') 
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "ordered");
			}
			else
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $floor_delivery_date);
			}
		}
		
		$cell_index++;

		//frame delivery
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $frame_delivery_date);
		$cell_index++;

		//visuals delivery
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $visuals_delivery_date);
		$cell_index++;


		//illuminate delivery
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $illumination_delivery_date);
		$cell_index++;

		//furniture delivery
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $furniture_delivery_date);
		$cell_index++;


		//actual opening date
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row['actual_opening_date']);
		$cell_index++;

		
			
		foreach($captions2 as $col=>$caption) {
			$sheet->getStyle($col . $row_index)->applyFromArray($style_normal_border);
		}

		
		$sheet->getStyle('M' . $row_index . ':R' . $row_index)->applyFromArray( $style_lease_normal);;
		$sheet->getStyle('S' . $row_index . ':W'. $row_index)->applyFromArray( $style_preparation_normal );
		$sheet->getStyle('XW' . $row_index . ':AA'. $row_index)->applyFromArray( $style_approval_normal );
		$sheet->getStyle('AB' . $row_index . ':AK' . $row_index)->applyFromArray( $style_construction_normal );

		if($row["order_actual_order_state_code"] == '620' and $cer_approved == false)
		{
			$sheet->getStyle('K' . $row_index . ':K' . $row_index)->applyFromArray( $style_normal_border_red);;
		}
	}
	else
	{
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["projectkind_code"]);
		$cell_index++;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["project_costtype_text"]);
		if($colwidth['G'] < strlen($row["project_costtype_text"])){$colwidth['G'] = 2+strlen($row["project_costtype_text"]);}
		$cell_index++;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["project_cost_sqms"]);
		if($colwidth['H'] < strlen($row["project_cost_sqms"])){$colwidth['H'] = 2+strlen($row["project_cost_sqms"]);}
		$cell_index++;
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["product_line_name"]);
		if($colwidth['I'] < strlen($row["product_line_name"])){$colwidth['I'] = 2+strlen($row["product_line_name"]);}
		$cell_index++;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["productline_subclass_name"]);
		if($colwidth['J'] < strlen($row["productline_subclass_name"])){$colwidth['J'] = strlen($row["productline_subclass_name"]);}
		$cell_index++;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["order_actual_order_state_code"]);
		$cell_index++;


		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["submission_date"]);
		$cell_index++;

		//LN Approved
		if(array_key_exists(13, $milestones)) {
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $milestones[13]);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell_index++;

		//Hand over date
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $handover_date);
		$cell_index++;

		//Rent Start Date
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $rent_start_date);
		$cell_index++;

		//preferred opening date
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row['preferred_opening_date']);
		$cell_index++;

		//realisitc opening date
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row['realistic_opening_date']);
		$cell_index++;


		//order states
		if(array_key_exists(5, $order_states)) //step 220
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $order_states[5]);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell_index++;

		if(array_key_exists(9, $order_states)) //step 260
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $order_states[9]);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell_index++;

		if(array_key_exists(11, $order_states)) //step 280
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $order_states[11]);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell_index++;

		if(array_key_exists(24, $order_states)) //step 460
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $order_states[24]);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell_index++;

		
		if(array_key_exists(34, $order_states)) //step 620
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $order_states[34]);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell_index++;



		//AF approved/CER back from President (1200)
		$cer_approved = false;
		if(array_key_exists(10, $milestones)) {
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $milestones[10]);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell_index++;

		//CER approved/below 50KCHF (1250)
		if(array_key_exists(21, $milestones)) {
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $milestones[21]);
			if($row["order_actual_order_state_code"] == '620' and $milestones[21] != NULL and $milestones[21] != '0000-00-0')
			{
				$cer_approved = true;
			}
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell_index++;

		//CER to Swatch Group
		if(array_key_exists(11, $milestones)) {
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $milestones[11]);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell_index++;

		//KL noticed received

		if(array_key_exists(12, $milestones)) {
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $milestones[12]);
			
			if($row["order_actual_order_state_code"] == '620' and $milestones[12] != NULL and $milestones[12] != '0000-00-0')
			{
				$cer_approved = true;
			}
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell_index++;


		


		//order states
		if(array_key_exists(35, $order_states)) //step 700
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $order_states[35]);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell_index++;

		if(array_key_exists(37, $order_states)) //step 720
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $order_states[37]);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell_index++;

		//preferred transportation by client
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row['transportation_type_name_arranged'] . " " . $row['transportation_type_name_mode']);
		if($colwidth['AC'] < strlen($row['transportation_type_name_arranged'] . " " . $row['transportation_type_name_mode'])){$colwidth['AC'] = 2+strlen($row['transportation_type_name_arranged'] . " " . $row['transportation_type_name_mode']);}
		$cell_index++;

		//construction starting date
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["construction_startdate"]);
		$cell_index++;

		//floor delivery
		if($row["order_actual_order_state_code"] >= '600')
		{
			if($floor_exists_in_the_list_of_materials == false) 
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "local");
			}
			elseif($floor_ordered == false and $floor_delivery_date == '') 
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "n.a.");
			}
			elseif($floor_ordered == true and $floor_delivery_date == '') 
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "ordered");
			}
			else
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $floor_delivery_date);
			}
		}
		else
		{
			if($floor_exists_in_the_list_of_materials == false) 
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "n.a.");
			}
			elseif($floor_ordered == false and $floor_delivery_date == '') 
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "n.a.");
			}
			elseif($floor_ordered == true and $floor_delivery_date == '') 
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "ordered");
			}
			else
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $floor_delivery_date);
			}
		}
		
		$cell_index++;

		//frame delivery
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $frame_delivery_date);
		$cell_index++;

		//visuals delivery
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $visuals_delivery_date);
		$cell_index++;


		//illuminate delivery
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $illumination_delivery_date);
		$cell_index++;

		//furniture delivery
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $furniture_delivery_date);
		$cell_index++;


		//actual opening date
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row['actual_opening_date']);
		$cell_index++;

		
			
		foreach($captions2 as $col=>$caption) {
			$sheet->getStyle($col . $row_index)->applyFromArray($style_normal_border);
		}


		$sheet->getStyle('L' . $row_index . ':Q' . $row_index)->applyFromArray( $style_lease_normal);;
		$sheet->getStyle('R' . $row_index . ':V'. $row_index)->applyFromArray( $style_preparation_normal );
		$sheet->getStyle('W' . $row_index . ':Z'. $row_index)->applyFromArray( $style_approval_normal );
		$sheet->getStyle('AA' . $row_index . ':AJ' . $row_index)->applyFromArray( $style_construction_normal );


		if($row["order_actual_order_state_code"] == '620' and $cer_approved == false)
		{
			$sheet->getStyle('K' . $row_index . ':K' . $row_index)->applyFromArray( $style_normal_border_red);
			$unapproved_cer_projects[] = $row["project_id"];
		}
	}

	$i++;
	$row_index++;
}




//TAB 2: Delivery
//show Supplier and Forwarder Detail
if($show_detail == 1) {
	
	$zebra_counter = 0;
	$i = 1;
	$row_index = 7;
	
	$objPHPExcel->setActiveSheetIndex(1);
	
	$sql_d = "select project_id, project_order, project_number, project_postype, " .
			   "user_name, project_cost_CER, project_cost_milestone_turnaround, " .
			   "project_projectkind, project_order, project_costtype_text, " .
			   "DATE_FORMAT(projects.project_construction_startdate, '%d.%m.%Y') as construction_startdate, " .
			   "DATE_FORMAT(projects.date_created, '%d.%m.%Y') as submission_date, " .
			   "DATE_FORMAT(project_planned_opening_date, '%d.%m.%Y') as preferred_opening_date, " .
			   "DATE_FORMAT(project_actual_opening_date, '%d.%m.%Y') as actual_opening_date, " .
			   "DATE_FORMAT(project_real_opening_date, '%d.%m.%Y') as realistic_opening_date, " .
			   "project_cost_type, project_costtype_text, projectkind_code, postype_name, " .
			   "country_name,  project_cost_sqms, order_shop_address_company, product_line_name, " .
			   "order_shop_address_place, order_shop_address_address, order_actual_order_state_code, " . 
			   "order_shop_address_country, country_region, possubclass_name, productline_subclass_name, " . 
			   "transportation_types1.transportation_type_name as transportation_type_name_arranged, " . 
			   "transportation_types2.transportation_type_name as transportation_type_name_mode, " . 
			   "region_name, salesregion_name, " .
			   " address_company " .
			   "from projects " .
			   "inner join orders on order_id = project_order " .
			   "left join project_costs on project_cost_order = project_order " .
			   "left join project_costtypes on project_costtype_id = project_cost_type " .
			   "left join projectkinds on projectkind_id = project_projectkind " .
			   "left join postypes on postype_id = project_postype " . 
			   "left join product_lines on product_line_id = project_product_line " .
			   "left join productline_subclasses on productline_subclass_id = project_product_line_subclass " .
			   "left join possubclasses on possubclass_id = project_pos_subclass " .
			   "left join addresses on address_id = order_franchisee_address_id " .
			   "left join countries on country_id = order_shop_address_country " .
			   "left join salesregions on salesregion_id = country_salesregion " .
			   "left join regions on region_id = country_region " .
			   "left join users on user_id = project_retail_coordinator " . 
			   "left join transportation_types as transportation_types1 on order_preferred_transportation_arranged = transportation_types1. transportation_type_id ".
				"left join transportation_types as transportation_types2 on order_preferred_transportation_mode = transportation_types2. transportation_type_id ".
			   $filter . 
		       " and project_cost_cms_completed != 1 " . 
			   $order2;

	$res = mysql_query($sql_d) or dberror($sql_d);
	while ($row = mysql_fetch_assoc($res))
	{

		$cell_index2 = 0;

		$posaddress = $row["order_shop_address_company"] . ', ' . $row["order_shop_address_place"];

		$sheet2->setCellValueByColumnAndRow($cell_index2, $row_index, $i);
		$cell_index2++;
		$sheet2->setCellValueByColumnAndRow($cell_index2, $row_index, $row["project_number"]);
		$cell_index2++;

		$sheet2->setCellValueByColumnAndRow($cell_index2, $row_index, $row["postype_name"]);
		$cell_index2++;

		$sheet2->setCellValueByColumnAndRow($cell_index2, $row_index, $row["order_actual_order_state_code"]);
		$cell_index2++;

		if($gr) 
		{
			$tmp = $row["salesregion_name"] . " - " . $row["country_name"];
		}
		elseif($gr) 
		{
			$tmp = $row["region_name"] . " - " . $row["country_name"];
		}
		else
		{
			$tmp = $row["country_name"];
		}
		$sheet2->setCellValueByColumnAndRow($cell_index2, $row_index, $tmp);
		if($colwidth2['E'] < strlen($tmp)){$colwidth2['E'] = 4+strlen($tmp);}
		$cell_index2++;
		$sheet2->setCellValueByColumnAndRow($cell_index2, $row_index,$posaddress);
		$cell_index2++;

		//get order dates
		$suppliers = '';
		$item_category = '';
		$forwarders = '';
		$order_dates = '';
		$ready_for_pickup_dates = '';
		$pickup_dates = '';
		$expected_arrival_dates = '';
		$arrival_dates = '';
		$shipment_codes = '';
		$sql_m = 'select DISTINCT suppliers.address_shortcut as supplier, forwarders.address_shortcut as forwarder,' . 
				 'DATE_FORMAT(order_item_ordered, "%d.%m.%Y") as date_ordered, ' .
				 'DATE_FORMAT(order_item_ready_for_pickup, "%d.%m.%Y") as date_ready_for_pickup, ' .
				 'DATE_FORMAT(order_item_pickup, "%d.%m.%Y") as pickup_date, ' .
				 'DATE_FORMAT(order_item_expected_arrival, "%d.%m.%Y") as expected_arrival_date, ' .
				 'DATE_FORMAT(order_item_arrival, "%d.%m.%Y") as arrival_date, ' .
				 'item_category_name, order_item_shipment_code ' .
				 'from order_items ' .
				 'left join addresses as suppliers on suppliers.address_id = order_item_supplier_address ' .
				 'left join addresses as forwarders on forwarders.address_id = order_item_forwarder_address ' .
				 'left join items on item_id = order_item_item ' . 
				 'left join item_categories on item_category_id = item_category ' .
				 'where order_item_order = ' . $row["project_order"] . 
				 ' and order_item_supplier_address > 0 ' . 
				 ' and (item_category IN (1, 3, 4, 5, 6, 7, 12, 16, 24) ' . $costmonitoring_group_filter . ')' .
				 ' order by order_item_ordered ASC';

		$res_m = mysql_query($sql_m) or dberror($sql_m);
		while ($row_m = mysql_fetch_assoc($res_m))
		{
			$suppliers .= $row_m['supplier'] . "\n";
			
			if($row_m['item_category_name']) {
				$item_category .= $row_m['item_category_name'] . "\n";
			}
			else
			{
				$item_category .= "Special Items" . "\n";
			}
			$forwarders .= $row_m['forwarder'] . "\n";
			$order_dates .= $row_m['date_ordered'] . "\n";
			$ready_for_pickup_dates .= $row_m['date_ready_for_pickup'] . "\n";
			$pickup_dates .= $row_m['pickup_date'] . "\n";
			$expected_arrival_dates .= $row_m['expected_arrival_date'] . "\n";
			$arrival_dates .= $row_m['arrival_date'] . "\n";
			$shipment_codes .= $row_m['order_item_shipment_code'] . "\n";
		}

		$suppliers = substr($suppliers, 0, strlen($suppliers)-1);
		$item_category = substr($item_category, 0, strlen($item_category)-1);
		$forwarders = substr($forwarders, 0, strlen($forwarders)-1);
		$order_dates = substr($order_dates, 0, strlen($order_dates)-1);
		$ready_for_pickup_dates = substr($ready_for_pickup_dates, 0, strlen($ready_for_pickup_dates)-1);
		$pickup_dates = substr($pickup_dates, 0, strlen($pickup_dates)-1);
		$shipment_codes = substr($shipment_codes, 0, strlen($shipment_codes)-1);
		$expected_arrival_dates = substr($expected_arrival_dates, 0, strlen($expected_arrival_dates)-1);
		$arrival_dates = substr($arrival_dates, 0, strlen($arrival_dates)-1);

		if($suppliers)
		{
			$sheet2->setCellValueByColumnAndRow($cell_index2, $row_index, $suppliers);
		}
		else
		{
			$sheet2->setCellValueByColumnAndRow($cell_index2, $row_index, "");
		}
		$cell_index2++;

		

		if($item_category)
		{
			$sheet2->setCellValueByColumnAndRow($cell_index2, $row_index, $item_category);
		}
		else
		{
			$sheet2->setCellValueByColumnAndRow($cell_index2, $row_index, "");
		}
		$cell_index2++;



		if($order_dates)
		{
			$sheet2->setCellValueByColumnAndRow($cell_index2, $row_index, $order_dates);
		}
		else
		{
			$sheet2->setCellValueByColumnAndRow($cell_index2, $row_index, "");
		}
		$cell_index2++;

		if($ready_for_pickup_dates)
		{
			$sheet2->setCellValueByColumnAndRow($cell_index2, $row_index, $ready_for_pickup_dates);
		}
		else
		{
			$sheet2->setCellValueByColumnAndRow($cell_index2, $row_index, "");
		}
		$cell_index2++;


		if($pickup_dates)
		{
			$sheet2->setCellValueByColumnAndRow($cell_index2, $row_index, $pickup_dates);
		}
		else
		{
			$sheet2->setCellValueByColumnAndRow($cell_index2, $row_index, "");
		}
		$cell_index2++;

		
		if($shipment_codes)
		{
			$sheet2->setCellValueByColumnAndRow($cell_index2, $row_index, $shipment_codes);
		}
		else
		{
			$sheet2->setCellValueByColumnAndRow($cell_index2, $row_index, "");
		}
		$cell_index2++;

		

		if($forwarders)
		{
			$sheet2->setCellValueByColumnAndRow($cell_index2, $row_index, $forwarders);
		}
		else
		{
			$sheet2->setCellValueByColumnAndRow($cell_index2, $row_index, "");
		}
		$cell_index2++;

		
		if($expected_arrival_dates)
		{
			$sheet2->setCellValueByColumnAndRow($cell_index2, $row_index, $expected_arrival_dates);
		}
		else
		{
			$sheet2->setCellValueByColumnAndRow($cell_index2, $row_index, "");
		}
		$cell_index2++;

		if($arrival_dates)
		{
			$sheet2->setCellValueByColumnAndRow($cell_index2, $row_index, $arrival_dates);
		}
		else
		{
			$sheet2->setCellValueByColumnAndRow($cell_index2, $row_index, "");
		}
		$cell_index2++;

		$sheet2->getStyle('G' . $row_index)->getAlignment()->setWrapText(true);
		$sheet2->getStyle('H' . $row_index)->getAlignment()->setWrapText(true);
		$sheet2->getStyle('I' . $row_index)->getAlignment()->setWrapText(true);
		$sheet2->getStyle('J' . $row_index)->getAlignment()->setWrapText(true);
		$sheet2->getStyle('K' . $row_index)->getAlignment()->setWrapText(true);
		$sheet2->getStyle('L' . $row_index)->getAlignment()->setWrapText(true);
		$sheet2->getStyle('M' . $row_index)->getAlignment()->setWrapText(true);
		$sheet2->getStyle('N' . $row_index)->getAlignment()->setWrapText(true);
		$sheet2->getStyle('O' . $row_index)->getAlignment()->setWrapText(true);
		
		if($zebra_counter == 1) { 
			$sheet2->getStyle('A' . $row_index . ':O' . $row_index)->applyFromArray( $style_zebra );
			$zebra_counter = 0;
		}
		else
		{   
			$sheet2->getStyle('A' . $row_index . ':O' . $row_index)->applyFromArray( $style_zebra_off );
			$zebra_counter = 1;
		}
		
		
		foreach($colwidth2 as $col=>$width) {
			$sheet2->getStyle($col . $row_index)->applyFromArray($style_normal_border);
		}



		if(in_array($row["project_id"], $unapproved_cer_projects))
		{
			$sheet2->getStyle('D' . $row_index . ':D' . $row_index)->applyFromArray( $style_normal_border_red);
		}
		

		$i++;
		$row_index++;
	}
}



//Format column heights and witdhs
foreach($colwidth as $col=>$width) {
	$sheet->getColumnDimension($col)->setWidth($width);
}

if($show_detail == 1) 
{
	foreach($colwidth2 as $col=>$width) {
		$sheet2->getColumnDimension($col)->setWidth($width);
	}
}

//reset active cell


if($show_detail == 1) {
	
	$sheet2->setCellValue('A1', "");
	$sheet2->getStyle('A1')->applyFromArray($style_title);
	$objPHPExcel->getActiveSheet()->insertNewRowBefore(1, 1);
	$sheet2->setCellValue('A1', "");
	$sheet2->getStyle('A1')->applyFromArray($style_title);

	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A3);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(1);

}


$objPHPExcel->setActiveSheetIndex(0);
$sheet->setCellValue('A1', "");
$sheet->getStyle('A1')->applyFromArray($style_title);
$objPHPExcel->getActiveSheet()->insertNewRowBefore(1, 1);
$sheet->setCellValue('A1', "");
$sheet->getStyle('A1')->applyFromArray($style_title);



$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A3);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(1);



/********************************************************************
    Start output
*********************************************************************/
$filename = 'masterplan_project_states_' . date('Ymd H:i:s') . '.xls';
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');
header('Pragma: ');

//send data
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
?>