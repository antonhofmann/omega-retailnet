<?php
/********************************************************************

    projects_query_2_xls.php

    Generate Excel File for the Current State of Projects

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-13
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2007-12-07
    Version:        1.1.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_perform_queries");

require_once "../include/xls/Writer.php";
require_once "include/get_functions.php";
require_once("projects_query_filter_strings.php");

/********************************************************************
    prepare Data Needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("projects_queries.php");
}


//check if filter is present
$query_name = "";
$print_query_filter = 0;
$sql = "select mis_query_name, mis_query_filter, mis_print_filter from mis_queries " .
	   "where mis_query_id = " . param("query_id");

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{

	$query_name = $row["mis_query_name"];

	if($row["mis_print_filter"] == 1)
	{
		$print_query_filter = 1;
	}

	$filters = array();
	$filters = unserialize($row["mis_query_filter"]);

	foreach($filters as $key=>$value)
	{
		
		$fosc = $filters["fst"]; //from order state
		$tosc = $filters["tst"]; // to order state

		$clt = $filters["clt"]; // Client Types
		$pl = $filters["pl"]; // product lines
		$pls = $filters["plsc"]; // product line subclasses
		$pk = $filters["pk"]; // product kinds
		$pt = $filters["pt"]; // project types
		$suc = $filters["ptsc"]; // POS Type Subclass
		$lpt = $filters["pct"]; // project cost type
		$gr = $filters["gr"]; // Geografical Regions
		$re = $filters["re"]; // Supplying Regions
		$cnt = $filters["co"]; // Countries
		$sc = $filters["rtc"]; // Retail Coordinators
		$lrtc = $filters["lrtc"]; // Local Project managers

		$fdy = $filters["fdy"]; // Actual Opening Year From
		$fdm = $filters["fdm"]; // Actual Opening Month From
		$tdy = $filters["tdy"]; // Actual Opening Year To
		$tdm = $filters["tdm"]; // Actual Opening Month To
		

		if(array_key_exists("areas", $filters))
		{
			$areas = $filters["areas"]; // Neighbourhood Locations
		
		}
		else
		{
			$areas = "";
		}

		if(array_key_exists("dos", $filters))
		{
			$dos = $filters["dos"]; // Design Objectives
		
		}
		else
		{
			$dos = "";
		}

		if(array_key_exists("rto", $filters))
		{
			$rto = $filters["rto"]; // Retail Operators
		
		}
		else
		{
			$rto = "";
		}

	}
}
else
{
	redirect("projects_queries.php");
}


//get product lines


$pos_types = array();

$sql = "select postype_id, postype_name ".
       "from postypes order by postype_name";

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
    $pos_types[$row["postype_id"]] = $row["postype_name"];        
}


$header = "Operating POS: " . $query_name . " (" . date("d.m.Y G:i") . ")";

/********************************************************************
    prepare Data
*********************************************************************/
$filter = "";

$clt = substr($clt,0, strlen($clt)-1); // remove last comma
$clt = str_replace("-", ",", $clt);
if($clt) // client type
{
    $filter =  " where (address_client_type IN (" . $clt . "))";
	$_filter_strings["Client Types"] = get_filter_string("clt", $clt);
}

$pl = substr($pl,0, strlen($pl)-1); // remove last comma
$pl = str_replace("-", ",", $pl);
if($pl and $filter) // product line
{
	$filter .=  " and (project_product_line IN (" . $pl . "))";
	$_filter_strings["Product Lines"] = get_filter_string("pl", $pl);
}
elseif($pl) // product line
{
    $filter =  " where (project_product_line IN (" . $pl . "))";
	$_filter_strings["Product Lines"] = get_filter_string("pl", $pl);
}

$pls = substr($pls,0, strlen($pls)-1); // remove last comma
$pls = str_replace("-", ",", $pls);
if($pls and $filter) // product line subclasses
{
    $filter .=  " and project_product_line_subclass IN (" . $pls . ")";
	$_filter_strings["Product Line Subclasses"] = get_filter_string("pls", $pls);
}
elseif($pls)
{
    $filter =  " where project_product_line_subclass IN (" . $pls . ")";
	$_filter_strings["Product Line Subclasses"] = get_filter_string("pls", $pls);
}

$pk = substr($pk,0, strlen($pk)-1); // remove last comma
$pk = str_replace("-", ",", $pk);

if($pk and $filter) // product line
{
	$filter .=  " and (project_projectkind IN (" . $pk . "))";
	$_filter_strings["Project Kinds"] = get_filter_string("pk", $pk);
}
elseif($pk) // product kind
{
    $filter =  " where (project_projectkind IN (" . $pk . "))";
	$_filter_strings["Project Kinds"] = get_filter_string("pk", $pk);
}

$pt = substr($pt,0, strlen($pt)-1); // remove last comma
$pt = str_replace("-", ",", $pt);
if($pt and $filter) // project type
{
    $filter .=  " and (project_postype IN (" . $pt . "))";
	$_filter_strings["POS Types"] = get_filter_string("pt", $pt);
}
elseif($pt)
{
    $filter =  " where (project_postype IN (" . $pt . "))";
	$_filter_strings["POS Types"] = get_filter_string("pt", $pt);
}

$suc = substr($suc,0, strlen($suc)-1); // remove last comma
$suc = str_replace("-", ",", $suc);
if($suc and $filter) // POS Type Subclass
{
    $filter .=  " and (project_pos_subclass IN (" . $suc . "))";
	$_filter_strings["POS Type Subclasses"] = get_filter_string("suc", $suc);
}
elseif($suc)
{
   $filter =  " where (project_pos_subclass IN (" . $suc . "))";
   $_filter_strings["POS Type Subclasses"] = get_filter_string("suc", $suc);
}

$lpt = substr($lpt,0, strlen($lpt)-1); // remove last comma
$lpt = str_replace("-", ",", $lpt);
if($lpt and $filter) // project cost type
{
    $filter .=  " and (project_cost_type IN (" . $lpt . "))";
	$_filter_strings["Legal Types"] = get_filter_string("lpt", $lpt);
}
elseif($lpt)
{
   $filter =  " where (project_cost_type IN (" . $lpt . "))";
   $_filter_strings["Legal Types"] = get_filter_string("lpt", $lpt);
}


$gr = substr($gr,0, strlen($gr)-1); // remove last comma
$gr = str_replace("-", ",", $gr);
$re = substr($re,0, strlen($re)-1); // remove last comma
$re = str_replace("-", ",", $re);

if($filter) {

	if($gr and $re) 
	{
		$filter .=  " and (country_salesregion IN (" . $gr . ") OR country_region IN (" . $re . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
		$_filter_strings["Supplying Regions"] = get_filter_string("re", $re);
	}
	elseif($gr) // geografical regions
	{
		$filter .=  " and (country_salesregion IN (" . $gr . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
	}
	elseif($re) //supplying
	{
		$filter .=  " and (country_region IN (" . $re . "))";
		$_filter_strings["Supplying Regions"] = get_filter_string("re", $re);
	}
}
else
{
	if($gr and $re) 
	{
		$filter .=  " where (country_salesregion IN (" . $gr . ") OR country_region IN (" . $re . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
		$_filter_strings["Supplying Regions"] = get_filter_string("re", $re);
	}
	elseif($gr) // geografical regions
	{
		$filter .=  " where (country_salesregion IN (" . $gr . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
	}
	elseif($re) //supplying
	{
		$filter .=  " where (country_region IN (" . $re . "))";
		$_filter_strings["Supplying Regions"] = get_filter_string("re", $re);
	}
}

$cnt = substr($cnt,0, strlen($cnt)-1); // remove last comma
$cnt = str_replace("-", ",", $cnt);
if($cnt and $filter) // country
{
    $filter .=  " and (order_shop_address_country IN (" . $cnt . "))";
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);

}
elseif($cnt)
{
    $filter =  " where (order_shop_address_country IN (" . $cnt . "))";
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);
}


$sc = substr($sc,0, strlen($sc)-1); // remove last comma
$sc = str_replace("-", ",", $sc);
if($sc and $filter) // Retail Coordinator
{
    $filter .=  " and (project_retail_coordinator IN (" . $sc . "))";
	$_filter_strings["Project Manager"] = get_filter_string("sc", $sc);

}
elseif($sc)
{
    $filter =  " where (project_retail_coordinator IN (" . $sc . "))";
	$_filter_strings["Project Manager"] = get_filter_string("sc", $sc);
}

$lrtc = substr($lrtc,0, strlen($lrtc)-1); // remove last comma
$lrtc = str_replace("-", ",", $lrtc);
if($lrtc and $filter) // Retail Coordinator
{
    $filter .=  " and (project_local_retail_coordinator IN (" . $lrtc . "))";
	$_filter_strings["Local Project Manager"] = get_filter_string("lrtc", $lrtc);

}
elseif($lrtc)
{
    $filter =  " where (project_local_retail_coordinator IN (" . $lrtc . "))";
	$_filter_strings["Local Project Manager"] = get_filter_string("lrtc", $lrtc);
}

if($filter) // opened from
{
	if($fdy > 0 and $fdm > 0)
	{
		$tmp = "" . $fdy . "" . $fdm;
		$filter.=  " and concat(YEAR(project_actual_opening_date), DATE_FORMAT(project_actual_opening_date,'%m')) >= " . $tmp;
		$_filter_strings["Opened from"] = $fdy . "-" . $fdm;
	}
	elseif($fdy > 0)
	{
		$filter.=  " and YEAR(project_actual_opening_date) >= " . $fdy;
		$_filter_strings["Opened from"] = $fdy;
	}
	elseif($fdm > 0)
	{
		$filter.=  " and MONTH(project_actual_opening_date) >= " . $fdm;
		$_filter_strings["Opened from"] = $fdm;
	}
}
else
{
	if($fdy > 0 and $fdm > 0)
	{
		$tmp = "" . $fdy . "" . $fdm;
		$filter.=  " where concat(YEAR(project_actual_opening_date), DATE_FORMAT(project_actual_opening_date,'%m')) >= " . $tmp;
		$_filter_strings["Opened from"] = $fdy . "-" . $fdm;;
	}
	elseif($fdy > 0)
	{
		$filter.=  " where YEAR(project_actual_opening_date) >= " . $fdy;
		$_filter_strings["Opened from"] = $fdy;
	}
	elseif($fdm > 0)
	{
		$filter.=  " where MONTH(project_actual_opening_date) >= " . $fdm;
		$_filter_strings["Opened from"] = $fdm;
	}
}

if($filter) // opened to
{
	if($tdy > 0 and $tdm > 0)
	{
		$tmp = "" . $tdy . "" . $tdm;
		$filter.=  " and concat(YEAR(project_actual_opening_date), DATE_FORMAT(project_actual_opening_date,'%m')) <= " . $tmp;
		$_filter_strings["Opened to"] = $tdy . "-" . $tdm;
	}
	elseif($tdy > 0)
	{
		$filter.=  " and YEAR(project_actual_opening_date) <= " . $tdy;
		$_filter_strings["Opened to"] = $tdy;
	}
	elseif($tdm > 0)
	{
		$filter.=  " and MONTH(project_actual_opening_date) <= " . $tdm;
		$_filter_strings["Opened to"] = $tdm;
	}
}
else
{
	if($tdy > 0 and $tdm > 0)
	{
		$tmp = "" . $tdy . "" . $tdm;
		$filter.=  " where concat(YEAR(project_actual_opening_date), DATE_FORMAT(project_actual_opening_date,'%m')) <= " . $tmp;
		$_filter_strings["Opened to"] = $tdy . "-" . $tdm;
	}
	elseif($tdy > 0)
	{
		$filter.=  " where YEAR(project_actual_opening_date) <= " . $tdy;
		$_filter_strings["Opened to"] = $tdy;
	}
	elseif($tdm > 0)
	{
		$filter.=  " where MONTH(project_actual_opening_date) <= " . $tdm;
		$_filter_strings["Opened to"] = $tdm;
	}
}



//only projects with an opening date

if($filter) // closed from year
{
    $filter.=  " and (project_actual_opening_date <> '0000-00-00' and project_actual_opening_date is not null) ";
}
else
{
    $filter =  "where (project_actual_opening_date <> '0000-00-00' and project_actual_opening_date is not null) ";
}

//only projects without an closing date

if($filter) // closed from year
{
    $filter.=  " and order_actual_order_state_code <> '900' and (project_shop_closingdate = '0000-00-00' or project_shop_closingdate is null) ";
}
else
{
    $filter =  "where order_actual_order_state_code <> '900' and project_shop_closingdate = '0000-00-00' or project_shop_closingdate is null) ";
}

$rto = substr($rto,0, strlen($rto)-1); // remove last comma
$rto = str_replace("-", ",", $rto);
if($rto and $filter) // Retail Operator
{
    $filter .=  " and (order_retail_operator IN (" . $rto . "))";
	$_filter_strings["Retail Operator"] = get_filter_string("rto", $rto);

}
elseif($rto)
{
    $filter =  " where (order_retail_operator IN (" . $rto . "))";
	$_filter_strings["Retail Operator"] = get_filter_string("rto", $rto);
}



//filter posareas get all matching orders
$areas = substr($areas,0, strlen($areas)-1); // remove last comma
$areas = str_replace("-", ",", $areas);

if($areas)
{
	
	$order_ids = array();
	$sql = "select project_order " . 
	       "from projects " .
		   "left join orders on order_id = project_order " .
		   "left join project_costs on project_cost_order = project_order " .
		   "left join project_costtypes on project_costtype_id = project_cost_type " .
		   "left join projectkinds on projectkind_id = project_projectkind " .
		   "left join product_lines on product_line_id = project_product_line " .
		   "left join productline_subclasses on productline_subclass_id = project_product_line_subclass " .
		   "left join possubclasses on possubclass_id = project_pos_subclass " .
		   "left join addresses on address_id = order_client_address " .
		   "left join countries on country_id = order_shop_address_country " .
		   "left join salesregions on salesregion_id = country_salesregion " .
		   "left join project_states on project_state_id = project_state " .
		   "left join users on user_id = project_retail_coordinator " . 
		   $filter;
	
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$sql_a = "select DISTINCT posorder_order " . 
			     "from posorders " . 
			     "left join posareas on posorder_posaddress = posarea_posaddress " .
			     "where posorder_order = " . $row["project_order"] . 
			     " and posarea_area IN (" . $areas . ")";

		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
			$order_ids[$row["project_order"]] = $row["project_order"];
		}

		$sql_a = "select DISTINCT posorder_order " . 
			     "from posorderspipeline " . 
			     "left join posareaspipeline on posorder_posaddress = posarea_posaddress " .
			     "where posorder_order = " . $row["project_order"] . 
			     " and posarea_area IN (" . $areas . ")";

		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
			$order_ids[$row["project_order"]] = $row["project_order"];
		}
	
	}

	if(count($order_ids) > 0)
	{
	
		
		if($filter)
		{
			$filter.=  " and project_order IN (" . implode ( ',' , $order_ids ) . ") ";
		}
		else
		{
			$filter =  " where project_order IN (" . implode ( ',' , $order_ids ) . ") ";
		}
	}
	$_filter_strings["Neighbourhood Locations"] = get_filter_string("areas", $areas);
}

//filter design objectives get all matching orders
$dos = substr($dos,0, strlen($dos)-1); // remove last comma
$dos = str_replace("-", ",", $dos);

if($dos)
{
	
	$order_ids2 = array();
	$sql = "select project_order " . 
	       "from project_items " .
		   "left join projects on project_id = project_item_project " .
		   "where project_item_item IN (" . $dos . ")";
	
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$order_ids2[$row["project_order"]] = $row["project_order"];
	}

	if(count($order_ids2) > 0)
	{
		if($filter) // Design Objectives
		{
			$filter.=  " and project_order IN (" . implode ( ',' , $order_ids2 ) . ") ";
		}
		else
		{
			$filter =  " where project_order IN (" . implode ( ',' , $order_ids2 ) . ") ";
		}
	}
	$_filter_strings["Design Objectives"] = get_filter_string("dos", $dos);
}

$sql_d = "select project_id, project_number, project_postype, " .
       "rtcs.user_name as rtc, rtos.user_name as rto,  project_cost_CER, project_cost_milestone_turnaround, " .
       "project_projectkind, project_order, project_state, project_state_text, " .
       "DATE_FORMAT(project_planned_opening_date, '%d.%m.%Y') as opening_date, " .
       "DATE_FORMAT(project_actual_opening_date, '%d.%m.%Y') as actual_opening_date, " .
       "project_cost_type, project_costtype_text, projectkind_code, " .
       "country_name,  salesregion_name, project_cost_sqms, order_shop_address_company, product_line_name, " .
       "order_shop_address_place, order_shop_address_address, order_actual_order_state_code, " . 
	   "order_shop_address_country, country_salesregion, possubclass_name, productline_subclass_name " .
       "from projects " .
       "left join orders on order_id = project_order " .
       "left join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join projectkinds on projectkind_id = project_projectkind " .
	   "left join product_lines on product_line_id = project_product_line " .
	   "left join productline_subclasses on productline_subclass_id = project_product_line_subclass " .
	   "left join possubclasses on possubclass_id = project_pos_subclass " .
       "left join addresses on address_id = order_client_address " .
       "left join countries on country_id = order_shop_address_country " .
	   "left join salesregions on salesregion_id = country_salesregion " .
       "left join project_states on project_state_id = project_state " .
       "left join users as rtcs on rtcs.user_id = project_retail_coordinator " .
	   "left join users as rtos on rtos.user_id =order_retail_operator " .
       $filter . 
       " order by salesregion_name, country_name, order_shop_address_place, project_number";


/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$filename = "projects_operating_" . date("Ymd") . ".xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename); 
$xls->setVersion(8);
$sheet =& $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

//formats
$header_row =& $xls->addFormat();
$header_row->setSize(10);
$header_row->setAlign('left');
$header_row->setBold();


$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);


$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);


$f_used =& $xls->addFormat();
$f_used->setSize(8);
$f_used->setBorder(1);
$f_used->setPattern(2);
$f_used->setBgColor('yellow');



//captions
$captions = array();
$captions[] = "Nr";
$sheet->setColumn(0, 0, 3);
$c01 = 0;

$captions[] = "Region";
$sheet->setColumn(1, 1, 24);

$captions[] = "Country";
$sheet->setColumn(2, 2, 16);

$captions[] = "POS Location Address";
$sheet->setColumn(3, 3, 50);
$captions[] = "sqm";
$sheet->setColumn(4, 4, 6);
$captions[] = "Project Number";
$sheet->setColumn(5, 5, 14);
$captions[] = "State";
$sheet->setColumn(6, 6, 4);
$captions[] = "Product Line";
$sheet->setColumn(7, 7, 18);

$captions[] = "Product Line Subclass";
$sheet->setColumn(8, 8, 10);

$captions[] = "Store Type";
$sheet->setColumn(9, 9, 10);

$captions[] = "POS Type Subclass";
$sheet->setColumn(10, 10, 10);

$captions[] = "Responsable";
$sheet->setColumn(11, 11, 10);

$captions[] = "Operator";
$sheet->setColumn(12, 12, 10);

$captions[] = "Opening Date";
$sheet->setColumn(13, 13, 10);

/*
$captions[] = "CER Amount";
$sheet->setColumn(8, 8, 8);

$captions[] = "Currency";
$sheet->setColumn(9, 9, 3);
*/

$num_of_milestones = -1;

$captions[] = "C=Corporate, F=Franchise";
$sheet->setColumn(14, 14, 2);
//$sheet->setColumn($num_of_milestones+12, $num_of_milestones+12, 4);

$captions[] = "N=New, R=Renovation, TR=Take Over/Renovation, T=Take Over, L=Lease Renewal";
$sheet->setColumn(15, 15, 10);
//$sheet->setColumn($num_of_milestones+13, $num_of_milestones+13, 8);

$captions[] = "Comments";
$sheet->setColumn(16, 16, 40);
//$sheet->setColumn($num_of_milestones+14, $num_of_milestones+14, 40);



/********************************************************************
    write all data
*********************************************************************/


$sheet->write(0, 0, $header, $header_row);
$row_index = 2;

if($print_query_filter == 1)
{
	foreach($_filter_strings as $key=>$value)
	{
		 $sheet->write($row_index, 0,$key . ": " . $value);
		 $row_index++;
	}
	$row_index++;
}

/********************************************************************
    write all captions
*********************************************************************/
$sheet->writeRow($row_index, 0, $captions, $f_caption);
$sheet->setRow($row_index, 110);
$row_index++;

$cell_index = 4;


$res = mysql_query($sql_d) or dberror($sql_d);
while ($row = mysql_fetch_assoc($res))
{
    
    //get all needed data of project
    $project_milestones  = array();
    $project_comments = "";

    $sql_m = "select project_milestone_milestone, project_milestone_comment, project_milestone_date_comment, " .
             "DATE_FORMAT(project_milestone_date, '%d.%m.%Y') as mdate " .
             "from project_milestones " .
             "where project_milestone_project = " . $row["project_id"];


    $res_m = mysql_query($sql_m) or dberror($sql_m);
    while ($row_m = mysql_fetch_assoc($res_m))
    {
        if($row_m["mdate"] == NULL or $row_m["mdate"] == "00.00.0000")
		{
			$project_milestones[$row_m["project_milestone_milestone"]] = $row_m["project_milestone_date_comment"];
		}
		else
		{
			$project_milestones[$row_m["project_milestone_milestone"]] = $row_m["mdate"];
		}

        if($project_comments)
        {
            if($row_m["project_milestone_comment"])
			{
				$project_comments.= " / " . $row_m["project_milestone_comment"];
			}
        }
        else
        {
            $project_comments = $row_m["project_milestone_comment"];
        }

    }

    $order_currency = get_order_currency($row["project_order"]);

    $shop_address = $row["order_shop_address_place"] . ": " . $row["order_shop_address_company"];

    // write row

    $c01++;
    $sheet->write($row_index, 0, $c01, $f_normal);

    
    $sheet->write($row_index, 1, $row["salesregion_name"], $f_normal);
    $sheet->write($row_index, 2, $row["country_name"], $f_normal);
    $sheet->write($row_index, 3, $shop_address, $f_normal);

    
    $sheet->write($row_index, 4, $row["project_cost_sqms"], $f_number);

    $sheet->write($row_index, 5, $row["project_number"], $f_normal);

	$sheet->write($row_index, 6, $row["order_actual_order_state_code"], $f_normal);

	$sheet->write($row_index, 7, $row["product_line_name"], $f_normal);

	$sheet->write($row_index, 8, $row["productline_subclass_name"], $f_normal);

    $sheet->write($row_index, 9, $pos_types[$row["project_postype"]], $f_normal);



	$sheet->write($row_index, 10, $row["possubclass_name"], $f_normal);
    
    $sheet->write($row_index, 11, $row["rtc"], $f_normal);
	$sheet->write($row_index, 12, $row["rto"], $f_normal);

    if($row["actual_opening_date"] != "00.00.0000" and $row["actual_opening_date"] != NULL)
    {
		$sheet->write($row_index, 13, $row["actual_opening_date"], $f_number); 
    }
    else
    {
        $sheet->write($row_index, 13, $row["project_state_text"], $f_normal);
		
    }

	
    $sheet->write($row_index, $num_of_milestones+15, substr($row["project_costtype_text"], 0, 1), $f_center);

    if($row["project_projectkind"] > 0)
    {
        $sheet->write($row_index, $num_of_milestones+16, $row["projectkind_code"], $f_center);
    }
    else
    {
        $sheet->write($row_index, $num_of_milestones+16, "", $f_center);
           
    }


    $sheet->write($row_index,  $num_of_milestones+17, $project_comments, $f_normal);
    

    $row_index++;
}

$xls->close(); 

?>