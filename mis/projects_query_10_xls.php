<?php
/********************************************************************

    projects_query_10_xls.php

    Generate Excel-File for Production Order Planning

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2010-11-03
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2010-11-03
    Version:        1.0.0

    Copyright (c) 2010, Swatch AG, All Rights Reserved.

*********************************************************************/


require_once "../include/frame.php";

check_access("can_perform_queries");

require_once "../include/xls/Writer.php";
require_once "include/get_functions.php";
require_once("projects_query_filter_strings.php");

/********************************************************************
    prepare Data Needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("projects_queries.php");
}


//check if filter is present
$query_name = "";
$print_query_filter = 0;
$sql = "select mis_query_name, mis_query_filter, mis_print_filter from mis_queries " .
	   "where mis_query_id = " . param("query_id");

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{

	$query_name = $row["mis_query_name"];

	if($row["mis_print_filter"] == 1)
	{
		$print_query_filter = 1;
	}

	$filters = array();
	$filters = unserialize($row["mis_query_filter"]);

	foreach($filters as $key=>$value)
	{
		$st =  $filters["ptst"]; // Project State
		$fosc = $filters["fst"]; //from order state
		$tosc = $filters["tst"]; // to order state

		$clt = $filters["clt"]; // Client Types
		$pl = $filters["pl"]; // product lines
		$pls = $filters["plsc"]; // product line subclasses
		$pk = $filters["pk"]; // product kinds
		$pt = $filters["pt"]; // project types
		$suc = $filters["ptsc"]; // POS Type Subclass
		$lpt = $filters["pct"]; // project cost type
		$gr = $filters["gr"]; // Geografical Regions
		$re = $filters["re"]; // Supplying Regions
		$cnt = $filters["co"]; // Countries
		$sc = $filters["rtc"]; // Retail Coordinators
		$lrtc = $filters["lrtc"]; // Local Project managers

		$fdy = $filters["fdy"]; // Closing Year From
		$fdm = $filters["fdm"]; // Closing Month From
		$tdy = $filters["tdy"]; // Closing Year To
		$tdm = $filters["tdm"]; // Closing Month To
		
		if(array_key_exists("areas", $filters))
		{
			$areas = $filters["areas"]; // Neighbourhood Locations
		
		}
		else
		{
			$areas = "";
		}

		if(array_key_exists("items", $filters))
		{
			$items = $filters["items"]; // Items
		
		}
		else
		{
			$items = "";
		}
	}
}
else
{
	redirect("projects_queries.php");
}


$header = "Production Order Planning - " . $query_name . " for: ";
$header .= " (" . date("d.m.Y G:i") . ")";


$product_lines = array();
$pl_h = substr($pl,0, strlen($pl)-1); // remove last comma
$pl_h = str_replace("-", ",", $pl_h);

if(strlen($pl_h) > 0)
{
	$sql = "select product_line_id, product_line_name ".
		   "from product_lines  " . 
		   "where (product_line_id IN (" . $pl_h . ")) " . 
		   "order by product_line_name";
}
else
{
	$sql = "select distinct product_line_id, product_line_name ".
		   "from projects " . 
		   "left join product_lines on product_line_id = project_product_line " .
		   "left join orders on order_id = project_order " . 
		   "where product_line_name <> '' and (order_archive_date is null or order_archive_date = '0000-00-00') " .  
		   "   order by product_line_name";
}
$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
	$product_lines[$row["product_line_id"]] = $row["product_line_name"];
	
}



/********************************************************************
    prepare Data
*********************************************************************/

$filter = "";

$clt = substr($clt,0, strlen($clt)-1); // remove last comma
$clt = str_replace("-", ",", $clt);
if($clt) // client type
{
    $filter =  " (address_client_type IN (" . $clt . "))";
	$_filter_strings["Client Types"] = get_filter_string("clt", $clt);
}

$pl = substr($pl,0, strlen($pl)-1); // remove last comma
$pl = str_replace("-", ",", $pl);
if($pl and $filter) // product line
{
	$filter .=  " and (project_product_line IN (" . $pl . "))";
	$_filter_strings["Product Lines"] = get_filter_string("pl", $pl);
}
elseif($pl) // product line
{
    $filter =  "  (project_product_line IN (" . $pl . "))";
	$_filter_strings["Product Lines"] = get_filter_string("pl", $pl);
}

$pls = substr($pls,0, strlen($pls)-1); // remove last comma
$pls = str_replace("-", ",", $pls);
if($pls and $filter) // product line subclasses
{
	$filter .=  " and (project_product_line_subclass IN (" . $pls . "))";
	$_filter_strings["Product Line Subclasses"] = get_filter_string("pls", $pls);
}
elseif($pls) // product line
{
    $filter =  "  (project_product_line_subclass IN (" . $pls . "))";
	$_filter_strings["Product Line Subclasses"] = get_filter_string("pls", $pls);
}

$pk = substr($pk,0, strlen($pk)-1); // remove last comma
$pk = str_replace("-", ",", $pk);

if($pk and $filter) // product line
{
	$filter .=  " and (project_projectkind IN (" . $pk . "))";
	$_filter_strings["Project Kinds"] = get_filter_string("pk", $pk);
}
elseif($pk) // product kind
{
    $filter =  "  (project_projectkind IN (" . $pk . "))";
	$_filter_strings["Project Kinds"] = get_filter_string("pk", $pk);
}


$pt = substr($pt,0, strlen($pt)-1); // remove last comma
$pt = str_replace("-", ",", $pt);
if($pt and $filter) // project type
{
    $filter .=  " and (project_postype IN (" . $pt . "))";
	$_filter_strings["POS Types"] = get_filter_string("pt", $pt);
}
elseif($pt)
{
    $filter =  "  (project_postype IN (" . $pt . "))";
	$_filter_strings["POS Types"] = get_filter_string("pt", $pt);
}

$suc = substr($suc,0, strlen($suc)-1); // remove last comma
$suc = str_replace("-", ",", $suc);
if($suc and $filter) // Pos Type Sub classe
{
    $filter .=  " and (project_pos_subclass IN (" . $suc . "))";
	$_filter_strings["POS Type Subclasses"] = get_filter_string("suc", $suc);
}
elseif($suc)
{
    $filter =  "   (project_pos_subclass IN (" . $suc . "))";
	$_filter_strings["POS Type Subclasses"] = get_filter_string("suc", $suc);
}

$lpt = substr($lpt,0, strlen($lpt)-1); // remove last comma
$lpt = str_replace("-", ",", $lpt);
if($lpt and $filter) // project cost type
{
    $filter .=  " and (project_cost_type IN (" . $lpt . "))";
	$_filter_strings["Legal Types"] = get_filter_string("lpt", $lpt);
}
elseif($lpt)
{
   $filter =  "  (project_cost_type IN (" . $lpt . "))";
   $_filter_strings["Legal Types"] = get_filter_string("lpt", $lpt);
}


$gr = substr($gr,0, strlen($gr)-1); // remove last comma
$gr = str_replace("-", ",", $gr);
$re = substr($re,0, strlen($re)-1); // remove last comma
$re = str_replace("-", ",", $re);

if($filter) {

	if($gr and $re) 
	{
		$filter .=  " and (country_salesregion IN (" . $gr . ") OR country_region IN (" . $re . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
		$_filter_strings["Supplying Regions"] = get_filter_string("re", $re);
	}
	elseif($gr) // geografical regions
	{
		$filter .=  " and (country_salesregion IN (" . $gr . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
	}
	elseif($re) //supplying
	{
		$filter .=  " and (country_region IN (" . $re . "))";
		$_filter_strings["Supplying Regions"] = get_filter_string("re", $re);
	}
}
else
{
	if($gr and $re) 
	{
		$filter .=  "  (country_salesregion IN (" . $gr . ") OR country_region IN (" . $re . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
		$_filter_strings["Supplying Regions"] = get_filter_string("re", $re);
	}
	elseif($gr) // geografical regions
	{
		$filter .=  "  (country_salesregion IN (" . $gr . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
	}
	elseif($re) //supplying
	{
		$filter .=  "  (country_region IN (" . $re . "))";
		$_filter_strings["Supplying Regions"] = get_filter_string("re", $re);
	}
}

$cnt = substr($cnt,0, strlen($cnt)-1); // remove last comma
$cnt = str_replace("-", ",", $cnt);
if($cnt and $filter) // country
{
    $filter .=  " and (order_shop_address_country IN (" . $cnt . "))";
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);

}
elseif($cnt)
{
    $filter =  "  (order_shop_address_country IN (" . $cnt . "))";
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);
}


$sc = substr($sc,0, strlen($sc)-1); // remove last comma
$sc = str_replace("-", ",", $sc);
if($sc and $filter) // Retail Coordinator
{
    $filter .=  " and (project_retail_coordinator IN (" . $sc . "))";
	$_filter_strings["Project Manager"] = get_filter_string("sc", $sc);

}
elseif($sc)
{
    $filter =  "  (project_retail_coordinator IN (" . $sc . "))";
	$_filter_strings["Project Manager"] = get_filter_string("sc", $sc);
}

$lrtc = substr($lrtc,0, strlen($lrtc)-1); // remove last comma
$lrtc = str_replace("-", ",", $lrtc);
if($lrtc and $filter) // Retail Coordinator
{
    $filter .=  " and (project_local_retail_coordinator IN (" . $lrtc . "))";
	$_filter_strings["Local Project Manager"] = get_filter_string("lrtc", $lrtc);

}
elseif($lrtc)
{
    $filter =  "   (project_local_retail_coordinator IN (" . $lrtc . "))";
	$_filter_strings["Local Project Manager"] = get_filter_string("lrtc", $lrtc);
}


if($filter and $fosc) // from order state
{
    $filter.=  " and order_actual_order_state_code >= '" . $fosc . "' ";
	$_filter_strings["From Order State Code"] = $fosc;
}
elseif($fosc)
{
	 $filter.=  "  order_actual_order_state_code >= '" . $fosc . "' ";
	 $_filter_strings["From Order State Code"] = $fosc;
}

if($filter and $tosc) // to order state
{
    $filter.=  " and order_actual_order_state_code <= '" . $tosc . "' ";
	$_filter_strings["To Order State Code"] = $tosc;
}
elseif($tosc)
{
	$filter.=  "  order_actual_order_state_code <= '" . $tosc . "' ";
	$_filter_strings["To Order State Code"] = $tosc;
}


//filter posareas get all matching orders
$areas = substr($areas,0, strlen($areas)-1); // remove last comma
$areas = str_replace("-", ",", $areas);
$filter2 = "";

if($areas)
{
	
	$order_ids = array();

	if($filter){$filter = "where " . $filter;}
	$sql = "select project_order " . 
		   "from projects " .
		   "left join orders on order_id = project_order " .
			"left join countries on country_id = order_shop_address_country " .
			 "left join addresses on address_id = order_client_address " .
			 "left join project_states on project_state_id = project_state " .
		   "left join project_costs on project_cost_order = project_order " .
		   $filter;
	
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$sql_a = "select DISTINCT posorder_order " . 
				 "from posorders " . 
				 "left join posareas on posorder_posaddress = posarea_posaddress " .
				 "where posorder_order = " . $row["project_order"] . 
				 " and posarea_area IN (" . $areas . ")";

		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
			$order_ids[$row["project_order"]] = $row["project_order"];
		}

		$sql_a = "select DISTINCT posorder_order " . 
				 "from posorderspipeline " . 
				 "left join posareaspipeline on posorder_posaddress = posarea_posaddress " .
				 "where posorder_order = " . $row["project_order"] . 
				 " and posarea_area IN (" . $areas . ")";

		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
			$order_ids[$row["project_order"]] = $row["project_order"];
		}
	
	}

	if(count($order_ids) > 0)
	{
		$filter2 =  " and project_order IN (" . implode ( ',' , $order_ids ) . ") ";
		
	}
	$_filter_strings["Neighbourhood Locations"] = get_filter_string("areas", $areas);
}


//items
$items = substr($items,0, strlen($items)-1); // remove last comma
$items = str_replace("-", ",", $items);

$item_filter = "";
if($items)
{
	$item_filter = " and item_id IN (" . $items . ") ";
}



/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$filename = "production_order_planning_" . date("Ymd") . ".xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename); 
$xls->setVersion(8);
$sheet =& $xls->addWorksheet("Summary");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

//formats
$header_row =& $xls->addFormat();
$header_row->setSize(10);
$header_row->setAlign('left');
$header_row->setBold();


$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);


$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);


$f_used =& $xls->addFormat();
$f_used->setSize(8);
$f_used->setBorder(1);
$f_used->setPattern(2);
$f_used->setBgColor('yellow');


$f_vertical =& $xls->addFormat();
$f_vertical->setSize(9);
$f_vertical->setAlign('left');
$f_vertical->setBorder(1);
$f_vertical->setTextRotation(270);



/********************************************************************
    write all data summary
*********************************************************************/
$sheet->write(0, 0, $header, $header_row);
$row_index = 2;

if($print_query_filter == 1)
{
	foreach($_filter_strings as $key=>$value)
	{
		 $sheet->write($row_index, 0,$key . ": " . $value);
		 $row_index++;
	}
	$row_index++;
}

$row_index_pl_header = $row_index;


$col_index = 0;


$product_lines_with_items = array();

foreach($product_lines as $id=>$name)
{
	
	$show_header = 1;
	$has_items = 0;


	//itemlist
	$itemrowpositions = array();
	$itemtotals = array();
	$category = '';

	$sql_i = 'SELECT DISTINCT item_category_id, item_category_name, item_id, item_code, item_category ' . 
			 'from categories ' .
			 'left join category_items on category_item_category = category_id ' . 
			 'left join items on item_id = category_item_item ' . 
			 'left join item_categories on item_category_id = item_category ' .
             'where item_visible_in_production_order = 1 ' . 
		     $item_filter .
		     ' and item_active = 1 and category_product_line = ' . $id . 
             ' order by item_category_sortorder, item_code ';
             //' where item_visible_in_production_order = 1 and item_active = 1 ' . $item_filter .



	$res_i = mysql_query($sql_i) or dberror($sql_i);
	while ($row_i = mysql_fetch_assoc($res_i))
	{
		
		if($show_header == 1)
		{
		
			$sheet->write($row_index, $col_index, $name, $header_row);
			$row_index++;
			$row_index++;
			$show_header = 0;
		}
		
		if($category != $row_i["item_category_id"])
		{
			$category = $row_i["item_category_id"];
			$sheet->write($row_index, $col_index, $row_i['item_category_name'], $header_row);
			$row_index++;

		}
		
		$sheet->write($row_index, $col_index, '   ' . $row_i['item_code'], $f_normal);
		$sheet->setColumn($col_index, $col_index, 20);

		$itemrowpositions[$row_i['item_id']] = $row_index;
		$itemtotals[$row_i['item_id']] = 0;
		$row_index++;

		$has_items = 1;

	
	}
	
	if($has_items == 1)
	{
		$product_lines_with_items[$name] = $name;

		$filter_tmp = $filter;
		if($filter and substr($filter, 0, 4 !=' and')){$filter_tmp= ' and ' . $filter;}

		$sql_p = "select order_id, order_shop_address_company, project_real_opening_date, " . 
			     "order_number, country_name, project_product_line ".
				 "from projects " . 
				 "left join orders on order_id = project_order " .
				 "left join countries on country_id = order_shop_address_country " . 
				 "left join project_costs on project_cost_order = project_order " .
				 "left join addresses on address_id = order_client_address " . 
				 " where project_product_line = " . $id . 
				 " and order_actual_order_state_code < '820' " . 
				 " and (order_archive_date is null or order_archive_date = '0000-00-00') " .
				 " and (project_actual_opening_date is null or project_actual_opening_date = '0000-00-00') " .
				 $filter_tmp . 
				 "   order by project_real_opening_date, order_number";



		$res = mysql_query($sql_p) or dberror($sql_p);
		while ($row = mysql_fetch_assoc($res))
		{
			
			foreach($itemrowpositions as $item_id=>$rowposition) {
				
				$sql_s = 'select sum(order_item_quantity) as amount ' . 
						 'from order_items ' . 
						 'where order_item_item = "' . $item_id . '" ' .
						 ' and order_item_order = "' . $row['order_id'] . '"';

				$res_s = mysql_query($sql_s) or dberror($sql_s);
				if($row_s = mysql_fetch_assoc($res_s))
				{
					$itemtotals[$item_id] = $itemtotals[$item_id] + $row_s['amount'];
				
				}
				
			}
		}


		foreach($itemrowpositions as $item_id=>$rowposition) 
		{
			$sheet->write($rowposition, $col_index+1, $itemtotals[$item_id], $f_number);
		}


		$col_index = $col_index+3;
		$row_index = $row_index_pl_header;
	}
	
}


/********************************************************************
    write all data detail for product lines
*********************************************************************/

$sheetnumber = 2;
foreach($product_lines as $id=>$name)
{
	
	if(in_array($name, $product_lines_with_items))
	{
	
		$sheet =& $xls->addWorksheet($name);
		$sheet->setInputEncoding("UTF-8");
		$header = "Production Order Planning - " . $query_name . " for: ";
		$header .= $name;

		
		$header .= " (" . date("d.m.Y G:i") . ")";

		$xls->activesheet = $sheetnumber;
		$sheet->write(0, 0, $header, $header_row);
		

		$row_index = 3;
		$col_index = 2;


		//itemlist
		$itemrowpositions = array();
		$itemtotals = array();
		$row_index2 = 7;
		$col_index2 = 0;
		$category = '';

		$sql_i = 'SELECT DISTINCT item_category_id, item_category_name, item_id, item_code, item_category ' . 
				 'from categories ' .
				 'left join category_items on category_item_category = category_id ' . 
				 'left join items on item_id = category_item_item ' . 
				 'left join item_categories on item_category_id = item_category ' .
				 'where item_visible_in_production_order = 1 ' . 
				 ' and item_active = 1 and category_product_line = ' . $id .
				 $item_filter .
				 ' order by item_category_sortorder, item_code ';


		
		$res_i = mysql_query($sql_i) or dberror($sql_i);
		while ($row_i = mysql_fetch_assoc($res_i))
		{
			if($category != $row_i["item_category_id"])
			{
				$category = $row_i["item_category_id"];
				$sheet->write($row_index2, $col_index2, $row_i['item_category_name'], $header_row);
				$row_index2++;

			}
			
			$sheet->write($row_index2, $col_index2, '   ' . $row_i['item_code'], $f_normal);

			$itemrowpositions[$row_i['item_id']] = $row_index2;
			$itemtotals[$row_i['item_id']] = 0;
			$row_index2++;

		
		}


	    $filter_tmp = $filter;
		if($filter and substr($filter, 0, 4 !=' and')){$filter_tmp= ' and ' . $filter;}

		$sql_p = "select order_id, order_shop_address_company, order_number, " . 
				 "project_real_opening_date, order_number, country_name, project_product_line, ".
				 "project_state_text " . 
				 "from projects " . 
				 "left join orders on order_id = project_order " .
				 "left join countries on country_id = order_shop_address_country " .
				 "left join addresses on address_id = order_client_address " .
				 "left join project_states on project_state_id = project_state " .
				 "left join project_costs on project_cost_order = project_order " .
				 "where project_product_line = " . $id . 
				 " and order_actual_order_state_code < '820' " . 
				 " and (order_archive_date is null or order_archive_date = '0000-00-00') " .
				 " and (project_actual_opening_date is null or project_actual_opening_date = '0000-00-00') " .
				 $filter_tmp .
				 $filter2 . 
				 "   order by project_state_id, project_real_opening_date, order_number";
		
		$columnnumber = 1;

		$res = mysql_query($sql_p) or dberror($sql_p);
		while ($row = mysql_fetch_assoc($res))
		{
			
			$order_date = substr($row['project_real_opening_date'], 8, 2) . '.' . substr($row['project_real_opening_date'], 5, 2) . '.' . substr($row['project_real_opening_date'], 0,4);
			$sheet->write($row_index-1, $col_index, $columnnumber, $f_normal);
			$sheet->write($row_index, $col_index, $order_date, $f_vertical);
			$sheet->write($row_index+1, $col_index, $row['order_shop_address_company'], $f_vertical);
			$sheet->write($row_index+2, $col_index, $row['country_name'], $f_vertical);
			$sheet->write($row_index+3, $col_index, $row['order_number'], $f_vertical);
			$sheet->write($row_index+4, $col_index, $row['project_state_text'], $f_vertical);


			

			$sheet->setRow(3, 60);
			$sheet->setRow(4, 250);
			$sheet->setRow(5, 100);
			$sheet->setRow(6, 60);
			$sheet->setRow(7, 60);

			$sheet->setColumn($col_index, $col_index, 3);
			$sheet->setColumn(0, 0, 20);


			foreach($itemrowpositions as $item_id=>$rowposition) {
				
				$sql_s = 'select sum(order_item_quantity) as amount ' . 
						 'from order_items ' . 
						 'where order_item_item = "' . $item_id . '" ' .
						 ' and order_item_order = "' . $row['order_id'] . '"';

				$res_s = mysql_query($sql_s) or dberror($sql_s);
				if($row_s = mysql_fetch_assoc($res_s))
				{
					$sheet->write($rowposition, $col_index, $row_s['amount'], $f_normal);
					$itemtotals[$item_id] = $itemtotals[$item_id] + $row_s['amount'];
				
				}
				
			}

			$col_index++;
			$columnnumber++;

		}

		foreach($itemrowpositions as $item_id=>$rowposition) 
		{
			$sheet->write($rowposition, 1, $itemtotals[$item_id], $f_number);
		}

		


		$sheetnumber++;
	}
}


$xls->close(); 

?>
