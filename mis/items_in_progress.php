<?php
/********************************************************************

    items_in_progress.php

    Show items beeing in progress

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-10-26
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-10-26
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/calculate_functions.php";

check_access("can_perform_queries");



/********************************************************************
    prepare all data needed
*********************************************************************/

$product_line = 0;
$category = 0;
$item = 0;
$order_state_code = 0;
$filter = "";
$mode = "";

if(isset($_REQUEST["product_line_id"]))
{
    if($_REQUEST["product_line_id"])
    {
        $product_line = $_REQUEST["product_line_id"];
        $mode = "1";
    }
    else
    {
        $mode = "0";
    }
}

if(isset($_REQUEST["category_id"]))
{
    if($_REQUEST["category_id"])
    {
        $category = $_REQUEST["category_id"];
        $mode.= "1";
    }
    else
    {
        $mode.= "0";
    }
}


if(isset($_REQUEST["item_id"]))
{
    if($_REQUEST["item_id"])
    {
        $item = $_REQUEST["item_id"];
        $mode.= "1";
    }
    else
    {
        $mode.= "0";
    }
}

if(isset($_REQUEST["order_state_code"]))
{
    if($_REQUEST["order_state_code"])
    {
        $order_state_code = $_REQUEST["order_state_code"];
        $mode.= "1";
    }
    else
    {
        $mode.= "0";
    }
}

// create sql for listboxes
$sql_product_lines = "select product_line_id, product_line_name ".
                 "from product_lines order by product_line_name";

$sql_categories = "select category_id, category_name ".
                  "from categories " .
                  "where (category_not_in_use = 0 " .
                  "   or category_not_in_use is null) " . 
                  "   and category_product_line = " . $product_line .
                  " order by category_name";

$sql_items = "select item_id, concat(item_code, ' ', item_name) as item " .
             "from category_items " .
             "left join items on item_id = category_item_item " .
             "where category_item_category = " . $category .
             "   and item_active = 1 " .
             "order by item_code";


$sql_order_states = "select distinct order_state_code, order_state_code " .
                    "from order_states " .
                    "where order_state_code > 619 " .
                    "   and order_state_code < 800 " .
                    "order by order_state_code";

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new form("countries", "delivered_by_country");

$form->add_list("product_line_id", "Product Line", $sql_product_lines, SUBMIT);
$form->add_list("category_id", "Category", $sql_categories, SUBMIT);
$form->add_list("item_id", "Item", $sql_items, 0);

$form->add_list("order_state_code", "Status", $sql_order_states, 0);

//$form->add_checkbox("all_countries", "Include all Countries");

$form->add_comment(" \n");
$form->add_button("execute", "Execute Query");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Process buttons
*********************************************************************/ 

if ($form->button("execute"))
{
    if(substr($mode,0,1) == 1)
    {
        $filter = "product_line_id = " . $product_line;
    }
    if(substr($mode,1,1) == 1)
    {
        if($filter)
        {
            $filter.= " and category_id = " . $category;
        }
        else
        {
            $filter = "category_id = " . $category;
        }
    }
    if(substr($mode,2,1) == 1)
    {
        if($filter)
        {
            $filter.= " and item_id = " . $item;
        }
        else
        {
            $filter = "item_id = " . $item;
        }
    }
    if(substr($mode,3,1) == 1)
    {
        if($filter)
        {
            $filter.= " and order_actual_order_state_code = " . $order_state_code;
        }
        else
        {
            $filter = "order_actual_order_state_code = " . $order_state_code;
        }
    }
}


/********************************************************************
    Create List for Items
*********************************************************************/ 
$list_filter = "where order_item_type <= " . ITEM_TYPE_SPECIAL . 
       "   and order_item_not_in_budget <> 1 " .
       "   and order_actual_order_state_code >=620 and order_actual_order_state_code < 800 ";

if($filter)
{
    $list_filter = $list_filter . " and " . $filter;
}

$sql = "select order_actual_order_state_code, item_code, " .
       "count(order_item_quantity) as amount " .
       "from order_items " .
       "left join items on item_id = order_item_item " .
       "left join category_items on category_item_item = item_id " .
       "left join categories on category_item_category = category_id " .
       "left join product_lines on product_line_id = category_product_line " .
       "left join orders on order_id = order_item_order " .
       $list_filter . 
       "   group by order_actual_order_state_code, item_code ";



$list = new ListView($sql);

$list->set_title("Items");
$list->set_entity("order_item");
//$list->set_filter($list_filter);
$list->set_order("order_actual_order_state_code, item_code");
//$list->set_group("order_actual_order_state_code, item_code");

$list->add_column("order_actual_order_state_code", "Status");
$list->add_column("item_code", "Item Code");
$list->add_column("amount", "Quantity");

$list->populate();
$list->process();



/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page("queries");

require "include/mis_page_actions.php";

$page->header();
$page->title("Items in Progress");

$form->render();

if($filter)
{
    $list->render();
}

$page->footer();

?>