<?php
/********************************************************************

    projects_query_7_xls.php

    Generate Excel File for CMS Overview of Projects

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-13
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-13
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_perform_queries");

require_once "../include/xls/Writer.php";
require_once("projects_query_filter_strings.php");

/********************************************************************
    prepare Data Needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("projects_queries.php");
}


//check if filter is present
$query_name = "";
$print_query_filter = 0;
$sql = "select mis_query_name, mis_query_filter, mis_print_filter from mis_queries " .
	   "where mis_query_id = " . param("query_id");

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$query_name = $row["mis_query_name"];
	if($row["mis_print_filter"] == 1)
	{
		$print_query_filter = 1;
	}

	$filters = array();
	$filters = unserialize($row["mis_query_filter"]);

	foreach($filters as $key=>$value)
	{
		$st =  $filters["ptst"]; // Project State
		$fosc = $filters["fst"]; //from order state
		$tosc = $filters["tst"]; // to order state

		$clt = $filters["clt"]; // Client Types
		$pl = $filters["pl"]; // product lines
		$pls = $filters["plsc"]; // product line subclasses
		$pk = $filters["pk"]; // product kinds
		$pt = $filters["pt"]; // project types
		$suc = $filters["ptsc"]; // POS Type Subclass
		$lpt = $filters["pct"]; // project cost type
		$gr = $filters["gr"]; // Geografical Regions
		$re = $filters["re"]; // Supplying Regions
		$cnt = $filters["co"]; // Countries
		$sc = $filters["rtc"]; // Retail Coordinators
		$lrtc = $filters["lrtc"]; // Local Project managers

		$fdy = $filters["fdy"]; // Opening Year From
		$fdm = $filters["fdm"]; // Opening Month From
		$tdy = $filters["tdy"]; // Opening Year To
		$tdm = $filters["tdm"]; // Opening Month To

		$fdy1 = $filters["fdy1"]; // Closing Year From
		$fdm1 = $filters["fdm1"]; // Closing Month From
		$tdy1 = $filters["tdy1"]; // Closing Year To
		$tdm1 = $filters["tdm1"]; // Closing Month To

		if(array_key_exists("areas", $filters))
		{
			$areas = $filters["areas"]; // Neighbourhood Locations
		
		}
		else
		{
			$areas = "";
		}

	}
}
else
{
	redirect("projects_queries.php");
}



$states = array(1=>"in process (steps 100-730)", 2=>"in deleivery (steps 740-750)", 3=>"operating (steps 800-820)", 4=>"cancelled (step 900)");


$header = $query_name;;


$header = "CMS Overview: " . $header . " (" . date("d.m.Y G:i") . ")";

/********************************************************************
    prepare Data
*********************************************************************/
$filter = "";

$clt = substr($clt,0, strlen($clt)-1); // remove last comma
$clt = str_replace("-", ",", $clt);
if($clt) // client type
{
    $filter =  " where (address_client_type IN (" . $clt . "))";
	$_filter_strings["Client Types"] = get_filter_string("clt", $clt);
}

$pl = substr($pl,0, strlen($pl)-1); // remove last comma
$pl = str_replace("-", ",", $pl);
if($pl and $filter) // product line
{
	$filter .=  " and (project_product_line IN (" . $pl . "))";
	$_filter_strings["Product Lines"] = get_filter_string("pl", $pl);
}
elseif($pl) // product line
{
    $filter =  " where (project_product_line IN (" . $pl . "))";
	$_filter_strings["Product Lines"] = get_filter_string("pl", $pl);
}

$pls = substr($pls,0, strlen($pls)-1); // remove last comma
$pls = str_replace("-", ",", $pls);
if($pls and $filter) // product line subclasses
{
    $filter .=  " and project_product_line_subclass IN (" . $pls . ")";
	$_filter_strings["Product Line Subclasses"] = get_filter_string("pls", $pls);
}
elseif($pls)
{
    $filter =  " where project_product_line_subclass IN (" . $pls . ")";
	$_filter_strings["Product Line Subclasses"] = get_filter_string("pls", $pls);
}

$pk = substr($pk,0, strlen($pk)-1); // remove last comma
$pk = str_replace("-", ",", $pk);

if($pk and $filter) // product line
{
	$filter .=  " and (project_projectkind IN (" . $pk . "))";
	$_filter_strings["Project Kinds"] = get_filter_string("pk", $pk);
}
elseif($pk) // product kind
{
    $filter =  " where (project_projectkind IN (" . $pk . "))";
	$_filter_strings["Project Kinds"] = get_filter_string("pk", $pk);
}

$pt = substr($pt,0, strlen($pt)-1); // remove last comma
$pt = str_replace("-", ",", $pt);
if($pt and $filter) // project type
{
    $filter .=  " and (project_postype IN (" . $pt . "))";
	$_filter_strings["POS Types"] = get_filter_string("pt", $pt);
}
elseif($pt)
{
    $filter =  " where (project_postype IN (" . $pt . "))";
	$_filter_strings["POS Types"] = get_filter_string("pt", $pt);
}

$suc = substr($suc,0, strlen($suc)-1); // remove last comma
$suc = str_replace("-", ",", $suc);
if($suc and $filter) // POS Type Subclass
{
    $filter .=  " and (project_pos_subclass IN (" . $suc . "))";
	$_filter_strings["POS Type Subclasses"] = get_filter_string("suc", $suc);
}
elseif($suc)
{
   $filter =  " where (project_pos_subclass IN (" . $suc . "))";
   $_filter_strings["POS Type Subclasses"] = get_filter_string("suc", $suc);
}

$lpt = substr($lpt,0, strlen($lpt)-1); // remove last comma
$lpt = str_replace("-", ",", $lpt);
if($lpt and $filter) // project cost type
{
    $filter .=  " and (project_cost_type IN (" . $lpt . "))";
	$_filter_strings["Legal Types"] = get_filter_string("lpt", $lpt);
}
elseif($lpt)
{
   $filter =  " where (project_cost_type IN (" . $lpt . "))";
   $_filter_strings["Legal Types"] = get_filter_string("lpt", $lpt);
}




$gr = substr($gr,0, strlen($gr)-1); // remove last comma
$gr = str_replace("-", ",", $gr);
$re = substr($re,0, strlen($re)-1); // remove last comma
$re = str_replace("-", ",", $re);

if($filter) {

	if($gr and $re) 
	{
		$filter .=  " and (country_salesregion IN (" . $gr . ") OR country_region IN (" . $re . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
		$_filter_strings["Supplying Regions"] = get_filter_string("re", $re);
	}
	elseif($gr) // geografical regions
	{
		$filter .=  " and (country_salesregion IN (" . $gr . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
	}
	elseif($re) //supplying
	{
		$filter .=  " and (country_region IN (" . $re . "))";
		$_filter_strings["Supplying Regions"] = get_filter_string("re", $re);
	}
}
else
{
	if($gr and $re) 
	{
		$filter .=  " where (country_salesregion IN (" . $gr . ") OR country_region IN (" . $re . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
		$_filter_strings["Supplying Regions"] = get_filter_string("re", $re);
	}
	elseif($gr) // geografical regions
	{
		$filter .=  " where (country_salesregion IN (" . $gr . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
	}

	elseif($re) //supplying
	{
		$filter .=  " where (country_region IN (" . $re . "))";
		$_filter_strings["Supplying Regions"] = get_filter_string("re", $re);
	}
}

$cnt = substr($cnt,0, strlen($cnt)-1); // remove last comma
$cnt = str_replace("-", ",", $cnt);
if($cnt and $filter) // country
{
    $filter .=  " and (order_shop_address_country IN (" . $cnt . "))";
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);

}
elseif($cnt)
{
    $filter =  " where (order_shop_address_country IN (" . $cnt . "))";
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);
}


$sc = substr($sc,0, strlen($sc)-1); // remove last comma
$sc = str_replace("-", ",", $sc);
if($sc and $filter) // Retail Coordinator
{
    $filter .=  " and (project_retail_coordinator IN (" . $sc . "))";
	$_filter_strings["Project Manager"] = get_filter_string("sc", $sc);

}
elseif($sc)
{
    $filter =  " where (project_retail_coordinator IN (" . $sc . "))";
	$_filter_strings["Project Manager"] = get_filter_string("sc", $sc);
}

$lrtc = substr($lrtc,0, strlen($lrtc)-1); // remove last comma
$lrtc = str_replace("-", ",", $lrtc);
if($lrtc and $filter) // Retail Coordinator
{
    $filter .=  " and (project_local_retail_coordinator IN (" . $lrtc . "))";
	$_filter_strings["Local Project Manager"] = get_filter_string("lrtc", $lrtc);

}
elseif($lrtc)
{
    $filter =  " where (project_local_retail_coordinator IN (" . $lrtc . "))";
	$_filter_strings["Local Project Manager"] = get_filter_string("lrtc", $lrtc);
}


if($filter) // opened from
{
	if($fdy > 0)
	{
		$filter.=  " and YEAR(project_actual_opening_date) >= " . $fdy;
		$_filter_strings["Opened from"] = $fdy;
	}
}
else
{
	if($fdy > 0)
	{
		$filter.=  " where YEAR(project_actual_opening_date) >= " . $fdy;
		$_filter_strings["Opened from"] = $fdy;
	}
}

if($filter) // opened to
{
	if($tdy > 0)
	{
		$filter.=  " and YEAR(project_actual_opening_date) <= " . $tdy;
		$_filter_strings["Opened to"] = $tdy;
	}
}
else
{
	if($tdy > 0)
	{
		$filter.=  " where YEAR(project_actual_opening_date) <= " . $tdy;
		$_filter_strings["Opened to"] = $tdy;
	}
}

if($filter) // closed from
{
	if($fdy1 > 0)
	{
		$filter.=  " and YEAR(project_shop_closingdate) >= " . $fdy1;
		$_filter_strings["Closed from"] = $fdy1;
	}
}
else
{
	if($fdy1 > 0)
	{
		$filter.=  " where YEAR(project_shop_closingdate) >= " . $fdy1;
		$_filter_strings["Closed from"] = $fdy1;
	}
}

if($filter) // closed to
{
	if($tdy1 > 0)
	{
		$filter.=  " and YEAR(project_shop_closingdate) <= " . $tdy1;
		$_filter_strings["Closed to"] = $tdy1;
	}
}
else
{
	if($tdy1 > 0)
	{
		$filter.=  " where YEAR(project_shop_closingdate) <= " . $tdy1;
		$_filter_strings["Closed to"] = $tdy1;
	}
}


//only open or closed shops
if($filter) // closed from year
{
    $filter.=  " and order_actual_order_state_code >= '800' and order_actual_order_state_code <= '820' ";
	$filter.=  " and project_actual_opening_date <> '0000-00-00' and project_actual_opening_date is not null ";
}
else
{
	$filter =  "where order_actual_order_state_code >= '800' and order_actual_order_state_code <= '820' ";
	$filter.=  " and project_actual_opening_date <> '0000-00-00' and project_actual_opening_date is not null ";
}



//filter posareas get all matching orders
$areas = substr($areas,0, strlen($areas)-1); // remove last comma
$areas = str_replace("-", ",", $areas);

if($areas)
{
	
	$order_ids = array();
	$sql = "select project_order " . 
	       "from projects " .
		   "left join orders on order_id = project_order " .
		   "left join project_costs on project_cost_order = project_order " .
		   "left join project_costtypes on project_costtype_id = project_cost_type " .
		   "left join projectkinds on projectkind_id = project_projectkind " . 
		   "left join addresses on address_id = order_client_address " .
		   "left join countries on country_id = order_shop_address_country " .
		   "left join possubclasses on possubclass_id = project_pos_subclass " .
		   $filter;
	
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$sql_a = "select DISTINCT posorder_order " . 
			     "from posorders " . 
			     "left join posareas on posorder_posaddress = posarea_posaddress " .
			     "where posorder_order = " . $row["project_order"] . 
			     " and posarea_area IN (" . $areas . ")";

		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
			$order_ids[$row["project_order"]] = $row["project_order"];
		}

		$sql_a = "select DISTINCT posorder_order " . 
			     "from posorderspipeline " . 
			     "left join posareaspipeline on posorder_posaddress = posarea_posaddress " .
			     "where posorder_order = " . $row["project_order"] . 
			     " and posarea_area IN (" . $areas . ")";

		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
			$order_ids[$row["project_order"]] = $row["project_order"];
		}
	}

	if(count($order_ids) > 0)
	{
	
		
		if($filter) 
		{
			$filter.=  " and project_order IN (" . implode ( ',' , $order_ids ) . ") ";
		}
		else
		{
			$filter =  " where project_order IN (" . implode ( ',' , $order_ids ) . ") ";
		}
	}
	$_filter_strings["Neighbourhood Locations"] = get_filter_string("areas", $areas);
}

$sql_d = "select project_id, project_number, project_projectkind, project_order, " .
       "DATE_FORMAT(project_actual_opening_date, '%d.%m.%Y') as openingdate, " .
	   "DATE_FORMAT(project_shop_closingdate, '%d.%m.%Y') as closingdate, " .
       "project_cost_type, project_costtype_text, projectkind_code, project_cost_sqms, order_actual_order_state_code, " .
       "country_name, project_planned_amount_current_year, order_archive_date, " .
       "project_actual_opening_date, project_shop_closingdate, order_cancelled, " .
       "order_shop_address_place, order_shop_address_company, order_shop_address_country, country_region, " .
	   "possubclass_name " . 
       "from projects " .
       "left join orders on order_id = project_order " .
       "left join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join projectkinds on projectkind_id = project_projectkind " . 
       "left join addresses on address_id = order_client_address " .
       "left join countries on country_id = order_shop_address_country " .
	   "left join possubclasses on possubclass_id = project_pos_subclass " .
       $filter . 
       " order by country_name, order_shop_address_place, project_number";

/********************************************************************
    prepare Excel Sheet
*********************************************************************/

$filename = "cms_overview_" . date("Ymd") . ".xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename); 
$xls->setVersion(8);
$sheet =& $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();


//formats
$header_row =& $xls->addFormat();
$header_row->setSize(10);
$header_row->setAlign('left');
$header_row->setBold();


$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);


$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_notrealized =& $xls->addFormat();
$f_notrealized->setSize(8);
$f_notrealized->setAlign('right');
$f_notrealized->setBorder(1);
$f_notrealized->setPattern(2);
$f_notrealized->setColor("white");
$f_notrealized->setBgColor('red');

$f_inprogress =& $xls->addFormat();
$f_inprogress->setSize(8);
$f_inprogress->setAlign('right');
$f_inprogress->setBorder(1);
$f_inprogress->setPattern(2);
$f_inprogress->setColor("white");
$f_inprogress->setBgColor('navy');

$f_open =& $xls->addFormat();
$f_open->setSize(8);
$f_open->setAlign('right');
$f_open->setBorder(1);
$f_open->setPattern(2);
$f_open->setColor("white");
$f_open->setBgColor('lime');


$f_delivery =& $xls->addFormat();
$f_delivery->setSize(8);
$f_delivery->setAlign('right');
$f_delivery->setBorder(1);
$f_delivery->setPattern(2);
$f_delivery->setColor("white");
$f_delivery->setBgColor('yellow');

$f_closed =& $xls->addFormat();
$f_closed->setSize(8);
$f_closed->setAlign('right');
$f_closed->setBorder(1);
$f_closed->setPattern(2);
$f_closed->setColor("white");
$f_closed->setBgColor('red');


$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);


$f_used =& $xls->addFormat();
$f_used->setSize(8);
$f_used->setBorder(1);
$f_used->setPattern(2);
$f_used->setBgColor('grey');




//captions
$captions = array();
$captions[] = "operating";
$captions[] = "closed";
$sheet->setColumn(0, 1, 3);

$captions[] = "Country";
$sheet->setColumn(2, 2, 16);

$captions[] = "Project Name";
$sheet->setColumn(3, 3, 50);
$captions[] = "Project Number";
$captions[] = "Project Sheet Planned Amount";
$captions[] = "Opened/Closed";

$sheet->setColumn(7, 7, 4);
$captions[] = "C=Corporate, F=Franchise";

$sheet->setColumn(8, 8, 10);
$captions[] = "N=New, R=Renovation, TR=Take Over/Renovation, T=Take Over, L=Lease Renewal";

$sheet->setColumn(9, 9, 12);
$captions[] = "POS Type Subclass";

$sheet->setColumn(10, 10, 4);
$captions[] = "Sales Surface sqm";



//get costmonitoring groups
$sql = "select * from costmonitoringgroups " .
       "order by costmonitoringgroup_code ";

$res = mysql_query($sql) or dberror($sql);

$costmonitoringgroups_ids = array();
$num_of_cms_groups = 0;
while ($row = mysql_fetch_assoc($res))
{
    $captions[] = $row["costmonitoringgroup_text"];
    $costmonitoringgroups_ids[] = $row["costmonitoringgroup_id"];
    $num_of_cms_groups++;
}

$sheet->setColumn(11, $num_of_cms_groups+10, 2);

$captions[] = "Total for HQ-Supplied Items in CHF";
$captions[] = "Price per sqm in CHF";
$captions[] = "Freight Charges in CHF";
$captions[] = "Freight Charges in %";
$captions[] = "HQ-Supplied Items + Freight Charges in CHF";
$captions[] = "Price per sqm in CHF";
$sheet->setColumn($num_of_cms_groups+11, $num_of_cms_groups+16, 8);


//get local work groups groups
$sql = "select * from lwgroups " .
       " where lwgroup_group < 8 " . 
       "order by lwgroup_code";


$res = mysql_query($sql) or dberror($sql);

$num_of_lwgroups = 0;
$lwgroups = array();
while ($row = mysql_fetch_assoc($res))
{
    $captions[] = $row["lwgroup_text"];
    $lwgroups[] = $row["lwgroup_code"];
    $num_of_lwgroups++;
}

$sheet->setColumn($num_of_cms_groups+16, $num_of_cms_groups+16+$num_of_lwgroups, 2);

$captions[] = "Total Local Works in CHF";
$captions[] = "Local Works in %";
$captions[] = "Price per sqm of Local Works";
$captions[] = "Grand Total in CHF";
$captions[] = "Price per sqm of Grand Total";





/********************************************************************
    write all data
*********************************************************************/
$sheet->write(0, 0, $header, $header_row);

$row_index = 2;

if($print_query_filter == 1)
{
	foreach($_filter_strings as $key=>$value)
	{
		 $sheet->write($row_index, 0,$key . ": " . $value);
		 $row_index++;
	}
	$row_index++;
}


/********************************************************************
    write all captions
*********************************************************************/
$sheet->writeRow($row_index, 0, $captions, $f_caption);
$sheet->setRow($row_index, 110);
$row_index++;

$cell_index = 2;

$c01 = 0;
$c02 = 0;
$c03 = 0;
$c04 = 0;

$res = mysql_query($sql_d) or dberror($sql_d);
while ($row = mysql_fetch_assoc($res))
{
    //get local work groups used
    $localworkgroups_used = array();
    $sql_c = "select lwoffergroup_code, " .
             "sum(lwofferposition_numofunits_billed*lwofferposition_price_billed) as totel_billed " . 
             "from lwoffers " .
             "left join lwoffergroups on lwoffergroup_offer = lwoffer_id " . 
             "left join lwofferpositions on lwofferposition_lwoffergroup = lwoffergroup_id " . 
             " where lwoffergroup_group < 8 " .
             " and lwoffer_order = " . $row["project_order"] . 
             " group by lwoffergroup_code " . 
             "order by lwoffergroup_code";

    $res_c = mysql_query($sql_c) or dberror($sql_c);

    while ($row_c = mysql_fetch_assoc($res_c))
    {
        $localworkgroups_used[$row_c["lwoffergroup_code"]] = $row_c["totel_billed"];
    }


    //get cost monitoring groups used
    $costmonitoringgroups_used = array();
    $sql_c = "select DISTINCT order_items_costmonitoring_group " .
             "from order_items " .
             "where order_item_order = " . $row["project_order"];

    $res_c = mysql_query($sql_c) or dberror($sql_c);

    while ($row_c = mysql_fetch_assoc($res_c))
    {
        $costmonitoringgroups_used[] = $row_c["order_items_costmonitoring_group"];
    }

    
    //get cost for project_costgroups
    $costgroup_cost = array();

    /*
	$sql_c = "select order_item_cost_group, " .
             "sum(order_item_quantity*order_item_system_price) as total " .
             "from order_items " . 
             "where order_item_order = " . $row["project_order"] . 
             " and (order_item_reinvoiced <> '0000-00-00' " .
             "     or order_item_reinvoiced is not Null) " . 
             " group by order_item_cost_group";
	*/

	$sql_c = "select order_item_cost_group, " .
             "sum(order_item_quantity*order_item_system_price) as total " .
             "from order_items " . 
             "where order_item_order = " . $row["project_order"] . 
             " group by order_item_cost_group";

    $res_c = mysql_query($sql_c) or dberror($sql_c);
    while ($row_c = mysql_fetch_assoc($res_c))
    {
        $costgroup_cost[$row_c["order_item_cost_group"]] =   $row_c["total"];
    }

    $shop_address = $row["order_shop_address_company"] . ", " . $row["order_shop_address_place"];


    //not realized
    /*
    if($row["order_cancelled"] == 1)
    {
        $c01++;
        $sheet->write($row_index, 0, $c01, $f_notrealized);
    }
    else
    {
        $sheet->write($row_index, 0, "", $f_normal);
    }

    
    //in process (steps 100-730)
    if($row["order_actual_order_state_code"] <= '730')
    {
        $c01++;
        $sheet->write($row_index, 0, $c01, $f_inprogress);
    }
    else
    {
        $sheet->write($row_index, 0, "", $f_normal);
    }
    
    
  
    //in delivery (steps 800-820)
    if($row["order_actual_order_state_code"] >= '740' and $row["order_actual_order_state_code"] <= '750')
    {
        $c02++;
        $sheet->write($row_index, 1, $c02, $f_delivery);
        $shop_closed = 1;
    }
    else
    {
        $sheet->write($row_index, 1, "", $f_normal);
        $shop_closed = 0;
        
    }

    //shop operating
    if(!$shop_closed and $row["order_actual_order_state_code"] <> '900')
    {
        if($row["project_actual_opening_date"] > '0000-00-00' and $row["project_actual_opening_date"] <= date("Y-m-d"))
        {
            $c03++;
            $sheet->write($row_index, 2, $c03, $f_open);
        }
        else
        {
            $sheet->write($row_index, 2, "", $f_normal);
        }
    }
    else
    {
        $sheet->write($row_index, 2, "", $f_normal);
    }
    */
    
    //operation (steps 800-820)
    if($row["project_shop_closingdate"] == "0000-00-00" or $row["project_shop_closingdate"] == NULL)
    {
		if($row["order_actual_order_state_code"] >= '800' and $row["order_actual_order_state_code"] <= '820')
		{
			$c01++;
			
			if($row["project_shop_closingdate"] and $row["project_shop_closingdate"] != "0000-00-00")
			{
				$sheet->write($row_index, 0, $c01, $f_notrealized);
			}
			else
			{
				$sheet->write($row_index, 0, $c01, $f_open);
			}
		}
	}
    else
    {
        $sheet->write($row_index, 0, "", $f_normal);
    }

	// shop closed
    if($row["project_shop_closingdate"] != "0000-00-00" and $row["project_shop_closingdate"])
    {
        $c02++;
        $sheet->write($row_index, 1, $c02, $f_closed);
        $shop_closed = 1;
    }
    else
    {
        $sheet->write($row_index, 1, "", $f_normal);
        $shop_closed = 0;
        
    }
    
    // in progress
    /*
    if($shop_closed != 1) // not closed
    {
        if($row["order_actual_order_state_code"] != '900' and (!$row["order_archive_date"] ))
        {
            $c04++;
            $sheet->write($row_index, 3, $c04, $f_inprogress);
        }
        else
        {
            $sheet->write($row_index, 3, "", $f_normal);
        }
    }
    else
    {
        $sheet->write($row_index, 3, "", $f_normal);
    }
    

    
    //cancelled (step 900)
    if($row["order_actual_order_state_code"] == '900')
    {
        $c04++;
        $sheet->write($row_index, 3, $c04, $f_notrealized);
    }
    else
    {
        $sheet->write($row_index, 3, "", $f_normal);
    }

	*/

    
    $sheet->write($row_index, $cell_index, $row["country_name"], $f_normal);
    $sheet->write($row_index, $cell_index+1, $shop_address, $f_normal);

    
    $sheet->write($row_index, $cell_index+2, $row["project_number"], $f_normal);
    
    if($row["project_planned_amount_current_year"] > 0)
    {
        $sheet->write($row_index, $cell_index+3, number_format($row["project_planned_amount_current_year"],2, ".", ""), $f_number);
    }
    else
    {
         $sheet->write($row_index, $cell_index+3, "", $f_number);
    }

    
	if($row["closingdate"] and $row["closingdate"] != "00.00.0000")
	{
		$sheet->write($row_index, $cell_index+4, $row["closingdate"], $f_closed);
	}
	elseif(!$row["openingdate"] or $row["openingdate"] == "00.00.0000")
	{
		$sheet->write($row_index, $cell_index+4, "", $f_number);
	}
	else
	{
		$sheet->write($row_index, $cell_index+4, $row["openingdate"], $f_open);
	}

    $sheet->write($row_index, $cell_index+5, substr($row["project_costtype_text"], 0, 1), $f_center);

    if($row["project_projectkind"] > 0)
    {
        $sheet->write($row_index, $cell_index+6, $row["projectkind_code"], $f_center);
    }
    else
    {
        $sheet->write($row_index, $cell_index+6, "", $f_center);
           
    }

	$sheet->write($row_index, $cell_index+7, $row["possubclass_name"], $f_normal);
        
	
	if($row["project_cost_sqms"] > 0)
    {
        $sheet->write($row_index, $cell_index+8, number_format($row["project_cost_sqms"], 0), $f_center);
    }
    else
    {
        $sheet->write($row_index, $cell_index+8, "", $f_center);
    }


    //Item Cost Groups

    $i = 1;
    foreach($costmonitoringgroups_ids as $key=>$value)
    {

        //$sheet->write($row_index, $cell_index+8+$i, $key, $f_used);
        

        if(in_array($value, $costmonitoringgroups_used))
        {
            $sheet->write($row_index, $cell_index+8+$i, "", $f_used);
        }
        else
        {
            $sheet->write($row_index, $cell_index+8+$i, "", $f_normal);
        }
        $i++;
    }
    
    //Total for HQ-Supplied Items
    $hq = 0;
    if(isset($costgroup_cost[2]) and $costgroup_cost[2] > 0)
    {
        $sheet->write($row_index, $cell_index+9+$num_of_cms_groups, number_format($costgroup_cost[2], 2, ".", ""), $f_number);
        $hq = $costgroup_cost[2];
    }
    else
    {
         $sheet->write($row_index, $cell_index+9+$num_of_cms_groups, "", $f_number);
    }

    //Cost of HQ-Items per sqm
    if($hq > 0 and $row["project_cost_sqms"] > 0)
    {
        $sheet->write($row_index, $cell_index+10+$num_of_cms_groups, number_format($hq/$row["project_cost_sqms"], 2, ".", ""), $f_number);
    }
    else
    {
        $sheet->write($row_index, $cell_index+10+$num_of_cms_groups, "", $f_number);
    }
    
    //Total for Freight Charges
    $fch = 0;
    if(isset($costgroup_cost[6]) and $costgroup_cost[6] > 0)
    {
        $sheet->write($row_index, $cell_index+11+$num_of_cms_groups, number_format($costgroup_cost[6], 2, ".", ""), $f_number);
        $fch = $costgroup_cost[6];
    }
    elseif(isset($costgroup_cost[6]))
    {
        $sheet->write($row_index, $cell_index+11+$num_of_cms_groups, "0.00", $f_number);
    }

    //Freight Charges in %
    if($fch > 0 and ($hq+$fch) > 0)
    {
        $sheet->write($row_index, $cell_index+12+$num_of_cms_groups, number_format(100*$fch/($hq+$fch), 2, ".", ""), $f_number);
    }
    elseif(isset($costgroup_cost[6]))
    {
        $sheet->write($row_index, $cell_index+12+$num_of_cms_groups, "0.00", $f_number);
    }


    //Total for HQ-Supplied Items and Freight Charges
    if($hq > 0)
    {
        $sheet->write($row_index, $cell_index+13+$num_of_cms_groups, number_format($hq + $fch, 2, ".", ""), $f_number);
    }
    else
    {
        $sheet->write($row_index, $cell_index+13+$num_of_cms_groups, "", $f_number);
    }

    //Cost of HQ-Items and Freight Charges per sqm
    if($hq > 0 and $row["project_cost_sqms"] > 0)
    {
        $sheet->write($row_index, $cell_index+14+$num_of_cms_groups, number_format(($hq+$fch)/$row["project_cost_sqms"], 2, ".", ""), $f_number);
    }
    else
    {
        $sheet->write($row_index, $cell_index+14+$num_of_cms_groups, "", $f_number);
    }


    //Cost Groups for Local Works
    $i=1;
    foreach($lwgroups as $key=>$value)
    {
        if(isset($localworkgroups_used[$value]))
        {
            if($localworkgroups_used[$value])
            {
                $sheet->write($row_index, $cell_index+14+$i+$num_of_cms_groups, "", $f_used);
            }
            else
            {
                $sheet->write($row_index, $cell_index+14+$num_of_cms_groups+$i, "", $f_normal);
            }
        }
        else
        {
            $sheet->write($row_index, $cell_index+14+$num_of_cms_groups+$i, "", $f_normal);
        }
        $i++;
    
    }

    //for($i=1;$i<=$num_of_lwgroups;$i++)
    //{
    //    $sheet->write($row_index, $cell_index+13+$num_of_cms_groups+$i, "", $f_normal);
    //}
    
    
    //Total for Local Works
    $tlw = 0;
    if(isset($costgroup_cost[3]) and $costgroup_cost[3] != 0) // Installation
    {
        $tlw = $costgroup_cost[3];
    }
	else
	{
	
	}

    if(isset($costgroup_cost[5]) and $costgroup_cost[5] != 0) //Import Duties
    {
         $tlw = $tlw + $costgroup_cost[5];
    }

    if(isset($costgroup_cost[7]) and $costgroup_cost[7] != 0) // local work
    {
        $tlw = $tlw + $costgroup_cost[7];
    }
    

    if($tlw != 0)
    {
        $sheet->write($row_index, $cell_index+15+$num_of_cms_groups+$num_of_lwgroups, number_format($tlw, 2, ".", ""), $f_number);
    }
    else
    {
        $sheet->write($row_index, $cell_index+15+$num_of_cms_groups+$num_of_lwgroups, $tlw, $f_number);
    }


    //Local Works in %
    if($tlw != 0 and ($hq+$fch+$tlw) > 0)
    {
        $sheet->write($row_index, $cell_index+16+$num_of_cms_groups+$num_of_lwgroups, number_format(100*$tlw/($hq+$fch+$tlw), 2, ".", ""), $f_number);
    }
    else
    {
        $sheet->write($row_index, $cell_index+16+$num_of_cms_groups+$num_of_lwgroups, "0.00", $f_number);
    
    }

	//Local Works per sqm
    if(($tlw) != 0 and $row["project_cost_sqms"] > 0)
    {
        $sheet->write($row_index, $cell_index+17+$num_of_cms_groups+$num_of_lwgroups, number_format(($tlw)/$row["project_cost_sqms"], 2, ".", ""), $f_number);
    }
    else
    {
        $sheet->write($row_index, $cell_index+17+$num_of_cms_groups+$num_of_lwgroups, "", $f_number);
    }


    //Grand Total
    if($hq+$fch+$tlw > 0)
    {
        $sheet->write($row_index, $cell_index+18+$num_of_cms_groups+$num_of_lwgroups, number_format(($hq+$fch+$tlw), 2, ".", ""), $f_number);
    }
    else
    {
        $sheet->write($row_index, $cell_index+18+$num_of_cms_groups+$num_of_lwgroups, "", $f_number);
    
    }

    //Grand Total per sqm
    if(($hq+$fch+$tlw) > 0 and $row["project_cost_sqms"] > 0)
    {
        $sheet->write($row_index, $cell_index+19+$num_of_cms_groups+$num_of_lwgroups, number_format(($hq+$fch+$tlw)/$row["project_cost_sqms"], 2, ".", ""), $f_number);
    }
    else
    {
        $sheet->write($row_index, $cell_index+19+$num_of_cms_groups+$num_of_lwgroups, "", $f_number);
    }

    $row_index++;
}

$xls->close(); 

?>