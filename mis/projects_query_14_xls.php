<?php
/********************************************************************

    pos_equipment_xls.php

    Print equipment of pos locations

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2012-05-18
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2012-05-18
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_perform_queries");

require_once "../include/phpexcel/PHPExcel.php";
require_once "include/get_functions.php";

/********************************************************************
    prepare all data needed
*********************************************************************/

if(!param("query_id"))
{
	redirect("projects_queries.php");
}

$sql = "select mis_query_filter from mis_queries " .
	   "where mis_query_id = " . param("query_id");

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{

	$filters = array();
	$filters = unserialize($row["mis_query_filter"]);

	foreach($filters as $key=>$value)
	{
		$pl = $filters["pl"]; // product lines
		$supp = $filters["supp"]; // suppliers
		

		if(array_key_exists("items", $filters))
		{
			$items = $filters["items"]; // Items
		
		}
		else
		{
			$items = "";
		}
	}
}
else
{
	redirect("projects_queries.php");
}

//items
$items = substr($items,0, strlen($items)-1); // remove last comma
$items = str_replace("-", ",", $items);

$item_filter = "";
$item_filter2 = "";
if($items)
{
	$item_filter = " and order_item_item IN (" . $items . ") ";
	$item_filter2 = " and item_id IN (" . $items . ") ";
}

if(!$item_filter)
{
	//suppliers
	$suppliers = substr($supp,0, strlen($supp)-1); // remove last comma
	$suppliers = str_replace("-", ",", $suppliers);

	if($suppliers)
	{
		$item_filter = " and supplier_address IN (" . $suppliers . ") ";
		$item_filter2 = " and supplier_address IN (" . $suppliers . ") ";
	}

	//product lines
	$pls = substr($pl,0, strlen($pl)-1); // remove last comma
	$pls = str_replace("-", ",", $pls);

	if($pls)
	{
		$item_filter = " and category_product_line IN (" . $pls . ") ";
		$item_filter2 = " and category_product_line IN (" . $pls . ") ";
	}
}

//get items in selection
$items = array();
$sql_o = "select item_id, item_code " .
		 "from items " . 
		 "left join suppliers on supplier_item = item_id " .
		 "left join category_items on category_item_item = item_id " . 
		 "left join categories on category_id = category_item_category " . 
		 "where item_type = 1 " . 
		 $item_filter2;


$res_o = mysql_query($sql_o) or dberror($sql_o);
while ($row_o = mysql_fetch_assoc($res_o))
{
	$items[$row_o["item_id"]] = $row_o["item_code"];
}

//get pos locations
$pos_locations = array();
$pos_location_projects = array();
$pos_locations_items = array();

$sql = "select posaddress_id, posaddress_name, country_name " .
       "from posaddresses " .
	   "inner join countries on country_id = posaddress_country " . 
	   " order by country_name, posaddress_name" ;
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	
	$latest_project = get_latest_pos_project($row["posaddress_id"]);
	
	if(count($latest_project) > 0)
	{
		//check if POS location has items
		$sql_o = "select order_item_item, item_code, order_item_quantity " .
			     "from order_items " . 
				 "left join items on item_id = order_item_item " .
			     "left join suppliers on supplier_item = item_id " .
			     "left join category_items on category_item_item = item_id " . 
			     "left join categories on category_id = category_item_category " . 
				 "where order_item_order = " . $latest_project["posorder_order"] . 
				 $item_filter;
		
		$tmp = array();
		$res_o = mysql_query($sql_o) or dberror($sql_o);
		while ($row_o = mysql_fetch_assoc($res_o))
		{
			$tmp[] = array("code"=>$row_o["item_code"], "quantity"=>$row_o["order_item_quantity"]);
		}

		if(count($tmp) > 0)
		{
			$pos_locations_items[$row["posaddress_id"]] = $tmp;
			$pos_location_projects[$row["posaddress_id"]] = $latest_project["project_id"];
			
			$tmp["posaddress_name"] = $row["posaddress_name"];
			$tmp["country_name"] = $row["country_name"];
			$tmp["project_number"] = $latest_project["project_number"];
			
			$pos_locations[$row["posaddress_id"]] = $tmp;
		}
	}

}


$captions['A'] = "Nr";
$captions['B'] = "Country";
$captions['C'] = "POS Location";
$captions['D'] = "Project Number";


$colwidth = array();
$colwidth['A'] = "5";
$colwidth['B'] = "5";
$colwidth['C'] = "5";
$colwidth['D'] = "5";

$achar = "D";
$colchars = 1;
foreach($items as $item_id=>$item_code)
{
	if(($achar == 'Z' and $colchars == 11) or $colchars == 12) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['K'. $achar] = $item_code;
		$colchars = 12;
		$colwidth['K' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 10) or $colchars == 11) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['J'. $achar] = $item_code;
		$colchars = 11;
		$colwidth['J' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 9) or $colchars == 10) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['I'. $achar] = $item_code;
		$colchars = 10;
		$colwidth['I' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 8) or $colchars == 9) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['H'. $achar] = $item_code;
		$colchars = 9;
		$colwidth['H' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 7) or $colchars == 8) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['G'. $achar] = $item_code;
		$colchars = 8;
		$colwidth['G' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 6) or $colchars == 7) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['F'. $achar] = $item_code;
		$colchars = 7;
		$colwidth['F' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 5) or $colchars == 6) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['E'. $achar] = $item_code;
		$colchars = 6;
		$colwidth['E' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 4) or $colchars == 5) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['D'. $achar] = $item_code;
		$colchars = 5;
		$colwidth['D' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 3) or $colchars == 4) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['C'. $achar] = $item_code;
		$colchars = 4;
		$colwidth['C' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 2) or $colchars == 3) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['B'. $achar] = $item_code;
		$colchars = 3;
		$colwidth['B' . $achar] = "5";
	}
	elseif($achar == 'Z' or $colchars == 2) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['A'. $achar] = $item_code;
		$colchars = 2;
		$colwidth['A' . $achar] = "5";
	}
	else
	{
		$achar = chr(ord($achar) + 1);
		$captions[$achar] = $item_code;
		$colwidth[$achar] = "5";
	}
}


/********************************************************************
    Start output
*********************************************************************/
$objPHPExcel = new PHPExcel();

$logo = new PHPExcel_Worksheet_Drawing();
$logo->setName('Logo');
$logo->setDescription('Logo');
$logo->setPath('../pictures/omega_logo.jpg');
$logo->setHeight(36);
$logo->setWidth(113);

$sheet = $objPHPExcel->getActiveSheet();
$objPHPExcel->getActiveSheet()->setTitle('POS_Equipment');

$logo->setWorksheet($objPHPExcel->getActiveSheet());

//output formats
$default_border = array(
    'style' => PHPExcel_Style_Border::BORDER_THIN,
    'color' => array('rgb'=>'1006A3')
);

$style_normal_border = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '1006A3'),
		),
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
);


$style_normal_border_right = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '1006A3'),
		),
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	),
);


$style_title = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
    'font' => array(
        'bold' => true,
		'size' => 16
    )
);

$style_header = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'font' => array(
        'bold' => true,
    )
);

$style_header_right = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	),
    'font' => array(
        'bold' => true,
    )
);

//default styles
$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(8); 


// HEADRES ROW 1
$sheet->setCellValue('D1', 'Equipment of POS Locations (' . date("d.m.Y H:i:s") . ')');
$sheet->getStyle('D1')->applyFromArray( $style_title );
$sheet->getRowDimension('1')->setRowHeight(36);


$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A3);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(1);


// HEADRES 
foreach($captions as $col=>$caption){
    $sheet->setCellValue($col . '3', $caption);
	$sheet->getStyle($col . '3')->applyFromArray( $style_header );
	$sheet->getStyle($col . '3')->getAlignment()->setTextRotation(90);
}

$sheet->getRowDimension('3')->setRowHeight(150);


//OUTPUT DATA


$zebra_counter = 0;
$row_index = 5;
$i = 1;
$cell_index = 0;


foreach($pos_locations as $pos_id=>$poslocation)
{
	
	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $i);
	if($colwidth['A'] < strlen($i)){$colwidth['A'] = strlen($i);}
	$cell_index++;

	if(array_key_exists("country_name", $poslocation))
	{
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $poslocation["country_name"]);
		if($colwidth['B'] < strlen($poslocation["country_name"])){$colwidth['B'] = 2+strlen($poslocation["country_name"]);}
		$cell_index++;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $poslocation["posaddress_name"]);
		if($colwidth['C'] < strlen($poslocation["posaddress_name"])){$colwidth['C'] = 2+strlen($poslocation["posaddress_name"]);}
		$cell_index++;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $poslocation["project_number"]);
		if($colwidth['D'] < strlen($poslocation["project_number"])){$colwidth['D'] = 2+strlen($poslocation["project_number"]);}
		$cell_index++;

		$i++;
		$row_index++;
		$cell_index = 0;
	}

}


//output items
$row_index = 5;
foreach($pos_locations_items as $pos_id=>$pos_items)
{
	$cell_index = 4;
	
	
	foreach($items as $item_id=>$item_code)
	{
		foreach($pos_items as $key=>$pos_item)
		{
			if($pos_item["code"] == $item_code)
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $pos_item["quantity"]);
			}
			
		}
		$cell_index++;
	}

	$row_index++;
}

//Format column heights and witdhs
foreach($colwidth as $col=>$width) {
	$sheet->getColumnDimension($col)->setWidth($width);
}

/********************************************************************
    Activate Sheet 1
*********************************************************************/

$objPHPExcel->setActiveSheetIndex(0);
$sheet->setCellValue('A1', "");
$sheet->getStyle('A1')->applyFromArray($style_title);
$objPHPExcel->getActiveSheet()->insertNewRowBefore(1, 1);
$sheet->setCellValue('A1', "");
$sheet->getStyle('A1')->applyFromArray($style_title);


/********************************************************************
    Start output
*********************************************************************/
$filename = 'POS_Location_Equipments_' . date('Ymd H:i:s') . '.xls';
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');
header('Pragma: ');

//send data
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
?>