<?php
/********************************************************************

    projects_query_11_xls.php

    Generate Excel File for the project milestones

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2012-01-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2012-01-15
    Version:        1.1.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/


require_once "../include/frame.php";

check_access("can_perform_queries");

require_once "../include/xls/Writer.php";
require_once "include/get_functions.php";
require_once("projects_query_filter_strings.php");


if(!param("query_id"))
{
	redirect("projects_queries.php");
}

/********************************************************************
    prepare Data Needed
*********************************************************************/

//check if filter is present
$query_name = '';
$print_query_filter = 0;
$sql = "select mis_query_filter, mis_query_name, mis_print_filter from mis_queries " .
	   "where mis_query_id = " . param("query_id");

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{

	$filters = array();
	$filters = unserialize($row["mis_query_filter"]);

    $query_name = $row["mis_query_name"];
	if($row["mis_print_filter"] == 1)
	{
		$print_query_filter = 1;
	}



	foreach($filters as $key=>$value)
	{
		$pct = $filters["pct"]; // project cost type
		$pt = $filters["pt"]; // project types
		$pk = $filters["pk"]; // product kinds

		$fosc = $filters["fst"]; //from order state
		$tosc = $filters["tst"]; // to order state

		
		$subfy = $filters["subfy"]; // LN Submited Year From
		$subfm = $filters["subfm"]; // LN Submited Month From
		$subty = $filters["subty"]; // LN Submited Year To
		$subtm = $filters["subtm"]; // LN Submited Month To

		$archive = $filters["arch"];
		$cnt = $filters["co"]; // Countries
	}
}
else
{
	redirect("projects_queries.php");
}





$header = "Project Milestones: " . $query_name;

$header .= " (" . date("d.m.Y G:i") . ") ";

/********************************************************************
    prepare Data
*********************************************************************/
$filter = "";


$pt = substr($pt,0, strlen($pt)-1); // remove last comma
$pt = str_replace("-", ",", $pt);
if($pt and $filter) // project type
{
    $filter .=  " and (project_postype IN (" . $pt . "))";
}
elseif($pt)
{
    $filter =  " (project_postype IN (" . $pt . "))";
}




$pk = substr($pk,0, strlen($pk)-1); // remove last comma
$pk = str_replace("-", ",", $pk);
if($pk and $filter) // project kind
{
    $filter .=  " and (project_projectkind IN (" . $pk . "))";
	$_filter_strings["Project Kinds"] = get_filter_string("pk", $pk);
}
elseif($pk)
{
    $filter =  " (project_projectkind IN (" . $pk . "))";
	$_filter_strings["Project Kinds"] = get_filter_string("pk", $pk);
}

$pct = substr($pct,0, strlen($pct)-1); // remove last comma
$pct = str_replace("-", ",", $pct);
if($pct and $filter) // project type
{
    $filter .=  " and (project_cost_type IN (" . $pct . "))";
	$_filter_strings["Legal Types"] = get_filter_string("lpt", $pct);
}
elseif($pct)
{
    $filter =  " (project_cost_type IN (" . $pct . "))";
	$_filter_strings["Legal Types"] = get_filter_string("lpt", $pct);
}



if($filter and $fosc) // from order state
{
    $filter.=  " and order_actual_order_state_code >= '" . $fosc . "' ";
	$_filter_strings["From Order State Code"] = $fosc;
}
elseif($fosc)
{
	$filter =  " order_actual_order_state_code >= '" . $fosc . "' ";
	$_filter_strings["From Order State Code"] = $fosc;
}

if($filter and $tosc) // to order state
{
    $filter.=  " and order_actual_order_state_code <= '" . $tosc . "' ";
	$_filter_strings["To Order State Code"] = $tosc;
}
elseif($tosc)
{
	$filter =  " order_actual_order_state_code <= '" . $tosc . "' ";
	$_filter_strings["To Order State Code"] = $tosc;
}

if($filter and $archive == 1) // include projects from archive
{
	
}

$cnt = substr($cnt,0, strlen($cnt)-1); // remove last comma
$cnt = str_replace("-", ",", $cnt);
if($cnt and $filter) // country
{
    $filter .=  " and (order_shop_address_country IN (" . $cnt . "))";
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);

}
elseif($cnt)
{
    $filter =  " where (order_shop_address_country IN (" . $cnt . "))";
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);
}

//get all projects where LN or CER was posted
$milestone_filter = "";
if($subfy > 0 and $subfm > 0 and $subty > 0 and $subtm > 0)
{
	$tmp = $subfy . "" . $subfm;
	$milestone_filter =  " where concat(YEAR(project_milestone_date), DATE_FORMAT(project_milestone_date,'%m')) >= " . $tmp;
	$_filter_strings["Posted from"] = $subfy . "-" . $subfm;
	
	$tmp = $subty . "" . $subtm;
	$milestone_filter.=  " and concat(YEAR(project_milestone_date), DATE_FORMAT(project_milestone_date,'%m')) <= " . $tmp;
	$_filter_strings["Posted to"] = $subty . "-" . $subtm;

}
elseif($subfy > 0 and $subty > 0)
{
	$milestone_filter =  " where YEAR(project_milestone_date) >= " . $subfy;
	$milestone_filter .=  " and YEAR(project_milestone_date) <= " . $subty;
	$_filter_strings["Posted from"] = $subfy;
	$_filter_strings["Posted to"] = $subty;
}

elseif($subfy > 0 and $subfm > 0)
{
	$tmp = $subfy . "" . $subfm;
	$milestone_filter =  " where concat(YEAR(project_milestone_date), DATE_FORMAT(project_milestone_date,'%m')) >= " . $tmp;
	$_filter_strings["Posted from"] = $subfy . "-" . $subfm;

}
elseif($subty > 0 and $subtm > 0)
{
	$tmp = $subty . "" . $subtm;
	$milestone_filter.=  " where concat(YEAR(project_milestone_date), DATE_FORMAT(project_milestone_date,'%m')) <= " . $tmp;
	$_filter_strings["Posted to"] = $subty . "-" . $subtm;
}
elseif($subfy > 0)
{
	$milestone_filter =  " where YEAR(project_milestone_date) >= " . $subfy;
	$_filter_strings["Posted from"] = $subfy;
}
elseif($subty > 0 )
{
	$milestone_filter =  " where YEAR(project_milestone_date) <= " . $subty;
	$_filter_strings["Posted to"] = $subty;
}

if($milestone_filter) {

	$tmp = array();
	$sql = "select project_milestone_project "  . 
		   "from project_milestones " . 
		   $milestone_filter  . 
		   " and project_milestone_milestone in (1, 16) ";

	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{
		$tmp[] = $row["project_milestone_project"];
	}

	if(count($tmp) > 0) {
	
		if($filter) // posted from
		{
			$filter .= " and project_id IN (" . implode ("," , $tmp ) . ") ";
		}
		else
		{
			$filter = " project_id IN (" . implode ("," , $tmp ) . ") ";
		}
	}


}


if($filter and $archive == 1)
{
	//$filter.= " and order_actual_order_state_code <> '900' ";
	$_filter_strings["Include archived projects"] = "yes";
}
elseif($archive == 1)
{
	$filter = " order_actual_order_state_code <> '900' ";
	$_filter_strings["Include archived projects"] = "yes";
}
elseif($filter)
{
	$filter.= " and order_actual_order_state_code < '820' " . 
		      " and (order_archive_date is null or order_archive_date = '0000-00-00') " .
		      " and (project_actual_opening_date is null or project_actual_opening_date = '0000-00-00') ";
}
else
{
	$filter = " order_actual_order_state_code < '820' " . 
		      " and (order_archive_date is null or order_archive_date = '0000-00-00') " .
		      " and (project_actual_opening_date is null or project_actual_opening_date = '0000-00-00') ";
}



/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$filename = "project_milestones_" . date("Ymd") . ".xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename); 
$xls->setVersion(8);
$sheet =& $xls->addWorksheet("Summary");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

//formats
$header_row =& $xls->addFormat();
$header_row->setSize(10);
$header_row->setAlign('left');
$header_row->setBold();


$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);


$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);


$f_used =& $xls->addFormat();
$f_used->setSize(8);
$f_used->setBorder(1);
$f_used->setPattern(2);
$f_used->setBgColor('yellow');


$f_vertical =& $xls->addFormat();
$f_vertical->setSize(9);
$f_vertical->setAlign('left');
$f_vertical->setBorder(1);
$f_vertical->setTextRotation(270);

$f_vertical_red =& $xls->addFormat();
$f_vertical_red->setSize(9);
$f_vertical_red->setAlign('left');
$f_vertical_red->setBorder(1);
$f_vertical_red->setTextRotation(270);
$f_vertical_red->setColor('red');
$f_vertical_red->setBold();

$f_bold_red =& $xls->addFormat();
$f_bold_red->setSize(8);
$f_bold_red->setAlign('left');
$f_bold_red->setBorder(1);
$f_bold_red->setColor('red');
$f_bold_red->setBold();

$f_bold =& $xls->addFormat();
$f_bold->setSize(8);
$f_bold->setAlign('left');
$f_bold->setBorder(1);

$f_bold->setBold();

/********************************************************************
    get milestones
*********************************************************************/
$milestones = array();
$milestones_sumups = array();

$sum_of_days = array();
$num_of_projects = array();


$sheet->write(0, 0, $header, $header_row);
$row_index_c = 2;

if($print_query_filter == 1)
{
	foreach($_filter_strings as $key=>$value)
	{
		 $sheet->write($row_index_c, 0,$key . ": " . $value);
		 $row_index_c++;
	}
	$row_index_c++;
}

$col_index_c = 8;


$sheet->write($row_index_c, $col_index_c-8, "Project Number", $f_vertical);
$sheet->write($row_index_c, $col_index_c-7, "Country Name", $f_vertical);
$sheet->write($row_index_c, $col_index_c-6, "Project Name", $f_vertical);
$sheet->write($row_index_c, $col_index_c-5, "POS Type", $f_vertical);
$sheet->write($row_index_c, $col_index_c-4, "Legal Type", $f_vertical);
$sheet->write($row_index_c, $col_index_c-3, "Project Kind", $f_vertical);
$sheet->write($row_index_c, $col_index_c-2, "Treatment State", $f_vertical);
$sheet->write($row_index_c, $col_index_c-1, "Satus", $f_vertical);


$sql_m = "select milestone_id, milestone_text, milestone_start_id, milestone_sumup_text, ".
         "milestone_start_id_replacement_id, milestone_replacement_if_empty " . 
			 "from milestones " . 
			 "   order by milestone_code";
	
$res_m = mysql_query($sql_m) or dberror($sql_m);
while ($row_m = mysql_fetch_assoc($res_m))
{
	$sheet->write($row_index_c, $col_index_c, $row_m['milestone_text'], $f_vertical);

	$milestones[$row_m['milestone_id']] = $col_index_c;
	$col_index_c++;

	$sum_of_days[$row_m['milestone_id']] = 0;
	$num_of_projects[$row_m['milestone_id']] = 0;
	
	if($row_m['milestone_start_id'] > 0) {
		$sheet->write($row_index_c, $col_index_c, $row_m['milestone_sumup_text'], $f_vertical_red);
		$col_index_c++;
		$milestones_sumups[$row_m['milestone_id']] = $col_index_c;
	}

	if($row_m['milestone_id'] == 14) //Cer/AF not posted
	{
		$sheet->write($row_index_c, $col_index_c, 'Preferred opening date', $f_vertical);
		$col_index_c++;
		
	}
}

$sheet->write($row_index_c, $col_index_c, "Total Number of Days from Projectstart to KL notice", $f_vertical_red);
$col_index_c++;

$sheet->write($row_index_c, $col_index_c, "Agreed opening date", $f_vertical);
$col_index_c++;

$sheet->write($row_index_c, $col_index_c, "Actual opening date", $f_vertical);
$col_index_c++;

$sheet->write($row_index_c, $col_index_c, "Number of days from KL approval to agreed or actual opening date", $f_vertical_red);
$col_index_c++;

$sheet->write($row_index_c, $col_index_c, "Total of days from Projectstart to agreed or actual opening date", $f_vertical_red);
$col_index_c++;

$sheet->write($row_index_c, $col_index_c, "Remarks", $f_vertical);


/********************************************************************
    write all data detail for product lines
*********************************************************************/
$row_index = $row_index_c + 1;
$col_index = 0;
$remark_column_width = 10;
$remark_column_index = 0;

$sum_of_total_days = 0;
$num_of_projects_total_days = 0;
$sum_of_total_days1 = 0;
$num_of_projects_total_days1 = 0;
$sum_of_total_days2 = 0;
$num_of_projects_total_days2 = 0;

$sql_p = "select project_id, order_shop_address_company, order_number, country_name, ".
         "project_costtype_text, project_cost_milestone_remarks, projectkind_code, postype_name, project_planned_opening_date, " .
		 "project_real_opening_date, project_actual_opening_date, order_actual_order_state_code, " .
		 "project_state_text " . 
		 "from projects " .
		 "left join project_costs on project_cost_order = project_order " .
		 "left join project_costtypes on project_costtype_id = project_cost_type " .
		 "left join projectkinds on projectkind_id = project_projectkind " .
		 "left join orders on order_id = project_order " .
		 "left join postypes on postype_id = project_postype " .
		 "left join countries on country_id = order_shop_address_country " .
		 "left join  project_states on project_state_id = project_state " . 
		 "where " . $filter  .
		 "   order by order_number";


$res = mysql_query($sql_p) or dberror($sql_p);
while ($row = mysql_fetch_assoc($res))
{
	
	$sheet->write($row_index, $col_index, $row['order_number'], $f_normal);
	$sheet->write($row_index, $col_index+1, $row['country_name'], $f_normal);
	$sheet->write($row_index, $col_index+2, $row['order_shop_address_company'], $f_normal);
	$sheet->write($row_index, $col_index+3, $row['postype_name'], $f_normal);
	$sheet->write($row_index, $col_index+4, $row['project_costtype_text'], $f_normal);
	$sheet->write($row_index, $col_index+5, $row['projectkind_code'], $f_normal);
	$sheet->write($row_index, $col_index+6, $row['project_state_text'], $f_normal);
	$sheet->write($row_index, $col_index+7, $row['order_actual_order_state_code'], $f_normal);
	
	

	$project_milestones = array();
	$project_milestone_replacements = array();
	$project_milestone_replacements_if_empty = array();
	$project_milestone_replacements_formula = array();
	$milestones_dates = array();
	$tmp_duration = array();
	$date = '';
	$firstdate = '';
	$lastdate = '';
	
	$sql_m = "select project_milestone_date, milestone_id, milestone_text, milestone_start_id, ".
			 "milestone_start_id_replacement_id, milestone_replacement_if_empty " .  
			 "from  milestones " . 
			 "left join project_milestones on milestone_id = project_milestone_milestone " . 
			 "where project_milestone_project = " . $row['project_id'] . 
			 "   order by milestone_code";
	
	
	$res_m = mysql_query($sql_m) or dberror($sql_m);
	while ($row_m = mysql_fetch_assoc($res_m))
	{
		
		$milestones_dates[$row_m['milestone_id']] = $row_m['project_milestone_date'];
		
		//milestone has a replacement id
		if($row_m['milestone_start_id_replacement_id'] > 0) {

			$sql_r = "select project_milestone_date, milestone_id " .  
					 "from  milestones " . 
					 "left join project_milestones on milestone_id = project_milestone_milestone " . 
					 "where project_milestone_project = " . $row['project_id'] . 
				     " and milestone_id = " . $row_m['milestone_start_id_replacement_id'];

			//echo $sql_r . '\n';

			$res_r = mysql_query($sql_r) or dberror($sql_r);
			if ($row_r = mysql_fetch_assoc($res_r))
			{
				if($row_m['milestone_id'] > $row_r['milestone_id']) {
					$project_milestone_replacements_formula[$row_m['milestone_id']] = 'forward';
				}
				else
				{
					$project_milestone_replacements_formula[$row_m['milestone_id']] = 'backward';
				}
				
				$project_milestone_replacements[$row_m['milestone_id']] = $row_r['project_milestone_date'];
				
			}
		
		}
		
		if($row_m['milestone_replacement_if_empty'] > 0) {
			
			$sql_r = "select project_milestone_date, milestone_id " .  
					 "from  milestones " . 
					 "left join project_milestones on milestone_id = project_milestone_milestone " . 
					 "where project_milestone_project = " . $row['project_id'] . 
				     " and milestone_id = " . $row_m['milestone_replacement_if_empty'];

			$res_r = mysql_query($sql_r) or dberror($sql_r);
			if ($row_r = mysql_fetch_assoc($res_r))
			{
				$project_milestone_replacements_if_empty[$row_m['milestone_id']] = $row_r['project_milestone_date'];
			}
		}
		
		//fromat milestone date for output
		$date = substr($row_m['project_milestone_date'], 8, 2) . '.' . substr($row_m['project_milestone_date'], 5, 2) . '.' . substr($row_m['project_milestone_date'], 0,4);
		$project_milestones[$row_m['milestone_id']] = $date;
		

		//set first and last date day of process
		if(!$firstdate and $row_m['project_milestone_date'] != NULL and $row_m['project_milestone_date'] != '0000-00-00') {
			$firstdate = $row_m['project_milestone_date'];
		}

		$lastdate = '';

		if($row_m['project_milestone_date'] != NULL and $row_m['project_milestone_date'] != '0000-00-00') {
			$lastdate = $row_m['project_milestone_date'];
		}
		
		
		//calculate duration of process
		$calculated = 0;
		if(array_key_exists($row_m['milestone_id'], $milestones_sumups)) {
			
			
			if($row_m['project_milestone_date'] != NULL and $row_m['project_milestone_date'] != '0000-00-00')
			{
				if(array_key_exists($row_m['milestone_id'], $project_milestone_replacements) 
					and array_key_exists($row_m['milestone_start_id'], $milestones_dates)
					and $project_milestone_replacements[$row_m['milestone_id']] != NULL 
					and $project_milestone_replacements[$row_m['milestone_id']] != '0000-00-00' 
					and $milestones_dates[$row_m['milestone_start_id']] != NULL 
					and $milestones_dates[$row_m['milestone_start_id']] != '0000-00-00') 
				{
					
					if($project_milestone_replacements_formula[$row_m['milestone_id']] == 'forward') {
						$tmp_duration[$row_m['milestone_id']] = ceil((strtotime($row_m['project_milestone_date']) - strtotime($project_milestone_replacements[$row_m['milestone_id']])) / 86400);
					}
					else
					{
						$tmp_duration[$row_m['milestone_id']] = ceil((strtotime($project_milestone_replacements[$row_m['milestone_id']]) - strtotime($milestones_dates[$row_m['milestone_start_id']])) / 86400);
					}
					$calculated = 1;
				}
				if($calculated == 0)
				{
					if(array_key_exists($row_m['milestone_start_id'], $milestones_dates) and $milestones_dates[$row_m['milestone_start_id']] != NULL and $milestones_dates[$row_m['milestone_start_id']] != '0000-00-00' and $row_m['project_milestone_date'] != NULL and $row_m['project_milestone_date'] != '0000-00-') 
					{
						$tmp_duration[$row_m['milestone_id']] = ceil((strtotime($row_m['project_milestone_date']) - strtotime($milestones_dates[$row_m['milestone_start_id']])) / 86400);
					}
				}
			}
			elseif(array_key_exists($row_m['milestone_id'], $project_milestone_replacements_if_empty) 
					and array_key_exists($row_m['milestone_start_id'], $milestones_dates)
					and $project_milestone_replacements_if_empty[$row_m['milestone_id']] != NULL 
					and $project_milestone_replacements_if_empty[$row_m['milestone_id']] != '0000-00-00' 
					and $milestones_dates[$row_m['milestone_start_id']] != NULL 
					and $milestones_dates[$row_m['milestone_start_id']] != '0000-00-00') 
				{
					
					$tmp_duration[$row_m['milestone_id']] = ceil((strtotime($project_milestone_replacements_if_empty[$row_m['milestone_id']]) - strtotime($milestones_dates[$row_m['milestone_start_id']])) / 86400);
					
			}
		}
	}

	//print dates and durations
	foreach($milestones as $id=>$column) {
	
		if(array_key_exists($id, $project_milestones) and $project_milestones[$id] != '' and $project_milestones[$id] != '..') {
			$sheet->write($row_index, $column, $project_milestones[$id], $f_normal);
		}
		else
		{
			$sheet->write($row_index, $column, "", $f_normal);
		}

		

		if(array_key_exists($id, $tmp_duration)) {
			$sheet->write($row_index, $column+1, $tmp_duration[$id], $f_bold_red);
			$sum_of_days[$id] = $sum_of_days[$id] + $tmp_duration[$id];
			$num_of_projects[$id] = $num_of_projects[$id] + 1;
		}
		else {
			$sheet->write($row_index, $column+1, "", $f_normal);
		}

		if($id == 14) //Cer/AF not posted
		{
			$date = substr($row['project_planned_opening_date'], 8, 2) . '.' . substr($row['project_planned_opening_date'], 5, 2) . '.' . substr($row['project_planned_opening_date'], 0,4);
			$sheet->write($row_index, $column+1, $date, $f_normal);
		}
	}
	

	//total days
	
	$difference_in_days = 0;
	if($lastdate) {
		$difference_in_days = ceil((strtotime($lastdate) - strtotime($firstdate)) / 86400);
	}


	
	if($difference_in_days == 0)
	{
		$difference_in_days = '';
	}
	else
	{
		$sum_of_total_days = $sum_of_total_days + $difference_in_days;
		$num_of_projects_total_days = $num_of_projects_total_days + 1;
	}

	$sheet->write($row_index, $col_index_c-5, $difference_in_days, $f_bold_red);

	

	$date = substr($row['project_real_opening_date'], 8, 2) . '.' . substr($row['project_real_opening_date'], 5, 2) . '.' . substr($row['project_real_opening_date'], 0,4);

	if($date == '..') {
		$sheet->write($row_index, $col_index_c-4, "", $f_normal);
	}
	else
	{
		$sheet->write($row_index, $col_index_c-4, $date, $f_normal);
	}

	$date = substr($row['project_actual_opening_date'], 8, 2) . '.' . substr($row['project_actual_opening_date'], 5, 2) . '.' . substr($row['project_actual_opening_date'], 0,4);
	if($date == '..') {
		$sheet->write($row_index, $col_index_c-3, "", $f_normal);
	}
	else
	{
		$sheet->write($row_index, $col_index_c-3, $date, $f_normal);
	}

	$difference_in_days = '';
	if($milestones_dates[12] and $milestones_dates[12] != '0000-00-00' and $row['project_actual_opening_date'] and $row['project_actual_opening_date'] != '0000-00-00') {
		$difference_in_days = ceil((strtotime($row['project_actual_opening_date']) - strtotime($milestones_dates[12])) / 86400);
	}
	elseif($milestones_dates[12] and $milestones_dates[12] != '0000-00-00' and $row['project_real_opening_date'] and $row['project_real_opening_date'] != '0000-00-00') {
		$difference_in_days = ceil((strtotime($row['project_real_opening_date']) - strtotime($milestones_dates[12])) / 86400);
	}

	if($difference_in_days)
	{
		$sum_of_total_days1 = $sum_of_total_days1 + $difference_in_days;
		$num_of_projects_total_days1 = $num_of_projects_total_days1 + 1;
	}
	
	
	$sheet->write($row_index, $col_index_c-2, $difference_in_days, $f_bold_red);



	$difference_in_days = '';
	if($milestones_dates[14] 
		and $milestones_dates[14] != '0000-00-00' 
		and $row['project_actual_opening_date'] 
		and $row['project_actual_opening_date'] != '0000-00-00') {
		$difference_in_days = ceil((strtotime($row['project_actual_opening_date']) - strtotime($milestones_dates[14])) / 86400);
	}
	elseif($milestones_dates[14] 
		and $milestones_dates[14] != '0000-00-00' 
		and $row['project_real_opening_date'] 
		and $row['project_real_opening_date'] != '0000-00-00') {
		$difference_in_days = ceil((strtotime($row['project_real_opening_date']) - strtotime($milestones_dates[14])) / 86400);
	}

	if($difference_in_days)
	{
		$sum_of_total_days2 = $sum_of_total_days2 + $difference_in_days;
		$num_of_projects_total_days2 = $num_of_projects_total_days2 + 1;
	}
	
	
	$sheet->write($row_index, $col_index_c-1, $difference_in_days, $f_bold_red);


	$tmp = str_replace("\r\n", ", " , $row['project_cost_milestone_remarks']);
	$sheet->write($row_index, $col_index_c, $tmp, $f_normal);

	if(strlen($tmp) > $remark_column_width) {
		$remark_column_width = strlen($tmp);
	}
	$remark_column_index = $col_index_c;

	$row_index++;
	
}

$row_index++;

$sheet->write($row_index, 0, "Average", $f_bold);
$sheet->write($row_index, 1, "", $f_bold);
$sheet->write($row_index, 2, "", $f_bold);
$sheet->write($row_index, 3, "", $f_bold);
$sheet->write($row_index, 4, "", $f_bold);
$sheet->write($row_index, 5, "", $f_bold);
$sheet->write($row_index, 6, "", $f_bold);

//print averages
foreach($milestones as $id=>$column) {

	$sheet->write($row_index, $column, "", $f_bold);


	if(array_key_exists($id, $sum_of_days) and $num_of_projects[$id] > 0) {
		$sheet->write($row_index, $column+1, round($sum_of_days[$id]/$num_of_projects[$id],0), $f_bold);
	}
	else {
		$sheet->write($row_index, $column+1, "", $f_bold);
	}
}

$column = $column+2;

if($num_of_projects_total_days > 0) {
	$sheet->write($row_index, $column, round($sum_of_total_days/$num_of_projects_total_days,0), $f_bold);
}
else {
	$sheet->write($row_index, $column, "", $f_bold);
}

$sheet->write($row_index, $column+1, "", $f_bold);
$sheet->write($row_index, $column+2, "", $f_bold);


if($num_of_projects_total_days1 > 0) {
	$sheet->write($row_index, $column+3, round($sum_of_total_days1/$num_of_projects_total_days1,0), $f_bold);
}
else {
	$sheet->write($row_index, $column+3, "", $f_bold);
}

if($num_of_projects_total_days2 > 0) {
	$sheet->write($row_index, $column+4, round($sum_of_total_days2/$num_of_projects_total_days2,0), $f_bold);
}
else {
	$sheet->write($row_index, $column+4, "", $f_bold);
}


$sheet->setColumn(1, 1, 20);
$sheet->setColumn(2, 2, 40);
$sheet->setColumn($remark_column_index, $remark_column_index, $remark_column_width);

$xls->close(); 

?>
