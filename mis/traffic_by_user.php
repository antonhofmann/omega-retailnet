<?php
/********************************************************************

    traffic_by_company.php

    Enter parameters for the query of traffic

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-05-17
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-05-17
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_perform_queries");



/********************************************************************
    prepare all data needed
*********************************************************************/
$years = array();

$years[2013] = 2013;

for ($i = date("Y"); $i > 2013; $i--) {
	$years[$i] = $i;
}

krsort($years);

$selected_year = date("Y");
if(param("year")) {
	$selected_year = param("year");
}

$selected_module = "user";
if(param("module")) {
	$selected_module = param("module");
}

$number_of_days = 365;
if($selected_year == date("Y"))
{
	$start = strtotime(date("Y") . '-01-01');
	$end = strtotime(date("Y-m-d"));
	$number_of_days = ceil(abs($end - $start) / 86400);

}


$modules = array();
$modules["archive"] = "Archive";
$modules["admin"] = "System Administration";
$modules["cer"] = "CER/AF";
$modules["flikflak"] = "Flik Flak Integration";
$modules["mis"] = "Management Information";
$modules["pos"] = "POS Index";
$modules["administration"] = "Merchandising Planning System Administration";
$modules["mps"] = "Merchandising Planning Swatch";
$modules["mpsflikflak"] = "Merchandising Planning Flik Flak";
$modules["red"] = "Retail Environment Development";
$modules["scpps"] = "Stock Contol and Production Planning";
$modules["sec"] = "Security";
$modules["user"] = "Projects and Catalogue Orders";

asort($modules);


$page_content = '<form id="main" name="main">';



$page_content .= "<select name=\"module\" onchange=\"javascript:document.forms['main'].submit();\">";
foreach($modules as $key=>$module)
{
	if($selected_module == $key)
	{
		$page_content .= '<option value="' . $key . '"selected="selected">' . $module .'</option>';
	}
	else
	{
		$page_content .= '<option value="' . $key . '">' . $module .'</option>';
	}
	
}
$page_content .= '</select>';




$page_content .= "<select name=\"year\" onchange=\"javascript:document.forms['main'].submit();\">";
foreach($years as $key=>$year)
{
	if($selected_year == $year)
	{
		$page_content .= '<option value="' . $year . '"selected="selected">' . $year .'</option>';
	}
	else
	{
		$page_content .= '<option value="' . $year . '">' . $year .'</option>';
	}
	
}
$page_content .= '</select>';



$page_content .= '<p>&nbsp;</p>';




$stats = array();
$access_totals = 0;

$table_names = array();
//get all statsitic tables
$sql = 'SHOW TABLES FROM db_retailnet where Tables_in_db_retailnet like "statistic%"';
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$table_names[] = $row["Tables_in_db_retailnet"];
}

foreach($table_names as $key=>$table_name)
{

	$sql = "SELECT statistic_id, statistic_url, user_id, address_id, address_company, country_name, " .
		   "user_name, user_firstname " . 
		   "FROM " . $table_name . 
		   " left join users on user_id = statistic_user " .
	       " left join addresses on address_id = user_address " . 
		   " left join countries on country_id = address_country " . 
		   " WHERE year(statistic_date) = " . $selected_year .
		   " and statistic_url like '%/" . $selected_module . "/%'" .
		   " and statistic_url not like '%login%'" .
		   " and statistic_url not like '%logoff%'" .
		   " order by country_name, address_company, user_name, user_firstname";

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$user = $row["user_name"] . ' ' . $row["user_firstname"];
		$user_key = $row["address_id"]. $row["user_id"];

		if(!$row["user_id"])
		{
			$row["country_name"] = 'n.a.';
			$row["address_company"] = 'n.a.';
			$row["user_name"] = 'n.a.';
			$row["user_firstname"] = 'n.a.';
			$user_key = "na";
		}

		
		
		if(array_key_exists($user_key, $stats))
		{
			$tmp = $stats[$user_key]["count"] + 1;
			$stats[$user_key] = array("country"=>$row["country_name"], "company"=>$row["address_company"], "user"=>$user, "count"=>$tmp);
		}
		else
		{
			$stats[$user_key] = array("country"=>$row["country_name"], "company"=>$row["address_company"], "user"=>$user, "count"=>1);
		}
		$access_totals++;
	}
}

$record_content = '';
foreach($stats as $address_id=>$data)
{
	
	
	$record_content .= '<tr>';
	$record_content .= '<td style="border-bottom:1px solid #aaaaaa;">';
	$record_content .= $data["country"];
	$record_content .= '</td>';

	$record_content .= '<td style="border-bottom:1px solid #aaaaaa;">';
	$record_content .= $data["company"];
	$record_content .= '</td>';

	$record_content .= '<td style="border-bottom:1px solid #aaaaaa;">';
	$record_content .= $data["user"];
	$record_content .= '</td>';

	$record_content .= '<td align="right" style="border-bottom:1px solid #aaaaaa;">';
	$record_content .= number_format ( $data["count"] , 0 , '.' , "'" );
	$record_content .= '</td>';

	$record_content .= '</tr>';
}

$page_content .= '<table>';


$page_content .= '<tr>';
$page_content .= '<td colspan="2">';
$page_content .= '<h3>' . $modules[$selected_module] . '</h3>';
$page_content .= '</td>';
$page_content .= '</tr>';




$access_info = "<strong>Acess Total = " . 
               number_format ( $access_totals , 0 , '.' , "'" ) . 
			   " ("  . number_format( $access_totals/$number_of_days , 0 , '.' , "'" ) . " per day) " . 
			   "</strong><br /><br />";
$page_content .= '<tr>';
$page_content .= '<td colspan="2">';
$page_content .= $access_info;
$page_content .= '</td>';
$page_content .= '</tr>';



$page_content .= '<tr>';
$page_content .= '<td class="list_header" nowrap="nowrap">';
$page_content .= 'Country';
$page_content .= '</td>';
$page_content .= '<td class="list_header" nowrap="nowrap">';
$page_content .= 'Company';
$page_content .= '</td>';
$page_content .= '<td class="list_header" nowrap="nowrap">';
$page_content .= 'User';
$page_content .= '</td>';
$page_content .= '<td class="list_header" nowrap="nowrap" align="right">';
$page_content .= '&nbsp;&nbsp;&nbsp;Access Count';
$page_content .= '</td>';
$page_content .= '</tr>';

$page_content .= $record_content;


$page_content .= '</table>';

/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page("queries");

require "include/mis_page_actions.php";

$page->header();
$page->title("Traffic by Users");

echo $page_content;

$page->footer();

?>