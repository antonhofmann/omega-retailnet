<?php
/********************************************************************

    projects_query_filter_selector.php

    Enter Filter Criteria for the Query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2011-07-30
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2011-07-30
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../include/page_modal.php";

check_access("can_perform_queries");

require_once "include/query_get_functions.php";

/********************************************************************
    prepare all data needed
*********************************************************************/
$query_id = param("query_id");

$qt = 1;
if(param("qt"))
{
	$qt = param("qt");
}


$filter = array();
$filter = get_query_filter($query_id);

//get  parameters


$client_types = array();

$sql_client_types = "select client_type_id, client_type_code ".
					"from client_types";

$tmp_filter = "";
$supplier_filter = "";
if(count($filter["supp"]) > 0)
{

	$suppliers = explode("-", $filter["supp"]);
	$tmp_filter = "";
	foreach($suppliers as $key=>$supplier)
	{
		$tmp_filter = $tmp_filter . $supplier . ",";
	}
	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);
	$supplier_filter = $tmp_filter;

	if($tmp_filter)
	{
		$tmp_filter = " and supplier_address in (" . $tmp_filter . ") ";
	}


	//get all product lines of the supplier
	$product_line_ids = array();
	$sql = "select DISTINCT product_line_id " . 
		   "from items " . 
		   "inner join category_items on category_item_item = item_id " . 
		   "inner join categories on category_id = category_item_category ". 
		   "inner join product_lines on product_line_id = category_product_line " . 
		   "inner join suppliers on supplier_item = item_id ". 
		   "where item_type = 1 " . 
		   $tmp_filter ;

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$product_line_ids[] = $row["product_line_id"];
	}

	
	$tmp_filter = "";
	if(count($product_line_ids) > 0)
	{
		$tmp_filter = " and product_line_id IN (" . implode(",", $product_line_ids) . ")";
	}
	
	$sql_product_lines = "select product_line_id, product_line_name " . 
					 "from product_lines " .
		             "where product_line_mis = 1 " .
					 $tmp_filter . 
					 "order by product_line_name";
}
else
{
	$sql_product_lines = "select product_line_id, product_line_name ".
						 "from product_lines " . 
						 "where product_line_mis = 1 " .
						 "   order by product_line_name";
}




$tmp_filter = "";
$productline_filter = "";
if(count($filter["pl"]) > 0)
{
	$productlines = explode("-", $filter["pl"]);
	$tmp_filter = "";
	foreach($productlines as $key=>$productline)
	{
		$tmp_filter = $tmp_filter . $productline . ",";
	}
	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);
	$productline_filter = $tmp_filter;

	if($tmp_filter)
	{
		$tmp_filter = "where productline_subclass_productline in (" . $tmp_filter . ") ";
	}
}

$sql_product_line_subclasses = "select productline_subclass_id, productline_subclass_name ".
							   "from productline_subclasses " .
							   $tmp_filter .
						       "   order by productline_subclass_name";


$sql_pos_types = "select DISTINCT postype_id, postype_name ".
                     "from postypes " .
                     "order by postype_name";


$sql_pos_type_subclasses = "select possubclass_id, possubclass_name ".
							 "from possubclasses " .
							 "order by possubclass_name";

$sql_project_kinds = "select projectkind_id, projectkind_name ".
						 "from projectkinds " .
						 "where projectkind_id > 0 " .
						 "order by projectkind_name";


$sql_project_cost_types = "select project_costtype_id, project_costtype_text ".
						  "from project_costtypes " .
                          "order by project_costtype_text";

$salesregions = array();
$supplyingregions = array();

$sql_salesregion = "select salesregion_id, salesregion_name ".
					"from salesregions order by salesregion_name";

$sql_supplyingregion = "select region_id, region_name ".
						"from regions order by region_name";



$tmp_filter = "";
if(count($filter["gr"]) > 0 and count($filter["re"]) > 0)
{
	$salesregions = explode("-", $filter["gr"]);
	$tmp_filter1 = "";
	foreach($salesregions as $key=>$salesregion)
	{
		$tmp_filter1 = $tmp_filter1 . $salesregion . ",";
	}
	$tmp_filter1 = substr($tmp_filter1, 0, strlen($tmp_filter1) - 2);

	if($tmp_filter1)
	{
		$tmp_filter1 = "country_salesregion in (" . $tmp_filter1 . ") ";
	}

	$supplyingregions = explode("-", $filter["re"]);
	$tmp_filter2 = "";
	foreach($supplyingregions as $key=>$supplyingregion)
	{
		$tmp_filter2 = $tmp_filter2 . $supplyingregion . ",";
	}
	$tmp_filter2 = substr($tmp_filter2, 0, strlen($tmp_filter2) - 2);

	if($tmp_filter2)
	{
		$tmp_filter2 = "country_region in (" . $tmp_filter2 . ") ";
	}

	if($tmp_filter1 and $tmp_filter2)
	{
		$tmp_filter = "where (" . $tmp_filter1 . ' OR ' . $tmp_filter2 . ')';
	}
	elseif($tmp_filter1)
	{
		$tmp_filter = "where " . $tmp_filter1;
	}
	elseif($tmp_filter2)
	{
		$tmp_filter = "where " . $tmp_filter2;
	}
}
elseif(count($filter["gr"]) > 0)
{
	$salesregions = explode("-", $filter["gr"]);
	$tmp_filter = "";
	foreach($salesregions as $key=>$salesregion)
	{
		$tmp_filter = $tmp_filter . $salesregion . ",";
	}
	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);

	if($tmp_filter)
	{
		$tmp_filter = "where country_salesregion in (" . $tmp_filter . ") ";
	}
}
elseif(count($filter["re"]) > 0)
{
	$supplyingregions = explode("-", $filter["re"]);
	$tmp_filter = "";
	foreach($supplyingregions as $key=>$supplyingregion)
	{
		$tmp_filter = $tmp_filter . $supplyingregion . ",";
	}
	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);

	if($tmp_filter)
	{
		$tmp_filter = "where country_region in (" . $tmp_filter . ") ";
	}
}

if($tmp_filter) {
	$tmp_filter .= ' and country_name <> ""';
}
else
{
	$tmp_filter .= ' where country_name <> ""';
}


$sql_country = "select DISTINCT country_id, country_name " .
			   "from orders " .
			   "left join countries on country_id = order_shop_address_country " .
			   $tmp_filter . 
			   " order by country_name";


$sql_store_coordinators = "select user_id, concat(user_name, ' ', user_firstname) as username " . 
                          "from users " . 
						  "left join user_roles on user_role_user = user_id " . 
						  "where user_role_role = 3 " .
						  " order by username";

$sql_local_store_coordinators = "select user_id, concat(user_name, ' ', user_firstname) as username " . 
                          "from users " . 
						  "left join user_roles on user_role_user = user_id " . 
						  "where user_role_role = 10 " .
						  " order by username";

$sql_retail_operators = "select user_id, concat(user_name, ' ', user_firstname) as username " . 
                          "from users " . 
						  "left join user_roles on user_role_user = user_id " . 
						  "where user_role_role = 2 " .
						  " order by username";



$sql_areas = "select posareatype_id, posareatype_name from posareatypes ". 
             "order by posareatype_name";



//get items
/*
if(array_key_exists("items", $filter))
{
	$items = $filter["items"]; // Items
	$item_filter = substr($items,0, strlen($items)-1); // remove last comma
	$item_filter = str_replace("-", ",", $item_filter);

	if($item_filter)
	{
	
		if($productline_filter)
		{
			$sql_items = "select DISTINCT item_id, concat(item_code, ' ', item_name) as itemname " . 
						 "from order_items ". 
						 "left join orders on order_item_order = order_id " .
				         "left join projects on project_order = order_id " .
						 "left join items on item_id = order_item_item " . 
						 "where item_active = 1 and item_type = 1 " . 
						 " and (order_actual_order_state_code < 820 or item_id IN (" . $item_filter . ")) " . 
				         " and project_product_line IN (" . $productline_filter . ") " .
						 "order by item_code";
		}
		else
		{
			$sql_items = "select DISTINCT item_id, concat(item_code, ' ', item_name) as itemname " . 
						 "from order_items ". 
						 "left join orders on order_item_order = order_id " .
						 "left join items on item_id = order_item_item " . 
						 "where item_active = 1 and item_type = 1 " . 
						 " and (order_actual_order_state_code < 820 or item_id IN (" . $item_filter . ")) " . 
						 "order by item_code";
		}
	}
	else
	{
		if($productline_filter)
		{
			$sql_items = "select DISTINCT item_id, concat(item_code, ' ', item_name) as itemname " . 
						 "from order_items ". 
						 "left join orders on order_item_order = order_id " .
				         "left join projects on project_order = order_id " .
						 "left join items on item_id = order_item_item " . 
						 "where item_active = 1 and item_type = 1 " . 
						 " and order_actual_order_state_code < 820 " . 
				         " and project_product_line IN (" . $productline_filter . ") " .
						 "order by item_code";
		}
		else
		{
			$sql_items = "select DISTINCT item_id, concat(item_code, ' ', item_name) as itemname " . 
						 "from order_items ". 
						 "left join orders on order_item_order = order_id " .
						 "left join items on item_id = order_item_item " . 
						 "where item_active = 1 and item_type = 1 " . 
						 " and order_actual_order_state_code < 820 " . 
						 "order by item_code";
		
		}
	
	}

	
}
*/

if(count($filter["supp"]) > 0)
{
	
	if($productline_filter and $supplier_filter )
	{
		$sql_items = "select DISTINCT item_id, concat(item_code, ' ', item_name) as itemname " . 
					 "from items  " . 
			         "left join category_items on category_item_item = item_id " . 
			         "left join categories on category_id = category_item_category " . 
			         "left join suppliers on supplier_item = item_id ".
					 "where item_type = 1 " . 
			         " and category_product_line IN (" . $productline_filter . ") " .
			         " and supplier_address IN (" . $supplier_filter  . ") " .  
					 " order by item_code";
	}
	elseif($productline_filter)
	{
		$sql_items = "select DISTINCT item_id, concat(item_code, ' ', item_name) as itemname " . 
					 "from items  " . 
			         "left join category_items on category_item_item = item_id " . 
			         "left join categories on category_id = category_item_category " . 
					 "where item_type = 1 " . 
			         " and category_product_line IN (" . $productline_filter . ") " .
					 " order by item_code";
	}
	elseif($supplier_filter )
	{
		$sql_items = "select DISTINCT item_id, concat(item_code, ' ', item_name) as itemname " . 
					 "from items  " . 
			         "left join category_items on category_item_item = item_id " . 
			         "left join categories on category_id = category_item_category " . 
			         "left join suppliers on supplier_item = item_id ".
					 "where item_type = 1 " . 
			         " and supplier_address IN (" . $supplier_filter  . ") " .
					 " order by item_code";
	}
	else
	{
		$sql_items = "select DISTINCT item_id, concat(item_code, ' ', item_name) as itemname " . 
					 "from items  " . 
					 "where item_type = 1 " . 
					 " order by item_code";
	}

}
elseif(array_key_exists("items", $filter))
{
	$items = $filter["items"]; // Items
	$item_filter = substr($items,0, strlen($items)-1); // remove last comma
	$item_filter = str_replace("-", ",", $item_filter);

			
	if($qt == 15) // all items
	{
		if($productline_filter)
		{
			$sql_items = "select DISTINCT item_id, concat(item_code, ' ', item_name) as itemname " . 
						 "from items  " . 
						 "left join category_items on category_item_item = item_id " . 
						 "left join categories on category_id = category_item_category " . 
						 "where and item_type = 1 " . 
						 " and category_product_line IN (" . $productline_filter . ") " .
						 "order by item_code";
		}
		else
		{
			$sql_items = "select DISTINCT item_id, concat(item_code, ' ', item_name) as itemname " . 
						 "from items  " . 
						 "where and item_type = 1 " . 
						 "order by item_code";
		}
	}
	else
	{
		if($productline_filter)
		{
			$sql_items = "select DISTINCT item_id, concat(item_code, ' ', item_name) as itemname " . 
						 "from items  " . 
						 "left join category_items on category_item_item = item_id " . 
						 "left join categories on category_id = category_item_category " . 
						 "where item_active = 1 and item_type = 1 " . 
						 " and category_product_line IN (" . $productline_filter . ") " .
						 "order by item_code";
		}
		else
		{
			$sql_items = "select DISTINCT item_id, concat(item_code, ' ', item_name) as itemname " . 
						 "from items  " . 
						 "where item_active = 1 and item_type = 1 " . 
						 "order by item_code";
		}
	}
}


$countries = $filter["co"]; // Countrie
$country_filter = substr($countries,0, strlen($countries)-1); // remove last comma
$country_filter = str_replace("-", ",", $country_filter);

if($country_filter)
{
	$sql_owners = "select distinct order_franchisee_address_id, " .
				  " concat(address_company, ' - ', country_name) as company " . 
				  " from orders " .
				  " left join addresses on address_id = order_franchisee_address_id " . 
				  " left join countries on country_id = address_country " . 
				  " where order_actual_order_state_code <> '900' and order_type = 1 " .
				  " and address_company <> '' and order_franchisee_address_id > 0 " . 
		          " and country_id IN (" . $country_filter . ") " .
				  " order by address_company, country_name";
}
else
{
	$sql_owners = "select distinct order_franchisee_address_id, " .
				  " concat(address_company, ' - ', country_name) as company " . 
				  " from orders " .
				  " left join addresses on address_id = order_franchisee_address_id " . 
				  " left join countries on country_id = address_country " . 
				  " where order_actual_order_state_code <> '900' and order_type = 1 " .
				  " and address_company <> ''  and order_franchisee_address_id > 0 " . 
				  " order by address_company, country_name";
}




$sql_suppliers = "select DISTINCT address_id, address_company " .
                 "from suppliers " .
				 "left join addresses on address_id = supplier_address " . 
				 "where address_type = 2 " .
				 "order by address_company";



$sql_design_objectives = "select design_objective_item_id, design_objective_item_name, " .
						 "    design_objective_group_name, postype_name " .
						 "from design_objective_items left join design_objective_groups on design_objective_item_group = design_objective_group_id " .
						 "    left join postypes on design_objective_group_postype = postype_id " . 
						 " where postype_name <> '' " . 
						 " order by postype_name, design_objective_group_name, design_objective_item_name ";

/********************************************************************
    save data
*********************************************************************/
if(param("save_form"))
{
	$new_filter = array();
	
	$new_filter["ptst"] =  $filter["ptst"];
	$new_filter["fst"] =  $filter["fst"];
	$new_filter["tst"] =  $filter["tst"];

	$new_filter["fdy"] =  $filter["fdy"];
	$new_filter["fdm"] =  $filter["fdm"];
	$new_filter["tdy"] =  $filter["tdy"];
	$new_filter["tdm"] =  $filter["tdm"];

	$new_filter["fdy1"] =  $filter["fdy1"];
	$new_filter["fdm1"] =  $filter["fdm1"];
	$new_filter["tdy1"] =  $filter["tdy1"];
	$new_filter["tdm1"] =  $filter["tdm1"];


	
	$new_filter["clt"] =  $filter["clt"];
	$new_filter["pl"] =  $filter["pl"];
	$new_filter["plsc"] =  $filter["plsc"];
	$new_filter["pk"] =  $filter["pk"];
	$new_filter["pt"] =  $filter["pt"];
	$new_filter["ptsc"] =  $filter["ptsc"];
	$new_filter["pct"] =  $filter["pct"];
	$new_filter["gr"] =  $filter["gr"];
	$new_filter["re"] =  $filter["re"];
	$new_filter["co"] =  $filter["co"];
	$new_filter["rtc"] =  $filter["rtc"];
	$new_filter["lrtc"] =  $filter["lrtc"];
	$new_filter["rto"] =  $filter["rto"];
	$new_filter["detail"] =  $filter["detail"];

	$new_filter["arch"] = $filter["arch"];

	$new_filter["subfy"] = $filter["subfy"];
	$new_filter["subfm"] = $filter["subfm"];
	$new_filter["subty"] = $filter["subty"];
	$new_filter["subtm"] = $filter["subtm"];
	$new_filter["areas"] = $filter["areas"];
	$new_filter["items"] = $filter["items"];

	$new_filter["owners"] = $filter["owners"];
	$new_filter["supp"] = $filter["supp"];
	$new_filter["dos"] = $filter["dos"];


	if(param("save_form") == "clt") //Client Types
	{
		$CLT = "";

		$res = mysql_query($sql_client_types) or dberror($sql_client_types);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["CLT_" . $row["client_type_id"]])
			{
				$CLT .=$row["client_type_id"] . "-";
			}
		}
		
		$new_filter["clt"] = $CLT;
	}
	elseif(param("save_form") == "pl") //Product Lines
	{
		$PL = "";

		$res = mysql_query($sql_product_lines) or dberror($sql_product_lines);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PL_" . $row["product_line_id"]])
			{
				$PL .=$row["product_line_id"] . "-";
			}
		}
		
		$new_filter["pl"] = $PL;
	}
	elseif(param("save_form") == "plsc") //Product Line Subclasses
	{
		$PLSC = "";

		$res = mysql_query($sql_product_line_subclasses) or dberror($sql_product_line_subclasses);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PLSC_" . $row["productline_subclass_id"]])
			{
				$PLSC .=$row["productline_subclass_id"] . "-";
			}
		}
		
		$new_filter["plsc"] = $PLSC;
	}
	elseif(param("save_form") == "pt") //POS Types
	{
		$PT = "";

		$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PT_" . $row["postype_id"]])
			{
				$PT .=$row["postype_id"] . "-";
			}
		}
		
		$new_filter["pt"] = $PT;
	}
	elseif(param("save_form") == "ptsc") //POS Type Subclasses
	{
		$PTSC = "";

		$res = mysql_query($sql_pos_type_subclasses) or dberror($sql_pos_type_subclasses);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PTSC_" . $row["possubclass_id"]])
			{
				$PTSC .=$row["possubclass_id"] . "-";
			}
		}
		
		$new_filter["ptsc"] = $PTSC;
	}
	elseif(param("save_form") == "pk") //Product Kinds
	{
		$PK = "";

		$res = mysql_query($sql_project_kinds) or dberror($sql_project_kinds);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PK_" . $row["projectkind_id"]])
			{
				$PK .=$row["projectkind_id"] . "-";
			}
		}
		
		$new_filter["pk"] = $PK;
	}
	elseif(param("save_form") == "pct") //project cost types
	{
		$PCT = "";

		$res = mysql_query($sql_project_cost_types) or dberror($sql_project_cost_types);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PCT_" . $row["project_costtype_id"]])
			{
				$PCT .=$row["project_costtype_id"] . "-";
			}
		}
		$new_filter["pct"] = $PCT;
	}
	elseif(param("save_form") == "gr") //Geographicals Regions
	{
		$GR = "";

		$res = mysql_query($sql_salesregion) or dberror($sql_salesregion);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["GR_" . $row["salesregion_id"]])
			{
				$GR .=$row["salesregion_id"] . "-";
			}
		}
		
		$new_filter["gr"] = $GR;
	}
	elseif(param("save_form") == "areas") //neighbourhood Areas
	{
		$AR = "";

		$res = mysql_query($sql_areas) or dberror($sql_areas);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["AR_" . $row["posareatype_id"]])
			{
				$AR .=$row["posareatype_id"] . "-";
			}
		}
		
		$new_filter["areas"] = $AR;
	}
	elseif(param("save_form") == "items") //Items
	{
		$IT = "";

		$res = mysql_query($sql_items) or dberror($sql_items);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["IT_" . $row["item_id"]])
			{
				$IT .=$row["item_id"] . "-";
			}
		}
		
		$new_filter["items"] = $IT;

	}
	elseif(param("save_form") == "re") //Supplying Regions
	{
		$RE = "";

		$res = mysql_query($sql_supplyingregion) or dberror($sql_supplyingregion);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["RE_" . $row["region_id"]])
			{
				$RE .=$row["region_id"] . "-";
			}
		}
		
		$new_filter["re"] = $RE;
	}
	elseif(param("save_form") == "co") //Countries
	{
		$CO = "";

		$res = mysql_query($sql_country) or dberror($sql_country);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["CO_" . $row["country_id"]])
			{
				$CO .=$row["country_id"] . "-";
			}
		}
		
		$new_filter["co"] = $CO;
	}
	elseif(param("save_form") == "rtc") //Store Coordinators
	{
		$RTC = "";

		$res = mysql_query($sql_store_coordinators) or dberror($sql_store_coordinators);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["RTC_" . $row["user_id"]])
			{
				$RTC .=$row["user_id"] . "-";
			}
		}
		
		$new_filter["rtc"] = $RTC;
	}
	elseif(param("save_form") == "lrtc") //Local Store Coordinators
	{
		$LRTC = "";

		$res = mysql_query($sql_local_store_coordinators) or dberror($sql_local_store_coordinators);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["LRTC_" . $row["user_id"]])
			{
				$LRTC .=$row["user_id"] . "-";
			}
		}
		
		$new_filter["lrtc"] = $LRTC;
	}
	elseif(param("save_form") == "rto") //Retail Coordinators
	{
		$RTO = "";

		$res = mysql_query($sql_retail_operators) or dberror($sql_retail_operators);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["RTO_" . $row["user_id"]])
			{
				$RTO .=$row["user_id"] . "-";
			}
		}
		
		$new_filter["rto"] = $RTO;
	}
	elseif(param("save_form") == "owners") //owner companies
	{
		$OWNERS = "";

		$res = mysql_query($sql_owners) or dberror($sql_owners);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["OWNERS_" . $row["order_franchisee_address_id"]])
			{
				$OWNERS .=$row["order_franchisee_address_id"] . "-";
			}
		}
		
		$new_filter["owners"] = $OWNERS;
	}
	elseif(param("save_form") == "supp") //Suppliers
	{
		$SUPP = "";

		$res = mysql_query($sql_suppliers) or dberror($sql_suppliers);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["SUPP_" . $row["address_id"]])
			{
				$SUPP .=$row["address_id"] . "-";
			}
		}
		
		$new_filter["supp"] = $SUPP;
	}
	elseif(param("save_form") == "dos") //Design Objectives
	{
		$DOS = "";

		$res = mysql_query($sql_design_objectives) or dberror($sql_design_objectives);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["DOS_" . $row["design_objective_item_id"]])
			{
				$DOS .=$row["design_objective_item_id"] . "-";
			}
		}
		
		$new_filter["dos"] = $DOS;
	}

	$sql = "update mis_queries " . 
		   "set mis_query_filter = " . dbquote(serialize($new_filter)) . 
		   " where mis_query_id = " . param("query_id");

	$result = mysql_query($sql) or dberror($sql);
	
}

/********************************************************************
    get existant filter
*********************************************************************/
$filter = array();
//get benchmark parameters
$salesregions = array();

$sql = "select mis_query_filter from mis_queries " .
	   "where mis_query_id = " . $query_id;

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{

	$filter = unserialize($row["mis_query_filter"]);

	$client_types = explode("-", $filter["clt"]);
	$product_lines = explode("-", $filter["pl"]);
	$product_line_subclasses = explode("-", $filter["plsc"]);
	$project_kinds = explode("-", $filter["pk"]);
	$pos_types = explode("-", $filter["pt"]);
	$pos_type_subclasses = explode("-", $filter["ptsc"]);
	$project_cost_types = explode("-", $filter["pct"]);
	$salesregions = explode("-", $filter["gr"]);
	$supplyingregions = explode("-", $filter["re"]);
	$countries = explode("-", $filter["co"]);
	$store_coordinators = explode("-", $filter["rtc"]);
	$local_store_coordinators = explode("-", $filter["lrtc"]);
	$retail_operators = explode("-", $filter["rto"]);
	$areas = explode("-", $filter["areas"]);
	$items = explode("-", $filter["items"]);
	$owners = explode("-", $filter["owners"]);
	$suppliers = explode("-", $filter["supp"]);
	$design_objectives = explode("-", $filter["dos"]);
}



/********************************************************************
    Create Form
*********************************************************************/ 

$form = new form("cer_benchmarks", "benchmark_selector");
$form->add_hidden("query_id", $query_id);
$form->add_hidden("qt", $qt);

if(param("s") == "owners") // Owner Companies
{
	$page_title = "Owner/Franchisee Selector";
	$form->add_hidden("save_form", "owners");

	$res = mysql_query($sql_owners) or dberror($sql_owners);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["order_franchisee_address_id"], $owners))
		{
			$form->add_checkbox("OWNERS_" . $row["order_franchisee_address_id"], $row["company"], true);
		}
		else
		{
			$form->add_checkbox("OWNERS_" . $row["order_franchisee_address_id"], $row["company"], false);
		}
		
	}
}
elseif(param("s") == "supp") // Suppliers
{
	$page_title = "Supplier Selector";
	$form->add_hidden("save_form", "supp");

	$res = mysql_query($sql_suppliers) or dberror($sql_suppliers);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["address_id"], $suppliers))
		{
			$form->add_checkbox("SUPP_" . $row["address_id"], $row["address_company"], true);
		}
		else
		{
			$form->add_checkbox("SUPP_" . $row["address_id"], $row["address_company"], false);
		}
		
	}
}
elseif(param("s") == "dos") // Design Objectives
{
	$page_title = "Design Objectives Selector";
	$form->add_hidden("save_form", "dos");

	$res = mysql_query($sql_design_objectives) or dberror($sql_design_objectives);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["design_objective_item_id"], $design_objectives))
		{
			$form->add_checkbox("DOS_" . $row["design_objective_item_id"], $row["postype_name"] . " - " . $row["design_objective_group_name"] . " - " . $row["design_objective_item_name"], true);
		}
		else
		{
			$form->add_checkbox("DOS_" . $row["design_objective_item_id"], $row["postype_name"] . " - " . $row["design_objective_group_name"] . " - " . $row["design_objective_item_name"], false);
		}
		
	}
}
elseif(param("s") == "clt") // Client Types
{
	$page_title = "Client Type Selector";
	$form->add_hidden("save_form", "clt");

	$res = mysql_query($sql_client_types) or dberror($sql_client_types);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["client_type_id"], $client_types))
		{
			$form->add_checkbox("CLT_" . $row["client_type_id"], $row["client_type_code"], true);
		}
		else
		{
			$form->add_checkbox("CLT_" . $row["client_type_id"], $row["client_type_code"], false);
		}
		
	}
}
elseif(param("s") == "pl") // Product Lines
{
	$page_title = "Product Line Selector";
	$form->add_hidden("save_form", "pl");

	$res = mysql_query($sql_product_lines) or dberror($sql_product_lines);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["product_line_id"], $product_lines))
		{
			$form->add_checkbox("PL_" . $row["product_line_id"], $row["product_line_name"], true);
		}
		else
		{
			$form->add_checkbox("PL_" . $row["product_line_id"], $row["product_line_name"], false);
		}
		
	}
}
elseif(param("s") == "plsc") // Product Line Subclasses
{
	$page_title = "Product Line Subclass Selector";
	$form->add_hidden("save_form", "plsc");

	$res = mysql_query($sql_product_line_subclasses) or dberror($sql_product_line_subclasses);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["productline_subclass_id"], $product_line_subclasses))
		{
			$form->add_checkbox("PLSC_" . $row["productline_subclass_id"], $row["productline_subclass_name"], true);
		}
		else
		{
			$form->add_checkbox("PLSC_" . $row["productline_subclass_id"], $row["productline_subclass_name"], false);
		}
		
	}
}
elseif(param("s") == "pk") // Project Kinds
{
	$page_title = "Project Kind Selector";
	$form->add_hidden("save_form", "pk");

	$res = mysql_query($sql_project_kinds) or dberror($sql_project_kinds);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["projectkind_id"], $project_kinds))
		{
			$form->add_checkbox("PK_" . $row["projectkind_id"], $row["projectkind_name"], true);
		}
		else
		{
			$form->add_checkbox("PK_" . $row["projectkind_id"], $row["projectkind_name"], false);
		}
		
	}
}
elseif(param("s") == "pt") // POS Types
{
	$page_title = "POS Type Selector";
	$form->add_hidden("save_form", "pt");

	$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["postype_id"], $pos_types))
		{
			$form->add_checkbox("PT_" . $row["postype_id"], $row["postype_name"], true);
		}
		else
		{
			$form->add_checkbox("PT_" . $row["postype_id"], $row["postype_name"], false);
		}
	}
}
elseif(param("s") == "ptsc") // POS Type Subclass
{
	$page_title = "POS Type Subclass Selector";
	$form->add_hidden("save_form", "ptsc");

	$res = mysql_query($sql_pos_type_subclasses) or dberror($sql_pos_type_subclasses);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["possubclass_id"], $pos_type_subclasses))
		{
			$form->add_checkbox("PTSC_" . $row["possubclass_id"], $row["possubclass_name"], true);
		}
		else
		{
			$form->add_checkbox("PTSC_" . $row["possubclass_id"], $row["possubclass_name"], false);
		}
		
	}
}
elseif(param("s") == "pct") // Project Cost Types
{
	$page_title = "Legal Type Selector";
	$form->add_hidden("save_form", "pct");

	$res = mysql_query($sql_project_cost_types) or dberror($sql_project_cost_types);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["project_costtype_id"], $project_cost_types))
		{
			$form->add_checkbox("PCT_" . $row["project_costtype_id"], $row["project_costtype_text"], true);
		}
		else
		{
			$form->add_checkbox("PCT_" . $row["project_costtype_id"], $row["project_costtype_text"], false);
		}
		
	}
}
elseif(param("s") == "gr") // Sales regions
{
	$page_title = "Geographical Region Selector";
	$form->add_hidden("save_form", "gr");

	$res = mysql_query($sql_salesregion) or dberror($sql_salesregion);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["salesregion_id"], $salesregions))
		{
			$form->add_checkbox("GR_" . $row["salesregion_id"], $row["salesregion_name"], true);
		}
		else
		{
			$form->add_checkbox("GR_" . $row["salesregion_id"], $row["salesregion_name"], false);
		}
		
	}
}
elseif(param("s") == "areas") // Neighbourhood Areas
{
	$page_title = "Neighbourhood Locations Selector";
	$form->add_hidden("save_form", "areas");

	$res = mysql_query($sql_areas) or dberror($sql_areas);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["posareatype_id"], $areas))
		{
			$form->add_checkbox("AR_" . $row["posareatype_id"], $row["posareatype_name"], true);
		}
		else
		{
			$form->add_checkbox("AR_" . $row["posareatype_id"], $row["posareatype_name"], false);
		}
		
	}
}
elseif(param("s") == "items") // items
{
	$page_title = "Item Selector";
	$form->add_hidden("save_form", "items");

	$res = mysql_query($sql_items) or dberror($sql_items);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["item_id"], $items))
		{
			$form->add_checkbox("IT_" . $row["item_id"], $row["itemname"], true);
		}
		else
		{
			$form->add_checkbox("IT_" . $row["item_id"], $row["itemname"], false);
		}
		
	}
}
elseif(param("s") == "re") // Supplying regions
{
	$page_title = "Supplying Region Selector";
	$form->add_hidden("save_form", "re");

	$res = mysql_query($sql_supplyingregion) or dberror($sql_supplyingregion);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["region_id"], $supplyingregions))
		{
			$form->add_checkbox("RE_" . $row["region_id"], $row["region_name"], true);
		}
		else
		{
			$form->add_checkbox("RE_" . $row["region_id"], $row["region_name"], false);
		}
		
	}
}
elseif(param("s") == "co") // countries
{
	$page_title = "Country Selector";
	$form->add_hidden("save_form", "co");

	$res = mysql_query($sql_country) or dberror($sql_country);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["country_id"], $countries))
		{
			$form->add_checkbox("CO_" . $row["country_id"], $row["country_name"], true);
		}
		else
		{
			$form->add_checkbox("CO_" . $row["country_id"], $row["country_name"], false);
		}
		
	}
}
elseif(param("s") == "rtc") // store coordinatros
{
	$page_title = "Project Manager Selector";
	$form->add_hidden("save_form", "rtc");

	$res = mysql_query($sql_store_coordinators) or dberror($sql_store_coordinators);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["user_id"], $store_coordinators))
		{
			$form->add_checkbox("RTC_" . $row["user_id"], $row["username"], true);
		}
		else
		{
			$form->add_checkbox("RTC_" . $row["user_id"], $row["username"], false);
		}
		
	}
}
elseif(param("s") == "lrtc") // localstore coordinatros
{
	$page_title = "Local Project Manager Selector";
	$form->add_hidden("save_form", "lrtc");

	$res = mysql_query($sql_local_store_coordinators) or dberror($sql_local_store_coordinators);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["user_id"], $local_store_coordinators))
		{
			$form->add_checkbox("LRTC_" . $row["user_id"], $row["username"], true);
		}
		else
		{
			$form->add_checkbox("LRTC_" . $row["user_id"], $row["username"], false);
		}
		
	}
}
elseif(param("s") == "rto") // retail operators
{
	$page_title = "Retail Operator Selector";
	$form->add_hidden("save_form", "rto");

	$res = mysql_query($sql_retail_operators) or dberror($sql_retail_operators);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["user_id"], $retail_operators))
		{
			$form->add_checkbox("RTO_" . $row["user_id"], $row["username"], true);
		}
		else
		{
			$form->add_checkbox("RTO_" . $row["user_id"], $row["username"], false);
		}
		
	}
}
$form->add_input_submit("submit", "Save Values", 0);

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Process buttons
*********************************************************************/ 

/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page_Modal("cer_benchmarks");

$page->header();
$page->title($page_title);

$form->render();

if(param("save_form"))
{
?>

<script languege="javascript">
var back_link = "projects_query_filter.php?query_id=<?php echo $query_id;?>&qt=<?php echo $qt;?>"; 
$.nyroModalRemove();
</script>

<?php
}
$page->footer();
