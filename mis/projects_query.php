<?php
/********************************************************************

    project_query.php

    Project Query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2011-07-30
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2011-07-30
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";


check_access("can_perform_queries");

require_once "include/query_get_functions.php";

$qt = 1;
if(param("qt"))
{
	$qt = param("qt");
}

if(param("query_id"))
{
	param("id", param("query_id"));
	$query_id = param("query_id");
}
if(id())
{
	param("query_id",id());
	$query_id = id();
}

$user = get_user(user_id());

//update filter in case elements are missing
if(isset($query_id))
{
	$filter = get_query_filter($query_id);

	$new_filter["ptst"] =  $filter["ptst"];
	$new_filter["fst"] =  $filter["fst"];
	$new_filter["tst"] =  $filter["tst"];

	$new_filter["fdy"] =  $filter["fdy"];
	$new_filter["fdm"] =  $filter["fdm"];
	$new_filter["tdy"] =  $filter["tdy"];
	$new_filter["tdm"] =  $filter["tdm"];

	$new_filter["fdy1"] =  $filter["fdy1"];
	$new_filter["fdm1"] =  $filter["fdm1"];
	$new_filter["tdy1"] =  $filter["tdy1"];
	$new_filter["tdm1"] =  $filter["tdm1"];

	
	$new_filter["clt"] =  $filter["clt"];
	$new_filter["pl"] =  $filter["pl"];
	$new_filter["plsc"] =  $filter["plsc"];
	$new_filter["pk"] =  $filter["pk"];
	$new_filter["pt"] =  $filter["pt"];
	$new_filter["ptsc"] =  $filter["ptsc"];
	$new_filter["pct"] =  $filter["pct"];
	$new_filter["gr"] =  $filter["gr"];
	$new_filter["re"] =  $filter["re"];
	$new_filter["co"] =  $filter["co"];
	$new_filter["rtc"] =  $filter["rtc"];

	if(array_key_exists("lrtc", $filter))
	{
		$new_filter["lrtc"] =  $filter["lrtc"];
	}
	else
	{
		$new_filter["lrtc"] =  "";
	}
	$new_filter["rto"] =  $filter["rto"];


	$new_filter["detail"] =  $filter["detail"];

	
	if(array_key_exists("arch", $filter))
	{
		$new_filter["arch"] =  $filter["arch"];
	}
	else
	{
		$new_filter["arch"] =  "";
	}

	if(array_key_exists("subfy", $filter))
	{
		$new_filter["subfy"] =  $filter["subfy"];
	}
	else
	{
		$new_filter["subfy"] =  "";
	}

	if(array_key_exists("subfm", $filter))
	{
		$new_filter["subfm"] =  $filter["subfm"];
	}
	else
	{
		$new_filter["subfm"] =  "";
	}

	if(array_key_exists("subty", $filter))
	{
		$new_filter["subty"] =  $filter["subty"];
	}
	else
	{
		$new_filter["subty"] =  "";
	}

	if(array_key_exists("subtm", $filter))
	{
		$new_filter["subtm"] =  $filter["subtm"];
	}
	else
	{
		$new_filter["subtm"] =  "";
	}

	if(array_key_exists("cmsst", $filter))
	{
		$new_filter["cmsst"] =  $filter["cmsst"];
	}
	else
	{
		$new_filter["cmsst"] =  "";
	}

	if(array_key_exists("areas", $filter))
	{
		$new_filter["areas"] =  $filter["areas"];
	}
	else
	{
		$new_filter["areas"] =  "";
	}

	if(array_key_exists("items", $filter))
	{
		$new_filter["items"] =  $filter["items"];
	}
	else
	{
		$new_filter["items"] =  "";
	}


	if(array_key_exists("owners", $filter))
	{
		$new_filter["owners"] =  $filter["owners"];
	}
	else
	{
		$new_filter["owners"] =  "";
	}

	if(array_key_exists("supp", $filter))
	{
		$new_filter["supp"] =  $filter["supp"];
	}
	else
	{
		$new_filter["supp"] =  "";
	}

	if(array_key_exists("dos", $filter))
	{
		$new_filter["dos"] =  $filter["dos"];
	}
	else
	{
		$new_filter["dos"] =  "";
	}

		

	$sql = "update mis_queries " . 
			   "set mis_query_filter = " . dbquote(serialize($new_filter)) . 
			   " where mis_query_id = " . param("query_id");

	$result = mysql_query($sql) or dberror($sql);
}

//prepare data


$form = new Form("mis_queries", "Project Query");

$form->add_hidden("query_id", param("query_id"));
$form->add_hidden("qt", $qt);

if($qt == 1) {
	$form->add_hidden("mis_query_context", "projects_in_process");
}
elseif($qt == 2) {
	$form->add_hidden("mis_query_context", "operating_pos");
}
elseif($qt == 3) {
	$form->add_hidden("mis_query_context", "closed_pos");
}
elseif($qt == 4) {
	$form->add_hidden("mis_query_context", "cancelled_projects");
}
elseif($qt == 5) {
	$form->add_hidden("mis_query_context", "projects_state");
}
elseif($qt == 6) {
	$form->add_hidden("mis_query_context", "masterplan");
}
elseif($qt == 7) {
	$form->add_hidden("mis_query_context", "cmsoverview");
}
elseif($qt == 8) {
	$form->add_hidden("mis_query_context", "klapproved");
}
elseif($qt == 9) {
	$form->add_hidden("mis_query_context", "projectsheets");
}
elseif($qt == 10) {
	$form->add_hidden("mis_query_context", "productionplanning");
}
elseif($qt == 11) {
	$form->add_hidden("mis_query_context", "projectmilestones");
}
elseif($qt == 12) {
	$form->add_hidden("mis_query_context", "cmsatatus");
}
elseif($qt == 13) {
	$form->add_hidden("mis_query_context", "transportationcost");
}
elseif($qt == 14) {
	$form->add_hidden("mis_query_context", "posequipment");
}
elseif($qt == 15) {
	$form->add_hidden("mis_query_context", "deliverybyitem");
}




$form->add_edit("mis_query_name", "Name*", NOTNULL);

$form->add_checkbox("mis_print_filter", "Print Query Filter Information in Excel Sheet", "", "", "Filter");

$form->add_hidden("mis_query_owner", user_id());


$sql_persons = "select distinct user_id, concat(user_name, ' ' , user_firstname) as username " . 
       "from users " . 
	   "left join user_roles on user_role_user = user_id  " . 
	   "where user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
	   " and (user_role_role IN (1, 2, 3, 8, 10, 33) or user_address = " . $user["address"] . ") " . 
	   "order by username";



	   

$form->add_section("Access Rights");
$form->add_comment("The following persons can have access to my queries");
//$form->add_checklist("Persons", "Persons", "mis_querypermissions", $sql_persons );
$form->add_checklist("Persons", "Persons", "mis_querypermissions", $sql_persons, 0, "", true );


$form->add_button("save", "Save Query");
if(param("query_id") > 0)
{
	$form->add_button("delete", "Delete Query");
}
$form->add_button("back", "Back to the List of Queries");

$form->populate();
$form->process();

if($form->button("save"))
{
	if($form->validate())
	{
		$form->save();

		
		if(!param("query_id")) // new record
		{
			
			//create filter
			$filter = array();
			
			$filter["ptst"] =  "";
			$filter["fst"] =  "";
			$filter["tst"] =  "";

			$filter["fdy"] =  "";
			$filter["fdm"] =  "";
			$filter["tdy"] =  "";
			$filter["tdm"] =  "";
			$filter["fdy1"] =  "";
			$filter["fdm1"] =  "";
			$filter["tdy1"] =  "";
			$filter["tdm1"] =  "";

		
			$filter["clt"] =  "";
			$filter["pl"] =  "";
			$filter["plsc"] =  "";
			$filter["pk"] =  "";
			$filter["pt"] =  "";
			$filter["ptsc"] =  "";
			$filter["pct"] =  "";
			$filter["gr"] =  "";
			$filter["re"] =  "";
			$filter["co"] =  "";
			$filter["rtc"] =  "";
			$filter["lrtc"] =  "";
			$filter["rto"] =  "";
			$filter["detail"] =  "";
			$filter["arch"] =  "";
			$filter["subfy"] =  "";
			$filter["subfm"] =  "";
			$filter["subty"] =  "";
			$filter["subtm"] =  "";
			$filter["areas"] =  "";
			$filter["items"] =  "";
			$filter["owners"] =  "";
			$filter["supp"] =  "";
			$filter["dos"] =  "";
			
			
			$sql_u = "update mis_queries " . 
				     "set mis_query_filter = " . dbquote(serialize($filter)) . 
				     " where mis_query_id = " . id();

			$result = mysql_query($sql_u) or dberror($sql_u);

			
			$result = mysql_query($sql_u) or dberror($sql_u);
		}


		redirect("projects_query.php?query_id=" .  id() . "&qt=" . $qt);
	}
}
elseif($form->button("delete"))
{
	$sql = "delete from mis_queries where mis_query_id = " . id();
	$result = mysql_query($sql) or dberror($sql);

	$sql = "delete from mis_querypermissions where mis_querypermission_query = " . id();
	$result = mysql_query($sql) or dberror($sql);

	redirect("projects_queries.php?qt=" . $qt);

}
elseif($form->button("back"))
{
	redirect("projects_queries.php?qt=" . $qt);

}


$page = new Page("queries");

require "include/mis_page_actions.php";

$page->header();

if($qt == 1) {
	$page->title(id() ? "Projects in Process: Edit Project Query - Name" : "Projects in Process: Add Project Query");
}
elseif($qt == 2) {
	$page->title(id() ? "Operating POS Locations: Edit Project Query - Name" : "Operating POS Locations: Add Project Query");
}
elseif($qt == 3) {
	$page->title(id() ? "Closed POS Locations: Edit Project Query - Name" : "Closed POS Locations: Add Project Query");
}
elseif($qt == 4) {
	$page->title(id() ? "Cancelled Projects: Edit Project Query - Name" : "Cancelled Projects: Add Project Query");
}
elseif($qt == 5) {
	$page->title(id() ? "Project's State: Edit Project Query - Name" : "Project's State: Add Project Query");
}
elseif($qt == 6) {
	$page->title(id() ? "Master Plan: Edit Project Query - Name" : "Master Plan: Add Project Query");
}
elseif($qt == 7) {
	$page->title(id() ? "CMS Overview: Edit Project Query - Name" : "CMS Overview: Add Project Query");
}
elseif($qt == 8) {
	$page->title(id() ? "Approvals versus real cost: Edit Project Query - Name" : "Approvals versus real cost: Add Project Query");
}
elseif($qt == 9) {
	$page->title(id() ? "Project Sheets: Edit Project Query - Name" : "Project Sheets: Add Project Query");
}
elseif($qt == 10) {
	$page->title(id() ? "Production Order Planning: Edit Project Query - Name" : "Production Order Planning: Add Project Query");
}
elseif($qt == 11) {
	$page->title(id() ? "Project Milestones: Edit Project Query - Name" : "Project Milestones: Add Project Query");
}
elseif($qt == 12) {
	$page->title(id() ? "CMS Statusreport: Edit Project Query - Name" : "CMS Statusreport: Add Project Query");
}
elseif($qt == 13) {
	$page->title(id() ? "Transportation Cost: Edit Project Query - Name" : "Transportation Cost: Add Project Query");
}
elseif($qt == 14) {
	$page->title(id() ? "Equipment of POS Locations: Edit Project Query - Name" : "Equipment of POS Locations: Add Project Query");
}
elseif($qt == 15) {
	$page->title(id() ? "Item Usage: Edit Query - Name" : "Item Usage: Add Query");
}

if(id() > 0 or param("query_id") > 0)
{
	require_once("include/project_query_tabs.php");
}

$form->render();
$page->footer();

?>