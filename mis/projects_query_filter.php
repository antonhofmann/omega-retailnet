<?php
/********************************************************************

    projects_query_filter.php

    Filter Selection for the Query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2011-07-30
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2011-07-30
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";


check_access("can_perform_queries");

require_once "include/query_get_functions.php";

/********************************************************************
    prepare all data needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("projects_queries.php");
}

$qt = 1;
if(param("qt"))
{
	$qt = param("qt");
}

$query_id = param("query_id");

$mis_query = get_query_name($query_id);
$icon = "../pictures/add_item.gif";
$link = "javascript:open_selector('')";


$filter = get_query_filter($query_id);

// create sql for filter criteria
$states = array();
$states[1] = "in progress";
$states[2] = "on hold";
$states[4] = "operating";
$states[6] = "cancelled";


$cms_states = array();
$cms_states[1] = "Completion overdue";
$cms_states[2] = "Approval overdue";


$months = array();
for($i=1;$i<10;$i++)
{
	$months["0" . $i] = "0" . $i;
}
for($i=10;$i<13;$i++)
{
	$months[$i] = $i;
}


if($qt == 2) {
	$sql_years = "select distinct YEAR(project_actual_opening_date) as year " .
				"from projects " .
				"where YEAR(project_actual_opening_date) > 0 ".
				"order by YEAR(project_actual_opening_date) DESC";
}
elseif($qt == 3) {
	$sql_years = "select distinct YEAR(project_shop_closingdate) as year " .
				  "from projects " .
				  "where YEAR(project_shop_closingdate) > 0 ".
				  "order by YEAR(project_shop_closingdate) DESC";
}
elseif($qt == 4) {
	$sql_years = "select distinct YEAR(actual_order_states.date_created) as year " .
					"from projects " .
					"left join orders on order_id = project_order " . 
					"left join actual_order_states on actual_order_state_order = order_id " . 
					"left join order_states on order_state_id = actual_order_state_state " .
					"left join order_state_groups on order_state_group_id = order_state_group " .
					"where order_state_code = '900' and  order_state_group_order_type = 1 ".
					"order by YEAR(actual_order_states.date_created) DESC";
}

if($qt == 7 or $qt == 8 or $qt == 12 or $qt == 13) {
	$sql_years = "select distinct YEAR(project_actual_opening_date) as year " .
				"from projects " .
				"where YEAR(project_actual_opening_date) > 0 ".
				"order by YEAR(project_actual_opening_date) DESC";

	$sql_years1 = "select distinct YEAR(project_shop_closingdate) as year " .
				  "from projects " .
				  "where YEAR(project_shop_closingdate) > 0 ".
				  "order by YEAR(project_shop_closingdate) DESC";
}


$sql_order_states = "select order_state_code, concat(order_state_code, ' ', order_state_name) as name " .
                    "from order_states ". 
					"left join order_state_groups on order_state_group_id = order_state_group " . 
					"where order_state_group_order_type = 1 and order_state_code < '910' " . 
					"order by order_state_code";


$sql_client_types = "select client_type_id, client_type_code ".
					"from client_types";


$sql_product_lines = "select product_line_id, product_line_name ".
                     "from product_lines " . 
                     "where product_line_posindex = 1 " . 
                     "   order by product_line_name";



$sql_product_line_subclasses = "select productline_subclass_id, productline_subclass_name ".
						 "from productline_subclasses " . 
						 "   order by productline_subclass_name";


$sql_project_kinds = "select projectkind_id, projectkind_name ".
						 "from projectkinds " .
						 "where projectkind_id > 0 " .
						 "order by projectkind_name";


$sql_pos_types = "select DISTINCT postype_id, postype_name ".
                     "from postypes " .
                     "order by postype_name";


$sql_pos_type_subclasses = "select possubclass_id, possubclass_name ".
							 "from possubclasses " .
							 "order by possubclass_name";


$sql_project_cost_types = "select project_costtype_id, project_costtype_text ".
						  "from project_costtypes " .
                          "order by project_costtype_text";

$sql_salesregion = "select salesregion_id, salesregion_name ".
               "from salesregions order by salesregion_name";

$sql_supplyingregion = "select region_id, region_name ".
						"from regions order by region_name";

$sql_country = "select DISTINCT country_id, country_name " .
			   "from posaddresses " .
			   "left join countries on country_id = posaddress_country " . 
			   "order by country_name";

$sql_store_coordinators = "select user_id, concat(user_name, ' ', user_firstname) as username " . 
                          "from users " . 
						  "left join user_roles on user_role_user = user_id " . 
						  "where user_role_role = 3 " .
						  " order by username";

$sql_local_store_coordinators = "select user_id, concat(user_name, ' ', user_firstname) as username " . 
                          "from users " . 
						  "left join user_roles on user_role_user = user_id " . 
						  "where user_role_role = 10 " .
						  " order by username";

$sql_retail_operators = "select user_id, concat(user_name, ' ', user_firstname) as username " . 
                          "from users " . 
						  "left join user_roles on user_role_user = user_id " . 
						  "where user_role_role = 2 " .
						  " order by username";


$sql_areas = "select posareatype_id, posareatype_name from posareatypes ". 
             "order by posareatype_name";

$sql_items = "select item_id, item_code from items ". 
             "where item_active = 1 and item_type = 1 " .
             "order by item_code";

$sql_items2 = "select item_id, item_code from items ". 
             "where item_type = 1 " .
             "order by item_code";

$sql_owners = "select distinct order_franchisee_address_id, " .
              " address_company " . 
			  " from orders " .
			  " left join addresses on address_id = order_franchisee_address_id " . 
			  " where order_actual_order_state_code <> '900' and order_type = 1 ".
			  " and order_franchisee_address_id > 0 and address_company <> '' " . 
			  " order by address_company";

$sql_suppliers = "select DISTINCT address_id, address_company " .
                 "from suppliers " .
				 "left join addresses on address_id = supplier_address " . 
				 "where address_type = 2 " .
				 "order by address_company";


$sql_design_objectives = "select design_objective_item_id, design_objective_item_name, " .
						 "    design_objective_group_name, postype_name " .
						 "from design_objective_items left join design_objective_groups on design_objective_item_group = design_objective_group_id " .
						 "    left join postypes on design_objective_group_postype = postype_id " . 
						 " where postype_name <> '' " . 
						 " order by postype_name, design_objective_group_name, design_objective_item_name ";


$sql_delivery_years = "select DISTINCT YEAR(order_item_arrival) as year " . 
                    "from order_items " . 
					" where order_item_type = 1 and YEAR(order_item_arrival) > 0" . 
					" order by year DESC";

//get parameters
$client_types = array();
$product_lines = array();
$product_line_subclasses = array();
$project_kinds = array();
$pos_types = array();
$pos_type_subclasses = array();
$project_cost_types = array();
$salesregions = array();
$supplyingregions = array();
$countries = array();
$store_coordinators = array();
$retail_operators = array();
$areas = array();
$items = array();
$from_state = 0;
$to_state = 0;
$detail = 0;
$ice_dunes_visuals = 0;
$include_archive = 0;
$owners = array();
$suppliers = array();
$design_objectives = array();

$subfy = 0;
$subfm = 0;
$subty = 0;
$subtm = 0;

//check if filter is present
$sql = "select mis_query_filter from mis_queries " .
	   "where mis_query_id = " . $query_id;

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{

	$filters = array();
	$filters = unserialize($row["mis_query_filter"]);

	foreach($filters as $key=>$value)
	{
		
		if(array_key_exists("ptst", $filters))
		{
			$treatment_state = $filters["ptst"];
		}
		if(array_key_exists("fst", $filters))
		{
			$from_state = $filters["fst"];
		}
		if(array_key_exists("tst", $filters))
		{
			$to_state = $filters["tst"];
		}
		if(array_key_exists("fdy", $filters))
		{
			$from_year = $filters["fdy"];
		}
		if(array_key_exists("fdm", $filters))
		{
			$from_month = $filters["fdm"];
		}
		if(array_key_exists("tdy", $filters))
		{
			$to_year = $filters["tdy"];
		}
		if(array_key_exists("tdm", $filters))
		{
			$to_month = $filters["tdm"];
		}

		if(array_key_exists("fdy1", $filters))
		{
			$from_year1 = $filters["fdy1"];
		}
		if(array_key_exists("fdm1", $filters))
		{
			$from_month1 = $filters["fdm1"];
		}
		if(array_key_exists("tdy1", $filters))
		{
			$to_year1 = $filters["tdy1"];
		}
		if(array_key_exists("tdm", $filters))
		{
			$to_month1 = $filters["tdm1"];
		}

		if(array_key_exists("clt", $filters))
		{
			$client_types = explode("-", $filters["clt"]);
		}
		if(array_key_exists("pl", $filters))
		{
			$product_lines = explode("-", $filters["pl"]);
		}

		if(array_key_exists("plsc", $filters))
		{
			$product_line_subclasses = explode("-", $filters["plsc"]);
		}
		if(array_key_exists("pk", $filters))
		{
			$project_kinds = explode("-", $filters["pk"]);
		}
		if(array_key_exists("pt", $filters))
		{
			$pos_types = explode("-", $filters["pt"]);
		}
		if(array_key_exists("ptsc", $filters))
		{
			$pos_type_subclasses = explode("-", $filters["ptsc"]);
		}
		if(array_key_exists("pct", $filters))
		{
			$project_cost_types = explode("-", $filters["pct"]);
		}
		if(array_key_exists("gr", $filters))
		{
			$salesregions = explode("-", $filters["gr"]);
		}
		if(array_key_exists("areas", $filters))
		{
			$areas = explode("-", $filters["areas"]);
		}
		if(array_key_exists("items", $filters))
		{
			$items = explode("-", $filters["items"]);
		}
		if(array_key_exists("re", $filters))
		{
			$supplyingregions = explode("-", $filters["re"]);
		}
		if(array_key_exists("co", $filters))
		{
			$countries = explode("-", $filters["co"]);
		}
		if(array_key_exists("rtc", $filters))
		{
			$store_coordinators = explode("-", $filters["rtc"]);
		}
		if(array_key_exists("lrtc", $filters))
		{
			$local_store_coordinators = explode("-", $filters["lrtc"]);
		}
		if(array_key_exists("rto", $filters))
		{
			$retail_operators = explode("-", $filters["rto"]);
		}
		if(array_key_exists("detail", $filters))
		{
			$detail = $filters["detail"];
		}
		
		if(array_key_exists("arch", $filters))
		{
			$include_archive = $filters["arch"];
		}

		if(array_key_exists("subfy", $filters))
		{
			$subfy = $filters["subfy"];
		}
		if(array_key_exists("subfm", $filters))
		{
			$subfm = $filters["subfm"];
		}
		if(array_key_exists("subty", $filters))
		{
			$subty = $filters["subty"];
		}
		if(array_key_exists("subtm", $filters))
		{
			$subtm = $filters["subtm"];
		}
		
		$cms_state = 0;
		if(array_key_exists("cmsst", $filters))
		{
			$cms_state = $filters["cmsst"];
		}

		if(array_key_exists("owners", $filters))
		{
			$owners = explode("-", $filters["owners"]);
		}

		if(array_key_exists("supp", $filters))
		{
			$suppliers = explode("-", $filters["supp"]);
		}

		if(array_key_exists("dos", $filters))
		{
			$design_objectives = explode("-", $filters["dos"]);
		}
	}
}

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("mis_queries", "query_generator");

$form->add_hidden("query_id", param("query_id"));
$form->add_hidden("qt", $qt);
$form->add_section($mis_query["name"]);

$form->add_section(" ");
$form->add_comment("Please select the the filter criteria of your query.");

$link = "javascript:open_selector('')";

if($qt == 1) {
	$form->add_section("Project States");
	$form->add_hidden("treatment_state");
	$form->add_list("from_state", "From State", $sql_order_states, SUBMIT, $from_state);
	$form->add_list("to_state", "To State", $sql_order_states, SUBMIT, $to_state);
	$form->add_hidden("fdy");
	$form->add_hidden("fdm");
	$form->add_hidden("tdy");
	$form->add_hidden("tdm");
	$form->add_hidden("fdy1");
	$form->add_hidden("fdm1");
	$form->add_hidden("tdy1");
	$form->add_hidden("tdm1");
	$form->add_hidden("detail");
}
elseif($qt == 2) {
	$form->add_hidden("treatment_state");
	$form->add_hidden("from_state");
	$form->add_hidden("to_state");
	$form->add_section("Actual Opening Date");
	$form->add_list("fdy", "Opened From", $sql_years, SUBMIT, $from_year);
	$form->add_list("fdm", "", $months, SUBMIT, $from_month);
	$form->add_list("tdy", "Opened To", $sql_years, SUBMIT, $to_year);
	$form->add_list("tdm", "", $months, SUBMIT, $to_month);
	$form->add_hidden("detail");
	$form->add_hidden("fdy1");
	$form->add_hidden("fdm1");
	$form->add_hidden("tdy1");
	$form->add_hidden("tdm1");
}
elseif($qt == 3) {
	$form->add_hidden("treatment_state");
	$form->add_hidden("from_state");
	$form->add_hidden("to_state");
	$form->add_section("Closing Date");
	$form->add_list("fdy", "Closed From", $sql_years, SUBMIT, $from_year);
	$form->add_list("fdm", "", $months, SUBMIT, $from_month);
	$form->add_list("tdy", "Closed To", $sql_years, SUBMIT, $to_year);
	$form->add_list("tdm", "", $months, SUBMIT, $to_month);
	$form->add_hidden("detail");
	$form->add_hidden("fdy1");
	$form->add_hidden("fdm1");
	$form->add_hidden("tdy1");
	$form->add_hidden("tdm1");
}
elseif($qt == 4) {
	$form->add_hidden("treatment_state");
	$form->add_hidden("from_state");
	$form->add_hidden("to_state");
	$form->add_section("Cancellation Date");
	$form->add_list("fdy", "Cancelled From", $sql_years, 0, $from_year);
	$form->add_list("fdm", "", $months, SUBMIT, $from_month);
	$form->add_list("tdy", "Cancelled To", $sql_years, 0, $to_year);
	$form->add_list("tdm", "", $months, SUBMIT, $to_month);
	$form->add_hidden("detail");
	$form->add_hidden("fdy1");
	$form->add_hidden("fdm1");
	$form->add_hidden("tdy1");
	$form->add_hidden("tdm1");
}
elseif($qt == 5) {
	$form->add_section("Project States");
	$form->add_list("treatment_state", "Project's Treatment State", $states, SUBMIT, $treatment_state);
	$form->add_list("from_state", "From State", $sql_order_states, SUBMIT, $from_state);
	$form->add_list("to_state", "To State", $sql_order_states, SUBMIT, $to_state);
	$form->add_hidden("fdy");
	$form->add_hidden("fdm");
	$form->add_hidden("tdy");
	$form->add_hidden("tdm");
	$form->add_hidden("detail");
	$form->add_hidden("fdy1");
	$form->add_hidden("fdm1");
	$form->add_hidden("tdy1");
	$form->add_hidden("tdm1");
}
elseif($qt == 6) {
	$form->add_section("Project States");
	$form->add_list("treatment_state", "Project's Treatment State", $states, SUBMIT, $treatment_state);
	$form->add_list("from_state", "From State", $sql_order_states, SUBMIT, $from_state);
	$form->add_list("to_state", "To State", $sql_order_states, SUBMIT, $to_state);
	$form->add_hidden("fdy");
	$form->add_hidden("fdm");
	$form->add_hidden("tdy");
	$form->add_hidden("tdm");

	$form->add_section("List Parameters");
	$form->add_checkbox("detail", "Show Details for Suppliers and Forwarders", $detail, SUBMIT, "Order and Delivery");

	$form->add_hidden("fdy1");
	$form->add_hidden("fdm1");
	$form->add_hidden("tdy1");
	$form->add_hidden("tdm1");
}
elseif($qt == 10)
{
	$form->add_section("Project States");
	$form->add_hidden("treatment_state");
	$form->add_list("from_state", "From State", $sql_order_states, SUBMIT, $from_state);
	$form->add_list("to_state", "To State", $sql_order_states, SUBMIT, $to_state);
	$form->add_hidden("fdy");
	$form->add_hidden("fdm");
	$form->add_hidden("tdy");
	$form->add_hidden("tdm");
	$form->add_hidden("fdy1");
	$form->add_hidden("fdm1");
	$form->add_hidden("tdy1");
	$form->add_hidden("tdm1");
	$form->add_hidden("detail");
}
elseif($qt == 11)
{
	$form->add_section("Project States");
	$form->add_hidden("treatment_state");
	$form->add_list("from_state", "From State", $sql_order_states, SUBMIT, $from_state);
	$form->add_list("to_state", "To State", $sql_order_states, SUBMIT, $to_state);
	$form->add_hidden("fdy");
	$form->add_hidden("fdm");
	$form->add_hidden("tdy");
	$form->add_hidden("tdm");
	$form->add_hidden("fdy1");
	$form->add_hidden("fdm1");
	$form->add_hidden("tdy1");
	$form->add_hidden("tdm1");
	$form->add_hidden("detail");
	$form->add_section("Archived Projects");
	$form->add_checkbox("arch", "Include projects from archive", $include_archive, SUBMIT, "Archive");

	$sql_submitted = "select distinct YEAR(date_created) as year " .
              "from projects " .
              "where YEAR(date_created) > 0 ".
              "order by YEAR(date_created) DESC";


	$months = array();
	for($i=1;$i<10;$i++)
	{
		$months["0" . $i] = "0" . $i;
	}
	for($i=10;$i<13;$i++)
	{
		$months[$i] = $i;
	}

	$form->add_section("LN posted or AF/CER posted");
	$form->add_list("subfy", "Posted from", $sql_submitted, SUBMIT, $subfy);
	$form->add_list("subfm", "", $months, SUBMIT, $subfm);
	$form->add_list("subty", "Posted to", $sql_submitted, SUBMIT, $subty);
	$form->add_list("subtm", "", $months, SUBMIT, $subtm);
}
elseif($qt == 15) {
	$form->add_hidden("treatment_state");
	$form->add_hidden("from_state");
	$form->add_hidden("to_state");
	$form->add_hidden("fdm");
	$form->add_hidden("tdm");
	$form->add_hidden("detail");
	$form->add_hidden("fdy1");
	$form->add_hidden("fdm1");
	$form->add_hidden("tdy1");
	$form->add_hidden("tdm1");
}

if($qt != 11) {
	$form->add_hidden("arch");
	$form->add_hidden("subfy");
	$form->add_hidden("subfm");
	$form->add_hidden("subty");
	$form->add_hidden("subtm");
}



$form->add_Section("Selected Filter Criteria");



if($qt == 15)
{
	$form->add_list("fdy", "Delivered from", $sql_delivery_years, SUBMIT, $from_year);
	$form->add_list("tdy", "Delivered to", $sql_delivery_years, SUBMIT, $to_year);

	$selected_countries = "";
	if(count($countries) > 0)
	{
		$res = mysql_query($sql_country) or dberror($sql_country);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["country_id"], $countries))
			{
				$selected_countries .= $row["country_name"] . ", ";
			}
		}
		$selected_countries = substr($selected_countries, 0, strlen($selected_countries) - 2);
	}
	$form->add_label_selector("countries", "Countries", 0, $selected_countries, $icon, $link);

}

if($qt == 14)
{
	$selected_suppliers = "";
	if(count($suppliers) > 0)
	{
		$res = mysql_query($sql_suppliers) or dberror($sql_suppliers);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["address_id"], $suppliers))
			{
				$selected_suppliers .= $row["address_company"] . ", ";
			}
		}
		$selected_suppliers = substr($selected_suppliers, 0, strlen($selected_suppliers) - 2);
	}
	$form->add_label_selector("supp", "Suppliers", 0, $selected_suppliers, $icon, $link);
}

if($qt != 11 and $qt != 14 and $qt != 15)
{

	$selected_client_types = "";
	if(count($client_types) > 0)
	{
		$res = mysql_query($sql_client_types) or dberror($sql_clientypes);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["client_type_id"], $client_types))
			{
				$selected_client_types .= $row["client_type_code"] . ", ";
			}
		}
		$selected_client_types = substr($selected_client_types, 0, strlen($selected_client_types) - 2);
	}
	$form->add_label_selector("client_types", "Client Type", 0, $selected_client_types, $icon, $link);
}
if($qt != 11)
{
	$selected_pls = "";
	if(count($product_lines) > 0)
	{
		$res = mysql_query($sql_product_lines) or dberror($sql_product_lines);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["product_line_id"], $product_lines))
			{
				$selected_pls .= $row["product_line_name"] . ", ";
			}
		}
		$selected_pls = substr($selected_pls, 0, strlen($selected_pls) - 2);
	}
	$form->add_label_selector("pls", "Product Lines", 0, $selected_pls, $icon, $link);
}

if($qt != 11 and $qt != 14 and $qt != 15)
{
	$selected_plscs = "";
	if(count($product_line_subclasses) > 0)
	{
		$res = mysql_query($sql_product_line_subclasses) or dberror($sql_product_line_subclasses);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["productline_subclass_id"], $product_line_subclasses))
			{
				$selected_plscs .= $row["productline_subclass_name"] . ", ";
			}
		}
		$selected_plscs = substr($selected_plscs, 0, strlen($selected_plscs) - 2);
	}
	$form->add_label_selector("plscs", "Product Line Subclasses", 0, $selected_plscs, $icon, $link);
}

if($qt != 14 and $qt != 15)
{
	$selected_pks = "";
	if(count($project_kinds) > 0)
	{
		$res = mysql_query($sql_project_kinds) or dberror($sql_project_kinds);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["projectkind_id"], $project_kinds))
			{
				$selected_pks .= $row["projectkind_name"] . ", ";
			}
		}
		$selected_pks = substr($selected_pks, 0, strlen($selected_pks) - 2);
	}
	$form->add_label_selector("projectkinds", "Project Kinds", 0, $selected_pks, $icon, $link);

	$selected_pts = "";
	if(count($pos_types) > 0)
	{
		$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["postype_id"], $pos_types))
			{
				$selected_pts .= $row["postype_name"] . ", ";
			}
		}
		$selected_pts = substr($selected_pts, 0, strlen($selected_pts) - 2);
	}
	$form->add_label_selector("pts", "POS Types", 0, $selected_pts, $icon, $link);
}

if($qt != 11 and $qt != 14 and $qt != 15)
{
	$selected_pos_type_subclasses = "";
	if(count($pos_type_subclasses) > 0)
	{
		$res = mysql_query($sql_pos_type_subclasses) or dberror($sql_pos_type_subclasses);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["possubclass_id"], $pos_type_subclasses))
			{
				$selected_pos_type_subclasses .= $row["possubclass_name"] . ", ";
			}
		}
		$selected_pos_type_subclasses = substr($selected_pos_type_subclasses, 0, strlen($selected_pos_type_subclasses) - 2);
	}
	$form->add_label_selector("pos_type_subclasses", "POS Type Subclasses", 0, $selected_pos_type_subclasses, $icon, $link);
}

if($qt != 14 and $qt != 15)
{
	$selected_pcts = "";
	if(count($project_cost_types) > 0)
	{
		$res = mysql_query($sql_project_cost_types) or dberror($sql_project_cost_types);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["project_costtype_id"], $project_cost_types))
			{
				$selected_pcts .= $row["project_costtype_text"] . ", ";
			}
		}
		$selected_pcts = substr($selected_pcts, 0, strlen($selected_pcts) - 2);
	}
	$form->add_label_selector("pcts", "Legal Types", 0, $selected_pcts, $icon, $link);
}

if($qt != 11 and $qt != 14 and $qt != 15)
{
	$selected_salesregions = "";
	if(count($salesregions) > 0)
	{
		$res = mysql_query($sql_salesregion) or dberror($sql_salesregion);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["salesregion_id"], $salesregions))
			{
				$selected_salesregions .= $row["salesregion_name"] . ", ";
			}
		}
		$selected_salesregions = substr($selected_salesregions, 0, strlen($selected_salesregions) - 2);
	}
	$form->add_label_selector("salesregions", "Geographical Regions", 0, $selected_salesregions, $icon, $link);


	$selected_supplyingregions = "";
	if(count($supplyingregions) > 0)
	{
		$res = mysql_query($sql_supplyingregion) or dberror($sql_supplyingregion);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["region_id"], $supplyingregions))
			{
				$selected_supplyingregions .= $row["region_name"] . ", ";
			}
		}
		$selected_supplyingregions = substr($selected_supplyingregions, 0, strlen($selected_supplyingregions) - 2);
	}
	$form->add_label_selector("supplyingregions", "Supplying Regions", 0, $selected_supplyingregions, $icon, $link);

	$selected_countries = "";
	if(count($countries) > 0)
	{
		$res = mysql_query($sql_country) or dberror($sql_country);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["country_id"], $countries))
			{
				$selected_countries .= $row["country_name"] . ", ";
			}
		}
		$selected_countries = substr($selected_countries, 0, strlen($selected_countries) - 2);
	}
	$form->add_label_selector("countries", "Countries", 0, $selected_countries, $icon, $link);

	
	
	
	
	if($qt == 6)
	{
		$selected_owners = "";
		if(count($owners) > 0)
		{
			$res = mysql_query($sql_owners) or dberror($sql_owners);
			while($row = mysql_fetch_assoc($res))
			{
				if(in_array($row["order_franchisee_address_id"], $owners))
				{
					$selected_owners .= $row["address_company"] . ", ";
				}
			}
			$selected_owners = substr($selected_owners, 0, strlen($selected_owners) - 2);
		}
		$form->add_label_selector("owners", "Owner/Franchisee", 0, $selected_owners, $icon, $link);
	}
	
	
	$selected_store_coordinators = "";
	if(count($store_coordinators) > 0)
	{
		$res = mysql_query($sql_store_coordinators) or dberror($sql_store_coordinators);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["user_id"], $store_coordinators))
			{
				$selected_store_coordinators .= $row["username"] . ", ";
			}
		}
		$selected_store_coordinators = substr($selected_store_coordinators, 0, strlen($selected_store_coordinators) - 2);
	}
	$form->add_label_selector("storecoordinators", "Project Managers", 0, $selected_store_coordinators, $icon, $link);


	$selected_local_store_coordinators = "";
	if(count($local_store_coordinators) > 0)
	{
		$res = mysql_query($sql_local_store_coordinators) or dberror($sql_local_store_coordinators);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["user_id"], $local_store_coordinators))
			{
				$selected_local_store_coordinators .= $row["username"] . ", ";
			}
		}
		$selected_local_store_coordinators = substr($selected_local_store_coordinators, 0, strlen($selected_local_store_coordinators) - 2);
	}
	$form->add_label_selector("local_storecoordinators", "Local Project Managers", 0, $selected_local_store_coordinators, $icon, $link);



	$selected_areas = "";
	if(count($areas) > 0)
	{
		$res = mysql_query($sql_areas) or dberror($sql_areas);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["posareatype_id"], $areas))
			{
				$selected_areas .= $row["posareatype_name"] . ", ";
			}
		}
		$selected_areas = substr($selected_areas, 0, strlen($selected_areas) - 2);
	}
	$form->add_label_selector("areas", "Neighbourhood Locations", 0, $selected_areas, $icon, $link);
}



if($qt == 11 ) // milestones
{
	$selected_countries = "";
	if(count($countries) > 0)
	{
		$res = mysql_query($sql_country) or dberror($sql_country);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["country_id"], $countries))
			{
				$selected_countries .= $row["country_name"] . ", ";
			}
		}
		$selected_countries = substr($selected_countries, 0, strlen($selected_countries) - 2);
	}
	$form->add_label_selector("countries", "Countries", 0, $selected_countries, $icon, $link);
}

if($qt == 10 or $qt == 14 or $qt == 15) // production order planning or POS Equipment
{
	$selected_items = "";
	$number_of_items = 0;
	if(count($items) > 0)
	{
		$res = mysql_query($sql_items2) or dberror($sql_items2);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["item_id"], $items))
			{
				$selected_items .= $row["item_code"] . ", ";

				$number_of_items++;
				if($number_of_items == 8)
				{
					$selected_items .= "<br />";
				}

			}
		}
		$selected_items = substr($selected_items, 0, strlen($selected_items) - 2);
	}
	$form->add_label_selector("items", "Items", RENDER_HTML, $selected_items, $icon, $link);
}

if($qt == 9 or $qt == 6) {
	
	if($qt == 9) {
		$form->add_hidden("treatment_state");
		$form->add_hidden("from_state");
		$form->add_hidden("to_state");
	}

	$form->add_hidden("fdy");
	$form->add_hidden("fdm");
	$form->add_hidden("tdy");
	$form->add_hidden("tdm");
	$form->add_hidden("fdy1");
	$form->add_hidden("fdm1");
	$form->add_hidden("tdy1");
	$form->add_hidden("tdm1");
	
	if($qt == 9) {
		$form->add_hidden("detail");
	}


	
	$selected_retail_operators = "";
	if(count($selected_retail_operators) > 0)
	{
		$res = mysql_query($sql_retail_operators) or dberror($sql_retail_operators);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["user_id"], $retail_operators))
			{
				$selected_retail_operators .= $row["username"] . ", ";
			}
		}
		$selected_retail_operators = substr($selected_retail_operators, 0, strlen($selected_retail_operators) - 2);
	}
	$form->add_label_selector("retailoperators", "Retail Operators", 0, $selected_retail_operators, $icon, $link);

	
}
elseif($qt == 1 or $qt == 2 or $qt == 3 or $qt == 4)
{
	$selected_retail_operators = "";
	if(count($selected_retail_operators) > 0)
	{
		$res = mysql_query($sql_retail_operators) or dberror($sql_retail_operators);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["user_id"], $retail_operators))
			{
				$selected_retail_operators .= $row["username"] . ", ";
			}
		}
		$selected_retail_operators = substr($selected_retail_operators, 0, strlen($selected_retail_operators) - 2);
	}
	$form->add_label_selector("retailoperators", "Retail Operators", 0, $selected_retail_operators, $icon, $link);
}
else
{
	$form->add_hidden("retailoperators");
}



if($qt == 7 or $qt == 8 or $qt == 13) {
	$form->add_hidden("treatment_state");
	$form->add_hidden("from_state");
	$form->add_hidden("to_state");
	$form->add_section("Actual Opening Year");
	$form->add_list("fdy", "Opened From", $sql_years, SUBMIT, $from_year);
	$form->add_hidden("fdm");
	$form->add_list("tdy", "Opened To", $sql_years, SUBMIT, $to_year);
	$form->add_hidden("tdm");
	$form->add_hidden("detail");

	$form->add_section("Closing Year");
	$form->add_list("fdy1", "Closed From", $sql_years1, SUBMIT, $from_year1);
	$form->add_hidden("fdm1");
	$form->add_list("tdy1", "Closed To", $sql_years1, SUBMIT, $to_year1);
	$form->add_hidden("tdm1");
}


if($qt == 12) {
	$form->add_hidden("treatment_state");
	$form->add_hidden("from_state");
	$form->add_hidden("to_state");

	$form->add_section("Actual Opening Year");
	$form->add_list("fdy", "Opened From", $sql_years, SUBMIT, $from_year);
	$form->add_hidden("fdm");

	$form->add_hidden("fdm");
	$form->add_list("tdy", "Opened To", $sql_years, SUBMIT, $to_year);
	$form->add_hidden("tdm");
	$form->add_hidden("detail");

	$form->add_hidden("fdy1");
	$form->add_hidden("tdy1");
	$form->add_hidden("fdm1");
	$form->add_hidden("tdm1");


	$form->add_section("CMS State");
	$form->add_list("cmsst", "CMS State", $cms_states, SUBMIT, $cms_state);
}
else
{
	$form->add_hidden("cmsst");
}

if($qt == 1  or $qt == 2 or $qt == 3 or $qt == 4  or $qt == 5 or $qt == 6) 
{
	$selected_design_objectives = "";
	if(count($design_objectives) > 0)
	{
		$res = mysql_query($sql_design_objectives) or dberror($sql_design_objectives);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["design_objective_item_id"], $design_objectives))
			{
				$selected_design_objectives .= $row["postype_name"] . " - " . $row["design_objective_group_name"] . " - " . $row["design_objective_item_name"] . ", ";
			}
		}
		$selected_design_objectives = substr($selected_design_objectives, 0, strlen($selected_design_objectives) - 2);
	}
	$form->add_label_selector("design_objectives", "Design Objectives", 0, $selected_design_objectives, $icon, $link);
}


//$form->add_button("save", "Save Filter");

$form->add_button("execute", "Execute Query");

$form->add_button("back", "Back to the List of Queries");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Process buttons
*********************************************************************/ 
if($form->button("save") 
	or $form->button("from_state")
	or $form->button("to_state")
	or $form->button("treatment_state")
	or $form->button("fdy")
	or $form->button("fdm")
	or $form->button("tdy")
	or $form->button("tdm")
	or $form->button("fdy1")
	or $form->button("fdm1")
	or $form->button("tdy1")
	or $form->button("tdm1")
	or $form->button("detail")
	or $form->button("arch")
	or $form->button("subfy")
	or $form->button("subfm")
	or $form->button("subty")
	or $form->button("subtm")
	or $form->button("cmsst")
	)
{
	$new_filter = array();

	$new_filter["ptst"] =  $form->value("treatment_state");
	$new_filter["fst"] =  $form->value("from_state");
	$new_filter["tst"] =  $form->value("to_state");

	$new_filter["fdy"] =  $form->value("fdy");
	$new_filter["fdm"] =  $form->value("fdm");
	$new_filter["tdy"] =  $form->value("tdy");
	$new_filter["tdm"] =  $form->value("tdm");

	$new_filter["fdy1"] =  $form->value("fdy1");
	$new_filter["fdm1"] =  $form->value("fdm1");
	$new_filter["tdy1"] =  $form->value("tdy1");
	$new_filter["tdm1"] =  $form->value("tdm1");

	
	$new_filter["clt"] =  $filter["clt"];
	$new_filter["pl"] =  $filter["pl"];
	$new_filter["plsc"] =  $filter["plsc"];
	$new_filter["pk"] =  $filter["pk"];
	$new_filter["pt"] =  $filter["pt"];
	$new_filter["ptsc"] =  $filter["ptsc"];
	$new_filter["pct"] =  $filter["pct"];
	$new_filter["gr"] =  $filter["gr"];
	$new_filter["re"] =  $filter["re"];
	$new_filter["co"] =  $filter["co"];
	$new_filter["rtc"] =  $filter["rtc"];
	if(array_key_exists("lrtc", $filter))
	{
		$new_filter["lrtc"] =  $filter["lrtc"];
	}
	else
	{
		$new_filter["lrtc"] =  "";
	}
	$new_filter["rto"] =  $filter["rto"];
	$new_filter["detail"] = $form->value("detail");
	$new_filter["arch"] = $form->value("arch");

	$new_filter["subfy"] = $form->value("subfy");
	$new_filter["subfm"] = $form->value("subfm");
	$new_filter["subty"] = $form->value("subty");
	$new_filter["subtm"] = $form->value("subtm");
	

	$new_filter["cmsst"] = $form->value("cmsst");

	$new_filter["areas"] =$filter["areas"];
	$new_filter["items"] =$filter["items"];

	$new_filter["owners"] = $filter["owners"];
	$new_filter["supp"] = $filter["supp"];
	$new_filter["dos"] = $filter["dos"];

	
	$sql = "update mis_queries " . 
		   "set mis_query_filter = " . dbquote(serialize($new_filter)) . 
		   " where mis_query_id = " . param("query_id");

	$result = mysql_query($sql) or dberror($sql);
}
elseif($form->button("back"))
{
	redirect("projects_queries.php?qt=" . $qt);
}
elseif($form->button("execute"))
{
	if($qt == 1) {
		redirect("projects_query_1_xls.php?query_id=" . param("query_id"));
	}
	elseif($qt == 2) {
		redirect("projects_query_2_xls.php?query_id=" . param("query_id"));
	}
	elseif($qt == 3) {
		redirect("projects_query_3_xls.php?query_id=" . param("query_id"));
	}
	elseif($qt == 4) {
		redirect("projects_query_4_xls.php?query_id=" . param("query_id"));
	}
	elseif($qt == 5) {
		redirect("projects_query_5_xls.php?query_id=" . param("query_id"));
	}
	elseif($qt == 6) {
		redirect("projects_query_6_xls.php?query_id=" . param("query_id"));
	}
	elseif($qt == 7) {
		redirect("projects_query_7_xls.php?query_id=" . param("query_id"));
	}
	elseif($qt == 8) {
		redirect("projects_query_8_xls.php?query_id=" . param("query_id"));
	}
	elseif($qt == 9) {
		redirect("projects_query_9_xls.php?query_id=" . param("query_id"));
	}
	elseif($qt == 10) {
		redirect("projects_query_10_xls.php?query_id=" . param("query_id"));
	}
	elseif($qt == 11) {
		redirect("projects_query_11_xls.php?query_id=" . param("query_id"));
	}
	elseif($qt == 12) {
		redirect("projects_query_12_xls.php?query_id=" . param("query_id"));
	}
	elseif($qt == 13) {
		redirect("projects_query_13_xls.php?query_id=" . param("query_id"));
	}
	elseif($qt == 14) {
		redirect("projects_query_14_xls.php?query_id=" . param("query_id"));
	}
	elseif($qt == 15) {
		redirect("projects_query_15_xls.php?query_id=" . param("query_id"));
	}
}

/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page("queries");

require "include/mis_page_actions.php";

$page->header();
if($qt == 1) {
	$page->title("Projects in Process: Edit Project Query - Filter");
}
elseif($qt == 2) {
	$page->title("Operating POS Locations: Edit Project Query - Filter");
}
elseif($qt == 3) {
	$page->title("Closed POS Locations: Edit Project Query - Filter");
}
elseif($qt == 4) {
	$page->title("Cancelled Projects: Edit Project Query - Filter");
}
elseif($qt == 5) {
	$page->title("Project's State: Edit Project Query - Filter");
}
elseif($qt == 6) {
	$page->title("Master Plan: Edit Project Query - Filter");
}
elseif($qt == 7) {
	$page->title("CMS Overview: Edit Project Query - Filter");
}
elseif($qt == 8) {
	$page->title("Approvals versus real cost: Edit Project Query - Filter");
}
elseif($qt == 9) {
	$page->title("Project Sheets: Edit Project Query - Filter");
}
elseif($qt == 10) {
	$page->title("Production Order Planning: Edit Project Query - Filter");
}
elseif($qt == 11) {
	$page->title("Project Milestones: Edit Project Query - Filter");
}
elseif($qt == 12) {
	$page->title("CMS Statusreport: Edit Project Query - Filter");
}
elseif($qt == 13) {
	$page->title("Transportation Cost: Edit Project Query - Filter");
}
elseif($qt == 14) {
	$page->title("Equipment of POS Locations: Edit Query - Filter");
}
elseif($qt == 14) {
	$page->title("Item Usage: Edit Query - Filter");
}


require_once("include/project_query_tabs.php");

$form->render();

?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#client_types_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=clt&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#pls_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=pl&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#plscs_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=plsc&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#projectkinds_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=pk&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#pts_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=pt&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#pos_type_subclasses_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=ptsc&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#pcts_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=pct&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#salesregions_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=gr&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#areas_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=areas&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#items_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=items&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#supplyingregions_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=re&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#countries_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=co&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#storecoordinators_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=rtc&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#local_storecoordinators_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=lrtc&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#retailoperators_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=rto&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#owners_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=owners&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#supp_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=supp&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#design_objectives_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=dos&qt=<?php echo $qt;?>'
    });
    return false;
  });
});
</script>


<?php
$page->footer();
?>