<?php
/********************************************************************

    projects_queries.php

    List all Porjects Queries.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2011-07-30
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2011-07-30
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_perform_queries");
set_referer("projects_query.php");


//create a copy of an existing query
if(param("copy_id"))
{
	$sql = "select * from mis_queries where mis_query_id = " . param("copy_id");
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$sql_i = "Insert into mis_queries (" . 
			     "mis_query_context, " . 
			     "mis_query_owner, " . 
			     "mis_query_name, " . 
			     "mis_query_filter, " . 
			     "user_created, " . 
			     "date_created) VALUES (" . 
			     dbquote($row["mis_query_context"]) . ", " . 
			     user_id() . ", " .
			     dbquote($row["mis_query_name"] .  " - copy") . ", " .
			     dbquote($row["mis_query_filter"]) . ", " .
			     dbquote(user_login()) . ", " .
			     "CURRENT_TIMESTAMP)";
		$result = mysql_query($sql_i);
	}


}

$qt = 1;
if(param("qt"))
{
	$qt = param("qt");
}

//build query for the list
$xls_file = "projects_query_1_xls.php";
$sql = "select mis_query_id, mis_query_name, mis_query_name,  mis_query_owner, " .
       "concat(user_name, ' ', user_firstname) as uname, " .
	   "mis_queries.date_created as cdate, mis_queries.date_modified as mdate " . 
       "from mis_queries " .
	   "left join users on user_id = mis_query_owner ";

$list_filter = "mis_query_context = 'projects_in_process' and mis_query_owner = " . user_id();
if($qt == 1)
{
	$list_filter = "mis_query_context = 'projects_in_process' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'projects_in_process'";
	$xls_file = "projects_query_1_xls.php";
}
elseif($qt == 2)
{
	$list_filter = "mis_query_context = 'operating_pos' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'operating_pos'";
	$xls_file = "projects_query_2_xls.php";
}
elseif($qt == 3)
{
	$list_filter = "mis_query_context = 'closed_pos' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'closed_pos'";
	$xls_file = "projects_query_3_xls.php";
}
elseif($qt == 4)
{
	$list_filter = "mis_query_context = 'cancelled_projects' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'cancelled_projects'";
	$xls_file = "projects_query_4_xls.php";
}
elseif($qt == 5)
{
	$list_filter = "mis_query_context = 'projects_state' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'projects_state'";
	$xls_file = "projects_query_5_xls.php";
}
elseif($qt == 6)
{
	$list_filter = "mis_query_context = 'masterplan' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'masterplan'";
	$xls_file = "projects_query_6_xls.php";
}
elseif($qt == 7)
{
	$list_filter = "mis_query_context = 'cmsoverview' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'cmsoverview'";
	$xls_file = "projects_query_7_xls.php";
}
elseif($qt == 8)
{
	$list_filter = "mis_query_context = 'klapproved' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'klapproved'";
	$xls_file = "projects_query_8_xls.php";
}
elseif($qt == 9)
{
	$list_filter = "mis_query_context = 'projectsheets' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'projectsheets'";
	$xls_file = "projects_query_9_xls.php";
}
elseif($qt == 10)
{
	$list_filter = "mis_query_context = 'productionplanning' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'productionplanning'";
	$xls_file = "projects_query_10_xls.php";
}
elseif($qt == 11)
{
	$list_filter = "mis_query_context = 'projectmilestones' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'projectmilestones'";
	$xls_file = "projects_query_11_xls.php";
}
elseif($qt == 12)
{
	$list_filter = "mis_query_context = 'cmsatatus' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'cmsatatus'";
	$xls_file = "projects_query_12_xls.php";
}
elseif($qt == 13)
{
	$list_filter = "mis_query_context = 'transportationcost' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'transportationcost'";
	$xls_file = "projects_query_13_xls.php";
}
elseif($qt == 14)
{
	$list_filter = "mis_query_context = 'posequipment' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'posequipment'";
	$xls_file = "projects_query_14_xls.php";
}
elseif($qt == 15)
{
	$list_filter = "mis_query_context = 'deliverybyitem' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'deliverybyitem'";
	$xls_file = "projects_query_15_xls.php";
}




$queries = array();
$query_links = array();
$query_permissions = array();
$copy_links = array();

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
	$link = "<a href='" .$xls_file . "?query_id=" .  $row["mis_query_id"] . "'><img src=\"/pictures/ico_xls.gif\" border='0'/></a>";
	$queries[$row["mis_query_id"]] = $link;
	

	

	if(user_id() == $row["mis_query_owner"])
	{
		$query_links[$row["mis_query_id"]] = "<a href='projects_query.php?id=" .  $row["mis_query_id"] . "&qt=" . $qt . "'>" . $row["mis_query_name"] . "</a>";
	}
	else
	{
		$query_links[$row["mis_query_id"]] = $row["mis_query_name"];
	}

	$copy_links[$row["mis_query_id"]] = "<a href='projects_queries.php?copy_id=" .  $row["mis_query_id"] . "'>create copy</a>";

	//get permissions
	
	$sql_p = "select * from mis_querypermissions " .
		     "left join mis_queries on mis_query_id = mis_querypermission_query " . 
			 "where  mis_querypermission_query = " . $row["mis_query_id"] . 
			 " and " . $list_filter2;

	//echo $sql_p . '<br />';
		     

	$res_p = mysql_query($sql_p) or dberror($sql_p);
	while ($row_p = mysql_fetch_assoc($res_p))
	{
		if($row_p["mis_querypermission_user"] == user_id())
		{
			$query_permissions[$row_p["mis_querypermission_query"]] = $row_p["mis_querypermission_query"];
		}
	}
}

if(count($query_permissions) > 0)
{
	$permission_filter =  implode(',', $query_permissions);
	$permission_filter = " or mis_query_id in (" . $permission_filter . ") ";
	$list_filter .= $permission_filter;
}
	


$list = new ListView($sql);

$list->set_entity("mis_queries");
$list->set_order("mis_query_name");
$list->set_filter($list_filter);

$list->add_hidden("qt", $qt);
$list->add_text_column("querylinks", "Query Name", COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $query_links);
$list->add_column("uname", "Owner", "");

$list->add_column("cdate", "Created", "");
$list->add_column("mdate", "Modified", "");

$list->add_text_column("queries", " ", COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $queries);
$list->add_text_column("copy", "", COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $copy_links);

$list->add_button(LIST_BUTTON_NEW, "New Query", "projects_query.php?qt=" . $qt);

$list->process();

$page = new Page("queries");

require "include/mis_page_actions.php";

$page->header();
if($qt == 1) {
	$page->title("Project Queries - Projects in Process");
}
elseif($qt == 2) {
	$page->title("Project Queries - Operating POS Locations");
}
elseif($qt == 3) {
	$page->title("Project Queries - Closed POS Locations");
}
elseif($qt == 4) {
	$page->title("Project Queries - Cancelled Projects");
}
elseif($qt == 5) {
	$page->title("Project Queries - Project's State");
}
elseif($qt == 6) {
	$page->title("Project Queries - Master Plan");
}
elseif($qt == 7) {
	$page->title("Project Queries - CMS Overview");
}
elseif($qt == 8) {
	$page->title("Project Queries - Approvals versus real cost");
}
elseif($qt == 9) {
	$page->title("Project Queries - Project Sheets");
}
elseif($qt == 10) {
	$page->title("Project Queries - Production Order Planning");
}
elseif($qt == 11) {
	$page->title("Project Queries - Project Milestones");
}
elseif($qt == 12) {
	$page->title("Project Queries - CMS Statusreport");
}
elseif($qt == 13) {
	$page->title("Project Queries - Transportation Cost");
}
elseif($qt == 14) {
	$page->title("Project Queries - Equipment of POS Locations");
}
elseif($qt == 15) {
	$page->title("Item Queries - Usage");
}


$list->render();
$page->footer();

?>
