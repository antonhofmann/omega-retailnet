<?php
/********************************************************************

    projects_query_9_xls.php

    Generate List of Project Sheets

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-10-31
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-10-31
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_perform_queries");

require_once "../include/xls/Writer.php";
require_once("projects_query_filter_strings.php");

/********************************************************************
    prepare Data Needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("projects_queries.php");
}


//check if filter is present
$query_name = "";
$print_query_filter = 0;
$sql = "select mis_query_name, mis_query_filter, mis_print_filter from mis_queries " .
	   "where mis_query_id = " . param("query_id");

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{

	$query_name = $row["mis_query_name"];
	if($row["mis_print_filter"] == 1)
	{
		$print_query_filter = 1;
	}

	$filters = array();
	$filters = unserialize($row["mis_query_filter"]);

	foreach($filters as $key=>$value)
	{
		
		$fosc = $filters["fst"]; //from order state
		$tosc = $filters["tst"]; // to order state

		$clt = $filters["clt"]; // Client Types
		$pl = $filters["pl"]; // product lines
		$pls = $filters["plsc"]; // product line subclasses
		$pk = $filters["pk"]; // product kinds
		$pt = $filters["pt"]; // project types
		$suc = $filters["ptsc"]; // POS Type Subclass
		$lpt = $filters["pct"]; // project cost type
		$gr = $filters["gr"]; // Geografical Regions
		$re = $filters["re"]; // Supplying Regions
		$cnt = $filters["co"]; // Countries
		$sc = $filters["rtc"]; // Retail Coordinators
		$lrtc = $filters["lrtc"]; // Local Project managers
		$rto = $filters["rto"]; // Retail Operators

		$fdy = $filters["fdy"]; // Closing Year From
		$fdm = $filters["fdm"]; // Closing Month From
		$tdy = $filters["tdy"]; // Closing Year To
		$tdm = $filters["tdm"]; // Closing Month To

		if(array_key_exists("areas", $filters))
		{
			$areas = $filters["areas"]; // Neighbourhood Locations
		
		}
		else
		{
			$areas = "";
		}

	}
}
else
{
	redirect("projects_queries.php");
}


$states = array(1=>"in process (steps 100-730)", 2=>"in deleivery (steps 740-750)", 3=>"operating (steps 800-820)", 4=>"cancelled (step 900)");

$header = $query_name;

$header = "List of Project Sheets: " . $header . " (" . date("d.m.Y G:i") . ")";

/********************************************************************
    prepare Data
*********************************************************************/
$filter = "";


$filter = "";

$clt = substr($clt,0, strlen($clt)-1); // remove last comma
$clt = str_replace("-", ",", $clt);
if($clt) // client type
{
    $filter =  " where (address_client_type IN (" . $clt . "))";
	$_filter_strings["Client Types"] = get_filter_string("clt", $clt);
}

$pl = substr($pl,0, strlen($pl)-1); // remove last comma
$pl = str_replace("-", ",", $pl);
if($pl and $filter) // product line
{
	$filter .=  " and (project_product_line IN (" . $pl . "))";
	$_filter_strings["Product Lines"] = get_filter_string("pl", $pl);
}
elseif($pl) // product line
{
    $filter =  " where (project_product_line IN (" . $pl . "))";
	$_filter_strings["Product Lines"] = get_filter_string("pl", $pl);
}

$pls = substr($pls,0, strlen($pls)-1); // remove last comma
$pls = str_replace("-", ",", $pls);
if($pls and $filter) // product line subclasses
{
    $filter .=  " and project_product_line_subclass IN (" . $pls . ")";
	$_filter_strings["Product Line Subclasses"] = get_filter_string("pls", $pls);
}
elseif($pls)
{
    $filter =  " where project_product_line_subclass IN (" . $pls . ")";
	$_filter_strings["Product Line Subclasses"] = get_filter_string("pls", $pls);
}

$pk = substr($pk,0, strlen($pk)-1); // remove last comma
$pk = str_replace("-", ",", $pk);

if($pk and $filter) // product line
{
	$filter .=  " and (project_projectkind IN (" . $pk . "))";
	$_filter_strings["Project Kinds"] = get_filter_string("pk", $pk);
}
elseif($pk) // product kind
{
    $filter =  " where (project_projectkind IN (" . $pk . "))";
	$_filter_strings["Project Kinds"] = get_filter_string("pk", $pk);
}


$pt = substr($pt,0, strlen($pt)-1); // remove last comma
$pt = str_replace("-", ",", $pt);
if($pt and $filter) // project type
{
    $filter .=  " and (project_postype IN (" . $pt . "))";
	$_filter_strings["POS Types"] = get_filter_string("pt", $pt);
}
elseif($pt)
{
    $filter =  " where (project_postype IN (" . $pt . "))";
	$_filter_strings["POS Types"] = get_filter_string("pt", $pt);
}

$suc = substr($suc,0, strlen($suc)-1); // remove last comma
$suc = str_replace("-", ",", $suc);
if($suc and $filter) // POS Type Subclass
{
    $filter .=  " and (project_pos_subclass IN (" . $suc . "))";
	$_filter_strings["POS Type Subclasses"] = get_filter_string("suc", $suc);
}
elseif($suc)
{
   $filter =  " where (project_pos_subclass IN (" . $suc . "))";
   $_filter_strings["POS Type Subclasses"] = get_filter_string("suc", $suc);
}

$lpt = substr($lpt,0, strlen($lpt)-1); // remove last comma
$lpt = str_replace("-", ",", $lpt);
if($lpt and $filter) // project cost type
{
    $filter .=  " and (project_cost_type IN (" . $lpt . "))";
	$_filter_strings["Legal Types"] = get_filter_string("lpt", $lpt);
}
elseif($lpt)
{
   $filter =  " where (project_cost_type IN (" . $lpt . "))";
   $_filter_strings["Legal Types"] = get_filter_string("lpt", $lpt);
}




$gr = substr($gr,0, strlen($gr)-1); // remove last comma
$gr = str_replace("-", ",", $gr);
$re = substr($re,0, strlen($re)-1); // remove last comma
$re = str_replace("-", ",", $re);

if($filter) {

	if($gr and $re) 
	{
		$filter .=  " and (country_salesregion IN (" . $gr . ") OR country_region IN (" . $re . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
		$_filter_strings["Supplying Regions"] = get_filter_string("re", $re);
	}
	elseif($gr) // geografical regions
	{
		$filter .=  " and (country_salesregion IN (" . $gr . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
	}
	elseif($re) //supplying
	{
		$filter .=  " and (country_region IN (" . $re . "))";
		$_filter_strings["Supplying Regions"] = get_filter_string("re", $re);
	}
}
else
{
	if($gr and $re) 
	{
		$filter .=  " where (country_salesregion IN (" . $gr . ") OR country_region IN (" . $re . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
		$_filter_strings["Supplying Regions"] = get_filter_string("re", $re);
	}
	elseif($gr) // geografical regions
	{
		$filter .=  " where (country_salesregion IN (" . $gr . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
	}
	elseif($re) //supplying
	{
		$filter .=  " where (country_region IN (" . $re . "))";
		$_filter_strings["Supplying Regions"] = get_filter_string("re", $re);
	}
}

$cnt = substr($cnt,0, strlen($cnt)-1); // remove last comma
$cnt = str_replace("-", ",", $cnt);
if($cnt and $filter) // country
{
    $filter .=  " and (order_shop_address_country IN (" . $cnt . "))";
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);

}
elseif($cnt)
{
    $filter =  " where (order_shop_address_country IN (" . $cnt . "))";
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);
}


$sc = substr($sc,0, strlen($sc)-1); // remove last comma
$sc = str_replace("-", ",", $sc);
if($sc and $filter) // Retail Coordinator
{
    $filter .=  " and (project_retail_coordinator IN (" . $sc . "))";
	$_filter_strings["Project Manager"] = get_filter_string("sc", $sc);

}
elseif($sc)
{
    $filter =  " where (project_retail_coordinator IN (" . $sc . "))";
	$_filter_strings["Project Manager"] = get_filter_string("sc", $sc);
}

$lrtc = substr($lrtc,0, strlen($lrtc)-1); // remove last comma
$lrtc = str_replace("-", ",", $lrtc);
if($lrtc and $filter) // Retail Coordinator
{
    $filter .=  " and (project_local_retail_coordinator IN (" . $lrtc . "))";
	$_filter_strings["Local Project Manager"] = get_filter_string("lrtc", $lrtc);

}
elseif($lrtc)
{
    $filter =  " where (project_local_retail_coordinator IN (" . $lrtc . "))";
	$_filter_strings["Local Project Manager"] = get_filter_string("lrtc", $lrtc);
}

$rto = substr($rto,0, strlen($rto)-1); // remove last comma
$rto = str_replace("-", ",", $rto);
if($rto and $filter) // Retail Operator
{
    $filter .=  " and (order_retail_operator IN (" . $rto . "))";
	$_filter_strings["Retail Operator"] = get_filter_string("rto", $rto);

}
elseif($rto)
{
    $filter =  " where (order_retail_operator IN (" . $rto . "))";
	$_filter_strings["Retail Operator"] = get_filter_string("rto", $rto);
}


if($filter)
{
	$filter .= " and project_is_on_project_sheet_list = 1 and order_actual_order_state_code < '820' ";
}
else
{
	$filter .= " where project_is_on_project_sheet_list = 1 and order_actual_order_state_code < '820' ";
}


//filter posareas get all matching orders
$areas = substr($areas,0, strlen($areas)-1); // remove last comma
$areas = str_replace("-", ",", $areas);

if($areas)
{
	
	$order_ids = array();
	$sql = "select project_order " . 
	       "from projects " .
		   "left join orders on order_id = project_order " .
		   "left join project_costs on project_cost_order = project_order " .
		   "left join project_costtypes on project_costtype_id = project_cost_type " .
		   "left join projectkinds on projectkind_id = project_projectkind " .
		   "left join postypes on postype_id = project_postype " . 
		   "left join product_lines on product_line_id = project_product_line " .
		   "left join addresses on address_id = order_client_address " .
		   "left join countries on country_id = order_shop_address_country " .
		   "left join users as rtcs on rtcs.user_id = project_retail_coordinator " .
		   "left join users as rops on rops.user_id = order_retail_operator " .
		   $filter;
	
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$sql_a = "select DISTINCT posorder_order " . 
			     "from posorders " . 
			     "left join posareas on posorder_posaddress = posarea_posaddress " .
			     "where posorder_order = " . $row["project_order"] . 
			     " and posarea_area IN (" . $areas . ")";

		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
			$order_ids[$row["project_order"]] = $row["project_order"];
		}

		$sql_a = "select DISTINCT posorder_order " . 
			     "from posorderspipeline " . 
			     "left join posareaspipeline on posorder_posaddress = posarea_posaddress " .
			     "where posorder_order = " . $row["project_order"] . 
			     " and posarea_area IN (" . $areas . ")";

		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
			$order_ids[$row["project_order"]] = $row["project_order"];
		}
	
	}

	if(count($order_ids) > 0)
	{
	
		
		if($filter) 
		{
			$filter.=  " and project_order IN (" . implode ( ',' , $order_ids ) . ") ";
		}
		else
		{
			$filter =  " where project_order IN (" . implode ( ',' , $order_ids ) . ") ";
		}
	}
	$_filter_strings["Neighbourhood Locations"] = get_filter_string("areas", $areas);
}

$sql = "select project_id, project_number, project_projectkind, project_order, " .
       "project_cost_type, project_costtype_text, project_cost_sqms, order_actual_order_state_code, postype_name, product_line_name, " .
       "country_name, project_planned_amount_current_year, project_planned_amount_next_year, order_archive_date, projectkind_code, " .
       "order_shop_address_place, order_shop_address_address, order_shop_address_country, country_region, project_approval_date9, " .
	   "project_account_number, project_cost_center, project_opening_date, project_closing_date_string, project_ps_spent_for, " . 
	   "project_ps_product_line, project_share_company, project_share_other, " . 
	   "concat(rtcs.user_name, ' ', rtcs.user_firstname) as rtc, " .
	   "concat(rops.user_name, ' ', rops.user_firstname) as rop " . 
       "from projects " .
       "left join orders on order_id = project_order " .
       "left join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join projectkinds on projectkind_id = project_projectkind " .
       "left join postypes on postype_id = project_postype " . 
       "left join product_lines on product_line_id = project_product_line " .
       "left join addresses on address_id = order_client_address " .
       "left join countries on country_id = order_shop_address_country " .
	   "left join users as rtcs on rtcs.user_id = project_retail_coordinator " .
	   "left join users as rops on rops.user_id = order_retail_operator " .
       $filter . 
       " order by country_name, order_shop_address_place, project_number";


/********************************************************************
    prepare Excel Sheet
*********************************************************************/

$filename = "list_of_project_sheets.xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename); 
$xls->setVersion(8);
$sheet =& $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();


//formats
$header_row =& $xls->addFormat();
$header_row->setSize(10);
$header_row->setAlign('left');
$header_row->setBold();


$f_cellgroup =& $xls->addFormat();
$f_cellgroup->setSize(8);
$f_cellgroup->setAlign('left');
$f_cellgroup->setBorder(1);
$f_cellgroup->setBold();

$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);


$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_number_bold =& $xls->addFormat();
$f_number_bold->setSize(8);
$f_number_bold->setAlign('right');
$f_number_bold->setBorder(1);
$f_number_bold->setBold();

$f_used =& $xls->addFormat();
$f_used->setSize(8);
$f_used->setBorder(1);
$f_used->setpattern(18);
$f_used->setBgColor('yellow');


//captions
$captions = array();
$sheet->setColumn(0, 0, 10);
$captions[] = "Country";
$sheet->setColumn(1, 1, 9);
$captions[] = "Project Number";
$sheet->setColumn(2, 2, 4);
$captions[] = "C=Corporate, F=Franchise";
$sheet->setColumn(3, 3, 10);
$captions[] = "N=New, R=Renovation, \nTR=Take Over/Renovation, \nT=Take Over,  \nL=Lease Renewal";
$sheet->setColumn(4, 4, 40);
$captions[] = "POS Location Address";
$sheet->setColumn(5, 5, 6);
$captions[] = "POS Type";
$sheet->setColumn(6, 6, 7);
$captions[] = "Product Line";
$sheet->setColumn(7, 7, 6);
$captions[] = "Planned Amount Current Year";
$sheet->setColumn(8, 8, 12);
$captions[] = "Project Manager";
$sheet->setColumn(9, 9, 12);
$captions[] = "Retail Operator";
$sheet->setColumn(10, 10, 12);
$captions[] = "Opening Date";
$sheet->setColumn(11, 11, 12);
$captions[] = "Closing Date";




/********************************************************************
    write all data
*********************************************************************/
$sheet->write(0, 0, $header, $header_row);

$row_index = 2;

if($print_query_filter == 1)
{
	foreach($_filter_strings as $key=>$value)
	{
		 $sheet->write($row_index, 0,$key . ": " . $value);
		 $row_index++;
	}
	$row_index++;
}


/********************************************************************
    write all captions
*********************************************************************/
$sheet->writeRow($row_index, 0, $captions, $f_caption);
$sheet->setRow($row_index, 130);
$row_index++;

$cell_index = 0;

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
       
    $shop_address = $row["order_shop_address_place"] . ", " . $row["order_shop_address_address"];

	$sheet->write($row_index, $cell_index, $row["country_name"], $f_normal);

    $sheet->write($row_index, $cell_index+1, $row["project_number"], $f_normal);

	$sheet->write($row_index, $cell_index+2, substr($row["project_costtype_text"], 0, 1), $f_center);

    if($row["project_projectkind"] > 0)
    {
        $sheet->write($row_index, $cell_index+3, $row["projectkind_code"], $f_center);
    }
    else
    {
        $sheet->write($row_index, $cell_index+3, "", $f_center);
           
    }

    $sheet->write($row_index, $cell_index+4, $shop_address, $f_normal);
    $sheet->write($row_index, $cell_index+5, $row["postype_name"], $f_normal);
    $sheet->write($row_index, $cell_index+6, $row["product_line_name"], $f_normal);

    $sheet->write($row_index, $cell_index+7, $row["project_planned_amount_current_year"], $f_number);
	$sheet->write($row_index, $cell_index+8, $row["rtc"], $f_normal);
	$sheet->write($row_index, $cell_index+9, $row["rop"], $f_normal);
	$sheet->write($row_index, $cell_index+10, to_system_date($row["project_opening_date"]), $f_normal);
	$sheet->write($row_index, $cell_index+11, $row["project_closing_date_string"], $f_normal);
	
    $row_index++;
}

$xls->close(); 

?>