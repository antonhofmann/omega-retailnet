<?php
/********************************************************************

    welcome.php

    Main entry page after successful login.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2003-06-22
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-06-22
    Version:        1.0.0

    Copyright (c) 2003, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_perform_queries");


$page = new Page("queries");

require_once "include/mis_page_actions.php";

$page->header();
echo "<p>Welcome to the Managament Information System of " . APPLICATION_NAME .".</p>";


$page->footer();

?>