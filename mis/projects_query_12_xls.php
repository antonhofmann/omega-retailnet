<?php
/********************************************************************

    projects_query_12_xls.php

    Generate Excel File for CMS Status Report of Projects

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2012-02-01
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2012-02-01
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_perform_queries");

require_once "../include/xls/Writer.php";
require_once("projects_query_filter_strings.php");

/********************************************************************
    prepare Data Needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("projects_queries.php");
}


//check if filter is present
$query_name = "";
$print_query_filter = 0;
$sql = "select mis_query_name, mis_query_filter, mis_print_filter from mis_queries " .
	   "where mis_query_id = " . param("query_id");

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{

	$query_name = $row["mis_query_name"];
	if($row["mis_print_filter"] == 1)
	{
		$print_query_filter = 1;
	}

	$filters = array();
	$filters = unserialize($row["mis_query_filter"]);

	foreach($filters as $key=>$value)
	{
		$clt = $filters["clt"]; // Client Types
		$pl = $filters["pl"]; // product lines
		$pls = $filters["plsc"]; // product line subclasses
		$pk = $filters["pk"]; // product kinds
		$pt = $filters["pt"]; // project types
		$suc = $filters["ptsc"]; // POS Type Subclass
		$lpt = $filters["pct"]; // project cost type
		$gr = $filters["gr"]; // Geografical Regions
		$re = $filters["re"]; // Supplying Regions
		$cnt = $filters["co"]; // Countries
		$sc = $filters["rtc"]; // Retail Coordinators
		$lrtc = $filters["lrtc"]; // Local Project managers

		$fdy = $filters["fdy"]; // Opening Year From
		$tdy = $filters["tdy"]; // Opening Year To

		$cms_state = $filters["cmsst"]; // CMS State

		if(array_key_exists("areas", $filters))
		{
			$areas = $filters["areas"]; // Neighbourhood Locations
		
		}
		else
		{
			$areas = "";
		}

	}
}
else
{
	redirect("projects_queries.php");
}



$header = "CMS Statusreport: " . $query_name . " (" . date("d.m.Y G:i") . ")";

/********************************************************************
    prepare Data
*********************************************************************/
$filter = "";

$clt = substr($clt,0, strlen($clt)-1); // remove last comma
$clt = str_replace("-", ",", $clt);
if($clt) // client type
{
    $filter =  " where (address_client_type IN (" . $clt . "))";
	$_filter_strings["Client Types"] = get_filter_string("clt", $clt);
}

$pl = substr($pl,0, strlen($pl)-1); // remove last comma
$pl = str_replace("-", ",", $pl);
if($pl and $filter) // product line
{
	$filter .=  " and (project_product_line IN (" . $pl . "))";
	$_filter_strings["Product Lines"] = get_filter_string("pl", $pl);
}
elseif($pl) // product line
{
    $filter =  " where (project_product_line IN (" . $pl . "))";
	$_filter_strings["Product Lines"] = get_filter_string("pl", $pl);
}

$pls = substr($pls,0, strlen($pls)-1); // remove last comma
$pls = str_replace("-", ",", $pls);
if($pls and $filter) // product line subclasses
{
    $filter .=  " and project_product_line_subclass IN (" . $pls . ")";
	$_filter_strings["Product Line Subclasses"] = get_filter_string("pls", $pls);
}
elseif($pls)
{
    $filter =  " where project_product_line_subclass IN (" . $pls . ")";
	$_filter_strings["Product Line Subclasses"] = get_filter_string("pls", $pls);
}

$pk = substr($pk,0, strlen($pk)-1); // remove last comma
$pk = str_replace("-", ",", $pk);

if($pk and $filter) // product line
{
	$filter .=  " and (project_projectkind IN (" . $pk . "))";
	$_filter_strings["Project Kinds"] = get_filter_string("pk", $pk);
}
elseif($pk) // product kind
{
    $filter =  " where (project_projectkind IN (" . $pk . "))";
	$_filter_strings["Project Kinds"] = get_filter_string("pk", $pk);
}

$pt = substr($pt,0, strlen($pt)-1); // remove last comma
$pt = str_replace("-", ",", $pt);
if($pt and $filter) // project type
{
    $filter .=  " and (project_postype IN (" . $pt . "))";
	$_filter_strings["POS Types"] = get_filter_string("pt", $pt);
}
elseif($pt)
{
    $filter =  " where (project_postype IN (" . $pt . "))";
	$_filter_strings["POS Types"] = get_filter_string("pt", $pt);
}

$suc = substr($suc,0, strlen($suc)-1); // remove last comma
$suc = str_replace("-", ",", $suc);
if($suc and $filter) // POS Type Subclass
{
    $filter .=  " and (project_pos_subclass IN (" . $suc . "))";
	$_filter_strings["POS Type Subclasses"] = get_filter_string("suc", $suc);
}
elseif($suc)
{
   $filter =  " where (project_pos_subclass IN (" . $suc . "))";
   $_filter_strings["POS Type Subclasses"] = get_filter_string("suc", $suc);
}

$lpt = substr($lpt,0, strlen($lpt)-1); // remove last comma
$lpt = str_replace("-", ",", $lpt);
if($lpt and $filter) // project cost type
{
    $filter .=  " and (project_cost_type IN (" . $lpt . "))";
	$_filter_strings["Legal Types"] = get_filter_string("lpt", $lpt);
}
elseif($lpt)
{
   $filter =  " where (project_cost_type IN (" . $lpt . "))";
   $_filter_strings["Legal Types"] = get_filter_string("lpt", $lpt);
}




$gr = substr($gr,0, strlen($gr)-1); // remove last comma
$gr = str_replace("-", ",", $gr);
$re = substr($re,0, strlen($re)-1); // remove last comma
$re = str_replace("-", ",", $re);

if($filter) {

	if($gr and $re) 
	{
		$filter .=  " and (country_salesregion IN (" . $gr . ") OR country_region IN (" . $re . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
		$_filter_strings["Supplying Regions"] = get_filter_string("re", $re);
	}
	elseif($gr) // geografical regions
	{
		$filter .=  " and (country_salesregion IN (" . $gr . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
	}
	elseif($re) //supplying
	{
		$filter .=  " and (country_region IN (" . $re . "))";
		$_filter_strings["Supplying Regions"] = get_filter_string("re", $re);
	}
}
else
{
	if($gr and $re) 
	{
		$filter .=  " where (country_salesregion IN (" . $gr . ") OR country_region IN (" . $re . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
		$_filter_strings["Supplying Regions"] = get_filter_string("re", $re);
	}
	elseif($gr) // geografical regions
	{
		$filter .=  " where (country_salesregion IN (" . $gr . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
	}
	elseif($re) //supplying
	{
		$filter .=  " where (country_region IN (" . $re . "))";
		$_filter_strings["Supplying Regions"] = get_filter_string("re", $re);
	}
}

$cnt = substr($cnt,0, strlen($cnt)-1); // remove last comma
$cnt = str_replace("-", ",", $cnt);
if($cnt and $filter) // country
{
    $filter .=  " and (order_shop_address_country IN (" . $cnt . "))";
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);

}
elseif($cnt)
{
    $filter =  " where (order_shop_address_country IN (" . $cnt . "))";
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);
}


$sc = substr($sc,0, strlen($sc)-1); // remove last comma
$sc = str_replace("-", ",", $sc);
if($sc and $filter) // Retail Coordinator
{
    $filter .=  " and (project_retail_coordinator IN (" . $sc . "))";
	$_filter_strings["Project Manager"] = get_filter_string("sc", $sc);

}
elseif($sc)
{
    $filter =  " where (project_retail_coordinator IN (" . $sc . "))";
	$_filter_strings["Project Manager"] = get_filter_string("sc", $sc);
}

$lrtc = substr($lrtc,0, strlen($lrtc)-1); // remove last comma
$lrtc = str_replace("-", ",", $lrtc);
if($lrtc and $filter) // Retail Coordinator
{
    $filter .=  " and (project_local_retail_coordinator IN (" . $lrtc . "))";
	$_filter_strings["Local Project Manager"] = get_filter_string("lrtc", $lrtc);

}
elseif($lrtc)
{
    $filter =  " where (project_local_retail_coordinator IN (" . $lrtc . "))";
	$_filter_strings["Local Project Manager"] = get_filter_string("lrtc", $lrtc);
}


if($filter) // opened from
{
	if($fdy > 0)
	{
		$filter.=  " and YEAR(project_actual_opening_date) >= " . $fdy;
		$_filter_strings["Opened from"] = $fdy;
	}
}
else
{
	if($fdy > 0)
	{
		$filter.=  " where YEAR(project_actual_opening_date) >= " . $fdy;
		$_filter_strings["Opened from"] = $fdy;
	}
}

if($filter) // opened to
{
	if($tdy > 0)
	{
		$filter.=  " and YEAR(project_actual_opening_date) <= " . $tdy;
		$_filter_strings["Opened to"] = $tdy;
	}
}
else
{
	if($tdy > 0)
	{
		$filter.=  " where YEAR(project_actual_opening_date) <= " . $tdy;
		$_filter_strings["Opened to"] = $tdy;
	}
}


//only open pos locations where CMS is not completed
if($filter) // closed from year
{
    $filter.=  " and project_projectkind IN (1,2,3) ";
	$filter.=  " and (project_cost_cms_completed = 0  or project_cost_cms_approved = 0) ";
	$filter.=  " and project_actual_opening_date <> '0000-00-00' and project_actual_opening_date is not null ";

}
else
{
	$filter=  " where (project_cost_cms_completed = 0  or project_cost_cms_approved = 0) ";
	$filter.=  " and project_actual_opening_date <> '0000-00-00' and project_actual_opening_date is not null ";
	$filter.=  " and project_projectkind IN (1,2,3) ";

	
}

if($cms_state == 1)
{
	$filter.=  " and (project_cost_cms_completion_date is null or project_cost_cms_completion_date = '0000-00-00') "; 
	$_filter_strings["CMS State"] = "Completion overdue";
}
elseif($cms_state == 2)
{
	$filter.=  " and project_cost_cms_completed = 1 and (project_cost_cms_approved_date is null or project_cost_cms_approved_date = '0000-00-00') ";
	$_filter_strings["CMS State"] = "Approival overdue";
}



//filter posareas get all matching orders
$areas = substr($areas,0, strlen($areas)-1); // remove last comma
$areas = str_replace("-", ",", $areas);

if($areas)
{
	
	$order_ids = array();
	$sql = "select project_order " . 
	       "from projects " .
		   "left join orders on order_id = project_order " .
		   "left join project_costs on project_cost_order = project_order " .
		   "left join project_costtypes on project_costtype_id = project_cost_type " .
		   "left join projectkinds on projectkind_id = project_projectkind " . 
		   "left join addresses on address_id = order_client_address " .
		   "left join countries on country_id = order_shop_address_country " .
		   "left join possubclasses on possubclass_id = project_pos_subclass " .
		   "left join users on users.user_id = project_cost_cms_completed_by " .
		   "left join users as rtos on rtos.user_id = order_retail_operator " .
		   "left join users as rtcs on rtcs.user_id = project_retail_coordinator " .
		   $filter;
	
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$sql_a = "select DISTINCT posorder_order " . 
			     "from posorders " . 
			     "left join posareas on posorder_posaddress = posarea_posaddress " .
			     "where posorder_order = " . $row["project_order"] . 
			     " and posarea_area IN (" . $areas . ")";

		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
			$order_ids[$row["project_order"]] = $row["project_order"];
		}

		$sql_a = "select DISTINCT posorder_order " . 
			     "from posorderspipeline " . 
			     "left join posareaspipeline on posorder_posaddress = posarea_posaddress " .
			     "where posorder_order = " . $row["project_order"] . 
			     " and posarea_area IN (" . $areas . ")";

		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
			$order_ids[$row["project_order"]] = $row["project_order"];
		}
	
	}

	if(count($order_ids) > 0)
	{
	
		
		if($filter)
		{
			$filter.=  " and project_order IN (" . implode ( ',' , $order_ids ) . ") ";
		}
		else
		{
			$filter =  " where project_order IN (" . implode ( ',' , $order_ids ) . ") ";
		}
	}
	$_filter_strings["Neighbourhood Locations"] = get_filter_string("areas", $areas);
}

$sql_d = "select project_id, project_number, project_projectkind, project_order, " .
       "DATE_FORMAT(project_actual_opening_date, '%d.%m.%Y') as openingdate, " .
	   "DATE_FORMAT(project_shop_closingdate, '%d.%m.%Y') as closingdate, " .
	   "DATE_FORMAT(project_cost_cms_completion_date, '%d.%m.%Y') as completed, " .
	   "DATE_FORMAT(project_cost_cms_approved_date, '%d.%m.%Y') as approved, " .
       "project_cost_type, project_costtype_text, projectkind_code, project_cost_sqms, order_actual_order_state_code, " .
       "country_name, project_planned_amount_current_year, order_archive_date, " .
       "project_actual_opening_date, project_shop_closingdate, order_cancelled, " .
       "order_shop_address_place, order_shop_address_company, order_shop_address_country, country_region, " .
	   "possubclass_name, project_cost_cms_completion_date, " . 
	   " concat(users.user_name, ' ' , users.user_firstname) as username, " .
	   " concat(rtos.user_name, ' ' , rtos.user_firstname) as rto, " .
	   " concat(rtcs.user_name, ' ' , rtcs.user_firstname) as rtc " .
       "from projects " .
       "left join orders on order_id = project_order " .
       "left join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join projectkinds on projectkind_id = project_projectkind " . 
       "left join addresses on address_id = order_client_address " .
       "left join countries on country_id = order_shop_address_country " .
	   "left join possubclasses on possubclass_id = project_pos_subclass " .
	   "left join users on users.user_id = project_cost_cms_completed_by " .
	   "left join users as rtos on rtos.user_id = order_retail_operator " .
	   "left join users as rtcs on rtcs.user_id = project_retail_coordinator " .
       $filter . 
       " order by country_name, order_shop_address_place, project_number";

/********************************************************************
    prepare Excel Sheet
*********************************************************************/

$filename = "cms_statusreport_" . date("Ymd") . ".xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename); 
$xls->setVersion(8);
$sheet =& $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();


//formats
$header_row =& $xls->addFormat();
$header_row->setSize(10);
$header_row->setAlign('left');
$header_row->setBold();


$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);


$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);


$xls->setCustomColor(12, 219, 17, 17);
$f_number_red =& $xls->addFormat();
$f_number_red->setSize(8);
$f_number_red->setAlign('right');
$f_number_red->setBorder(1);
$f_number_red->setColor("white");
$f_number_red->setFgColor(12);
$f_number_red->setPattern(1);
$f_number_red->setBold();

$xls->setCustomColor(13, 246, 194, 54);
$f_number_orange =& $xls->addFormat();
$f_number_orange->setSize(8);
$f_number_orange->setAlign('right');
$f_number_orange->setBorder(1);
$f_number_orange->setFgColor(13);
$f_number_orange->setPattern(1);
$f_number_orange->setBold();

$xls->setCustomColor(14, 255, 246, 14);
$f_number_yellow =& $xls->addFormat();
$f_number_yellow->setSize(8);
$f_number_yellow->setAlign('right');
$f_number_yellow->setBorder(1);
$f_number_yellow->setFgColor(14);;
$f_number_yellow->setPattern(1);
$f_number_yellow->setBold();


$xls->setCustomColor(15, 218, 218, 218);
$f_number_grey =& $xls->addFormat();
$f_number_grey->setSize(8);
$f_number_grey->setAlign('right');
$f_number_grey->setBorder(1);
$f_number_grey->setFgColor(15);;
$f_number_grey->setPattern(1);
$f_number_grey->setBold();

//captions
$captions = array();
$captions[] = "No.";
$sheet->setColumn(0, 0, 3);

$captions[] = "Country";
$sheet->setColumn(1, 1, 16);

$captions[] = "Project Name";
$sheet->setColumn(2, 2, 50);
$captions[] = "Project Number";
$captions[] = "Opened";

$sheet->setColumn(5, 5, 4);
$captions[] = "C=Corporate, F=Franchise";

$sheet->setColumn(6, 6, 6);
$captions[] = "N=New, R=Renovation, TR=Take Over/Renovation";

$sheet->setColumn(7, 7, 12);
$captions[] = "POS Type Subclass";

$sheet->setColumn(8, 8, 14);
$captions[] = "Project Manager";

$sheet->setColumn(9, 9, 14);
$captions[] = "Retail Operator";

if($cms_state != 1)
{
	$captions[] = "CMS Completed";

	$captions[] = "Completed by";
	$sheet->setColumn(11,11, 14);
}

if($cms_state == 1)
{
	$captions[] = "Completion Overdue";
	$sheet->setColumn(10, 10, 4);
}
elseif($cms_state == 2)
{
	$captions[] = "Approval Overdue";
	$sheet->setColumn(12, 12, 4);
}
else
{
	$captions[] = "Completion Overdue";
	$sheet->setColumn(12, 12, 4);
	$captions[] = "Approval Overdue";
	$sheet->setColumn(13, 13, 4);
}




/********************************************************************
    write all data
*********************************************************************/
$sheet->write(0, 0, $header, $header_row);

$row_index = 2;

if($print_query_filter == 1)
{
	foreach($_filter_strings as $key=>$value)
	{
		 $sheet->write($row_index, 0,$key . ": " . $value);
		 $row_index++;
	}
	$row_index++;
}

/********************************************************************
    write all captions
*********************************************************************/
$sheet->writeRow($row_index, 0, $captions, $f_caption);
$sheet->setRow($row_index, 110);
$row_index++;

$cell_index = 1;

$c01 = 0;
$c02 = 0;
$c03 = 0;
$c04 = 0;

$res = mysql_query($sql_d) or dberror($sql_d);
while ($row = mysql_fetch_assoc($res))
{
     
    $shop_address = $row["order_shop_address_company"] . ", " . $row["order_shop_address_place"];

	$opening_date = $row["project_actual_opening_date"];
	$completion_date = $row["project_cost_cms_completion_date"];
	$today = date("Y-m-d");

	$overdue_completed = "";
	if(!$row["completed"])
	{
		$overdue_completed = round((strtotime($today) - strtotime($opening_date)) / ( 60 * 60 * 24), 0);
	}

	$overdue_approved = "";
	if($row["completed"])
	{
		$overdue_approved = round((strtotime($today) - strtotime($completion_date)) / ( 60 * 60 * 24), 0);
	}



	$sheet->write($row_index, 0, $row_index-1, $f_number);

            
    $sheet->write($row_index, $cell_index, $row["country_name"], $f_normal);
    $sheet->write($row_index, $cell_index+1, $shop_address, $f_normal);

    
    $sheet->write($row_index, $cell_index+2, $row["project_number"], $f_normal);
    
        
	$sheet->write($row_index, $cell_index+3, $row["openingdate"], $f_normal);
	
    $sheet->write($row_index, $cell_index+4, substr($row["project_costtype_text"], 0, 1), $f_center);

    if($row["project_projectkind"] > 0)
    {
        $sheet->write($row_index, $cell_index+5, $row["projectkind_code"], $f_center);
    }
    else
    {
        $sheet->write($row_index, $cell_index+5, "", $f_center);
           
    }

	$sheet->write($row_index, $cell_index+6, $row["possubclass_name"], $f_normal);

	$sheet->write($row_index, $cell_index+7, $row["rtc"], $f_normal);

	$sheet->write($row_index, $cell_index+8, $row["rto"], $f_normal);

	
	if($cms_state == 1)
	{
		if($overdue_completed > 90 )
		{
			$sheet->write($row_index, $cell_index+9, $overdue_completed, $f_number_red);
		}
		elseif($overdue_completed > 60 )
		{
			$sheet->write($row_index, $cell_index+9, $overdue_completed, $f_number_orange);
		}
		elseif($overdue_completed > 30 )
		{
			$sheet->write($row_index, $cell_index+9, $overdue_completed, $f_number_yellow);
		}
		else
		{
			if($overdue_completed > 0)
			{
				$sheet->write($row_index, $cell_index+9, $overdue_completed, $f_number_grey);
			}
			else
			{
				$sheet->write($row_index, $cell_index+9, $overdue_completed, $f_number);
			}
		}
	}
	elseif($cms_state == 2)
	{
		$sheet->write($row_index, $cell_index+9, $row["completed"], $f_normal);
		$sheet->write($row_index, $cell_index+10, $row["username"], $f_normal);

		if($overdue_approved > 90 )
		{
			$sheet->write($row_index, $cell_index+11, $overdue_approved, $f_number_red);
		}
		elseif($overdue_approved > 60 )
		{
			$sheet->write($row_index, $cell_index+11, $overdue_approved, $f_number_orange);
		}
		elseif($overdue_approved > 30 )
		{
			$sheet->write($row_index, $cell_index+11, $overdue_approved, $f_number_yellow);
		}
		else
		{
			if($overdue_approved > 0)
			{
				$sheet->write($row_index, $cell_index+11, $overdue_approved, $f_number_grey);
			}
			else
			{
				$sheet->write($row_index, $cell_index+11, $overdue_approved, $f_number);
			}
		}
	}
	else
	{
		$sheet->write($row_index, $cell_index+9, $row["completed"], $f_normal);
		$sheet->write($row_index, $cell_index+10, $row["username"], $f_normal);

		if($overdue_completed > 90 )
		{
			$sheet->write($row_index, $cell_index+11, $overdue_completed, $f_number_red);
		}
		elseif($overdue_completed > 60 )
		{
			$sheet->write($row_index, $cell_index+11, $overdue_completed, $f_number_orange);
		}
		elseif($overdue_completed > 30 )
		{
			$sheet->write($row_index, $cell_index+11, $overdue_completed, $f_number_yellow);
		}
		else
		{
			if($overdue_completed > 0)
			{
				$sheet->write($row_index, $cell_index+11, $overdue_completed, $f_number_grey);
			}
			else
			{
				$sheet->write($row_index, $cell_index+11, $overdue_completed, $f_number);
			}
		}

		
		if($overdue_approved > 90 )
		{
			$sheet->write($row_index, $cell_index+12, $overdue_approved, $f_number_red);
		}
		elseif($overdue_approved > 60 )
		{
			$sheet->write($row_index, $cell_index+12, $overdue_approved, $f_number_orange);
		}
		elseif($overdue_approved > 30 )
		{
			$sheet->write($row_index, $cell_index+12, $overdue_approved, $f_number_yellow);
		}
		else
		{
			if($overdue_approved > 0)
			{
				$sheet->write($row_index, $cell_index+12, $overdue_approved, $f_number_grey);
			}
			else
			{
				$sheet->write($row_index, $cell_index+12, $overdue_approved, $f_number);
			}
		}
	}
        
	
	
    $row_index++;
}


$xls->close(); 

?>