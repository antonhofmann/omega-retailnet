<?php
/********************************************************************

    mis_page_actions.php

    Defines the possible page actions depending on access rights

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2003-06-22
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-06-25
    Version:        1.0.0

    Copyright (c) 2003, Swatch AG, All Rights Reserved.

*********************************************************************/

if(has_access("can_perform_all_queries") or has_access("can_perform_projectstate_query"))
{
	$page->register_action('projects_state', 'Project\'s State', "projects_queries.php?qt=5");
}

if(has_access("can_perform_all_queries") or has_access("can_perform_project_inprogress_query"))
{
	$page->register_action('projects_inprogress', 'Projects in Process', "projects_queries.php?qt=1");
}

if(has_access("can_perform_all_queries") or has_access("can_perform_project_operating_query"))
{
	$page->register_action('projects_operating', 'Operating POS', "projects_queries.php?qt=2");
}

if(has_access("can_perform_all_queries") or has_access("can_perform_project_closed_query"))
{
	$page->register_action('projects_closed', 'Closed POS', "projects_queries.php?qt=3");

}
if(has_access("can_perform_all_queries") or has_access("can_perform_project_cancelled_query"))
{
	$page->register_action('projects_cancelled', 'Cancelled Projects', "projects_queries.php?qt=4");
}	

if(has_access("can_perform_all_queries") or has_access("can_perform_masterplan_query"))
{
	$page->register_action('nothing0', '', "");
	$page->register_action('masterplan', 'Projects Masterplan', "projects_queries.php?qt=6");
}

/*
if(has_access("can_perform_all_queries") or has_access("can_perform_cmsoverview_query"))
{
	$page->register_action('nothing1', '', "");
	$page->register_action('cms_overview', 'CMS Overview', "projects_queries.php?qt=7");
}

if(has_access("can_perform_all_queries") or has_access("can_perform_statusreport_query"))
{
	$page->register_action('cms_progress', 'CMS Statusreport', "projects_queries.php?qt=12");
}
*/


if(has_access("can_perform_all_queries") or has_access("can_perform_klcost_query"))
{
	$page->register_action('cms_projects', 'Approvals versus Real Costs', "projects_queries.php?qt=8");
}

if(has_access("can_perform_all_queries") or has_access("can_perform_project_milestones_query"))
{
	$page->register_action('project_milestones', 'Project Milestones', "projects_queries.php?qt=11");
}

if(has_access("can_perform_all_queries") or has_access("can_perform_transportation_cost_query"))
{
	$page->register_action('transportation_cost', 'Transportation Cost', "projects_queries.php?qt=13");
}

if(has_access("can_perform_all_queries") or has_access("can_perform_project_sheets_query"))
{	
	$page->register_action('nothing2', '', "");
	$page->register_action('list_of_project_sheets', 'List of Project Sheets', "projects_queries.php?qt=9");
}

if(has_access("can_perform_all_queries") or has_access("can_perform_production_planning_query"))
{
	$page->register_action('nothing3', '', "");
	$page->register_action('production_order_planning', 'Production Order Planning', "projects_queries.php?qt=10");
}

if(has_access("can_perform_all_queries") or has_access("can_perform_delivered_query"))
{
	$page->register_action('nothing4', '', "");
	$page->register_action('delivered_by_item', 'Delivered Items', "projects_queries.php?qt=15");
	
	//$page->register_action('delivered_by_item', 'Delivery by Item', "delivered_by_item.php");
	//$page->register_action('delivered_by_country', 'Delivery by Country', "delivered_by_country.php");
}

if(has_access("can_perform_all_queries") or has_access("can_perform_equipment_query"))
{
	$page->register_action('pos_equipment', 'Equipment of POS Locations', "projects_queries.php?qt=14");
	$page->register_action('nothing6', '', "");
}

if(has_access("can_perform_all_queries") or has_access("can_perform_items_in_progress_query"))
{
	$page->register_action('items_in_progress', 'Items in Progress', "items_in_progress.php");
}


if(has_access("can_perform_all_queries") or has_access("can_perform_queries"))
{
	$page->register_action('nothing4', '', "");

	$page->register_action('traffic_by_modules', 'Traffic by Modules', "traffic_by_modules.php");
	$page->register_action('traffic_by_company', 'Traffic by Company', "traffic_by_company.php");
	$page->register_action('traffic_by_company', 'Traffic by User', "traffic_by_user.php");
}

$page->register_action('nothing5', '', "");
$page->register_action('home', 'Home', "welcome.php");

?>