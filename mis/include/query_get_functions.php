<?php
/********************************************************************

    query_get_functions.php

    Various utility functions to get information from tables.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2011-07-22
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2011-07-22
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/


/********************************************************************
    get the field values of a user
*********************************************************************/
function get_user($id)
{
    $user = array();

    if ($id == '')
    {
        $user["firstname"] = "";
        $user["name"] = "";
        $user["contact"] = "";
        $user["phone"] = "";
        $user["fax"] = "";
        $user["email"] = "";
        $user["cc"] = "";
        $user["deputy"] = "";
        $user["address"] = "";
		$user["country"] = "";
		$user["user_id"] = "";
		$user["password"] = "";
    }
    else
    {
        $sql = "select * from users " .
			   "left join addresses on address_id = user_address " . 
			   "where user_id = " . $id;

        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $user["firstname"] = $row["user_firstname"];
            $user["name"] = $row["user_name"];
            $user["contact"] = $row["user_name"] . " " . $row["user_firstname"];
            $user["phone"] = $row["user_phone"];
            $user["fax"] = $row["user_fax"];
            $user["email"] = $row["user_email"];
            $user["cc"] = $row["user_email_cc"];
            $user["deputy"] = $row["user_email_deputy"];
            $user["address"] = $row["user_address"];
			$user["country"] = $row["address_country"];
			$user["user_id"] = $row["user_login"];
			$user["password"] = $row["user_password"];
        }
    }
    return $user;
}


/********************************************************************
    get DB Info for POS Address Fields
*********************************************************************/
function get_query_filter($query_id)
{
	$filter = array();
	
	$sql = "select mis_query_filter from mis_queries " .
	       "where mis_query_id = " . $query_id;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{

		$filter = unserialize($row["mis_query_filter"]);

	}
	return $filter;
}


/********************************************************************
    get query name
*********************************************************************/
function get_query_name($id)
{
    $query = array();
	
	$sql = "select * from mis_queries " .
		   "where mis_query_id = " . $id;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$query["name"] = $row["mis_query_name"];
	}
    return $query;
}

?>