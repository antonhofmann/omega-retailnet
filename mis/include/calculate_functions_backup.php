<?php
/********************************************************************

    calculate_functions.php

    Various utility functions to get information from tables.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2003-06-22
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-01-08
    Version:        1.1.0

    Copyright (c) 2003, Swatch AG, All Rights Reserved.

*********************************************************************/

/*************************************************************************
   get item consumption data
*************************************************************************/
function get_item_total_consumption_data($item_id, $from_date, $to_date, $country, $order_id)
{

    $item_consumption = array();
    $turn_over = array();
    $turn_over_1 = array();

    // evaluate parameters
    $date_filter1 = "";
    $date_filter2 = "";
    $country_filter = "";
    $order_filter = "";

    if($from_date)
    {
        $from_date = from_system_date($from_date);
        $date_filter1 = "    and order_item_arrival >=  '" . $from_date . "'";
    }

    if($to_date)
    {
        $to_date = from_system_date($to_date);
        $date_filter2 = "    and order_item_arrival <=  '" . $to_date . "'";
    }

    if($country)
    {
        $country_filter = "   and order_shop_address_country = " . $country;
    }

    if($order_id)
    {
        $order_filter = "   and order_id= " . $order_id;
    }

    // calculate quantities having actual arrival date
    $item_consumption["item_consumption"] = number_format(0,2);
    $item_consumption["turn_over"] = number_format(0,2);
    $item_consumption["turn_over_1"] = 0;

    $sql = "Select sum(order_item_quantity) as quantity, " .
           "   sum(order_item_quantity * order_item_system_price) as turnover ".
           "from order_items ".
           "left join orders on order_item_order = order_id ".
           "where order_item_item= " . $item_id .
           "    and order_item_arrival > '0000-00-00' " .
           "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
           $date_filter1 . $date_filter2 .
           $country_filter .
           $order_filter.
           "    and order_cancelled is null";

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $item_consumption["item_consumption"] = number_format($row["quantity"],2);
        $item_consumption["turn_over"] = number_format($row["turnover"], 2);
        $item_consumption["item_consumption_1"] = $row["quantity"];
        $item_consumption["turn_over_1"] = $row["turnover"];
    }
    

    return $item_consumption;

}


/*************************************************************************
   get chart data
*************************************************************************/
function get_item_chart_consumption_data($item_id, $from_date, $to_date, $country, $order_id)
{

    $i = 0;
	
	$chart_item_consumption = array();
	
    // evaluate parameters
    $chart_date_filter1 = "";
    $chart_date_filter2 = "";
	$chart_date_filter3 = "";
    $chart_country_filter = "";
    $chart_order_filter = "";

    if($from_date)
    {
        $from_date = from_system_date($from_date);
        $chart_date_filter1 = "    and order_item_arrival >=  '" . $from_date . "'";
		$form_year = date("Y", $from_date);
	}

    if($to_date)
    {
        $to_date = from_system_date($to_date);
        $chart_date_filter2 = "    and order_item_arrival <=  '" . $to_date . "'";
    }

    if($country)
    {
        $chart_country_filter = "   and order_shop_address_country = " . $country;
    }

    if($order_id)
    {
        $chart_order_filter = "   and order_id= " . $order_id;
    }

    $chart_sql = "Select order_item_quantity as quantity " .
           //"  , order_item_arrival as date ".
           "from order_items ".
           "left join orders on order_item_order = order_id ".
           "where order_item_item= " . $item_id .
           "    and order_item_arrival > '0000-00-00' " .
           "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
           $chart_date_filter1 . $chart_date_filter2 .
           $chart_country_filter .
           $chart_order_filter.
           "    and order_cancelled is null";

    $chart_res = mysql_query($chart_sql) or dberror($chart_sql);
    while ($chart_row = mysql_fetch_assoc($chart_res))
    {
        $chart_item_consumption[$i] = number_format($chart_row["quantity"],0);
		$i++;
    }
	
	return $chart_item_consumption;

}

function get_item_chart_arrival_date($item_id, $from_date, $to_date, $country, $order_id)
{

    $i = 0;
	
	$chart_arrival_date = array();

    // evaluate parameters
    $chart_arrival_date_filter1 = "";
    $chart_arrival_date_filter2 = "";
    $chart_arrival_country_filter = "";
    $chart_arrival_order_filter = "";

    if($from_date)
    {
        $from_date = from_system_date($from_date);
        $chart_date_filter1 = "    and order_item_arrival >=  '" . $from_date . "'";
    }

    if($to_date)
    {
        $to_date = from_system_date($to_date);
        $chart_date_filter2 = "    and order_item_arrival <=  '" . $to_date . "'";
    }

    if($country)
    {
        $chart_country_filter = "   and order_shop_address_country = " . $country;
    }

    if($order_id)
    {
        $chart_order_filter = "   and order_id= " . $order_id;
    }

    $chart_arrival_sql = "Select order_item_arrival as date " .
           "from order_items ".
           "left join orders on order_item_order = order_id ".
           "where order_item_item= " . $item_id .
           "    and order_item_arrival > '0000-00-00' " .
           "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
           $chart_arrival_date_filter1 . $chart_arrival_date_filter2 .
           $chart_arrival_country_filter .
           $chart_arrival_order_filter.
           "    and order_cancelled is null";

    $chart_arrival_res = mysql_query($chart_arrival_sql) or dberror($chart_arrival_sql);
    while ($chart_arrival_row = mysql_fetch_assoc($chart_arrival_res))
    {
        $chart_arrival_date[$i] = $chart_arrival_row["date"];
		$i++;
    }
	
	return $chart_arrival_date;

}

function get_chart_item_code($item_id)
{

    $chart_item_code = array();

    $chart_code_sql = "Select item_code " .
           "from items ".
           "where item_id = " . $item_id;

    $chart_code_res = mysql_query($chart_code_sql) or dberror($chart_code_sql);
    while ($chart_code_row = mysql_fetch_assoc($chart_code_res))
    {
        $chart_item_code = $chart_code_row["item_code"];
    }
	
	return $chart_item_code;
}

?>