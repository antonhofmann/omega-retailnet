<?php
/********************************************************************

    calculate_functions.php

    Various utility functions to get information from tables.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2003-06-22
    Modified by:    Stefan Jaberg (sja@mediaparx.ch)
    Date modified:  2004-07-08
    Version:        1.2.0

    Copyright (c) 2003, Swatch AG, All Rights Reserved.

*********************************************************************/

/*************************************************************************
   get item consumption data
*************************************************************************/
function get_item_total_consumption_data($item_id, $from_date, $to_date, $country, $order_id)
{

    $item_consumption = array();
    $turn_over = array();
    $turn_over_1 = array();

    // evaluate parameters
    $date_filter1 = "";
    $date_filter2 = "";
    $country_filter = "";
    $order_filter = "";

    if($from_date)
    {
        $from_date = from_system_date($from_date);
        $date_filter1 = "    and order_item_arrival >=  '" . $from_date . "'";
    }

    if($to_date)
    {
        $to_date = from_system_date($to_date);
        $date_filter2 = "    and order_item_arrival <=  '" . $to_date . "'";
    }

    if($country)
    {
        $country_filter = "   and order_shop_address_country = " . $country;
    }

    if($order_id)
    {
        $order_filter = "   and order_id= " . $order_id;
    }

    // calculate quantities having actual arrival date
    $item_consumption["item_consumption"] = number_format(0,2);
    $item_consumption["turn_over"] = number_format(0,2);
    $item_consumption["turn_over_1"] = 0;

    $sql = "Select sum(order_item_quantity) as quantity, " .
           "   sum(order_item_quantity * order_item_system_price) as turnover ".
           "from order_items ".
           "left join orders on order_item_order = order_id ".
           "where order_item_item= " . $item_id .
           "    and order_item_arrival > '0000-00-00' " .
           "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
           $date_filter1 . $date_filter2 .
           $country_filter .
           $order_filter.
           "    and order_cancelled is null";

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $item_consumption["item_consumption"] = number_format($row["quantity"],2);
        $item_consumption["turn_over"] = number_format($row["turnover"], 2);
        $item_consumption["item_consumption_1"] = $row["quantity"];
        $item_consumption["turn_over_1"] = $row["turnover"];
    }
    

    return $item_consumption;

}


/*************************************************************************
   get chart data
*************************************************************************/
function get_chart_data($item_id, $from_date, $to_date, $country, $order_id)
{

    $i = 0;
	
	$chart_item_consumption = array();
	$axis_labels = array();	
	
    // evaluate parameters
    $chart_date_filter1 = "";
    $chart_date_filter2 = "";
	$chart_date_filter3 = "";
	$chart_country_filter = "";
    $chart_order_filter = "";

	if(!$from_date)
    {
        $from_date = '31.10.02';
		$from_date = from_system_date($from_date);
        $chart_date_filter1 = "    and order_item_arrival >=  '" . $from_date . "'";
		
		$from_date_all = strtotime($from_date);
		$from_date_year = date("Y", $from_date_all);
		$from_date_month = date("m", $from_date_all);
		$from_date_day = date("d", $from_date_all);
	}
	
	if(!$to_date)
    {
        $to_date = date("d.m.Y");
		$to_date = from_system_date($to_date);
        $chart_date_filter2 = "    and order_item_arrival >=  '" . $to_date . "'";
		
		$to_date_all = strtotime($to_date);
		$to_date_year = date("Y", $to_date_all);
		$to_date_month = date("m", $to_date_all);
		$to_date_day = date("d", $to_date_all);
	}
	
	if($from_date)
    {
        $from_date = from_system_date($from_date);
        $chart_date_filter1 = "    and order_item_arrival >=  '" . $from_date . "'";
		
		$from_date_all = strtotime($from_date);
		$from_date_year = date("Y", $from_date_all);
		$from_date_month = date("m", $from_date_all);
		$from_date_day = date("d", $from_date_all);
	}

    if($to_date)
    {
		$to_date = from_system_date($to_date);
		$chart_date_filter2 = "    and order_item_arrival <=  '" . $to_date . "'";
		
		$to_date_all = strtotime($to_date);
		$to_date_year = date("Y", $to_date_all);
		$to_date_month = date("m", $to_date_all);
		$to_date_day = date("d", $to_date_all);
    }

    if($country)
    {
        $chart_country_filter = "   and order_shop_address_country = " . $country;
    }

    if($order_id)
    {
        $chart_order_filter = "   and order_id= " . $order_id;
	}	
	
	if($from_date_year < $to_date_year)
	{
		$month_axis = 1;
		
		$nr_months = ($to_date_all-$from_date_all)/2635200;
		$nr_months = number_format($nr_months, 0);
		
		$nr_years = $to_date_year-$from_date_year;
		
		if($nr_months > 12)
		{
			$month_factor = $nr_months/12;
			$month_factor = ceil($month_factor);
			$nr_months = $nr_months/$month_factor;
			$next_month = 0;
			$month_step = $month_factor-1;
			
			for($i = 0; $i <= $nr_months; $i++)
			{
				$month = mktime(0, 0, 0, date("m", $from_date_all)+$i+$next_month, date("d", $from_date_all),  date("Y", $from_date_all));
				$month = date("Y-m-d", $month);
				$next = mktime(0, 0, 0, date("m", $from_date_all)+$i+$next_month+$month_step, date("d", $from_date_all),  date("Y", $from_date_all));
				$next = date("Y-m-d", $next);
				$axis_labels[$i] = $month;
				
				$next_month = $next_month+$month_step;
				
				
				$chart_sql = "Select sum(order_item_quantity) as quantity " .
				   "from order_items ".
				   "left join orders on order_item_order = order_id ".
				   "where order_item_item= " . $item_id .
				   "    and order_item_arrival > '0000-00-00' " .
				   "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
				   "    and order_item_arrival >= '". $month . "' ".
				   "    and order_item_arrival < '". $next . "' ".
				   /*$chart_date_filter1 . $chart_date_filter2 .*/
				   $chart_country_filter .
				   $chart_order_filter.
				   "    and order_cancelled is null";
	
				$chart_res = mysql_query($chart_sql) or dberror($chart_sql);
				if ($chart_row = mysql_fetch_assoc($chart_res))
				{
					$chart_item_consumption[$i] = number_format($chart_row["quantity"],0);
				}
			}
		}
		else
		{
			for($i = 0; $i <= $nr_months; $i++)
			{
				$month = mktime(0, 0, 0, date("m", $from_date_all)+$i, date("d", $from_date_all),  date("Y", $from_date_all));
				$month = date("Y-m", $month);
				$axis_labels[$i] = $month;
				
				
				$chart_sql = "Select sum(order_item_quantity) as quantity " .
				   "from order_items ".
				   "left join orders on order_item_order = order_id ".
				   "where order_item_item= " . $item_id .
				   "    and order_item_arrival > '0000-00-00' " .
				   "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
				   "    and order_item_arrival LIKE '". $month . "%' ".
				   /*$chart_date_filter1 . $chart_date_filter2 .*/
				   $chart_country_filter .
				   $chart_order_filter.
				   "    and order_cancelled is null";
	
				$chart_res = mysql_query($chart_sql) or dberror($chart_sql);
				if ($chart_row = mysql_fetch_assoc($chart_res))
				{
					$chart_item_consumption[$i] = number_format($chart_row["quantity"],0);
				}
			}
		}
	}
	else if($from_date_year == $to_date_year && $from_date_month < $to_date_month)
	{
		if(($to_date_month-$from_date_month)<7)
		{
			$week_axis = 1;
			
			$nr_weeks = ($to_date_all-$from_date_all)/604800;
			$nr_weeks = number_format($nr_weeks, 0);
			
			$act_week = 0;
			
			for ($i = 0; $i <= $nr_weeks; $i++)
			{
				$calendar_week = mktime(0, 0, 0, date("m", $from_date_all), date("d", $from_date_all)+7+$act_week,  date("Y", $from_date_all));
				$calendar_week = (strftime("%W", $calendar_week));
				
				$week_day = mktime(0, 0, 0, date("m", $from_date_all), date("d", $from_date_all)+$act_week, date("Y", $from_date_all));
				$week_day = date("w", $week_day);
				
				$monday = date("Y-m-d", mktime(0, 0, 0, date("m", $from_date_all), date("d", $from_date_all)+$act_week-$week_day+1, date("Y", $from_date_all)));
				$sunday = date("Y-m-d", mktime(0, 0, 0, date("m", $from_date_all), date("d", $from_date_all)+$act_week-$week_day+7, date("Y", $from_date_all)));
			
				$chart_sql = "Select sum(order_item_quantity) as quantity " .
				   "from order_items ".
				   "left join orders on order_item_order = order_id ".
				   "where order_item_item= " . $item_id .
				   "    and order_item_arrival > '0000-00-00' " .
				   "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
				   "    and order_item_arrival >= '".$monday."' " .
				   "	and order_item_arrival <= '".$sunday."' " .
				   /*$chart_date_filter1 . $chart_date_filter2 .*/
				   $chart_country_filter .
				   $chart_order_filter.
				   "    and order_cancelled is null";
	
				$chart_res = mysql_query($chart_sql) or dberror($chart_sql);
				if ($chart_row = mysql_fetch_assoc($chart_res))
				{
					$chart_item_consumption[$i] = number_format($chart_row["quantity"],0);
				}
				
				$act_week = $act_week+7;
			}
		}
		else
		{
			$month_axis = 1;
			
			$nr_months = ($to_date_all-$from_date_all)/2635200;
			$nr_months = number_format($nr_months, 0);
			
			for($i = 0; $i <= $nr_months; $i++)
			{
				$month = mktime(0, 0, 0, date("m", $from_date_all)+$i, date("d", $from_date_all),  date("Y", $from_date_all));
				$month = date("Y-m", $month);
				$axis_labels[$i] = $month;
				
				
				$chart_sql = "Select sum(order_item_quantity) as quantity " .
				   "from order_items ".
				   "left join orders on order_item_order = order_id ".
				   "where order_item_item= " . $item_id .
				   "    and order_item_arrival > '0000-00-00' " .
				   "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
				   "    and order_item_arrival LIKE '". $month . "%' ".
				   /*$chart_date_filter1 . $chart_date_filter2 .*/
				   $chart_country_filter .
				   $chart_order_filter.
				   "    and order_cancelled is null";
	
				$chart_res = mysql_query($chart_sql) or dberror($chart_sql);
				if ($chart_row = mysql_fetch_assoc($chart_res))
				{
					$chart_item_consumption[$i] = number_format($chart_row["quantity"],0);
				}
			}
		}
	}
	else if($from_date_year == $to_date_year && $from_date_month == $to_date_month)
	{
		$day_axis = 1;
		
		$nr_days = $to_date_day-$from_date_day;
		
		for ($i = 0; $i <= $nr_days; $i++)
		{
			$act_day = $from_date_day+$i;
			
			if($act_day < 10)
			{
				$act_day = "0".$act_day;
			}
			
			$day = $from_date_year."-".$from_date_month."-".$act_day;
			
			$chart_sql = "Select sum(order_item_quantity) as quantity " .
			   "from order_items ".
			   "left join orders on order_item_order = order_id ".
			   "where order_item_item= " . $item_id .
			   "    and order_item_arrival > '0000-00-00' " .
			   "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
			   "    and order_item_arrival = '".$day."' " .
			   $chart_country_filter .
			   $chart_order_filter.
			   "    and order_cancelled is null";

			$chart_res = mysql_query($chart_sql) or dberror($chart_sql);
			if ($chart_row = mysql_fetch_assoc($chart_res))
			{
				$chart_item_consumption[$i] = number_format($chart_row["quantity"],0);
			}
		}
	}
	
	return $chart_item_consumption;
}

/*************************************************************************
   get chart x-axis labels
*************************************************************************/

function get_chart_labels($item_id, $from_date, $to_date, $country, $order_id)
{

    $i = 0;
	
	$chart_item_consumption = array();
	$axis_labels = array();	
	
    // evaluate parameters
    $chart_date_filter1 = "";
    $chart_date_filter2 = "";
	$chart_date_filter3 = "";
	$chart_country_filter = "";
    $chart_order_filter = "";

    if(!$from_date)
    {
        $from_date = '31.10.02';
		$from_date = from_system_date($from_date);
        $chart_date_filter1 = "    and order_item_arrival >=  '" . $from_date . "'";
		
		$from_date_all = strtotime($from_date);
		$from_date_year = date("Y", $from_date_all);
		$from_date_month = date("m", $from_date_all);
		$from_date_day = date("d", $from_date_all);
	}
	
	if(!$to_date)
    {
        $to_date = date("d.m.Y");
		$to_date = from_system_date($to_date);
        $chart_date_filter2 = "    and order_item_arrival >=  '" . $to_date . "'";
		
		$to_date_all = strtotime($to_date);
		$to_date_year = date("Y", $to_date_all);
		$to_date_month = date("m", $to_date_all);
		$to_date_day = date("d", $to_date_all);
	}
	
	if($from_date)
    {
        $from_date = from_system_date($from_date);
        $chart_date_filter1 = "    and order_item_arrival >=  '" . $from_date . "'";
		
		$from_date_all = strtotime($from_date);
		$from_date_year = date("Y", $from_date_all);
		$from_date_month = date("m", $from_date_all);
		$from_date_day = date("d", $from_date_all);
	}

    if($to_date)
    {
		$to_date = from_system_date($to_date);
		$chart_date_filter2 = "    and order_item_arrival <=  '" . $to_date . "'";
		
		$to_date_all = strtotime($to_date);
		$to_date_year = date("Y", $to_date_all);
		$to_date_month = date("m", $to_date_all);
		$to_date_day = date("d", $to_date_all);
    }

    if($country)
    {
        $chart_country_filter = "   and order_shop_address_country = " . $country;
    }

    if($order_id)
    {
        $chart_order_filter = "   and order_id= " . $order_id;
    }
	
	
	if($from_date_year < $to_date_year)
	{
		$month_axis = 1;
		
		$nr_months = ($to_date_all-$from_date_all)/2635200;
		$nr_months = number_format($nr_months, 0);
		
		$nr_years = $to_date_year-$from_date_year;
		
		if($nr_months > 12)
		{
			$month_factor = $nr_months/12;
			$month_factor = ceil($month_factor);
			$nr_months = $nr_months/$month_factor;
			$next_month = 0;
			$month_step = $month_factor-1;
			
			for($i = 0; $i <= $nr_months; $i++)
			{
				$month = mktime(0, 0, 0, date("m", $from_date_all)+$i+$next_month, date("d", $from_date_all),  date("Y", $from_date_all));
				$label = mktime(0, 0, 0, date("m", $from_date_all)+$i+$next_month, date("d", $from_date_all),  date("Y", $from_date_all));
				$month = date("Y-m", $month);
				$label = date("m.y", $label);
				$axis_labels[$i] = $label;
				
				$next_month = $next_month+$month_step;
			}
		}
		else
		{
			for($i = 0; $i <= $nr_months; $i++)
			{
				$month = mktime(0, 0, 0, date("m", $from_date_all)+$i, date("d", $from_date_all),  date("Y", $from_date_all));
				$label = mktime(0, 0, 0, date("m", $from_date_all)+$i, date("d", $from_date_all),  date("Y", $from_date_all));
				$month = date("Y-m", $month);
				$label = date("m.y", $label);
				$axis_labels[$i] = $label;
			}
		}
	}
	else if($from_date_year == $to_date_year && $from_date_month < $to_date_month)
	{
		if(($to_date_month-$from_date_month)<7)
		{
			$week_axis = 1;
			
			$nr_weeks = ($to_date_all-$from_date_all)/604800;
			$nr_weeks = number_format($nr_weeks, 0);
			
			$act_week = 0;
			
			for ($i = 0; $i <= $nr_weeks; $i++)
			{
				$calendar_week = mktime(0, 0, 0, date("m", $from_date_all), date("d", $from_date_all)+7+$act_week,  date("Y", $from_date_all));
				$calendar_week = (strftime("%W", $calendar_week));
				
				$axis_labels[$i] = $calendar_week;
				
				$act_week = $act_week+7;
			}
		}
		else
		{
			$month_axis = 1;
		
			$nr_months = ($to_date_all-$from_date_all)/2635200;
			$nr_months = number_format($nr_months, 0);
			
			for($i = 0; $i <= $nr_months; $i++)
			{
				$month = mktime(0, 0, 0, date("m", $from_date_all)+$i, date("d", $from_date_all),  date("Y", $from_date_all));
				$label = mktime(0, 0, 0, date("m", $from_date_all)+$i, date("d", $from_date_all),  date("Y", $from_date_all));
				$month = date("Y-m", $month);
				$label = date("m.y", $label);
				$axis_labels[$i] = $label;
			}
		}
	}
	else if($from_date_year == $to_date_year && $from_date_month == $to_date_month)
	{
		$day_axis = 1;
		
		$nr_days = $to_date_day-$from_date_day;
		
		for ($i = 0; $i <= $nr_days; $i++)
		{
			$day = $from_date_day+$i;
			
			if($day < 10)
			{
				$day = "0".$day;
			}
			
			$axis_labels[$i] = $day;
		}
	}
	
	return $axis_labels;

}

function get_chart_item_code($item_id)
{

    $chart_item_code = array();

    $chart_code_sql = "Select item_code " .
           "from items ".
           "where item_id = " . $item_id;

    $chart_code_res = mysql_query($chart_code_sql) or dberror($chart_code_sql);
    while ($chart_code_row = mysql_fetch_assoc($chart_code_res))
    {
        $chart_item_code = $chart_code_row["item_code"];
    }
	
	return $chart_item_code;
}

?>