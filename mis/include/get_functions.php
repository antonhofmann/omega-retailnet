<?php
/********************************************************************

    get.php

    Various utility functions to get information from tables.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-23
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-23
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/


/********************************************************************
    get the field values of a user
*********************************************************************/
function get_user($id)
{
    $user = array();

    if ($id == '')
    {
        $user["firstname"] = "";
        $user["name"] = "";
        $user["contact"] = "";
        $user["phone"] = "";
        $user["fax"] = "";
        $user["email"] = "";
        $user["cc"] = "";
        $user["deputy"] = "";
        $user["address"] = "";
		$user["country"] = "";
		$user["user_id"] = "";
		$user["password"] = "";
    }
    else
    {
        $sql = "select * from users " .
			   "left join addresses on address_id = user_address " . 
			   "where user_id = " . $id;

        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $user["firstname"] = $row["user_firstname"];
            $user["name"] = $row["user_name"];
            $user["contact"] = $row["user_name"] . " " . $row["user_firstname"];
            $user["phone"] = $row["user_phone"];
            $user["fax"] = $row["user_fax"];
            $user["email"] = $row["user_email"];
            $user["cc"] = $row["user_email_cc"];
            $user["deputy"] = $row["user_email_deputy"];
            $user["address"] = $row["user_address"];
			$user["country"] = $row["address_country"];
			$user["user_id"] = $row["user_login"];
			$user["password"] = $row["user_password"];
        }
    }
    return $user;
}


/********************************************************************
    get currency informations assigned to an order
*********************************************************************/
function get_order_currency($order_id)
{
    $currency = array();

    $sql = "select * from orders where order_id = " . $order_id;
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $currency_id = $row["order_client_currency"];
        $currency["exchange_rate"] = $row["order_client_exchange_rate"];
    }


    if ($currency_id > 0)
    {
        $sql = "select * from currencies where currency_id = " . $currency_id;
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $currency["id"] = $currency_id;
            $currency["symbol"] = $row["currency_symbol"];
            $currency["exchange_rate"] = $row["currency_exchange_rate"];
            $currency["factor"] = $row["currency_factor"];
        }
    }
    else
    {
        $currency["id"] = 0;
        $currency["symbol"] = "";
        $currency["exchange_rate"] = 0;
        $currency["factor"] = 0;
    }
    
    return $currency;
}


/********************************************************************
    get latest project from POS
*********************************************************************/

function get_latest_pos_project($pos_id)
{
	//get the latest project
	$sql = 'select posorder_id, project_id, project_number, posorder_order, ' . 
	       'order_actual_order_state_code, order_id, order_client_address  ' . 
		   'from posorders ' . 
		   'left join orders on order_id = posorder_order ' .
		   'left join projects on project_order = order_id  ' .
		   'where posorder_posaddress = ' . $pos_id . 
		   ' and (project_shop_closingdate is NULL or project_shop_closingdate = "0000-00-00") ' .
	       ' and project_actual_opening_date is not NULL and project_actual_opening_date <> "0000-00-00" ' .
		   ' and posorder_order > 0 ' .
		   ' and posorder_type = 1 ' .
		   ' and project_projectkind in (1,2, 3, 4) ' . 
		   ' order by project_id DESC ';

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		return $row;
	}
	

	return array();

}
?>