<?php
/********************************************************************

    generate_charts.php

    Enter parameters for the query of consumption

    Created by:     Stefan Jaberg (sja@mediaparx.ch)
    Date created:   2004-06-04
    Modified by:    Stefan Jaberg (sja@mediaparx.ch)
    Date modified:  2004-06-04
    Version:        1.0.0

    Copyright (c) 2003, Swatch AG, All Rights Reserved.

*********************************************************************/


echo "<br>";

/************
create charts
*************/

if(!$from_date)
{
	$from_date = '31.10.02';
	$from_date = from_system_date($from_date);
	$chart_date_filter1 = "    and order_item_arrival >=  '" . $from_date . "'";
	
	$from_date_all = strtotime($from_date);
	$from_date_year = date("Y", $from_date_all);
	$from_date_month = date("m", $from_date_all);
	$from_date_day = date("d", $from_date_all);
}
else
{
	$from_date = from_system_date($from_date);

	$from_date_all = strtotime($from_date);
	$from_date_year = date("Y", $from_date_all);
	$from_date_month = date("m", $from_date_all);
	$from_date_day = date("d", $from_date_all);
}

if(!$to_date)
{
	$to_date = date("d.m.Y");
	$to_date = from_system_date($to_date);
	$chart_date_filter2 = "    and order_item_arrival >=  '" . $to_date . "'";
	
	$to_date_all = strtotime($to_date);
	$to_date_year = date("Y", $to_date_all);
	$to_date_month = date("m", $to_date_all);
	$to_date_day = date("d", $to_date_all);
}
else
{
	$to_date = from_system_date($to_date);

	$to_date_all = strtotime($to_date);
	$to_date_year = date("Y", $to_date_all);
	$to_date_month = date("m", $to_date_all);
	$to_date_day = date("d", $to_date_all);
}


if($from_date_year < $to_date_year)
{
	$axis_title = "Month";
}
else if($from_date_year == $to_date_year && $from_date_month < $to_date_month)
{
	if(($to_date_month-$from_date_month)<7)
	{
		$axis_title = "Calendar Week";
	}
	else
	{
		$axis_title = "Month";
	}
}
else if($from_date_year == $to_date_year && $from_date_month == $to_date_month)
{
	$axis_title = "Day";
}

if($all_items)
{
	$item_filter = "item_type = 1";
	
	$row_count = 1;
	
	$sql = $sql_items . " where item_type = 1";
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$nr_items = $row_count;
		$row_count++;
	}
}

$nr_charts = $nr_items/5;

$nr_charts = ceil($nr_charts);

$chartURL = array();

$z = 0;

for($x = 1; $x <= $nr_charts; $x++)
{
	$data0 = array();
	$data1 = array();
	$data2 = array();
	$data3 = array();
	$data4 = array();
	$item_codes = array();
	
	$chart_values = array($data0, $data1, $data2, $data3, $data4); /*, $data5, $data6, $data7, $data8, $data9*/

	
	#Colors
	$colors = array(0x006699, 0xE10000, 0xE8E503, 0xE99F03, 0x04C809, 0x00E4EC, 0xDE00EC, 0x7AABC3, 0x9A9A9A, 0x710078);
	
	#Create a XYChart object
	$c = new XYChart(610, 220, 0xF6F6FF, 0xCCCCCC, 0);
	
	#Set the plotarea
	$c->setPlotArea(50, 15, 385, 160, 0xffffff, -1, -1, 0xc0c0c0, -1);
	
	#Add a legend box
	$legendObj = $c->addLegend(440, 15, true, "", 8);
	$legendObj->setBackground(Transparent);
	
	#Add a title to the y axis
	$c->yAxis->setTitle("Consumed");
	
	#Add a title to the x axis
	$c->xAxis->setTitle($axis_title);
	
	#Add a line layer to the chart
	$layer = $c->addLineLayer2();
	
	#Set the default line width to 2 pixels
	$layer->setLineWidth(2);
	
	#Add data sets to the line layer
	if ($form->button("execute"))
	{
		if ($form->validate())
		{
			
			if (!$all_items)
			{        
				$check_ids = array();
	
				foreach (array_keys($_REQUEST) as $key)
				{
					if (preg_match("/__check__(\d+)/", $key, $matches))
					{
						$check_ids[$matches[1]] = true;
					}
				}
				
				$item_records = array();
				$i = 0;
				
				foreach (array_keys($check_ids) as $id)
				{
					$item_records[$i] = $id;
					
					$i++;
				}
				
			}
			else
			{
				$item_filter = "item_type = 1";
	
				$item_records = array();
				$i = 0;
				
				$sql = $sql_items . " where item_type = 1 order by item_code";
				$res = mysql_query($sql) or dberror($sql);
				while ($row = mysql_fetch_assoc($res))
				{
					$item_records[$i] = $row["item_id"];
					
					$i++;
				}
			}
				
			$loop_end = $z+5;
			
			if($nr_items < $loop_end)
			{
				$loop_end = $nr_items;
			}
			
			$colors_index = 0;
			
			for($y = $z; $y < $loop_end; $y++)
			{	
				if ($item_filter == "")
				{
					$item_filter = $item_filter . " item_type = 1 and (item_id = " . $item_records[$y];
				}
				else
				{
					$item_filter = $item_filter . " or item_id = " . $item_records[$y];
				}
				
				$chart_data = get_chart_data($item_records[$y], $from_date, $to_date, $form->value("country"), "");
				
				if($colors_index > 4)
				{
					$colors_index = 0;
				}
				
				$chart_values[$y] = array_merge ($chart_values[$colors_index], $chart_data);
				
				$chart_item_code = get_chart_item_code($item_records[$y]);
				
				$item_codes[$y] = $chart_item_code;
				
				$layer->addDataSet($chart_values[$y], $colors[$colors_index], $item_codes[$y]);
			
				$colors_index++;
			}
			
			$z = $y;

			if ($item_filter == "")
			{
				// user has not choosen any items
				$item_filter = "item_type = 999999999999";
			}
			else
				{
				$item_filter = $item_filter . ")";
			}
		}
	}
	
	$chart_labels = get_chart_labels($item_records[$y-1], $from_date, $to_date, $form->value("country"), "");
					
	#The labels for the line chart
	$labels = $chart_labels;
	
	#Set the labels on the x axis
	$legend = $c->xAxis->setLabels($labels);
	$legend->setFontSize(6);

	# output the chart
	//header("Content-type: image/png");

	$_SESSION["img"] = $c->makeChart2(GIF);
	//print($c->makeChart2(GIF));
	
	//#Output the chart
	//$chartURL[$x] = $c->makeSession("chart".$x);
	
	echo "<img src=\"charts/map_handler.php\" border=\"0\"><br><br>";
	echo "<br><br>";
}


?>